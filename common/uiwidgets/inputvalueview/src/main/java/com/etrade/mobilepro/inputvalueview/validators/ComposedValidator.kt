package com.etrade.mobilepro.inputvalueview.validators

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.inputvalueview.InputFieldManager.Value

/**
 * This class is responsible to merge multiple validators into a single validator
 * @see InputFieldManager.Validator parent class that is used by InputFieldManager to
 * verify if the value is valid or not
 * @property validators a groups of validators that should be executed as one to verify
 * the value is valid or not
 * @constructor Creates a new Composed Validator using the validators, the order of
 * appearance is the order they will be apply to the value
 */
class ComposedValidator(private vararg val validators: InputFieldManager.Validator) : InputFieldManager.Validator {

    override fun validate(value: String): Value {
        for (validator in validators) {
            val partialValidationResult = validator.validate(value)
            if (partialValidationResult is Value.Invalid) {
                return partialValidationResult
            }
        }
        return Value.Valid(value)
    }
}
