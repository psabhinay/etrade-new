package com.etrade.mobilepro.chat.usecase

import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.api.ChatState
import com.etrade.mobilepro.chat.api.ChatState.Error
import com.etrade.mobilepro.chat.api.ChatState.Loading
import com.etrade.mobilepro.chat.api.ChatState.MessagesShown
import com.etrade.mobilepro.chat.api.ChatState.QueueShown
import com.etrade.mobilepro.chat.api.ChatState.TerminatedByAgent
import com.etrade.mobilepro.chat.api.ChatState.TerminatedByUser
import com.etrade.mobilepro.chat.api.ChatState.Unknown
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType.AgentDisconnect
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType.AgentNotTyping
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType.AgentTyping
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType.ChatEnded
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType.ChatEstablished
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType.ChatRequestFail
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType.ChatRequestSuccess
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType.ChatTransferred
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType.QueueUpdate
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType.TransferToButtonInitiated
import com.etrade.mobilepro.chat.api.message.models.ChatRequestModel
import com.etrade.mobilepro.chat.api.message.models.ChatRetrieveMessageItemModel
import com.etrade.mobilepro.chat.api.message.models.ChatRetrievedMessageModel
import com.etrade.mobilepro.chat.api.message.repo.ChatMessagesRepo
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.util.UseCase
import kotlinx.collections.immutable.toImmutableList
import javax.inject.Inject

interface ChatRequestUseCase : UseCase<ChatRequestModel, ETResult<ChatState>> {
    fun clearMessages()

    fun getMessages(): List<ChatMessage>
}

class ChatRequestUseCaseImpl @Inject constructor(private val chatMessagesRepo: ChatMessagesRepo) : ChatRequestUseCase {

    private val messagesList = mutableListOf<ChatMessage>()
    private var unread = 0

    override fun clearMessages() {
        messagesList.clear()
    }

    override fun getMessages() = messagesList.toImmutableList()

    override suspend fun execute(parameter: ChatRequestModel): ETResult<ChatState> =
        when (parameter) {
            is ChatRequestModel.GetMessages -> getMessages(parameter)
            is ChatRequestModel.PostMessage -> postMessage(parameter)
            is ChatRequestModel.TerminateChat -> terminateChat(parameter)
        }

    private suspend fun getMessages(parameter: ChatRequestModel.GetMessages) =
        chatMessagesRepo.getMessages(parameter).map { convertToChatState(it) }

    private suspend fun terminateChat(parameter: ChatRequestModel.TerminateChat) =
        chatMessagesRepo.terminateChat(parameter).map { TerminatedByUser }

    private suspend fun postMessage(parameter: ChatRequestModel.PostMessage) =
        chatMessagesRepo.postMessage(parameter).map {
            requireNotNull(parameter.message).let { message ->
                messagesList.add(ChatMessage.User(message))
            }
            MessagesShown(messagesList)
        }

    private fun handleQueueUpdate(model: ChatRetrieveMessageItemModel): ChatState {
        // ETAND-14369: we should display minutes and hours and chat queue position should be removed
        val estimatedWaitTime = model.message.estimatedWaitTime
        return if (estimatedWaitTime == null) {
            Error(model.message.reason)
        } else {
            QueueShown(estimatedWaitTime)
        }
    }

    private fun convertToChatState(model: ChatRetrievedMessageModel): ChatState {
        var chatState: ChatState = Loading
        val items = model.data.items ?: return chatState

        return if (items.isEmpty()) {
            Unknown
        } else {
            unread = 0
            items.forEach {
                chatState = mapMessageModelToState(it)
            }
            chatState
        }
    }

    @Suppress("ComplexMethod")
    private fun mapMessageModelToState(model: ChatRetrieveMessageItemModel): ChatState {
        return when (model.type) {
            AgentDisconnect -> QueueShown(estimatedWaitTime = 0, isAgentDisconnected = true)
            AgentTyping -> handleAgentTyping()
            AgentNotTyping -> handleAgentNotTyping()
            ChatEnded -> TerminatedByAgent
            ChatEstablished -> MessagesShown(messagesList)
            ChatRequestFail -> Error(model.message.reason)
            ChatRetrieveMessageType.ChatMessage -> handleChatMessage(model)
            ChatTransferred -> handleChatAgentTransfer(ChatMessage.Agent(model.message.name ?: ""))
            ChatRequestSuccess,
            QueueUpdate -> handleQueueUpdate(model)
            TransferToButtonInitiated -> handleChatAgentTransfer(ChatMessage.TransferQueue)
            ChatRetrieveMessageType.Unknown -> Unknown
        }
    }

    private fun handleAgentNotTyping() = MessagesShown(
        messagesList.toMutableList().filterNot { message -> message is ChatMessage.AgentTyping },
        unread
    )

    private fun handleAgentTyping() = if (messagesList.contains(ChatMessage.AgentTyping)) {
        MessagesShown(messagesList)
    } else {
        MessagesShown(
            messagesList.toMutableList().apply { add(ChatMessage.AgentTyping) },
            unread
        )
    }

    private fun handleChatMessage(model: ChatRetrieveMessageItemModel): ChatState {
        model.message.text?.let { content ->
            messagesList.add(ChatMessage.Agent(content))
            unread++
        }
        return MessagesShown(messagesList, unread)
    }

    private fun handleChatAgentTransfer(chatMessage: ChatMessage): ChatState {
        messagesList.add(chatMessage)
        return ChatState.ChatTransferredMessage(messagesList)
    }
}
