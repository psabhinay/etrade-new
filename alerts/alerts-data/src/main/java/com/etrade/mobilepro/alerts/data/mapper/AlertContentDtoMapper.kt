package com.etrade.mobilepro.alerts.data.mapper

import com.etrade.mobilepro.alerts.api.model.AlertMessage
import com.etrade.mobilepro.alerts.data.dto.AlertMessageDto

private const val STYLE_START =
    "<!DOCTYPE html><html><head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"></head><body>"
private const val STYLE_END = "</body></html>"

// For some reason the server sends this in every response
private val htmlErrorFixes = mapOf(
    "width=\"578\"" to "width=\"0\""
)

// we need to remove hidden text in the footer. This text is between(inclusive) these strings. // ETAND-2731
// deletion will be applied sequentially, from the first Pair to the last
private val stringRemovalIntervals = listOf(
    "<span class=\"f1\" style=\"visibility:hidden\"" to "</span>"
)

internal class AlertContentDtoMapper {
    fun map(xmlDto: AlertMessageDto): AlertMessage {
        return AlertMessage(
            id = xmlDto.id ?: "",
            createdAtFormatted = xmlDto.createdAt ?: "",
            readAtFormatted = xmlDto.readAt ?: "",
            content = fixContent(xmlDto.content ?: "")
        )
    }

    private fun fixContent(input: String): String =
        STYLE_START + input.replaceHtmlErrors().removeUnnecessaryFooter() + STYLE_END
}

private fun String.replaceHtmlErrors(): String {
    var updated = this
    htmlErrorFixes.forEach { entry ->
        updated = updated.replace(entry.key, entry.value, ignoreCase = true)
    }
    return updated
}

private fun String.removeUnnecessaryFooter(): String {
    var updated = this
    stringRemovalIntervals.forEach { pair ->
        val startIndex = updated.indexOf(pair.first)
        val endIndex = updated.indexOf(pair.second)
        if (startIndex >= 0 && endIndex >= 0 && startIndex <= endIndex) {
            updated = updated.removeRange(startIndex, endIndex + pair.second.length)
        }
    }
    return updated
}
