package com.etrade.mobilepro.encryption.data

import android.util.Base64
import com.etrade.mobilepro.encryption.api.Base64Processor
import javax.inject.Inject

class DefaultBase64Processor @Inject constructor() : Base64Processor {
    override fun encode(input: ByteArray): String {
        return Base64.encodeToString(input, Base64.DEFAULT)
    }

    override fun decode(input: String): ByteArray {
        return Base64.decode(input, Base64.DEFAULT)
    }
}
