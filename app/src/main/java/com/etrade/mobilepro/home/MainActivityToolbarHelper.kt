package com.etrade.mobilepro.home

import android.content.res.Resources
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.etrade.mobilepro.R
import com.etrade.mobilepro.accounts.util.android.view.AccountsSpinnerModel
import com.etrade.mobilepro.common.ToolbarUpdateBlocker
import com.etrade.mobilepro.dropdown.DropDownManager
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.displayDropDownArrow
import com.etrade.mobilepro.util.android.invisibleIf

class MainActivityToolbarHelper(
    private val toolbarView: Toolbar,
    private val accountsDropDownManager: DropDownManager<AccountsSpinnerModel>,
    private val toolbarViewModel: ToolbarViewModel,
    private val owner: LifecycleOwner,
    private val mainActivityMenuHelper: MainActivityMenuHelper,
    private val toolbarTitleView: TextView?,
    private val fragmentManager: FragmentManager,
    private val isUserLoggedIn: () -> Boolean
) {
    fun bindToolbarViewModel() {
        toolbarViewModel.apply {
            displayDropDownArrow.observeIfAllowed { toolbarTitleView?.displayDropDownArrow(it) }
            isSearchVisible.observeIfAllowed { mainActivityMenuHelper.isSearchVisible = it }
            toolbarContentDescription.observeIfAllowed { toolbarTitleView?.contentDescription = it }
            toolbarTitle.observeIfAllowed(::onTitleUpdated)
            dropDownEnabled.observeIfAllowed(::onDropDownEnabled)
            resetToolbar.observe(::onResetToolbar)
            isVisible.observe { visible -> toolbarView.visibility = if (visible) View.VISIBLE else View.GONE }
        }
    }

    private fun onTitleUpdated(titleText: CharSequence) {
        toolbarTitleView?.apply {
            text = titleText
            invisibleIf(
                titleText.isBlank() || resources.alwaysHide(titleText) ||
                    resources.hideOnAuth(isUserLoggedIn(), titleText)
            )
        }
        toolbarViewModel.dropDownEnabled.apply {
            removeObservers(owner)
            observeIfAllowed(::onDropDownEnabled)
        }
    }

    private fun onResetToolbar(event: ConsumableLiveEvent<Unit>) {
        event.consume { bindToolbarViewModel() }
    }

    private fun onDropDownEnabled(enabled: Boolean?) {
        enabled?.let {
            if (!toolbarViewModel.isToolBarLocked) {
                if (it && toolbarViewModel.isTitleNavigationEnabled) {
                    toolbarTitleView?.setOnClickListener {
                        accountsDropDownManager.openDropDown()
                    }
                } else {
                    toolbarTitleView?.setOnClickListener(null)
                }
                toolbarTitleView?.isClickable = it && toolbarViewModel.isTitleNavigationEnabled
            }
        }
    }

    private fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) = observe(owner, Observer { onChanged(it) })
    private fun <T> LiveData<T>.observeIfAllowed(onChanged: (T) -> Unit) = observe(
        owner,
        Observer {
            if ((fragmentManager.getCurrentNavigationFragment() as? ToolbarUpdateBlocker)?.isToolbarUpdateBlocked != true) {
                onChanged(it)
            }
        }
    )
}

private fun Resources.hideOnAuth(isAuthenticated: Boolean, titleText: CharSequence?): Boolean {
    if (!isAuthenticated) {
        return false
    }
    return when (titleText) {
        getString(R.string.title_accounts) -> true // when user authenticated overview behave as viewPager fragment
        else -> false
    }
}

private fun Resources.alwaysHide(titleText: CharSequence?) =
    when (titleText) {
        getString(R.string.title_complete_view) -> true
        else -> false
    }
