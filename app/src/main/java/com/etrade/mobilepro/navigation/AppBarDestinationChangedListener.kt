package com.etrade.mobilepro.navigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.FloatingWindow
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.getNavigationButtonDescription
import com.etrade.mobilepro.home.ToolbarViewModel
import com.etrade.mobilepro.home.topLevelDestinations

class AppBarDestinationChangedListener(
    private val activity: AppCompatActivity,
    private val toolbarViewModel: ToolbarViewModel
) : NavController.OnDestinationChangedListener {

    /**
     * Collection of non-standard up indicators for custom fragments.
     */
    private val upIndicatorIconMap = mapOf(
        R.id.chatMessagesFragment to R.drawable.ic_close,
        R.id.chatSetupFragment to R.drawable.ic_close,
        R.id.checkDepositServiceAgreement to R.drawable.ic_close,
        R.id.depositConfirmationFragment to R.drawable.ic_close,
        R.id.etfResultsFragment to R.drawable.ic_close,
        R.id.prospectusFragment to R.drawable.ic_close,
        R.id.ms_accounts_web_view_fragment to R.drawable.ic_close,
        R.id.mutualFundResultsFragment to R.drawable.ic_close,
        R.id.orderConfirmationFragment to R.drawable.ic_close,
        R.id.screenersDisclosureFragment to R.drawable.ic_close,
        R.id.stockResultsFragment to R.drawable.ic_close,
        R.id.tradeCalculatorFragment to R.drawable.ic_close,
        R.id.tradeMutualFundDisclosuresFragment to R.drawable.ic_close,
        R.id.marketMoversFragment to R.drawable.ic_close,
        R.id.widget_settings_fragment to R.drawable.ic_close,
        R.id.personalNotificationsFragment to R.drawable.ic_close,
        R.id.portfolioNewsFragment to R.drawable.ic_close
    )

    override fun onDestinationChanged(controller: NavController, destination: NavDestination, arguments: Bundle?) {
        if (destination !is FloatingWindow) {
            if (topLevelDestinations.contains(destination.id)) {
                activity.supportActionBar?.setDisplayHomeAsUpEnabled(false)
            } else {
                activity.supportActionBar?.apply {
                    setDisplayHomeAsUpEnabled(true)
                    val icon = upIndicatorIconMap.getOrElse(destination.id) { R.drawable.ic_arrow_back }
                    setHomeAsUpIndicator(icon)
                    setHomeActionContentDescription(activity.getNavigationButtonDescription(icon))
                }
            }
        }
        toolbarViewModel.destinationNeedsToBeLocked(destination)
    }
}
