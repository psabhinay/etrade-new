package com.etrade.mobilepro.screener.presentation.common

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.withStyledAttributes
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.util.android.extension.inflate

class ScreenerSelectorItem : LinearLayout {

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    lateinit var titleView: TextView
    lateinit var selectedValueTextView: TextView

    var selectedValue: CharSequence
        get() = selectedValueTextView.text
        set(value) {
            selectedValueTextView.text = value
        }

    var selectionEnabled: Boolean
        get() = selectedValueTextView.isEnabled
        set(value) {
            isEnabled = value
            titleView.isEnabled = value
            selectedValueTextView.isEnabled = value
            updateContentDescription(value)
        }

    private fun init(attributeSet: AttributeSet?) {
        orientation = VERTICAL
        importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        inflate(R.layout.item_screener_selector, true)
        titleView = findViewById(R.id.title_selector)
        selectedValueTextView = findViewById(R.id.value_selector)
        context.withStyledAttributes(attributeSet, R.styleable.ScreenerSelectorItem) {
            titleView.text = getString(R.styleable.ScreenerSelectorItem_selectorTitle)
        }
    }

    private fun updateContentDescription(isEnabled: Boolean) {
        contentDescription = if (isEnabled) null else "${titleView.text} ${selectedValueTextView.text}"
    }
}
