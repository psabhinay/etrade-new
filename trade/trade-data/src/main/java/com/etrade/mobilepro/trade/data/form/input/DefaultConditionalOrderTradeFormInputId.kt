package com.etrade.mobilepro.trade.data.form.input

import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.data.form.CommonAllOrNoneValueCoder
import com.etrade.mobilepro.trade.data.form.DefaultSymbolInputValueCoder
import com.etrade.mobilepro.trade.data.form.value.CommonOrderTermValueId
import com.etrade.mobilepro.trade.data.form.value.ConditionalOrderActionValueId
import com.etrade.mobilepro.trade.data.form.value.DefaultConditionalPriceTypeValueId
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.trade.form.api.value.ConditionalPriceTypeValueId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.util.ValueCoder

object DefaultConditionalOrderTradeFormInputId :
    ConditionalOrderTradeFormInputId,
    TradeFormInputId by DefaultTradeFormInputId {
    override val walkthroughFormCompleted: String = ""
    override val accountId: String = "accountid"
    override val action: String = "transaction"
    override val actionValueId: ActionValueId = ConditionalOrderActionValueId
    override val allOrNone: String = "aon"
    override val allOrNoneValueCoder: ValueCoder<Boolean> = CommonAllOrNoneValueCoder
    override val limitPrice: String = "price"
    override val limitPriceForStopLimit: String = "price (Stop Limit on Quote)"
    override val orderId: String = "OrderId"
    override val priceType: String = "pricetype"
    override val priceTypeValueId: ConditionalPriceTypeValueId = DefaultConditionalPriceTypeValueId
    override val underlierSymbol: String = "underlier"
    override val stopPrice: String = "stopprice"
    override val stopPriceForStopLimit: String = "stopprice (Stop Limit on Quote)"
    override val term: String = "orderterm"
    override val termValueId: OrderTermValueId = CommonOrderTermValueId
    override val symbol: String = "symbol"
    override val symbolCoder: ValueCoder<WithSymbolInfo> = DefaultSymbolInputValueCoder
    override val quantity: String = "quantity"
    override val offsetValue: String = "offsetvalue"
}
