package com.etrade.mobilepro.bank.transactions

import com.etrade.mobilepro.bank.BR
import com.etrade.mobilepro.bank.R
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.transactions.Transaction

class BankTransactionViewModel(
    private val transaction: Transaction
) : DynamicViewModel(R.layout.adapter_transaction_item), Transaction by transaction {

    override val variableId: Int = BR.obj
}
