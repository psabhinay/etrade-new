package com.etrade.mobilepro.indices

import androidx.annotation.ColorRes
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.util.android.ChangeColorPicker
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.formatters.formatMarketPercentDataCustom
import com.etrade.mobilepro.util.safeParseBigDecimal

data class IndexDataWithColor constructor(
    val ticker: String,
    val displayName: String,
    val last: String = "--",
    val change: String = "--",
    val changePercentage: String = "--%",
    @ColorRes val textColorRes: Int,
    @ColorRes val indicatorColor: Int,
    val isPositiveChange: Boolean
)

internal fun createIndexDataWithColor(
    indexDataParameter: IndexDataParameter
): IndexDataWithColor {
    indexDataParameter.apply {
        val decimalChange = change?.safeParseBigDecimal()
        val textColor = changeColorPicker.getChangeColor(decimalChange)
        val percentChangeWithoutSign = percentChange?.replace("%", "")
        val formattedPercentChange: String = MarketDataFormatter.formatMarketPercentDataCustom(percentChangeWithoutSign?.toBigDecimalOrNull())
        val formattedChange: String = MarketDataFormatter.formatMarketDataCustom(decimalChange)
        val calculatedIndicatorColor = if (useMarketChangeColor) {
            textColor
        } else {
            indicatorColor
        }

        return IndexDataWithColor(
            ticker = symbol,
            displayName = symbolDesc,
            last = price,
            change = "$formattedChange ($formattedPercentChange)",
            textColorRes = textColor,
            indicatorColor = calculatedIndicatorColor,
            isPositiveChange = decimalChange?.signum() != -1
        )
    }
}

internal data class IndexDataParameter constructor(
    val symbol: String,
    val symbolDesc: String,
    val price: String,
    val change: String?,
    val percentChange: String?,
    @ColorRes val indicatorColor: Int,
    val changeColorPicker: ChangeColorPicker,
    val useMarketChangeColor: Boolean = false
)
