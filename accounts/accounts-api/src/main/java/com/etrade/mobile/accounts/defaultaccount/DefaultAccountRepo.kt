package com.etrade.mobile.accounts.defaultaccount

import com.etrade.mobilepro.common.result.ETResult

interface DefaultAccountRepo {
    suspend fun getAccountId(): ETResult<String?>
    suspend fun setAccountId(accountId: String?)
    suspend fun getDisplayName(): ETResult<String?>
}
