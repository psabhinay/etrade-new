package com.etrade.mobilepro.news.data.repo.datasource

import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.data.dto.NewsDto
import com.etrade.mobilepro.news.data.rest.NewsApiClient
import io.reactivex.Observable
import io.reactivex.Single

internal class CompanyNewsDataSource(
    private val newsApiClient: NewsApiClient,
    requestParams: NewsRequest?,
    headerItemsSource: Observable<List<NewsItem>>?
) : NewsPagingSource(requestParams, headerItemsSource) {
    override fun getNewsRange(startIndex: Int, size: Int, requestParams: NewsRequest?): Single<NewsDto> {
        return when (requestParams) {
            is NewsRequest.Symbol -> newsApiClient.getSymbolNews(requestParams.joinSymbols(), startIndex, size)
            else -> throw IllegalArgumentException("Wrong request params type for company news, got Portfolio")
        }
    }
}
