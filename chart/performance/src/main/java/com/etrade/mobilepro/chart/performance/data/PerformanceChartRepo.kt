package com.etrade.mobilepro.chart.performance.data

import com.etrade.mobilepro.chart.performance.model.PerformanceChartData
import com.etrade.mobilepro.chartengine.TimeFrame
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable

interface PerformanceChartRepo {
    fun getPerformanceChart(accounts: List<String>, indexes: List<String>, period: TimeFrame): Observable<Resource<PerformanceChartData>>
}
