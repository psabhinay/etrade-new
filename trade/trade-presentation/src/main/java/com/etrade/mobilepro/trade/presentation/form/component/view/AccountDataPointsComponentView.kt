package com.etrade.mobilepro.trade.presentation.form.component.view

import android.content.Context
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.ui.ComponentView
import com.etrade.mobilepro.trade.presentation.AccountDataPoint
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.databinding.TradeViewAccountDataPointsBinding
import com.etrade.mobilepro.trade.presentation.form.component.AccountDataPointsComponent
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.extension.inflater
import com.etrade.mobilepro.util.android.goneUnless

internal class AccountDataPointsComponentView(context: Context) : FrameLayout(context), ComponentView<AccountDataPointsComponent> {

    private var binding = TradeViewAccountDataPointsBinding.inflate(inflater, this, true)

    private val verticalMarginDelta = context.resources.getDimensionPixelSize(R.dimen.spacing_small)

    private val dataPointsObserver: Observer<List<AccountDataPoint>> = Observer { dataPoints ->
        binding.sectionLayoutView.setContent(dataPoints.map { it.toTableSectionItemLayout() })
    }

    private val loadingIndicatorObserver: Observer<Boolean> = Observer { loading ->
        binding.progressView.goneUnless(loading)
    }

    private val needsRetryObserver: Observer<Boolean> = Observer { needsRefresh ->
        binding.retryButtonView.goneUnless(needsRefresh)
    }

    private var currentComponent: AccountDataPointsComponent? = null

    init {
        binding.retryButtonView.setOnClickListener {
            currentComponent?.refresh()
        }
    }

    override fun setLayoutParams(params: ViewGroup.LayoutParams) {
        if (params is MarginLayoutParams) {
            params.topMargin = verticalMarginDelta
        }
        super.setLayoutParams(params)
    }

    override fun render(component: AccountDataPointsComponent, owner: LifecycleOwner) {
        currentComponent?.apply {
            dataPoints.removeObserver(dataPointsObserver)
            loadingIndicator.removeObserver(loadingIndicatorObserver)
            needsRetry.removeObserver(needsRetryObserver)
        }

        currentComponent = component

        component.apply {
            dataPoints.observe(owner, dataPointsObserver)
            loadingIndicator.observe(owner, loadingIndicatorObserver)
            needsRetry.observe(owner, needsRetryObserver)
        }
    }
}

internal fun AccountDataPoint.toTableSectionItemLayout(): TableSectionItemLayout = createSummarySection(label = label, value = value)
