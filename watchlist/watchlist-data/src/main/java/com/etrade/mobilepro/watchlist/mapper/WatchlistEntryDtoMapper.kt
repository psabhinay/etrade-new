package com.etrade.mobilepro.watchlist.mapper

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.watchlist.dto.WatchlistEntryDto
import com.etrade.mobilepro.watchlistapi.WatchlistEntry

internal class WatchlistEntryDtoMapper {
    fun map(jsonDto: WatchlistEntryDto, watchlistId: String): WatchlistEntry {
        val watchlistEntry = WatchlistEntry(
            ticker = jsonDto.ticker ?: "",
            description = jsonDto.description,
            entryID = jsonDto.entryId ?: jsonDto.entryID,
            underlyingSymbol = jsonDto.underLyingSymbol,
            typeCode = jsonDto.typeCode?.let { InstrumentType.from(it) },
            lastTradeTime = jsonDto.lastTradeTime,
            lastTradeTimeStamp = jsonDto.lastTradeTimeStamp?.toLongOrNull(),
            lastPrice = jsonDto.lastPrice?.toBigDecimalOrNull(),
            change = jsonDto.change?.toBigDecimalOrNull(),
            changePercent = jsonDto.changePercent?.toBigDecimalOrNull(),
            volume = jsonDto.volume?.toBigDecimalOrNull(),
            bid = jsonDto.bid?.toBigDecimalOrNull(),
            ask = jsonDto.ask?.toBigDecimalOrNull(),
            dayHigh = jsonDto.dayHigh?.toBigDecimalOrNull(),
            dayLow = jsonDto.dayLow?.toBigDecimalOrNull(),
            week52High = jsonDto.week52High?.toBigDecimalOrNull(),
            week52Low = jsonDto.week52Low?.toBigDecimalOrNull(),
            priceEarningsRatio = jsonDto.priceEarningsRatio?.toBigDecimalOrNull(),
            earningsPerShare = jsonDto.earningsPerShare?.toBigDecimalOrNull()
        )
        watchlistEntry.watchlistId = watchlistId
        return watchlistEntry
    }
}
