package com.etrade.mobilepro.dynamic.form.api

import androidx.lifecycle.LiveData

interface InputHolder {
    fun <T : Input<*>> getInput(id: String): T?
}

abstract class BaseInputHolder(internal val inputMap: Map<String, Input<Any>>) : InputHolder {
    @Suppress("UNCHECKED_CAST")
    final override fun <T : Input<*>> getInput(id: String): T? = inputMap[id] as? T
}

fun <T : Input<*>> InputHolder.requireInput(id: String): T {
    return requireNotNull(getInput(id)) {
        "Input \"$id\" is missing."
    }
}

fun InputHolder.requireAnyInput(id: String): Input<Any> = requireInput(id)

fun InputHolder.inputParameterValue(id: String): LiveData<Any?> = requireAnyInput(id).parameterValue

fun <T> InputHolder.inputValue(id: String): LiveData<T?> = requireInput<Input<T>>(id).value
