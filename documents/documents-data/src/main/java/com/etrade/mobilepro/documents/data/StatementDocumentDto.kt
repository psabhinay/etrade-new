package com.etrade.mobilepro.documents.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StatementDocumentDto(
    @Json(name = "documentId")
    val documentId: String,
    @Json(name = "institutionType")
    val institutionType: String,
    @Json(name = "postEdgeStatementDate")
    val title: String,
    @Json(name = "postEdgeFolder")
    val postEdgeFolder: String,
    @Json(name = "accountUuid")
    val accountUuid: String
)
