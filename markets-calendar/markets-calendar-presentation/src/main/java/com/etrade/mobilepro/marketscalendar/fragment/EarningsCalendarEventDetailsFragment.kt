package com.etrade.mobilepro.marketscalendar.fragment

import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.viewmodel.EtNavigationViewModel
import com.etrade.mobilepro.earnings.EarningsDetailsOverlayFragmentArgs
import com.etrade.mobilepro.marketscalendar.MarketCalendarRouter
import com.etrade.mobilepro.marketscalendar.R
import com.etrade.mobilepro.marketscalendar.adapter.delegate.EarningsEventDetailsAdapterDelegate
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import javax.inject.Inject

@RequireLogin
class EarningsCalendarEventDetailsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    router: MarketCalendarRouter
) : CalendarEventDetailsFragment(
    viewModelFactory
) {

    private val etNavigationViewModel: EtNavigationViewModel by activityViewModels()

    override val item: EventDetails? by lazy { router.getEarningEventDetailsItem(arguments) }

    override fun createAdapterDelegate(layoutInflater: LayoutInflater, noValue: String): AdapterDelegate<MutableList<EventDetails>> {
        return EarningsEventDetailsAdapterDelegate(noValue) {
            openEarningsDetails()
        }
    }

    private fun openEarningsDetails() {
        item?.symbol?.let {
            etNavigationViewModel.navigate(object : NavDirections {
                override fun getArguments(): Bundle = EarningsDetailsOverlayFragmentArgs(it).toBundle()

                override fun getActionId(): Int = R.id.open_earnings_details_overlay
            })
        }
    }
}
