package com.etrade.mobilepro.tracking.params

import com.etrade.mobilepro.alerts.api.model.AlertValueType
import com.etrade.mobilepro.tracking.EVENT_ATTRIBUTE_CUSTOMER_ID
import com.etrade.mobilepro.tracking.EVENT_ATTRIBUTE_SYMBOL
import com.etrade.mobilepro.tracking.UNSPECIFIED

private const val EVENT_NAME = "Set Market Alert"

private const val EVENT_ATTRIBUTE_LOW_TARGET_PRICE = "Low Target Price"
private const val EVENT_ATTRIBUTE_HIGH_TARGET_PRICE = "High Target Price"
private const val EVENT_ATTRIBUTE_LOW_DAILY_PERCENT = "Low Daily Percent"
private const val EVENT_ATTRIBUTE_HIGH_DAILY_PERCENT = "High Daily Percent"
private const val EVENT_ATTRIBUTE_HIGH_PE_TARGET = "High PE Target"
private const val EVENT_ATTRIBUTE_LOW_PE_TARGET = "Low PE Target"
private const val EVENT_ATTRIBUTE_DAILY_VOLUME = "Daily Volume"

class SetMarketAlertAnalyticsParams(
    private val symbol: String,
    private val customerId: String,
    private val attributes: Map<AlertValueType, String>
) : AnalyticsParams {

    override val name: String = EVENT_NAME

    override val params: Map<String, String>
        get() = mapOf(
            EVENT_ATTRIBUTE_SYMBOL to symbol,
            EVENT_ATTRIBUTE_CUSTOMER_ID to customerId,
            EVENT_ATTRIBUTE_LOW_TARGET_PRICE to getAttributeValue(AlertValueType.LOW_TARGET_PRICE),
            EVENT_ATTRIBUTE_HIGH_TARGET_PRICE to getAttributeValue(AlertValueType.HIGH_TARGET_PRICE),
            EVENT_ATTRIBUTE_LOW_DAILY_PERCENT to getAttributeValue(AlertValueType.LOW_DAILY_PERCENT),
            EVENT_ATTRIBUTE_HIGH_DAILY_PERCENT to getAttributeValue(AlertValueType.HIGH_DAILY_PERCENT),
            EVENT_ATTRIBUTE_HIGH_PE_TARGET to getAttributeValue(AlertValueType.HIGH_PE_TARGET),
            EVENT_ATTRIBUTE_LOW_PE_TARGET to getAttributeValue(AlertValueType.LOW_PE_TARGET),
            EVENT_ATTRIBUTE_DAILY_VOLUME to getAttributeValue(AlertValueType.DAILY_VOLUME)
        )

    private fun getAttributeValue(key: AlertValueType): String = attributes[key] ?: UNSPECIFIED
}
