package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input

private const val MAX_PHONE_LENGTH = 10

class UsPhoneLengthValidator(
    errorMessage: String
) : PhoneLengthValidator(
    MAX_PHONE_LENGTH,
    errorMessage
)
