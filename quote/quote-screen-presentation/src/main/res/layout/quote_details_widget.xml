<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <variable
            name="quoteItem"
            type="com.etrade.mobilepro.quoteapi.QuoteWidgetItem" />

    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:id="@+id/quote_details_widget_root_layout"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:focusable="true"
        android:importantForAccessibility="yes"
        android:padding="@dimen/spacing_medium"
        app:processQuoteWithBidAskDescription="@{quoteItem.description}">

        <TextView
            android:id="@+id/quote_widget_title"
            style="@style/QuoteWidget.Trade"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:importantForAccessibility="no"
            android:paddingBottom="@dimen/spacing_xsmall"
            android:text="@{quoteItem.title}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            tools:text="PMDV - Poly Media Division" />

        <TextView
            android:id="@+id/quote_widget_price"
            style="@style/QuoteWidget"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginEnd="@dimen/spacing_large"
            android:importantForAccessibility="no"
            app:formatPrice="@{quoteItem.lastPrice.price}"
            app:layout_constraintStart_toEndOf="@id/divider_lhs"
            app:layout_constraintTop_toBottomOf="@id/quote_widget_title"
            app:liveTextColor="@{quoteItem.lastPrice.priceColor}"
            tools:text="91.73" />

        <View
            android:id="@+id/divider_lhs"
            android:layout_width="@dimen/divider_width"
            android:layout_height="0dp"
            android:layout_marginTop="@dimen/spacing_medium"
            android:background="@color/light_grey"
            android:visibility="invisible"
            app:layout_constraintBottom_toBottomOf="@id/quote_widget_price"
            app:layout_constraintEnd_toStartOf="@id/divider_rhs"
            app:layout_constraintHorizontal_chainStyle="spread_inside"
            app:layout_constraintStart_toStartOf="parent" />

        <TextView
            android:id="@+id/quote_widget_change"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:importantForAccessibility="no"
            android:textAppearance="@style/TextAppearance.Etrade.Text.Medium.Medium"
            app:formatChange="@{quoteItem.change}"
            app:layout_constraintStart_toEndOf="@id/divider_lhs"
            app:layout_constraintTop_toBottomOf="@id/quote_widget_price"
            app:liveTextColor="@{quoteItem.changeColor}"
            tools:text="0.32 (2.51%)" />

        <TextView
            android:id="@+id/quote_widget_volume"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:importantForAccessibility="no"
            android:textAppearance="@style/TextAppearance.Etrade.Text.Medium.Small"
            app:formatVolume="@{quoteItem.volume}"
            app:invisibleUnless="@{safeUnbox(quoteItem.showVolume)}"
            app:layout_constraintStart_toEndOf="@id/divider_lhs"
            app:layout_constraintTop_toBottomOf="@id/quote_widget_change"
            tools:text="12.57M" />

        <androidx.constraintlayout.widget.Barrier
            android:id="@+id/quote_details_barrier"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:barrierDirection="end"
            app:constraint_referenced_ids="quote_widget_price,quote_widget_change,quote_widget_volume" />

        <View
            android:id="@+id/divider_rhs"
            android:layout_width="@dimen/divider_width"
            android:layout_height="0dp"
            android:visibility="invisible"
            app:layout_constraintBottom_toBottomOf="@id/quote_widget_volume"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@id/divider_lhs" />

        <com.etrade.mobilepro.uiwidgets.quote.QuotePriceTextView
            android:id="@+id/quote_bid_title"
            style="@style/TextAppearance.Etrade.Text.Regular.Small"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:importantForAccessibility="no"
            android:gravity="end"
            app:formatBidPrice="@{quoteItem.bidPrice}"
            app:invisibleUnless="@{quoteItem.showBidPrice}"
            app:layout_constraintEnd_toStartOf="@id/divider_rhs"
            app:layout_constraintStart_toEndOf="@id/quote_details_barrier"
            app:layout_constraintTop_toTopOf="@id/quote_widget_price"
            tools:text="Bid: 12.34 x 45" />

        <com.etrade.mobilepro.uiwidgets.quote.QuotePriceTextView
            android:id="@+id/quote_ask_title"
            style="@style/TextAppearance.Etrade.Text.Regular.Small"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:importantForAccessibility="no"
            android:gravity="end"
            app:formatAskPrice="@{quoteItem.askPrice}"
            app:invisibleUnless="@{quoteItem.showAskPrice}"
            app:layout_constraintEnd_toStartOf="@id/divider_rhs"
            app:layout_constraintStart_toEndOf="@id/quote_details_barrier"
            app:layout_constraintTop_toBottomOf="@id/quote_bid_title"
            tools:text="Ask: 12.34 x 45" />

        <TextView
            android:id="@+id/quote_widget_time_stamp"
            style="@style/TextAppearance.Etrade.Text.Regular.Small"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:importantForAccessibility="no"
            android:gravity="end"
            android:paddingTop="@dimen/spacing_xsmall"
            android:text="@{quoteItem.asOfDateTime}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toBottomOf="@id/quote_ask_title"
            app:layout_constraintEnd_toStartOf="@id/divider_rhs"
            app:layout_constraintStart_toEndOf="@id/quote_details_barrier"
            tools:text="As of 9:45 AM EST 03/01/2018" />

    </androidx.constraintlayout.widget.ConstraintLayout>

</layout>