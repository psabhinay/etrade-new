package com.etrade.mobilepro.testutil

class FileReader {
    companion object {
        fun <T> getStringFromFile(resourcePath: String, clazz: Class<T>): String {
            return clazz.getResource(resourcePath).readText()
        }
    }
}
