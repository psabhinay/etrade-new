package com.etrade.mobilepro.chartiq.api.model

data class Account(val id: String, val displayName: String)
