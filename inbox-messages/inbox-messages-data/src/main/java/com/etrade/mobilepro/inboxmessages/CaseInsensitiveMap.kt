package com.etrade.mobilepro.inboxmessages

class CaseInsensitiveMap<V> : HashMap<String, V>() {
    override fun put(key: String, value: V): V? {
        return super.put(key.lowercase(), value)
    }
    override fun get(key: String): V? {
        return super.get(key.lowercase())
    }
}

fun Map<String, String>.toCaseInsensitiveMap(): CaseInsensitiveMap<String> {
    val caseInsensitiveMap = CaseInsensitiveMap<String>()
    this.forEach {
        caseInsensitiveMap[it.key] = it.value
    }
    return caseInsensitiveMap
}
