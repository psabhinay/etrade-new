package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.backends.neo.MessageDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OrderGroupCancelResponseDto(
    @Json(name = "invest.trading.errors")
    val errors: List<MessageDto>?,
    @Json(name = "invest.trading.warnings")
    val warnings: List<MessageDto>?
)
