package com.etrade.mobilepro.dagger

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.messagecenter.presentation.details.service.ServiceMessageDetailsFragment
import com.etrade.mobilepro.messagecenter.presentation.details.support.SupportMessageDetailsFragment
import com.etrade.mobilepro.messagecenter.presentation.messagelist.MessageCenterListFragment
import com.etrade.mobilepro.messagecenter.presentation.newmessage.NewMessageFragment
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface MessageCenterFragmentModule {
    @Binds
    @IntoMap
    @FragmentKey(MessageCenterListFragment::class)
    fun bindMessageListFragment(fragment: MessageCenterListFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ServiceMessageDetailsFragment::class)
    fun bindMessageServiceDetailsFragment(fragment: ServiceMessageDetailsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SupportMessageDetailsFragment::class)
    fun bindMessageSupportDetailsFragment(fragment: SupportMessageDetailsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(NewMessageFragment::class)
    fun bindNewMessageFragment(fragment: NewMessageFragment): Fragment
}
