package com.etrade.mobilepro.messagecenter.data.mapper

import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.api.model.SupportMessage
import com.etrade.mobilepro.messagecenter.data.dto.CommunicationDto
import com.etrade.mobilepro.messagecenter.data.dto.SupportMessageDetailsResponseDto
import com.etrade.mobilepro.util.json.coerceToBool
import org.threeten.bp.format.DateTimeFormatter

private const val EMPTY_VALUE = "--"

@Suppress("LongMethod") // it is simple and straightforward
internal fun SupportMessageDetailsResponseDto.toSupportMessage(formatter: DateTimeFormatter, message: Message): SupportMessage {
    val createdAt = parseToDate(formatter, this.commonDetails?.creationTs)
    return SupportMessage(
        commonDetails = SupportMessage.CommonDetails(
            userId = this.commonDetails?.userId ?: "",
            topicId = this.commonDetails?.topicId ?: "",
            logId = this.commonDetails?.logId ?: "",
            replyEnabled = coerceToBool(this.commonDetails?.replyFlag, false),
            subject = this.commonDetails?.subject ?: EMPTY_VALUE,
            etFirstname = this.commonDetails?.etFirstname ?: "",
            etLastname = this.commonDetails?.etLastname ?: "",
            status = message.status,
            tier = this.commonDetails?.tier ?: "",
            accountName = this.commonDetails?.accountName ?: EMPTY_VALUE,
            createdAt = createdAt,
            createdAtFormatted = this.commonDetails?.creationTs ?: EMPTY_VALUE
        ),
        history = this.communicationList.toCommunications(formatter)
    )
}

internal fun List<CommunicationDto?>?.toCommunications(formatter: DateTimeFormatter): List<SupportMessage.Communication> =
    this
        ?.filterNotNull()
        ?.map {
            val msgCreatedAt = parseToDate(formatter, it.creationTs)
            val msgCreateAtFormatted = it.creationTs ?: EMPTY_VALUE
            SupportMessage.Communication(
                caseNo = it.caseNo ?: EMPTY_VALUE,
                message = it.message ?: EMPTY_VALUE,
                createdAt = msgCreatedAt,
                createdAtFormatted = msgCreateAtFormatted
            )
        } ?: emptyList()
