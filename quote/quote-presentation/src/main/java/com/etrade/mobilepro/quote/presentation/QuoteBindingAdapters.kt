package com.etrade.mobilepro.quote.presentation

import android.content.Context
import android.content.res.Resources
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ImageSpan
import android.util.TypedValue
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.eo.core.util.MarketDataFormatter.EMPTY_FORMATTED_VALUE
import com.etrade.mobilepro.quoteapi.QuoteChangeWidgetItem
import com.etrade.mobilepro.quoteapi.QuoteHeaderViewItem
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem
import com.etrade.mobilepro.util.android.accessibility.processAccessibilityStreamingEvent
import com.etrade.mobilepro.util.android.binding.QUOTE_ACCESSIBILITY_DEBOUNCE
import com.etrade.mobilepro.util.android.textutil.append
import com.etrade.mobilepro.util.formatLetterRepresentationToNumber
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.safeParseBigDecimal

private const val PRICE_TO_EARNINGS_POSTFIX = 'x'
private const val COLOR_RES_UNDEFINED = 0
private const val QUOTE_STATS_MARGIN = "     "

@BindingAdapter("processContentDescription")
fun View.processContentDescription(value: String?) {
    processAccessibilityStreamingEvent(value ?: EMPTY_FORMATTED_VALUE)
}

@BindingAdapter("processQuoteDescription")
fun View.processQuoteDescription(item: QuoteWidgetItem.QuiteWidgetDescription?) {
    processQuoteDescription(item, withBidAsk = false)
}

@BindingAdapter("processQuoteWithBidAskDescription")
fun View.processQuoteWithBidAskDescription(item: QuoteWidgetItem.QuiteWidgetDescription?) {
    processQuoteDescription(item, withBidAsk = true)
}

@BindingAdapter("formatPrice")
fun TextView.setQuotePrice(price: String?) {
    text = formatPrice(price)
}

@BindingAdapter("formatChange")
fun TextView.setQuoteChange(change: QuoteChangeWidgetItem?) {
    text = formatChange(change)
}

@BindingAdapter("formatVolume")
fun TextView.setQuoteVolume(volume: String?) {
    text = formatVolume(volume)
}

@Suppress("LongParameterList")
@BindingAdapter(
    value = ["price", "change", "volume", "priceColor", "changeColor", "hideVolume"],
    requireAll = false
)
fun TextView.setQuotePrice(
    price: String? = null,
    change: QuoteChangeWidgetItem? = null,
    volume: String? = null,
    @ColorRes priceColorRes: Int = COLOR_RES_UNDEFINED,
    @ColorRes changeColorRes: Int = COLOR_RES_UNDEFINED,
    hideVolume: Boolean = false
) {
    setText(
        quoteStats(
            formatPrice(price),
            formatChange(change),
            formatVolume(volume).takeIf { !hideVolume },
            resolve(priceColorRes),
            resolve(changeColorRes),
        ),
        TextView.BufferType.SPANNABLE,
    )
}

@BindingAdapter(value = ["processPriceLabelDescription", "processPriceDescription"], requireAll = false)
fun View.processPriceDescription(priceLabel: String?, price: String?) {
    processAccessibilityStreamingEvent("$priceLabel, ${formatPrice(price)}")
}

@BindingAdapter(requireAll = false, value = ["processChangeLabelDescription", "processChangeDescription"])
fun View.processChangeDescription(changeLabel: String?, change: QuoteChangeWidgetItem?) {
    processAccessibilityStreamingEvent("$changeLabel, ${formatChange(change)}")
}

@BindingAdapter(requireAll = false, value = ["processVolumeLabelDescription", "processVolumeDescription"])
fun View.processVolumeDescription(volumeLabel: String?, volume: String?) {
    processAccessibilityStreamingEvent("$volumeLabel, ${volumeContentDescription(volume)}")
}

@BindingAdapter(value = ["processPerformanceValue", "performanceValueDescriptionFormat"], requireAll = false)
fun TextView.setPerformanceRounded(performance: String?, contentDescriptionFormat: String?) {
    val value = MarketDataFormatter.formatTrailingReturn(performance?.safeParseBigDecimal())
    text = value
    contentDescription = value.formatContentDescription(contentDescriptionFormat)
}

@BindingAdapter("formatPriceToEarnings")
fun TextView.setPriceToEarnings(value: String?) {
    text = formatPriceToEarnings(value)
}

@BindingAdapter(requireAll = false, value = ["processPriceToEarningsLabelDescription", "processPriceToEarningsDescription"])
fun View.processPriceToEarningsDescription(label: String?, value: String?) {
    processAccessibilityStreamingEvent("$label, ${formatPriceToEarnings(value).formatContentDescription()}")
}

@Suppress("LongMethod")
@BindingAdapter(value = ["affiliatedImage", "quoteItem", "affiliatedProductAction", "affiliatedProductTitleAction"])
fun TextView.updateAffiliatedImage(
    affiliatedImage: Int,
    quoteItem: QuoteHeaderViewItem?,
    onAffiliatedProductAction: AffiliatedProductAction?,
    affiliatedProductTitleAction: AffiliatedProductTitleAction?
) {
    if (quoteItem == null) return

    if (!quoteItem.isAffiliatedProduct) {
        text = quoteItem.title
        return
    }

    adjustAffiliatedImage()

    val quoteTitle = quoteItem.title

    // Text is : title and image.
    val builder = SpannableStringBuilder("$quoteTitle  ")

    val imageSpan = affiliatedImage.asImageSpan(context)
    builder.setSpan(
        imageSpan,
        quoteTitle.length + 1,
        quoteTitle.length + 2,
        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    onAffiliatedProductAction?.run {
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                onAffiliatedProductAction.onAction.invoke()
            }
        }
        builder.setSpan(
            clickableSpan,
            quoteTitle.length + 1,
            quoteTitle.length + 2,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    affiliatedProductTitleAction?.run {
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                affiliatedProductTitleAction.onAction.invoke()
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.isUnderlineText = false
            }
        }
        builder.setSpan(
            clickableSpan,
            0,
            quoteTitle.length - 2,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    movementMethod = LinkMovementMethod.getInstance()
    text = builder
}

private fun volumeContentDescription(volume: String?) = formatVolume(volume).formatContentDescription()

private fun formatVolume(volume: String?) = MarketDataFormatter.formatVolumeTwoDecimals(volume?.toBigDecimalOrNull())

private fun View.formatChange(change: QuoteChangeWidgetItem?) = change?.let {
    change.formatQuoteChange(resources.getString(R.string.quote_widget_change_format), change.change, change.changePercent).formatContentDescription()
} ?: EMPTY_FORMATTED_VALUE

private fun formatPrice(price: String?) = MarketDataFormatter.formatMarketDataCustom(price?.toBigDecimalOrNull())

private fun View.processQuoteDescription(item: QuoteWidgetItem.QuiteWidgetDescription?, withBidAsk: Boolean) {
    item?.let {
        val resources = resources
        val bidAsk = if (withBidAsk) {
            getBidAskContentDescription(item)
        } else {
            ""
        }

        val contentDescription = "${resources.getString(R.string.symbol)} ${it.titleContentDescription}, " +
            "${resources.getString(R.string.price)} ${formatPrice(it.price?.price)}, " +
            "${resources.getString(R.string.change)} ${formatChange(it.change)}, " +
            "${resources.getString(R.string.volume)} ${volumeContentDescription((it.volume))}, " +
            bidAsk + it.asOfDateTime
        processAccessibilityStreamingEvent(accessibilityText = contentDescription, debounce = QUOTE_ACCESSIBILITY_DEBOUNCE)
    }
}

private fun View.getBidAskContentDescription(item: QuoteWidgetItem.QuiteWidgetDescription): String {
    val stringBuilder = StringBuilder()
    val resources = resources
    if (item.showBidPrice) {
        stringBuilder.append(
            "${resources.getString(
                R.string.quote_content_description_ask_bid_price,
                resources.getString(R.string.bid),
                item.bid?.price,
                item.bid?.getFormattedSize()
            )}, "
        )
    }
    if (item.showAskPrice) {
        stringBuilder.append(
            "${resources.getString(
                R.string.quote_content_description_ask_bid_price,
                resources.getString(R.string.ask),
                item.ask?.price,
                item.ask?.getFormattedSize()
            )}, "
        )
    }
    return stringBuilder.toString()
}

private fun CharSequence.formatContentDescription(contentDescriptionFormat: String? = null) =
    (contentDescriptionFormat?.format(this) ?: this).toString().formatLetterRepresentationToNumber()

private fun formatPriceToEarnings(value: String?) =
    if (value != null && isPriceToEarningsFormatted(value)) {
        value
    } else {
        value?.safeParseBigDecimal()?.let {
            MarketDataFormatter.formatPriceToBeUpdated(it).plus(PRICE_TO_EARNINGS_POSTFIX)
        } ?: EMPTY_FORMATTED_VALUE
    }

private fun isPriceToEarningsFormatted(value: String): Boolean = value.isNotEmpty() && value.last() == PRICE_TO_EARNINGS_POSTFIX

private fun TextView.resolve(@ColorRes color: Int): Int =
    try {
        context.getColor(color)
    } catch (e: Resources.NotFoundException) {
        currentTextColor
    }

@Suppress("LongParameterList")
private fun quoteStats(
    price: String,
    change: String,
    volume: String? = null,
    priceColor: Int,
    changeColor: Int,
) = with(SpannableStringBuilder()) {
    append(price, priceColor)
    append(QUOTE_STATS_MARGIN)
    if (volume != null) {
        append(change, changeColor)
        append(QUOTE_STATS_MARGIN)
        append(volume)
    } else {
        append(QUOTE_STATS_MARGIN)
        append(QUOTE_STATS_MARGIN)
        append(QUOTE_STATS_MARGIN)
        append(change, changeColor)
    }
    this
}

private fun Int.asImageSpan(context: Context): ImageSpan? {
    val drawable = ContextCompat.getDrawable(context, this)
    return drawable?.let {
        it.setBounds(0, 0, it.intrinsicWidth, it.intrinsicHeight)
        ImageSpan(it, ImageSpan.ALIGN_BASELINE)
    }
}

@Suppress("MagicNumber")
private fun TextView.adjustAffiliatedImage() {
    viewTreeObserver.addOnPreDrawListener {
        viewTreeObserver.removeOnPreDrawListener(this)
        layout?.run {
            if (layout.lineCount >= 2) {
                val start = layout.getLineStart(layout.lineCount - 1)
                val end = layout.getLineEnd(layout.lineCount - 1)

                // When affiliated image is alone on the last line, reduce the font size
                // so that it will be fit in same line.
                if (end - start == 1 && end == text.length - 1) {
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize * 0.9f)
                }
            }
        }
        true
    }
}
