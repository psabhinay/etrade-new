package com.etrade.mobilepro.dynamicui

import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

class ViewAllAccountsViewModel : DynamicViewModel(R.layout.view_all_accounts_row) {

    override val variableId: Int = BR.obj

    fun viewAllAccounts() {
        clickEvents.value = CtaEvent.ViewAllBrokerageAccounts
    }
}

class ViewAllAccountsViewFactory @Inject constructor() : DynamicUiViewFactory {
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        return object : GenericLayout {

            override val viewModelClass = ViewAllAccountsViewModel::class.java

            override val viewModelFactory = viewModelFactory {
                ViewAllAccountsViewModel()
            }

            override val uniqueIdentifier: String = javaClass.name
        }
    }
}
