package com.etrade.mobilepro.encryption.api

import javax.crypto.Cipher

typealias CipherAuthenticator = suspend (Cipher) -> Cipher

interface EncryptionProvider {

    /**
     * Verifies if current device supports secure encryption (e.g., hardware-backed).
     *
     * @return true if supported, otherwise false.
     */
    fun isSecureEncryptionSupported(): Boolean

    /**
     * Encrypts a raw input and returns an encrypted version of it.
     *
     * @return the encrypted version of the input provided.
     */
    suspend fun encrypt(plaintext: String, keyMaterial: SecretKeyMaterial, authenticate: CipherAuthenticator): String

    /**
     * Try to decrypt a previously encrypted piece of data that was processed using the same
     * [EncryptionProvider] implementation.
     *
     * @return The decrypted data.
     */
    suspend fun decrypt(encrypted: String, keyMaterial: SecretKeyMaterial, authenticate: CipherAuthenticator): String

    /**
     * Applies secure one-way hash function to an arbitrary value.
     *
     * @param value The arbitrary string.
     *
     * @return Base-64 string that represents the value of a fixed length.
     */
    suspend fun hash(value: String): String
}
