package com.etrade.mobilepro.trade.data.stock

import com.etrade.mobilepro.trade.data.TradeRequestParamsUpdater
import com.etrade.mobilepro.trade.data.form.input.DefaultContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.util.removeParams

class ContingentStockTradeRequestParamsUpdater : TradeRequestParamsUpdater {

    private val redundantParams: Set<String> = setOf(
        DefaultContingentOptionTradeFormInputId.securityType
    )

    override fun updatePreviewOrderParams(map: MutableMap<String, Any?>) {
        map.removeParams(redundantParams)
    }

    override fun updatePlaceOrderParams(map: MutableMap<String, Any?>) {
        updatePreviewOrderParams(map)
    }
}
