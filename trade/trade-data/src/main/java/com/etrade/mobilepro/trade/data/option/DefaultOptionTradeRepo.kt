package com.etrade.mobilepro.trade.data.option

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.trade.api.option.OptionTradeRepo
import com.etrade.mobilepro.trade.api.option.OptionTradeStrategyResponse
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.CommonOrderConstants
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.etrade.mobilepro.trade.data.customorder.CustomOrderStrategyResponseBodyDto
import com.etrade.mobilepro.trade.data.customorder.OrderStrategyApiClient
import com.etrade.mobilepro.trade.data.form.toLegDetailsDtoList
import com.etrade.mobilepro.trade.data.form.value.CommonOrderTermValueId
import com.etrade.mobilepro.trade.data.form.value.CommonPriceTypeValueId
import com.etrade.mobilepro.trade.data.setOptionOrderType
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.util.json.toJsonString
import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import javax.inject.Inject

class DefaultOptionTradeRepo @Inject constructor(
    @MobileTrade retrofit: Retrofit,
    private val moshi: Moshi,
    private val optionTradeFormInputId: OptionTradeFormInputId
) : OptionTradeRepo {

    private val apiClient: OrderStrategyApiClient = retrofit.create(OrderStrategyApiClient::class.java)

    override suspend fun getStrategyIdentifier(symbol: String, legs: List<TradeLeg>, params: InputValuesSnapshot): ETResult<OptionTradeStrategyResponse> {
        return getStrategyIdentifier(createBody(symbol, legs, params), legs)
    }

    override suspend fun getStrategyIdentifier(
        symbol: String,
        legs: List<TradeLeg>,
        accountId: String,
        orderType: OrderType
    ): ETResult<OptionTradeStrategyResponse> {
        val params = mutableMapOf(
            optionTradeFormInputId.accountId to accountId,
            CommonOrderConstants.ORDER_TYPE to orderType.typeCode
        )
        return getStrategyIdentifier(createBody(symbol, legs, params), legs)
    }

    private suspend fun getStrategyIdentifier(body: String, legs: List<TradeLeg>): ETResult<OptionTradeStrategyResponse> {
        return apiClient.runCatchingET {
            getCustomOrderStrategy(body)
                .doIfSuccessfulOrThrow {
                    it.toOptionTradeStrategyResponse(legs)
                }
        }
    }

    private fun createBody(symbol: String, legs: List<TradeLeg>, params: InputValuesSnapshot): String {
        val body: MutableMap<String, Any?> = params.toMutableMap()
        val legDetailsDtoList = legs.toLegDetailsDtoList()
        with(optionTradeFormInputId) {
            body[this.symbol] = symbol
            body[tradeLegs] = legDetailsDtoList
            body.setOptionOrderType(securityType, securityTypeValueId, legDetailsDtoList, params)

            // these parameters are hardcoded for the sake or compatibility with the endpoint's API
            body[priceType] = CommonPriceTypeValueId.market
            body[term] = CommonOrderTermValueId.goodForDay
        }

        body[CommonOrderConstants.REORDER] = true
        return moshi.toJsonString(mapOf("value" to body))
    }

    private fun CustomOrderStrategyResponseBodyDto.toOptionTradeStrategyResponse(requestLegs: List<TradeLeg>): OptionTradeStrategyResponse {
        val content = requireNotNull(content)
        require(requestLegs.size == content.legRequestList.size) {
            "The number of requested legs doesn't match the response's legs."
        }

        val legs = requestLegs.associateBy { it.key }
        return object : OptionTradeStrategyResponse {
            override val strategyType: StrategyType = StrategyType.fromName(content.ordStrategy)
            override val optionLegs: List<TradeLeg> = content.legRequestList.map { requireNotNull(legs[it.key]) }
            override val warnings: Collection<ServerMessage> = getAllMessages()
        }
    }

    private val LegDetailsCommonDto.key: OptionKey
        get() = OptionKey(symbol, transactionCode, quantity)

    private val TradeLeg.key: OptionKey
        get() = OptionKey(symbol, transactionType.typeCode.toString(), quantity)

    private data class OptionKey(
        val symbol: String,
        val action: String,
        val quantity: Int
    )
}
