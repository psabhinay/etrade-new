package com.etrade.mobilepro.pdf.presentation

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.pdf.api.PdfRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.io.File

private val logger = LoggerFactory.getLogger(PdfViewModel::class.java)

@Suppress("StaticFieldLeak")
class PdfViewModel @AssistedInject constructor(
    private val pdfRepository: PdfRepository,
    private val context: Context,
    @Assisted private val url: String?
) : ViewModel() {
    private val _pdfViewStateLiveData: MutableLiveData<PdfViewState> = MutableLiveData()
    val pdfViewStateLiveData: LiveData<PdfViewState>
        get() = _pdfViewStateLiveData

    private var cachedFile: File? = null

    @AssistedFactory
    interface Factory {
        fun create(url: String?): PdfViewModel
    }

    init {
        url?.also {
            downloadPdf(it)
        }
    }

    private fun downloadPdf(url: String) {
        viewModelScope.launch {
            _pdfViewStateLiveData.postValue(PdfViewState.Loading)
            val file = File.createTempFile("PdfDownload_", null, context.cacheDir)

            pdfRepository.downloadPdf(url, file).fold(
                onSuccess = { downloadedFile ->
                    cachedFile = downloadedFile
                    _pdfViewStateLiveData.postValue(PdfViewState.Success(downloadedFile))
                },
                onFailure = {
                    logger.error(it.localizedMessage)
                    _pdfViewStateLiveData.postValue(PdfViewState.Error)
                }
            )
        }
    }

    override fun onCleared() {
        try {
            cachedFile?.also { file ->
                file.delete()
            }
        } catch (e: Exception) {
            logger.error("Could not delete pdf", e)
        }
    }

    sealed class PdfViewState {
        object Loading : PdfViewState()
        data class Success(val pdfFile: File) : PdfViewState()
        object Error : PdfViewState()
    }
}
