package com.etrade.mobilepro.news.presentation.watchlist

import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.news.api.filter.FilterDataSource
import com.etrade.mobilepro.news.presentation.NewsSharedViewModel
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.news.presentation.base.BaseFilterableNewsFragment
import com.etrade.mobilepro.news.presentation.filter.watchlist.WatchlistFilterDataSource
import com.etrade.mobilepro.watchlistapi.Watchlist
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

/**
 * Refactor to be used as news fragment in watchlist 2.0 in https://jira.corp.etradegrp.com/browse/ETAND-12577.
 */
// TODO to be deleted in scope of watchlist 2.0 epic
class WatchlistNewsFragment @Inject constructor() : BaseFilterableNewsFragment<Watchlist, WatchlistFilterDataSource, WatchlistNewsViewModel>() {

    override val viewModelClass: Class<WatchlistNewsViewModel> = WatchlistNewsViewModel::class.java

    override val filterDropDownTitle: Int = R.string.watchlist_title

    @ExperimentalCoroutinesApi
    override fun applyFilter(selectedItem: Watchlist?) = viewModel.applyFilter(selectedItem)

    override fun getFilterDataSource(sharedViewModel: NewsSharedViewModel): FilterDataSource<Watchlist> = sharedViewModel.watchlistFilterDataSource

    override fun getSelectedItemTitle(selectedItem: Watchlist?): CharSequence = selectedItem?.name.orEmpty()

    override fun onSingleFilterValueClicked() {
        viewModel.onFilterClickedUnAuth(findNavController())
    }
}
