package com.etrade.mobilepro.chart.performance;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\r\n\u0002\b\u000b\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u0002:\u0001EB)\b\u0007\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0001\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010>\u001a\u00020?H\u0002J\u0016\u0010@\u001a\u00020?2\u0006\u0010A\u001a\u00020\t2\u0006\u0010B\u001a\u00020\u000fJ\u000e\u0010C\u001a\u00020?2\u0006\u0010D\u001a\u00020\tR\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u00040\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\t0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R/\u0010\u0013\u001a\u0004\u0018\u00010\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u00128F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\r0\u001b8F\u00a2\u0006\u0006\u001a\u0004\b\u001c\u0010\u001dR\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010 \u001a\u00020!8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b\"\u0010#R\u001d\u0010&\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\'0\u00040\u001b\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001dR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010)\u001a\u0004\u0018\u00010\u0012X\u0096D\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010\u0015R\u001a\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b,\u0010-R\u001a\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b/\u0010-R\u001d\u00100\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\u00040\u001b8F\u00a2\u0006\u0006\u001a\u0004\b1\u0010\u001dR\u0014\u00102\u001a\b\u0012\u0004\u0012\u00020\u000f03X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00104\u001a\u0002058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b6\u00107R\u0017\u00108\u001a\b\u0012\u0004\u0012\u00020\t0\u001b8F\u00a2\u0006\u0006\u001a\u0004\b9\u0010\u001dR\u000e\u0010:\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001d\u0010;\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\'0\u00040\u001b\u00a2\u0006\b\n\u0000\u001a\u0004\b<\u0010\u001dR\u0014\u0010=\u001a\b\u0012\u0004\u0012\u0002050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006F"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel;", "Landroidx/lifecycle/ViewModel;", "Lcom/etrade/mobilepro/tracking/ScreenViewModel;", "markets", "", "Lcom/etrade/mobilepro/chart/data/ChartParams;", "getChartLineDataUseCase", "Lcom/etrade/mobilepro/chart/data/GetChartLineDataUseCase;", "performanceLineColor", "", "(Ljava/util/List;Lcom/etrade/mobilepro/chart/data/GetChartLineDataUseCase;I)V", "_chartState", "Landroidx/lifecycle/MutableLiveData;", "Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState;", "_selectedMarkets", "", "_selectedTimeInterval", "<set-?>", "", "accountId", "getAccountId", "()Ljava/lang/String;", "setAccountId", "(Ljava/lang/String;)V", "accountId$delegate", "Lkotlin/properties/ReadWriteProperty;", "chartState", "Landroidx/lifecycle/LiveData;", "getChartState", "()Landroidx/lifecycle/LiveData;", "disposable", "Lio/reactivex/disposables/CompositeDisposable;", "logger", "Lorg/slf4j/Logger;", "getLogger", "()Lorg/slf4j/Logger;", "logger$delegate", "Lkotlin/Lazy;", "marketNames", "", "getMarketNames", "screenName", "getScreenName", "selectedAccounts", "getSelectedAccounts", "()Ljava/util/List;", "selectedMarketTickers", "getSelectedMarketTickers", "selectedMarkets", "getSelectedMarkets", "selectedMarketsRaw", "", "selectedTimeFrame", "Lcom/etrade/mobilepro/chart/model/ChartTimeFrame;", "getSelectedTimeFrame", "()Lcom/etrade/mobilepro/chart/model/ChartTimeFrame;", "selectedTimeInterval", "getSelectedTimeInterval", "selectedTimeIntervalRaw", "timeFrameNames", "getTimeFrameNames", "timeFrames", "loadChart", "", "updateMarketSelection", "index", "selected", "updateSelectedTimeInterval", "selectedIndex", "ChartState", "performance_debug"})
public final class PerformanceChartViewModel extends androidx.lifecycle.ViewModel implements com.etrade.mobilepro.tracking.ScreenViewModel {
    private final java.util.List<com.etrade.mobilepro.chart.data.ChartParams> markets = null;
    private final com.etrade.mobilepro.chart.data.GetChartLineDataUseCase getChartLineDataUseCase = null;
    private final int performanceLineColor = 0;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String screenName = "Performance Chart";
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.List<java.lang.CharSequence>> marketNames = null;
    private final java.util.List<com.etrade.mobilepro.chart.model.ChartTimeFrame> timeFrames = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.List<java.lang.CharSequence>> timeFrameNames = null;
    @org.jetbrains.annotations.Nullable()
    private final kotlin.properties.ReadWriteProperty accountId$delegate = null;
    private java.util.List<java.lang.Boolean> selectedMarketsRaw;
    private int selectedTimeIntervalRaw = 1;
    private final androidx.lifecycle.MutableLiveData<com.etrade.mobilepro.chart.performance.PerformanceChartViewModel.ChartState> _chartState = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Boolean>> _selectedMarkets = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Integer> _selectedTimeInterval = null;
    private final io.reactivex.disposables.CompositeDisposable disposable = null;
    private final kotlin.Lazy logger$delegate = null;
    
    @javax.inject.Inject()
    public PerformanceChartViewModel(@org.jetbrains.annotations.NotNull()
    java.util.List<com.etrade.mobilepro.chart.data.ChartParams> markets, @org.jetbrains.annotations.NotNull()
    @javax.inject.Named(value = "Performance")
    com.etrade.mobilepro.chart.data.GetChartLineDataUseCase getChartLineDataUseCase, @androidx.annotation.ColorRes()
    @com.etrade.mobilepro.common.di.PrimaryColor()
    int performanceLineColor) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.String getScreenName() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<java.lang.CharSequence>> getMarketNames() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.etrade.mobilepro.chart.performance.PerformanceChartViewModel.ChartState> getChartState() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<java.lang.Boolean>> getSelectedMarkets() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Integer> getSelectedTimeInterval() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<java.lang.CharSequence>> getTimeFrameNames() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccountId() {
        return null;
    }
    
    public final void setAccountId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    private final java.util.List<com.etrade.mobilepro.chart.data.ChartParams> getSelectedAccounts() {
        return null;
    }
    
    private final java.util.List<com.etrade.mobilepro.chart.data.ChartParams> getSelectedMarketTickers() {
        return null;
    }
    
    private final com.etrade.mobilepro.chart.model.ChartTimeFrame getSelectedTimeFrame() {
        return null;
    }
    
    private final org.slf4j.Logger getLogger() {
        return null;
    }
    
    public final void updateMarketSelection(int index, boolean selected) {
    }
    
    public final void updateSelectedTimeInterval(int selectedIndex) {
    }
    
    private final void loadChart() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b\u00a8\u0006\t"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState;", "", "()V", "Error", "Idle", "Loading", "Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState$Idle;", "Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState$Loading;", "Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState$Error;", "performance_debug"})
    public static abstract class ChartState {
        
        private ChartState() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0011"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState$Idle;", "Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState;", "lineData", "Lcom/etrade/mobilepro/chart/model/ChartLineData;", "(Lcom/etrade/mobilepro/chart/model/ChartLineData;)V", "getLineData", "()Lcom/etrade/mobilepro/chart/model/ChartLineData;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "performance_debug"})
        public static final class Idle extends com.etrade.mobilepro.chart.performance.PerformanceChartViewModel.ChartState {
            @org.jetbrains.annotations.NotNull()
            private final com.etrade.mobilepro.chart.model.ChartLineData lineData = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.etrade.mobilepro.chart.performance.PerformanceChartViewModel.ChartState.Idle copy(@org.jetbrains.annotations.NotNull()
            com.etrade.mobilepro.chart.model.ChartLineData lineData) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Idle(@org.jetbrains.annotations.NotNull()
            com.etrade.mobilepro.chart.model.ChartLineData lineData) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.etrade.mobilepro.chart.model.ChartLineData component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.etrade.mobilepro.chart.model.ChartLineData getLineData() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0015\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0011"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState$Loading;", "Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState;", "lineData", "Lcom/etrade/mobilepro/chart/model/ChartLineData;", "(Lcom/etrade/mobilepro/chart/model/ChartLineData;)V", "getLineData", "()Lcom/etrade/mobilepro/chart/model/ChartLineData;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "performance_debug"})
        public static final class Loading extends com.etrade.mobilepro.chart.performance.PerformanceChartViewModel.ChartState {
            @org.jetbrains.annotations.Nullable()
            private final com.etrade.mobilepro.chart.model.ChartLineData lineData = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.etrade.mobilepro.chart.performance.PerformanceChartViewModel.ChartState.Loading copy(@org.jetbrains.annotations.Nullable()
            com.etrade.mobilepro.chart.model.ChartLineData lineData) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Loading() {
                super();
            }
            
            public Loading(@org.jetbrains.annotations.Nullable()
            com.etrade.mobilepro.chart.model.ChartLineData lineData) {
                super();
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.etrade.mobilepro.chart.model.ChartLineData component1() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.etrade.mobilepro.chart.model.ChartLineData getLineData() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0011\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0015\u0010\b\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0011"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState$Error;", "Lcom/etrade/mobilepro/chart/performance/PerformanceChartViewModel$ChartState;", "lineData", "Lcom/etrade/mobilepro/chart/model/ChartLineData;", "(Lcom/etrade/mobilepro/chart/model/ChartLineData;)V", "getLineData", "()Lcom/etrade/mobilepro/chart/model/ChartLineData;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "performance_debug"})
        public static final class Error extends com.etrade.mobilepro.chart.performance.PerformanceChartViewModel.ChartState {
            @org.jetbrains.annotations.Nullable()
            private final com.etrade.mobilepro.chart.model.ChartLineData lineData = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.etrade.mobilepro.chart.performance.PerformanceChartViewModel.ChartState.Error copy(@org.jetbrains.annotations.Nullable()
            com.etrade.mobilepro.chart.model.ChartLineData lineData) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Error() {
                super();
            }
            
            public Error(@org.jetbrains.annotations.Nullable()
            com.etrade.mobilepro.chart.model.ChartLineData lineData) {
                super();
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.etrade.mobilepro.chart.model.ChartLineData component1() {
                return null;
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.etrade.mobilepro.chart.model.ChartLineData getLineData() {
                return null;
            }
        }
    }
}