package com.etrade.mobilepro.launcher

import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticator
import com.etrade.mobilepro.biometric.presentation.bindCallbacksToExistingBiometricAuthenticationFragment
import com.etrade.mobilepro.biometric.presentation.showBiometricAuthenticationFragment
import com.etrade.mobilepro.dialog.showDialog
import com.etrade.mobilepro.dialog.viewmodel.AppDialogViewDelegate
import com.etrade.mobilepro.login.presentation.AuthenticationFailedException
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumeIfNot
import com.etrade.mobilepro.viewdelegate.ActivitySignalViewDelegate
import com.etrade.mobilepro.viewdelegate.ViewDelegate
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.crypto.Cipher
import javax.inject.Inject

class AppLauncherActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: AppLauncherViewModel by viewModels { viewModelFactory }

    private val viewDelegates: Iterable<ViewDelegate> by lazy {
        setOf(
            ActivitySignalViewDelegate(viewModel, this),
            AppDialogViewDelegate(viewModel) {
                showDialog(it)
            }
        )
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            installSplashScreen()
        }

        if (savedInstanceState != null) {
            supportFragmentManager.bindCallbacksToExistingBiometricAuthenticationFragment {
                bindCallbacks()
            }
        }

        viewDelegates.forEach {
            it.observe(this)
        }

        viewModel.authenticateCipherSignal.observe(this) { onAuthenticateCipherSignal(it) }
    }

    private fun onAuthenticateCipherSignal(event: ConsumableLiveEvent<Cipher>) {
        event.consumeIfNot(isFinishing) { cipher ->
            supportFragmentManager.showBiometricAuthenticationFragment(cipher = cipher, isUserInitiated = false) {
                bindCallbacks()
            }
        }
    }

    private fun BiometricAuthenticator.bindCallbacks() {
        onAuthenticationCancel = viewModel::onCipherAuthenticationCancelled
        onAuthenticationFail = { viewModel.onCipherAuthenticationFailed(AuthenticationFailedException()) }
        onAuthenticationSuccess = viewModel::onCipherAuthenticated
    }
}
