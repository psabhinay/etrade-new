package com.etrade.mobilepro.streaming.client.side.calculations

import android.os.SystemClock
import java.util.concurrent.TimeUnit

internal fun isCalculationsExpired(timeStamp: Long, expirationTime: Long) =
    SystemClock.elapsedRealtime() - timeStamp >= TimeUnit.SECONDS.toMillis(expirationTime)

internal fun extractAccountIdFromStreamId(streamId: String): String {
    return streamId.substring(streamId.indexOf(':') + 1, streamId.indexOf('.'))
}
