package com.etrade.mobilepro.trade.data.conditional

import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrder
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderPreviewResponse
import com.etrade.mobilepro.trade.api.advanced.TrailingStopOrderDetails
import com.etrade.mobilepro.trade.api.advanced.TrailingStopOrderResponse
import com.etrade.mobilepro.trade.data.TradeOrderArguments
import com.etrade.mobilepro.trade.data.advanced.AdvancedTradingApiClient
import com.etrade.mobilepro.trade.data.advanced.FORM_TARGET_KEY
import com.etrade.mobilepro.trade.data.advanced.KEY_SECURITY_TYPE
import com.etrade.mobilepro.trade.data.advanced.OPTION_EXPIRATION_KEY
import com.etrade.mobilepro.trade.data.advanced.OPTION_STRIKE_PRICE_KEY
import com.etrade.mobilepro.trade.data.advanced.OPTION_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_COUNT_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_TRIGGER_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.PLATFORM_KEY
import com.etrade.mobilepro.trade.data.advanced.POSITION_EFFECT_KEY
import com.etrade.mobilepro.trade.data.advanced.PREVIEW_ID_KEY
import com.etrade.mobilepro.trade.data.advanced.PREVIEW_X_KEY
import com.etrade.mobilepro.trade.data.advanced.SIDE_KEY
import com.etrade.mobilepro.trade.data.advanced.TRADE_X_KEY
import com.etrade.mobilepro.trade.data.advanced.VALUE_SECURITY_TYPE_OPTION
import com.etrade.mobilepro.trade.data.advanced.VALUE_SECURITY_TYPE_STOCK
import com.etrade.mobilepro.trade.data.form.input.DefaultConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultConditionalTradeFormInputId
import com.etrade.mobilepro.trade.data.form.value.ConditionalOrderOptionsActionValueId
import com.etrade.mobilepro.trade.form.api.input.CONDITIONAL_ORDER_SECURITY_TYPE_ID
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ConditionalTradeFormInputId
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class NewConditionalOrderTradeDelegateTest {

    private val inputId: ConditionalOrderTradeFormInputId = DefaultConditionalOrderTradeFormInputId
    private val tradeFormInputId: ConditionalTradeFormInputId = DefaultConditionalTradeFormInputId

    private val fakeClient: AdvancedTradingApiClient = mock {
        runBlocking {
            whenever(it.previewOrder(any(), any())).thenReturn(mock())
            whenever(it.placeOrder(any(), any())).thenReturn(mock())
        }
    }

    @Test
    fun `PREVIEW - LIMIT BUY 1 AAPL GOOD_FOR_60_DAYS - LIMIT SELL 1 AAPL GOOD_FOR_DAY - LIMIT BUY 1 GOOG GOOD_FOR_DAY`() {
        val order1 = mapOf(
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "100",
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.securityType to inputId.securityTypeValueId.stock,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.stock
        )
        val order2 = mapOf(
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.sell,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "110",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.securityType to inputId.securityTypeValueId.stock,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.stock
        )
        val order3 = mapOf(
            inputId.symbol to "GOOG:EQ",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "1000",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.securityType to inputId.securityTypeValueId.stock,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.stock
        )
        val input = mapOf(
            tradeFormInputId.accountId to "31463268",
            tradeFormInputId.securityType to tradeFormInputId.securityTypeValueId.conditional,
            tradeFormInputId.order1 to order1,
            tradeFormInputId.order2 to order2,
            tradeFormInputId.order3 to order3
        )

        val output = mapOf(
            tradeFormInputId.accountId to "31463268",
            "${inputId.symbol}0" to "AAPL",
            "${inputId.action}0" to inputId.actionValueId.buy,
            "${inputId.quantity}0" to "1",
            "${inputId.priceType}0" to inputId.priceTypeValueId.limit,
            "${inputId.limitPrice}0" to "100",
            "${inputId.term}0" to inputId.termValueId.goodUntilCancel,
            "${inputId.allOrNone}0" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}0" to VALUE_SECURITY_TYPE_STOCK,
            "${inputId.symbol}1" to "AAPL",
            "${inputId.action}1" to inputId.actionValueId.sell,
            "${inputId.quantity}1" to "1",
            "${inputId.priceType}1" to inputId.priceTypeValueId.limit,
            "${inputId.limitPrice}1" to "110",
            "${inputId.term}1" to inputId.termValueId.goodForDay,
            "${inputId.allOrNone}1" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}1" to VALUE_SECURITY_TYPE_STOCK,
            "${inputId.symbol}2" to "GOOG",
            "${inputId.action}2" to inputId.actionValueId.buy,
            "${inputId.quantity}2" to "1",
            "${inputId.priceType}2" to inputId.priceTypeValueId.limit,
            "${inputId.limitPrice}2" to "1000",
            "${inputId.term}2" to inputId.termValueId.goodForDay,
            "${inputId.allOrNone}2" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}2" to VALUE_SECURITY_TYPE_STOCK,
            ORDER_COUNT_KEY to "3",
            FORM_TARGET_KEY to "otacreateentry",
            PLATFORM_KEY to "android",
            PREVIEW_X_KEY to "65"
        )

        verifyPreviewOutputWithGivenInput(input, output)
    }

    @Test
    fun `PLACE - LIMIT BUY 1 AAPL GOOD_FOR_60_DAYS - LIMIT SELL 1 AAPL GOOD_FOR_DAY - LIMIT BUY 1 GOOG GOOD_FOR_DAY`() {
        val order1 = mapOf(
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "100",
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.securityType to inputId.securityTypeValueId.stock,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.stock
        )
        val order2 = mapOf(
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.sell,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "110",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.securityType to inputId.securityTypeValueId.stock,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.stock
        )
        val order3 = mapOf(
            inputId.symbol to "GOOG:EQ",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "1000",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.securityType to inputId.securityTypeValueId.stock,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.stock
        )
        val input = mapOf(
            tradeFormInputId.accountId to "31463268",
            tradeFormInputId.securityType to tradeFormInputId.securityTypeValueId.conditional,
            tradeFormInputId.order1 to order1,
            tradeFormInputId.order2 to order2,
            tradeFormInputId.order3 to order3
        )

        val orderType = OrderType.EQUITY
        val output = mapOf(
            tradeFormInputId.accountId to "31463268",
            "${inputId.symbol}0" to "AAPL",
            "${inputId.quantity}0" to "1",
            "${ORDER_TYPE_KEY}0" to orderType.typeCode,
            "${ORDER_TRIGGER_KEY}0" to "2",
            "${SIDE_KEY}0" to "2",
            "${POSITION_EFFECT_KEY}0" to "1",
            "${inputId.limitPrice}0" to "100",
            "${inputId.term}0" to inputId.termValueId.goodUntilCancel,
            "${inputId.allOrNone}0" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}0" to VALUE_SECURITY_TYPE_STOCK,
            "${inputId.symbol}1" to "AAPL",
            "${inputId.quantity}1" to "1",
            "${ORDER_TYPE_KEY}1" to orderType.typeCode,
            "${ORDER_TRIGGER_KEY}1" to "2",
            "${SIDE_KEY}1" to "3",
            "${POSITION_EFFECT_KEY}1" to "2",
            "${inputId.limitPrice}1" to "110",
            "${inputId.term}1" to inputId.termValueId.goodForDay,
            "${inputId.allOrNone}1" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}1" to VALUE_SECURITY_TYPE_STOCK,
            "${inputId.symbol}2" to "GOOG",
            "${inputId.quantity}2" to "1",
            "${ORDER_TYPE_KEY}2" to orderType.typeCode,
            "${ORDER_TRIGGER_KEY}2" to "2",
            "${SIDE_KEY}2" to "2",
            "${POSITION_EFFECT_KEY}2" to "1",
            "${inputId.limitPrice}2" to "1000",
            "${inputId.term}2" to inputId.termValueId.goodForDay,
            "${inputId.allOrNone}2" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}2" to VALUE_SECURITY_TYPE_STOCK,
            PREVIEW_ID_KEY to "11446384279",
            ORDER_COUNT_KEY to "3",
            FORM_TARGET_KEY to "otapreview",
            PLATFORM_KEY to "android",
            TRADE_X_KEY to "60"
        )

        verifyPlaceOutputWithGivenInput(input, output, "11446384279", listOf(orderType, orderType, orderType))
    }

    @Test
    fun `PREVIEW - LIMIT BUY_OPEN 1 AAPL--210122C00107000 GOOD_FOR_60_DAYS - LIMIT SELL_OPEN 1 GOOG--210122P01665000 GOOD_FOR_60_DAYS`() {
        val order1 = mapOf(
            inputId.symbol to "AAPL--210122C00107000:OPTNC",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "10",
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.underlierSymbol to "AAPL",
            inputId.securityType to inputId.securityTypeValueId.option,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.option
        )
        val order2 = mapOf(
            inputId.symbol to "GOOG--210122P01665000:OPTNP",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "400",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.underlierSymbol to "GOOG",
            inputId.securityType to inputId.securityTypeValueId.option,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.option
        )
        val input = mapOf(
            tradeFormInputId.accountId to "83851862",
            tradeFormInputId.securityType to tradeFormInputId.securityTypeValueId.conditional,
            tradeFormInputId.order1 to order1,
            tradeFormInputId.order2 to order2
        )

        val output = mapOf(
            tradeFormInputId.accountId to "83851862",
            "${inputId.symbol}0" to "AAPL--210122C00107000",
            "${inputId.action}0" to inputId.actionValueId.buy,
            "${inputId.quantity}0" to "1",
            "${inputId.priceType}0" to inputId.priceTypeValueId.limit,
            "${inputId.limitPrice}0" to "10",
            "${inputId.term}0" to inputId.termValueId.goodUntilCancel,
            "${inputId.allOrNone}0" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}0" to VALUE_SECURITY_TYPE_OPTION,
            "${OPTION_EXPIRATION_KEY}0" to "1",
            "${OPTION_STRIKE_PRICE_KEY}0" to "107",
            "${OPTION_TYPE_KEY}0" to "1",
            "${inputId.symbol}1" to "GOOG--210122P01665000",
            "${inputId.quantity}1" to "1",
            "${inputId.priceType}1" to inputId.priceTypeValueId.limit,
            "${inputId.limitPrice}1" to "400",
            "${inputId.term}1" to inputId.termValueId.goodForDay,
            "${inputId.allOrNone}1" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${inputId.action}1" to ConditionalOrderOptionsActionValueId.sellShort,
            "${KEY_SECURITY_TYPE}1" to VALUE_SECURITY_TYPE_OPTION,
            "${OPTION_EXPIRATION_KEY}1" to "1",
            "${OPTION_STRIKE_PRICE_KEY}1" to "1665",
            "${OPTION_TYPE_KEY}1" to "2",
            ORDER_COUNT_KEY to "2",
            FORM_TARGET_KEY to "otacreateentry",
            PLATFORM_KEY to "android",
            PREVIEW_X_KEY to "65"
        )

        verifyPreviewOutputWithGivenInput(input, output)
    }

    @Test
    fun `PLACE - LIMIT BUY_OPEN 1 AAPL--210122C00107000 GOOD_FOR_60_DAYS - LIMIT SELL_OPEN 1 GOOG--210122P01665000 GOOD_FOR_60_DAYS`() {
        val order1 = mapOf(
            inputId.symbol to "AAPL--210122C00107000:OPTNC",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "10",
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.underlierSymbol to "AAPL",
            inputId.securityType to inputId.securityTypeValueId.option,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.option
        )
        val order2 = mapOf(
            inputId.symbol to "GOOG--210122P01665000:OPTNP",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "400",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.underlierSymbol to "GOOG",
            inputId.securityType to inputId.securityTypeValueId.option,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.option
        )
        val input = mapOf(
            tradeFormInputId.accountId to "83851862",
            tradeFormInputId.securityType to tradeFormInputId.securityTypeValueId.conditional,
            tradeFormInputId.order1 to order1,
            tradeFormInputId.order2 to order2
        )

        val orderType = OrderType.SIMPLE_OPTION
        val output = mapOf(
            tradeFormInputId.accountId to "83851862",
            "${inputId.symbol}0" to "AAPL--210122C00107000",
            "${ORDER_TYPE_KEY}0" to orderType.typeCode,
            "${ORDER_TRIGGER_KEY}0" to "2",
            "${SIDE_KEY}0" to "2",
            "${POSITION_EFFECT_KEY}0" to "1",
            "${inputId.quantity}0" to "1",
            "${inputId.limitPrice}0" to "10",
            "${inputId.term}0" to inputId.termValueId.goodUntilCancel,
            "${inputId.allOrNone}0" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}0" to VALUE_SECURITY_TYPE_OPTION,
            "${OPTION_EXPIRATION_KEY}0" to "1",
            "${OPTION_STRIKE_PRICE_KEY}0" to "107",
            "${OPTION_TYPE_KEY}0" to "1",
            "${inputId.symbol}1" to "GOOG--210122P01665000",
            "${inputId.quantity}1" to "1",
            "${ORDER_TYPE_KEY}1" to orderType.typeCode,
            "${ORDER_TRIGGER_KEY}1" to "2",
            "${SIDE_KEY}1" to "3",
            "${POSITION_EFFECT_KEY}1" to "1",
            "${inputId.limitPrice}1" to "400",
            "${inputId.term}1" to inputId.termValueId.goodForDay,
            "${inputId.allOrNone}1" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}1" to VALUE_SECURITY_TYPE_OPTION,
            "${OPTION_EXPIRATION_KEY}1" to "1",
            "${OPTION_STRIKE_PRICE_KEY}1" to "1665",
            "${OPTION_TYPE_KEY}1" to "2",
            ORDER_COUNT_KEY to "2",
            FORM_TARGET_KEY to "otapreview",
            PLATFORM_KEY to "android",
            PREVIEW_ID_KEY to "1234567890",
            TRADE_X_KEY to "60"
        )

        verifyPlaceOutputWithGivenInput(input, output, "1234567890", listOf(orderType, orderType))
    }

    @Test
    fun `PREVIEW - LIMIT BUY 1 AAPL GOOD_FOR_DAY - MARKET BUY_OPEN 1 AAPL--210122C00112000 GOOD_FOR_DAY`() {
        val order1 = mapOf(
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "100",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.securityType to inputId.securityTypeValueId.stock,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.stock
        )
        val order2 = mapOf(
            inputId.symbol to "AAPL--210122C00112000:OPTNC",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.underlierSymbol to "AAPL",
            inputId.securityType to inputId.securityTypeValueId.option,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.option
        )
        val input = mapOf(
            tradeFormInputId.accountId to "83851862",
            tradeFormInputId.securityType to tradeFormInputId.securityTypeValueId.conditional,
            tradeFormInputId.order1 to order1,
            tradeFormInputId.order2 to order2
        )

        val output = mapOf(
            tradeFormInputId.accountId to "83851862",
            "${inputId.symbol}0" to "AAPL",
            "${inputId.action}0" to inputId.actionValueId.buy,
            "${inputId.quantity}0" to "1",
            "${inputId.priceType}0" to inputId.priceTypeValueId.limit,
            "${inputId.limitPrice}0" to "100",
            "${inputId.term}0" to inputId.termValueId.goodForDay,
            "${inputId.allOrNone}0" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}0" to VALUE_SECURITY_TYPE_STOCK,
            "${inputId.symbol}1" to "AAPL--210122C00112000",
            "${inputId.action}1" to inputId.actionValueId.buy,
            "${inputId.quantity}1" to "1",
            "${inputId.priceType}1" to inputId.priceTypeValueId.market,
            "${inputId.term}1" to inputId.termValueId.goodForDay,
            "${inputId.allOrNone}1" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}1" to VALUE_SECURITY_TYPE_OPTION,
            "${OPTION_EXPIRATION_KEY}1" to "1",
            "${OPTION_STRIKE_PRICE_KEY}1" to "112",
            "${OPTION_TYPE_KEY}1" to "1",
            ORDER_COUNT_KEY to "2",
            FORM_TARGET_KEY to "otacreateentry",
            PLATFORM_KEY to "android",
            PREVIEW_X_KEY to "65"
        )

        verifyPreviewOutputWithGivenInput(input, output)
    }

    @Test
    fun `PLACE - LIMIT BUY 1 AAPL GOOD_FOR_DAY - MARKET BUY_OPEN 1 AAPL--210122C00112000 GOOD_FOR_DAY`() {
        val order1 = mapOf(
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "100",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.securityType to inputId.securityTypeValueId.stock,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.stock
        )
        val order2 = mapOf(
            inputId.symbol to "AAPL--210122C00112000:OPTNC",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to inputId.allOrNoneValueCoder.encode(false),
            inputId.accountId to "83851862",
            inputId.underlierSymbol to "AAPL",
            inputId.securityType to inputId.securityTypeValueId.option,
            CONDITIONAL_ORDER_SECURITY_TYPE_ID to inputId.securityTypeValueId.option
        )
        val input = mapOf(
            tradeFormInputId.accountId to "83851862",
            tradeFormInputId.securityType to tradeFormInputId.securityTypeValueId.conditional,
            tradeFormInputId.order1 to order1,
            tradeFormInputId.order2 to order2
        )

        val output = mapOf(
            tradeFormInputId.accountId to "83851862",
            "${inputId.symbol}0" to "AAPL",
            "${ORDER_TYPE_KEY}0" to OrderType.EQUITY.typeCode,
            "${ORDER_TRIGGER_KEY}0" to "2",
            "${SIDE_KEY}0" to "2",
            "${POSITION_EFFECT_KEY}0" to "1",
            "${inputId.quantity}0" to "1",
            "${inputId.limitPrice}0" to "100",
            "${inputId.term}0" to inputId.termValueId.goodForDay,
            "${inputId.allOrNone}0" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}0" to VALUE_SECURITY_TYPE_STOCK,
            "${inputId.symbol}1" to "AAPL--210122C00112000",
            "${ORDER_TYPE_KEY}1" to OrderType.SIMPLE_OPTION.typeCode,
            "${ORDER_TRIGGER_KEY}1" to "1",
            "${SIDE_KEY}1" to "2",
            "${POSITION_EFFECT_KEY}1" to "1",
            "${inputId.quantity}1" to "1",
            "${inputId.term}1" to inputId.termValueId.goodForDay,
            "${inputId.allOrNone}1" to inputId.allOrNoneValueCoder.encode(false) as String,
            "${KEY_SECURITY_TYPE}1" to VALUE_SECURITY_TYPE_OPTION,
            "${OPTION_EXPIRATION_KEY}1" to "1",
            "${OPTION_STRIKE_PRICE_KEY}1" to "112",
            "${OPTION_TYPE_KEY}1" to "1",
            ORDER_COUNT_KEY to "2",
            PREVIEW_ID_KEY to "1234567890",
            FORM_TARGET_KEY to "otapreview",
            PLATFORM_KEY to "android",
            TRADE_X_KEY to "60"
        )

        verifyPlaceOutputWithGivenInput(input, output, "1234567890", listOf(OrderType.EQUITY, OrderType.SIMPLE_OPTION))
    }

    private fun createDelegate() = NewConditionalOrderTradeDelegate(fakeClient, inputId, tradeFormInputId, isTablet = false)

    private fun verifyPreviewOutputWithGivenInput(input: Map<String, Any>, expectedOutput: Map<String, String>) {
        runBlockingTest {
            createDelegate().previewOrder(input, MarketDestination.AUTO)
        }
        runBlockingTest {
            verify(fakeClient).previewOrder(AdvancedTradingApiClient.PATH_OTA, expectedOutput)
        }
    }

    private fun verifyPlaceOutputWithGivenInput(input: Map<String, Any>, output: Map<String, String>, previewId: String, orderTypes: List<OrderType>) {
        runBlockingTest {
            val argument = TradeOrderArguments(input, MarketDestination.AUTO)
            createDelegate().placeOrder(argument, createTradeOrder(previewId, orderTypes))
        }
        runBlockingTest {
            verify(fakeClient).placeOrder(AdvancedTradingApiClient.PATH_OTA, output)
        }
    }

    private fun createTradeOrder(previewId: String, orderTypes: List<OrderType>): TradeOrder {
        val response = mock<StubResponseInterface>()
        val trailingDetails = mock<TrailingStopOrderDetails>()
        whenever(response.trailingStopOrderDetails).thenReturn(listOf(trailingDetails, trailingDetails, trailingDetails))
        whenever(response.previewId).thenReturn(previewId)
        val orders = orderTypes.map { orderType ->
            val order = mock<AdvancedOrder>()
            whenever(order.orderType).thenReturn(orderType)
            order
        }
        whenever(response.orders).thenReturn(orders)

        return TradeOrder(previewId, "", response)
    }

    private interface StubResponseInterface : AdvancedOrderPreviewResponse, TrailingStopOrderResponse
}
