package com.etrade.mobilepro.chat.data.repo

import com.etrade.mobilepro.chat.api.ChatParamsModel
import com.etrade.mobilepro.chat.api.setup.models.ChatActiveStatusModel
import com.etrade.mobilepro.chat.api.setup.repo.ChatSetupRepo
import com.etrade.mobilepro.chat.api.setup.repo.ChatSetupResult
import com.etrade.mobilepro.chat.data.setup.repo.ChatSetupRepoImpl
import com.etrade.mobilepro.chat.data.setup.service.ChatSetupApiService
import com.google.gson.JsonSyntaxException
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
internal class ChatSetupRepoTest {

    private val fakeApiClient: ChatSetupApiService = mock()
    private lateinit var chatSetupRepo: ChatSetupRepo
    private lateinit var paramsModel: ChatParamsModel
    private lateinit var testChatActiveStatusModel: ChatActiveStatusModel

    @Before
    fun setup() {
        chatSetupRepo = ChatSetupRepoImpl(fakeApiClient)
        paramsModel = ChatParamsModel()
        testChatActiveStatusModel = ChatActiveStatusModel(isActive = true, metadata = chatTestMetadataModel)
    }

    @Test
    fun testGetChatActiveStatus() {
        runBlockingTest {
            whenever(fakeApiClient.getChatActiveStatus()).thenReturn(chatTestActiveStatusDto)

            assertEquals(chatSetupRepo.getChatActiveStatus().getOrThrow(), testChatActiveStatusModel)
        }
    }

    @Test
    fun testSetupChatSuccess() {
        runBlockingTest {
            prepareChatSetupTest(isError = false, isNonOperatingHour = false)

            verifyChatParamsModelSuccess(chatSetupRepo.setupChat(paramsModel))
        }
    }

    @Test
    fun testSetupChatError() {
        runBlockingTest {
            prepareChatSetupTest(isError = true, isNonOperatingHour = false)

            assertEquals(ChatSetupResult.Failure(TEST_ERROR_MESSAGE), chatSetupRepo.setupChat(paramsModel))
        }
    }

    @Test
    fun testSetupChatNonOperatingHours() {
        runBlockingTest {
            prepareChatSetupTest(isError = false, isNonOperatingHour = true)

            assertEquals(ChatSetupResult.NonOperatingHour(isHoliday = false), chatSetupRepo.setupChat(paramsModel))
        }
    }

    private fun verifyChatParamsModelSuccess(result: ChatSetupResult) {
        assertEquals(ChatSetupResult.Success, result)
        assertEquals(paramsModel.chatProductItems, chatProductItemsModel)
        assertEquals(paramsModel.chatContactId, TEST_PRODUCT_ID)
        assertEquals(paramsModel.chatCustomerDetailsModel, chatTestCustomerDetailsModel)
        assertEquals(paramsModel.metadata, chatTestMetadataModel)
    }

    private suspend fun prepareChatSetupTest(isError: Boolean, isNonOperatingHour: Boolean) {
        whenever(fakeApiClient.getChatActiveStatus()).thenReturn(chatTestActiveStatusDto)
        whenever(fakeApiClient.getCustomerDetailsForMobile(any())).thenReturn(chatTestCustomerDetailsForMobileDto)
        if (isNonOperatingHour) {
            whenever(fakeApiClient.getChatOperatingHours()).thenReturn(chatTestNonOperatingHoursDto)
        } else {
            whenever(fakeApiClient.getChatOperatingHours()).thenReturn(chatTestOperatingHoursDto)
        }
        whenever(fakeApiClient.getChatProducts()).thenReturn(chatTestProductsDto)
        if (isError) {
            whenever(fakeApiClient.getChatContactId()).thenThrow(JsonSyntaxException(TEST_ERROR_MESSAGE))
        } else {
            whenever(fakeApiClient.getChatContactId()).thenReturn(chatTestContactIdDto)
        }
    }
}
