package com.etrade.mobilepro.trade.help

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.trade.api.TradeOptionInfoType
import javax.inject.Inject

class TradeHelpViewModel @Inject constructor(
    private val res: Resources
) : ViewModel() {

    val title: LiveData<String>
        get() = _title
    private var _title: MutableLiveData<String> = MutableLiveData()

    val listItems: LiveData<List<TradeHelpItem>>
        get() = _listItems
    private var _listItems: MutableLiveData<List<TradeHelpItem>> = MutableLiveData()

    fun setInformationType(tradeOptionInfoType: TradeOptionInfoType, learnOtherPriceTypes: Boolean) {
        when (tradeOptionInfoType) {
            TradeOptionInfoType.PRICE_TYPE -> {
                setData(
                    res.getString(tradeOptionInfoType.getTitleId(learnOtherPriceTypes)),
                    getPriceTypeDefinitions()
                )
            }
            TradeOptionInfoType.PRICE_TYPE_OPTIONS -> {
                setData(res.getString(tradeOptionInfoType.getTitleId(learnOtherPriceTypes)), getPriceTypeOptionsDefinitions())
            }
            TradeOptionInfoType.ORDER_TYPE -> {
                setData(res.getString(tradeOptionInfoType.getTitleId(learnOtherPriceTypes)), getOrderTypeDefinitions())
            }
            TradeOptionInfoType.ORDER_TYPE_MF -> {
                setData(res.getString(tradeOptionInfoType.getTitleId(learnOtherPriceTypes)), getOrderTypeMfDefinitions())
            }
            TradeOptionInfoType.ORDER_TYPE_OPTIONS -> {
                setData(res.getString(tradeOptionInfoType.getTitleId(learnOtherPriceTypes)), getOrderTypeOptionDefinitions())
            }
            TradeOptionInfoType.SECURITY_TYPE -> {
                setData(res.getString(tradeOptionInfoType.getTitleId(learnOtherPriceTypes)), getSecurityTypeDefinitions())
            }
            TradeOptionInfoType.SECURITY_TYPE_OPTIONS -> {
                setData(res.getString(tradeOptionInfoType.getTitleId(learnOtherPriceTypes)), getSecurityTypeOptionDefinitions())
            }
        }
    }

    private fun setData(title: String, listItems: List<TradeHelpItem>) {
        _title.value = title
        _listItems.value = listItems
    }

    private fun getPriceTypeDefinitions(): List<TradeHelpItem> = mutableListOf<TradeHelpItem>().apply {
        addItem(R.string.trade_market_price_type, R.string.trade_market_price_type_description)
        addItem(R.string.trade_limit_price_type, R.string.trade_limit_price_type_description)
        addItem(R.string.trade_stop_on_quote_price_type, R.string.trade_stop_on_quote_price_type_description)
        addItem(R.string.trade_stop_limit_on_quote_price_type, R.string.trade_stop_limit_on_quote_price_type_description)
        addItem(R.string.trade_trailing_stop_dollar_price_type, R.string.trade_trailing_stop_dollar_price_type_description)
        addItem(R.string.trade_trailing_stop_percentage_price_type, R.string.trade_trailing_stop_percentage_price_type_description)
        addItem(R.string.trade_hidden_stop_price_type, R.string.trade_hidden_stop_price_type_description)
    }.toList()

    private fun getPriceTypeOptionsDefinitions(): List<TradeHelpItem> = mutableListOf<TradeHelpItem>().apply {
        addItem(R.string.trade_market_price_type, R.string.trade_market_price_type_description)
        addItem(R.string.trade_limit_price_type, R.string.trade_limit_price_type_description)
        addItem(R.string.trade_stop_on_quote_price_type, R.string.trade_stop_on_quote_price_type_description)
        addItem(R.string.trade_stop_limit_on_quote_price_type, R.string.trade_stop_limit_on_quote_price_type_description)
        addItem(R.string.trade_trailing_stop_dollar_price_type, R.string.trade_trailing_stop_dollar_price_type_description)
    }.toList()

    private fun getSecurityTypeDefinitions(): List<TradeHelpItem> = mutableListOf<TradeHelpItem>().apply {
        addItem(R.string.stocks, R.string.trade_stock_security_typ_description)
        addItem(R.string.options, R.string.trade_options_security_type_description)
        addItem(R.string.security_type_mutual_fund, R.string.trade_mutual_funds_security_type_description)
        addItem(R.string.conditional_one_trigger_other, R.string.trade_conditional_oto_security_type_description)
    }

    private fun getOrderTypeDefinitions(): List<TradeHelpItem> = mutableListOf<TradeHelpItem>().apply {
        addItem(R.string.buy, R.string.trade_help_content_order_type_buy)
        addItem(R.string.sell, R.string.trade_help_content_order_type_sell)
        addItem(R.string.sell_short, R.string.trade_help_content_order_type_sell_short)
        addItem(R.string.buy_to_cover, R.string.trade_help_content_order_type_buy_to_cover)
    }

    private fun getOrderTypeMfDefinitions(): List<TradeHelpItem> = mutableListOf<TradeHelpItem>().apply {
        addItem(R.string.buy, R.string.trade_help_content_order_type_buy)
        addItem(R.string.sell, R.string.trade_help_content_order_type_sell)
        addItem(R.string.exchange, R.string.trade_help_content_order_type_exchange)
    }

    private fun getSecurityTypeOptionDefinitions(): List<TradeHelpItem> = mutableListOf<TradeHelpItem>().apply {
        addItem(R.string.stocks, R.string.trade_stock_security_typ_description)
        addItem(R.string.options, R.string.trade_options_security_type_description)
        addItem(R.string.conditional_one_trigger_other, R.string.trade_conditional_oto_security_type_description)
    }

    private fun getOrderTypeOptionDefinitions(): List<TradeHelpItem> = mutableListOf<TradeHelpItem>().apply {
        addItem(R.string.trade_help_order_type_options_buy_open, R.string.trade_help_order_type_options_buy_open_desc)
        addItem(R.string.trade_help_order_type_options_sell_open, R.string.trade_help_order_type_options_sell_open_desc)
        addItem(R.string.trade_help_order_type_options_buy_close, R.string.trade_help_order_type_options_buy_close_desc)
        addItem(R.string.trade_help_order_type_options_sell_close, R.string.trade_help_order_type_options_sell_close_desc)
    }

    private fun MutableList<TradeHelpItem>.addItem(title: Int, description: Int) {
        this.add(TradeHelpItem(res.getString(title), res.getString(description)))
    }
}
