package com.etrade.mobilepro.util.secure

import java.util.Random

fun Random.nextByteArray(length: Int): ByteArray = ByteArray(length).apply(::nextBytes)
