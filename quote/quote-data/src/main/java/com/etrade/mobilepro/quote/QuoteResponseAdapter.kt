package com.etrade.mobilepro.quote

import com.etrade.mobilepro.dynamicui.addSubtypes
import com.etrade.mobilepro.quote.dto.BaseQuoteViewType
import com.etrade.mobilepro.quote.dto.QuoteResponse
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory

fun createQuoteResponseAdapter(): PolymorphicJsonAdapterFactory<QuoteResponse> {
    return addSubtypes(
        factory = PolymorphicJsonAdapterFactory.of(QuoteResponse::class.java, "typeCode"),
        enumTypes = BaseQuoteViewType.values().toList()
    )
}
