package com.etrade.mobilepro.trade.presentation.form.component

import android.content.res.Resources
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.dynamic.form.api.BaseInput
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.util.ValueCoder
import com.etrade.mobilepro.util.ValueFormatter

internal class StrategyTypeInput(
    id: String,
    resources: Resources
) : BaseInput<StrategyType>(id, StrategyTypeValueCoder(), StrategyTypeValueFormatter(resources)) {

    override val isVisible: LiveData<Boolean>
        get() = hasValue

    private class StrategyTypeValueCoder : ValueCoder<StrategyType> {
        override fun encode(value: StrategyType): Any = value.typeCode
        override fun decode(encoded: Any): StrategyType? = StrategyType.fromTypeCode(encoded as? String)
    }

    private class StrategyTypeValueFormatter(private val resources: Resources) : ValueFormatter<StrategyType> {
        override fun format(value: StrategyType): CharSequence = resources.getString(value.getLabel())
    }
}
