package com.etrade.mobilepro.util.android

import android.content.Context
import android.content.Intent
import android.provider.CalendarContract
import java.util.concurrent.TimeUnit

// Calendar event end time calculated as start_time + 30 minutes
private const val CALENDAR_EVENT_END_TIME_ADDITIONAL_MINUTES = 30L

/**
 * @param title - event title
 * @param startTimeMillis - event start time in millis
 * @param endTimeMillis - event end time in millis
 */
data class CalendarEvent(
    val title: String,
    val startTimeMillis: Long,
    val endTimeMillis: Long = startTimeMillis + TimeUnit.MINUTES.toMillis(CALENDAR_EVENT_END_TIME_ADDITIONAL_MINUTES)
)

fun addToCalendar(context: Context, event: CalendarEvent) {
    val intent = Intent(Intent.ACTION_EDIT)
        .setData(CalendarContract.Events.CONTENT_URI)
        .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, event.startTimeMillis)
        .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, event.endTimeMillis)
        .putExtra(CalendarContract.Events.TITLE, event.title)

    context.startActivity(intent)
}
