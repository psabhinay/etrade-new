package com.etrade.mobilepro.calendar

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.res.Configuration
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.accessibility.AccessibilityEvent
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.widget.ContentLoadingProgressBar
import com.etrade.mobilepro.calendar.databinding.ViewCaledarDialogBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.threeten.bp.LocalDate
import org.threeten.bp.format.TextStyle
import java.util.Locale

class CalendarDialog : FrameLayout {
    private lateinit var progressBar: ContentLoadingProgressBar
    private lateinit var date: TextView
    private lateinit var calendar: CalendarView
    private lateinit var calendarMessage: TextView
    private lateinit var year: TextView
    private lateinit var dummy: View
    private lateinit var titleContainer: LinearLayout

    var isLoading: Boolean
        get() { return calendar.visibility != View.VISIBLE }
        set(value) {
            if (value) {
                progressBar.visibility = View.VISIBLE
                progressBar.show()
                calendar.visibility = View.INVISIBLE
            } else {
                progressBar.hide()
                calendar.visibility = View.VISIBLE
            }
        }

    var selectedDate: LocalDate? = null
        get() = calendar.selectedDate
        @SuppressLint("SetTextI18n")
        set(value) {
            field = value

            calendar.selectedDate = value

            (value ?: calendar.currentDate).let {
                titleContainer.contentDescription = "${it.dayOfWeek.getDisplayName(TextStyle.FULL, Locale.US)} $it"
                year.text = "${it.year}"
                date.text = if (context.resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                    " "
                } else {
                    "\n"
                }.let { comma ->
                    "${it.dayOfWeek.getDisplayName(TextStyle.SHORT, Locale.US)},$comma${it.month.getDisplayName(TextStyle.SHORT, Locale.US)} ${it.dayOfMonth}"
                }
            }
        }

    private var clickableDaysPredicate: (LocalDate) -> Boolean = { false }
        set(value) {
            field = value

            calendar.clickableDaysPredicate = value
            calendar.disabledDaysPredicate = { !value(it) }
        }

    private var selectedDateCalendarPage: Int = MIDDLE_OF_CALENDAR_RANGE

    private var onDateClick: ((LocalDate?, Int) -> Unit)? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        contentDescription = resources.getString(R.string.label_calendar)

        setupBinding()

        dummy.requestFocus()

        progressBar.hide()

        calendar.apply {
            updateCalendarMode(CalendarMode.MONTH)
            disabledDaysPredicate = { !this@CalendarDialog.clickableDaysPredicate(it) }
            clickableDaysPredicate = this@CalendarDialog.clickableDaysPredicate
            onDateSelected = {
                this@CalendarDialog.selectedDate = it
                selectedDateCalendarPage = currentCalendarPage
                onDateClick?.invoke(it, currentCalendarPage)
            }
        }

        year.setAccessibilityDelegate(object : AccessibilityDelegate() {
            override fun onPopulateAccessibilityEvent(host: View?, event: AccessibilityEvent?) {
                // The super method would normally add the text, but we want to
                // suppress year reading when dialog appears on the screen
                event?.text?.add("")
            }
        })
    }

    private fun setupBinding() {
        val binding = ViewCaledarDialogBinding.inflate(LayoutInflater.from(context), this, true)
        progressBar = binding.progressBar
        date = binding.date
        calendar = binding.calendar
        year = binding.year
        dummy = binding.dummy
        calendarMessage = binding.calendarMessage
        titleContainer = binding.titleContainer
    }

    @Suppress("LongParameterList")
    fun show(
        onDateSelected: (LocalDate?, Int) -> Unit,
        onDateClick: ((LocalDate?, Int) -> Unit)? = null,
        clickableDaysPredicate: (LocalDate) -> Boolean = { true },
        onCancel: () -> Unit = {},
        selectedDate: LocalDate? = null,
        currentCalendarPage: Int = MIDDLE_OF_CALENDAR_RANGE,
        message: String? = null
    ) {
        createDialog(onDateSelected, onDateClick, clickableDaysPredicate, onCancel, selectedDate, currentCalendarPage, message).show()
    }

    fun refresh() {
        calendar.refresh()
    }

    @Suppress("LongParameterList")
    fun createDialog(
        onDateSelected: (LocalDate?, Int) -> Unit,
        onDateClick: ((LocalDate?, Int) -> Unit)? = null,
        clickableDaysPredicate: (LocalDate) -> Boolean = { true },
        onCancel: () -> Unit = {},
        selectedDate: LocalDate? = null,
        currentCalendarPage: Int = MIDDLE_OF_CALENDAR_RANGE,
        message: String? = null
    ): Dialog {
        this.clickableDaysPredicate = clickableDaysPredicate
        this.selectedDate = selectedDate
        this.selectedDateCalendarPage = currentCalendarPage
        this.onDateClick = onDateClick
        calendar.setCalendarSelectedRange(currentCalendarPage)

        setCalendarMessage(message)

        return MaterialAlertDialogBuilder(context)
            .setPositiveButton(R.string.ok) { _, _ ->
                onDateSelected(this.selectedDate, this.selectedDate?.let { selectedDateCalendarPage } ?: currentCalendarPage)
            }
            .setNegativeButton(
                resources.getString(R.string.cancel).uppercase(Locale.getDefault())
            ) { dlg, _ ->
                onCancel()
                dlg.cancel()
            }
            .setView(this)
            .create()
    }

    private fun setCalendarMessage(message: String?) {
        calendarMessage.apply {
            text = message

            visibility = if (message.isNullOrEmpty()) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }
    }
}
