package com.etrade.mobilepro.screener.presentation.etf.performance

import com.etrade.mobilepro.screener.api.filter.etf.EtfPerformanceFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class WithinCategory : ScreenerFilterEvent<EtfPerformanceFilter, EtfPerformanceViewModel.ViewState, Boolean> {
    BOTTOM_20 {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(withinCategoryBottom20 = eventValue))
    },
    BELOW_MID_20 {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(withinCategoryBelowMid20 = eventValue))
    },
    MID_20 {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(withinCategoryMid20 = eventValue))
    },
    ABOVE_MID_20 {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(withinCategoryAboveMid20 = eventValue))
    },
    TOP_20 {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(withinCategoryTop20 = eventValue))
    };

    override fun conditionalUpdate(
        viewState: EtfPerformanceViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<EtfPerformanceViewModel.ViewState> {
        var newState = update(viewState, eventValue)
        if (!newState.filter.isWithinCategoryPerformanceValuesSelected()) {
            newState = clearPeriodButtons(newState)
        }
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = (
                newState.filter.isWithinCategoryPerformanceValuesSelected() &&
                    newState.filter.isWithinCategoryPerformancePeriodSelected()
                ) ||
                (newState.filter.isWithinCategoryEmpty() && !viewState.filter.isWithinCategoryEmpty())
        )
    }

    private fun clearPeriodButtons(viewState: EtfPerformanceViewModel.ViewState): EtfPerformanceViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                withinCategoryOverLast1Month = false,
                withinCategoryOverLast3Months = false,
                withinCategoryOverLast6Months = false,
                withinCategoryOverLastYTD = false,
                withinCategoryOverLast1Year = false,
                withinCategoryOverLast3Years = false,
                withinCategoryOverLast5Years = false,
                withinCategoryOverLast10Years = false
            )
        )
}

internal enum class WithinCategoryPeriod : ScreenerFilterEvent<EtfPerformanceFilter, EtfPerformanceViewModel.ViewState, Boolean> {
    ONE_MONTH {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    withinCategoryOverLast1Month = eventValue && viewState.filter.isWithinCategoryPerformanceValuesSelected()
                )
            )
    },
    THREE_MONTHS {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    withinCategoryOverLast3Months = eventValue && viewState.filter.isWithinCategoryPerformanceValuesSelected()
                )
            )
    },
    SIX_MONTHS {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    withinCategoryOverLast6Months = eventValue && viewState.filter.isWithinCategoryPerformanceValuesSelected()
                )
            )
    },
    YTD {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    withinCategoryOverLastYTD = eventValue && viewState.filter.isWithinCategoryPerformanceValuesSelected()
                )
            )
    },
    ONE_YEAR {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    withinCategoryOverLast1Year = eventValue && viewState.filter.isWithinCategoryPerformanceValuesSelected()
                )
            )
    },
    THREE_YEARS {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    withinCategoryOverLast3Years = eventValue && viewState.filter.isWithinCategoryPerformanceValuesSelected()
                )
            )
    },
    FIVE_YEARS {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    withinCategoryOverLast5Years = eventValue && viewState.filter.isWithinCategoryPerformanceValuesSelected()
                )
            )
    },
    TEN_YEARS {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    withinCategoryOverLast10Years = eventValue && viewState.filter.isWithinCategoryPerformanceValuesSelected()
                )
            )
    }
}

internal enum class NavCategory : ScreenerFilterEvent<EtfPerformanceFilter, EtfPerformanceViewModel.ViewState, Boolean> {
    BOTTOM_20 {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(navReturnBottom20 = eventValue))
    },
    BELOW_MID_20 {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(navReturnBelowMid20 = eventValue))
    },
    MID_20 {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(navReturnMid20 = eventValue))
    },
    ABOVE_MID_20 {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(navReturnAboveMid20 = eventValue))
    },
    TOP_20 {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(navReturnTop20 = eventValue))
    };

    override fun conditionalUpdate(
        viewState: EtfPerformanceViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<EtfPerformanceViewModel.ViewState> {
        var newState = update(viewState, eventValue)
        if (!newState.filter.isNavReturnPerformanceValuesSelected()) {
            newState = clearPeriodButtons(newState)
        }
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = (
                newState.filter.isNavReturnPerformanceValuesSelected() &&
                    newState.filter.isNavReturnPerformancePeriodSelected()
                ) ||
                (newState.filter.isNavReturnEmpty() && !viewState.filter.isNavReturnEmpty())
        )
    }

    private fun clearPeriodButtons(viewState: EtfPerformanceViewModel.ViewState): EtfPerformanceViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                navReturnOverLast1Month = false,
                navReturnOverLast3Months = false,
                navReturnOverLast6Months = false,
                navReturnOverLastYTD = false,
                navReturnOverLast1Year = false,
                navReturnOverLast3Years = false,
                navReturnOverLast5Years = false,
                navReturnOverLast10Years = false
            )
        )
}

internal enum class NavCategoryPeriod : ScreenerFilterEvent<EtfPerformanceFilter, EtfPerformanceViewModel.ViewState, Boolean> {
    ONE_MONTH {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    navReturnOverLast1Month = eventValue && viewState.filter.isNavReturnPerformanceValuesSelected()
                )
            )
    },
    THREE_MONTHS {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    navReturnOverLast3Months = eventValue && viewState.filter.isNavReturnPerformanceValuesSelected()
                )
            )
    },
    SIX_MONTHS {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    navReturnOverLast6Months = eventValue && viewState.filter.isNavReturnPerformanceValuesSelected()
                )
            )
    },
    YTD {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    navReturnOverLastYTD = eventValue && viewState.filter.isNavReturnPerformanceValuesSelected()
                )
            )
    },
    ONE_YEAR {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    navReturnOverLast1Year = eventValue && viewState.filter.isNavReturnPerformanceValuesSelected()
                )
            )
    },
    THREE_YEARS {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    navReturnOverLast3Years = eventValue && viewState.filter.isNavReturnPerformanceValuesSelected()
                )
            )
    },
    FIVE_YEARS {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    navReturnOverLast5Years = eventValue && viewState.filter.isNavReturnPerformanceValuesSelected()
                )
            )
    },
    TEN_YEARS {
        override fun update(viewState: EtfPerformanceViewModel.ViewState, eventValue: Boolean): EtfPerformanceViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(
                    navReturnOverLast10Years = eventValue && viewState.filter.isNavReturnPerformanceValuesSelected()
                )
            )
    }
}
