package com.etrade.mobilepro.landing.presentation.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.landing.presentation.LandingPageSelectionViewModel
import com.etrade.mobilepro.landing.presentation.R
import com.etrade.mobilepro.landing.presentation.adapter.LandingPageHeader
import com.etrade.mobilepro.landing.presentation.databinding.LandingPageSelectionItemHeaderBinding

internal class LandingPageHeaderViewHolder(private val binding: LandingPageSelectionItemHeaderBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bindTo(item: LandingPageHeader) {
        when (item.status) {
            LandingPageSelectionViewModel.ViewState.Status.LOADING,
            LandingPageSelectionViewModel.ViewState.Status.MAIN_ITEMS -> {
                binding.title.text = itemView.context.getString(R.string.landing_page_selection_header_main_title)
                binding.subtitle.text = itemView.context.getString(R.string.landing_page_selection_header_main_subtitle)
                binding.subtitle.visibility = View.VISIBLE
            }
            LandingPageSelectionViewModel.ViewState.Status.PORTFOLIO_ITEMS -> {
                binding.title.text = itemView.context.getString(R.string.landing_page_selection_header_portfolio)
                binding.subtitle.visibility = View.GONE
            }
        }
    }
}
