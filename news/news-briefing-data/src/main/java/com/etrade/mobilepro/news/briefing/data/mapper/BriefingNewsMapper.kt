package com.etrade.mobilepro.news.briefing.data.mapper

import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.briefing.data.dto.BriefingNewsDto
import com.etrade.mobilepro.util.formatters.parseCustomDateFromString
import java.util.Locale

private val BRIEFING_NEWS_INPUT_FORMATS =
    listOf(
        "yyyy-MM-dd'T'HH:mm:ssyyyy-MM-dd HH:mm:ss 'ET'",
        "MM-dd-yy HH:mm:ss 'ET'"
    )
private const val BRIEFING_NEWS_OUTPUT_FORMAT = "MM/dd/yyyy '|' hh:mm a 'EDT'"

class BriefingNewsMapper(private val htmlParser: (html: String) -> String) {

    fun map(xmlDto: BriefingNewsDto): List<NewsItem> {
        val payload = xmlDto.payloadDto ?: return emptyList()
        val formattedDate = payload.dateTime?.let { inputDate ->
            BRIEFING_NEWS_INPUT_FORMATS.mapNotNull { format ->
                parseCustomDateFromString(
                    inputDate,
                    format,
                    BRIEFING_NEWS_OUTPUT_FORMAT
                )
            }.firstOrNull {
                it.isNotEmpty()
            }
        }?.uppercase(Locale.getDefault()) ?: ""
        val item = NewsItem.Text(
            newsDateTimeSource = formattedDate,
            title = htmlParser(payload.title ?: ""),
            url = "",
            symbols = emptyList(),
            content = htmlParser(payload.content ?: ""),
            docId = (payload.stockTickerId ?: "") + (payload.dateTime ?: "")
        )
        return listOf(item)
    }
}
