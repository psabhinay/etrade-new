package com.etrade.mobilepro.checkdeposit.usecases.api

import androidx.annotation.IntDef

const val CHECK_FRONT = 0
const val CHECK_BACK = 1

interface UploadCheckImage {
    /**
     * Indicates whether you are uploading the front or back of the check
     */
    @CheckSide val checkSide: Int
    val checkImage: ByteArray

    /**
     * The "instNo" retrieved from the API to return check deposit eligible accounts"
     */
    val instNo: String

    /**
     * The account identifier
     */
    val accountNumber: String

    /**
     * The amount to deposit
     */
    val depositAmount: String

    @Retention(AnnotationRetention.SOURCE)
    @IntDef(CHECK_FRONT, CHECK_BACK)
    annotation class CheckSide
}
