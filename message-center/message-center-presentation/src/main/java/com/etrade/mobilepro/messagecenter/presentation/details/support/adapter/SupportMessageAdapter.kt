package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter

import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.delegate.SupportMessageHeaderDelegate
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.delegate.SupportMessageLetterDelegate
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.delegate.SupportMessageReplyDelegate
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.delegate.SupportMessageSeparatorDelegate
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class SupportMessageAdapter(
    replyContentListener: (String) -> Unit
) : ListDelegationAdapter<MutableList<SupportMessageItem>>() {
    init {
        delegatesManager.addDelegate(SupportMessageHeaderDelegate())
        delegatesManager.addDelegate(SupportMessageReplyDelegate(replyContentListener))
        delegatesManager.addDelegate(SupportMessageSeparatorDelegate())
        delegatesManager.addDelegate(SupportMessageLetterDelegate())
        items = mutableListOf()
    }

    fun submitItems(supportMessageItems: List<SupportMessageItem>, actionOnReplyBlockStateChange: (Boolean) -> Unit) {
        val previousHadReplyBlock = items.find { it is Reply } != null
        val newHasReplyBlock = supportMessageItems.find { it is Reply } != null
        dispatchUpdates(items, supportMessageItems)
        items.clear()
        items.addAll(supportMessageItems)
        if (previousHadReplyBlock != newHasReplyBlock) {
            actionOnReplyBlockStateChange.invoke(newHasReplyBlock)
        }
    }
}
