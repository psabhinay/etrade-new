package com.etrade.mobilepro.chart.util

import com.etrade.mobilepro.chart.model.ChartConfiguration
import com.etrade.mobilepro.chart.model.ChartTimeFrame
import com.etrade.mobilepro.util.mapDateAccessibility
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import kotlin.math.abs

private const val COMMA = ", "
private const val MAX_DAILY_RECORDS = 130 // it's plotting 6 hours 30 mins in 3 min intervals
private const val SPACE = " "

internal fun LineChart.createContentDescription(
    xValues: List<Pair<Float, String>>,
    chartConfiguration: ChartConfiguration,
    dataSets: List<LineDataSet>? = null
) {
    val contentDescription: StringBuilder = StringBuilder()
    val rightAxisValueFormatter: ValueFormatter? = axisRight.valueFormatter

    xValues.forEach { (xValue, xFormattedValue) ->
        contentDescription.append(xFormattedValue.mapDateAccessibility()).append(COMMA)
        dataSets?.forEach { dataSet ->
            val timeValues = dataSet.getTimeFrameValues(chartConfiguration, xValue)
            val entry = dataSet.findPeriodEntry(chartConfiguration, timeValues, xValue)
            entry?.let {
                contentDescription
                    .append(dataSet.label.mapIndex())
                    .append(SPACE)
                    .append(rightAxisValueFormatter?.run { getFormattedValue(it.y) } ?: it.y)
                    .append(COMMA)
            }
        }
    }

    this.contentDescription = contentDescription
}

private fun findExactlyOrClosest(of: Float, orthogonalEntries: List<Entry>): Entry? {
    val matchesExactly = findExactlyOrNull(of, orthogonalEntries)
    if (matchesExactly != null) {
        return matchesExactly
    }

    var min = Float.MAX_VALUE
    var closestEntry: Entry? = null
    for (entry in orthogonalEntries) {
        val diff = abs(entry.x - of)
        if (diff < min) {
            min = diff
            closestEntry = entry
        }
    }
    return closestEntry
}

private fun findExactlyOrNull(of: Float, orthogonalEntries: List<Entry>) =
    orthogonalEntries.firstOrNull { entry ->
        entry.x == of
    }

private fun LineDataSet.findPeriodEntry(chartConfiguration: ChartConfiguration, timeValues: List<Entry>, xValue: Float): Entry? {
    return if (chartConfiguration.timeFrame == ChartTimeFrame.Day && timeValues.isEmpty() && entryCount == MAX_DAILY_RECORDS) {
        values.last()
    } else {
        findExactlyOrClosest(xValue, timeValues)
    }
}

private fun LineDataSet.getTimeFrameValues(chartConfiguration: ChartConfiguration, xValue: Float): List<Entry> {
    return if (chartConfiguration.timeFrame == ChartTimeFrame.Day && entryCount < MAX_DAILY_RECORDS) {
        values.filter {
            it.x >= xValue
        }
    } else {
        values
    }
}

private fun String.mapIndex(): String? = when (this) {
    "DJIND" -> "DOW"
    "COMP.IDX" -> "NASDAQ"
    "SPX" -> "S&P 500"
    else -> this
}
