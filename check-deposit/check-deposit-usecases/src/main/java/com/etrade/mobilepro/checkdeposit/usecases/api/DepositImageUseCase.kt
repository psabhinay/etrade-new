package com.etrade.mobilepro.checkdeposit.usecases.api

import com.etrade.mobilepro.checkdeposit.usecases.deposit.CheckDepositResult
import com.etrade.mobilepro.checkdeposit.usecases.deposit.DepositImageRequest
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.util.UseCase

interface DepositImageUseCase : UseCase<DepositImageRequest, ETResult<CheckDepositResult>>
