package com.etrade.mobilepro.checkdeposit.presentation.inputvalidation.amount

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.CoreMatchers.isA
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

internal class DepositAmountValidatorTest {

    val sut = DepositAmountValidator { "invalid" }

    @Test
    fun `allows single decimal for cents value`() {
        val result = sut.validate("5.5")
        assertThat((result as InputFieldManager.Value.Valid).content, `is`("5.50"))
    }

    @Test
    fun `treats blank strings as invalid`() {
        val result = sut.validate(" ")
        assertThat(result as InputFieldManager.Value.Invalid, isA(InputFieldManager.Value.Invalid::class.java))
    }

    @Test
    fun `allows for omission of dollar amount if decimal is present`() {
        val result = sut.validate(".25")
        assertThat((result as InputFieldManager.Value.Valid).content, `is`("0.25"))
    }

    @Test
    fun `allows for omission of cents amount if dollars are present`() {
        val result = sut.validate("50")
        assertThat((result as InputFieldManager.Value.Valid).content, `is`("50.00"))
    }

    @Test
    fun `allows for input with improperly placed commas`() {
        val result = sut.validate("0,01,0.25")
        assertThat((result as InputFieldManager.Value.Valid).content, `is`("10.25"))
    }

    @ParameterizedTest
    @ValueSource(strings = ["100000.001", "100000.0099"])
    fun `allows values exceeding max value by cents fracture`(testInput: String) {
        val result = sut.validate(testInput)
        assertThat((result as InputFieldManager.Value.Valid).content, `is`("100000.00"))
    }

    @ParameterizedTest
    @ValueSource(strings = ["100000.01", "1000000", "999999999999.99"])
    fun `no max value validation`(testInput: String) {
        val result = sut.validate(testInput)
        assertThat(result, instanceOf(InputFieldManager.Value.Valid::class.java))
    }

    @ParameterizedTest
    @ValueSource(strings = ["cat", "0.0.0", "-0.20"])
    fun `returns validation failure for invalid currency amounts`(testInput: String) {
        val result = sut.validate(testInput)
        assertThat(result, instanceOf(InputFieldManager.Value.Invalid::class.java))
    }

    @ParameterizedTest
    @ValueSource(strings = ["3", "03", "02.25", ".1", ".10", "1.255"])
    fun `returns validation success for valid currency input`(testInput: String) {
        val result = sut.validate(testInput)
        assertThat(result, instanceOf(InputFieldManager.Value.Valid::class.java))
    }

    @ParameterizedTest
    @ValueSource(strings = ["3.1599", "3.000"])
    fun `truncates additional decimal digits after 2 places`(testInput: String) {
        val result = sut.validate(testInput)
        assertThat(result, instanceOf(InputFieldManager.Value.Valid::class.java))
    }
}
