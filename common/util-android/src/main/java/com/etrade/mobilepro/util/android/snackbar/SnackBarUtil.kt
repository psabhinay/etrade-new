package com.etrade.mobilepro.util.android.snackbar

import android.content.Context
import android.view.View
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.R
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.addOnShown
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

/**
 * Class to be used when generating snackbars. The snackbars generated from this utility will automatically dismiss
 * themselves when the lifecycle the utility is associated with is destroyed.
 *
 * Retry & error snackbars will also be a no-operation when the device has no network connectivity.
 */
interface SnackBarUtil {
    fun snackbar(message: CharSequence, duration: Int = Snackbar.LENGTH_INDEFINITE): Snackbar?
    fun snackbarWithAction(message: CharSequence, actionText: CharSequence, duration: Int = Snackbar.LENGTH_INDEFINITE, action: (View) -> Unit): Snackbar?
    fun snackbarWithCustomViewAction(
        message: CharSequence,
        titleId: Int,
        action: () -> Unit,
        duration: Int = Snackbar.LENGTH_INDEFINITE,
        customerViewProvider: (Context, Int, listener: () -> Unit) -> View = ::buttonWithCarrot
    ): Snackbar?
    fun retrySnackbar(message: CharSequence? = null, dismissOnPause: Boolean = false, actionOnRetry: () -> Unit): Snackbar?
    fun errorSnackbar(message: CharSequence? = null, duration: Int = Snackbar.LENGTH_SHORT): Snackbar?

    companion object
}

class SnackbarUtilFactory @Inject constructor(
    private val connectionNotifier: ConnectionNotifier,
    context: Context
) {
    private val resources = context.resources
    private val snackbarTheme = createSnackbarTheme(context)
    private val generalErrorMessage: String
        get() = resources.getString(R.string.error_message_general)

    fun createSnackbarUtil(lifecycleOwner: () -> LifecycleOwner?, view: () -> View?): SnackBarUtil = DefaultSnackbarUtil(lifecycleOwner, view)

    private inner class DefaultSnackbarUtil(private val lifecycleOwner: () -> LifecycleOwner?, private val view: () -> View?) : SnackBarUtil {

        override fun snackbar(message: CharSequence, duration: Int): Snackbar? = view()?.let {
            makeThemedSnackbar(it, message, duration, snackbarTheme).also { snackBar ->
                snackBar.installAutoDismissOnPause()
            }
        }

        override fun errorSnackbar(message: CharSequence?, duration: Int) = if (connectionNotifier.connected) {
            snackbar(message ?: generalErrorMessage, duration)
        } else {
            null
        }

        override fun retrySnackbar(message: CharSequence?, dismissOnPause: Boolean, actionOnRetry: () -> Unit): Snackbar? = view()?.let {
            if (connectionNotifier.connected) {
                try {
                    val snackbar = makeThemedSnackbar(it, message ?: generalErrorMessage, Snackbar.LENGTH_INDEFINITE, snackbarTheme)
                    val observer = snackbar.getObserver(dismissOnPause)
                    snackbar.setAction(resources.getString(R.string.retry)) {
                        lifecycleOwner()?.lifecycle?.removeObserver(observer)
                        snackbar.dismiss()
                        actionOnRetry()
                    }
                    snackbar
                } catch (ex: IllegalArgumentException) {
                    null
                }
            } else {
                null
            }
        }

        override fun snackbarWithAction(message: CharSequence, actionText: CharSequence, duration: Int, action: (View) -> Unit): Snackbar? = view()?.let {
            makeThemedSnackbar(it, message, duration, snackbarTheme).apply {
                setAction(actionText, action)
                installAutoDismissOnPause()
            }
        }

        override fun snackbarWithCustomViewAction(
            message: CharSequence,
            titleId: Int,
            action: () -> Unit,
            duration: Int,
            customerViewProvider: (Context, Int, listener: () -> Unit) -> View
        ): Snackbar? = view()?.let {
            makeThemedSnackbar(it, message, duration, snackbarTheme).apply {
                (this.view as? Snackbar.SnackbarLayout)?.addView(
                    customerViewProvider(it.context, titleId) {
                        dismiss()
                        action()
                    }
                )
                installAutoDismissOnPause()
            }
        }

        private fun Snackbar.getObserver(dismissOnPause: Boolean) = if (dismissOnPause) {
            installAutoDismissOnPause()
        } else {
            installAutoDismiss()
        }

        private fun Snackbar.installAutoDismissOnPause(): DefaultLifecycleObserver {
            val observer = object : DefaultLifecycleObserver {

                override fun onPause(owner: LifecycleOwner) {
                    dismiss()
                    owner.lifecycle.removeObserver(this)
                }

                override fun onResume(owner: LifecycleOwner) {
                    show()
                }
            }
            lifecycleOwner()?.lifecycle?.addObserver(observer)
            return observer
        }

        private fun Snackbar.installAutoDismiss(): DefaultLifecycleObserver {
            val observer = object : DefaultLifecycleObserver {

                override fun onDestroy(owner: LifecycleOwner) {
                    dismiss()
                    owner.lifecycle.removeObserver(this)
                }

                override fun onResume(owner: LifecycleOwner) {
                    show()
                }
            }
            lifecycleOwner()?.lifecycle?.addObserver(observer)
            return observer
        }
    }
}

/**
 * Function created to fix a bug regarding bottomSheetFragment
 *
 * If we have a bottomSheet, and a LiveData is being consumed by the full screen fragment and by
 * the bottomSheetFragment at the same time, then the full screen fragment consumes the content first,
 * but it cannot do anything with it because we have a view on top of it (the bottomSheetFragment);
 * and then, when the bottomSheetFragment tries to consume the LiveData's content, it can't because
 * the full screen fragment already consumed it (although it did nothing with it)
 *
 * With this change we are sure that the content is only consumed if the observer was able to show
 * the snackbar
 */
fun SnackBarUtil.showIfNotConsumed(signal: ConsumableLiveEvent<String>, message: String? = null) {
    if (signal.isConsumed) { return }

    snackbar(message ?: signal.peekContent(), Snackbar.LENGTH_SHORT)
        ?.addOnShown(signal::consume)
        ?.show()
}
