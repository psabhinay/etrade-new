package com.etrade.mobilepro.trade.presentation.form.delegate

import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter

interface TradeFormFactoryDelegate {

    fun createForm(
        accounts: List<TradeAccount>,
        accountDelegate: SelectedAccountDelegate,
        parameter: CreateTradeFormParameter,
        updateForm: OnFormChangeListener
    ): Form

    interface OnFormChangeListener {
        fun onSecurityType(securityType: SecurityType)
        fun onAdvancedOrderType(formType: TradeFormType, snapshot: InputValuesSnapshot)
    }
}
