package com.etrade.mobilepro.trade.api.stock

import com.etrade.mobilepro.trade.api.ConfirmedOrder

interface ConfirmedStockOrder : ConfirmedOrder, StockOrder
