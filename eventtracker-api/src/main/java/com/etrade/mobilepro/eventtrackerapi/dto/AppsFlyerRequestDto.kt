package com.etrade.mobilepro.eventtrackerapi.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AppsFlyerRequestDto(
    @Json(name = "subject_request_id")
    val subjectRequestId: String,
    @Json(name = "subject_request_type")
    val subjectRequestType: String,
    @Json(name = "submitted_time")
    val submittedRequestTime: String,
    @Json(name = "subject_identities")
    val subjectIdentities: List<SubjectIdentities>,
    @Json(name = "property_id")
    val propteryId: String,
    @Json(name = "status_callback_urls")
    val statusCallbackUrl: List<String>
)

@JsonClass(generateAdapter = true)
data class SubjectIdentities(
    @Json(name = "identity_type")
    val identityType: String,
    @Json(name = "identity_value")
    val identityValue: String,
    @Json(name = "identity_format")
    val identityFormat: String
)
