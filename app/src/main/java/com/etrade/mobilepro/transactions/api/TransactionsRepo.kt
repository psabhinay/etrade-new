package com.etrade.mobilepro.transactions.api

import com.etrade.mobilepro.transactions.TransactionsResponseInfo
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable

interface TransactionsRepo {
    fun getTransactions(transactionsRequest: TransactionsRequest, cacheExpiration: Long?): Observable<Resource<TransactionsResponseInfo>>
}
