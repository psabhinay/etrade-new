package com.etrade.mobilepro.quote.mapper

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.dto.QuoteDto
import com.etrade.mobilepro.quote.dto.QuoteStockDto
import com.etrade.mobilepro.quoteapi.QuoteTemplateResources
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem
import com.etrade.mobilepro.testutil.getObjectFromJson
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

private const val GREEN = 0xFF00FF00.toInt()
private const val RED = 0xFFFF0000.toInt()

@RunWith(RobolectricTestRunner::class)
internal class MobileQuoteWidgetItemMapperTest {

    private lateinit var sut: MobileQuoteWidgetItemMapper

    private lateinit var mapperResources: QuoteTemplateResources

    @Before
    fun setUp() {
        mapperResources = getQuoteResources()
    }

    @Test
    fun `maps QuotesDto Json to QuoteWidgetItem`() {
        sut = MobileQuoteWidgetItemMapper(mapperResources)
        val mockQuoteJson = getMockQuotes()!!
        val quotes = MobileQuoteMapper().map(mockQuoteJson)
        val result = sut.map(quotes)

        assertNotNull(result)
        assertEquals("AAPL", result.symbol)
        assertEquals(InstrumentType.EQ, result.instrumentType)
        assertEquals("AAPL - Apple Inc Com", result.title)
        assertEquals("188.72", result.lastPrice.value?.price)
        assertEquals("100", result.lastPrice.value?.size)
        assertEquals("As of 04:15 PM EDT 03/28/2019", result.asOfDateTime.value)
        assertEquals("0.250000", result.change.value?.change)
        assertEquals("0.13", result.change.value?.changePercent)
        assertEquals("189.45", result.askPrice.value?.price)
        assertEquals("100", result.askPrice.value?.size)
        assertEquals("189.29", result.bidPrice.value?.price)
        assertEquals("100", result.bidPrice.value?.size)
        assertEquals("20780363", result.volume.value)

        mockQuoteJson.timestampOfLastTrade = 1578075280
        val modifiedTimeStampResult = sut.map(MobileQuoteMapper().map(mockQuoteJson))
        assertEquals("As of 01:14 PM EST 01/03/2020", modifiedTimeStampResult.asOfDateTime.value)
    }

    @Test
    fun `maps QuotesDto Json to Map of QuoteWidgetItem`() {
        sut = MobileQuoteWidgetItemMapper(mapperResources)
        val quotes = getListOfQuotes()?.map {
            MobileQuoteMapper().map(it)
        }

        val quoteMap = quotes?.let { sut.map(it) }
        assertNotNull(quoteMap)

        quoteMap?.apply {
            assertEquals(5, entries.size)
            assertTrue(this.containsKey("SPXW--190814C02690000"))
            assertTrue(this.containsKey("SPX"))
            val spxOption = this["SPXW--190814C02690000"] as QuoteWidgetItem
            assertEquals("319.00", spxOption.bidPrice.value?.price)
            assertEquals("40", spxOption.bidPrice.value?.size)
            assertEquals("324.10", spxOption.askPrice.value?.price)
            assertEquals("40", spxOption.askPrice.value?.size)
            assertEquals("3013.18", spxOption.lastPrice.value?.price)
            assertEquals("", spxOption.lastPrice.value?.size)
            assertEquals("-7.790000", spxOption.change.value?.change)
            assertEquals("-0.26", spxOption.change.value?.changePercent)
            assertEquals(RED, spxOption.changeColor.value)

            assertEquals("SPX - Cboe S&p 500 Index S&p 500", spxOption.title)
            val spxEquity = this["SPX"] as QuoteWidgetItem
            assertEquals("0.00", spxEquity.bidPrice.value?.price)
            assertEquals("0", spxEquity.bidPrice.value?.size)
            assertEquals("0.00", spxEquity.askPrice.value?.price)
            assertEquals("0", spxEquity.askPrice.value?.size)
            assertEquals("3013.18", spxEquity.lastPrice.value?.price)
            assertEquals("", spxEquity.lastPrice.value?.size)
            assertEquals("-7.790000", spxEquity.change.value?.change)
            assertEquals("-0.26", spxEquity.change.value?.changePercent)
            assertEquals("SPX - Cboe S&p 500 Index S&p 500", spxEquity.title)
            assertEquals(RED, spxEquity.changeColor.value)

            val googOption = this["GOOGL-190726C01162500"] as QuoteWidgetItem
            assertEquals("0.00", googOption.bidPrice.value?.price)
            assertEquals("0", googOption.bidPrice.value?.size)
            assertEquals("0.00", googOption.askPrice.value?.price)
            assertEquals("0", googOption.askPrice.value?.size)
            assertEquals("1228.00", googOption.lastPrice.value?.price)
            assertEquals("700", googOption.lastPrice.value?.size)
            assertEquals("-13.840000", googOption.change.value?.change)
            assertEquals("-1.11", googOption.change.value?.changePercent)
            assertEquals("GOOGL - Alphabet Inc Cap Stk Cl A", googOption.title)
            assertEquals(RED, googOption.changeColor.value)

            val googEquity = this["GOOGL"] as QuoteWidgetItem
            assertEquals("1228.15", googEquity.bidPrice.value?.price)
            assertEquals("100", googEquity.bidPrice.value?.size)
            assertEquals("1231.81", googEquity.askPrice.value?.price)
            assertEquals("700", googEquity.askPrice.value?.size)
            assertEquals("1228.00", googEquity.lastPrice.value?.price)
            assertEquals("700", googEquity.lastPrice.value?.size)
            assertEquals("-13.840000", googEquity.change.value?.change)
            assertEquals("-1.11", googEquity.change.value?.changePercent)
            assertEquals("GOOGL - Alphabet Inc Cap Stk Cl A", googEquity.title)
            assertEquals(RED, googEquity.changeColor.value)
        }
    }

    private fun getMockQuotes(): QuoteDto? =
        MobileQuoteWidgetItemMapper::class.getObjectFromJson<QuoteStockDto>("quotes.json")

    private fun getListOfQuotes(): List<QuoteDto>? =
        MobileQuoteWidgetItemMapper::class.getObjectFromJson<List<QuoteStockDto>>("multi_quote_list.json", List::class.java, QuoteStockDto::class.java)
}
