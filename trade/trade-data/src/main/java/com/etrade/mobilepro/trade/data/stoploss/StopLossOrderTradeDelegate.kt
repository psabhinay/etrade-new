package com.etrade.mobilepro.trade.data.stoploss

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.trade.api.AdvancedTradeType.StopLoss
import com.etrade.mobilepro.trade.api.OrderPlaceResponse
import com.etrade.mobilepro.trade.api.OrderPreviewResponse
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderPreviewResponse
import com.etrade.mobilepro.trade.api.advanced.TrailingStopOrderResponse
import com.etrade.mobilepro.trade.data.TradeDelegate
import com.etrade.mobilepro.trade.data.TradeOrderArguments
import com.etrade.mobilepro.trade.data.advanced.AdvancedTradingApiClient
import com.etrade.mobilepro.trade.data.advanced.AdvancedTradingApiClient.Companion.PATH_OTA
import com.etrade.mobilepro.trade.data.advanced.ORDER_COUNT_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_TRIGGER_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.POSITION_EFFECT_KEY
import com.etrade.mobilepro.trade.data.advanced.PREVIEW_ID_KEY
import com.etrade.mobilepro.trade.data.advanced.PRIMARY_SECURITY_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.ParametersModifierWithInputId
import com.etrade.mobilepro.trade.data.advanced.SIDE_KEY
import com.etrade.mobilepro.trade.data.advanced.VALUE_SECURITY_TYPE_OPTION
import com.etrade.mobilepro.trade.data.advanced.addSymbol
import com.etrade.mobilepro.trade.data.advanced.copyTrailingStopParamsFromResponse
import com.etrade.mobilepro.trade.data.advanced.copyTrailingStopPriceToOffsetValue
import com.etrade.mobilepro.trade.data.advanced.getAdvancedOrderPlaceTradeParams
import com.etrade.mobilepro.trade.data.advanced.getAdvancedOrderPreviewTradeParams
import com.etrade.mobilepro.trade.data.advanced.place
import com.etrade.mobilepro.trade.data.advanced.preview
import com.etrade.mobilepro.trade.data.advanced.removeRedundantAdvancedOrderParams
import com.etrade.mobilepro.trade.data.fixLimitStopPriceParam
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.getTransactionType
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractAction
import com.etrade.mobilepro.trade.form.api.input.extractPriceType
import com.etrade.mobilepro.trade.form.api.input.shared.WithStopLossInputId
import com.etrade.mobilepro.trade.form.api.input.shared.WithTrailingStopInputId
import com.etrade.mobilepro.util.filterNotNullValues

private const val BUY_SIDE = "2"
private const val SELL_SIDE = "3"

internal class StopLossOrderTradeDelegate<T>(
    private val apiClient: AdvancedTradingApiClient,
    private val inputId: T
) : TradeDelegate where T : TradeFormInputId,
                        T : WithStopLossInputId,
                        T : WithAdvancedTradeFormInputId,
                        T : WithSymbolTradeFormInputId,
                        T : WithPriceTradeFormInputId,
                        T : WithTrailingStopInputId {

    override suspend fun previewOrder(map: InputValuesSnapshot, marketDestination: MarketDestination): ETResult<OrderPreviewResponse> {
        return apiClient.preview(PATH_OTA, preparePreviewParameters(map), StopLoss)
    }

    override suspend fun placeOrder(arguments: TradeOrderArguments, order: TradeOrder): ETResult<OrderPlaceResponse> {
        val response = order.response
        require(response is AdvancedOrderPreviewResponse)
        return apiClient.place(PATH_OTA, preparePlaceParameters(arguments.params, response), StopLoss)
    }

    private fun preparePreviewParameters(snapshot: InputValuesSnapshot): MutableMap<String, String> {
        return prepareParameters(snapshot).apply {
            putAll(
                getAdvancedOrderPreviewTradeParams(
                    formTarget = "${PATH_OTA}createentry",
                    previewX = "65"
                )
            )
        }
    }

    private fun preparePlaceParameters(snapshot: Map<String, Any?>, response: AdvancedOrderPreviewResponse): Map<String, String> {

        val result = prepareParameters(snapshot) { orderSnapshot, index, inputId ->
            require(inputId is WithPriceTradeFormInputId)
            val isOption = orderSnapshot[inputId.securityType] == VALUE_SECURITY_TYPE_OPTION
            orderSnapshot.copyTrailingStopParamsFromResponse(response as TrailingStopOrderResponse, index, inputId, index.toString())
            orderSnapshot["$ORDER_TYPE_KEY$index"] = response.orders[index].orderType.typeCode
            orderSnapshot["$ORDER_TRIGGER_KEY$index"] = response.orders[index].orderTrigger
            orderSnapshot.remove(inputId.priceType)
            (inputId as WithActionTradeFormInputId).extractAction(orderSnapshot)
                ?.also {
                    orderSnapshot.remove(inputId.action)
                    orderSnapshot.remove(inputId.priceType)
                }
                ?.let {
                    /**
                     * The values for side and position effect is different for Stop Loss Orders than the regular Order Params,
                     * possible cause :  Usage of legacy service and probably followed a different convention.
                     */
                    orderSnapshot["$SIDE_KEY$index"] = it.getStopLossSide()
                    orderSnapshot["$POSITION_EFFECT_KEY$index"] = it.getPositionEffect(isOption)
                }
        }
        result.putAll(
            getAdvancedOrderPlaceTradeParams(
                formTarget = "${PATH_OTA}preview",
                tradeX = "26"
            )
        )
        response.previewId?.let { result[PREVIEW_ID_KEY] = it }
        return result.toMap()
    }

    private fun prepareParameters(
        snapshot: InputValuesSnapshot,
        update: ParametersModifierWithInputId = { _, _, _ -> }
    ): MutableMap<String, String> {
        val result = mutableMapOf<String, String>()
        result[inputId.accountId] = snapshot[inputId.accountId] as String
        result.addOrder(snapshot, update)
        return result
    }

    private fun MutableMap<String, String>.addOrder(snapshot: InputValuesSnapshot, update: ParametersModifierWithInputId) {

        var orderCount = 0

        val orderSnapshot = snapshot.toMutableMap().apply {
            removeRedundantAdvancedOrderParams(inputId)
            remove(inputId.securityType)
            if (inputId is OptionTradeFormInputId) {
                remove(inputId.lookupSymbol)
            }

            fixLimitStopPriceParam(inputId, inputId.extractPriceType(this))
            fixLimitStopPriceParam(inputId.stopLossInputId, inputId.stopLossInputId.extractPriceType(this))
            inputId.copyTrailingStopPriceToOffsetValue(this)
            inputId.stopLossInputId.copyTrailingStopPriceToOffsetValue(this)

            orderCount += addSymbol(inputId, PRIMARY_SECURITY_TYPE_KEY, orderCount, update)
            orderCount += addSymbol(inputId.stopLossInputId, inputId.stopLossInputId.securityType, orderCount, update)
        }

        putAll(
            orderSnapshot
                .mapValues { it.value as? String }
                .filterNotNullValues()
        )

        this[ORDER_COUNT_KEY] = orderCount.toString()
    }
}

private fun TradeAction.getStopLossSide(): String = when (this) {
    TradeAction.BUY, TradeAction.BUY_TO_COVER -> BUY_SIDE
    TradeAction.SELL, TradeAction.SELL_SHORT -> SELL_SIDE
    else -> SELL_SIDE
}

private fun TradeAction.getPositionEffect(isOption: Boolean): String {
    return getTransactionType(isOption).positionEffect
}
