package com.etrade.mobilepro.chat.presentation.message.adapter

import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.presentation.message.delegate.ChatAgentTransferQueueAdapterDelegate
import com.etrade.mobilepro.chat.presentation.message.delegate.ChatAgentTypingAdapterDelegate
import com.etrade.mobilepro.chat.presentation.message.delegate.ChatMessageAgentAdapterDelegate
import com.etrade.mobilepro.chat.presentation.message.delegate.ChatMessageLoadingAdapterDelegate
import com.etrade.mobilepro.chat.presentation.message.delegate.ChatMessageUserAdapterDelegate
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class ChatMessagesAdapter : ListDelegationAdapter<MutableList<ChatMessage>>() {

    init {
        delegatesManager.apply {
            addDelegate(ChatMessageUserAdapterDelegate())
            addDelegate(ChatMessageAgentAdapterDelegate())
            addDelegate(ChatMessageLoadingAdapterDelegate())
            addDelegate(ChatAgentTypingAdapterDelegate())
            addDelegate(ChatAgentTransferQueueAdapterDelegate())
        }

        items = mutableListOf()
    }

    fun submitItems(messages: List<ChatMessage>) {
        dispatchUpdates(items, messages)
        items = messages.toMutableList()
    }
}
