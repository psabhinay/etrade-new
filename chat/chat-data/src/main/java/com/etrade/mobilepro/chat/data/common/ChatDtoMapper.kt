package com.etrade.mobilepro.chat.data.common

import com.etrade.mobilepro.chat.api.ChatGeolocationModel
import com.etrade.mobilepro.chat.api.ChatMetadataModel
import com.etrade.mobilepro.chat.api.ChatParamsRequestModel
import com.etrade.mobilepro.chat.api.message.models.ChatRequestModel
import com.etrade.mobilepro.chat.data.messages.dto.ChatDefaultRequestDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatGetMessagesRequestDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatMetadataDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatParamsDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatParamsRequestDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatPostMessageRequestDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatTerminateRequestDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatGeoLocationDto

fun ChatRequestModel.GetMessages.toRequestBody(ackMessage: Int?) =
    ChatGetMessagesRequestDto(getDefaultRequestBody(sessionID = sessionId, metadata = metadata, ackMessage = ackMessage))

fun ChatRequestModel.PostMessage.toRequestBody() =
    ChatPostMessageRequestDto(getDefaultRequestBody(message = message, sessionID = sessionID, metadata = metadata))

fun ChatRequestModel.TerminateChat.toRequestBody() =
    ChatTerminateRequestDto(getDefaultRequestBody(sessionID = sessionID, metadata = metadata))

fun ChatParamsRequestModel.toRequestBody(ackMessage: Int? = null) = with(value) {
    ChatParamsRequestDto(
        ChatParamsDto(
            metadata = getChatMetadataDto(metadata, ackMessage),
            productID = productID,
            cid = cid,
            lastName = lastName,
            isStockPlan = isStockPlan,
            isOH = isOH,
            isBusinessHrs = isBusinessHrs,
            firstName = firstName,
            contactID = contactID,
            chatSource = chatSource,
            chatInfo = chatInfo,
            acctInstName = acctInstName,
            accountNo = accountNo,
            accountHolderTier = accountHolderTier,
            chatGeoLoc = getChatGeolocationDto(chatGeoLoc)
        )
    )
}

private fun getDefaultRequestBody(message: String? = null, sessionID: String, metadata: ChatMetadataModel? = null, ackMessage: Int? = null) =
    ChatDefaultRequestDto(
        message = message,
        sessionID = sessionID,
        metadata = getChatMetadataDto(metadata, ackMessage)
    )

private fun getChatMetadataDto(metadata: ChatMetadataModel? = null, ackMessage: Int?) = ChatMetadataDto(
    affinity = metadata?.affinity,
    buttonId = metadata?.buttonId,
    buttonName = metadata?.buttonName,
    sessionKey = metadata?.sessionKey,
    messagesAck = ackMessage
)

fun getChatGeolocationDto(chatGeoLoc: ChatGeolocationModel?) =
    chatGeoLoc?.let {
        ChatGeoLocationDto(
            city = it.city,
            cityCode = it.cityCode,
            connectionSpeed = it.connectionSpeed,
            continentCode = it.continentCode,
            countryCode = it.countryCode,
            country = it.country,
            domain = it.domain,
            ipAddress = it.ipAddress,
            isp = it.isp,
            latitude = it.latitude,
            longitude = it.longitude,
            metroCode = it.metroCode,
            postalCode = it.postalCode,
            proxyType = it.proxyType,
            regionCode = it.regionCode,
            region = it.region,
            twoLetterCountry = it.twoLetterCountry
        )
    }
