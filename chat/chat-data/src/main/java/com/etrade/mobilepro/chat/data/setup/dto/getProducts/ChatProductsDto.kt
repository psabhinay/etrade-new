package com.etrade.mobilepro.chat.data.setup.dto.getProducts

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ChatProductsDto(
    @Json(name = "data")
    var data: ChatProductsDataDto? = null
)

@JsonClass(generateAdapter = true)
class ChatProductsDataDto(
    @Json(name = "Messages")
    val messages: List<ChatProductMessagesDto> = emptyList()
)

@JsonClass(generateAdapter = true)
class ChatProductMessagesDto(
    @Json(name = "id") val id: String,
    @Json(name = "lookupName") val lookupName: String? = null
)
