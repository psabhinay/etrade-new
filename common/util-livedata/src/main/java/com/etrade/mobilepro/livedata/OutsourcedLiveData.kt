package com.etrade.mobilepro.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * This is a [LiveData] object that doesn't emit the values by itself, but uses external `LiveData` [source] as a source for the values allowing a client to
 * switch sources seamlessly.
 *
 * @param A The type of values that [source] emits.
 * @param B The type of values this live data emits.
 * @param accept Accepts a value that [source] emits, converting it to [A] type and assigning it to this live data if needed.
 *
 * @see sourceFor
 */
class OutsourcedLiveData<A, B>(private val accept: (A, MutableLiveData<B>) -> Unit) : MediatorLiveData<B>() {

    /**
     * The source of values. When a new source is attached to this live data, the old one is removed, meaning that there can only be only one source of values.
     */
    var source: LiveData<A>? = null
        set(fieldValue) {
            if (field == fieldValue) return
            field?.let { removeSource(it) }
            field = fieldValue?.also { source ->
                addSource(source) {
                    accept(it, this)
                }
            }
        }
}

/**
 * Property delegate that creates [LiveData] property for [OutsourcedLiveData.source]. When the new value is set to this property, it sets the new source to
 * [consumer].
 *
 * @param consumer The consumer of a source for [LiveData] created by this property.
 * @param property The actual source of [LiveData] that a value of this property holds.
 *
 * @see OutsourcedLiveData
 */
fun <T, A, B> sourceFor(consumer: OutsourcedLiveData<A, B>, property: T.() -> LiveData<A>): ReadWriteProperty<Any, T?> {
    return LiveDataSourceProperty(consumer, property)
}

private class LiveDataSourceProperty<T, A, B>(
    private val consumer: OutsourcedLiveData<A, B>,
    private val property: T.() -> LiveData<A>
) : ReadWriteProperty<Any, T?> {

    private var field: T? = null

    override fun getValue(thisRef: Any, property: KProperty<*>): T? = field

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T?) {
        if (field == value) { return }
        consumer.source = value?.property()
        field = value
    }
}
