package com.etrade.mobilepro.checkdeposit.usecases.api

import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount
import org.hamcrest.CoreMatchers.endsWith
import org.hamcrest.CoreMatchers.startsWith
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.math.BigInteger

const val DEFAULT_ACCOUNT_ID = "ABCDEFGH"
const val DEFAULT_ACCOUNT_CATEGORY = "CAT"
const val DEFAULT_NICKNAME = "nick"
val DEFAULT_INST_NO: BigInteger = BigInteger.ZERO
const val DEFAULT_ACCOUNT_TYPE = "type"

internal class AccountTest {

    @Test
    fun `should start with & obscure all but last 4 of account number`() {
        assertThat(sut().displayString(), startsWith("EFGH "))
        assertThat(sut(accountId = "AB").displayString(), startsWith("AB "))
    }

    @Test
    fun `should display nickname over account type if present`() {
        with(sut()) {
            assertThat(displayString(), endsWith(" $DEFAULT_NICKNAME"))
        }
    }

    @Test
    fun `determines if IRA account type`() {
        with(sut(accountCategory = "IRA")) {
            assertTrue(isIraAccount)
        }
        with(sut(accountCategory = "ira")) {
            assertTrue(isIraAccount)
        }
        with(sut(accountCategory = "not ira")) {
            assertFalse(isIraAccount)
        }
    }

    @ParameterizedTest
    @MethodSource("nickNameValues")
    fun `should display account type if nickname is not set`(nickname: String?) {
        with(sut(nickname = nickname)) {
            assertThat(displayString(), endsWith(" $accountType"))
        }
    }

    private fun sut(
        accountCategory: String = DEFAULT_ACCOUNT_CATEGORY,
        accountId: String = DEFAULT_ACCOUNT_ID,
        nickname: String? = DEFAULT_NICKNAME,
        instNo: BigInteger = DEFAULT_INST_NO,
        accountType: String = DEFAULT_ACCOUNT_TYPE
    ): FundsFlowAccount = object : FundsFlowAccount {
        override val accountCategory: String = accountCategory
        override val accountId: String = accountId
        override val nickname: String? = nickname
        override val instNo: BigInteger = instNo
        override val accountType: String = accountType
    }

    @Suppress("unused") // Used in JUnit's @MethodSource
    companion object {
        @JvmStatic
        fun nickNameValues() = listOf(null, "", " ")
    }
}
