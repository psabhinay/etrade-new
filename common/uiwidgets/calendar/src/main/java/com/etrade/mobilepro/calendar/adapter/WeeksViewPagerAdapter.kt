package com.etrade.mobilepro.calendar.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.etrade.mobilepro.calendar.CalendarMode
import com.etrade.mobilepro.calendar.databinding.ViewDaysBinding
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate

internal const val PAGER_ITEMS_COUNT = 50 // ~12 mounts backward and forward from now
private const val ADAPTER_SELECTION_DELAY_MS = 100L

class WeeksViewPagerAdapter : PagerAdapter() {

    var currentDate: LocalDate = LocalDate.now()
        set(value) {
            field = value

            notifyDataSetChanged()
        }

    var selectedDate: LocalDate? = null

    var dayFormatter: (LocalDate) -> String = { date -> date.dayOfMonth.toString() }
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var disabledDaysPredicate: (LocalDate) -> Boolean = { false } // show days as enabled by default
        set(value) {
            field = value

            notifyDataSetChanged()
        }

    var clickableDaysPredicate: (LocalDate) -> Boolean =
        { true } // // all days clickable by default and could be selected
        set(value) {
                field = value

                notifyDataSetChanged()
            }

    var mode: CalendarMode = CalendarMode.TWO_WEEKS
        set(value) {
            field = value

            notifyDataSetChanged()
        }

    var onClickListener: ((LocalDate) -> Unit)? = null

    var weekDayHeight: Int = 0
        set(value) {
            field = value

            notifyDataSetChanged()
        }

    private val events: HashSet<LocalDate> = hashSetOf()

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding =
            ViewDaysBinding.inflate(LayoutInflater.from(container.context), container, false)

        initDaysView(
            DaysAdapter(
                mode = mode,
                position = position,
                currentDate = currentDate,
                selectedDate = selectedDate,
                disabledDaysPredicate = disabledDaysPredicate,
                clickableDaysPredicate = clickableDaysPredicate,
                events = events,
                onDateSelectListener = createDateSelectListener(binding),
                dayFormatter = dayFormatter,
                weekDayHeight = weekDayHeight
            ),
            binding
        )

        container.addView(binding.root)

        return binding.root
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean = view == obj

    override fun getCount(): Int = PAGER_ITEMS_COUNT

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun getItemPosition(obj: Any): Int = POSITION_NONE

    private fun initDaysView(daysAdapter: DaysAdapter, binding: ViewDaysBinding) {
        binding.daysRv.apply {
            layoutManager =
                GridLayoutManager(context, DayOfWeek.values().size, RecyclerView.VERTICAL, false)
            adapter = daysAdapter
        }
    }

    fun updateCalendarEvents(items: Set<LocalDate>) {
        events.clear()
        events.addAll(items)

        notifyDataSetChanged()
    }

    fun handleSelectedDate(date: LocalDate) {
        if (clickableDaysPredicate(date)) {
            selectedDate = date
            onClickListener?.invoke(date)
        }
    }

    private fun createDateSelectListener(binding: ViewDaysBinding): OnDateSelectListener =
        object : OnDateSelectListener {
            override fun onDateSelected(view: View, position: Int, item: LocalDate) {
                handleSelectedDate(view.tag as LocalDate)
                selectedDate = item
                val recyclerView = binding.daysRv
                (recyclerView.adapter as? DaysAdapter)?.apply {
                    this.selectedDate = item
                    // notifying all non-selected days in adapter to maintain focus on selected (e.g. accessibility focus purpose)
                    notifyItemRangeChanged(0, position)
                    notifyItemRangeChanged(position + 1, itemCount)
                    // delay to avoid previous and new day selection be visible at the same time
                    recyclerView.postOnAnimationDelayed(
                        {
                            processDaySelection(view, item)
                        },
                        ADAPTER_SELECTION_DELAY_MS
                    )
                }
            }
        }
}
