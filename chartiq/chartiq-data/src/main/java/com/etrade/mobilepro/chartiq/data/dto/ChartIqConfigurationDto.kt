package com.etrade.mobilepro.chartiq.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class ChartIqConfigurationDto(

    @Json(name = "symbolInfo")
    val symbolInfo: SymbolInfoDto,

    @Json(name = "accountsList")
    val accounts: List<AccountDto>,

    @Json(name = "deviceType")
    val deviceType: String,

    @Json(name = "defaultAccount")
    val defaultAccount: AccountDto?,

    @Json(name = "isTabletDevice")
    val isTablet: Boolean,

    @Json(name = "environment")
    val environmentName: String,

    @Json(name = "envBaseUrl")
    val environmentUrl: String,

    @Json(name = "chartBaseUrl")
    val chartUrl: String,

    @Json(name = "syncServerData")
    val syncServerData: Boolean,

    @Json(name = "smSession")
    val sessionCookie: String,

    @Json(name = "isLoggedIn")
    val isLoggedIn: Boolean,

    @Json(name = "stk1")
    val csrfToken: String? = null,

    @Json(name = "loggedInFromCharts")
    val restoreState: Boolean = false,

    @Json(name = "showDrawingAlert")
    val showDrawingNotImplementedAlert: Boolean = false
)
