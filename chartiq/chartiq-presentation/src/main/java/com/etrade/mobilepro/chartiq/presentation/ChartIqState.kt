package com.etrade.mobilepro.chartiq.presentation

internal sealed class ChartIqState {
    object None : ChartIqState()
    object Reload : ChartIqState()
    data class Initializing(val sessionCookie: String) : ChartIqState()
    object Ready : ChartIqState()
    data class Error(val retryAction: () -> Unit) : ChartIqState()
}
