package com.etrade.mobilepro.optionschains.data.dto

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.util.json.BigDecimalAdapter
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test

class OptionsChainMapperTest {

    @Test
    fun `check mapper`() {
        val optionsChain = getResponse().data?.optionChainsResponse?.toOptionsChain(false)!!
        assertFalse(optionsChain.expirationDates.isEmpty())
    }

    private fun getResponse(): ServerResponseDto<OptionsChainDto> {
        return OptionsChainDto::class.getObjectFromJson(
            resourcePath = "option-chains-with-quotes.json",
            cls1 = ServerResponseDto::class.java,
            cls2 = OptionsChainDto::class.java,
            adapters = listOf(BigDecimalAdapter)
        )
    }
}
