package com.etrade.mobilepro.news.presentation.watchlist

import androidx.navigation.NavController
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.compose.components.LazyListStateRetainer
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.api.repo.NewsRepo
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.news.presentation.base.BaseNewsViewModel
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentLoader
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsFontAdjustHandler
import com.etrade.mobilepro.util.android.recyclerviewutil.SelectionTracker
import com.etrade.mobilepro.watchlistapi.ALL_WATCHLISTS_ID
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.Watchlist
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class WatchlistNewsViewModel @Inject constructor(
    newsRepo: NewsRepo,
    statusRepo: ReadStatusRepo,
    contentLoader: TextNewsContentLoader,
    fontAdjustHandler: TextNewsFontAdjustHandler,
    selectionTracker: SelectionTracker,
    val newsRouter: NewsRouter
) : BaseNewsViewModel(newsRepo, statusRepo, contentLoader, fontAdjustHandler, selectionTracker) {
    val newsListStateRetainer = LazyListStateRetainer()

    private var previousWatchlist: Watchlist? = null

    override fun getNewsType(): NewsType = NewsType.WATCHLIST

    @ExperimentalCoroutinesApi
    fun refreshWithCurrentParams() {
        refreshWithParams(NewsRequest.Watchlist(resolveWatchlistId(previousWatchlist)))
    }

    @ExperimentalCoroutinesApi
    fun applyFilter(watchlist: Watchlist?, forced: Boolean = false) {
        if (previousWatchlist == watchlist && !forced) {
            return
        }
        previousWatchlist = watchlist

        refreshWithParams(NewsRequest.Watchlist(resolveWatchlistId(watchlist)))
    }

    fun onFilterClickedUnAuth(navController: NavController) {
        newsRouter.navigateWatchlistTabWithLogin(navController, tabIndex = 1)
    }

    private fun resolveWatchlistId(watchlist: Watchlist?): String = watchlist?.uuid ?: if (watchlist?.isAllWatchlistsItem == true) {
        ALL_WATCHLISTS_ID
    } else {
        DEVICE_WATCHLIST_ID
    }
}
