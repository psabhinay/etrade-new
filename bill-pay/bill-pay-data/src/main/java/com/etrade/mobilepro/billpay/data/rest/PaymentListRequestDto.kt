package com.etrade.mobilepro.billpay.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PaymentListRequestDto(
    @Json(name = "value")
    val value: PaymentListRequestValueDto
)

@JsonClass(generateAdapter = true)
class PaymentListRequestValueDto(
    @Json(name = "sessionId")
    val sessionId: String = "",

    @Json(name = "status")
    val status: String = "",

    @Json(name = "numOfDays")
    val numOfDays: Int = 0,

    @Json(name = "startDate")
    val startDate: String = ""
)
