package com.etrade.mobilepro.dao

interface CachedDaoProvider : AccountDaoProvider, InstrumentDaoProvider, PositionsDaoProvider {

    // Initialize the provider or db instance.
    fun open()

    // Close the resources or db instance.
    fun close()
}
