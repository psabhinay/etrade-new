package com.etrade.mobilepro.chat.data.messages.service

import com.etrade.mobilepro.chat.data.messages.dto.ChatForMobileResponseDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatRetrieveMessageDto
import com.etrade.mobilepro.chat.data.setup.service.CHAT_PLATFORM_HEADER
import com.etrade.mobilepro.util.json.JsonMoshi
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

private const val CHAT_API_PATH = "/app/chat/v2/"

interface ChatMessagesApiService {

    @JsonMoshi
    @POST(CHAT_API_PATH + "getChatForMobile.json")
    @Headers(CHAT_PLATFORM_HEADER)
    suspend fun getChatForMobile(@Body request: RequestBody): ChatForMobileResponseDto

    @JsonMoshi
    @POST(CHAT_API_PATH + "postChatMessage.json")
    @Headers(CHAT_PLATFORM_HEADER)
    suspend fun postChat(@Body request: RequestBody): ChatForMobileResponseDto

    @JsonMoshi
    @POST(CHAT_API_PATH + "terminateChat.json")
    @Headers(CHAT_PLATFORM_HEADER)
    suspend fun terminateChat(@Body request: RequestBody): ChatForMobileResponseDto

    @JsonMoshi
    @POST(CHAT_API_PATH + "retrieveChatMessages.json")
    @Headers(CHAT_PLATFORM_HEADER)
    suspend fun retrieveMessages(@Body request: RequestBody): ChatRetrieveMessageDto
}
