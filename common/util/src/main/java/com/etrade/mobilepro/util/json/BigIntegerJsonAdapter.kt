package com.etrade.mobilepro.util.json

import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.ToJson
import java.math.BigInteger

class BigIntegerJsonAdapter {

    @FromJson
    fun fromJson(json: String) = BigInteger(json)

    @ToJson
    fun toJson(writer: JsonWriter, value: BigInteger?) {
        writer.value(value)
    }
}
