package com.etrade.mobilepro.trade.optionlegselect.adapter.viewholder

import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.common.customview.SwipeRevealLayout
import com.etrade.mobilepro.trade.optionlegselect.adapter.BaseOptionLegItemAdapter
import com.etrade.mobilepro.trade.optionlegselect.databinding.TradeItemOptionLegDeletableBinding

internal typealias OnOptionLegItemDeleteListener = (index: Int) -> Unit
internal typealias OnOptionLegItemDeleteInteractionListener = OnOptionLegItemDeleteListener
internal typealias OnOptionLegItemDeletePendingListener = (index: Int, deletePending: Boolean) -> Unit

internal class DeletableOptionLegItemViewHolder(
    binding: TradeItemOptionLegDeletableBinding,
    lifecycleOwner: LifecycleOwner,
    getItemAtPosition: (Int) -> BaseOptionLegItemAdapter.Item?,
    quantityChangedListener: OnOptionLegItemQuantityChangedListener,
    private val itemDeleteListener: OnOptionLegItemDeleteListener,
    private val itemDeleteInteractionListener: OnOptionLegItemDeleteInteractionListener,
    private val itemDeletePendingListener: OnOptionLegItemDeletePendingListener
) : OptionLegItemViewHolder(
    binding.optionLeg,
    binding.root,
    lifecycleOwner,
    getItemAtPosition,
    quantityChangedListener
) {

    private val deleteButton: TextView = binding.optionLegDeleteButton
    private val optionLegSwipeRevealLayout: SwipeRevealLayout = binding.root

    private val isDeletePendingObserver: Observer<Boolean> = Observer {
        optionLegSwipeRevealLayout.toggle(isDeletePending = it, animate = true)
    }

    private var previousItem: BaseOptionLegItemAdapter.Item? = null

    override fun bindTo(item: BaseOptionLegItemAdapter.Item, index: Int, isLastItem: Boolean) {
        super.bindTo(item, index, isLastItem)

        deleteButton.setOnClickListener {
            itemDeleteListener(index)
        }

        optionLegSwipeRevealLayout.bindTo(item, index)

        previousItem?.isDeletePending?.removeObserver(isDeletePendingObserver)
        previousItem =
            item.apply { isDeletePending.observe(lifecycleOwner, isDeletePendingObserver) }
    }

    private fun SwipeRevealLayout.bindTo(item: BaseOptionLegItemAdapter.Item, index: Int) {
        // this sets the initial state of the view
        toggle(isDeletePending = item.isDeletePending.value == true, animate = false)

        actionListener = { action ->
            when (action) {
                SwipeRevealLayout.Action.CLOSE -> itemDeletePendingListener(index, false)
                SwipeRevealLayout.Action.OPEN -> itemDeletePendingListener(index, true)
                SwipeRevealLayout.Action.START_DRAG -> itemDeleteInteractionListener(index)
            }
        }
    }

    private fun SwipeRevealLayout.toggle(isDeletePending: Boolean, animate: Boolean) {
        if (isDeletePending) {
            open(animation = animate)
        } else {
            close(animation = animate)
        }
    }
}
