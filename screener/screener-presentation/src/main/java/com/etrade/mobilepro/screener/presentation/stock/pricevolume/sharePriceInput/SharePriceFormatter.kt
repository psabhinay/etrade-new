package com.etrade.mobilepro.screener.presentation.stock.pricevolume.sharePriceInput

import com.etrade.mobilepro.inputvalueview.InputFieldManager

internal class SharePriceFormatter : InputFieldManager.Formatter {
    override fun formatRawContent(rawContent: String): String = rawContent
}
