package com.etrade.mobilepro.trade.presentation.confirmation

import android.content.res.Resources
import com.etrade.mobilepro.order.details.presentation.helper.conditionalOtoSectionHeaderResource
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.OrderRequest
import com.etrade.mobilepro.trade.api.OrderResponse
import com.etrade.mobilepro.trade.api.advanced.ContingentOrderResponse
import com.etrade.mobilepro.trade.api.option.ConfirmedOptionOrder
import com.etrade.mobilepro.trade.api.option.OptionOrder
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.util.OrderDisplayItem
import com.etrade.mobilepro.trade.presentation.util.adapters.OrderAdapter
import com.etrade.mobilepro.trade.presentation.util.getSectionItems
import javax.inject.Inject

class OrderResponseAdapter @Inject constructor(private val resources: Resources, private val orderAdapter: OrderAdapter) {

    fun createConfirmationItems(responseWithLegs: Pair<OrderResponse, OrderRequest?>): List<OrderConfirmationItem> {
        var index = 0
        val response = responseWithLegs.first
        return response.orders.flatMap { order ->
            val orderDisplayItems = createOrderItems(order, responseWithLegs.second)
            listOfNotNull(
                conditionalOtoSectionHeaderResource(index++)?.let {
                    OrderConfirmationItem.Divider(resources.getString(it))
                },
                orderDisplayItems
            ) + contingentOrderItems(response)
        }
    }

    private fun createOrderItems(order: Order, orderRequest: OrderRequest?): OrderConfirmationItem.Order {
        val confirmedOrder = (order as? ConfirmedOptionOrder)?.let {
            val updatedOrder: Order = object : ConfirmedOptionOrder, OptionOrder by it {
                override val confirmationOrderId: String? = it.confirmationOrderId
                override val placedDate: String? = it.placedDate
                override val orderRequest: OrderRequest? = orderRequest
            }
            updatedOrder
        } ?: order

        return OrderConfirmationItem.Order(
            orderAdapter.createOrderDisplayItem(confirmedOrder)
        )
    }

    private fun contingentOrderItems(response: OrderResponse): List<OrderConfirmationItem> {
        val details = (response as? ContingentOrderResponse)?.contingentOrderDetails?.firstOrNull()
        return details?.let {
            listOf(
                OrderConfirmationItem.Divider(resources.getString(R.string.orders_divider_contingent)),
                OrderConfirmationItem.Order(
                    OrderDisplayItem(
                        label = details.displaySymbol,
                        sections = details.getSectionItems(resources)
                    )
                )
            )
        }.orEmpty()
    }
}
