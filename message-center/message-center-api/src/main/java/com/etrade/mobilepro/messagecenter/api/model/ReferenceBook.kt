package com.etrade.mobilepro.messagecenter.api.model

import kotlinx.collections.immutable.ImmutableList

private const val EMPTY_VAL = "--"

data class ReferenceBook(
    val accounts: ImmutableList<Account>,
    val subjects: ImmutableList<Subject>
) {
    data class Account(
        val id: String,
        val name: String,
        val shortDescription: String,
        val acctNo: String,
        val instNo: String,
        val instName: String
    )

    data class Subject(
        val id: String,
        val desc: String,
        val isActive: Boolean,
        val topics: ImmutableList<Topic>
    )

    data class Topic(
        val id: String,
        val subjectId: String,
        val desc: String,
        val isActive: Boolean
    )

    fun getSubjectTopics(subject: Subject?): ImmutableList<Topic>? =
        subjects.find { it == subject }?.topics

    fun getTopicDescById(
        id: String,
        subjectId: String? = null,
        default: String = EMPTY_VAL
    ): String = getTopicById(id, subjectId)?.desc ?: default

    private fun getSubjectById(id: String): Subject? = subjects.find { it.id == id }

    private fun getTopicById(
        id: String,
        subjectId: String? = null
    ): Topic? =
        if (subjectId != null) {
            getSubjectById(subjectId)
                ?.topics
                ?.find { it.id == id }
        } else {
            subjects
                .flatMap { it.topics }
                .find { it.id == id }
        }
}
