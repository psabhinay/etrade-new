package com.etrade.mobilepro.marketscalendar.viewmodel

import org.threeten.bp.LocalDate

data class EventsSummary(val items: Set<LocalDate>)
