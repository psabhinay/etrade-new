package com.etrade.mobilepro.trade.presentation.form.delegate.shared

import android.content.res.Resources
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.balance.viewmodel.AccountBalanceViewModel
import com.etrade.mobilepro.dynamic.form.api.ExternalAction
import com.etrade.mobilepro.dynamic.form.api.FormBuilder
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.walkthrough.api.State

internal fun FormBuilder.accountSectionGroup(
    resources: Resources,
    inputId: TradeFormInputId,
    accountSectionGroup: AccountSectionGroup,
    init: GroupBuilder.() -> Unit = { /* intentionally blank */ }
) {
    verticalGroup {
        val accounts = accountSectionGroup.accounts
        account(resources, inputId, accounts, accountSectionGroup.defaultAccountId)
        accountDataPoints(resources, inputId, accounts, accountSectionGroup.balanceViewModel)
        init()
    }
}

@Suppress("LongParameterList")
internal fun GroupBuilder.securityTypeAndSymbol(
    resources: Resources,
    inputId: WithSymbolTradeFormInputId,
    securityType: SecurityType,
    params: SymbolInputParams,
    externalAction: ExternalAction,
    walkthroughState: LiveData<State>,
    initialSymbol: WithSymbolInfo? = null
) {
    horizontalGroup {
        securityType(resources, inputId, securityType, walkthroughState)
        symbol(resources, params, externalAction, initialSymbol)
    }
}

internal class AccountSectionGroup(
    val accounts: List<TradeAccount>,
    val defaultAccountId: String?,
    val balanceViewModel: AccountBalanceViewModel
)
