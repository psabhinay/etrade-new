package com.etrade.mobilepro.inboxmessages.binding

import android.view.View
import com.etrade.mobilepro.inboxmessages.InboxMessage

interface InboxMessageBinding {

    val layoutRes: Int

    fun bind(item: InboxMessage, view: View)
}
