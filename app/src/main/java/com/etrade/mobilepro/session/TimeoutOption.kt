package com.etrade.mobilepro.session

import android.content.res.Resources
import androidx.annotation.StringRes
import com.etrade.mobilepro.R
import java.util.concurrent.TimeUnit

enum class TimeoutOption(@StringRes val titleRes: Int, val timeoutInMinutes: Long, val timeoutInSeconds: Long) {
    THIRTY_MINUTES(R.string.TIME_OUT_OPTION_30_MINUTES, 30, TimeUnit.MINUTES.toSeconds(30)),
    ONE_HOUR(R.string.TIME_OUT_OPTION_1_HOUR, TimeUnit.HOURS.toMinutes(1), TimeUnit.HOURS.toSeconds(1)),
    TWO_HOURS(R.string.TIME_OUT_OPTION_2_HOURS, TimeUnit.HOURS.toMinutes(2), TimeUnit.HOURS.toSeconds(2)),
    THREE_HOURS(R.string.TIME_OUT_OPTION_3_HOURS, TimeUnit.HOURS.toMinutes(3), TimeUnit.HOURS.toSeconds(3)),
    FOUR_HOURS(R.string.TIME_OUT_OPTION_4_HOURS, TimeUnit.HOURS.toMinutes(4), TimeUnit.HOURS.toSeconds(4));

    companion object {
        fun titles(resources: Resources): List<String> = values()
            .map { resources.getString(it.titleRes) }

        fun getIndexByTime(timeout: Long) = values()
            .indexOfFirst {
                if (timeout < THIRTY_MINUTES.timeoutInSeconds) {
                    it.timeoutInMinutes == timeout
                } else {
                    it.timeoutInSeconds == timeout
                }
            }.takeIf { it != -1 }

        fun getIndexByTitle(resources: Resources, title: String) = titles(resources).indexOf(title)
    }
}
