package com.etrade.mobilepro.chart.cache

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Represents one line in android mp chart, used specifically for realm
 */
open class SymbolChartDataDb(
    @PrimaryKey
    var queryValue: String = "",
    var chartOHLCDataList: RealmList<ChartOHLCDataDb> = RealmList(),
    var yesterdaysClose: Double = 1.0,
    var cachedAt: Long = 0,
    var shouldCachedTimeInSeconds: Long = 0
) : RealmObject()

open class ChartOHLCDataDb(
    var date: String = "",
    var open: Double = 0.0,
    var high: Double = 0.0,
    var low: Double = 0.0,
    var close: Double = 0.0,
    var volume: Long = 0
) : RealmObject()
