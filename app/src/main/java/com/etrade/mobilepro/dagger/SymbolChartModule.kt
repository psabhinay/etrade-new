package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.chart.DefaultChartRepo
import com.etrade.mobilepro.chart.cache.CachedAggregationChartController
import com.etrade.mobilepro.chart.cache.CachedAggregationChartDataSourceDelegate
import com.etrade.mobilepro.chart.cache.CachedChartController
import com.etrade.mobilepro.chart.cache.CachedChartDataSourceDelegate
import com.etrade.mobilepro.chartengine.ChartRepo
import com.etrade.mobilepro.chartengine.ChartRequest
import com.etrade.mobilepro.chartengine.SymbolChartData
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class SymbolChartModule {

    @Binds
    @Singleton
    abstract fun bindChartRepo(chartRepo: DefaultChartRepo): ChartRepo

    @Binds
    @Singleton
    abstract fun bindSymbolChartCacheController(controller: CachedChartController): SimpleCachedResource.Controller<SymbolChartData>

    @Binds
    @Singleton
    abstract fun bindSymbolChartDataSourceDelegate(
        delegate: CachedChartDataSourceDelegate
    ): SimpleCachedResource.DataSourceDelegate<SymbolChartData, ChartRequest>

    @Binds
    @Singleton
    abstract fun bindCachedAggregationChartController(
        controller: CachedAggregationChartController
    ): SimpleCachedResource.Controller<List<SymbolChartData>>

    @Binds
    @Singleton
    abstract fun bindCachedAggregationChartDataSourceDelegate(
        delegate: CachedAggregationChartDataSourceDelegate
    ): SimpleCachedResource.DataSourceDelegate<List<SymbolChartData>, List<ChartRequest>>
}
