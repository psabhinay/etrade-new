package com.etrade.mobilepro.backends.af

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "Fault", strict = false)
data class FaultDto constructor(
    @field:ElementList(entry = "Error", inline = true, required = false, type = ErrorDto::class)
    var errors: List<ErrorDto>? = null
)
