package com.etrade.mobilepro.iraaccount.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class IraContributionInfoRequestDto(
    @Json(name = "value")
    val value: Value
) {
    @JsonClass(generateAdapter = true)
    class Value(
        @Json(name = "fromAcctNum")
        val fromAcctNum: String,
        @Json(name = "fromInstNum")
        val fromInstNum: String,
        @Json(name = "toAcctNum")
        val toAcctNum: String,
        @Json(name = "toAcctType")
        val toAcctType: String,
        @Json(name = "toInstNum")
        val toInstNum: String,
        @Json(name = "amount")
        val amount: String,
        @Json(name = "enteredDate")
        val enteredDate: String
    )
}
