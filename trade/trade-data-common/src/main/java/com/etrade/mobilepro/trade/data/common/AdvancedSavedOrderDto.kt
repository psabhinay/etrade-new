package com.etrade.mobilepro.trade.data.common

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
open class AdvancedSavedOrderDto(
    @field:Json(name = "priceType")
    var priceType: String? = null,

    @field:Json(name = "origSysCodeReq")
    var origSysCodeReq: String = "57",

    @field:Json(name = "orderType")
    var orderType: String? = null,

    @field:Json(name = "limitPrice")
    var limitPrice: String? = null,

    @field:Json(name = "stopPrice")
    var stopPrice: String? = null,

    @field:Json(name = "orderTerm")
    var orderTerm: String? = null,

    @field:Json(name = "allOrNone")
    var allOrNone: String = "0",

    @field:Json(name = "overnightIndicatorFlag")
    var overnightIndicatorFlag: String = "0",

    @field:Json(name = "legRequestList")
    var legRequestList: List<LegDetailsCommonDto> = emptyList(),

    @field:Json(name = "underlier")
    var contingentSymbol: String? = null,

    @field:Json(name = "productType1")
    var contingentProductType: String? = null,

    @field:Json(name = "stopPrice1")
    var contingentValue: String? = null,

    @field:Json(name = "aoOffSetType")
    var contingentCondition: String? = null,

    @field:Json(name = "aoFollowPrice")
    var contingentQualifier: String? = null
)
