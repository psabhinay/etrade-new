package com.etrade.mobilepro.portfolio

import com.etrade.mobilepro.accounts.COUNT_TO_SHOW_ACCOUNT_ORGANIZATION
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.tooltip.TooltipViewModel
import javax.inject.Inject

class PortfolioToolTipViewModel @Inject constructor(
    private val applicationPreferences: ApplicationPreferences
) : TooltipViewModel() {

    var isToolTipEnabled = true

    val showControlBarToolTip
        get() = isToolTipEnabled && if (applicationPreferences.homeLandingCount == COUNT_TO_SHOW_ACCOUNT_ORGANIZATION) {
            applicationPreferences.displayPortfolioTooltip.value == true &&
                applicationPreferences.displayPortfolioControlBarToolTip
        } else {
            applicationPreferences.displayPortfolioControlBarToolTip
        }

    val showSettingsToolTip
        get() = isToolTipEnabled && if (applicationPreferences.homeLandingCount == COUNT_TO_SHOW_ACCOUNT_ORGANIZATION) {
            applicationPreferences.displayPortfolioTooltip.value == true &&
                applicationPreferences.displayPortfolioSettingsToolTip
        } else {
            applicationPreferences.displayPortfolioSettingsToolTip
        }

    val showMoreInfoToolTip
        get() = isToolTipEnabled && if (applicationPreferences.homeLandingCount == COUNT_TO_SHOW_ACCOUNT_ORGANIZATION) {
            applicationPreferences.displayPortfolioTooltip.value == true &&
                applicationPreferences.displayPortfolioMoreInfoToolTip
        } else {
            applicationPreferences.displayPortfolioMoreInfoToolTip
        }

    fun onDismiss(type: PortfolioToolTipType) {
        when (type) {
            PortfolioToolTipType.MORE_INFO -> {
                applicationPreferences.displayPortfolioMoreInfoToolTip = false
            }
            PortfolioToolTipType.CONTROL_BAR -> {
                applicationPreferences.displayPortfolioControlBarToolTip = false
            }
            PortfolioToolTipType.SETTINGS -> {
                applicationPreferences.displayPortfolioSettingsToolTip = false
                applicationPreferences.displayPortfolioTooltip.value = false
            }
        }
    }

    enum class PortfolioToolTipType {
        CONTROL_BAR,
        SETTINGS,
        MORE_INFO
    }
}
