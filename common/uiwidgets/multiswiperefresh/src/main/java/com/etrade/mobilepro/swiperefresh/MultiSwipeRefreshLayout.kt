package com.etrade.mobilepro.swiperefresh

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

/**
 * SwipeRefreshLayout, Add your scrollable views when they are not direct children of
 * SwipeRefreshLayout and you want pull down to refresh to be disabled when any of the
 * scrollable views can to be scrolled up vertically.
 */
class MultiSwipeRefreshLayout : SwipeRefreshLayout {

    private var swipeableChildren: List<View> = listOf()

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    init {
        applyColorScheme()
    }

    fun setSwipeableChildren(vararg views: View) {
        swipeableChildren = views.toList()
    }

    override fun canChildScrollUp(): Boolean {
        return if (swipeableChildren.isNotEmpty()) {
            swipeableChildren.any { view ->
                view.isShown && (view.canScrollVertically(-1))
            }
        } else {
            super.canChildScrollUp()
        }
    }
}
