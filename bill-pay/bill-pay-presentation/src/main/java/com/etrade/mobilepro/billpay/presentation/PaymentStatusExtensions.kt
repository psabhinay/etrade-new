package com.etrade.mobilepro.billpay.presentation

import com.etrade.mobilepro.billpay.api.PaymentStatus

@Suppress("ComplexMethod")
fun PaymentStatus.getLabel() = when (this) {
    PaymentStatus.ALL -> R.string.all
    PaymentStatus.SCHEDULED -> R.string.bill_pay_status_scheduled
    PaymentStatus.PAID -> R.string.bill_pay_status_paid
    PaymentStatus.CANCELLED -> R.string.cancelled
    PaymentStatus.FAILED -> R.string.failed
    PaymentStatus.INPROCESS -> R.string.bill_pay_status_inprocess
    PaymentStatus.UNKNOWN -> R.string.empty_default
    else -> R.string.empty_default
}
