package com.etrade.mobilepro.caching.androidx

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import com.etrade.mobilepro.caching.CachedResource
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.util.Resource
import io.reactivex.BackpressureStrategy

fun <ResultType : Any, NetworkType : Any, CacheType : Any> CachedResource<ResultType, NetworkType, CacheType>.asLiveData(): LiveData<Resource<ResultType>> {
    return LiveDataReactiveStreams.fromPublisher(asObservable().toFlowable(BackpressureStrategy.LATEST))
}

fun <ResultType : Any, IdType> SimpleCachedResource<ResultType, IdType>.asLiveData(): LiveData<Resource<ResultType>> {
    return LiveDataReactiveStreams.fromPublisher(asObservable().toFlowable(BackpressureStrategy.LATEST))
}
