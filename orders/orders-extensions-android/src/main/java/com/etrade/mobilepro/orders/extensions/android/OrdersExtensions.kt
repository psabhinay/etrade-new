package com.etrade.mobilepro.orders.extensions.android

import com.etrade.mobilepro.orders.api.ContingentCondition
import com.etrade.mobilepro.orders.api.ContingentQualifier
import com.etrade.mobilepro.orders.api.DividendReInvestmentType
import com.etrade.mobilepro.orders.api.EventType
import com.etrade.mobilepro.orders.api.FundsOrderActionType
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType

fun DividendReInvestmentType.getLabel() = when (this) {
    DividendReInvestmentType.RE_INVEST -> R.string.investment_type_reinvestment
    DividendReInvestmentType.DEPOSIT_FUND -> R.string.investment_type_deposit
    else -> null
}

@Suppress("ComplexMethod")
fun OrderStatus.getLabel() = when (this) {
    OrderStatus.ALL -> R.string.all
    OrderStatus.ALL_BY_DATE -> R.string.all_by_date
    OrderStatus.ALL_BY_SYMBOL -> R.string.all_by_symbol
    OrderStatus.SAVED -> R.string.saved
    OrderStatus.OPEN -> R.string.open
    OrderStatus.CANCEL_REQUESTED -> R.string.cancel_requested
    OrderStatus.CANCEL_REJECTED -> R.string.cancel_rejected
    OrderStatus.CANCELLED -> R.string.cancelled
    OrderStatus.REJECT_REQUESTED -> R.string.reject_requested
    OrderStatus.REQUEST_SENT_TO_MARKET -> R.string.request_sent_to_market
    OrderStatus.EXECUTED -> R.string.executed
    OrderStatus.REJECTED -> R.string.rejected
    OrderStatus.EXPIRED -> R.string.expired
    OrderStatus.UNKNOWN -> R.string.all
    OrderStatus.OPEN_AND_UNSETTLED -> R.string.open_and_unsettled
    OrderStatus.SETTLED -> R.string.order_status_settled
}

@Suppress("ComplexMethod")
fun OrderType.getLabel() = when (this) {
    OrderType.EQUITY -> R.string.equity
    OrderType.ADVANCE_EQUITY -> R.string.advance_equity
    OrderType.SIMPLE_OPTION -> R.string.simple_option
    OrderType.SPREADS -> R.string.spreads
    OrderType.BUY_WRITES -> R.string.buy_writes
    OrderType.OPTION_EXERCISED -> R.string.option_exercised
    OrderType.OPTION_ASSIGNED -> R.string.option_assigned
    OrderType.OPTION_EXPIRED -> R.string.option_expired
    OrderType.MUTUAL_FUND -> R.string.mutual_fund
    OrderType.MONEY_MARKET_FUND -> R.string.money_market_fund
    OrderType.BOND -> R.string.bond
    OrderType.ADVANCE_OPTION -> R.string.advance_option
    OrderType.OPTION_DO_NOT_EXERCISE -> R.string.option_do_not_exercise
    OrderType.BUTTERFLY -> R.string.butterfly
    OrderType.CONDOR -> R.string.condor
    OrderType.IRON_CONDOR -> R.string.iron_condor
    OrderType.IRON_BUTTERFLY -> R.string.iron_butterfly
    OrderType.COLLARS -> R.string.collars
    OrderType.CUSTOM -> R.string.custom
    OrderType.CONTINGENT -> R.string.contingent
    OrderType.CONDITIONAL_ONE_CANCEL_OTHER -> R.string.conditional_one_cancel_other
    OrderType.CONDITIONAL_ONE_TRIGGER_OTHER -> R.string.conditional_one_trigger_other
    OrderType.CONDITIONAL_OTOCO -> R.string.conditional_one_trigger_oco
    OrderType.OTHER -> R.string.other
    OrderType.UNKNOWN -> R.string.unknown
}

@Suppress("ComplexMethod")
fun TransactionType.getLabel() = when (this) {
    TransactionType.BUY_OPEN -> R.string.buy
    TransactionType.SELL_CLOSE -> R.string.sell
    TransactionType.SELL_OPEN -> R.string.sell_short
    TransactionType.BUY_CLOSE -> R.string.buy_to_cover

    TransactionType.BUY_OPEN_OPT -> R.string.buy_open
    TransactionType.SELL_CLOSE_OPT -> R.string.sell_close
    TransactionType.SELL_OPEN_OPT -> R.string.sell_open
    TransactionType.BUY_CLOSE_OPT -> R.string.buy_close
    TransactionType.UNKNOWN -> R.string.unknown
}

@Suppress("LongMethod", "ComplexMethod")
fun PriceType.getLabel() = when (this) {
    PriceType.UNKNOWN -> R.string.other
    PriceType.LIMIT -> R.string.limit
    PriceType.MARKET -> R.string.market
    PriceType.STOP -> R.string.stop
    PriceType.STOP_LIMIT -> R.string.stop_limit
    PriceType.TRAILING_STOP_DOLLAR -> R.string.trailing_stop_dollar
    PriceType.TRAILING_STOP_PERCENT -> R.string.trailing_stop_percent
    PriceType.TRAILING_STOP_LIMIT -> R.string.trailing_stop_limit
    PriceType.EVEN -> R.string.even
    PriceType.NET_DEBIT -> R.string.net_debit
    PriceType.NET_CREDIT -> R.string.net_credit
    PriceType.BRACKETED -> R.string.bracketed
    PriceType.HIDDEN_STOP -> R.string.hidden_stop
    PriceType.HIDDEN_LIMIT -> R.string.hidden_limit
    PriceType.RESERVE_LIMIT -> R.string.reserve_limit
    PriceType.MARKET_ON_OPEN -> R.string.market_on_open
    PriceType.MARKET_ON_CLOSE -> R.string.market_on_close
    PriceType.LIMIT_ON_OPEN -> R.string.limit_on_open
    PriceType.LIMIT_ON_CLOSE -> R.string.limit_on_close
    PriceType.FIRM -> R.string.firm
    PriceType.SUBJECT -> R.string.subject
    PriceType.H_STOP_LOWER_TRIGGER -> R.string.h_stop_lower_trigger
    PriceType.UPPER_TRIGGER_H_STOP -> R.string.upper_trigger_h_stop
    PriceType.T_STOP_PERCENT_LOWER_TRIGGER -> R.string.t_stop_percent_lower_trigger
    PriceType.T_STOP_DOLLAR_LOWER_TRIGGER -> R.string.t_stop_dollar_lower_trigger
    PriceType.UPPER_TRIGGER_H_STOP_PERCENT -> R.string.upper_trigger_h_stop_percent
    PriceType.UPPER_TRIGGER_H_STOP_DOLLAR -> R.string.upper_trigger_h_stop_dollar
    PriceType.H_STOP -> R.string.h_stop
    PriceType.LIMIT_PRESENTABLE -> R.string.limit_presentable
    else -> R.string.other
}

@Suppress("ComplexMethod")
fun StrategyType.getLabel() = when (this) {
    StrategyType.STOCK -> R.string.stock
    StrategyType.CALL -> R.string.call
    StrategyType.PUT -> R.string.put
    StrategyType.CALL_SPREAD -> R.string.call_spread
    StrategyType.PUT_SPREAD -> R.string.put_spread
    StrategyType.BUY_WRITE -> R.string.covered_call
    StrategyType.MARRIED_PUT -> R.string.married_put
    StrategyType.STRADDLE -> R.string.straddle
    StrategyType.STRANGLE -> R.string.strangle
    StrategyType.CALENDAR -> R.string.calendar
    StrategyType.DIAGONAL -> R.string.diagonal
    StrategyType.BUTTERFLY -> R.string.butterfly
    StrategyType.IRON_BUTTERFLY -> R.string.iron_butterfly
    StrategyType.CONDOR -> R.string.condor
    StrategyType.IRON_CONDOR -> R.string.iron_condor
    StrategyType.COMBO -> R.string.combo
    StrategyType.COLLARS -> R.string.collars
    StrategyType.CUSTOM -> R.string.custom
    StrategyType.UNKNOWN -> R.string.unknown
    else -> R.string.unknown
}

@Suppress("ComplexMethod")
fun EventType.getLabel() = when (this) {
    EventType.PLACED -> R.string.placed
    EventType.SENT_TO_CMS -> R.string.sent_to_cms
    EventType.SENT_TO_MARKET -> R.string.sent_to_market
    EventType.SENT_ACKNOWLEDGED -> R.string.sent_acknowledged
    EventType.CANCEL_REQUESTED -> R.string.cancel_requested
    EventType.MODIFIED -> R.string.modified
    EventType.BROKER_RELEASE -> R.string.broker_release
    EventType.SYSTEM_REJECTED -> R.string.system_rejected
    EventType.MARKET_REJECTED -> R.string.rejected
    EventType.CANCEL_CONFIRMED -> R.string.cancelled
    EventType.CANCEL_REJECTED -> R.string.cancel_rejected
    EventType.EXPIRED -> R.string.expired
    EventType.EXECUTED -> R.string.executed
    EventType.EXECUTION_ADJUSTED -> R.string.adjusted
    EventType.EXECUTION_REVERSED -> R.string.reversal
    EventType.CANCELLATION_REVERSED -> R.string.cancellation_reversed
    EventType.EXPIRATION_REVERSED -> R.string.expiration_reversed
    EventType.OPT_POSITION_ASSIGNED -> R.string.option_assignment
    EventType.OPT_POSITION_EXPIRED -> R.string.open_order_adjusted
    EventType.OPEN_ORDER_ADJUSTED -> R.string.ca_cancelled
    EventType.OPT_POSITION_EXERCISED -> R.string.pla_ca_bookedced
    EventType.CA_CANCELLED -> R.string.ipo_allocated
    EventType.DONE_TRADE_EXECUTED -> R.string.done_trade_executed
    EventType.REJECTION_REVERSED -> R.string.rejection_reversed
    EventType.UNKNOWN -> R.string.unknown
    else -> R.string.unknown
}

@Suppress("ComplexMethod")
fun OrderTerm.getLabel() = when (this) {
    OrderTerm.GOOD_FOR_DAY -> R.string.term_day
    OrderTerm.GOOD_UNTIL_CANCEL -> R.string.term_gtc
    OrderTerm.FILL_OR_KILL -> R.string.term_fok
    OrderTerm.IMMEDIATE_OR_CANCEL -> R.string.term_ioc
    OrderTerm.EXTENDED_HOUR_DAY -> R.string.term_ehday
    OrderTerm.EXTENDED_HOUR_IOC -> R.string.term_ehioc
    OrderTerm.GOOD_TILL_DATE -> R.string.term_gtd
    OrderTerm.GOOD_TILL_DATE_DNI -> R.string.term_gtd
    OrderTerm.GOOD_TILL_DATE_DNR -> R.string.term_gtd
    OrderTerm.GOOD_UNTIL_CANCEL_DNI -> R.string.term_gtc
    OrderTerm.GOOD_UNTIL_CANCEL_DNR -> R.string.term_gtc
    OrderTerm.UNKNOWN -> R.string.unknown
}

fun ContingentQualifier.getLabel() = when (this) {
    ContingentQualifier.ASK -> R.string.ask_price
    ContingentQualifier.BID -> R.string.bid_price
    ContingentQualifier.LAST_PRICE -> R.string.last_price
    ContingentQualifier.UNKNOWN -> R.string.unknown
}

fun ContingentCondition.getLabel() = when (this) {
    ContingentCondition.GREATER_THAN_OR_EQUAL -> R.string.contingent_order_condition_greater_than
    ContingentCondition.LESS_THAN_OR_EQUAL -> R.string.contingent_order_condition_less_than
    ContingentCondition.UNKNOWN -> R.string.unknown
}

fun PriceType?.getTerms(isStocks: Boolean): List<OrderTerm> = when (this) {
    PriceType.MARKET -> listOf(OrderTerm.GOOD_FOR_DAY)
    PriceType.LIMIT ->
        mutableListOf(
            OrderTerm.GOOD_FOR_DAY,
            OrderTerm.GOOD_UNTIL_CANCEL,
            OrderTerm.FILL_OR_KILL,
            OrderTerm.IMMEDIATE_OR_CANCEL
        ).apply {
            if (isStocks) {
                add(OrderTerm.EXTENDED_HOUR_DAY)
                add(OrderTerm.EXTENDED_HOUR_IOC)
            }
        }
    PriceType.STOP, PriceType.STOP_LIMIT, PriceType.TRAILING_STOP_DOLLAR, PriceType.TRAILING_STOP_PERCENT -> listOf(
        OrderTerm.GOOD_FOR_DAY,
        OrderTerm.GOOD_UNTIL_CANCEL
    )
    else -> listOf(OrderTerm.GOOD_FOR_DAY)
}

fun FundsOrderActionType.getLabel() = when (this) {
    FundsOrderActionType.BUY -> R.string.buy
    FundsOrderActionType.SELL -> R.string.sell
    FundsOrderActionType.EXCHANGE -> R.string.exchange
}
