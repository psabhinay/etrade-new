package com.etrade.mobilepro.viewmodel

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.util.AppMessage

/**
 * Marks a view model that may want to signal a view model to show an app message.
 */
interface AppMessageViewModel {

    /**
     * Error message signal.
     */
    val appMessageSignal: LiveData<AppMessage?>

    /**
     * Dismisses an app message when it is no longer needed.
     */
    fun dismissAppMessage()
}
