package com.etrade.mobilepro.news.api.repo

import androidx.paging.Pager
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.api.model.NewsType
import io.reactivex.Observable

interface NewsRepo {
    fun news(
        type: NewsType,
        requestParams: NewsRequest? = null,
        headerItemsSource: Observable<List<NewsItem>>? = null
    ): Pager<Int, NewsItem>
    fun textContent(contentId: String): Observable<NewsItem.Text>
    fun symbolNews(requestParams: NewsRequest.Symbol, maxCount: Int = 3): Observable<List<NewsItem.Text>>
    fun marketNews(maxCount: Int = 3): Observable<List<NewsItem.Text>>
}
