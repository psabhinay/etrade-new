package com.etrade.mobilepro.watchlist.presentation.compose

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.window.Popup
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.FontSizes
import com.etrade.mobilepro.watchlist.overview.viewmodel.WatchlistOverviewViewModel
import com.etrade.mobilepro.watchlist.presentation.R
import com.etrade.mobilepro.watchlist.presentation.tableview.WatchlistTableViewControlBarViewModel
import com.etrade.mobilepro.watchlistapi.Watchlist

private const val ROUND_PERCENTAGE = 10

private const val FLOAT_ZERO = 0F
private const val FLOAT_ONE = 1F
private const val FLOAT_POINT_FIVE = 0.5F
private const val FLOAT_POINT_THREE = 0.3F
private const val INT_ZERO = 0
private const val GRID_BUTTON_INDEX = 0
private const val LIST_BUTTON_INDEX = 1
private const val TILE_BUTTON_INDEX = 2

@Suppress("LongParameterList")
@Composable
fun ControlBar(
    overviewViewModel: WatchlistOverviewViewModel,
    controlBarViewModel: WatchlistTableViewControlBarViewModel,
    tableTypeSelectedIndex: Int,
    onDropDownClick: () -> Unit,
    onTableTypeChange: (Int) -> Unit,
    onCustomizeViewClick: () -> Unit,
    onTooltipDismissRequest: () -> Unit,
) {
    val highlightWatchlistActionSignal by overviewViewModel.highlightWatchlistActionSignal.observeAsState()
    val selectedWatchlist: Watchlist? by overviewViewModel.selectedWatchList.observeAsState()
    val tooltipDismissed by controlBarViewModel.tooltipDismissed.observeAsState()
    val enableCustomizeSettings by controlBarViewModel.enableCustomizeSettings.observeAsState()

    val showTooltip = (tooltipDismissed == false).and(highlightWatchlistActionSignal?.isConsumed == false)

    Column(modifier = Modifier.requiredHeight(Dimens.Size48dp)) {
        Row(modifier = Modifier.weight(FLOAT_ONE), verticalAlignment = Alignment.CenterVertically) {

            DropDownButton(
                showTooltip,
                Modifier.weight(FLOAT_ONE),
                selectedWatchlist, onDropDownClick, onTooltipDismissRequest
            )

            GridButton(tableTypeSelectedIndex, onTableTypeChange)

            ListButton(tableTypeSelectedIndex, onTableTypeChange)

            TileButton(tableTypeSelectedIndex, onTableTypeChange)

            SettingsButton(enableCustomizeSettings, onCustomizeViewClick)
        }
        Divider(
            color = colorResource(id = R.color.light_grey),
            thickness = Dimens.Size1dp
        )
    }
}

@Suppress("LongMethod")
@Composable
private fun DropDownButton(
    showTooltip: Boolean,
    modifier: Modifier,
    selectedWatchlist: Watchlist?,
    onDropDownClick: () -> Unit,
    onTooltipDismissRequest: () -> Unit
) {
    var buttonSize by remember { mutableStateOf(IntSize.Zero) }
    OutlinedButton(
        onClick = {
            onDropDownClick()
        },
        colors = ButtonDefaults.outlinedButtonColors(backgroundColor = Color.Transparent),
        contentPadding = PaddingValues(
            start = Dimens.Size8dp,
            bottom = Dimens.Size1dp,
            end = Dimens.Size14dp
        ),
        modifier = modifier
            .padding(horizontal = Dimens.Size8dp)
            .height(Dimens.Size32dp)
            .border(ROUND_PERCENTAGE)
            .onSizeChanged {
                buttonSize = it
            }
    ) {
        Text(
            text = selectedWatchlist?.name ?: "",
            color = colorResource(id = R.color.textColorPrimary),
            fontSize = FontSizes.Size16sp,
            textAlign = TextAlign.Start,
            fontWeight = FontWeight.Normal,
            modifier = Modifier.weight(FLOAT_ONE),
            maxLines = 1,
            overflow = TextOverflow.Ellipsis

        )
        Icon(
            painter = painterResource(id = R.drawable.ic_dropdown_arrow_auto_color),
            contentDescription = null,
            tint = colorResource(id = R.color.primaryColor)
        )
        if (showTooltip) {
            Popup(
                alignment = Alignment.TopStart,
                offset = IntOffset(INT_ZERO, (buttonSize.height * FLOAT_POINT_FIVE).toInt()),
                onDismissRequest = {
                    onTooltipDismissRequest.invoke()
                }
            ) {
                Tooltip(stringResource(R.string.watchlist_action_in_watchlist))
            }
        }
    }
}

@Composable
fun Tooltip(text: String) {
    Column {
        Row(
            modifier = Modifier
                .padding(PaddingValues(start = Dimens.Size14dp))
                .background(
                    color = colorResource(id = R.color.colorAccent),
                    shape = tooltipTriangleShape()
                )
                .width(Dimens.Size14dp)
                .height(Dimens.Size8dp)
        ) {}

        Row(
            modifier = Modifier
                .background(
                    color = colorResource(id = R.color.colorAccent),
                    shape = RoundedCornerShape(size = Dimens.Size4dp)
                )
        ) {
            Text(
                text = text,
                color = colorResource(id = R.color.white),
                modifier = Modifier
                    .padding(Dimens.Size16dp)
                    .fillMaxWidth(FLOAT_POINT_FIVE),
                textAlign = TextAlign.Center,
            )
        }
    }
}

fun tooltipTriangleShape() = object : Shape {
    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ): Outline {
        val trianglePath = Path()
        trianglePath.apply {
            moveTo(x = size.width / 2, y = FLOAT_ZERO)
            lineTo(x = size.width, y = size.height)
            lineTo(x = FLOAT_ZERO, y = size.height)
        }
        return Outline.Generic(trianglePath)
    }
}

@Composable
private fun GridButton(tableTypeSelectedIndex: Int, onTableTypeChange: (Int) -> Unit) {
    IconButton(
        onClick = { onTableTypeChange(GRID_BUTTON_INDEX) },
        modifier = Modifier
            .requiredSize(Dimens.Size48dp, Dimens.Size32dp)
            .border(ROUND_PERCENTAGE, INT_ZERO, INT_ZERO, ROUND_PERCENTAGE)
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_position_view_type_grid),
            contentDescription = "Grid view",
            tint = tintColor(tableTypeSelectedIndex, GRID_BUTTON_INDEX)
        )
    }
}

@Composable
private fun ListButton(tableTypeSelectedIndex: Int, onTableTypeChange: (Int) -> Unit) {
    IconButton(
        onClick = { onTableTypeChange(LIST_BUTTON_INDEX) },
        modifier = Modifier
            .requiredSize(Dimens.Size48dp, Dimens.Size32dp)
            .border()
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_position_view_type_list),
            contentDescription = "List view",
            tint = tintColor(tableTypeSelectedIndex, LIST_BUTTON_INDEX)
        )
    }
}

@Composable
private fun TileButton(tableTypeSelectedIndex: Int, onTableTypeChange: (Int) -> Unit) {
    IconButton(
        onClick = { onTableTypeChange(TILE_BUTTON_INDEX) },
        modifier = Modifier
            .requiredSize(Dimens.Size48dp, Dimens.Size32dp)
            .border(INT_ZERO, ROUND_PERCENTAGE, ROUND_PERCENTAGE, INT_ZERO)
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_position_view_type_tile),
            contentDescription = "Tile view",
            tint = tintColor(tableTypeSelectedIndex, TILE_BUTTON_INDEX)
        )
    }
}

@Composable
private fun SettingsButton(
    enableCustomizeSettings: Boolean?,
    onCustomizeViewClick: () -> Unit
) {
    IconButton(
        onClick = { onCustomizeViewClick() },
        enabled = enableCustomizeSettings == true,
        modifier = Modifier
            .alpha(settingsAlpha(enableCustomizeSettings))
            .padding(vertical = Dimens.Size4dp, horizontal = Dimens.Size8dp)
            .requiredSize(Dimens.Size32dp)
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_controlbar_settings),
            contentDescription = "Settings",
            tint = Color.Unspecified
        )
    }
}

private fun settingsAlpha(enableCustomizeSettings: Boolean?): Float = if (enableCustomizeSettings == true) {
    FLOAT_ONE
} else {
    FLOAT_POINT_THREE
}

private fun Modifier.border(percent: Int = INT_ZERO): Modifier =
    this.border(percent, percent, percent, percent)

@SuppressLint("UnnecessaryComposedModifier")
private fun Modifier.border(
    topStartPercent: Int = INT_ZERO,
    topEndPercent: Int = INT_ZERO,
    bottomEndPercent: Int = INT_ZERO,
    bottomStartPercent: Int = INT_ZERO
): Modifier = composed {
    this.border(
        Dimens.Size1dp,
        colorResource(id = R.color.light_grey),
        shape = RoundedCornerShape(topStartPercent, topEndPercent, bottomEndPercent, bottomStartPercent)
    )
}

@Composable
private fun tintColor(tableTypeSelectedIndex: Int, itemIndex: Int): Color {
    return if (tableTypeSelectedIndex == itemIndex) {
        colorResource(id = R.color.purple)
    } else {
        colorResource(id = R.color.light_black)
    }
}
