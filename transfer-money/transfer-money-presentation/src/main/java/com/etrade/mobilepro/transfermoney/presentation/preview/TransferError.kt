package com.etrade.mobilepro.transfermoney.presentation.preview

class TransferError(val code: String?, val description: String)
