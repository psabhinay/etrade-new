package com.etrade.mobilepro.checkdeposit.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.ByteArrayOutputStream

/*
 * None of these Bitmap related Android APIs function correctly on Robolectric 4.2
 */

private const val MAX_WIDTH = 1600
private const val JPEG_COMPRESSION_QUALITY = 30

/**
 * Scales the bitmap maintaining aspect ratio so there is a maximum width of 1600 pixels.
 * Compresses the bitmap to JPEG format
 */
fun prepareCheckImageForUpload(image: ByteArray): ByteArray {
    val scaledBitmap = createScaledBitmap(image, MAX_WIDTH)
    val compressedBitmap = compressBitmap(scaledBitmap)
    return compressedBitmap.toByteArray()
}

internal fun compressBitmap(image: Bitmap): ByteArrayOutputStream {
    return ByteArrayOutputStream().also {
        image.compress(Bitmap.CompressFormat.JPEG, JPEG_COMPRESSION_QUALITY, it)
    }
}

internal fun createScaledBitmap(image: ByteArray, maxWidth: Int): Bitmap {
    val imageWidth = getBitmapWidth(image)
    val options = if (imageWidth > maxWidth) {
        BitmapFactory.Options().apply {
            inScaled = true
            inDensity = imageWidth
            inTargetDensity = maxWidth
        }
    } else { null }

    return BitmapFactory.decodeByteArray(image, 0, image.size, options)
}

internal fun getBitmapWidth(image: ByteArray): Int {
    val options = BitmapFactory.Options().apply {
        inJustDecodeBounds = true
    }
    BitmapFactory.decodeByteArray(image, 0, image.size, options)
    return options.outWidth
}
