package com.etrade.mobilepro.bloombergtv.presentation.delegate

import android.view.View
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvActivity
import com.etrade.mobilepro.util.android.network.ConnectionNotifier

internal class ConnectionNotifierDelegate(
    private val onConnectivityChange: (hasConnectivity: Boolean) -> Unit
) : BloombergTvActivityDelegate {
    override fun init(activity: BloombergTvActivity) {
        fun updateNetworkDependentViews(isNetworkConnected: Boolean) {
            with(activity.binding.videoSurface) {
                if (isNetworkConnected) {
                    visibility = View.VISIBLE
                    onConnectivityChange(true)
                } else {
                    visibility = View.GONE
                    onConnectivityChange(false)
                }
            }
        }

        val connectionNotifier = ConnectionNotifier(activity.baseContext)
        connectionNotifier.createNotifier(object : ConnectionNotifier.ConnectionNotification {

            override fun onNetworkAvailable() = activity.runOnUiThread {
                updateNetworkDependentViews(true)
            }

            override fun onNetworkLost() = activity.runOnUiThread {
                updateNetworkDependentViews(false)
            }
        })?.let { activity.lifecycle.addObserver(it) }
    }
}
