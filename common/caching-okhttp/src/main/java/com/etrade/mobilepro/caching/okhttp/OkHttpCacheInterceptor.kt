package com.etrade.mobilepro.caching.okhttp

import okhttp3.Interceptor
import okhttp3.Response

private const val CACHE_CONTROL_HEADER_NAME = "Cache-Control"
const val CACHE_CONTROL_DEFAULT_EXPIRATION = "$CACHE_CONTROL_HEADER_NAME: max-age=20"

class OkHttpCacheInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalResponse = chain.proceed(originalRequest)

        return originalRequest.headers[CACHE_CONTROL_HEADER_NAME]?.let {
            originalResponse.newBuilder()
                .removeHeader(CACHE_CONTROL_HEADER_NAME)
                .removeHeader("Pragma")
                .addHeader(CACHE_CONTROL_HEADER_NAME, it)
                .build()
        } ?: originalResponse
    }
}
