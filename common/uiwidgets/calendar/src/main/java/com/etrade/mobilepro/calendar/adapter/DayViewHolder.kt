package com.etrade.mobilepro.calendar.adapter

import android.view.View
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.calendar.R
import com.etrade.mobilepro.calendar.databinding.AdapterWeekDayBinding
import com.etrade.mobilepro.util.android.binding.resources
import org.threeten.bp.LocalDate
import org.threeten.bp.format.TextStyle
import java.util.Locale

class DayViewHolder(
    private val binding: AdapterWeekDayBinding,
    private val onDateSelectListener: OnDateSelectListener,
    private val disabledDaysPredicate: (LocalDate) -> Boolean,
    private val clickableDaysPredicate: (LocalDate) -> Boolean,
    private val dayFormatter: ((LocalDate) -> String)?,
    private val weekDayHeight: Int
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: LocalDate, events: Set<LocalDate>, currentDate: LocalDate, selectedDate: LocalDate?) {
        val isSelectedDate: Boolean = item == selectedDate
        val isCurrentDate: Boolean = item == currentDate

        itemView.layoutParams.height = weekDayHeight
        binding.setupDayView(item, dayFormatter)

        if (disabledDaysPredicate(item)) {
            markDayAsDisabled()
        }

        itemView.setOnClickListener {
            onDateSelectListener.onDateSelected(it, absoluteAdapterPosition, item)
        }

        if (clickableDaysPredicate(item)) {
            itemView.isEnabled = true
        } else {
            itemView.isEnabled = false
            markDayAsDisabled()
        }

        binding.markDayAndEvents(item, events, isSelectedDate, isCurrentDate)
    }

    private fun markDayAsDisabled() {
        binding.backgroundView.visibility = View.VISIBLE
        TextViewCompat.setTextAppearance(binding.day, R.style.Day_Disabled)
        binding.day.changeViewBackground(R.color.white_transparent)
    }
}

internal fun AdapterWeekDayBinding.markDayAndEvents(item: LocalDate, events: Set<LocalDate>, isSelectedDate: Boolean, isCurrentDate: Boolean) {
    day.processSelection(isSelectedDate, isCurrentDate)
    markEvents(item, events, isSelectedDate, isCurrentDate)
}

private fun TextView.processSelection(isSelectedDate: Boolean, isCurrentDate: Boolean) {
    when {
        isSelectedDate -> {
            setTextAppearance(R.style.Day_Selected)
            changeViewBackground(R.color.purple)
            isSelected = true
        }
        isCurrentDate -> {
            setTextAppearance(R.style.Day_Selected)
            changeViewBackground(R.color.light_purple)
            isSelected = false
        }
        else -> {
            setTextAppearance(R.style.Day_Active)
            changeViewBackground(R.color.white_transparent)
            isSelected = false
        }
    }
}

private fun AdapterWeekDayBinding.setupDayView(item: LocalDate, dayFormatter: ((LocalDate) -> String)?) {
    root.tag = item
    day.text = dayFormatter?.let { it(item) }
    root.contentDescription = "${item.dayOfWeek.getDisplayName(TextStyle.FULL, Locale.US)} $item"

    TextViewCompat.setTextAppearance(day, R.style.Day_Active)
    backgroundView.visibility = View.GONE
}

private fun AdapterWeekDayBinding.markEvents(item: LocalDate, events: Set<LocalDate>, isSelectedDate: Boolean, isCurrentDate: Boolean) {
    if (events.contains(item)) {
        if (isSelectedDate || isCurrentDate) {
            event.changeViewBackground(R.color.white)
        } else {
            event.changeViewBackground(R.color.purple)
        }
        day.contentDescription = resources.getString(R.string.has_report)
    } else {
        event.changeViewBackground(R.color.white_transparent)
        day.contentDescription = resources.getString(R.string.has_no_report)
    }
}

private fun View.changeViewBackground(@ColorRes colorRes: Int) {
    ContextCompat.getDrawable(context, R.drawable.shape_circle_white)?.mutate()?.let {
        val drawable = DrawableCompat.wrap(it)
        background = drawable
        DrawableCompat.setTint(drawable, ContextCompat.getColor(context, colorRes))
    }
}
