package com.etrade.mobilepro.personalnotifications.fragment

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.personalnotifications.R
import com.etrade.mobilepro.personalnotifications.adapter.PersonalNotificationListDelegationAdapter
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.Localytics
import com.etrade.mobilepro.personalnotifications.databinding.FragmentPersonalNotificationsBinding
import com.etrade.mobilepro.personalnotifications.viewmodel.PersonalNotificationsViewModel
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@RequireLogin
class PersonalNotificationsFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackBarUtilFactory: SnackbarUtilFactory,
    imageLoader: ImageLoader
) : Fragment(R.layout.fragment_personal_notifications) {

    private val binding by viewBinding(FragmentPersonalNotificationsBinding::bind) { onDestroyBinding() }
    private val notificationsListAdapter by lazy {
        PersonalNotificationListDelegationAdapter(
            imageLoader,
            readStatusChecker = {
                viewModel.wasRead(it)
            },
            clickListener = {
                viewModel.selectNotification(it)
            },
            itemDismissListener = {
                viewModel.dismissInboxMessage(it)
            }
        )
    }
    private val snackBarUtil by lazy { snackBarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private var snackBar: Snackbar? = null
    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(PersonalNotificationsViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding.notificationsList) {
            adapter = notificationsListAdapter
            setupRecyclerViewDivider(R.drawable.thin_divider, countOfLastItemsWithoutDivider = 0)
        }

        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                handleViewState(it)
            }
        )

        viewModel.clickEvents.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { ctaEvent ->
                    when (ctaEvent) {
                        is CtaEvent.UriEvent -> findNavController().navigate(ctaEvent.uri)
                        is CtaEvent.ActionEvent -> findNavController().navigate(Uri.parse(ctaEvent.url))
                        is CtaEvent.NavDirectionsNavigation -> findNavController().navigate(ctaEvent.ctaDirections)
                    }
                }
            }
        )
    }

    private fun handleViewState(viewState: PersonalNotificationsViewModel.ViewState) {
        when (viewState) {
            is PersonalNotificationsViewModel.ViewState.Success -> {
                binding.progressBar.hide()
                snackBar?.dismiss()
                if (viewState.notifications.isEmpty()) {
                    binding.emptyNotificationsMessage.visibility = View.VISIBLE
                }
                notificationsListAdapter.updateItems(viewState.notifications)
                // EBM and Banner notifications are considered as read after seen
                viewModel.markNotificationsAsRead(
                    viewState.notifications.filter {
                        it is Localytics.Banner || it is Localytics.EmergencyBroadcast
                    }
                )
                viewModel.tagLocalyticsMessage(viewState.notifications.filterIsInstance<Localytics>())
            }
            is PersonalNotificationsViewModel.ViewState.Failure -> {
                binding.progressBar.hide()
                notificationsListAdapter.updateItems(emptyList())
                snackBar = snackBarUtil.retrySnackbar { viewModel.getPersonalNotifications() }
            }
            is PersonalNotificationsViewModel.ViewState.Loading -> {
                binding.progressBar.show()
                snackBar?.dismiss()
            }
        }
    }

    fun FragmentPersonalNotificationsBinding.onDestroyBinding() {
        notificationsList.adapter = null
    }
}
