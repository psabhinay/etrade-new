package com.etrade.mobilepro.appwidget.watchlist.selection

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.appwidget.R
import com.etrade.mobilepro.appwidget.databinding.AppwidgetItemWatchlistBinding
import com.etrade.mobilepro.util.android.extension.inflater
import com.etrade.mobilepro.watchlistapi.Watchlist

data class WatchlistRow(
    val watchlist: Watchlist,
    val isSelected: Boolean
)

class WatchlistViewHolder(
    private val binding: AppwidgetItemWatchlistBinding,
    private val clickListener: (Watchlist) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    constructor(parent: ViewGroup, clickListener: (Watchlist) -> Unit) : this(
        AppwidgetItemWatchlistBinding.inflate(parent.inflater, parent, false), clickListener
    )

    private var item: WatchlistRow? = null

    init {
        binding.root.setOnClickListener {
            item?.watchlist?.let(clickListener)
        }
    }

    internal fun bind(item: WatchlistRow) {
        this.item = item
        binding.tvWatchlistName.text = item.watchlist.name
        if (item.isSelected) {
            binding.tvWatchlistName.setCompoundDrawablesRelativeWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_tableview_checkmark,
                0
            )
            binding.tvWatchlistName.isSelected = true
        } else {
            binding.tvWatchlistName.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0)
            binding.tvWatchlistName.isSelected = false
        }
    }
}
