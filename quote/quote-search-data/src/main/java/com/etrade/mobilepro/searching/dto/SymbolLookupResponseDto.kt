package com.etrade.mobilepro.searching.dto

import com.etrade.mobilepro.backends.af.ResultDto
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false, name = "SymbolLookupResponse")
data class SymbolLookupResponseDto(
    @field:ElementList(name = "SymbolLookupList", required = false)
    var symbolLookupList: List<SymbolLookupDto>? = null,
    @field:Element(name = "OptionMonthsList", required = false)
    var optionMonthsList: OptionMonthsDto? = null,
    @field:Element(name = "Result")
    var result: ResultDto? = null
)
