package com.etrade.mobilepro.chart.data;

import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/etrade/mobilepro/chart/data/ChartRequestDto;", "", "value", "Lcom/etrade/mobilepro/chart/data/ChartRequestValueDto;", "(Lcom/etrade/mobilepro/chart/data/ChartRequestValueDto;)V", "getValue", "()Lcom/etrade/mobilepro/chart/data/ChartRequestValueDto;", "chartdata"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ChartRequestDto {
    @org.jetbrains.annotations.NotNull()
    private final com.etrade.mobilepro.chart.data.ChartRequestValueDto value = null;
    
    public ChartRequestDto(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "value")
    com.etrade.mobilepro.chart.data.ChartRequestValueDto value) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.chart.data.ChartRequestValueDto getValue() {
        return null;
    }
}