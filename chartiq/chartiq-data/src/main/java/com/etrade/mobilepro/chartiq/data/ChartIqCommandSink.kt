package com.etrade.mobilepro.chartiq.data

/**
 * Represents a destination for ChartIQ commands
 */
interface ChartIqCommandSink {

    fun sendCommand(command: String)

    fun sendStreamingUpdateCommand(command: String)
}
