package com.etrade.mobilepro.trade.presentation.conditional

import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.getToolbarTitleContentDescription
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.dialog.viewmodel.DialogViewModel
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.presentation.BaseTradeFormFragment
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.util.android.fragment.OrientationLock
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.squareup.moshi.Moshi
import javax.inject.Inject

private const val KEY_ACCOUNT_ID = "accountId"
private const val KEY_IS_MAIN_TRADE_PAGE = "isMainTradePage"
private const val KEY_ORDER_INPUT_ID = "orderInputId"
private const val KEY_ORDER_NUMBER = "orderNumber"
private const val QUOTE_WIDGET_BID_ASK = "advancedBidAsk"

@RequireLogin
class ConditionalOrderFormFragment @Inject constructor(
    snackbarUtilFactory: SnackbarUtilFactory,
    viewModelFactory: ViewModelProvider.Factory,
    tradeRouter: TradeRouter,
    dialogViewModel: DialogViewModel,
    moshi: Moshi,
    walkthroughViewModel: WalkthroughStatesViewModel,
    orientationLock: OrientationLock
) : BaseTradeFormFragment(snackbarUtilFactory, viewModelFactory, tradeRouter, dialogViewModel, orientationLock, moshi, walkthroughViewModel) {

    override val formViewModel: ConditionalOrderFormViewModel by viewModels { viewModelFactory }

    private val viewModelProvider: ViewModelProvider by lazy { ViewModelProvider(this, viewModelFactory) }

    private val optionsBidAskOrderWidgetViewModel: QuotesWidgetViewModel by lazy {
        viewModelProvider.get(QUOTE_WIDGET_BID_ASK, QuotesWidgetViewModel::class.java)
    }

    override val securityType: SecurityType = SecurityType.CONDITIONAL

    private val accountId: String by lazy {
        requireNotNull(requireArguments().getString(KEY_ACCOUNT_ID))
    }

    private val orderInputId: String by lazy {
        requireNotNull(requireArguments().getString(KEY_ORDER_INPUT_ID))
    }

    private val orderNumber: Int by lazy {
        requireArguments().getInt(KEY_ORDER_NUMBER)
    }

    override fun onResume() {
        super.onResume()
        val pageTitle = getString(R.string.trade_title_conditional_order, orderNumber)

        if (isBottomSheetPage()) {
            findNavController().currentDestination?.label = pageTitle
        } else {
            toolbarTitleView?.apply {
                text = pageTitle
                contentDescription = getToolbarTitleContentDescription(pageTitle, requireContext())
            }
        }
    }

    private fun isBottomSheetPage() = !requireNotNull(requireArguments().getBoolean(KEY_IS_MAIN_TRADE_PAGE))

    override fun beforeFormFetch() {
        super.beforeFormFetch()

        formViewModel.accountId = accountId
        formViewModel.advancedOrderBidAskViewModel = optionsBidAskOrderWidgetViewModel
        formViewModel.orderInputId = orderInputId
        formViewModel.orderNumber = orderNumber
    }
}
