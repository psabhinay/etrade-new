package com.etrade.mobilepro.dao

import com.etrade.mobilepro.instrument.dao.InstrumentDao

interface InstrumentDaoProvider {
    fun getInstrumentDao(): InstrumentDao
}
