package com.etrade.mobilepro.trade.data

interface TradeRequestParamsUpdater {
    fun updatePreviewOrderParams(map: MutableMap<String, Any?>)
    fun updatePlaceOrderParams(map: MutableMap<String, Any?>)
}
