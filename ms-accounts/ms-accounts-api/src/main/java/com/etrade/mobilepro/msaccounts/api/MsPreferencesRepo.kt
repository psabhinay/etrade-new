package com.etrade.mobilepro.msaccounts.api

import kotlinx.coroutines.flow.StateFlow

interface MsPreferencesRepo {
    val msUserPreferences: StateFlow<MsUserPreferences>

    fun updateMSUserPreferences(nextPrefs: MsUserPreferences)
}
