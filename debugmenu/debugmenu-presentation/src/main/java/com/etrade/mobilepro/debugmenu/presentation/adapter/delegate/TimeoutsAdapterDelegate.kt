package com.etrade.mobilepro.debugmenu.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder.TimeoutsViewHolder
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemTimeoutsBinding
import com.etrade.mobilepro.debugmenu.presentation.item.DebugMenuItem
import com.etrade.mobilepro.debugmenu.presentation.item.TimeoutsItem
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class TimeoutsAdapterDelegate(
    private val listener: (sessionLimitEnabled: Boolean) -> Unit
) : AbsListItemAdapterDelegate<TimeoutsItem, DebugMenuItem, TimeoutsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): TimeoutsViewHolder {
        return TimeoutsViewHolder(parent.viewBinding(DebugmenuItemTimeoutsBinding::inflate), listener)
    }

    override fun isForViewType(item: DebugMenuItem, items: MutableList<DebugMenuItem>, position: Int): Boolean =
        item is TimeoutsItem

    override fun onBindViewHolder(item: TimeoutsItem, holder: TimeoutsViewHolder, payloads: MutableList<Any>) =
        holder.bindTo(item)
}
