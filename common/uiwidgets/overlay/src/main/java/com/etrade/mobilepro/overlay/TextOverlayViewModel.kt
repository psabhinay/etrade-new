package com.etrade.mobilepro.overlay

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.util.Resource

interface TextOverlayViewModel {
    val text: LiveData<Resource<CharSequence?>>
    val title: LiveData<String>
    val icon: LiveData<Int>
}
