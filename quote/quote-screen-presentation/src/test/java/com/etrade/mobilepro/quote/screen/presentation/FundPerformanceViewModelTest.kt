package com.etrade.mobilepro.quote.screen.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.quote.dto.FundQuoteResponseDto
import com.etrade.mobilepro.quote.mapper.DefaultFundQuoteDetailItemsMapper
import com.etrade.mobilepro.quote.mapper.MobileQuoteMapper
import com.etrade.mobilepro.quote.screen.presentation.ui.performance.FundPerformance
import com.etrade.mobilepro.quote.screen.presentation.ui.performance.FundPerformanceViewModel
import com.etrade.mobilepro.quote.screen.presentation.ui.performance.mapFundPerformanceAdjusted
import com.etrade.mobilepro.quote.screen.presentation.ui.performance.mapFundPerformanceNonAdjusted
import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.QuoteTemplateResources
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito

private const val GREEN = 0xFF00FF00.toInt()
private const val RED = 0xFFFF0000.toInt()

class FundPerformanceViewModelTest {

    private lateinit var viewModel: FundPerformanceViewModel
    private lateinit var nonLoadPerformanceItem: FundPerformance
    private lateinit var loadPerformanceItem: FundPerformance

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var mapperResources: QuoteTemplateResources

    private val quoteService: MobileQuoteRepo = mockQuoteService()

    private fun mockQuoteService(): MobileQuoteRepo =
        mock {
            on {
                loadMobileQuote("AAAAX", requireEarningsDate = false, extendedHours = true, requireExchangeCode = false)
            } doReturn
                Observable.just(MobileQuoteMapper().map(getMockMFQuotes()))
        }

    private fun getMockMFQuotes(): FundQuoteResponseDto {
        return this::class.getObjectFromJson("/responses/quotes_mutual_funds.json")
    }

    private val mockFundQuoteDetailItemsMapper = Mockito.mock(FundQuoteDetailItemsMapper::class.java)

    @Before
    fun setUp() {
        mapperResources = getQuoteResources()
        val defaultFundQuoteDetailItemsMapper = DefaultFundQuoteDetailItemsMapper(mapperResources)

        val quotes = MobileQuoteMapper().map(getMockMFQuotes())
        val result = defaultFundQuoteDetailItemsMapper.getFundCategoryPerformanceItem(quotes)
        nonLoadPerformanceItem = mapFundPerformanceNonAdjusted(result, "non-load")
        loadPerformanceItem = mapFundPerformanceAdjusted(result, "load")

        whenever(mockFundQuoteDetailItemsMapper.getFundCategoryPerformanceItem(quote = quotes)).thenReturn(result)

        viewModel = FundPerformanceViewModel(
            mock {
                whenever(it.getString(R.string.quote_performance_load_adjusted)).thenReturn("load")
                whenever(it.getString(R.string.quote_performance_non_load_adjusted)).thenReturn("non-load")
            },
            isExtendedHoursOnUseCase = mockIsUseExtendedHoursOn(),
            fundQuoteDetailItemsMapper = mockFundQuoteDetailItemsMapper,
            quoteService = quoteService, symbol
        )
    }

    @Test
    fun `Get Mobile Quote Info on success response table index NON_ADJUSTED`() {
        val mockObserver = mock<Observer<FundPerformance>>()
        viewModel.fundPerformance.observeForever(mockObserver)
        viewModel.getMobileQuote(symbol)

        assertEquals(viewModel.fundPerformance.value, nonLoadPerformanceItem)
    }

    @Test
    fun `Get Mobile Quote Info on success response for table index as ADJUSTED`() {
        val mockObserver = mock<Observer<FundPerformance>>()
        viewModel.fundPerformance.observeForever(mockObserver)
        viewModel.tableViewIndex = 1

        viewModel.getMobileQuote(symbol)

        assertEquals(viewModel.fundPerformance.value, loadPerformanceItem)
    }

    private fun getQuoteResources(): QuoteTemplateResources = QuoteTemplateResources(
        valueMissing = "--",
        titleFormat = "%1\$s - %2\$s",
        asOfDateTimeFormat = "As of %1\$s",
        nasdaqAsOfDateTimeFormat = "NASDAQ as of %1\$s",
        positiveChangeColorRes = GREEN,
        negativeChangeColorRes = RED,
        neutralChangeColorRes = 0,
        dateFormat = "%1s/%2s/%3s",
        peFormat = "%.1fx",
        changeFormat = "%s (/%2s$%%)",
        closingPrice = "Closing Price",
        underlyerPrice = "Underlyer Price",
        underlyerClosingPrice = "Underlyer Closing Price",
        fundClassTitle = "Class",
        fundDescriptionFormat = "Category: %1\$s\\n\\n%2\$s",
        fundDescriptionNotFound = "Fund description not found",
        fundPerformanceCategoryFormat = "Morningstar %1\$s",
        priceFormat = "price is %1s",
        sizeFormat = "size is %1s",
        contentDescriptionFormat = "Symbol %1s, %2s, Change %3s, Volume %4s, %5s"
    )
}
