package com.etrade.mobilepro.accountvalues.pnl.calculator

import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult.CalculatedAccountValuesVO
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult.EmptyCalculationResult
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.positions.dao.PositionRealmObject
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.spy
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class AccountPnlValuesCalculatorImplTest {

    @Test
    fun `test calculateAccountValues method`() {
        data class TestParams(
            override val testDescription: String,
            val intermediateValues: IntermediateAccountValues,
            val cashValue: BigDecimal,
            override val expectedResult: CalculatedAccountValuesVO
        ) : CommonTestParams<CalculatedAccountValuesVO>

        val calculator = spy(AccountPnlValuesCalculatorImpl())
        listOf(
            TestParams(
                testDescription = "All intermediate values are null",
                intermediateValues = IntermediateAccountValues(null, null, null, null, null),
                cashValue = BigDecimal.ZERO,
                expectedResult = CalculatedAccountValuesVO(marketValue = BigDecimal.ZERO, netLiquidationValue = BigDecimal.ZERO)
            ),
            TestParams(
                testDescription = "marketValue = intermediate marketValueSum, netLiquidationValue = intermediate marketValueSum",
                intermediateValues = IntermediateAccountValues(null, null, BigDecimal.TEN, null, null),
                cashValue = BigDecimal.ZERO,
                expectedResult = CalculatedAccountValuesVO(marketValue = BigDecimal.TEN, netLiquidationValue = BigDecimal.TEN)
            ),
            TestParams(
                testDescription = "netLiquidationValue = cashValue",
                intermediateValues = IntermediateAccountValues(null, null, null, null, null),
                cashValue = BigDecimal.TEN,
                expectedResult = CalculatedAccountValuesVO(marketValue = BigDecimal.ZERO, netLiquidationValue = BigDecimal.TEN)
            ),
            TestParams(
                testDescription = "netLiquidationValue = intermediate marketValueSum + cashValue",
                intermediateValues = IntermediateAccountValues(null, null, BigDecimal(2), null, null),
                cashValue = BigDecimal(3),
                expectedResult = CalculatedAccountValuesVO(marketValue = BigDecimal(2), netLiquidationValue = BigDecimal(5))
            ),
            TestParams(
                testDescription = "Days Gain = intermediate daysGainSum",
                intermediateValues = IntermediateAccountValues(null, BigDecimal.TEN, BigDecimal.TEN, null, null),
                cashValue = BigDecimal.ONE,
                expectedResult = CalculatedAccountValuesVO(
                    marketValue = BigDecimal.TEN,
                    netLiquidationValue = BigDecimal("11"),
                    plDay = BigDecimal.TEN,
                    plDayPercent = BigDecimal("1000.0000")
                )
            ),
            TestParams(
                testDescription = "daysGain Percentage is null due to dividing 0",
                intermediateValues = IntermediateAccountValues(null, BigDecimal.TEN, BigDecimal.TEN, null, null),
                cashValue = BigDecimal.ZERO,
                expectedResult = CalculatedAccountValuesVO(marketValue = BigDecimal.TEN, netLiquidationValue = BigDecimal.TEN, plDay = BigDecimal.TEN)
            ),
            TestParams(
                testDescription = "days gain 100% if market value is zero",
                intermediateValues = IntermediateAccountValues(null, BigDecimal.TEN, null, null, null),
                cashValue = BigDecimal.ZERO,
                expectedResult = CalculatedAccountValuesVO(
                    marketValue = BigDecimal.ZERO,
                    netLiquidationValue = BigDecimal.ZERO,
                    plDay = BigDecimal.TEN,
                    plDayPercent = BigDecimal("100.0000")
                )
            ),
            TestParams(
                testDescription = "totalCost = intermediate marketValueSum",
                intermediateValues = IntermediateAccountValues(null, null, null, null, BigDecimal.TEN),
                cashValue = BigDecimal.ZERO,
                expectedResult = CalculatedAccountValuesVO(marketValue = BigDecimal.ZERO, netLiquidationValue = BigDecimal.ZERO, totalCost = BigDecimal.TEN)
            ),
            TestParams(
                testDescription = "Total Gain = intermediate totalGainSum",
                intermediateValues = IntermediateAccountValues(null, null, null, BigDecimal(2), BigDecimal.TEN),
                cashValue = BigDecimal.ZERO,
                expectedResult = CalculatedAccountValuesVO(
                    marketValue = BigDecimal.ZERO,
                    netLiquidationValue = BigDecimal.ZERO,
                    totalCost = BigDecimal.TEN,
                    totalGain = BigDecimal(2),
                    totalGainPercent = BigDecimal("20.0000")
                )
            ),
            TestParams(
                testDescription = "If Total Gain Percentage is null then Total Gain is also null",
                intermediateValues = IntermediateAccountValues(null, null, null, BigDecimal(2), null),
                cashValue = BigDecimal.ZERO,
                expectedResult = CalculatedAccountValuesVO(marketValue = BigDecimal.ZERO, netLiquidationValue = BigDecimal.ZERO)
            ),
            TestParams(
                testDescription = "Negative cash test",
                intermediateValues = IntermediateAccountValues(null, BigDecimal.TEN, BigDecimal.TEN, null, null),
                cashValue = BigDecimal("-100"),
                expectedResult = CalculatedAccountValuesVO(
                    marketValue = BigDecimal.TEN,
                    netLiquidationValue = BigDecimal("-90"),
                    plDay = BigDecimal.TEN,
                    plDayPercent = BigDecimal("10.0000")
                )
            ),
            TestParams(
                testDescription = "Zero dollars market value 100% gain test",
                intermediateValues = IntermediateAccountValues(null, BigDecimal.TEN, BigDecimal.TEN, null, null),
                cashValue = BigDecimal("-10"),
                expectedResult = CalculatedAccountValuesVO(
                    marketValue = BigDecimal.TEN,
                    netLiquidationValue = BigDecimal.ZERO,
                    plDay = BigDecimal.TEN,
                    plDayPercent = BigDecimal("100.0000")
                )
            ),
            TestParams(
                testDescription = "null day's gain percentage due to negative cash",
                intermediateValues = IntermediateAccountValues(null, BigDecimal.TEN, BigDecimal("20"), null, null),
                cashValue = BigDecimal("-10"),
                expectedResult = CalculatedAccountValuesVO(
                    marketValue = BigDecimal("20"),
                    netLiquidationValue = BigDecimal.TEN,
                    plDay = BigDecimal.TEN
                )
            )
        ).forEachIndexed { index, params ->
            doReturn(params.intermediateValues).whenever(calculator).calculateIntermediateAccountValues(any(), any())
            doTesting(index, params) {
                calculator.calculateAccountValues(emptyMap(), emptyList(), params.cashValue)
            }
        }
    }

    @Test
    fun `test calculateIntermediateAccountValues method`() {
        data class TestParams(
            override val testDescription: String,
            val totalCost: BigDecimal?,
            val positions: List<Position>,
            override val expectedResult: IntermediateAccountValues
        ) : CommonTestParams<IntermediateAccountValues>

        val calculator = spy(AccountPnlValuesCalculatorImpl())
        listOf(
            TestParams(
                testDescription = "Position list is empty",
                totalCost = null,
                positions = emptyList(),
                expectedResult = IntermediateAccountValues(null, null, null, null, null)
            ),
            TestParams(
                testDescription = "Days Gain result is not zero nor null",
                totalCost = null,
                positions = listOf(PositionRealmObject(daysGainValue = "2"), PositionRealmObject(daysGainValue = "3")),
                expectedResult = IntermediateAccountValues(null, BigDecimal("5"), BigDecimal.ZERO, null, null)
            ),
            TestParams(
                testDescription = "Test that if daysGainValueSum is null then calculated marketValueSum is also null",
                totalCost = null,
                positions = listOf(
                    PositionRealmObject(markToMarket = "5", quantityRaw = "2"),
                    PositionRealmObject(markToMarket = "5", quantityRaw = "1")
                ),
                expectedResult = IntermediateAccountValues(null, null, null, null, null)
            ),
            TestParams(
                testDescription = "Test that calculated marketValueSum values is correct",
                totalCost = null,
                positions = listOf(
                    PositionRealmObject(daysGainValue = "0", markToMarket = "5", quantityRaw = "2"),
                    PositionRealmObject(daysGainValue = "0", markToMarket = "5", quantityRaw = "1")
                ),
                expectedResult = IntermediateAccountValues(null, BigDecimal.ZERO, BigDecimal(15), null, null)
            ),
            TestParams(
                testDescription = "Test that if totalCost is null then totalCost is also null",
                totalCost = null,
                positions = listOf(
                    PositionRealmObject(totalGainValue = "5"),
                    PositionRealmObject(totalGainValue = "3")
                ),
                expectedResult = IntermediateAccountValues(null, null, null, null, null)
            ),
            TestParams(
                testDescription = "Test that calculated totalGain values is correct",
                totalCost = BigDecimal.TEN,
                positions = listOf(
                    PositionRealmObject(totalGainValue = "5"),
                    PositionRealmObject(totalGainValue = "3")
                ),
                expectedResult = IntermediateAccountValues(null, null, null, BigDecimal(8), BigDecimal(20))
            )
        ).forEachIndexed { index, params ->
            doReturn(params.totalCost).whenever(calculator).getTotalCost(anyOrNull(), any())
            doTesting(index, params) {
                calculator.calculateIntermediateAccountValues(emptyMap(), params.positions)
            }
        }
    }

    @Test
    fun `test calculateAllBrokerageAccountValues method`() {
        data class TestParams(
            override val testDescription: String,
            val intermediateValues: IntermediateAccountValues?,
            override val expectedResult: CalculationResult
        ) : CommonTestParams<CalculationResult>

        val calculator = spy(AccountPnlValuesCalculatorImpl())
        listOf(
            TestParams(
                testDescription = "All intermediate values are null",
                intermediateValues = IntermediateAccountValues(null, null, null, null, null),
                expectedResult = CalculatedAccountValuesVO()
            ),
            TestParams(
                testDescription = "Intermediate values are null",
                intermediateValues = null,
                expectedResult = EmptyCalculationResult
            ),
            TestParams(
                testDescription = "netLiquidationValue = intermediate accountValueSum",
                intermediateValues = IntermediateAccountValues(BigDecimal.TEN, null, null, null, null),
                expectedResult = CalculatedAccountValuesVO(netLiquidationValue = BigDecimal.TEN)
            ),
            TestParams(
                testDescription = "Days Gain = intermediate daysGainSum",
                intermediateValues = IntermediateAccountValues(null, BigDecimal(3), BigDecimal(5), null, null),
                expectedResult = CalculatedAccountValuesVO(plDay = BigDecimal(3), plDayPercent = BigDecimal("150.0000"))
            ),
            TestParams(
                testDescription = "Total Gain = intermediate totalGainSum",
                intermediateValues = IntermediateAccountValues(null, null, null, BigDecimal(3), BigDecimal(2)),
                expectedResult = CalculatedAccountValuesVO(totalGain = BigDecimal(3), totalGainPercent = BigDecimal("150.0000"))
            ),
            TestParams(
                testDescription = "If totalGain Percentage is null then totalGain is also null",
                intermediateValues = IntermediateAccountValues(null, null, null, BigDecimal.TEN, null),
                expectedResult = CalculatedAccountValuesVO()
            )
        ).forEachIndexed { index, params ->
            doReturn(params.intermediateValues).whenever(calculator).calculateAllBrokerageIntermediateAccountValues(any(), any())
            doTesting(index, params) {
                calculator.calculateAllBrokerageAccountValues(emptyList(), emptyMap())
            }
        }
    }

    @Test
    fun `test calculateAllBrokerageIntermediateAccountValues method`() {
        data class TestParams(
            override val testDescription: String,
            val accountIds: List<String>,
            val lastCalculatedValues: Pair<CalculatedAccountValues?, Array<CalculatedAccountValues?>>,
            override val expectedResult: IntermediateAccountValues?
        ) : CommonTestParams<IntermediateAccountValues?>

        val mockedLastCalculatedValues: Map<String, CalculatedAccountValues> = mock()
        val calculator = AccountPnlValuesCalculatorImpl()
        listOf(
            TestParams(
                testDescription = "Account ids list is empty",
                accountIds = emptyList(),
                lastCalculatedValues = null to emptyArray(),
                expectedResult = IntermediateAccountValues(null, null, null, null, null)
            ),
            TestParams(
                testDescription = "CalculatedAccountValues for selected account id is null",
                accountIds = listOf("A"),
                lastCalculatedValues = null to emptyArray(),
                expectedResult = null
            ),
            TestParams(
                testDescription = "If all values in CalculatedAccountValuesVO are null then all values in the result are null",
                accountIds = listOf("A"),
                lastCalculatedValues = CalculatedAccountValues(CalculatedAccountValuesVO(), 0L) to emptyArray(),
                expectedResult = IntermediateAccountValues(null, null, null, null, null)
            ),
            TestParams(
                testDescription = "Test accountValueSum is calculated correct",
                accountIds = listOf("A", "B"),
                lastCalculatedValues = CalculatedAccountValues(CalculatedAccountValuesVO(netLiquidationValue = BigDecimal.TEN), 0L) to
                    arrayOf<CalculatedAccountValues?>(
                        CalculatedAccountValues(CalculatedAccountValuesVO(netLiquidationValue = BigDecimal.TEN), 0L)
                    ),
                expectedResult = IntermediateAccountValues(BigDecimal(20), null, null, null, null)
            ),
            TestParams(
                testDescription = "Test daysGainSum and marketValueSum are calculated correct",
                accountIds = listOf("A", "B"),
                lastCalculatedValues = CalculatedAccountValues(CalculatedAccountValuesVO(marketValue = BigDecimal(2), plDay = BigDecimal(4)), 0L) to
                    arrayOf<CalculatedAccountValues?>(
                        CalculatedAccountValues(CalculatedAccountValuesVO(marketValue = BigDecimal(3), plDay = BigDecimal(5)), 0L)
                    ),
                expectedResult = IntermediateAccountValues(null, BigDecimal(9), BigDecimal(5), null, null)
            ),
            TestParams(
                testDescription = "Test totalGainSum and totalCostSum are calculated correct",
                accountIds = listOf("A", "B"),
                lastCalculatedValues = CalculatedAccountValues(CalculatedAccountValuesVO(totalCost = BigDecimal(2), totalGain = BigDecimal(4)), 0L) to
                    arrayOf<CalculatedAccountValues?>(
                        CalculatedAccountValues(CalculatedAccountValuesVO(totalCost = BigDecimal(3), totalGain = BigDecimal(5)), 0L)
                    ),
                expectedResult = IntermediateAccountValues(null, null, null, BigDecimal(9), BigDecimal(5))
            )
        ).forEachIndexed { index, params ->
            val lastCalcValues = params.lastCalculatedValues
            doReturn(lastCalcValues.first, *lastCalcValues.second).whenever(mockedLastCalculatedValues)[any()]
            doTesting(index, params) {
                calculator.calculateAllBrokerageIntermediateAccountValues(params.accountIds, mockedLastCalculatedValues)
            }
        }
    }

    @Test
    fun `test calculateCompleteViewSummaryAccountValues method`() {
        data class TestParams(
            override val testDescription: String,
            val accounts: List<Account>,
            val intermediateValues: Pair<IntermediateCompleteViewAccountValues?, Array<IntermediateCompleteViewAccountValues?>>,
            override val expectedResult: CalculationResult
        ) : CommonTestParams<CalculationResult>

        val calculator = spy(AccountPnlValuesCalculatorImpl())
        val mockedAccount = Account()
        listOf(
            TestParams(
                testDescription = "Account list is empty",
                accounts = emptyList(),
                intermediateValues = null to emptyArray(),
                expectedResult = CalculatedAccountValuesVO()
            ),
            TestParams(
                testDescription = "Account list is not empty but intermediate values for any account are null",
                accounts = listOf(mockedAccount, mockedAccount),
                intermediateValues = null to arrayOf(
                    IntermediateCompleteViewAccountValues(BigDecimal.TEN, BigDecimal.ONE),
                    null
                ),
                expectedResult = EmptyCalculationResult
            ),
            TestParams(
                testDescription = "Intermediate values are null",
                accounts = listOf(mockedAccount),
                intermediateValues = IntermediateCompleteViewAccountValues(null, null) to emptyArray(),
                expectedResult = CalculatedAccountValuesVO()
            ),
            TestParams(
                testDescription = "If intermediate nav is not null then result netAssets is not null",
                accounts = listOf(mockedAccount),
                intermediateValues = IntermediateCompleteViewAccountValues(BigDecimal.TEN, null) to emptyArray(),
                expectedResult = CalculatedAccountValuesVO(netAssets = BigDecimal.TEN)
            ),
            TestParams(
                testDescription = "If intermediate daysGain is not null then result daysGain is not null",
                accounts = listOf(mockedAccount),
                intermediateValues = IntermediateCompleteViewAccountValues(null, BigDecimal.TEN) to emptyArray(),
                expectedResult = CalculatedAccountValuesVO(plDay = BigDecimal.TEN)
            ),
            TestParams(
                testDescription = "Positive test",
                accounts = listOf(mockedAccount, mockedAccount, mockedAccount),
                intermediateValues = IntermediateCompleteViewAccountValues(BigDecimal(2), BigDecimal(3)) to arrayOf<IntermediateCompleteViewAccountValues?>(
                    IntermediateCompleteViewAccountValues(BigDecimal(2), BigDecimal(3)),
                    IntermediateCompleteViewAccountValues(BigDecimal(2), BigDecimal(3))
                ),
                expectedResult = CalculatedAccountValuesVO(netAssets = BigDecimal(6), plDay = BigDecimal(9))
            )
        ).forEachIndexed { index, params ->
            doReturn(params.intermediateValues.first, *params.intermediateValues.second)
                .whenever(calculator).calculateCompleteViewValuesForProvidedAccount(any(), any())
            doTesting(index, params) {
                calculator.calculateCompleteViewSummaryAccountValues(params.accounts, emptyMap())
            }
        }
    }

    @Test
    fun `test calculateCompleteViewValuesForProvidedAccount method`() {
        data class TestParams(
            override val testDescription: String,
            val account: Account,
            val lastCalculatedValues: CalculatedAccountValuesVO?,
            override val expectedResult: IntermediateCompleteViewAccountValues?
        ) : CommonTestParams<IntermediateCompleteViewAccountValues?>

        val mockedLastCalculatedValues: Map<String, CalculatedAccountValuesVO> = mock()
        val calculator = AccountPnlValuesCalculatorImpl()
        listOf(
            TestParams(
                testDescription = "Default AccountType",
                account = Account(),
                lastCalculatedValues = null,
                expectedResult = IntermediateCompleteViewAccountValues(null, null)
            ),
            TestParams(
                testDescription = "AccountType.Bank accountType",
                account = Account(_accountType = AccountType.Bank.name, availableBalance = "5"),
                lastCalculatedValues = null,
                expectedResult = IntermediateCompleteViewAccountValues(BigDecimal(5), null)
            ),
            TestParams(
                testDescription = "AccountType.ESP accountType",
                account = Account(_accountType = AccountType.ESP.name, accountValue = "5", daysGain = "3"),
                lastCalculatedValues = null,
                expectedResult = IntermediateCompleteViewAccountValues(BigDecimal(5), BigDecimal(3))
            ),
            TestParams(
                testDescription = "AccountType.Managed accountType",
                account = Account(_accountType = AccountType.Managed.name, accountValue = "5", daysGain = "3"),
                lastCalculatedValues = null,
                expectedResult = IntermediateCompleteViewAccountValues(BigDecimal(5), BigDecimal(3))
            ),
            TestParams(
                testDescription = "AccountType.Brokerage accountType - lastCalculatedValues are null",
                account = Account(_accountType = AccountType.Brokerage.name),
                lastCalculatedValues = null,
                expectedResult = null
            ),
            TestParams(
                testDescription = "AccountType.Brokerage accountType - lastCalculatedValues are null, but accountValue is not null",
                account = Account(_accountType = AccountType.Brokerage.name, accountValue = "4"),
                lastCalculatedValues = null,
                expectedResult = IntermediateCompleteViewAccountValues(BigDecimal(4), BigDecimal(0))
            ),
            TestParams(
                testDescription = "AccountType.Brokerage accountType - lastCalculatedValues are not null",
                account = Account(_accountType = AccountType.Brokerage.name, accountValue = "5", daysGain = "3"),
                lastCalculatedValues = CalculatedAccountValuesVO(netLiquidationValue = BigDecimal(7), plDay = BigDecimal(5)),
                expectedResult = IntermediateCompleteViewAccountValues(BigDecimal(7), BigDecimal(5))
            )
        ).forEachIndexed { index, params ->
            doReturn(params.lastCalculatedValues).whenever(mockedLastCalculatedValues)[anyOrNull()]
            doTesting(index, params) {
                calculator.calculateCompleteViewValuesForProvidedAccount(params.account, mockedLastCalculatedValues)
            }
        }
    }

    private interface CommonTestParams<Result> {
        val testDescription: String
        val expectedResult: Result
    }

    private fun <Result, Params : CommonTestParams<Result>> doTesting(testIndex: Int, params: Params, getResult: () -> Result) {
        val result = getResult()
        println("Test $testIndex: ${params.testDescription} \n   testResult     = $result,\n   expectedResult = ${params.expectedResult}")
        assert(result == params.expectedResult)
    }
}
