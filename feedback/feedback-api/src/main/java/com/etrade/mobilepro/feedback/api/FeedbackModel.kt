package com.etrade.mobilepro.feedback.api

class FeedbackModel(
    val stars: Int,
    val feedbackText: String,
    val page: String
) {
    val source: String
        get() =
            if (page == FeedbackConstants.menu) {
                FeedbackConstants.menu
            } else {
                FeedbackConstants.contextual
            }
}

object FeedbackConstants {
    const val menu = "Menu"
    const val contextual = "Contextual"

    const val savedRatingKey = "savedRatingKey"
    const val savedTextKey = "savedTextKey"
}
