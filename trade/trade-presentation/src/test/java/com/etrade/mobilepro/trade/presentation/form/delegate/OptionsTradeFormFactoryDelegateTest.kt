package com.etrade.mobilepro.trade.presentation.form.delegate

import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.dynamic.form.api.inputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.data.form.input.DefaultOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormType
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.O_MR1])
@RunWith(AndroidJUnit4::class)
class OptionsTradeFormFactoryDelegateTest {

    private val tradeFormType = TradeFormType(SecurityType.OPTION)

    private val sut: TradeFormFactoryDelegate? = mockDelegates.getFormDelegate(tradeFormType)

    private val form = sut?.createForm(
        mockTradeAccounts,
        mockSelectedAccountDelegate("31459185"),
        mockOptionsTradeParameter,
        object : TradeFormFactoryDelegate.OnFormChangeListener {
            override fun onSecurityType(securityType: SecurityType) =
                Unit

            override fun onAdvancedOrderType(
                formType: TradeFormType,
                snapshot: InputValuesSnapshot
            ) = Unit
        }
    )

    @Test
    fun `check OptionTradeFormFactoryDelegate is configured for Option trade type`() {
        assertTrue(mockDelegates.getFormDelegate(tradeFormType) is OptionTradeFormFactoryDelegate)
    }

    @Test
    fun `OptionTradeFormFactoryDelegate creates form successfully`() {
        assertNotNull(form)

        // Top level sections : Account, Option form, Advanced order
        assertTrue(form?.groups?.size == VERTICAL_GROUP_SECTIONS)
    }

    @Test
    // Buttons : Preview, Save
    fun `Option Trade Form check if button state is valid`() {
        assertValidActionButtons(form?.actions)
    }

    @Test
    fun `Option Trade Form mandatory view components`() {
        val formIds = form?.inputValuesSnapshot?.keys ?: emptySet()
        DefaultOptionTradeFormInputId.run {
            assertTrue(formIds.contains(accountId))
            assertTrue(formIds.contains(securityType))
            assertTrue(formIds.contains(accountId))
        }
    }

    @Test
    fun `check Option form content for injected user input`() {
        assertNotNull(sut is OptionTradeFormFactoryDelegate)
        val testOptionsInput = with(DefaultOptionTradeFormInputId) {
            mapOf(
                securityType to securityTypeValueId.option,
                accountId to "31459185",
                orderId to "275",
                lookupSymbol to "FB:EQ",
                strategy to "3",
                tradeLegs to encodedTradeLegs,
                priceType to priceTypeValueId.limit,
                limitPrice to "0.45",
                term to termValueId.goodForDay,
                allOrNone to "0"
            )
        }

        form?.apply {
            resetValues()
            injectValuesFrom(
                testOptionsInput
            )
        }

        assertFormValues(testOptionsInput, form)
    }
}
