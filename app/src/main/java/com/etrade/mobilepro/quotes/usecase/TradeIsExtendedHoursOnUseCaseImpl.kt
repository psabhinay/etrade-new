package com.etrade.mobilepro.quotes.usecase

import com.etrade.mobilepro.marketschedule.MarketScheduleProviderHolder
import com.etrade.mobilepro.quoteapi.usecase.TradeIsExtendedHoursOnUseCase
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.market.time.isPreMarketTime
import javax.inject.Inject

class TradeIsExtendedHoursOnUseCaseImpl @Inject constructor(
    private val marketScheduleProviderHolder: MarketScheduleProviderHolder,
    private val user: User
) : TradeIsExtendedHoursOnUseCase {

    override suspend fun execute(parameter: Unit): Boolean {
        return marketScheduleProviderHolder.isExtendedHourEligiblePeriod || !user.isRealtimeUser && isPreMarketTime()
    }
}
