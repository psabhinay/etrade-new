package com.etrade.mobilepro.orders.list.mapper

import androidx.paging.PagingSource
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.list.api.OrderItemsAndKey
import com.etrade.mobilepro.orders.list.api.OrderParams
import com.etrade.mobilepro.orders.list.api.OrderParamsLoadSizeAndKey
import com.etrade.mobilepro.orders.list.api.OrdersRepo
import com.etrade.mobilepro.orders.list.paging.ORDERS_CACHE_EXPIRATION
import com.etrade.mobilepro.orders.list.paging.OrderListPagingSource
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.threeten.bp.LocalDate

@ExperimentalCoroutinesApi
internal class OrdersDataSourceTest {

    private lateinit var sut: OrderListPagingSource

    @Mock
    private lateinit var cachedOrdersRepo: OrdersRepo

    @BeforeEach
    fun setup() {
        MockitoAnnotations.openMocks(this)

        runBlockingTest {
            whenever(cachedOrdersRepo.getOrders(anyOrNull(), anyOrNull())).thenReturn(
                OrderItemsAndKey(
                    listOf(),
                    "",
                    false
                )
            )
        }
    }

    @Test
    fun `test open orders load initial is expected`() {
        val orderParams = OrderParams(
            "",
            LocalDate.of(2019, 1, 1),
            LocalDate.of(2019, 7, 3),
            orderStatus = OrderStatus.OPEN,
            isStockPlan = false,
            accountIndex = null
        )
        val orderParamsLoadSizeAndKey = OrderParamsLoadSizeAndKey(
            orderParams,
            20,
            null
        )

        sut = OrderListPagingSource(cachedOrdersRepo, orderParams)
        runBlockingTest {
            sut.load(PagingSource.LoadParams.Refresh(null, 20, false))
            verify(cachedOrdersRepo).getOrders(orderParamsLoadSizeAndKey, ORDERS_CACHE_EXPIRATION)
        }
    }

    @Test
    fun `test open orders load after is expected`() {
        val orderParams = OrderParams(
            "",
            LocalDate.of(2019, 1, 1),
            LocalDate.of(2019, 7, 3),
            orderStatus = OrderStatus.OPEN,
            isStockPlan = false,
            accountIndex = null
        )
        val orderParamsLoadSizeAndKey = OrderParamsLoadSizeAndKey(
            orderParams,
            20,
            "100"
        )

        sut = OrderListPagingSource(cachedOrdersRepo, orderParams)
        runBlockingTest {
            sut.load(PagingSource.LoadParams.Append("100", 20, false))
            verify(cachedOrdersRepo).getOrders(orderParamsLoadSizeAndKey, ORDERS_CACHE_EXPIRATION)
        }
    }

    @Test
    fun `test saved orders load initial is expected`() {
        val orderParams = OrderParams(
            "",
            LocalDate.of(2019, 1, 1),
            LocalDate.of(2019, 7, 3),
            orderStatus = OrderStatus.SAVED,
            isStockPlan = false,
            accountIndex = null
        )
        val orderParamsLoadSizeAndKey = OrderParamsLoadSizeAndKey(
            orderParams,
            20,
            null
        )

        sut = OrderListPagingSource(cachedOrdersRepo, orderParams)
        runBlockingTest {
            sut.load(PagingSource.LoadParams.Refresh(null, 20, false))
            verify(cachedOrdersRepo).getOrders(orderParamsLoadSizeAndKey, ORDERS_CACHE_EXPIRATION)
        }
    }

    @Test
    fun `test saved orders load after is expected`() {
        val orderParams = OrderParams(
            "",
            LocalDate.of(2019, 1, 1),
            LocalDate.of(2019, 7, 3),
            orderStatus = OrderStatus.SAVED,
            isStockPlan = false,
            accountIndex = null
        )
        val orderParamsLoadSizeAndKey = OrderParamsLoadSizeAndKey(
            orderParams,
            20,
            "100"
        )

        sut = OrderListPagingSource(cachedOrdersRepo, orderParams)
        runBlockingTest {
            sut.load(PagingSource.LoadParams.Append("100", 20, false))
            verify(cachedOrdersRepo).getOrders(orderParamsLoadSizeAndKey, ORDERS_CACHE_EXPIRATION)
        }
    }

    @Test
    fun `test stock plan orders load initial is expected`() {
        val orderParams = OrderParams(
            "",
            LocalDate.of(2019, 1, 1),
            LocalDate.of(2019, 7, 3),
            orderStatus = OrderStatus.OPEN,
            isStockPlan = true,
            accountIndex = "2"
        )

        val orderParamsLoadSizeAndKey = OrderParamsLoadSizeAndKey(
            orderParams,
            20,
            null
        )

        sut = OrderListPagingSource(cachedOrdersRepo, orderParams)
        runBlockingTest {
            sut.load(PagingSource.LoadParams.Refresh(null, 20, false))
            verify(cachedOrdersRepo).getOrders(orderParamsLoadSizeAndKey, ORDERS_CACHE_EXPIRATION)
        }
    }
}
