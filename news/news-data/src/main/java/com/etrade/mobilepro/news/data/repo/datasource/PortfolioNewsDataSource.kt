package com.etrade.mobilepro.news.data.repo.datasource

import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.data.dto.NewsDto
import com.etrade.mobilepro.news.data.rest.NewsApiClient
import io.reactivex.Single

internal class PortfolioNewsDataSource(private val newsApiClient: NewsApiClient, requestParams: NewsRequest?) :
    NewsPagingSource(requestParams) {

    override fun getNewsRange(startIndex: Int, size: Int, requestParams: NewsRequest?): Single<NewsDto> {
        return when (requestParams) {
            is NewsRequest.Portfolio -> newsApiClient.getPortfolioNews(requestParams.resource, startIndex, size, requestParams.includeStockPlan)
            is NewsRequest.Symbol -> newsApiClient.getSymbolNews(requestParams.joinSymbols(), startIndex, size)
            else -> throw IllegalArgumentException("Wrong request params type for portfolio news, got null")
        }
    }
}
