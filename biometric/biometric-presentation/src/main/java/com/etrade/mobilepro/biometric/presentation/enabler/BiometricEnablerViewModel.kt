package com.etrade.mobilepro.biometric.presentation.enabler

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.biometric.api.BiometricProviderError
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticationException
import com.etrade.mobilepro.biometric.presentation.R
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.encryption.api.SecretKeyRepo
import com.etrade.mobilepro.encryption.viewmodel.CipherAuthenticatorViewModel
import com.etrade.mobilepro.login.api.repo.LoginFailure
import com.etrade.mobilepro.login.api.repo.LoginPreferencesError
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.login.api.repo.LoginRepo
import com.etrade.mobilepro.login.api.repo.LoginResultCode
import com.etrade.mobilepro.login.api.repo.LoginSuccess
import com.etrade.mobilepro.login.usecase.extractPassword
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.BiometricAuthenticationAnalyticsParams
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.crypto.Cipher
import javax.inject.Inject

class BiometricEnablerViewModel @Inject constructor(
    private val cipherAuthenticatorViewModelDelegate: CipherAuthenticatorViewModel,
    private val loginRepo: LoginRepo,
    private val loginPreferences: LoginPreferencesRepo,
    private val secretKeyRepo: SecretKeyRepo,
    private val resources: Resources,
    private val user: User,
    private val tracker: Tracker
) : ViewModel(), CipherAuthenticatorViewModel by cipherAuthenticatorViewModelDelegate {

    val isUserRsa: Boolean
        get() = user.session?.isRSATokenUser ?: throw IllegalStateException("User session is not available")

    private val _passwordValidationState = MutableLiveData<ConsumableLiveEvent<PasswordValidationState>>()
    val passwordValidationState: LiveData<ConsumableLiveEvent<PasswordValidationState>>
        get() = _passwordValidationState

    private val logger: Logger = LoggerFactory.getLogger(BiometricEnablerViewModel::class.java)

    private var loginJob: Job? = null

    @Suppress("LongMethod")
    fun validatePassword(password: String, authenticate: (Cipher) -> Unit) {
        if (password.isEmpty()) {
            _passwordValidationState.value = PasswordValidationState.Failed(
                resources.getString(R.string.empty_password),
                BiometricEnablerError.BIOMETRIC_EMPTY_PASSWORD
            ).consumable()
            return
        }
        _passwordValidationState.value = ConsumableLiveEvent(PasswordValidationState.Loading)
        loginJob = viewModelScope.launch {
            val username = loginPreferences.getUserName().getOrThrow()
            val loginResult = loginRepo.login(username, password)
            loginResult
                .onSuccess {
                    _passwordValidationState.value = when (it) {
                        is LoginSuccess -> ConsumableLiveEvent(PasswordValidationState.Success)
                        is LoginFailure -> {
                            when (it.loginResultCode) {
                                LoginResultCode.INVALID_CREDENTIALS, LoginResultCode.WRONG_PASSWORD ->
                                    PasswordValidationState.Failed(
                                        resources.getString(R.string.password_verification_error),
                                        BiometricEnablerError.BIOMETRIC_INVALID_PASSWORD
                                    ).consumable()
                                else -> createGeneralFailedState(it.sessionResultMessage)
                            }
                        }
                    }

                    if (it is LoginSuccess) {
                        savePassword(password, it.isRsaUser, authenticate)
                    }
                }
                .onFailure {
                    _passwordValidationState.value = handleLoginPreferencesException(it)
                }
        }
    }

    fun cancelLoginRequest() {
        loginJob?.cancel()
    }

    private suspend fun savePassword(
        password: String,
        isRsaUser: Boolean,
        authenticate: (Cipher) -> Unit
    ) {
        secretKeyRepo.recreateSecretKey()
        loginPreferences.setUserPassword(extractPassword(password, isRsaUser)) { cipher ->
            cipherAuthenticatorViewModelDelegate.authenticate(cipher, authenticate)
        }.onSuccess {
            tracker.event(BiometricAuthenticationAnalyticsParams(true))
        }.onFailure {
            logger.error("Password saving has failed", it)
            _passwordValidationState.value = handleLoginPreferencesException(it)
        }
    }

    private fun createGeneralFailedState(customMessage: String? = null): ConsumableLiveEvent<PasswordValidationState.Failed> {
        return PasswordValidationState.Failed(
            customMessage ?: resources.getString(R.string.error_message_general),
            BiometricEnablerError.BIOMETRIC_LOGIN_SERVICE_ERROR
        ).consumable()
    }

    private fun handleLoginPreferencesException(exception: Throwable): ConsumableLiveEvent<PasswordValidationState> {
        if (exception is BiometricAuthenticationException) {
            // Error message already displayed by BiometricAuthenticationFragment, just cancel the flow
            return PasswordValidationState.Canceled.consumable()
        }
        return when (loginPreferences.checkException(exception)) {
            LoginPreferencesError.LOGIN_CANCELED -> PasswordValidationState.Canceled.consumable()
            LoginPreferencesError.NO_VALID_BIOMETRIC ->
                createGeneralFailedState(BiometricProviderError.ERROR_NO_BIOMETRICS.getLabelFrom(resources))
            LoginPreferencesError.SECRET_KEY_INVALIDATED,
            LoginPreferencesError.GENERIC_ERROR -> createGeneralFailedState()
        }
    }
}

sealed class PasswordValidationState {

    object Loading : PasswordValidationState()

    object Success : PasswordValidationState()

    object Canceled : PasswordValidationState()

    data class Failed(val message: String, val biometricEnablerError: BiometricEnablerError) : PasswordValidationState()
}
