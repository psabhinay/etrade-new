package com.etrade.mobilepro.holdingsapi

data class PositionHoldingsItem(
    val totalPositionsCount: String,
    val hasHoldings: Boolean
)
