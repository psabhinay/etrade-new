package com.etrade.mobilepro.dynamic.form.ui

import android.content.Context
import android.util.AttributeSet
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.api.DynamicText
import com.etrade.mobilepro.valuepicker.ValuePickerWidget

class DynamicTextView : ValuePickerWidget, ComponentView<DynamicText> {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val componentViewDelegate: ComponentViewDelegate = ComponentViewDelegate(this)

    private val valueObserver: Observer<CharSequence?> = Observer {
        value = it
        contentDescription = "${dynamicText?.title ?: ""} ${it ?: ""}"
    }

    init {
        isEnabled = true
    }

    private var dynamicText: DynamicText? = null

    override fun render(component: DynamicText, owner: LifecycleOwner) {
        componentViewDelegate.render(component, owner)

        dynamicText?.displayValue?.removeObserver(valueObserver)

        dynamicText = component

        component.displayValue.observe(owner, valueObserver)

        hint = component.title
    }
}
