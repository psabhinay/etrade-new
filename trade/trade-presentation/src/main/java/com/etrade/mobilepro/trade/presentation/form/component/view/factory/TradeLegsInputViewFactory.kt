package com.etrade.mobilepro.trade.presentation.form.component.view.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.ui.factory.ComponentViewFactory
import com.etrade.mobilepro.trade.presentation.form.component.TradeLegsInput
import com.etrade.mobilepro.trade.presentation.form.component.view.TradeLegsInputView

internal class TradeLegsInputViewFactory : ComponentViewFactory<TradeLegsInput, TradeLegsInputView> {
    override fun createView(context: Context): TradeLegsInputView = TradeLegsInputView(context)
}
