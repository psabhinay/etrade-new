package com.etrade.mobilepro.backends.mgs

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PaginationMeta(
    @Json(name = "type")
    override val type: String?,
    @Json(name = "nextPage")
    val nextPage: NextPage
) : BaseMeta()

@JsonClass(generateAdapter = true)
class NextPage(
    @Json(name = "continuationToken")
    val continuationToken: String?,
    @Json(name = "hasNext")
    val hasNext: Boolean
)
