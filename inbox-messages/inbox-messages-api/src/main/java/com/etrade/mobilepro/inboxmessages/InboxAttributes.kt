package com.etrade.mobilepro.inboxmessages

object InboxAttributes {
    const val ASSET_HEIGHT = "asset_height"
    const val SHOW_BANNER = "showBanner"
    const val CALL_TO_ACTION = "call_to_action"
    const val DEEP_LINK = "deep_link"
    const val MESSAGE_TYPE = "message_type"
    const val PLACE = "place"
    const val SUBTITLE = "subtitle"
    const val TITLE = "title"
    const val USER_TYPE = "user_type"
    const val THEME = "theme"
    const val BACKGROUND_COLOR = "background_color"
    const val ICON = "Icon"
    const val ARROW = "Arrow"
    const val HEADLINE = "Headline"
    const val BODY_TEXT = "Body Text"
    const val SYMBOL = "Symbol"
}
