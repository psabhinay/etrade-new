package com.etrade.mobilepro.bloombergtv.presentation.pip

import android.content.Context
import android.content.Intent
import android.view.Gravity
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.view.isVisible
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvActivity
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvService
import com.etrade.mobilepro.bloombergtv.presentation.databinding.VideoControllerPipBinding
import com.etrade.mobilepro.bloombergtv.presentation.delegate.pip.KeyEventDelegate
import com.etrade.mobilepro.util.android.extension.lifecycleScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

private const val DEFAULT_TIMEOUT_MS = 1000L

@Suppress("ViewConstructor") // it is created only manually
internal class PipVideoControllerView private constructor(
    view: ViewGroup,
    private val scope: CoroutineScope?,
    startPlay: () -> Unit,
    isPlaying: () -> Boolean
) : FrameLayout(view.context) {
    companion object {
        fun createAndAttachTo(view: ViewGroup, startPlay: () -> Unit, isPlaying: () -> Boolean) {
            PipVideoControllerView(view, view.lifecycleScope, startPlay, isPlaying)
        }
    }

    private val binding: VideoControllerPipBinding =
        VideoControllerPipBinding.inflate(context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater)
    private val keyEventDelegate = KeyEventDelegate(::show, ::hide, startPlay, isPlaying, fallback = { event -> super.dispatchKeyEvent(event) })
    private var hideJob: Job? = null

    init {
        view.addView(binding.root, LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.TOP))
        binding.videoControllerPictureInPicture.setOnClickListener {
            val intent = Intent(context, BloombergTvActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
            context.stopService(Intent(context, BloombergTvService::class.java))
        }
        binding.videoControllerPictureInPictureClose.setOnClickListener {
            context.stopService(Intent(context, BloombergTvService::class.java))
        }
        binding.root.setOnClickListener { show() }
    }

    fun show() {
        binding.root.isVisible = true
        hideJob?.cancel()
        hideJob = scope?.launch {
            delay(DEFAULT_TIMEOUT_MS)
            hide()
        }
    }

    private fun hide() {
        binding.root.isVisible = false
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean = keyEventDelegate.dispatchKeyEvent(event)
}
