package com.etrade.mobilepro.trade.presentation.form.delegate.conditional

import android.content.Context
import android.content.res.Resources
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.FormBuilder
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.form
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.underlier
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.trade.api.TradeOptionInfoType
import com.etrade.mobilepro.trade.dropdown.form.tradeSelect
import com.etrade.mobilepro.trade.dropdown.help.TradeHelpParams
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.input.CONDITIONAL_ORDER_SECURITY_TYPE_ID
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.SymbolInputParams
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.allOrNoneSwitch
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.conditionalOrderActions
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.conditionalOrderFooter
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.conditionalOrderSymbol
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.observeQuantityChange
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.observeSymbolChange
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.optionsBidAskWidget
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.price
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.quantity
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.requiredCommonFormRules
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.term
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.underlierQuoteWidget
import com.etrade.mobilepro.trade.presentation.usecase.CreateConditionalTradeFormParameter
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences

internal class ConditionalOptionTradeFormFactoryDelegate(
    private val context: Context,
    private val inputId: ConditionalOrderTradeFormInputId,
    private val dependencies: FormFactoryDelegateDependencies,
    private val searchIntentFactory: SearchIntentFactory,
    private val tradingDefaultsPreferences: TradingDefaultsPreferences
) : ConditionalFormFactoryDelegate {

    private val resources = context.resources

    override fun createForm(
        parameter: CreateConditionalTradeFormParameter,
        symbolChangedListener: ConditionalFormFactoryDelegate.SymbolChangedListener,
        initialSymbol: WithSymbolInfo?
    ): Form {

        return form {
            conditionalOptionOrderHiddenInputs(parameter)

            verticalGroup {
                val symbolInputParams = SymbolInputParams(InstrumentType.validStocksAndOptionTradingTypes, inputId.symbol, inputId.symbolCoder)
                conditionalOrderSymbol(context, searchIntentFactory, inputId, parameter, symbolInputParams, initialSymbol)
                underlierQuoteWidget(inputId.symbol, parameter.quoteWidgetViewModel)
                optionsBidAskWidget(inputId.symbol, parameter.optionsQuoteBidAskViewModel)
                orderTypeAndQuantity()
                conditionalOptionOrderPriceType()
                term(resources, inputId, dependencies.limitOrderTermLabelMap, dependencies.allowShortSellUpdatedTerms, tradingDefaultsPreferences.optionsTerm)
                allOrNoneSwitch(resources, inputId, dependencies.allOrNoneValidPriceTypes)
                conditionalOrderFooter(resources, parameter.onActionClickListener)
            }

            rules {
                observeSymbolChange(inputId.symbol, symbolChangedListener, SecurityType.OPTION, initialSymbol)
                setHiddenSymbol()
                requiredCommonFormRules(inputId)
                observeQuantityChange(resources, parameter.onActionClickListener, inputId)
            }

            actions(parameter.onActionClickListener) {
                conditionalOrderActions(context, dependencies, parameter.orderNumber)
            }
        }
    }

    private fun FormBuilder.conditionalOptionOrderHiddenInputs(parameter: CreateConditionalTradeFormParameter) {
        hidden(inputId.accountId) {
            value = parameter.accountId
        }
        hidden(inputId.securityType) {
            value = inputId.securityTypeValueId.option
        }
        hidden(CONDITIONAL_ORDER_SECURITY_TYPE_ID) {
            value = SecurityType.OPTION.toString()
        }
        hidden(inputId.underlierSymbol)
    }

    private fun RuleListBuilder.setHiddenSymbol() {
        rule {
            inputValue<WithSymbolInfo?>(inputId.symbol).observeForever { symbol ->
                symbol?.let {
                    requireAnyInput(inputId.underlierSymbol).setParameterValue(it.underlier.symbol)
                }
            }
        }
    }

    private fun GroupBuilder.conditionalOptionOrderPriceType() {
        // price type and term section
        horizontalGroup {
            optionPriceType(resources, inputId)
            price(inputId.limitPrice, inputId, dependencies.priceTypePriceLabelMap, null, dependencies.isLimitPriceInputVisible)
            price(inputId.stopPrice, inputId, dependencies.priceTypePriceLabelMap, null, dependencies.isStopPriceInputVisible)
        }
        horizontalGroup {
            price(inputId.limitPriceForStopLimit, inputId, null, dependencies.limitPriceLabel, dependencies.isStopLimitRowVisible)
            price(inputId.stopPriceForStopLimit, inputId, null, dependencies.stopPriceLabel, dependencies.isStopLimitRowVisible)
        }
    }

    private fun GroupBuilder.optionPriceType(
        resources: Resources,
        inputId: WithPriceTradeFormInputId
    ) {
        tradeSelect(inputId.priceType) {
            title = resources.getString(R.string.trade_hint_price_type)
            helpParams = TradeHelpParams(TradeOptionInfoType.PRICE_TYPE_OPTIONS)
            tradingDefaultsPreferences.optionsPriceType?.let {
                defaultOptionId = inputId.priceTypeValueId.converter.toKey(it)
            }
            options {
                val priceTypeValueId = inputId.priceTypeValueId
                option(priceTypeValueId.market) { title = resources.getString(R.string.trade_price_type_market) }
                option(priceTypeValueId.limit) { title = resources.getString(R.string.trade_price_type_limit) }
                option(priceTypeValueId.stop) { title = resources.getString(R.string.trade_price_type_stop) }
                option(priceTypeValueId.stopLimit) { title = resources.getString(R.string.trade_price_type_stop_limit) }
                option(priceTypeValueId.trailingStopDollar) { title = resources.getString(R.string.trade_price_type_trailing_stop) }
            }
        }
    }

    private fun GroupBuilder.orderTypeAndQuantity() {
        horizontalGroup {
            orderType(inputId)
            val contractsAmount = tradingDefaultsPreferences.optionsContracts.toString()
            quantity(inputId, resources.getString(R.string.trade_hint_contracts), defaultQuantity = contractsAmount)
        }
    }

    private fun GroupBuilder.orderType(inputId: WithActionTradeFormInputId) {
        tradeSelect(inputId.action) {
            title = resources.getString(R.string.trade_hint_order_type)
            helpParams = TradeHelpParams(TradeOptionInfoType.ORDER_TYPE_OPTIONS)
            options {
                val actionValueId = inputId.actionValueId
                option(actionValueId.buy) { title = resources.getString(R.string.trade_action_buy_open) }
                option(actionValueId.sellShort) { title = resources.getString(R.string.trade_action_sell_open) }
                option(actionValueId.buyToCover) { title = resources.getString(R.string.trade_action_buy_close) }
                option(actionValueId.sell) { title = resources.getString(R.string.trade_action_sell_close) }
            }
        }
    }
}
