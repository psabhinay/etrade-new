package com.etrade.mobilepro.bank.databinding;
import com.etrade.mobilepro.bank.R;
import com.etrade.mobilepro.bank.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class BankDateRangeSelectorBindingImpl extends BankDateRangeSelectorBinding implements com.etrade.mobilepro.bank.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.datesDivider, 3);
        sViewsWithIds.put(R.id.dateRangeLabel, 4);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    @Nullable
    private final android.view.View.OnClickListener mCallback2;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public BankDateRangeSelectorBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private BankDateRangeSelectorBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 4
            , (android.widget.TextView) bindings[4]
            , (android.view.View) bindings[3]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[2]
            );
        this.endDateRange.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.startDateRange.setTag(null);
        setRootTag(root);
        // listeners
        mCallback1 = new com.etrade.mobilepro.bank.generated.callback.OnClickListener(this, 1);
        mCallback2 = new com.etrade.mobilepro.bank.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x20L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.obj == variableId) {
            setObj((com.etrade.mobilepro.bank.daterangeselector.BankDateRangeSelectorViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setObj(@Nullable com.etrade.mobilepro.bank.daterangeselector.BankDateRangeSelectorViewModel Obj) {
        this.mObj = Obj;
        synchronized(this) {
            mDirtyFlags |= 0x10L;
        }
        notifyPropertyChanged(BR.obj);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeObjFormattedStartDate((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 1 :
                return onChangeObjStartDateContentDescription((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 2 :
                return onChangeObjEndDateContentDescription((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
            case 3 :
                return onChangeObjFormattedEndDate((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeObjFormattedStartDate(androidx.lifecycle.LiveData<java.lang.String> ObjFormattedStartDate, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeObjStartDateContentDescription(androidx.lifecycle.LiveData<java.lang.String> ObjStartDateContentDescription, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeObjEndDateContentDescription(androidx.lifecycle.LiveData<java.lang.String> ObjEndDateContentDescription, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeObjFormattedEndDate(androidx.lifecycle.LiveData<java.lang.String> ObjFormattedEndDate, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String objStartDateContentDescriptionGetValue = null;
        java.lang.String objEndDateContentDescriptionGetValue = null;
        java.lang.String objFormattedStartDateGetValue = null;
        androidx.lifecycle.LiveData<java.lang.String> objFormattedStartDate = null;
        androidx.lifecycle.LiveData<java.lang.String> objStartDateContentDescription = null;
        com.etrade.mobilepro.bank.daterangeselector.BankDateRangeSelectorViewModel obj = mObj;
        androidx.lifecycle.LiveData<java.lang.String> objEndDateContentDescription = null;
        androidx.lifecycle.LiveData<java.lang.String> objFormattedEndDate = null;
        java.lang.String objFormattedEndDateGetValue = null;

        if ((dirtyFlags & 0x3fL) != 0) {


            if ((dirtyFlags & 0x31L) != 0) {

                    if (obj != null) {
                        // read obj.formattedStartDate
                        objFormattedStartDate = obj.getFormattedStartDate();
                    }
                    updateLiveDataRegistration(0, objFormattedStartDate);


                    if (objFormattedStartDate != null) {
                        // read obj.formattedStartDate.getValue()
                        objFormattedStartDateGetValue = objFormattedStartDate.getValue();
                    }
            }
            if ((dirtyFlags & 0x32L) != 0) {

                    if (obj != null) {
                        // read obj.startDateContentDescription
                        objStartDateContentDescription = obj.getStartDateContentDescription();
                    }
                    updateLiveDataRegistration(1, objStartDateContentDescription);


                    if (objStartDateContentDescription != null) {
                        // read obj.startDateContentDescription.getValue()
                        objStartDateContentDescriptionGetValue = objStartDateContentDescription.getValue();
                    }
            }
            if ((dirtyFlags & 0x34L) != 0) {

                    if (obj != null) {
                        // read obj.endDateContentDescription
                        objEndDateContentDescription = obj.getEndDateContentDescription();
                    }
                    updateLiveDataRegistration(2, objEndDateContentDescription);


                    if (objEndDateContentDescription != null) {
                        // read obj.endDateContentDescription.getValue()
                        objEndDateContentDescriptionGetValue = objEndDateContentDescription.getValue();
                    }
            }
            if ((dirtyFlags & 0x38L) != 0) {

                    if (obj != null) {
                        // read obj.formattedEndDate
                        objFormattedEndDate = obj.getFormattedEndDate();
                    }
                    updateLiveDataRegistration(3, objFormattedEndDate);


                    if (objFormattedEndDate != null) {
                        // read obj.formattedEndDate.getValue()
                        objFormattedEndDateGetValue = objFormattedEndDate.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x34L) != 0) {
            // api target 4
            if(getBuildSdkInt() >= 4) {

                this.endDateRange.setContentDescription(objEndDateContentDescriptionGetValue);
            }
        }
        if ((dirtyFlags & 0x20L) != 0) {
            // api target 1

            this.endDateRange.setOnClickListener(mCallback1);
            this.startDateRange.setOnClickListener(mCallback2);
        }
        if ((dirtyFlags & 0x38L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.endDateRange, objFormattedEndDateGetValue);
        }
        if ((dirtyFlags & 0x32L) != 0) {
            // api target 4
            if(getBuildSdkInt() >= 4) {

                this.startDateRange.setContentDescription(objStartDateContentDescriptionGetValue);
            }
        }
        if ((dirtyFlags & 0x31L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.startDateRange, objFormattedStartDateGetValue);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 1: {
                // localize variables for thread safety
                // obj != null
                boolean objJavaLangObjectNull = false;
                // obj
                com.etrade.mobilepro.bank.daterangeselector.BankDateRangeSelectorViewModel obj = mObj;



                objJavaLangObjectNull = (obj) != (null);
                if (objJavaLangObjectNull) {


                    obj.onEndDateClicked();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // obj != null
                boolean objJavaLangObjectNull = false;
                // obj
                com.etrade.mobilepro.bank.daterangeselector.BankDateRangeSelectorViewModel obj = mObj;



                objJavaLangObjectNull = (obj) != (null);
                if (objJavaLangObjectNull) {


                    obj.onStartDateClicked();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): obj.formattedStartDate
        flag 1 (0x2L): obj.startDateContentDescription
        flag 2 (0x3L): obj.endDateContentDescription
        flag 3 (0x4L): obj.formattedEndDate
        flag 4 (0x5L): obj
        flag 5 (0x6L): null
    flag mapping end*/
    //end
}