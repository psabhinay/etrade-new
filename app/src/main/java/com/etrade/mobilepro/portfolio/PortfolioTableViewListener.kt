package com.etrade.mobilepro.portfolio

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.tableviewutils.presentation.TableViewListener
import com.evrencoskun.tableview.TableView

class PortfolioTableViewListener(
    private val sortListener: TableViewSortListener,
    private val actionListener: PortfolioViewActionListener
) : TableViewListener() {

    var displayMode: PortfolioPositionsDisplayMode? = PortfolioPositionsDisplayMode.TABLE

    override fun onCornerViewClicked(tableView: TableView, cornerView: View) {
        sortListener.sortColumn(PortfolioRecordColumn.SYMBOL)
    }

    override fun onColumnHeaderClicked(columnHeaderView: RecyclerView.ViewHolder, column: Int) {
        if (column >= 0) {
            sortListener.sortColumn(column)
        }
    }

    override fun onRowHeaderClicked(rowHeaderView: RecyclerView.ViewHolder, row: Int) {
        if (row >= 0) {
            actionListener.requestActions(row)
        }
    }

    override fun onCellClicked(cellView: RecyclerView.ViewHolder, column: Int, row: Int) {
        if (row >= 0) {
            val position = if (displayMode == PortfolioPositionsDisplayMode.TILE) {
                row * TILES_PER_ROW + column
            } else {
                row
            }
            actionListener.requestActions(position)
        }
    }
}

interface TableViewSortListener {
    fun sortColumn(column: PortfolioRecordColumn)
    fun sortColumn(column: Int)
}

interface PortfolioViewActionListener {
    fun requestActions(position: Int)
}
