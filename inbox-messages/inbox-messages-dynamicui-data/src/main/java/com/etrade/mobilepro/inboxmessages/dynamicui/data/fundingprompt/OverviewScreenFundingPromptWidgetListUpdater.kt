package com.etrade.mobilepro.inboxmessages.dynamicui.data.fundingprompt

import com.etrade.completeview.dto.AccountSummaryViewDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.MobileScreen
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.MessagePlace
import com.etrade.mobilepro.inboxmessages.MessageType
import com.etrade.mobilepro.inboxmessages.dynamicui.api.InboxMessagesWidgetPlace
import com.etrade.mobilepro.inboxmessages.dynamicui.data.WidgetListUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.data.extension.extractViewsAndAccounts
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import javax.inject.Inject

class OverviewScreenFundingPromptWidgetListUpdater @Inject constructor(
    private val inboxMessagesRepo: InboxMessagesRepository,
    private val fundingPromptDtoFactory: FundingPromptDtoFactory,
    private val darkModeChecker: DarkModeChecker
) : WidgetListUpdater {
    override val belongsToWidgetPlace: InboxMessagesWidgetPlace = InboxMessagesWidgetPlace.OVERVIEW

    override fun updateWithInboxMessages(input: MobileScreen?): List<BaseScreenView> {
        val (views, accounts) = input.extractViewsAndAccounts()
        val messages = inboxMessagesRepo.getMessagesBlocking(
            type = MessageType.CONTEXTUAL,
            place = MessagePlace.FUNDING_PROMPT_OVERVIEW_PORTFOLIO,
            userType = null
        )
        val resultingViews = mutableListOf<BaseScreenView>()
        views.forEach { view ->
            if (view is AccountSummaryViewDto) {
                createFundingPrompt(fundingPromptDtoFactory, view, accounts, messages, InboxMessagesWidgetPlace.OVERVIEW, darkModeChecker.isInDarkMode())
                    ?.let {
                        resultingViews.add(it)
                        return@forEach
                    }
            }
            resultingViews.add(view)
        }
        return resultingViews
    }
}
