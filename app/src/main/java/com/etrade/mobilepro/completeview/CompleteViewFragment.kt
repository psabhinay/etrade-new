package com.etrade.mobilepro.completeview

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.doOnLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.accounts.AccountsOrganizeBoardingViewModel
import com.etrade.mobilepro.common.OnAccountsClickListener
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ToolbarUpdateBlocker
import com.etrade.mobilepro.common.ToolbarUpdateBlockerImpl
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.common.networkUnavailableView
import com.etrade.mobilepro.common.toolbarView
import com.etrade.mobilepro.completeview.edit.EditCompleteViewActivity
import com.etrade.mobilepro.completeview.edit.RESULT_CODE_EDIT_COMPLETE_VIEW_CLOSED_WITH_WEB_VIEW_CHANGE
import com.etrade.mobilepro.completeview.model.NetSummaryViewModel
import com.etrade.mobilepro.databinding.FragmentCompleteviewBinding
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.dynamicui.DiffingDynamicLayoutAdapter
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.toDynamicViewModels
import com.etrade.mobilepro.home.AccountsTabHostFragmentArgs
import com.etrade.mobilepro.home.NavigateTabs
import com.etrade.mobilepro.home.TOOLTIP_SHOW_DELAY_MS
import com.etrade.mobilepro.home.ToolbarViewModel
import com.etrade.mobilepro.indices.BaseIndices
import com.etrade.mobilepro.landing.presentation.LandingPageSelectionViewModel
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.movers.portfolio.PortfolioMoversViewModel
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsErrorViewModel
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsWebViewFragment
import com.etrade.mobilepro.navdestinations.openDisclosures
import com.etrade.mobilepro.navigation.EtNavController
import com.etrade.mobilepro.news.presentation.dynamic.NewsWidgetViewModel
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.toolbar.ToolBarController
import com.etrade.mobilepro.tooltip.Tooltip
import com.etrade.mobilepro.tooltip.TooltipGravity
import com.etrade.mobilepro.tooltip.TooltipViewModel
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.binding.dataBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.etrade.mobilepro.util.android.extension.awaitForMainThread
import com.etrade.mobilepro.util.android.extension.isVisibleOnScreen
import com.etrade.mobilepro.util.android.extension.viewCoroutineScope
import com.etrade.mobilepro.util.android.goneUnless
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.badge.ExperimentalBadgeUtils
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.slf4j.LoggerFactory
import javax.inject.Inject

private const val NET_ASSETS_POSITION: Int = 0
private const val NOTIFICATION_ICON_FULL_OPACITY = 255
private const val NOTIFICATION_ICON_DIMMED = 100
private const val PF_WIDGET_ACCNT_DROPDOWN_TAG = "PORTFOLIO WIDGET ACCOUNT SELECTION DROPDOWN"

@Suppress("LargeClass", "TooManyFunctions")
@RequireLogin
@ExperimentalCoroutinesApi
class CompleteViewFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val toolBarController: ToolBarController,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val mainNavigation: MainNavigation,
    private val marketTradeStatusDelegate: MarketTradeStatusDelegate,
    private var userViewModel: UserViewModel,
    private val darkModeChecker: DarkModeChecker,
    private val applicationPreferences: ApplicationPreferences
) : Fragment(R.layout.fragment_completeview),
    OnAccountsClickListener,
    ToolbarUpdateBlocker by ToolbarUpdateBlockerImpl() {

    private val binding by dataBinding(FragmentCompleteviewBinding::bind) { onDestroyBinding() }

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    private lateinit var viewModel: CompleteViewViewModel

    private val landingSelectionViewModel: LandingPageSelectionViewModel by activityViewModels { viewModelFactory }

    private val toolbarViewModel: ToolbarViewModel by activityViewModels { viewModelFactory }

    private val tooltipViewModel: TooltipViewModel by activityViewModels()

    private val accountsOrganizeBoardingViewModel: AccountsOrganizeBoardingViewModel by activityViewModels { viewModelFactory }

    private var snackBar: Snackbar? = null

    private var tooltip: Tooltip? = null

    private var badgeDrawable: BadgeDrawable? = null

    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    private val viewAdapter: DiffingDynamicLayoutAdapter
        get() = binding.accountList.adapter as DiffingDynamicLayoutAdapter

    private val etNavController: EtNavController by lazy {
        EtNavController(
            findNavController(),
            userViewModel
        )
    }

    private val customizeAccountsRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            when (result.resultCode) {
                Activity.RESULT_OK -> {
                    viewModel.getCompleteView(false)
                    setUpStateOnActivityResult()
                }
                RESULT_CODE_EDIT_COMPLETE_VIEW_CLOSED_WITH_WEB_VIEW_CHANGE -> {
                    viewModel.reloadMsCompleteView()
                    setUpStateOnActivityResult()
                }
            }
        }

    private fun setUpStateOnActivityResult() {
        binding.completeviewScrollContainer.scrollTo(0, 0)
        binding.completeviewScrollContainer.isSaveEnabled = true
        toolBarController.updateTitle(showTitle = false)
    }

    @Suppress("LongMethod", "ComplexMethod")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.obj = viewModel
        setupRetry()

        // Back stack navigation does not always trigger the destination changed listeners
        // causing the homeAsUp button to remain visible and the title not to be updated.
        toolbarViewModel.manualToolbar(false, "")
        (activity as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)

        binding.accountList.adapter = DiffingDynamicLayoutAdapter(
            this,
            constructViewModels(viewModel.lastLayouts),
            ::awaitForMainThread
        ) {
            viewModel.updateFooterVisibility()
        }

        with(binding.accountList) {
            layoutManager = LinearLayoutManager(context)
            decorateCompleteViewItems(
                secondItemOffsetRes = R.dimen.spacing_none,
                verticalOffsetRes = R.dimen.spacing_xsmall,
                horizontalOffsetRes = R.dimen.spacing_small,
                lastItemOffsetRes = R.dimen.spacing_none
            )
        }

        // This is needed to prevent purple highlight over entire screen on load
        binding.completeviewScrollContainer.apply {
            defaultFocusHighlightEnabled = false
            setOnScrollChangeListener { _, _, _, _, _ ->
                updateToolbar()
            }
        }

        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is CompleteViewViewModel.ViewState.Loading -> {
                        if (!binding.swipeRefresh.isRefreshing) {
                            binding.loadingIndicator.show()
                        }
                        this.snackBar?.dismiss()
                        viewAdapter.values = constructViewModels(it.views, false)
                    }
                    is CompleteViewViewModel.ViewState.Error -> {
                        binding.loadingIndicator.hide()
                        binding.swipeRefresh.isRefreshing = false
                        snackBar = snackbarUtil.retrySnackbar { viewModel.getCompleteView() }
                    }
                    is CompleteViewViewModel.ViewState.Success -> {
                        if (darkModeChecker.uiMode != it.uiMode) {
                            viewAdapter.values = emptyList()
                            viewModel.getCompleteView()
                        } else {
                            viewAdapter.values = constructViewModels(it.views, it.shouldRefresh)
                            binding.loadingIndicator.hide()
                            snackBar?.dismiss()
                            binding.swipeRefresh.isRefreshing = false
                        }
                    }
                }
            }
        )
        viewModel.customizeClickedSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume {
                    binding.completeviewScrollContainer.isSaveEnabled = false
                    val intent = EditCompleteViewActivity.intent(requireContext()) {
                        isDeletableItems = false
                        isToggleableItems = true
                        listFooterMessage = if (userViewModel.hasMoreThanOneAccount) {
                            R.string.edit_list_complete_view_footer_message
                        } else {
                            R.string.edit_list_footer_message
                        }
                    }
                    customizeAccountsRequest.launch(intent)
                }
            }
        )
        viewModel.disclosuresClickedSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume {
                    binding.completeviewScrollContainer.isSaveEnabled = false
                    startActivity(activity?.openDisclosures())
                }
            }
        )
        accountsOrganizeBoardingViewModel.accountsOrderingChanged.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { viewModel.getCompleteView(true) }
            }
        )

        userViewModel.selectMultipleAccountView()

        marketTradeStatusDelegate.marketStatus.observe(
            viewLifecycleOwner,
            Observer {
                // Network banner takes precedence over market event.
                val showMarketHaltBanner = it.peekContent().isMarketHalted
                binding.overviewHaltBanner.goneUnless(showMarketHaltBanner)
            }
        )

        activity?.hideTradeBannerOnNetworkBannerDisplayed()
        activity?.let { activity ->
            toolBarController.toolbarSignal.observeBy(viewLifecycleOwner) { signal ->
                signal.consume {
                    it.invoke(activity.toolbarView)
                }
            }
        }
        bindLandingSelectionViewModel()

        setFragmentResultListener(MsAccountsWebViewFragment.RESULT_MS_ACCOUNTS_WEB_VIEW_CLOSED) { _, _ ->
            viewModel.reloadMsCompleteView()
        }
    }

    private fun bindLandingSelectionViewModel() {
        landingSelectionViewModel.apply {
            var landingDisplayedNow = false
            showLandingPageSelection.observeBy(viewLifecycleOwner) {
                if (it) {
                    landingDisplayedNow = true
                } else if (it.not() && landingDisplayedNow.not()) {
                    showTooltip()
                }
            }
        }
    }

    private fun updateToolbar() {
        binding.accountList.getChildAt(NET_ASSETS_POSITION)?.let {
            val previousState = toolBarController.isShowingTitle
            val isNetAssetsHidden = it.isVisibleOnScreen().not()
            if (previousState != isNetAssetsHidden) {
                toolBarController.updateTitle(showTitle = isNetAssetsHidden)
            }
        }
    }

    private fun Activity.hideTradeBannerOnNetworkBannerDisplayed() {
        networkUnavailableView?.doOnLayout {
            if (view != null) {
                if (binding.overviewHaltBanner.visibility == View.VISIBLE && it.visibility == View.VISIBLE) {
                    binding.overviewHaltBanner.visibility = View.GONE
                }
            }
        }
    }

    @Suppress("LongMethod", "ComplexMethod")
    private fun constructViewModels(
        views: List<GenericLayout>,
        shouldRefresh: Boolean = false
    ): List<DynamicViewModel> {
        val viewModels =
            views.toDynamicViewModels(this, shouldRefresh = shouldRefresh) { viewModel ->
                if (viewModel is LifecycleObserver) {
                    lifecycle.addObserver(viewModel)
                }
                viewModel.observeClickEvents()
            }
        viewModels.forEachIndexed { index, viewModel ->
            when (viewModel) {
                is BaseIndices -> {
                    viewModel.isFirstItem = index == 1
                }
                is NetSummaryViewModel -> {
                    viewModel.netAssets.observeBy(viewLifecycleOwner) {
                        toolBarController.updateTitle(titleText = it.text)
                    }
                }
                is PortfolioMoversViewModel -> {
                    viewModel.openAccountSelection.observeBy(viewLifecycleOwner) {
                        it.consume { accountUuid ->
                            showAccountSelection(accountUuid) { account ->
                                viewModel.onAccountSelection(account)
                            }
                        }
                    }
                }
                is NewsWidgetViewModel -> {
                    viewModel.openAccountSelection.observeBy(viewLifecycleOwner) {
                        it.consume { accountUuid ->
                            showAccountSelection(accountUuid) { account ->
                                viewModel.onAccountSelection(account)
                            }
                        }
                    }
                }
            }
        }
        return viewModels
    }

    @Suppress("ComplexMethod")
    private fun DynamicViewModel.observeClickEvents() {
        clickEvents.removeObservers(viewLifecycleOwner)
        clickEvents.observe(
            viewLifecycleOwner,
            Observer { event ->
                when (event) {
                    is CtaEvent.AccountsTabNavigation -> (parentFragment as? NavigateTabs)?.navigateToTab(
                        event
                    )
                    is CtaEvent.NavDirectionsNavigation -> {
                        updateSelectedAccount(event)
                        findNavController().navigate(event.ctaDirections)
                    }
                    is CtaEvent.LaunchQuote -> mainNavigation.navigateToQuoteDetails(
                        activity,
                        event.bundle
                    )
                    is CtaEvent.UriEvent -> findNavController().navigate(event.uri)
                    CtaEvent.ViewAllBrokerageAccounts -> etNavController.navigateToAllPositions()
                    is CtaEvent.InfoDialogEvent -> showInfoDialog(event)
                    CtaEvent.RetryEvent -> handleRetryEvent()
                    is CtaEvent.DismissInboxMessage -> this@CompleteViewFragment.viewModel.dismissInboxMessage(event.message)
                    else -> logger.warn("Unhandled event $event")
                }
            }
        )
    }

    private fun DynamicViewModel.handleRetryEvent() {
        if (this is MsAccountsErrorViewModel) {
            this@CompleteViewFragment.viewModel.reloadMsCompleteView()
        }
    }

    private fun showInfoDialog(event: CtaEvent.InfoDialogEvent) {
        CustomDialogFragment.newInstance(
            title = event.title,
            message = event.message,
            cancelable = false,
            okTitle = getString(R.string.ok)
        ).show(parentFragmentManager, INFO_DIALOG_TAG)
    }

    private fun updateSelectedAccount(event: CtaEvent.NavDirectionsNavigation) {
        val arguments = event.ctaDirections.arguments

        val accountUuid = when (event.ctaDirections.actionId) {
            R.id.action_home_to_bank, R.id.action_home_to_account_tabs -> {
                AccountsTabHostFragmentArgs.fromBundle(arguments).accountUuid
            }
            else -> null
        }

        accountUuid?.let {
            userViewModel.updateSelectedAccount(it)
        }
    }

    private fun setupRetry() {
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.getCompleteView(false)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(CompleteViewViewModel::class.java)
        setHasOptionsMenu(true)
    }

    override fun onPause() {
        super.onPause()
        marketTradeStatusDelegate.resetMarketStatus()
    }

    override fun onResume() {
        super.onResume()
        if (badgeDrawable == null) {
            requireActivity().invalidateOptionsMenu()
        }
    }

    @ExperimentalBadgeUtils
    override fun onStop() {
        super.onStop()
        removeBadgeDrawable()
    }

    private fun FragmentCompleteviewBinding.onDestroyBinding() {
        accountList.adapter = null
        swipeRefresh.isEnabled = false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        toolbarViewModel.resetToolbar()
        hideTooltip()
    }

    private fun showTooltip() {
        if (applicationPreferences.shouldDisplayCompleteViewTooltip) {
            view?.postDelayed(
                {
                    activity?.toolbarView?.findViewById<View>(R.id.menu_quote_lookup)?.let {
                        val doOnDismiss = {
                            hideTooltip()
                        }
                        tooltip = tooltipViewModel.show {
                            target = it
                            text = getString(R.string.search_tooltip_description)
                            onExplicitDismiss = doOnDismiss
                            onImplicitDismiss = doOnDismiss
                            gravity = TooltipGravity.BOTTOM
                            durationParams = Tooltip.DurationParams(viewCoroutineScope)
                        }
                    }
                },
                TOOLTIP_SHOW_DELAY_MS
            )
        }
    }

    private fun hideTooltip() {
        tooltipViewModel.dismiss(tooltip)
        tooltip = null
    }

    override fun onAccountsClick() {
        binding.accountList.stopScroll()
        binding.completeviewScrollContainer.smoothScrollTo(0, 0)
    }

    @ExperimentalBadgeUtils
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.complete_view_menu, menu)
        setPersonalNotificationsMenuItem(menu)
        blockTitleUpdates()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_personal_notifications) {
            userViewModel.userSeenNotificationsInCurrentSession = true
            findNavController().navigate(MainGraphDirections.openPersonalNotifications())
        }
        return super.onOptionsItemSelected(item)
    }

    @ExperimentalBadgeUtils
    private fun setPersonalNotificationsMenuItem(menu: Menu) {
        val notificationItem = menu.findItem(R.id.menu_personal_notifications)
        val showIconWithBadge = userViewModel.hasNewOrUnreadPersonalNotifications &&
            !userViewModel.userSeenNotificationsInCurrentSession
        setBadgeDrawable(showIconWithBadge)
        notificationItem.icon.alpha = if (showIconWithBadge) {
            NOTIFICATION_ICON_FULL_OPACITY
        } else {
            NOTIFICATION_ICON_DIMMED
        }
    }

    @ExperimentalBadgeUtils
    @SuppressLint("UnsafeExperimentalUsageError")
    private fun setBadgeDrawable(visible: Boolean) {
        badgeDrawable = BadgeDrawable.create(requireContext())
        badgeDrawable?.apply {
            isVisible = visible
            backgroundColor = ContextCompat.getColor(requireContext(), R.color.red_light)
            horizontalOffset =
                requireContext().resources.getInteger(R.integer.personal_notification_icon_horizontal_offset)
            verticalOffset =
                requireContext().resources.getInteger(R.integer.personal_notification_icon_vertical_offset)
            BadgeUtils.attachBadgeDrawable(
                this,
                requireActivity().toolbarView,
                R.id.menu_personal_notifications
            )
        }
    }

    @ExperimentalBadgeUtils
    @SuppressLint("UnsafeExperimentalUsageError")
    private fun removeBadgeDrawable() {
        badgeDrawable?.let {
            BadgeUtils.detachBadgeDrawable(
                it,
                requireActivity().toolbarView,
                R.id.menu_personal_notifications
            )
            badgeDrawable = null
        }
    }

    private fun showAccountSelection(
        currentSelectedAccountUuid: String?,
        onAccountSelection: (Account) -> Unit
    ) {
        val accounts = userViewModel.brokerageAccounts
        val selectedAccountIndex = accounts.indexOf(accounts.find { it.account?.accountUuid == currentSelectedAccountUuid })
        BottomSheetSelector<String>(childFragmentManager, resources).apply {
            init(
                PF_WIDGET_ACCNT_DROPDOWN_TAG,
                getString(R.string.title_account),
                accounts.mapNotNull { it.account?.accountShortName },
                selectedAccountIndex
            )
            setListener { _, selectedAccount ->
                accounts.find { it.account?.accountShortName == selectedAccount }?.let {
                    it.account?.let { account -> onAccountSelection(account) }
                }
            }
        }.openDropDown()
    }

    companion object {
        const val INFO_DIALOG_TAG = "OverviewFragment/INFO_DIALOG_TAG"
    }
}
