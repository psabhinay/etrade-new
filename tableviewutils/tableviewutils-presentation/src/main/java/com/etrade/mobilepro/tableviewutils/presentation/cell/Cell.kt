package com.etrade.mobilepro.tableviewutils.presentation.cell

import com.evrencoskun.tableview.filter.IFilterableModel
import com.evrencoskun.tableview.sort.ISortableModel
import java.util.Objects

open class Cell(private val id: String, val displayText: String, val displayTextContentDescription: String = displayText) : ISortableModel, IFilterableModel {

    private val filterKeyword: String? = displayText

    /**
     * This is necessary for sorting process.
     * See ISortableModel
     */
    override fun getId(): String = id

    /**
     * This is necessary for sorting process.
     * See ISortableModel
     */
    override fun getContent(): Any = displayText

    override fun getFilterableKeyword(): String? = filterKeyword

    override fun toString(): String = displayText

    override fun hashCode(): Int {
        return Objects.hash(getId(), displayText, content)
    }

    override fun equals(other: Any?): Boolean = (other is Cell) &&
        getId() == other.getId() &&
        displayText == other.displayText &&
        content == other.content
}
