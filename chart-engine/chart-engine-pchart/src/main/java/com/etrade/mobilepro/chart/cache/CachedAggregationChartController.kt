package com.etrade.mobilepro.chart.cache

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.chartengine.SymbolChartData
import javax.inject.Inject

class CachedAggregationChartController @Inject constructor(
    private val singleEntryCacheController: SimpleCachedResource.Controller<SymbolChartData>
) : SimpleCachedResource.Controller<List<@JvmSuppressWildcards SymbolChartData>> {

    override fun shouldRefresh(itemFromCache: List<SymbolChartData>): Boolean {
        return itemFromCache.any { singleEntryCacheController.shouldRefresh(it) }
    }
}
