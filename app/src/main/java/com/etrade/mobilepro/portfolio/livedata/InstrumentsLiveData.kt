package com.etrade.mobilepro.portfolio.livedata

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.dao.InstrumentDao
import com.etrade.mobilepro.instrument.data.getInstrumentsAssociated
import javax.inject.Inject

class InstrumentsLiveData @Inject constructor(private val instrumentDao: InstrumentDao) : SingleSourceLiveData<Map<String, Instrument>>() {
    override fun getData(ids: List<String>): LiveData<Map<String, Instrument>> = instrumentDao.getInstrumentsAssociated(ids)

    override fun shouldEmit(value: Map<String, Instrument>): Boolean = value.isNotEmpty()
}
