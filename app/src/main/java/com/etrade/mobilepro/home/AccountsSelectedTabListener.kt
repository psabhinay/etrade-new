package com.etrade.mobilepro.home

interface AccountsSelectedTabListener {

    var selectedTabIndex: Int
}

class DefaultAccountsSelectedTabListener : AccountsSelectedTabListener {

    override var selectedTabIndex = 0
}
