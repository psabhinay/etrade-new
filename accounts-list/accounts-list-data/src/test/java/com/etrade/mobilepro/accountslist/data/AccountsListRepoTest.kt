package com.etrade.mobilepro.accountslist.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.accountslist.api.AccountsListResult
import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.referencehelper.ReferenceHelper
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.rx.test.RxImmediateSchedulerRule
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.util.Resource
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doNothing
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Assert
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class AccountsListRepoTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    private fun getInboxMessagesRepo(result: ETResult<List<InboxMessage>>): InboxMessagesRepository =
        mock {
            on { getMessagesBlocking(any(), anyOrNull(), anyOrNull()) } doReturn result
        }

    private val referenceHelper: ReferenceHelper = mock()

    private fun getBaseJsonResponse(): BaseMobileResponse {
        return this::class.getObjectFromJson(
            resourcePath = "accounts_list.json",
            BaseMobileResponse::class.java,
            adapters = listOf(
                createBaseReferenceAdapter(),
                createBaseScreenViewAdapter(),
                createDynamicMetaTypeAdapter()
            )
        )
    }

    private fun mockDynamicScreenService(response: BaseMobileResponse): ScreenDataSource {
        val mockResponse: Observable<Resource<BaseMobileResponse>> =
            Observable.just(Resource.Success(response))
        return mock {
            on { getScreen(any(), any(), anyOrNull()) } doReturn mockResponse
        }
    }

    private fun mockDynamicScreenServiceWithErrorResponse(): ScreenDataSource {
        val mockResponse: Observable<Resource<BaseMobileResponse>> =
            Observable.just(Resource.Failed(error = Throwable("Failed due to error")))
        return mock {
            on { getScreen(any(), any(), anyOrNull()) } doReturn mockResponse
        }
    }

    @Test
    fun `test the validity of data from a error response of getScreen call`() {

        // Given
        doNothing().`when`(referenceHelper).persist(any())
        val sut = DefaultAccountsListRepo(
            mockDynamicScreenServiceWithErrorResponse(),
            getInboxMessagesRepo(ETResult.success(emptyList())),
            referenceHelper
        )
        val testObserver = TestObserver<Resource<AccountsListResult>>()

        // When
        sut.getScreen().subscribe(testObserver)

        // Then
        testObserver.assertValue { it.data?.personalNotifications?.isEmpty() == true }

        testObserver.values()?.first()?.assertExpectedFailureData()
    }

    private fun Resource<AccountsListResult>.assertExpectedFailureData() {
        data?.msUserPreferences?.run {
            Assert.assertNotNull(data?.msUserPreferences)
            Assert.assertNull(data?.msUserPreferences?.loginUuid)
        }
    }

    @Test
    fun `test the validity of data from a successful response of getScreen call`() {

        // Given
        doNothing().`when`(referenceHelper).persist(any())
        val sut = DefaultAccountsListRepo(
            mockDynamicScreenService(getBaseJsonResponse()),
            getInboxMessagesRepo(ETResult.success(emptyList())),
            referenceHelper
        )
        val testObserver = TestObserver<Resource<AccountsListResult>>()

        // When
        sut.getScreen().subscribe(testObserver)

        // Then
        testObserver.assertValue { it.data?.personalNotifications?.isEmpty() == true }

        testObserver.values()?.first()?.assertExpectedData()
    }

    private fun Resource<AccountsListResult>.assertExpectedData() {

        data?.msUserPreferences?.run {
            Assert.assertEquals("YWJtYWwy", loginUuid)
            Assert.assertTrue(isMsEligible)

            Assert.assertTrue(ctaList.isNotEmpty())
            ctaList.first().run {
                Assert.assertEquals("MS Settings", label)
                Assert.assertEquals("Morgan Stanley Settings", title)
                Assert.assertEquals("/etx/hw/mssettings", clickActionDto.webViewUrl)
            }

            ctaList.getOrNull(2)?.run {
                Assert.assertEquals("MS Enrollment", label)
                Assert.assertEquals("Display Accounts", title)
                Assert.assertEquals(
                    "/etx/pxy/morgan-stanley/account-visibility-enrollment",
                    clickActionDto.webViewUrl
                )
            }
        }
    }
}
