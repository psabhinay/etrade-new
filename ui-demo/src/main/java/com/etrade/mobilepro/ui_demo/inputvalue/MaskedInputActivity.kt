package com.etrade.mobilepro.ui_demo.inputvalue

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.ui_demo.databinding.ActivityMaskedInputBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.google.android.material.textfield.TextInputEditText

class MaskedInputActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMaskedInputBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)

        binding.longPhoneNumber.apply {
            initTextWatcher {
                binding.longRawPhoneNumber.text = getValue().toString()
            }
            setText("234")
        }

        binding.decimalDollarValue.apply {
            initTextWatcher {
                binding.decimalRawDollarValue.text = getValue().toString()
            }
        }
    }

    private fun TextInputEditText.initTextWatcher(onTextChanged: () -> Unit) {
        onTextChanged()
        addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // unused
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // unused
            }

            override fun afterTextChanged(s: Editable?) {
                onTextChanged()
            }
        })
    }
}
