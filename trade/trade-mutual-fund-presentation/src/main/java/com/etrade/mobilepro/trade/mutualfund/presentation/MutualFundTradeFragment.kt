package com.etrade.mobilepro.trade.mutualfund.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.dialog.showDialog
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.progressoverlay.ProgressDialogFragment
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.mutualfund.api.MutualFundEntry
import com.etrade.mobilepro.trade.presentation.util.SharedTradeViewModel
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import javax.inject.Inject
import javax.inject.Provider

private const val MUTUAL_FUND_SELL_TAG = "mutual_fund_sell_tag"

@RequireLogin
class MutualFundTradeFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    tradeRouter: TradeRouter,
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>
) : BottomSheetDialogFragment() {
    private val accountId: String
        get() = requireNotNull(requireArguments().getString("accountId"))
    private val type: MutualFundTradeType
        get() = requireNotNull(requireArguments().get("type")) as MutualFundTradeType
    private val symbol: String?
        get() = requireArguments().getString("symbol")
    private val typeCode: String?
        get() = requireArguments().getString("typeCode")

    private val sharedTradeViewModel: SharedTradeViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }
    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }) { view } }
    private val viewModel: MutualFundTradeViewModel by viewModels { viewModelFactory }

    override fun getTheme() = R.style.BottomSheetDialogTheme

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.trade_fragment_mutual_fund, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.apply {
            actionLoadingIndicator.observe(viewLifecycleOwner, Observer { onActionLoadingIndicator(it) })
            errorMessage.observe(viewLifecycleOwner, Observer { onErrorMessage(it) })
            mutualFundSellEntry.observe(viewLifecycleOwner, Observer { popupSellEntryDropdown(it) })
            mutualFundBuyEntry.observe(viewLifecycleOwner, Observer { popupBuyEntryDropdown(it) })
            mutualFundErrorMessage.observe(viewLifecycleOwner, Observer { showErrorDialog(it) })
        }
        if (savedInstanceState == null) {
            viewModel.start(accountId, type, symbol, typeCode)
        }
    }

    private fun onErrorMessage(errorMessage: ConsumableLiveEvent<ErrorMessage>) {
        errorMessage.consume {
            snackBarUtil.errorSnackbar(it.message)
        }
    }

    private fun showErrorDialog(errorMessage: String) {
        showDialog(
            appDialog {
                title = resources.getString(R.string.trade_error_invalid_symbol_dialog_title)
                message = errorMessage
                positiveAction {
                    label = resources.getString(R.string.ok)
                    action = { dismissTheFragment() }
                }
            }
        )
    }

    private fun onActionLoadingIndicator(loading: Boolean) {
        if (loading) {
            ProgressDialogFragment.show(childFragmentManager, fullscreen = false, dimBackground = false)
        } else {
            ProgressDialogFragment.dismiss(childFragmentManager)
        }
    }

    private fun popupSellEntryDropdown(list: List<MutualFundEntry>) {
        setupPopup(list, getString(R.string.trade_select_a_fund_to_sell)) {
            sharedTradeViewModel.injectFormInputValues(
                tradeFormParametersBuilderProvider.get().apply {
                    symbol = Symbol(symbol = it.symbol, instrumentType = it.instrumentType)
                }
                    .create(SecurityType.MUTUAL_FUND)
                    .toMap()
            )
        }
    }

    private fun popupBuyEntryDropdown(list: List<MutualFundEntry>) {
        setupPopup(list, getString(R.string.trade_select_a_fund)) {
            sharedTradeViewModel.injectFormInputValues(
                tradeFormParametersBuilderProvider.get().apply {
                    fundToBuy = Symbol(symbol = it.symbol, instrumentType = it.instrumentType)
                }
                    .create(SecurityType.MUTUAL_FUND)
                    .toMap()
            )
        }
    }

    private fun setupPopup(it: List<MutualFundEntry>, title: String, action: (selected: MutualFundEntry) -> Unit) {
        BottomSheetSelector<MutualFundEntry>(childFragmentManager, resources).apply {
            init(
                tag = MUTUAL_FUND_SELL_TAG,
                title = title,
                items = it,
                initialSelectedPosition = -1,
                dividerPosition = -1,
                ghostText = getString(R.string.trade_mutual_fund_inline_disclosure)
            )

            setListener { _, selected ->
                action(selected)
            }

            setDismissAction {
                if (isOpen(MUTUAL_FUND_SELL_TAG)) {
                    dismissTheFragment()
                }
            }
            openDropDown()
        }
    }

    private fun dismissTheFragment() {
        findNavController().popBackStack()
    }
}
