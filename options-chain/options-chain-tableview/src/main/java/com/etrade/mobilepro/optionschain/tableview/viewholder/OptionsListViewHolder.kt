package com.etrade.mobilepro.optionschain.tableview.viewholder

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.optionschain.tableview.BR
import com.etrade.mobilepro.optionschain.tableview.R
import com.etrade.mobilepro.optionschain.tableview.SelectedOption

class OptionsListViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(option: SelectedOption) {
        highlightCells(option)
        binding.setVariable(BR.obj, option)
        binding.executePendingBindings()
    }

    /**
     * Highlight the cells that are in the money
     */
    private fun highlightCells(option: SelectedOption) {
        val colorRes = when {
            option.isSelected -> R.color.primaryColor
            option.option.isInTheMoney -> R.color.in_the_money_highlight
            else -> R.color.white
        }
        itemView.setBackgroundResource(colorRes)
    }
}
