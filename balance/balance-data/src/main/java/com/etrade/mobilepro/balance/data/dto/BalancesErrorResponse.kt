package com.etrade.mobilepro.balance.data.dto

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false, name = "LoginUserSSOResponse")
data class BalancesErrorResponse constructor(

    @field:Element(name = "Result")
    var result: Result? = null
)

data class Result constructor(

    @field:Attribute(name = "code")
    var code: Int? = null,

    @field:Element(name = "Fault")
    var fault: Fault? = null

)

data class Fault constructor(

    @field:Element(name = "Error")
    var error: Error? = null
)

data class Error constructor(

    @field:Element(name = "ErrorCode")
    var code: Int? = null,

    @field:Element(name = "ErrorType")
    var type: Int? = null,

    @field:Element(name = "ErrorDescription")
    var description: String? = null
)
