package com.etrade.mobilepro.trade.optionlegselect

import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import javax.inject.Qualifier

@Qualifier
annotation class OptionTradeActionItemProvider

@Qualifier
annotation class StockTradeActionItemProvider

interface TradeActionItemProvider {
    fun getActions(): List<ActionItem>
    fun getTransactionType(id: Int): TransactionType
    fun isActionIdSupported(id: Int): Boolean
}

abstract class BaseTradeActionItemProvider : TradeActionItemProvider {
    abstract val actionList: List<ActionItem>
    abstract val actionTransactionTypeMap: Map<Int, TransactionType>

    override fun getActions(): List<ActionItem> = actionList

    override fun getTransactionType(id: Int): TransactionType = actionTransactionTypeMap[id] ?: throw IllegalArgumentException("Invalid action item")

    override fun isActionIdSupported(id: Int): Boolean = actionTransactionTypeMap[id] != null
}

object DefaultOptionTradeActionItemProvider : BaseTradeActionItemProvider() {

    override val actionTransactionTypeMap = mapOf(
        R.id.trade_action_buy_open to TransactionType.BUY_OPEN_OPT,
        R.id.trade_action_sell_open to TransactionType.SELL_OPEN_OPT,
        R.id.trade_action_buy_close to TransactionType.BUY_CLOSE_OPT,
        R.id.trade_action_sell_close to TransactionType.SELL_CLOSE_OPT
    )

    override val actionList = listOf(
        ActionItem(R.id.trade_action_buy_open, R.string.trade_action_buy_open),
        ActionItem(R.id.trade_action_sell_open, R.string.trade_action_sell_open),
        ActionItem(R.id.trade_action_buy_close, R.string.trade_action_buy_close),
        ActionItem(R.id.trade_action_sell_close, R.string.trade_action_sell_close)
    )
}

object DefaultStockTradeActionItemProvider : BaseTradeActionItemProvider() {

    override val actionTransactionTypeMap = mapOf(
        R.id.trade_action_buy to TransactionType.BUY_OPEN,
        R.id.trade_action_sell to TransactionType.SELL_CLOSE,
        R.id.trade_action_sell_short to TransactionType.SELL_OPEN,
        R.id.trade_action_buy_to_cover to TransactionType.BUY_CLOSE
    )

    override val actionList = listOf(
        ActionItem(R.id.trade_action_buy, R.string.trade_action_buy),
        ActionItem(R.id.trade_action_sell, R.string.trade_action_sell),
        ActionItem(R.id.trade_action_sell_short, R.string.trade_action_sell_short),
        ActionItem(R.id.trade_action_buy_to_cover, R.string.trade_action_buy_to_cover)
    )
}
