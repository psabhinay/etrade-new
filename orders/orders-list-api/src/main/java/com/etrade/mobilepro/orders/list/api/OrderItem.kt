package com.etrade.mobilepro.orders.list.api

import com.etrade.mobilepro.orders.api.OrderStatus

/**
 * An interface to represent items in the orders list
 */
interface OrderItem {
    val orderNumber: String
    val orderStatus: OrderStatus
    val symbols: List<String>
}
