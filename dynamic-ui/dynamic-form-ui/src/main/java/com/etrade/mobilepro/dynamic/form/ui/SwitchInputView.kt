package com.etrade.mobilepro.dynamic.form.ui

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.api.SwitchInput
import com.google.android.material.switchmaterial.SwitchMaterial

internal class SwitchInputView : SwitchMaterial, ComponentView<SwitchInput> {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val sideMargin: Int = context.resources.getDimensionPixelSize(R.dimen.spacing_medium)

    private var input: SwitchInput? = null

    private val inputViewDelegate: InputViewDelegate = InputViewDelegate(this, ::setTitle)

    private val valueObserver = Observer<Boolean?> {
        isChecked = it == true
    }

    init {
        setOnCheckedChangeListener { _, isChecked ->
            input?.setValue(isChecked)
        }
    }

    override fun render(component: SwitchInput, owner: LifecycleOwner) {
        inputViewDelegate.render(component, owner)

        input?.value?.removeObserver(valueObserver)

        input = component

        component.value.observe(owner, valueObserver)
    }

    override fun setLayoutParams(params: ViewGroup.LayoutParams) {
        if (params is ViewGroup.MarginLayoutParams) {
            params.leftMargin = sideMargin
            params.rightMargin = sideMargin
        }
        super.setLayoutParams(params)
    }

    private fun setTitle(title: CharSequence?) {
        text = title
    }
}
