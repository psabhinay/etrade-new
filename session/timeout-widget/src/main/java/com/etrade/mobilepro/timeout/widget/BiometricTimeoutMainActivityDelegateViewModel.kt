package com.etrade.mobilepro.timeout.widget

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.biometric.api.BiometricsProvider
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.timeout.state.TimeoutState
import javax.inject.Inject

class BiometricTimeoutMainActivityDelegateViewModel @Inject constructor(
    private val sessionState: UserAuthenticationState,
    private val loginPreferences: LoginPreferencesRepo,
    private val biometricProvider: BiometricsProvider,
    private val timeoutState: TimeoutState
) : ViewModel() {

    private val isLoggedOut: Boolean
        get() {
            return !sessionState.invoke()
        }

    private val isShowingTimeout: Boolean
        get() {
            return timeoutState.isDialogPresent()
        }

    val shouldShowBiometricAuthentication: Boolean
        get() = timeoutState.shouldShowBiometricAuthentication && !isShowingTimeout

    val isBiometricLoginAvailable: Boolean
        get() {
            return isLoggedOut &&
                loginPreferences.isBiometricAuthenticationEnabled &&
                biometricProvider.canAuthenticate().isSuccessful
        }
}
