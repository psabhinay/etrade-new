package com.etrade.mobilepro.chartiq.data

import com.etrade.mobilepro.androidconfig.api.AndroidConfigInfo
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.chartiq.api.ChartIqClient
import com.etrade.mobilepro.chartiq.api.model.Account
import com.etrade.mobilepro.chartiq.api.model.Environment
import com.etrade.mobilepro.chartiq.api.model.SymbolInfo
import com.etrade.mobilepro.chartiq.data.dto.ChartIqConfigurationDto
import com.etrade.mobilepro.chartiq.data.dto.ChartSettingsDto
import com.etrade.mobilepro.chartiq.data.dto.MarketDataDto
import com.etrade.mobilepro.chartiq.data.dto.SymbolDto
import com.etrade.mobilepro.chartiq.data.dto.Toggle
import com.etrade.mobilepro.chartiq.data.dto.mapping.toDto
import com.etrade.mobilepro.chartiq.data.dto.mapping.toMarketData
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.util.AuthenticationStatusProvider
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.slf4j.LoggerFactory

private const val CHART_ENDPOINT_PATH = "/phx/pchart/%s/graphs/"

class ChartIqClientImpl(
    private val moshi: Moshi,
    private val applicationInfo: ApplicationInfo,
    private val authStatusProvider: AuthenticationStatusProvider,
    private val commandSink: ChartIqCommandSink
) : ChartIqClient {

    private val logger = LoggerFactory.getLogger(ChartIqClientImpl::class.java)
    private val marketDataJsonAdapter: JsonAdapter<MarketDataDto> = moshi.adapter(MarketDataDto::class.java)

    override fun initialize(akamaiConfiguration: AndroidConfigInfo, environmentName: String) {
        val chartSettings = ChartSettingsDto(akamaiConfiguration, environmentName)
        val jsonAdapter: JsonAdapter<ChartSettingsDto> = moshi.adapter(ChartSettingsDto::class.java)
        val json = jsonAdapter.toJson(chartSettings)
        val configurationCommand = "javascript: var akamaiInfo = $json;"
        send(configurationCommand)
    }

    @Suppress("LongMethod")
    override fun updateChartConfiguration(
        symbolInfo: SymbolInfo,
        accounts: List<Account>,
        defaultAccount: Account?,
        environment: Environment,
        syncServerData: Boolean,
        sessionCookie: String,
        csrfToken: String?,
        updateSession: Boolean,
        restoreState: Boolean,
        showDrawingNotImplementedAlert: Boolean
    ) {
        val chartIqConfiguration = ChartIqConfigurationDto(
            symbolInfo = symbolInfo.toDto(),
            accounts = accounts.map { it.toDto() },
            deviceType = applicationInfo.platform(),
            defaultAccount = defaultAccount?.toDto(),
            isTablet = applicationInfo.isTablet,
            environmentName = environment.name,
            environmentUrl = environment.baseUrl,
            chartUrl = buildEndpointUrl(environment.chartUrl),
            syncServerData = syncServerData,
            isLoggedIn = authStatusProvider.isAuthenticated(),
            sessionCookie = sessionCookie,
            csrfToken = csrfToken,
            restoreState = restoreState,
            showDrawingNotImplementedAlert = showDrawingNotImplementedAlert
        )
        val jsonAdapter: JsonAdapter<ChartIqConfigurationDto> = moshi.adapter(ChartIqConfigurationDto::class.java)
        val json = jsonAdapter.toJson(chartIqConfiguration)
        val commandName = if (updateSession) {
            ChartIqCommandNames.UPDATE_SESSION
        } else {
            ChartIqCommandNames.INIT_CONFIG
        }
        val command = "$commandName($json);"
        send(command)
    }

    private fun buildEndpointUrl(chartBaseUrl: String): String {
        val endpointType = if (authStatusProvider.isAuthenticated()) {
            com.etrade.mobilepro.chart.rest.PchartEndpointType.SECURED
        } else {
            com.etrade.mobilepro.chart.rest.PchartEndpointType.PUBLIC
        }
        return "$chartBaseUrl${CHART_ENDPOINT_PATH.format(endpointType.typeCode)}"
    }

    override fun addStreamingData(data: Level1Data, ticker: String?) {
        data.toMarketData(ticker).let { marketDataDto ->
            val json = marketDataJsonAdapter.toJson(marketDataDto)
            val updateStreamingDataCommand = "${ChartIqCommandNames.ON_CHART_STREAMING}($json);"
            logger.debug("JS input: $updateStreamingDataCommand")
            commandSink.sendStreamingUpdateCommand(updateStreamingDataCommand)
        }
    }

    override fun updateExtendedHoursState(isExtendedHours: Boolean) {
        val toggleValue = if (isExtendedHours) {
            Toggle.ON
        } else {
            Toggle.OFF
        }
        val extendedHoursToggleCommand = "${ChartIqCommandNames.TOGGLE_EXTENDED_HOUR}(\"$toggleValue\");"
        send(extendedHoursToggleCommand)
    }

    override fun forwardAlertCallback(alertId: String, callBack: String) {
        val alertCallbackCommand = "${ChartIqCommandNames.JS_BRIDGE_PREFIX}$callBack(\"$alertId\");"
        send(alertCallbackCommand)
    }

    override fun closeChartSettings() {
        val command = "${ChartIqCommandNames.CLOSE_SETTINGS}();"
        send(command)
    }

    override fun addSymbolToCompare(symbol: WithSymbolInfo) {
        val jsonAdapter: JsonAdapter<SymbolDto> = moshi.adapter(SymbolDto::class.java)
        val json = jsonAdapter.toJson(symbol.toDto())
        val command = "${ChartIqCommandNames.ON_SYMBOL_LOOKUP_CALLBACK}($json);"
        send(command)
    }

    override fun sendLoadedData(requestId: String, requestType: String, result: String?) {
        val command = "${ChartIqCommandNames.HANDLE_LOAD_DATA_RESPONSE}($requestId, $requestType, $result);"
        send(command)
    }

    private fun send(command: String) {
        logger.debug("JS input: $command")
        commandSink.sendCommand(command)
    }
}
