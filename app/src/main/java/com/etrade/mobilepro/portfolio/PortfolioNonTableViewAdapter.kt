package com.etrade.mobilepro.portfolio

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableData
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableRow
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.evrencoskun.tableview.adapter.AbstractTableAdapter
import com.evrencoskun.tableview.adapter.recyclerview.AbstractRecyclerViewAdapter
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

abstract class PortfolioNonTableViewAdapter(context: Context) :
    AbstractTableAdapter<Cell, Cell, Cell>(context) {

    abstract val layoutId: Int
        @LayoutRes get

    var data: PortfolioRecordsTableData =
        PortfolioRecordsTableData()
        set(value) {
                field = value
                columnMap = value.columns.mapIndexed { index, portfolioColumn ->
                    portfolioColumn to index
                }.toMap()
                updateAllItems()
            }

    var columnMap: Map<PortfolioColumn, Int> = mapOf()

    abstract fun getItems(): List<List<PortfolioRecordsTableRow>>

    fun updateAllItems(forceNotifyData: Boolean = false) {
        val items = getItems()
        cellRecyclerViewAdapter.setItems(items, forceNotifyData)
        updateVisibleRows(items)
    }

    override fun getCellItemViewType(row: Int, column: Int) = layoutId
    override fun getColumnHeaderItemViewType(column: Int) = R.layout.layout_empty
    override fun getRowHeaderItemViewType(row: Int) = R.layout.layout_empty
    override fun onCreateColumnHeaderViewHolder(parent: ViewGroup?, viewType: Int) =
        object : AbstractViewHolder(parent) {}

    override fun onBindColumnHeaderViewHolder(
        holder: AbstractViewHolder?,
        columnHeaderItemModel: Any?,
        columnPosition: Int
    ) {
        holder?.itemView?.isVisible = false
    }

    override fun onCreateRowHeaderViewHolder(
        parent: ViewGroup?,
        viewType: Int
    ) = object : AbstractViewHolder(parent) {}

    override fun onBindRowHeaderViewHolder(
        holder: AbstractViewHolder?,
        rowHeaderItemModel: Any?,
        rowPosition: Int
    ) {
        holder?.itemView?.isVisible = false
    }

    override fun onCreateCornerView(parent: ViewGroup) = View(parent.context)

    @Suppress("UNCHECKED_CAST")
    protected fun updateVisibleRows(cellItems: List<List<PortfolioRecordsTableRow>>) {
        tableView.cellLayoutManager.visibleCellRowRecyclerViews.forEach { rowRecyclerView ->
            val holderPosition = tableView.cellRecyclerView.getChildAdapterPosition(rowRecyclerView)

            if (holderPosition != RecyclerView.NO_POSITION) {
                val updatedRowItems = cellItems.getOrElse(holderPosition) {
                    emptyList()
                }
                val rowAdapter =
                    rowRecyclerView.adapter as AbstractRecyclerViewAdapter<PortfolioRecordsTableRow>
                val oldRowItems: List<PortfolioRecordsTableRow> = rowAdapter.items

                if (updatedRowItems != oldRowItems) {
                    val result: DiffUtil.DiffResult = DiffUtil.calculateDiff(
                        CellItemsDiffUtilCallback(
                            oldRowItems,
                            updatedRowItems
                        )
                    )
                    rowAdapter.setItems(updatedRowItems, false)
                    result.dispatchUpdatesTo(rowAdapter)
                }
            }
        }
    }

    private class CellItemsDiffUtilCallback(
        val oldCellItems: List<PortfolioRecordsTableRow>?,
        val newCellItems: List<PortfolioRecordsTableRow>?
    ) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = true

        override fun getOldListSize(): Int = oldCellItems?.size ?: 0

        override fun getNewListSize(): Int = newCellItems?.size ?: 0

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldCellItems?.get(oldItemPosition) == newCellItems?.get(newItemPosition)
        }
    }
}

internal fun List<PortfolioColumn>.toHeaders(
    row: PortfolioRecordsTableRow
): List<String> = mapIndexed { index, _ -> row.cells[index].displayText }
