package com.etrade.mobilepro.dagger

import androidx.navigation.NavController
import androidx.navigation.NavDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.dynamic.form.fragment.createFormValuesArgument
import com.etrade.mobilepro.home.KEY_HIDE_BOTTOM_BAR
import com.etrade.mobilepro.instrument.OptionType
import com.etrade.mobilepro.instrument.getOptionType
import com.etrade.mobilepro.navigation.EtNavController
import com.etrade.mobilepro.optionschain.presentation.OptionChainActionProvider
import com.etrade.mobilepro.optionschain.presentation.OptionChainActionProviderImpl
import com.etrade.mobilepro.optionschain.presentation.OptionsChainRouter
import com.etrade.mobilepro.optionschain.presentation.TradeNavArgs
import com.etrade.mobilepro.optionschains.api.Option
import com.etrade.mobilepro.optionschains.api.OptionsChainRepo
import com.etrade.mobilepro.optionschains.data.repo.DefaultOptionsChainRepoImpl
import com.etrade.mobilepro.optionschains.data.rest.OptionsChainApiClient
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.quote.screen.presentation.QuoteTabsFragmentDirections
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.START_DESTINATION
import com.etrade.mobilepro.trade.TRADE_OPTION_LEG_SELECT_FRAGMENT
import com.etrade.mobilepro.trade.api.option.OptionTradeStrategy
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.getTradeAction
import com.etrade.mobilepro.trade.optionlegselect.OptionLegSelectFragmentArgs
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Provider
import javax.inject.Singleton

@Module
abstract class OptionsChainModule {

    @Binds
    @Singleton
    abstract fun providesOptionsChainApiProvider(repo: DefaultOptionsChainRepoImpl): OptionsChainRepo

    @Binds
    @Singleton
    abstract fun providesOptionChainActionProvider(optionsChainActionProvider: OptionChainActionProviderImpl): OptionChainActionProvider

    companion object {

        @Provides
        @Singleton
        fun providesOptionsChainApiClient(@MobileTrade retrofit: Retrofit): OptionsChainApiClient = retrofit.create(OptionsChainApiClient::class.java)

        @Provides
        @Singleton
        fun provideOptionsChainRouter(
            moshi: Moshi,
            userViewModel: UserViewModel,
            tradingDefaultsPreferences: TradingDefaultsPreferences,
            tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>
        ): OptionsChainRouter = object : OptionsChainRouter {

            override fun openDisclosuresCharacteristicsAndRisks(navController: NavController) {
                navController.navigate(R.id.action_to_disclosure_characteristics_risks)
            }

            override fun navigateToQuoteDetails(navController: NavController, quoteTicker: SearchResultItem.Symbol) {
                val etNavController = EtNavController(navController, userViewModel)
                etNavController.navigate(QuoteTabsFragmentDirections.actionLaunchQuote(quoteTicker))
            }

            override fun navigateToOptionLegSelect(navArgs: TradeNavArgs) {
                val formValues = tradeFormParametersBuilderProvider.get()
                    .apply {
                        this.symbol = navArgs.symbol
                        accountId = userViewModel.currentSelectedAccount.value?.account?.accountId
                        action = navArgs.transactionType.getTradeAction()
                        optionTradeStrategy = getOptionTradeStrategy(navArgs.option, navArgs.transactionType)
                        priceType = tradingDefaultsPreferences.optionsPriceType
                        quantity = tradingDefaultsPreferences.optionsContracts.toBigDecimal()
                        term = tradingDefaultsPreferences.optionsTerm
                    }
                    .create(navArgs.securityType)

                val formBundle = createFormValuesArgument(moshi, formValues)

                val directions: NavDirections = if (navArgs.securityType == SecurityType.CONDITIONAL) {
                    QuoteTabsFragmentDirections.actionQuoteLaunchTrade(formBundle)
                } else {
                    object : NavDirections {
                        override fun getArguments() = OptionLegSelectFragmentArgs(formBundle).toBundle().apply {
                            putBoolean(KEY_HIDE_BOTTOM_BAR, true)
                            putString(START_DESTINATION, TRADE_OPTION_LEG_SELECT_FRAGMENT)
                        }

                        override fun getActionId(): Int = R.id.action_quote_launch_trade
                    }
                }
                navArgs.navController.navigate(directions)
            }

            override fun navigateToLogin(navController: NavController) {
                navController.navigate(com.etrade.mobilepro.optionschain.presentation.R.id.activity_login)
            }

            private fun getOptionTradeStrategy(
                option: Option,
                transactionType: TransactionType
            ): OptionTradeStrategy {
                return object : OptionTradeStrategy {
                    override val strategyType: StrategyType
                        get() = when (getOptionType(option.symbol)) {
                            OptionType.CALL -> StrategyType.CALL
                            OptionType.PUT -> StrategyType.PUT
                            else -> StrategyType.UNKNOWN
                        }

                    override val optionLegs: List<TradeLeg> =
                        listOf(
                            object : TradeLeg {
                                override val symbol: String = option.symbol
                                override val transactionType: TransactionType = transactionType
                                override val quantity: Int = tradingDefaultsPreferences.optionsContracts
                                override val displaySymbol: String = option.displaySymbol
                                override val isAMOption: Boolean = option.isAMOption
                            }
                        )
                }
            }
        }
    }
}
