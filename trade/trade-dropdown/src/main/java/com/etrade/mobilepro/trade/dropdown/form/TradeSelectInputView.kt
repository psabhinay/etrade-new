package com.etrade.mobilepro.trade.dropdown.form

import android.content.Context
import android.util.AttributeSet
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import com.etrade.mobilepro.dropdown.DropDownItemConverter
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.ui.AbstractSelectInputView
import com.etrade.mobilepro.trade.dropdown.TradeSelector

class TradeSelectInputView : AbstractSelectInputView<TradeSelectInput> {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun createDropDownManager(
        itemConverter: DropDownItemConverter<DropDownOption>,
        input: TradeSelectInput
    ) = TradeSelector(findFragment<Fragment>().childFragmentManager, resources, itemConverter).apply {
        init(
            "${TradeSelectInputView::class.java.name}/${input.id}",
            bottomSheetSelectorTitle,
            dropDownOptions,
            selectedOptionIndex,
            input.helpParams
        )
    }
}
