package com.etrade.mobilepro.brokerage

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.OnAccountsClickListener
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.home.ToolbarViewModel
import com.etrade.mobilepro.portfolio.PortfolioRecordsViewModel
import com.etrade.mobilepro.portfolio.PositionsState
import com.etrade.mobilepro.positions.PositionListFragment
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.tableviewutils.presentation.RestrictedTableView
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.etrade.mobilepro.util.android.logAppAction
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.evrencoskun.tableview.TableView
import javax.inject.Inject
import kotlin.reflect.KClass

const val ALL_POSITIONS_PATH = "/allPositions"

@RequireLogin
class AllBrokerageAccountsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    mainNavigation: MainNavigation,
    marketTradeStatusDelegate: MarketTradeStatusDelegate,
    darkModeChecker: DarkModeChecker
) : PositionListFragment<AllBrokerageAccountsViewModel>(
    viewModelFactory,
    snackbarUtilFactory,
    mainNavigation,
    marketTradeStatusDelegate,
    darkModeChecker
),
    OnAccountsClickListener {

    override val viewModelClass: KClass<AllBrokerageAccountsViewModel> =
        AllBrokerageAccountsViewModel::class

    private val toolbarViewModel: ToolbarViewModel by activityViewModels { viewModelFactory }

    private val View?.collapseExpandButton: TextView?
        get() = positionsHeaderView?.findViewById(R.id.collapseExpandButton)

    private val RestrictedTableView.portfolioRecordsViewModel: PortfolioRecordsViewModel?
        get() = portfolioRecordsViewModels.firstOrNull {
            it.accountName == findViewById<TextView>(R.id.accountName).text.toString()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logAppAction(activity?.intent, ALL_POSITIONS_PATH)
    }

    override fun saveScrollState() {
        view.positionsView?.getScrollState()?.let {
            positionListViewModel.saveScrollState(it)
        }
    }

    override fun onAccountsClick() {
        scrollToTop()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbarTitle()
    }

    override fun TableView.onDynamicViewAttached() {
        val button = positionsHeaderView.collapseExpandButton ?: return
        val viewModel = positionsView?.portfolioRecordsViewModel ?: return
        button.isVisible = true
        button.setOnClickListener { changePositionsState(button, viewModel) }
        if (viewModel.positionsState == PositionsState.COLLAPSED) {
            setCollapsedState(button, viewModel)
        } else {
            setExpandedState(button, viewModel)
        }
    }

    private fun setCollapsedState(
        button: TextView,
        viewModel: PortfolioRecordsViewModel
    ) {
        viewModel.positionsState = PositionsState.COLLAPSED
        button.apply {
            text = getString(R.string.expand)
        }
    }

    private fun setExpandedState(
        button: TextView,
        viewModel: PortfolioRecordsViewModel
    ) {
        viewModel.positionsState = PositionsState.EXPANDED
        button.apply {
            text = getString(R.string.collapse)
        }
    }

    private fun changePositionsState(
        button: TextView,
        viewModel: PortfolioRecordsViewModel
    ) {
        removeTableviewChildAttachStateChangeListener()
        if (viewModel.positionsState == PositionsState.COLLAPSED) {
            setExpandedState(button, viewModel)
        } else {
            setCollapsedState(button, viewModel)
        }
        addTableviewChildAttachStateChangeListener()
    }

    private fun setupToolbarTitle() {
        val title = getString(R.string.all_brokerage_title)
        val contentDescription = getString(R.string.label_dropdown_description, title)
        toolbarViewModel.manualToolbar(
            displayDropDownArrow = true,
            title = title,
            titleContentDescription = contentDescription
        )
    }
}
