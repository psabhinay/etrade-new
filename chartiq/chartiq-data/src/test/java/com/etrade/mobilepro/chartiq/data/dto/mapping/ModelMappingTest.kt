package com.etrade.mobilepro.chartiq.data.dto.mapping

import com.etrade.mobilepro.chartiq.api.model.Account
import com.etrade.mobilepro.chartiq.api.model.SymbolInfo
import com.etrade.mobilepro.chartiq.data.AlertDialogAction
import com.etrade.mobilepro.chartiq.data.AlertDialogContent
import com.etrade.mobilepro.chartiq.data.dto.AccountDto
import com.etrade.mobilepro.chartiq.data.dto.AlertActionDto
import com.etrade.mobilepro.chartiq.data.dto.AlertDialogContentDto
import com.etrade.mobilepro.chartiq.data.dto.AlertDto
import com.etrade.mobilepro.chartiq.data.dto.MarketDataDto
import com.etrade.mobilepro.chartiq.data.dto.SymbolDto
import com.etrade.mobilepro.chartiq.data.dto.SymbolInfoDto
import com.etrade.mobilepro.chartiq.data.dto.Toggle
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.streaming.api.Level1Data
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Test
import org.threeten.bp.LocalDate

class ModelMappingTest {

    @Test
    fun `Account toDto should transform correctly from Account to AccountDto`() {
        val id = "id"
        val displayName = "displayName"

        val account = Account(id, displayName)
        val accountDto: AccountDto = account.toDto()

        assertEquals("ids should be the same", account.id, accountDto.id)
        assertEquals("displayNames should be the same", account.displayName, accountDto.displayName)
    }

    @Test
    fun `Level1Data toMarketData should transform correctly from Level1Data to MarketData`() {
        val lastPriceAsString = "4.56"
        val lastPriceAsDouble = 4.56
        val volumeAsString = "456"
        val volumeAsLong = 456L
        val timestampSinceEpoch = 1000L
        val timestamp = 1L
        val givenTicker = "ticker"

        val level1Data: Level1Data =
            createLevel1Data(lastPriceAsString, volumeAsString, timestampSinceEpoch)
        val marketData: MarketDataDto? = level1Data.toMarketData(givenTicker)

        assertNotNull("markedData should not be null", marketData)
        assertEquals(
            "lastPrice should be the same",
            lastPriceAsDouble,
            marketData?.price ?: 123.456,
            0.01
        )
        assertEquals("volume should be the same", volumeAsLong, marketData?.volume)
        assertEquals("timeStamp should be calculated correctly", timestamp, marketData?.timestamp)
        assertEquals("ticker should be the same", givenTicker, marketData?.ticker)
    }

    @Test
    fun `Level1Data toMarketData should be null if lastPrice is null`() {
        val level1Data: Level1Data = createLevel1Data()
        val marketData: MarketDataDto? = level1Data.toMarketData("ticker")

        assertNull("marketData should be null", marketData)
    }

    @Test
    fun `SymbolInfo toDto should transform correctly from SymbolInfo to SymbolInfoDto`() {
        val givenTicker = "ticker"
        val price = "$12.34"
        val isExtendedHours = true
        val isExtendedHoursToggle = Toggle.ON
        val isStreaming = true
        val isStreamingToggle = Toggle.ON

        val symbolInfo = SymbolInfo(
            givenTicker,
            InstrumentType.BOND,
            price,
            isExtendedHours,
            isStreaming
        )
        val symbolInfoDto: SymbolInfoDto = symbolInfo.toDto()

        assertEquals("ticker should be the same", givenTicker, symbolInfoDto.ticker)
        assertEquals("security time should be the same", "BOND", symbolInfoDto.securityType)
        assertEquals(
            "isExtendedHours should match",
            isExtendedHoursToggle,
            symbolInfoDto.isExtendedHours
        )
        assertEquals("isStreaming should match", isStreamingToggle, symbolInfoDto.isExtendedHours)
        assertEquals("price should match", price, symbolInfoDto.price.price)
    }

    @Test
    fun `WithSymbolInfo toDto should transform correctly from WithSymbolInfo to SymbolDto`() {
        val givenSymbol = "symbol"
        val symbolInfo: WithSymbolInfo = object : WithSymbolInfo {
            override val instrumentType = InstrumentType.BOND
            override val symbol = givenSymbol
        }
        val symbolDto: SymbolDto = symbolInfo.toDto()

        assertEquals(
            "instrumentType should not change",
            InstrumentType.BOND,
            symbolDto.instrumentType
        )
        assertEquals("symbol should match", givenSymbol, symbolDto.symbol)
    }

    @Test
    fun `AlertActionDto toAlertAction should transform correctly from AlertActionDto to AlertDialogAction`() {
        val givenTitle = "title"
        val givenCallback = "callback"

        val actionDto = AlertActionDto(givenTitle, givenCallback)
        val alertAction: AlertDialogAction = actionDto.toAlertAction()

        assertEquals("title should match", givenTitle, alertAction.title)
        assertEquals("callback should match", givenCallback, alertAction.callback)
    }

    @Test
    fun `AlertDialogContentDto toAlertDialogContent should transform correctly from AlertDialogContentDto to AlertDialogContent`() {
        val givenAlertId = "alertId"
        val givenTitle = "title"
        val givenMessage = "message"
        val givenTitles = (1..3).map { "title$it" }
        val givenCallbacks = (1..3).map { "callback$it" }
        val givenAlertActions = (0..2).map { AlertActionDto(givenTitles[it], givenCallbacks[it]) }

        val alertDialogContentDto = AlertDialogContentDto(
            AlertDto(givenAlertId, givenTitle, givenMessage),
            givenAlertActions
        )
        val alertDialogContent: AlertDialogContent = alertDialogContentDto.toAlertDialogContent()

        assertEquals("alertId should match", givenAlertId, alertDialogContent.alertId)
        assertEquals("title should match", givenTitle, alertDialogContent.title)
        assertEquals("message should match", givenMessage, alertDialogContent.message)
        (alertDialogContent.positiveButton).let {
            assertEquals("action title should match", givenTitles[0], it?.title)
            assertEquals("action message should match", givenCallbacks[0], it?.callback)
        }
        (alertDialogContent.negativeButton).let {
            assertEquals("action title should match", givenTitles[1], it?.title)
            assertEquals("action message should match", givenCallbacks[1], it?.callback)
        }
        (alertDialogContent.neutralButton).let {
            assertEquals("action title should match", givenTitles[2], it?.title)
            assertEquals("action message should match", givenCallbacks[2], it?.callback)
        }
    }

    private fun createLevel1Data(
        lastPriceString: String? = null,
        lastVolumeString: String? = null,
        timestampSinceEpoch: Long? = null
    ): Level1Data {
        return object : Level1Data {
            override val ask: String?
                get() = TODO("Not yet implemented")
            override val askExchange: String?
                get() = TODO("Not yet implemented")
            override val askSize: String?
                get() = TODO("Not yet implemented")
            override val bid: String?
                get() = TODO("Not yet implemented")
            override val bidExchange: String?
                get() = TODO("Not yet implemented")
            override val bidSize: String?
                get() = TODO("Not yet implemented")
            override val change: String?
                get() = TODO("Not yet implemented")
            override val changePercent: String?
                get() = TODO("Not yet implemented")
            override val closingMark: String?
                get() = TODO("Not yet implemented")
            override val dayHigh: String?
                get() = TODO("Not yet implemented")
            override val dayLow: String?
                get() = TODO("Not yet implemented")
            override val earningsPerShare: String?
                get() = TODO("Not yet implemented")
            override val lastPrice: String?
                get() = lastPriceString
            override val lastPriceExchange: String?
                get() = TODO("Not yet implemented")
            override val lastVolume: String?
                get() = lastVolumeString
            override val mark: String?
                get() = TODO("Not yet implemented")
            override val openPrice: String?
                get() = TODO("Not yet implemented")
            override val previousClosePrice: String?
                get() = TODO("Not yet implemented")
            override val priceToEarnings: String?
                get() = TODO("Not yet implemented")
            override val timestampSinceEpoch: Long?
                get() = timestampSinceEpoch
            override val volume: String?
                get() = TODO("Not yet implemented")
            override val tickIndicator: Short?
                get() = TODO("Not yet implemented")
            override val yearHigh: String?
                get() = TODO("Not yet implemented")
            override val yearHighDate: LocalDate?
                get() = TODO("Not yet implemented")
            override val yearLow: String?
                get() = TODO("Not yet implemented")
            override val yearLowDate: LocalDate?
                get() = TODO("Not yet implemented")
        }
    }
}
