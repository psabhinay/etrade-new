package com.etrade.mobilepro.feedback

import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.feedback.api.FeedbackModel
import com.etrade.mobilepro.feedback.api.FeedbackRequest
import com.etrade.mobilepro.feedback.api.FeedbackRequestFactory
import com.etrade.mobilepro.feedback.api.Field
import com.etrade.mobilepro.feedback.api.Value
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import javax.inject.Inject

class DefaultFeedbackRequestFactory @Inject constructor(
    private val defaultAccountRepo: DefaultAccountRepo,
    private val streamingStatusController: StreamingStatusController,
    private val user: User,
    private val applicationInfo: ApplicationInfo
) : FeedbackRequestFactory {

    override suspend fun createFeedbackRequest(feedbackModel: FeedbackModel): FeedbackRequest {
        val recordTypeField = Field("IL_Record_Type__c", "Mobile_Customer_Feedback")
        val recordTypeIdField = Field("RecordTypeId", "Mobile Feedback")
        val accountIdField = Field("Account_Number__c", defaultAccountRepo.getAccountId().getOrNull() ?: "")
        val userIdField = Field("Customer_User_ID__c", user.getUserId()?.toString())
        val feedbackStarsField = Field("Stars__c", feedbackModel.stars.toString())
        val feedbackTextField = Field("Feedback__c", feedbackModel.feedbackText)
        val feedbackPageField = Field("Page__c", feedbackModel.page)
        val appVersionField = Field("App_Version__c", applicationInfo.appVersionCode())
        val operationSystemVersionField = Field("Operating_System_Version__c", applicationInfo.operationSystemVersion())
        val deviceDetailsField = Field("Device__c", applicationInfo.deviceDetails())
        val streamingField = Field("Streaming__c", streamingStatusController.isStreamingToggleEnabled.convertToString())
        val connectionField = Field("Connection__c", applicationInfo.connectionType())
        val platformField = Field("Platform__c", applicationInfo.platform())
        val sourceField = Field("Source__c", feedbackModel.source)
        val deviceIdField = Field("Device_Id__c", applicationInfo.deviceId())

        val fields = listOf(
            recordTypeField, recordTypeIdField, accountIdField,
            userIdField, feedbackStarsField, feedbackTextField,
            feedbackPageField, appVersionField, operationSystemVersionField,
            deviceDetailsField, streamingField, connectionField,
            platformField, sourceField, deviceIdField
        )

        return FeedbackRequest(value = Value(fields = fields, objectType = "Customer_Feedback__c", returnField = "Name__c"))
    }
}

private fun Boolean.convertToString() =
    if (this) {
        "Y"
    } else {
        "N"
    }
