package com.etrade.mobilepro.util.color

enum class DataColor {
    POSITIVE,
    NEGATIVE,
    NEUTRAL,
    FILLED_POSITIVE,
    FILLED_NEGATIVE
}
