package com.etrade.mobilepro.newaccount

import android.webkit.CookieManager
import android.webkit.WebView
import androidx.lifecycle.lifecycleScope
import com.etrade.mobilepro.BuildConfig
import com.etrade.mobilepro.common.di.DeviceId
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.di.OpenNewAccount
import com.etrade.mobilepro.eventtrackerapi.AppsFlyerRepo
import com.etrade.mobilepro.menu.MenuWebViewFragment
import com.etrade.mobilepro.menu.MenuWebViewModel
import com.squareup.moshi.Moshi
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val APPS_FLYER_ID_KEY = "AppsFlyerID"

class NewAccountWebViewFragment @Inject constructor(
    @DeviceType deviceType: String,
    @DeviceId private val deviceId: String,
    viewModelFactory: MenuWebViewModel.Factory,
    private val appsFlyerRepo: AppsFlyerRepo,
    private val moshi: Moshi,
    @OpenNewAccount private val openNewAccountUrl: String
) : MenuWebViewFragment(deviceType, viewModelFactory) {

    override fun getUrl(): String = openNewAccountUrl

    override fun setupWebView(webView: WebView) {
        super.setupWebView(webView)
        webView.addJavascriptInterface(CompleteOlaEventJSInterface(appsFlyerRepo, viewLifecycleOwner, moshi), OLA_EVENT_INTERFACE_NAME)
        setCookies(CookieManager.getInstance(), getUrl())
    }

    /*
    * Use function below instead once we don't need support devices before Android 7. Can not use both to avoid duplicated requests
    *
    * override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?) =
    *    interceptUrlIfNeeded(request?.url?.toString())
    * */
    override fun shouldOverrideUrlLoading(view: WebView?, url: String?) = interceptUrlIfNeeded(url)

    private fun interceptUrlIfNeeded(url: String?): Boolean {
        url ?: return false
        val event = analyticsOLAEvents.firstOrNull { url.contains(it.relativePath, true) } ?: return false

        viewLifecycleOwner.lifecycleScope.launch {
            appsFlyerRepo.trackEvent(event.name, emptyMap())
            webViewReference.loadUrl(url, mapOf(APPS_FLYER_ID_KEY to appsFlyerRepo.appsFlyerId))
        }
        return true
    }

    private fun setCookies(cookieManager: CookieManager, url: String) {
        val cookie = "deviceID=$deviceId; appsFlyerID=${appsFlyerRepo.appsFlyerId}; applicationID=${BuildConfig.APPLICATION_ID}; "
        cookie.split(";").forEach {
            cookieManager.setCookie(url, it)
        }
    }
}
