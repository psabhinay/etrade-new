package com.etrade.mobilepro.trade.presentation.form.delegate

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.InputHolder
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.TextInputType
import com.etrade.mobilepro.dynamic.form.api.inputParameterValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.livedata.combine
import com.etrade.mobilepro.livedata.mergeLatest
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quoteapi.FundsTradeQuoteItem
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesResult
import com.etrade.mobilepro.quoteapi.MutualPositionType
import com.etrade.mobilepro.trade.form.api.input.MutualFundTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.PositionTypeInputId
import com.etrade.mobilepro.trade.form.api.input.QuantityInputId
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.QUANTITY_MAX_FRACTION_DIGITS
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.safeParseBigDecimal

private fun getAmountFormatter(): (String) -> String {
    return { textValue ->
        MarketDataFormatter.formatMarketDataWithCurrency(textValue.safeParseBigDecimal())
    }
}

private fun InputHolder.selectedOptionHasValue(inputId: String, valueId: String): LiveData<Boolean> {
    return requireInput<SelectInput>(inputId).value.map {
        it?.id == valueId
    }
}

internal fun GroupBuilder.amountForPosition(
    resources: Resources,
    inputId: MutualFundTradeFormInputId,
    funds: LiveData<ConsumableLiveEvent<MutualFundsTradeQuotesResult>>
) {
    text(inputId.quantity.dollars.cash) {
        textInputType = TextInputType.CurrencyDecimal(QUANTITY_MAX_FRACTION_DIGITS)
        title = resources.getString(R.string.trade_hint_amount_for_cash)
        visibleIf {
            selectedOptionHasValue(inputId.positionTypeToTrade, inputId.amountToSellValueId.dollars)
        }
        formatter = getAmountFormatter()
    }
    text(inputId.quantity.dollars.margin) {
        textInputType = TextInputType.CurrencyDecimal(QUANTITY_MAX_FRACTION_DIGITS)
        title = resources.getString(R.string.trade_hint_amount_for_margin)
        visibleIf {
            selectedOptionHasValue(inputId.positionTypeToTrade, inputId.amountToSellValueId.dollars).combine(funds) { result, positions ->
                result == true && positions?.peekContent().hasMarginPosition()
            }
        }
        formatter = getAmountFormatter()
    }
}

internal fun GroupBuilder.sharesForPosition(
    resources: Resources,
    inputId: MutualFundTradeFormInputId,
    funds: LiveData<ConsumableLiveEvent<MutualFundsTradeQuotesResult>>
) {
    text(inputId.quantity.shares.cash) {
        textInputType = TextInputType.Decimal(QUANTITY_MAX_FRACTION_DIGITS)
        title = resources.getString(R.string.trade_hint_shares_for_cash)
        visibleIf { selectedOptionHasValue(inputId.positionTypeToTrade, inputId.amountToSellValueId.shares) }
    }
    text(inputId.quantity.shares.margin) {
        textInputType = TextInputType.Decimal(QUANTITY_MAX_FRACTION_DIGITS)
        title = resources.getString(R.string.trade_hint_shares_for_margin)
        visibleIf {
            selectedOptionHasValue(inputId.positionTypeToTrade, inputId.amountToSellValueId.shares).combine(funds) { result, positions ->
                result == true && positions?.peekContent().hasMarginPosition()
            }
        }
    }
}

internal fun GroupBuilder.investmentAmount(resources: Resources, inputId: MutualFundTradeFormInputId) {
    text(inputId.amount) {
        textInputType = TextInputType.CurrencyDecimal(QUANTITY_MAX_FRACTION_DIGITS)
        title = resources.getString(R.string.trade_hint_investment_amount)
        formatter = getAmountFormatter()
        visibleIf { selectedOptionHasValue(inputId.action, inputId.actionValueId.buy) }
    }
}

internal fun RuleListBuilder.resetAmountToSell(
    inputId: MutualFundTradeFormInputId,
    quotesWidgetViewModel: QuotesWidgetViewModel,
    previewDependIdsLiveData: MutableLiveData<Collection<String>>,
    previewDependIds: MutableList<String>
) {
    rule {
        inputParameterValue(inputId.positionTypeToTrade).observeForever {
            when (it) {
                inputId.amountToSellValueId.entirePosition -> {
                    onSellEntirePosition(inputId, quotesWidgetViewModel, previewDependIdsLiveData, previewDependIds)
                }
                inputId.amountToSellValueId.cashPosition -> {
                    previewDependIds.remove(inputId.entireMarginPosition)
                    previewDependIdsLiveData.value = getPreviewDependIds(previewDependIds, inputId, inputId.entireCashPosition)
                    onEntirePositionSelected(inputId, quotesWidgetViewModel, inputId.entireCashPosition)
                }
                inputId.amountToSellValueId.marginPosition -> {
                    previewDependIds.remove(inputId.entireCashPosition)
                    previewDependIdsLiveData.value = getPreviewDependIds(previewDependIds, inputId, marginInputId = inputId.entireMarginPosition)
                    onEntirePositionSelected(inputId, quotesWidgetViewModel, marginInputId = inputId.entireMarginPosition)
                }
                inputId.amountToSellValueId.dollars -> onDollarsTypeSelected(inputId, previewDependIdsLiveData, previewDependIds)
                inputId.amountToSellValueId.shares -> onSharesTypeSelected(inputId, previewDependIdsLiveData, previewDependIds)
            }
        }
    }
}

private fun InputHolder.onSellEntirePosition(
    inputId: MutualFundTradeFormInputId,
    quotesWidgetViewModel: QuotesWidgetViewModel,
    previewDependIdsLiveData: MutableLiveData<Collection<String>>,
    previewDependIds: MutableList<String>
) {
    previewDependIds.remove(inputId.positionTypeToTrade)
    previewDependIdsLiveData.value = getPreviewDependIds(previewDependIds, inputId, inputId.entireCashPosition, inputId.entireMarginPosition)
    onEntirePositionSelected(inputId, quotesWidgetViewModel, inputId.entireCashPosition, inputId.entireMarginPosition)
    if (requireAnyInput(inputId.entireMarginPosition).hasValue.value == false) {
        previewDependIds.remove(inputId.entireMarginPosition)
        previewDependIdsLiveData.value = previewDependIds
    }
}

internal fun RuleListBuilder.onQuantityFieldsInput(
    inputId: PositionTypeInputId,
    previewDependIds: MutableList<String>,
    previewDependIdsLiveData: MutableLiveData<Collection<String>>
) {
    rule {
        inputParameterValue(inputId.cash).mergeLatest(inputParameterValue(inputId.margin)).observeForever {
            onQuantityValueChanged(previewDependIds, inputId, it, previewDependIdsLiveData)
        }
    }
}

private fun onQuantityValueChanged(
    previewDependIds: MutableList<String>,
    positionId: PositionTypeInputId,
    cashMarginPair: Pair<Any?, Any?>,
    previewDependIdsLiveData: MutableLiveData<Collection<String>>
) {
    if (cashMarginPair.first != null || cashMarginPair.second != null) {
        // Cash value entered, remove Margin dependency
        cashMarginPair.first?.let { previewDependIds.remove(positionId.margin) }

        // Margin value entered, remove Cash dependency
        cashMarginPair.second?.let { previewDependIds.remove(positionId.cash) }
    } else {

        // No value entered, add Cash/Margin dependency
        previewDependIds.addQuantityFields(positionId)
    }
    previewDependIdsLiveData.value = previewDependIds
}

private fun getPreviewDependIds(
    previewDependIds: MutableList<String>,
    inputId: MutualFundTradeFormInputId,
    cashInputId: String? = null,
    marginInputId: String? = null
): MutableList<String> {
    previewDependIds.removeQuantityFields(inputId.quantity.dollars)
    previewDependIds.removeQuantityFields(inputId.quantity.shares)

    cashInputId?.let { previewDependIds.addDistinct(it) }
    marginInputId?.let { previewDependIds.addDistinct(it) }
    return previewDependIds
}

internal fun InputHolder.resetQuantityFields(quantity: QuantityInputId) {
    requireAnyInput(quantity.dollars.cash).resetValue()
    requireAnyInput(quantity.dollars.margin).resetValue()
    requireAnyInput(quantity.shares.cash).resetValue()
    requireAnyInput(quantity.shares.margin).resetValue()
}

private fun MutableList<String>.addQuantityFields(positionInputId: PositionTypeInputId) {
    addDistinct(positionInputId.cash)
    addDistinct(positionInputId.margin)
}

private fun MutableList<String>.removeQuantityFields(positionInputId: PositionTypeInputId) {
    remove(positionInputId.cash)
    remove(positionInputId.margin)
}

private fun InputHolder.onEntirePositionSelected(
    inputId: MutualFundTradeFormInputId,
    quotesWidgetViewModel: QuotesWidgetViewModel,
    cashInputId: String? = null,
    marginInputId: String? = null
) {
    requireAnyInput(inputId.entireMarginPosition).resetValue()
    requireAnyInput(inputId.entireCashPosition).resetValue()
    val funds = quotesWidgetViewModel.primaryFundsQuote.value?.peekContent()
    if (funds is FundsTradeQuoteItem?) {
        funds?.summaryItem?.positions?.forEach {
            if (marginInputId != null && it.type == MutualPositionType.MARGIN) {
                requireAnyInput(marginInputId).setParameterValue(it.quantity)
            }
            if (cashInputId != null && it.type == MutualPositionType.CASH) {
                requireAnyInput(cashInputId).setParameterValue(it.quantity)
            }
        }
    }
    resetQuantityFields(inputId.quantity)
}

private fun InputHolder.onDollarsTypeSelected(
    inputId: MutualFundTradeFormInputId,
    previewDependIdsLiveData: MutableLiveData<Collection<String>>,
    previewDependIds: MutableList<String>
) {
    previewDependIds.remove(inputId.entireCashPosition)
    previewDependIds.remove(inputId.entireMarginPosition)
    previewDependIds.addQuantityFields(inputId.quantity.dollars)
    previewDependIds.removeQuantityFields(inputId.quantity.shares)
    previewDependIdsLiveData.value = previewDependIds
    resetQuantityTypeFields(inputId)
}

private fun InputHolder.onSharesTypeSelected(
    inputId: MutualFundTradeFormInputId,
    previewDependIdsLiveData: MutableLiveData<Collection<String>>,
    previewDependIds: MutableList<String>
) {
    previewDependIds.addQuantityFields(inputId.quantity.shares)
    previewDependIds.removeQuantityFields(inputId.quantity.dollars)
    previewDependIds.remove(inputId.entireCashPosition)
    previewDependIds.remove(inputId.entireMarginPosition)
    previewDependIdsLiveData.value = previewDependIds
    resetQuantityTypeFields(inputId)
}

private fun InputHolder.resetQuantityTypeFields(
    inputId: MutualFundTradeFormInputId
) {
    resetQuantityFields(inputId.quantity)
    requireAnyInput(inputId.entireMarginPosition).resetValue()
    requireAnyInput(inputId.entireCashPosition).resetValue()
}

internal fun MutableList<String>.addDistinct(id: String) {
    if (this.contains(id).not()) this.add(id)
}

internal fun MutualFundsTradeQuotesResult.toFundsQuote(): FundsTradeQuoteItem? = when (this) {
    is FundsTradeQuoteItem -> this
    else -> null
}

internal fun MutualFundsTradeQuotesResult?.hasMarginPosition(): Boolean {
    val marginPosition = this?.toFundsQuote()?.summaryItem?.positions?.firstOrNull {
        it.type == MutualPositionType.MARGIN
    }
    return marginPosition != null
}
