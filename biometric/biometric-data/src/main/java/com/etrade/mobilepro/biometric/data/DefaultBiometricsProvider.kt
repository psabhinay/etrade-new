package com.etrade.mobilepro.biometric.data

import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators
import com.etrade.mobilepro.biometric.api.BiometricProviderError
import com.etrade.mobilepro.biometric.api.BiometricProviderResult
import com.etrade.mobilepro.biometric.api.BiometricsProvider
import com.etrade.mobilepro.encryption.api.EncryptionProvider
import javax.inject.Inject

class DefaultBiometricsProvider @Inject constructor(
    private val encryptionProvider: EncryptionProvider,
    private val biometricManager: BiometricManager
) : BiometricsProvider {

    override fun canAuthenticate(): BiometricProviderResult {
        return when (biometricManager.canAuthenticate(Authenticators.BIOMETRIC_WEAK)) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
                if (encryptionProvider.isSecureEncryptionSupported()) {
                    BiometricProviderResult(true)
                } else {
                    BiometricProviderResult(false, BiometricProviderError.ERROR_BIOMETRIC_NO_HW_BACKED_KEYSTORE)
                }
            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> BiometricProviderResult(false, BiometricProviderError.ERROR_HW_UNAVAILABLE)
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> BiometricProviderResult(false, BiometricProviderError.ERROR_NO_BIOMETRICS)
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> BiometricProviderResult(false, BiometricProviderError.ERROR_HW_NOT_PRESENT)
            else -> BiometricProviderResult(false, BiometricProviderError.ERROR_BIOMETRIC_GENERIC)
        }
    }
}
