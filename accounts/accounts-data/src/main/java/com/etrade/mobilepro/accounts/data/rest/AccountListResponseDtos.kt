package com.etrade.mobilepro.accounts.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class AccountListResponseHolderDto(
    @Json(name = "AccountListResponse")
    val holder: AccountListResponseDto?
)

@JsonClass(generateAdapter = true)
class AccountListResponseDto(
    @Json(name = "Summary")
    val summary: AccountListSummaryDto?,
    @Json(name = "Restricted")
    val restricted: AccountListRestrictedDto?,
    @Json(name = "Accounts")
    val holder: AccountsHolderDto?
)

@JsonClass(generateAdapter = true)
class AccountsHolderDto(
    @Json(name = "Account")
    val accounts: List<AccountDetailsDto>?
)

@JsonClass(generateAdapter = true)
class AccountListSummaryDto(
    @Json(name = "liabilityValue")
    val liabilityValue: Double?,
    @Json(name = "netAssetValue")
    val netAssetValue: Double?
)

@JsonClass(generateAdapter = true)
class AccountListRestrictedDto(
    @Json(name = "highRiskFlag")
    val highRiskFlag: Boolean?
)

@JsonClass(generateAdapter = true)
class AccountDetailsDto(
    @Json(name = "accountId")
    val accountId: String?,
    @Json(name = "accountIdKey")
    val accountIdKey: String?,
    @Json(name = "maskedAccountId")
    val maskedAccountId: String?,
    @Json(name = "accountMode")
    val accountMode: String?,
    @Json(name = "accountDesc")
    val accountDesc: String?,
    @Json(name = "accountName")
    val accountName: String?,
    @Json(name = "accountShortName")
    val accountShortName: String?,
    @Json(name = "accountLongName")
    val accountLongName: String?,
    @Json(name = "mobileDescription")
    val mobileDescription: String?,
    @Json(name = "accountValue")
    val accountValue: String?,
    @Json(name = "accountType")
    val accountType: String?,
    @Json(name = "instNo")
    val instNo: String?,
    @Json(name = "institutionType")
    val institutionType: String?,
    @Json(name = "accountStatus")
    val accountStatus: String?,
    @Json(name = "restrictionLevel")
    val restrictionLevel: String?,
    @Json(name = "closedDate")
    val closedDate: Long?,
    @Json(name = "Brokerage")
    val brokerage: AccountBrokerageDto?
)

@JsonClass(generateAdapter = true)
class AccountBrokerageDto(
    @Json(name = "cashAvailableForWithdrawal")
    val cashAvailableForWithdrawal: String?,
    @Json(name = "marginAvailableForWithdrawal")
    val marginAvailableForWithdrawal: String?,
    @Json(name = "purchasingPower")
    val purchasingPower: String?,
    @Json(name = "totalAvailableForWithdrawal")
    val totalAvailableForWithdrawal: String?,
    @Json(name = "marginBalance")
    val marginBalance: Double?,
    @Json(name = "netCashBalance")
    val netCashBalance: Double?,
    @Json(name = "cmAccount")
    val cmAccount: Boolean?,
    @Json(name = "contributionDetails")
    val contributionDetails: List<ContributionDetailDto>?
)

@JsonClass(generateAdapter = true)
class ContributionDetailDto(
    @Json(name = "contributionAmount")
    val contributionAmount: Double?,
    @Json(name = "taxYear")
    val taxYear: Int?
)
