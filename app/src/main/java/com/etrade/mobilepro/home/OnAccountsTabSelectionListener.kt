package com.etrade.mobilepro.home

interface OnAccountsTabSelectionListener {
    fun onTabSelected()
}
