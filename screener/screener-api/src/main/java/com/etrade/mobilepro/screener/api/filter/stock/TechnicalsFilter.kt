package com.etrade.mobilepro.screener.api.filter.stock

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

const val DEFAULT_TECHNICALS_PERCENT = "0"

data class TechnicalsFilter(
    val stock52WeekHigh: String = DEFAULT_TECHNICALS_PERCENT,
    val stock52WeekLow: String = DEFAULT_TECHNICALS_PERCENT
) : ScreenerFilter {
    val stock52WeekHighNotAvailable: Boolean
        get() = stock52WeekHigh.isEmpty() || stock52WeekHigh == DEFAULT_TECHNICALS_PERCENT
    val stock52WeekLowNotAvailable: Boolean
        get() = stock52WeekLow.isEmpty() || stock52WeekLow == DEFAULT_TECHNICALS_PERCENT

    override fun isEmpty(): Boolean = stock52WeekHighNotAvailable && stock52WeekLowNotAvailable

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.STOCK
}
