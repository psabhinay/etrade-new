package com.etrade.mobilepro.menu

import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.userviewmodel.UserViewModel
import javax.inject.Inject

class VerifyAccountBeforeWebViewDialogFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    userViewModel: UserViewModel
) : VerifyAccountBeforeNavDialogFragment(viewModelFactory, userViewModel) {

    private val args: VerifyAccountBeforeWebViewDialogFragmentArgs by navArgs()

    override fun getDirections(withLogin: Boolean): NavDirections {
        return getWebViewDirections(withLogin, args.webViewTitle, args.webViewUrl)
    }
}
