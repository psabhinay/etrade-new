package com.etrade.mobilepro.formsandapplications.presentation

import android.webkit.WebSettings
import android.webkit.WebView
import androidx.core.app.ActivityCompat
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

@RequireLogin
class AccountServicesWebViewFragment @Inject constructor(
    @DeviceType deviceType: String,
    snackbarUtilFactory: SnackbarUtilFactory
) : WebViewFragment(deviceType) {

    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    private val webChromeClient by lazy {
        AccountServicesWebChromeClient(
            context = requireContext(),
            registry = requireActivity().activityResultRegistry,
            lifecycleOwner = viewLifecycleOwner,
            requestPermissionCallback = { permission, requestCode ->
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(permission),
                    requestCode
                )
            },
            errorCallback = {
                snackbarUtil.errorSnackbar()
            }
        )
    }

    override fun setupWebView(webView: WebView) {
        super.setupWebView(webView)
        webView.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
        webView.webChromeClient = webChromeClient
    }
}
