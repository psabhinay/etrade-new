package com.etrade.mobilepro.movers.markets

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.movers.EmptyQuotesException
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.util.executeBlocking
import kotlinx.coroutines.rx2.awaitLast
import javax.inject.Inject

interface GetMarketMoversUseCase : UseCase<Unit, ETResult<List<MobileQuote>>>

class GetMarketMoversUseCaseImpl @Inject constructor(
    private val marketMoversService: MoverService,
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase,
    private val quoteRepo: MobileQuoteRepo
) : GetMarketMoversUseCase {

    override suspend fun execute(parameter: Unit): ETResult<List<MobileQuote>> {
        return runCatchingET {
            val quotes = marketMoversService.getMarketMovers()
                .toObservable()
                .awaitLast()
                .quotes

            if (quotes.isNullOrEmpty()) {
                throw EmptyQuotesException()
            }

            val symbols = quotes.joinToString(",") { it.symbol ?: "" }
            val isExtendedHours = isExtendedHoursOnUseCase.executeBlocking()
            quoteRepo.loadMobileQuotes(symbols, extendedHours = isExtendedHours).awaitLast()
        }
    }
}
