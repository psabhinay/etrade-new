package com.etrade.mobilepro.chartiq.presentation

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.View
import android.webkit.CookieManager
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.baseactivity.ActivityNetworkConnectionDelegate
import com.etrade.mobilepro.chartiq.data.JAVASCRIPT_INTERFACE_NAME
import com.etrade.mobilepro.chartiq.presentation.ChartIqViewModel.ChartSettingsState
import com.etrade.mobilepro.chartiq.presentation.databinding.ChartiqFragmentChartiqBinding
import com.etrade.mobilepro.chartiq.presentation.util.getSymbol
import com.etrade.mobilepro.chartiq.presentation.util.userAgentDeviceType
import com.etrade.mobilepro.dialog.showDialog
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.searchingapi.EXTRA_SELECTED_SYMBOL
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.android.binding.dataBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.goneIf
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.BlockingLoadingIndicatorViewDelegate
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import org.slf4j.LoggerFactory
import javax.inject.Inject

class ChartIqFragment @Inject constructor(
    private val applicationInfo: ApplicationInfo,
    private val searchIntentFactory: SearchIntentFactory,
    private val activityNetworkConnectionDelegate: ActivityNetworkConnectionDelegate,
    private val router: ChartIqRouter,
    assistedModelFactory: ChartIqViewModelFactoryFactory,
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.chartiq_fragment_chartiq) {

    private val binding by dataBinding(ChartiqFragmentChartiqBinding::bind) { onDestroyBinding() }

    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private var snackBar: Snackbar? = null
    private val quoteWidgetViewModel by viewModels<QuotesWidgetViewModel> { viewModelFactory }
    private val symbol by lazy { arguments?.getSymbol() ?: throw IllegalArgumentException() }
    private val viewModel: ChartIqViewModel by viewModels { assistedModelFactory.create(symbol, null) }
    private val toolbarViewModel: ChartIqToolbarViewModel by activityViewModels { viewModelFactory }
    private val blockingLoadingIndicatorDelegate by lazy {
        BlockingLoadingIndicatorViewDelegate(
            viewModel,
            childFragmentManager
        )
    }

    @ExperimentalCoroutinesApi
    private val symbolLookupRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                (result.data?.extras?.get(EXTRA_SELECTED_SYMBOL) as? SearchResultItem.Symbol)?.let {
                    viewModel.addSymbolToCompare(it)
                }
            }
        }

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.obj = quoteWidgetViewModel
        activity?.let { activityNetworkConnectionDelegate.initNetworkConnectivityBanner(it) }
        activity?.onBackPressedDispatcher?.addCallback {
            onBackPressed()
        }
        blockingLoadingIndicatorDelegate.observe(viewLifecycleOwner)
        bindQuoteWidgetViewModel()
        bindChartIqViewModel()
        lifecycle.addObserver(viewModel)
    }

    @ExperimentalCoroutinesApi
    @SuppressLint("SetJavaScriptEnabled")
    @Suppress("DEPRECATION")
    private fun setUpWebView() {
        setUpWebViewClient()
        binding.webView.settings.apply {
            javaScriptEnabled = true
            builtInZoomControls = true
            domStorageEnabled = true
            databaseEnabled = true
            userAgentString = applicationInfo.userAgentDeviceType
            allowUniversalAccessFromFileURLs = true
            allowContentAccess = false
            allowFileAccess = false
        }
        binding.webView.addJavascriptInterface(viewModel.chartIqJavascriptBridge, JAVASCRIPT_INTERFACE_NAME)
        viewModel.onJavascriptInterfaceAdded()
    }

    private fun setUpWebViewClient() {
        binding.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?) =
                false

            override fun onPageFinished(view: WebView?, url: String?) {
                view?.clearHistory()
            }

            override fun onReceivedHttpError(
                view: WebView?,
                request: WebResourceRequest?,
                errorResponse: WebResourceResponse?
            ) {
                super.onReceivedHttpError(view, request, errorResponse)
                val cookie = request?.url?.let {
                    CookieManager.getInstance().getCookie(it.toString())
                }

                val data = "Chart IQ webview network stats: \n" +
                    "Path : ${request?.url}" +
                    "Status : ${errorResponse?.statusCode} \n" +
                    "reason : ${errorResponse?.reasonPhrase} \n" +
                    "cookie : $cookie"
                "Response headers = ${errorResponse?.responseHeaders?.entries} \n" +
                    "Request headers = ${request?.requestHeaders?.entries}"
                viewModel.recordLogs(data)
            }
        }
    }

    private fun loadChartIq() {
        binding.webView.loadUrl("file:///android_asset/chartiq.html")
    }

    @ExperimentalCoroutinesApi
    @SuppressWarnings("LongMethod")
    private fun bindChartIqViewModel() {
        viewModel.chartIqState.observe(
            this@ChartIqFragment.viewLifecycleOwner,
            Observer {
                handleChartIqStateUpdate(it)
            }
        )
        viewModel.dialog.observe(
            this@ChartIqFragment.viewLifecycleOwner,
            Observer { dialog ->
                dialog?.let { showDialog(it) }
            }
        )
        viewModel.showSymbolLookup.observe(
            this@ChartIqFragment.viewLifecycleOwner,
            Observer { event ->
                event.consume { params -> lookupSymbol(params) }
            }
        )
        viewModel.chartSettingsState.observe(
            this@ChartIqFragment.viewLifecycleOwner,
            Observer { state ->
                handleSettingsState(state)
            }
        )
        viewModel.errorMessage.observe(
            this@ChartIqFragment.viewLifecycleOwner,
            Observer { event ->
                event.consume { showGenericError(it) }
            }
        )
        viewModel.showLogin.observe(
            this@ChartIqFragment.viewLifecycleOwner,
            Observer { event ->
                event.consume { router.showLogin(activity, symbol) }
            }
        )
        viewModel.editOrder.observe(
            this@ChartIqFragment.viewLifecycleOwner,
            Observer { event ->
                event.consume { router.showEditOrderScreen(activity, it) }
            }
        )
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.chartIqCommands.collect { event ->
                event.consume { command ->
                    if (command.isNotBlank()) {
                        binding.webView.evaluateJavascript(command, null)
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.chartIqStreamingUpdateCommands.collect { event ->
                event.consume { command ->
                    if (command.isNotBlank()) {
                        binding.webView.evaluateJavascript(command, null)
                    }
                }
            }
        }
    }

    private fun showGenericError(message: ErrorMessage) {
        val retryAction = message.retryAction ?: return
        snackBar =
            snackbarUtil.retrySnackbar(message = message.message, actionOnRetry = retryAction)
                ?.apply { show() }
    }

    @ExperimentalCoroutinesApi
    private fun lookupSymbol(params: StartSearchParams) {
        activity?.let {
            val intent = searchIntentFactory.createIntent(it, params, true)
            symbolLookupRequest.launch(intent)
        }
    }

    @ExperimentalCoroutinesApi
    private fun handleChartIqStateUpdate(state: ChartIqState) {
        when (state) {
            is ChartIqState.None -> setUpWebView()
            is ChartIqState.Initializing -> loadChartIq()
            is ChartIqState.Reload -> binding.webView.reload()
            is ChartIqState.Ready -> logger.debug("ChartIQ is ready")
            is ChartIqState.Error -> handleError()
        }
    }

    @ExperimentalCoroutinesApi
    private fun handleSettingsState(state: ChartSettingsState) {
        val settingsOpen = state == ChartSettingsState.OPEN
        binding.quoteWidgetLayout?.root?.goneIf(settingsOpen)
        toolbarViewModel.setToolbarUpdateAction(
            if (settingsOpen) {
                ToolbarUpdateAction(false, resources.getString(R.string.chartiq_title_settings)) {
                    viewModel.closeSettings()
                }
            } else {
                ToolbarUpdateAction(true, resources.getString(R.string.chartiq_title_chart)) {
                    activity?.finish()
                }
            }
        )
    }

    private fun bindQuoteWidgetViewModel() {
        val progressBar: ContentLoadingProgressBar =
            binding.root.findViewById(R.id.quote_widget_loading_indicator)
        quoteWidgetViewModel.getQuotes(symbol.title)
        quoteWidgetViewModel.loadingIndicator.observe(
            this@ChartIqFragment.viewLifecycleOwner,
            Observer {
                snackBar?.dismiss()
                if (it) {
                    progressBar.show()
                } else {
                    progressBar.hide()
                }
            }
        )
        quoteWidgetViewModel.errorMessage.observe(
            this@ChartIqFragment.viewLifecycleOwner,
            Observer {
                progressBar.hide()
                handleError()
            }
        )
    }

    private fun handleError() {
        if (snackBar?.isShown != true) {
            snackBar = snackbarUtil.retrySnackbar {
                val chartIqState = viewModel.chartIqState.value
                if (chartIqState is ChartIqState.Error) {
                    chartIqState.retryAction()
                }
                quoteWidgetViewModel.errorMessage.value?.consume {
                    quoteWidgetViewModel.getQuotes(symbol.title)
                }
            }
            snackBar?.show()
        }
    }

    @ExperimentalCoroutinesApi
    private fun onBackPressed() {
        if (viewModel.isSettingsOpen()) {
            viewModel.closeSettings()
        } else {
            activity?.finish()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        lifecycle.removeObserver(viewModel)
    }

    private fun ChartiqFragmentChartiqBinding.onDestroyBinding() {
        if (activity?.isChangingConfigurations == false) {
            webView.releaseWebView()
            viewModel.onWebViewDestroyed()
        }
    }

    private fun WebView.releaseWebView() {
        // Following line removes JavaScriptInterface but that doesn't affect
        // currently running JavaScript until the next page is loaded.
        removeJavascriptInterface(JAVASCRIPT_INTERFACE_NAME)
        // To actually stop JavaScript from running
        // and release page resources we need to load an empty page.
        loadUrl("about:blank")
        // Only after the JavaScript is stopped we destroy it
        destroy()
    }
}
