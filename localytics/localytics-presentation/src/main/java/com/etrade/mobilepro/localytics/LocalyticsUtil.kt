package com.etrade.mobilepro.localytics

import android.app.Application
import com.localytics.android.CustomLocalyticsActivityLifecycleCallbacks
import com.localytics.androidx.Localytics

fun integrateLocalytics(application: Application, localyticsActivityLifecycleCallbacks: CustomLocalyticsActivityLifecycleCallbacks) {
    application.registerActivityLifecycleCallbacks(localyticsActivityLifecycleCallbacks)
    Localytics.integrate(application)
}
