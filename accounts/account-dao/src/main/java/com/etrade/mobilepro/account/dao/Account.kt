package com.etrade.mobilepro.account.dao

import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobile.accounts.dto.InstitutionType
import com.etrade.mobile.accounts.dto.ManagedAccountType
import com.etrade.mobilepro.util.enumValueOrDefault
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Represents user's account
 *
 * @property encAccountId encrypted account id, which is used in some network request, like fetching account-related news
 * @property fetchedAt Timestamp in milliseconds denoting when this account was fetched from network.
 *                     This is used when making a decision about renewing the cached account
 */
open class Account(
    @PrimaryKey
    var accountUuid: String = "",
    var accountId: String = "",
    var accountMode: String = "",
    var accountDescription: String = "",
    var accountShortName: String = "",
    var accountLongName: String = "",
    var _accountType: String = "",
    var _institutionType: String = "",
    var restrictionLevel: String? = null,
    var cashAvailableForWithdrawal: String? = null,
    var marginAccountFlag: Boolean = false,
    var marginAvailableForWithdrawal: String? = null,
    var purchasingPower: String? = null,
    var totalAvailableForWithdrawal: String? = null,
    var funded: Boolean = false,
    var ledgerAccountValue: String? = null,
    var accountValue: String? = null,
    var encAccountId: String? = null,
    var optionLevel: Int? = null,
    var fetchedAt: Long = 0,
    var accountIndex: String? = "",
    var availableBalance: String? = null,
    var daysGain: String? = null,
    var isIra: Boolean? = null,
    var totalBalance: String? = null,
    var isAccountStreamingAllowed: Boolean = false,
    var fundingPrompt: String? = null,
    var _managedAccountType: String = ""
) : RealmObject() {

    var accountType: AccountType
        get() = enumValueOrDefault(_accountType, AccountType.Unknown)
        set(value) {
            _accountType = value.name
        }

    var managedAccountType: ManagedAccountType
        get() = enumValueOrDefault(_managedAccountType, ManagedAccountType.Unknown)
        set(value) {
            _managedAccountType = value.name
        }

    var institutionType: InstitutionType
        get() = enumValueOrDefault(_institutionType, InstitutionType.UNKNOWN)
        set(value) {
            _institutionType = value.name
        }

    fun isRetirementAccount(): Boolean =
        accountShortName.contains(Regex("\\s+IRA\\s+")) ||
            accountShortName.contains(Regex("\\s+ESA\\s+")) ||
            accountShortName.contains(Regex("\\s+UTMA/UGMA\\s+"))

    fun isStockPlan(): Boolean = institutionType == InstitutionType.OLINK

    fun isBankAccount(): Boolean = institutionType == InstitutionType.TELEBANK

    fun isManagedAccount(): Boolean = accountType == AccountType.Managed

    fun isBrokerageAccount(): Boolean = !isManagedAccount() && !isBankAccount() && !isStockPlan()
}
