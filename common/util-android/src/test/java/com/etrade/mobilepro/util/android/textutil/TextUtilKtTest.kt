package com.etrade.mobilepro.util.android.textutil

import android.text.Html
import android.text.Spannable
import android.text.style.URLSpan
import com.etrade.mobilepro.util.android.textutil.TextUtil.Companion.convertTextWithHmlTagsToSpannable
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class TextUtilKtTest {

    @Test
    fun updateTextPaintForUnderlineSpans_EmptyInput() {
        val testString = ""

        val result = Html.fromHtml(testString, Html.FROM_HTML_MODE_LEGACY) as Spannable
        result.updateTextPaintForUrlSpans(false)

        val totalUrlSpans = result.getSpans(0, result.length, URLSpan::class.java).size

        Assert.assertEquals(0, totalUrlSpans.toLong())
    }

    @Test
    fun updateTextPaintForUnderlineSpans_singleOccurrenceInput() {
        val testString = "hello <a href=\"https://www.google.com\">google link</a>"
        val underlinedStatus = false
        val result = Html.fromHtml(testString, Html.FROM_HTML_MODE_LEGACY) as Spannable

        result.updateTextPaintForUrlSpans(underlinedStatus)

        val totalUrlSpans = result.getSpans(0, result.length, URLSpan::class.java).size
        val totalConfiguredUnderlineSpans = result.getSpans(0, result.length, UnderlineSpanWithParam::class.java).size
        Assert.assertEquals(1, totalUrlSpans.toLong())
        Assert.assertEquals(1, totalConfiguredUnderlineSpans.toLong())

        for (spanWithParam in result.getSpans(0, result.length, UnderlineSpanWithParam::class.java)) {
            Assert.assertEquals(underlinedStatus, spanWithParam.textIsUnderlined())
        }
    }

    @Test
    fun updateTextPaintForUnderlineSpans_multiOccurrenceInput() {
        val testString = "hello <a href=\"https://www.google.com\">google link</a>" + " hello <a href=\"https://www.google.com\">google link</a>"
        val underlinedStatus = false
        val result = Html.fromHtml(testString, Html.FROM_HTML_MODE_LEGACY) as Spannable

        result.updateTextPaintForUrlSpans(underlinedStatus)

        val totalUrlSpans = result.getSpans(0, result.length, URLSpan::class.java).size
        val totalConfiguredUnderlineSpans = result.getSpans(0, result.length, UnderlineSpanWithParam::class.java).size
        Assert.assertEquals(2, totalUrlSpans.toLong())
        Assert.assertEquals(2, totalConfiguredUnderlineSpans.toLong())

        for (spanWithParam in result.getSpans(0, result.length, UnderlineSpanWithParam::class.java)) {
            Assert.assertEquals(underlinedStatus, spanWithParam.textIsUnderlined())
        }
    }

    @Test
    fun convertTextWithHmlTagsToSpannableTest() {
        var result = convertTextWithHmlTagsToSpannable("&amp;").toString()
        Assert.assertEquals(result, "&")
        result = convertTextWithHmlTagsToSpannable("&#40;&#41;").toString()
        Assert.assertEquals(result, "()")
        result = convertTextWithHmlTagsToSpannable("&#233;").toString()
        Assert.assertEquals(result, "é")
        result = convertTextWithHmlTagsToSpannable("&#225;").toString()
        Assert.assertEquals(result, "á")
        result = convertTextWithHmlTagsToSpannable("&#160;").toString()
        Assert.assertEquals(result, " ")
        result = convertTextWithHmlTagsToSpannable("&#252;").toString()
        Assert.assertEquals(result, "ü")
        result = convertTextWithHmlTagsToSpannable("&#231;").toString()
        Assert.assertEquals(result, "ç")
        result = convertTextWithHmlTagsToSpannable("&#227;").toString()
        Assert.assertEquals(result, "ã")
        result = convertTextWithHmlTagsToSpannable("&#203;").toString()
        Assert.assertEquals(result, "Ë")
    }
}
