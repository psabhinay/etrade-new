package com.etrade.mobilepro.util.android.accessibility

import android.view.View
import android.view.View.NO_ID
import android.view.accessibility.AccessibilityEvent
import androidx.core.view.AccessibilityDelegateCompat
import androidx.core.view.ViewCompat
import com.etrade.mobilepro.util.android.R
import java.util.Timer
import java.util.TimerTask
import kotlin.concurrent.timerTask

private const val ACCESSIBILITY_DEBOUNCE = 10000L
private const val ACCESSIBILITY_FOCUS_DELAY = 300L
private const val NO_VALID_ACTION = 0

private var lastTimer: TimerTask? = null
private var isLastEventAnnounced = false
private var debounceTime: Long? = null
private var lastEvent: String = ""

private val View.isAccessible get() = isAccessibilityFocused && context.accessibilityEnabled
private val isAnnouncementAllowed get() = lastEvent.isNotBlank() && !isLastEventAnnounced

private val defaultDelegate = object : AccessibilityDelegateCompat() {

    override fun sendAccessibilityEvent(host: View, eventType: Int) {
        if (eventType == AccessibilityEvent.TYPE_VIEW_HOVER_EXIT || eventType == AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED) {
            cancelTimer()
            val eventText = host.getTag(R.id.accessibility_user_touch_text_tag) ?: host.getTag(R.id.accessibility_text_tag)
            val eventAction = host.getTag(R.id.accessibility_action_tag) ?: ""
            lastEvent = "$eventText $eventAction"
            isLastEventAnnounced = false
            host.sendAnnouncementEventIfAny()
        } else {
            logger.debug("sendAccessibilityEvent: ${AccessibilityEvent.eventTypeToString(eventType)} ,host:$host")
            super.sendAccessibilityEvent(host, eventType)
        }
    }
}

/**
 * Call when [accessibilityText] is changed. Make sure the view has a defined ID before calling this.
 * [defaultDelegate] will be assigned for this view to handle manual events (e.g. user's clicks)
 * Do not update ContentDescription attribute if the view is accessibility focused and timer is active to avoid event conflicts.
 *
 * @param accessibilityText text that accessibility manager should announce
 * @param accessibilityAction action text that will be pronounced after [accessibilityText] only on manual events in [defaultDelegate]
 * @param debounce debounce time before next event announcement
 * @param accessibilityUserTouchText text that accessibility manager should announce when user touches the view.
 * Set this argument if it is different from [accessibilityText]
 */
fun View.processAccessibilityStreamingEvent(
    accessibilityText: String,
    accessibilityAction: String? = null,
    debounce: Long? = ACCESSIBILITY_DEBOUNCE,
    accessibilityUserTouchText: String? = null
) {
    setDelegateIfNeeded()

    setTag(R.id.accessibility_text_tag, accessibilityText)
    setTag(R.id.accessibility_action_tag, accessibilityAction)
    setTag(R.id.accessibility_user_touch_text_tag, accessibilityUserTouchText)

    if (isAccessible) {
        isLastEventAnnounced = false
        lastEvent = accessibilityText
        if (lastTimer == null) {
            debounceTime = debounce
            sendAnnouncementEventIfAny()
        } else if (debounceTime != debounce) {
            cancelTimer()
            debounceTime = debounce
            setTimer()
        }
    } else {
        contentDescription = accessibilityUserTouchText ?: accessibilityText
    }
}

private fun View.setDelegateIfNeeded() {
    if (ViewCompat.getAccessibilityDelegate(this) != defaultDelegate) {
        if (id == NO_ID) {
            logger.error("Accessibility streaming error", IllegalArgumentException("view must have a unique id set!"))
        }
        ViewCompat.setAccessibilityDelegate(this, defaultDelegate)
    }
}

private fun cancelTimer() {
    lastTimer?.cancel()
    lastTimer = null
}

private fun View.setTimer() {
    lastTimer = Timer(false).schedule(debounceTime ?: ACCESSIBILITY_DEBOUNCE) {
        // execute on UI thread
        lastTimer = null
        handler?.post {
            if (isAccessible) {
                sendAnnouncementEventIfAny()
            }
        }
    }
}

private fun View.sendAnnouncementEventIfAny() {
    if (isAnnouncementAllowed && isShown && isAccessible) {
        context.accessibilityManager?.apply {
            announceForAccessibility(lastEvent)
            isLastEventAnnounced = true
            setTimer()
        }
    }
}

private inline fun Timer.schedule(delay: Long, crossinline action: TimerTask.() -> Unit): TimerTask {
    val task = timerTask(action)
    schedule(task, delay)
    return task
}
