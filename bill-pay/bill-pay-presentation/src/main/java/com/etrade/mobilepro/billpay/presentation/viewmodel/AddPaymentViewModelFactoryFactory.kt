package com.etrade.mobilepro.billpay.presentation.viewmodel

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.presentation.addpayment.AddPaymentViewModel
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

@SuppressWarnings("LongParameterList")
interface AddPaymentViewModelFactoryFactory {
    fun create(
        validAccountList: List<AccountDetails>,
        payeeList: List<Payee>,
        originAccount: String?,
        destinationPayeeId: String?
    ): ViewModelProvider.Factory
}

class AddPaymentViewModelFactoryFactoryImp @Inject constructor(
    private val factory: AddPaymentViewModel.Factory
) : AddPaymentViewModelFactoryFactory {
    override fun create(
        validAccountList: List<AccountDetails>,
        payeeList: List<Payee>,
        originAccount: String?,
        destinationPayeeId: String?
    ): ViewModelProvider.Factory =
        viewModelFactory {
            factory.create(
                validAccountList,
                payeeList,
                originAccount,
                destinationPayeeId
            )
        }
}
