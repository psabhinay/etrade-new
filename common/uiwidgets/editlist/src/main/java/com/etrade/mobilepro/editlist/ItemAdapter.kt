package com.etrade.mobilepro.editlist

import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.editlist.databinding.ItemEditListBinding
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.etrade.mobilepro.util.android.extension.viewBinding

open class ItemAdapter(
    private val viewModel: EditListViewModel<*>,
    private val itemTouchHelper: ItemTouchHelper,
    private val isDeletableItem: Boolean,
    private val isToggleableItem: Boolean,
    private val isDragFinished: () -> Boolean
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items: List<EditListItem> = emptyList()
        set(value) {
            dispatchUpdates(
                oldItems = field, newItems = value,
                callback = object : DiffUtil.Callback() {
                    override fun getOldListSize(): Int = field.size
                    override fun getNewListSize(): Int = value.size
                    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
                        field[oldItemPosition].displayText == value[newItemPosition].displayText
                    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = field[oldItemPosition] == value[newItemPosition]
                }
            )
            field = value
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        bindItemType(holder as ItemEditListViewHolder, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ItemEditListViewHolder(parent.viewBinding(ItemEditListBinding::inflate))
    }

    override fun getItemCount(): Int = items.size

    @Suppress("ClickableViewAccessibility")
    private fun bindDragHandle(holder: ItemViewHolder) {
        holder.dragHandle.setOnTouchListener { _, event ->
            if (event.actionMasked == MotionEvent.ACTION_DOWN && items.size > 1 && isDragFinished()) {
                itemTouchHelper.startDrag(holder)
                true
            } else {
                false
            }
        }
    }

    private fun View.setLeftPadding(@DimenRes resourceId: Int) {
        val left = if (resourceId != 0) {
            resources.getDimensionPixelOffset(resourceId)
        } else {
            0
        }
        setPadding(left, 0, 0, 0)
    }

    fun bindItemType(holder: ItemViewHolder, position: Int) {
        holder.apply {
            bindTo(items[position])

            removeView?.apply {
                visibility = if (isDeletableItem) {
                    setOnClickListener {
                        viewModel.removeItemAt(holder.bindingAdapterPosition)
                        setOnClickListener(null)
                    }
                    holder.titleView.setLeftPadding(0)
                    View.VISIBLE
                } else {
                    setOnClickListener(null)
                    holder.titleView.setLeftPadding(R.dimen.spacing_medium)
                    View.GONE
                }
            }

            toggleView?.apply {
                visibility = if (isToggleableItem) {
                    setOnCheckedChangeListener { _, _ -> isChecked = viewModel.toggleItemAt(holder.bindingAdapterPosition) }

                    View.VISIBLE
                } else {
                    View.GONE
                }
            }

            bindDragHandle(this)
        }
    }
}
