package com.etrade.mobilepro.quotelookup

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.etrade.mobilepro.R
import com.etrade.mobilepro.quotelookup.options.call.StockCallOptionLookupFragment
import com.etrade.mobilepro.quotelookup.options.put.StockPutOptionLookupFragment
import com.etrade.mobilepro.quotelookup.stock.StockQuoteLookupFragment
import com.etrade.mobilepro.quotelookup.stock.StockQuoteSearchFragment

sealed class QuoteLookupTab(@StringRes val title: Int?, val fragmentClass: Class<out Fragment>) {

    object StockQuote : QuoteLookupTab(R.string.stock_tab, StockQuoteLookupFragment::class.java)
    object StockCallOption : QuoteLookupTab(R.string.call_option, StockCallOptionLookupFragment::class.java)
    object StockPutOption : QuoteLookupTab(R.string.put_option, StockPutOptionLookupFragment::class.java)
    object StockQuoteSearch : QuoteLookupTab(null, StockQuoteSearchFragment::class.java)
}
