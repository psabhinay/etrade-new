package com.etrade.mobilepro.backends.api

data class ServerMessage(
    val type: Type,
    val text: String,
    val priority: Int,
    val code: String? = null
) {

    enum class Type {
        ERROR, WARN, DISCLOSURE_ERROR
    }

    companion object {
        fun error(text: String, priority: Int = 0): ServerMessage = ServerMessage(Type.ERROR, text, priority)
        fun warning(text: String, priority: Int): ServerMessage = ServerMessage(Type.WARN, text, priority)
    }
}
