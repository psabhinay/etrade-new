package com.etrade.mobilepro.ui_demo.lifecycle

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map

class LifecycleItemViewModel {

    private val _value = MutableLiveData(0)
    val value: LiveData<String>
        get() = _value.map(Int::toString)

    fun increment() {
        _value.postValue((_value.value ?: 0) + 1)
    }
}
