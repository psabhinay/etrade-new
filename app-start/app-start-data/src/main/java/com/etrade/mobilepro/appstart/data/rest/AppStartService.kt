package com.etrade.mobilepro.appstart.data.rest

import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.POST

internal interface AppStartService {
    @JsonMoshi
    @POST("/phx/etm/services/v1/prospect/appStart")
    suspend fun getAppStart(@JsonMoshi @Body appStartRequestDto: AppStartRequestDto = AppStartRequestDto()): AppStartResponseDto
}
