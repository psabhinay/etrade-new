package com.etrade.mobilepro.screener.data.result

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.qualifier.Etf
import com.etrade.mobilepro.screener.api.qualifier.MutualFund
import com.etrade.mobilepro.screener.api.qualifier.Stock
import com.etrade.mobilepro.tableviewutils.api.ColumnRepo
import io.reactivex.Completable
import io.reactivex.Single
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class DefaultResultColumnsProvider @Inject constructor(
    @Stock private val stockColumnsResolver: ResultColumnsResolver,
    @MutualFund private val mfColumnsResolver: ResultColumnsResolver,
    @Etf private val etfResultColumnsResolver: ResultColumnsResolver,
    @Stock private val stockColumnRepo: ColumnRepo<ResultColumn>,
    @MutualFund private val mfColumnRepo: ColumnRepo<ResultColumn>,
    @Etf private val etfColumnRepo: ColumnRepo<ResultColumn>
) : ResultColumnsProvider {

    override fun saveColumns(group: ScreenerType.Group, columns: List<ResultColumn>): Completable =
        Completable.fromAction {
            runBlocking {
                when (group) {
                    ScreenerType.Group.STOCK -> stockColumnRepo
                    ScreenerType.Group.MUTUAL_FUND -> mfColumnRepo
                    ScreenerType.Group.ETF -> etfColumnRepo
                }.also {
                    it.saveColumns(columns)
                }
            }
        }

    override fun getColumns(filterSnapshot: List<ScreenerFilter>): Single<List<ResultColumn>> =
        Single.fromCallable {
            val requiredColumnsSet = mapFilterSnapshotToColumnsSet(filterSnapshot)
            val group = determineScreenerGroup(filterSnapshot)
            val columnOrder = runBlocking {
                when (group) {
                    ScreenerType.Group.STOCK -> stockColumnRepo
                    ScreenerType.Group.MUTUAL_FUND -> mfColumnRepo
                    ScreenerType.Group.ETF -> etfColumnRepo
                }.getColumns()
            }
            arrangeResultColumns(requiredColumnsSet, columnOrder)
        }

    private fun mapFilterSnapshotToColumnsSet(filterSnapshot: List<ScreenerFilter>): Set<ResultColumn> =
        filterSnapshot
            .map {
                return@map when (it.belongsTo()) {
                    ScreenerType.Group.STOCK -> stockColumnsResolver.getColumns(it)
                    ScreenerType.Group.MUTUAL_FUND -> mfColumnsResolver.getColumns(it)
                    ScreenerType.Group.ETF -> etfResultColumnsResolver.getColumns(it)
                }
            }
            .flatten()
            .toSet() // remove duplicates

    private fun arrangeResultColumns(requiredColumns: Set<ResultColumn>, columnsOrder: List<ResultColumn>): List<ResultColumn> {
        val result = mutableListOf<ResultColumn>()
        if (requiredColumns.isNotEmpty()) {
            columnsOrder.forEach {
                if (requiredColumns.contains(it)) {
                    result.add(it)
                }
            }
        }
        return result
    }

    private fun determineScreenerGroup(filterSnapshot: List<ScreenerFilter>): ScreenerType.Group =
        filterSnapshot.firstOrNull()?.belongsTo() ?: ScreenerType.Group.STOCK
}
