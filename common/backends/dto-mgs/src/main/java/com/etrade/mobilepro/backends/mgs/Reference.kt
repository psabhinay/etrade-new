package com.etrade.mobilepro.backends.mgs

/**
 * Marker interface that is intended to convert a data transfer object from the backend into an application model that can be persisted.
 */
interface Reference<T> {
    fun convertToModel(): T
}
