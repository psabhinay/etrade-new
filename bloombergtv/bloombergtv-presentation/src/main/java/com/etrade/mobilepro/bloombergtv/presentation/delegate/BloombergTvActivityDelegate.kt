package com.etrade.mobilepro.bloombergtv.presentation.delegate

import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvActivity

interface BloombergTvActivityDelegate {
    fun init(activity: BloombergTvActivity)
}
