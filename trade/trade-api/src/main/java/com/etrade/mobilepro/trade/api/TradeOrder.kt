package com.etrade.mobilepro.trade.api

import com.etrade.mobilepro.trade.api.option.TradeLeg

data class TradeOrder(
    val orderId: String,
    val accountId: String,
    val response: OrderResponse,
    override val orderRequest: OrderRequest? = null
) : WithOrderRequest

data class OrderRequest(val legDetails: List<TradeLeg> = emptyList())

interface WithOrderRequest {
    val orderRequest: OrderRequest?
}
