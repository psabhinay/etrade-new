package com.etrade.mobilepro.bloombergtv.presentation.delegate.service

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioFocusRequest
import android.media.AudioManager

internal class AudioFocusServiceDelegate(
    private val onFocusGain: () -> Unit,
    private val onFocusLoss: () -> Unit,
    private val onFocusLossTransient: () -> Unit,
    private val onFocusLossCanDuck: () -> Unit
) {
    private lateinit var audioManager: AudioManager
    private lateinit var audioFocusRequest: AudioFocusRequest

    fun init(context: Context) {
        val listener = AudioManager.OnAudioFocusChangeListener { focusState ->
            // Invoked when the audio focus of the system is updated.
            when (focusState) {
                AudioManager.AUDIOFOCUS_GAIN -> onFocusGain()
                AudioManager.AUDIOFOCUS_LOSS -> onFocusLoss() // Lost focus for an unbounded amount of time: stop playback and release media player
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ->
                    // Lost focus for a short time, but we have to stop
                    // playback. We don't release the media player because playback
                    // is likely to resume
                    onFocusLossTransient()
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK ->
                    // Lost focus for a short time, but it's ok to keep playing
                    // at an attenuated level
                    onFocusLossCanDuck()
            }
        }
        audioManager = context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val audioAttributes = AudioAttributes.Builder()
            .setUsage(AudioAttributes.USAGE_MEDIA)
            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
            .build()

        audioFocusRequest = AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
            .setAudioAttributes(audioAttributes)
            .setAcceptsDelayedFocusGain(true)
            .setOnAudioFocusChangeListener(listener)
            .build()
    }

    fun requestFocus(): Boolean =
        audioManager.requestAudioFocus(audioFocusRequest) == AudioManager.AUDIOFOCUS_REQUEST_GRANTED

    fun dismantle() {
        audioManager.abandonAudioFocusRequest(audioFocusRequest)
    }
}
