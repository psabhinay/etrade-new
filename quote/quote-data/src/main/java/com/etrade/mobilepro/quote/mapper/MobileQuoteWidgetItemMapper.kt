package com.etrade.mobilepro.quote.mapper

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.QuoteChangeWidgetItem
import com.etrade.mobilepro.quoteapi.QuotePrice
import com.etrade.mobilepro.quoteapi.QuotePriceWidgetItem
import com.etrade.mobilepro.quoteapi.QuoteTemplateResources
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem
import com.etrade.mobilepro.quoteapi.getTimeStamp
import com.etrade.mobilepro.quoteapi.isAffiliatedProduct
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.domain.data.deriveChangePercent
import com.etrade.mobilepro.util.domain.data.deriveDayChange
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.toSentence
import javax.inject.Inject

class MobileQuoteWidgetItemMapper @Inject constructor(val templateResources: QuoteTemplateResources) {

    fun map(quote: MobileQuote, showDayChange: Boolean = false): QuoteWidgetItem = getQuoteWidget(quote, showDayChange)

    fun map(quotes: List<MobileQuote>, showDayChange: Boolean = false): Map<String, QuoteWidgetItem> = getQuoteWidgetMap(quotes, showDayChange)

    private fun getQuoteWidget(quote: MobileQuote, showDayChange: Boolean): QuoteWidgetItem {
        val quotePrice = quote.price
        val showBidAsk = quote.showBidAsk

        val changeItem = when {
            showDayChange && quote.extendedQuoteHrDetail != null -> {
                val dayChange = deriveDayChange(quote.extendedQuoteHrDetail?.change, quote.quotePrice.change) ?: ""
                QuoteChangeWidgetItem(dayChange, deriveChangePercent(quote.previousClose, dayChange) ?: "")
            }
            else -> quotePrice.changeItem
        }

        return QuoteWidgetItem(
            quote.title,
            quote.symbol,
            quote.typeCode,
            quote.subSecType,
            quotePrice.getLastPriceItem(quote.tradeExchange),
            changeItem,
            getChangeColor(changeItem.change),
            quotePrice.volume,
            quote.getTimeStamp(templateResources),
            quotePrice.getBidPriceItem(quote.bidExchange),
            quotePrice.getAskPriceItem(quote.askExchange),
            quote.showVolume,
            showBidAsk,
            showBidAsk,
            templateResources,
            titleContentDescription = quote.titleContentDescription,
            isAffiliatedProduct = quote.isAffiliatedProduct()
        )
    }

    private fun getQuoteWidgetMap(quotes: List<MobileQuote>, showDayChange: Boolean): Map<String, QuoteWidgetItem> {
        val stocks = quotes.filter {
            it.typeCode == InstrumentType.EQ || it.typeCode == InstrumentType.INDX || it.typeCode.isMutualFundOrMoneyMarketFund
        }.associateBy { it.symbol }

        val stockItems = stocks.mapValues { (_, stock) -> map(stock, showDayChange) }

        val optionItems = quotes
            .filter { it.typeCode.isOption }
            .map { option -> option to stocks.getValue(option.underlyingSymbol) }
            .associateBy { (option, _) -> option.symbol }
            .mapValues { (_, value) -> merge(value.first, value.second) }

        return stockItems + optionItems
    }

    private fun merge(option: MobileQuote, stock: MobileQuote): QuoteWidgetItem {
        val optionPrice = option.price
        val stockPrice = stock.price
        val showBidAsk = stock.showBidAsk

        return QuoteWidgetItem(
            stock.title,
            option.symbol,
            option.typeCode,
            stock.subSecType,
            stockPrice.getLastPriceItem(stock.tradeExchange),
            stockPrice.changeItem,
            getChangeColor(stockPrice.change),
            stockPrice.volume,
            stock.getTimeStamp(templateResources),
            optionPrice.getBidPriceItem(stock.bidExchange),
            optionPrice.getAskPriceItem(stock.askExchange),
            stock.showVolume,
            showBidAsk,
            showBidAsk,
            templateResources,
            underlyingSymbol = stock.symbol,
            underlyingInstrumentType = stock.typeCode,
            showUnderlyingInfo = true,
            titleContentDescription = stock.titleContentDescription,
            isAffiliatedProduct = stock.isAffiliatedProduct()
        )
    }

    private fun MobileQuote.isAffiliatedProduct() = affiliatedStatusCode?.equals("Y") ?: false

    private val MobileQuote.title: String
        get() {
            return if (typeCode.isOption) {
                description
            } else {
                String.format(templateResources.titleFormat, symbol, description.toSentence())
            }
        }

    private val MobileQuote.price: QuotePrice
        get() {
            return if (typeCode.isOption) {
                quotePrice
            } else {
                extendedQuoteHrDetail ?: quotePrice
            }
        }

    private val MobileQuote.showVolume: Boolean
        get() = !typeCode.isMutualFundOrMoneyMarketFund

    private val MobileQuote.showBidAsk: Boolean
        get() = typeCode != InstrumentType.INDX && !typeCode.isMutualFundOrMoneyMarketFund

    private val MobileQuote.titleContentDescription: String
        get() {
            return if (typeCode.isOption) {
                description
            } else {
                String.format(templateResources.titleFormat, symbol.characterByCharacter(), description.toSentence())
            }
        }

    private val QuotePrice.changeItem: QuoteChangeWidgetItem
        get() {
            return QuoteChangeWidgetItem(
                change = change,
                changePercent = percentChange
            )
        }

    private fun getChangeColor(change: String): Int {
        val changeVal = change.toDoubleOrNull() ?: 0.0
        return if (changeVal >= 0) {
            templateResources.positiveChangeColorRes
        } else {
            templateResources.negativeChangeColorRes
        }
    }

    private fun QuotePrice.getLastPriceItem(exchangeCode: String): QuotePriceWidgetItem = createQuotePriceWidgetItem(lastPrice, lastTradeSize, exchangeCode)

    private fun QuotePrice.getBidPriceItem(exchangeCode: String): QuotePriceWidgetItem = createQuotePriceWidgetItem(bid, bidSize, exchangeCode)

    private fun QuotePrice.getAskPriceItem(exchangeCode: String): QuotePriceWidgetItem = createQuotePriceWidgetItem(ask, askSize, exchangeCode)

    private fun createQuotePriceWidgetItem(price: String, size: String, exchangeCode: String): QuotePriceWidgetItem {
        return QuotePriceWidgetItem(
            price = MarketDataFormatter.formatMarketDataCustom(price.toBigDecimalOrNull()),
            priceColor = templateResources.neutralChangeColorRes,
            size = size,
            exchangeCode = exchangeCode
        )
    }
}
