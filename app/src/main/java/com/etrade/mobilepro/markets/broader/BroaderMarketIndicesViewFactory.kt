package com.etrade.mobilepro.markets.broader

import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.markets.broader.dto.BroaderMarketIndicesViewDto
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import com.etrade.mobilepro.util.color.DataColorResolver
import javax.inject.Inject

class BroaderMarketIndicesViewFactory @Inject constructor(
    private val dataColorResolver: DataColorResolver,
    private val streamingController: StreamingSubscriptionController,
    private val marketsService: MoverService,
    private val resources: Resources,
    private val streamingStatusController: StreamingStatusController,
    private val user: User,
    private val tracker: Tracker
) : DynamicUiViewFactory {
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is BroaderMarketIndicesViewDto) {
            return object : GenericLayout {

                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is BroaderMarketIndicesViewModel) {
                        viewModel.refresh()
                    }
                }

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    BroaderMarketIndicesViewModel(
                        dataColorResolver, streamingController, marketsService, resources, streamingStatusController, user, tracker
                    )
                }

                override val viewModelClass = BroaderMarketIndicesViewModel::class.java

                override val uniqueIdentifier: String = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("BroaderMarketIndicesViewFactory registered to unknown type")
        }
    }
}
