package com.etrade.mobilepro.marketschedule

import com.etrade.eo.marketschedule.api.MarketScheduleProvider
import com.etrade.eo.marketschedule.api.status.MarketScheduleStatus
import javax.inject.Inject

class MarketScheduleProviderHolderImpl @Inject constructor() : MarketScheduleProviderHolder {

    override var marketScheduleProvider: MarketScheduleProvider? = null

    override val isExtendedHourEligiblePeriod: Boolean
        get() {
            val status = marketScheduleProvider?.getCurrentMarketSchedule()?.status
            return (status?.isExtendedHours() == true || status == MarketScheduleStatus.CLOSED)
        }
}
