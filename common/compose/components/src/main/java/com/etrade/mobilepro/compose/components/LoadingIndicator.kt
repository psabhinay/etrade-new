package com.etrade.mobilepro.compose.components

import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.etrade.mobilepro.common.compose.theme.etColors

@Composable
fun LoadingIndicator(modifier: Modifier = Modifier) {
    CircularProgressIndicator(
        modifier,
        color = MaterialTheme.etColors.progressIndicator
    )
}
