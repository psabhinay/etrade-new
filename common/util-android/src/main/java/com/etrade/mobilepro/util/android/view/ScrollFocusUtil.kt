package com.etrade.mobilepro.util.android.view

import android.view.View
import android.view.ViewParent
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.util.android.extension.isCompletelyVisible

private const val MAX_TRAVERSALS = 10

/**
 * Scrolls to a [LinearLayoutManager]'s child View if it is not completely visible.
 */
fun scrollToView(
    targetView: View,
    layoutManager: LinearLayoutManager,
    adapterPosition: Int,
    scrollWithOffset: Boolean
) {
    if (!targetView.isCompletelyVisible || scrollWithOffset) {
        val to = targetView.top
            .takeIf { it > 0 }
            ?: (targetView.parent as? View)?.top ?: 0
        layoutManager.scrollToPositionWithOffset(adapterPosition, -to)
    }
}

/**
 * Scrolls to a [NestedScrollView]'s child View if it is not completely visible.
 */
fun scrollToView(
    targetView: View,
    scrollView: NestedScrollView
) {
    if (!targetView.isCompletelyVisible) {
        var distance = targetView.top
        var viewParent: ViewParent = targetView.parent

        for (i in 0..MAX_TRAVERSALS) {
            if ((viewParent as View) == scrollView) {
                break
            }
            distance += (viewParent as View).top
            viewParent = viewParent.getParent()
        }

        scrollView.scrollTo(0, distance)
    }
}
