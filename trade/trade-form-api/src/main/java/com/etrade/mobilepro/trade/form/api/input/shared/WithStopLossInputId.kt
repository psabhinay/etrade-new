package com.etrade.mobilepro.trade.form.api.input.shared

interface WithStopLossInputId {
    val stopLossInputId: StopLossInputId
}
