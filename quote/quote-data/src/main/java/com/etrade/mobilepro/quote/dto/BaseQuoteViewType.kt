package com.etrade.mobilepro.quote.dto

import com.etrade.mobilepro.dynamicui.DynamicEnum
import com.etrade.mobilepro.instrument.InstrumentType

enum class BaseQuoteViewType(
    override val dtoName: String,
    override val dtoClass: Class<out QuoteResponse>
) : DynamicEnum<QuoteResponse> {
    INDEX(InstrumentType.INDX.typeCode, QuoteIndexDto::class.java),
    STOCK(InstrumentType.EQ.typeCode, QuoteStockDto::class.java),
    OPTIONS(InstrumentType.OPTN.typeCode, QuoteOptionsDto::class.java),
    MUTUAL_FUND(InstrumentType.MF.typeCode, FundQuoteDto::class.java),
    MONEY_MARKET_FUND(InstrumentType.MMF.typeCode, MoneyMarketFundQuoteDto::class.java),
    BOND(InstrumentType.BOND.typeCode, QuoteBondDto::class.java),
    ETF(InstrumentType.ETF.typeCode, QuoteETFDto::class.java)
}
