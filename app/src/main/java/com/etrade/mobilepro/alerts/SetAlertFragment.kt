package com.etrade.mobilepro.alerts

import android.os.Bundle
import android.text.InputFilter
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.R
import com.etrade.mobilepro.alerts.api.model.AlertValueType
import com.etrade.mobilepro.alerts.api.model.NewsAlertValueType
import com.etrade.mobilepro.alerts.presentation.AlertsSharedViewModel
import com.etrade.mobilepro.alerts.presentation.setalert.SetAlertViewModel
import com.etrade.mobilepro.alerts.presentation.setalert.clearTextFieldFocuses
import com.etrade.mobilepro.alerts.presentation.setalert.getPeTargetFieldHint
import com.etrade.mobilepro.alerts.presentation.setalert.resetViews
import com.etrade.mobilepro.alerts.presentation.setalert.toggleViewsEnabledState
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.customview.SelfFocusHandlingTextInputEditText
import com.etrade.mobilepro.common.getNavigationButtonDescription
import com.etrade.mobilepro.databinding.FragmentSetAlertBinding
import com.etrade.mobilepro.home.model.MainActivityViewModel
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.inputvalueview.InputValueView
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.styledalertdialog.StyledAlertDialog
import com.etrade.mobilepro.util.android.binding.dataBinding
import com.etrade.mobilepro.util.android.goneUnless
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import javax.inject.Inject

@Suppress("LargeClass")
@RequireLogin
class SetAlertFragment @Inject constructor(
    val viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.fragment_set_alert) {

    private val binding by dataBinding(FragmentSetAlertBinding::bind)

    private val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private var snackBar: Snackbar? = null
    private val viewModel by viewModels<SetAlertViewModel> { viewModelFactory }
    private val mainActivityViewModel by activityViewModels<MainActivityViewModel> { viewModelFactory }
    private val sharedViewModel by activityViewModels<AlertsSharedViewModel> { viewModelFactory }
    private val quoteWidgetViewModel: QuotesWidgetViewModel by viewModels { viewModelFactory }
    private val args: SetAlertFragmentArgs by navArgs()
    private var peValue: String? = null
    private val editTexts by lazy {
        with(binding.inputs) {
            listOf<TextInputEditText>(
                tHighTargetPrice,
                tLowTargetPrice,
                tHighDailyPercent,
                tLowDailyPercent,
                tHighPeTarget,
                tLowPeTarget,
                tDailyVolume
            )
        }
    }
    private val switches by lazy {
        with(binding.switches) {
            listOf(
                upDowngrade, inPlay, earnings, splitNotice
            )
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                findNavController().popBackStack()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
    }

    override fun onStart() {
        super.onStart()
        if (args.showCrossAsUpButton) {
            (activity as? AppCompatActivity)?.supportActionBar?.apply {
                val icon = R.drawable.ic_close
                setHomeAsUpIndicator(icon)
                setHomeActionContentDescription(activity?.getNavigationButtonDescription(icon))
            }
        }
    }

    private fun setupFragment() {
        with(binding) {
            args.symbol?.let {
                viewModel.setSymbol(it)
                symbol.isEnabled = false
            } ?: symbol.setOnClickListener { viewModel.openQuoteLookup() }
            setAlertBtn.setOnClickListener {
                clearTextFieldFocuses(editTexts, binding.inputs.dummy, view)
                viewModel.setAlert()
            }
            with(inputs) {
                tHighTargetPriceLayout.setupInput(AlertValueType.HIGH_TARGET_PRICE)
                tLowTargetPriceLayout.setupInput(AlertValueType.LOW_TARGET_PRICE)
                tHighDailyPercentLayout.setupInput(AlertValueType.HIGH_DAILY_PERCENT)
                tLowDailyPercentLayout.setupInput(AlertValueType.LOW_DAILY_PERCENT)

                tHighPeTargetLayout.setupInputFilter(AlertValueType.HIGH_PE_TARGET, true, tHighDailyPercent)
                tLowPeTargetLayout.setupInputFilter(AlertValueType.LOW_PE_TARGET, false, tLowDailyPercent)
                tDailyVolumeLayout.setupView(
                    dependencies = viewModel.inputValueDepsProvider.getDependencies(AlertValueType.DAILY_VOLUME),
                    rawContentData = viewModel.inputValueHolder(AlertValueType.DAILY_VOLUME)
                )
                tDailyVolume.setOnEditorActionListener { _, actionId, event ->
                    if (actionId == EditorInfo.IME_ACTION_DONE || event.keyCode == KeyEvent.KEYCODE_ENTER) {
                        clearTextFieldFocuses(editTexts, dummy, view)
                    }
                    false
                }
            }
            with(switches) {
                upDowngrade.setupSwitch(NewsAlertValueType.UP_DOWN)
                inPlay.setupSwitch(NewsAlertValueType.IN_PLAY)
                earnings.setupSwitch(NewsAlertValueType.EARNINGS)
                splitNotice.setupSwitch(NewsAlertValueType.SPLIT_NOTICE)
            }
            binding.setAlertPb.bringToFront()
            binding.symbolLoadingPb.hide()
        }
        toggleViewsEnabledState(editTexts, switches, isEnabled = false)
    }

    private fun InputValueView.setupInput(valueType: AlertValueType) {
        setupView(
            dependencies = viewModel.inputValueDepsProvider.getDependencies(valueType),
            rawContentData = viewModel.inputValueHolder(valueType)
        )
    }

    private fun InputValueView.setupInputFilter(valueType: AlertValueType, isHigh: Boolean, editText: SelfFocusHandlingTextInputEditText) {
        setupView(
            dependencies = viewModel.inputValueDepsProvider.getDependencies(valueType)
                .copy(
                    customizer = object : InputFieldManager.Customizer {
                        override fun updateInputFilters(event: InputFieldManager.Event): List<InputFilter> {
                            hint =
                                getPeTargetFieldHint(
                                    resources,
                                    isHigh = isHigh,
                                    pe = peValue,
                                    hasFocus = event == InputFieldManager.Event.ON_FOCUS_GAINED
                                )
                            return editText.filters.toList()
                        }
                    }
                ),
            rawContentData = viewModel.inputValueHolder(valueType)
        )
    }

    private fun SwitchCompat.setupSwitch(valueType: NewsAlertValueType) {
        setOnCheckedChangeListener { _, isChecked ->
            clearTextFieldFocuses(editTexts, binding.inputs.dummy, view)
            viewModel.switchValueHolder(valueType).value = isChecked
        }
    }

    private fun bindViewModel() {
        bindViewsStates()
        bindSymbol()
        bindNavigationRelated()
    }

    private fun bindNavigationRelated() {
        viewModel.needOpenQuoteLookup.observe(
            viewLifecycleOwner,
            Observer { needToOpenSearch ->
                if (needToOpenSearch) {
                    mainActivityViewModel.openSearchActivity(SearchQuery(SearchType.SYMBOL), animated = false, simpleSymbolSearch = true)
                }
            }
        )
        viewModel.needResetViews.observe(
            viewLifecycleOwner,
            Observer {
                resetViews(editTexts, switches)
            }
        )
        viewModel.needLeaveBlankPage.observe(
            viewLifecycleOwner,
            Observer {
                if (it) {
                    findNavController().popBackStack()
                }
            }
        )
    }

    private fun bindViewsStates() {
        viewModel.setAlertButtonState().observe(
            viewLifecycleOwner,
            Observer {
                binding.setAlertBtn.isEnabled =
                    it.first && it.second != SetAlertViewModel.ViewState.Loading
            }
        )
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                toggleRootScrollVisibility(it)
                handleViewState(it)
            }
        )
        viewModel.widgetViewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    SetAlertViewModel.WidgetViewState.Loading -> handleLoadingWidgetViewState()
                    is SetAlertViewModel.WidgetViewState.Success -> handleSuccessWidgetViewState(it)
                    is SetAlertViewModel.WidgetViewState.Error -> handleErrorWidgetViewState(it)
                }
            }
        )
    }

    private fun adjustEditTexts(symbolType: InstrumentType) {
        with(binding.inputs) {
            tHighPeTargetLayout.goneUnless(symbolType.isEquity)
            tLowPeTargetLayout.goneUnless(symbolType.isEquity)
            tDailyVolumeLayout.goneUnless(symbolType.isEquity)
        }
    }

    private fun adjustSwitches(symbolType: InstrumentType) {
        with(binding.switches) {
            upDowngrade.goneUnless(symbolType.isEquity)
            upDowngradeDivider.goneUnless(symbolType.isEquity)
            inPlay.goneUnless(symbolType.isEquity)
            inPlayDivider.goneUnless(symbolType.isEquity)
            earnings.goneUnless(symbolType.isEquity)
            earningsDivider.goneUnless(symbolType.isEquity)
            splitNotice.goneUnless(symbolType.isEquity)
            splitNoticeDivider.goneUnless(symbolType.isEquity)
        }
    }

    private fun adjustView(symbolType: InstrumentType) {
        adjustEditTexts(symbolType)
        binding.newsTextviewSeparator.goneUnless(symbolType.isEquity)
        adjustSwitches(symbolType)
    }

    private fun bindSymbol() {
        viewModel.currentSymbol.observe(
            viewLifecycleOwner,
            Observer {
                with(binding.symbol) {
                    when {
                        it != null -> {
                            toggleViewsEnabledState(editTexts, switches, isEnabled = true)
                            value = it.title
                            contentDescription = "${getString(R.string.symbol)} ${it.title}"

                            // Depending on the symbol type, we might have to remove some of the Irrelevant user input options
                            adjustView(it.type)
                        }
                        else -> {
                            toggleViewsEnabledState(editTexts, switches, isEnabled = false)
                            value = null
                            contentDescription = "${getString(R.string.symbol)}"
                        }
                    }
                }
                binding.rootScroll.smoothScrollTo(0, 0)
            }
        )
    }

    private fun handleLoadingWidgetViewState() {
        snackBar?.dismiss()
        binding.symbolLoadingPb.show()
        binding.additionalSpaceBeforeQuote.visibility = View.VISIBLE
        binding.quoteWidget.root.visibility = View.GONE
    }

    @Suppress("LongMethod")
    private fun handleSuccessWidgetViewState(viewState: SetAlertViewModel.WidgetViewState.Success) {
        snackBar?.dismiss()
        binding.symbolLoadingPb.hide()
        viewState.quoteWidgetItem?.let { item ->
            binding.additionalSpaceBeforeQuote.visibility = View.GONE
            binding.quoteWidget.root.visibility = View.VISIBLE
            binding.lifecycleOwner = viewLifecycleOwner
            binding.obj = quoteWidgetViewModel
            quoteWidgetViewModel.updateQuotes(item)
        }
        if (viewState.quoteWidgetItem == null) {
            binding.additionalSpaceBeforeQuote.visibility = View.VISIBLE
            binding.quoteWidget.root.visibility = View.GONE
        }
        if (viewModel.currentSymbol.value == null) {
            viewModel.openQuoteLookup()
        }
        peValue = viewState.pe
        with(binding.inputs.tHighPeTargetLayout) {
            hint = getPeTargetFieldHint(
                resources,
                isHigh = true,
                pe = viewState.pe,
                hasFocus = isFocused
            )
        }
        with(binding.inputs.tLowPeTargetLayout) {
            hint = getPeTargetFieldHint(
                resources,
                isHigh = false,
                pe = viewState.pe,
                hasFocus = isFocused
            )
        }
    }

    private fun handleErrorWidgetViewState(viewState: SetAlertViewModel.WidgetViewState.Error) =
        when (viewState) {
            SetAlertViewModel.WidgetViewState.Error.Loading -> {
                binding.symbolLoadingPb.hide()
                snackBar = snackbarUtil.retrySnackbar {
                    viewModel.retryWidget()
                }
            }
            SetAlertViewModel.WidgetViewState.Error.Type -> {
                binding.symbolLoadingPb.hide()
                context?.let {
                    StyledAlertDialog(it).showAlert(
                        title = getString(R.string.title_error),
                        message = getString(R.string.wrong_symbol_type_error_message),
                        isCancelable = false,
                        positiveAnswerAction = {
                            viewModel.resetCurrentSymbol()
                        }
                    )
                }
            }
        }

    private fun toggleRootScrollVisibility(viewState: SetAlertViewModel.ViewState) {
        binding.rootScroll.visibility = if (viewState is SetAlertViewModel.ViewState.Loading) {
            View.INVISIBLE
        } else {
            View.VISIBLE
        }
    }

    private fun handleViewState(viewState: SetAlertViewModel.ViewState) {
        when (viewState) {
            is SetAlertViewModel.ViewState.Loading -> handleLoadingViewState()
            is SetAlertViewModel.ViewState.Success -> handleSuccessViewState(viewState)
            is SetAlertViewModel.ViewState.Error -> handleErrorViewState(viewState)
            is SetAlertViewModel.ViewState.InitialSearchInProgress -> binding.setAlertPb.hide()
        }
    }

    private fun handleLoadingViewState() {
        snackBar?.dismiss()
        binding.setAlertPb.show()
    }

    private fun handleSuccessViewState(viewState: SetAlertViewModel.ViewState.Success) {
        binding.setAlertPb.hide()
        val description = if (viewState.setAlertResponse != null && viewState.setAlertResponse!!.hasError) {
            viewState.setAlertResponse!!.errorMessage
        } else {
            getString(R.string.alert_was_set)
        }
        if (viewState.setAlertResponse?.hasError == true) {
            snackBar = snackbarUtil.snackbar(description, Snackbar.LENGTH_LONG)?.apply { show() }
        } else {
            sharedViewModel.didSetAlert(description)
            findNavController().popBackStack()
        }
    }

    private fun handleErrorViewState(viewState: SetAlertViewModel.ViewState.Error) = when (viewState) {
        is SetAlertViewModel.ViewState.Error.AlertRelated -> {
            binding.setAlertPb.hide()
            viewState.consumableErrorMessage?.let {
                snackBar = snackbarUtil.errorSnackbar(it)
            }
            clearTextFieldFocuses(editTexts, binding.inputs.dummy, view)
        }
        is SetAlertViewModel.ViewState.Error.NetworkRelated -> {
            binding.setAlertPb.hide()
            snackbarUtil.retrySnackbar {
                viewModel.retryAlert()
            }
            clearTextFieldFocuses(editTexts, binding.inputs.dummy, view)
        }
    }
}
