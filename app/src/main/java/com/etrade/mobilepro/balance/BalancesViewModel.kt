package com.etrade.mobilepro.balance

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.common.di.LocalDynamicScreen
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.Resource
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class BalancesViewModel @Inject constructor(
    @LocalDynamicScreen
    private val dynamicScreenRepo: DynamicScreenRepo,
    private val user: User
) : ViewModel() {

    private val _viewStates = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState> = _viewStates
    private var compositeDisposable = CompositeDisposable()
    private var accountId: String? = null

    fun refresh() {
        accountId?.let {
            disposeCurrent()
            compositeDisposable.add(
                dynamicScreenRepo.getScreen(ServicePath.Balances(it, user.getUserId().toString())).subscribeBy(
                    onNext = { response ->
                        when (response) {
                            is Resource.Success -> _viewStates.postValue(ViewState.Success(response.data.orEmpty()))
                            is Resource.Loading -> _viewStates.postValue(ViewState.Loading())
                            is Resource.Failed -> _viewStates.postValue(ViewState.Error("error"))
                        }
                    },
                    onError = { throwable ->
                        _viewStates.postValue(ViewState.Error(throwable.message))
                    }
                )
            )
        }
    }

    /**
     * Method ensures the fetching balances is conditional on refreshed or new account selected.
     */

    fun fetchBalances(accountId: String) {
        if (this.accountId == accountId) return
        this.accountId = accountId
        refresh()
    }

    override fun onCleared() {
        disposeCurrent()
        super.onCleared()
    }

    private fun disposeCurrent() {
        compositeDisposable.clear()
    }

    sealed class ViewState {
        data class Loading(val views: List<GenericLayout> = emptyList()) : ViewState()

        data class Error(val message: String? = "Something went wrong!") : ViewState()

        data class Success(val views: List<GenericLayout>) : ViewState()
    }
}
