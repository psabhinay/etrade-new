package com.etrade.mobilepro.common.customview

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.google.android.material.textfield.TextInputEditText

/**
 * Marker interface
 *
 * Views, marked with it will not participate on "tap outside to clear focus" logic implemented in MainActivity::onDispatchTouchEvent
 *
 */
interface SelfFocusHandlingView

class SelfFocusHandlingTextInputEditText : TextInputEditText, SelfFocusHandlingView {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
}

class SelfFocusEditText : AppCompatEditText, SelfFocusHandlingView {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
}
