package com.etrade.mobilepro.watchlist.usecase

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import java.util.concurrent.TimeUnit
import javax.inject.Inject

interface ResolveMutualFundTimestampUseCase : UseCase<List<WatchlistEntry>, ETResult<String?>>

typealias MutualFundTimestampFormatter = (String) -> String

class ResolveMutualFundTimestampUseCaseImpl @Inject constructor(
    private val formatter: MutualFundTimestampFormatter
) : ResolveMutualFundTimestampUseCase {
    override suspend fun execute(parameter: List<WatchlistEntry>): ETResult<String?> {
        return runCatchingET {
            val firstMFEntry = parameter.firstOrNull { it.typeCode?.isMutualFundOrMoneyMarketFund == true }
            firstMFEntry?.lastTradeTimeStamp?.let {
                formatter(DateFormattingUtils.formatLocaleIndependentShortDateWithSlashes(TimeUnit.SECONDS.toMillis(it)))
            }
        }
    }
}
