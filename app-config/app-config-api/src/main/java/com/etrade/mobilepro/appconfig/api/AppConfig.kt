package com.etrade.mobilepro.appconfig.api

import com.etrade.eo.streaming.SubscriptionManager
import com.etrade.mobilepro.common.di.Level1Streamer
import com.etrade.mobilepro.common.di.Level2Streamer
import org.threeten.bp.Duration
import java.net.URL
import java.util.Locale

const val LIGHT_STREAMER_LEVEL1_IDENTIFIER = "LEVEL1"
const val LIGHT_STREAMER_LEVEL2_IDENTIFIER = "LEVEL2"

data class AppConfig(
    val isCircuitBreakEnabled: Boolean,
    val environment: AppEnvironment,
    val baseUrls: BaseUrls,
    val timeouts: Timeouts,
    val streamings: Map<String, Streaming>
)

enum class AppEnvironment(val asString: String) {
    SIT("SIT"),
    UAT("UAT"),
    PROD("PROD"),
    PROD_M3_ONLY("PROD_M3"),
    PROD_M5_ONLY("PROD_M5"),
    DR("DR"),
    DR_EXTERNAL("DR_EXTERNAL"),
    M5("M5"),
    MOCK("MOCK"),
    PLT("PLT");

    companion object {
        @Suppress("ComplexMethod")
        fun fromString(input: String): AppEnvironment = when (input) {
            DR.asString -> DR
            DR_EXTERNAL.asString -> DR_EXTERNAL
            M5.asString -> M5
            MOCK.asString -> MOCK
            PLT.asString -> PLT
            PROD.asString -> PROD
            PROD_M3_ONLY.asString -> PROD_M3_ONLY
            PROD_M5_ONLY.asString -> PROD_M5_ONLY
            SIT.asString -> SIT
            UAT.asString -> UAT
            else -> throw IllegalArgumentException("$input is unknown type of AppEnvironment")
        }
    }
}

data class BaseUrls(
    val web: String = "",
    val eDocWeb: String = "",
    val express: String = "",
    val mobileTrade: String = "",
    val mobileETrade: String = "",
    val marketOnDemand: String = "",
    val api: String = "",
    val level1StreamerUrl: String = "",
    val level2StreamerUrl: String = "",
    val environmentChange: String = ""
)

/**
 * This class is created to hold all the subscriptionManagers in order to avoid lint long parameter error
 * */
data class LightStreamerSubscriptionManagerHolder(
    @Level1Streamer val level1SubscriptionManager: SubscriptionManager,
    @Level2Streamer val level2SubscriptionManager: SubscriptionManager
)

data class Timeouts(
    val connection: Duration,
    val read: Duration,
    val write: Duration,
    val limitSessionTo30Sec: Boolean
) {
    companion object {
        const val DEFAULT_SESSION_TIMEOUT_SEC = 30L * 60
        const val DEBUG_SESSION_TIMEOUT_SEC = 30L
    }
}

private const val STREAMING_PARAM_NULL_VALUE_DESCRIPTION = "Not connected"
private const val STREAMING_PARAM_PARSE_FAILED_DESCRIPTION = "Parse failed:"

data class Streaming(
    private val serverUrl: String?,
    private val serverInstance: String?
) {
    val url: String = serverUrl ?: STREAMING_PARAM_NULL_VALUE_DESCRIPTION
    val instance: String = serverInstance ?: STREAMING_PARAM_NULL_VALUE_DESCRIPTION
    val instanceServer: String = if (instance == STREAMING_PARAM_NULL_VALUE_DESCRIPTION) {
        instance
    } else {
        runCatching { URL(instance).host }.getOrNull()?.split(".")?.firstOrNull()?.uppercase(Locale.getDefault())
            ?: STREAMING_PARAM_PARSE_FAILED_DESCRIPTION + instance
    }
}
