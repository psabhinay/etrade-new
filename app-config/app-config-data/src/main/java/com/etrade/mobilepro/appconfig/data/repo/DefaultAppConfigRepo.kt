package com.etrade.mobilepro.appconfig.data.repo

import com.etrade.eo.streaming.SERVER_ADDRESS
import com.etrade.eo.streaming.SERVER_INSTANCE
import com.etrade.eo.streaming.SubscriptionManager
import com.etrade.mobilepro.appconfig.api.AppConfig
import com.etrade.mobilepro.appconfig.api.AppEnvironment
import com.etrade.mobilepro.appconfig.api.BaseUrls
import com.etrade.mobilepro.appconfig.api.LIGHT_STREAMER_LEVEL1_IDENTIFIER
import com.etrade.mobilepro.appconfig.api.LIGHT_STREAMER_LEVEL2_IDENTIFIER
import com.etrade.mobilepro.appconfig.api.LightStreamerSubscriptionManagerHolder
import com.etrade.mobilepro.appconfig.api.Streaming
import com.etrade.mobilepro.appconfig.api.Timeouts
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.appconfig.api.repo.datasource.BaseUrlsDataSource
import com.etrade.mobilepro.appconfig.data.repo.datasource.defaultTimeouts
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.util.mutableLazy
import org.threeten.bp.Duration
import javax.inject.Inject

// Not thread safe
class DefaultAppConfigRepo @Inject constructor(
    private val baseUrlsDataSource: BaseUrlsDataSource,
    private val defaultEnvironment: String,
    private val appPreferences: ApplicationPreferences,
    private val lsSubscriptionManagerHolder: LightStreamerSubscriptionManagerHolder
) : AppConfigRepo {
    private var cachedAppEnvironment: AppEnvironment by mutableLazy(
        initializer = {
            appPreferences
                .appEnvironment
                .takeIf { it.isNotEmpty() }
                ?.let { AppEnvironment.fromString(it) } ?: AppEnvironment.fromString(defaultEnvironment)
        },
        onValueChanged = {
            appPreferences.appEnvironment = it.asString
        }
    )

    private var cachedTimeouts: Timeouts by mutableLazy(
        initializer = {
            Timeouts(
                connection = appPreferences.connectionTimeoutSec.toDuration() ?: defaultTimeouts.connection,
                read = appPreferences.readTimeoutSec.toDuration() ?: defaultTimeouts.read,
                write = appPreferences.writeTimeoutSec.toDuration() ?: defaultTimeouts.write,
                limitSessionTo30Sec = appPreferences.limitSessionTo30Sec
            )
        },
        onValueChanged = {
            appPreferences.connectionTimeoutSec = it.connection.seconds.toInt()
            appPreferences.writeTimeoutSec = it.write.seconds.toInt()
            appPreferences.readTimeoutSec = it.read.seconds.toInt()
            appPreferences.limitSessionTo30Sec = it.limitSessionTo30Sec
        }
    )

    private var circuitBreakerEnabled: Boolean by mutableLazy(
        initializer = {
            appPreferences.isCircuitBreakEnabled
        },
        onValueChanged = {
            appPreferences.isCircuitBreakEnabled = it
        }
    )

    override fun getAppConfig(): AppConfig = AppConfig(
        environment = cachedAppEnvironment,
        baseUrls = baseUrlsDataSource.getBaseUrls(cachedAppEnvironment),
        timeouts = cachedTimeouts,
        isCircuitBreakEnabled = appPreferences.isCircuitBreakEnabled,
        streamings = mapOf(
            lsSubscriptionManagerHolder.level1SubscriptionManager.getMarketDataFeedAndStreamingPair(LIGHT_STREAMER_LEVEL1_IDENTIFIER),
            lsSubscriptionManagerHolder.level2SubscriptionManager.getMarketDataFeedAndStreamingPair(LIGHT_STREAMER_LEVEL2_IDENTIFIER)
        )
    )

    override fun setEnvironment(environment: AppEnvironment, baseUrls: BaseUrls) {
        cachedAppEnvironment = environment
        baseUrlsDataSource.saveBaseUrls(baseUrls, environment)
    }

    override fun setTimeouts(timeouts: Timeouts) {
        cachedTimeouts = timeouts
    }

    override fun setMarketCircuitBreaker(enabled: Boolean) {
        circuitBreakerEnabled = enabled
    }

    private fun SubscriptionManager.getMarketDataFeedAndStreamingPair(
        key: String
    ): Pair<String, Streaming> {
        return key to Streaming(
            serverUrl = subscriptionClientInfoProvider.getServerConnectionDetails()[SERVER_ADDRESS],
            serverInstance = subscriptionClientInfoProvider.getServerConnectionDetails()[SERVER_INSTANCE]
        )
    }
}

private fun Int.toDuration(): Duration? = this.takeIf { it > 0 }?.let { Duration.ofSeconds(it.toLong()) }
