package com.etrade.mobilepro.quotes

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.etrade.eo.marketschedule.api.status.MarketScheduleStatus
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.marketschedule.MarketScheduleProviderHolder
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.quote.mapper.MobileQuoteWidgetItemMapper
import com.etrade.mobilepro.quoteapi.GetQuoteParameter
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MobileQuoteDataHolder
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.quoteapi.QuoteItemInterface
import com.etrade.mobilepro.quoteapi.QuoteItems
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuoteUseCase
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.quotes.QuoteViewModel.ViewState.Success
import com.etrade.mobilepro.quotes.usecase.GetPositionHoldingsUseCase
import com.etrade.mobilepro.quotes.usecase.GetQuoteDetailsUseCase
import com.etrade.mobilepro.quotes.usecase.GetQuoteDetailsUseCaseParameter
import com.etrade.mobilepro.quotes.usecase.GetQuoteNewsUseCase
import com.etrade.mobilepro.quotes.usecase.MapDualPaneMobileQuoteUseCase
import com.etrade.mobilepro.quotes.usecase.MapMobileQuoteUseCase
import com.etrade.mobilepro.quotes.usecase.MapMobileQuoteUseCaseParameter
import com.etrade.mobilepro.quotes.usecase.MapQuoteNewsParameter
import com.etrade.mobilepro.quotes.usecase.MapQuoteNewsUseCase
import com.etrade.mobilepro.quotes.util.SecurityAndOrderTypeSelectionHelper
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.OptionData
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.tracking.SCREEN_TITLE_QUOTE_DETAILS
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.usersession.api.LoginSignalHandler
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.invoke
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingState
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.etrade.mobilepro.walkthrough.presentation.createConditionalSignal
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import javax.inject.Inject

private typealias TabSelector = (QuoteItems) -> List<QuoteItemInterface>

class QuoteViewModel @Inject constructor(
    private val getMobileQuoteUseCase: GetMobileQuoteUseCase,
    private val getPositionHoldingsUseCase: GetPositionHoldingsUseCase,
    private val getQuoteDetailsUseCase: GetQuoteDetailsUseCase,
    private val getQuoteNewsUseCase: GetQuoteNewsUseCase,
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase,
    private val marketTradeStatusDelegate: MarketTradeStatusDelegate,
    private val mapMobileQuoteUseCase: MapMobileQuoteUseCase,
    private val mapDualPaneMobileQuoteUseCase: MapDualPaneMobileQuoteUseCase,
    private val mapQuoteNewsUseCase: MapQuoteNewsUseCase,
    private val marketScheduleProviderHolder: MarketScheduleProviderHolder,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController,
    private val quoteMapper: MobileQuoteWidgetItemMapper,
    val securityAndOrderTypeHelper: SecurityAndOrderTypeSelectionHelper,
    val walkthroughStatesViewModel: WalkthroughStatesViewModel
) : ViewModel(), ScreenViewModel, MobileQuoteDataHolder, LifecycleObserver {

    override val screenName: String = SCREEN_TITLE_QUOTE_DETAILS

    val loginSignalHandler = LoginSignalHandler()
    val quoteDescription: LiveData<MobileQuote> get() = _quoteDescription.asStateFlow().asLiveData()
    val lastSelectedTabIndex: LiveData<Int?> get() = _lastSelectedTabIndex
    val viewState: LiveData<ViewState> get() = _viewState
    val symbol: LiveData<SearchResultItem.Symbol> get() = _symbol
    val symbolTrackingHighlightWatchlistActionSignal: LiveData<ConsumableLiveEvent<SymbolTrackingState>>
    val cusipId: String?
        get() = mobileQuote?.cusip

    private val _showDisclosures = MutableLiveData<Boolean>()
    val showDisclosures: LiveData<Boolean> = _showDisclosures

    private val logger = LoggerFactory.getLogger(QuoteViewModel::class.java)

    private val tabSelectors: List<TabSelector> = listOf(
        { items: QuoteItems -> items.snapshot },
        { items: QuoteItems -> items.performance },
        { items: QuoteItems -> items.feesAndExpenses }
    )

    private val stockSubscriptionFields: Set<Level1Field> = StockData.fields()
    private val optionSubscriptionFields: Set<Level1Field> = OptionData.fields()
    private val underlyerSymbolSubscriptionFields: Set<Level1Field> = setOf(
        Level1Field.CHANGE,
        Level1Field.CHANGE_PERCENT,
        Level1Field.LAST_PRICE,
        Level1Field.TICK_INDICATOR,
        Level1Field.TIMESTAMP_SINCE_EPOCH,
        Level1Field.VOLUME
    )

    private val _quoteDescription: MutableStateFlow<MobileQuote> = MutableStateFlow(MobileQuote())
    private val _lastSelectedTabIndex = MutableLiveData<Int?>()
    private val _viewState = MutableLiveData<ViewState>()
    private val _symbol = MutableLiveData<SearchResultItem.Symbol>()

    private val disposable: CompositeDisposable = CompositeDisposable()
    private val streamingCompositeDisposable = CompositeDisposable()

    private var primaryPaneQuoteItems: QuoteItems = QuoteItems(emptyList())
    private var secondaryPaneQuoteItems: QuoteItems = QuoteItems(emptyList())
    private var newsItems: List<QuoteItemInterface> = emptyList()
    private var holdingsItems: List<QuoteItemInterface> = emptyList()

    private var optionSnapshotItems: List<QuoteItemInterface> = emptyList()
    private var optionUnderlyerSymbolItems: List<QuoteItemInterface> = emptyList()

    private var activeJob: Job? = null
    private var marketStatus: MarketScheduleStatus = MarketScheduleStatus.UNKNOWN

    private var mobileQuote: MobileQuote? = null
    private var newsItemsResult: ETResult<List<NewsItem.Text>>? = null
    private var isStreamingEnabledOnLastUpdate = false

    val openShowAffiliatedDialog: LiveData<Boolean?>
        get() = mutableShowAffiliatedDialogSignal
    private val mutableShowAffiliatedDialogSignal: MutableLiveData<Boolean?> = MutableLiveData()

    var isInDualPane: Boolean = false
        set(value) {
            if (field == value) { return }
            field = value

            launchInvalidateViewState()
        }

    override val mobileQuoteDataFlow: StateFlow<MobileQuote>
        get() = _quoteDescription

    override val quoteWidgetItemFlow: Flow<QuoteWidgetItem>
        get() = _quoteDescription.map {
            return@map quoteMapper.map(it, isExtendedHoursOnUseCase())
        }

    init {
        symbolTrackingHighlightWatchlistActionSignal = walkthroughStatesViewModel.createConditionalSignal<SymbolTrackingState, ViewState, Success>(
            viewState = viewState,
            goalState = SymbolTrackingState.HighlightWatchlistActionInQuotePage,
            successStatePredicate = { successState ->
                successState.results
                    .let { (quoteItems, _) -> quoteItems }
                    .isNotEmpty()
            }
        )
    }

    override fun onCleared() {
        super.onCleared()
        clear()
    }

    fun fetch(ticker: SearchResultItem.Symbol) {
        if (_symbol.value?.title == ticker.title) {
            if (streamingStatusController.isStreamingToggleEnabled && !isStreamingEnabledOnLastUpdate) {
                launchInvalidateViewState()
            }
            return
        }

        _symbol.value = ticker

        refresh()
    }

    override fun refresh() {
        val ticker = _symbol.value ?: return

        _viewState.value = ViewState.Loading
        activeJob?.cancel()
        activeJob = viewModelScope.launch {
            val parameter = GetQuoteParameter(ticker, isExtendedHoursOnUseCase())
            val quotesDeferred = async { getMobileQuoteUseCase(parameter) }
            val newsDeferred = async { getQuoteNewsUseCase(parameter) }
            val holdingsDeferred = async { getPositionHoldingsUseCase(parameter) }
            handleDeferred(quotesDeferred, newsDeferred, holdingsDeferred)
        }
    }

    fun selectTab(index: Int) {
        _lastSelectedTabIndex.postValue(index)
        updateTabState(index)
    }

    private suspend fun handleDeferred(
        quotesDeferred: Deferred<ETResult<MobileQuote>>,
        newsDeferred: Deferred<ETResult<List<NewsItem.Text>>>,
        holdingsDeferred: Deferred<ETResult<List<QuoteItemInterface>>>
    ) {
        val quoteResult = quotesDeferred.await()
        if (quoteResult.isSuccess) {
            holdingsItems = holdingsDeferred.await().fold(
                onSuccess = { it },
                onFailure = { emptyList() }
            )
            mobileQuote = quoteResult.getOrThrow()
            newsItemsResult = newsDeferred.await()
            invalidateViewState()
        } else {
            val exception = quoteResult.exceptionOrNull()
            logger.error("Could not load quote(s)", exception)
            _viewState.value = ViewState.Error(exception?.localizedMessage)
        }
    }

    private suspend fun invalidateViewState() {
        val ticker = _symbol.value ?: return
        val news = newsItemsResult
        @Suppress("FoldInitializerAndIfToElvis") // Result is not allowed to be used with Elvis operator.
        if (news == null) { return }

        onNewMobileQuote()
        val noFooter = isInDualPane || ticker.type.isIndex
        newsItems = mapQuoteNewsUseCase(MapQuoteNewsParameter(ticker, news, noFooter))
        selectTab(_lastSelectedTabIndex.value ?: 0)
    }

    private suspend fun onNewMobileQuote() {
        mobileQuote?.let { mobileQuote ->
            val newSymbol = _symbol.value?.copy(
                securityType = mobileQuote.subSecType,
                underlyingTypeCode = mobileQuote.underlyingTypeCode
            ) ?: createNewSymbol(mobileQuote)
            _symbol.value = newSymbol
            securityAndOrderTypeHelper.updateCurrentSymbol(newSymbol)

            _quoteDescription.value = mobileQuote

            isStreamingEnabledOnLastUpdate = streamingStatusController.isStreamingToggleEnabled
            if (isInDualPane) {
                val quoteItems =
                    mapDualPaneMobileQuoteUseCase(MapMobileQuoteUseCaseParameter(mobileQuote, isStreamingEnabledOnLastUpdate, holdingsItems.isNotEmpty()))
                primaryPaneQuoteItems = quoteItems.first
                secondaryPaneQuoteItems = quoteItems.second
            } else {
                primaryPaneQuoteItems =
                    mapMobileQuoteUseCase(MapMobileQuoteUseCaseParameter(mobileQuote, isStreamingEnabledOnLastUpdate, holdingsItems.isNotEmpty()))
            }
            optionSnapshotItems = primaryPaneQuoteItems.snapshot.filter { it !is QuoteItem.ClosingPriceItem }
            optionUnderlyerSymbolItems = primaryPaneQuoteItems.snapshot.filterIsInstance<QuoteItem.ClosingPriceItem>()

            if (isStreamingEnabledOnLastUpdate) {
                streamQuotes()
            }
        }
    }

    private fun createNewSymbol(mobileQuote: MobileQuote): SearchResultItem.Symbol =
        SearchResultItem.Symbol(
            mobileQuote.symbol,
            mobileQuote.typeCode,
            securityType = mobileQuote.subSecType,
            underlyingTypeCode = mobileQuote.underlyingTypeCode
        )

    private fun updateTabState(tabIndex: Int) {
        viewModelScope.launch {
            _viewState.value = Success(
                if (isInDualPane) {
                    Pair(
                        getQuoteDetailsUseCase(GetQuoteDetailsUseCaseParameter(primaryPaneQuoteItems, emptyList(), tabSelectors[tabIndex])),
                        getQuoteDetailsUseCase(GetQuoteDetailsUseCaseParameter(secondaryPaneQuoteItems, newsItems, tabSelectors[tabIndex]))
                    )
                } else {
                    Pair(
                        getQuoteDetailsUseCase(GetQuoteDetailsUseCaseParameter(primaryPaneQuoteItems, newsItems, tabSelectors[tabIndex])),
                        getQuoteDetailsUseCase(GetQuoteDetailsUseCaseParameter(secondaryPaneQuoteItems, emptyList(), tabSelectors[tabIndex]))
                    )
                }
            )
        }
    }

    private fun streamQuotes() {
        streamingCompositeDisposable.clear()
        marketStatus = MarketScheduleStatus.UNKNOWN
        mobileQuote?.let { quote ->
            streamMarketStatus(quote)
            if (!quote.typeCode.isOption) {
                streamingCompositeDisposable.add(
                    streamingQuoteProvider.getQuote(quote.symbol, quote.typeCode, stockSubscriptionFields).subscribeBy { data ->
                        viewModelScope.launch {
                            val isExtendedHours = isExtendedHoursOnUseCase()
                            primaryPaneQuoteItems.snapshot.forEach {
                                logger.debug("stock data: ${data.lastPrice}")
                                marketTradeStatusDelegate.onMarketStatusUpdate(data)
                                it.updateItem(data, isExtendedHours)
                            }
                        }
                    }
                )
            }
        }
    }

    private fun streamMarketStatus(quote: MobileQuote) {
        marketScheduleProviderHolder.marketScheduleProvider
            ?.getMarketSchedule()
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeBy {
                val newMarketStatus = it.status
                if (marketStatus != newMarketStatus) {
                    val invalidateView = marketStatus != MarketScheduleStatus.UNKNOWN
                    marketStatus = newMarketStatus
                    // we only want to invalidate if we updated from a known market status to another known one
                    if (invalidateView) {
                        refresh()
                    } else if (quote.typeCode.isOption) {
                        handleOptionStreaming(quote)
                    }
                }
            }?.addTo(streamingCompositeDisposable)
    }

    private fun launchInvalidateViewState() {
        viewModelScope.launch {
            invalidateViewState()
        }
    }

    private fun handleOptionStreaming(quote: MobileQuote) {
        if (marketStatus == MarketScheduleStatus.REGULAR &&
            streamingStatusController.isStreamingToggleEnabled
        ) {
            streamingCompositeDisposable.add(
                streamingQuoteProvider.getQuote(quote.symbol, quote.typeCode, optionSubscriptionFields).subscribeBy { data ->
                    optionSnapshotItems.forEach {
                        logger.debug("options data: ${data.lastPrice}")
                        it.updateItem(data)
                    }
                }
            )
            streamingCompositeDisposable.add(
                streamingQuoteProvider.getQuote(
                    quote.underlyingSymbol,
                    quote.underlyingTypeCode,
                    underlyerSymbolSubscriptionFields
                ).subscribeBy { data ->
                    optionUnderlyerSymbolItems.forEach {
                        logger.debug("underlying symbol data: ${data.lastPrice}")
                        it.updateItem(data)
                    }
                }
            )
        } else {
            streamingCompositeDisposable.clear()
        }
    }

    fun setDisclosureState(state: Boolean) {
        _showDisclosures.value = state
    }

    private fun clear() {
        marketStatus = MarketScheduleStatus.UNKNOWN
        disposable.clear()
        streamingCompositeDisposable.clear()
    }

    sealed class ViewState {
        object Loading : ViewState()
        data class Error(val message: String?) : ViewState()
        data class Success(val results: Pair<List<QuoteItemInterface>, List<QuoteItemInterface>>) : ViewState()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onLifeCyclePause() {
        /** We want to unsubscribe from streaming */
        streamingCompositeDisposable.clear()
        marketTradeStatusDelegate.resetMarketStatus()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onLifeCycleResume() {
        /** We need to subscribe streaming here because gets unsubscribe onPause lifecycle */
        if (streamingStatusController.isStreamingToggleEnabled) {
            streamQuotes()
        } else {
            streamingCompositeDisposable.clear()
        }
    }

    fun showAffiliatedDialog() {
        mutableShowAffiliatedDialogSignal.value = true
    }

    fun closeAffiliatedDialog() {
        mutableShowAffiliatedDialogSignal.value = false
    }
}
