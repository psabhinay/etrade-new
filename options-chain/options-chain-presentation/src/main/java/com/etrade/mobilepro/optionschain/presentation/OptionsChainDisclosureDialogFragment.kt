package com.etrade.mobilepro.optionschain.presentation

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.optionschain.tableview.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.chromium.customtabsclient.shared.internalUrlToUri
import javax.inject.Inject

private const val DISCLOSURES_URL = "/l/f/disclosure-library/options-price-reporting-authority"

class OptionsChainDisclosureDialogFragment @Inject constructor(
    @Web private val baseUrl: String,
    private val optionsChainRouter: OptionsChainRouter
) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return context?.let {
            createDialog(positiveListener, cancelListener, characteristicsAndRisksListener)
        } ?: super.onCreateDialog(savedInstanceState)
    }

    private val characteristicsAndRisksListener = DialogInterface.OnClickListener { _, _ ->
        optionsChainRouter.openDisclosuresCharacteristicsAndRisks(findNavController())
    }

    private val positiveListener = DialogInterface.OnClickListener { _, _ ->
        val url = baseUrl + DISCLOSURES_URL
        findNavController().navigate(internalUrlToUri(getString(R.string.options_chain_disclosure), url, R.drawable.ic_close, true))
    }

    private val cancelListener = DialogInterface.OnClickListener { dialog, _ ->
        dialog.dismiss()
    }

    private fun createDialog(
        positiveListener: DialogInterface.OnClickListener,
        cancelListener: DialogInterface.OnClickListener,
        characteristicsAndRisksListener: DialogInterface.OnClickListener
    ): Dialog {
        val disclosureDialog = MaterialAlertDialogBuilder(requireContext())
            .setCancelable(true)
            .setTitle(getString(R.string.options_chain_disclosure))
            .setMessage(getString(R.string.options_chain_disclosure_description))
            .setPositiveButton(getString(R.string.options_chain_view_disclosure), positiveListener)
            .setNegativeButton(getString(R.string.cancel), cancelListener)
            .setNeutralButton(getString(R.string.options_chain_characteristics_risks), characteristicsAndRisksListener)
            .create()
        disclosureDialog.setOnShowListener {
            val btnNeutral = disclosureDialog.getButton(DialogInterface.BUTTON_NEUTRAL)
            btnNeutral.setSingleLine(false)
            btnNeutral.gravity = Gravity.END
        }
        return disclosureDialog
    }
}
