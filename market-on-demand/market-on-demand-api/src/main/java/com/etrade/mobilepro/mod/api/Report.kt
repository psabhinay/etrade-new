package com.etrade.mobilepro.mod.api

data class Report(

    /**
     * The provider who created this report
     */
    val provider: String?,

    /**
     * Http URL of the report pdf
     */
    val reportUrl: String?,

    /**
     * report date of when this report was created
     */
    val reportDate: String?
)
