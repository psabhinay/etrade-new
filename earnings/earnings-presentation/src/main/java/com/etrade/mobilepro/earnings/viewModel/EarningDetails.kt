package com.etrade.mobilepro.earnings.viewModel

import com.etrade.mobilepro.earnings.EarningsDetailData
import java.math.BigDecimal
import java.text.DecimalFormat

private val ONE_HUNDRED = BigDecimal(100)
private const val DIVIDER_SCALE = 5
private val VALUE_TEMPLATE: DecimalFormat = DecimalFormat("#.##")

data class EarningDetails(
    val estimateLabel: String,
    val estimate: String,
    val actualLabel: String,
    val actual: String,
    val surpriseLabel: String,
    val surprise: String,
    val historyQuartersRange: String?,
    val historyQuartersValue: String,
    val forecastQuartersRange: String?,
    val forecastQuartersValue: String,
    val surpriseTextColor: Int,
    val historyQuartersTextColor: Int,
    val forecastQuartersTextColor: Int,
    val historyQuartersDescription: String?,
    val forecastQuartersDescription: String?
)

data class EarningDetailsResources(
    val noValue: String,
    val estimate: String,
    val highEst: String,
    val actual: String,
    val meanEst: String,
    val surprise: String,
    val lowEst: String,
    val textColorNormal: Int,
    val textColorRed: Int,
    val textColorGreen: Int,
    val quartersPeriodTemplate: String,
    val selectedQuarterDescriptionTemplate: String
)

private fun getEstimate(item: EarningsDetailData, isFuturePeriod: Boolean): BigDecimal? {
    return if (isFuturePeriod) {
        item.high
    } else {
        item.surpriseMean
    }
}

private fun getActual(item: EarningsDetailData, isFuturePeriod: Boolean): BigDecimal? {
    return if (isFuturePeriod) {
        item.estMean
    } else {
        item.actualValue
    }
}

private fun getSurpriseValue(item: EarningsDetailData, isFuturePeriod: Boolean): BigDecimal? {
    return if (isFuturePeriod) {
        item.low
    } else {
        item.actualValue?.let { actual ->
            item.surpriseMean?.let { estimate ->
                actual - estimate
            }
        }
    }
}

private fun getSurprisePercent(surpriseValue: BigDecimal?, estimateValue: BigDecimal?): BigDecimal? {
    return surpriseValue?.let { surprise ->
        estimateValue?.let { estimate ->
            val divisor: BigDecimal? = estimate.abs().divideOrNull(ONE_HUNDRED, DIVIDER_SCALE, BigDecimal.ROUND_UP)

            divisor?.let { (surprise.divideOrNull(it, DIVIDER_SCALE, BigDecimal.ROUND_UP)) }
        }
    }
}

private fun getSurpriseFormatted(surpriseValue: BigDecimal?, surprisePercent: BigDecimal?, isFuturePeriod: Boolean): String? {
    return if (isFuturePeriod) {
        surpriseValue?.toString()
    } else if (surpriseValue != null && surprisePercent != null) {
        "$surpriseValue (${VALUE_TEMPLATE.format(surprisePercent)}%)"
    } else {
        null
    }
}

private fun getHistoryQuarters(selectedGraphData: GraphData, items: List<GraphData>): Pair<EarningsDetailData, EarningsDetailData>? {
    return items
        .filter { it.quarter == selectedGraphData.quarter && !it.isFuturePeriod }
        .takeIf { it.size > 1 }
        ?.let { Pair(it.first().earningsDetailData, it.last().earningsDetailData) }
}

private fun getForecastQuarters(selectedGraphData: GraphData, items: List<GraphData>): Pair<EarningsDetailData, EarningsDetailData>? {
    val selectedQuarterItems: List<GraphData> = items.filter { it.quarter == selectedGraphData.quarter }

    val lastHistoryPeriod: EarningsDetailData? = selectedQuarterItems
        .lastOrNull { !it.isFuturePeriod }
        ?.earningsDetailData

    val firstForecastPeriod: EarningsDetailData? = selectedQuarterItems
        .firstOrNull { it.isFuturePeriod }
        ?.earningsDetailData

    return if (lastHistoryPeriod != null && firstForecastPeriod != null) {
        Pair(lastHistoryPeriod, firstForecastPeriod)
    } else {
        null
    }
}

private fun getQuartersPeriodLabel(period: Pair<EarningsDetailData, EarningsDetailData>?, resources: EarningDetailsResources): String? {
    return period?.let {
        resources.quartersPeriodTemplate.format(it.first.quarter, it.first.fiscalPeriodYear, it.second.quarter, it.second.fiscalPeriodYear)
    }
}

private fun getQuartersDescription(period: Pair<EarningsDetailData, EarningsDetailData>?, quartersValue: String, resources: EarningDetailsResources): String? {
    return period?.let {
        resources.selectedQuarterDescriptionTemplate.format(
            it.first.quarter,
            it.first.fiscalPeriodYear,
            it.second.quarter,
            it.second.fiscalPeriodYear,
            quartersValue
        )
    }
}

private fun getQuartersPercent(firstValue: BigDecimal?, secondValue: BigDecimal?): BigDecimal? {
    if (firstValue != null && secondValue != null) {
        val divisor: BigDecimal? = firstValue.abs().divide(ONE_HUNDRED, DIVIDER_SCALE, BigDecimal.ROUND_UP)

        return divisor?.let { (secondValue - firstValue).divideOrNull(it, DIVIDER_SCALE, BigDecimal.ROUND_UP) }
    }

    return null
}

private fun getQuartersPercentFormatted(percent: BigDecimal?): String? {
    return percent?.let { "${VALUE_TEMPLATE.format(it)}%" }
}

private fun getTextColorForValue(value: BigDecimal?, resources: EarningDetailsResources): Int {
    return value?.let {
        if (it < BigDecimal.ZERO) {
            resources.textColorRed
        } else {
            resources.textColorGreen
        }
    } ?: resources.textColorNormal
}

@Suppress("LongMethod", "ComplexMethod")
fun createEarningDetails(selectedIndex: Int, items: List<GraphData>, resources: EarningDetailsResources): EarningDetails {
    val selectedGraphData: GraphData = items[selectedIndex]
    val selectedMarketPeriod: EarningsDetailData = selectedGraphData.earningsDetailData
    val isFuturePeriod: Boolean = selectedGraphData.isFuturePeriod

    val historyPeriod: Pair<EarningsDetailData, EarningsDetailData>? = getHistoryQuarters(selectedGraphData, items)
    val forecastPeriod: Pair<EarningsDetailData, EarningsDetailData>? = getForecastQuarters(selectedGraphData, items)
    val historyQuartersValue = getQuartersPercent(historyPeriod?.first?.actualValue, historyPeriod?.second?.actualValue)
    val forecastQuartersValue = getQuartersPercent(forecastPeriod?.first?.actualValue, forecastPeriod?.second?.estMean)

    val surpriseValue = getSurpriseValue(selectedMarketPeriod, isFuturePeriod)
    val surprisePercent = if (isFuturePeriod) null else getSurprisePercent(surpriseValue, selectedMarketPeriod.surpriseMean)

    val historyQuartersValueFormatted = getQuartersPercentFormatted(historyQuartersValue) ?: resources.noValue
    val forecastQuartersValueFormatted = getQuartersPercentFormatted(forecastQuartersValue) ?: resources.noValue

    return EarningDetails(
        estimate = getEstimate(selectedMarketPeriod, isFuturePeriod)?.toString() ?: resources.noValue,
        estimateLabel = if (isFuturePeriod) resources.highEst else resources.estimate,
        actual = getActual(selectedMarketPeriod, isFuturePeriod)?.toString() ?: resources.noValue,
        actualLabel = if (isFuturePeriod) resources.meanEst else resources.actual,
        surprise = getSurpriseFormatted(surpriseValue, surprisePercent, isFuturePeriod) ?: resources.noValue,
        surpriseLabel = if (isFuturePeriod) resources.lowEst else resources.surprise,
        historyQuartersRange = getQuartersPeriodLabel(historyPeriod, resources),
        historyQuartersValue = historyQuartersValueFormatted,
        forecastQuartersRange = getQuartersPeriodLabel(forecastPeriod, resources),
        forecastQuartersValue = forecastQuartersValueFormatted,
        surpriseTextColor = if (isFuturePeriod) resources.textColorNormal else getTextColorForValue(surpriseValue, resources),
        historyQuartersTextColor = getTextColorForValue(historyQuartersValue, resources),
        forecastQuartersTextColor = getTextColorForValue(forecastQuartersValue, resources),
        historyQuartersDescription = getQuartersDescription(historyPeriod, historyQuartersValueFormatted, resources),
        forecastQuartersDescription = getQuartersDescription(forecastPeriod, forecastQuartersValueFormatted, resources)
    )
}

private fun BigDecimal.divideOrNull(divisor: BigDecimal, scale: Int, roundingMode: Int): BigDecimal? {
    return if (divisor.compareTo(BigDecimal.ZERO) == 0) {
        null
    } else {
        divide(divisor, scale, roundingMode)
    }
}
