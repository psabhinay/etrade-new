package com.etrade.mobilepro.trade.presentation.form.delegate.shared

import android.content.res.Resources
import androidx.annotation.StringRes
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.util.createNotEligibleSymbolDialog

internal fun OnActionClickListener.showNotEligibleSymbolDialog(resources: Resources, @StringRes message: Int) {
    showDialog(
        resources.createNotEligibleSymbolDialog(message) {
            dismissAction = {
                hideDialog()
            }
        }
    )
}

internal fun OnActionClickListener.showInvalidMutualFundDialog(resources: Resources, errorMessage: String) {
    showDialog {
        title = resources.getString(R.string.title_note)
        message = errorMessage
        positiveAction {
            label = resources.getString(R.string.ok)
        }
        dismissAction = {
            hideDialog()
        }
    }
}

internal fun OnActionClickListener.showAffiliatedProductDialog(resources: Resources) {
    showDialog {
        title = resources.getString(R.string.affiliated_product_title)
        message = resources.getString(R.string.affiliated_product_body)
        positiveAction {
            label = resources.getString(R.string.ok)
        }
    }
}
