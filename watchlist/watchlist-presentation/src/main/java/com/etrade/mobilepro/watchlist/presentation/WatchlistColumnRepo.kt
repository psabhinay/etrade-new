package com.etrade.mobilepro.watchlist.presentation

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.tableviewutils.presentation.data.BaseColumnRepo
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.WatchlistColumn
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnInterface
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnRepo
import javax.inject.Inject

private const val COLUMN_ASK = "Ask"
private const val COLUMN_BID = "Bid"
private const val COLUMN_CHANGE_DOLLAR = "ChangeDollar"
private const val COLUMN_CHANGE_PERCENT = "ChangePercent"
private const val COLUMN_DAY_HIGH = "DayHigh"
private const val COLUMN_DAY_LOW = "DayLow"
private const val COLUMN_EARNINGS_PER_SHARE = "EarningsPerShare"
private const val COLUMN_FIFTY_TWO_WEEK_HIGH = "FiftyTwoWeekHigh"
private const val COLUMN_FIFTY_TWO_WEEK_LOW = "FiftyTwoWeekLow"
private const val COLUMN_LAST_PRICE = "LastPrice"
private const val COLUMN_PRICE_EARNINGS_RATION = "PriceEarningRatio"
private const val COLUMN_VOLUME = "Volume"

/**
 * Implementation of [WatchlistColumnRepo] that uses [storage] to persist columns order.
 */
class WatchlistColumnRepoImpl @Inject constructor(storage: KeyValueStorage) :
    BaseColumnRepo<WatchlistColumnInterface>(storage),
    WatchlistColumnRepo {

    override val storageKey: String = "watchlistTableColumns"

    override val defaultColumnsOrder: List<String> = listOf(
        COLUMN_LAST_PRICE,
        COLUMN_CHANGE_DOLLAR,
        COLUMN_CHANGE_PERCENT,
        COLUMN_VOLUME,
        COLUMN_BID,
        COLUMN_ASK,
        COLUMN_DAY_HIGH,
        COLUMN_DAY_LOW,
        COLUMN_FIFTY_TWO_WEEK_HIGH,
        COLUMN_FIFTY_TWO_WEEK_LOW,
        COLUMN_PRICE_EARNINGS_RATION,
        COLUMN_EARNINGS_PER_SHARE
    )

    override val allColumns: Map<String, WatchlistColumn> = mapOf(
        COLUMN_ASK to WatchlistColumn.Ask,
        COLUMN_BID to WatchlistColumn.Bid,
        COLUMN_CHANGE_DOLLAR to WatchlistColumn.ChangeDollar,
        COLUMN_CHANGE_PERCENT to WatchlistColumn.ChangePercent,
        COLUMN_DAY_HIGH to WatchlistColumn.DayHigh,
        COLUMN_DAY_LOW to WatchlistColumn.DayLow,
        COLUMN_EARNINGS_PER_SHARE to WatchlistColumn.EarningsPerShare,
        COLUMN_FIFTY_TWO_WEEK_HIGH to WatchlistColumn.FiftyTwoWeekHigh,
        COLUMN_FIFTY_TWO_WEEK_LOW to WatchlistColumn.FiftyTwoWeekLow,
        COLUMN_LAST_PRICE to WatchlistColumn.LastPrice,
        COLUMN_PRICE_EARNINGS_RATION to WatchlistColumn.PriceEarningRatio,
        COLUMN_VOLUME to WatchlistColumn.Volume
    )
}
