package com.etrade.mobilepro.quote.dto

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class QuoteDataContainer(
    @Json(name = "QuoteResponse")
    val quotes: List<QuoteResponse>? = null
) : BaseDataDto()

interface QuoteResponse

@JsonClass(generateAdapter = true)
class QuoteStockDto : QuoteDto()

@JsonClass(generateAdapter = true)
class QuoteIndexDto : QuoteDto()

@JsonClass(generateAdapter = true)
class QuoteOptionsDto : QuoteDto()

@JsonClass(generateAdapter = true)
class QuoteBondDto : QuoteDto()

@JsonClass(generateAdapter = true)
class QuoteETFDto : QuoteDto()

@JsonClass(generateAdapter = true)
open class QuoteDto : QuoteResponse {
    @Json(name = "symbol")
    var symbol: String? = null
    @Json(name = "symbolDescription")
    var description: String? = null
    @Json(name = "exchangeCode")
    var exchangeCode: String? = null
    @Json(name = "exchangeName")
    var exchangeName: String? = null
    @Json(name = "typeCode")
    var typeCode: String? = null
    @Json(name = "lastPrice")
    var lastPrice: String? = null
    @Json(name = "change")
    var change: String? = null
    @Json(name = "percentChange")
    var percentChange: String? = null
    @Json(name = "bid")
    var bid: String? = null
    @Json(name = "bidSize")
    var bidSize: String? = null
    @Json(name = "ask")
    var ask: String? = null
    @Json(name = "askSize")
    var askSize: String? = null
    @Json(name = "volume")
    var volume: String? = null
    @Json(name = "openPrice")
    var openPrice: String? = null
    @Json(name = "previousClose")
    var previousClose: String? = null
    @Json(name = "timeOfLastTrade")
    var timestampOfLastTrade: Long? = null
    @Json(name = "dayHi")
    var dayHigh: String? = null
    @Json(name = "dayLo")
    var dayLow: String? = null
    @Json(name = "delayedorrtq")
    var delayedOrRtq: Int? = null
    @Json(name = "timeZone")
    var timeZone: String? = null
    @Json(name = "lastTradeTime")
    var lastTradeTime: String? = null
    @Json(name = "specialEvents")
    var specialEvents: QuoteSpecialEventsDto? = null
    @Json(name = "week52Lo")
    var week52Low: String? = null
    @Json(name = "week52Hi")
    var week52High: String? = null
    @Json(name = "week52LoDate")
    var week52LowDate: DateStructDto? = null
    @Json(name = "week52HiDate")
    var week52HighDate: DateStructDto? = null
    @Json(name = "marketCap")
    var marketCap: String? = null
    @Json(name = "sharesOutstanding")
    var sharesOutstanding: String? = null
    @Json(name = "averageVolume")
    var averageVolume: String? = null
    @Json(name = "pe")
    var pe: String? = null
    @Json(name = "eps")
    var eps: String? = null
    @Json(name = "dividendYield")
    var dividendYield: String? = null
    @Json(name = "declaredDividend")
    var declaredDividend: String? = null
    @Json(name = "dividendPayDate")
    var dividentPayDate: DateStructDto? = null
    @Json(name = "beta")
    var beta: String? = null
    @Json(name = "secYield")
    var secYield: String? = null
    @Json(name = "exDividendDate")
    var exDividendDate: DateStructDto? = null
    @Json(name = "subSecType")
    var subSecType: String? = null
    @Json(name = "hasMiniOptions")
    var hasMiniOptions: Boolean? = null
    @Json(name = "msRating")
    var msRating: String? = null
    @Json(name = "fundCategory")
    var fundCategory: String? = null
    @Json(name = "showDrip")
    var showDrip: Int? = null
    @Json(name = "commissionFree")
    var commissionFree: Boolean? = null
    @Json(name = "isOptionable")
    var isOptionable: Boolean? = null
    @Json(name = "cusip")
    var cusip: String? = null
    @Json(name = "overnightTradingFlag")
    var overnightTrading: Int? = null
    @Json(name = "oneYYield")
    var oneYYield: String? = null
    @Json(name = "bidExchange")
    var bidExchange: String? = null
    @Json(name = "askExchange")
    var askExchange: String? = null
    @Json(name = "tradeExchange")
    var tradeExchange: String? = null
    @Json(name = "tradeSize")
    var tradeSize: String? = null
    @Json(name = "allStar")
    var allStar: Boolean? = null
    @Json(name = "exchangeTradeNotes")
    var exchangeTradeNotes: Boolean? = null
    @Json(name = "leveragedFund")
    var leveragedFund: Boolean? = null
    @Json(name = "inverseFund")
    var inverseFund: Boolean? = null
    @Json(name = "commodity")
    var commodity: Boolean? = null
    @Json(name = "overNightExchange")
    var overnightExchange: Boolean? = null
    @Json(name = "dstFlag")
    var dst: Boolean? = null
    @Json(name = "nextEarningDate")
    var nextEarningDate: DateStructDto? = null
    @Json(name = "extendedQuoteHrDetail")
    var extendedQuoteHrDetail: ExtendedHoursQuoteDto? = null
    @Json(name = "availability")
    var availability: String? = null
    @Json(name = "ahFlag")
    var ahFlag: Any? = null
    @Json(name = "underLyingSymbol")
    var underlyingSymbol: String? = null
    @Json(name = "rho")
    var rho: String? = null
    @Json(name = "vega")
    var vega: String? = null
    @Json(name = "theta")
    var theta: String? = null
    @Json(name = "gamma")
    var gamma: String? = null
    @Json(name = "deliverableContract")
    var deliverableContract: String? = null
    @Json(name = "daysToExpire")
    var daysToExpire: String? = null
    @Json(name = "intrinsicValue")
    var intrinsicValue: String? = null
    @Json(name = "timePremium")
    var timePremium: String? = null
    @Json(name = "openInterest")
    var openInterest: String? = null
    @Json(name = "delta")
    var delta: String? = null
    @Json(name = "iv")
    var iv: String? = null
    @Json(name = "underLyingSymbolDescription")
    var underLyingSymbolDescription: String? = null
    @Json(name = "underLyingExchangeCode")
    var underLyingExchangeCode: String? = null
    @Json(name = "underLyingExchangeName")
    var underLyingExchangeName: String? = null
    @Json(name = "underLyingLastPrice")
    var underLyingLastPrice: String? = null
    @Json(name = "underLyingChange")
    var underLyingChange: String? = null
    @Json(name = "underLyingPercentChange")
    var underLyingPercentChange: String? = null
    @Json(name = "underLyingVolume")
    var underLyingVolume: String? = null
    @Json(name = "affiliatedStatusCd")
    var affiliatedStatusCd: String? = null
}

@JsonClass(generateAdapter = true)
data class QuoteSpecialEventsDto(
    @Json(name = "nextEarningDateFlag")
    val nextEarningData: Boolean?,
    @Json(name = "dividendPayDateFlag")
    val dividendPayDate: Boolean?,
    @Json(name = "exDividendDateFlag")
    val exDividendDate: Boolean?,
    @Json(name = "splitRatioFlag")
    val splitRatio: Boolean?,
    @Json(name = "tradeHaltFlag")
    val tradeHalt: Boolean?,
    @Json(name = "daysToExpirationFlag")
    val daysToExpiration: Boolean?,
    @Json(name = "qtyFactorSplitFrom")
    val qtyFactorSplitFrom: Int?,
    @Json(name = "qtyFactorSplitTo")
    val qtyFactorSplitTo: Int?
)

@JsonClass(generateAdapter = true)
data class DateStructDto(
    @Json(name = "day")
    val day: String?,
    @Json(name = "month")
    val month: String?,
    @Json(name = "year")
    val year: String?
)

@JsonClass(generateAdapter = true)
data class ExtendedHoursQuoteDto(
    @Json(name = "lastPrice")
    val lastPrice: String?,
    @Json(name = "change")
    val change: String?,
    @Json(name = "percentChange")
    val percentChange: String?,
    @Json(name = "bid")
    val bid: String?,
    @Json(name = "bidSize")
    val bidSize: String?,
    @Json(name = "ask")
    val ask: String?,
    @Json(name = "askSize")
    val askSize: String?,
    @Json(name = "volume")
    val volume: String?,
    @Json(name = "timeOfLastTrade")
    val timeOfLastTrade: Long?
)
