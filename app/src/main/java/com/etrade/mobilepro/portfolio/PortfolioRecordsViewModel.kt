package com.etrade.mobilepro.portfolio

import android.content.res.Resources
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import androidx.paging.PagingSource
import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.account.dao.AccountDao
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.instrument.dao.InstrumentDao
import com.etrade.mobilepro.instrument.isOption
import com.etrade.mobilepro.instrument.underlier
import com.etrade.mobilepro.livedata.combineWith
import com.etrade.mobilepro.livedata.concatCombine
import com.etrade.mobilepro.livedata.concatCombineCoroutine
import com.etrade.mobilepro.portfolio.action.PortfolioRecordActionProvider
import com.etrade.mobilepro.portfolio.data.PositionIdDataSource
import com.etrade.mobilepro.portfolio.data.merge
import com.etrade.mobilepro.portfolio.delegate.PortfolioSortDelegate
import com.etrade.mobilepro.portfolio.livedata.InstrumentsLiveData
import com.etrade.mobilepro.portfolio.livedata.PositionsLiveData
import com.etrade.mobilepro.portfolio.model.PageLoadingState
import com.etrade.mobilepro.portfolio.model.SimpleInstrument
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableData
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.positions.dao.PortfolioPositionDao
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.tableview.TableFooter
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.etrade.mobilepro.util.throttleLast
import com.evrencoskun.tableview.listener.ITableViewListener
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.concurrent.Executor
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

const val MESSAGE_ACTION_SHEET = 1

private const val SUBSEQUENT_PAGE_SIZE = 20

/**

Here is the flow that is implemented by this class in Mermaid (https://mermaidjs.github.io/) notation:

```mermaid
graph LR
A -- Update --> localSortInfo
positionsLiveData -- Update --> instrumentLiveData
instrumentLiveData -- Merge in --> positionRecordsLiveData
localSortInfo -- Apply to --> orderedPositionRecordsLiveData a.k.a records
```

You may use StackEdit (https://stackedit.io/) to preview it.

*/

@Suppress("TooManyFunctions", "LargeClass", "DEPRECATION", "LongParameterList")
class PortfolioRecordsViewModel constructor(
    val accountUuid: String,
    private val accountRepo: DefaultAccountRepo,
    private val dataColorResolver: DataColorResolver,
    private val getPortfolioNextPageUseCase: GetPortfolioNextPageUseCase,
    private val portfolioRecordActionProvider: PortfolioRecordActionProvider,
    private val daoProvider: CachedDaoProvider,
    private val mainThreadExecutor: Executor,
    private val marketTradeStatusDelegate: MarketTradeStatusDelegate,
    private val streamingController: StreamingSubscriptionController,
    private val streamingStatusController: StreamingStatusController,
    private val resources: Resources,
    val sortDelegate: PortfolioSortDelegate,
    val portfolioSettingsRepository: PortfolioSettingsRepository
) : DynamicViewModel(R.layout.view_portfolio_positions), TableFooter, PortfolioViewActionListener {

    private val positionDao: PortfolioPositionDao by lazy { daoProvider.getPositionsDao() }

    private val accountDao: AccountDao by lazy { daoProvider.getAccountDao() }

    private val instrumentDao: InstrumentDao by lazy { daoProvider.getInstrumentDao() }

    override val variableId: Int = BR.obj

    override val primaryTimestamp: LiveData<CharSequence>
        get() = _primaryTimestamp

    override val secondaryTimestamp: LiveData<CharSequence> = MutableLiveData()

    override val isFooterVisible: LiveData<Boolean>
        get() = _isFooterVisible

    var positionsState: PositionsState = portfolioSettingsRepository.getExpandStatus(accountUuid)
        set(value) {
            if (field != value) {
                field = value
                portfolioSettingsRepository.setExpandStatus(accountUuid, value)
                _isExpanded.value = value == PositionsState.EXPANDED
                if (value == PositionsState.EXPANDED) {
                    resumeStreaming()
                } else {
                    pauseStreaming()
                }
            }
        }
        get() = if (sortDelegate.isPortfolioView) {
            PositionsState.EXPANDED
        } else {
            field
        }

    private val _isExpanded = MutableLiveData<Boolean>(positionsState == PositionsState.EXPANDED)

    val isExpanded: LiveData<Boolean>
        get() = _isExpanded

    val accountName: String =
        accountDao.getAccountWithUuid(accountUuid)?.accountShortName ?: accountUuid

    val accountId: String? = accountDao.getAccountWithUuid(accountUuid)?.accountId

    override fun onDisclosuresViewClicked(v: View) {
        clickEvents.postValue(CtaEvent.Disclosures)
    }

    val portfolioTableViewScrollListener: PortfolioTableViewScrollListener =
        object : PortfolioTableViewScrollListener {
            override fun onLastVisibleItemPositionChanged(position: Int) {
                if (sortDelegate.isPortfolioView && positionsState.isExpanded) {
                    val recs = records.value ?: return
                    recs.getOrNull(position)?.let {
                        val index = positionRecordsLiveData.value.orEmpty().indexOf(it)
                        if (index != -1) {
                            positionIdList?.loadAroundSafeIndex(index)
                        }
                    }
                }
            }

            override fun onVisibleItemsChanged(visibleSymbols: Set<Symbol>) {
                if (positionsState.isExpanded) {
                    notifyVisibleItemsChanged(visibleSymbols)
                }
            }
        }

    private fun PagedList<String>.loadAroundSafeIndex(index: Int) {
        if (index >= size && index != 0) {
            if (size > 0) {
                loadAround(size - 1)
            }
        } else {
            loadAround(index)
        }
    }

    val pageLoadingState: LiveData<PageLoadingState>
        get() = _pageLoadingState

    val positionsDisplayMode: LiveData<PortfolioPositionsDisplayMode>
        get() = _positionsDisplayMode

    private val _primaryTimestamp: MutableLiveData<CharSequence> = MutableLiveData()
    private val _isFooterVisible: MutableLiveData<Boolean> = MutableLiveData(false)

    private val positionsLiveData: PositionsLiveData = PositionsLiveData(positionDao)
    private val instrumentLiveData: InstrumentsLiveData = InstrumentsLiveData(instrumentDao)
    private val positionRecordsLiveData: LiveData<List<PortfolioRecord>> =
        instrumentLiveData.concatCombineCoroutine(positionsLiveData, ::combine) { positionsLiveData.value?.size == 0 }
    private val sortInfoAndPositionsDisplayMode: LiveData<Pair<SortInfo, PortfolioPositionsDisplayMode>> =
        sortDelegate.localSortInfo.concatCombine(sortDelegate.positionsDisplayMode) { first, second ->
            Pair(first, second)
        }
    private val orderedPositionRecordsLiveData: MutableLiveData<List<PortfolioRecord>> =
        positionRecordsLiveData.concatCombineCoroutine(sortInfoAndPositionsDisplayMode, ::applySort)

    private val _pageLoadingState: MediatorLiveData<PageLoadingState> = MediatorLiveData()
    private val _records = orderedPositionRecordsLiveData
    val tableData: LiveData<PortfolioRecordsTableData> =
        MediatorLiveData<PortfolioRecordsTableData>().apply {
            val updateItems = {
                runBlocking {
                    val columns = sortDelegate.columns.value
                    val records = _records.value
                    if (columns != null && records != null) {
                        val updatedTable = withContext(Dispatchers.Default) {
                            updateTableRows(columns, records, value, dataColorResolver)
                        }
                        value = updatedTable
                    }
                }
            }
            addSource(_records) { updateItems() }
            addSource(sortDelegate.columns) { updateItems() }
        }

    /**
     * Contains all records in a portfolio.
     */
    val records: LiveData<List<PortfolioRecord>>
        get() = _records

    val showNoPositionsText: LiveData<Boolean>
        get() = records.combineWith(isExpanded) { records, isExpanded ->
            records.isEmpty() && isExpanded == true
        }

    private val fetchExecutor: ExecutorService = Executors.newSingleThreadExecutor()
    private val positionIdListCallback: PagedList.Callback = PagedListCallback()

    private val _positionsDisplayMode = MutableLiveData<PortfolioPositionsDisplayMode>(
        PortfolioPositionsDisplayMode.from(sortDelegate.positionsViewIndex)
    )

    private var dataSource: PositionIdDataSource? = null
    private var positionIdList: PagedList<String>? = null

    private var previousSymbols: Set<String>? = null

    var hasPositions: Boolean = false
        private set

    private var forceFooter: Boolean = false

    private val subscriptionFieldsProvider: (InstrumentType) -> Set<Level1Field> =
        { type -> if (type.isOption) optionFields else stockFields }

    private val visibleSymbolsSubject = BehaviorSubject.create<Set<Symbol>>()

    private val instrumentsToWrite: MutableList<Instrument> = mutableListOf()

    // This holds onto underlying instruments that might never be in realm
    private val missingUnderlyingInstruments: MutableMap<String, Instrument> = mutableMapOf()

    private val throttleUpdate = throttleLast<Unit>(
        coroutineScope = viewModelScope,
        destinationFunction = {
            instrumentDao.updateInstruments(instrumentsToWrite.toList())
            instrumentsToWrite.clear()
        }
    )

    private val updateCallback: (symbol: String, instrumentType: InstrumentType, data: Level1Data) -> Unit =
        { symbol, type, data ->
            val updatedInstrument = instrumentLiveData.getUpdatedInstrument(symbol, data)
            mainThreadExecutor.execute {
                if (positionsState.isExpanded) {
                    if (updatedInstrument == null) {
                        data.lastPrice?.safeParseBigDecimal()?.let {
                            missingUnderlyingInstruments[symbol] =
                                SimpleInstrument(symbol, symbol, symbol, type, lastPrice = it)
                        }
                    } else {
                        missingUnderlyingInstruments.remove(symbol)
                        instrumentsToWrite.add(updatedInstrument)
                    }
                    throttleUpdate(Unit)
                    marketTradeStatusDelegate.onMarketStatusUpdate(data)
                }
            }
        }

    private val positionsObserver = Observer<List<Position>> { positions ->
        runBlocking {
            val (newSymbols, symbolsSet) = withContext(Dispatchers.Default) {
                val list = positions.map { it.symbol }.distinct()
                Pair(list, list.toSet())
            }

            if (previousSymbols != symbolsSet) {
                previousSymbols = symbolsSet
                instrumentLiveData.fetchData(newSymbols)
            }
        }
    }

    init {
        daoProvider.open()

        sortDelegate.apply {
            isEndOfPage = {
                positionIdList?.isImmutable == true || dataSource?.hasNextPage != true
            }
            updateColumns()
        }
        streamingController.initSubscription(
            subscriptionFieldsProvider,
            visibleSymbolsSubject,
            updateCallback
        )

        if (streamingStatusController.isStreamingToggleEnabled) {
            streamingController.resumeSubscription()
        }

        positionsLiveData.observeForever(positionsObserver)
    }

    override fun onCleared() {
        super.onCleared()
        streamingController.cancelSubscription()
        sortDelegate.onCleared()
        positionsLiveData.removeObserver(positionsObserver)
        fetchExecutor.shutdown()
        daoProvider.close()
    }

    fun forceFooter(visible: Boolean) {
        forceFooter = visible
        updateFooterVisibility()
    }

    fun refresh(positionIds: List<String>, timestamp: CharSequence, nextPageToken: String? = null) {
        hasPositions = positionIds.isNotEmpty()
        if (!hasPositions) {
            _records.value = emptyList()
        }
        updateFooterVisibility()

        dataSource?.pageLoadingState?.also { _pageLoadingState.removeSource(it) }

        positionIdList = createPagedList(positionIds, nextPageToken).apply {
            addWeakCallback(null, positionIdListCallback)
        }

        previousSymbols = null

        sortDelegate.setLocalSort()
        positionsLiveData.fetchData(positionIds)
        positionDao.deleteInvalidPositions(accountUuid, positionIds)
        _primaryTimestamp.postValue(timestamp)
    }

    fun createTableViewListener(): ITableViewListener = PortfolioTableViewListener(sortDelegate, this)

    override fun requestActions(position: Int) {
        val actualRecords = records.value ?: return
        if (position >= actualRecords.size) {
            return
        }
        val recordForAction: PortfolioRecord = actualRecords[position]
        if (recordForAction.instrument.instrumentType == InstrumentType.BOND) {
            mutableAppMessageSignal.value =
                ErrorMessage(resources.getString(R.string.portfolio_error_bond_not_supported))
            return
        }

        val actions = portfolioRecordActionProvider.getActions(
            recordForAction,
            findOptionsPositions(recordForAction.instrument.underlyingSymbolInfo?.symbol).size
        )
        if (actions.isNotEmpty()) {
            clickEvents.postClickEvent(MESSAGE_ACTION_SHEET, position, actions)
        }
    }

    fun updatePositionsDisplayMode(mode: PortfolioPositionsDisplayMode) {
        _positionsDisplayMode.value = mode
    }

    fun peekRecord(position: Int): PortfolioRecord? = records.value?.getOrNull(position)

    // Persist Account Id for any trade page navigation.
    fun persistAccountId() {
        viewModelScope.launch {
            accountRepo.setAccountId(accountId)
        }
    }

    fun pauseStreaming() {
        streamingController.pauseSubscription()
        marketTradeStatusDelegate.resetMarketStatus()
    }

    fun resumeStreaming() {
        if (streamingStatusController.isStreamingToggleEnabled && positionsState.isExpanded) {
            streamingController.resumeSubscription()
        } else {
            streamingController.pauseSubscription()
        }
    }

    private fun notifyVisibleItemsChanged(updatedSymbols: Set<Symbol>) {
        val updatedSymbolsSet = mutableSetOf<Symbol>()
        for (symbol in updatedSymbols) {
            if (symbol.instrumentType != InstrumentType.BOND) {
                updatedSymbolsSet.add(symbol)
                if (symbol.isOption) {
                    updatedSymbolsSet.add(Symbol(symbol.underlier))
                }
            }
        }
        visibleSymbolsSubject.onNext(updatedSymbolsSet)
    }

    private suspend fun combine(
        instruments: Map<String, Instrument>,
        positions: List<Position>
    ): List<PortfolioRecord> {
        logger.debug("combine() called")
        return withContext(Dispatchers.Default) {
            positions.map {
                val instrument = instruments[it.symbol]
                val baseSymbolAndPrice = extractBaseSymbolAndPrice(instrument, it, instruments)
                merge(it.positionId, it, instruments[it.symbol], baseSymbolAndPrice)
            }
        }
    }

    private val <T> LiveData<T>.requireValue: T
        get() = value!!

    private fun createPagedList(
        positionIds: List<String>,
        nextPageToken: String?
    ): PagedList<String> {
        val dataSource = PositionIdDataSource(
            accountUuid,
            sortDelegate.localSortInfo.requireValue,
            positionIds,
            nextPageToken,
            getPortfolioNextPageUseCase
        ).also {
            dataSource = it
        }

        _pageLoadingState.addSource(dataSource.pageLoadingState) {
            if (it is PageLoadingState.Error) {
                mutableAppMessageSignal.postValue(ErrorMessage())
                _pageLoadingState.postValue(PageLoadingState.Idle(nextPageToken != null))
            } else {
                _pageLoadingState.postValue(it)
            }
        }

        return PagedList.Builder(dataSource, PagingSource.LoadResult.Page(positionIds, null, nextPageToken), SUBSEQUENT_PAGE_SIZE)
            .setFetchExecutor(fetchExecutor)
            .setNotifyExecutor(mainThreadExecutor)
            .build()
    }

    private fun updateFooterVisibility() {
        _isFooterVisible.value = forceFooter || sortDelegate.isPortfolioView
    }

    fun findOptionsPositions(underlyingSymbol: String?): List<PortfolioRecord> {
        return records.value?.filter { record ->
            (
                record.instrument.instrumentType.isOption &&
                    record.instrument.underlyingSymbolInfo?.symbol == underlyingSymbol
                )
        }.orEmpty()
    }

    private inner class PagedListCallback : PagedList.Callback() {
        override fun onChanged(position: Int, count: Int) = Unit
        override fun onInserted(position: Int, count: Int) {
            val currentPositions = positionIdList.orEmpty()
            positionsLiveData.fetchData(currentPositions)
            if (currentPositions.isNotEmpty()) {
                positionDao.deleteInvalidPositions(accountUuid, currentPositions)
            }
        }

        override fun onRemoved(position: Int, count: Int) = Unit
    }
}

enum class PositionsState {
    EXPANDED,
    COLLAPSED
}

val PositionsState.isExpanded: Boolean
    get() = this == PositionsState.EXPANDED
