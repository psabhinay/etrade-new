package com.etrade.mobilepro.transfermoney.presentation.adapter.activity

import android.content.res.Resources
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.transfermoney.api.TransferMoneyActivity
import com.etrade.mobilepro.transfermoney.presentation.R
import com.etrade.mobilepro.transfermoney.presentation.common.combineAmountAndOccurrence
import com.etrade.mobilepro.transfermoney.presentation.databinding.TransferItemActivityBinding
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription

internal class TransferActivityViewHolder(
    private val binding: TransferItemActivityBinding,
    private val clickListener: (TransferMoneyActivity) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    private var item: TransferMoneyActivity? = null

    init {
        binding.root.setOnClickListener {
            item?.let { item ->
                clickListener.invoke(item)
            }
        }
    }

    internal fun bind(item: TransferMoneyActivity) {
        this.item = item
        binding.apply {
            status.text = item.status
            amount.text = item.combineAmountAndOccurrence(root.context)
            val from = item.fromAccount
            val to = item.toAccount
            fromToLabel.text = root.resources.getString(R.string.transfer_from_to, from, to)
            fromToLabel.contentDescription = getContentDescription(root.resources, from, to)
            date.text = item.date
        }
    }

    private fun getContentDescription(resources: Resources, from: String, to: String): String {
        val fromFormatted = resources.accountNameContentDescription(from)
        val toFormatted = resources.accountNameContentDescription(to)
        return resources.getString(R.string.transfer_content_description_from_to, fromFormatted, toFormatted)
    }
}
