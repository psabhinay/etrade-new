package com.etrade.mobilepro.streaming.api

interface StockData : Level1Data {
    val beta: String?
    val dayChange: String?
    val dayChangePercent: String?
    val dividend: String?
    val dividendYield: String?
    val marketCap: String?
    val tradeStatus: String?
    val tradeHaltReason: String?
    val volumeTenDayAverage: String?

    companion object {

        /**
         * List of all [StockData] fields.
         */
        @Suppress("LongMethod")
        fun fields(): Set<Level1Field> {
            return setOf(
                Level1Field.ASK,
                Level1Field.ASK_SIZE,
                Level1Field.BETA,
                Level1Field.BID,
                Level1Field.BID_SIZE,
                Level1Field.CHANGE,
                Level1Field.CHANGE_PERCENT,
                Level1Field.CLOSING_MARK,
                Level1Field.DAY_CHANGE,
                Level1Field.DAY_CHANGE_PERCENT,
                Level1Field.DAY_HIGH,
                Level1Field.DAY_LOW,
                Level1Field.DIVIDEND,
                Level1Field.DIVIDEND_YIELD,
                Level1Field.EPS,
                Level1Field.LAST_PRICE,
                Level1Field.LAST_VOLUME,
                Level1Field.MARK,
                Level1Field.MARKET_CAP,
                Level1Field.OPEN_PRICE,
                Level1Field.PREVIOUS_CLOSE_PRICE,
                Level1Field.PRICE_TO_EARNINGS,
                Level1Field.TICK_INDICATOR,
                Level1Field.TIMESTAMP_SINCE_EPOCH,
                Level1Field.TRADE_STATUS,
                Level1Field.TRADE_HALT_REASON,
                Level1Field.VOLUME,
                Level1Field.VOLUME_TEN_DAY_AVERAGE,
                Level1Field.YEAR_HIGH,
                Level1Field.YEAR_HIGH_DATE,
                Level1Field.YEAR_LOW,
                Level1Field.YEAR_LOW_DATE
            )
        }
    }
}
