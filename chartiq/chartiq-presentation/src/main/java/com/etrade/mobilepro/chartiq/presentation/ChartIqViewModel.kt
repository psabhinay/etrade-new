package com.etrade.mobilepro.chartiq.presentation

import android.annotation.SuppressLint
import android.content.res.Resources
import android.webkit.CookieManager
import android.webkit.WebView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobilepro.androidconfig.api.repo.AndroidConfigRepo
import com.etrade.mobilepro.chartiq.api.ChartIqClient
import com.etrade.mobilepro.chartiq.api.model.Account
import com.etrade.mobilepro.chartiq.api.model.Environment
import com.etrade.mobilepro.chartiq.api.model.OrderDetails
import com.etrade.mobilepro.chartiq.api.model.SymbolInfo
import com.etrade.mobilepro.chartiq.api.repo.ChartIqDataRepo
import com.etrade.mobilepro.chartiq.data.AlertDialogAction
import com.etrade.mobilepro.chartiq.data.AlertDialogContent
import com.etrade.mobilepro.chartiq.data.ChartDelegate
import com.etrade.mobilepro.chartiq.data.ChartIqCommandSink
import com.etrade.mobilepro.chartiq.data.ChartIqJavascriptInterface
import com.etrade.mobilepro.chartiq.data.dto.ChartRequestType
import com.etrade.mobilepro.chartiq.presentation.util.getPchartCompatibleInstrumentType
import com.etrade.mobilepro.chartiq.presentation.util.getPriceMessage
import com.etrade.mobilepro.chartiq.presentation.util.hasNoActions
import com.etrade.mobilepro.chartiq.presentation.util.orderCancelNotNeededDialog
import com.etrade.mobilepro.chartiq.presentation.util.orderCancelSubmittedDialog
import com.etrade.mobilepro.chartiq.presentation.util.orderDetailsDialog
import com.etrade.mobilepro.chartiq.presentation.util.simpleDialog
import com.etrade.mobilepro.chartiq.usecase.CancelOrderResult
import com.etrade.mobilepro.chartiq.usecase.CancelOrderUserCase
import com.etrade.mobilepro.chartiq.usecase.EditOrderDataUseCase
import com.etrade.mobilepro.common.di.CurrentEnvironmentName
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.AppDialogBuilder
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.order.details.api.OrderData
import com.etrade.mobilepro.orders.extensions.android.cancelOrderDialog
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.quoteapi.GetQuoteParameter
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuoteUseCase
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.util.AuthenticationStatusProvider
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.debuglog.DebugLog
import com.etrade.mobilepro.util.debuglog.RemoteDebugLogger
import com.etrade.mobilepro.util.executeBlocking
import com.etrade.mobilepro.util.getSessionCookieStringOrEmpty
import com.etrade.mobilepro.util.invoke
import com.etrade.mobilepro.util.safeLet
import com.etrade.mobilepro.viewmodel.BlockingLoadingIndicatorViewModel
import com.squareup.moshi.Moshi
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import okhttp3.CookieJar
import org.slf4j.LoggerFactory
import retrofit2.HttpException

@SuppressWarnings("TooManyFunctions", "LargeClass", "LongParameterList")
class ChartIqViewModel @AssistedInject constructor(
    private val accountListRepo: AccountListRepo,
    private val moshi: Moshi,
    private val defaultAccountRepo: DefaultAccountRepo,
    private val chartIqClientFactory: ChartIqClientFactory,
    private val akamaiConfigRepo: AndroidConfigRepo,
    private val chartIqDataRepo: ChartIqDataRepo,
    private val cookieJar: CookieJar,
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase,
    private val getQuoteUseCase: GetMobileQuoteUseCase,
    private val user: User,
    private val preferences: ApplicationPreferences,
    private val chartsRemoteLogger: RemoteDebugLogger,
    @CurrentEnvironmentName private val environmentName: String,
    @MobileTrade private val mobileBaseUrl: String,
    @MobileTrade private val chartUrl: String,
    private val authStatusProvider: AuthenticationStatusProvider,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController,
    private val resources: Resources,
    private val cancelOrderUseCase: CancelOrderUserCase,
    private val editOrderDataUseCase: EditOrderDataUseCase,
    @Assisted private val symbol: SearchResultItem.Symbol,
    @Assisted private val mobileQuoteStateFlow: StateFlow<MobileQuote>?
) : ViewModel(), ChartDelegate, LifecycleObserver, BlockingLoadingIndicatorViewModel {

    internal enum class ChartSettingsState { OPEN, CLOSE }

    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val compositeDisposable = CompositeDisposable()

    @SuppressLint("StaticFieldLeak")
    var webViewInstance: WebView? = null
        set(value) {
            if (value != field) {
                field?.destroy()
            }

            field = value
        }

    private val chartIqClient: ChartIqClient by lazy {
        // TODO: convert to use SharedFlow when we're using coroutine 1.4 with kotlin 1.4
        chartIqClientFactory.create(object : ChartIqCommandSink {
            override fun sendCommand(command: String) {
                viewModelScope.launch {
                    _chartIqCommands.emit(ConsumableLiveEvent(command))
                }
            }

            override fun sendStreamingUpdateCommand(command: String) {
                _chartIqStreamingUpdateCommands.setValueAllowRepeat(command)
            }
        })
    }

    private fun MutableStateFlow<ConsumableLiveEvent<String>>.setValueAllowRepeat(command: String) {
        val commandEvent = ConsumableLiveEvent(command)
        if (value == commandEvent) {
            value = ConsumableLiveEvent("")
        }
        value = commandEvent
    }

    private val symbolSubscriptionFields: Set<Level1Field> = setOf(
        Level1Field.TIMESTAMP_SINCE_EPOCH,
        Level1Field.LAST_VOLUME,
        Level1Field.LAST_PRICE
    )
    private val dismissDialogAction = { _dialog.value = null }
    private val symbolsToCompare: MutableList<Symbol> = mutableListOf()
    private var awaitingSessionUpdate = false
    private var awaitingSymbolLookUpCompletion = false
    private var configurationInitialized = false

    private val accountsList: List<Account> by lazy {
        accountListRepo.getAccountList().map { Account(it.accountId, it.accountShortName) }
    }

    private var lastViewOrderAccount: Account? = null

    private val _chartIqState = MutableLiveData<ChartIqState>(ChartIqState.None)
    private val _dialog = MutableLiveData<AppDialog?>()
    private val _showSymbolLookup = MutableLiveData<ConsumableLiveEvent<StartSearchParams>>()
    private val _chartIqCommands = MutableSharedFlow<ConsumableLiveEvent<String>>(
        replay = 5,
        extraBufferCapacity = 5
    )
    private val _chartIqStreamingUpdateCommands = MutableStateFlow(ConsumableLiveEvent(""))
    private val _chartSettingsState = MutableLiveData<ChartSettingsState>(ChartSettingsState.CLOSE)
    private val _errorMessage = MutableLiveData<ConsumableLiveEvent<ErrorMessage>>()
    private val _showLogin = MutableLiveData<ConsumableLiveEvent<Unit>>()
    private val _editOrder = MutableLiveData<ConsumableLiveEvent<OrderData>>()
    private val _showLoadingIndicator = MutableLiveData(false)

    internal val chartIqJavascriptBridge by lazy { ChartIqJavascriptInterface(moshi, this) }
    internal val chartIqState: LiveData<ChartIqState>
        get() = _chartIqState
    internal val dialog: LiveData<AppDialog?>
        get() = _dialog

    internal val chartIqCommands: SharedFlow<ConsumableLiveEvent<String>>
        get() = _chartIqCommands.asSharedFlow()
    internal val chartIqStreamingUpdateCommands: StateFlow<ConsumableLiveEvent<String>>
        get() = _chartIqStreamingUpdateCommands
    internal val showSymbolLookup: LiveData<ConsumableLiveEvent<StartSearchParams>>
        get() = _showSymbolLookup
    internal val chartSettingsState: LiveData<ChartSettingsState>
        get() = _chartSettingsState
    internal val errorMessage: LiveData<ConsumableLiveEvent<ErrorMessage>>
        get() = _errorMessage
    internal val showLogin: LiveData<ConsumableLiveEvent<Unit>>
        get() = _showLogin
    internal val editOrder: LiveData<ConsumableLiveEvent<OrderData>>
        get() = _editOrder
    override val blockingLoadingIndicator: LiveData<Boolean>
        get() = _showLoadingIndicator

    @AssistedFactory
    interface Factory {
        fun create(
            symbol: SearchResultItem.Symbol,
            mobileQuoteStateFlow: StateFlow<MobileQuote>?
        ): ChartIqViewModel
    }

    internal fun onJavascriptInterfaceAdded() {
        val cookieString: String = cookieJar.getSessionCookieStringOrEmpty(mobileBaseUrl)
        viewModelScope.launch {
            runCatching {
                akamaiConfigRepo.getAndroidConfiguration()
            }.onSuccess { config ->
                chartIqClient.initialize(config, environmentName)
                _chartIqState.value = ChartIqState.Initializing(cookieString)
            }.onFailure {
                chartsRemoteLogger.log(DebugLog(it, "Additional Info:  Cookie: $cookieString"))
                _chartIqState.value = ChartIqState.Error {
                    onJavascriptInterfaceAdded()
                }
            }
        }
    }

    internal fun addSymbolToCompare(symbolToCompare: SearchResultItem.Symbol) {
        if (symbol.title == symbolToCompare.title) {
            _dialog.value = resources.simpleDialog(R.string.chartiq_comparison_warning_same_symbol, dismissDialogAction)
        } else when (symbolToCompare.type) {
            InstrumentType.EQ, InstrumentType.ETF, InstrumentType.INDX -> {
                chartIqClient.addSymbolToCompare(symbolToCompare)
            }
            else -> {
                _dialog.value = resources.simpleDialog(R.string.chartiq_comparison_warning_not_supported, dismissDialogAction)
            }
        }
    }

    private fun toggleChartSettingsState() {
        if (isSettingsOpen()) {
            _chartSettingsState.postValue(ChartSettingsState.CLOSE)
            chartIqClient.closeChartSettings()
        } else {
            _chartSettingsState.postValue(ChartSettingsState.OPEN)
        }
    }

    private fun configureSymbol() {
        val isStreaming = streamingStatusController.isStreamingToggleEnabled
        val syncData = authStatusProvider.isAuthenticated() && preferences.shouldSyncChartIqData
        if (mobileQuoteStateFlow == null) {
            viewModelScope.launch {
                getQuoteUseCase(GetQuoteParameter(symbol, isExtendedHoursOnUseCase()))
                    .onSuccess { quote ->
                        onQuoteSuccess(quote, syncData, isStreaming)
                    }
                    .onFailure { error ->
                        onQuoteFetchFailed(error)
                    }
            }
        } else {
            viewModelScope.launch {
                mobileQuoteStateFlow.collect {
                    if (it.symbol.isNotBlank() && !configurationInitialized) {
                        onQuoteSuccess(it, syncData, isStreaming)
                    }
                }
            }
        }
    }

    private suspend fun onQuoteSuccess(quote: MobileQuote, syncData: Boolean, isStreaming: Boolean) {
        updateChartConfiguration(quote, syncData, isStreaming)
        awaitingSessionUpdate = false
        if (syncData) {
            preferences.shouldSyncChartIqData = false
        }
        _chartIqState.postValue(ChartIqState.Ready)
        onSymbolConfigured(isStreaming)
    }

    private suspend fun updateChartConfiguration(quote: MobileQuote, syncData: Boolean, isStreaming: Boolean) {
        val lastPrice = quote.quotePrice.lastPrice
        val instrumentType = symbol.getPchartCompatibleInstrumentType()
        chartIqClient.updateChartConfiguration(
            symbolInfo = SymbolInfo(symbol.title, instrumentType, lastPrice, isExtendedHoursOnUseCase(), isStreaming),
            accounts = accountsList,
            defaultAccount = lastViewOrderAccount ?: getDefaultAccount(),
            environment = Environment(environmentName, mobileBaseUrl, chartUrl),
            syncServerData = syncData,
            csrfToken = user.token,
            updateSession = awaitingSessionUpdate
        )
        configurationInitialized = true
    }

    private fun onQuoteFetchFailed(error: Throwable) {
        logger.error("Unable to fetch quote", error)
        _chartIqState.postValue(
            ChartIqState.Error {
                configureSymbol()
            }
        )
    }

    private fun onSymbolConfigured(isStreaming: Boolean) {
        if (isStreaming) {
            compositeDisposable.add(
                streamingQuoteProvider.getQuote(symbol.title, symbol.type, symbolSubscriptionFields).subscribeBy {
                    chartIqClient.addStreamingData(it)
                }
            )
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun updateExtendedHours() {
        if (chartIqState.value == ChartIqState.Ready) {
            when {
                awaitingSymbolLookUpCompletion -> {
                    // mark the completion of symbol lookup flow
                    awaitingSymbolLookUpCompletion = true
                }
                awaitingSessionUpdate -> {
                    awaitingSessionUpdate = false
                    showChart()
                    _chartIqState.value = ChartIqState.Reload
                }
                else -> {
                    val isExtendedHours = isExtendedHoursOnUseCase.executeBlocking()
                    chartIqClient.updateExtendedHoursState(isExtendedHours)
                }
            }
        }
    }

    override fun onWebViewReady() = configureSymbol()

    override fun showAlertDialog(content: AlertDialogContent) {
        _dialog.postValue(
            appDialog {
                title = if (content.title.isNullOrEmpty()) {
                    resources.getString(R.string.chartiq_dialog_default_title)
                } else {
                    content.title ?: ""
                }
                message = content.message
                dismissAction = dismissDialogAction
                if (content.hasNoActions()) {
                    positiveAction {
                        label = resources.getString(R.string.chartiq_dialog_default_action)
                        action = dismissDialogAction
                    }
                } else {
                    addButton(content.alertId, content.positiveButton, chartIqClient, AppDialogBuilder::positiveAction)
                    addButton(content.alertId, content.negativeButton, chartIqClient, AppDialogBuilder::negativeAction)
                    addButton(content.alertId, content.neutralButton, chartIqClient, AppDialogBuilder::neutralAction)
                }
            }
        )
    }

    override fun onSymbolLookup() {
        val params = StartSearchParams(
            query = SearchQuery(SearchType.SYMBOL),
            stockQuoteSearchOnly = true,
            finishOnSearch = true
        )
        _showSymbolLookup.postValue(ConsumableLiveEvent(params))
        awaitingSymbolLookUpCompletion = true
    }

    override fun onSymbolsToCompareUpdated(symbols: List<Symbol>) {
        if (symbols.size == symbolsToCompare.size &&
            symbols.containsAll(symbolsToCompare) &&
            symbolsToCompare.containsAll(symbols)
        ) {
            return
        }

        compositeDisposable.clear()
        symbolsToCompare.clear()

        if (symbols.isNotEmpty()) {
            symbolsToCompare.addAll(symbols)
            if (streamingStatusController.isStreamingToggleEnabled) {
                symbolsToCompare.forEach {
                    compositeDisposable.add(
                        streamingQuoteProvider.getQuote(it.symbol, it.instrumentType, symbolSubscriptionFields).subscribeBy { data ->
                            chartIqClient.addStreamingData(data, it.symbol)
                        }
                    )
                }
                onSymbolConfigured(true)
            }
        }
    }

    override fun showMessage(message: String) = _dialog.postValue(resources.simpleDialog(message, dismissDialogAction))

    override fun showOrderDetails(details: OrderDetails) {
        viewModelScope.launch {
            val orderNumber = resources.getString(R.string.chartiq_order_dialog_order_number_template, details.orderNumber)
            val account = accountsList.firstOrNull { it.id == details.accountId }
            lastViewOrderAccount = account
            account?.let {
                val accountName = it.displayName
                val transactionType = resources.getString(details.transactionType.getLabel())
                val priceType = resources.getString(details.priceType.getLabel())
                val price = details.getPriceMessage(resources)
                val text = "$transactionType $priceType ${details.sharesAmount ?: ""} ${details.symbol} $price\n$orderNumber\n$accountName"
                showOrderDetailsDialog(details, text)
            }
        }
    }

    private fun showOrderDetailsDialog(details: OrderDetails, text: String) {
        _dialog.postValue(
            resources.orderDetailsDialog(
                text = text,
                dismissDialogAction = dismissDialogAction,
                cancelOrderAction = {
                    _dialog.value = resources.cancelOrderDialog(
                        positiveAction = { cancelOrder(details) },
                        negativeAction = dismissDialogAction
                    )
                },
                editOrderAction = { editOrder(details) }
            )
        )
    }

    private fun editOrder(details: OrderDetails) {
        _showLoadingIndicator.value = true
        viewModelScope.launch {
            val orderData = editOrderDataUseCase(details).getOrNull()
            _showLoadingIndicator.value = false
            orderData?.let {
                _editOrder.value = it.consumable()
            }
        }
    }

    private fun cancelOrder(details: OrderDetails) {
        _showLoadingIndicator.value = true
        viewModelScope.launch {
            cancelOrderUseCase(details)
                .onSuccess { result ->
                    _dialog.postValue(
                        when (result) {
                            is CancelOrderResult.CancelNotNeeded -> {
                                resources.orderCancelNotNeededDialog(dismissDialogAction)
                            }
                            is CancelOrderResult.CancelSubmitted -> {
                                _chartIqState.value = ChartIqState.Reload
                                resources.orderCancelSubmittedDialog(result.messages, dismissDialogAction)
                            }
                        }
                    )
                }
                .onFailure {
                    _errorMessage.postValue(
                        ConsumableLiveEvent(
                            ErrorMessage {
                                cancelOrder(details)
                            }
                        )
                    )
                }
            _showLoadingIndicator.value = false
        }
    }

    override fun showChart() {
        if (isSettingsOpen()) {
            toggleChartSettingsState()
        }
    }

    override fun showLogin() {
        _showLogin.postValue(ConsumableLiveEvent(Unit))
        awaitingSessionUpdate = true
    }

    override fun makeServiceCall(requestId: String, query: String?, requestBody: String?, requestType: ChartRequestType) {
        val requestTypeAsString = moshi.adapter(ChartRequestType::class.java).toJson(requestType)
        viewModelScope.launch {
            val result = getResultWithRequest(query, requestBody, requestType)
            result.fold(
                onSuccess = {
                    logger.debug("chart IQ $requestType service loaded with result = $it")
                    chartIqClient.sendLoadedData(requestId, requestTypeAsString, it)
                },
                onFailure = {
                    logger.debug("chart IQ $requestType service loaded failed $it")
                    if (it is HttpException) {
                        val cookieMessage =
                            "$requestType call failed, cookies: ${CookieManager.getInstance().getCookie(chartUrl)}, request body: $requestBody" +
                                ", query string: $query"
                        chartsRemoteLogger.log(DebugLog(it, cookieMessage))
                        chartIqClient.sendLoadedData(requestId, requestTypeAsString, null)
                    }
                }
            )
        }
    }

    @Suppress("ComplexMethod")
    private suspend fun getResultWithRequest(query: String?, requestBody: String?, requestType: ChartRequestType) = when (requestType) {
        ChartRequestType.Studies -> {
            chartIqDataRepo.getStudies()
        }
        ChartRequestType.ChartData -> chartIqDataRepo.getChartData(query)
        ChartRequestType.Drawings -> chartIqDataRepo.getDrawing(query)
        ChartRequestType.SaveView -> chartIqDataRepo.saveView(requestBody)
        ChartRequestType.DeleteView -> chartIqDataRepo.deleteView(requestBody)
        ChartRequestType.SaveDrawings -> chartIqDataRepo.saveDrawing(requestBody)
        ChartRequestType.DeleteDrawings -> chartIqDataRepo.deleteDrawing(requestBody)
        ChartRequestType.Views -> chartIqDataRepo.getAllViews()
        ChartRequestType.OpenOrders -> chartIqDataRepo.getOpenOrders(requestBody)
        ChartRequestType.Unknown -> ETResult.failure(IllegalArgumentException("Unknown command"))
    }

    override fun onChartSettingsClicked() = toggleChartSettingsState()

    internal fun isSettingsOpen() = _chartSettingsState.value == ChartSettingsState.OPEN

    internal fun closeSettings() = showChart()

    internal fun onWebViewDestroyed() {
        _chartIqState.value = ChartIqState.None
    }

    override fun onCleared() {
        compositeDisposable.clear()
        webViewInstance?.destroy()
        webViewInstance = null
        super.onCleared()
    }

    internal fun recordLogs(data: String) {
        chartsRemoteLogger.log(DebugLog(Throwable(data)))
    }

    private suspend fun getDefaultAccount(): Account? {
        val accountId = defaultAccountRepo.getAccountId().getOrNull()
        val accountDisplayName = defaultAccountRepo.getDisplayName().getOrNull()

        return safeLet(accountId, accountDisplayName) { id, displayName ->
            Account(id, displayName)
        }
    }
}

private fun AppDialogBuilder.addButton(
    alertId: String,
    button: AlertDialogAction?,
    chartIqClient: ChartIqClient,
    builder: AppDialogBuilder.(AppDialogBuilder.ActionBuilder.() -> Unit) -> Unit
) {
    button?.run {
        builder {
            label = title
            action = { chartIqClient.forwardAlertCallback(alertId, button.callback) }
        }
    }
}
