package com.etrade.mobilepro.alerts.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.alerts.api.model.AlertAction
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.presentation.adapter.viewholder.AlertViewHolder
import com.etrade.mobilepro.alerts.presentation.databinding.ItemAlertBinding
import com.etrade.mobilepro.common.customview.SwipeRevealLayout
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class AlertAdapterDelegate(
    private val readStatusChecker: ((AlertItem.Alert) -> Boolean)?,
    private val alertListener: ((AlertItem.Alert, AlertAction) -> Unit)?,
    private val swipeRevealListener: ((AlertItem.Alert, SwipeRevealLayout.Action) -> Unit)?,
    private val dragLocker: (() -> Boolean)?
) : AbsListItemAdapterDelegate<AlertItem.Alert, AlertItem, AlertViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): AlertViewHolder {
        return AlertViewHolder(parent.viewBinding(ItemAlertBinding::inflate), alertListener, swipeRevealListener, dragLocker)
    }

    override fun isForViewType(item: AlertItem, items: MutableList<AlertItem>, position: Int): Boolean {
        return item is AlertItem.Alert
    }

    override fun onBindViewHolder(item: AlertItem.Alert, holder: AlertViewHolder, payloads: MutableList<Any>) {
        holder.bindTo(item, readStatusChecker?.invoke(item) ?: false)
    }
}
