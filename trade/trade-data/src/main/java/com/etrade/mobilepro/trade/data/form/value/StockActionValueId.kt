package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.util.MutualConverter

object StockActionValueId : ActionValueId {
    override val buy: String = "buy"
    override val buyToCover: String = "buy to cover"
    override val exchange: String = "exchange"
    override val sell: String = "sell"
    override val sellShort: String = "sell short"
    override val converter: MutualConverter<String, TradeAction> by lazy { createConverter() }
}
