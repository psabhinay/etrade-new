package com.etrade.mobilepro.util.android.recyclerviewutil

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListUpdateCallback
import com.etrade.mobilepro.util.android.accessibility.logger
import org.slf4j.Logger

fun Logger.debugDiffResult(result: DiffUtil.DiffResult, logPrefix: String = "DiffResult") {
    result.dispatchUpdatesTo(object : ListUpdateCallback {
        override fun onInserted(position: Int, count: Int) {
            logger.debug("$logPrefix onInserted position: $position, count: $count")
        }

        override fun onRemoved(position: Int, count: Int) {
            logger.debug("$logPrefix onRemoved position: $position, count: $count")
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            logger.debug("$logPrefix onMoved fromPosition: $fromPosition, toPosition: $toPosition")
        }

        override fun onChanged(position: Int, count: Int, payload: Any?) {
            logger.debug("$logPrefix onChanged position: $position, count: $count, payload: $payload")
        }
    })
}
