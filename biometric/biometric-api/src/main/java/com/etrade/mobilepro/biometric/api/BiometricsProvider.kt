package com.etrade.mobilepro.biometric.api

interface BiometricsProvider {
    fun canAuthenticate(): BiometricProviderResult
}
