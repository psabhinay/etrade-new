package com.etrade.mobilepro.util

import com.etrade.mobilepro.BuildConfig
import junit.framework.TestCase.assertEquals
import org.junit.Test

class VersionCodeTest {
    @Test
    fun `versionCodeInt for BuildConfig`() {
        assertEquals(BuildConfig.VERSION_CODE, BuildConfig.VERSION_NAME.versionCodeInt)
    }

    @Test
    fun `versionCodeInt valid version names`() {
        assertEquals("9.3.0".versionCodeInt, 90300)
        assertEquals("9.4.1".versionCodeInt, 90401)
        assertEquals("9.4.1.MS".versionCodeInt, 90401)
        assertEquals("10.3.2".versionCodeInt, 100302)
        assertEquals("10.3.2.55".versionCodeInt, 100302)
        assertEquals("9".versionCodeInt, 90000)
        assertEquals("9.bar".versionCodeInt, 90000)
        assertEquals("9.bar.55".versionCodeInt, 90055)
    }

    @Test
    fun `versionCodeInt returns 0 for invalid versions`() {
        assertEquals("foo.bar.extra".versionCodeInt, 0)
        assertEquals("".versionCodeInt, 0)
    }
}
