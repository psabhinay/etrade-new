package com.etrade.mobilepro.bloombergtv

import com.etrade.mobilepro.bloombergtv.api.BloombergTvRepository
import com.etrade.mobilepro.bloombergtv.common.BloombergTvPipStatusHelper
import com.etrade.mobilepro.bloombergtv.data.BloombergTvService
import com.etrade.mobilepro.bloombergtv.data.DefaultBloombergTvRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class BloombergtvModule {

    @Provides
    @Singleton
    internal fun provideBloombergTvRepository(bloombergService: BloombergTvService): BloombergTvRepository = DefaultBloombergTvRepository(bloombergService)

    @Singleton
    @Provides
    internal fun provideBloombergTvPipStatusHelper(): BloombergTvPipStatusHelper = BloombergTvPipStatusHelper()
}
