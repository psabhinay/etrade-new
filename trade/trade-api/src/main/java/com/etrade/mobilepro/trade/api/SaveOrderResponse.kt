package com.etrade.mobilepro.trade.api

import com.etrade.mobilepro.backends.api.HasWarnings

interface SaveOrderResponse : HasWarnings {
    val accountId: String
    val orderId: String
}
