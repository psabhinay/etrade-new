package com.etrade.mobilepro.menuitem

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import com.etrade.mobilepro.menuitem.databinding.MenuItemWithArrowAndSubtitleMergeBinding
import com.etrade.mobilepro.util.android.extension.layoutInflater

/**
 * Class implements custom view which has text on the left, description aligning below text, and a icon anchored on the right parent edge.
 * The icon width and height depends on the size of image.
 */
class MenuItemViewWithArrowAndSubtitle : ConstraintLayout {

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private val binding = MenuItemWithArrowAndSubtitleMergeBinding.inflate(layoutInflater, this)

    /**
     * Property to get/set menu title.
     */
    var menuTitleText: CharSequence
        get() = binding.menuTitle.text
        set(value) {
            binding.menuTitle.text = value
        }

    var menuSubTitleText: CharSequence
        get() = binding.menuSubTitle.text
        set(value) {
            binding.menuSubTitle.text = value
        }

    var showLabelPill: Boolean
        get() = binding.menuTitle.showLabelPill
        set(value) {
            binding.menuTitle.showLabelPill = value
        }

    private fun init(attributeSet: AttributeSet?) {
        context.withStyledAttributes(attributeSet, R.styleable.MenuItemViewWithSubTitleAndArrow) {
            binding.menuTitle.text = getString(R.styleable.MenuItemViewWithSubTitleAndArrow_menu_title).orEmpty()
            binding.menuSubTitle.text = getString(R.styleable.MenuItemViewWithSubTitleAndArrow_menu_sub_title)
            binding.menuTitle.titleTextView.setTextColor(
                getColor(
                    R.styleable.MenuItemViewWithSubTitleAndArrow_menu_title_text_color,
                    binding.menuTitle.titleTextView.currentTextColor
                )
            )
            binding.menuSubTitle.setTextColor(
                getColor(
                    R.styleable.MenuItemViewWithSubTitleAndArrow_menu_subtitle_text_color,
                    binding.menuSubTitle.currentTextColor
                )
            )
            binding.menuIconRight.setColorFilter(
                getColor(
                    R.styleable.MenuItemViewWithSubTitleAndArrow_menu_arrow_tint_color,
                    Color.TRANSPARENT
                )
            )
        }
    }
}
