package com.etrade.mobilepro.screener.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.presentation.adapter.viewholder.ScreenerFooterViewHolder
import com.etrade.mobilepro.screener.presentation.databinding.ItemScreenerFooterBinding
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class ScreenerFooterAdaperDelegate : AbsListItemAdapterDelegate<ScreenerItem.Footer, ScreenerItem, ScreenerFooterViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): ScreenerFooterViewHolder =
        ScreenerFooterViewHolder(parent.viewBinding(ItemScreenerFooterBinding::inflate))

    override fun isForViewType(item: ScreenerItem, items: MutableList<ScreenerItem>, position: Int): Boolean =
        item is ScreenerItem.Footer

    override fun onBindViewHolder(item: ScreenerItem.Footer, holder: ScreenerFooterViewHolder, payloads: MutableList<Any>) {
        // nothing to do at the moment
    }
}
