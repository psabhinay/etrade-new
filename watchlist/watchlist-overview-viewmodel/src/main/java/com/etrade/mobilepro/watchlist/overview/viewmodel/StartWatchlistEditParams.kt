package com.etrade.mobilepro.watchlist.overview.viewmodel

/**
 * Start parameters for [EditWatchlistActivity].
 *
 * @param watchlistId id of a watchlist
 * @param isDeletable `true` if list can be deleted, `false` otherwise
 */
data class StartWatchlistEditParams(
    val watchlistId: String,
    val isDeletable: Boolean
)
