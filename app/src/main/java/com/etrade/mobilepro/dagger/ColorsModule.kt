package com.etrade.mobilepro.dagger

import androidx.annotation.ColorRes
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.di.DowJonesColor
import com.etrade.mobilepro.common.di.NasdaqColor
import com.etrade.mobilepro.common.di.NegativeColor
import com.etrade.mobilepro.common.di.NeutralColor
import com.etrade.mobilepro.common.di.PositiveColor
import com.etrade.mobilepro.common.di.PrimaryColor
import com.etrade.mobilepro.common.di.SP500Color
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.util.android.ChangeColorPicker
import com.etrade.mobilepro.util.android.DefaultChangeColorPicker
import com.etrade.mobilepro.util.color.DataColorResolver
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ColorsModule {

    // DO NOT USE APPLICATION CONTEXT TO RESOLVE ColorInt SINCE DARK MODE NEEDS TO USE ACTIVITY/VIEW CONTEXT TO RECREATE COLORS CORRECTLY
    // The Application#applicationContext does not keep information about the theme that you have set via AppCompatDelegate.setDefaultNightMode(),
    // only a View or Activity context has this information stored.

    @Provides
    @Singleton
    @ColorRes
    @PositiveColor
    internal fun providesPositiveColor() = R.color.green

    @Provides
    @Singleton
    @ColorRes
    @NegativeColor
    internal fun providesNegativeColor() = R.color.red

    @Provides
    @Singleton
    @ColorRes
    @NeutralColor
    internal fun providesNeutralColor() = R.color.textColorPrimary

    @Provides
    @Singleton
    @ColorRes
    @DowJonesColor
    internal fun providesDowJonesColor() = R.color.indices_table_dow_jones

    @Provides
    @Singleton
    @ColorRes
    @NasdaqColor
    internal fun providesNasdaqColor() = R.color.indices_table_nasdaq

    @Provides
    @Singleton
    @ColorRes
    @SP500Color
    internal fun providesSP500Color() = R.color.indices_table_sp500

    @Provides
    @Singleton
    @ColorRes
    @PrimaryColor
    internal fun providesPrimaryColor() = R.color.primaryColor

    @Provides
    @Singleton
    internal fun providesChangeColorPicker(
        @PositiveColor positiveColor: Int,
        @NegativeColor negativeColor: Int,
        @NeutralColor neutralColor: Int
    ): ChangeColorPicker {
        return DefaultChangeColorPicker(positiveColor, negativeColor, neutralColor)
    }

    @Provides
    @Singleton
    internal fun providesDataColorResolver(streamingStatusController: StreamingStatusController): DataColorResolver =
        DefaultDataColorResolver(streamingStatusController)
}
