package com.etrade.mobilepro.education.adapter.delegate

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.education.EducationItem
import com.etrade.mobilepro.education.adapter.holder.EducationSectionViewHolder
import com.etrade.mobilepro.education.databinding.AdapterEducationSectionBinding
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate

class EducationSectionAdapterDelegate(
    private val levelTemplate: String,
    private val itemViewClickListener: View.OnClickListener
) : AdapterDelegate<MutableList<EducationItem>>() {

    override fun isForViewType(items: MutableList<EducationItem>, position: Int): Boolean =
        !DelegateHelper.isLastItem(position, items) && (DelegateHelper.isFirstItem(position) || DelegateHelper.isLevelChanged(items, position))

    override fun onBindViewHolder(items: MutableList<EducationItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        val educationHolder: EducationSectionViewHolder? = holder as? EducationSectionViewHolder

        educationHolder?.bind(
            items[position],
            levelTemplate,
            itemViewClickListener
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup): EducationSectionViewHolder {
        return EducationSectionViewHolder(parent.viewBinding(AdapterEducationSectionBinding::inflate))
    }
}
