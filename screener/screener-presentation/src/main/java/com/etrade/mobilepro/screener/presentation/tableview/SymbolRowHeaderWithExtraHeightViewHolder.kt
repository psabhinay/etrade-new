package com.etrade.mobilepro.screener.presentation.tableview

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.etrade.mobilepro.screener.api.result.ResultSymbolCell
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.HasSymbolCell
import com.etrade.mobilepro.util.android.textutil.setSymbolSpan
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

class SymbolRowHeaderWithExtraHeightViewHolder(itemView: View) :
    AbstractViewHolder(itemView), HasSymbolCell {
    private val rowHeaderTextView: TextView = itemView.findViewById(R.id.txtRowHeader)
    private val ntfLabel: TextView = itemView.findViewById(R.id.ntf_label)
    private val cfLabel: TextView = itemView.findViewById(R.id.cf_label)
    private val starLabel: ImageView = itemView.findViewById(R.id.star_label)
    override var cell: SymbolCell? = null
        set(value) {
            field = value

            value?.let {
                rowHeaderTextView.setSymbolSpan(
                    it.displayText,
                    it.instrumentType,
                    it.description ?: it.displayText
                )
                if (it is ResultSymbolCell) {
                    ntfLabel.visibility = if (it.hasNTF) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                    cfLabel.visibility = if (it.hasCF) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                    starLabel.visibility = if (it.hasStar) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                }
            }
        }
}
