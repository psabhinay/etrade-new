package com.etrade.mobile.accounts.defaultaccount

private const val DEFAULT_ACCOUNT_STATE = true

interface WithAccountOptions {
    val accountUuid: String

    val isEnabled: Boolean
}

fun List<WithAccountOptions>.getStateFor(accountUuid: String?): Boolean {
    return firstOrNull {
        it.accountUuid == accountUuid
    }?.isEnabled ?: DEFAULT_ACCOUNT_STATE
}

fun List<WithAccountOptions>.getIndexOf(accountUuid: String?): Int {
    mapIndexed { index, withAccountOptions ->
        if (withAccountOptions.accountUuid == accountUuid) {
            return index
        }
    }
    return 0
}
