package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.trade.form.api.value.InvestmentTypeValueId

object MutualFundsInvestmentTypeValueId : InvestmentTypeValueId {
    override val reInvestInFund: String = "2"
    override val depositInFund: String = "3"
}
