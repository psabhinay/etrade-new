package com.etrade.mobilepro.screener.api.filter.stock

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class PriceVolumeFilter(
    val sharePriceGreaterThan: String = "",
    val sharePriceLessThan: String = "",

    val pricePerformanceSpLessThanMinus40: Boolean = false,
    val pricePerformanceSpMinus40toMinus20: Boolean = false,
    val pricePerformanceSpMinus20to0: Boolean = false,
    val pricePerformanceSpZeroTo20: Boolean = false,
    val pricePerformanceSp20to40: Boolean = false,
    val pricePerformanceSpGreaterThan40: Boolean = false,
    val pricePerformanceSpOver4Weeks: Boolean = false,
    val pricePerformanceSpOver13Weeks: Boolean = false,
    val pricePerformanceSpOver52Weeks: Boolean = false,

    val pricePerformanceIndustryLessThanMinus40: Boolean = false,
    val pricePerformanceIndustryMinus40toMinus20: Boolean = false,
    val pricePerformanceIndustryMinus20to0: Boolean = false,
    val pricePerformanceIndustryZeroTo20: Boolean = false,
    val pricePerformanceIndustry20to40: Boolean = false,
    val pricePerformanceIndustryGreaterThan40: Boolean = false,
    val pricePerformanceIndustryOver4Weeks: Boolean = false,
    val pricePerformanceIndustryOver13Weeks: Boolean = false,
    val pricePerformanceIndustryOver52Weeks: Boolean = false
) : ScreenerFilter {
    fun isPricePerformanceSpOverSelected() = pricePerformanceSpOver4Weeks ||
        pricePerformanceSpOver13Weeks ||
        pricePerformanceSpOver52Weeks

    fun isPricePerformanceSpValuesSelected() = pricePerformanceSpLessThanMinus40 ||
        pricePerformanceSpMinus40toMinus20 ||
        pricePerformanceSpMinus20to0 ||
        pricePerformanceSpZeroTo20 ||
        pricePerformanceSp20to40 ||
        pricePerformanceSpGreaterThan40

    fun isPricePerformanceIndustryOverSelected() = pricePerformanceIndustryOver4Weeks ||
        pricePerformanceIndustryOver13Weeks ||
        pricePerformanceIndustryOver52Weeks

    fun isPricePerformanceIndustryValuesSelected() = pricePerformanceIndustryLessThanMinus40 ||
        pricePerformanceIndustryMinus40toMinus20 ||
        pricePerformanceIndustryMinus20to0 ||
        pricePerformanceIndustryZeroTo20 ||
        pricePerformanceIndustry20to40 ||
        pricePerformanceIndustryGreaterThan40

    override fun isEmpty(): Boolean = sharePriceGreaterThan.isEmpty() && sharePriceLessThan.isEmpty() &&
        !isPricePerformanceSpValuesSelected() && !isPricePerformanceSpOverSelected() &&
        !isPricePerformanceIndustryValuesSelected() && !isPricePerformanceIndustryOverSelected()

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.STOCK
}
