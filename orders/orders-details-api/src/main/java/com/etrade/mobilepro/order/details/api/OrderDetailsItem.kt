package com.etrade.mobilepro.order.details.api

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.OptionType
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.ContingentCondition
import com.etrade.mobilepro.orders.api.ContingentQualifier
import com.etrade.mobilepro.orders.api.ExecInstruction
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.orders.api.MarketSession
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.option.TradeLeg
import java.math.BigDecimal

sealed class BaseOrderDetailsItem {

    data class AdvancedOrderItem(
        val qualifier: ContingentQualifier,
        val condition: ContingentCondition,
        val advancedValue: String,
        val symbol: String,
        val displaySymbol: String,
        val orderType: AdvancedOrderType,
        val offsetType: String,
        val offsetValue: BigDecimal?,
        val price: BigDecimal?
    ) : BaseOrderDetailsItem()

    data class OrderDetailsItem(
        val orderStatus: OrderStatus,
        val orderType: OrderType,
        val priceType: PriceType,
        val quantity: String,
        val orderNumber: String,
        val orderPlacedDate: String?,
        val executedDate: String?,
        val term: OrderTerm,
        val priceExecuted: BigDecimal?,
        val commission: BigDecimal?,
        val estimatedCommission: BigDecimal?,
        val estimatedTotalCost: BigDecimal?,
        val cancelledDate: String?,
        val cancelledRequestedDate: String?,
        val account: String,
        val orderDescriptions: List<OrderDetailDescription>,
        val limitPrice: BigDecimal?,
        val expiredData: String?,
        val orderAction: TransactionType,
        val netBid: BigDecimal,
        val netAsk: BigDecimal,
        val stopPrice: BigDecimal?,
        val strategyType: StrategyType?,
        val systemRejectedData: String?,
        val orderSavedDate: String?,
        val hasMultipleLegs: Boolean = orderDescriptions.size > 1,
        val preparedOrderId: String?,
        val hasPartialExecution: Boolean,
        val goodTillDateFlag: Boolean,
        val gtdExpirationData: String?,
        val marketSession: MarketSession,
        val marketDestination: MarketDestination,
        val origSysCode: OrigSysCode,
        val execInstruction: ExecInstruction,
        val limitOffsetValue: BigDecimal?,
        val advancedItem: AdvancedOrderItem?,
        val hasDependentFlag: Boolean,
        val dependentOrderNumber: Int,
        val hiddenFlag: Boolean,
        val orderSpecialAttribCd: String,
        val isAdvancedOrder: Boolean = advancedItem != null,
        val tradeLegs: List<TradeLeg>
    ) : BaseOrderDetailsItem()

    object PreviewOrderDetailsItem : BaseOrderDetailsItem()

    data class StockPlanOrderDetailsItem(
        val orderHistoryList: List<OrderHistoryItem>,
        val orderNumber: String,
        val orderType: String,
        val orderStatus: OrderStatus,
        val description: String,
        val term: String,
        val gtdExpiration: String,
        val sharesExercised: String,
        val sharesSold: String,
        val sharesToExercise: String,
        val sharesToSell: String,
        val estSharesToSell: String,
        val priceType: String,
        val limitPrice: String,
        val grossProceeds: String,
        val estGrossProceeds: String,
        val totalPrice: String,
        val commission: String,
        val secFee: String,
        val brokerAssistFee: String,
        val disbursementFee: String,
        val taxesWithheld: String,
        val estNetProceeds: String,
        val netProceeds: String,
        val exerciseValue: String,
        val sharesToKeep: String,
        val estSharesToKeep: String,
        val sharesIssued: String,
        val sharesToIssue: String,
        val appreciationSharesValue: String,
        val estAppreciationSharesValue: String,
        val taxSharesValue: String,
        val estTaxSharesValue: String,
        val planPrice: String,
        val grantType: String,
        val planNumber: String,
        val planDate: String,
        val vestPeriod: String,
        val costBasis: String,
        val planType: String,
        val symbol: String
    ) : BaseOrderDetailsItem()
}

data class OrderDetailDescription(
    val displayQuantity: String,
    val fillQuantity: String,
    val quantity: String,
    val transactionType: TransactionType,
    val displaySymbol: String,
    val originalSymbol: String,
    val underlyingProductId: String = "",
    val optionType: OptionType = OptionType.UNKNOWN,
    val typeCode: InstrumentType = InstrumentType.UNKNOWN,
    val optExpireDate: String = "",
    val optExpireMonth: String = "",
    val optExpireYear: String = "",
    val optStrikePrice: String? = null,
    val isPartialOrder: Boolean = false,
    val executedDate: String? = null,
    val priceExecuted: BigDecimal? = null,
    val commission: BigDecimal? = null,
    val qtyReserveShow: BigDecimal? = null,
    val isAdvancedOrder: Boolean = false
)

data class OrderHistoryItem(
    val price: String,
    val transaction: String,
    val sellQuantity: String,
    val date: String
)
