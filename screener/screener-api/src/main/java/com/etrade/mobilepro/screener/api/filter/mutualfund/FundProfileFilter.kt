package com.etrade.mobilepro.screener.api.filter.mutualfund

import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.FundFamily
import com.etrade.mobilepro.screener.api.MutualFundGroup
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class FundProfileFilter(
    val allStarsFund: Boolean = false,
    val excludeClosedFunds: Boolean = false,
    val fundFamilies: Set<FundFamily>,
    val fundGroup: MutualFundGroup,
    val fundCategories: Set<FundCategory>,
    val morningStarRating: Set<Int> = emptySet(),

    val index: Boolean = false,
    val fundOfFunds: Boolean = false,
    val societyResponsible: Boolean = false,
    val lifeOfCycleFund: Boolean = false,
    val dividendStrategy: Boolean = false,
    val enhancedIndex: Boolean = false,
    val diversified: Boolean = false,
    val activelyManaged: Boolean = false
) : ScreenerFilter {
    fun isFundCategoryEmpty() = fundCategories.isEmpty() || (fundCategories.size == 1 && fundCategories.first().isAllItemsElement)
    fun isFundFamiliesEmpty() = fundFamilies.isEmpty() || (fundFamilies.size == 1 && fundFamilies.first().isAllItemsElement)

    override fun isEmpty(): Boolean = !allStarsFund && !excludeClosedFunds && isFundFamiliesEmpty() && fundGroup.isAllItemsElement &&
        isFundCategoryEmpty() && morningStarRating.isEmpty() && !index && !fundOfFunds && !societyResponsible && !lifeOfCycleFund &&
        !dividendStrategy && !enhancedIndex && !diversified && !activelyManaged

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND
}
