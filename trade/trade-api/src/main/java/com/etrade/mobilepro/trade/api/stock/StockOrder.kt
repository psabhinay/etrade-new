package com.etrade.mobilepro.trade.api.stock

import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.WithCostOrder
import com.etrade.mobilepro.trade.api.WithHardToBorrowOrder
import com.etrade.mobilepro.trade.api.WithOtcTierOrder
import com.etrade.mobilepro.trade.api.WithPriceOrder

interface StockOrder : Order, WithPriceOrder, WithCostOrder, WithHardToBorrowOrder, WithOtcTierOrder
