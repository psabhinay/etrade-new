package com.etrade.mobilepro.quoteapi

interface FundQuoteDetailItemsMapper {
    fun getMutualFundQuoteDetail(quote: MobileQuote): FundQuoteItem.MutualFundDetailItem
    fun getMoneyMarketFundQuoteDetail(quote: MobileQuote): FundQuoteItem.MoneyMarketFundDetail
    fun getMutualFundClassSection(quote: MobileQuote?): FundQuoteItem.FundClassSection
    fun getFundIconSection(quote: MobileQuote): FundQuoteItem.FundIconSection
    fun getMorningStarRatings(mutualFundDetails: MutualFundDetails?): FundQuoteItem.MorningStarRatings
    fun getMorningStarDetails(mutualFundDetails: MutualFundDetails?): FundQuoteItem.MorningStarDetails
    fun getQuarterlyReturns(mutualFundDetails: MutualFundDetails?): FundQuoteItem.QuarterlyTrailingReturnsSection
    fun getSummarySection(mutualFundDetails: MutualFundDetails?): FundQuoteItem.SummarySection

    fun getFundCategoryPerformanceItem(quote: MobileQuote): FundQuoteItem.FundPerformanceItem

    fun getQuoteFundFooterItem(quote: MobileQuote): FundQuoteItem.QuoteFundsFooterItem
    fun getFeesAndExpensesItem(mutualFundDetails: MutualFundDetails?): FundQuoteItem.FeesAndExpensesItem
}
