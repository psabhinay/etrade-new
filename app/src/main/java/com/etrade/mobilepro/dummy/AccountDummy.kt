package com.etrade.mobilepro.dummy

import com.etrade.mobile.accounts.dto.AccountDto
import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobile.accounts.dto.InstitutionType
import com.etrade.mobile.accounts.dto.ManagedAccountType
import com.etrade.mobilepro.accountslist.data.dto.AccountReferenceDto
import java.util.Random
import java.util.UUID

private val random = Random()

@Suppress("LongMethod")
fun createDummyAccountReferenceDto(accountId: String = random.nextInt().toString()): AccountReferenceDto {
    return AccountReferenceDto(
        listOf(
            AccountDto(
                accountUuid = UUID.randomUUID().toString(),
                accountDescription = getRandomString(),
                accountId = accountId,
                accountLongName = getRandomString(),
                accountMode = getRandomString(),
                accountShortName = getRandomString(),
                accountType = AccountType.Managed,
                cashAvailableForWithdrawal = "$1",
                institutionType = InstitutionType.ADP,
                isIra = false,
                encAccountId = getRandomString(),
                restrictionLevel = "restricted?",
                marginAccountFlag = false,
                marginAvailableForWithdrawal = "--",
                purchasingPower = "--",
                totalAvailableForWithdrawal = "--",
                funded = true,
                ledgerAccountValue = "$0.00",
                accountValue = "--",
                accountIndex = "0",
                daysGain = "--",
                optionLevel = null,
                streamingRestrictions = null,
                promptsForFunding = "",
                managedAccountType = ManagedAccountType.Unknown
            )
        )
    )
}
