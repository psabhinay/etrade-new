package com.etrade.mobilepro.searchingapi.items

import com.etrade.mobilepro.instrument.ExpirationDate
import com.etrade.mobilepro.instrument.formatExpirationDate

data class SymbolLookUpResult(
    var searchResultList: List<SearchResultItem.Symbol>,
    var optionMonths: List<OptionDate>,
    var errorInfo: ErrorInfo?
)

class OptionDate(val originSelectedDate: ExpirationDate, private val selectedDate: String) {
    override fun toString(): String = originSelectedDate.settlementSessionCode.formatExpirationDate(selectedDate)
}
