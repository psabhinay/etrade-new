package com.etrade.mobilepro.appconfig.data.repo.datasource

import com.etrade.mobilepro.appconfig.api.AppEnvironment
import com.etrade.mobilepro.appconfig.api.BaseUrls
import com.etrade.mobilepro.appconfig.api.repo.datasource.BaseUrlsDataSource
import com.etrade.mobilepro.appconfig.data.repo.datasource.combiner.BaseUrlSetsCombiner
import com.etrade.mobilepro.preferences.ApplicationPreferences
import javax.inject.Inject

class DefaultBaseUrlsDataSource @Inject constructor(
    private val appPreferences: ApplicationPreferences,
    private val baseUrlSetsCombiner: BaseUrlSetsCombiner
) : BaseUrlsDataSource {
    override fun getBaseUrls(environment: AppEnvironment): BaseUrls = baseUrlSetsCombiner.combine(getSavedBaseUrls(), getDefaultBaseUrls(environment))

    override fun saveBaseUrls(baseUrls: BaseUrls, environment: AppEnvironment) {
        saveReadyBaseUrls(BaseUrls())
        saveReadyBaseUrls(baseUrlSetsCombiner.combine(baseUrls, getDefaultBaseUrls(environment)))
    }

    private fun saveReadyBaseUrls(baseUrls: BaseUrls) = with(baseUrls) {
        appPreferences.webUrl = web
        appPreferences.eDocWebUrl = eDocWeb
        appPreferences.expressUrl = express
        appPreferences.mobileTradeUrl = mobileTrade
        appPreferences.mobileETradeUrl = mobileETrade
        appPreferences.marketOnDemandUrl = marketOnDemand
        appPreferences.apiUrl = api
        appPreferences.level1StreamerUrl = level1StreamerUrl
        appPreferences.level2StreamerUrl = level2StreamerUrl
    }

    private fun getSavedBaseUrls(): BaseUrls = BaseUrls(
        web = appPreferences.webUrl,
        eDocWeb = appPreferences.eDocWebUrl,
        express = appPreferences.expressUrl,
        mobileTrade = appPreferences.mobileTradeUrl,
        mobileETrade = appPreferences.mobileETradeUrl,
        marketOnDemand = appPreferences.marketOnDemandUrl,
        api = appPreferences.apiUrl,
        level1StreamerUrl = appPreferences.level1StreamerUrl,
        level2StreamerUrl = appPreferences.level2StreamerUrl,
        environmentChange = ""
    )

    private fun getDefaultBaseUrls(environment: AppEnvironment): BaseUrls = when (environment) {
        AppEnvironment.DR -> drBaseUrls
        AppEnvironment.DR_EXTERNAL -> drExternalBaseUrls
        AppEnvironment.M5 -> m5BaseUrls
        AppEnvironment.MOCK -> mockBaseUrls
        AppEnvironment.PLT -> pltBaseUrls
        AppEnvironment.PROD -> prodBaseUrls
        AppEnvironment.PROD_M3_ONLY -> m3_onlyExternalBaseUrls
        AppEnvironment.PROD_M5_ONLY -> m5_onlyExternalBaseUrls
        AppEnvironment.SIT -> sitBaseUrls
        AppEnvironment.UAT -> uatBaseUrl
    }
}
