package com.etrade.mobilepro.markets.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.BaseApiTest
import com.etrade.mobilepro.markets.data.response.MarketFutureResponse
import com.etrade.mobilepro.testutil.XmlDeserializer
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

internal class MarketsFutureApiTest : BaseApiTest() {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun `markets future api success call test`() {
        val marketIndexResponse: MarketFutureResponse = XmlDeserializer.getObjectFromXml("markets_future.xml", MarketFutureResponse::class.java)

        assertEquals(3, marketIndexResponse.quotes?.size)
        assertEquals("YM", marketIndexResponse.quotes?.get(0)?.symbol)
    }
}
