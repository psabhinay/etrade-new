package com.etrade.mobilepro.screener.data.json.mutualfund

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_EQUALS
import com.etrade.mobilepro.screener.api.OPERATOR_LIKE
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.FundProfileFilter
import javax.inject.Inject

private const val MORNINGSTAR_FIELD = "MorningstarRatingSinceIncep"

private const val FAMILIES_FIELD = "FundFamilyName"
private const val CATEGORIES_FIELD = "FundCategory"
private const val GROUP_FIELD = "SuperCategory"

private const val STARS_ENABLED_FIELD = "AllStarListFlag"
private const val INDEX_FIELD = "IndexFundFlag"
private const val FUND_OF_FUNDS_FIELD = "FundofFunds"
private const val SOCIETY_RESPONSIBLE_FIELD = "SociallyResponsibleFlag"
private const val LIFE_OF_CYCLE_FIELD = "LifeCycleFundFlag"
private const val DIVIDENT_STRATEGY_FIELD = "DividendStrategyFlag"
private const val ENHANCED_INDEX_FIELD = "EnhancedIndexFund"
private const val DIVERSIFIED_FIELD = "DiversifiedFund"
private const val ACTIVELY_MANAGED_FIELD = "ActivelyManagedFlag"
private const val EXCLUDE_CLOSED_FIELD = "OpenToNewInvestment"

class FundProfileFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    @Suppress("LongMethod", "ComplexMethod") // not complex method, there is no nesting conditions
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val fundProfileFilter = screenerFilter as FundProfileFilter
        val arguments = mutableListOf<Argument>()

        if (!fundProfileFilter.isFundFamiliesEmpty()) {
            val values = if (fundProfileFilter.fundFamilies.find { it.isAllItemsElement } != null) {
                listOf("%")
            } else {
                fundProfileFilter.fundFamilies.map { it.id }
            }

            arguments.add(Argument(listOf(getLikeClause(values)), FAMILIES_FIELD))
        }

        if (!fundProfileFilter.isFundCategoryEmpty()) {
            val values = if (fundProfileFilter.fundCategories.find { it.isAllItemsElement } != null) {
                listOf("%")
            } else {
                fundProfileFilter.fundCategories.map { it.id }
            }

            arguments.add(Argument(listOf(getLikeClause(values)), CATEGORIES_FIELD))
        }

        if (!fundProfileFilter.fundGroup.isAllItemsElement) {
            arguments.add(Argument(listOf(getLikeClause(listOf(fundProfileFilter.fundGroup.id))), GROUP_FIELD))
        }

        if (fundProfileFilter.morningStarRating.isNotEmpty()) {
            arguments.add(Argument(getStarClauses(fundProfileFilter), MORNINGSTAR_FIELD))
        }

        createFundCharacterArguments(arguments, fundProfileFilter)

        return PartialScreenerParams(arguments)
    }

    @Suppress("LongMethod", "ComplexMethod") // not complex method, there is no nesting conditions
    private fun createFundCharacterArguments(arguments: MutableList<Argument>, fundProfileFilter: FundProfileFilter) {
        if (fundProfileFilter.allStarsFund) {
            arguments.add(Argument(listOf(equalsTrueClause), STARS_ENABLED_FIELD))
        }
        if (fundProfileFilter.index) {
            arguments.add(Argument(listOf(equalsTrueClause), INDEX_FIELD))
        }
        if (fundProfileFilter.fundOfFunds) {
            arguments.add(Argument(listOf(equalsTrueClause), FUND_OF_FUNDS_FIELD))
        }
        if (fundProfileFilter.societyResponsible) {
            arguments.add(Argument(listOf(equalsTrueClause), SOCIETY_RESPONSIBLE_FIELD))
        }
        if (fundProfileFilter.lifeOfCycleFund) {
            arguments.add(Argument(listOf(equalsTrueClause), LIFE_OF_CYCLE_FIELD))
        }
        if (fundProfileFilter.dividendStrategy) {
            arguments.add(Argument(listOf(equalsTrueClause), DIVIDENT_STRATEGY_FIELD))
        }
        if (fundProfileFilter.enhancedIndex) {
            arguments.add(Argument(listOf(equalsTrueClause), ENHANCED_INDEX_FIELD))
        }
        if (fundProfileFilter.diversified) {
            arguments.add(Argument(listOf(equalsFalseClause), DIVERSIFIED_FIELD))
        }
        if (fundProfileFilter.activelyManaged) {
            arguments.add(Argument(listOf(equalsTrueClause), ACTIVELY_MANAGED_FIELD))
        }
        if (fundProfileFilter.excludeClosedFunds) {
            arguments.add(Argument(listOf(equalsTrueClause), EXCLUDE_CLOSED_FIELD))
        }
    }

    private fun getStarClauses(fundProfileFilter: FundProfileFilter): List<Clause> {
        return fundProfileFilter.morningStarRating.map { getStarClause(it) }
    }

    private val equalsTrueClause = Clause(OPERATOR_EQUALS, listOf("1"))
    private val equalsFalseClause = Clause(OPERATOR_EQUALS, listOf("0"))
    private fun getLikeClause(values: List<String>) = Clause(OPERATOR_LIKE, values)
    private fun getStarClause(value: Int) = Clause(OPERATOR_LIKE, listOf(value.toString()))
}
