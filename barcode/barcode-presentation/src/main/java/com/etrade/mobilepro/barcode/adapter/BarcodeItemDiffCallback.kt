package com.etrade.mobilepro.barcode.adapter

import androidx.recyclerview.widget.DiffUtil
import com.etrade.mobilepro.barcode.api.BarcodeItem

class BarcodeItemDiffCallback(
    private val oldItems: List<BarcodeItem>,
    private val newItems: List<BarcodeItem>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val old: BarcodeItem = oldItems[oldItemPosition]
        val new: BarcodeItem = newItems[newItemPosition]

        return old.id == new.id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size
}
