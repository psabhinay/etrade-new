package com.etrade.mobilepro.common.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.etrade.mobilepro.common.navigation.NavRouterSignal
import com.etrade.mobilepro.common.navigation.NavRouterSignalViewModel
import com.etrade.mobilepro.common.navigation.navigate
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

/**
 * This field holds pending [NavDirections] to redirect a user to a specific location after an action is performed.
 *
 * We need to be sure that the direction is there throughout the lifecycle of the whole app, and not a specific component. Because in some cases, say during
 * logging in which is done in a separate activity, the parent activity which is started the process may have been destroyed when we returned to it.
 */
var pendingNavDirections: PendingNavDirection? = null
/**
 * Sometimes before we take the users to the pending location we will need to take them back to the page that they were at prior to the login,
 * such as company news for set alert
 */
var navDirectionsPriorToPending: NavDirections? = null

class EtNavigationViewModel : ViewModel(), NavRouterSignalViewModel {

    override val navRouterSignal: LiveData<ConsumableLiveEvent<NavRouterSignal>>
        get() = _navRouterSignal

    private val _navRouterSignal: MutableLiveData<ConsumableLiveEvent<NavRouterSignal>> = MutableLiveData()

    fun navigate(directions: NavDirections) {
        _navRouterSignal.navigate(directions)
    }
}

data class PendingNavDirection(
    val sourceNavControllerId: Int,
    val navDirections: NavDirections
)
