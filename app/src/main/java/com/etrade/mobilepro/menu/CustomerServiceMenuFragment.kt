package com.etrade.mobilepro.menu

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.menu.viewmodel.CustomerServiceMenuPageViewModel
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class CustomerServiceMenuFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    userViewModel: UserViewModel,
    snackbarUtilFactory: SnackbarUtilFactory
) : AppBaseMenuFragment(viewModelFactory, userViewModel) {

    private val snackBarUtil: SnackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    override val menuPageViewModel by viewModels<CustomerServiceMenuPageViewModel> { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        menuPageViewModel.showCallUsError.observe(
            viewLifecycleOwner,
            Observer {
                it.consume {
                    snackBarUtil.errorSnackbar()
                }
            }
        )
    }

    override fun onResume() {
        super.onResume()
        popNav()
    }
}
