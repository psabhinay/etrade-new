package com.etrade.mobilepro.trade.dropdown

import android.app.Dialog
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.setToolbarIcon
import com.etrade.mobilepro.dropdown.setUpBottomSheetDialog
import com.etrade.mobilepro.trade.dropdown.databinding.FragmentTradeSelectBottomSheetBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

private val FRAG_TAG = "${TradeSelectBottomSheetFragment::class.java}.bottomNavFragment"

class TradeSelectBottomSheetFragment : BottomSheetDialogFragment(), HasTradeSelectorHolder {

    private val binding by viewBinding(FragmentTradeSelectBottomSheetBinding::bind)

    private val hostNavFragment: NavHostFragment?
        get() = childFragmentManager.findFragmentByTag(FRAG_TAG) as? NavHostFragment

    private val isLastPageInStack: Boolean
        get() = hostNavFragment?.childFragmentManager?.backStackEntryCount == 0

    private val toolbarView: Toolbar?
        get() = binding.viewToolbar.findViewById(R.id.toolBar)

    override var holder: TradeSelectorHolder? = null

    override fun getTheme() = R.style.BottomSheetDialogTheme

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_trade_select_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHostFragment(hostNavFragment ?: createNavHost())

        childFragmentManager.registerFragmentLifecycleCallbacks(
            object : FragmentManager.FragmentLifecycleCallbacks() {
                override fun onFragmentViewCreated(fm: FragmentManager, f: Fragment, v: View, savedInstanceState: Bundle?) {
                    super.onFragmentViewCreated(fm, f, v, savedInstanceState)

                    if (f.tag == FRAG_TAG) {
                        hostNavFragment?.childFragmentManager?.addOnBackStackChangedListener {
                            setupToolbar()
                        }
                    }
                }
            },
            false
        )

        dialog?.window?.attributes?.windowAnimations = R.style.DialogAnimation
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            setUpBottomSheetDialog()
            setOnBackKeyListener()
        }
    }

    private fun setHostFragment(host: NavHostFragment) {
        childFragmentManager.beginTransaction()
            .replace(R.id.bottom_sheet_nav_host, host, FRAG_TAG)
            .commit()
    }

    private fun createNavHost() = NavHostFragment.create(
        R.navigation.trade_select_nav_graph,
        arguments
    )

    private fun setupToolbar() {
        toolbarView?.apply {
            setToolbarIcon(isLastPageInStack, isWhite = true)
            setNavigationOnClickListener { onBackPressed(isLastPageInStack) }
        }
    }

    private fun Dialog.setOnBackKeyListener() {
        setOnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action != KeyEvent.ACTION_DOWN) {
                onBackPressed(isLastPageInStack)
                true
            } else {
                false
            }
        }
    }

    private fun onBackPressed(isLastPageInStack: Boolean) {
        if (isLastPageInStack) {
            dismiss()
        } else {
            hostNavFragment?.findNavController()?.popBackStack()
        }
    }
}
