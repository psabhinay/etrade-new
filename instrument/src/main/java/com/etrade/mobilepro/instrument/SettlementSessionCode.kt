package com.etrade.mobilepro.instrument

enum class SettlementSessionCode(val code: Int) {
    AM(2),
    PM(1),
    UNKNOWN(0);

    companion object {
        fun from(code: Int?): SettlementSessionCode {
            return when (code) {
                1 -> PM
                2 -> AM
                else -> UNKNOWN
            }
        }
    }
}

fun SettlementSessionCode.formatExpirationDate(expirationDate: String): String {
    return if (this == SettlementSessionCode.AM) {
        "$expirationDate (AM)"
    } else {
        expirationDate
    }
}
