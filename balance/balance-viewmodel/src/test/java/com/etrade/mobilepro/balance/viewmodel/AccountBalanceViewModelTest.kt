package com.etrade.mobilepro.balance.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobilepro.balance.api.AccountBalance
import com.etrade.mobilepro.balance.api.GetAccountBalanceParameter
import com.etrade.mobilepro.balance.api.GetAccountBalanceUseCase
import com.etrade.mobilepro.common.result.ETResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.AfterClass
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertNull
import org.junit.BeforeClass
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import java.math.BigDecimal
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class AccountBalanceViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val mockDefaultAccountRepo = object : DefaultAccountRepo {
        override suspend fun getAccountId(): ETResult<String?> = ETResult.success("")
        override suspend fun setAccountId(accountId: String?) = Unit
        override suspend fun getDisplayName(): ETResult<String?> = ETResult.success("")
    }

    @Test
    fun `check job cancels when account switches`() {
        val useCase = TestGetAccountBalanceUseCase()
        val viewModel = AccountBalanceViewModel(mockDefaultAccountRepo, useCase)

        viewModel.fetch("account1")
        val account1Continuation = useCase.continuation!!

        viewModel.fetch("account2")
        val account2Continuation = useCase.continuation!!

        val expectedPatternDayTrader = true

        account1Continuation.resume(!expectedPatternDayTrader)
        account2Continuation.resume(expectedPatternDayTrader)

        assertEquals(expectedPatternDayTrader, viewModel.accountBalance.value?.isPatternDayTrader)
    }

    @Test
    fun `check joins if refresh is called when other refresh is still executing`() {
        val useCase = TestGetAccountBalanceUseCase()
        val viewModel = AccountBalanceViewModel(mockDefaultAccountRepo, useCase)

        val expectedPatternDayTrader = true

        viewModel.fetch("account")
        useCase.continuation?.resume(!expectedPatternDayTrader)

        viewModel.refresh()
        val expectedContinuation = useCase.continuation!!

        viewModel.refresh()
        assertEquals(expectedContinuation, useCase.continuation)

        expectedContinuation.resume(expectedPatternDayTrader)

        assertEquals(expectedPatternDayTrader, viewModel.accountBalance.value?.isPatternDayTrader)
    }

    @Test
    fun `check refresh`() {
        val useCase = TestGetAccountBalanceUseCase()
        val viewModel = AccountBalanceViewModel(mockDefaultAccountRepo, useCase)

        val expectedPatternDayTrader = true

        viewModel.fetch("account")
        useCase.continuation?.resume(!expectedPatternDayTrader)

        viewModel.refresh()
        useCase.continuation?.resume(expectedPatternDayTrader)
        assertEquals(expectedPatternDayTrader, viewModel.accountBalance.value?.isPatternDayTrader)

        viewModel.refresh()
        useCase.continuation?.resume(!expectedPatternDayTrader)
        assertNotEquals(expectedPatternDayTrader, viewModel.accountBalance.value?.isPatternDayTrader)
    }

    @Test
    fun `sets account balance to empty account balance when account id is empty`() {
        val useCase = TestGetAccountBalanceUseCase()
        val viewModel = AccountBalanceViewModel(mockDefaultAccountRepo, useCase)

        viewModel.fetch("")

        viewModel.refresh()
        assertEquals(false, viewModel.accountBalance.value?.isPatternDayTrader)
        assertNull(viewModel.accountBalance.value?.accountValue)
        assertNull(viewModel.accountBalance.value?.buyingPower)
        assertNull(viewModel.accountBalance.value?.cashAvailableForInvestment)
        assertNull(viewModel.accountBalance.value?.totalAvailableForWithdrawal)
        assertNull(viewModel.accountBalance.value?.availableExcessEquity)
        assertNull(viewModel.accountBalance.value?.intradayMargin)
        assertNull(viewModel.accountBalance.value?.intradayNonMargin)
    }

    @Test
    fun `sets account balance value when get account balance service is successful`() {
        val useCase = TestGetAccountBalanceUseCase()
        val viewModel = AccountBalanceViewModel(mockDefaultAccountRepo, useCase)

        viewModel.fetch("account")

        viewModel.refresh()
        useCase.continuation?.resume(isPatternDayTrader = true)

        assertEquals(true, viewModel.accountBalance.value?.isPatternDayTrader)
        assertEquals(BigDecimal("250000.00"), viewModel.accountBalance.value?.accountValue)
        assertEquals(BigDecimal("900000.00"), viewModel.accountBalance.value?.buyingPower)
        assertEquals(BigDecimal("50000.00"), viewModel.accountBalance.value?.cashAvailableForInvestment)
        assertEquals(BigDecimal("50000.00"), viewModel.accountBalance.value?.totalAvailableForWithdrawal)
        assertEquals(BigDecimal("900000.00"), viewModel.accountBalance.value?.availableExcessEquity)
        assertEquals(BigDecimal("1000000.00"), viewModel.accountBalance.value?.intradayMargin)
        assertEquals(BigDecimal("15000.00"), viewModel.accountBalance.value?.intradayNonMargin)
    }

    @Test
    fun `sets account balance failed when get account balance service fails`() {
        val useCase = TestGetAccountBalanceUseCase()
        val viewModel = AccountBalanceViewModel(mockDefaultAccountRepo, useCase)

        viewModel.fetch("account")

        viewModel.refresh()
        useCase.continuation?.resumeFailure()

        assertNull(viewModel.accountBalance.value)
        assertEquals(true, viewModel.accountBalanceFailed.value)
    }

    private fun Continuation<ETResult<AccountBalance>>.resume(isPatternDayTrader: Boolean) {
        resume(ETResult.success(TestAccountBalance(isPatternDayTrader)))
    }

    private fun Continuation<ETResult<AccountBalance>>.resumeFailure() {
        resume(ETResult.failure(RuntimeException("Something went wrong!")))
    }

    private class TestAccountBalance(override val isPatternDayTrader: Boolean) : AccountBalance {
        override val accountValue: BigDecimal = BigDecimal("250000.00")
        override val buyingPower: BigDecimal = BigDecimal("900000.00")
        override val cashAvailableForInvestment: BigDecimal = BigDecimal("50000.00")
        override val totalAvailableForWithdrawal: BigDecimal = BigDecimal("50000.00")
        override val availableExcessEquity: BigDecimal = BigDecimal("900000.00")
        override val intradayMargin: BigDecimal = BigDecimal("1000000.00")
        override val intradayNonMargin: BigDecimal = BigDecimal("15000.00")
    }

    private class TestGetAccountBalanceUseCase : GetAccountBalanceUseCase {

        var continuation: Continuation<ETResult<AccountBalance>>? = null

        override suspend fun execute(parameter: GetAccountBalanceParameter): ETResult<AccountBalance> {
            return suspendCoroutine {
                continuation = it
            }
        }
    }

    @ExperimentalCoroutinesApi
    companion object {

        private val testCoroutineDispatcher = TestCoroutineDispatcher()

        @BeforeClass
        @JvmStatic
        fun prepare() {
            Dispatchers.setMain(testCoroutineDispatcher)
        }

        @AfterClass
        @JvmStatic
        fun tearDown() {
            Dispatchers.resetMain()
            testCoroutineDispatcher.cleanupTestCoroutines()
        }
    }
}
