package com.etrade.mobilepro.screener.api

private const val STOCK_NAME = "Stock"
private const val MF_NAME = "Mf"
private const val ETF_NAME = "Etf"

sealed class ScreenerType {
    abstract val belongsTo: Group
    enum class Group(
        val contents: List<ScreenerType>,
        val constantName: String
    ) {
        STOCK(
            contents = listOf(
                Stock.MarketSegment,
                Stock.PriceAndVolume,
                Stock.Opinions,
                Stock.Fundamentals,
                Stock.EarningsAndDividends,
                Stock.Technicals
            ),
            constantName = STOCK_NAME
        ),
        MUTUAL_FUND(
            contents = listOf(
                MutualFund.FundProfile,
                MutualFund.Performance,
                MutualFund.Expenses,
                MutualFund.Fees,
                MutualFund.Risks,
                MutualFund.Portfolio
            ),
            constantName = MF_NAME
        ),
        ETF(
            contents = listOf(
                Etf.FundProfile,
                Etf.Performance,
                Etf.Risk,
                Etf.Portfolio,
                Etf.Technicals
            ),
            constantName = ETF_NAME
        );
        val contentsAsSet by lazy { contents.toSet() }
    }

    sealed class Stock : ScreenerType() {
        object MarketSegment : Stock()
        object PriceAndVolume : Stock()
        object Opinions : Stock()
        object Fundamentals : Stock()
        object EarningsAndDividends : Stock()
        object Technicals : Stock()
        override val belongsTo: Group
            get() = Group.STOCK
    }

    sealed class MutualFund : ScreenerType() {
        object FundProfile : MutualFund()
        object Performance : MutualFund()
        object Expenses : MutualFund()
        object Fees : MutualFund()
        object Risks : MutualFund()
        object Portfolio : MutualFund()
        override val belongsTo: Group
            get() = Group.MUTUAL_FUND
    }

    sealed class Etf : ScreenerType() {
        object FundProfile : Etf()
        object Performance : Etf()
        object Risk : Etf()
        object Portfolio : Etf()
        object Technicals : Etf()
        override val belongsTo: Group
            get() = Group.ETF
    }
}
