package com.etrade.mobilepro.transfermoney.api

data class TransferMoneyPreview(
    val fundHoldInfoMessage: String? = null,
    val scheduledDate: String? = null,
    val amount: String? = null,
    val errorCode: String? = null,
    val errorDescription: String? = null,
    val headerMessage: String? = null,
    val footerMessage: String? = null
)
