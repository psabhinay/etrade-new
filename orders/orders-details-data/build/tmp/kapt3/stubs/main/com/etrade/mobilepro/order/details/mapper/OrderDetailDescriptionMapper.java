package com.etrade.mobilepro.order.details.mapper;

import com.etrade.mobilepro.order.details.api.OrderDetailDescription;
import com.etrade.mobilepro.orders.api.TransactionType;
import com.etrade.mobilepro.trade.api.option.TradeLeg;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rJ\u0014\u0010\u000e\u001a\u00020\u0004*\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0002R\u0018\u0010\u0003\u001a\u00020\u0004*\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u000f"}, d2 = {"Lcom/etrade/mobilepro/order/details/mapper/OrderDetailDescriptionMapper;", "", "()V", "safeIntValue", "", "", "getSafeIntValue", "(Ljava/lang/String;)I", "map", "Lcom/etrade/mobilepro/trade/api/option/TradeLeg;", "description", "Lcom/etrade/mobilepro/order/details/api/OrderDetailDescription;", "hasPartialExecution", "", "resolveQuantity", "orders-details-data"})
public final class OrderDetailDescriptionMapper {
    
    public OrderDetailDescriptionMapper() {
        super();
    }
    
    private final int getSafeIntValue(java.lang.String $this$safeIntValue) {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.trade.api.option.TradeLeg map(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.order.details.api.OrderDetailDescription description, boolean hasPartialExecution) {
        return null;
    }
    
    private final int resolveQuantity(com.etrade.mobilepro.order.details.api.OrderDetailDescription $this$resolveQuantity, boolean hasPartialExecution) {
        return 0;
    }
}