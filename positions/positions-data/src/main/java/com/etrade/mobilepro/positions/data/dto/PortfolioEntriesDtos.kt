package com.etrade.mobilepro.positions.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false, name = "GetPortfolioEntriesResponse")
data class PortfolioEntryDto(
    @field:Element(name = "AccountId")
    var accountId: String? = null,
    @field:Element(name = "TotalCash")
    var totalCash: String? = null,
    @field:ElementList(name = "Position", inline = true, empty = true, required = false)
    var positions: List<PortfolioPositionDto>? = null
)

@Root(strict = false, name = "Position")
data class PortfolioPositionDto(
    @field:Element(name = "PositionId")
    var positionId: String? = null,
    @field:Element(name = "Symbol")
    var symbol: String? = null,
    @field:Element(name = "Display_Symbol")
    var displaySymbol: String? = null,
    @field:Element(name = "MarketValue")
    var marketValue: String? = null,
    @field:Element(name = "DaysGainValue")
    var daysGainValue: String? = null,
    @field:Element(name = "TotalGainValue")
    var totalGainValue: String? = null,
    @field:Element(name = "PricePaid")
    var pricePaid: String? = null,
    @field:Element(name = "Commission")
    var commission: String? = null,
    @field:Element(name = "Fees")
    var fees: String? = null,
    @field:Element(name = "Quantity")
    var quantity: String? = null,
    @field:Element(name = "DisplayQuantity")
    var displayQuantity: String? = null,
    @field:Element(name = "TypeCode")
    var typeCode: String? = null,
    @field:Element(name = "Bid")
    var bid: String? = null,
    @field:Element(name = "Ask")
    var ask: String? = null,
    @field:Element(name = "DaysGainPercentage")
    var daysGainPercentage: String? = null,
    @field:Element(name = "PE")
    var priceEarningRation: String? = null,
    @field:Element(name = "TotalGainPercentage")
    var totalGainPercentage: String? = null,
    @field:Element(name = "EPS")
    var earningsPerShare: String? = null,
    @field:Element(name = "Week52High")
    var fiftyTwoWeekHigh: String? = null,
    @field:Element(name = "Week52Low")
    var fiftyTwoWeekLow: String? = null,
    @field:Element(name = "LastPrice")
    var lastPrice: String? = null,
    @field:Element(name = "MarketCap")
    var marketCap: String? = null,
    @field:Element(name = "TodaysClose")
    var todayClose: String? = null,
    @field:Element(name = "Volume")
    var volume: String? = null,
    @field:Element(name = "DaysChangeVal")
    var daysChange: String? = null,
    @field:Element(name = "DaysChangePer")
    var daysChangePercentage: String? = null,
    @field:Element(name = "MarkToMarket")
    var markToMarket: String? = null,
    @field:Element(name = "Delta", required = false)
    var delta: String? = null,
    @field:Element(name = "Gamma", required = false)
    var gamma: String? = null,
    @field:Element(name = "Theta", required = false)
    var theta: String? = null,
    @field:Element(name = "Vega", required = false)
    var vega: String? = null,
    @field:Element(name = "ImpliedVolatilityPct", required = false)
    var impliedVolatilityPercentage: String? = null,
    @field:Element(name = "BaseSymbolPrice", required = false)
    var baseSymbolPrice: String? = null,
    @field:Element(name = "DaysExpiration", required = false)
    var daysExpiration: String? = null,
    @field:Element(name = "UnderlyingSymbol", required = false)
    var underlyingSymbol: String? = null,
    @field:Element(name = "UnderlyingTypeCode", required = false)
    var underlyingTypeCode: String? = null,
    @field:Element(name = "InTheMoneyFlag", required = false)
    var inTheMoneyFlag: String? = null
)
