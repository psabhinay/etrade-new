package com.etrade.mobilepro.editlist

import android.content.DialogInterface
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import com.etrade.mobilepro.baseactivity.ActivityNetworkConnectionDelegate
import com.etrade.mobilepro.baseactivity.ActivitySessionDelegate
import com.etrade.mobilepro.common.getToolbarTitleContentDescription
import com.etrade.mobilepro.common.setupToolbarWithUpButton
import com.etrade.mobilepro.editlist.databinding.ActivityEditListBinding
import com.etrade.mobilepro.editlist.intent.emptyListMessageResId
import com.etrade.mobilepro.editlist.intent.isDeletableItems
import com.etrade.mobilepro.editlist.intent.isToggleableItems
import com.etrade.mobilepro.editlist.intent.listActionConfirmationIsRequired
import com.etrade.mobilepro.editlist.intent.listActionConfirmationMessageResId
import com.etrade.mobilepro.editlist.intent.listActionConfirmationTitleResId
import com.etrade.mobilepro.editlist.intent.listActionIsEnabled
import com.etrade.mobilepro.editlist.intent.listActionIsVisible
import com.etrade.mobilepro.editlist.intent.listActionTextResId
import com.etrade.mobilepro.editlist.intent.listFooterMessage
import com.etrade.mobilepro.progressoverlay.ProgressDialogFragment
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.addDividerItemDecoration
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.button.MaterialButton
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

private const val KEY_IS_CONFIRMATION_DIALOG_SHOWING = "isConfirmationDialogShowing"

/**
 * Lets a user to edit a list of some items.
 *
 * Supported operations:
 *
 * * items sorting (drag & drop)
 * * an item removal
 * * a custom action for a whole list
 *
 * This is an abstract class that is meant to be used for any arbitrary data. To take advantage of it you must extend it and [EditListViewModel] for a specific
 * data source.
 */
abstract class EditListActivity<T : EditListItem> : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var activitySessionDelegate: ActivitySessionDelegate

    @Inject
    lateinit var activityNetworkConenectionDelegate: ActivityNetworkConnectionDelegate

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var snackbarUtilFactory: SnackbarUtilFactory

    protected val binding by viewBinding(ActivityEditListBinding::inflate)
    private val itemAdapter: ItemAdapter
        get() = binding.widgetList.adapter as ItemAdapter
    /**
     * A class that extends [EditListViewModel] that substitutes [EditListActivity] descendant's view model.
     */
    protected abstract val viewModelClass: Class<out EditListViewModel<T>>

    protected val viewModel: EditListViewModel<T> by lazy {
        ViewModelProvider(this, viewModelFactory).get(viewModelClass)
    }

    protected val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ this }, { binding.widgetList }) }

    private var isConfirmationDialogShowing = false

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(binding.root)

        setupToolbarWithUpButton(titleResId = R.string.edit, upButtonResId = R.drawable.ic_close)

        binding.toolbarApply.setOnClickListener { onApplyClicked() }
        binding.toolbarTitle.text = title
        binding.toolbarTitle.contentDescription = getToolbarTitleContentDescription(title, this)

        val callback = DragNDropTouchHelperCallback(viewModel)
        val itemTouchHelper = ItemTouchHelper(callback)
        binding.widgetList.adapter = ItemAdapter(
            viewModel,
            itemTouchHelper,
            intent.isDeletableItems,
            intent.isToggleableItems
        ) {
            callback.isDragFinished
        }

        binding.widgetList.apply {
            addDividerItemDecoration(R.drawable.thin_divider)
            itemTouchHelper.attachToRecyclerView(this)
            itemAnimator?.changeDuration = 0
        }

        intent.emptyListMessageResId
            .takeIf { it != 0 }
            ?.also { binding.noItemsMessage.setText(it) }

        binding.listAction.setupListAction()
        onCreate(viewModel)

        viewModel.listItems.observe(this) {
            itemAdapter.items = it
            if (it.isEmpty()) {
                binding.noItemsMessage.visibility = View.VISIBLE
                binding.widgetList.visibility = View.GONE
            } else {
                binding.noItemsMessage.visibility = View.GONE
                binding.listItems.visibility = View.VISIBLE
                showFooterMessage(intent.listFooterMessage)
                binding.widgetList.visibility = View.VISIBLE
            }
        }

        viewModel.viewState.observe(
            this,
            Observer {
                onEditState(it)
            }
        )

        if (savedInstanceState == null) {
            viewModel.fetchItems()
        }

        activityNetworkConenectionDelegate.initNetworkConnectivityBanner(this)

        activitySessionDelegate.observeTimeoutEvent(this)
    }

    private fun MaterialButton.setupListAction() {
        visibility = if (intent.listActionIsVisible) {
            // setup the text first
            intent.listActionTextResId
                .takeIf { it != 0 }
                ?.also { setText(it) }

            if (intent.listActionIsEnabled) {
                if (intent.listActionConfirmationIsRequired) {
                    setOnClickListener {
                        showConfirmationDialog()
                    }
                } else {
                    setOnClickListener {
                        viewModel.performListAction()
                    }
                }
            } else {
                isEnabled = false
            }
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    protected open fun onApplyClicked() {
        viewModel.applyChanges()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(KEY_IS_CONFIRMATION_DIALOG_SHOWING, isConfirmationDialogShowing)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState.getBoolean(KEY_IS_CONFIRMATION_DIALOG_SHOWING)) {
            showConfirmationDialog()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            viewModel.cancelChanges()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        activitySessionDelegate.onUserInteraction(this)
    }

    /**
     * Lets a descendant to initialize [viewModel] before any other calls or subscriptions. Called from [onCreate].
     *
     * @param viewModel view model for [EditListActivity]
     */
    protected open fun onCreate(viewModel: EditListViewModel<T>) {
        // does nothing by default
    }

    private fun onEditState(state: EditListViewModel.ViewState) {
        when (state) {
            EditListViewModel.ViewState.Idle -> {
                ProgressDialogFragment.dismiss(supportFragmentManager)
                updateListActionVisibility(View.VISIBLE)
            }
            EditListViewModel.ViewState.Canceled -> finish()
            EditListViewModel.ViewState.InProgress -> {
                ProgressDialogFragment.show(supportFragmentManager).onCancelListener = { finish() }
                updateListActionVisibility(View.GONE)
            }
            is EditListViewModel.ViewState.Success -> {
                if (state.intent != null) {
                    setResult(state.resultCode, intent)
                } else {
                    setResult(state.resultCode)
                }
                finish()
            }
            EditListViewModel.ViewState.Error -> {
                ProgressDialogFragment.dismiss(supportFragmentManager)
                snackbarUtil.snackbar(getString(R.string.error_message_general), Snackbar.LENGTH_LONG)?.show()
                updateListActionVisibility(View.GONE)
            }
        }
    }

    private fun showConfirmationDialog() {
        val listener = DialogInterface.OnClickListener { dialog: DialogInterface, which: Int ->
            if (which == DialogInterface.BUTTON_POSITIVE) {
                viewModel.performListAction()
            }
            dialog.dismiss()
        }

        MaterialAlertDialogBuilder(this)
            .setTitle(intent.listActionConfirmationTitleResId)
            .setMessage(intent.listActionConfirmationMessageResId)
            .setPositiveButton(R.string.yes, listener)
            .setNegativeButton(R.string.no, listener)
            .setOnDismissListener { isConfirmationDialogShowing = false }
            .show()

        isConfirmationDialogShowing = true
    }

    private fun updateListActionVisibility(visibility: Int) {
        if (intent.listActionIsVisible) {
            binding.listAction.visibility = visibility
        }
    }

    private fun showFooterMessage(footerMessageResId: Int) {
        if (footerMessageResId != 0) {
            binding.footerMessage.message.text = getString(footerMessageResId)
            binding.footerMessage.root.visibility = View.VISIBLE
        }
    }
}
