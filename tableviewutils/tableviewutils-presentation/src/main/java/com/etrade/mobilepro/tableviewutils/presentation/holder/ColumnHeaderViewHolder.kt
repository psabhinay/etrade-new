package com.etrade.mobilepro.tableviewutils.presentation.holder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.etrade.mobilepro.tableviewutils.presentation.R
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.rotateSortArrow
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractSorterViewHolder
import com.evrencoskun.tableview.sort.SortState

class ColumnHeaderViewHolder(itemView: View) : AbstractSorterViewHolder(itemView) {

    private val columnHeaderContainer = itemView.findViewById<View>(R.id.columnHeaderContainer)
    private val columnHeaderSortButton: ImageView = itemView.findViewById<View>(R.id.columnHeaderSortButton) as ImageView
    val columnHeaderTextview: TextView = itemView.findViewById<View>(R.id.columnHeaderTextview) as TextView

    /**
     * This method is calling from onBindColumnHeaderHolder on TableViewAdapter
     */
    fun setColumnHeader(columnHeader: Cell) {
        columnHeaderTextview.text = columnHeader.displayText
        columnHeaderTextview.contentDescription = columnHeader.displayTextContentDescription
        columnHeaderContainer.contentDescription = "${columnHeader.displayTextContentDescription} ${columnHeaderSortButton.contentDescription}"
    }

    private var previousState: SortState? = null

    override fun onSortingStatusChanged(sortState: SortState) {
        super.onSortingStatusChanged(sortState)
        if (sortState != previousState) {
            columnHeaderSortButton.rotateSortArrow(sortState)
            previousState = sortState
            columnHeaderTextview.requestLayout()
            columnHeaderSortButton.requestLayout()
            columnHeaderContainer.requestLayout()
            itemView.requestLayout()
            setContainerContentDescription(sortState)
        }
    }

    private fun setContainerContentDescription(sortState: SortState) {
        columnHeaderContainer.apply {
            contentDescription = "${columnHeaderTextview.contentDescription} ${
            if (sortState != SortState.UNSORTED) {
                "${resources.getString(R.string.sorted)} ${sortState.name}"
            } else {
                SortState.UNSORTED.name
            }
            }"
        }
    }
}
