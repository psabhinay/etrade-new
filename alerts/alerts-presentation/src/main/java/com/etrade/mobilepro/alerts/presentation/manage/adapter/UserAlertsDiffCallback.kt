package com.etrade.mobilepro.alerts.presentation.manage.adapter

import androidx.recyclerview.widget.DiffUtil
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.alerts.api.model.UserAlertItem.Companion.isSameDeleteInfo

class UserAlertsDiffCallback(
    private val oldItems: List<UserAlertItem>,
    private val newItems: List<UserAlertItem>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }

    // item can have revealed and hidden delete button states, but it is still the same item
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]
        return oldItem.javaClass == newItem.javaClass && isSameDeleteInfo(oldItem, newItem)
    }
}
