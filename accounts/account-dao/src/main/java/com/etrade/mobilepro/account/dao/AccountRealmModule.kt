package com.etrade.mobilepro.account.dao

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class AccountRealmModule
