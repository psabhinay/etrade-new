package com.etrade.mobilepro.order.details.dto;

import com.etrade.mobilepro.backends.neo.BaseDataDto;
import com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto;
import com.etrade.mobilepro.orders.data.dto.AuditInfoDto;
import com.etrade.mobilepro.orders.data.dto.HeaderDto;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0013\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001BA\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0001\u0010\b\u001a\u00020\u0003\u0012\b\b\u0001\u0010\t\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003JE\u0010\u0019\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00032\b\b\u0003\u0010\u0006\u001a\u00020\u00072\b\b\u0003\u0010\b\u001a\u00020\u00032\b\b\u0003\u0010\t\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001J\t\u0010\u001f\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\fR\u0011\u0010\b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\fR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\f\u00a8\u0006 "}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/EventHeaderDto;", "", "eventType", "", "marketSession", "price", "eventTimeEST", "", "groupOrderNo", "eventDesc", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V", "getEventDesc", "()Ljava/lang/String;", "getEventTimeEST", "()J", "getEventType", "getGroupOrderNo", "getMarketSession", "getPrice", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "orders-details-data"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class EventHeaderDto {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String eventType = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String marketSession = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String price = null;
    private final long eventTimeEST = 0L;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String groupOrderNo = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String eventDesc = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.EventHeaderDto copy(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "eventType")
    java.lang.String eventType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "marketSession")
    java.lang.String marketSession, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "price")
    java.lang.String price, @com.squareup.moshi.Json(name = "eventTimeEST")
    long eventTimeEST, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "groupOrderNo")
    java.lang.String groupOrderNo, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "eventDesc")
    java.lang.String eventDesc) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public EventHeaderDto(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "eventType")
    java.lang.String eventType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "marketSession")
    java.lang.String marketSession, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "price")
    java.lang.String price, @com.squareup.moshi.Json(name = "eventTimeEST")
    long eventTimeEST, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "groupOrderNo")
    java.lang.String groupOrderNo, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "eventDesc")
    java.lang.String eventDesc) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getEventType() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMarketSession() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPrice() {
        return null;
    }
    
    public final long component4() {
        return 0L;
    }
    
    public final long getEventTimeEST() {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getGroupOrderNo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getEventDesc() {
        return null;
    }
}