package com.etrade.mobilepro.alerts.data.mapper

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.api.model.AlertType
import com.etrade.mobilepro.alerts.data.dto.InboxEventDto
import com.etrade.mobilepro.util.json.coerceToBool
import java.util.concurrent.TimeUnit

private val knownAlertTypes: Map<String, AlertType> by lazy {
    mapOf(
        "account" to AlertType.ACCOUNT,
        "adhoc" to AlertType.ADHOC,
        "bank" to AlertType.BANK,
        "dividend" to AlertType.DIVIDEND,
        "stock" to AlertType.MARKET
    )
}

private const val DATE_SOURCE_FORMAT = "%s | %s"

internal class AlertDtoMapper {
    fun map(dtoNullable: InboxEventDto?): AlertItem.Alert? {
        val jsonDto = dtoNullable ?: return null
        val type = jsonDto.alertCode
        if (type.isNullOrEmpty() || type == "0") {
            return null
        }
        val occurredAtFormatted = jsonDto.occurredAtEpochTimestamp?.let {
            val formattedDate = DateFormattingUtils.formatToShortDateWithDeviceTimeZone(TimeUnit.SECONDS.toMillis(it))
            val formattedTime = DateFormattingUtils.formatTimeWithoutSecondsWithShowingTimezone(TimeUnit.SECONDS.toMillis(it))
            String.format(DATE_SOURCE_FORMAT, formattedDate, formattedTime)
        } ?: ""
        return AlertItem.Alert(
            id = jsonDto.id ?: "",
            subject = (jsonDto.subject ?: "").trim(),
            occurredAt = jsonDto.occurredAtEpochTimestamp ?: 0L,
            occurredAtFormatted = occurredAtFormatted,
            code = type.toInt(),
            status = AlertItem.Status(
                wasRead = coerceToBool(jsonDto.wasRead)
            )
        )
    }

    private fun parseAlertType(type: String?): AlertType {
        val input = type ?: return AlertType.UNKNOWN
        knownAlertTypes.entries.forEach { entry ->
            if (input.contains(entry.key, ignoreCase = true)) {
                return entry.value
            }
        }
        return AlertType.UNKNOWN
    }
}
