package com.etrade.mobilepro.news.presentation.dynamic

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.dynamicui.api.Diffable
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.briefing.api.BriefingNewsRepo
import com.etrade.mobilepro.news.presentation.BR
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.util.Resource
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

private const val BALANCE_CACHE_EXPIRATION_EXPIRATION = 15000L

class NewsWidgetBriefingViewModel(
    private val newsRepo: BriefingNewsRepo,
    private val statusRepo: ReadStatusRepo,
    private val router: NewsRouter,
    override val layoutId: Int = R.layout.widget_briefing_news
) : DynamicViewModel(layoutId), Diffable {

    override val variableId: Int = BR.obj

    private val compositeDisposable = CompositeDisposable()
    private val _viewState = MutableLiveData<DynamicUiViewState>()
    private val _items = MutableLiveData<List<NewsItemWidgetPayload>>()

    val newsItems: LiveData<List<NewsItemWidgetPayload>>
        get() = _items
    val viewState: LiveData<DynamicUiViewState>
        get() = _viewState

    fun fetchNews() {
        _viewState.value = DynamicUiViewState.Loading
        compositeDisposable.add(
            newsRepo
                .getBriefingNews(BALANCE_CACHE_EXPIRATION_EXPIRATION)
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                    onNext = {
                        val state = handleOnNextAndGetViewState(it)
                        _viewState.postValue(state)
                    },
                    onError = {
                        val state = handleErrorAndGetViewState(it)
                        _viewState.postValue(state)
                    }
                )
        )
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    // Assumes that there will be only one NewsWidgetBriefingModel in a list
    override fun isSameItem(other: Any): Boolean = other.javaClass == javaClass

    override fun hasSameContents(other: Any): Boolean = false

    private fun getItemSelectListener(): (NavController?, NewsItem.Text, Int) -> Unit {
        return { navController, item, _ ->
            _items.value?.let {
                _items.value = it.markAsRead(item)
            }
            router.navigateToTextNewsDetails(navController, item)
        }
    }

    private fun handleOnNextAndGetViewState(resource: Resource<List<NewsItem>>): DynamicUiViewState {
        return when (resource) {
            is Resource.Loading -> {
                mapAndPostNewsItems(resource.data)
                DynamicUiViewState.Loading
            }
            is Resource.Success -> {
                mapAndPostNewsItems(resource.data)
                DynamicUiViewState.Success
            }
            is Resource.Failed -> {
                handleErrorAndGetViewState(resource.error)
            }
        }
    }

    private fun mapAndPostNewsItems(items: List<NewsItem>?) {
        items?.let { data ->
            _items.postValue(mapBriefingNewsItems(data))
        }
    }

    private fun mapBriefingNewsItems(items: List<NewsItem>) = items.map { item ->
        val wasRead = if (item is NewsItem.Text) {
            statusRepo.wasRead(item.docId)
        } else {
            false
        }
        NewsItemWidgetPayload(item, wasRead, getItemSelectListener(), null)
    }

    private fun handleErrorAndGetViewState(error: Throwable?): DynamicUiViewState.Error {
        _items.postValue(
            listOf(
                NewsItemWidgetPayload(item = NewsItem.Empty, wasRead = false, itemSelectListener = null, symbolSelectListener = null)
            )
        )
        return DynamicUiViewState.Error(error?.message)
    }
}
