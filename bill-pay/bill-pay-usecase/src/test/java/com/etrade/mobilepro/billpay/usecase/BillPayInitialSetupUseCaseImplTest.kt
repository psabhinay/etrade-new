package com.etrade.mobilepro.billpay.usecase

import com.etrade.mobile.accounts.billpay.BillPay
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.BillPayStatus
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.common.result.ETResult
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyString
import org.threeten.bp.Instant
import java.math.BigDecimal
import java.math.BigInteger

internal class BillPayInitialSetupUseCaseImplTest {

    private val mockRepo: BillPayRepo = mock {
        onBlocking { getBillPaySessionId() } doReturn ETResult.success("12345")
        onBlocking { getPayeesList("123456") } doReturn ETResult.success(
            listOf(
                Payee("1", "test1", "", "1234", "0", -1, false),
                Payee("2", "test2", "", "1235", "0", -1, false)
            )
        )
        onBlocking { getAccounts() } doReturn ETResult.success(
            getMockAccountDetails(
                BillPay(
                    isRestricted = false,
                    isEnrolled = true,
                    isEligible = true,
                    isPrimary = true
                )
            )
        )
    }

    private fun getMockAccountDetails(billPay: BillPay): List<AccountDetails> {
        return listOf(
            object : AccountDetails {
                override val accountIdKey: String = ""
                override val maskedAccountId: String = ""
                override val accountMode: String = ""
                override val accountName: String = ""
                override val accountShortName: String = ""
                override val accountLongName: String = ""
                override val mobileDescription: String = ""
                override val accountValue: BigDecimal? = BigDecimal.TEN
                override val institutionType: String = ""
                override val accountStatus: String = ""
                override val restrictionLevel: String = ""
                override val closedDate: Long = Instant.now().epochSecond
                override val brokerage: AccountDetails.Brokerage? = null
                override val billPay: BillPay? = billPay
                override val accountId: String = ""
                override val nickname: String? = ""
                override val instNo: BigInteger = BigInteger.TEN
                override val accountType: String = ""
                override val accountCategory: String = ""
            })
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `BillPay Setup should be successful when sessionId payeeList, and accounts is valid`() {
        runBlockingTest {

            whenever(mockRepo.getBillPaySessionId()).thenReturn(ETResult.success("123456"))

            val sut = BillPayInitialSetupUseCaseImpl(mockRepo, PayeeListUseCaseImpl(mockRepo), FilterBillPayAccountsUseCaseImp(), GenerateBillPayStatusUseCaseImp())
            val result = sut.execute(Unit)

            assertTrue(result.isSuccess)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should return failure result when sessionId is invalid`() {
        runBlockingTest {
            whenever(mockRepo.getBillPaySessionId()).thenReturn(ETResult.failure(Throwable("bad thing")))

            val sut = BillPayInitialSetupUseCaseImpl(mockRepo, PayeeListUseCaseImpl(mockRepo), FilterBillPayAccountsUseCaseImp(), GenerateBillPayStatusUseCaseImp())
            val result = sut.execute(Unit)

            assertEquals("bad thing", result.exceptionOrNull()?.message)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should return failure result when getAccounts throws exception`() {
        runBlockingTest {
            whenever(mockRepo.getAccounts()).thenReturn(ETResult.failure(Throwable("bad thing")))

            val sut = BillPayInitialSetupUseCaseImpl(mockRepo, PayeeListUseCaseImpl(mockRepo), FilterBillPayAccountsUseCaseImp(), GenerateBillPayStatusUseCaseImp())
            val result = sut.execute(Unit)

            assertEquals("bad thing", result.exceptionOrNull()?.message)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should return failure result when payeeList throws exception`() {
        runBlockingTest {
            whenever(mockRepo.getPayeesList(anyString())).thenReturn(ETResult.failure(Throwable("bad thing")))

            val sut = BillPayInitialSetupUseCaseImpl(mockRepo, PayeeListUseCaseImpl(mockRepo), FilterBillPayAccountsUseCaseImp(), GenerateBillPayStatusUseCaseImp())
            val result = sut.execute(Unit)

            assertEquals("bad thing", result.exceptionOrNull()?.message)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should return Not Eligible status when an account is restricted`() {
        runBlockingTest {
            val restrictedBillPayStatus = BillPay(isRestricted = true, isEnrolled = true, isEligible = true, isPrimary = true)
            whenever(mockRepo.getAccounts()).thenReturn(ETResult.success(getMockAccountDetails(restrictedBillPayStatus)))

            val sut = BillPayInitialSetupUseCaseImpl(mockRepo, PayeeListUseCaseImpl(mockRepo), FilterBillPayAccountsUseCaseImp(), GenerateBillPayStatusUseCaseImp())
            val result = sut.execute(Unit)

            assertEquals(BillPayStatus.NotEligible, result.getOrNull()?.status)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should return Not Enrolled status when NO accounts is Enrolled for billPay`() {
        runBlockingTest {
            val invalidBillPayStatus = BillPay(isRestricted = false, isEnrolled = false, isEligible = true, isPrimary = true)
            whenever(mockRepo.getAccounts()).thenReturn(ETResult.success(getMockAccountDetails(invalidBillPayStatus)))

            val sut = BillPayInitialSetupUseCaseImpl(mockRepo, PayeeListUseCaseImpl(mockRepo), FilterBillPayAccountsUseCaseImp(), GenerateBillPayStatusUseCaseImp())
            val result = sut.execute(Unit)

            assertEquals(BillPayStatus.NotEnrolled, result.getOrNull()?.status)
        }
    }
}
