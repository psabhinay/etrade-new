package com.etrade.mobilepro.billpay.data.repo

import com.etrade.mobilepro.billpay.api.Payment
import com.etrade.mobilepro.billpay.api.PaymentStatus
import com.etrade.mobilepro.billpay.data.rest.PaymentListResponseDto
import com.etrade.mobilepro.util.formatters.parseAmericanDateFromString
import com.etrade.mobilepro.util.safeParseBigDecimal

private const val NULL_IN_STRING = "null"
internal fun PaymentListResponseDto.toPaymentEntry(): List<Payment> {
    return payments.map {
        val status = PaymentStatus.fromStatusString(it.paymentDetails.status)
        Payment(
            paymentId = it.paymentDetails.paymentId,
            status = status,
            amount = it.paymentDetails.amount?.replace("$", "")?.safeParseBigDecimal(),
            payeeName = it.payeeDetails.name,
            payeeNickname = it.payeeDetails.nickName?.replace(NULL_IN_STRING, "").takeIf { nickname -> !nickname.isNullOrBlank() },
            payeeAccountNumber = it.payeeDetails.accountNumber,
            deliveryDate = it.paymentDetails.deliveryDate?.let { deliveryDate -> parseAmericanDateFromString(deliveryDate) },
            scheduleDate = it.paymentDetails.scheduleDate?.let { scheduleDate -> parseAmericanDateFromString(scheduleDate) },
            memo = it.paymentDetails.memo,
            confirmationNumber = it.paymentDetails.confirmationNumber,
            accountId = it.paymentDetails.accountId
        )
    }
}
