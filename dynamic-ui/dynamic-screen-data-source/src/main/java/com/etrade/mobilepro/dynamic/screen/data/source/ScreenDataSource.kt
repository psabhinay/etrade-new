package com.etrade.mobilepro.dynamic.screen.data.source

import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable

interface ScreenDataSource {
    fun getScreen(request: ScreenRequest, showCachedData: Boolean = true, cacheExpiration: Long? = null): Observable<Resource<BaseMobileResponse>>
}
