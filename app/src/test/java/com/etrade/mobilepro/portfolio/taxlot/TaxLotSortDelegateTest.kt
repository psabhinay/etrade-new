package com.etrade.mobilepro.portfolio.taxlot

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.portfolio.data.PortfolioColumnRepo
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.util.SortOrder
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class TaxLotSortDelegateTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val portfolioColumnRepo: PortfolioColumnRepo = mock()

    private val mockObserver = mock<Observer<SortInfo>>()

    val sut = TaxLotSortDelegate(
        portfolioColumnRepo,
        PortfolioTableViewType.STANDARD_VIEW,
        PositionTableMode.PORTFOLIO_VIEW
    )

    @Test
    fun `when sort by Column with the tax lot sort state should match the input sort`() {

        // given
        val sortInfo = SortInfo(PortfolioRecordColumn.SYMBOL, SortOrder.DESCENDING)
        sut.sortInfo.observeForever(mockObserver)

        // when
        sut.sortColumn(sortInfo)

        // then
        verify(mockObserver).onChanged(eq(sortInfo))
    }

    @Test
    fun `when sort by Column,tax lot sort state should match expected sort`() {

        // given
        sut.sortInfo.observeForever(mockObserver)

        // when
        sut.sortColumn(PortfolioRecordColumn.DAYS_GAIN)

        // then
        val sortInfo = SortInfo(PortfolioRecordColumn.DAYS_GAIN, SortOrder.ASCENDING)
        verify(mockObserver).onChanged(eq(sortInfo))
    }

    @Test
    fun `when sort by column done repeated twice then the tax lot sort order should be inverse of initial sort state`() {

        // given
        sut.sortInfo.observeForever(mockObserver)

        // when
        sut.sortColumn(PortfolioRecordColumn.DAYS_GAIN)
        sut.sortColumn(PortfolioRecordColumn.DAYS_GAIN)

        // then
        val sortInfo = SortInfo(PortfolioRecordColumn.DAYS_GAIN, SortOrder.DESCENDING)
        verify(mockObserver).onChanged(eq(sortInfo))
    }
}
