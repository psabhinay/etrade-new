package com.etrade.mobilepro.education.adapter.delegate

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.education.EducationItem
import com.etrade.mobilepro.education.adapter.holder.EducationOptionsRiskDisclosureViewHolder
import com.etrade.mobilepro.education.databinding.AdapterEducationOptionsRiskDisclosureBinding
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate

class EducationOptionsRiskDisclosureAdapterDelegate(
    private val optionsRiskClickListenter: View.OnClickListener,
    private val disclosureClickListenter: View.OnClickListener
) : AdapterDelegate<MutableList<EducationItem>>() {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return EducationOptionsRiskDisclosureViewHolder(
            parent.viewBinding(AdapterEducationOptionsRiskDisclosureBinding::inflate)
        )
    }

    override fun isForViewType(items: MutableList<EducationItem>, position: Int): Boolean = DelegateHelper.isLastItem(position, items)

    override fun onBindViewHolder(items: MutableList<EducationItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        val educationOptionsRiskDisclosureHolder: EducationOptionsRiskDisclosureViewHolder? = holder as? EducationOptionsRiskDisclosureViewHolder

        educationOptionsRiskDisclosureHolder?.bind(optionsRiskClickListenter, disclosureClickListenter)
    }
}
