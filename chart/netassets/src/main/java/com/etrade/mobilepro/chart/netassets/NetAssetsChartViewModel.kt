package com.etrade.mobilepro.chart.netassets

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.barchartview.DataEntry
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.delegate.suppressInitial
import com.etrade.mobilepro.util.invoke
import kotlinx.coroutines.launch
import javax.inject.Inject

class NetAssetsChartViewModel @Inject constructor(
    private val getNetAssetsChartDataEntriesUseCase: GetNetAssetsChartDataEntriesUseCase
) : ViewModel() {

    val chartState: LiveData<ChartState>
        get() = _chartState

    var accountId: String? by suppressInitial(null) { loadChart() }

    private val _chartState: MutableLiveData<ChartState> = MutableLiveData()

    private fun loadChart() {
        _chartState.postValue(ChartState.Loading)
        viewModelScope.launch {
            when (val resource = getNetAssetsChartDataEntriesUseCase(GetNetAssetsChartDataEntriesParameter(accountId))) {
                is Resource.Success -> _chartState.postValue(ChartState.Idle(resource.data.orEmpty()))
                is Resource.Loading -> _chartState.postValue(ChartState.Loading)
                is Resource.Failed -> _chartState.postValue(ChartState.Error)
            }
        }
    }

    sealed class ChartState {
        data class Idle(val entries: List<DataEntry>) : ChartState()
        object Loading : ChartState()
        object Error : ChartState()
    }
}
