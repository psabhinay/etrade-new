package com.etrade.mobilepro.dialog.viewmodel

import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable

class DialogViewModel(private val resources: Resources) : AppDialogViewModel {

    private val _dialog: MutableLiveData<ConsumableLiveEvent<AppDialog?>?> = MutableLiveData()
    override val dialog: LiveData<ConsumableLiveEvent<AppDialog?>?> = _dialog

    fun postDialog(@StringRes titleResId: Int, @StringRes messageResId: Int) {
        _dialog.postValue(
            resources.appDialog {
                titleRes = titleResId
                messageRes = messageResId
                resourcesPositiveAction {
                    labelRes = R.string.ok
                }
                dismissAction = {
                    _dialog.value = null
                }
            }.consumable()
        )
    }

    fun dismissDialog() {
        _dialog.value = null
    }
}
