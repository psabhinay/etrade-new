package com.etrade.mobilepro.trade.data.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class DateTimeUtilTest {

    @Test
    fun `formatOrderPlacedTime for null argument`() {
        assertNull(formatOrderPlacedTime(null))
    }

    @Test
    fun `formatOrderPlacedTime for arbitrary argument`() {
        assertEquals("06/09/2020 01:40:06 PM EDT", formatOrderPlacedTime(1591724406669L))
    }
}
