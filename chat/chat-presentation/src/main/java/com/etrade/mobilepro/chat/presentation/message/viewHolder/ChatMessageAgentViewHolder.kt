package com.etrade.mobilepro.chat.presentation.message.viewHolder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.presentation.databinding.ChatItemMessageAgentBinding

class ChatMessageAgentViewHolder(private val binding: ChatItemMessageAgentBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: ChatMessage.Agent) {
        binding.agentMessageText.text = item.message
    }
}
