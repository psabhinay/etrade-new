package com.etrade.mobilepro.completeview.edit

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.home.overview.edit.OverviewWidget
import com.etrade.mobilepro.home.overview.edit.OverviewWidgetRepo
import com.etrade.mobilepro.msaccounts.api.MsAccountsRepo
import com.etrade.mobilepro.msaccounts.api.MsPreferencesRepo
import com.etrade.mobilepro.msaccounts.api.getSettingsCta
import com.etrade.mobilepro.tracking.SCREEN_TITLE_COMPLETE_VIEW_CUSTOMIZE
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.prependBaseUrl
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNot
import kotlinx.coroutines.launch
import javax.inject.Inject

const val RESULT_CODE_EDIT_COMPLETE_VIEW_CLOSED_WITH_WEB_VIEW_CHANGE = 1111

class EditCompleteViewViewModel @Inject constructor(
    @Web private val baseUrl: String,
    private val msPreferencesRepo: MsPreferencesRepo,
    private val resources: Resources,
    @CompleteViewRepo private val widgetsRepo: OverviewWidgetRepo,
    private val msAccountsRepo: MsAccountsRepo
) : EditListViewModel<OverviewWidget>(), ScreenViewModel {

    private var isMsVisibilitySettingsChanged: Boolean = false

    private var refreshMsWidgetTogglesJob: Job? = null

    private val _errorSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val errorSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _errorSignal

    val isMsAccountVisibilityCtaAvailable: Boolean
        get() {
            val msUserPreferences = msPreferencesRepo.msUserPreferences.value
            return msUserPreferences.isMsEligible && msUserPreferences.getSettingsCta() != null
        }

    val msSettingsCtaTitle: String
        get() = msPreferencesRepo.msUserPreferences.value.getSettingsCta()?.title
            ?: resources.getString(R.string.complete_view_ms_settings_default_title)

    val msSettingsCtaUrl: String
        get() = msPreferencesRepo.msUserPreferences.value.getSettingsCta()?.clickActionDto?.webViewUrl.prependBaseUrl(
            baseUrl
        )

    override val screenName: String = SCREEN_TITLE_COMPLETE_VIEW_CUSTOMIZE

    override fun createFrom(item: OverviewWidget, value: Boolean): OverviewWidget {
        return OverviewWidget(item.widgetName, item.displayText, value)
    }

    override fun onFetchItems(viewState: MutableLiveData<ViewState>) {
        viewModelScope.launch {
            widgetsRepo.getWidgetsFlow().collect { widgets ->
                provideItems(widgets)
            }

            viewState.value = ViewState.Idle
        }
    }

    override fun onApplyChanges(items: List<OverviewWidget>, removedItems: List<OverviewWidget>, viewState: MutableLiveData<ViewState>) {
        viewModelScope.launch {
            widgetsRepo.saveWidgets(items)

            viewState.value = ViewState.Success()
        }
    }

    override fun onCancelChanges(viewState: MutableLiveData<ViewState>) {
        if (isMsVisibilitySettingsChanged) {
            viewState.value =
                ViewState.Success(RESULT_CODE_EDIT_COMPLETE_VIEW_CLOSED_WITH_WEB_VIEW_CHANGE)
        } else {
            super.onCancelChanges(viewState)
        }
    }

    fun refreshMsWidgetToggles() {
        isMsVisibilitySettingsChanged = true
        refreshMsWidgetTogglesJob?.cancel()
        refreshMsWidgetTogglesJob = viewModelScope.launch {
            msAccountsRepo.getMsCompleteView()
                .filterNot { it is Resource.Loading }
                .collect {
                    if (it is Resource.Failed) {
                        _errorSignal.value = ConsumableLiveEvent(Unit)
                    }
                }
        }
    }
}
