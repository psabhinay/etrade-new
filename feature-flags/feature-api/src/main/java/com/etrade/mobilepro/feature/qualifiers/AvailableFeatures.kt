package com.etrade.mobilepro.feature.qualifiers

import com.etrade.mobilepro.feature.Feature
import javax.inject.Qualifier

/**
 * Dependency qualifier for a [List] of [Feature]s that can be resolved and displayed by a
 * Feature Flag UI.
 */
@Qualifier
annotation class AvailableFeatures
