package com.etrade.completeview.dto

import com.etrade.mobilepro.backends.mgs.StreamingValueDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AccountSummaryDto(
    @Json(name = "account_uuid")
    val accountUuid: String?,
    @Json(name = "account_name")
    val name: String?,
    @Json(name = "account_source_label")
    val accountSourceLabel: String?,
    @Json(name = "account_detail_label")
    val detailLabel: String,
    @Json(name = "account_detail_value")
    val detailValue: String,
    @Json(name = "account_additional_labels")
    val additionalSectionDtos: List<AccountSummarySectionDto>,
    @Json(name = "account_extra_details")
    val extraDetailsDtos: List<AccountSummarySectionDto> = emptyList(),
    // prompts and other data from Localytics to display in the widget
    val extraAugmentationInfo: List<Any> = emptyList()
)

@JsonClass(generateAdapter = true)
data class AccountSummarySectionDto(
    @Json(name = "account_additional_label_title")
    val title: String,
    @Json(name = "account_additional_label_streamable_value")
    val value: StreamingValueDto,
    @Json(name = "account_additional_label_value_detail")
    val valueDetail: String?
)
