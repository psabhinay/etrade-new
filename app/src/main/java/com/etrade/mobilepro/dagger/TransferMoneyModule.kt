package com.etrade.mobilepro.dagger

import com.etrade.mobile.accounts.details.AccountsDetailsRepo
import com.etrade.mobilepro.caching.coroutine.Cache
import com.etrade.mobilepro.caching.coroutine.DataSource
import com.etrade.mobilepro.caching.coroutine.SimpleCache
import com.etrade.mobilepro.common.di.Api
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.session.api.UserConsumerKeyInterface
import com.etrade.mobilepro.transfermoney.DefaultTransferStockAccountsDataSource
import com.etrade.mobilepro.transfermoney.api.Holidays
import com.etrade.mobilepro.transfermoney.api.repo.TransferCalendarRepo
import com.etrade.mobilepro.transfermoney.api.repo.TransferIraContributionRepo
import com.etrade.mobilepro.transfermoney.api.repo.TransferMoneyRepo
import com.etrade.mobilepro.transfermoney.api.repo.TransferRepeatDataSource
import com.etrade.mobilepro.transfermoney.api.repo.TransferStockAccountsDataSource
import com.etrade.mobilepro.transfermoney.data.repo.DefaultTransferIraContributionRepo
import com.etrade.mobilepro.transfermoney.data.repo.DefaultTransferMoneyRepo
import com.etrade.mobilepro.transfermoney.data.repo.DefaultTransferRepeatDataSource
import com.etrade.mobilepro.transfermoney.data.repo.HolidaysDataSource
import com.etrade.mobilepro.transfermoney.data.repo.TransferCalendarRepoImpl
import com.etrade.mobilepro.transfermoney.data.rest.TransferCalendarService
import com.etrade.mobilepro.transfermoney.data.rest.TransferIraContributionApiClient
import com.etrade.mobilepro.transfermoney.data.rest.TransferMoneyApiClient
import com.etrade.mobilepro.transfermoney.presentation.preview.ira.DefaultIraContributionAdapter
import com.etrade.mobilepro.transfermoney.presentation.preview.ira.IraContributionAdapter
import com.etrade.mobilepro.transfermoney.presentation.transfer.source.DefaultTransferSourceAdapter
import com.etrade.mobilepro.transfermoney.presentation.transfer.source.DefaultTransferSourceFilterer
import com.etrade.mobilepro.transfermoney.presentation.transfer.source.TransferSourceAdapter
import com.etrade.mobilepro.transfermoney.presentation.transfer.source.TransferSourceFilterer
import com.etrade.mobilepro.transfermoney.usecase.calendar.GetTransferCalendarUseCase
import com.etrade.mobilepro.transfermoney.usecase.calendar.GetTransferCalendarUseCaseImpl
import com.etrade.mobilepro.transfermoney.usecase.confirm.QuickTransferConfirmUseCase
import com.etrade.mobilepro.transfermoney.usecase.confirm.QuickTransferConfirmUseCaseImpl
import com.etrade.mobilepro.transfermoney.usecase.preview.GetQuickTransferPreviewUseCase
import com.etrade.mobilepro.transfermoney.usecase.preview.GetQuickTransferPreviewUseCaseIml
import com.etrade.mobilepro.util.JsonSerializer
import com.etrade.mobilepro.util.KeyValueStore
import com.etrade.mobilepro.util.Serializer
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
private const val CACHE_EXPIRATION_IN_MINUTES = 30L
private val HOLIDAYS_EXPIRATION_TIME_IN_MILLIS = TimeUnit.MINUTES.toMillis(CACHE_EXPIRATION_IN_MINUTES)

@Module
interface TransferMoneyModule {

    @Binds
    @Singleton
    fun bindHolidaysDataSource(impl: HolidaysDataSource): DataSource<Holidays, String>

    @Binds
    @Singleton
    fun bindTransferRepeatDataSource(dataSource: DefaultTransferRepeatDataSource): TransferRepeatDataSource

    @Binds
    @Singleton
    fun bindGetQuickTransferPreviewUseCase(usecase: GetQuickTransferPreviewUseCaseIml): GetQuickTransferPreviewUseCase

    @Binds
    @Singleton
    fun bindTransferAccountsRepo(repo: DefaultTransferMoneyRepo): TransferMoneyRepo

    @Binds
    fun bindTransferMoneyCalendarUseCase(useCase: GetTransferCalendarUseCaseImpl): GetTransferCalendarUseCase

    @Binds
    fun bindQuickTransferConfirmUseCase(useCase: QuickTransferConfirmUseCaseImpl): QuickTransferConfirmUseCase

    @Binds
    fun bindTransferMoneyCalendarRepo(repo: TransferCalendarRepoImpl): TransferCalendarRepo

    @Binds
    fun bindIraContributionAdapter(adapter: DefaultIraContributionAdapter): IraContributionAdapter

    @Binds
    fun bindTransferSourceAdapter(adapter: DefaultTransferSourceAdapter): TransferSourceAdapter

    @Binds
    fun bindTransferStockAccountsDataSource(dataSource: DefaultTransferStockAccountsDataSource): TransferStockAccountsDataSource

    @Binds
    fun bindTransferSourceFilterer(filterer: DefaultTransferSourceFilterer): TransferSourceFilterer

    companion object {

        @Provides
        fun provideHolidaysCache(
            converter: Serializer<Holidays>,
            keyValueStore: KeyValueStore
        ): Cache<Holidays, String> = SimpleCache(
            expirationTimeInMillis = HOLIDAYS_EXPIRATION_TIME_IN_MILLIS,
            serializer = converter,
            keyValueStore = keyValueStore
        )

        @Provides
        fun provideHolidaysConverter(moshi: Moshi): Serializer<Holidays> =
            JsonSerializer<Holidays>(moshi).apply { init<Holidays>() }

        @Provides
        @Singleton
        fun provideTransferMoneyApiClient(@MobileTrade retrofit: Retrofit): TransferMoneyApiClient = retrofit.create(TransferMoneyApiClient::class.java)

        @Provides
        @Singleton
        fun provideTransferMoneyCalendarService(@MobileTrade retrofit: Retrofit): TransferCalendarService = retrofit.create(
            TransferCalendarService::class.java
        )

        @Provides
        @Singleton
        fun provideTransferIraContributionApiClient(@Api retrofit: Retrofit): TransferIraContributionApiClient =
            retrofit.create(TransferIraContributionApiClient::class.java)

        @Provides
        fun provideTransferIraContributionRepo(
            apiClient: TransferIraContributionApiClient,
            accountsDetailsRepo: AccountsDetailsRepo,
            user: UserConsumerKeyInterface
        ): TransferIraContributionRepo = DefaultTransferIraContributionRepo(apiClient, accountsDetailsRepo, user::getConsumerKey)
    }
}
