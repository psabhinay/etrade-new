package com.etrade.mobilepro.dynamic.form.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.livedata.combine
import com.etrade.mobilepro.livedata.setDistinctValue
import com.etrade.mobilepro.util.ValueCoder
import com.etrade.mobilepro.util.ValueFormatter
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable

interface Input<T> : Component, InputEnabler {

    /**
     * The unique identifier of the input.
     */
    val id: String

    /**
     * The display title of the input dynamically.
     */
    val title: LiveData<CharSequence?>

    val contentDescription: LiveData<CharSequence?>

    /**
     * This is an actual value of the input.
     */
    val value: LiveData<T?>

    /**
     * This is a value that a user sees on screen.
     */
    val displayValue: LiveData<CharSequence?>

    /**
     * This is a value that an endpoint uses as part of its request parameters.
     */
    val parameterValue: LiveData<Any?>

    /**
     * Indicates that an input has some value.
     */
    val hasValue: LiveData<Boolean>

    /**
     * Removes a value from an input.
     */
    fun clearValue()

    /**
     * Resets a value to initial.
     */
    fun resetValue()

    /**
     * Sets a [value] from [encoded] string, that represents [parameterValue]. If decoding fails, the sets `null`.
     *
     * @param encoded The encoded representation of [value].
     */
    fun setParameterValue(encoded: Any?)

    /**
     * Sets a [value] if its `null` or valid.
     *
     * @param value The value to set.
     */
    fun setValue(value: T?)
}

typealias ContentDescriptionFormatter = (CharSequence?, CharSequence?) -> CharSequence?

/**
 * Base implementation for any [Input]s.
 */
abstract class BaseInput<T>(
    final override val id: String,
    private val valueCoder: ValueCoder<T>,
    private val valueFormatter: ValueFormatter<T>? = null,
    final override val title: LiveData<CharSequence?> = MutableLiveData(),
    private val customContentDescription: CharSequence? = null,
    private val contentDescriptionFormatter: ContentDescriptionFormatter? = null,
    isEnabled: LiveData<Boolean> = MutableLiveData(true),
    private val initialValue: T? = null,
    private val doBeforeReset: () -> Unit = { /* Intentionally blank */ }
) : Input<T>, InputEnabler by InputEnablerImpl(isEnabled) {

    private val _value: MutableLiveData<T?> = MutableLiveData(initialValue)

    final override val value: LiveData<T?>
        get() = _value

    final override val displayValue: LiveData<CharSequence?> = value.map { value -> value?.let { valueFormatter?.format(it) ?: "" } }

    final override val parameterValue: LiveData<Any?> = value.map { value -> value?.let { valueCoder.encode(it) } }

    final override val hasValue: LiveData<Boolean> = value.map { it != null }

    override val isVisible: LiveData<Boolean> = MutableLiveData(true)

    override val contentDescription: LiveData<CharSequence?>
        get() {
            return getFinalContentDescription(
                title = title,
                displayValue = displayValue,
                customContentDescription = customContentDescription,
                contentDescriptionFormatter = contentDescriptionFormatter
            )
        }

    init {
        parameterValue.observeForever { /* Intentionally blank. */ }
    }

    /**
     * Checks if a [value] is valid.
     *
     * Default implementation of this function treats all values as valid.
     *
     * @param value The value to check.
     */
    open fun isValueValid(value: T): Boolean = true

    /**
     * The callback to be invoked when a user tries to set invalid value.
     */
    open fun onInvalidValue(): Unit = Unit

    final override fun clearValue() {
        _value.value = null
    }

    final override fun resetValue() {
        doBeforeReset()
        _value.value = initialValue
    }

    final override fun setValue(value: T?) {
        if (value == null || isValueValid(value)) {
            _value.setDistinctValue(value)
        } else {
            onInvalidValue()
        }
    }

    final override fun setParameterValue(encoded: Any?) = setValue(encoded?.let { valueCoder.decode(it) })
}

typealias ExternalAction = () -> Unit

class ExternalInput<T>(
    id: String,
    title: LiveData<CharSequence?>,
    contentDescription: CharSequence? = null,
    contentDescriptionFormatter: ContentDescriptionFormatter?,
    initialValue: T?,
    valueCoder: ValueCoder<T>,
    valueFormatter: ValueFormatter<T>,
    override val isVisible: LiveData<Boolean>,
    isEnabled: LiveData<Boolean>,
    doBeforeReset: () -> Unit,
    val action: ExternalAction,
    val showClearAction: Boolean,
    private val isValid: (T) -> Boolean,
    private val onInvalidValue: () -> Unit
) : BaseInput<T>(id, valueCoder, valueFormatter, title, contentDescription, contentDescriptionFormatter, isEnabled, initialValue, doBeforeReset) {

    override fun isValueValid(value: T): Boolean = isValid(value)
    override fun onInvalidValue() = onInvalidValue.invoke()
}

open class SelectInput(
    override val id: String,
    override val title: LiveData<CharSequence?>,
    private val customContentDescription: CharSequence? = null,
    private val contentDescriptionFormatter: ContentDescriptionFormatter?,
    override val isVisible: LiveData<Boolean>,
    isEnabled: LiveData<Boolean> = MutableLiveData(true),
    shouldInterceptClick: LiveData<Boolean> = MutableLiveData(false),
    val dropDown: DropDown,
    val toolBarActionEnd: ToolbarActionEnd? = null
) : Input<DropDownOption>,
    InputEnabler by InputEnablerImpl(isEnabled),
    ClickInterceptor by ClickInterceptorImpl(shouldInterceptClick),
    ClickRequester by ClickRequesterImpl() {

    final override val value: LiveData<DropDownOption?> = dropDown.selectedOption
    override val displayValue: LiveData<CharSequence?> = dropDown.selectedOption.map { it?.name }
    final override val parameterValue: LiveData<Any?> = dropDown.selectedOption.map { it?.id }
    override val hasValue: LiveData<Boolean> = value.map { it != null }
    override val contentDescription: LiveData<CharSequence?>
        get() {
            return getFinalContentDescription(
                title = title,
                displayValue = displayValue,
                customContentDescription = customContentDescription,
                contentDescriptionFormatter = contentDescriptionFormatter
            )
        }

    init {
        parameterValue.observeForever { /* Intentionally blank. */ }
    }

    override fun clearValue() = dropDown.selectOption(null)

    override fun resetValue() = dropDown.resetValue()

    override fun setParameterValue(encoded: Any?) = dropDown.selectOption(encoded as? String)

    override fun setValue(value: DropDownOption?) {
        if (value == null) {
            dropDown.selectOption(null)
        } else {
            dropDown.selectOption(value)
        }
    }
}

class SwitchInput(
    id: String,
    title: LiveData<CharSequence?>,
    contentDescription: CharSequence? = null,
    contentDescriptionFormatter: ContentDescriptionFormatter?,
    initialValue: Boolean?,
    valueCoder: ValueCoder<Boolean>,
    valueFormatter: ValueFormatter<Boolean>,
    override val isVisible: LiveData<Boolean>,
    isEnabled: LiveData<Boolean>
) : BaseInput<Boolean>(id, valueCoder, valueFormatter, title, contentDescription, contentDescriptionFormatter, isEnabled, initialValue)

class TextInput(
    id: String,
    title: LiveData<CharSequence?>,
    contentDescription: CharSequence? = null,
    contentDescriptionFormatter: ContentDescriptionFormatter?,
    initialValue: String?,
    valueCoder: ValueCoder<String>,
    valueFormatter: ValueFormatter<String>,
    override val isVisible: LiveData<Boolean>,
    isEnabled: LiveData<Boolean>,
    val textInputIcon: TextInputIcon?,
    val textInputType: LiveData<TextInputType>
) : BaseInput<String>(id, valueCoder, valueFormatter, title, contentDescription, contentDescriptionFormatter, isEnabled, initialValue) {

    val isIconVisible: LiveData<Boolean>
        get() = _isIconVisible

    private val _isIconVisible = MutableLiveData(false)

    val isKeyboardVisible: LiveData<Boolean>
        get() = _isKeyboardVisible
    private val _isKeyboardVisible = MutableLiveData<Boolean>()

    val monitorKeyboardVisibilitySignal: LiveData<ConsumableLiveEvent<Boolean>>
        get() = _monitorKeyboardVisibilitySignal
    private val _monitorKeyboardVisibilitySignal = MutableLiveData<ConsumableLiveEvent<Boolean>>()

    fun setIconVisible(isVisible: Boolean) {
        _isIconVisible.value = isVisible
    }

    fun requestToMonitorKeyboardVisibility(shouldMonitor: Boolean) {
        _monitorKeyboardVisibilitySignal.value = shouldMonitor.consumable()
    }

    fun notifyKeyboardVisibility(isVisible: Boolean) {
        _isKeyboardVisible.value = isVisible
    }
}

class HiddenInput(
    id: String,
    initialValue: String?,
    valueCoder: ValueCoder<String>,
    valueFormatter: ValueFormatter<String>,
    override val isVisible: LiveData<Boolean>,
    isEnabled: LiveData<Boolean>
) : BaseInput<String>(id = id, initialValue = initialValue, isEnabled = isEnabled, valueCoder = valueCoder, valueFormatter = valueFormatter)

class TextInputIcon(val iconResId: Int, val contentDescription: String? = null, val action: () -> Unit)

private fun getFinalContentDescription(
    title: LiveData<CharSequence?>,
    displayValue: LiveData<CharSequence?>,
    customContentDescription: CharSequence? = null,
    contentDescriptionFormatter: ContentDescriptionFormatter? = null
): LiveData<CharSequence?> {
    val finalTitle: LiveData<CharSequence?> = if (customContentDescription.isNullOrBlank()) {
        title
    } else {
        MutableLiveData(customContentDescription)
    }
    return finalTitle.combine(displayValue) { t, dv ->
        contentDescriptionFormatter?.invoke(t, dv) ?: "${t ?: ""} ${dv ?: ""}"
    }
}
