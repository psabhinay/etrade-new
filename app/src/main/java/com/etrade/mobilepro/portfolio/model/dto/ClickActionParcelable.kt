package com.etrade.mobilepro.portfolio.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ClickActionParcelable(
    val webUrl: String?,
    val webViewUrl: String?,
    val appUrl: String?
) : Parcelable
