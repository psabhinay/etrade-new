package com.etrade.mobilepro.checkdeposit.presentation.details

import android.content.res.Resources
import android.text.format.DateUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.checkdeposit.activity.service.DepositActivityItemDto
import com.etrade.mobilepro.checkdeposit.presentation.DepositStatus
import com.etrade.mobilepro.checkdeposit.presentation.R
import com.etrade.mobilepro.common.di.Api
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.safeParseBigDecimal
import javax.inject.Inject

class DepositDetailsViewModel @Inject constructor(
    private val resources: Resources,
    @Api private val apiBaseUrl: String
) : ViewModel() {

    private lateinit var accountName: String
    private val depositDetails: MutableLiveData<DepositActivityItemDto> = MutableLiveData()

    val detailsItems: LiveData<List<DepositDetail>> = Transformations.map(depositDetails) {
        val result: MutableList<DepositDetail> = mutableListOf()

        addDepositDetails(result, it)

        if (it.frontCheckId.isNullOrEmpty() && it.backCheckId.isNullOrEmpty() || it.accountIdKey.isNullOrEmpty()) {
            result.add(DepositDetail.FooterMessage(resources.getString(R.string.check_images_not_available_message)))
        } else {
            it.accountIdKey?.also { accountIdKey ->
                result.add(DepositDetail.Divider)

                it.frontCheckId?.also { frontCheckId ->
                    addDepositImage(result, accountIdKey, frontCheckId, true)
                }

                it.backCheckId?.also { backCheckId ->
                    addDepositImage(result, accountIdKey, backCheckId, false)
                }
            }
        }

        result
    }

    private fun resolveDepositCheckImageUrlFromId(accountIdKey: String, id: String): String {
        return "$apiBaseUrl/v1/accounts/$accountIdKey/documents/images/$id?docType=DEPOSIT_CHECK"
    }

    @Suppress("LongMethod")
    private fun addDepositDetails(result: MutableList<DepositDetail>, itemDto: DepositActivityItemDto) {
        val emptyValue = resources.getString(R.string.empty_default)

        result.add(
            DepositDetail.LabelValue(
                resources.getString(R.string.deposit_status),
                itemDto.status?.let { resources.getString(DepositStatus.labelFromValue(it)) } ?: emptyValue
            )
        )
        result.add(
            DepositDetail.LabelValue(
                resources.getString(R.string.deposit_date),
                itemDto.depositDate?.toLongOrNull()
                    ?.let { DateFormattingUtils.formatToShortDate(it * DateUtils.SECOND_IN_MILLIS) }
                    ?: resources.getString(R.string.empty_date_default)
            )
        )
        result.add(
            DepositDetail.LabelValue(
                resources.getString(R.string.amount),
                itemDto.amount?.safeParseBigDecimal()?.let { MarketDataFormatter.formatCurrencyValue(it) } ?: emptyValue
            )
        )
        result.add(
            DepositDetail.LabelValue(
                resources.getString(R.string.to),
                accountName
            )
        )
        result.add(
            DepositDetail.LabelValue(
                resources.getString(R.string.deposit_type),
                resources.getString(R.string.mobile_deposit)
            )
        )
        result.add(
            DepositDetail.LabelValue(
                resources.getString(R.string.deposit_reference_number),
                itemDto.requestId ?: emptyValue,
                itemDto.requestId?.characterByCharacter()
            )
        )
    }

    private fun addDepositImage(result: MutableList<DepositDetail>, accountIdKey: String, imageId: String, isFront: Boolean) {
        resolveDepositCheckImageUrlFromId(accountIdKey, imageId)
            .also { result.add(DepositDetail.Image(it, isFront)) }
    }

    fun setDepositDetails(item: DepositActivityItemDto, accountName: String) {
        this.accountName = accountName
        depositDetails.value = item
    }
}
