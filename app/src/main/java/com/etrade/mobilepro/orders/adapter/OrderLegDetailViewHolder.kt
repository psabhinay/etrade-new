package com.etrade.mobilepro.orders.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem
import com.etrade.mobilepro.trade.presentation.databinding.TradeItemOrderLegBinding
import com.etrade.mobilepro.trade.presentation.util.formatLegDetails
import com.etrade.mobilepro.util.android.binding.resources
import com.etrade.mobilepro.util.formatters.injectAMLabel

class OrderLegDetailViewHolder(
    private val binding: TradeItemOrderLegBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(item: OrderDetailItemInterface?) {
        binding.root.text = binding.resources.run {
            when (item) {
                is OrderDetailDisplayItem.OrderLegItem -> {
                    formatLegDetails(item.transactionType, item.displaySymbol, item.displayQuantity)
                        .injectAMLabel(item.isAMOption)
                }
                is OrderDetailDisplayItem.OrderExtraLegItem -> formatLegDetails(item.transactionType, item.displaySymbol)
                else -> getString(R.string.empty_default)
            }
        }
    }
}
