package com.etrade.mobilepro.dynamic.form.api.util

import android.content.res.Resources
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertEquals
import org.junit.Test

class StringResourceExtensionTest {

    @Test
    fun `check drop down option from string resource id`() {
        val resources = mock<Resources>().apply {
            whenever(getString(any())).thenAnswer { invocation ->
                "resource ${invocation.arguments[0]}"
            }
        }

        (-5..5).forEach { stringResId ->
            val actual = stringResId.toDropDownOption(resources)
            assertEquals(stringResId.toString(), actual.id)
            assertEquals("resource $stringResId", actual.name)
        }
    }
}
