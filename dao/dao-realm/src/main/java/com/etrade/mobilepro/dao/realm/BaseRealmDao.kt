package com.etrade.mobilepro.dao.realm

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dao.BaseDao
import io.reactivex.Observable
import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmObject
import io.realm.RealmQuery
import io.realm.RealmResults

open class BaseRealmDao<T : RealmModel>(protected val realm: Realm, protected val classObj: Class<T>) : BaseDao<T, LiveData<List<T>>> {
    private val primaryKeyName: String by lazy { realm.schema.get(classObj.simpleName)!!.primaryKey }

    override fun get(key: String): T? = realm.where(classObj).equalTo(primaryKeyName, key).findFirst()

    override fun getWhere(field: String, value: String): T? = realm.where(classObj).equalTo(field, value).findFirst()

    override fun getAll(async: Boolean): LiveData<List<T>> = realm.where(classObj).findAll(async).asLiveData()

    override fun getAllIn(field: String, value: Array<String>, async: Boolean): LiveData<List<T>> = getAllInAsRealmResults(field, value, async).asLiveData()

    override fun save(obj: T) = realm.use {
        it.executeTransaction { realmTxn ->
            realmTxn.insertOrUpdate(obj)
        }
    }

    // executeTransactionAsync will close the realm for us
    override fun saveAsync(obj: T) =
        runCatchingET {
            realm.executeTransactionAsync {
                it.insertOrUpdate(obj)
            }
            Unit
        }

    override fun saveAll(objList: List<T>) = realm.use {
        it.executeTransaction { realmTxn ->
            realmTxn.insertOrUpdate(objList)
        }
    }

    override fun saveAllAsync(objList: List<T>) =
        runCatchingET {
            realm.executeTransactionAsync {
                it.insertOrUpdate(objList)
            }
            Unit
        }

    override fun delete(obj: T) = realm.use {
        it.executeTransaction {
            RealmObject.deleteFromRealm(obj)
        }
    }

    override fun deleteAll() = realm.use {
        it.executeTransaction { realmTxn ->
            realmTxn.delete(classObj)
        }
    }

    fun getAllWhere(field: String, value: String, async: Boolean = false): RealmResults<T> = realm.where(classObj).equalTo(field, value).findAll(async)

    fun getAllObservable(): Observable<List<T>> = realm.where(classObj).findAll().asObservable()

    fun getAllInAsRealmResults(field: String, value: Array<String>, async: Boolean = false): RealmResults<T> =
        realm.where(classObj).`in`(field, value).findAll(async)
}

private fun <E : RealmModel> RealmQuery<E>.findAll(async: Boolean): RealmResults<E> = if (async) {
    this.findAllAsync()
} else {
    this.findAll()
}
