package com.etrade.mobilepro.transfermoney.data.rest

import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface TransferIraContributionApiClient {
    @JsonMoshi
    @GET("v1/accounts/{accountIdKey}/transfers/iracontribinfo.json")
    suspend fun getTransferIraContributionInfo(
        @Header("DataToken")
        consumerKey: String,
        @Path("accountIdKey")
        accountIdKey: String,
        @Query("contribTaxDate")
        taxDate: String,
        @Query("_")
        fromAccountId: String
    ): TransferIraContributionResponseDto
}
