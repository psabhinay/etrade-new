package com.etrade.mobilepro.msaccounts.presentation

import android.content.res.Resources
import androidx.core.text.buildSpannedString
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.completeview.dto.AccountSummarySectionDto
import com.etrade.mobilepro.completeview.model.AccountSummarySection
import com.etrade.mobilepro.completeview.toAccountSummarySections
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.Diffable
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.msaccounts.presentation.model.MsAccountFootnote
import com.etrade.mobilepro.msaccounts.presentation.model.MsAccountSummary
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.accessibility.listDescription
import com.etrade.mobilepro.util.android.textutil.scaleSuperscript
import com.etrade.msaccounts.dto.MsAccountFootnoteDto
import com.etrade.msaccounts.dto.MsAccountSectionDto
import com.etrade.msaccounts.dto.MsAccountsSummaryDto

class MsAccountsSummaryViewModel(
    dto: MsAccountsSummaryDto,
    private val streamingTextManager: StreamingTextManager,
    private val resources: Resources
) : DynamicViewModel(R.layout.ms_accounts_summary_card), Diffable {

    override val variableId: Int = BR.obj

    private val _additionalLabels = MutableLiveData<List<AccountSummarySection>>()
    val additionalLabels: LiveData<List<AccountSummarySection>>
        get() = _additionalLabels

    private val _msAccounts = MutableLiveData<List<MsAccountSummary>>()
    val msAccounts: LiveData<List<MsAccountSummary>>
        get() = _msAccounts

    private val _footnotes = MutableLiveData<List<MsAccountFootnote>>()
    val footnotes: LiveData<List<MsAccountFootnote>>
        get() = _footnotes

    init {
        initValues(dto)
    }

    fun refresh(dto: MsAccountsSummaryDto) {
        initValues(dto)
    }

    // Assumes that there will be only one MsAccountsSummaryViewModel in a list
    override fun isSameItem(other: Any) =
        other.javaClass == javaClass

    override fun hasSameContents(other: Any) = false

    fun onInfoClicked() {
        clickEvents.postValue(
            CtaEvent.InfoDialogEvent(
                resources.getString(R.string.ms_accounts_summary_info_title),
                resources.getString(R.string.ms_accounts_summary_info_content)
            )
        )
    }

    private fun initValues(dto: MsAccountsSummaryDto) {
        _additionalLabels.value = dto.accountAdditionalLabels.toAccountLabelSections()
        _msAccounts.value = dto.accountSections.toMsAccountSummary()
        _footnotes.value = dto.accountSections.toMsAccountFootnotes()
    }

    private fun List<AccountSummarySectionDto>.toAccountLabelSections(): List<AccountSummarySection> =
        toAccountSummarySections(
            streamingTextManager,
            null,
            false // TODO: TBD
        )

    private fun List<MsAccountSectionDto>.toMsAccountSummary() = map { account ->
        val footnoteSymbols = account.accountFootnotes.organizeFootnotes().map { it.symbol }
        val title = buildAccountTitle(account.accountLabel, footnoteSymbols)
        val titleDescription = buildAccountTitleDescription(account.accountLabel, footnoteSymbols)
        MsAccountSummary(
            title = title,
            titleDescription = titleDescription,
            additionalLabels = account.accountAdditionalLabels.toAccountLabelSections()
        )
    }

    private fun List<MsAccountSectionDto>.toMsAccountFootnotes() = flatMap { it.accountFootnotes }
        .organizeFootnotes()
        .map {
            MsAccountFootnote(
                symbol = it.symbol,
                text = it.text,
                description = resources.getString(
                    R.string.ms_accounts_summary_footnote_item_desc,
                    it.symbol,
                    it.text
                )
            )
        }

    private fun buildAccountTitle(
        label: String,
        footnoteSymbols: List<String>
    ): CharSequence = buildSpannedString {
        append(label)
        if (footnoteSymbols.isNotEmpty()) {
            append(" ")
            scaleSuperscript { append(footnoteSymbols.joinToString(",")) }
        }
    }

    private fun buildAccountTitleDescription(
        label: String,
        footnoteSymbols: List<String>
    ): String = buildString {
        append(resources.accountNameContentDescription(label))
        if (footnoteSymbols.isNotEmpty()) {
            append(" ")
            append(
                resources.getString(
                    R.string.ms_accounts_summary_footnote_desc,
                    footnoteSymbols.listDescription(resources, R.plurals.ms_accounts_footnote)
                )
            )
        }
    }

    private fun List<MsAccountFootnoteDto>.organizeFootnotes() = distinct().sortedBy { it.symbol }
}
