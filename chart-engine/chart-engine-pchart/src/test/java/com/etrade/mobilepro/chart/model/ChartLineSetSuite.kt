package com.etrade.mobilepro.chart.model

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.chartengine.TimedChartPoint
import com.etrade.mobilepro.util.math.percentageChange
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.threeten.bp.LocalDateTime
import org.threeten.bp.Month

private const val DOUBLE_DELTA = 0.001
private const val BASE = 10.0

@RunWith(AndroidJUnit4::class)
class ChartLineSetSuite {

    private val points: List<Pair<Month, Double>> = listOf(
        Month.JANUARY to 38.69,
        Month.FEBRUARY to 31.68,
        Month.MARCH to 51.89,
        Month.APRIL to -66.37,
        Month.MAY to -25.99,
        Month.JUNE to 13.49,
        Month.JULY to 35.69,
        Month.AUGUST to 7.4,
        Month.SEPTEMBER to 73.42,
        Month.OCTOBER to 52.06,
        Month.NOVEMBER to -76.73,
        Month.DECEMBER to 88.89
    )

    private val minYPoint: Double = points.minByOrNull { it.second }!!.second
    private val maxYPoint: Double = points.maxByOrNull { it.second }!!.second

    @Test
    fun `verify points with raw values`() {
        val lineSet = createLineSet(false)
        assertEquals(lineSet.lowestValue, minYPoint, DOUBLE_DELTA)
        assertEquals(lineSet.highestValue, maxYPoint, DOUBLE_DELTA)

        lineSet.getLineSet().values.forEachIndexed { index, entry ->
            assertEquals(points[index].second, entry.y.toDouble(), DOUBLE_DELTA)
        }
    }

    @Test
    fun `verify points with percentage values`() {
        val lineSet = createLineSet(true)
        assertEquals(lineSet.lowestValue, percentageChange(minYPoint, BASE), DOUBLE_DELTA)
        assertEquals(lineSet.highestValue, percentageChange(maxYPoint, BASE), DOUBLE_DELTA)

        lineSet.getLineSet().values.forEachIndexed { index, entry ->
            assertEquals(percentageChange(points[index].second, BASE), entry.y.toDouble(), DOUBLE_DELTA)
        }
    }

    private fun createLineSet(showAsPercentage: Boolean): ChartLineSet = ChartLineSet("label", 0, BASE, showAsPercentage, false).apply {
        points.forEach { point -> addPoint(Point(LocalDateTime.of(2000, point.first, 1, 0, 0), point.second), ChartTimeFrame.OneYear) }
    }

    private class Point(override val x: LocalDateTime, override val y: Double) : TimedChartPoint
}
