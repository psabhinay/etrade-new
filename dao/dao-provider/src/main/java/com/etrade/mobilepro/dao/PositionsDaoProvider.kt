package com.etrade.mobilepro.dao

import com.etrade.mobilepro.positions.dao.PortfolioPositionDao

interface PositionsDaoProvider {
    fun getPositionsDao(): PortfolioPositionDao
}
