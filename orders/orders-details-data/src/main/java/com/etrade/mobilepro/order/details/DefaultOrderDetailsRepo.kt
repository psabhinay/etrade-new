package com.etrade.mobilepro.order.details

import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.backends.api.ServerResponseException
import com.etrade.mobilepro.backends.neo.toServerMessage
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderCancelRequest
import com.etrade.mobilepro.order.details.api.OrderDetailDescription
import com.etrade.mobilepro.order.details.api.OrderDetailsRepo
import com.etrade.mobilepro.order.details.api.OrderDetailsRequestParams
import com.etrade.mobilepro.order.details.api.StockPlanOrderCancelResponse
import com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequest
import com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequestDto
import com.etrade.mobilepro.order.details.dto.OrderDeleteRequestDto
import com.etrade.mobilepro.order.details.dto.OrderDeleteRequestValueDto
import com.etrade.mobilepro.order.details.dto.OrderDetailsRequestDto
import com.etrade.mobilepro.order.details.dto.OrderDetailsRequestValueDto
import com.etrade.mobilepro.order.details.mapper.OrderDetailsMapper
import com.etrade.mobilepro.order.details.rest.OrderDetailsApiClient
import com.etrade.mobilepro.orders.api.OrderRequestType
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.etrade.mobilepro.util.safeParseBigDecimal
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class DefaultOrderDetailsRepo @Inject constructor(
    private val orderDetailsApiClient: OrderDetailsApiClient,
    private val mapper: OrderDetailsMapper
) : OrderDetailsRepo {
    override fun getSavedOrderDetails(requestParams: OrderDetailsRequestParams): Observable<List<BaseOrderDetailsItem>> {

        val orderDetailsRequestDto = OrderDetailsRequestDto(
            OrderDetailsRequestValueDto(
                requestParams.accountId,
                requestParams.orderNumber
            )
        )
        return orderDetailsApiClient.getSavedOrderDetails(orderDetailsRequestDto)
            .toObservable()
            .map {
                it.data?.preparedOrderDetails ?: throw IllegalArgumentException("Empty payload")
            }
            .map {
                mapper.map(it)
            }
    }

    override fun getOrderDetails(requestParams: OrderDetailsRequestParams): Observable<List<BaseOrderDetailsItem>> {
        val orderDetailsRequestDto = OrderDetailsRequestDto(
            OrderDetailsRequestValueDto(
                requestParams.accountId,
                requestParams.orderNumber
            )
        )
        return orderDetailsApiClient.getOrderDetails(orderDetailsRequestDto)
            .toObservable()
            .map {
                it.data?.viewOrderEvents ?: throw IllegalArgumentException("Empty payload")
            }
            .map {
                mapper.map(it)
            }
    }

    override suspend fun getStockPlanOrderDetails(orderNumber: String, symbol: String): ETResult<BaseOrderDetailsItem.StockPlanOrderDetailsItem> {
        return orderDetailsApiClient.runCatchingET {
            getEmployeeStockPlanDetails(orderNumber)
        }.map {
            mapper.mapStockPlanOrderDetails(it, symbol)
        }
    }

    override fun cancelOrder(requestParams: OrderCancelRequest): Single<List<ServerMessage>> {
        val underlier = if (requestParams.instrumentType == InstrumentType.EQ) {
            requestParams.orderDetailsItem.orderDescriptions[0].displaySymbol
        } else {
            requestParams.orderDetailsItem.orderDescriptions[0].underlyingProductId
        }

        val legRequestList = requestParams.orderDetailsItem.orderDescriptions.map {
            val symbol = when (it.typeCode) {
                InstrumentType.EQ -> it.underlyingProductId
                InstrumentType.OPTN -> it.originalSymbol
                else -> ""
            }
            createLegDetailsCommonDto(symbol, it)
        }

        return orderDetailsApiClient.cancelConfirm(
            OrderConfirmCancelRequestDto(
                createOrderConfirmCancelRequest(underlier, requestParams, legRequestList)
            )
        ).map {
            it.data?.getAllMessages()
        }
    }

    private fun createOrderConfirmCancelRequest(
        underlier: String,
        requestParams: OrderCancelRequest,
        legRequestList: List<LegDetailsCommonDto>
    ): OrderConfirmCancelRequest {
        return OrderConfirmCancelRequest(
            underlier = underlier,
            strategyType = requestParams.orderDetailsItem.strategyType?.typeCode ?: "",
            requestType = OrderRequestType.ORDER_REQTYPE_CANCEL.toString(),
            originalOrderNo = requestParams.orderDetailsItem.orderNumber,
            priceType = requestParams.orderDetailsItem.priceType.typeCode,
            allOrNone = requestParams.orderDetailsItem.execInstruction.allOrNoneFlag.toString(),
            accountId = requestParams.accountId,
            orderType = requestParams.orderDetailsItem.orderType.typeCode,
            orderTerm = requestParams.orderTerm,
            stopPrice = requestParams.orderDetailsItem.stopPrice?.toPlainString() ?: "0",
            limitPrice = requestParams.orderDetailsItem.limitPrice?.toPlainString() ?: "0",
            orderNumber = requestParams.orderDetailsItem.orderNumber,
            origSysCodeReq = requestParams.origSysCode.numberCode,
            legRequestList = legRequestList
        )
    }

    private fun createLegDetailsCommonDto(
        symbol: String,
        description: OrderDetailDescription
    ): LegDetailsCommonDto {
        return LegDetailsCommonDto().also {
            it.symbol = symbol
            it.quantity = description.quantity.safeParseBigDecimal()?.toInt() ?: 0
            it.transactionCode = description.transactionType.typeCode.toString()
            it.optionType = description.optionType.requestCode
            it.expiryDay = description.optExpireDate
            it.expiryMonth = description.optExpireMonth
            it.expiryYear = description.optExpireYear
            it.cancelQty = description.quantity
        }
    }

    override suspend fun cancelStockPlanOrder(orderNumber: String): ETResult<StockPlanOrderCancelResponse> {
        return orderDetailsApiClient.runCatchingET {
            cancelEmployeeStockPlanOrder(orderNumber)
        }.map {
            mapper.mapStockOrderCancelResponse(it)
        }
    }

    override fun cancelGroupOrder(requestParams: OrderCancelRequest): Single<List<ServerMessage>> {
        return orderDetailsApiClient.groupCancel(
            requestParams.accountId,
            requestParams.orderDetailsItem.orderNumber,
            requestParams.orderDetailsItem.orderType.typeCode
        ).map { response ->
            response.errors?.let {
                if (it.isNotEmpty()) {
                    throw ServerResponseException(
                        ServerError.Known(
                            response.errors.map { error ->
                                error.toServerMessage()
                            }
                        )
                    )
                }
            }

            response.warnings?.map {
                it.toServerMessage()
            } ?: emptyList()
        }
    }

    override suspend fun deleteOrder(accountId: String, orderId: String): ETResult<Unit> {
        return orderDetailsApiClient.runCatchingET {
            deleteSavedOrder(
                OrderDeleteRequestDto(
                    OrderDeleteRequestValueDto(
                        accountId = accountId,
                        preparedId = orderId
                    )
                )
            )
            Unit
        }
    }
}
