package com.etrade.mobilepro.progressoverlay

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

/**
 * Full screen progress dialog that doesn't allow any interactions with underlying views.
 *
 * Do not add this fragment manually, use [show(FragmentManager)] method.
 */
class ProgressDialogFragment : DialogFragment() {

    private val isFullscreen: Boolean by lazy { arguments?.getBoolean(KEY_FULLSCREEN, DEFAULT_VALUE_FULLSCREEN) ?: DEFAULT_VALUE_FULLSCREEN }
    private val dimBackground: Boolean by lazy { arguments?.getBoolean(KEY_DIM_BACKGROUND, DEFAULT_VALUE_DIM_BACKGROUND) ?: DEFAULT_VALUE_DIM_BACKGROUND }

    /**
     * Let's you set cancellation listener for the dialog.
     */
    var onCancelListener: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (isFullscreen) {
            setStyle(STYLE_NO_FRAME, R.style.BaseAppTheme)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_progress_dialog, container, false)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.apply {
            if (isFullscreen) {
                setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            }
            if (!dimBackground) {
                setDimAmount(0F)
            }
        }
    }

    override fun onCancel(dialog: DialogInterface) {
        onCancelListener?.invoke()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(requireContext(), theme) {
            override fun onBackPressed() {
                runnable?.also { handler.removeCallbacks(it) }
                runnable = null
                onCancelListener?.invoke()
                dismissAllowingStateLoss()
            }
        }
    }

    companion object {

        private const val TAG = "ProgressDialogFragment"

        private const val SHOW_TIMEOUT = 500L

        private const val KEY_DIM_BACKGROUND = "dim_background"
        private const val KEY_FULLSCREEN = "fullscreen"
        private const val DEFAULT_VALUE_DIM_BACKGROUND = true
        private const val DEFAULT_VALUE_FULLSCREEN = true

        private val handler by lazy { Handler(Looper.getMainLooper()) }

        private var runnable: Runnable? = null

        /**
         * Shows [ProgressDialogFragment] if it not already showing.
         *
         * The fragment will wait some time to be dismissed before it is showed.
         *
         * There will always be only one [ProgressDialogFragment]!
         *
         * @param manager fragment manager
         * @param fullscreen If `true`, the progress indicator will hide the content of a screen (default behavior).
         *
         * @return the dialog to show
         */
        @MainThread
        fun show(manager: FragmentManager, fullscreen: Boolean = DEFAULT_VALUE_FULLSCREEN, dimBackground: Boolean = true): ProgressDialogFragment {
            val fragment = ProgressDialogFragment().apply {
                arguments = bundleOf(KEY_FULLSCREEN to fullscreen, KEY_DIM_BACKGROUND to dimBackground)
            }
            fragment.isCancelable = false
            runnable = Runnable {
                runnable = null // whatever happen we want to release this reference to avoid any leaks
                if (manager.isStateSaved) return@Runnable
                val dialogFragment = manager.findFragmentByTag(TAG) as? ProgressDialogFragment
                if (dialogFragment?.dialog?.isShowing == true) return@Runnable
                fragment.showNow(manager, TAG)
            }

            if (!handler.postDelayed(requireNotNull(runnable), SHOW_TIMEOUT)) {
                // release the reference as it will not be needed anymore
                runnable = null
            }

            return fragment
        }

        /**
         * Restores a dialog from [fragmentManager] if it is present.
         *
         * @param fragmentManager fragment manager to look into
         *
         * @return a dialog if available
         */
        fun restore(fragmentManager: FragmentManager): ProgressDialogFragment? {
            return fragmentManager.findFragmentByTag(TAG) as? ProgressDialogFragment
        }

        /**
         * Dismisses a dialog.
         *
         * @param manager fragment manager
         */
        @MainThread
        fun dismiss(manager: FragmentManager) {
            runnable?.also { handler.removeCallbacks(it) }
            runnable = null

            (manager.findFragmentByTag(TAG) as? ProgressDialogFragment)?.dismissAllowingStateLoss()
        }
    }
}
