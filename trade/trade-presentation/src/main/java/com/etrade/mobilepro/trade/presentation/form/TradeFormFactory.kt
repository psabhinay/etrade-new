package com.etrade.mobilepro.trade.presentation.form

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter

interface TradeFormFactory {

    suspend fun createTradeForm(
        accounts: List<TradeAccount>,
        accountDelegate: SelectedAccountDelegate,
        parameter: CreateTradeFormParameter
    ): ETResult<LiveData<Form>>
}
