package com.etrade.mobilepro.orders.viewmodel

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.recyclerview.widget.RecyclerView
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.R
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.orders.adapter.OrderFilterItem
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.list.api.DefaultOrderItem
import com.etrade.mobilepro.orders.list.api.OrderItem
import com.etrade.mobilepro.orders.list.api.OrdersRepo
import com.etrade.mobilepro.orders.list.paging.OrderListPagingSource
import com.etrade.mobilepro.orders.list.paging.OrdersPagingSettings
import com.etrade.mobilepro.preferences.preference
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.recyclerviewutil.SelectionTracker
import kotlinx.coroutines.flow.Flow
import org.threeten.bp.LocalDate
import javax.inject.Inject

private const val NUMBER_OF_MONTH_DIFF = 3L

/**
 * View model for `OrdersFragment`.
 */
class OrdersViewModel @Inject constructor(
    private val cachedOrdersRepo: OrdersRepo,
    val selectionTracker: SelectionTracker,
    private val resources: Resources,
    private val helper: OrdersHelper,
    keyValueStorage: KeyValueStorage
) : ViewModel() {

    val isDualPane: Boolean
        get() = resources.getBoolean(R.bool.is_dual_pane)

    private val _selectedStatus = MutableLiveData<OrderStatus>()
    private val _viewState = MutableLiveData<ViewState>()
    private val _refreshItemAtIndexesEvent = MutableLiveData<ConsumableLiveEvent<List<Int>>>()
    val selectedStatus: LiveData<OrderStatus>
        get() = _selectedStatus
    val viewState: LiveData<ViewState>
        get() = _viewState
    val refreshItemAtIndexesEvent: LiveData<ConsumableLiveEvent<List<Int>>>
        get() = _refreshItemAtIndexesEvent
    var ordersFlow: Flow<PagingData<OrderItem>>? = null
        private set

    private val _showStartDate = MutableLiveData<Boolean>()
    val showStartDate: LiveData<Boolean> = _showStartDate
    private val _showEndDate = MutableLiveData<Boolean>()
    val showEndDate: LiveData<Boolean> = _showEndDate

    private val uiStatusMap = getUiStatusMap()

    private var persistedOrderLastSelectedStatus: String by keyValueStorage.preference(
        key = "persistedOrderLastSelectedStatus",
        default = OrderStatus.OPEN.toString()
    )

    internal var lastSelectedStatus: OrderStatus
        set(value) {
            persistedOrderLastSelectedStatus = value.toString()
        }
        get() = OrderStatus.valueOf(persistedOrderLastSelectedStatus)

    private var persistedOrderToDateEpochDay: Int by keyValueStorage.preference(
        key = "persistedOrderToDateEpochDay",
        default = LocalDate.now().toEpochDay().toInt()
    ) // safe for EpochDay

    internal var toDate: LocalDate
        set(value) {
            persistedOrderToDateEpochDay = value.toEpochDay().toInt()
        }
        get() = LocalDate.ofEpochDay(persistedOrderToDateEpochDay.toLong())

    private var persistedOrderFromDateEpochDay: Int by keyValueStorage.preference(
        key = "persistedOrderFromDateEpochDay",
        default = toDate.minusMonths(NUMBER_OF_MONTH_DIFF).toEpochDay().toInt()
    )

    internal var fromDate: LocalDate
        set(value) {
            persistedOrderFromDateEpochDay = value.toEpochDay().toInt()
        }
        get() = LocalDate.ofEpochDay(persistedOrderFromDateEpochDay.toLong())

    internal var shouldRefresh = true

    init {
        this._selectedStatus.value = lastSelectedStatus
        selectionTracker.selectionChangedListener = { oldPos, newPos ->
            _refreshItemAtIndexesEvent.postValue(ConsumableLiveEvent(listOf(oldPos, newPos)))
        }
    }

    fun fetchOrders(
        accountId: String,
        forceRefresh: Boolean
    ): Flow<PagingData<OrderItem>> {
        resetSelectedPositionAndItem()
        val params = helper.buildOrdersParams(
            accountId,
            lastSelectedStatus,
            fromDate,
            toDate,
            forceRefresh
        )
        val pagingSettings = OrdersPagingSettings.getOrdersPageSetting(lastSelectedStatus)
        val pagedListConfig = PagingConfig(
            enablePlaceholders = false,
            initialLoadSize = pagingSettings.initialLoadCount,
            prefetchDistance = pagingSettings.prefetchDistance,
            pageSize = pagingSettings.pageSize
        )

        val ordersFlow = Pager(pagedListConfig) {
            OrderListPagingSource(
                cachedOrdersRepo,
                params
            )
        }.flow.cachedIn(viewModelScope)

        this.ordersFlow = ordersFlow

        return ordersFlow
    }

    fun getSelectedStatusFilterIndex(): Int? {
        return uiStatusMap.keys.indexOf(_selectedStatus.value)
    }

    fun setSelectedStatusFilter(selected: OrderStatus) {
        _selectedStatus.value = selected
    }

    private fun resetSelectedPositionAndItem() {
        selectionTracker.itemWasSelectedAt(RecyclerView.NO_POSITION)
        helper.selectedOrderItem = null
    }

    fun getStatusList(): List<OrderFilterItem> {
        return uiStatusMap.values.toList()
    }

    fun getLabelForSelectedStatus(status: OrderStatus): String {
        return uiStatusMap[status]?.label ?: resources.getString(OrderStatus.UNKNOWN.getLabel())
    }

    fun isFirstItemSelected(): Boolean {
        return selectionTracker.current == 0
    }

    fun setSelectedItemPositionInDualPane(position: Int) {
        if (isDualPane) {
            selectionTracker.itemWasSelectedAt(position)
        }
    }

    fun isPositionSelected(position: Int): Boolean {
        return selectionTracker.current == position && isDualPane
    }

    fun getOrderDetailArgument(accountId: String): OrderDetailsArgument? =
        helper.buildOrderDetailArgument(accountId)

    sealed class ViewState {
        object Loading : ViewState()
        data class Error(val message: String? = "Something went wrong!") : ViewState()
        data class Success(val orders: List<DefaultOrderItem>) : ViewState()
    }

    private fun getUiStatusMap(): Map<OrderStatus, OrderFilterItem> {
        if (helper.isEmployeeStockPlan) {
            return mapOf(
                OrderStatus.OPEN to OrderFilterItem(resources.getString(OrderStatus.OPEN.getLabel()), OrderStatus.OPEN),
                OrderStatus.OPEN_AND_UNSETTLED to OrderFilterItem(
                    resources.getString(OrderStatus.OPEN_AND_UNSETTLED.getLabel()),
                    OrderStatus.OPEN_AND_UNSETTLED
                ),
                OrderStatus.EXECUTED to OrderFilterItem(resources.getString(OrderStatus.EXECUTED.getLabel()), OrderStatus.EXECUTED),
                OrderStatus.CANCELLED to OrderFilterItem(resources.getString(OrderStatus.CANCELLED.getLabel()), OrderStatus.CANCELLED)
            )
        } else {
            return mapOf(
                OrderStatus.ALL_BY_DATE to OrderFilterItem(resources.getString(OrderStatus.ALL_BY_DATE.getLabel()), OrderStatus.ALL_BY_DATE),
                OrderStatus.ALL_BY_SYMBOL to OrderFilterItem(resources.getString(OrderStatus.ALL_BY_SYMBOL.getLabel()), OrderStatus.ALL_BY_SYMBOL),
                OrderStatus.OPEN to OrderFilterItem(resources.getString(OrderStatus.OPEN.getLabel()), OrderStatus.OPEN),
                OrderStatus.SAVED to OrderFilterItem(resources.getString(OrderStatus.SAVED.getLabel()), OrderStatus.SAVED),
                OrderStatus.EXECUTED to OrderFilterItem(resources.getString(OrderStatus.EXECUTED.getLabel()), OrderStatus.EXECUTED),
                OrderStatus.CANCELLED to OrderFilterItem(resources.getString(OrderStatus.CANCELLED.getLabel()), OrderStatus.CANCELLED),
                OrderStatus.EXPIRED to OrderFilterItem(resources.getString(OrderStatus.EXPIRED.getLabel()), OrderStatus.EXPIRED),
                OrderStatus.REJECTED to OrderFilterItem(resources.getString(OrderStatus.REJECTED.getLabel()), OrderStatus.REJECTED)
            )
        }
    }

    fun setSelectedOrderItem(selectedItem: OrderItem) {
        helper.selectedOrderItem = selectedItem
    }

    fun setShowStartDate(shouldShow: Boolean = true) {
        _showStartDate.value = shouldShow
    }

    fun setShowEndDate(shouldShow: Boolean = true) {
        _showEndDate.value = shouldShow
    }
}
