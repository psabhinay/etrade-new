package com.etrade.mobilepro.chat.api

import java.io.Serializable

data class ChatParamsRequestModel(var value: ChatParamsDataModel) : Serializable

data class ChatParamsDataModel(
    var metadata: ChatMetadataModel?,
    var productID: String? = null,
    var cid: String? = null,
    var lastName: String? = null,
    var isStockPlan: String? = null,
    var isOH: String? = null,
    var isBusinessHrs: String? = null,
    var firstName: String? = null,
    var contactID: String? = null,
    var chatSource: String? = "ET_Mobile_Android_phone_Secure_Message",
    var chatInfo: String? = null,
    var acctInstName: String? = null,
    var accountNo: String? = null,
    var accountHolderTier: String? = null,
    val chatGeoLoc: ChatGeolocationModel? = null
) : Serializable

data class ChatGeolocationModel(
    var city: String? = null,
    var cityCode: String? = null,
    var connectionSpeed: String? = null,
    var continentCode: String? = null,
    var country: String? = null,
    var countryCode: String? = null,
    var domain: String? = null,
    var ipAddress: String? = null,
    var isp: String? = null,
    var latitude: String? = null,
    var longitude: String? = null,
    var metroCode: String? = null,
    var postalCode: String? = null,
    var proxyType: String? = null,
    var region: String? = null,
    var regionCode: String? = null,
    var twoLetterCountry: String? = null
) : Serializable
