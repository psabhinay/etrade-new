package com.etrade.mobilepro.instrument.dao

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.instrument.Instrument

/**
 * Data access object for instruments.
 */
interface InstrumentDao {

    /**
     * Gets list of instruments to specified [symbols].
     *
     * @param symbols collection of symbols
     *
     * @return map of symbols to instruments
     */
    fun getInstrumentsAsLiveData(symbols: Collection<String>): LiveData<List<Instrument>>

    fun getInstruments(symbols: Collection<String>): List<Instrument>

    fun getInstrumentsNonAsync(symbols: Collection<String>): List<Instrument>

    fun updateInstruments(instruments: List<Instrument>)
}
