package com.etrade.mobilepro.screener.data.result

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.eo.core.util.toBigDecimalOrZero
import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.screener.api.result.MorningstarRatingResult
import com.etrade.mobilepro.screener.api.result.PrescreenedQuote
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.domain.data.deriveDayChange
import com.etrade.mobilepro.util.domain.data.deriveDayChangePercent
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.formatters.formatMarketPercentDataCustom
import com.etrade.mobilepro.util.safeParseBigDecimal
import java.math.BigDecimal

sealed class ResultColumn(val storageKey: String) {
    object LastPrice : ResultColumn("LastPrice") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val last = entry.quote.quotePrice.lastPrice.safeParseBigDecimal()
            return ResultDataCell(
                value = last ?: BigDecimal.ZERO,
                displayText = MarketDataFormatter.formatMarketDataCustom(last),
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol
            )
        }

        override fun updateCell(dataCell: ResultDataCell, data: Level1Data, dataColorResolver: DataColorResolver): ResultDataCell {
            val last = data.lastPrice?.safeParseBigDecimal() ?: return super.updateCell(dataCell, data, dataColorResolver)
            return ResultDataCell(
                value = last,
                displayText = MarketDataFormatter.formatMarketDataCustom(last),
                instrumentType = dataCell.instrumentType,
                symbol = dataCell.symbol
            )
        }
    }

    object ChangeDollar : ResultColumn("ChangeDollar") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val nonExtendedHoursChange = entry.quote.quotePrice.change
            val nonExtendedHoursChangeBigDecimal = nonExtendedHoursChange.toBigDecimalOrZero()
            val extendedHourChange = entry.quote.extendedQuoteHrDetail?.change ?: BigDecimal.ZERO.toString()
            val dayChange = deriveDayChange(extendedHourChange, nonExtendedHoursChange).toBigDecimalOrZero()
            return ResultDataCell(
                value = dayChange,
                displayText = MarketDataFormatter.formatMarketDataCustom(nonExtendedHoursChangeBigDecimal),
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                dataColor = dataColorResolver.resolveDataColor(nonExtendedHoursChangeBigDecimal)
            )
        }

        override fun updateCell(dataCell: ResultDataCell, data: Level1Data, dataColorResolver: DataColorResolver): ResultDataCell {
            data as StockData
            val change = data.dayChange?.safeParseBigDecimal() ?: return super.updateCell(dataCell, data, dataColorResolver)
            return ResultDataCell(
                value = change,
                displayText = MarketDataFormatter.formatMarketDataCustom(change),
                instrumentType = dataCell.instrumentType,
                symbol = dataCell.symbol,
                dataColor = dataColorResolver.resolveDataColor(change)
            )
        }
    }

    object ChangePercent : ResultColumn("ChangePercent") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val intradayPercentChange = entry.quote.quotePrice.percentChange.toBigDecimalOrZero()
            val nonExtendedHoursChange = entry.quote.quotePrice.change
            val extendedHourChange = entry.quote.extendedQuoteHrDetail?.change ?: BigDecimal.ZERO.toString()
            val dayChange = deriveDayChange(extendedHourChange, nonExtendedHoursChange)
            val closingPrice = entry.quote.quotePrice.lastPrice
            val dayChangePercent = deriveDayChangePercent(dayChange, closingPrice, nonExtendedHoursChange).toBigDecimalOrZero()
            return ResultDataCell(
                displayText = MarketDataFormatter.formatMarketPercentDataCustom(intradayPercentChange),
                value = dayChangePercent,
                dataColor = dataColorResolver.resolveDataColor(intradayPercentChange),
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol
            )
        }

        override fun updateCell(dataCell: ResultDataCell, data: Level1Data, dataColorResolver: DataColorResolver): ResultDataCell {
            data as StockData
            val changePercent = data.dayChangePercent?.safeParseBigDecimal() ?: return super.updateCell(dataCell, data, dataColorResolver)
            return ResultDataCell(
                value = changePercent,
                displayText = MarketDataFormatter.formatMarketPercentDataCustom(changePercent),
                instrumentType = dataCell.instrumentType,
                symbol = dataCell.symbol,
                dataColor = dataColorResolver.resolveDataColor(changePercent)
            )
        }
    }

    object Volume : ResultColumn("Volume") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val volume = entry.quote.quotePrice.volume.safeParseBigDecimal()
            return ResultDataCell(
                displayText = MarketDataFormatter.formatVolumeTwoDecimals(volume),
                value = volume ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol
            )
        }

        override fun updateCell(dataCell: ResultDataCell, data: Level1Data, dataColorResolver: DataColorResolver): ResultDataCell {
            val volume = data.volume?.safeParseBigDecimal() ?: return super.updateCell(dataCell, data, dataColorResolver)
            return ResultDataCell(
                value = volume,
                displayText = MarketDataFormatter.formatVolumeTwoDecimals(volume),
                instrumentType = dataCell.instrumentType,
                symbol = dataCell.symbol
            )
        }
    }

    object MarketCap : ResultColumn("MarketCap") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val marketCap = entry.quote.marketCap.safeParseBigDecimal()
            return ResultDataCell(
                displayText = MarketDataFormatter.formatVolumeTwoDecimals(marketCap),
                value = marketCap ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol
            )
        }
    }

    object Description : ResultColumn("Description") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val description = entry.prescreenData?.fundName ?: ""
            return ResultDataCell(
                displayText = description,
                value = description,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                hasEllipsize = true
            )
        }
    }

    object EtfDescription : ResultColumn("EtfDescription") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val description = entry.quote.description
            return ResultDataCell(
                displayText = description,
                value = description,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                hasEllipsize = true
            )
        }
    }

    object MorningStarRating : ResultColumn("MorningStarRating") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val morningstarRatingResult = MorningstarRatingResult(
                starsCount = entry.prescreenData?.morningstarRatingSinceIncep?.toIntOrNull() ?: 1,
                fundsCount = entry.prescreenData?.morningstarRatingOutOfFunds3Year?.toIntOrNull() ?: 0
            )
            return object : ResultDataCell(
                displayText = "",
                value = morningstarRatingResult,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol
            ) {
                override fun getContent(): Any = (value as MorningstarRatingResult).starsCount
            }
        }
    }

    object FundCategory : ResultColumn("FundCategory") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            return ResultDataCell(
                displayText = entry.prescreenData?.fundCategory ?: "",
                value = entry.prescreenData?.fundCategory ?: "",
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                hasEllipsize = true
            )
        }
    }

    object TotalAssets : ResultColumn("TotalAssets") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val totalAsset = entry.prescreenData?.totalAssets?.removeGarbageChars()?.safeParseBigDecimal()
            return ResultDataCell(
                displayText = MarketDataFormatter.formatVolumeTwoDecimals(totalAsset),
                value = totalAsset ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol
            )
        }
    }

    object ExpenseRatio : ResultColumn("ExpenseRatio") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val expenseRatio = entry.prescreenData?.expenseRatio?.safeParseBigDecimal()
            return ResultDataCell(
                value = expenseRatio ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(expenseRatio)
            )
        }
    }

    object ThreeYearsAlpha : ResultColumn("ThreeYearsAlpha") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val threeYearAlpha = entry.prescreenData?.alpha?.safeParseBigDecimal()
            return ResultDataCell(
                value = threeYearAlpha ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(threeYearAlpha)
            )
        }
    }

    object ThreeYearsBeta : ResultColumn("ThreeYearsBeta") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val threeYearBeta = entry.prescreenData?.beta?.safeParseBigDecimal()
            return ResultDataCell(
                value = threeYearBeta ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(threeYearBeta)
            )
        }
    }

    object ThreeYearsSharpeRatio : ResultColumn("ThreeYearsSharpeRatio") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val threeYearSharpeRatio = entry.prescreenData?.sharpeRatio?.safeParseBigDecimal()
            return ResultDataCell(
                value = threeYearSharpeRatio ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(threeYearSharpeRatio)
            )
        }
    }

    object ThreeYearsRSquare : ResultColumn("ThreeYearsRSquare") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val threeYearRSquare = entry.prescreenData?.rSquared?.safeParseBigDecimal()
            return ResultDataCell(
                value = threeYearRSquare ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(threeYearRSquare)
            )
        }
    }

    object Price4WeeksSP500 : ResultColumn("Price4WeeksSP500") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val price4WeeksSP500 = entry.prescreenData?.price4WeeksSP500?.safeParseBigDecimal()
            return ResultDataCell(
                value = price4WeeksSP500 ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(price4WeeksSP500)
            )
        }
    }

    object Price13WeeksSP500 : ResultColumn("Price13WeeksSP500") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val price13WeeksSP500 = entry.prescreenData?.price13WeeksSP500?.safeParseBigDecimal()
            return ResultDataCell(
                value = price13WeeksSP500 ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(price13WeeksSP500)
            )
        }
    }

    object Price52WeeksSP500 : ResultColumn("Price52WeeksSP500") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val price52WeeksSP500 = entry.prescreenData?.price52WeeksSP500?.safeParseBigDecimal()
            return ResultDataCell(
                value = price52WeeksSP500 ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(price52WeeksSP500)
            )
        }
    }

    object Price4WeeksIndustry : ResultColumn("Price4WeeksIndustry") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val price4WeeksIndustry = entry.prescreenData?.price4WeeksIndustry?.safeParseBigDecimal()
            return ResultDataCell(
                value = price4WeeksIndustry ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(price4WeeksIndustry)
            )
        }
    }

    object Price13WeeksIndustry : ResultColumn("Price13WeeksIndustry") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val price13WeeksIndustry = entry.prescreenData?.price13WeeksIndustry?.safeParseBigDecimal()
            return ResultDataCell(
                value = price13WeeksIndustry ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(price13WeeksIndustry)
            )
        }
    }

    object Price52WeeksIndustry : ResultColumn("Price52WeeksIndustry") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val price52WeeksIndustry = entry.prescreenData?.price52WeeksIndustry?.safeParseBigDecimal()
            return ResultDataCell(
                value = price52WeeksIndustry ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(price52WeeksIndustry)
            )
        }
    }

    object PeRatioTrailing12 : ResultColumn("PeRatioTrailing12") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val peRatioTrailing12 = entry.prescreenData?.peRatioTrailing12?.safeParseBigDecimal()
            return ResultDataCell(
                value = peRatioTrailing12 ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(peRatioTrailing12)
            )
        }
    }

    object PeRatioCurrentYear : ResultColumn("PeRatioCurrentYear") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val peRatioCurrentYear = entry.prescreenData?.peRatioCurrentYear?.safeParseBigDecimal()
            return ResultDataCell(
                value = peRatioCurrentYear ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(peRatioCurrentYear)
            )
        }
    }

    object PeRatioNextYear : ResultColumn("PeRatioNextYear") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val peRatioNextYear = entry.prescreenData?.peRatioNextYear?.safeParseBigDecimal()
            return ResultDataCell(
                value = peRatioNextYear ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(peRatioNextYear)
            )
        }
    }

    object PriceToBook : ResultColumn("PriceToBook") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val priceToBook = entry.prescreenData?.priceToBook?.safeParseBigDecimal()
            return ResultDataCell(
                value = priceToBook ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(priceToBook)
            )
        }
    }

    object EpsGrowthAnnualCurrent : ResultColumn("EpsGrowthAnnualCurrent") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val epsGrowthAnnualCurrent = entry.prescreenData?.epsGrowthAnnualCurrent?.safeParseBigDecimal()
            return ResultDataCell(
                value = epsGrowthAnnualCurrent ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(epsGrowthAnnualCurrent)
            )
        }
    }

    object EpsGrowthAnnualNext : ResultColumn("EpsGrowthAnnualNext") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val epsGrowthAnnualNext = entry.prescreenData?.epsGrowthAnnualNext?.safeParseBigDecimal()
            return ResultDataCell(
                value = epsGrowthAnnualNext ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(epsGrowthAnnualNext)
            )
        }
    }

    object RevenueGrowthAnnualCurrent : ResultColumn("RevenueGrowthAnnualCurrent") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val revenueGrowthAnnualCurrent = entry.prescreenData?.revenueGrowthAnnualCurrent?.safeParseBigDecimal()
            return ResultDataCell(
                value = revenueGrowthAnnualCurrent ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(revenueGrowthAnnualCurrent)
            )
        }
    }

    object RevenueGrowthAnnualNext : ResultColumn("RevenueGrowthAnnualNext") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val revenueGrowthAnnualNext = entry.prescreenData?.revenueGrowthAnnualNext?.safeParseBigDecimal()
            return ResultDataCell(
                value = revenueGrowthAnnualNext ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(revenueGrowthAnnualNext)
            )
        }
    }

    object EpsGrowthQuarterly : ResultColumn("EpsGrowthQuarterly") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val epsGrowthQuarterly = entry.prescreenData?.epsGrowthQuarterly?.safeParseBigDecimal()
            return ResultDataCell(
                value = epsGrowthQuarterly ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(epsGrowthQuarterly)
            )
        }
    }

    object RevenueGrowthQuarterly : ResultColumn("RevenueGrowthQuarterly") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val revenueGrowthQuarterly = entry.prescreenData?.revenueGrowthQuarterly?.safeParseBigDecimal()
            return ResultDataCell(
                value = revenueGrowthQuarterly ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(revenueGrowthQuarterly)
            )
        }
    }

    object DividendYield : ResultColumn("DividendYield") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val dividendYield = entry.prescreenData?.dividendYield?.safeParseBigDecimal()
            return ResultDataCell(
                value = dividendYield ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = formatWithTwoOrFiveDecimals(dividendYield)
            )
        }
    }

    object PriceChange52WeeksLow : ResultColumn("PriceChange52WeeksLow") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val priceChange52WeeksLow = entry.prescreenData?.priceChange52WeeksLow?.safeParseBigDecimal()
            return ResultDataCell(
                value = priceChange52WeeksLow ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = formatWithTwoOrFiveDecimals(priceChange52WeeksLow)
            )
        }
    }

    object PriceChange52WeeksHigh : ResultColumn("PriceChange52WeeksHigh") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val priceChange52WeeksHigh = entry.prescreenData?.priceChange52WeeksHigh?.safeParseBigDecimal()
            return ResultDataCell(
                value = priceChange52WeeksHigh ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = formatWithTwoOrFiveDecimals(priceChange52WeeksHigh)
            )
        }
    }

    object MarketReturnCategory1Month : ResultColumn("MarketReturnCategory1Month") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val marketReturnCategory1Month = entry.prescreenData?.marketReturnCategory1Month?.safeParseBigDecimal()
            return ResultDataCell(
                value = marketReturnCategory1Month ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(marketReturnCategory1Month)
            )
        }
    }

    object MarketReturnCategory3Months : ResultColumn("MarketReturnCategory3Months") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val marketReturnCategory3Months = entry.prescreenData?.marketReturnCategory3Months?.safeParseBigDecimal()
            return ResultDataCell(
                value = marketReturnCategory3Months ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(marketReturnCategory3Months)
            )
        }
    }

    object MarketReturnCategory6Months : ResultColumn("MarketReturnCategory6Months") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val marketReturnCategory6Months = entry.prescreenData?.price52WeeksIndustry?.safeParseBigDecimal()
            return ResultDataCell(
                value = marketReturnCategory6Months ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(marketReturnCategory6Months)
            )
        }
    }

    object MarketReturnCategoryYTD : ResultColumn("MarketReturnCategoryYTD") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val marketReturnCategoryYTD = entry.prescreenData?.marketReturnCategoryYTD?.safeParseBigDecimal()
            return ResultDataCell(
                value = marketReturnCategoryYTD ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(marketReturnCategoryYTD)
            )
        }
    }

    object MarketReturnCategory1Year : ResultColumn("MarketReturnCategory1Year") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val marketReturnCategory1Year = entry.prescreenData?.marketReturnCategory1Year?.safeParseBigDecimal()
            return ResultDataCell(
                value = marketReturnCategory1Year ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(marketReturnCategory1Year)
            )
        }
    }

    object MarketReturnCategory3Years : ResultColumn("MarketReturnCategory3Years") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val marketReturnCategory3Years = entry.prescreenData?.marketReturnCategory3Years?.safeParseBigDecimal()
            return ResultDataCell(
                value = marketReturnCategory3Years ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(marketReturnCategory3Years)
            )
        }
    }

    object MarketReturnCategory5Years : ResultColumn("MarketReturnCategory5Years") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val marketReturnCategory5Years = entry.prescreenData?.marketReturnCategory5Years?.safeParseBigDecimal()
            return ResultDataCell(
                value = marketReturnCategory5Years ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(marketReturnCategory5Years)
            )
        }
    }

    object MarketReturnCategory10Years : ResultColumn("MarketReturnCategory10Years") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val marketReturnCategory10Years = entry.prescreenData?.marketReturnCategory10Years?.safeParseBigDecimal()
            return ResultDataCell(
                value = marketReturnCategory10Years ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(marketReturnCategory10Years)
            )
        }
    }

    object MarketReturnCategorySinceInception : ResultColumn("MarketReturnCategorySinceInception") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val marketReturnCategorySinceInception = entry.prescreenData?.marketReturnCategorySinceInception?.safeParseBigDecimal()
            return ResultDataCell(
                value = marketReturnCategorySinceInception ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(marketReturnCategorySinceInception)
            )
        }
    }

    object NavReturnCategory1Month : ResultColumn("NavReturnCategory1Month") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val navReturnCategory1Month = entry.prescreenData?.navReturnCategory1Month?.safeParseBigDecimal()
            return ResultDataCell(
                value = navReturnCategory1Month ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(navReturnCategory1Month)
            )
        }
    }

    object NavReturnCategory3Months : ResultColumn("NavReturnCategory3Months") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val navReturnCategory3Months = entry.prescreenData?.navReturnCategory3Months?.safeParseBigDecimal()
            return ResultDataCell(
                value = navReturnCategory3Months ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(navReturnCategory3Months)
            )
        }
    }

    object NavReturnCategory6Months : ResultColumn("NavReturnCategory6Months") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val navReturnCategory6Months = entry.prescreenData?.navReturnCategory6Months?.safeParseBigDecimal()
            return ResultDataCell(
                value = navReturnCategory6Months ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(navReturnCategory6Months)
            )
        }
    }

    object NavReturnCategoryYTD : ResultColumn("NavReturnCategoryYTD") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val navReturnCategoryYTD = entry.prescreenData?.navReturnCategoryYTD?.safeParseBigDecimal()
            return ResultDataCell(
                value = navReturnCategoryYTD ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(navReturnCategoryYTD)
            )
        }
    }

    object NavReturnCategory1Year : ResultColumn("NavReturnCategory1Year") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val navReturnCategory1Year = entry.prescreenData?.navReturnCategory1Year?.safeParseBigDecimal()
            return ResultDataCell(
                value = navReturnCategory1Year ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(navReturnCategory1Year)
            )
        }
    }

    object NavReturnCategory3Years : ResultColumn("NavReturnCategory3Years") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val navReturnCategory3Years = entry.prescreenData?.navReturnCategory3Years?.safeParseBigDecimal()
            return ResultDataCell(
                value = navReturnCategory3Years ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(navReturnCategory3Years)
            )
        }
    }

    object NavReturnCategory5Years : ResultColumn("NavReturnCategory5Years") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val navReturnCategory5Years = entry.prescreenData?.navReturnCategory5Years?.safeParseBigDecimal()
            return ResultDataCell(
                value = navReturnCategory5Years ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(navReturnCategory5Years)
            )
        }
    }

    object NavReturnCategory10Years : ResultColumn("NavReturnCategory10Years") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val navReturnCategory10Years = entry.prescreenData?.navReturnCategory10Years?.safeParseBigDecimal()
            return ResultDataCell(
                value = navReturnCategory10Years ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(navReturnCategory10Years)
            )
        }
    }

    object NavReturnCategorySinceInception : ResultColumn("NavReturnCategorySinceInception") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val navReturnCategorySinceInception = entry.prescreenData?.navReturnCategorySinceInception?.safeParseBigDecimal()
            return ResultDataCell(
                value = navReturnCategorySinceInception ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(navReturnCategorySinceInception)
            )
        }
    }

    object StandardDeviation : ResultColumn("StandardDeviation") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val standardDeviation = entry.prescreenData?.standardDeviation?.safeParseBigDecimal()
            return ResultDataCell(
                value = standardDeviation ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketData(standardDeviation)
            )
        }
    }

    object Yield : ResultColumn("Yield") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val yieldValue = entry.prescreenData?.yieldValue?.removeGarbageChars()?.safeParseBigDecimal()
            return ResultDataCell(
                value = yieldValue ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(yieldValue)
            )
        }
    }

    object DistributionYield : ResultColumn("DistributionYield") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val distributionYield = entry.prescreenData?.distributionYield?.safeParseBigDecimal()
            return ResultDataCell(
                value = distributionYield ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(distributionYield)
            )
        }
    }

    object CategoryReturn1Year : ResultColumn("CategoryReturn1Year") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val categoryReturn1Year = entry.prescreenData?.categoryReturn1Year?.safeParseBigDecimal()
            return ResultDataCell(
                value = categoryReturn1Year ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(categoryReturn1Year)
            )
        }
    }

    object CategoryReturn5Years : ResultColumn("CategoryReturn5Years") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val categoryReturn5Years = entry.prescreenData?.categoryReturn5Years?.safeParseBigDecimal()
            return ResultDataCell(
                value = categoryReturn5Years ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(categoryReturn5Years)
            )
        }
    }

    object CategoryReturn10Years : ResultColumn("CategoryReturn10Years") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val categoryReturn10Years = entry.prescreenData?.categoryReturn10Years?.safeParseBigDecimal()
            return ResultDataCell(
                value = categoryReturn10Years ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(categoryReturn10Years)
            )
        }
    }

    object TurnOverRatio : ResultColumn("TurnOverRatio") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val turnOverRatio = entry.prescreenData?.turnOverRatio?.safeParseBigDecimal()
            return ResultDataCell(
                value = turnOverRatio ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(turnOverRatio)
            )
        }
    }

    object PortfolioConcentration : ResultColumn("PortfolioConcentration") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val portfolioConcentration = entry.prescreenData?.portfolioConcentration?.safeParseBigDecimal()
            return ResultDataCell(
                value = portfolioConcentration ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(portfolioConcentration)
            )
        }
    }

    object AverageMarketCap : ResultColumn("AverageMarketCap") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val averageMarketCap = entry.prescreenData?.averageMarketCap?.removeGarbageChars() ?: "--"
            return ResultDataCell(
                value = averageMarketCap,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = averageMarketCap
            )
        }
    }

    object Volume30DaysAvg : ResultColumn("Volume30DaysAvg") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val averageMarketCap = entry.prescreenData?.volume30daysAvg ?: "--"
            return ResultDataCell(
                value = averageMarketCap,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = averageMarketCap
            )
        }
    }

    object FrontLoad : ResultColumn("FrontLoad") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val frontLoad = entry.prescreenData?.frontLoad?.safeParseBigDecimal()
            return ResultDataCell(
                value = frontLoad ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(frontLoad)
            )
        }
    }

    object DeferLoad : ResultColumn("DeferLoad") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val deferredLoad = entry.prescreenData?.deferredLoad?.safeParseBigDecimal()
            return ResultDataCell(
                value = deferredLoad ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(deferredLoad)
            )
        }
    }

    object RedemptionFee : ResultColumn("RedemptionFee") {
        override fun createCell(
            entry: PrescreenedQuote,
            mfMapper: FundQuoteDetailItemsMapper,
            dataColorResolver: DataColorResolver
        ): ResultDataCell {
            val redemptionFee = entry.prescreenData?.redemptionFee?.safeParseBigDecimal()
            return ResultDataCell(
                value = redemptionFee ?: BigDecimal.ZERO,
                instrumentType = entry.quote.typeCode,
                symbol = entry.quote.symbol,
                displayText = MarketDataFormatter.formatMarketPercData(redemptionFee)
            )
        }
    }

    abstract fun createCell(
        entry: PrescreenedQuote,
        mfMapper: FundQuoteDetailItemsMapper,
        dataColorResolver: DataColorResolver
    ): ResultDataCell

    open fun updateCell(dataCell: ResultDataCell, data: Level1Data, dataColorResolver: DataColorResolver) = dataCell

    protected fun formatWithTwoOrFiveDecimals(value: BigDecimal?): String = when {
        value == null -> MarketDataFormatter.formatMoneyValue(value)
        value.abs() < BigDecimal.ONE -> MarketDataFormatter.formatNumberWithTwoToFiveDecimals(value)
        else -> MarketDataFormatter.formatMoneyValue(value)
    }
}

private val garbageChars = listOf("$", "%")
private fun String.removeGarbageChars(): String {
    var result = this
    garbageChars.forEach {
        result = result.replace(it, "")
    }
    return result
}
