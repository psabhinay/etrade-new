package com.etrade.mobilepro.trade.api

const val EMPTY_ACCOUNT_ID = ""

interface TradeAccount {
    val id: String
    val name: String
    val accountMode: AccountMode
    val approvalLevel: Int
}
