package com.etrade.mobilepro.landing.api

sealed class LandingPage {
    abstract val id: String
    abstract val title: String
    abstract val contentDescription: String
    abstract val subItems: List<LandingPage>?
    open val uuid: String = ""
    open val accountId: String? = null

    companion object {
        const val ALL_ACCOUNTS_ID = "landing_page_all_accounts_id"
        const val ALL_BROKERAGE_ID = "landing_page_all_brokerage_id"
        const val PORTFOLIO_ID = "landing_page_portfolio_id"
        const val WATCHLIST_ID = "landing_page_watchlist_id"
    }

    /**
     * Subitems are excluded from comparison
     */
    infix fun isSameAs(other: LandingPage?): Boolean {
        if (other == null || javaClass != other.javaClass) {
            return false
        }
        return uuid == other.uuid && id == other.id && title == other.title
    }

    fun hasNoneOrOneSubItem(): Boolean = subItems.isNullOrEmpty() || subItems?.size == 1

    data class AllAccounts(
        override val title: String
    ) : LandingPage() {
        override val id: String
            get() = ALL_ACCOUNTS_ID
        override val subItems: List<LandingPage>?
            get() = null
        override val contentDescription: String
            get() = title

        override fun toString(): String = title
    }

    data class AllBrokerage(
        override val title: String
    ) : LandingPage() {
        override val id: String
            get() = ALL_BROKERAGE_ID
        override val subItems: List<LandingPage>?
            get() = null
        override val contentDescription: String
            get() = title

        override fun toString(): String = title
    }

    data class Portfolio(
        override val uuid: String,
        override val title: String,
        override val contentDescription: String,
        override val accountId: String?,
        override val subItems: List<LandingPage>?
    ) : LandingPage() {
        override val id: String
            get() = PORTFOLIO_ID

        override fun toString(): String = title
    }

    data class Watchlist(
        override val title: String
    ) : LandingPage() {
        override val id: String
            get() = WATCHLIST_ID
        override val subItems: List<LandingPage>?
            get() = null
        override val contentDescription: String
            get() = title

        override fun toString(): String = title
    }

    class BottomInfoBox : LandingPage() {
        override val id: String
            get() = ""
        override val title: String
            get() = ""
        override val contentDescription: String
            get() = ""
        override val subItems: List<LandingPage>?
            get() = null
    }
}
