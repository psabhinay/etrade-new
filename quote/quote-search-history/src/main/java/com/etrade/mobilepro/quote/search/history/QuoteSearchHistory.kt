package com.etrade.mobilepro.quote.search.history

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.searchingapi.items.SearchResultItem

interface QuoteSearchHistory {
    val items: LiveData<List<SearchResultItem>?>
}
