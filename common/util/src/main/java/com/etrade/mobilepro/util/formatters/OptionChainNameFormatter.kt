package com.etrade.mobilepro.util.formatters

private const val DOLLAR_SEPARATOR = " $"
private const val SETTLEMENT_AM_LABEL = "AM"

fun <T : CharSequence> T.injectAMLabel(isAMOption: Boolean): String {
    return if (isAMOption) {
        split(DOLLAR_SEPARATOR)
            .mapIndexed { index: Int, name: String ->
                if (index == 0) {
                    "%s (%s)"
                } else {
                    "%s"
                }.format(name, SETTLEMENT_AM_LABEL)
            }.joinToString(DOLLAR_SEPARATOR)
    } else {
        toString()
    }
}
