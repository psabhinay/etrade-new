package com.etrade.mobilepro.inboxmessages.dynamicui.data.lowcash

import com.etrade.completeview.dto.AccountSummaryViewDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.MobileScreen
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.MessagePlace
import com.etrade.mobilepro.inboxmessages.MessageType
import com.etrade.mobilepro.inboxmessages.dynamicui.api.InboxMessagesWidgetPlace
import com.etrade.mobilepro.inboxmessages.dynamicui.data.WidgetListUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.data.extension.extractViewsAndAccounts
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import javax.inject.Inject

class OverviewScreenLowCashWidgetListUpdater @Inject constructor(
    private val lowCashPromptFactory: LowCashPromptFactory,
    private val inboxMessagesRepo: InboxMessagesRepository,
    private val darkModeChecker: DarkModeChecker
) : WidgetListUpdater {
    override val belongsToWidgetPlace: InboxMessagesWidgetPlace = InboxMessagesWidgetPlace.OVERVIEW

    override fun updateWithInboxMessages(input: MobileScreen?): List<BaseScreenView> {
        val (views, accounts) = input.extractViewsAndAccounts()
        val messages = inboxMessagesRepo.getMessagesBlocking(
            type = MessageType.CONTEXTUAL,
            place = MessagePlace.LOW_CASH_OVERVIEW_PORTFOLIO,
            userType = null
        )
        val resultingViews = mutableListOf<BaseScreenView>()
        views.forEach { view ->
            resultingViews.add(
                if (view is AccountSummaryViewDto) {
                    createLowCashPrompt(lowCashPromptFactory, view, accounts, messages, darkModeChecker.isInDarkMode())
                        ?.let { view.augmentWithLowCashPrompt(it) }
                        ?: view
                } else {
                    view
                }
            )
        }
        return resultingViews
    }
}
