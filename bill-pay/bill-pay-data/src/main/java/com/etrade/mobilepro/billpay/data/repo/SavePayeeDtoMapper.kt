package com.etrade.mobilepro.billpay.data.repo

import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.billpay.api.SavePayeeResult
import com.etrade.mobilepro.billpay.data.rest.SavePayeeResponseDto

internal fun ServerResponseDto<SavePayeeResponseDto>.toSavePayeeResult(): SavePayeeResult {
    when (val errorMessages = extractError()) {
        ServerError.NullResponse, ServerError.NoError -> {
            if (data?.successResult?.status != SavePayeeResult.Successful.code) {
                return SavePayeeResult.GenericError
            }

            return SavePayeeResult.Successful
        }
        is ServerError.Known -> {
            return buildKnownError(errorMessages)
        }
    }
}

private fun buildKnownError(errorMessages: ServerError): SavePayeeResult {
    val knownError = errorMessages as? ServerError.Known
    val errorCode = knownError?.messages?.firstOrNull()?.code
    return if (errorCode == SavePayeeResult.PayeeAlreadyAdded.code) {
        SavePayeeResult.PayeeAlreadyAdded
    } else {
        SavePayeeResult.GenericError
    }
}
