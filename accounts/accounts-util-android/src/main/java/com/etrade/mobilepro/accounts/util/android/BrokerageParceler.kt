package com.etrade.mobilepro.accounts.util.android

import android.os.Parcel
import androidx.core.os.ParcelCompat.readBoolean
import androidx.core.os.ParcelCompat.writeBoolean
import com.etrade.mobile.accounts.details.AccountDetails
import kotlinx.parcelize.Parceler
import java.math.BigDecimal

object BrokerageParceler : Parceler<AccountDetails.Brokerage> {

    override fun create(parcel: Parcel): AccountDetails.Brokerage = parcel.run {
        object : AccountDetails.Brokerage {
            override val cashAvailableForWithdrawal: BigDecimal? = readSerializable() as? BigDecimal
            override val marginAvailableForWithdrawal: BigDecimal? = readSerializable() as? BigDecimal
            override val purchasingPower: BigDecimal? = readSerializable() as? BigDecimal
            override val totalAvailableForWithdrawal: BigDecimal? = readSerializable() as? BigDecimal
            override val marginBalance: BigDecimal? = readSerializable() as? BigDecimal
            override val netCashBalance: BigDecimal? = readSerializable() as? BigDecimal
            override val cmAccount: Boolean? = readBoolean(parcel).let { isBooleanPresent ->
                if (isBooleanPresent) { readBoolean(parcel) } else { null }
            }
            override val contributionDetails: List<AccountDetails.ContributionDetails> = mutableListOf<AccountDetails.ContributionDetails>().apply {
                for (i in 0 until readInt()) {
                    add(ContributionDetailsParceler.create(parcel))
                }
            }
        }
    }

    override fun AccountDetails.Brokerage.write(parcel: Parcel, flags: Int) {
        parcel.apply {
            writeSerializable(cashAvailableForWithdrawal)
            writeSerializable(marginAvailableForWithdrawal)
            writeSerializable(purchasingPower)
            writeSerializable(totalAvailableForWithdrawal)
            writeSerializable(marginBalance)
            writeSerializable(netCashBalance)
            writeBoolean(parcel, cmAccount != null)
            cmAccount?.let { writeBoolean(parcel, it) }
            writeInt(contributionDetails.size)
            contributionDetails.forEach {
                ContributionDetailsParceler.run { it.write(parcel, flags) }
            }
        }
    }
}
