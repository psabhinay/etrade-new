package com.etrade.mobilepro.chat.data.setup.dto.contactId

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ChatItemDto(

    @Json(name = "id")
    var id: String? = null
)
