package com.etrade.mobilepro.trade.optionlegselect

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.dialog.showDialog
import com.etrade.mobilepro.dialog.viewmodel.AppDialogViewDelegate
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.dynamic.form.fragment.formValues
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.optionschain.tableview.OptionChainTableViewFragment
import com.etrade.mobilepro.optionschains.api.Option
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toSearchResultItem
import com.etrade.mobilepro.searchingapi.symbolFromSearchResult
import com.etrade.mobilepro.trade.api.TradeOptionInfoType
import com.etrade.mobilepro.trade.optionlegselect.adapter.OptionLegItemAdapter
import com.etrade.mobilepro.trade.optionlegselect.databinding.TradeFragmentOptionLegSelectBinding
import com.etrade.mobilepro.trade.presentation.util.SharedTradeViewModel
import com.etrade.mobilepro.trade.router.OptionTradeRouter
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetClicksViewModel
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetViewDelegate
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.accessibility.initTextInputAccessibility
import com.etrade.mobilepro.util.android.binding.dataBinding
import com.etrade.mobilepro.util.android.clicklisteners.OnViewDebounceClickListener
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.clearFocusSafely
import com.etrade.mobilepro.util.android.goneIf
import com.etrade.mobilepro.util.android.goneUnless
import com.etrade.mobilepro.util.android.navigation.hasDestinationInBackStack
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.AppMessageViewDelegate
import com.etrade.mobilepro.viewdelegate.BlockingLoadingIndicatorViewDelegate
import com.etrade.mobilepro.viewdelegate.LifecycleObserverViewDelegate
import com.etrade.mobilepro.viewdelegate.ViewDelegate
import com.squareup.moshi.Moshi
import javax.inject.Inject

private const val KEY_SYMBOL = "selectedSymbol"
private const val KEY_TRANSACTION_TYPE = "transactionType"
private const val KEY_WAIT_FOR_RESULT = "waitForResult"
private const val ORDERTYPE_BOTTOMSHEETSELECTOR_ACTIVITY_TAG = "orderTypeBottomSheetSelectorActivityTag"

@RequireLogin
class OptionLegSelectFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val tradeRouter: TradeRouter,
    private val moshi: Moshi,
    @StockTradeActionItemProvider private val stockTradeActionItemProvider: TradeActionItemProvider,

    private val optionTradeRouter: OptionTradeRouter
) : OptionChainTableViewFragment(
    R.layout.trade_fragment_option_leg_select,
    viewModelFactory = viewModelFactory,
    snackbarUtilFactory = snackbarUtilFactory,
    optionsChainDatePickerId = R.id.optionsChainDatePickerView,
    optionsChainSwipeId = R.id.optionsChainSwipeRefreshView,
    optionsChainTableId = R.id.optionsChainTableView
) {

    private val sharedTradeViewModel: SharedTradeViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }
    private val quoteWidgetViewModel: QuotesWidgetViewModel by viewModels { viewModelFactory }
    private val tradeLegBidAskViewModel: TradeLegBidAskViewModel by viewModels { viewModelFactory }
    private val viewModel: OptionLegSelectViewModel by viewModels { viewModelFactory }

    private val actionSheetClicksViewModel: ActionSheetClicksViewModel by viewModels { viewModelFactory }
    private val actionSheetViewDelegate: ActionSheetViewDelegate by lazy {
        ActionSheetViewDelegate(
            viewModel,
            actionSheetClicksViewModel,
            childFragmentManager
        )
    }

    private val orderTypeBottomSheetSelector: BottomSheetSelector<String> by lazy { BottomSheetSelector(childFragmentManager, resources) }
    private val viewDelegates: Iterable<ViewDelegate> by lazy {
        listOf(
            actionSheetViewDelegate,
            AppDialogViewDelegate(viewModel) { showDialog(it) },
            AppMessageViewDelegate(viewModel, snackBarUtil),
            BlockingLoadingIndicatorViewDelegate(viewModel, childFragmentManager),
            LifecycleObserverViewDelegate(listOf(quoteWidgetViewModel))
        )
    }

    private val optionLegItemAdapter: OptionLegItemAdapter = OptionLegItemAdapter(
        lifecycleOwner = this,
        quantityChangedListener = { index, quantity ->
            viewModel.updateQuantity(index, quantity)
        },
        itemDeleteListener = { index ->
            viewModel.deselectOption(index)
        },
        itemDeleteInteractionListener = { index ->
            viewModel.touchItemDelete(index)
        },
        itemDeletePendingListener = { index, deletePending ->
            viewModel.pendDeletion(index, deletePending)
        }
    )

    private val startQuoteLookup = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        isWaitingForActivityResult = false
        viewModel.resultSymbol(symbolFromSearchResult(result.resultCode, result.data))
    }

    private val binding by dataBinding(TradeFragmentOptionLegSelectBinding::bind)

    private var isWaitingForActivityResult: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionSheetViewDelegate.onCreate(savedInstanceState)
        viewModel.apply {
            attachTo(optionChainTableViewModel)
            attachTo(tradeLegBidAskViewModel)
        }
        tradeLegBidAskViewModel.quoteWidgetViewModel = quoteWidgetViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.disclosureView.setOnClickListener(
            OnViewDebounceClickListener(
                {
                    optionTradeRouter.openOptionDisclosures(findNavController())
                }
            )
        )
        binding.selectActionView.setOnClickListener {
            view.findFocus()?.clearFocusSafely()
            viewModel.invokeSelectAction()
        }
        binding.selectedOptionLegsView.apply {
            adapter = optionLegItemAdapter
        }

        setInitialData(savedInstanceState)
        observeViewModel()

        viewDelegates.forEach { it.observe(viewLifecycleOwner) }

        binding.quoteWidgetsViewModel = quoteWidgetViewModel
        binding.quoteBidAskItem = tradeLegBidAskViewModel.quoteBidAskItem

        setupStockLegViews()
    }

    private fun setInitialData(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            isWaitingForActivityResult = savedInstanceState.getBoolean(KEY_WAIT_FOR_RESULT)
            if (!isWaitingForActivityResult) {
                viewModel.injectSymbol(savedInstanceState.symbol)
            }
        } else {
            arguments?.run {
                viewModel.injectValues(requireNotNull(formValues(moshi)), symbol, transactionType)
                arguments = null
            }
        }
    }

    private fun observeViewModel() {
        viewModel.apply {
            finishSelectSignal.observe(::onFinishSelectSignal)
            openQuoteLookupPage.observe(::onOpenQuoteLookupPage)
            optionLegVisible.observe(::onOptionLegVisible)
            scrollUpSignal.observe(::onScrollUpSignal)
            selectActionEnabled.observe(::onSelectActionEnabled)
            selectedOptionLegs.observe(::onSelectedOptionLegs)
            selectedSymbol.observe(::onSelectedSymbol)
            selectedStockLegItem.observe(::onStockTradeLegUpdate)
            showStockLeg.observe(::onShowStockLeg)
        }
    }

    private fun onStockTradeLegUpdate(stockLegItem: StockLegItem) {
        binding.stockAndOptionsRow.apply {
            stockLegOrderTypeView.value = stockLegItem.orderType
            stockLegOrderTypeView.contentDescription = "${getString(R.string.trade_label_order_type)} ${stockLegItem.orderType}"
            if (stockLegQuantityView.text?.toString() != stockLegItem.quantity) {
                stockLegQuantityView.setText(stockLegItem.quantity)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(KEY_WAIT_FOR_RESULT, isWaitingForActivityResult)
        outState.putParcelable(KEY_SYMBOL, viewModel.selectedSymbol.value?.toSearchResultItem())
    }

    override fun onOptionClicked(option: Option) {
        viewModel.selectOption(option)
    }

    private fun initDropdown() {
        orderTypeBottomSheetSelector.apply {
            init(
                ORDERTYPE_BOTTOMSHEETSELECTOR_ACTIVITY_TAG,
                getString(R.string.order_type),
                stockTradeActionItemProvider.getActions().map { getString(it.displayNameId) },
                0,
                null,
                toolbarActionEnd = ToolbarActionEnd(
                    isIcon = true,
                    icon = R.drawable.ic_help,
                    contentDescription = resources.getString(R.string.help),
                    onClick = {
                        tradeRouter.openHelp(findNavController(), TradeOptionInfoType.ORDER_TYPE)
                    }
                )
            )
            setListener { position, _ ->
                val orderType = stockTradeActionItemProvider.getTransactionType(stockTradeActionItemProvider.getActions()[position].id)
                viewModel.updateStockOrderType(orderType)
            }
        }
    }

    private fun onFinishSelectSignal(event: ConsumableLiveEvent<InputValuesSnapshot>) {
        event.consume {
            findNavController().apply {
                if (this.hasDestinationInBackStack(optionTradeRouter.tradeFragmentId)) {
                    sharedTradeViewModel.injectFormInputValues(it)
                    sharedTradeViewModel.signalOptionsLegSelected()
                    popBackStack()
                } else {
                    optionTradeRouter.openTradeFromLegSelect(this, it)
                }
            }
        }
    }

    private fun onOpenQuoteLookupPage(event: ConsumableLiveEvent<Intent>) {
        event.consume {
            isWaitingForActivityResult = true
            startQuoteLookup.launch(it)
        }
    }

    private fun onOptionLegVisible(visible: Boolean) {
        binding.optionLegsContainer.goneIf(!visible)
        binding.quoteWidgetBidAsk.root.goneIf(!visible)
    }

    private fun onScrollUpSignal(event: ConsumableLiveEvent<Unit>) {
        event.consume {
            view?.scrollTo(0, 0)
        }
    }

    private fun onSelectActionEnabled(enabled: Boolean) {
        binding.selectActionView.isEnabled = enabled
    }

    private fun onSelectedOptionLegs(items: List<OptionLegItem>) {
        optionLegItemAdapter.items = items
    }

    private fun onSelectedSymbol(symbolInfo: WithSymbolInfo) {
        view?.visibility = View.VISIBLE

        val symbol = symbolInfo.symbol
        quoteWidgetViewModel.getQuotes(symbol, true)
        loadPage(symbol)
    }

    private fun onShowStockLeg(showStockLeg: Boolean) {
        binding.stockAndOptionsRow.root.goneUnless(showStockLeg)
    }

    private fun setupStockLegViews() {
        binding.stockAndOptionsRow.apply {
            stockLegQuantityView.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
                if (!hasFocus) {
                    viewModel.updateStockOrderQuantity(stockLegQuantityView.text.toString())
                }
            }
            stockLegOrderTypeView.setOnClickListener {
                orderTypeBottomSheetSelector.openDropDown()
            }
            stockLegQuantityLayout.initTextInputAccessibility {
                hintText = getString(R.string.quantity)
            }
        }
        initDropdown()
    }

    private inline fun <T> LiveData<T>.observe(crossinline onChange: (T) -> Unit) {
        observe(viewLifecycleOwner, { onChange(it) })
    }
}

private val Bundle.symbol: SearchResultItem.Symbol?
    get() = getParcelable(KEY_SYMBOL)

private val Bundle.transactionType: TransactionType?
    get() = getSerializable(KEY_TRANSACTION_TYPE) as TransactionType?
