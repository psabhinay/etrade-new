package com.etrade.mobilepro.trade.presentation.form.delegate.dependencies

import android.content.res.Resources
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.shared.WithStopLossInputId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.presentation.R

internal class StopLossDelegateDependencies<T>(
    inputId: T,
    resources: Resources
) : WithLimitOrderTermDependencies, WithSaveAndPreviewDependencies
        where T : WithAdvancedTradeFormInputId,
              T : WithStopLossInputId,
              T : WithPriceTradeFormInputId,
              T : WithSymbolTradeFormInputId {

    private val termValueId: OrderTermValueId = inputId.termValueId

    override val saveAndPreviewActionRequiredInputs = setOf(
        inputId.accountId,
        inputId.securityType,
        inputId.symbol,
        inputId.priceType,
        inputId.term,
        inputId.limitPrice,
        inputId.limitPriceForStopLimit,
        inputId.stopPrice,
        inputId.stopPriceForStopLimit,
        inputId.advancedOrderType,
        inputId.stopLossInputId.quantity,
        inputId.stopLossInputId.priceType,
        inputId.stopLossInputId.limitPriceForStopLimit,
        inputId.stopLossInputId.stopPrice,
        inputId.stopLossInputId.stopPriceForStopLimit,
        inputId.stopLossInputId.term
    )

    override val limitOrderTermLabelMap = mapOf(
        termValueId.goodUntilCancel to resources.getString(R.string.trade_term_gtc),
        termValueId.fillOrKill to resources.getString(R.string.trade_term_fok),
        termValueId.immediateOrCancel to resources.getString(R.string.trade_term_ioc)
    )
}
