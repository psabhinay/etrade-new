package com.etrade.mobilepro.tableview.controlbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\bf\u0018\u00002\u00020\u0001J\b\u0010\u0012\u001a\u00020\u0013H&J\b\u0010\u0014\u001a\u00020\u0013H\u0016J\u0018\u0010\u0014\u001a\u00020\u00132\u000e\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u0016H&J\u0010\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0010H&J\u0012\u0010\u0019\u001a\u00020\u00132\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004H&R\u001a\u0010\u0002\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\t\u0010\u0006R\u0012\u0010\n\u001a\u00020\bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0012\u0010\r\u001a\u00020\bX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u000e\u0010\fR\u0018\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0011\u0010\u0006\u00a8\u0006\u001b"}, d2 = {"Lcom/etrade/mobilepro/tableview/controlbar/TableViewControlBarViewModel;", "", "dropdownSelectedText", "Landroidx/lifecycle/LiveData;", "", "getDropdownSelectedText", "()Landroidx/lifecycle/LiveData;", "enableCustomizeSettings", "", "getEnableCustomizeSettings", "enableDropDown", "getEnableDropDown", "()Z", "enableTableTypeSelection", "getEnableTableTypeSelection", "tableTypeSelectedIndex", "", "getTableTypeSelectedIndex", "onCustomizeViewClick", "", "onDropDownClick", "onUnAuthFilterClickAction", "Lkotlin/Function0;", "onTableTypeChange", "index", "setDropdownSelectedText", "selectedText", "tableview-controlbar_debug"})
public abstract interface TableViewControlBarViewModel {
    
    public abstract boolean getEnableDropDown();
    
    public abstract boolean getEnableTableTypeSelection();
    
    @org.jetbrains.annotations.NotNull()
    public abstract androidx.lifecycle.LiveData<java.lang.String> getDropdownSelectedText();
    
    @org.jetbrains.annotations.NotNull()
    public abstract androidx.lifecycle.LiveData<java.lang.Integer> getTableTypeSelectedIndex();
    
    @org.jetbrains.annotations.NotNull()
    public abstract androidx.lifecycle.LiveData<java.lang.Boolean> getEnableCustomizeSettings();
    
    public abstract void onCustomizeViewClick();
    
    public abstract void onDropDownClick(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function0<kotlin.Unit> onUnAuthFilterClickAction);
    
    public abstract void onDropDownClick();
    
    public abstract void onTableTypeChange(int index);
    
    public abstract void setDropdownSelectedText(@org.jetbrains.annotations.Nullable()
    java.lang.String selectedText);
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 3)
    public final class DefaultImpls {
        
        public static void onDropDownClick(@org.jetbrains.annotations.NotNull()
        com.etrade.mobilepro.tableview.controlbar.TableViewControlBarViewModel $this) {
        }
    }
}