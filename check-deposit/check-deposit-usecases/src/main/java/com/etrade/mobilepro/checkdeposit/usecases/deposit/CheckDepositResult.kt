package com.etrade.mobilepro.checkdeposit.usecases.deposit

import java.math.BigDecimal

sealed class CheckDepositResult {

    data class Valid(
        val amount: BigDecimal,
        val workflowId: String,
        val accountNumber: String
    ) : CheckDepositResult()

    data class Invalid(
        val message: String? = "",
        val code: String? = "",
        val frontImageFieldToFlag: Boolean = false,
        val rearImageFieldToFlag: Boolean = false,
        val amountFieldToFlag: Boolean = false
    ) : CheckDepositResult()
}
