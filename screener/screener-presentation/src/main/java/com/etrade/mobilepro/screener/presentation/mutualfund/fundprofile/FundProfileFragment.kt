package com.etrade.mobilepro.screener.presentation.mutualfund.fundprofile

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.FundFamily
import com.etrade.mobilepro.screener.api.MutualFundGroup
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.mutualfund.FundProfileFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.FragmentFundProfileBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.toggleCheckedState
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.button.MaterialButtonToggleGroup
import javax.inject.Inject

private const val FUND_FAMILY_TAG = "fund family tag"
private const val FUND_GROUP_TAG = "fund group tag"
private const val FUND_CATEGORY_TAG = "fund category tag"

private const val MORNING_STAR_1 = 1
private const val MORNING_STAR_2 = 2
private const val MORNING_STAR_3 = 3
private const val MORNING_STAR_4 = 4
private const val MORNING_STAR_5 = 5

class FundProfileFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<FundProfileFilter, FundProfileViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {
    override val layoutRes: Int = R.layout.fragment_fund_profile
    override val viewModel by viewModels<FundProfileViewModel> { viewModelFactory }
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND
    private val binding by viewBinding(FragmentFundProfileBinding::bind) { onDestroyBinding() }

    private lateinit var fundFamilySelector: DropDownMultiSelector<FundFamily>
    private lateinit var fundGroupSelector: DropDownMultiSelector<MutualFundGroup>
    private lateinit var fundCategorySelector: DropDownMultiSelector<FundCategory>
    private val handler = Handler(Looper.getMainLooper())
    private val buttonGroupListener =
        MaterialButtonToggleGroup.OnButtonCheckedListener { _, checkedId, isChecked ->
            val ratingPair = when (checkedId) {
                binding.morningstarRating.button1.id -> Pair(MORNING_STAR_1, binding.morningstarRating.button1)
                binding.morningstarRating.button2.id -> Pair(MORNING_STAR_2, binding.morningstarRating.button2)
                binding.morningstarRating.button3.id -> Pair(MORNING_STAR_3, binding.morningstarRating.button3)
                binding.morningstarRating.button4.id -> Pair(MORNING_STAR_4, binding.morningstarRating.button4)
                binding.morningstarRating.button5.id -> Pair(MORNING_STAR_5, binding.morningstarRating.button5)
                else -> throw IllegalArgumentException("Unknown rating button")
            }
            ratingPair.second.toggleCheckedState(isChecked, handler)
            viewModel.morningStarRatingToggled(ratingPair.first, isChecked)
        }
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<FundProfileFilter, FundProfileViewModel.ViewState, Boolean>>
        get() = mapOf(
            binding.checkableIndex to FundCharacteristic.INDEX,
            binding.fundOfFunds to FundCharacteristic.FUND_OF_FUNDS,
            binding.societyResponsible to FundCharacteristic.SOCIETY_RESPONSIBLE,
            binding.lifeOfCycleFund to FundCharacteristic.LIFE_OF_CYCLE,
            binding.dividendStrategy to FundCharacteristic.DIVIDEND_STRATEGY,
            binding.enhancedIndex to FundCharacteristic.ENHANCED_INDEX,
            binding.diversified to FundCharacteristic.DIVERSIFIED,
            binding.activelyManaged to FundCharacteristic.ACTIVELY_MANAGED
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
    }

    private fun FragmentFundProfileBinding.onDestroyBinding() {
        morningstarRating.buttonGroup.removeOnButtonCheckedListener(buttonGroupListener)
    }

    override fun onDestroyView() {
        handler.removeCallbacksAndMessages(null)
        super.onDestroyView()
    }

    private fun setupFragment() {
        setupPopups()
        setupDropDownLists()
        setupDropDownListsClickListeners()
        setupFundSwitchListeners()
        binding.morningstarRating.buttonGroup.addOnButtonCheckedListener(buttonGroupListener)
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                updateFunds(viewState = it)
                updateMorningStar(viewState = it)
                updateCheckableButtons(viewState = it)
            }
        )
        viewModel.stageData.observe(
            viewLifecycleOwner,
            Observer {
                binding.fundCategory.selectionEnabled = it !is Resource.Loading
            }
        )
    }

    private fun updateFunds(viewState: FundProfileViewModel.ViewState) {
        with(binding) {
            fundFamily.selectedValue = when (viewState.filter.fundFamilies.size) {
                0 -> getString(R.string.fund_profile_fund_all_families)
                1 -> viewState.filter.fundFamilies.first().title
                else -> getString(R.string.multiple_items_selected)
            }
            fundGroup.selectedValue = viewState.filter.fundGroup.title
            fundCategory.selectedValue = when (viewState.filter.fundCategories.size) {
                0 -> getString(R.string.fund_profile_fund_all_categories)
                1 -> viewState.filter.fundCategories.first().title
                else -> getString(R.string.multiple_items_selected)
            }
        }
    }

    private fun updateMorningStar(viewState: FundProfileViewModel.ViewState) {
        binding.apply {
            morningstarRating.button1.toggleCheckedState(viewState.filter.morningStarRating.contains(MORNING_STAR_1), handler)
            morningstarRating.button2.toggleCheckedState(viewState.filter.morningStarRating.contains(MORNING_STAR_2), handler)
            morningstarRating.button3.toggleCheckedState(viewState.filter.morningStarRating.contains(MORNING_STAR_3), handler)
            morningstarRating.button4.toggleCheckedState(viewState.filter.morningStarRating.contains(MORNING_STAR_4), handler)
            morningstarRating.button5.toggleCheckedState(viewState.filter.morningStarRating.contains(MORNING_STAR_5), handler)
        }
    }

    private fun updateCheckableButtons(viewState: FundProfileViewModel.ViewState) {
        binding.apply {
            switchAllStarsFund.isChecked = viewState.filter.allStarsFund
            switchExcludeClosedFund.isChecked = viewState.filter.excludeClosedFunds
            checkableIndex.isChecked = viewState.filter.index
            fundOfFunds.isChecked = viewState.filter.fundOfFunds
            societyResponsible.isChecked = viewState.filter.societyResponsible
            lifeOfCycleFund.isChecked = viewState.filter.lifeOfCycleFund
            dividendStrategy.isChecked = viewState.filter.dividendStrategy
            enhancedIndex.isChecked = viewState.filter.enhancedIndex
            diversified.isChecked = viewState.filter.diversified
            activelyManaged.isChecked = viewState.filter.activelyManaged
        }
    }

    private fun setupPopups() {
        binding.apply {
            infoAllStarsFund.setOnClickListener {
                screenersRouter.actionShowPopup(findNavController(), getString(R.string.fund_profile_info_all_stars_fund))
            }
            infoMorningstarRating.setOnClickListener {
                screenersRouter.actionShowPopup(findNavController(), getString(R.string.fund_profile_info_morningstar_rating))
            }
        }
    }

    private fun setupDropDownListsClickListeners() {
        with(binding) {
            fundFamily.setOnClickListener {
                viewModel.backupFamiliesSelection()
                fundFamilySelector.open()
            }
            fundGroup.setOnClickListener {
                fundGroupSelector.open()
            }
            fundCategory.setOnClickListener {
                viewModel.backupCategoriesSelection()
                fundCategorySelector.open()
            }
        }
    }

    @Suppress("LongMethod") // already grouped
    private fun setupDropDownLists() {
        fundFamilySelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = FUND_FAMILY_TAG,
                multiSelectEnabled = true,
                title = getString(R.string.fund_profile_title_fund_family),
                itemsData = viewModel.fundFamilyData,
                selectListener = { _, selected ->
                    viewModel.fundFamilySelected(selected.value)
                },
                onCancelListener = { fallBackValues ->
                    viewModel.familySelectionCanceled(fallBackValues)
                },
                eventListener = { event, dropDownDialog ->
                    when (event) {
                        DropDownMultiSelector.Settings.Event.CLEAR_ALL -> viewModel.fundFamilyCleared()
                        DropDownMultiSelector.Settings.Event.SAVE -> dropDownDialog.get()?.dismiss()
                    }
                }
            ),
            fallBackValuesProvider = {
                viewModel.prevFamiliesSelection
            }
        )
        fundGroupSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = FUND_GROUP_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.fund_profile_title_fund_group),
                snackbarUtilFactory = snackbarUtilFactory,
                itemsData = viewModel.etfFundGroupData,
                selectListener = { _, selected ->
                    viewModel.fundGroupSelected(selected.value)
                },
                onCancelListener = {
                    viewModel.didSelectItem()
                },
                selectionFlow = DropDownMultiSelector.SelectionFlow(
                    willSelectListener = { pos, item ->
                        viewModel.willSelectItem(pos, item)
                    },
                    didSelectListener = { _, _ ->
                        viewModel.didSelectItem()
                    },
                    stageData = viewModel.stageData,
                    onRetry = { pos, item ->
                        viewModel.willSelectItem(pos, item)
                    }
                )
            )
        )
        fundCategorySelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = FUND_CATEGORY_TAG,
                multiSelectEnabled = true,
                title = getString(R.string.fund_profile_title_fund_category),
                itemsData = viewModel.fundCategoryData,
                selectListener = { _, selected ->
                    viewModel.fundCategorySelected(selected.value)
                },
                onCancelListener = { fallBackValues ->
                    viewModel.categorySelectionCancelled(fallBackValues)
                },
                eventListener = { event, dropDownDialog ->
                    when (event) {
                        DropDownMultiSelector.Settings.Event.CLEAR_ALL -> viewModel.fundCategoryCleared()
                        DropDownMultiSelector.Settings.Event.SAVE -> dropDownDialog.get()?.dismiss()
                    }
                }
            ),
            fallBackValuesProvider = {
                viewModel.prevCategorySelection
            }
        )
    }

    private fun setupFundSwitchListeners() {
        binding.apply {
            switchAllStarsFund.setOnCheckedChangeListener { _, isChecked ->
                viewModel.consumeEvent(FundSwitch.ALL_STAR, isChecked)
            }
            switchExcludeClosedFund.setOnCheckedChangeListener { _, isChecked ->
                viewModel.consumeEvent(FundSwitch.EXCLUDE_CLOSED, isChecked)
            }
        }
    }
}
