package com.etrade.mobilepro.ui_demo.dropdown

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dropdown.expandableselect.DropDownExpandableSelector
import com.etrade.mobilepro.dropdown.expandableselect.findInTree
import kotlin.random.Random

class DropDownViewModel: ViewModel() {
    private val _viewState = MutableLiveData<ViewState>().apply { value = ViewState(isSingleItemHidden = false) }
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    private val expandableDataSource = if (Random.nextBoolean()) { generateExpandableItems() } else { generateLongExpandableItems() }
    private val expandableSingleDataSource = generateExpandableItemsWithOnlyOneSubItem()
    internal val expandableItemsData: LiveData<List<DropDownExpandableSelector.Selectable<DummyItem>>> = Transformations.map(viewState) { viewState ->
        expandableDataSource.mapToSelectable(selectedValue = viewState.selectedItem)
    }

    internal val expandableSingleItemsData: LiveData<List<DropDownExpandableSelector.Selectable<DummyItem>>> = Transformations.map(viewState) { viewState ->
        expandableSingleDataSource.mapToSelectable(selectedValue = viewState.selectedItem)
    }

    internal fun expandableItemSelected(item: DummyItem?) {
        _viewState.value = _viewState.value?.copy(selectedItem = item)
    }

    internal fun singleItemSwitchChanged(shouldHide: Boolean) {
        _viewState.value = ViewState(isSingleItemHidden = shouldHide)
    }

    internal fun getFallBackValues(): Set<DummyItem>? {
        return expandableItemsData.value?.findInTree(comparator = { it.selected })?.let { setOf(it.value) }
    }

    internal fun getFallBackSingleValues(): Set<DummyItem>? {
        return expandableSingleItemsData.value?.findInTree(comparator = { it.selected })?.let { setOf(it.value) }
    }

    internal data class ViewState(
        val selectedItem: DummyItem? = null,
        val isSingleItemHidden: Boolean
    )


    private fun generateExpandableItems(): List<DummyItem> =
        listOf(
            DummyItem.ExpandableTextItem(
                data = 0,
                subItems = listOf(
                    DummyItem.DoubleItem(),
                    DummyItem.DoubleItem(),
                    DummyItem.SimpleTextItem(),
                    DummyItem.SimpleTextItem(),
                    DummyItem.ExpandableTextItem(
                        data = 777
                    )
                )
            ),
            DummyItem.DoubleItem()
        ) +
        (1..10).map {
            if (Random.nextInt(10) < 7) {
                DummyItem.SimpleTextItem()
            } else {
                DummyItem.ExpandableTextItem(data = it)
            }
        }

    private fun generateLongExpandableItems(): List<DummyItem> =
        listOf(
            DummyItem.DoubleItem(),
            DummyItem.DoubleItem(),
            DummyItem.ExpandableTextItem(
                data = 1,
                subItems = (0..30).map { DummyItem.SimpleTextItem(data = "1_${it}") }
            ),
            DummyItem.ExpandableTextItem(
                data = 2,
                subItems = (0..30).map { DummyItem.SimpleTextItem(data = "2_${it}") }
            ),
            DummyItem.DoubleItem(),
            DummyItem.DoubleItem()
        )

    private fun generateExpandableItemsWithOnlyOneSubItem(): List<DummyItem> =
        listOf(
            DummyItem.SimpleTextItem(),
            DummyItem.SingleExpandableTextItem(data = 0xDEADBEE),
            DummyItem.DoubleItem()
        )
}
