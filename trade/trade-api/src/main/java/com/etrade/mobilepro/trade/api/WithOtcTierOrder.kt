package com.etrade.mobilepro.trade.api

interface WithOtcTierOrder {
    val otcTier: String?
}
