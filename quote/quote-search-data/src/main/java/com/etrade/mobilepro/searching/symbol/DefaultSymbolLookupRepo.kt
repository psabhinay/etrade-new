package com.etrade.mobilepro.searching.symbol

import com.etrade.mobilepro.searching.dto.SymbolLookupResponseDto
import com.etrade.mobilepro.searching.rest.SymbolSearchApiClient
import com.etrade.mobilepro.searchingapi.SymbolLookupRepo
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.searchingapi.items.SymbolLookUpResult
import io.reactivex.Observable

private const val CALL_OPTION = "OPTNC"
private const val PUT_OPTION = "OPTNP"

class DefaultSymbolLookupRepo(
    private val apiClient: SymbolSearchApiClient,
    private val mapper: (SymbolLookupResponseDto, SearchQuery) -> SymbolLookUpResult
) : SymbolLookupRepo {
    override fun loadSymbolLookup(id: SearchQuery): Observable<SymbolLookUpResult> {
        val type = when (id.type) {
            SearchType.CALL_OPTION -> CALL_OPTION
            SearchType.PUT_OPTION,
            SearchType.SYMBOL -> PUT_OPTION
        }
        return apiClient.symbolLookup(id.value.trim(), type, id.expiryYear, id.expiryMonth, id.expiryDay, 2, 1)
            .map {
                mapper.invoke(it, id)
            }.toObservable()
    }
}
