package com.etrade.mobilepro.biometric.presentation

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.etrade.mobilepro.util.reference
import org.slf4j.LoggerFactory
import java.lang.ref.WeakReference
import javax.crypto.Cipher
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(BiometricAuthenticationDelegateImpl::class.java)

class BiometricAuthenticationDelegateImpl @Inject constructor() : BiometricAuthenticationDelegate, LifecycleObserver {

    override var onAuthenticationCancel: (() -> Unit)? = null
    override var onAuthenticationFail: ((reason: Int?) -> Unit)? = null
    override var onAuthenticationSuccess: ((Cipher) -> Unit)? = null

    val _isBiometricPromptShowing = MutableLiveData<Boolean>(false)
    private var activity = WeakReference<AppCompatActivity>(null)

    private var biometricPromptCancelled: Boolean = false

    private var cipher: Cipher? = null
    private var isUserInitiated: Boolean = false
    private var biometricPromptType: BiometricPromptType = BiometricPromptType.AUTHENTICATE

    override fun init(activity: AppCompatActivity) {
        this.activity = WeakReference(activity)
        (activity as LifecycleOwner).lifecycle.addObserver(this)
    }

    override val isBiometricPromptCancelled
        get() = biometricPromptCancelled

    override val isAuthenticationNeeded
        get() = isBiometricPromptShowing.value == false && restore() != null

    override val isBiometricPromptShowing: LiveData<Boolean>
        get() = _isBiometricPromptShowing

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        withFragmentManager { fragment ->
            fragment.bindCallbacksToExistingBiometricAuthenticationFragment {
                bindBiometricCallbacks(this)

                /**
                 * Local callback implementation of AuthPromptCallback is de referenced when the lifecycle owner
                 * i.e Fragment instance is in destroyed state.
                 * Here existing BiometricAuthenticationFragment is recreated and because callback was reset,
                 * the biometric prompt events are lost.  Refer BiometricPrompt#ResetCallbackObserver
                 * Call to this method will initialize the Biometric Prompt and set callback that handles authentication events.
                 */
                this@BiometricAuthenticationDelegateImpl.cipher?.let {
                    this.authenticate(
                        cipher = it,
                        isUserInitiated = this@BiometricAuthenticationDelegateImpl.isUserInitiated,
                        type = this@BiometricAuthenticationDelegateImpl.biometricPromptType
                    )
                }
            }
        }
    }

    override fun authenticate(cipher: Cipher, type: BiometricPromptType, isUserInitiated: Boolean) {
        biometricPromptCancelled = false
        _isBiometricPromptShowing.value = true

        this.cipher = cipher
        this.biometricPromptType = type
        this.isUserInitiated = isUserInitiated

        withFragmentManager { manager ->
            manager.showBiometricAuthenticationFragment(cipher, type, isUserInitiated) {
                bindBiometricCallbacks(this)
            }
        }
    }

    /**
     * Restores a dialog if it is present.
     *
     * @return a dialog if available
     */
    private fun restore(): BiometricAuthenticationFragment? {
        return withFragmentManager { fragmentManager ->
            fragmentManager.restoreBiometricAuthenticationFragment()
        }
    }

    private fun bindBiometricCallbacks(fragment: Fragment) {
        (fragment as? BiometricAuthenticationFragment)?.run {
            onAuthenticationSuccess = ::onSuccess
            doFinally = ::doFinally
            onAuthenticationFail = ::onFail
            onAuthenticationCancel = ::onCancel
        }
    }

    private fun onSuccess(cipher: Cipher) {
        onAuthenticationSuccess?.invoke(cipher)
        biometricPromptCancelled = false
        _isBiometricPromptShowing.value = false
    }

    private fun onFail(reason: Int?) {
        logger.debug("onAuthenticationFail reason: $reason")
        onAuthenticationFail?.invoke(reason)
        biometricPromptCancelled = false
        _isBiometricPromptShowing.value = false
    }

    private fun doFinally() {
        withFragmentManager { fragmentManager ->
            fragmentManager.removeBiometricAuthenticationFragment()
        }
    }

    private fun onCancel() {
        onAuthenticationCancel?.invoke()
        biometricPromptCancelled = true
        _isBiometricPromptShowing.value = false
    }

    private fun <T> withFragmentManager(block: (FragmentManager) -> T): T? {
        return activity.reference { activity ->
            block(activity.supportFragmentManager)
        }
    }
}
