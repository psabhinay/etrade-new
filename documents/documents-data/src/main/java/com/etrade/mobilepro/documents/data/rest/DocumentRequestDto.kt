package com.etrade.mobilepro.documents.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class DocumentRequestDto(
    @Json(name = "pdfDocument")
    val holderDto: DocumentRequestHolderDto
)

@JsonClass(generateAdapter = true)
internal class DocumentRequestHolderDto(
    @Json(name = "accountUuid")
    val accountUuid: String,
    @Json(name = "institutionType")
    val instType: String,
    @Json(name = "documentId")
    val documentId: String,
    @Json(name = "postEdgeFolder")
    val postEdgeFolder: String,
    @Json(name = "appVersion")
    val appVersion: String
)
