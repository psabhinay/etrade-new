package com.etrade.mobilepro.appstart.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class AppStartRequestDto(
    @Json(name = "appStart")
    val appStart: Any = Any()
)
