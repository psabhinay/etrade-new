package com.etrade.mobilepro.dynamic.form.api

import com.etrade.mobilepro.util.ValueCoder
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class TextInputBuilderTest {

    @Test
    fun `check SwitchInputBuilder default value coder`() {
        TextInputBuilder("id", mock()).valueCoder.let { coder ->
            coder.check("")
            coder.check("value")
        }
    }

    private fun ValueCoder<String>.check(expected: String) = assertEquals(expected, decode(encode(expected)))
}
