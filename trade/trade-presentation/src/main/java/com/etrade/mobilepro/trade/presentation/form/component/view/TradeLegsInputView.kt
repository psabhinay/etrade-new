package com.etrade.mobilepro.trade.presentation.form.component.view

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.dynamic.form.ui.ComponentView
import com.etrade.mobilepro.dynamic.form.ui.InputViewDelegate
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.optionlegselect.adapter.BaseOptionLegItemAdapter
import com.etrade.mobilepro.trade.optionlegselect.adapter.viewholder.OptionLegItemViewHolder
import com.etrade.mobilepro.trade.optionlegselect.databinding.TradeItemOptionLegBinding
import com.etrade.mobilepro.trade.optionlegselect.toOptionLegItem
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.component.TradeLegsInput
import com.etrade.mobilepro.trade.presentation.util.formatLegDetails
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.etrade.mobilepro.util.formatters.injectAMLabel

internal class TradeLegsInputView(context: Context) : RecyclerView(context), ComponentView<TradeLegsInput> {

    private val staticTradeLegAdapter: TradeLegAdapter by lazy { TradeLegAdapter() }

    private val editableLegBottomPadding by lazy { resources.getDimensionPixelSize(R.dimen.spacing_small) }

    private val inputViewDelegate: InputViewDelegate = InputViewDelegate(this)

    private val valueObserver: Observer<List<TradeLeg>?> = Observer { value ->
        val legs = value.orEmpty()
        val editableAdapter = editableTradeLegAdapter
        val (currentAdapter, bottomPadding) = if (legs.size > 1 || editableAdapter == null) {
            staticTradeLegAdapter.apply { items = legs } to 0
        } else {
            editableAdapter.apply { items = legs.map { it.toOptionLegItem(resources) } } to editableLegBottomPadding
        }

        if (adapter != currentAdapter) {
            adapter = currentAdapter
            setPadding(0, 0, 0, bottomPadding)
        }
    }

    private var input: TradeLegsInput? = null
    private var editableTradeLegAdapter: EditableTradeLegAdapter? = null

    init {
        layoutManager = LinearLayoutManager(context)
        setupRecyclerViewDivider(R.drawable.thin_divider)
    }

    override fun render(component: TradeLegsInput, owner: LifecycleOwner) {
        inputViewDelegate.render(component, owner)

        input?.value?.removeObserver(valueObserver)

        input = component

        if (editableTradeLegAdapter?.lifecycleOwner != owner) {
            editableTradeLegAdapter = EditableTradeLegAdapter(owner)
        }

        component.value.observe(owner, valueObserver)
    }

    private class TradeLegViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val textView: TextView = itemView as TextView

        fun bindTo(leg: TradeLeg) {
            textView.text = textView.resources
                .formatLegDetails(leg.transactionType, leg.displaySymbol, leg.quantity.toString())
                .injectAMLabel(leg.isAMOption)
        }
    }

    private class TradeLegAdapter : RecyclerView.Adapter<TradeLegViewHolder>() {

        var items: List<TradeLeg> = emptyList()
            set(value) {
                if (field != value) {
                    field = value
                    notifyDataSetChanged()
                }
            }

        override fun getItemCount(): Int = items.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TradeLegViewHolder {
            return TradeLegViewHolder(parent.inflate(R.layout.trade_item_order_leg))
        }

        override fun onBindViewHolder(holder: TradeLegViewHolder, position: Int) {
            holder.bindTo(items[position])
        }
    }

    private inner class EditableTradeLegAdapter(val lifecycleOwner: LifecycleOwner) : BaseOptionLegItemAdapter() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionLegItemViewHolder {
            val binding = parent.viewBinding(TradeItemOptionLegBinding::inflate)
            return OptionLegItemViewHolder(
                binding = binding,
                rootView = binding.root,
                lifecycleOwner = lifecycleOwner,
                getItemAtPosition = { position ->
                    takeIf { position <= underlyingItems.lastIndex }
                        ?.let { underlyingItems[position] }
                },
            ) { index, quantity ->
                input?.updateQuantity(index, quantity)
            }
        }
    }
}
