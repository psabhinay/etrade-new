package com.etrade.mobilepro.billpay.data.rest.searchpayee

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SearchPayeeResponseBodyDto(
    @Json(name = "sessionId")
    val sessionId: String? = null,
    @Json(name = "payeeList")
    val payeeList: List<SearchPayeeItemDto>? = null
) : BaseDataDto()

@JsonClass(generateAdapter = true)
data class SearchPayeeItemDto(
    @Json(name = "payeeId")
    val payeeId: String?,
    @Json(name = "name")
    val name: String,
    @Json(name = "category")
    val category: String?,
    @Json(name = "merchantId")
    val merchantId: Int?,
    @Json(name = "merchantZipRequired")
    val merchantZipRequired: Boolean?,
    @Json(name = "exactMatch")
    val exactMatch: Boolean?
)
