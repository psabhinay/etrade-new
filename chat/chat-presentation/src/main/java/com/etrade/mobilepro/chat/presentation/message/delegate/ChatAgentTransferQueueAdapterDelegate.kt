package com.etrade.mobilepro.chat.presentation.message.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.api.ChatMessage.TransferQueue
import com.etrade.mobilepro.chat.presentation.R
import com.etrade.mobilepro.chat.presentation.message.viewHolder.ChatMessageTransferQueueViewHolder
import com.etrade.mobilepro.util.android.extension.inflate
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class ChatAgentTransferQueueAdapterDelegate : AbsListItemAdapterDelegate<TransferQueue, ChatMessage, ChatMessageTransferQueueViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup) =
        ChatMessageTransferQueueViewHolder(parent.inflate(R.layout.chat_item_message_transfer_queue))

    override fun isForViewType(item: ChatMessage, items: MutableList<ChatMessage>, position: Int) = item is TransferQueue

    override fun onBindViewHolder(item: TransferQueue, holder: ChatMessageTransferQueueViewHolder, payloads: MutableList<Any>) {
        /* do nothing */
    }
}
