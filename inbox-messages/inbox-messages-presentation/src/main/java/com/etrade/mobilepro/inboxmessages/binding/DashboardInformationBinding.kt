package com.etrade.mobilepro.inboxmessages.binding

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.R

class DashboardInformationBinding(
    private val onItemDismissListener: ((InboxMessage) -> Unit)? = null,
    private val onItemClickListener: ((InboxMessage) -> Unit)? = null
) : InboxMessageBinding {

    override val layoutRes: Int = R.layout.inbox_message_dashboard_information_view

    override fun bind(item: InboxMessage, view: View) {
        view.findViewById<ImageView>(R.id.logo).apply {
            setOnClickListener { onItemClickListener?.invoke(item) }
        }
        view.findViewById<TextView>(R.id.title).apply {
            text = item.title
            setOnClickListener { onItemClickListener?.invoke(item) }
        }
        view.findViewById<TextView>(R.id.subtitle).apply {
            text = item.subtitle
            setOnClickListener { onItemClickListener?.invoke(item) }
        }
        view.findViewById<ImageView>(R.id.close).setOnClickListener {
            onItemDismissListener?.invoke(item)
        }
    }
}
