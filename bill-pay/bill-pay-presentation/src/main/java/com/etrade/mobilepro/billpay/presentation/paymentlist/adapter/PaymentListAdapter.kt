package com.etrade.mobilepro.billpay.presentation.paymentlist.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.billpay.presentation.paymentlist.PaymentListItem
import com.etrade.mobilepro.util.android.extension.dispatchUpdates

internal class PaymentListAdapter(
    private val clickListener: (PaymentListItem) -> Unit
) : RecyclerView.Adapter<PaymentListViewHolder>() {
    var items: List<PaymentListItem> = emptyList()
        set(value) {
            val oldValue = field
            field = value
            dispatchUpdates(oldValue, value)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentListViewHolder =
        PaymentListViewHolder(parent, clickListener)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PaymentListViewHolder, position: Int) = holder.bind(items[position])
}
