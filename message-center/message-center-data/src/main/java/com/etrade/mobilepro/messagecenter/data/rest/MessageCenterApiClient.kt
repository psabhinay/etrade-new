package com.etrade.mobilepro.messagecenter.data.rest

import com.etrade.eo.rest.retrofit.Xml
import com.etrade.mobilepro.messagecenter.data.dto.AccountHoldersDto
import com.etrade.mobilepro.messagecenter.data.dto.MessageListResponseDto
import com.etrade.mobilepro.messagecenter.data.dto.ReferenceBookResponseDto
import com.etrade.mobilepro.messagecenter.data.dto.ReplyMessageDto
import com.etrade.mobilepro.messagecenter.data.dto.SendNewMessageDto
import com.etrade.mobilepro.messagecenter.data.dto.ServiceMessageDetailsResponseDto
import com.etrade.mobilepro.messagecenter.data.dto.SupportMessageDetailsResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface MessageCenterApiClient {
    @POST("e/t/mobile/SecureMsgInBox")
    @Xml
    suspend fun getMessageList(): MessageListResponseDto

    @POST("e/t/mobile/GetMobileSecServiceDetails")
    @Xml
    suspend fun getServiceMessageDetails(@Query("i_proc_instid") caseNo: String): ServiceMessageDetailsResponseDto

    @POST("e/t/mobile/GetMobileSecMsgDetail")
    @Xml
    suspend fun getSupportMessageDetails(@Query("MsgId") msgId: String, @Query("caseno") caseNo: String): SupportMessageDetailsResponseDto

    @POST("e/t/mobile/CreateSecureMsg")
    @Xml
    suspend fun getReferenceBook(): ReferenceBookResponseDto

    @POST("e/t/mobile/getaccountholders.json")
    @FormUrlEncoded
    @JsonMoshi
    suspend fun getAccountHolders(
        @Field("AcctNo") acctNo: String,
        @Field("InstNo") instNo: String,
        @Field("InstName") instName: String,
        @Field("stk1") stk1: String
    ): AccountHoldersDto

    @Suppress("LongParameterList")
    @POST("e/t/mobile/CreateSecureMsg")
    @FormUrlEncoded
    @Xml
    suspend fun sendNewMessage(
        @Field("selAcctHolder") selAcctHolder: String,
        @Field("AccountId") accountId: String,
        @Field("SubjectCode") subjectCode: String,
        @Field("TopicCode") topicCode: String,
        @Field("Message") message: String,
        @Field("InstName") instName: String,
        @Field("InstNo") instNo: String,
        @Field("Mode") mode: String = "create",
        @Field("Channel") channel: String = "1"
    ): SendNewMessageDto

    @Suppress("LongParameterList")
    @POST("e/t/mobile/ReplyCSMsgV2")
    @FormUrlEncoded
    @Xml
    suspend fun sendReply(
        @Field("caseno") caseNo: String,
        @Field("MsgId") msgId: String,
        @Field("txtMessage") content: String,
        @Field("Send") send: String = "Send", // some constant parameter
        @Field("_formtarget") formTarget: String = "replycsmsgv2" // some constant parameter
    ): ReplyMessageDto
}
