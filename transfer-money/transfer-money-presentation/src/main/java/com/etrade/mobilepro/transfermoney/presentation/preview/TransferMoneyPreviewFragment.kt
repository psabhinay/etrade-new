package com.etrade.mobilepro.transfermoney.presentation.preview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.transfermoney.presentation.R
import com.etrade.mobilepro.transfermoney.presentation.contributiondetail.TransferIraContributionDetailViewModel
import com.etrade.mobilepro.transfermoney.presentation.databinding.TransferFragmentPreviewBinding
import com.etrade.mobilepro.transfermoney.presentation.preview.ira.IraContributionAdapter
import com.etrade.mobilepro.transfermoney.presentation.transfer.TransferViewModel
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.util.android.accessibility.bindTextInputButtonAccessibility
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.setEnabledState
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.ActivitySignalViewDelegate
import com.etrade.mobilepro.viewmodel.BackButtonInterceptorViewModel
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

private const val TAX_YEAR_TAG = "tax_year_tag"

@RequireLogin
class TransferMoneyPreviewFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val iraContributionAdapter: IraContributionAdapter,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : OverlayBaseFragment() {

    override val layoutRes: Int = R.layout.transfer_fragment_preview
    private val binding by viewBinding(TransferFragmentPreviewBinding::bind)
    private val backInterceptorViewModel by activityViewModels<BackButtonInterceptorViewModel> { viewModelFactory }
    private val viewModel: TransferMoneyPreviewViewModel by viewModels { viewModelFactory }
    private val transferMoneyViewModel by navGraphViewModels<TransferViewModel>(R.id.transfer_money_nav_graph) { viewModelFactory }
    private val iraContributionDetailViewModel by navGraphViewModels<TransferIraContributionDetailViewModel>(R.id.transfer_money_nav_graph) { viewModelFactory }
    private val args: TransferMoneyPreviewFragmentArgs by navArgs()
    private lateinit var taxYearSelector: DropDownMultiSelector<String>
    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    private var isCancelEnabled: Boolean = false
        set(value) {
            (activity as? AppCompatActivity)?.apply {
                toolbarActionEndView?.apply {
                    setEnabledState(value)
                }
                supportActionBar?.apply {
                    if (value) {
                        setDisplayHomeAsUpEnabled(true)
                        setHomeAsUpIndicator(R.drawable.ic_arrow_back)
                    } else {
                        setDisplayHomeAsUpEnabled(false)
                    }
                }
            }
            field = value
        }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        activity?.apply {
            toolbarActionEndView?.apply {
                setToolbarActionTextAndContentDescription(R.string.cancel)
                setOnClickListener {
                    transferMoneyViewModel.resetTransferMoneyData()
                    findNavController().navigateUp()
                }
                isCancelEnabled = true
                visibility = View.VISIBLE
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState == null) {
            viewModel.loadPreview(args.parameter)
        }

        setupViewModel()
        setupBackInterceptor()
        bindIraContributionData()
        setupOnClickListeners()
    }

    private fun setupBackInterceptor() {
        ActivitySignalViewDelegate(backInterceptorViewModel, requireActivity()).observe(viewLifecycleOwner)
        backInterceptorViewModel.backPressedSignal.observeBy(viewLifecycleOwner) {
            it.consume {
                if (isCancelEnabled) {
                    findNavController().navigateUp()
                }
            }
        }
    }

    private fun setupOnClickListeners() {
        binding.submit.setOnClickListener {
            viewModel.submitTransferConfirmation(args.parameter.toRepositoryConfirmRequest(viewModel.iraYear))
        }
        binding.taxYear.setOnClickListener {
            taxYearSelector.open()
        }
    }

    @Suppress("LongMethod")
    private fun setupViewModel() {
        viewModel.popupErrorMessage.observe(
            viewLifecycleOwner,
            Observer {
                transferMoneyViewModel.showPopupErrorMessage(it)
                findNavController().navigateUp()
            }
        )
        viewModel.snackbarMessage.observe(
            viewLifecycleOwner,
            Observer {
                transferMoneyViewModel.showSnackbarMessage()
                findNavController().navigateUp()
            }
        )
        viewModel.loadingIndicator.observe(viewLifecycleOwner, Observer { changeLoadingIndicatorState(it) })
        viewModel.transferMoneyPreviewItems.observe(viewLifecycleOwner, Observer { changeTransferMoneyPreviewItemsState(it) })
        viewModel.footerMessage.observe(viewLifecycleOwner, Observer { binding.footerMessage.text = it })
        viewModel.iraAccountDisclaimer.observe(viewLifecycleOwner, Observer { changeIraDisclaimerState(it) })
        viewModel.contentVisibility.observe(viewLifecycleOwner, Observer { changeContentVisibility(it) })
        viewModel.selectedIraYear.observe(viewLifecycleOwner, Observer { changeTaxYear(it) })
        viewModel.taxYearVisibility.observe(viewLifecycleOwner, Observer { changeTaxYearState(it) })
        viewModel.noTaxYear.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.let {
                    snackBarUtil.snackbar(getString(R.string.transfer_no_tax_year), Snackbar.LENGTH_SHORT)
                }
            }
        )
        setupConfirmViewState()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        lifecycle.addObserver(backInterceptorViewModel)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onDestroyView() {
        lifecycle.removeObserver(backInterceptorViewModel)
        super.onDestroyView()
    }

    @Suppress("LongMethod")
    private fun setupConfirmViewState() {
        viewModel.confirmationViewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    ConfirmationViewState.Loading -> {
                        isCancelEnabled = false
                        changeLoadingIndicatorState(true)
                        changeContentVisibility(false)
                    }
                    is ConfirmationViewState.Success -> {
                        isCancelEnabled = true
                        transferMoneyViewModel.resetTransferMoneyData()
                        // Refresh accounts after success transfer submission
                        // By unknown reason if we don't perform QuickTransfer request
                        // we not able to submit one more transfer requests
                        // In existing app we call QuickTransfer every time when transfer page appears
                        transferMoneyViewModel.refreshAccounts()
                        findNavController().navigate(TransferMoneyPreviewFragmentDirections.openConfirmation(it.confirmation))
                    }
                    is ConfirmationViewState.Failed -> {
                        isCancelEnabled = true
                        changeLoadingIndicatorState(false)
                        changeContentVisibility(true)

                        it.consumableErrorDescription?.let { errorDescription ->
                            if (errorDescription.isEmpty()) {
                                snackBarUtil.errorSnackbar()
                            } else {
                                findNavController().navigate(TransferMoneyPreviewFragmentDirections.openPreviewPopupDialog(null, errorDescription))
                            }
                        }
                    }
                }
            }
        )
    }

    private fun bindIraContributionData() {
        val iraPreviewData = iraContributionAdapter.adaptIraContributionToPreview(iraContributionDetailViewModel, viewModel)
        taxYearSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = TAX_YEAR_TAG,
                multiSelectEnabled = false,
                title = resources.getString(R.string.transfer_tax_year),
                itemsData = iraPreviewData.taxYearData,
                loadingState = iraPreviewData.iraInfoState,
                snackbarUtilFactory = snackbarUtilFactory,
                selectListener = { _, selected ->
                    viewModel.taxYearSelected(selected.value)
                }
            )
        )
    }

    private fun changeTransferMoneyPreviewItemsState(it: List<TableSectionItemLayout>) {
        binding.apply {
            contentTable.setContent(it)
            divider.visibility = View.VISIBLE
        }
    }

    private fun changeIraDisclaimerState(it: Boolean) {
        binding.iraDisclaimer.visibility = if (it) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun changeTaxYearState(it: Boolean) {
        binding.apply {
            taxYearLayout.visibility = if (it) {
                View.VISIBLE
            } else {
                View.GONE
            }
            divider.visibility = if (it) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }
    }

    private fun changeTaxYear(taxYearValue: String?) {
        binding.apply {
            taxYear.setText(taxYearValue)
            taxYearLayout.bindTextInputButtonAccessibility(
                "${resources.getString(R.string.transfer_tax_year)} ${taxYearValue ?: ""}"
            )
        }
    }

    private fun changeLoadingIndicatorState(value: Boolean) {
        binding.apply {
            if (value) {
                loadingIndicator.show()
            } else {
                loadingIndicator.hide()
            }
        }
    }

    private fun changeContentVisibility(isVisible: Boolean) {
        binding.content.visibility = if (isVisible) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
}
