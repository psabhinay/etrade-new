package com.etrade.mobilepro.marketscalendar.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.etrade.mobilepro.marketscalendar.MarketCalendarRouter
import com.etrade.mobilepro.marketscalendar.R
import com.etrade.mobilepro.marketscalendar.adapter.MarketsCalendarEventAdapter
import com.etrade.mobilepro.marketscalendar.data.dto.EARNINGS_EVENT
import com.etrade.mobilepro.marketscalendar.data.dto.ECONOMIC_EVENT
import com.etrade.mobilepro.marketscalendar.databinding.FragmentMarketsResultBinding
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails
import com.etrade.mobilepro.marketscalendar.viewmodel.MarketsCalendarResultViewModel
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import org.threeten.bp.LocalDate

abstract class BaseResultMarketsCalendarFragment<T, V : MarketsCalendarResultViewModel<T>>(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val router: MarketCalendarRouter
) : Fragment(R.layout.fragment_markets_result) {

    abstract val noResultMessage: Int
    abstract val title: Int
    abstract val createViewModel: (ViewModelProvider) -> V

    private val viewModel: V by lazy { createViewModel(ViewModelProvider(parentFragment ?: activity ?: this, viewModelFactory)) }

    private var snackBar: Snackbar? = null
    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private var recyclerViewEventAdapter: MarketsCalendarEventAdapter? = null
    private val binding by viewBinding(FragmentMarketsResultBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        initEventsViewModel()
        initEventsSummaryViewModel()
    }

    private fun initEventsSummaryViewModel() {
        viewModel.eventsSummary.observe(
            viewLifecycleOwner,
            Observer {
                if (it is Resource.Failed) {
                    showRetrySnackBar { viewModel.loadEventsSummary() }
                } else if (it is Resource.Success) {
                    dismissRetrySnackBar()
                    it.data?.items?.let { items -> binding.calendarView.updateCalendarEvents(items) }
                }
            }
        )
    }

    private fun initEventsViewModel() {
        viewModel.events.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is Resource.Loading -> {
                        handleEventsLoading()
                    }
                    is Resource.Failed -> {
                        handleEventsFailed()
                    }
                    is Resource.Success -> {
                        handleEventsSuccess(it)
                    }
                }
            }
        )
    }

    private fun handleEventsSuccess(it: Resource<List<EventDetails>>) {
        dismissRetrySnackBar()
        binding.apply {
            swipeRefresh.isRefreshing = false
            progress.visibility = View.GONE
            if (it.data?.isEmpty() == true) {
                noResult.visibility = View.VISIBLE
                dividerHorizontal.visibility = View.VISIBLE
            } else {
                noResult.visibility = View.GONE
                dividerHorizontal.visibility = View.GONE
            }
        }

        recyclerViewEventAdapter?.updateEvents(it.data)
    }

    private fun handleEventsFailed() {
        recyclerViewEventAdapter?.updateEvents(null)

        binding.swipeRefresh.isRefreshing = false
        binding.progress.visibility = View.GONE

        showRetrySnackBar { viewModel.loadEvents() }
    }

    private fun handleEventsLoading() {
        recyclerViewEventAdapter?.updateEvents(null)

        if (!binding.swipeRefresh.isRefreshing) {
            binding.progress.visibility = View.VISIBLE
        }
    }

    private fun initViews() {
        binding.calendarView.apply {
            onDateSelected = { date ->
                viewModel.onDateSelected(date)
                viewModel.selectedDatePagePosition = currentCalendarPage
            }
            onCalendarRangeSelected = { start, end, position -> viewModel.updateSelectedCalendarRange(start, end, position) }
            disabledDaysPredicate = { it < LocalDate.now() }
            selectedDate = viewModel.selectedDate

            setCalendarSelectedRange(viewModel.selectedDatePagePosition)
        }
        initRecyclerView()
        binding.noResult.setText(noResultMessage)
        initSwipeToRefresh()
    }

    private fun initRecyclerView() {
        recyclerViewEventAdapter = MarketsCalendarEventAdapter { item ->
            when (item.eventType) {
                EARNINGS_EVENT -> router.navigateToEarningEventDetails(findNavController(), item)
                ECONOMIC_EVENT -> router.navigateToEconomicEventDetails(findNavController(), item)
            }
        }

        binding.calendarRv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = recyclerViewEventAdapter
            setupRecyclerViewDivider(R.drawable.thin_divider)
        }

        (binding.calendarRv.itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
    }

    private fun initSwipeToRefresh() {
        binding.swipeRefresh.apply {
            applyColorScheme()
            setOnRefreshListener {
                snackBar?.dismiss()

                viewModel.loadEventsSummary()
                viewModel.loadEvents()
            }
        }
    }

    private fun showRetrySnackBar(action: () -> Unit) {
        snackBar = snackbarUtil.retrySnackbar(actionOnRetry = action)
    }

    private fun dismissRetrySnackBar() {
        snackBar?.dismiss()
    }
}
