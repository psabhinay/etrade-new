package com.etrade.mobilepro.brokerage

import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.common.di.RemoteDynamicScreen
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.portfolio.PortfolioSettingsRepository
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.positions.PositionListViewModel
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.squareup.moshi.Moshi
import javax.inject.Inject
import javax.inject.Provider

class AllBrokerageAccountsViewModel @Inject constructor(
    @RemoteDynamicScreen
    dynamicScreenRepo: DynamicScreenRepo,
    moshi: Moshi,
    tradingDefaultsPreferences: TradingDefaultsPreferences,
    tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    darkModeChecker: DarkModeChecker,
    portfolioSettingsRepository: PortfolioSettingsRepository
) : PositionListViewModel(
    dynamicScreenRepo,
    moshi,
    tradingDefaultsPreferences,
    darkModeChecker,
    tradeFormParametersBuilderProvider,
    portfolioSettingsRepository,
    PositionTableMode.ALL_BROKERAGE_VIEW
) {

    override fun getScreenRequest(sortInfo: SortInfo): ScreenRequest = ServicePath.AllBrokeragePositions(sortInfo.column, sortInfo.order)
}
