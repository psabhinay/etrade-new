package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import androidx.annotation.StringRes
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.orders.api.FundsOrderActionType
import com.etrade.mobilepro.orders.api.FundsOrderActionType.BUY
import com.etrade.mobilepro.orders.api.FundsOrderActionType.EXCHANGE
import com.etrade.mobilepro.orders.api.FundsOrderActionType.SELL
import com.etrade.mobilepro.orders.api.FundsQuantityType
import com.etrade.mobilepro.orders.api.FundsQuantityType.DOLLARS
import com.etrade.mobilepro.orders.api.FundsQuantityType.ENTIRE_CASH_POSITION
import com.etrade.mobilepro.orders.api.FundsQuantityType.ENTIRE_MARGIN_POSITION
import com.etrade.mobilepro.orders.api.FundsQuantityType.ENTIRE_POSITION
import com.etrade.mobilepro.orders.api.FundsQuantityType.SHARES
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrder
import com.etrade.mobilepro.trade.api.quantity
import com.etrade.mobilepro.trade.presentation.util.R
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createMultiLineSummarySection
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import java.math.BigDecimal

fun toAccountsOrderTypeItems(resources: Resources, account: String, orderActionType: FundsOrderActionType): List<TableSectionItemLayout> {
    return listOfNotNull(
        resources.sectionItem(R.string.trade_label_account, account),
        resources.sectionItem(R.string.trade_label_order_type, orderActionType.getLabel())
    )
}

fun MutualFundsOrder.toAmountItems(resources: Resources): List<TableSectionItemLayout> {
    return when (orderActionType) {
        BUY -> listOfNotNull(
            resources.sectionItem(R.string.trade_label_investment_amount, quantity.formatCurrency()),
            dividendReInvestmentType.getLabel()?.let { resources.sectionItem(R.string.trade_label_reinvestment_type, it) }
        )
        SELL, EXCHANGE -> listOfNotNull(
            resources.sectionItem(R.string.trade_label_amount_to_sell, resources.getSellAmountValue(quantityType, quantity))
        )
        else -> emptyList()
    }
}

fun MutualFundsOrder.toMutualFundsCommissionItems(resources: Resources, isLoadWaivedFund: Boolean = false): List<TableSectionItemLayout> {
    return when (orderActionType) {

        BUY, EXCHANGE -> listOfNotNull(
            if (isLoadWaivedFund) {
                commissionMessage?.let { resources.sectionMultiLineItem(R.string.trade_label_commission, it) }
            } else {
                commission?.let { resources.sectionMultiLineItem(R.string.trade_label_commission, it.formatCurrency()) }
            },
            resources.sectionMultiLineItem(R.string.trade_label_estimated_total_cost, priceTypeMessage)
        )

        SELL -> listOfNotNull(
            commission?.let { resources.sectionMultiLineItem(R.string.trade_label_commission, it.formatCurrency()) },
            resources.sectionMultiLineItem(R.string.trade_label_estimated_total_proceeds, priceTypeMessage)
        )
    }
}

fun getMutualFundAmountDisplayText(
    orderActionType: FundsOrderActionType,
    quantity: BigDecimal?,
    quantityType: FundsQuantityType,
    resources: Resources
): String {
    return when (orderActionType) {
        BUY -> quantity.formatCurrency()
        SELL, EXCHANGE -> resources.getSellAmountValue(quantityType, quantity)
        else -> resources.getString(R.string.empty_default)
    }
}

private fun Resources.getSellAmountValue(quantityType: FundsQuantityType, amount: BigDecimal?): String {
    return when (quantityType) {
        ENTIRE_CASH_POSITION -> getString(R.string.trade_label_entire_cash_position, amount)
        ENTIRE_MARGIN_POSITION -> getString(R.string.trade_label_entire_margin_position, amount)
        ENTIRE_POSITION -> getString(R.string.trade_label_entire_position, amount)
        DOLLARS -> amount.formatCurrency()
        SHARES -> amount?.let {
            getString(R.string.trade_label_share_information_shares_decimal, it)
        } ?: getString(R.string.empty_default)
        else -> getString(R.string.empty_default)
    }
}

internal fun BigDecimal?.formatCurrency(): String = this?.let { MarketDataFormatter.formatCurrencyValue(it) } ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE

internal fun Resources.sectionItem(@StringRes labelRes: Int, @StringRes valueRes: Int): TableSectionItemLayout? {
    return sectionItem(labelRes, getString(valueRes))
}

internal fun Resources.sectionItem(@StringRes labelRes: Int, value: String?): TableSectionItemLayout? {
    return value?.let { createSummarySection(getString(labelRes), it) }
}

fun Resources.sectionMultiLineItem(@StringRes labelRes: Int, value: String?): TableSectionItemLayout? {
    return value?.let { createMultiLineSummarySection(getString(labelRes), it) }
}
