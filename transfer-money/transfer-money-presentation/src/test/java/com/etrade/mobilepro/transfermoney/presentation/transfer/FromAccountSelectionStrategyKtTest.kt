package com.etrade.mobilepro.transfermoney.presentation.transfer

import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Test

class FromAccountSelectionStrategyKtTest {

    @Test
    fun `select account when a preselected id is provided`() {
        val preselectedId = "preselected"
        val fromAccounts = listOf(
            mockTransferAccount("1"),
            mockTransferAccount("2"),
            mockTransferAccount(preselectedId)
        )

        val selectedAccount = selectFromAccount(preselectedId, fromAccounts)

        assertNotNull(selectedAccount)
        assertEquals(selectedAccount!!.accountId, preselectedId)
    }

    @Test
    fun `select account when there is only one possibility`() {
        val id = "one shot"
        val fromAccounts = listOf(
            mockTransferAccount(id)
        )

        val selectedAccount = selectFromAccount(null, fromAccounts)

        assertNotNull(selectedAccount)
        assertEquals(selectedAccount!!.accountId, id)
    }

    @Test
    fun `do not select account when there is not exactly one match for preselected ID`() {
        val preselectedId = "the one"
        // Account IDs are only unique per account type. If we can't disambiguate, better to not select at all
        val fromAccounts = listOf(
            mockTransferAccount(preselectedId, accountType = "i aint got no type"),
            mockTransferAccount(preselectedId, accountType = "statically typed languages is the only thing that i like")
        )

        val selectedAccount = selectFromAccount(preselectedId, fromAccounts)

        assertNull(selectedAccount)
    }

    private fun mockTransferAccount(accountId: String, accountType: String = "type") = mock<TransferMoneyAccount> {
        on { this.accountId } doReturn accountId
        on { this.accountType } doReturn accountType
    }
}
