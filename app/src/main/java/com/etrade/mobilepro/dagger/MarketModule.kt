package com.etrade.mobilepro.dagger

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.common.DefaultTimeStampReporter
import com.etrade.mobilepro.common.TimeStampReporter
import com.etrade.mobilepro.common.di.DowJonesColor
import com.etrade.mobilepro.common.di.NasdaqColor
import com.etrade.mobilepro.common.di.SP500Color
import com.etrade.mobilepro.dummy.DefaultStreamingTextManager
import com.etrade.mobilepro.home.overview.viewfactory.FullTimestampFormatterType
import com.etrade.mobilepro.indices.usecase.GetTopIndicesUseCase
import com.etrade.mobilepro.indices.usecase.GetTopIndicesUseCaseImpl
import com.etrade.mobilepro.markets.data.MoverRepoImpl
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.earnings.GetTodayEarningsUseCase
import com.etrade.mobilepro.markets.earnings.GetTodayEarningsUseCaseImpl
import com.etrade.mobilepro.markets.futures.widget.DefaultFutureIndicesLoader
import com.etrade.mobilepro.markets.futures.widget.FutureIndicesLoader
import com.etrade.mobilepro.marketsapi.MoverRepo
import com.etrade.mobilepro.movers.markets.GetMarketMoversUseCase
import com.etrade.mobilepro.movers.markets.GetMarketMoversUseCaseImpl
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import com.etrade.mobilepro.util.android.ChangeColorPicker
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */
@Module(
    includes = [
        StreamingModule::class
    ]
)
abstract class MarketModule {

    @Binds
    abstract fun bindGetTopIndicesUseCase(impl: GetTopIndicesUseCaseImpl): GetTopIndicesUseCase

    @Binds
    abstract fun bindMoverRepo(impl: MoverRepoImpl): MoverRepo

    @Binds
    @Singleton
    abstract fun bindGetMarketMoversUseCase(useCase: GetMarketMoversUseCaseImpl): GetMarketMoversUseCase

    @Binds
    @Singleton
    abstract fun bindStreamingTextManager(manager: DefaultStreamingTextManager): StreamingTextManager

    @Binds
    abstract fun bindTimeStampReporter(reporter: DefaultTimeStampReporter): TimeStampReporter

    @Binds
    @Singleton
    abstract fun bindGetTodayEarningsUseCase(useCase: GetTodayEarningsUseCaseImpl): GetTodayEarningsUseCase

    companion object {
        @Provides
        fun provideFullTimestampFormatterType(): FullTimestampFormatterType = DateFormattingUtils::formatFullShortTimeDateWithSlashes

        @Provides
        fun provideFuturesLoader(
            @DowJonesColor dowJonesColor: Int,
            @NasdaqColor nasdaqColor: Int,
            @SP500Color sp500Color: Int,
            moverService: MoverService,
            changeColorPicker: ChangeColorPicker
        ): FutureIndicesLoader =
            DefaultFutureIndicesLoader(
                dowJonesColor, nasdaqColor, sp500Color, moverService, changeColorPicker
            )
    }
}
