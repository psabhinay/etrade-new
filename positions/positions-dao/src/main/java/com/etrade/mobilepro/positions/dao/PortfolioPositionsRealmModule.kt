package com.etrade.mobilepro.positions.dao

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class PortfolioPositionsRealmModule
