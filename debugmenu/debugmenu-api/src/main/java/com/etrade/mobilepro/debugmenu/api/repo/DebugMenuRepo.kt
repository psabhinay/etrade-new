package com.etrade.mobilepro.debugmenu.api.repo

import com.etrade.mobilepro.appconfig.api.AppEnvironment
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.debugmenu.api.EnvChange

interface DebugMenuRepo {
    suspend fun requestEnvChange(appEnvironment: AppEnvironment, login: String, password: String): ETResult<EnvChange>
}
