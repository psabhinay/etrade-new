package com.etrade.mobilepro.home

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelper
import javax.inject.Inject

class AccountsTabHostViewModel @Inject constructor(
    private val viewPagerTabsTrackerHelper: ViewPagerTabsTrackerHelper,
    private val applicationPreferences: ApplicationPreferences
) : ViewModel() {
    internal var shouldUpdateTabContentDescriptions = true
    fun trackTab(position: Int, tabTitle: String) {
        viewPagerTabsTrackerHelper.trackTab(position, tabTitle)
    }

    fun storeSelectedTabIndex(index: Int) {
        applicationPreferences.lastVisitedAccountsTabIndex = index
    }
}
