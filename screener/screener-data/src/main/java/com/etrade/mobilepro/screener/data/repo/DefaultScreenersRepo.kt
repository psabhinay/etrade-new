package com.etrade.mobilepro.screener.data.repo

import com.etrade.mobilepro.mod.auth.api.AuthTokenProvider
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.repo.ScreenersRepo
import com.etrade.mobilepro.screener.api.result.PrescreenData
import com.etrade.mobilepro.screener.data.rest.ResultResponse
import com.etrade.mobilepro.screener.data.rest.ScreenerApiClient
import com.squareup.moshi.Moshi
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import java.net.URLEncoder
import javax.inject.Inject

private const val prescreenStockBaseUrl = "/Research/Screener/GetStockPrescreenAPI/"
private const val prescreenMFBaseUrl = "/Research/api/MFPrescreener/"
private const val prescreenETFBaseUrl = "/Research/Screener/GetETFPrescreenAPI/"
private const val encoding = "UTF-8"
private const val stockResultsBaseUrl = "/Research/Screener/GetStockscreenAPI/"
private const val mfResultsBaseUrl = "/Research/api/MFscreener/"
private const val etfResultsBaseUrl = "/Research/Screener/GetETFscreenAPI/"

private val responseGarbage = mapOf(
    "var results =" to "",
    "};" to "}",
    """\u003cspan class=\"neg color \"\u003e""" to "",
    """%\u003c/span\u003e""" to "",
    """\u003cspan class=\"pos color \"\u003e""" to "",
    "<span class=pos>+" to "",
    "%</span>" to "",
    "<span class=neg>" to ""
)

class DefaultScreenersRepo @Inject constructor(
    private val apiClient: ScreenerApiClient,
    private val authTokenProvider: AuthTokenProvider,
    private val toDomainMapper: (ResultResponse) -> List<PrescreenData>,
    moshi: Moshi
) : ScreenersRepo {
    private val resultsAdapter = moshi.adapter(ResultResponse::class.java)

    override fun preScreenResults(group: ScreenerType.Group, requestData: String): Single<Int> =
        getToken()
            .flatMap { token ->
                when (group) {
                    ScreenerType.Group.STOCK ->
                        apiClient.getStockPrescreen("$prescreenStockBaseUrl?$token&Inputs=${encodeRequestData(requestData)}")
                    ScreenerType.Group.MUTUAL_FUND ->
                        apiClient.getMFPrescreen(
                            url = "$prescreenMFBaseUrl?$token",
                            body = requestData.toRequestBody("application/json".toMediaTypeOrNull())
                        )
                    ScreenerType.Group.ETF ->
                        apiClient.getETFPrescreen("$prescreenETFBaseUrl?$token&Inputs=${encodeRequestData(requestData)}")
                }
            }
            .map {
                it.matchCount ?: 0
            }
            .toObservable()
            .firstOrError()

    override fun resultingPrescreenData(group: ScreenerType.Group, requestData: String): Single<List<PrescreenData>> =
        getToken()
            .flatMap { token ->
                when (group) {
                    ScreenerType.Group.STOCK ->
                        apiClient
                            .getStockResults("$stockResultsBaseUrl?$token&Inputs=${encodeRequestData(requestData)}")
                    ScreenerType.Group.MUTUAL_FUND -> apiClient.getMfResults(
                        url = "$mfResultsBaseUrl?$token",
                        body = requestData.toRequestBody("application/json".toMediaTypeOrNull())
                    )
                    ScreenerType.Group.ETF -> apiClient.getEtfResults("$etfResultsBaseUrl?$token&Inputs=${encodeRequestData(requestData)}")
                }
            }
            .map(this::removeGarbageFromResultsResponse)
            .map {
                resultsAdapter.fromJson(it)
            }
            .map {
                toDomainMapper.invoke(it)
            }

    private fun getToken(): Single<String> =
        Single
            .fromCallable {
                authTokenProvider.getModAuthToken()
            }
            .subscribeOn(Schedulers.io())

    private fun removeGarbageFromResultsResponse(rawResponse: String): String {
        var result = rawResponse
        responseGarbage.entries.forEach { garbage ->
            result = result.replace(garbage.key, garbage.value)
        }
        return result
    }

    private fun encodeRequestData(requestData: String): String = URLEncoder.encode(requestData, encoding)
}
