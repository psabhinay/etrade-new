package com.etrade.mobilepro.tier

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class CustomerTierResponse(
    @Json(name = "CustTierDetail")
    val detail: TierDetail?
) : BaseDataDto()

@JsonClass(generateAdapter = true)
class TierDetail(
    @Json(name = "tier")
    val tier: String?
)
