package com.etrade.mobilepro.bank;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u001f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\b\u0010&\u001a\u00020\'H\u0002J\u0012\u0010(\u001a\u00020\'2\b\b\u0002\u0010)\u001a\u00020*H\u0002J\u0018\u0010+\u001a\u00020\'2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020-H\u0002J\u0010\u0010/\u001a\u00020\'2\u0006\u00100\u001a\u000201H\u0002J\u001a\u00102\u001a\u00020\'2\u0006\u00103\u001a\u0002042\b\u00105\u001a\u0004\u0018\u000106H\u0016R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\fR\u001b\u0010\u000f\u001a\u00020\u00108BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0015\u001a\u00020\u00168BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u0017\u0010\u0018R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u001d\u001a\u00020\u001e8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b!\u0010\u0014\u001a\u0004\b\u001f\u0010 R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\"\u001a\u00020#8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\b$\u0010%R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00067"}, d2 = {"Lcom/etrade/mobilepro/bank/BankHomeFragment;", "Landroidx/fragment/app/Fragment;", "viewModelFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "snackbarUtilFactory", "Lcom/etrade/mobilepro/util/android/snackbar/SnackbarUtilFactory;", "darkModeChecker", "Lcom/etrade/mobilepro/util/android/darkmode/DarkModeChecker;", "(Landroidx/lifecycle/ViewModelProvider$Factory;Lcom/etrade/mobilepro/util/android/snackbar/SnackbarUtilFactory;Lcom/etrade/mobilepro/util/android/darkmode/DarkModeChecker;)V", "args", "Lcom/etrade/mobilepro/bank/BankHomeFragmentArgs;", "getArgs", "()Lcom/etrade/mobilepro/bank/BankHomeFragmentArgs;", "args$delegate", "Landroidx/navigation/NavArgsLazy;", "bankHomeViewModel", "Lcom/etrade/mobilepro/bank/BankHomeViewModel;", "getBankHomeViewModel", "()Lcom/etrade/mobilepro/bank/BankHomeViewModel;", "bankHomeViewModel$delegate", "Lkotlin/Lazy;", "binding", "Lcom/etrade/mobilepro/bank/databinding/BankHomeFragmentBinding;", "getBinding", "()Lcom/etrade/mobilepro/bank/databinding/BankHomeFragmentBinding;", "binding$delegate", "Lkotlin/properties/ReadOnlyProperty;", "snackBar", "Lcom/google/android/material/snackbar/Snackbar;", "snackBarUtil", "Lcom/etrade/mobilepro/util/android/snackbar/SnackBarUtil;", "getSnackBarUtil", "()Lcom/etrade/mobilepro/util/android/snackbar/SnackBarUtil;", "snackBarUtil$delegate", "viewAdapter", "Lcom/etrade/mobilepro/dynamicui/DynamicUiLayoutRecyclerAdapter;", "getViewAdapter", "()Lcom/etrade/mobilepro/dynamicui/DynamicUiLayoutRecyclerAdapter;", "bindViewModel", "", "fetchScreen", "forceRefresh", "", "loadBankTransactions", "startDate", "Lorg/threeten/bp/LocalDate;", "endDate", "observeWidgetEvents", "viewModel", "Lcom/etrade/mobilepro/dynamicui/api/DynamicViewModel;", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "bank-dynamic_debug"})
@com.etrade.mobilepro.common.RequireLogin()
public final class BankHomeFragment extends androidx.fragment.app.Fragment {
    private final androidx.lifecycle.ViewModelProvider.Factory viewModelFactory = null;
    private final com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory snackbarUtilFactory = null;
    private final com.etrade.mobilepro.util.android.darkmode.DarkModeChecker darkModeChecker = null;
    private final kotlin.properties.ReadOnlyProperty binding$delegate = null;
    private final androidx.navigation.NavArgsLazy args$delegate = null;
    private final kotlin.Lazy snackBarUtil$delegate = null;
    private com.google.android.material.snackbar.Snackbar snackBar;
    private final kotlin.Lazy bankHomeViewModel$delegate = null;
    
    @javax.inject.Inject()
    public BankHomeFragment(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.ViewModelProvider.Factory viewModelFactory, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory snackbarUtilFactory, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.util.android.darkmode.DarkModeChecker darkModeChecker) {
        super();
    }
    
    private final com.etrade.mobilepro.bank.databinding.BankHomeFragmentBinding getBinding() {
        return null;
    }
    
    private final com.etrade.mobilepro.bank.BankHomeFragmentArgs getArgs() {
        return null;
    }
    
    private final com.etrade.mobilepro.util.android.snackbar.SnackBarUtil getSnackBarUtil() {
        return null;
    }
    
    private final com.etrade.mobilepro.bank.BankHomeViewModel getBankHomeViewModel() {
        return null;
    }
    
    private final com.etrade.mobilepro.dynamicui.DynamicUiLayoutRecyclerAdapter getViewAdapter() {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @kotlin.Suppress(names = {"LongMethod"})
    private final void bindViewModel() {
    }
    
    private final void loadBankTransactions(org.threeten.bp.LocalDate startDate, org.threeten.bp.LocalDate endDate) {
    }
    
    private final void observeWidgetEvents(com.etrade.mobilepro.dynamicui.api.DynamicViewModel viewModel) {
    }
    
    private final void fetchScreen(boolean forceRefresh) {
    }
}