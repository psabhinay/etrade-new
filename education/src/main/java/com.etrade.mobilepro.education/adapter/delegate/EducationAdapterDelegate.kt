package com.etrade.mobilepro.education.adapter.delegate

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.education.EducationItem
import com.etrade.mobilepro.education.adapter.holder.EducationViewHolder
import com.etrade.mobilepro.education.databinding.AdapterEducationBinding
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate

class EducationAdapterDelegate(
    private val itemViewClickListener: View.OnClickListener
) : AdapterDelegate<MutableList<EducationItem>>() {

    override fun isForViewType(items: MutableList<EducationItem>, position: Int): Boolean =
        !(DelegateHelper.isFirstItem(position) || DelegateHelper.isLastItem(position, items) || DelegateHelper.isLevelChanged(items, position))

    override fun onBindViewHolder(items: MutableList<EducationItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        val educationHolder: EducationViewHolder? = holder as? EducationViewHolder

        educationHolder?.bind(
            items[position],
            itemViewClickListener
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup): EducationViewHolder {
        return EducationViewHolder(parent.viewBinding(AdapterEducationBinding::inflate))
    }
}
