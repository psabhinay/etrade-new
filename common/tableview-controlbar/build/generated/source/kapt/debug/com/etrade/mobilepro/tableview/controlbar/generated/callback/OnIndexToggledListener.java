package com.etrade.mobilepro.tableview.controlbar.generated.callback;
public final class OnIndexToggledListener implements com.etrade.mobilepro.ui.multitoggle.MultiToggle.OnIndexToggledListener {
    final Listener mListener;
    final int mSourceId;
    public OnIndexToggledListener(Listener listener, int sourceId) {
        mListener = listener;
        mSourceId = sourceId;
    }
    @Override
    public void onIndexToggled(int callbackArg_0) {
        mListener._internalCallbackOnIndexToggled(mSourceId , callbackArg_0);
    }
    public interface Listener {
        void _internalCallbackOnIndexToggled(int sourceId , int callbackArg_0);
    }
}