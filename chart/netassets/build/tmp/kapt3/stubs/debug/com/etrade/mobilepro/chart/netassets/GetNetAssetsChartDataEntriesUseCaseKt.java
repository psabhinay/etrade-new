package com.etrade.mobilepro.chart.netassets;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0080T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0003"}, d2 = {"NUMBER_OF_MONTHS", "", "NUMBER_OF_POINTS", "netassets_debug"})
public final class GetNetAssetsChartDataEntriesUseCaseKt {
    public static final int NUMBER_OF_MONTHS = 12;
    private static final int NUMBER_OF_POINTS = 6;
}