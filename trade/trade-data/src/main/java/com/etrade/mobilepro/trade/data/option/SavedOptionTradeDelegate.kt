package com.etrade.mobilepro.trade.data.option

import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.customorder.CustomOrderApiClient
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.util.ValueCoder
import com.squareup.moshi.Moshi

internal class SavedOptionTradeDelegate(
    apiClient: CustomOrderApiClient,
    moshi: Moshi,
    tradeFormInputId: OptionTradeFormInputId,
    tradeLegsCoder: ValueCoder<List<TradeLeg>>,
    isTablet: Boolean
) : BaseOptionTradeDelegate(apiClient, moshi, tradeFormInputId, tradeLegsCoder, isTablet) {

    override suspend fun CustomOrderApiClient.previewOrderCall(body: String) = editPreviewOrder(body)

    override suspend fun CustomOrderApiClient.placeOrderCall(body: String) = editPlaceOrder(body)
}
