package com.etrade.mobilepro.marketscalendar

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.RestoreTabPositionDelegate
import com.etrade.mobilepro.marketscalendar.adapter.MarketsCalendarPagerAdapter
import com.etrade.mobilepro.marketscalendar.adapter.MarketsCalendarTab
import com.etrade.mobilepro.marketscalendar.databinding.FragmentMarketsCalendarBinding
import com.etrade.mobilepro.tracking.initTabsTracking
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

@RequireLogin
class MarketsCalendarFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val restoreTabPositionDelegate: RestoreTabPositionDelegate
) : Fragment(R.layout.fragment_markets_calendar) {

    private val binding by viewBinding(FragmentMarketsCalendarBinding::bind)
    private val viewModel: MarketsCalendarViewModel by viewModels { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer { viewState ->
                if (viewState.isAuthenticated) {
                    binding.apply {
                        viewpager.visibility = View.VISIBLE
                        viewpager.isSaveEnabled = false
                        calendarTabToggle.visibility = View.VISIBLE
                        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
                            viewpager.initTabsTracking(viewModel, calendarTabToggle)
                            viewModel.trackTab(0)
                        }
                        viewpager.adapter = MarketsCalendarPagerAdapter(
                            this@MarketsCalendarFragment,
                            listOf(MarketsCalendarTab.Earnings, MarketsCalendarTab.Events)
                        )

                        restoreTabPositionDelegate.init(this@MarketsCalendarFragment, viewpager)
                    }
                } else {
                    binding.apply {
                        calendarTabToggle.visibility = View.GONE
                        viewpager.visibility = View.GONE
                        viewpager.adapter = null
                    }
                }
            }
        )
    }
}
