package com.etrade.mobilepro.chart.api

class ChartLine(
    val chartId: String,
    val points: List<ChartPoint>
)
