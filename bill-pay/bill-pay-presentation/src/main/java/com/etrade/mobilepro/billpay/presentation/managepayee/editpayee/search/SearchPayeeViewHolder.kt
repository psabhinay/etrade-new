package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.search

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.billpay.api.SearchPayeeResultItem
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayItemSearchBinding
import com.etrade.mobilepro.util.android.extension.inflater

internal class SearchPayeeViewHolder(
    private val binding: BillPayItemSearchBinding,
    private val clickListener: (SearchPayeeResultItem) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    constructor(parent: ViewGroup, clickListener: (SearchPayeeResultItem) -> Unit) : this(
        BillPayItemSearchBinding.inflate(parent.inflater, parent, false), clickListener
    )

    private var _item: SearchPayeeResultItem? = null

    init {
        binding.root.setOnClickListener {
            _item?.let { clickListener.invoke(it) }
        }
    }

    internal fun bind(item: SearchPayeeResultItem) {
        _item = item
        binding.searchPayeeName.text = item.name
        if (item.category.isNullOrBlank()) {
            binding.searchPayeeDataPoints.visibility = View.GONE
        } else {
            binding.searchPayeeDataPoints.visibility = View.VISIBLE
            binding.searchPayeeDataPoints.text = item.category
        }
    }
}
