package com.etrade.mobilepro.trade.data.customorder

import com.etrade.eo.rest.retrofit.Scalar
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

internal interface CustomOrderApiClient {

    @JsonMoshi
    @POST("/app/customorder/preview.json")
    @Headers("Content-Type: application/json")
    suspend fun previewOrder(
        @Scalar @Body body: String
    ): ServerResponseDto<CustomOrderPreviewResponseDto>

    @JsonMoshi
    @POST("/app/customorder/place.json")
    @Headers("Content-Type: application/json")
    suspend fun placeOrder(
        @Scalar @Body body: String
    ): ServerResponseDto<CustomOrderPlaceResponseDto>

    @JsonMoshi
    @POST("/app/customorder/changepreview.json")
    @Headers("Content-Type: application/json")
    suspend fun editPreviewOrder(
        @Scalar @Body request: String
    ): ServerResponseDto<CustomOrderEditPreviewResponseDto>

    @JsonMoshi
    @POST("/app/customorder/change.json")
    @Headers("Content-Type: application/json")
    suspend fun editPlaceOrder(
        @Scalar @Body request: String
    ): ServerResponseDto<CustomOrderEditPlaceResponseDto>
}
