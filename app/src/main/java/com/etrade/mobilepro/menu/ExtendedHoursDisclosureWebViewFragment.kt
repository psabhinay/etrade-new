package com.etrade.mobilepro.menu

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.webkit.WebView
import androidx.activity.OnBackPressedCallback
import androidx.core.app.ActivityCompat
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.OnNavigateUpListener
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.dialog.viewmodel.DialogViewModel
import com.etrade.mobilepro.formsandapplications.presentation.AccountServicesWebChromeClient
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

const val URL_AFTER_DISCLOSURE_AGREEMENT = "e/t/estation/pricing"

private const val REQUEST_CODE_BACK_MESSAGE_DIALOG = 1235
private val BACK_MESSAGE_DIALOG_TAG =
    "${ExtendedHoursDisclosureWebViewFragment::class.java.canonicalName}_back_message_dialog_tag"

class ExtendedHoursDisclosureWebViewFragment @Inject constructor(
    private val dialogViewModel: DialogViewModel,
    private val user: User,
    @DeviceType deviceType: String,
    snackbarUtilFactory: SnackbarUtilFactory
) : WebViewFragment(deviceType), OnNavigateUpListener {

    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    private val pageFinishedHandler = { _: WebView?, url: String? ->
        if (isAdded && url?.contains(URL_AFTER_DISCLOSURE_AGREEMENT, true) == true) {
            findNavController().popBackStack()
            dialogViewModel.dismissDialog()
        }
    }

    private val webChromeClient by lazy {
        AccountServicesWebChromeClient(
            context = requireContext(),
            registry = requireActivity().activityResultRegistry,
            lifecycleOwner = viewLifecycleOwner,
            requestPermissionCallback = { permission, requestCode ->
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(permission), requestCode)
            },
            errorCallback = {
                snackbarUtil.errorSnackbar()
            }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun setupWebView(webView: WebView) {
        super.setupWebView(webView)
        webView.webViewClient = ExtendedHoursDisclosureWebClient(pageFinishedHandler)
        webView.webChromeClient = webChromeClient
    }

    override fun onNavigateUp(): Boolean {
        checkExtendedHoursAccepted()
        return true
    }

    private fun checkExtendedHoursAccepted() {
        if (user.isExtendedHoursAccepted) {
            findNavController().popBackStack()
            return
        }
        showBackMessageDialog()
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                REQUEST_CODE_BACK_MESSAGE_DIALOG -> findNavController().popBackStack()
            }
        }
    }

    private fun setupBackPressed() {
        activity?.onBackPressedDispatcher?.addCallback(
            viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    checkExtendedHoursAccepted()
                }
            }
        )
    }

    private fun showBackMessageDialog() {
        val dialog = CustomDialogFragment.newInstance(
            message = getString(R.string.menu_extended_hours_back_message),
            okTitle = getString(R.string.ok),
            requestCode = REQUEST_CODE_BACK_MESSAGE_DIALOG
        )
        dialog.show(parentFragmentManager, BACK_MESSAGE_DIALOG_TAG)
    }

    inner class ExtendedHoursDisclosureWebClient(
        private val pageFinished: ((wv: WebView?, url: String?) -> Unit)? = null
    ) : WebViewClientImpl() {

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            pageFinished?.invoke(view, url)
        }
    }
}
