package com.etrade.mobilepro.instrument.dao

import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.util.safeParseBigDecimal
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.math.BigDecimal

@SuppressWarnings("LongParameterList")
open class InstrumentRealmObject(
    @PrimaryKey
    override var symbol: String = "",
    override var symbolDescription: String = "",
    override var displaySymbol: String = "",
    var instrumentId: String = "",
    var typeCodeRaw: String = "",
    var exchangeCode: String = "",
    var bidRaw: String = "",
    var askRaw: String = "",
    var daysChangeVal: String = "",
    var daysChangePer: String = "",
    var volumeRaw: String = "",
    var marketCapRaw: String = "",
    var pe: String = "",
    var eps: String = "",
    var lastPriceRaw: String = "",
    var tickIndicatorRaw: String = "",
    var week52High: String = "",
    var week52Low: String = "",
    var impliedVolatilityPct: String = "",
    var openInterest: String = "",
    var deltaRaw: String = "",
    var premium: String = "",
    var gammaRaw: String = "",
    var vegaRaw: String = "",
    var thetaRaw: String = "",
    var expirationDate: String = "",
    var underlyingTypeCode: String = "",
    var underlyingExchangeCode: String = "",
    var underlyingSymbol: String = "",
    var daysExpiration: String = "",
    var prevClose: String = ""
) : RealmObject(), Instrument {

    override val priceEarningRation: BigDecimal?
        get() = pe.safeParseBigDecimal()

    override val earningsPerShare: BigDecimal?
        get() = eps.safeParseBigDecimal()

    override val fiftyTwoWeekHigh: BigDecimal?
        get() = week52High.safeParseBigDecimal()

    override val fiftyTwoWeekLow: BigDecimal?
        get() = week52Low.safeParseBigDecimal()

    override val daysChange: BigDecimal?
        get() = daysChangeVal.safeParseBigDecimal()

    override val daysChangePercentage: BigDecimal?
        get() = daysChangePer.safeParseBigDecimal()

    override val impliedVolatilityPercentage: BigDecimal?
        get() = impliedVolatilityPct.safeParseBigDecimal()

    override val daysToExpire: Int?
        get() = daysExpiration.toIntOrNull()

    override val instrumentType: InstrumentType
        get() = InstrumentType.from(typeCodeRaw)

    override val ask: BigDecimal?
        get() = askRaw.safeParseBigDecimal()

    override val bid: BigDecimal?
        get() = bidRaw.safeParseBigDecimal()

    override val lastPrice: BigDecimal?
        get() = lastPriceRaw.safeParseBigDecimal()

    override val tickIndicator: Short?
        get() = tickIndicatorRaw.toShortOrNull()

    override val marketCap: BigDecimal?
        get() = marketCapRaw.safeParseBigDecimal()

    override val volume: BigDecimal?
        get() = volumeRaw.safeParseBigDecimal()

    override val delta: BigDecimal?
        get() = deltaRaw.safeParseBigDecimal()

    override val gamma: BigDecimal?
        get() = gammaRaw.safeParseBigDecimal()

    override val theta: BigDecimal?
        get() = thetaRaw.safeParseBigDecimal()

    override val vega: BigDecimal?
        get() = vegaRaw.safeParseBigDecimal()

    override val underlyingSymbolInfo: WithSymbolInfo?
        get() = Symbol(underlyingSymbol, InstrumentType.from(underlyingTypeCode))

    override val previousClose: BigDecimal?
        get() = prevClose.safeParseBigDecimal()

    @Suppress("LongParameterList", "LongMethod")
    fun copy(
        instrumentId: String = this.instrumentId,
        symbol: String = this.symbol,
        symbolDescription: String = this.symbolDescription,
        displaySymbol: String = this.displaySymbol,
        typeCodeRaw: String = this.typeCodeRaw,
        exchangeCode: String = this.exchangeCode,
        bidRaw: String = this.bidRaw,
        askRaw: String = this.askRaw,
        daysChangeVal: String = this.daysChangeVal,
        daysChangePer: String = this.daysChangePer,
        volumeRaw: String = this.volumeRaw,
        marketCapRaw: String = this.marketCapRaw,
        pe: String = this.pe,
        eps: String = this.eps,
        lastPriceRaw: String = this.lastPriceRaw,
        tickIndicatorRaw: String = this.tickIndicatorRaw,
        week52High: String = this.week52High,
        week52Low: String = this.week52Low,
        impliedVolatilityPct: String = this.impliedVolatilityPct,
        openInterest: String = this.openInterest,
        deltaRaw: String = this.deltaRaw,
        premium: String = this.premium,
        gammaRaw: String = this.gammaRaw,
        vegaRaw: String = this.vegaRaw,
        thetaRaw: String = this.thetaRaw,
        expirationDate: String = this.expirationDate,
        underlyingTypeCode: String = this.underlyingTypeCode,
        underlyingExchangeCode: String = this.underlyingExchangeCode,
        underlyingSymbol: String = this.underlyingSymbol,
        daysExpiration: String = this.daysExpiration,
        previousClose: String = prevClose
    ): InstrumentRealmObject {
        return InstrumentRealmObject(
            instrumentId = instrumentId,
            symbol = symbol,
            symbolDescription = symbolDescription,
            displaySymbol = displaySymbol,
            typeCodeRaw = typeCodeRaw,
            exchangeCode = exchangeCode,
            bidRaw = bidRaw,
            askRaw = askRaw,
            daysChangeVal = daysChangeVal,
            daysChangePer = daysChangePer,
            volumeRaw = volumeRaw,
            marketCapRaw = marketCapRaw,
            pe = pe,
            eps = eps,
            lastPriceRaw = lastPriceRaw,
            tickIndicatorRaw = tickIndicatorRaw,
            week52High = week52High,
            week52Low = week52Low,
            impliedVolatilityPct = impliedVolatilityPct,
            openInterest = openInterest,
            deltaRaw = deltaRaw,
            premium = premium,
            gammaRaw = gammaRaw,
            vegaRaw = vegaRaw,
            thetaRaw = thetaRaw,
            expirationDate = expirationDate,
            underlyingTypeCode = underlyingTypeCode,
            underlyingExchangeCode = underlyingExchangeCode,
            underlyingSymbol = underlyingSymbol,
            daysExpiration = daysExpiration,
            prevClose = previousClose
        )
    }
}
