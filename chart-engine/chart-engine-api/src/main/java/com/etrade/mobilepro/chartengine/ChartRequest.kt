package com.etrade.mobilepro.chartengine
/**
 * ChartRequest is used to generate a string in the following format used by pchart service.
 * 0:0.COMP.IDX.d06.t1.s4.v0.o1.f1.rt.txt
 *
 * @param ticker a symbol's ticker such as "AAPL"
 * @param timeFrame used with isLandScape to determine a time period string.
 * @param chartType chart api supports line, bar, mountain, candlestick.
 * @param isOption whether the symbol you're fetching is option or not
 * @param isRealTime whether fetched chart data is delayed data or real time
 * @param isLandScape used with timeFrame to determine a time period string.
 */
data class ChartRequest(
    val ticker: String,
    val timeFrame: TimeFrame,
    val chartType: ChartType = ChartType.Line,
    val isOption: Boolean = false,
    val isRealTime: Boolean = false,
    val isLandScape: Boolean = false
) {
    fun getChartRequestString(): String {
        val request = StringBuilder()
        // First two values represent width and height of the chart if we fetch .png, 0:0 is fine for data
        // v0 and o1 are hard coded, not sure what they represent.
        request.append("0:0.")
            .append(ticker)
            .append(timeFrame.getPeriodString(isLandScape))
            .append(if (isOption) ".t2" else ".t1")
            .append(chartType.typeCode)
            .append(".v0").append(".o1")
            .append(getFrequencyCode(chartType, timeFrame))
            .append(if (isRealTime) ".rl" else ".dl")
        return request.toString()
    }
}
