package com.etrade.mobilepro.dynamicui

import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.dynamicui.api.DynamicLayout

open class DynamicUiLayoutRecyclerAdapter(
    lifecycleOwner: LifecycleOwner,
    values: List<DynamicLayout>
) : BaseRecyclerAdapter(lifecycleOwner) {

    @Suppress("VariableNaming")
    protected var _values = values

    open var values: List<DynamicLayout>
        get() = _values
        set(newValue) {
            _values = newValue
            notifyDataSetChanged()
        }

    override fun getObjForPosition(position: Int): Any? = values[position].viewModel

    override fun getLayoutIdForPosition(position: Int) = values[position].layoutId

    override fun getVariableIdForPosition(position: Int): Int = values[position].variableId

    override fun getItemCount() = values.size

    override fun removeItemAt(position: Int) {
        val updated = _values.toMutableList()
        updated.removeAt(position)
        _values = updated
        notifyItemRemoved(position)
    }
}
