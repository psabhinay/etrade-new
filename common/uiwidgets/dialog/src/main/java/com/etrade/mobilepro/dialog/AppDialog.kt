package com.etrade.mobilepro.dialog

import android.content.res.Resources
import androidx.annotation.StringRes

fun Resources.appDialog(init: ResourcesAppDialogBuilder.() -> Unit): AppDialog = ResourcesAppDialogBuilder(this).apply(init).create()

fun appDialog(init: AppDialogBuilder.() -> Unit): AppDialog = AppDialogBuilder().apply(init).create()

@DslMarker
annotation class DialogDsl

data class AppDialog internal constructor(
    val title: CharSequence,
    val message: CharSequence,
    val linkifyMessage: Boolean,
    val isCancelable: Boolean,
    val positiveAction: Action?,
    val negativeAction: Action?,
    val neutralAction: Action?,
    val cancelAction: (() -> Unit)?,
    val dismissAction: (() -> Unit)?
) {

    data class Action internal constructor(
        val label: CharSequence,
        val perform: (() -> Unit)?
    )
}

@DialogDsl
open class AppDialogBuilder internal constructor() {

    var title: CharSequence = ""
    var message: CharSequence = ""
    var cancelable: Boolean = false
    var linkifyMessage: Boolean = true

    var cancelAction: (() -> Unit)? = null
    var dismissAction: (() -> Unit)? = null

    protected var positiveAction: AppDialog.Action? = null
    protected var negativeAction: AppDialog.Action? = null
    protected var neutralAction: AppDialog.Action? = null

    open fun positiveAction(init: ActionBuilder.() -> Unit) {
        positiveAction = ActionBuilder().apply(init).create()
    }

    fun negativeAction(init: ActionBuilder.() -> Unit) {
        negativeAction = ActionBuilder().apply(init).create()
    }

    fun neutralAction(init: ActionBuilder.() -> Unit) {
        neutralAction = ActionBuilder().apply(init).create()
    }

    fun create(): AppDialog = AppDialog(title, message, linkifyMessage, cancelable, positiveAction, negativeAction, neutralAction, cancelAction, dismissAction)

    @DialogDsl
    open class ActionBuilder {

        var label: CharSequence = ""
        var action: (() -> Unit)? = null

        fun create(): AppDialog.Action = AppDialog.Action(label, action)
    }
}

class ResourcesAppDialogBuilder(private val resources: Resources) : AppDialogBuilder() {

    @StringRes
    var titleRes: Int = 0
        set(value) {
            field = value
            title = resources.getString(value)
        }

    @StringRes
    var messageRes: Int = 0
        set(value) {
            field = value
            message = resources.getString(value)
        }

    fun resourcesPositiveAction(init: ResourcesActionBuilder.() -> Unit) {
        positiveAction = ResourcesActionBuilder(resources).apply(init).create()
    }

    fun resourcesNegativeAction(init: ResourcesActionBuilder.() -> Unit) {
        negativeAction = ResourcesActionBuilder(resources).apply(init).create()
    }

    fun resourcesNeutralAction(init: ResourcesActionBuilder.() -> Unit) {
        neutralAction = ResourcesActionBuilder(resources).apply(init).create()
    }

    class ResourcesActionBuilder(private val resources: Resources) : ActionBuilder() {

        @StringRes
        var labelRes: Int = 0
            set(value) {
                field = value
                label = resources.getString(value)
            }
    }
}
