package com.etrade.mobilepro.chart.cache

import com.etrade.mobilepro.chart.ChartRealmModule
import com.etrade.mobilepro.chart.dto.ChartDataDto
import com.etrade.mobilepro.chart.rest.PchartApiClient
import com.etrade.mobilepro.chart.rest.PchartEndpointType
import com.etrade.mobilepro.chart.util.getShouldCacheTime
import com.etrade.mobilepro.chartengine.ChartOHLCData
import com.etrade.mobilepro.chartengine.ChartRequest
import com.etrade.mobilepro.chartengine.SymbolChartData
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.util.AuthenticationStatusProvider
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmList
import retrofit2.Retrofit

open class BaseCachedChartDataSourceDelegate(
    private val authStatusProvider: AuthenticationStatusProvider,
    @MobileTrade retrofit: Retrofit
) {

    protected val apiClient: PchartApiClient = retrofit.create(PchartApiClient::class.java)
    protected val realmConfig: RealmConfiguration = RealmConfiguration.Builder()
        .name("et.charts")
        .modules(ChartRealmModule())
        .build()

    protected fun getEndpointType() = if (authStatusProvider.isAuthenticated()) {
        PchartEndpointType.SECURED
    } else {
        PchartEndpointType.PUBLIC
    }

    protected fun fetchSymbolChartDataFromDb(request: ChartRequest, realm: Realm): SymbolChartData {
        val chartDataFromDb = realm.where(SymbolChartDataDb::class.java)
            .equalTo("queryValue", request.getChartRequestString())
            .findFirst()?.also { realm.copyFromRealm(it) } ?: throw NoSuchElementException()
        return chartDataFromDb.mapToSymbolChartData()
    }

    protected fun updateSymbolChartDataToDb(fetchedResult: SymbolChartData, realm: Realm): SymbolChartDataDb {
        val chartDataToSave =
            realm.where(SymbolChartDataDb::class.java).equalTo("queryValue", fetchedResult.queryValue).findFirst() ?: realm.createObject(
                SymbolChartDataDb::class.java,
                fetchedResult.queryValue
            )
        chartDataToSave.cachedAt = fetchedResult.cachedAt
        chartDataToSave.shouldCachedTimeInSeconds = fetchedResult.shouldCachedTimeInSeconds
        chartDataToSave.yesterdaysClose = fetchedResult.yesterdaysClose
        val realmList = RealmList<ChartOHLCDataDb>()
        realmList.addAll(
            fetchedResult.displayData.map {
                val chartOHLCDataDb = realm.createObject(ChartOHLCDataDb::class.java)
                chartOHLCDataDb.date = it.date
                chartOHLCDataDb.open = it.open
                chartOHLCDataDb.high = it.high
                chartOHLCDataDb.low = it.low
                chartOHLCDataDb.close = it.close
                chartOHLCDataDb.volume = it.volume
                chartOHLCDataDb
            }
        )
        chartDataToSave.chartOHLCDataList.deleteAllFromRealm()
        chartDataToSave.chartOHLCDataList = realmList
        return chartDataToSave
    }

    protected fun ChartDataDto.mapToSymbolChartData(chartRequest: ChartRequest) = SymbolChartData(
        queryValue = chartRequest.getChartRequestString(),
        displayData = displayData.map {
            ChartOHLCData(
                date = it.date,
                open = it.open,
                high = it.high,
                low = it.low,
                close = it.close,
                volume = it.volume
            )
        },
        yesterdaysClose = yesterdaysClose.close,
        cachedAt = System.currentTimeMillis(),
        shouldCachedTimeInSeconds = if (displayData.isEmpty()) 0 else getShouldCacheTime(chartRequest)
    )

    private fun SymbolChartDataDb.mapToSymbolChartData() = SymbolChartData(
        queryValue = queryValue,
        displayData = chartOHLCDataList.map {
            ChartOHLCData(
                date = it.date,
                open = it.open,
                high = it.high,
                low = it.low,
                close = it.close,
                volume = it.volume
            )
        },
        yesterdaysClose = yesterdaysClose,
        cachedAt = cachedAt,
        shouldCachedTimeInSeconds = shouldCachedTimeInSeconds
    )
}
