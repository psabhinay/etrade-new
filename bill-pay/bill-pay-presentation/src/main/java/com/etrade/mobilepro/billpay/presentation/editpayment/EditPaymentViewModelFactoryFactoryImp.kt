package com.etrade.mobilepro.billpay.presentation.editpayment

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.presentation.paymentdetail.ParcelablePayment
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

interface EditPaymentViewModelFactoryFactory {
    fun create(
        validAccountList: List<AccountDetails>,
        selectedPayee: Payee?,
        originAccount: String?,
        currentPayment: ParcelablePayment?
    ): ViewModelProvider.Factory
}

class EditPaymentViewModelFactoryFactoryImp @Inject constructor(
    private val factory: EditPaymentViewModel.Factory
) : EditPaymentViewModelFactoryFactory {
    override fun create(
        validAccountList: List<AccountDetails>,
        selectedPayee: Payee?,
        originAccount: String?,
        currentPayment: ParcelablePayment?
    ): ViewModelProvider.Factory =
        viewModelFactory {
            factory.create(
                validAccountList,
                selectedPayee,
                originAccount,
                currentPayment
            )
        }
}
