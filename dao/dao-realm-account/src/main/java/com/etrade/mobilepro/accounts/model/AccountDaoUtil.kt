package com.etrade.mobilepro.accounts.model

import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.accounts.util.android.view.AccountsSpinnerModel
import com.etrade.mobilepro.accounts.util.android.view.getAccountViewType
import io.realm.Realm

fun getRealmAccountList() = RealmAccountDao(Realm.getDefaultInstance()).getAllAccountsNonAsync()

fun List<Account>.toAccountDropDownModel() = map {
    AccountsSpinnerModel(it.accountShortName, it.getAccountViewType(), it)
}
