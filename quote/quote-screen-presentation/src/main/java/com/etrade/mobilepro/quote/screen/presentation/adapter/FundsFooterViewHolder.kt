package com.etrade.mobilepro.quote.screen.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemFundsFooterBinding
import com.etrade.mobilepro.quoteapi.FundQuoteItem

class FundsFooterViewHolder(private val binding: QuoteItemFundsFooterBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bindTo(
        item: FundQuoteItem.QuoteFundsFooterItem? = null,
        fundDescriptionClickListener: ((description: String) -> Unit)? = null,
        fundReportClickListener: (() -> Unit)? = null,
        prospectusClickListener: (() -> Unit)? = null
    ) {
        binding.fundDescriptionTitle.setOnClickListener {
            item?.let {
                fundDescriptionClickListener?.invoke(item.descriptionText)
            }
        }
        binding.fundReportTitle.setOnClickListener {
            fundReportClickListener?.invoke()
        }
        binding.prospectusTitle.setOnClickListener {
            prospectusClickListener?.invoke()
        }
    }
}
