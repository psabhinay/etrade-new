package com.etrade.mobilepro.barcode.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.barcode.api.BarcodeItem
import com.etrade.mobilepro.barcode.databinding.AdapterBarcodeResultsBinding
import com.etrade.mobilepro.common.customview.SwipeRevealLayout
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import org.threeten.bp.format.DateTimeFormatter

class BarcodeResultAdapterDelegate(
    private val layoutInflater: LayoutInflater,
    private val itemViewClickListener: View.OnClickListener,
    private val deleteItemClickListener: View.OnClickListener,
    private val swipeActionListener: (BarcodeItem, Int, SwipeRevealLayout.Action) -> Unit,
    private val upcStringTemplate: String,
    private val dateFormat: DateTimeFormatter
) : AdapterDelegate<MutableList<BarcodeItem>>() {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return BarcodeResultViewHolder(
            AdapterBarcodeResultsBinding.inflate(layoutInflater, parent, false),
            itemViewClickListener,
            deleteItemClickListener,
            swipeActionListener,
            upcStringTemplate,
            dateFormat
        )
    }

    override fun isForViewType(items: MutableList<BarcodeItem>, position: Int): Boolean = true

    override fun onBindViewHolder(items: MutableList<BarcodeItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        holder
            .let { it as? BarcodeResultViewHolder }
            ?.bind(items[position])
    }
}
