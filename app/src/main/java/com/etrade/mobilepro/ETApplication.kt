package com.etrade.mobilepro

import android.app.ActivityManager
import android.app.Application
import android.content.Context
import android.os.Build
import android.webkit.WebView
import com.etrade.mobilepro.apiguard.ApiGuard
import com.etrade.mobilepro.appconfig.api.AppEnvironment
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.common.di.ApplicationScope
import com.etrade.mobilepro.dagger.ApplicationComponent
import com.etrade.mobilepro.dagger.ApplicationModule
import com.etrade.mobilepro.dagger.DaggerApplicationComponent
import com.etrade.mobilepro.di.dagger.core.DaggerComponentLocator
import com.etrade.mobilepro.di.dagger.core.DaggerComponentLocatorImpl
import com.etrade.mobilepro.di.dagger.core.DaggerComponentProvider
import com.etrade.mobilepro.eventtrackerapi.EventTrackerInterface
import com.etrade.mobilepro.feature.firebase.FirebaseFeatureFlagProvider
import com.etrade.mobilepro.localytics.integrateLocalytics
import com.etrade.mobilepro.menu.helper.DynamicMenuHelper
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.realm.RealmSetup
import com.etrade.mobilepro.themeselection.NightModePreferences
import com.etrade.mobilepro.util.android.extension.isApplicationDebuggable
import com.etrade.mobilepro.util.updater.AppUpdater
import com.jakewharton.threetenabp.AndroidThreeTen
import com.localytics.android.CustomLocalyticsActivityLifecycleCallbacks
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import timber.log.Timber
import java.io.File
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(ETApplication::class.java)

class ETApplication : Application(), HasAndroidInjector, DaggerComponentProvider {

    @Inject
    @Volatile
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var activityLifecycleCallbacks: ActivityLifecycleCallbacks

    @Inject
    lateinit var appUpdater: AppUpdater

    @Inject
    lateinit var appConfigRepo: AppConfigRepo

    @Inject
    @ApplicationScope
    lateinit var appScope: CoroutineScope

    @Inject
    lateinit var dynamicMenuHelper: DynamicMenuHelper

    @Inject
    lateinit var firebaseFeatureFlagProvider: FirebaseFeatureFlagProvider

    @Inject
    lateinit var nightModePreferences: NightModePreferences

    @Inject
    lateinit var localyticsActivityLifecycleCallbacks: CustomLocalyticsActivityLifecycleCallbacks

    @Inject
    lateinit var eventTracker: EventTrackerInterface

    @Inject
    lateinit var appPreferences: ApplicationPreferences

    lateinit var binder: ApplicationComponent

    val daggerComponentLocator: DaggerComponentLocator = DaggerComponentLocatorImpl()

    override fun onCreate() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        super.onCreate()
        AndroidThreeTen.init(this)

        getProcessName(this)?.let {
            // Prevents the WebView bug for Android 9+
            // See https://developer.android.com/about/versions/pie/android-9.0-changes-28#framework-security-changes
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                WebView.setDataDirectorySuffix(it.replace(File.pathSeparator, "_", false))
            }

            if (it == packageName) {
                initMainProcess()
            }
        } ?: initMainProcess()

        // See https://developer.android.com/about/versions/pie/android-9.0-changes-28#framework-security-changes
        // to fix a WebView crash on OS11
        if (isApplicationDebuggable()) {
            WebView.setWebContentsDebuggingEnabled(true)
        }
        setRxUndeliverableExceptionHandler()

        try {
            eventTracker.setupTracker(appPreferences.shouldTrackWithAppsFlyer, this)
        } catch (ex: Exception) {
            logger.error("eventTracker.setupTracker exception: ", ex)
        }
    }

    /**
     * Put whatever needs to happen when the application is created here
     *
     * Because ETApplication.onCreate() runs whenever a new process starts (like MiSnap) we need to be careful about what goes in onCreate()
     */
    private fun initMainProcess() {
        RealmSetup(this).configureRealm()

        binder = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()

        binder.inject(this)
        nightModePreferences.initTheme()
        initApiGuard()
        integrateLocalytics(this, localyticsActivityLifecycleCallbacks)
        // this runs on main thread intentionally to make sure that migration is done before we try to access any data
        appUpdater.update()
        dynamicMenuHelper.prefetchDynamicMenuItems()
        registerActivityLifecycleCallbacks(activityLifecycleCallbacks)
        appScope.launch { firebaseFeatureFlagProvider.initializeRemoteConfig() }
    }

    private fun initApiGuard() {
        appConfigRepo.getAppConfig().environment.let {
            when (it) {
                AppEnvironment.SIT,
                AppEnvironment.UAT,
                AppEnvironment.PROD,
                AppEnvironment.PLT -> ApiGuard.init(this, R.raw.apiguard, it.asString)
                else -> ApiGuard.init(this, R.raw.apiguard)
            }
        }
    }

    private fun getProcessName(context: Context): String? {
        val manager: ActivityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        return manager.runningAppProcesses?.find { it.pid == android.os.Process.myPid() }?.processName
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    override fun <T : Any> getComponent(componentClass: Class<T>): T? =
        daggerComponentLocator.getComponent(componentClass)

    private fun setRxUndeliverableExceptionHandler() {
        RxJavaPlugins.setErrorHandler { exception ->
            if (exception is UndeliverableException) {
                // If it couldn't be delivered, no one is subscribed. Ignore it
                logger.error("Undeliverable Exception: ", exception)
            } else {
                logger.error("Exception: ", exception)
                Thread.currentThread().uncaughtExceptionHandler?.uncaughtException(Thread.currentThread(), exception)
            }
        }
    }
}
