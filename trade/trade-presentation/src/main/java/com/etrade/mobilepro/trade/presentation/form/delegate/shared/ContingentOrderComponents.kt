package com.etrade.mobilepro.trade.presentation.form.delegate.shared

import android.content.Context
import android.content.res.Resources
import androidx.lifecycle.map
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.ExternalAction
import com.etrade.mobilepro.dynamic.form.api.FormBuilder
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.TextInputType
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.form.api.input.ContingentOrderInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithContingentOrderInputId
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter

private const val CONDITION_VALUE_MAX_FRACTION_DIGITS = 5

private typealias ContingentOrderExternalActionProvider = (SymbolInputParams) -> ExternalAction

private fun GroupBuilder.externalActionAndSymbolInputParams(
    parameter: CreateTradeFormParameter,
    inputId: ContingentOrderInputId,
    resources: Resources,
    externalActionProvider: ContingentOrderExternalActionProvider
): Pair<SymbolInputParams, ExternalAction> {
    val symbolInputId = inputId.symbol
    val symbolInputParams = SymbolInputParams(InstrumentType.validOptionsTradingTypes, symbolInputId, inputId.symbolCoder, showOwnedSymbols = false) {
        requireAnyInput(symbolInputId).resetValue()
        parameter.onActionClickListener.showNotEligibleSymbolDialog(resources, R.string.trade_error_invalid_stock_etf_symbol)
    }
    return symbolInputParams to externalActionProvider(symbolInputParams)
}

internal fun <T> GroupBuilder.contingentOrderForm(
    context: Context,
    inputId: T,
    parameter: CreateTradeFormParameter,
    externalActionProvider: ContingentOrderExternalActionProvider
) where T : WithAdvancedTradeFormInputId, T : WithContingentOrderInputId, T : WithSymbolTradeFormInputId {
    val (symbolInputParams, externalAction) = externalActionAndSymbolInputParams(
        parameter, inputId.contingentOrderInputId, context.resources, externalActionProvider
    )
    verticalGroup {
        symbol(context.resources, symbolInputParams, externalAction)
        underlierQuoteWidget(inputId.contingentOrderInputId.symbol, parameter.advancedOrderQuoteWidgetViewModel)
        optionsBidAskWidget(inputId.contingentOrderInputId.symbol, parameter.advancedOrderBidAskQuoteWidgetViewModel)
        orderQualifiers(context.resources, inputId)
        orderCondition(context.resources, inputId)
        conditionValue(context.resources, inputId, CONDITION_VALUE_MAX_FRACTION_DIGITS)

        visibleIf {
            inputValue<DropDownOption?>(inputId.advancedOrderType).map {
                it?.id == inputId.advancedOrderTypeValueId.contingent
            }
        }
    }
}

internal fun FormBuilder.contingentOrderHiddenInputs(inputId: WithContingentOrderInputId) {
    hidden(inputId.contingentOrderInputId.contingentSymbol)
}

internal fun <T> RuleListBuilder.observeContingentSymbolChanges(inputId: T)
        where T : WithSymbolTradeFormInputId, T : WithContingentOrderInputId {
    rule {
        inputValue<WithSymbolInfo>(inputId.contingentOrderInputId.symbol).observeForever { symbol ->
            symbol?.let {
                val value = inputId.symbolCoder.encode(it)
                // this is required by backend and we copy the value of contingent order symbol
                requireAnyInput(inputId.contingentOrderInputId.contingentSymbol).setValue(value)
            }
        }
    }
}

private fun GroupBuilder.orderQualifiers(resources: Resources, inputId: WithContingentOrderInputId) {
    select(inputId.contingentOrderInputId.qualifier) {
        title = resources.getString(R.string.trade_contingent_order_qualifiers)
        options {
            val qualifierValueId = inputId.contingentOrderInputId.qualifierValueId
            option(qualifierValueId.lastPrice) { title = resources.getString(R.string.trade_contingent_order_qualifiers_last_price) }
            option(qualifierValueId.bidPrice) { title = resources.getString(R.string.trade_contingent_order_qualifiers_bid_price) }
            option(qualifierValueId.askPrice) { title = resources.getString(R.string.trade_contingent_order_qualifiers_ask_price) }
        }
    }
}

private fun GroupBuilder.orderCondition(resources: Resources, inputId: WithContingentOrderInputId) {
    select(inputId.contingentOrderInputId.condition) {
        title = resources.getString(R.string.trade_contingent_order_condition)
        options {
            val conditionValueId = inputId.contingentOrderInputId.conditionValueId
            option(conditionValueId.lessThanOrEqualTo) { title = resources.getString(R.string.contingent_order_condition_less_than) }
            option(conditionValueId.greaterThanOrEqualTo) { title = resources.getString(R.string.contingent_order_condition_greater_than) }
        }
    }
}

private fun GroupBuilder.conditionValue(resources: Resources, inputId: WithContingentOrderInputId, maxFractionDigits: Int) {
    text(inputId.contingentOrderInputId.conditionValue) {
        title = resources.getString(R.string.trade_contingent_order_value)
        textInputType = TextInputType.Decimal(maxFractionDigits)
    }
}
