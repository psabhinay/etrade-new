package com.etrade.mobilepro.tracking.trackers

import android.app.Application
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.AnalyticsParams
import com.localytics.androidx.Customer
import com.localytics.androidx.Localytics
import com.localytics.androidx.LocalyticsActivityLifecycleCallbacks

private const val METHOD_NAME = "Native"

class LocalyticsTracker(application: Application) : Tracker {

    init {
        application.registerActivityLifecycleCallbacks(LocalyticsActivityLifecycleCallbacks(true))
        Localytics.integrate(application)
    }

    override fun logIn(customerId: String, paramsTrack: AnalyticsParams) {
        Localytics.tagCustomerLoggedIn(
            Customer.Builder().setCustomerId(customerId).build(),
            METHOD_NAME,
            paramsTrack.params
        )
    }

    override fun event(paramsTrack: AnalyticsParams) {
        Localytics.tagEvent(paramsTrack.name, paramsTrack.params)
    }

    override fun screen(screen: String, paramsTrack: AnalyticsParams) {
        Localytics.tagScreen(screen)
        Localytics.tagEvent(paramsTrack.name, paramsTrack.params)
    }
}
