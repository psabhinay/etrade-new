package com.etrade.mobilepro.util.json

import com.squareup.moshi.Json
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test
import java.math.BigInteger

internal class BigIntegerJsonAdapterTest {

    private val sut = BigIntegerJsonAdapter()

    private val number = "12345678912345679"

    private val moshi = Moshi.Builder().add(sut).build()

    private val adapter: JsonAdapter<TestObject> = moshi.adapter(TestObject::class.java)

    @Test
    fun `parses a non null integer`() {
        val input =
            """ { "test": $number } """
        assertThat(adapter.fromJson(input)?.test, `is`(BigInteger(number)))
    }

    @Test
    fun `parses a null integer`() {
        val input =
            """ { "test": null } """
        assertThat(adapter.fromJson(input)?.test, nullValue())
    }

    @Test
    fun `serializes non null integer`() {
        val json = adapter.toJson(TestObject(BigInteger(number)))
        assertThat(json, `is`("""{"test":$number}"""))
    }

    @Test
    fun `serializes null integer`() {
        val json = adapter.toJson(TestObject(null))
        assertThat(json, `is`("{}"))
    }

    @JsonClass(generateAdapter = true)
    internal class TestObject(
        @Json(name = "test")
        val test: BigInteger?
    )
}
