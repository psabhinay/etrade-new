package com.etrade.mobilepro.inboxmessages.dynamicui.api

import com.etrade.mobile.accounts.dto.AccountDto

interface WithAccountDtoInfo {
    val account: AccountDto?
}
