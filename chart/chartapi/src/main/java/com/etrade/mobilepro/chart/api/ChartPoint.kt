package com.etrade.mobilepro.chart.api

/**
 * Represents raw data of a chart point.
 */
interface ChartPoint {

    /**
     * Raw x value.
     */
    val rawX: String

    /**
     * Raw y value.
     */
    val rawY: String
}
