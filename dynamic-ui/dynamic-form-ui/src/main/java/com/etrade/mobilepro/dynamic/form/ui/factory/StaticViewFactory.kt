package com.etrade.mobilepro.dynamic.form.ui.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.api.StaticText
import com.etrade.mobilepro.dynamic.form.ui.StaticTextView

object StaticViewFactory : ComponentViewFactory<StaticText, StaticTextView> {
    override fun createView(context: Context): StaticTextView =
        StaticTextView(context)
}
