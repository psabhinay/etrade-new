package com.etrade.mobilepro.screener.api.filter.etf

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class EtfRiskFilter(
    val bottom20: Boolean = false,
    val belowMid20: Boolean = false,
    val mid20: Boolean = false,
    val aboveMid20: Boolean = false,
    val top20: Boolean = false
) : ScreenerFilter {
    override fun isEmpty(): Boolean = !bottom20 && !belowMid20 && !mid20 && !aboveMid20 && !top20

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.ETF
}
