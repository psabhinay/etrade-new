package com.etrade.mobilepro.arr.data

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root
import org.simpleframework.xml.Text

@Root(strict = false, name = "ReportSearch")
data class AnalystReportDto constructor(
    @field:Element(name = "TotalMatches", required = false)
    var totalMatches: Int? = null,
    @field:ElementList(name = "Results")
    var results: List<ReportItemResult>? = null
)

@Root(strict = false, name = "ReportItem")
data class ReportItemResult constructor(
    @field:Element(name = "ReportAvailable", required = false)
    var reportAvailable: Int? = null,
    @field:Element(name = "Provider", required = false)
    var provider: String? = null,
    @field:Element(name = "ReportUrl", required = false)
    var reportUrl: String? = null,
    @field:Element(name = "ReportDate", required = false)
    var reportDate: String? = null
)

@Root(strict = false, name = "WSODResearchReportUrl")
class WSODResearchReportUrl {
    @field:Text
    var text: String? = null
}
