package com.etrade.mobilepro.dynamic.form.ui.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.api.DynamicText
import com.etrade.mobilepro.dynamic.form.ui.DynamicTextView

object DynamicTextViewFactory : ComponentViewFactory<DynamicText, DynamicTextView> {
    override fun createView(context: Context): DynamicTextView =
        DynamicTextView(context)
}
