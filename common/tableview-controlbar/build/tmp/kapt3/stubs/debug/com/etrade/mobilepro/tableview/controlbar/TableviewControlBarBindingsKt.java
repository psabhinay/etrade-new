package com.etrade.mobilepro.tableview.controlbar;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u0016\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0007\u00a8\u0006\u0005"}, d2 = {"bindDropdownText", "", "Lcom/google/android/material/button/MaterialButton;", "selectedText", "", "tableview-controlbar_debug"})
public final class TableviewControlBarBindingsKt {
    
    @androidx.databinding.BindingAdapter(value = {"dropdownText"})
    public static final void bindDropdownText(@org.jetbrains.annotations.NotNull()
    com.google.android.material.button.MaterialButton $this$bindDropdownText, @org.jetbrains.annotations.Nullable()
    java.lang.String selectedText) {
    }
}