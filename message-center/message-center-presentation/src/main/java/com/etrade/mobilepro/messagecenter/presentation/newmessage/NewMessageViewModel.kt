package com.etrade.mobilepro.messagecenter.presentation.newmessage

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.messagecenter.api.model.AccountHolderRequest
import com.etrade.mobilepro.messagecenter.api.model.ReferenceBook
import com.etrade.mobilepro.messagecenter.api.model.SendNewMessageRequest
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterReferenceBookDataSource
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterRepo
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.tracking.SCREEN_TITLE_NEW_MESSAGE
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.delegate.consumable
import com.etrade.mobilepro.util.executeBlocking
import kotlinx.coroutines.launch
import javax.inject.Inject

class NewMessageViewModel @Inject constructor(
    private val referenceBookDataSource: MessageCenterReferenceBookDataSource,
    private val messageCenterRepo: MessageCenterRepo,
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase,
    private val streamingStatusController: StreamingStatusController,
    private val resources: Resources
) : ViewModel(), ScreenViewModel {

    override val screenName: String? = SCREEN_TITLE_NEW_MESSAGE

    private val selectedAccount: MutableLiveData<ReferenceBook.Account> = MutableLiveData()

    private val accountHoldersRetry = MutableLiveData<Unit>().apply { value = Unit }

    private val accountHolderFirstNameLastName: LiveData<String?> = selectedAccount
        .distinctUntilChanged()
        .combineLatest(accountHoldersRetry)
        .switchMap { (account, _) ->
            liveData(context = viewModelScope.coroutineContext) {
                messageCenterRepo.getAccountHoldersFirstNameLastName(
                    AccountHolderRequest(
                        acctNo = account.acctNo,
                        instNo = account.instNo,
                        instName = account.instName,
                        name = account.name
                    )
                )
                    .onSuccess { emit(it) }
                    .onFailure { emit(null) }
            }
        }

    private val _viewState = MediatorLiveData<ViewState>().apply {
        addSource(selectedAccount) {
            value = value?.copy(selectedAccount = it)
        }
        addSource(accountHolderFirstNameLastName) {
            val status = if (it?.isNotBlank() == true) {
                ViewState.Status.Idle
            } else {
                ViewState.Status.Error { accountHoldersRetry.value = Unit }
            }

            value = value?.copy(accountHolderFirstNameLastName = it, status = status)
        }
    }

    internal val viewState: LiveData<ViewState>
        get() = _viewState
    private var referenceBook: ReferenceBook? = null

    internal val accountsData: LiveData<List<DropDownMultiSelector.Selectable<ReferenceBook.Account>>> = Transformations.map(viewState) { viewState ->
        referenceBook?.let {
            it.accounts.map { account ->
                DropDownMultiSelector.Selectable(
                    title = account.shortDescription,
                    value = account,
                    selected = account == viewState.selectedAccount,
                    contentDescriptionFormatter = { resources.accountNameContentDescription(account.shortDescription) ?: account.shortDescription }
                )
            }
        }
    }

    internal val subjectsData: LiveData<List<DropDownMultiSelector.Selectable<ReferenceBook.Subject>>> = Transformations.map(viewState) { viewState ->
        referenceBook?.let {
            it.subjects.map { subject ->
                DropDownMultiSelector.Selectable(
                    title = subject.desc,
                    value = subject,
                    selected = subject == viewState.selectedSubject
                )
            }
        }
    }

    internal val topicsData: LiveData<List<DropDownMultiSelector.Selectable<ReferenceBook.Topic>>> = Transformations.map(viewState) { viewState ->
        referenceBook?.let {
            it.getSubjectTopics(viewState.selectedSubject)?.map { topic ->
                DropDownMultiSelector.Selectable(
                    title = topic.desc,
                    value = topic,
                    selected = topic == viewState.selectedTopic
                )
            }
        }
    }

    internal fun refresh(forced: Boolean) {
        val currentState = _viewState.value
        if (currentState == null || forced) {
            _viewState.value = currentState?.copy(status = ViewState.Status.Loading) ?: ViewState(status = ViewState.Status.Loading)
            viewModelScope.launch {
                val refBook = referenceBookDataSource.getCachedReferenceBook() ?: referenceBookDataSource.fetchReferenceBook().getOrNull()
                if (refBook == null) {
                    _viewState.updateValue { it.copy(status = ViewState.Status.Error(retry = { refresh(forced = true) })) }
                } else {
                    _viewState.updateValue { it.copy(status = ViewState.Status.Idle) }
                    selectedAccount.value = selectInitialAccount(refBook)
                }
                referenceBook = refBook
            }
        }
    }

    internal fun messageTextChanged(content: String?) = _viewState.updateValue { it.copy(content = content) }

    internal fun accountSelected(account: ReferenceBook.Account) {
        selectedAccount.value = account
    }

    internal fun subjectSelected(subject: ReferenceBook.Subject) = _viewState.updateValue { it.copy(selectedSubject = subject, selectedTopic = null) }

    internal fun topicSelected(topic: ReferenceBook.Topic) = _viewState.updateValue { it.copy(selectedTopic = topic) }

    internal fun sendNewMessage() {
        _viewState.updateValue { it.copy(status = ViewState.Status.Loading) }

        viewModelScope.launch {
            messageCenterRepo.sendNewMessage(
                SendNewMessageRequest(
                    selAcctHolder = requireNotNull(accountHolderFirstNameLastName.value),
                    accountId = requireNotNull(selectedAccount.value?.id),
                    subjectCode = requireNotNull(_viewState.value?.selectedSubject?.id),
                    topicCode = requireNotNull(_viewState.value?.selectedTopic?.id),
                    instName = requireNotNull(selectedAccount.value?.instName),
                    message = requireNotNull(_viewState.value?.content),
                    instNo = requireNotNull(selectedAccount.value?.instNo),
                    isStreamingOn = convertBooleanValueToSendNewMessageRequestValue(streamingStatusController.isStreamingToggleEnabled),
                    isExtendedHoursOn = convertBooleanValueToSendNewMessageRequestValue(isExtendedHoursOnUseCase.executeBlocking())
                )
            )
                .onSuccess {
                    _viewState.updateValue { it.copy(status = ViewState.Status.Finished) }
                }
                .onFailure {
                    _viewState.updateValue { it.copy(status = ViewState.Status.SendNewMessageFailed()) }
                }
        }
    }

    private fun selectInitialAccount(referenceBook: ReferenceBook): ReferenceBook.Account? = referenceBook.accounts.takeIf { it.size == 1 }?.first()

    private fun convertBooleanValueToSendNewMessageRequestValue(value: Boolean): String {
        return if (value) {
            "Y"
        } else {
            "N"
        }
    }

    internal data class ViewState(
        val status: Status = Status.Idle,
        val selectedAccount: ReferenceBook.Account? = null,
        val selectedSubject: ReferenceBook.Subject? = null,
        val selectedTopic: ReferenceBook.Topic? = null,
        val content: String? = null,
        val accountHolderFirstNameLastName: String? = null
    ) {
        sealed class Status {
            object Idle : Status()
            object Finished : Status()
            object Loading : Status()
            class Error(val retry: () -> Unit) : Status()
            class SendNewMessageFailed(showSnackBarMessage: Unit = Unit) : Status() {

                val consumableSnackBarMessage by consumable(showSnackBarMessage)
            }
        }

        fun isSendButtonEnabled(): Boolean = selectedAccount != null &&
            selectedSubject != null &&
            selectedTopic != null &&
            content?.isNotBlank() == true &&
            (status == Status.Idle || status is Status.SendNewMessageFailed) &&
            accountHolderFirstNameLastName?.isNotBlank() == true

        fun isSubjectEnabled(): Boolean =
            status == Status.Idle

        fun isTopicEnabled(): Boolean =
            selectedSubject != null &&
                (status == Status.Idle || status is Status.SendNewMessageFailed)
    }
}
