package com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class FundingPromptViewDto(
    @Json(name = "data")
    override val data: Any?,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?,
    @Json(name = "action")
    override val clickActionDto: ClickActionDto? = null
) : BaseScreenView()
