package com.etrade.mobilepro.screener.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.annotation.CallSuper
import androidx.core.widget.ContentLoadingProgressBar
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.EtfMarketsScreenerResultsAnalyticsParams
import com.etrade.mobilepro.tracking.params.MutualFundMarketsScreenerResultsAnalyticsParams
import com.etrade.mobilepro.tracking.params.StockMarketsScreenerResultsAnalyticsParams
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar

abstract class BaseFilterScreenFragment<F : ScreenerFilter, T : BaseFilterScreenViewModel.ViewState<F>>(
    protected val snackbarUtilFactory: SnackbarUtilFactory,
    private val screenersRouter: ScreenersRouter,
    private val tracker: Tracker
) : OverlayBaseFragment() {
    protected abstract val viewModel: BaseFilterScreenViewModel<F, T>
    protected abstract val screenerGroup: ScreenerType.Group
    internal open val checkableButtonsToEvents = mapOf<CheckableButton, ScreenerFilterEvent<F, T, Boolean>>()
    private var viewResultsBtn: Button? = null
    private var resultsLoadingPb: ContentLoadingProgressBar? = null
    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private var snackbar: Snackbar? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return inflater.inflate(layoutRes, container, false)
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        baseSetupFragment()
        baseBindViewModel()
        if (savedInstanceState == null) {
            viewModel.initialize()
        }
    }

    private fun baseSetupFragment() {
        viewResultsBtn = view?.findViewById(R.id.view_results_btn)
        resultsLoadingPb = view?.findViewById(R.id.results_loading_pb)
        viewResultsBtn?.setOnClickListener {
            when (screenerGroup) {
                ScreenerType.Group.STOCK -> {
                    screenersRouter.actionToStockResultsFragment(findNavController())
                    tracker.event(StockMarketsScreenerResultsAnalyticsParams())
                }
                ScreenerType.Group.MUTUAL_FUND -> {
                    screenersRouter.actionToMfResultsFragment(findNavController())
                    tracker.event(MutualFundMarketsScreenerResultsAnalyticsParams())
                }
                ScreenerType.Group.ETF -> {
                    screenersRouter.actionToEtfResultsFragment(findNavController())
                    tracker.event(EtfMarketsScreenerResultsAnalyticsParams())
                }
            }
        }
        checkableButtonsToEvents.forEach { entry ->
            entry.key.checkedStateListener = createDefaultCheckableButtonListener(event = entry.value)
        }
    }

    private fun baseBindViewModel() {
        viewModel.resultsState.observe(
            viewLifecycleOwner,
            Observer { applyPreScreenResults(it) }
        )
    }

    private fun applyPreScreenResults(results: Resource<Int>) {
        applyResultsCount(results.data, forceDisableButton = results is Resource.Loading)
        snackbar?.dismiss()
        when (results) {
            is Resource.Loading -> resultsLoadingPb?.show()
            is Resource.Success -> resultsLoadingPb?.hide()
            is Resource.Failed -> {
                snackbar = snackbarUtil.retrySnackbar {
                    viewModel.retryPrescreen()
                }
            }
        }
    }

    private fun applyResultsCount(count: Int?, forceDisableButton: Boolean) {
        viewResultsBtn?.apply {
            if (count == null) {
                isEnabled = false
                setText(R.string.view_results_btn_title)
            } else {
                text = getString(R.string.view_results_btn_title_with_count, count)
                isEnabled = !forceDisableButton && count in 1..MAX_RESULTS_TO_ENABLE_BUTTON
            }
        }
    }

    @Suppress("FunctionMaxLength")
    private fun createDefaultCheckableButtonListener(event: ScreenerFilterEvent<F, T, Boolean>): (Boolean, Boolean) -> Unit = { isChecked, byUser ->
        if (byUser) {
            viewModel.consumeEvent(event, isChecked)
        }
    }

    override fun onDestroyView() {
        viewResultsBtn = null
        resultsLoadingPb = null
        super.onDestroyView()
    }
}
