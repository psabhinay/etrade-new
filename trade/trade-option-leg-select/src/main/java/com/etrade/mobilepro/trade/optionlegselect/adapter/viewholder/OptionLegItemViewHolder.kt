package com.etrade.mobilepro.trade.optionlegselect.adapter.viewholder

import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.annotation.CallSuper
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.trade.optionlegselect.R
import com.etrade.mobilepro.trade.optionlegselect.adapter.BaseOptionLegItemAdapter
import com.etrade.mobilepro.trade.optionlegselect.databinding.TradeItemOptionLegBinding
import com.etrade.mobilepro.util.android.ClearFocusOnImeActionDone
import com.etrade.mobilepro.util.android.accessibility.initTextInputAccessibility
import com.etrade.mobilepro.valuepicker.ValuePickerWidget
import com.google.android.material.textfield.TextInputLayout

internal typealias OnOptionLegItemQuantityChangedListener = (index: Int, quantity: String) -> Unit

open class OptionLegItemViewHolder(
    binding: TradeItemOptionLegBinding,
    rootView: View? = null,
    protected val lifecycleOwner: LifecycleOwner,
    private val getItemAtPosition: (Int) -> BaseOptionLegItemAdapter.Item?,
    private val quantityChangedListener: OnOptionLegItemQuantityChangedListener,
) : RecyclerView.ViewHolder(rootView ?: binding.root) {

    private val quantityLayout: TextInputLayout = binding.optionLegQuantityLayout
    private val quantityView: EditText = binding.optionLegQuantityView
    private val displayNameView: ValuePickerWidget = binding.optionLegDisplayNameView

    private val quantityObserver: Observer<String> = Observer {
        if (it != quantityView.text.toString()) {
            quantityView.setText(it)
        }
    }

    private var previousItem: BaseOptionLegItemAdapter.Item? = null

    @CallSuper
    open fun bindTo(item: BaseOptionLegItemAdapter.Item, index: Int, isLastItem: Boolean) {
        displayNameView.value = item.displayName
        displayNameView.contentDescription =
            "${displayNameView.context.getString(R.string.trade_label_position)} ${item.displayName}"

        // Update the hint text from "Qty" to "Quantity"
        quantityLayout.initTextInputAccessibility {
            hintText = quantityLayout.context.getString(R.string.quantity)
        }

        quantityView.apply {
            imeOptions = if (isLastItem) {
                EditorInfo.IME_ACTION_DONE
            } else {
                EditorInfo.IME_ACTION_NEXT
            }

            onFocusChangeListener = OnFocusChangeListener(index)

            setOnEditorActionListener(ClearFocusOnImeActionDone())
        }

        previousItem?.quantity?.removeObserver(quantityObserver)
        previousItem = item.apply { quantity.observe(lifecycleOwner, quantityObserver) }
    }

    private inner class OnFocusChangeListener(private val index: Int) : View.OnFocusChangeListener {

        override fun onFocusChange(v: View?, hasFocus: Boolean) {
            val text = quantityView.text
            if (hasFocus) {
                quantityView.setSelection(text.length)
            } else {
                val item = getItemAtPosition(index)
                val quantity = text.toString()
                item?.takeIf { it.quantity.value != quantity }
                    ?.let { quantityChangedListener(index, quantity) }
            }
        }
    }
}
