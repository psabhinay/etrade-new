package com.etrade.mobilepro.news.data.mapper

import com.etrade.mobilepro.news.data.dto.TextDocDto
import com.etrade.mobilepro.news.data.dto.TimestampDto
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

private const val TEST_SOURCE = "test news source title"
private const val TEST_TIMESTAMP_VALUE = "2018-12-20T06:07:06.000-05:00"
private const val TEST_SINGLE_SYMBOL = "#!#@*) #$^*HMVXCVBM?ZX§1''\"\$\nGDFSHB^E%*"
private const val TEST_SYMBOLS = "S1,S2"
private const val TEST_SYMBOLS_WITH_SPACES = "S1 , S2"
private const val TEST_ZERO_LENGTH_SYMBOLS = ","

@RunWith(RobolectricTestRunner::class)
class TextNewsMapperTest {
    private val sut = TextNewsMapper()

    @Test
    fun `null json fields should result in non null empty strings or empty lists`() {
        val input =
            TextDocDto(docId = null, title = null, timestamp = null, url = null, symbols = null, content = null, source = null)

        val output = sut.map(input)

        assertTrue("nulls should result in empties", output.content.isEmpty())
        assertTrue("nulls should result in empties", output.docId.isEmpty())
        assertTrue("nulls should result in empties", output.symbols.isEmpty())
        assertTrue("nulls should result in empties", output.title.isEmpty())
        assertTrue("nulls should result in empties", output.url.isEmpty())
    }

    @Test
    fun `null timestamp and non null source should result in newsDateTimeSource that equals source without format`() {
        val input = TextDocDto(
            docId = null,
            title = null,
            timestamp = null,
            url = null,
            symbols = null,
            content = null,
            source = TEST_SOURCE
        )

        val output = sut.map(input)

        assertTrue("must be only source", output.newsDateTimeSource == TEST_SOURCE)
    }

    @Test
    fun `non null timestamp value and source should result in formatted newsDateTimeSource`() {
        val testTimestampDto = TimestampDto(value = TEST_TIMESTAMP_VALUE)
        val input = TextDocDto(
            docId = null,
            title = null,
            timestamp = testTimestampDto,
            url = null,
            symbols = null,
            content = null,
            source = TEST_SOURCE
        )

        val output = sut.map(input)

        assertTrue(
            "newsDataSource should be formatted with {DATE} | {TIME} | {SOURCE}",
            output.newsDateTimeSource == "12/20/2018 | 06:07 AM EST | test news source title"
        )
    }

    @Test
    fun `empty symbols field should result in emplylist`() {
        val input = TextDocDto(
            docId = null,
            title = null,
            timestamp = null,
            url = null,
            symbols = "",
            content = null,
            source = TEST_SOURCE
        )

        val output = sut.map(input)

        assertTrue("sybmols must be empty list", output.symbols.isEmpty())
    }

    @Test
    fun `symbols field containing various chars without comma should result in list with one value`() {
        val input = TextDocDto(
            docId = null,
            title = null,
            timestamp = null,
            url = null,
            symbols = TEST_SINGLE_SYMBOL,
            content = null,
            source = TEST_SOURCE
        )

        val output = sut.map(input)

        assertTrue("symbols must be one-value list", output.symbols.size == 1)
    }

    @Test
    fun `symbols field containing various chars without comma should result in list member with the same value`() {
        val input = TextDocDto(
            docId = null,
            title = null,
            timestamp = null,
            url = null,
            symbols = TEST_SINGLE_SYMBOL,
            content = null,
            source = TEST_SOURCE
        )

        val output = sut.map(input)
        assertTrue("symbol value must be the same", output.symbols[0] == TEST_SINGLE_SYMBOL)
    }

    @Test
    fun `symbols field containing comma separated symbols(strings) must result in list of this strings`() {
        val input = TextDocDto(
            docId = null,
            title = null,
            timestamp = null,
            url = null,
            symbols = TEST_SYMBOLS,
            content = null,
            source = TEST_SOURCE
        )

        val output = sut.map(input)

        assertTrue("symbol value must be the same", output.symbols[0] == "S1")
        assertTrue("symbol value must be the same", output.symbols[1] == "S2")
    }

    @Test
    fun `no trim or other format should be applied to symbols`() {
        val input =
            TextDocDto(
                docId = null,
                title = null,
                timestamp = null,
                url = null,
                symbols = TEST_SYMBOLS_WITH_SPACES,
                content = null,
                source = TEST_SOURCE
            )

        val output = sut.map(input)

        assertTrue("symbol value must be the same", output.symbols[0] == "S1 ")
        assertTrue("symbol value must be the same", output.symbols[1] == " S2")
    }

    @Test
    fun `zero length input symbols result in zero length mapped symbols`() {
        val input =
            TextDocDto(
                docId = null,
                title = null,
                timestamp = null,
                url = null,
                symbols = TEST_ZERO_LENGTH_SYMBOLS,
                content = null,
                source = TEST_SOURCE
            )

        val output = sut.map(input)

        assertTrue("symbol value must be the same", output.symbols[0] == "")
        assertTrue("symbol value must be the same", output.symbols[1] == "")
    }

    @Test
    fun `widget news widet get date and source string without time`() {
        val testTimestampDto = TimestampDto(value = TEST_TIMESTAMP_VALUE)
        val input = TextDocDto(
            docId = null,
            title = null,
            timestamp = testTimestampDto,
            url = null,
            symbols = null,
            content = null,
            source = TEST_SOURCE
        )

        val output = sut.map(input, omitTime = true)

        assertTrue(
            "newsDataSource for widget should be formatted with {DATE} | {SOURCE}",
            output.newsDateTimeSource == "12/20/2018 | test news source title"
        )
    }
}
