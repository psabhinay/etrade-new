package com.etrade.mobilepro.hidecontent

import androidx.appcompat.app.AppCompatActivity

interface HideContentOverlayDelegate {
    fun init(activity: AppCompatActivity)
}
