package com.etrade.mobilepro.order.details.dto;

import com.etrade.mobilepro.backends.neo.BaseDataDto;
import com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto;
import com.etrade.mobilepro.orders.data.dto.AuditInfoDto;
import com.etrade.mobilepro.orders.data.dto.HeaderDto;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001BG\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u000e\b\u0001\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t\u0012\b\b\u0001\u0010\u000b\u001a\u00020\f\u0012\b\b\u0001\u0010\r\u001a\u00020\f\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0007H\u00c6\u0003J\u000f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u00c6\u0003J\t\u0010\u001e\u001a\u00020\fH\u00c6\u0003J\t\u0010\u001f\u001a\u00020\fH\u00c6\u0003JK\u0010 \u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u00072\u000e\b\u0003\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\b\b\u0003\u0010\u000b\u001a\u00020\f2\b\b\u0003\u0010\r\u001a\u00020\fH\u00c6\u0001J\u0013\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010$\u001a\u00020%H\u00d6\u0001J\t\u0010&\u001a\u00020\fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\r\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019\u00a8\u0006\'"}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/OrdersDto;", "", "advancedOrder", "Lcom/etrade/mobilepro/orders/data/dto/AdvancedOrderDto;", "auditInfo", "Lcom/etrade/mobilepro/orders/data/dto/AuditInfoDto;", "header", "Lcom/etrade/mobilepro/orders/data/dto/HeaderDto;", "legDetails", "", "Lcom/etrade/mobilepro/order/details/dto/LegDetailsDto;", "egOverride", "", "egQual", "(Lcom/etrade/mobilepro/orders/data/dto/AdvancedOrderDto;Lcom/etrade/mobilepro/orders/data/dto/AuditInfoDto;Lcom/etrade/mobilepro/orders/data/dto/HeaderDto;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V", "getAdvancedOrder", "()Lcom/etrade/mobilepro/orders/data/dto/AdvancedOrderDto;", "getAuditInfo", "()Lcom/etrade/mobilepro/orders/data/dto/AuditInfoDto;", "getEgOverride", "()Ljava/lang/String;", "getEgQual", "getHeader", "()Lcom/etrade/mobilepro/orders/data/dto/HeaderDto;", "getLegDetails", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "orders-details-data"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class OrdersDto {
    @org.jetbrains.annotations.NotNull()
    private final com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto advancedOrder = null;
    @org.jetbrains.annotations.NotNull()
    private final com.etrade.mobilepro.orders.data.dto.AuditInfoDto auditInfo = null;
    @org.jetbrains.annotations.NotNull()
    private final com.etrade.mobilepro.orders.data.dto.HeaderDto header = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.etrade.mobilepro.order.details.dto.LegDetailsDto> legDetails = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String egOverride = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String egQual = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.OrdersDto copy(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "advancedOrder")
    com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto advancedOrder, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "auditInfo")
    com.etrade.mobilepro.orders.data.dto.AuditInfoDto auditInfo, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "header")
    com.etrade.mobilepro.orders.data.dto.HeaderDto header, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "legDetails")
    java.util.List<com.etrade.mobilepro.order.details.dto.LegDetailsDto> legDetails, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "egOverride")
    java.lang.String egOverride, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "egQual")
    java.lang.String egQual) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public OrdersDto(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "advancedOrder")
    com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto advancedOrder, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "auditInfo")
    com.etrade.mobilepro.orders.data.dto.AuditInfoDto auditInfo, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "header")
    com.etrade.mobilepro.orders.data.dto.HeaderDto header, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "legDetails")
    java.util.List<com.etrade.mobilepro.order.details.dto.LegDetailsDto> legDetails, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "egOverride")
    java.lang.String egOverride, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "egQual")
    java.lang.String egQual) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto getAdvancedOrder() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.orders.data.dto.AuditInfoDto component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.orders.data.dto.AuditInfoDto getAuditInfo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.orders.data.dto.HeaderDto component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.orders.data.dto.HeaderDto getHeader() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.order.details.dto.LegDetailsDto> component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.order.details.dto.LegDetailsDto> getLegDetails() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getEgOverride() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getEgQual() {
        return null;
    }
}