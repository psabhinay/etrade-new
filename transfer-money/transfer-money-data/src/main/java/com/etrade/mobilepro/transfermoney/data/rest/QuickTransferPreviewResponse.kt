package com.etrade.mobilepro.transfermoney.data.rest

import com.etrade.mobilepro.backends.af.ResultDto
import org.simpleframework.xml.Element
import org.simpleframework.xml.Path
import org.simpleframework.xml.Root

@Root(name = "QuickTransferPreviewResponse", strict = false)
data class QuickTransferPreviewResponse constructor(

    @field:Element(name = "Result", required = false)
    var result: ResultDto? = null,

    @field:Element(name = "ScheduledDate", required = false)
    var scheduledDate: String? = null,

    @field:Element(name = "Amount", required = false)
    var amount: String? = null,

    @field:Path("FundHoldInfo")
    @field:Element(name = "Message", required = false)
    var fundHoldInfoMessage: String? = null,

    @field:Element(name = "HeaderMessage", required = false)
    var headerMessage: String? = null,

    @field:Element(name = "FooterMessage", required = false)
    var footerMessage: String? = null
)
