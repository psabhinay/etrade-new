package com.etrade.mobilepro.trade.mutualfund.api

import com.etrade.mobilepro.backends.api.HasWarnings
import com.etrade.mobilepro.backends.api.ServerMessage

class MutualFundResult(
    val entryList: List<MutualFundEntry>
) : HasWarnings {
    override var warnings: Collection<ServerMessage> = emptyList()
}
