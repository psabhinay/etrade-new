package com.etrade.mobilepro.education

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.common.BaseDaggerFragment
import com.etrade.mobilepro.common.DisclosureOverlayHandler
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.education.adapter.EducationAdapter
import com.etrade.mobilepro.education.databinding.FragmentEducationBinding
import com.etrade.mobilepro.mediaplayer.EtCloudMediaPlayer
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.util.android.accessibility.bindButtonAccessibility
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

class EducationFragment : BaseDaggerFragment<EducationViewModel>() {

    private val binding by viewBinding(FragmentEducationBinding::bind)

    override val viewModelClass: Class<EducationViewModel> = EducationViewModel::class.java

    override val layoutRes: Int = R.layout.fragment_education

    @Inject
    lateinit var overlayHandler: DisclosureOverlayHandler

    @Suppress("LongMethod")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val educationAdapter = EducationAdapter(
            getString(R.string.education_level),
            {
                context?.apply {
                    EtCloudMediaPlayer.start(this, it.videoId)
                }
            },
            {
                activity?.let {
                    startActivity(
                        SpannableTextOverlayActivity.intent(
                            it,
                            getString(R.string.education_option_risk_disclosure),
                            getString(R.string.education_option_risk_disclosure_text),
                            R.drawable.ic_close
                        )
                    )
                }
            },
            { startActivity(overlayHandler.getDisclosureOverlayIntent(activity)) }
        )
        with(binding) {
            topicLabel.bindButtonAccessibility(
                getString(R.string.education_topic_content_description),
                getString(R.string.accessibility_action_select)
            )
            with(educationRecyclerView) {
                layoutManager = LinearLayoutManager(context)
                adapter = educationAdapter
            }
        }
        setUpViewModel(viewModel, educationAdapter)
    }

    @Suppress("LongMethod")
    private fun setUpViewModel(viewModel: EducationViewModel, educationAdapter: EducationAdapter) {
        viewModel.selectedTopic.observe(viewLifecycleOwner) {
            with(binding.topicValue) {
                text = it
                bindButtonAccessibility(
                    getString(R.string.education_filter_content_description, it),
                    getString(R.string.accessibility_action_select)
                )
            }
        }

        viewModel.topics.observe(viewLifecycleOwner) { items ->
            parentFragmentManager.also { frm ->
                val dropDownManager: BottomSheetSelector<String> =
                    BottomSheetSelector(frm, resources)

                dropDownManager.init(
                    "EDUCATION TOPICS",
                    getString(R.string.education_topic_label),
                    items,
                    viewModel.selectedTopicIndex.value
                )

                dropDownManager.setListener { position, _ ->
                    viewModel.selectedTopicIndex.value = position
                }

                binding.topicContainer.setOnClickListener {
                    dropDownManager.openDropDown()
                }
            }
        }

        viewModel.articles.observe(viewLifecycleOwner) {
            educationAdapter.updateItems(it)
            educationAdapter.notifyDataSetChanged()
            binding.educationRecyclerView.smoothScrollToPosition(0)
        }
    }
}
