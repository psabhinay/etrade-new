package com.etrade.mobilepro.accountvalues

/**
 * Synthetic account id that represent summary values of all accounts that belong to a user
 */
const val COMPLETE_VIEW_SUMMARY_STREAM_ID: String = "COMPLETE_VIEW_SUMMARY"
