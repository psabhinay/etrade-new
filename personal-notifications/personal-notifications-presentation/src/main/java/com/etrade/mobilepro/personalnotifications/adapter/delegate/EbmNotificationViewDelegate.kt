package com.etrade.mobilepro.personalnotifications.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.inboxmessages.databinding.InboxMessageDashboardInformationViewBinding
import com.etrade.mobilepro.personalnotifications.adapter.holder.EbmNotificationViewHolder
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.Localytics
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class EbmNotificationViewDelegate(
    private val clickListener: ((PersonalNotification) -> Unit),
    private val itemDismissListener: ((Localytics) -> Unit)
) : AbsListItemAdapterDelegate<Localytics.EmergencyBroadcast, PersonalNotification, EbmNotificationViewHolder>() {
    override fun isForViewType(
        item: PersonalNotification,
        items: MutableList<PersonalNotification>,
        position: Int
    ): Boolean {
        return item is Localytics.EmergencyBroadcast
    }

    override fun onCreateViewHolder(parent: ViewGroup): EbmNotificationViewHolder {
        return EbmNotificationViewHolder(
            parent.viewBinding(InboxMessageDashboardInformationViewBinding::inflate),
            clickListener,
            itemDismissListener
        )
    }

    override fun onBindViewHolder(
        item: Localytics.EmergencyBroadcast,
        holder: EbmNotificationViewHolder,
        payloads: MutableList<Any>
    ) {
        holder.bind(item)
    }
}
