package com.etrade.mobilepro.balance.dagger

import android.content.Context
import android.content.res.Resources
import androidx.core.content.ContextCompat
import com.etrade.mobilepro.R
import com.etrade.mobilepro.balance.api.GetAccountBalanceUseCase
import com.etrade.mobilepro.balance.caching.AccountBalancesRepoImpl
import com.etrade.mobilepro.balance.caching.BalanceDataSourceDelegate
import com.etrade.mobilepro.balance.data.GetAccountBalanceUseCaseImpl
import com.etrade.mobilepro.balance.data.repo.AccountBalancesRepo
import com.etrade.mobilepro.balance.data.repo.BalanceRequest
import com.etrade.mobilepro.balance.section.BalanceHeaderLabels
import com.etrade.mobilepro.balance.section.BalanceSectionMapper
import com.etrade.mobilepro.balance.section.BalancesLabelResources
import com.etrade.mobilepro.balance.section.BalancesResources
import com.etrade.mobilepro.balance.section.CashAccountLabels
import com.etrade.mobilepro.balance.section.MarginAccountLabels
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.dynamicui.CachedResponseModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import java.util.Currency
import java.util.Locale
import javax.inject.Singleton

@Module
abstract class BalancesModule {
    @Binds
    @Singleton
    abstract fun bindBalanceRepo(repo: AccountBalancesRepoImpl): AccountBalancesRepo

    @Binds
    abstract fun bindBalanceDataSourceDelegate(
        delegate: BalanceDataSourceDelegate
    ): SimpleCachedResource.DataSourceDelegate<CachedResponseModel, BalanceRequest>

    @Binds
    @Singleton
    abstract fun bindGetAccountBalanceUseCase(useCase: GetAccountBalanceUseCaseImpl): GetAccountBalanceUseCase

    companion object {

        @Provides
        @Singleton
        internal fun provideBalancesSectionMapper(resources: BalancesLabelResources): BalanceSectionMapper = BalanceSectionMapper(resources)

        @Provides
        internal fun provideBalancesResources(
            header: BalanceHeaderLabels,
            cash: CashAccountLabels,
            margin: MarginAccountLabels,
            resources: BalancesResources
        ): BalancesLabelResources =
            BalancesLabelResources(header, cash, margin, resources)

        @Provides
        internal fun provideBalancesHeaderLabels(resources: Resources): BalanceHeaderLabels = BalanceHeaderLabels(
            resources.getString(R.string.header_net_account_value),
            resources.getString(R.string.header_cash_call),
            resources.getString(R.string.header_margin_call),
            resources.getString(R.string.header_securities),
            resources.getString(R.string.header_purchasing_power),
            resources.getString(R.string.header_funds_withheld),
            resources.getString(R.string.header_funds_withdrawal),
            resources.getString(R.string.header_cash_balance),
            resources.getString(R.string.header_funds_hold),
            resources.getString(R.string.header_cash_margin_balance),
            resources.getString(R.string.header_day_trading),
            resources.getString(R.string.header_daytrade_purchasing_power)
        )

        @Provides
        internal fun provideBalanceCashResources(resources: Resources): CashAccountLabels = CashAccountLabels(
            resources.getString(R.string.cash_title),
            resources.getString(R.string.cash_total_market_value),
            resources.getString(R.string.cash_available_investment),
            resources.getString(R.string.cash_available_withdrawal),
            resources.getString(R.string.cash_net_balance),
            resources.getString(R.string.cash_call),
            resources.getString(R.string.cash_securities),
            resources.getString(R.string.cash_settled_investment),
            resources.getString(R.string.cash_unsettled_investment),
            resources.getString(R.string.cash_sweep_deposit),
            resources.getString(R.string.cash_uninvested_cash),
            resources.getString(R.string.cash_holding_net_cash_balance),
            resources.getString(R.string.cash_purchasing_power_funds_withheld),
            resources.getString(R.string.cash_purchasing_power_funds_withdrawal)
        )

        @Provides
        internal fun provideBalancesMarginResources(resources: Resources): MarginAccountLabels = MarginAccountLabels(
            resources.getString(R.string.margin_title),
            resources.getString(R.string.margin_marginable_securities),
            resources.getString(R.string.margin_non_marginable_securities_or_options),
            resources.getString(R.string.margin_max_available_withdrawal),
            resources.getString(R.string.margin_net_cash_margin_balance),
            resources.getString(R.string.margin_fed_call),
            resources.getString(R.string.margin_house),
            resources.getString(R.string.margin_min_equity),
            resources.getString(R.string.margin_call),
            resources.getString(R.string.margin_securities),
            resources.getString(R.string.margin_short_securities),
            resources.getString(R.string.margin_equity),
            resources.getString(R.string.margin_equity_percentage),
            resources.getString(R.string.margin_balance),
            resources.getString(R.string.margin_net_margin_balance),
            resources.getString(R.string.margin_short_reserve),
            resources.getString(R.string.margin_daytrade_purchasing_power),
            resources.getString(R.string.margin_daytrade_status),
            resources.getString(R.string.margin_daytrade_cash),
            resources.getString(R.string.margin_daytrade_margin),
            resources.getString(R.string.margin_daytrade_count),
            resources.getString(R.string.margin_daytrade_allowed_count)
        )

        @Provides
        internal fun provideBalanceCosmeticResources(resources: Resources, context: Context): BalancesResources = BalancesResources(
            Currency.getInstance(Locale.US).symbol,
            resources.getString(R.string.empty_default),
            "%",

            // Margin Calls are always a negative value, hence RED
            marginCallTextColor = ContextCompat.getColor(context, R.color.red),
            infoIcon = R.drawable.ic_info,
            infoIconDescription = resources.getString(R.string.more_information)
        )
    }
}
