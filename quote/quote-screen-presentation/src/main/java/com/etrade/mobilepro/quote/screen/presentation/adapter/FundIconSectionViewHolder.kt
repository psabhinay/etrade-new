package com.etrade.mobilepro.quote.screen.presentation.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.screen.presentation.R
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemFundIconSectionBinding
import com.etrade.mobilepro.quoteapi.FundQuoteItem

class FundIconSectionViewHolder(private val binding: QuoteItemFundIconSectionBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bindTo(
        section: FundQuoteItem.FundIconSection?,
        fundInfoPopUpListener: ((Int) -> Unit)?
    ) {
        section?.let {
            setUpIcons(it, fundInfoPopUpListener)
        }
    }

    private fun setUpIcons(
        section: FundQuoteItem.FundIconSection,
        fundInfoPopUpListener: ((Int) -> Unit)?
    ) {
        with(binding) {
            noTransactionFeeIcon.visibility =
                if (section.hasNoTransactionsFee) View.VISIBLE else View.GONE
            noTransactionFeeIcon.setOnClickListener {
                fundInfoPopUpListener?.invoke(R.id.no_transaction_fee_icon)
            }
            allStarIcon.visibility = if (section.isAllStar) View.VISIBLE else View.GONE
            allStarIcon.setOnClickListener {
                fundInfoPopUpListener?.invoke(R.id.all_star_icon)
            }
            verticalDivider.visibility =
                if (section.hasNoTransactionsFee && section.isAllStar) View.VISIBLE else View.GONE

            allStarIconDivider.visibility = if (section.isAllStar && section.isAffiliatedProduct) View.VISIBLE else View.GONE
            affiliatedProductFundIcon.visibility = if (section.isAffiliatedProduct) View.VISIBLE else View.GONE
            affiliatedProductFundIcon.setOnClickListener {
                fundInfoPopUpListener?.invoke(R.id.affiliated_product_fund_icon)
            }
        }
    }
}
