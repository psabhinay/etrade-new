package com.etrade.mobilepro.trade.presentation

import com.etrade.mobilepro.balance.api.AccountBalance
import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.trade.api.TradeAccount
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever

fun mockTradeAccount(accountId: String = "id", displayName: String = accountId, mode: AccountMode = AccountMode.UNKNOWN): TradeAccount {
    return mock<TradeAccount>().apply {
        whenever(id).thenReturn(accountId)
        whenever(name).thenReturn(displayName)
        whenever(accountMode).thenReturn(mode)
    }
}

fun mockAccountBalance(isPdt: Boolean = false): AccountBalance {
    return mock<AccountBalance>().apply {
        whenever(isPatternDayTrader).thenReturn(isPdt)
    }
}
