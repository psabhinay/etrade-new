package com.etrade.mobilepro.searching.symbol

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.caching.androidx.asLiveData
import com.etrade.mobilepro.searchingapi.SearchQueryValidator
import com.etrade.mobilepro.searchingapi.SymbolSearchRepo
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable

class DefaultSymbolSearchRepo(
    private val cacheController: SimpleCachedResource.Controller<List<SearchResultItem.Symbol>>,
    private val cacheDataDelegate: SimpleCachedResource.DataSourceDelegate<List<SearchResultItem.Symbol>, SearchQuery>,
    private val searchQueryValidator: SearchQueryValidator
) : SymbolSearchRepo {

    override fun searchAsLiveData(query: SearchQuery): LiveData<Resource<List<SearchResultItem.Symbol>>> {
        val validatedQuery = searchQueryValidator.makeValidOrEmpty(query)
        return if (validatedQuery.isNotEmpty()) {
            SimpleCachedResource(validatedQuery, cacheDataDelegate, cacheController).asLiveData()
        } else {
            MutableLiveData<Resource<List<SearchResultItem.Symbol>>>().apply { value = Resource.Success(emptyList()) }
        }
    }

    override fun searchAsObservable(query: SearchQuery): Observable<Resource<List<SearchResultItem.Symbol>>> {
        val validatedQuery = searchQueryValidator.makeValidOrEmpty(query)
        return if (validatedQuery.isNotEmpty()) {
            SimpleCachedResource(validatedQuery, cacheDataDelegate, cacheController).asObservable()
        } else {
            Observable.just(Resource.Success(emptyList()))
        }
    }
}
