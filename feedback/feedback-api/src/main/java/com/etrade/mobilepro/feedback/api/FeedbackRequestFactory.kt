package com.etrade.mobilepro.feedback.api

interface FeedbackRequestFactory {
    suspend fun createFeedbackRequest(feedbackModel: FeedbackModel): FeedbackRequest
}
