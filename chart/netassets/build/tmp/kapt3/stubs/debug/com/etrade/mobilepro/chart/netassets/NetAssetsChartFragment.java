package com.etrade.mobilepro.chart.netassets;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0018\u001a\u00020\u00192\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001bH\u0002J\b\u0010\u001d\u001a\u00020\u0019H\u0002J\b\u0010\u001e\u001a\u00020\u0019H\u0002J,\u0010\u001f\u001a\u00020 2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b2\u0014\u0010\"\u001a\u0010\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020 \u0018\u00010#H\u0002J\u0010\u0010%\u001a\u00020\u00192\u0006\u0010&\u001a\u00020\'H\u0002J\u0012\u0010(\u001a\u00020\u00192\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J\u001a\u0010+\u001a\u00020\u00192\u0006\u0010,\u001a\u00020-2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016R\u001b\u0010\u0007\u001a\u00020\b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\nR\u001b\u0010\r\u001a\u00020\u000e8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0013\u001a\u00020\u00148BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0017\u0010\u0012\u001a\u0004\b\u0015\u0010\u0016\u00a8\u0006."}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartFragment;", "Landroidx/fragment/app/Fragment;", "viewModelFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "snackbarUtilFactory", "Lcom/etrade/mobilepro/util/android/snackbar/SnackbarUtilFactory;", "(Landroidx/lifecycle/ViewModelProvider$Factory;Lcom/etrade/mobilepro/util/android/snackbar/SnackbarUtilFactory;)V", "binding", "Lcom/etrade/mobilepro/chart/netassets/databinding/FragmentNetAssetsBinding;", "getBinding", "()Lcom/etrade/mobilepro/chart/netassets/databinding/FragmentNetAssetsBinding;", "binding$delegate", "Lkotlin/properties/ReadOnlyProperty;", "snackBarUtil", "Lcom/etrade/mobilepro/util/android/snackbar/SnackBarUtil;", "getSnackBarUtil", "()Lcom/etrade/mobilepro/util/android/snackbar/SnackBarUtil;", "snackBarUtil$delegate", "Lkotlin/Lazy;", "viewModel", "Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel;", "getViewModel", "()Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel;", "viewModel$delegate", "displayChart", "", "entries", "", "Lcom/etrade/mobilepro/barchartview/DataEntry;", "displayChartError", "displayChartLoading", "getChartContentDescription", "", "list", "xAxisValueFormatter", "Lkotlin/Function1;", "", "onChartState", "state", "Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "Landroid/view/View;", "netassets_debug"})
@com.etrade.mobilepro.common.RequireLogin()
public final class NetAssetsChartFragment extends androidx.fragment.app.Fragment {
    private final kotlin.properties.ReadOnlyProperty binding$delegate = null;
    private final kotlin.Lazy viewModel$delegate = null;
    private final kotlin.Lazy snackBarUtil$delegate = null;
    
    @javax.inject.Inject()
    public NetAssetsChartFragment(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.ViewModelProvider.Factory viewModelFactory, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory snackbarUtilFactory) {
        super();
    }
    
    private final com.etrade.mobilepro.chart.netassets.databinding.FragmentNetAssetsBinding getBinding() {
        return null;
    }
    
    private final com.etrade.mobilepro.chart.netassets.NetAssetsChartViewModel getViewModel() {
        return null;
    }
    
    private final com.etrade.mobilepro.util.android.snackbar.SnackBarUtil getSnackBarUtil() {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void onChartState(com.etrade.mobilepro.chart.netassets.NetAssetsChartViewModel.ChartState state) {
    }
    
    private final void displayChart(java.util.List<? extends com.etrade.mobilepro.barchartview.DataEntry> entries) {
    }
    
    private final java.lang.String getChartContentDescription(java.util.List<? extends com.etrade.mobilepro.barchartview.DataEntry> list, kotlin.jvm.functions.Function1<? super java.lang.Float, java.lang.String> xAxisValueFormatter) {
        return null;
    }
    
    private final void displayChartLoading() {
    }
    
    private final void displayChartError() {
    }
}