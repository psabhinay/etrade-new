package com.etrade.mobilepro.streaming.market.status

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

interface MarketTradeStatusDelegate {
    val marketStatus: LiveData<ConsumableLiveEvent<MarketTradeStatus>>

    fun onMarketStatusUpdate(level1Data: Level1Data)

    // method invoked to reset market status when Streaming ceases due to network or user intervention.
    fun resetMarketStatus()
}
