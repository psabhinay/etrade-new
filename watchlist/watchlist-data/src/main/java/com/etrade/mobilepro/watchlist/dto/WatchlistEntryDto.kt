package com.etrade.mobilepro.watchlist.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class WatchlistEntryDto(
    @Json(name = "symbol")
    val ticker: String? = null,

    @Json(name = "symbolDescription")
    val description: String? = null,

    // This entryID is returned by AddWatchListEntryResponse and SaveWatchlistEntriesResponse
    @Json(name = "entryID")
    val entryID: String? = null,

    // This entryId is returned by get WatchlistEntriesResponse
    @Json(name = "entryId")
    val entryId: String? = null,

    @Json(name = "instrumentId")
    val instrumentId: Any? = null,

    @Json(name = "cuspid")
    val cuspid: String? = null,

    @Json(name = "typeCode")
    val typeCode: String? = null,

    @Json(name = "exchangeCode")
    val exchangeCode: String? = null,

    @Json(name = "exchangeGroup")
    val exchangeGroup: String? = null,

    @Json(name = "lastPrice")
    val lastPrice: String? = null,

    @Json(name = "lastTradeTime")
    val lastTradeTime: String? = null,

    @Json(name = "lastTradeTimeStamp")
    val lastTradeTimeStamp: String? = null,

    @Json(name = "priceWhenAdded")
    val priceWhenAdded: String? = null,

    @Json(name = "changePercentSinceAdded")
    val changePercentSinceAdded: String? = null,

    @Json(name = "change")
    val change: String? = null,

    @Json(name = "changePercent")
    val changePercent: String? = null,

    @Json(name = "volume")
    val volume: String? = null,

    @Json(name = "bid")
    val bid: String? = null,

    @Json(name = "ask")
    val ask: String? = null,

    @Json(name = "dayHigh")
    val dayHigh: String? = null,

    @Json(name = "dayLow")
    val dayLow: String? = null,

    @Json(name = "week52High")
    val week52High: String? = null,

    @Json(name = "week52Low")
    val week52Low: String? = null,

    @Json(name = "priceEarningsRatio")
    val priceEarningsRatio: String? = null,

    @Json(name = "earningsPerShare")
    val earningsPerShare: String? = null,

    @Json(name = "underLyingSymbol")
    val underLyingSymbol: String? = null,

    @Json(name = "bondIssuerName")
    val bondIssuerName: String? = null,

    @Json(name = "bondMaturityDate")
    val bondMaturityDate: String? = null,

    @Json(name = "bondRate")
    val bondRate: String? = null
)
