package com.etrade.mobilepro.util

/**
 * Represents resource, that can have three states - SUCCESS, LOADING, FAILED
 */
sealed class Resource<out T> {

    abstract val data: T?

    fun isEmpty() = data == null

    abstract fun <O> map(mapper: (T?) -> O?): Resource<O>

    data class Loading<T>(override val data: T? = null) : Resource<T>() {
        override fun <O> map(mapper: (T?) -> O?) = Loading(mapper(data))
    }

    data class Success<T>(override val data: T? = null) : Resource<T>() {
        override fun <O> map(mapper: (T?) -> O?) = Success(mapper(data))
    }

    data class Failed<T>(override val data: T? = null, val error: Throwable? = null, val retry: (() -> Unit)? = null) : Resource<T>() {
        override fun <O> map(mapper: (T?) -> O?) = Failed(mapper(data), this.error, this.retry)
    }
}
