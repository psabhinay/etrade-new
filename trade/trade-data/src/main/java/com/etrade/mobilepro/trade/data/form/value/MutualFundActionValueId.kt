package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.trade.data.mutualfunds.BUY_FUNDS_ORDER
import com.etrade.mobilepro.trade.data.mutualfunds.EXCHANGE_FUNDS_ORDER
import com.etrade.mobilepro.trade.data.mutualfunds.SELL_FUNDS_ORDER
import com.etrade.mobilepro.trade.data.mutualfunds.UNKNOWN_FUNDS_ORDER
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.util.MutualConverter

object MutualFundActionValueId : ActionValueId {
    override val buy: String = BUY_FUNDS_ORDER
    override val buyToCover: String = UNKNOWN_FUNDS_ORDER // Not used
    override val exchange: String = EXCHANGE_FUNDS_ORDER
    override val sell: String = SELL_FUNDS_ORDER
    override val sellShort: String = UNKNOWN_FUNDS_ORDER // Not used
    override val converter: MutualConverter<String, TradeAction> by lazy { createConverter() }
}
