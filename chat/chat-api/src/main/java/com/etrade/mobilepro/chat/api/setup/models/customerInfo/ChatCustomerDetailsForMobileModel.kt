package com.etrade.mobilepro.chat.api.setup.models.customerInfo

import com.etrade.mobilepro.chat.api.ChatGeolocationModel

data class GetCustomerDetailsForMobileModel(val data: CustomerDetailsDataModel? = null)

data class CustomerDetailsDataModel(val chatCustomerDetails: ChatCustomerDetailsModel? = null)

data class ChatCustomerDetailsModel(
    val userId: String? = null,
    val customerAccounts: List<ChatCustomerAccountModel>? = emptyList(),
    val userGeolocation: ChatGeolocationModel? = null
)
