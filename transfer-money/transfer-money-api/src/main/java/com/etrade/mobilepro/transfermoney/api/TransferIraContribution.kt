package com.etrade.mobilepro.transfermoney.api

import org.threeten.bp.LocalDate

interface TransferIraContribution {
    val year: Int
    val accountId: String
    val accountIdKey: String
    val previousYearDetails: Details?
    val currentYearDetails: Details?
    val eligibleYears: List<Int>

    interface Details {
        val taxYear: Int
        val taxDueDate: LocalDate
        val amountLeft: String
        val maxLimit: String
        val amount: String
    }
}
