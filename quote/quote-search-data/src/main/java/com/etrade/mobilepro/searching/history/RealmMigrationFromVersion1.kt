package com.etrade.mobilepro.searching.history

import com.etrade.mobilepro.instrument.SettlementSessionCode
import com.etrade.mobilepro.searching.symbol.cache.SymbolDb
import io.realm.DynamicRealm
import javax.inject.Inject

/**
 * Migration by this class is set the new field {@code settlementSessionCd}
 * with default as {@link #SettlementSessionCode.UNKNOWN} because there is no way to know
 * if they should be treated as AM or PM
 */
class RealmMigrationFromVersion1 @Inject constructor() : RealMigrationDelegate {
    override val oldVersion: Long = 1

    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        if (oldVersion == this.oldVersion) {
            realm.schema.get(SymbolDb::class.java.simpleName)
                ?.transform {
                    it.setInt(
                        SymbolDb::settlementSessionCd.name,
                        SettlementSessionCode.UNKNOWN.code
                    )
                }
        }
    }
}
