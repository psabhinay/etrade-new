package com.etrade.mobilepro.portfolio.model

sealed class PageLoadingState {
    data class Idle(val hasNextPage: Boolean) : PageLoadingState()
    object Error : PageLoadingState()
    object Loading : PageLoadingState()
}
