package com.etrade.mobilepro.screener.presentation.mutualfund

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.datasource.ScreenerSetsDataSource
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.MutualFund
import com.etrade.mobilepro.screener.presentation.base.BaseScreenersViewModel
import com.etrade.mobilepro.tracking.Tracker
import javax.inject.Inject

class MutualFundScreenersViewModel @Inject constructor(
    screenerSetsDataSource: ScreenerSetsDataSource,
    @MutualFund screenerInteractor: ScreenerInteractor,
    tracker: Tracker
) : BaseScreenersViewModel(
    screenerGroup = ScreenerType.Group.MUTUAL_FUND,
    screenerSetsDataSource = screenerSetsDataSource,
    screenerInteractor = screenerInteractor,
    tracker = tracker
)
