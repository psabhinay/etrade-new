package com.etrade.mobilepro.trade.data.advanced

import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.trade.api.action
import com.etrade.mobilepro.trade.api.advanced.ConfirmedAdvancedStockOrder
import com.etrade.mobilepro.util.ObjectArrayMismatchAdapter
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class AdvancedOrderPlaceResponseDtoTest {

    @Test
    fun `check successful place response parsing`() {
        val response = readPlaceResponse("advanced-order-place-response-successful.json")
        assertTrue(response.errors.isEmpty())
        assertEquals(2, response.orders.size)
        assertEquals(1, response.warnings.size)

        val orders = response.orders
        orders.forEach {
            require(it is ConfirmedAdvancedStockOrder)
            assertEquals("XXXX-3924", it.account)
            assertEquals(TransactionType.BUY_OPEN, it.action)
            assertEquals(PriceType.MARKET, it.priceType)
            assertEquals(OrderTerm.GOOD_FOR_DAY, it.term)
            assertEquals(OrderType.EQUITY, it.orderType)
            assertEquals(BigDecimal.ZERO, it.fee)
            assertFalse(it.allOrNone)
            assertNull(it.limitPrice)
            assertNull(it.stopPrice)
        }

        val iterator = orders.iterator()

        var order = iterator.next()
        require(order is ConfirmedAdvancedStockOrder)
        var leg = order.legs.first()

        assertEquals("AAPL", leg.symbol)
        assertEquals(BigDecimal("4"), leg.quantity)
        assertEquals(BigDecimal("459.56"), order.cost)
        assertEquals("208", order.confirmationOrderId)
        assertEquals("10/08/2020 03:36:32 PM EDT", order.placedDate)

        order = iterator.next()
        require(order is ConfirmedAdvancedStockOrder)
        leg = order.legs.first()

        assertEquals("FB", leg.symbol)
        assertEquals(BigDecimal("4"), leg.quantity)
        assertEquals(BigDecimal("1054.96"), order.cost)
        assertEquals("208", order.confirmationOrderId)
        assertEquals("10/08/2020 03:36:32 PM EDT", order.placedDate)
    }

    private fun readPlaceResponse(path: String): AdvancedOrderPlaceResponseDto {
        return AdvancedOrderPlaceResponseDto::class.getObjectFromJson(
            resourcePath = path,
            type = AdvancedOrderPlaceResponseDto::class.java,
            adapters = listOf(ObjectArrayMismatchAdapter.newFactory(AdvancedOrderServerMessageDto::class.java))
        )
    }
}
