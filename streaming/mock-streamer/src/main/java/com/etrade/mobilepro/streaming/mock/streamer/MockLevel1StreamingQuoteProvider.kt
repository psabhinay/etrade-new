package com.etrade.mobilepro.streaming.mock.streamer

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val INITIAL_DELAY = 500L
private const val EMISSION_INTERVAL = 500L

class MockLevel1StreamingQuoteProvider @Inject constructor() : StreamingQuoteProvider<Level1Field, Level1Data, Any> {

    override fun getQuote(symbol: String, instrumentType: InstrumentType, fields: Set<Level1Field>, payload: Any?): Observable<Level1Data> {
        val isOption = instrumentType.isOption
        return Observable.interval(INITIAL_DELAY, EMISSION_INTERVAL, TimeUnit.MILLISECONDS)
            .map {
                if (isOption) {
                    MockOptionData()
                } else {
                    MockStockData()
                }
            }
    }
}
