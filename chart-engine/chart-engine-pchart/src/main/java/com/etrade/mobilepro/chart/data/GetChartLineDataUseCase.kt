package com.etrade.mobilepro.chart.data

import com.etrade.mobilepro.chart.model.ChartLineData
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.UseCase
import io.reactivex.Observable

/**
 * Use case to get ChartLineData from an arbitrary data source.
 */
interface GetChartLineDataUseCase : UseCase<GetChartLineDataParameter, Observable<Resource<ChartLineData>>>
