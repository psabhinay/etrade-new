package com.etrade.mobilepro.inboxmessages

import android.content.Context
import android.util.AttributeSet
import android.view.View.MeasureSpec.getMode
import android.view.View.MeasureSpec.getSize
import android.view.View.MeasureSpec.makeMeasureSpec
import android.widget.ImageView
import androidx.core.view.children
import androidx.viewpager.widget.ViewPager
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.util.delegate.getValue
import com.etrade.mobilepro.util.delegate.setValue

class InboxMessagesView : ViewPager {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    private val messagesAdapter = InboxMessageViewPagerAdapter()

    var imageLoader: ImageLoader? by messagesAdapter::imageLoader

    var onItemDismissListener: ((InboxMessage) -> Unit)? by messagesAdapter::onItemDismissListener

    var onItemClickListener: ((InboxMessage) -> Unit)? by messagesAdapter::onItemClickListener

    // If any of the child view height is different than ViewPager itself,
    // for example when font size is increased/for small size menu item, ViewPager's height must be re-calculated.
    // decreasing the font size would not impact as there is min height for the child views
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        // do not update height if the inbox message is a image
        if (children.any { it is ImageView }) {
            return
        }

        val currentHeight = getSize(heightMeasureSpec)

        val childWidthSpec = makeMeasureSpec(
            getSize(widthMeasureSpec),
            getMode(widthMeasureSpec)
        )

        val childMaxHeight = children.map {
            it.measure(childWidthSpec, MeasureSpec.UNSPECIFIED)
            it.measuredHeight
        }.maxOrNull() ?: 0

        if (childMaxHeight != currentHeight) {
            val updatedHeightMeasureSpec = makeMeasureSpec(childMaxHeight, MeasureSpec.EXACTLY)
            super.onMeasure(widthMeasureSpec, updatedHeightMeasureSpec)
        }
    }

    fun setMessages(payload: List<InboxMessage>) {
        messagesAdapter.updateItems(payload)
    }

    private fun init() {
        adapter = messagesAdapter
    }
}
