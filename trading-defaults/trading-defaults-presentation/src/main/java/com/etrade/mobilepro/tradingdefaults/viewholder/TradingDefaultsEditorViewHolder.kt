package com.etrade.mobilepro.tradingdefaults.viewholder

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.tradingdefaults.TradingDefaultsItem
import com.etrade.mobilepro.tradingdefaults.databinding.AdapterTradingDefaultsEditorBinding
import com.etrade.mobilepro.util.android.extension.viewBinding

class TradingDefaultsEditorViewHolder(private val binding: AdapterTradingDefaultsEditorBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: TradingDefaultsItem.EditorValue) {
        binding.inputValueView.hint = item.hint
        binding.inputValueView.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = object : InputFieldManager.Formatter {
                    override fun formatRawContent(rawContent: String): String = rawContent
                },
                validator = object : InputFieldManager.Validator {
                    override fun validate(value: String): InputFieldManager.Value =
                        InputFieldManager.Value.Valid(value)
                }
            ),
            rawContentData = item.value
        )
    }

    companion object {
        fun from(parent: ViewGroup) =
            TradingDefaultsEditorViewHolder(parent.viewBinding(AdapterTradingDefaultsEditorBinding::inflate))
    }
}
