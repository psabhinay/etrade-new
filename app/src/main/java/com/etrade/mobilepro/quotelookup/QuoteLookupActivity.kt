package com.etrade.mobilepro.quotelookup

import android.app.Activity
import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.SearchView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.R
import com.etrade.mobilepro.barcode.BarcodeFragment
import com.etrade.mobilepro.baseactivity.ActivityNetworkConnectionDelegate
import com.etrade.mobilepro.baseactivity.ActivitySessionDelegate
import com.etrade.mobilepro.common.OnFragmentBackPressListener
import com.etrade.mobilepro.databinding.ActivityQuoteLookupBinding
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.quotes.QUOTE_BOTTOM_SHEET_TAG
import com.etrade.mobilepro.quotes.QuoteBottomSheetFragment
import com.etrade.mobilepro.searchingapi.EXTRA_SELECTED_SYMBOL
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.util.android.accessibility.clearAccessibilityFocus
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setInputFilters
import com.etrade.mobilepro.util.android.hideSoftKeyboard
import com.etrade.mobilepro.util.android.showSoftKeyboard
import com.etrade.mobilepro.util.android.textutil.PatternInputFilter
import com.google.android.material.tabs.TabLayoutMediator
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

// Pattern for lower case, upper case latin characters, digits and dot symbol.
private const val QUOTE_TEXT_FILTER = "[^a-zA-Z0-9. ]"

class QuoteLookupActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var appPrefs: ApplicationPreferences

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var fragmentInjectionFactory: FragmentFactory

    @Inject
    lateinit var activitySessionDelegate: ActivitySessionDelegate

    @Inject
    lateinit var activityNetworkConenectionDelegate: ActivityNetworkConnectionDelegate

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var quoteBottomSheet: QuoteBottomSheetFragment

    private val binding by viewBinding(ActivityQuoteLookupBinding::inflate)

    private lateinit var pagerAdapter: QuoteLookupPagerAdapter

    private val stockQuoteSearchOnly: Boolean get() = intent.getBooleanExtra(EXTRA_STOCK_QUOTE_SEARCH_ONLY, false)
    private val showOwnedSymbols: Boolean get() = intent.getBooleanExtra(EXTRA_STOCK_QUOTE_SHOW_OWNED_SYMBOLS, false)
    private val selectedAccountId: String? get() = intent.getStringExtra(EXTRA_STOCK_QUOTE_SELECTED_ACCOUNT_ID)
    private val quoteLookupLocation: String? get() = intent.getStringExtra(EXTRA_QUOTE_LOOKUP_LOCATION)
    private val viewModel: SearchViewModel by viewModels { viewModelFactory }

    private val overlayRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            handleSpannableTextOverlayResult(result.resultCode)
        }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        viewModel.configure(showOwnedSymbols, selectedAccountId, quoteLookupLocation)

        supportFragmentManager.fragmentFactory = fragmentInjectionFactory

        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setListeners()
        setupSearchView()

        if (savedInstanceState == null) {
            onNewIntent(intent)
        }

        setupPager()

        activityNetworkConenectionDelegate.initNetworkConnectivityBanner(this)

        activitySessionDelegate.observeTimeoutEvent(this)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        if (intent.hasExtra(SearchManager.QUERY)) {
            val query = getQuery()

            if (!TextUtils.isEmpty(query.value)) {
                hideSoftKeyboard()
                binding.searchView.clearFocus()
            }

            binding.searchView.setQuery(query.value, false)
            viewModel.search(query)
        }
    }

    override fun onBackPressed() {
        val currentFragment = supportFragmentManager.currentFragment
        if (currentFragment is OnFragmentBackPressListener && currentFragment.onBackPressed()) {
            return
        }
        if (supportFragmentManager.backStackEntryCount == 1) {
            supportFragmentManager.popBackStack()
        } else {
            dismiss()
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        activitySessionDelegate.onUserInteraction(this)
    }

    override fun onResume() {
        super.onResume()

        binding.barcodeAction.visibility = if (viewModel.queryStr?.isEmpty() != false) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    override fun onEnterAnimationComplete() {
        if (binding.viewpager.visibility == View.VISIBLE) {
            showKeyboard()
        } else {
            hideKeyboard()
        }
    }

    fun showBottomSheet(bundle: Bundle) {
        quoteBottomSheet.arguments = bundle
        quoteBottomSheet.show(supportFragmentManager, QUOTE_BOTTOM_SHEET_TAG)
    }

    fun dismiss(resultItem: SearchResultItem.Symbol? = null) {
        // clearReferences the background else the touch ripple moves with the translation which looks bad
        binding.searchBack.background = null

        binding.barcodeAction.visibility = View.GONE

        if (resultItem != null) {
            setResult(RESULT_OK, Intent().putExtra(EXTRA_SELECTED_SYMBOL, resultItem))
        } else {
            setResult(RESULT_CANCELED)

            if (viewModel.queryType == SearchType.SYMBOL) {
                viewModel.queryType?.also { type ->
                    viewModel.itemSelected(type, SearchResultItem.EmptySearchResultItem)
                }
            }
        }

        finishAfterTransition()
    }

    fun toggleSearchView(shouldShow: Boolean) {
        // On LG devices, the search view must have visibility set to GONE when on the barcode
        // page otherwise the keyboard will appear when it should not
        if (shouldShow) {
            binding.searchView.visibility = View.VISIBLE
            showKeyboard()
        } else {
            binding.searchView.visibility = View.GONE
        }
    }

    fun toggleViewPager(shouldShow: Boolean) {
        // There exists and issue where when voice assist is enabled the quote lookup history list
        // items remain selectable on the barcode scanner screen despite not being visible.
        // Actively hiding the viewpager fixes this issue
        if (shouldShow) {
            binding.viewpager.visibility = View.VISIBLE
        } else {
            binding.viewpager.visibility = View.GONE
        }
    }

    private fun setupPager() {
        val tabItems: List<QuoteLookupTab> = if (stockQuoteSearchOnly) {
            binding.tabLayout.visibility = View.GONE

            listOf(QuoteLookupTab.StockQuoteSearch)
        } else {
            binding.tabLayout.visibility = View.VISIBLE

            listOf(
                QuoteLookupTab.StockQuote,
                QuoteLookupTab.StockCallOption,
                QuoteLookupTab.StockPutOption
            )
        }

        pagerAdapter = QuoteLookupPagerAdapter(this, tabItems, intent.extras)
        with(binding.viewpager) {
            adapter = pagerAdapter
            offscreenPageLimit = 2
        }
        TabLayoutMediator(binding.tabLayout, binding.viewpager) { tab, position ->
            tab.text = pagerAdapter.getPageTitle(position)
        }.attach()
    }

    private fun setupSearchView() {
        // hint, inputType & ime options seem to be ignored from XML! Set in code
        with(binding.searchView) {
            queryHint = getString(R.string.quote_lookup_hint)
            inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
            imeOptions = imeOptions or EditorInfo.IME_ACTION_SEARCH or
                EditorInfo.IME_FLAG_NO_EXTRACT_UI or EditorInfo.IME_FLAG_NO_FULLSCREEN
            setInputFilters(arrayOf(PatternInputFilter(QUOTE_TEXT_FILTER.toRegex())))
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(updated: String): Boolean {
                    viewModel.searchSpecificOrPickTheFirst()
                    isIconified = true
                    return true
                }

                override fun onQueryTextChange(updated: String): Boolean {
                    viewModel.search(getQuery(updated))

                    binding.barcodeAction.visibility = if (updated.isEmpty()) View.VISIBLE else View.GONE

                    return true
                }
            })
        }
    }

    private fun getQuery(updated: String? = null): SearchQuery {
        val fromIntent: SearchQuery = requireNotNull(intent.getParcelableExtra(SearchManager.QUERY))
        return if (updated == null) {
            fromIntent
        } else {
            SearchQuery(fromIntent.type, updated)
        }
    }

    private fun setListeners() {
        binding.searchBack.setOnClickListener { dismiss() }

        binding.barcodeAction.setOnClickListener {
            hideKeyboard()
            if (appPrefs.isBarcodeTermsAgreed) {
                openBarcode()
            } else {
                openBarcodeTermsAndConditions()
            }
        }
    }

    private fun openBarcodeTermsAndConditions() {
        overlayRequest.launch(
            SpannableTextOverlayActivity.intent(
                this,
                getString(R.string.barcode_terms_and_conditions_title),
                getString(R.string.barcode_disclaimer),
                R.drawable.ic_close,
                getString(R.string.agree)
            )
        )
    }

    private fun openBarcode() {
        binding.barcodeAction.clearAccessibilityFocus()

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.content_fragment, fragmentInjectionFactory.instantiate(classLoader, BarcodeFragment::class.java.name))
            .addToBackStack(null)
            .commit()
    }

    private fun handleSpannableTextOverlayResult(resultCode: Int) {
        if (resultCode == Activity.RESULT_OK) {
            appPrefs.isBarcodeTermsAgreed = true
            hideKeyboard()
            openBarcode()
        }
    }

    private fun showKeyboard() {
        binding.searchView.requestFocus()
        this.showSoftKeyboard()
    }

    private fun hideKeyboard() {
        binding.searchView.clearFocus()
        this.hideSoftKeyboard()
    }
}

val FragmentManager.currentFragment: Fragment get() = fragments.last()
