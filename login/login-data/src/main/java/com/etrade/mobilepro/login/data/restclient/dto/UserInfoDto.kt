package com.etrade.mobilepro.login.data.restclient.dto

import com.etrade.mobilepro.session.api.Session
import org.simpleframework.xml.Element
import java.util.Locale

/**
 * User Info XML Model
 */

private val trueValues = arrayOf("true", "y", "yes")

/**
 * Test of the specified value is a known true value.
 */
fun isTrueValue(strValue: String?): Boolean = trueValues.contains(strValue?.lowercase(Locale.US))

internal data class UserInfoDto(
    @field:Element(name = "UserId", required = false) private var userLoginId: String? = null,
    @field:Element(name = "AccountId", required = false) private var accountId: String = "",
    @field:Element(name = "UserType", required = false) private var userTypeName: String = "",
    @field:Element(name = "NumAccounts", required = false) private var numAccounts: Int = 0,
    @field:Element(name = "CustomerProfile", required = false) private var customerProfile: String = "",
    @field:Element(name = "LastLoginDateTime", required = false) private var lastLoginDateTime: String = "",
    @field:Element(name = "CSRPage", required = false) private var csrPage: String = "",
    @field:Element(name = "MDA_USER", required = false) private var mdaUser: String = "",
    @field:Element(name = "SM_ET_AUTHMETHOD", required = false) private var smEtAuthMethod: String = "",
    @field:Element(name = "COMPLIANCE_REVIEW", required = false) private var complianceReviewCode: String = "",
    @field:Element(name = "CSRFTOKEN", required = false) private var csrfToken: String = "",
    @field:Element(name = "PR_STRUM", required = false) private var prStrum: String = "",
    @field:Element(name = "IsPET", required = false) private var petFlag: String = "",
    @field:Element(name = "IsNYSE", required = false) private var nyseFlag: String = "",
    @field:Element(name = "realTimeQuotes", required = false) private var realTimeQuotes: String = "",
    @field:Element(name = "lightstreamerUrl", required = false) private var level1Url: String = "",
    @field:Element(name = "lightstreamerL2Url", required = false) private var level2Url: String = "",
    @field:Element(name = "MProConsent", required = false) private var mobileProConsentGivenFlag: String = "",
    @field:Element(name = "AndroidConsent", required = false) private var androidConsentGivenFlag: String = "",
    @field:Element(name = "TradingEnabled", required = false) private var tradingEnabledFlag: String = "",
    @field:Element(name = "QuickTransferEnabled", required = false) private var quickTransferEnabledFlag: String = "",
    @field:Element(name = "AO_DISCLOSURE", required = false) private var advancedOrderDisclosureFlag: String = "",
    @field:Element(name = "CONDITIONAL_ORDER_DISCLOSURE", required = false) private var conditionalOrderDisclosureFlag: String = "",
    @field:Element(name = "IsInternationalCustomer", required = false) private var isInternationalCustomerFlag: String? = null,
    @field:Element(name = "currentTime", required = false) private var currentTime: Long = System.currentTimeMillis(),
    @field:Element(name = "Encryptedconsumerkey", required = false) private var encryptedConsumerKey: String = "",
    @field:Element(name = "WEB_SESSION_TIMEOUT", required = false) private var sessionTimeout: Int = 0,
    @field:Element(name = "AATID", required = false) private var aatIdToken: String? = null,
    @field:Element(name = "AndroidNotification", required = false) private var androidNotificationToken: String = "",
    @field:Element(name = "EHFlag", required = false) private var ehFlag: String = ""
) : Session {
    override var realTimeOfLastTouchMillis: Long = 0

    override fun touchSession(timeStamp: Long) {
        realTimeOfLastTouchMillis = timeStamp
    }

    override val defaultAccountId: String get() = accountId
    override val isPET: Boolean get() = isTrueValue(petFlag)
    override val isNYSE: Boolean get() = isTrueValue(nyseFlag)
    override val isInternationalUser: Boolean get() = isTrueValue(isInternationalCustomerFlag)
    override val isMobileProConsentGiven: Boolean get() = isTrueValue(mobileProConsentGivenFlag)
    override val isAndroidConsentGiven: Boolean get() = isTrueValue(androidConsentGivenFlag)
    override val isTradingEnabled: Boolean get() = isTrueValue(tradingEnabledFlag)
    override val isQuickTransferEnabled: Boolean get() = isTrueValue(quickTransferEnabledFlag)
    override val isAdvancedOrderDisclosure: Boolean get() = isTrueValue(advancedOrderDisclosureFlag)
    override val isConditionalOrderDisclosure: Boolean get() = isTrueValue(conditionalOrderDisclosureFlag)
    override val isPreclearanceRequired: Boolean get() = complianceReviewCode == "DB"
    override val isRSATokenUser: Boolean get() = smEtAuthMethod == "U_PWD_RSA"
    override val principalId: String get() = userLoginId ?: ""
    override val userId: Long get() = userLoginId?.toLong() ?: 0L
    override val token: String get() = csrfToken
    override val level1StreamerUrl: String get() = level1Url
    override val level2StreamerUrl: String get() = level2Url
    override var timeZoneOffset: Long = 0
    override var isRealTimeQuotes: Boolean
        get() = isTrueValue(realTimeQuotes)
        set(value) {
            realTimeQuotes = value.toString()
        }
    override val numberOfAccounts: Int get() = numAccounts
    override val currentServerTime: Long get() = currentTime
    override val consumerKey: String get() = encryptedConsumerKey
    override var webSessionTimeout: Int
        get() = sessionTimeout
        set(value) {
            sessionTimeout = value
        }
    override val aatId: String? get() = aatIdToken
    override val notificationToken: String get() = androidNotificationToken
    override val isExtendedHoursAccepted: Boolean
        get() = isTrueValue(ehFlag)
}
