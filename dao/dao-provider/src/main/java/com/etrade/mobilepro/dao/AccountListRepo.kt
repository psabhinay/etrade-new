package com.etrade.mobilepro.dao

import com.etrade.mobilepro.account.dao.Account
import io.realm.Realm
import javax.inject.Inject

interface AccountListRepo {
    fun getAccountList(): List<Account>
}

class DefaultRealmAccountListRepo @Inject constructor() : AccountListRepo {

    /**
     * Fetch list of accounts from the realm DB without requirement of previously opened Realm Instance.
     * Also ensures realm instance is closed finally.
     */
    override fun getAccountList(): List<Account> = Realm.getDefaultInstance().use {
        it.copyFromRealm(it.where(Account::class.java).findAll())
    }
}
