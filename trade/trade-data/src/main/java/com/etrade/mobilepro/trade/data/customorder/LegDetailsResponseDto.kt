package com.etrade.mobilepro.trade.data.customorder

import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LegDetailsResponseDto(
    @Json(name = "ask")
    val ask: String? = null,
    @Json(name = "askSize")
    val askSize: String? = null,
    @Json(name = "avgFillPrice")
    val avgFillPrice: String? = null,
    @Json(name = "bid")
    val bid: String? = null,
    @Json(name = "bidSize")
    val bidSize: String? = null,
    @Json(name = "borrowRate")
    val borrowRate: String? = null,
    @Json(name = "cusip")
    val cusip: String? = null,
    @Json(name = "estDailyBorrowInterest")
    val estDailyBorrowInterest: String? = null,
    @Json(name = "lastPrice")
    val lastPrice: String? = null,
    @Json(name = "legDesc")
    val legDesc: String? = null,
    @Json(name = "qtyReserveShow")
    val qtyReserveShow: Int? = null,
    @Json(name = "rootSymbol")
    val rootSymbol: String? = null,
    @Json(name = "shortSaleCd")
    val shortSaleCd: String? = null,
    @Json(name = "symbolDesc")
    val symbolDesc: String? = null,
    @Json(name = "typeCode")
    val typeCode: String? = null,
    @Json(name = "underlyingSymbol")
    val underlyingSymbol: String? = null,
    @Json(name = "volume")
    val volume: String? = null,
    @Json(name = "washSaleFlag")
    val washSaleFlag: Int? = null
) : LegDetailsCommonDto()
