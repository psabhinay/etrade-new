package com.etrade.mobilepro.common.result

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.AfterClass
import org.junit.Assert.assertTrue
import org.junit.BeforeClass
import org.junit.Test
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

@ExperimentalCoroutinesApi
class ETResultSuspendTest {
    interface UseCase<P, R> {
        suspend fun execute(param: P): R
    }

    interface BooleanInterface : UseCase<Unit, Boolean>

    class BooleanImpl : BooleanInterface {
        override suspend fun execute(param: Unit): Boolean = true
    }

    @Test
    fun `non-result value from direct class reference`() {
        runBlockingTest {
            val subject = BooleanImpl()
            assertTrue(subject.execute(Unit))
        }
    }

    @Test
    fun `non-result value from interface reference`() {
        runBlockingTest {
            val subject: BooleanInterface = BooleanImpl()
            assertTrue(subject.execute(Unit))
        }
    }

    interface BooleanResultInterface : UseCase<Unit, ETResult<Boolean>>

    class BooleanResultImpl : BooleanResultInterface {
        override suspend fun execute(param: Unit): ETResult<Boolean> {
            return ETResult.success(true)
        }
    }

    @Test
    fun `result value from direct class reference`() {
        runBlockingTest {
            val subject = BooleanResultImpl()
            assertTrue(subject.execute(Unit).getOrThrow())
        }
    }

    @Test
    fun `result value from interface reference`() {
        runBlockingTest {
            val subject: BooleanResultInterface = BooleanResultImpl()
            assertTrue(subject.execute(Unit).getOrThrow())
        }
    }

    class ContinuationBooleanImpl : BooleanInterface {
        var continuation: Continuation<Boolean>? = null
        override suspend fun execute(param: Unit): Boolean {
            return suspendCoroutine {
                continuation = it
            }
        }
    }

    @Test
    fun `non-result value from direct reference with continuation`() {
        val subject = ContinuationBooleanImpl()
        val scope = TestCoroutineScope(testCoroutineDispatcher)
        var result = false
        scope.launch {
            val innerResult = subject.execute(Unit)
            result = innerResult
        }
        subject.continuation?.resume(true)
        assertTrue(result)
    }

    @Test
    fun `non-result value from interface reference with continuation`() {
        val subject = ContinuationBooleanImpl()
        val iface: BooleanInterface = subject
        val scope = TestCoroutineScope(testCoroutineDispatcher)
        var result = false
        scope.launch {
            val innerResult = iface.execute(Unit)
            result = innerResult
        }
        subject.continuation?.resume(true)
        assertTrue(result)
    }

    class ContinuationResultImpl : BooleanResultInterface {
        var continuation: Continuation<ETResult<Boolean>>? = null
        override suspend fun execute(param: Unit): ETResult<Boolean> {
            return suspendCoroutine {
                continuation = it
            }
        }
    }

    @Test
    fun `result value from direct reference with continuation`() {
        val subject = ContinuationResultImpl()
        val scope = TestCoroutineScope(testCoroutineDispatcher)
        var result = ETResult.success(false)
        scope.launch {
            val innerResult = subject.execute(Unit)
            result = innerResult
        }
        subject.continuation?.resume(ETResult.success(true))
        assertTrue(result.getOrThrow())
    }

    @Test
    fun `result value from interface reference with continuation`() {
        val subject = ContinuationResultImpl()
        val iface: BooleanResultInterface = subject
        val scope = TestCoroutineScope(testCoroutineDispatcher)
        var result = ETResult.success(false)
        scope.launch {
            val innerResult = iface.execute(Unit)
            result = innerResult
        }
        subject.continuation?.resume(ETResult.success(true))
        assertTrue(result.getOrThrow())
    }

    companion object {

        private val testCoroutineDispatcher = TestCoroutineDispatcher()

        @BeforeClass
        @JvmStatic
        fun prepare() {
            Dispatchers.setMain(testCoroutineDispatcher)
        }

        @AfterClass
        @JvmStatic
        fun tearDown() {
            Dispatchers.resetMain()
            testCoroutineDispatcher.cleanupTestCoroutines()
        }
    }
}
