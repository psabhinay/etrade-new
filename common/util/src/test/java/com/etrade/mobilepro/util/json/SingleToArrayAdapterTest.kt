package com.etrade.mobilepro.util.json

import com.squareup.moshi.Json
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

internal class SingleToArrayAdapterTest {

    private val factory = SingleToArrayAdapter.SingleToArrayAdapterFactory()
    private val moshi = Moshi.Builder().add(factory).build()
    private val adapter: JsonAdapter<TestObject> = moshi.adapter(TestObject::class.java)

    @Test
    fun `should parse single object to list`() {
        val input =
            """ { "test": "test string" } """
        val response = adapter.fromJson(input)

        assertNotNull(response)
        assertNotNull(response?.test)
        assertEquals("test string", response?.test?.firstOrNull())
    }

    @Test
    fun `should parse list of objects to list`() {
        val input =
            """ { "test": [ "test 1", "test 2" ] } """
        val response = adapter.fromJson(input)

        assertNotNull(response)
        assertNotNull(response?.test)
        assertEquals(2, response?.test?.size)
    }

    @Test
    fun `should parse null`() {
        val input =
            """ { } """
        val response = adapter.fromJson(input)

        assertNotNull(response)
        assertNull(response?.test)
    }

    @JsonClass(generateAdapter = true)
    internal class TestObject(
        @SingleToArray
        @Json(name = "test")
        val test: List<String>?
    )
}
