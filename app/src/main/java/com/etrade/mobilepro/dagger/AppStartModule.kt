package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.appstart.api.AppStartRepo
import com.etrade.mobilepro.appstart.data.DefaultAppStartRepo
import com.etrade.mobilepro.common.di.MobileTrade
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
internal class AppStartModule {
    @Provides
    @Singleton
    internal fun provideAppStartRepo(@MobileTrade retrofit: Retrofit): AppStartRepo = DefaultAppStartRepo(retrofit)
}
