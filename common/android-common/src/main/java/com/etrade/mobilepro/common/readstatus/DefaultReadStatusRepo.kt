package com.etrade.mobilepro.common.readstatus

import com.etrade.mobilepro.common.CommonRealmModule
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Inject

// Used for NEWS and ALERT, for now news article ids are uuids and alert ids are ints,
// so there is no chance of matching between them
class DefaultReadStatusRepo @Inject constructor() : BaseReadStatusRepo() {
    override val realmConfig: RealmConfiguration
        get() = RealmConfiguration.Builder()
            .name("common.history")
            .modules(CommonRealmModule())
            .build()
}

abstract class BaseReadStatusRepo : ReadStatusRepo {
    private val readIds = mutableSetOf<String>().apply { addAll(getListOfReadIds()) }

    override fun markAsRead(entityId: String, type: ReadableType) {
        readIds.add(entityId)
        Realm.getInstance(realmConfig).use {
            val articleDb = ReadableEntityDb(
                entityId = entityId,
                wasReadAt = System.currentTimeMillis(),
                type = type.name
            )
            it.executeTransaction { realm ->
                realm.insertOrUpdate(articleDb)
            }
        }
    }

    override fun markAsRead(entityIds: List<String>, type: ReadableType) {
        readIds.addAll(entityIds)
        val articles = entityIds.map {
            ReadableEntityDb(entityId = it, wasReadAt = System.currentTimeMillis(), type = type.name)
        }
        Realm.getInstance(realmConfig).use { realm ->
            realm.executeTransaction { realmTxn ->
                realmTxn.insertOrUpdate(articles)
            }
        }
    }

    override fun wasRead(entityId: String): Boolean = readIds.contains(entityId)

    override fun clearExpiredReadableEntities(elapsedTimeInMillis: Long, type: ReadableType) {
        Realm.getInstance(realmConfig).use {
            it.executeTransaction { realm ->
                realm
                    .where(ReadableEntityDb::class.java)
                    .equalTo("type", type.name)
                    .lessThan("wasReadAt", System.currentTimeMillis() - elapsedTimeInMillis)
                    .findAll()
                    .deleteAllFromRealm()
            }
        }
    }

    override fun clearAll() {
        readIds.clear()
        Realm.getInstance(realmConfig).use { realm ->
            realm.executeTransaction {
                it.deleteAll()
            }
        }
    }

    private fun getListOfReadIds(): List<String> {
        return Realm.getInstance(realmConfig).use { realm ->
            realm
                .where(ReadableEntityDb::class.java)
                .findAll()
                .map {
                    realm.copyFromRealm(it)
                }
                .map {
                    it.entityId
                }
        }
    }
}
