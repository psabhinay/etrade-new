package com.etrade.mobilepro.billpay.presentation.managepayee.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayItemPayeeBinding
import com.etrade.mobilepro.util.android.accessibility.getPayeeAccountDescription
import com.etrade.mobilepro.util.android.extension.inflater
import com.etrade.mobilepro.util.formatters.formatPayeeName

internal class PayeesListViewHolder(
    private val binding: BillPayItemPayeeBinding,
    private val clickListener: (Payee) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    constructor(parent: ViewGroup, clickListener: (Payee) -> Unit) : this(
        BillPayItemPayeeBinding.inflate(parent.inflater, parent, false), clickListener
    )

    private var item: Payee? = null

    init {
        binding.root.setOnClickListener {
            item?.let { item ->
                clickListener.invoke(item)
            }
        }
    }

    internal fun bind(item: Payee) {
        this.item = item

        if (!item.nickName.isNullOrBlank()) {
            binding.payeeName.text = item.nickName
        } else {
            binding.payeeName.text = formatPayeeName(
                payeeName = item.name,
                accountNumber = item.accountNumber,
                payeeNickname = item.nickName
            )
        }

        binding.payeeName.contentDescription = getPayeeAccountDescription(
            resources = itemView.resources,
            accountName = item.name,
            accountNickname = item.nickName,
            accountNumber = item.accountNumber
        )
    }
}
