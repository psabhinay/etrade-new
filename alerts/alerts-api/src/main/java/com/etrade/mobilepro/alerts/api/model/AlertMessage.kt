package com.etrade.mobilepro.alerts.api.model

data class AlertMessage(
    val id: String,
    val createdAtFormatted: String,
    val readAtFormatted: String,
    val content: String,
    val group: AlertGroup? = null
)
