package com.etrade.mobilepro.news.data.rest

import com.etrade.mobilepro.caching.okhttp.CACHE_CONTROL_DEFAULT_EXPIRATION
import com.etrade.mobilepro.news.data.dto.NewsDto
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface NewsApiClient {
    @JsonMoshi
    @Headers(CACHE_CONTROL_DEFAULT_EXPIRATION)
    @GET("/app/news/portfolio/accountId/{resource}.json")
    fun getPortfolioNews(
        @Path("resource") resource: String,
        @Query("startingDoc") startIndex: Int,
        @Query("maxResults") maxCount: Int,
        @Query("stockplan") stockPlan: Boolean = false
    ): Single<NewsDto>

    @JsonMoshi
    @Headers(CACHE_CONTROL_DEFAULT_EXPIRATION)
    @GET("/app/news/market.json")
    fun getMarketNews(
        @Query("startingDoc") startIndex: Int,
        @Query("maxResults") maxCount: Int
    ): Single<NewsDto>

    @JsonMoshi
    @POST("/app/nvs-api/mobile/article/{id}.json")
    fun getNewsDetail(@Path("id") id: String): Single<NewsDto>

    @JsonMoshi
    @Headers(CACHE_CONTROL_DEFAULT_EXPIRATION)
    @GET("/app/news/symbol/{resource}.json")
    fun getSymbolNews(
        @Path("resource") resource: String,
        @Query("startingDoc") startIndex: Int,
        @Query("maxResults") maxCount: Int
    ): Single<NewsDto>

    @JsonMoshi
    @GET("app/news/watchlist/symbols/{symbols}.json")
    fun getSymbolsNews(@Path("symbols") symbols: String, @Query("startingDoc") startIndex: Int, @Query("maxResults") maxCount: Int): Single<NewsDto>

    @JsonMoshi
    @GET("app/news/watchlist/watchlistId/{watchlistId}.json")
    fun getWatchlistNews(@Path("watchlistId") watchlistId: String, @Query("startingDoc") startIndex: Int, @Query("maxResults") maxCount: Int): Single<NewsDto>
}
