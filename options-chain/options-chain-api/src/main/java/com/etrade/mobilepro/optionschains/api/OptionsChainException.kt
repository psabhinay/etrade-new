package com.etrade.mobilepro.optionschains.api

/**
 * An Exception to hold partial options chain data and error message.
 */
class OptionsChainException(val optionsChain: OptionsChain?, val errorMessage: String?) : RuntimeException()
