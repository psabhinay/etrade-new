package com.etrade.mobilepro.billpay.api

sealed class SavePayeeResult(val code: String) {

    object Successful : SavePayeeResult("SUCCESS")

    object GenericError : SavePayeeResult("-1")

    object PayeeAlreadyAdded : SavePayeeResult("1053")
}
