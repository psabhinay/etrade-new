package com.etrade.mobilepro.session

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DepositActivityErrorResponse(
    @Json(name = "Error")
    val error: Error?
)

@JsonClass(generateAdapter = true)
data class Error(
    @Json(name = "message")
    val message: String?
)
