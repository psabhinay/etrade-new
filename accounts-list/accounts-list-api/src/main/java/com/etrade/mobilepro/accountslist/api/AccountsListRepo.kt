package com.etrade.mobilepro.accountslist.api

import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable

interface AccountsListRepo {
    fun getScreen(cacheExpiration: Long? = null): Observable<Resource<AccountsListResult>>
}
