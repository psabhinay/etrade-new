package com.etrade.mobilepro.eventtrackerapi

interface AppsFlyerRepo {

    val appsFlyerId: String

    suspend fun sendAppsFlyerEraseRequest(adId: String)

    suspend fun trackEvent(event: String, params: Map<String, Any>)
}
