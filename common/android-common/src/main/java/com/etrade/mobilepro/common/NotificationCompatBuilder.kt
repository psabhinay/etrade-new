package com.etrade.mobilepro.common

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

sealed class AppNotificationChannel(
    val id: String,
    val name: String,
    val importance: Int
) {
    object Default : AppNotificationChannel(
        id = "ETRADE_NOTIFICATION_CHANNEL",
        name = "Etrade Notifications",
        importance = NotificationManager.IMPORTANCE_DEFAULT
    )
    object Silent : AppNotificationChannel(
        id = "SILENT_ETRADE_NOTIFICATION_CHANNEL",
        name = "Etrade Silent Notifications",
        importance = NotificationManager.IMPORTANCE_LOW
    )
}

private fun NotificationManagerCompat.setupNotificationChannel(appNotificationChannel: AppNotificationChannel) {
    if (getNotificationChannel(appNotificationChannel.id) == null) {
        NotificationChannel(
            appNotificationChannel.id,
            appNotificationChannel.name,
            appNotificationChannel.importance
        ).run(::createNotificationChannel)
    }
}

fun createNotification(
    context: Context,
    appNotificationChannel: AppNotificationChannel = AppNotificationChannel.Default
): NotificationCompat.Builder {
    NotificationManagerCompat.from(context)
        .setupNotificationChannel(appNotificationChannel)

    return NotificationCompat.Builder(context, appNotificationChannel.id).setAutoCancel(true)
}
