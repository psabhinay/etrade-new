package com.etrade.mobilepro.home.model

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobile.accounts.dto.InstitutionType
import com.etrade.mobilepro.R
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.accounts.util.android.view.AccountViewType
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.randomString
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

internal class UserViewModelTest {
    private val mockRes = lazy { mockResourcesString() }.value

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun `default state upon userView model creation`() {
        val vm = createViewModel()
        assert(vm.peekUserSessionState is SessionStateChange.UNAUTHENTICATED)
        assert(vm.previousSessionState is SessionStateChange.UNAUTHENTICATED)
    }

    @Test
    fun `accounts model content for Single Brokerage account`() {
        val mockAccounts = getBrokerageMockAccounts(1)
        val vm = createViewModel(mockAccounts)
        vm.userSessionStateChange(SessionStateChange.AUTHENTICATED)

        // simulate db fetch
        assertNotNull(vm.accountList)
        assertNotNull(vm.accountList)
        vm.accountList.let { accounts ->
            assertEquals(mockAccounts[0].accountId, accounts[0].account?.accountId)
            assertEquals(mockAccounts[0].accountShortName, accounts[0].accountDisplayName)
            assertEquals(AccountViewType.SINGLE_BROKERAGE_ACCOUNT_VIEW, accounts[0].accountViewType)
            assertEquals(1, accounts.size)
        }

        assertFalse(vm.hasMoreThanOneAccount)
    }

    @Test
    fun `accounts model content for Single BANK account`() {
        val mockAccounts = getBankMockAccounts()
        val vm = createViewModel(mockAccounts)
        vm.userSessionStateChange(SessionStateChange.AUTHENTICATED)

        // simulate db fetch
        assertNotNull(vm.accountList)
        vm.accountList.let { accounts ->
            assertEquals(mockAccounts[0].accountId, accounts[0].account?.accountId)
            assertEquals(mockAccounts[0].accountShortName, accounts[0].accountDisplayName)
            assertEquals(AccountViewType.BANK_ACCOUNT_VIEW, accounts[0].accountViewType)
            assertEquals(1, accounts.size)
        }
    }

    @Test
    fun `accounts model content for Single ETCS or STOCKPLAN account`() {
        val mockAccounts = getEtcsMockAccounts()
        val vm = createViewModel(mockAccounts)
        vm.userSessionStateChange(SessionStateChange.AUTHENTICATED)

        // simulate db fetch
        assertNotNull(vm.accountList)

        vm.accountList.let { accounts ->
            assertEquals(mockAccounts[0].accountId, accounts[0].account?.accountId)
            assertEquals(mockAccounts[0].accountShortName, accounts[0].accountDisplayName)
            assertEquals(AccountViewType.ESP_ACCOUNT_VIEW, accounts[0].accountViewType)
            assertEquals(1, accounts.size)
        }
    }

    @Test
    fun `accounts model content if user has Multiple accounts`() {
        val mockAccounts = getBrokerageMockAccounts(3)
        val vm = createViewModel(mockAccounts)
        vm.userSessionStateChange(SessionStateChange.AUTHENTICATED)

        assertNotNull(vm.accountList)

        vm.accountList.let { accounts ->
            assert(accounts.size == 5)

            // check for first and second elements
            assertEquals(
                accounts[0].accountViewType,
                AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW
            )
            assertEquals(accounts[1].accountViewType, AccountViewType.ALL_BROKERAGE_ACCOUNT_VIEW)
        }
        assertTrue(vm.hasMoreThanOneAccount)
    }

    @Test
    fun `accounts model content if user has Multiple accounts but only one is Brokerage`() {
        val mockAccounts = getBrokerageMockAccounts(3, 1)
        val vm = createViewModel(mockAccounts)
        vm.userSessionStateChange(SessionStateChange.AUTHENTICATED)

        assertNotNull(vm.accountList)

        vm.accountList.let { accounts ->
            assert(accounts.size == 4)

            // check for first and second elements
            assertEquals(
                accounts[0].accountViewType,
                AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW
            )
        }
        assertTrue(vm.hasMoreThanOneAccount)
    }

    @Test
    fun `On successful login accounts list service is called`() {
        val vm = createViewModel()
        vm.userSessionStateChange(SessionStateChange.AUTHENTICATED)
    }

    @Test
    fun `On logout view model contents should reset`() {
        val mockUserStateObserver = mock<Observer<ConsumableLiveEvent<SessionStateChange>>>()
        val mockAccounts = getBrokerageMockAccounts(1)
        val vm = createViewModel(mockAccounts)
        vm.userSessionState.observeForever(mockUserStateObserver)
        vm.userSessionStateChange(SessionStateChange.AUTHENTICATED)
        vm.userSessionStateChange(SessionStateChange.UNAUTHENTICATED)

        inOrder(mockUserStateObserver, mockUserStateObserver) {
            verify(mockUserStateObserver).onChanged(eq(ConsumableLiveEvent(SessionStateChange.AUTHENTICATED)))
            verify(mockUserStateObserver).onChanged(eq(ConsumableLiveEvent(SessionStateChange.UNAUTHENTICATED)))
        }
    }

    private fun mockResourcesString() = mock<Resources> {
        on { getString(R.string.title_complete_view) } doReturn ("Test1")
        on { getString(R.string.all_brokerage_title) } doReturn ("Test2")
    }

    private fun getBankMockAccounts(count: Int = 1): List<Account> {
        val mockAccounts = mutableListOf<Account>()
        for (index in count downTo 1) {
            mockAccounts.add(mockAccountsFromRealmDB(institutionType = InstitutionType.TELEBANK.name))
        }
        return mockAccounts
    }

    private fun getEtcsMockAccounts(count: Int = 1): List<Account> {
        val mockAccounts = mutableListOf<Account>()
        for (index in count downTo 1) {
            mockAccounts.add(mockAccountsFromRealmDB(institutionType = InstitutionType.OLINK.name))
        }

        return mockAccounts
    }

    private fun getBrokerageMockAccounts(
        count: Int = 0,
        numberOfBrokerageAccounts: Int = count
    ): List<Account> {
        val mockAccounts = emptyList<Account>().toMutableList()
        var brokerageAccounts = 0
        for (index in count downTo 1) {
            val accountType = "Brokerage".takeIf { brokerageAccounts != numberOfBrokerageAccounts }
                ?.also { brokerageAccounts++ }.orEmpty()
            mockAccounts.add(mockAccountsFromRealmDB(accountType))
        }

        return mockAccounts
    }

    private fun mockAccountsFromRealmDB(
        accountType: String = "",
        institutionType: String = ""
    ): Account {
        val accountId = ('0'..'9').randomString(8)
        val accountShortName = ('a'..'z').randomString(10)
        return Account(
            accountUuid = accountId,
            accountId = "",
            accountMode = "",
            accountDescription = "",
            accountShortName = accountShortName,
            accountLongName = "",
            _accountType = accountType,
            _institutionType = institutionType
        )
    }

    private fun createViewModel(mockAccounts: List<Account> = emptyList()): UserViewModel {
        val mockAccountDaoProvider = createMockAccountListProvider(mockAccounts)
        return UserViewModel(mockAccountDaoProvider, mockRes, mock(), mock(), mock(), mock())
    }

    private fun createMockAccountListProvider(mockAccounts: List<Account> = emptyList()) =
        mock<AccountListRepo> {
            on { getAccountList() } doReturn (mockAccounts)
        }
}
