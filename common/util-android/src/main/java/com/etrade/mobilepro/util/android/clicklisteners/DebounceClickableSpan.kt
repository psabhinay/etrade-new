package com.etrade.mobilepro.util.android.clicklisteners

import android.text.SpannableString
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View

/**
 * Ignore all the click events until [timeoutMillis] pass after a valid click event
 * Use this with [SpannableString.setSpan]
 *
 * @param action callback to be get called for valid click event
 * @param timeoutMillis duration to ignore all other click events after a valid click
 */
class DebounceClickableSpan(
    private val action: () -> Unit,
    private val timeoutMillis: Long = 1000
) : ClickableSpan() {

    private var canClick = true

    override fun onClick(widget: View) {
        if (canClick) {
            canClick = false
            action.invoke()
            widget.postDelayed(
                {
                    canClick = true
                },
                timeoutMillis
            )
        }
    }

    override fun updateDrawState(textPaint: TextPaint) {
        super.updateDrawState(textPaint)
        textPaint.isUnderlineText = false
    }
}
