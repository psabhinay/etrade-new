package com.etrade.mobilepro.chat.data.setup.dto.chatActiveStatus

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChatActiveStatusDataDto(
    @Json(name = "isActive") val isActive: Boolean,
    @Json(name = "metadata") val metadata: ChatActiveStatusMetadataDto
)

@JsonClass(generateAdapter = true)
data class ChatActiveStatusMetadataDto(
    @Json(name = "buttonName") val buttonName: String,
    @Json(name = "buttonId") val buttonId: String
)
