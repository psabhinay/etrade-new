package com.etrade.mobilepro.orders

import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel
import com.etrade.mobilepro.order.details.router.OrderConfirmationAction
import com.etrade.mobilepro.orders.viewmodel.OrderDetailsViewDelegateMap
import com.etrade.mobilepro.trade.presentation.util.SharedTradeViewModel
import com.etrade.mobilepro.trade.router.QuoteRouter
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.squareup.moshi.Moshi
import javax.inject.Inject
import javax.inject.Provider

@RequireLogin
class TradeOrderDetailsFragment @Inject constructor(
    moshi: Moshi,
    quoteRouter: QuoteRouter,
    snackbarUtilFactory: SnackbarUtilFactory,
    walkthroughViewModelProvider: Provider<WalkthroughStatesViewModel>,
    orderDetailsViewModelFactory: OrderDetailsViewModel.Factory,
    viewDelegateMap: OrderDetailsViewDelegateMap,
    private val viewModelFactory: ViewModelProvider.Factory,
    private val tradeRouter: TradeRouter,
) : OrderDetailsFragment(
    snackbarUtilFactory,
    orderDetailsViewModelFactory,
    moshi,
    quoteRouter,
    viewModelFactory,
    viewDelegateMap,
    walkthroughViewModelProvider
) {

    private val sharedTradeViewModel: SharedTradeViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }

    override fun setupViewModel(viewModel: OrderDetailsViewModel) {
        viewModel.viewModelProvider = ViewModelProvider(
            findNavController().getBackStackEntry(tradeRouter.navGraphId),
            viewModelFactory
        )
    }

    override fun onOrderConfirmed(orderId: String, orderConfirmationAction: OrderConfirmationAction) {
        super.onOrderConfirmed(orderId, orderConfirmationAction)
        sharedViewModel.requireRefreshOrderList()
        sharedViewModel.resetOrderDetailRequested = true
        tradeRouter.openConfirmation(findNavController(), orderId, orderConfirmationAction)
    }

    override fun onOrderDeleted(orderId: String) {
        super.onOrderDeleted(orderId)
        sharedTradeViewModel.clearOrder(orderId)
        sharedViewModel.sendTradeCanceledSignal()
    }
}
