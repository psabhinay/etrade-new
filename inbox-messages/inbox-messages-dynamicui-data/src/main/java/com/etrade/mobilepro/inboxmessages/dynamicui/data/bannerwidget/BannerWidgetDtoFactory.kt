package com.etrade.mobilepro.inboxmessages.dynamicui.data.bannerwidget

import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.dynamicui.bannerwidget.BannerWidgetViewDto
import javax.inject.Inject

class BannerWidgetDtoFactory @Inject constructor() {
    fun create(message: InboxMessage): BannerWidgetViewDto {
        return BannerWidgetViewDto(
            data = message,
            ctaList = null,
            clickActionDto = ClickActionDto(
                appUrl = message.deepLink
            )
        )
    }
}
