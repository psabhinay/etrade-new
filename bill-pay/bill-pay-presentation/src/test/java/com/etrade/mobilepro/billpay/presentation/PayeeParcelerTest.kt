package com.etrade.mobilepro.billpay.presentation

import android.os.Parcel
import android.os.Parcelable
import com.etrade.mobilepro.billpay.api.MerchantInfo
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.PayeeAddress
import com.etrade.mobilepro.billpay.presentation.common.ParcelablePayee
import com.etrade.mobilepro.billpay.presentation.common.toParcelable
import com.etrade.mobilepro.billpay.presentation.common.toPayee
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class PayeeParcelerTest {

    @Test
    fun `data A can be serialized and deserialized`() {
        testData(dataA)
    }

    @Test
    fun `data B can be serialized and deserialized`() {
        testData(dataB)
    }

    private fun testData(data: Payee) {
        val parcel = Parcel.obtain()
        data.toParcelable().run { writeToParcel(parcel, describeContents()) }
        parcel.setDataPosition(0)
        val creator = getParcelableCreator(ParcelablePayee::class.java)
        val result = creator.createFromParcel(parcel) as ParcelablePayee
        assertEquals(data, result.toPayee())
    }

    private val dataB = Payee(
        payeeId = "a1",
        name = "a2",
        nickName = null,
        phoneNumber = null,
        accountNumber = "a5",
        leadDays = 10,
        isAddressAvailable = null,
        address = null
    )

    private val dataA = Payee(
        payeeId = "a1",
        name = "a2",
        nickName = "a3",
        phoneNumber = "a4",
        accountNumber = "a5",
        leadDays = 10,
        isAddressAvailable = true,
        address = PayeeAddress(
            address1 = "a1",
            address2 = "a2",
            city = "a3",
            state = "a4",
            zipCode = "a5"
        ),
        merchantInfo = MerchantInfo(
            id = 20,
            merchantZip = "a2",
            merchantZip4 = "a3"
        ),
        merchantZipRequired = true,
        category = "a6"
    )

    companion object {
        @Suppress("UNCHECKED_CAST")
        fun getParcelableCreator(clazz: Class<out Parcelable>): Parcelable.Creator<out Parcelable> {
            val creatorField = clazz.getField("CREATOR")
            return creatorField.get(null) as Parcelable.Creator<Parcelable>
        }
    }
}
