package com.etrade.mobilepro.login.data

import android.content.Context
import com.etrade.mobilepro.common.di.Express
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.login.api.repo.LoginDataSource
import javax.inject.Inject

class DefaultLoginDataSource @Inject constructor(
    private val context: Context,
    @Web private val webBaseUrl: String,
    @Express private val expressBaseUrl: String
) : LoginDataSource {

    override fun getUrl(location: LoginDataSource.ExternalLocation): String {
        return when (location) {
            LoginDataSource.ExternalLocation.SIGN_UP -> expressBaseUrl + context.getString(com.etrade.mobilepro.R.string.sign_up_url)
            LoginDataSource.ExternalLocation.FORGOT_USERNAME -> webBaseUrl + context.getString(com.etrade.mobilepro.R.string.forgot_username_url)
            LoginDataSource.ExternalLocation.FORGOT_PWD -> webBaseUrl + context.getString(com.etrade.mobilepro.R.string.forgot_password_url)
            LoginDataSource.ExternalLocation.PRIVACY_POLICY -> webBaseUrl + context.getString(com.etrade.mobilepro.R.string.privacy_policy_url)
        }
    }
}
