package com.etrade.mobilepro.tracking.params

private const val PAGE_NAME = "PageName"
private const val PAGE_VIEWED_EVENT = "PageViewed"

class ScreenViewedAnalyticsParams(val screen: String) : AnalyticsParams {

    override val name: String = PAGE_VIEWED_EVENT

    override val params: Map<String, String>
        get() = mapOf(PAGE_NAME to screen)
}
