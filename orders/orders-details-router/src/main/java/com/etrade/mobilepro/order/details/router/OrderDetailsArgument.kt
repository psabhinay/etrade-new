package com.etrade.mobilepro.order.details.router

import android.os.Parcelable
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderType
import kotlinx.parcelize.Parcelize

sealed class OrderDetailsArgument : Parcelable {

    abstract val orderId: String
    abstract val orderStatus: OrderStatus
    abstract val symbols: List<String>

    abstract fun clone(status: OrderStatus): OrderDetailsArgument
    abstract fun isSame(other: OrderDetailsArgument?): Boolean
}

@Parcelize
data class GeneralOrderDetailsArgument(
    override val orderId: String,
    override val orderStatus: OrderStatus,
    override val symbols: List<String>,
    val accountId: String,
    val orderType: OrderType,
    val originalQuantity: String,
    val instrumentType: InstrumentType,
    val advancedOrderSymbol: String?,
    val advancedOrderDisplaySymbol: String?,
    override val snackBarMessage: String?
) : OrderDetailsArgument(), WithSnackBarMessage {

    val orderNumber: String
        get() = orderId

    override fun clone(status: OrderStatus): OrderDetailsArgument = copy(orderStatus = status)

    override fun isSame(other: OrderDetailsArgument?): Boolean {
        return if (other is GeneralOrderDetailsArgument) {
            accountId == other.accountId && orderId == other.orderId
        } else {
            false
        }
    }
}

@Parcelize
data class GeneralMutualFundOrderDetailsArgument(
    override val orderId: String,
    override val orderStatus: OrderStatus,
    override val symbols: List<String>
) : OrderDetailsArgument() {

    val orderNumber: String
        get() = orderId

    override fun clone(status: OrderStatus): OrderDetailsArgument = copy(orderStatus = status)

    override fun isSame(other: OrderDetailsArgument?): Boolean = equals(other)
}

@Parcelize
data class PreviewMutualFundOrderDetailsArgument(
    override val orderId: String,
    override val orderStatus: OrderStatus,
    override val symbols: List<String>,
    override val orderConfirmationAction: OrderConfirmationAction
) : OrderDetailsArgument(), WithOrderConfirmationAction {

    override fun clone(status: OrderStatus): OrderDetailsArgument = copy(orderStatus = status)

    override fun isSame(other: OrderDetailsArgument?): Boolean = equals(other)
}

@Parcelize
data class PreviewOrderDetailsArgument(
    override val orderId: String,
    override val orderStatus: OrderStatus,
    override val symbols: List<String>,
    override val orderConfirmationAction: OrderConfirmationAction,
    val allowCancel: Boolean = true
) : OrderDetailsArgument(), WithOrderConfirmationAction {

    override fun clone(status: OrderStatus): OrderDetailsArgument = copy(orderStatus = status)

    override fun isSame(other: OrderDetailsArgument?): Boolean = equals(other)
}

@Parcelize
data class StockPlanOrderDetailsArgument(
    override val orderId: String,
    override val orderStatus: OrderStatus,
    override val symbols: List<String>
) : OrderDetailsArgument() {

    val orderNumber: String
        get() = orderId

    override fun clone(status: OrderStatus): OrderDetailsArgument = copy(orderStatus = status)

    override fun isSame(other: OrderDetailsArgument?): Boolean {
        return if (other is StockPlanOrderDetailsArgument) {
            orderId == other.orderId
        } else {
            false
        }
    }
}

interface WithSnackBarMessage {
    val snackBarMessage: String?
}

fun OrderDetailsArgument.isOpen() = orderStatus == OrderStatus.OPEN
