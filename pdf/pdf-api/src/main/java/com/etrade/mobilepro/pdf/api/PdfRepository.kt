package com.etrade.mobilepro.pdf.api

import com.etrade.mobilepro.common.result.ETResult
import java.io.File

interface PdfRepository {
    /**
     * @param url : The url to download from
     * @param filePath: File path to store the pdf
     * @return File : The file holding the pdf
     * Downloads a pdf from the server with given [url]
     * and returns a file representation of that pdf
     * or an error encapsulating what went wrong.
     */
    suspend fun downloadPdf(url: String, filePath: File): ETResult<File>
}
