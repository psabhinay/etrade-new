package com.etrade.mobilepro.util

import android.content.Context
import java.io.File
import javax.inject.Inject

/**
 * Production implementation of [DatabaseLocator] that delegates its functions to the app's [context].
 *
 * @param context app's context
 */
class DefaultDatabaseLocator @Inject constructor(private val context: Context) : DatabaseLocator {

    override fun getDatabasePath(name: String): File = context.getDatabasePath(name)
}
