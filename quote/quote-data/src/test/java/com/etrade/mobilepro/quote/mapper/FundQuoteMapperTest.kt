package com.etrade.mobilepro.quote.mapper

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.R
import com.etrade.mobilepro.quote.dto.FundQuoteDto
import com.etrade.mobilepro.quoteapi.FeeDetail
import com.etrade.mobilepro.quoteapi.FundClassInfo
import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.quoteapi.QuoteChangeWidgetItem
import com.etrade.mobilepro.quoteapi.QuoteTemplateResources
import com.etrade.mobilepro.testutil.getObjectFromJson
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

private const val GREEN = 0xFF00FF00.toInt()
private const val RED = 0xFFFF0000.toInt()

internal class FundQuoteMapperTest {

    private lateinit var sut: FundQuoteDetailItemsMapper

    private lateinit var mapperResources: QuoteTemplateResources

    @Before
    fun setUp() {
        mapperResources = getQuoteResources()
    }

    @Test
    fun `map FundQuoteDto to MutualFundQuoteDetailItem`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        val result = sut.getMutualFundQuoteDetail(quotes)
        assertNotNull(result)

        assertEquals("10.120000", result.netAssetValue)
        assertEquals(QuoteChangeWidgetItem("0.000000", "0.00"), result.change)
        assertEquals(GREEN, result.changeColor)
        assertEquals("10.120000", result.publicOfferPrice)
        assertEquals("10.120000", result.previousClose)
        assertEquals("Dws", result.familyName)
        assertEquals("NASDAQ as of 07/29/2019", result.timeStamp)
    }

    @Test
    fun `invalid map FundQuoteDto to MutualFundQuoteDetailItem`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getInvalidJson()!!)
        val result = sut.getMutualFundQuoteDetail(quotes)
        assertNotNull(result)

        assertEquals(mapperResources.valueMissing, result.netAssetValue)
        assertEquals(QuoteChangeWidgetItem("", ""), result.change)
        assertEquals(GREEN, result.changeColor)
        assertEquals(mapperResources.valueMissing, result.publicOfferPrice)
        assertEquals(mapperResources.valueMissing, result.previousClose)
        assertEquals(mapperResources.valueMissing, result.familyName)
        assertNull(result.timeStamp)
    }

    @Test
    fun `map FundQuoteDto to MoneyMarketFundQuoteDetailItem`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        val result = sut.getMoneyMarketFundQuoteDetail(quotes)

        assertNotNull(result)
        assertEquals("2.13", result.sevenDayYield)
        assertEquals("10.740000", result.annualTotalReturn)
        assertEquals("0.000000", result.weightedMaturity)
        assertEquals("Dws", result.familyName)
        assertEquals("NASDAQ as of 07/29/2019", result.timeStamp)
    }

    @Test
    fun `invalid map FundQuoteDto to MoneyMarketFundQuoteDetailItem`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getInvalidJson()!!)
        val result = sut.getMoneyMarketFundQuoteDetail(quotes)
        assertNotNull(result)

        assertEquals(mapperResources.valueMissing, result.sevenDayYield)
        assertEquals(mapperResources.valueMissing, result.annualTotalReturn)
        assertEquals(mapperResources.valueMissing, result.weightedMaturity)
        assertEquals(mapperResources.valueMissing, result.familyName)
        assertNull(result.timeStamp)
    }

    @Test
    fun `map FundQuoteDto to FundClassSection`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        val result = sut.getMutualFundClassSection(quotes)

        assertNotNull(result)
        assertEquals("Class", result.title)
        assertEquals(IndexedValue(1, FundClassInfo("AAAAX", "", InstrumentType.MF, "A")), result.selectedFundClass)
        val fundClassList = listOf(
            FundClassInfo("AAAPX", "", InstrumentType.MF, "C"),
            FundClassInfo("AAAAX", "", InstrumentType.MF, "A"),
            FundClassInfo("AAAZX", "", InstrumentType.MF, "Inst"),
            FundClassInfo("AAASX", "", InstrumentType.MF, "S")
        )
        assertEquals(fundClassList, result.fundClassInfoList)
    }

    @Test
    fun `invalid map FundQuoteDto to FundClassSection`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getInvalidJson()!!)
        val result = sut.getMutualFundClassSection(quotes)

        assertNotNull(result)
        assertEquals("Class", result.title)
        assertNull(result.selectedFundClass)
        assertEquals(emptyList<FundClassInfo>(), result.fundClassInfoList)
    }

    @Test
    fun `map FundQuoteDto to FundIconSection`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        val result = sut.getFundIconSection(quotes)

        assertNotNull(result)
        assertEquals(true, result.hasNoTransactionsFee)
        assertEquals(false, result.isAllStar)
    }

    @Test
    fun `invalid map FundQuoteDto to FundIconSection`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getInvalidJson()!!)
        val result = sut.getFundIconSection(quotes)

        assertNotNull(result)
        assertEquals(false, result.hasNoTransactionsFee)
        assertEquals(false, result.isAllStar)
    }

    @Test
    fun `map FundQuoteDto to MorningStarRatings`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        val result = sut.getMorningStarRatings(quotes.fundDetails)

        assertNotNull(result)
        assertEquals("05/31/2019", result.asOfDate)
        assertEquals(405, result.numFundsInOverallRating)
        assertEquals(R.drawable.fund_ratings_2, result.overAllDrawable)
        assertEquals(405, result.numFundsInMsRatingFor3y)
        assertEquals(R.drawable.fund_ratings_4, result.threeYearsDrawable)
        assertEquals(323, result.numFundsInMsRatingFor5y)
        assertEquals(R.drawable.fund_ratings_3, result.fiveYearsDrawable)
        assertEquals(168, result.numFundsInMsRatingFor10y)
        assertEquals(R.drawable.fund_ratings_1, result.tenYearsDrawable)
    }

    @Test
    fun `invalid map FundQuoteDto to MorningStarRatings`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getInvalidJson()!!)
        val result = sut.getMorningStarRatings(quotes.fundDetails)

        assertNotNull(result)
        assertEquals(mapperResources.valueMissing, result.asOfDate)
        assertEquals(0, result.numFundsInOverallRating)
        assertEquals(R.drawable.fund_ratings_0, result.overAllDrawable)
        assertEquals(0, result.numFundsInMsRatingFor3y)
        assertEquals(R.drawable.fund_ratings_0, result.threeYearsDrawable)
        assertEquals(0, result.numFundsInMsRatingFor5y)
        assertEquals(R.drawable.fund_ratings_0, result.fiveYearsDrawable)
        assertEquals(0, result.numFundsInMsRatingFor10y)
        assertEquals(R.drawable.fund_ratings_0, result.tenYearsDrawable)
    }

    @Test
    fun `map FundQuoteDto to MorningStarDetails`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        val result = sut.getMorningStarDetails(quotes.fundDetails)

        assertNotNull(result)
        assertEquals("World Allocation", result.morningStarCategory)
        assertEquals("Open To New Investors", result.availability)
    }

    @Test
    fun `invalid map FundQuoteDto to MorningStarDetails`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getInvalidJson()!!)
        val result = sut.getMorningStarDetails(quotes.fundDetails)

        assertNotNull(result)
        assertEquals(mapperResources.valueMissing, result.morningStarCategory)
        assertEquals(mapperResources.valueMissing, result.availability)
    }

    @Test
    fun `map FundQuoteDto to QuarterlyReturns`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        val result = sut.getQuarterlyReturns(quotes.fundDetails)

        assertNotNull(result)
        assertEquals("03/31/2019", result.asOfDate)
        assertEquals("12.79461", result.quarterlyTrailingReturnYtd)
        assertEquals("9.4878", result.quarterlyTrailingReturn1y)
        assertEquals("2.93621", result.quarterlyTrailingReturn5y)
        assertEquals("6.38769", result.quarterlyTrailingReturn10y)
    }

    @Test
    fun `invalid map FundQuoteDto to QuarterlyReturns`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getInvalidJson()!!)
        val result = sut.getQuarterlyReturns(quotes.fundDetails)

        assertNotNull(result)
        assertEquals(mapperResources.valueMissing, result.asOfDate)
        assertEquals(mapperResources.valueMissing, result.quarterlyTrailingReturnYtd)
        assertEquals(mapperResources.valueMissing, result.quarterlyTrailingReturn1y)
        assertEquals(mapperResources.valueMissing, result.quarterlyTrailingReturn5y)
        assertEquals(mapperResources.valueMissing, result.quarterlyTrailingReturn10y)
    }

    @Test
    fun `map FundQuoteDto to SummarySection`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        val result = sut.getSummarySection(quotes.fundDetails)

        assertNotNull(result)
        assertEquals("1.22", result.netExpenseRatio)
        assertEquals("1.4", result.grossExpenseRatio)
        assertEquals("522650890", result.totalNetAssets)
        assertEquals("07/30/2007", result.inceptionDate)
        assertEquals("0", result.thirtyDaySecYield)
        assertEquals("1.44", result.oneYearSecYield)
        assertEquals("1000.000000", result.initialInvestment)
        assertEquals("0.000000", result.subsequentInvestment)
        assertEquals("Front-end Load (Waived at E*TRADE)", result.salesCharge)
        assertEquals("None", result.fundRedemptionFee)
        assertEquals("None", result.etradeTransactionFee)
    }

    @Test
    fun `invalid map FundQuoteDto to SummarySection`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getInvalidJson()!!)
        val result = sut.getSummarySection(quotes.fundDetails)

        assertNotNull(result)
        assertEquals(mapperResources.valueMissing, result.netExpenseRatio)
        assertEquals(mapperResources.valueMissing, result.grossExpenseRatio)
        assertEquals(mapperResources.valueMissing, result.totalNetAssets)
        assertEquals(mapperResources.valueMissing, result.inceptionDate)
        assertEquals(mapperResources.valueMissing, result.thirtyDaySecYield)
        assertEquals(mapperResources.valueMissing, result.oneYearSecYield)
        assertEquals(mapperResources.valueMissing, result.initialInvestment)
        assertEquals(mapperResources.valueMissing, result.subsequentInvestment)
        assertEquals(mapperResources.valueMissing, result.salesCharge)
        assertEquals(mapperResources.valueMissing, result.fundRedemptionFee)
        assertEquals(mapperResources.valueMissing, result.etradeTransactionFee)
    }

    @Test
    fun `Invalid map FundQuoteDto to FundPerformanceItem`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getInvalidJson()!!)
        val result = sut.getFundCategoryPerformanceItem(quotes)
        assertNotNull(result)

        assertEquals("", result.symbol)
        assertEquals(mapperResources.valueMissing, result.fundCategoryName)
        assertEquals(mapperResources.valueMissing, result.nonAdjMonthlyTrailingReturnYtd)
        assertEquals(mapperResources.valueMissing, result.nonAdjMonthlyTrailingReturn1y)
        assertEquals(mapperResources.valueMissing, result.nonAdjMonthlyTrailingReturn5y)
        assertEquals(mapperResources.valueMissing, result.nonAdjMonthlyTrailingReturn10y)
        assertEquals(mapperResources.valueMissing, result.adjMonthlyTrailingReturnYtd)
        assertEquals(mapperResources.valueMissing, result.adjMonthlyTrailingReturn1y)
        assertEquals(mapperResources.valueMissing, result.adjMonthlyTrailingReturn5y)
        assertEquals(mapperResources.valueMissing, result.adjMonthlyTrailingReturn10y)
        assertEquals(mapperResources.valueMissing, result.nonAdjQuarterlyTrailingReturnYtd)
        assertEquals(mapperResources.valueMissing, result.nonAdjQuarterlyTrailingReturn1y)
        assertEquals(mapperResources.valueMissing, result.nonAdjQuarterlyTrailingReturn5y)
        assertEquals(mapperResources.valueMissing, result.nonAdjQuarterlyTrailingReturn10y)
        assertEquals(mapperResources.valueMissing, result.adjQuarterlyTrailingReturnYtd)
        assertEquals(mapperResources.valueMissing, result.adjQuarterlyTrailingReturn1y)
        assertEquals(mapperResources.valueMissing, result.adjQuarterlyTrailingReturn5y)
        assertEquals(mapperResources.valueMissing, result.adjQuarterlyTrailingReturn10y)
        assertEquals(mapperResources.valueMissing, result.monthlyLoadAdjTrailingReturnYtd)
        assertEquals(mapperResources.valueMissing, result.monthlyLoadAdjTrailingReturn1y)
        assertEquals(mapperResources.valueMissing, result.monthlyLoadAdjTrailingReturn5y)
        assertEquals(mapperResources.valueMissing, result.monthlyLoadAdjTrailingReturn10y)
        assertEquals(mapperResources.valueMissing, result.quarterlyLoadAdjTrailingReturnYtd)
        assertEquals(mapperResources.valueMissing, result.quarterlyLoadAdjTrailingReturn1y)
        assertEquals(mapperResources.valueMissing, result.quarterlyLoadAdjTrailingReturn5y)
        assertEquals(mapperResources.valueMissing, result.quarterlyLoadAdjTrailingReturn10y)
        assertEquals(mapperResources.valueMissing, result.monthlyTrailingReturnYtd)
        assertEquals(mapperResources.valueMissing, result.monthlyTrailingReturn1y)
        assertEquals(mapperResources.valueMissing, result.monthlyTrailingReturn5y)
        assertEquals(mapperResources.valueMissing, result.monthlyTrailingReturn10y)
        assertEquals(mapperResources.valueMissing, result.quarterlyTrailingReturnYtd)
        assertEquals(mapperResources.valueMissing, result.quarterlyTrailingReturn1y)
        assertEquals(mapperResources.valueMissing, result.quarterlyTrailingReturn5y)
        assertEquals(mapperResources.valueMissing, result.quarterlyTrailingReturn10y)
        assertEquals(mapperResources.valueMissing, result.inceptionDate)
        assertEquals(mapperResources.valueMissing, result.performanceAsOfDate)
        assertEquals(mapperResources.valueMissing, result.quarteryPerformanceAsOfDate)
        assertEquals(mapperResources.valueMissing, result.monthlyLoadAdjTrailingReturnSinceInception)
        assertEquals(mapperResources.valueMissing, result.monthlyTrailingReturnSinceInception)
        assertEquals(mapperResources.valueMissing, result.quarterlyLoadAdjTrailingReturnSinceInception)
        assertEquals(mapperResources.valueMissing, result.quarterlyTrailingReturnSinceInception)
    }

    @Test
    fun `map FundQuoteDto to FundPerformanceItem`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        val result = sut.getFundCategoryPerformanceItem(quotes)
        assertNotNull(result)

        assertEquals("AAAAX", result.symbol)
        assertEquals("Morningstar World Allocation", result.fundCategoryName)
        assertEquals("6.06388", result.nonAdjMonthlyTrailingReturnYtd)
        assertEquals("-1.71526", result.nonAdjMonthlyTrailingReturn1y)
        assertEquals("2.14952", result.nonAdjMonthlyTrailingReturn5y)
        assertEquals("6.79477", result.nonAdjMonthlyTrailingReturn10y)
        assertEquals("5.21372", result.adjMonthlyTrailingReturnYtd)
        assertEquals("-2.53405", result.adjMonthlyTrailingReturn1y)
        assertEquals("1.94887", result.adjMonthlyTrailingReturn5y)
        assertEquals("6.67846", result.adjMonthlyTrailingReturn10y)
        assertEquals("7.81483", result.nonAdjQuarterlyTrailingReturnYtd)
        assertEquals("0.09148", result.nonAdjQuarterlyTrailingReturn1y)
        assertEquals("3.01394", result.nonAdjQuarterlyTrailingReturn5y)
        assertEquals("8.37943", result.nonAdjQuarterlyTrailingReturn10y)
        assertEquals("1.1", result.adjQuarterlyTrailingReturnYtd)
        assertEquals("2.2", result.adjQuarterlyTrailingReturn1y)
        assertEquals("3.3", result.adjQuarterlyTrailingReturn5y)
        assertEquals("4.4", result.adjQuarterlyTrailingReturn10y)
        assertEquals("5.14534", result.monthlyLoadAdjTrailingReturnYtd)
        assertEquals("-1.63132", result.monthlyLoadAdjTrailingReturn1y)
        assertEquals("1.06043", result.monthlyLoadAdjTrailingReturn5y)
        assertEquals("4.04047", result.monthlyLoadAdjTrailingReturn10y)
        assertEquals("6.30892", result.quarterlyLoadAdjTrailingReturnYtd)
        assertEquals("3.19225", result.quarterlyLoadAdjTrailingReturn1y)
        assertEquals("1.72423", result.quarterlyLoadAdjTrailingReturn5y)
        assertEquals("5.75953", result.quarterlyLoadAdjTrailingReturn10y)
        assertEquals("11.56004", result.monthlyTrailingReturnYtd)
        assertEquals("4.36995", result.monthlyTrailingReturn1y)
        assertEquals("2.2645", result.monthlyTrailingReturn5y)
        assertEquals("4.65842", result.monthlyTrailingReturn10y)
        assertEquals("12.79461", result.quarterlyTrailingReturnYtd)
        assertEquals("9.4878", result.quarterlyTrailingReturn1y)
        assertEquals("2.93621", result.quarterlyTrailingReturn5y)
        assertEquals("6.38769", result.quarterlyTrailingReturn10y)
        assertEquals("07/30/2007", result.inceptionDate)
        assertEquals("05/31/2019", result.performanceAsOfDate)
        assertEquals("03/31/2019", result.quarteryPerformanceAsOfDate)
        assertEquals("2.00", result.monthlyLoadAdjTrailingReturnSinceInception)
        assertEquals("2.51", result.monthlyTrailingReturnSinceInception)
        assertEquals("2.12", result.quarterlyLoadAdjTrailingReturnSinceInception)
        assertEquals("2.64", result.quarterlyTrailingReturnSinceInception)
    }

    @Test
    fun `map FundQuoteDto to QuoteFundFooterItem`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        val result = sut.getQuoteFundFooterItem(quotes)

        assertNotNull(result)
        val expected = String.format(
            mapperResources.fundDescriptionFormat,
            "World Allocation",
            "The investment seeks total return in excess of inflation through capital growth and current income. The fund will invest at least 80% of its " +
                "net assets, plus the amount of any borrowings for investment purposes, in a combination of investments that the Advisor believes offer " +
                "exposure to \"real assets.\" It generally invests between 25% and 75% of fund assets in securities of foreign issuers, including up to " +
                "10% of fund assets in issuers located in countries with new or emerging markets."
        )
        assertEquals(expected, result.descriptionText)
    }

    @Test
    fun `invalid map FundQuoteDto to QuoteFundFooterItem`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getInvalidJson()!!)
        val result = sut.getQuoteFundFooterItem(quotes)

        assertNotNull(result)
        val expected = String.format(
            mapperResources.fundDescriptionFormat,
            mapperResources.valueMissing,
            mapperResources.fundDescriptionNotFound
        )
        assertEquals(expected, result.descriptionText)
    }

    @Test
    fun `map FundQuoteDto to FeesAndExpensesItem`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        val result = sut.getFeesAndExpensesItem(quotes.fundDetails)

        assertNotNull(result)
        assertEquals("Front-end Load (Waived at E*TRADE)", result.salesCharge)
        assertEquals("None", result.fundRedemptionFee)
        assertEquals("None", result.etradeTransactionFee)
        assertEquals("90 days or less - $49.99", result.etradeEarlyRedemptionFee)
        assertEquals("Open To New Investors", result.availability)
        assertEquals("$1,000.00", result.initialInvestment)
        assertEquals("$0.00", result.subsequentInvestment)
        assertEquals("$0.00", result.initialIra)
        assertEquals("$0.00", result.subsequentIra)
        assertEquals("1.22%", result.netExpenseRatio)
        assertEquals("1.40%", result.grossExpenseRatio)
        assertEquals("0.80%", result.managementFee)
        assertEquals("0.10%", result.administrationFee)
        assertEquals("0.25%", result.twelveB1Fee)
        assertTrue(result.additionalFeeDetails.first == true)
        assertEquals(4, result.additionalFeeDetails.second.size)
        assertEquals(FeeDetail("5.75%", "$0 - $49,999"), result.additionalFeeDetails.second[0])
        assertEquals(FeeDetail("4.50%", "$50,000 - $99,999"), result.additionalFeeDetails.second[1])
        assertEquals(FeeDetail("3.50%", "$100,000 - $249,999"), result.additionalFeeDetails.second[2])
        assertEquals(FeeDetail("0.00%", "> $250,000"), result.additionalFeeDetails.second[3])
    }

    @Test
    fun `map MutualFundQuoteDto to MobileQuote`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val result = MobileQuoteMapper().map(getMockQuotes()!!)

        assertNotNull(result)

        assertThat("AAAAX", equalTo(result.symbol))
        assertThat("DWS RREEF REAL ASSETS A", equalTo(result.description))
        assertThat("BETA", equalTo(result.exchangeCode))
        assertThat("US Mutual Fund Providers", equalTo(result.exchangeName))
        assertThat(InstrumentType.from("MF"), equalTo(result.typeCode))
        assertThat("1564440300", equalTo(result.lastTradeTime))
        assertThat(2, equalTo(result.delayedOrRtq))
        assertThat("EST", equalTo(result.timeZone))
        assertThat("25159K879", equalTo(result.cusip))
        assertThat("1.44", equalTo(result.oneYYield))
        assertThat(false, equalTo(result.allStar))
        assertThat("Open To New Investors", equalTo(result.availability))
    }

    @Test
    fun `invalid map FundQuoteDto to FeesAndExpensesItem`() {
        sut = DefaultFundQuoteDetailItemsMapper(mapperResources)
        val quotes = MobileQuoteMapper().map(getInvalidJson()!!)
        val result = sut.getFeesAndExpensesItem(quotes.fundDetails)

        assertNotNull(result)
        assertEquals(mapperResources.valueMissing, result.salesCharge)
        assertEquals(mapperResources.valueMissing, result.fundRedemptionFee)
        assertEquals(mapperResources.valueMissing, result.etradeTransactionFee)
        assertEquals(mapperResources.valueMissing, result.etradeEarlyRedemptionFee)
        assertEquals(mapperResources.valueMissing, result.availability)
        assertEquals(mapperResources.valueMissing, result.initialInvestment)
        assertEquals(mapperResources.valueMissing, result.subsequentInvestment)
        assertEquals(mapperResources.valueMissing, result.initialIra)
        assertEquals(mapperResources.valueMissing, result.subsequentIra)
        assertEquals(mapperResources.valueMissing, result.netExpenseRatio)
        assertEquals(mapperResources.valueMissing, result.grossExpenseRatio)
        assertEquals(mapperResources.valueMissing, result.managementFee)
        assertEquals(mapperResources.valueMissing, result.administrationFee)
        assertEquals(mapperResources.valueMissing, result.twelveB1Fee)
        assertNull(result.additionalFeeDetails.first)
        assertTrue(result.additionalFeeDetails.second.isEmpty())
    }

    private fun getMockQuotes(): FundQuoteDto? {
        return MobileQuoteWidgetItemMapper::class.getObjectFromJson<FundQuoteDto>("mutual_fund_quote.json")
    }

    private fun getInvalidJson(): FundQuoteDto? {
        return MobileQuoteWidgetItemMapper::class.getObjectFromJson<FundQuoteDto>("invalid_quotes.json")
    }
}
