/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.etrade.mobilepro.tooltip.viewmodel;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.etrade.mobilepro.tooltip.viewmodel";
  public static final String BUILD_TYPE = "debug";
}
