package com.etrade.mobilepro.util.json

import com.etrade.mobilepro.util.ObjectArrayMismatchAdapter
import com.squareup.moshi.Json
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

internal class ObjectArrayMismatchAdapterTest {

    private val sut = ObjectArrayMismatchAdapter.newFactory(TestObject::class.java)

    private val moshi = Moshi.Builder().add(sut).build()

    private val testValue = "testValue"

    private val adapter: JsonAdapter<TestResponse> = moshi.adapter(TestResponse::class.java)

    @Test
    fun `parses string list`() {
        val input =
            """{ "testList": [ { "value": "" }, { "value": "" }, { "value": "" } ] } """
        assertThat(adapter.fromJson(input)?.testList?.size, `is`(3))
    }

    @Test
    fun `parses single test object`() {
        val input =
            """{ "testList": { "value": "$testValue" } } """
        assertThat(adapter.fromJson(input)?.testList?.size, `is`(1))
        assertThat(adapter.fromJson(input)?.testList?.get(0)?.test, `is`(testValue))
    }

    @JsonClass(generateAdapter = true)
    internal class TestObject(
        @Json(name = "value")
        val test: String?
    )

    @JsonClass(generateAdapter = true)
    internal class TestResponse(
        @Json(name = "testList")
        val testList: List<TestObject?>?
    )
}
