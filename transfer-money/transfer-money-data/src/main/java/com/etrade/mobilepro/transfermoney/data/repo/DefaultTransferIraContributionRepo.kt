package com.etrade.mobilepro.transfermoney.data.repo

import com.etrade.mobile.accounts.details.AccountsDetailsRepo
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.transfermoney.api.TransferIraContribution
import com.etrade.mobilepro.transfermoney.api.repo.TransferIraContributionRepo
import com.etrade.mobilepro.transfermoney.data.rest.IraContribInfoDto
import com.etrade.mobilepro.transfermoney.data.rest.TransferIraContributionApiClient
import com.etrade.mobilepro.transfermoney.data.rest.YearContribDto
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.Locale
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val EMPTY_STRING_VALUE = "--"

private val CASH_TRANSFER_DATE_FORMAT by lazy { DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US) }

private val SECONDS_IN_DAY = TimeUnit.DAYS.toSeconds(1)

class DefaultTransferIraContributionRepo @Inject constructor(
    private val apiClient: TransferIraContributionApiClient,
    private val accountsDetailsRepo: AccountsDetailsRepo,
    private val consumerKeyProvider: () -> String?
) : TransferIraContributionRepo {

    private val accIdToKeyMap = mutableMapOf<String, String>()

    override suspend fun getIraContributionInfo(fromAccountId: String): ETResult<TransferIraContribution> = runCatchingET {
        val accountIdKey = getAccountIdKey(fromAccountId)
        apiClient
            .getTransferIraContributionInfo(
                consumerKey = consumerKeyProvider.invoke() ?: "",
                accountIdKey = accountIdKey,
                taxDate = LocalDate.now().format(CASH_TRANSFER_DATE_FORMAT),
                fromAccountId = fromAccountId
            )
            .infoDto?.let {
                val prevYearDetails = mapYearContribDto(it.previousYearContribDto)
                val currentYearDetails = mapYearContribDto(it.currentYearContribDto)
                val eligibleYears = mapEligibleYears(it)
                object : TransferIraContribution {
                    override val year: Int = it.year
                    override val accountId: String = it.accountId ?: ""
                    override val accountIdKey: String = it.accountIdKey ?: ""
                    override val previousYearDetails: TransferIraContribution.Details? = prevYearDetails
                    override val currentYearDetails: TransferIraContribution.Details? = currentYearDetails
                    override val eligibleYears: List<Int> = eligibleYears
                }
            } ?: throw IllegalArgumentException("Wrong ira contribution response")
    }

    private suspend fun getAccountIdKey(accountId: String): String {
        val accKeyCached = accIdToKeyMap[accountId]
        if (accKeyCached == null) {
            val accountsDetails = accountsDetailsRepo.getAccountsDetailedList(consumerKeyProvider.invoke() ?: "")
            accountsDetails.accounts.forEach { details ->
                accIdToKeyMap[details.accountId] = details.accountIdKey
            }
        }
        val accKeyFetched = accIdToKeyMap[accountId] ?: throw IllegalArgumentException("Cannot find account key for $accountId")
        return accKeyCached ?: accKeyFetched
    }

    private fun mapYearContribDto(dto: YearContribDto?): TransferIraContribution.Details? {
        if (dto == null) {
            return null
        }
        return object : TransferIraContribution.Details {
            override val taxYear: Int = dto.taxYear
            override val taxDueDate: LocalDate = LocalDate.ofEpochDay(dto.taxDueDate.div(SECONDS_IN_DAY))
            override val amountLeft: String = dto.amountLeft ?: EMPTY_STRING_VALUE
            override val maxLimit: String = dto.maxLimit ?: EMPTY_STRING_VALUE
            override val amount: String = dto.amount ?: EMPTY_STRING_VALUE
        }
    }

    private fun mapEligibleYears(dto: IraContribInfoDto): List<Int> =
        listOfNotNull(
            dto.eligibleContribTaxYearsDto?.taxYear1,
            dto.eligibleContribTaxYearsDto?.taxYear2
        )
}
