package com.etrade.mobilepro.dagger

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import com.etrade.mobilepro.R
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.dynamicui.CachedResponseModel
import com.etrade.mobilepro.order.details.DefaultOrderDetailsRepo
import com.etrade.mobilepro.order.details.api.OrderDetailDescriptionMapperType
import com.etrade.mobilepro.order.details.api.OrderDetailsRepo
import com.etrade.mobilepro.order.details.mapper.OrderDetailDescriptionMapper
import com.etrade.mobilepro.order.details.mapper.OrderDetailsMapper
import com.etrade.mobilepro.order.details.presentation.GeneralOrder
import com.etrade.mobilepro.order.details.presentation.MutualFundOrder
import com.etrade.mobilepro.order.details.presentation.OrderDetailItemInterfaceMapperType
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel
import com.etrade.mobilepro.order.details.presentation.PreviewMutualFundOrder
import com.etrade.mobilepro.order.details.presentation.PreviewOrder
import com.etrade.mobilepro.order.details.presentation.StockPlanOrder
import com.etrade.mobilepro.order.details.presentation.delegate.GeneralOrderDetailsDelegate
import com.etrade.mobilepro.order.details.presentation.delegate.MutualFundOrderDetailsDelegate
import com.etrade.mobilepro.order.details.presentation.delegate.PreviewMutualFundsOrderDetailsDelegate
import com.etrade.mobilepro.order.details.presentation.delegate.PreviewOrderDetailsDelegate
import com.etrade.mobilepro.order.details.presentation.delegate.StockPlanOrderDetailsDelegate
import com.etrade.mobilepro.order.details.rest.OrderDetailsApiClient
import com.etrade.mobilepro.order.details.router.OrderDetailsRouter
import com.etrade.mobilepro.orders.OrderDetailsRouterImpl
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.orders.caching.CachedOrdersDataSourceDelegate
import com.etrade.mobilepro.orders.caching.CachedOrdersRepo
import com.etrade.mobilepro.orders.list.api.OrderParamsLoadSizeAndKey
import com.etrade.mobilepro.orders.list.api.OrdersRepo
import com.etrade.mobilepro.orders.list.rest.OrdersListApiClient
import com.etrade.mobilepro.orders.mapper.OrderDetailPageItemMapper
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.orders.presentation.section.OrderDetailSectionMapper
import com.etrade.mobilepro.orders.presentation.section.OrderLabels
import com.etrade.mobilepro.orders.presentation.section.OrderResources
import com.etrade.mobilepro.orders.presentation.section.viewfactory.MutualFundOrderDetailSectionViewFactory
import com.etrade.mobilepro.orders.presentation.section.viewfactory.MutualOrderSectionViewFactory
import com.etrade.mobilepro.orders.sharedviewmodel.OrderRefreshDelegate
import com.etrade.mobilepro.orders.sharedviewmodel.OrderSharedViewModel
import com.etrade.mobilepro.orders.viewmodel.OrdersHelper
import com.etrade.mobilepro.stockplan.AccountIndexHandler
import com.etrade.mobilepro.stockplan.AccountTypeHandler
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import java.util.Currency
import java.util.Locale
import javax.inject.Singleton

@Module
interface OrdersModule {

    /* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */

    @Binds
    @Singleton
    fun bindCachedOrdersDataSourceDelegate(delegate: CachedOrdersDataSourceDelegate): SimpleCachedResource.DataSourceDelegate<CachedResponseModel,
        OrderParamsLoadSizeAndKey>

    @Binds
    @Singleton
    fun bindCachedOrdersRepo(cachedOrdersRepo: CachedOrdersRepo): OrdersRepo

    @Binds
    @GeneralOrder
    fun bindGeneralOrderDetailsDelegate(delegateGeneral: GeneralOrderDetailsDelegate): OrderDetailsViewModel.OrderDetailsDelegate

    @Binds
    @MutualFundOrder
    fun bindMutualFundOrderDetailsDelegate(delegateGeneral: MutualFundOrderDetailsDelegate): OrderDetailsViewModel.OrderDetailsDelegate

    @Binds
    @Singleton
    fun bindOrderDetailsRouter(router: OrderDetailsRouterImpl): OrderDetailsRouter

    @Binds
    @PreviewMutualFundOrder
    fun bindPreviewMutualFundOrderDetailsDelegate(delegateGeneral: PreviewMutualFundsOrderDetailsDelegate): OrderDetailsViewModel.OrderDetailsDelegate

    @Binds
    @PreviewOrder
    fun bindPreviewOrderDetailsDelegate(delegateGeneral: PreviewOrderDetailsDelegate): OrderDetailsViewModel.OrderDetailsDelegate

    @Binds
    @StockPlanOrder
    fun bindStockPlanOrderDetailsDelegate(delegate: StockPlanOrderDetailsDelegate): OrderDetailsViewModel.OrderDetailsDelegate

    @Suppress("LongMethod", "TooManyFunctions")
    companion object {

        @Provides
        @Singleton
        fun provideDefaultOrderDetailsRepo(
            apiClient: OrderDetailsApiClient,
            orderDetailDescriptionMapperType: OrderDetailDescriptionMapperType
        ): OrderDetailsRepo =
            DefaultOrderDetailsRepo(apiClient, OrderDetailsMapper(orderDetailDescriptionMapperType))

        @Provides
        fun provideMutualOrderSectionViewFactory(orderDetailLabelResources: OrderDetailLabelResources): MutualFundOrderDetailSectionViewFactory {
            return MutualOrderSectionViewFactory(orderDetailLabelResources)
        }

        @Provides
        @Singleton
        fun provideOrdersApiClient(@MobileTrade retrofit: Retrofit): OrdersListApiClient = retrofit.create(OrdersListApiClient::class.java)

        @Provides
        @Singleton
        fun provideOrdersDetailsApiClient(@MobileTrade retrofit: Retrofit): OrderDetailsApiClient = retrofit.create(OrderDetailsApiClient::class.java)

        @Provides
        fun provideOrderDetailDescriptionMapper(): OrderDetailDescriptionMapperType {
            return OrderDetailDescriptionMapper()::map
        }

        @Provides
        fun provideOrderDetailPageItemMapper(
            orderDetailSectionMapper: OrderDetailSectionMapper
        ): OrderDetailItemInterfaceMapperType {
            return OrderDetailPageItemMapper(orderDetailSectionMapper)::map
        }

        @Provides
        @Singleton
        fun provideOrderDetailSectionMapper(
            resources: OrderDetailLabelResources
        ): OrderDetailSectionMapper =
            OrderDetailSectionMapper(resources)

        @Provides
        fun provideOrderDetailResources(
            labels: OrderLabels,
            defaultResources: OrderResources
        ): OrderDetailLabelResources =
            OrderDetailLabelResources(labels, defaultResources)

        @Provides
        @Singleton
        fun provideOrderHelper(
            accountTypeHandler: AccountTypeHandler,
            accountIndexHandler: AccountIndexHandler
        ): OrdersHelper =
            OrdersHelper(accountTypeHandler, accountIndexHandler)

        @Provides
        fun provideOrderLabels(resources: Resources): OrderLabels = OrderLabels(
            allOrNone = resources.getString(R.string.all_or_none),
            orderNumber = resources.getString(R.string.order_number),
            orderPlaced = resources.getString(R.string.order_placed),
            orderStatus = resources.getString(R.string.order_status),
            executed = resources.getString(R.string.executed_date),
            orderType = resources.getString(R.string.order_type),
            quantity = resources.getString(R.string.quantity),
            estimatedCommissionFees = resources.getString(R.string.estimated_commission_fees),
            term = resources.getString(R.string.term),
            priceType = resources.getString(R.string.price_type),
            limitPrice = resources.getString(R.string.limit_price),
            stopPrice = resources.getString(R.string.stop_price),
            priceExecuted = resources.getString(R.string.price_executed),
            commissionFees = resources.getString(R.string.commission_fees),
            cancelRequested = resources.getString(R.string.cancel_requested),
            expired = resources.getString(R.string.expired),
            systemRejected = resources.getString(R.string.system_rejected),
            cancelledDate = resources.getString(R.string.cancelled),
            orderSavedDate = resources.getString(R.string.order_saved_date),
            account = resources.getString(R.string.title_account),
            strategy = resources.getString(R.string.strategy),
            estimatedCommission = resources.getString(R.string.estimated_commission),
            estimatedTotalCost = resources.getString(R.string.estimated_total_cost),
            estimatedTotalProceeds = resources.getString(R.string.estimated_total_proceeds),
            quantityExecutedEntered = resources.getString(R.string.quantity_executed_entered),
            orderTermGTD = resources.getString(R.string.order_term_gtd),
            orderTermWeb = resources.getString(R.string.term_ehday_web),
            orderTermNight = resources.getString(R.string.term_ehday_night),
            marketDestination = resources.getString(R.string.order_destination),
            orderExecuted = resources.getString(R.string.order_executed),
            qualifier = resources.getString(R.string.qualifier),
            condition = resources.getString(R.string.condition),
            advancedValue = resources.getString(R.string.value),
            stopValDollar = resources.getString(R.string.stop_val_dollar),
            stopValPercent = resources.getString(R.string.stop_val_percent),
            price = resources.getString(R.string.price),
            limitOffsetValue = resources.getString(R.string.advanced_limit_offset_value),
            description = resources.getString(R.string.description),
            exerciseValue = resources.getString(R.string.excercise_value),
            sharesExercised = resources.getString(R.string.shares_excercised),
            sharesSold = resources.getString(R.string.shares_sold),
            sharesToSell = resources.getString(R.string.shares_to_sell),
            estGrossProceeds = resources.getString(R.string.est_gross_proceeds),
            totalPrice = resources.getString(R.string.total_price),
            secFee = resources.getString(R.string.sec_fee),
            brokerAssistFee = resources.getString(R.string.broker_assist_fee),
            disbursementFee = resources.getString(R.string.disbursement_fee),
            estTaxesWithheld = resources.getString(R.string.est_tax_withheld),
            estSharesWithheldValue = resources.getString(R.string.est_shares_withheld_value),
            estNetProceeds = resources.getString(R.string.est_net_proceeds),
            sharesToExcercise = resources.getString(R.string.shares_to_excercise),
            sharesToIssue = resources.getString(R.string.shares_to_issue),
            sharesWithheldValue = resources.getString(R.string.shares_withheld_value)
        )

        @Provides
        @Singleton
        fun provideOrderRefreshDelegate(ordersHelper: OrdersHelper): OrderRefreshDelegate {
            return object : OrderRefreshDelegate {
                override fun refreshOrderPages(
                    orderSharedViewModel: OrderSharedViewModel,
                    navController: NavController,
                    activity: Activity,
                    accountId: String,
                    orderId: String,
                    snackBarMessage: String?,
                    tradeOrderType: TradeOrderType
                ) {
                    // when user saved a new order from trade page in the main navigation
                    if (tradeOrderType == TradeOrderType.NEW) {
                        if (navController.graph.id != R.id.quoteGraph) {
                            orderSharedViewModel.requireRefreshOrderList(snackBarMessage)
                        } else {
                            // when it is new order from trade page inside a bottom quote dialog
                            // there is no need a refresh request
                            // because we stay on the bottom quote dialog
                            // and it manages to show the snackbar message itself
                        }
                    }
                    // when user edit saved/open order from orders tab under account
                    else {
                        ordersHelper.buildOrderDetailArgument(accountId, orderId, snackBarMessage)?.let {
                            when (navController.graph.id) {
                                // when user edit order from orders bottom navigation dialog
                                R.id.orders_navigation_graph -> {
                                    orderSharedViewModel.refreshOrderDetailWith(it)
                                    navController.popBackStack()
                                }
                                // when user edit order from orders tab
                                else -> {
                                    orderSharedViewModel.refreshOrderDetailWith(it)
                                    orderSharedViewModel.requireRefreshOrderList(snackBarMessage)
                                    activity.onBackPressed()
                                }
                            }
                        }
                    }
                }
            }
        }

        @Provides
        fun provideOrderResources(resources: Resources, context: Context): OrderResources = OrderResources(
            Currency.getInstance(Locale.US).symbol,
            resources.getString(R.string.empty_default),
            "%",
            // Margin Calls are always a negative value, hence RED
            ContextCompat.getColor(context, R.color.red),
            resources
        )
    }
}
