package com.etrade.mobilepro.editlist.intent

import android.content.Intent

/**
 * Base implementation of intent builder. At any time the [intent] can be used to start the activity.
 *
 * @param intent an intent, prepared with provided parameters
 */
@ListEditDslMarker
open class BaseIntentBuilder(val intent: Intent) {

    /**
     * Provides text resource ID for an empty list state's message.
     */
    var emptyListMessageResId: Int
        get() = intent.emptyListMessageResId
        set(value) {
            intent.emptyListMessageResId = value
        }

    /**
     * `True` if items are non-deletable. `False` otherwise.
     */
    var isDeletableItems: Boolean
        get() = intent.isDeletableItems
        set(value) {
            intent.isDeletableItems = value
        }

    /**
     * `True` if items are toggleable. `False` otherwise.
     */
    var isToggleableItems: Boolean
        get() = intent.isToggleableItems
        set(value) {
            intent.isToggleableItems = value
        }

    var listFooterMessage: Int
        get() = intent.listFooterMessage
        set(value) {
            intent.listFooterMessage = value
        }
}
