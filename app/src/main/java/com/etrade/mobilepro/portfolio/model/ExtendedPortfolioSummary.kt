package com.etrade.mobilepro.portfolio.model

import androidx.annotation.DrawableRes
import com.etrade.mobilepro.portfolio.model.dto.CallToActionParcelable
import com.etrade.mobilepro.portfolio.model.dto.ParcelablePortfolioSummaryItemDto

interface ExtendedPortfolioSummary : ExtendedPortfolioAccountInfo {
    var extraSummarySections: List<ParcelablePortfolioSummaryItemDto>
    val callToActions: List<CallToActionParcelable>

    /**
     * Gets icon resource for an item in [callToActions] at specified [index].
     *
     * @param index index of call to action
     *
     * @return drawable resource ID
     */
    @DrawableRes
    fun getIconResource(index: Int): Int

    /**
     * Called when a call to action is clicked at specified [index].
     *
     * @param index index of call to action
     */
    fun onCtaClicked(index: Int)
}

interface ExtendedPortfolioAccountInfo {
    val accountTitle: String?
    val accountNumber: String?
}
