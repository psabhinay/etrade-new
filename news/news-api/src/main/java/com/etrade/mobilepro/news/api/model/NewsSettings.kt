package com.etrade.mobilepro.news.api.model

class NewsSettings(
    val pageSize: Int = 20,
    val initialLoadCount: Int = 20,
    val prefetchDistance: Int = 5
)
