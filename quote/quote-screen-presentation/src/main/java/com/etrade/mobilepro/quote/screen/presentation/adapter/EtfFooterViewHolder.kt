package com.etrade.mobilepro.quote.screen.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemEtfFooterBinding
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.util.android.accessibility.bindButtonAccessibility

class EtfFooterViewHolder(private val binding: QuoteItemEtfFooterBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bindTo(
        item: QuoteItem.QuoteEtfLegalFooterItem?,
        prospectusClickListener: (() -> Unit)? = null,
        performanceClickListener: (() -> Unit)? = null
    ) {
        item?.let {
            binding.prospectusTitle.bindButtonAccessibility()
            binding.prospectusTitle.setOnClickListener {
                prospectusClickListener?.invoke()
            }
            binding.performanceTitle.bindButtonAccessibility()
            binding.performanceTitle.setOnClickListener {
                performanceClickListener?.invoke()
            }
        }
    }
}
