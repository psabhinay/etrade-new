package com.etrade.mobilepro.util.android.coroutine

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class LifecycleJobScope @Inject constructor(
    dispatchers: DispatcherProvider
) : LifecycleObserver, CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext = job + dispatchers.Main

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun cancelAllJobs() {
        job.cancel()
    }
}
