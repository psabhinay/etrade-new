package com.etrade.mobilepro.dynamic.form.api

import androidx.annotation.StyleRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class StaticText(
    val title: String?,
    @StyleRes val textStyleId: Int? = 0,
    override val isVisible: LiveData<Boolean> = MutableLiveData(true)
) : Component
