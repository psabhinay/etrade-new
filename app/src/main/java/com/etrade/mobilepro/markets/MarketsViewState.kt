package com.etrade.mobilepro.markets

import androidx.annotation.LayoutRes
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.indices.IndexDataWithColor
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.util.android.SingleLiveEvent

sealed class ViewState {
    object Loading : ViewState()
    data class Error(val message: String? = "Something went wrong!") : ViewState()
    data class Success(val views: List<DynamicViewModel>, val timeStamp: String?) : ViewState()
}

sealed class MarketsGenericLayout(
    @LayoutRes override val layoutId: Int
) : DynamicViewModel(layoutId) {
    data class Header(val showHeader: Boolean) : MarketsGenericLayout(R.layout.market_overview_list_header) {
        override val variableId: Int = BR.obj
    }

    class Index(val indices: List<IndexDataWithColor>) :
        MarketsGenericLayout(R.layout.markets_indices_table) {
        override val variableId: Int = BR.obj
        override val clickEvents = SingleLiveEvent<CtaEvent>()
        override fun onCtaClicked(appUrl: String?) {
            appUrl?.let {
                clickEvents.postValue(CtaEvent.LaunchQuote(SearchResultItem.Symbol(title = it, type = InstrumentType.INDX).toBundle()))
            }
        }
    }

    data class Quote(val overviewItem: OverviewItem) : MarketsGenericLayout(R.layout.market_overview_item) {
        override val variableId: Int = BR.obj
        override val clickEvents = SingleLiveEvent<CtaEvent>()
        override fun onCtaClicked(appUrl: String?) {
            appUrl?.let { ticker ->
                clickEvents.postValue(
                    CtaEvent.LaunchQuote(
                        SearchResultItem.Symbol(
                            title = ticker,
                            type = InstrumentType.EQ,
                            displaySymbol = ticker
                        ).toBundle()
                    )
                )
            }
        }
    }
}
