package com.etrade.mobilepro.barcode.fragment

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.StringRes
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.barcode.BARCODE_LOOKUP_FAILED
import com.etrade.mobilepro.barcode.BarcodeReaderActivity
import com.etrade.mobilepro.barcode.CAMERA_PERMISSION_MISSING_RESULT_CODE
import com.etrade.mobilepro.barcode.EXTRA_QUOTE_SYMBOL
import com.etrade.mobilepro.barcode.R
import com.etrade.mobilepro.barcode.databinding.FragmentBarcodeScanBinding
import com.etrade.mobilepro.barcode.viewmodel.BarcodeSharedViewModel
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.afterStarted
import com.etrade.mobilepro.util.android.extension.openApplicationSettings
import com.etrade.mobilepro.util.android.permissions.PermissionDeniedResponse
import com.etrade.mobilepro.util.android.permissions.PermissionGrantedResponse
import com.etrade.mobilepro.util.android.permissions.PermissionsChecker
import com.etrade.mobilepro.util.android.permissions.SinglePermissionCheckerListener
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

private const val REQUEST_CODE_SHOW_APP_SETTINGS = 2
private const val REQUEST_CODE_CAMERA_PERMISSION = 3

class BarcodeScanFragment @Inject constructor(
    snackbarUtilFactory: SnackbarUtilFactory,
    private val viewModelFactory: ViewModelProvider.Factory
) : BarcodeTermsAndConditionsFragment(R.layout.fragment_barcode_scan, snackbarUtilFactory) {

    private val binding by viewBinding(FragmentBarcodeScanBinding::bind)
    private val barcodeSharedViewModel: BarcodeSharedViewModel by activityViewModels { viewModelFactory }

    private val permissionsChecker: PermissionsChecker = buildPermissionsChecker()

    private val barcodeReaderRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            when (result.resultCode) {
                Activity.RESULT_OK -> handleBarcodeRequestResultOk(result.data)
                BARCODE_LOOKUP_FAILED -> handleBarcodeRequestResultFailed()
                CAMERA_PERMISSION_MISSING_RESULT_CODE -> permissionsChecker.check()
            }
        }

    override fun getBarcodeTermsAndConditions() =
        binding.barcodeTermsAndConditionsView.barcodeTermsAndConditions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.barcodeScanLogo.setOnClickListener {
            permissionsChecker.check()
        }
    }

    private fun buildPermissionsChecker(): PermissionsChecker = PermissionsChecker
        .with(this)
        .withPermission(Manifest.permission.CAMERA)
        .withListener(object : SinglePermissionCheckerListener {
            override fun onPermissionGranted(response: PermissionGrantedResponse) {
                barcodeReaderRequest.launch(Intent(activity, BarcodeReaderActivity::class.java))
            }

            override fun onPermissionDenied(response: PermissionDeniedResponse) {
                if (response.permanentlyDenied) {
                    showCameraPermissionRationaleDialog(R.string.settings, REQUEST_CODE_SHOW_APP_SETTINGS)
                }
            }

            override fun onPermissionRationaleShouldBeShown(permission: String) {
                showCameraPermissionRationaleDialog(R.string.ok, REQUEST_CODE_CAMERA_PERMISSION)
            }
        })

    private fun showCameraPermissionRationaleDialog(@StringRes actionLabel: Int, requestCode: Int) {
        val fragment = CustomDialogFragment
            .newInstance(
                message = getString(R.string.camera_permission_rationale_message),
                okTitle = getString(actionLabel),
                cancelTitle = getString(R.string.cancel),
                requestCode = requestCode
            )
        showDialog(fragment)
    }

    private fun handleBarcodeRequestResultOk(data: Intent?) {
        data?.getStringExtra(EXTRA_QUOTE_SYMBOL).also { symbol ->
            symbol?.also {
                barcodeSharedViewModel.quoteSymbolLookupFound(it)
            } ?: run {
                afterStarted {
                    val fragment = CustomDialogFragment
                        .newInstance(
                            message = getString(R.string.barcode_scanning_no_result),
                            okTitle = getString(android.R.string.ok)
                        )

                    showDialog(fragment)
                }
            }
        }
    }

    private fun handleBarcodeRequestResultFailed() {
        snackbarUtil.snackbar(getString(R.string.error_message_general), Snackbar.LENGTH_SHORT)?.show()
    }

    private fun showDialog(fragment: CustomDialogFragment) {
        parentFragmentManager.also { fragment.show(it, "barcode_alert_dialog_fragment") }
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                REQUEST_CODE_SHOW_APP_SETTINGS -> context?.openApplicationSettings()
                REQUEST_CODE_CAMERA_PERMISSION -> permissionsChecker.check()
            }
        }
    }
}
