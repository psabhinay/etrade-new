package com.etrade.mobilepro.orders.presentation

import com.etrade.eo.core.util.isZero
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.ExecInstruction
import com.etrade.mobilepro.orders.api.MarketSession
import com.etrade.mobilepro.orders.api.OrderIdentifier
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.USER_SELECTED_DNR_FLAG
import com.etrade.mobilepro.orders.extensions.extractOrderIdentifier
import java.math.BigDecimal

internal const val NEW_BUTTON_VISIBILITY_FLAG = 1
internal const val EDIT_BUTTON_VISIBILITY_FLAG = 2
internal const val CANCEL_BUTTON_VISIBILITY_FLAG = 4
internal const val DELETE_BUTTON_VISIBILITY_FLAG = 8
internal const val PREVIEW_BUTTON_VISIBILITY_FLAG = 16

data class DefaultOrderButtonVisibilityManager(
    private val orderType: OrderType,
    private val orderStatus: OrderStatus,
    private val advancedOrderType: AdvancedOrderType,
    private val priceType: PriceType,
    private val orderTerm: OrderTerm,
    private val origSysCode: OrigSysCode,
    private val hasDependentFlag: Boolean,
    private val dependentOrderNo: Int,
    private val isMultiLeg: Boolean,
    private val qtyReserveShow: BigDecimal,
    private val hiddenFlag: Boolean,
    private val goodThroughDateFlag: Boolean,
    private val instrumentType: InstrumentType,
    private val marketSession: MarketSession,
    private val stopPrice: BigDecimal,
    private val execInstruction: ExecInstruction,
    private val orderSpecialAttribCd: String
) : OrderButtonVisibilityManager {

    val orderIdentifier = extractOrderIdentifier(orderType, advancedOrderType, origSysCode, instrumentType, isMultiLeg)

    private val hasDependent = hasDependentFlag || dependentOrderNo > 0

    private val isAdvancedOrderTrailStop = advancedOrderType == AdvancedOrderType.AO_TRAIL_STOP
    private val isSavedStockOrderEditablePreviewable = orderIdentifier == OrderIdentifier.STOCK &&
        OrderStatus.SAVED == this.orderStatus && isSavedStockOrderSupported()

    private val validOptionsOrderTypes = listOf(
        OrderType.EQUITY, OrderType.ADVANCE_EQUITY, OrderType.SIMPLE_OPTION,
        OrderType.SPREADS, OrderType.BUY_WRITES,
        OrderType.ADVANCE_OPTION,
        OrderType.BUTTERFLY, OrderType.CONDOR, OrderType.IRON_CONDOR, OrderType.IRON_BUTTERFLY, OrderType.COLLARS, OrderType.BUY_WRITES, OrderType.CUSTOM
    )

    private val validStockOrderTypes = listOf(
        OrderType.EQUITY, OrderType.ADVANCE_EQUITY, OrderType.SIMPLE_OPTION,
        OrderType.SPREADS, OrderType.BUY_WRITES, OrderType.MUTUAL_FUND, OrderType.MONEY_MARKET_FUND, OrderType.ADVANCE_OPTION
    )

    private val validOptionsOrderStatus = listOf(
        OrderStatus.OPEN, OrderStatus.CANCEL_REQUESTED, OrderStatus.CANCEL_REJECTED, OrderStatus.REJECT_REQUESTED, OrderStatus.REQUEST_SENT_TO_MARKET
    )

    private var buttonsVisibilityResult: Int = 0

    override val isCancellable = _isCancelable()

    override val isEditable = _isEditable()

    override val isCompleted = !isCancellable && _isCompleted()

    override val isPreviewable = _isPreviewable()

    override val isDeletable = _isDeletable()

    override val isCancelOnly = buttonsVisibilityResult == CANCEL_BUTTON_VISIBILITY_FLAG

    private fun _isCompleted(): Boolean {
        val result = when (orderStatus) {
            OrderStatus.SAVED -> false
            else -> when (orderIdentifier) {
                OrderIdentifier.STOCK -> OrderStatus.EXECUTED == this.orderStatus || !isStockViewOnly()
                OrderIdentifier.OPTION,
                OrderIdentifier.CONDITIONAL, OrderIdentifier.CONTINGENT, OrderIdentifier.BRACKETED -> true
            }
        }
        if (result) {
            buttonsVisibilityResult = buttonsVisibilityResult or NEW_BUTTON_VISIBILITY_FLAG
        }
        return result
    }

    private fun _isCancelable(): Boolean {
        val result = when (orderStatus) {
            OrderStatus.SAVED -> false
            else -> when (orderIdentifier) {
                OrderIdentifier.STOCK -> isOpenAndValidType
                OrderIdentifier.OPTION -> isValidTypeAndStatus
                OrderIdentifier.CONDITIONAL, OrderIdentifier.CONTINGENT, OrderIdentifier.BRACKETED -> this.orderStatus == OrderStatus.OPEN
            }
        }
        if (result) {
            buttonsVisibilityResult = buttonsVisibilityResult or CANCEL_BUTTON_VISIBILITY_FLAG
        }
        return result
    }

    private val isOpenAndValidType
        get() = OrderStatus.OPEN == orderStatus && validStockOrderTypes.contains(orderType)

    private val isValidTypeAndStatus
        get() = validOptionsOrderTypes.contains(orderType) && validOptionsOrderStatus.contains(orderStatus)

    private fun _isEditable(): Boolean {
        val result = when (orderStatus) {
            OrderStatus.SAVED -> {
                when (orderIdentifier) {
                    OrderIdentifier.STOCK -> isSavedStockOrderEditablePreviewable
                    OrderIdentifier.OPTION -> !isOptionsViewOnly()
                    OrderIdentifier.CONDITIONAL, OrderIdentifier.CONTINGENT, OrderIdentifier.BRACKETED -> false
                }
            }
            else -> when (orderIdentifier) {
                OrderIdentifier.STOCK ->
                    (isOpenAndValidType && !orderType.isMutualFund()) &&
                        !isStockViewOnly() &&
                        priceType != PriceType.MARKET_ON_CLOSE &&
                        !(origSysCode == OrigSysCode.ORIG_SYS_CODE_DETW && marketSession == MarketSession.MARKET_OVERNIGHT) &&
                        !isStockViewOnlyCommon()
                OrderIdentifier.OPTION ->
                    (isValidTypeAndStatus && !orderType.isMutualFund()) &&
                        !goodThroughDateFlag && priceType != PriceType.MARKET_ON_CLOSE && !isOptionsViewOnly() &&
                        origSysCode != OrigSysCode.ORIG_SYS_CODE_BTDT
                OrderIdentifier.CONDITIONAL, OrderIdentifier.CONTINGENT, OrderIdentifier.BRACKETED -> false
            }
        }

        if (result) {
            buttonsVisibilityResult = buttonsVisibilityResult or EDIT_BUTTON_VISIBILITY_FLAG
        }
        return result
    }

    private fun _isPreviewable(): Boolean {
        var result = false
        if (this.orderStatus == OrderStatus.SAVED) {
            result = when (orderIdentifier) {
                OrderIdentifier.STOCK -> isSavedStockOrderEditablePreviewable
                OrderIdentifier.OPTION -> !isOptionsViewOnly()
                OrderIdentifier.CONDITIONAL -> !isSavedConditionalOrderViewOnly()
                OrderIdentifier.CONTINGENT -> true
                OrderIdentifier.BRACKETED -> false
            }
        }
        if (result) {
            buttonsVisibilityResult = buttonsVisibilityResult or PREVIEW_BUTTON_VISIBILITY_FLAG
        }
        return result
    }

    private fun _isDeletable(): Boolean {
        if (isPreviewable) {
            buttonsVisibilityResult = buttonsVisibilityResult or DELETE_BUTTON_VISIBILITY_FLAG
        }
        return isPreviewable
    }

    private fun isSavedConditionalOrderViewOnly(): Boolean {
        return orderType == OrderType.CONDITIONAL_ONE_CANCEL_OTHER || orderType == OrderType.CONDITIONAL_OTOCO
    }

    private fun isSavedStockOrderSupported(): Boolean {
        val editableTerms = listOf(
            OrderTerm.GOOD_FOR_DAY, OrderTerm.GOOD_UNTIL_CANCEL,
            OrderTerm.IMMEDIATE_OR_CANCEL, OrderTerm.FILL_OR_KILL, OrderTerm.EXTENDED_HOUR_DAY
        )
        val editableDayGtcTypes = listOf(PriceType.STOP, PriceType.STOP_LIMIT, PriceType.TRAILING_STOP_DOLLAR, PriceType.TRAILING_STOP_PERCENT)
        var result = (priceType == PriceType.MARKET && orderTerm == OrderTerm.GOOD_FOR_DAY && !execInstruction.allOrNoneFlag) ||
            (priceType == PriceType.LIMIT && editableTerms.contains(orderTerm)) ||
            (editableDayGtcTypes.contains(priceType) && (orderTerm == OrderTerm.GOOD_FOR_DAY || orderTerm == OrderTerm.GOOD_UNTIL_CANCEL))

        if (!qtyReserveShow.isZero()) {
            result = false
        }

        if (origSysCode == OrigSysCode.ORIG_SYS_CODE_OH) {
            if (advancedOrderType == AdvancedOrderType.AO_TRAIL_STOP_LIMIT ||
                hiddenFlag ||
                qtyReserveShow > BigDecimal.ZERO ||
                execInstruction.doNotIncreaseFlag ||
                goodThroughDateFlag ||
                (orderSpecialAttribCd != USER_SELECTED_DNR_FLAG && execInstruction.doNotReduceFlag)
            ) {
                result = false
            }
        }
        return result
    }

    private fun isStockViewOnly(): Boolean {
// Check for fill triggers
        if (hasDependent) {
            return false
        }

        return when (priceType) {
            PriceType.MARKET -> !(orderTerm == OrderTerm.GOOD_FOR_DAY && !execInstruction.allOrNoneFlag)
            PriceType.LIMIT -> !(
                orderTerm == OrderTerm.GOOD_FOR_DAY ||
                    orderTerm == OrderTerm.GOOD_UNTIL_CANCEL ||
                    orderTerm == OrderTerm.IMMEDIATE_OR_CANCEL ||
                    orderTerm == OrderTerm.FILL_OR_KILL ||
                    orderTerm == OrderTerm.EXTENDED_HOUR_DAY
                )
            PriceType.STOP -> !(orderTerm == OrderTerm.GOOD_FOR_DAY || orderTerm == OrderTerm.GOOD_UNTIL_CANCEL)
            PriceType.STOP_LIMIT -> !(orderTerm == OrderTerm.GOOD_FOR_DAY || orderTerm == OrderTerm.GOOD_UNTIL_CANCEL)
            PriceType.TRAILING_STOP_PERCENT, PriceType.TRAILING_STOP_DOLLAR ->
                !(!execInstruction.allOrNoneFlag && (orderTerm == OrderTerm.GOOD_FOR_DAY || orderTerm == OrderTerm.GOOD_UNTIL_CANCEL))
            else -> true
        }
    }

    private fun isStockViewOnlyCommon(): Boolean {
        if (orderStatus != OrderStatus.EXECUTED && (priceType == PriceType.HIDDEN_STOP || origSysCode == OrigSysCode.ORIG_SYS_CODE_BTDT)) {
            return true
        } else if (origSysCode == OrigSysCode.ORIG_SYS_CODE_OH && (execInstruction.doNotIncreaseFlag || execInstruction.doNotReduceFlag)) {
            return true
        }
        return false
    }

    private fun isOptionsViewOnly(): Boolean {
        var isViewOnly = false

        if (origSysCode in arrayOf(OrigSysCode.ORIG_SYS_CODE_OH, OrigSysCode.ORIG_SYS_CODE_DETW)) {
            isViewOnly = isOptionViewOnlyCommon()
        }

        return isViewOnly
    }

    @Suppress("ComplexCondition", "ComplexMethod", "LongMethod")
    private fun isOptionViewOnlyCommon(): Boolean {
        if (hasDependent) {
            return true
        }
        val isLimitPriceType = PriceType.isLimitPrice(priceType.typeCode)

        val isMarketPriceType = PriceType.isMarketPrice(priceType.typeCode)
        val isStopPriceType = PriceType.isStopPrice(priceType.typeCode) && stopPrice > BigDecimal.ZERO

        if (isMultiLeg) {
            if ((isMarketPriceType && (orderTerm == OrderTerm.GOOD_UNTIL_CANCEL || goodThroughDateFlag)) ||
                (isLimitPriceType && goodThroughDateFlag) ||
                isStopPriceType
            ) {
                return true
            }
        } else {
            // limit orders
            if (isLimitPriceType && (goodThroughDateFlag || priceType == PriceType.LIMIT_ON_CLOSE)) {
                return true
            } else if (PriceType.isMarketPrice(priceType.typeCode) &&
                (
                    (!isAdvancedOrderTrailStop && orderTerm == OrderTerm.GOOD_UNTIL_CANCEL) ||
                        (execInstruction.allOrNoneFlag && orderTerm == OrderTerm.GOOD_FOR_DAY) || goodThroughDateFlag ||
                        priceType == PriceType.MARKET_ON_CLOSE
                    )
            ) {
                return true
            } else {
                when (priceType) {
                    PriceType.STOP, PriceType.TRAILING_STOP_DOLLAR, PriceType.STOP_LIMIT -> if (goodThroughDateFlag) {
                        return true
                    }
                    PriceType.HIDDEN_STOP -> return true
                    else -> {
                    }
                }
            }
            if (advancedOrderType == AdvancedOrderType.AO_TRAIL_STOP_LIMIT) {
                return true
            }

            if (hiddenFlag) {
                return true
            }

            if (qtyReserveShow > BigDecimal.ZERO) {
                return true
            }
        }
        return false
    }
}
