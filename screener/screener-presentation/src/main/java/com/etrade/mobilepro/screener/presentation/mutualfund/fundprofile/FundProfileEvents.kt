package com.etrade.mobilepro.screener.presentation.mutualfund.fundprofile

import com.etrade.mobilepro.screener.api.filter.mutualfund.FundProfileFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class FundCharacteristic : ScreenerFilterEvent<FundProfileFilter, FundProfileViewModel.ViewState, Boolean> {
    INDEX {
        override fun update(viewState: FundProfileViewModel.ViewState, eventValue: Boolean): FundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(index = eventValue))
    },
    FUND_OF_FUNDS {
        override fun update(viewState: FundProfileViewModel.ViewState, eventValue: Boolean): FundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(fundOfFunds = eventValue))
    },
    SOCIETY_RESPONSIBLE {
        override fun update(viewState: FundProfileViewModel.ViewState, eventValue: Boolean): FundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(societyResponsible = eventValue))
    },
    LIFE_OF_CYCLE {
        override fun update(viewState: FundProfileViewModel.ViewState, eventValue: Boolean): FundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(lifeOfCycleFund = eventValue))
    },
    DIVIDEND_STRATEGY {
        override fun update(viewState: FundProfileViewModel.ViewState, eventValue: Boolean): FundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(dividendStrategy = eventValue))
    },
    ENHANCED_INDEX {
        override fun update(viewState: FundProfileViewModel.ViewState, eventValue: Boolean): FundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(enhancedIndex = eventValue))
    },
    DIVERSIFIED {
        override fun update(viewState: FundProfileViewModel.ViewState, eventValue: Boolean): FundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(diversified = eventValue))
    },
    ACTIVELY_MANAGED {
        override fun update(viewState: FundProfileViewModel.ViewState, eventValue: Boolean): FundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(activelyManaged = eventValue))
    }
}

internal enum class FundSwitch : ScreenerFilterEvent<FundProfileFilter, FundProfileViewModel.ViewState, Boolean> {
    ALL_STAR {
        override fun update(viewState: FundProfileViewModel.ViewState, eventValue: Boolean): FundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(allStarsFund = eventValue))
    },
    EXCLUDE_CLOSED {
        override fun update(viewState: FundProfileViewModel.ViewState, eventValue: Boolean): FundProfileViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(excludeClosedFunds = eventValue))
    }
}
