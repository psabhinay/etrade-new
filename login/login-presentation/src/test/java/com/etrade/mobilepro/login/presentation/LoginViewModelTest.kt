package com.etrade.mobilepro.login.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.login.api.repo.LoginSuccess
import com.etrade.mobilepro.login.presentation.cta.LoginViewProvider
import com.etrade.mobilepro.login.usecase.LogInUseCase
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
internal class LoginViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val layoutId: Int = R.layout.item_overview_login

    private val mockLogInUseCase: LogInUseCase = mock()
    private val mockLoginViewProvider: LoginViewProvider = mock()
    private val mockLoginPreferences: LoginPreferencesRepo = mock()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    private lateinit var sut: LoginViewModel

    @ExperimentalCoroutinesApi
    @Before
    fun setup() {
        Dispatchers.setMain(testCoroutineDispatcher)
        MockitoAnnotations.initMocks(this)
        whenever(mockLoginViewProvider.isRememberUserNameEnabled()).thenReturn(true)
        whenever(mockLoginPreferences.isFirstLogin).thenReturn(false)
        runBlockingTest {
            whenever(mockLogInUseCase.execute(any())).thenReturn(
                LoginSuccess(TEST_USER_ID, isInternationalUser = false, isRsaUser = false, isRealTimeUser = false)
            )
        }

        sut = LoginViewModel(
            layoutId = layoutId,
            loginViewProvider = mockLoginViewProvider,
            biometricAuthenticationDelegate = mock(),
            loginPreferences = mockLoginPreferences,
            logInUseCase = mockLogInUseCase,
            timeoutState = mock(),
            preferences = mock(),
            tracker = mock(),
            biometricsProvider = mock(),
            cipherAuthenticatorViewModelDelegate = mock(),
            resources = mock()
        )
    }

    @After
    fun cleanup() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `should display loading indicator`() {
        runBlockingTest {
            val mockObserver = mock<Observer<ConsumableLiveEvent<LoginViewModel.ViewState>>>()
            val mockCipherAuthenticator = mock<BiometricCipherAuthenticator>()
            sut.loginViewState.observeForever(mockObserver)

            // WHEN
            sut.login(
                TEST_USER_NAME,
                TEST_PASSWORD,
                LoginParameter(mockCipherAuthenticator)
            )

            verify(mockLogInUseCase).execute(any())
            inOrder(mockObserver, mockObserver) {
                verify(mockObserver).onChanged(eq(ConsumableLiveEvent(LoginViewModel.ViewState.Loading)))
                verify(mockObserver).onChanged(eq(ConsumableLiveEvent(LoginViewModel.ViewState.Success(null))))
            }
        }
    }

    @Test
    fun `should not block login after wrong params`() {
        val mockObserver = mock<Observer<ConsumableLiveEvent<LoginViewModel.ViewState>>>()
        // THEN
        runBlockingTest {
            sut.loginViewState.observeForever(mockObserver)
            sut.login("", "", LoginParameter({ _, _, _ -> }))

            sut.login(
                TEST_USER_NAME,
                TEST_PASSWORD,
                LoginParameter({ _, _, _ -> })
            )
            verify(mockLogInUseCase).execute(any())

            // THEN
            inOrder(mockObserver, mockObserver) {
                verify(mockObserver).onChanged(eq(ConsumableLiveEvent(LoginViewModel.ViewState.Loading)))
                verify(mockObserver).onChanged(eq(ConsumableLiveEvent(LoginViewModel.ViewState.Success(null))))
            }
        }
    }

    @Test
    fun `returns LoginSuccess on successful login`() {
        val mockObserver = mock<Observer<ConsumableLiveEvent<LoginViewModel.ViewState>>>()
        runBlockingTest {
            sut.login(TEST_USER_NAME, TEST_PASSWORD, LoginParameter({ _, _, _ -> }))
            sut.loginViewState.observeForever(mockObserver)

            verify(mockObserver).onChanged(eq(ConsumableLiveEvent(LoginViewModel.ViewState.Success(null))))
        }
    }

    companion object {
        private const val TEST_USER_ID = "12345678"
        private const val TEST_USER_NAME = "testUserName"
        private const val TEST_PASSWORD = "TestPassword"
    }
}
