package org.chromium.customtabsclient.shared

import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.di.DeviceType
import javax.inject.Inject

/**
 * Fragment used to resolve pdf destinations, open chrome custom tabs or fall back to external browser
 */

private const val GOOGLE_DOC_URL = "http://docs.google.com/gview?embedded=true&url="
private const val PDF_SUFFIX = ".pdf"

class PdfWebviewFragment @Inject constructor(
    @DeviceType deviceType: String,
    private val webViewHelper: WebViewHelper
) : WebViewFragment(deviceType) {

    override fun setupWebView(webView: WebView) {
        super.setupWebView(webView)
        webView.webViewClient = PDFWebviewClient()
    }

    inner class PDFWebviewClient : WebViewClient() {
        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            super.onReceivedError(view, request, error)
            loading = false
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            loading = false
        }

        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            if (request?.url?.lastPathSegment?.endsWith(PDF_SUFFIX) == true) {
                openGoogleDocs(request.url.toString())
                return true
            }
            return super.shouldOverrideUrlLoading(view, request)
        }

        @Suppress("DEPRECATION")
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            if (url?.endsWith(PDF_SUFFIX) == true) {
                openGoogleDocs(url)
                return true
            }
            return super.shouldOverrideUrlLoading(view, url)
        }
    }

    private fun openGoogleDocs(url: String) {
        webViewHelper.openCustomTab(
            GOOGLE_DOC_URL + url,
            ContextCompat.getColor(requireContext(), R.color.plum),
            true
        )
        // pop this fragment when we see a pdf
        findNavController().navigateUp()
    }
}
