package com.etrade.mobilepro.dynamic.form.ui

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class DecimalPatternTest {

    // Tests do not cover negative values because there are no requirements for them yet.

    @Test(expected = IllegalArgumentException::class)
    fun `check creation of decimal pattern with invalid number of fraction digits`() {
        createDecimalPattern(-1)
    }

    @Test
    fun `check decimal pattern accepts empty string`() {
        (0..10).forEach { maxDecimalDigits ->
            assertTrue(createDecimalPattern(maxDecimalDigits).matcher("").matches())
        }
    }

    @Test
    fun `check decimal pattern valid inputs`() {
        val pattern = createDecimalPattern(3)
        listOf(
            "0",
            "0.",
            "0.123",
            "1",
            "1.",
            "1.123",
            "12",
            "12.",
            "12.123",
            "123",
            "123.",
            "123.123",
            "123456789012345678901234567890",
            "123456789012345678901234567890.",
            "123456789012345678901234567890.1",
            "123456789012345678901234567890.12",
            "123456789012345678901234567890.123"
        ).forEach { value ->
            assertTrue("value = $value", pattern.matcher(value).matches())
        }
    }

    @Test
    fun `check decimal pattern invalid inputs`() {
        val pattern = createDecimalPattern(3)
        listOf(
            "a",
            "1.2.",
            "1.2.3",
            "1.1234"
        ).forEach { value ->
            assertFalse("value = $value", pattern.matcher(value).matches())
        }
    }
}
