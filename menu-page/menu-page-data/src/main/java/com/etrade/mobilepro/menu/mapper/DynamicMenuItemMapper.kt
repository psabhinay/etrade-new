package com.etrade.mobilepro.menu.mapper

import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.menu.dto.MenuDto
import com.etrade.mobilepro.menu.dto.MenuItem
import com.etrade.mobilepro.menu.repo.DynamicMenuItem
import com.etrade.mobilepro.menu.repo.MenuItemType
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.enumValueOrDefault

fun ETResult<Resource<BaseMobileResponse>>.mapToDynamicMenuItemListResult(
    baseUrl: String
): ETResult<List<DynamicMenuItem>> {
    return this.map { response ->
        @Suppress("UNCHECKED_CAST")
        (response.data?.screen?.views as? List<MenuDto>).orEmpty().mapToDynamicMenuItems(baseUrl)
    }
}

fun List<MenuDto>.mapToDynamicMenuItems(
    baseUrl: String
): List<DynamicMenuItem> {
    val dynamicMenuItems = mutableListOf<DynamicMenuItem>()
    this.mapIndexed { index, menuDto ->
        dynamicMenuItems += menuDto.data.dynamicMenuItems.convertToDynamicMenuItems(baseUrl)
        if (index < this.size - 1) {
            // This is for divider, divider doesn't have title and ctaAction.
            dynamicMenuItems += DynamicMenuItem(menuItemType = MenuItemType.Divider)
        }
    }
    return dynamicMenuItems
}

private fun List<MenuItem>.convertToDynamicMenuItems(
    baseUrl: String
): List<DynamicMenuItem> {
    return this.map { menuItem ->
        DynamicMenuItem(
            ctaDeepLink = menuItem.ctaAction?.mapToDeepLink(baseUrl, menuItem.label),
            icon = menuItem.icon,
            label = menuItem.label,
            menuItemType = enumValueOrDefault(menuItem.menuType, MenuItemType.MenuItemView),
            subLabel = menuItem.subLabel
        )
    }
}

fun ClickActionDto.mapToDeepLink(baseUrl: String, defaultTitle: String?): String? {
    return when {
        appUrl != null -> appUrl
        webViewUrl != null -> toWebViewDeepLink(baseUrl, defaultTitle)
        webUrl != null -> toWebDeepLink()
        else -> null
    }
}
