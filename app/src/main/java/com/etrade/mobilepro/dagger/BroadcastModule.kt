package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.appwidget.widgetcontainer.DefaultAppWidgetProvider
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BroadcastModule {
    @ContributesAndroidInjector
    internal abstract fun contributeDefaultAppWidgetProvider(): DefaultAppWidgetProvider
}
