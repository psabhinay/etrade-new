package com.etrade.mobilepro.encryption.viewmodel

import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.crypto.Cipher
import javax.inject.Inject
import kotlin.coroutines.resume

class CipherAuthenticatorViewModelDelegate @Inject constructor() : CipherAuthenticatorViewModel {

    private var cipherAuthenticatorContinuation: CancellableContinuation<Cipher>? = null

    override suspend fun authenticate(cipher: Cipher, authenticate: (Cipher) -> Unit): Cipher {
        onCipherAuthenticationCancelled()

        return suspendCancellableCoroutine { continuation ->
            cipherAuthenticatorContinuation = continuation
            authenticate(cipher)
        }
    }

    override fun onCipherAuthenticated(cipher: Cipher) {
        withCipherAuthenticatorContinuation {
            resume(cipher)
        }
    }

    override fun onCipherAuthenticationCancelled() {
        withCipherAuthenticatorContinuation {
            cancel()
        }
    }

    override fun onCipherAuthenticationFailed(cause: Throwable) {
        withCipherAuthenticatorContinuation {
            cancel(cause)
        }
    }

    private fun withCipherAuthenticatorContinuation(proceed: CancellableContinuation<Cipher>.() -> Unit) {
        cipherAuthenticatorContinuation?.apply {
            cipherAuthenticatorContinuation = null
            proceed()
        }
    }
}
