package com.etrade.mobilepro.inboxmessages

enum class CampaignAssetsTheme(val theme: String?) {

    LIGHT_THEME("light_theme"),

    DARK_THEME("dark_theme");

    companion object {
        fun convert(theme: String?): CampaignAssetsTheme? {
            return when (theme?.lowercase()) {
                "light_theme" -> LIGHT_THEME
                "dark_theme" -> DARK_THEME
                else -> null
            }
        }
    }
}
