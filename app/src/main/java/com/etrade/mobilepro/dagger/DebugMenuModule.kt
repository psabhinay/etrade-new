package com.etrade.mobilepro.dagger

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.etrade.mobilepro.MenuGraphDirections
import com.etrade.mobilepro.appconfig.api.repo.datasource.BaseUrlsDataSource
import com.etrade.mobilepro.common.di.Unauthenticated
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.debugmenu.api.TripleEventSequenceDetector
import com.etrade.mobilepro.debugmenu.api.repo.DebugMenuRepo
import com.etrade.mobilepro.debugmenu.data.detector.DefaultTripleEventSequenceDetector
import com.etrade.mobilepro.debugmenu.data.repo.DefaultDebugMenuRepo
import com.etrade.mobilepro.debugmenu.presentation.DebugMenuFragment
import com.etrade.mobilepro.debugmenu.presentation.DebugMenuNavDirections
import com.etrade.mobilepro.debugmenu.presentation.DebugMenuViewModel
import com.etrade.mobilepro.util.android.timer.DefaultTimeoutTimer
import com.etrade.mobilepro.util.android.timer.TimeoutTimer
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit

@Module
abstract class DebugMenuModule {
    @Binds
    abstract fun bindTimeoutTimer(timer: DefaultTimeoutTimer): TimeoutTimer

    @Binds
    abstract fun bindTripleEventDetector(detector: DefaultTripleEventSequenceDetector): TripleEventSequenceDetector

    @Binds
    @IntoMap
    @FragmentKey(DebugMenuFragment::class)
    abstract fun bindDebugMenuFragment(fragment: DebugMenuFragment): Fragment

    @Binds
    @IntoMap
    @ViewModelKey(DebugMenuViewModel::class)
    abstract fun bindDebugMenuViewModel(viewModel: DebugMenuViewModel): ViewModel

    companion object {
        @Provides
        fun provideDebugMenuRepo(
            @Unauthenticated retrofitBuilder: Retrofit.Builder,
            baseUrlsDataSource: BaseUrlsDataSource,
        ): DebugMenuRepo =
            DefaultDebugMenuRepo(
                retrofitBuilder = retrofitBuilder,
                baseUrlDataSource = baseUrlsDataSource,
            )

        @Provides
        fun providesDebugMenuDirections(): DebugMenuNavDirections = object : DebugMenuNavDirections {
            override val featureFlags: NavDirections
                get() = MenuGraphDirections.actionFeatureFlagsFragment()
        }
    }
}
