package com.etrade.mobilepro.transactions.data

import com.etrade.mobilepro.transactions.api.TransactionsRequest

fun TransactionsRequest.getCacheId(): String {
    val request = StringBuilder()
    request.append(userId)
        .append(accountId)
        .append(institutionType)
        .append(fromDate.toEpochDay().toString())
        .append(toDate.toEpochDay().toString())
    return request.toString()
}
