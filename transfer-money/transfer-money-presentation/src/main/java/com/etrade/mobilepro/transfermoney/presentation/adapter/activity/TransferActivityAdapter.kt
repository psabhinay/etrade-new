package com.etrade.mobilepro.transfermoney.presentation.adapter.activity

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.transfermoney.api.TransferMoneyActivity
import com.etrade.mobilepro.transfermoney.presentation.databinding.TransferItemActivityBinding
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.etrade.mobilepro.util.android.extension.viewBinding

internal class TransferActivityAdapter(
    private val clickListener: (TransferMoneyActivity) -> Unit
) : RecyclerView.Adapter<TransferActivityViewHolder>() {
    var items: List<TransferMoneyActivity> = emptyList()
        set(newItems) {
            val oldItems = field
            field = newItems
            dispatchUpdates(oldItems, newItems)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransferActivityViewHolder =
        TransferActivityViewHolder(parent.viewBinding(TransferItemActivityBinding::inflate), clickListener)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: TransferActivityViewHolder, position: Int) = holder.bind(items[position])
}
