package com.etrade.mobilepro.common.compose.theme

import androidx.compose.material.Typography

private val defaultTypography = Typography()
internal val etradeTypography = Typography(
    h1 = defaultTypography.h1.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    h2 = defaultTypography.h2.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    h3 = defaultTypography.h3.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    h4 = defaultTypography.h4.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    h5 = defaultTypography.h5.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    h6 = defaultTypography.h6.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    subtitle1 = defaultTypography.subtitle1.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    subtitle2 = defaultTypography.subtitle2.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    body1 = defaultTypography.body1.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    body2 = defaultTypography.body2.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    button = defaultTypography.button.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    caption = defaultTypography.caption.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp),
    overline = defaultTypography.overline.copy(fontFamily = etradeFonts, letterSpacing = FontSizes.Size0sp)
)
