package com.etrade.mobilepro.chart.data;

import com.etrade.mobilepro.chart.api.ChartLine;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0003\u00a8\u0006\u0004"}, d2 = {"toChartLine", "Lcom/etrade/mobilepro/chart/api/ChartLine;", "Lcom/etrade/mobilepro/chart/data/AccountDataDto;", "Lcom/etrade/mobilepro/chart/data/IndexDataDto;", "chartdata"})
public final class ExtensionsKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final com.etrade.mobilepro.chart.api.ChartLine toChartLine(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.chart.data.AccountDataDto $this$toChartLine) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final com.etrade.mobilepro.chart.api.ChartLine toChartLine(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.chart.data.IndexDataDto $this$toChartLine) {
        return null;
    }
}