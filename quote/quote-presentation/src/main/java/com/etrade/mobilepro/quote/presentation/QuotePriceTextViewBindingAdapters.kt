package com.etrade.mobilepro.quote.presentation

import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.quoteapi.QuotePriceWidgetItem
import com.etrade.mobilepro.uiwidgets.quote.QuotePriceTextView

@BindingAdapter("formatBidPrice")
fun QuotePriceTextView.setQuoteBidPrice(item: QuotePriceWidgetItem?) {
    item?.let {
        val size = it.getFormattedSize()
        setDetailsViewFormattedPrice(
            item.price,
            size,
            resources.getString(R.string.quote_widget_bid_format)
        )
    }
}

@BindingAdapter("formatAskPrice")
fun QuotePriceTextView.setQuoteAskPrice(item: QuotePriceWidgetItem?) {
    item?.let {
        val size = it.getFormattedSize()
        setDetailsViewFormattedPrice(
            item.price,
            size,
            resources.getString(R.string.quote_widget_ask_format)
        )
    }
}
