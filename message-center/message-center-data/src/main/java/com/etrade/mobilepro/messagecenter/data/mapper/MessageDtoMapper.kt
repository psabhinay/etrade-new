package com.etrade.mobilepro.messagecenter.data.mapper

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.data.dto.MessageDto
import com.etrade.mobilepro.util.json.coerceToBool
import org.threeten.bp.Instant
import org.threeten.bp.format.DateTimeFormatter

private const val EMPTY_VALUE = "--"

internal fun MessageDto.toMessage(formatter: DateTimeFormatter, accountIdDescMap: Map<String, String>): Message {
    val createdAt = parseToDate(formatter, this.creationTs)
    val createAtFormatted = createdAt?.let { DateFormattingUtils.formatToShortDate(it.toEpochMilli()) } ?: EMPTY_VALUE
    val accountShortName = accountIdDescMap[this.accountId] ?: this.fmtAccountId ?: EMPTY_VALUE
    return Message(
        id = this.msgId ?: "",
        accountId = this.accountId ?: "",
        accountShortName = accountShortName,
        descId = this.descId ?: "",
        subjectId = this.subjId ?: "",
        caseTypeId = this.casetypeId ?: "",
        caseTypeDesc = this.casetypeDesc ?: EMPTY_VALUE,
        caseNo = this.caseNo ?: EMPTY_VALUE,
        isUnread = coerceToBool(this.unreadFlag, false),
        status = this.status ?: EMPTY_VALUE,
        createdAt = createdAt,
        createdAtFormatted = createAtFormatted,
        readAt = parseToDate(formatter, this.readTs)
    )
}

internal fun parseToDate(formatter: DateTimeFormatter, input: String?): Instant? =
    input?.let {
        runCatching {
            Instant.from(formatter.parse(it))
        }.getOrNull()
    }
