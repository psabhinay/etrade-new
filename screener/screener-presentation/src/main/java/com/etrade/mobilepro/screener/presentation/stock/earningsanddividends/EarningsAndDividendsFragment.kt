package com.etrade.mobilepro.screener.presentation.stock.earningsanddividends

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.stock.EarningsDividendsFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentEarningsAndDividendsDividendYieldBinding
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentEarningsAndDividendsEpsGrowthAnnualBinding
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentEarningsAndDividendsEpsGrowthQuarterlyBinding
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentEarningsAndDividendsRevenueGrowthAnnualBinding
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentEarningsAndDividendsRevenueGrowthQuarterlyBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class EarningsAndDividendsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<EarningsDividendsFilter, EarningsAndDividendsViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {

    private val epsGrowthAnnual by viewBinding(PartialFragmentEarningsAndDividendsEpsGrowthAnnualBinding::bind)
    private val revenueGrowthAnnual by viewBinding(PartialFragmentEarningsAndDividendsRevenueGrowthAnnualBinding::bind)
    private val epsGrowthQuarterly by viewBinding(PartialFragmentEarningsAndDividendsEpsGrowthQuarterlyBinding::bind)
    private val revenueGrowthQuarterly by viewBinding(PartialFragmentEarningsAndDividendsRevenueGrowthQuarterlyBinding::bind)
    private val dividendYield by viewBinding(PartialFragmentEarningsAndDividendsDividendYieldBinding::bind)
    override val layoutRes: Int = R.layout.fragment_earnings_and_dividends
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.STOCK
    override val viewModel by viewModels<EarningsAndDividendsViewModel> { viewModelFactory }
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<EarningsDividendsFilter, EarningsAndDividendsViewModel.ViewState, Boolean>>
        get() = mapOf(
            epsGrowthAnnual.epsZeroToFivePercent to EpsAnnualGrowth.ZERO_5,
            epsGrowthAnnual.epsFiveToTenPercent to EpsAnnualGrowth.FIVE_10,
            epsGrowthAnnual.epsTenToFifteenPercent to EpsAnnualGrowth.TEN_15,
            epsGrowthAnnual.epsFifteenToTwentyFivePercent to EpsAnnualGrowth.FIFTEEN_25,
            epsGrowthAnnual.epsTwentyFiveToFiftyPercent to EpsAnnualGrowth.TWENTY_FIVE_50,
            epsGrowthAnnual.epsMoreThanFifty to EpsAnnualGrowth.GREATER_THAN_50,
            epsGrowthAnnual.epsPositiveChange to EpsAnnualGrowth.POSITIVE,
            epsGrowthAnnual.epsNegativeChange to EpsAnnualGrowth.NEGATIVE,
            epsGrowthAnnual.epsCurrentFiscalYear to EpsAnnualGrowthPeriod.CURRENT_FISCAL_YEAR,
            epsGrowthAnnual.epsNextFiscalYear to EpsAnnualGrowthPeriod.NEXT_FISCAL_YEAR,
            revenueGrowthAnnual.revenueZeroToFivePercent to RevenueAnnualGrowth.ZERO_5,
            revenueGrowthAnnual.revenueFiveToTenPercent to RevenueAnnualGrowth.FIVE_10,
            revenueGrowthAnnual.revenueTenToFifteenPercent to RevenueAnnualGrowth.TEN_15,
            revenueGrowthAnnual.revenueFifteenToTwentyFivePercent to RevenueAnnualGrowth.FIFTEEN_25,
            revenueGrowthAnnual.revenueTwentyFiveToFiftyPercent to RevenueAnnualGrowth.TWENTY_FIVE_50,
            revenueGrowthAnnual.revenueMoreThanFifty to RevenueAnnualGrowth.GREATER_THAN_50,
            revenueGrowthAnnual.revenuePositiveChange to RevenueAnnualGrowth.POSITIVE,
            revenueGrowthAnnual.revenueNegativeChange to RevenueAnnualGrowth.NEGATIVE,
            revenueGrowthAnnual.revenueCurrentFiscalYear to RevenueAnnualGrowthPeriod.CURRENT_FISCAL_YEAR,
            revenueGrowthAnnual.revenueNextFiscalYear to RevenueAnnualGrowthPeriod.NEXT_FISCAL_YEAR,
            epsGrowthQuarterly.epsQuarterlyZeroToFivePercent to EpsQuarterlyGrowth.ZERO_5,
            epsGrowthQuarterly.epsQuarterlyFiveToTenPercent to EpsQuarterlyGrowth.FIVE_10,
            epsGrowthQuarterly.epsQuarterlyTenToFifteenPercent to EpsQuarterlyGrowth.TEN_15,
            epsGrowthQuarterly.epsQuarterlyFifteenToTwentyFivePercent to EpsQuarterlyGrowth.FIFTEEN_25,
            epsGrowthQuarterly.epsQuarterlyTwentyFiveToFiftyPercent to EpsQuarterlyGrowth.TWENTY_FIVE_50,
            epsGrowthQuarterly.epsQuarterlyMoreThanFifty to EpsQuarterlyGrowth.GREATER_THAN_50,
            epsGrowthQuarterly.epsQuarterlyPositiveChange to EpsQuarterlyGrowth.POSITIVE,
            epsGrowthQuarterly.epsQuarterlyNegativeChange to EpsQuarterlyGrowth.NEGATIVE,
            revenueGrowthQuarterly.revenueQuarterlyZeroToFivePercent to RevenueQuarterlyGrowth.ZERO_5,
            revenueGrowthQuarterly.revenueQuarterlyFiveToTenPercent to RevenueQuarterlyGrowth.FIVE_10,
            revenueGrowthQuarterly.revenueQuarterlyTenToFifteenPercent to RevenueQuarterlyGrowth.TEN_15,
            revenueGrowthQuarterly.revenueQuarterlyFifteenToTwentyFivePercent to RevenueQuarterlyGrowth.FIFTEEN_25,
            revenueGrowthQuarterly.revenueQuarterlyTwentyFiveToFiftyPercent to RevenueQuarterlyGrowth.TWENTY_FIVE_50,
            revenueGrowthQuarterly.revenueQuarterlyMoreThanFifty to RevenueQuarterlyGrowth.GREATER_THAN_50,
            revenueGrowthQuarterly.revenueQuarterlyPositiveChange to RevenueQuarterlyGrowth.POSITIVE,
            revenueGrowthQuarterly.revenueQuarterlyNegativeChange to RevenueQuarterlyGrowth.NEGATIVE,
            dividendYield.dividendYieldPositive to DividendYield.POSITIVE,
            dividendYield.dividendYieldZeroToTwoPercent to DividendYield.ZERO_2,
            dividendYield.dividendYieldTwoToFourPercent to DividendYield.TWO_4,
            dividendYield.dividendYieldFourToSixPercent to DividendYield.FOUR_6,
            dividendYield.dividendYieldSixToEightPercent to DividendYield.SIX_8,
            dividendYield.dividendYieldMoreThanEightPercent to DividendYield.GREATER_THAN_8,
            dividendYield.dividendYieldBelowIndustryAvg to DividendYield.BELOW_AVG,
            dividendYield.dividendYieldAboveIndustryAvg to DividendYield.ABOVE_AVG
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                updateEpsGrowthAnnualBlock(viewState = it)
                updateRevenueGrowthAnnualBlock(viewState = it)
                updateEpsGrowthQuarterlyBlock(viewState = it)
                updateRevenueGrowthQuarterlyBlock(viewState = it)
                updateDividendYieldBlock(viewState = it)
            }
        )
    }

    private fun updateEpsGrowthAnnualBlock(viewState: EarningsAndDividendsViewModel.ViewState) =
        viewState.filter.apply {
            epsGrowthAnnual.apply {
                epsZeroToFivePercent.isChecked = epsGrowthAnnual0to5
                epsFiveToTenPercent.isChecked = epsGrowthAnnual5to10
                epsTenToFifteenPercent.isChecked = epsGrowthAnnual10to15
                epsFifteenToTwentyFivePercent.isChecked = epsGrowthAnnual15to25
                epsTwentyFiveToFiftyPercent.isChecked = epsGrowthAnnual25to50
                epsMoreThanFifty.isChecked = epsGrowthAnnualGreaterThan50
                epsPositiveChange.isChecked = epsGrowthAnnualPositiveChange
                epsNegativeChange.isChecked = epsGrowthAnnualNegativeChange
                epsCurrentFiscalYear.isChecked = epsGrowthAnnualForCurrentFiscalYear
                epsNextFiscalYear.isChecked = epsGrowthAnnualForNextFiscalYear
            }
        }

    private fun updateRevenueGrowthAnnualBlock(viewState: EarningsAndDividendsViewModel.ViewState) =
        viewState.filter.apply {
            revenueGrowthAnnual.apply {
                revenueZeroToFivePercent.isChecked = revenueGrowthAnnual0to5
                revenueFiveToTenPercent.isChecked = revenueGrowthAnnual5to10
                revenueTenToFifteenPercent.isChecked = revenueGrowthAnnual10to15
                revenueFifteenToTwentyFivePercent.isChecked = revenueGrowthAnnual15to25
                revenueTwentyFiveToFiftyPercent.isChecked = revenueGrowthAnnual25to50
                revenueMoreThanFifty.isChecked = revenueGrowthAnnualGreaterThan50
                revenuePositiveChange.isChecked = revenueGrowthAnnualPositiveChange
                revenueNegativeChange.isChecked = revenueGrowthAnnualNegativeChange
                revenueCurrentFiscalYear.isChecked = revenueGrowthAnnualForCurrentFiscalYear
                revenueNextFiscalYear.isChecked = revenueGrowthAnnualForNextFiscalYear
            }
        }

    private fun updateEpsGrowthQuarterlyBlock(viewState: EarningsAndDividendsViewModel.ViewState) =
        viewState.filter.apply {
            epsGrowthQuarterly.apply {
                epsQuarterlyZeroToFivePercent.isChecked = epsGrowthQuarterly0to5
                epsQuarterlyFiveToTenPercent.isChecked = epsGrowthQuarterly5to10
                epsQuarterlyTenToFifteenPercent.isChecked = epsGrowthQuarterly10to15
                epsQuarterlyFifteenToTwentyFivePercent.isChecked = epsGrowthQuarterly15to25
                epsQuarterlyTwentyFiveToFiftyPercent.isChecked = epsGrowthQuarterly25to50
                epsQuarterlyMoreThanFifty.isChecked = epsGrowthQuarterlyGreaterThan50
                epsQuarterlyPositiveChange.isChecked = epsGrowthQuarterlyPositiveChange
                epsQuarterlyNegativeChange.isChecked = epsGrowthQuarterlyNegativeChange
            }
        }

    @Suppress("FunctionMaxLength")
    private fun updateRevenueGrowthQuarterlyBlock(viewState: EarningsAndDividendsViewModel.ViewState) =
        viewState.filter.apply {
            revenueGrowthQuarterly.apply {
                revenueQuarterlyZeroToFivePercent.isChecked = revenueGrowthQuarterly0to5
                revenueQuarterlyFiveToTenPercent.isChecked = revenueGrowthQuarterly5to10
                revenueQuarterlyTenToFifteenPercent.isChecked = revenueGrowthQuarterly10to15
                revenueQuarterlyFifteenToTwentyFivePercent.isChecked = revenueGrowthQuarterly15to25
                revenueQuarterlyTwentyFiveToFiftyPercent.isChecked = revenueGrowthQuarterly25to50
                revenueQuarterlyMoreThanFifty.isChecked = revenueGrowthQuarterlyGreaterThan50
                revenueQuarterlyPositiveChange.isChecked = revenueGrowthQuarterlyPositiveChange
                revenueQuarterlyNegativeChange.isChecked = revenueGrowthQuarterlyNegativeChange
            }
        }

    private fun updateDividendYieldBlock(viewState: EarningsAndDividendsViewModel.ViewState) =
        viewState.filter.apply {
            dividendYield.dividendYieldPositive.isChecked = dividendYieldPositive
            dividendYield.apply {
                dividendYieldZeroToTwoPercent.isChecked = dividendYield0to2
                dividendYieldTwoToFourPercent.isChecked = dividendYield2to4
                dividendYieldFourToSixPercent.isChecked = dividendYield4to6
                dividendYieldSixToEightPercent.isChecked = dividendYield6to8
                dividendYieldMoreThanEightPercent.isChecked = dividendYieldGreaterThan8
            }
            dividendYield.dividendYieldBelowIndustryAvg.isChecked = dividendYieldBelowIndustryAvg
            dividendYield.dividendYieldAboveIndustryAvg.isChecked = dividendYieldAboveIndustryAvg
        }
}
