package com.etrade.mobilepro.login.data

import com.etrade.mobilepro.login.api.repo.LoginFailure
import com.etrade.mobilepro.login.api.repo.LoginResultCode
import com.etrade.mobilepro.login.api.repo.LoginSuccess
import com.etrade.mobilepro.login.data.restclient.dto.LoginUserSSOResponse
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.testutil.XmlDeserializer
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

internal class LoginResultTest {
    private val mockUser: User = mock()

    @Test
    fun `returns LoginSuccess on successful login`() {
        runBlocking {
            val etLoginResponse = XmlDeserializer.getObjectFromXml("LoginSuccess.xml", LoginUserSSOResponse::class.java)
            val result: LoginSuccess = parseLoginResult(etLoginResponse, mockUser) as LoginSuccess
            assertEquals("17488131", result.userId)
        }
    }

    @Test
    fun `returns LoginSuccess Warning on successful login with warning`() {
        val etLoginResponse = XmlDeserializer.getObjectFromXml("LoginSuccessWarning.xml", LoginUserSSOResponse::class.java)
        val result: LoginSuccess = parseLoginResult(etLoginResponse, mockUser) as LoginSuccess
        assertEquals("17481131", result.userId)
        assertNotNull(result.sessionResultMessage)
    }

    @Test
    fun `returns LoginFailure on invalid credentials login`() {
        val etLoginResponse = XmlDeserializer.getObjectFromXml("LoginFailure_9603.xml", LoginUserSSOResponse::class.java)
        val result: LoginFailure = parseLoginResult(etLoginResponse, mockUser) as LoginFailure
        assertEquals(LoginResultCode.INVALID_CREDENTIALS, result.loginResultCode)
        assertNotNull(result.sessionResultMessage)
    }

    @Test
    fun `returns LoginFailure on wrong password login`() {
        val etLoginResponse = XmlDeserializer.getObjectFromXml("LoginFailure_9604.xml", LoginUserSSOResponse::class.java)
        val result: LoginFailure = parseLoginResult(etLoginResponse, mockUser) as LoginFailure
        assertEquals(LoginResultCode.WRONG_PASSWORD, result.loginResultCode)
        assertNotNull(result.sessionResultMessage)
    }
}
