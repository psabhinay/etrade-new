package com.etrade.mobilepro.sar.service.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.sar.service.DEFAULT_CHECK_INTERVAL_MILLIS
import com.etrade.mobilepro.sar.service.SarMessage
import com.etrade.mobilepro.sar.service.restclient.SarService
import retrofit2.Retrofit

class DefaultSarRepo constructor(retrofit: Retrofit) : SarRepo {
    private val sarService: SarService = retrofit.create(SarService::class.java)

    override suspend fun checkSar(): ETResult<SarMessage> {
        return sarService.runCatchingET { getSarStatus() }.map {
            SarMessage(
                it.nextCheckMillis ?: DEFAULT_CHECK_INTERVAL_MILLIS,
                it.terminateReason,
                terminateSession = it.terminateSession
            )
        }
    }
}
