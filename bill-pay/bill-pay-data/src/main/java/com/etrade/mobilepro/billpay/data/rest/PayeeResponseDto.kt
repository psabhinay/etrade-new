package com.etrade.mobilepro.billpay.data.rest

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PayeeResponseDto(
    @Json(name = "payeesList")
    val payeesList: List<PayeeDto>? = null
) : BaseDataDto()

@JsonClass(generateAdapter = true)
class PayeeDto(
    @Json(name = "payee")
    val payee: PayeeDetailDto,
    @Json(name = "lastPayInfo")
    val lastPayInfo: LastPayInfo
)

@JsonClass(generateAdapter = true)
class PayeeDetailDto(
    @Json(name = "payeeId")
    val payeeId: String?,
    @Json(name = "name")
    val name: String,
    @Json(name = "nickName")
    val nickName: String?,
    @Json(name = "phoneNumber")
    val phoneNumber: String?,
    @Json(name = "leadDays")
    val leadDays: Int?,
    @Json(name = "isAddressAvailable")
    val isAddressAvailable: Boolean?,
    @Json(name = "address")
    val address: PayeeAddressDto?,
    @Json(name = "eBillActivationStatus")
    val eBillActivationStatus: String?,
    @Json(name = "merchantInfo")
    val merchantInfo: MerchantInfoDto?,
    @Json(name = "merchantZipRequired")
    val merchantZipRequired: Boolean?,
    @Json(name = "accountNumber")
    val accountNumber: String?,
    @Json(name = "category")
    val category: String?,
    @Json(name = "sessionId")
    val sessionId: String? = null
)

@JsonClass(generateAdapter = true)
class PayeeAddressDto(
    @Json(name = "address1")
    val address1: String?,
    @Json(name = "address2")
    val address2: String?,
    @Json(name = "city")
    val city: String?,
    @Json(name = "state")
    val state: String?,
    @Json(name = "zipCode")
    val zipCode: String?,
    @Json(name = "zipCode2")
    val zipCode2: String? = null,
    @Json(name = "zipCode4")
    val zipCode4: String? = null
)

@JsonClass(generateAdapter = true)
class MerchantInfoDto(
    @Json(name = "merchantId")
    val merchantId: Int?,
    @Json(name = "merchantZipCode")
    val merchantZipCode: String?,
    @Json(name = "merchantZipCode4")
    val merchantZipCode4: String?
)

@JsonClass(generateAdapter = true)
class LastPayInfo(
    @Json(name = "paymentId")
    val paymentId: String?,
    @Json(name = "amount")
    val amount: String?,
    @Json(name = "paymentAccountId")
    val paymentAccountId: String?,
    @Json(name = "paymentDate")
    val paymentDate: String?
)
