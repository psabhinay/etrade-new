package com.etrade.msaccounts.dto

import com.etrade.completeview.dto.AccountSummarySectionDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MsAccountsSummaryViewDto(
    @Json(name = "data")
    override val data: MsAccountsSummaryDto,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>? = null
) : BaseScreenView()

@JsonClass(generateAdapter = true)
data class MsAccountsSummaryDto(
    @Json(name = "account_additional_labels")
    val accountAdditionalLabels: List<AccountSummarySectionDto>,
    @Json(name = "account_sections")
    val accountSections: List<MsAccountSectionDto>,
    @Json(name = "account_summary_label")
    val accountSummaryLabel: String,
    @Json(name = "account_uuids")
    val accountUuids: List<String>
)

@JsonClass(generateAdapter = true)
data class MsAccountSectionDto(
    @Json(name = "account_additional_labels")
    val accountAdditionalLabels: List<AccountSummarySectionDto>,
    @Json(name = "account_footnotes")
    val accountFootnotes: List<MsAccountFootnoteDto>,
    @Json(name = "account_label")
    val accountLabel: String,
    @Json(name = "account_uuid")
    val accountUuid: String
)

@JsonClass(generateAdapter = true)
data class MsAccountFootnoteDto(
    @Json(name = "symbol")
    val symbol: String,
    @Json(name = "text")
    val text: String
)
