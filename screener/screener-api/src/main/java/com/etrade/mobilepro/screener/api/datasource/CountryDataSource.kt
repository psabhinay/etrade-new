package com.etrade.mobilepro.screener.api.datasource

import com.etrade.mobilepro.screener.api.Country

interface CountryDataSource {
    fun getAllCountriesItem(): Country
    fun getCountries(): List<Country>
}
