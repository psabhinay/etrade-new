package com.etrade.mobilepro.bank;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000,\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0006\u001a\u00020\u0007*\b\u0012\u0004\u0012\u00020\t0\bH\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000*0\b\u0002\u0010\n\"\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\f0\u000b2\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\f0\u000b\u00a8\u0006\r"}, d2 = {"INVALID_PARAM", "", "LOCALDATE_DISPLAYED_DATE_FORMAT", "Lorg/threeten/bp/format/DateTimeFormatter;", "PREVIOUS_DAYS", "", "formatDate", "", "Landroidx/lifecycle/LiveData;", "Lorg/threeten/bp/LocalDate;", "DateRange", "Lcom/etrade/mobilepro/util/android/ConsumableLiveEvent;", "Lkotlin/Pair;", "bank-dynamic_debug"})
public final class BankHomeViewModelKt {
    private static final long PREVIOUS_DAYS = 60L;
    private static final int INVALID_PARAM = 20044;
    private static final org.threeten.bp.format.DateTimeFormatter LOCALDATE_DISPLAYED_DATE_FORMAT = null;
    
    private static final java.lang.String formatDate(androidx.lifecycle.LiveData<org.threeten.bp.LocalDate> $this$formatDate) {
        return null;
    }
}