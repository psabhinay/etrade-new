package com.etrade.mobilepro.inboxmessages

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.util.android.extension.dpToPx
import kotlinx.coroutines.launch

class InboxMessagesDynamicViewModel(
    @Web private val baseUrl: String,
    val imageLoader: ImageLoader,
    val messages: MutableLiveData<List<InboxMessage>>,
    private val repository: InboxMessagesRepository,
    context: Context
) : DynamicViewModel(R.layout.inbox_message_container_view) {

    init {
        viewModelScope.launch {
            messages.value?.forEach { repository.setMessageSend(it) }
        }
    }

    override val variableId: Int = BR.obj

    var assetHeight: Int = messages.value?.first()?.assetHeight?.let { context.dpToPx(it) } ?: context.resources.getDimensionPixelOffset(R.dimen.spacing_huge)

    var onItemClickListener: (InboxMessage) -> Unit = {
        var deepLink = it.deepLink
        clickEvents.value = if (deepLink?.isNotEmpty() == true) {
            val uri = Uri.parse(deepLink)
            if (uri.isValidGenericLinkFormat()) {
                deepLink = uri.getResolvedGenericDeepLink(baseUrl)
            }
            CtaEvent.UriEvent(Uri.parse(deepLink))
        } else if (it.message.isNotEmpty()) {
            CtaEvent.NavDirectionsNavigation(
                object : NavDirections {
                    override fun getArguments(): Bundle =
                        InboxMessagesPopupFragmentArgs(it.message).toBundle()

                    override fun getActionId(): Int = R.id.inbox_messages_nav_graph
                }
            )
        } else {
            null
        }

        viewModelScope.launch { repository.setMessageOpen(it) }
    }

    var onItemDismissListener: (InboxMessage) -> Unit = { message ->
        viewModelScope.launch {
            repository.dismissMessage(message)

            messages.value
                ?.toMutableList()
                ?.let {
                    it.remove(message)
                    messages.postValue(it)
                }
        }
    }
}
