package com.etrade.mobilepro.marketscalendar.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.marketscalendar.api.model.EventsRequest
import com.etrade.mobilepro.marketscalendar.api.model.EventsResponse
import com.etrade.mobilepro.marketscalendar.api.repo.MarketsCalendarRepo
import com.etrade.mobilepro.marketscalendar.data.dto.EARNINGS_EVENT
import com.etrade.mobilepro.marketscalendar.util.createRequestForMarketsEvents
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.market.time.NEW_YORK_ZONE_ID
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.DateTimeFormatterBuilder
import org.threeten.bp.temporal.ChronoField
import java.text.DecimalFormat
import java.util.Locale

private const val WIDTH_YEAR = 4
private const val WIDTH_MONTH_OF_YEAR = 2
private const val WIDTH_DAY_OF_MONTH = 2
private const val WIDTH_HOUR_OF_DAY = 2
private const val WIDTH_MINUTE_OF_HOUR = 2
private const val WIDTH_SECOND_OF_MINUTE = 2

val EVENTS_DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm a z", Locale.US)
// for 20190606083000 date strings
val EVENTS_FULL_DATE_TIME_FORMATTER: DateTimeFormatter = DateTimeFormatterBuilder()
    .parseCaseInsensitive()
    .appendValue(ChronoField.YEAR, WIDTH_YEAR)
    .appendValue(ChronoField.MONTH_OF_YEAR, WIDTH_MONTH_OF_YEAR)
    .appendValue(ChronoField.DAY_OF_MONTH, WIDTH_DAY_OF_MONTH)
    .appendValue(ChronoField.HOUR_OF_DAY, WIDTH_HOUR_OF_DAY)
    .appendValue(ChronoField.MINUTE_OF_HOUR, WIDTH_MINUTE_OF_HOUR)
    .appendValue(ChronoField.SECOND_OF_MINUTE, WIDTH_SECOND_OF_MINUTE)
    .toFormatter()

abstract class MarketsCalendarResultViewModel<T>(
    private val marketsCalendarRepo: MarketsCalendarRepo,
    private val sharedCalendarState: SharedCalendarState
) : ViewModel() {

    abstract val eventType: String

    abstract val eventFilterEnable: String?

    abstract val marketsEventsRequest: MarketsCalendarRepo.(EventsRequest) -> Single<EventsResponse<T>>

    abstract fun mapEventItem(item: T): EventDetails

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val _events: MediatorLiveData<Resource<List<EventDetails>>> = MediatorLiveData()
    private val _eventsSummary: MediatorLiveData<Resource<EventsSummary>> = MediatorLiveData()

    private val twoWeekDaysCount = DayOfWeek.values().size * 2 - 1
    protected val nyTimeZone: ZoneId = NEW_YORK_ZONE_ID
    protected val bigDecimalValueTemplate: DecimalFormat = DecimalFormat("#.###")

    val events: LiveData<Resource<List<EventDetails>>> = _events
    val eventsSummary: LiveData<Resource<EventsSummary>> = _eventsSummary

    protected val currentDate: LocalDate = LocalDate.now()

    private val startDate: LocalDate
        get() = sharedCalendarState.selectedCalendarRange.value!!.first

    private val endDate: LocalDate
        get() = sharedCalendarState.selectedCalendarRange.value!!.second

    val selectedRange: Int
        get() = sharedCalendarState.selectedCalendarRange.value!!.third

    var selectedEventsDate: LocalDate = sharedCalendarState.selectedDate

    var selectedEarningsDate: LocalDate = sharedCalendarState.selectedDate

    var selectedDate: LocalDate
        get() = sharedCalendarState.selectedDate
        set(value) = sharedCalendarState.updateSelectedDate(value)

    var selectedDatePagePosition: Int = sharedCalendarState.selectedCalendarRange.value!!.third

    init {
        _events.addSource(sharedCalendarState.selectedDateLiveData) {
            loadEvents()
        }

        _eventsSummary.addSource(sharedCalendarState.selectedCalendarRange) { (start, end, _) ->
            loadEventsSummary(start, end)
        }
    }

    private fun loadEvents(date: LocalDate) {
        _events.value = Resource.Loading()

        val request = createRequestForMarketsEvents(
            startDate = date,
            endDate = date,
            eventType = eventType,
            eventFilterEnable = eventFilterEnable
        )

        compositeDisposable.add(
            marketsEventsRequest(marketsCalendarRepo, request)
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                    onSuccess = {
                        it
                            .eventsResponseData
                            ?.eventsResponseDm
                            ?.eventsDataList
                            ?.map(::mapEventItem)
                            ?.sorted()
                            ?.also { result -> _events.postValue(Resource.Success(result)) }
                            ?: _events.postValue(Resource.Success(listOf()))
                    },
                    onError = { _events.postValue(Resource.Failed()) }
                )
        )
    }

    private fun loadEventsSummary(startDate: LocalDate, endDate: LocalDate) {
        val requestBody = createRequestForMarketsEvents(
            startDate = startDate,
            endDate = endDate,
            eventType = eventType,
            eventFilterEnable = eventFilterEnable
        )

        compositeDisposable.add(
            marketsCalendarRepo.getEventsSummary(requestBody)
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                    onSuccess = { response ->
                        response
                            .eventsSummaryResponseData
                            ?.eventsSummaryResponseDm
                            ?.eventsSummaryList
                            ?.filter { it.dateTime != null }
                            ?.map { LocalDate.parse(it.dateTime, EVENTS_FULL_DATE_TIME_FORMATTER) }
                            ?.also { items -> _eventsSummary.postValue(Resource.Success(EventsSummary(items.toHashSet()))) }
                            ?: _eventsSummary.postValue(Resource.Success(EventsSummary(setOf())))
                    },
                    onError = { _eventsSummary.postValue(Resource.Failed()) }
                )
        )
    }

    fun loadEvents() {
        if (eventType == EARNINGS_EVENT) {
            loadEvents(selectedEarningsDate)
        } else {
            loadEvents(selectedEventsDate)
        }
    }

    fun loadEventsSummary() {
        loadEventsSummary(startDate, endDate)
    }

    fun updateSelectedCalendarRange(startDate: LocalDate, endDate: LocalDate, position: Int) {
        sharedCalendarState.updateSelectedCalendarRange(Triple(startDate, endDate, position))
    }

    /**
     * Based on Event type selected date will be updated
     * @param date: This is the user selected date for event/earnings
     */
    fun onDateSelected(date: LocalDate) {
        if (eventType == EARNINGS_EVENT) {
            selectedEarningsDate = date
        } else {
            selectedEventsDate = date
        }
        selectedDate = date
    }

    override fun onCleared() {
        compositeDisposable.clear()

        super.onCleared()
    }
}
