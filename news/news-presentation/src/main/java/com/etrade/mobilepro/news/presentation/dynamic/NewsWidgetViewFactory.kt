package com.etrade.mobilepro.news.presentation.dynamic

import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicWidget
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.news.api.dto.NewsWidgetPortfolioDto
import com.etrade.mobilepro.news.api.dto.NewsWidgetTodayDto
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.api.repo.WidgetNewsRepo
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

class NewsWidgetViewFactory @Inject constructor(
    private val widgetNewsRepo: WidgetNewsRepo,
    private val statusRepo: ReadStatusRepo,
    private val selectedAccountProvider: () -> Account?,
    private val userHasMoreThanOneBrokerageAccount: () -> Boolean,
    private val userHasMoreThanOneBAccount: () -> Boolean,
    private val newsRouter: NewsRouter,
    private val resources: Resources,
    private val userPreferences: UserPreferences
) : DynamicUiViewFactory {

    @Suppress("LongMethod")
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is NewsWidgetPortfolioDto || dto is NewsWidgetTodayDto) {
            return object : GenericLayout, DynamicWidget {
                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is NewsWidgetViewModel) {
                        viewModel.fetchNews()
                    }
                }

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    val newsType = if (dto is NewsWidgetPortfolioDto) {
                        NewsType.PORTFOLIO
                    } else {
                        NewsType.MARKET
                    }
                    val showAccountSelection =
                        userHasMoreThanOneBrokerageAccount() && newsType == NewsType.PORTFOLIO

                    NewsWidgetViewModel(
                        widgetNewsRepo,
                        statusRepo,
                        showAccountSelection,
                        userHasMoreThanOneBAccount(),
                        selectedAccountProvider,
                        newsRouter,
                        newsType,
                        userPreferences,
                        resources
                    )
                }

                override val viewModelClass = NewsWidgetViewModel::class.java
                override val type: DynamicWidget.Type
                    get() = DynamicWidget.Type.NEWS

                override val uniqueIdentifier: String = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("NewsWidgetViewModel registered to unknown type")
        }
    }
}
