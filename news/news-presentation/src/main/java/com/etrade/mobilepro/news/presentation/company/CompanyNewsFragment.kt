package com.etrade.mobilepro.news.presentation.company

import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.etrade.mobilepro.alerts.presentation.AlertsSharedViewModel
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.presentation.NewsSharedViewModel
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.news.presentation.adapter.NewsAdapter
import com.etrade.mobilepro.news.presentation.base.BaseNewsFragment
import com.etrade.mobilepro.news.presentation.databinding.FragmentNewsCompanyBinding
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.presentation.util.toFormBundle
import com.etrade.mobilepro.usersession.api.UserSessionStateProvider
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.snackbar.showIfNotConsumed
import com.etrade.mobilepro.viewdelegate.LifecycleObserverViewDelegate
import com.etrade.mobilepro.watchlist.delegates.AddToWatchListDelegate
import com.squareup.moshi.Moshi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.Collections
import javax.inject.Inject
import javax.inject.Provider

class CompanyNewsFragment : BaseNewsFragment<CompanyNewsViewModel>() {
    @Inject
    lateinit var moshi: Moshi

    @Inject
    lateinit var userState: UserSessionStateProvider

    @Inject
    lateinit var addToWatchListDelegate: AddToWatchListDelegate

    @Inject
    lateinit var tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>

    private var currentSymbol: SearchResultItem.Symbol? = null
    private val alertsSharedViewModel: AlertsSharedViewModel by activityViewModels { viewModelFactory }

    private val quotesWidgetViewModel: QuotesWidgetViewModel by viewModels { viewModelFactory }
    private val lifecycleObserverViewDelegate: LifecycleObserverViewDelegate by lazy {
        LifecycleObserverViewDelegate(
            listOf(quotesWidgetViewModel)
        )
    }

    private val binding by viewBinding(FragmentNewsCompanyBinding::bind)

    override val layoutRes: Int = R.layout.fragment_news_company

    override val viewModelClass: Class<CompanyNewsViewModel> = CompanyNewsViewModel::class.java

    override fun getNewsDetailContainer(): View? = binding.newsDetailContainer?.root

    override fun getSwipeRefreshLayout(): SwipeRefreshLayout = binding.swipeRefresh

    override fun getNewsRecyclerView(): RecyclerView = binding.newsRv

    override fun setupFragment() {
        super.setupFragment()
        addToWatchListDelegate.init(this, binding.loadingIndicator)
    }

    @ExperimentalCoroutinesApi
    @Suppress("LongMethod")
    override fun bindViewModels(
        viewModel: CompanyNewsViewModel,
        sharedViewModel: NewsSharedViewModel,
        adapter: NewsAdapter
    ) {
        viewModel.quoteWidgetViewModel = quotesWidgetViewModel

        viewModel.currentSymbol.observe(
            viewLifecycleOwner,
            Observer { symbol ->
                currentSymbol = symbol
                symbol?.let {
                    viewModel.applySymbol(it)
                    addToWatchListDelegate.setSymbol(it)
                }
            }
        )
        viewModel.needOpenQuoteLookup.observe(
            viewLifecycleOwner,
            Observer {
                it.consume {
                    sharedViewModel.isCompanyTabOpenedFirstTime = true
                    router.openSearch()
                }
            }
        )

        viewModel.setAlertSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume {
                    currentSymbol?.let { symbol ->
                        router.navigateToSetAlert(findNavController(), symbol, NewsType.COMPANY)
                    }
                }
            }
        )
        viewModel.addToWatchlistSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume {
                    currentSymbol?.let {
                        addToWatchListDelegate.showWatchList()
                    }
                }
            }
        )
        viewModel.tradeClickedSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume {
                    currentSymbol?.let { symbol ->
                        val args = symbol.toAdjustedSymbol()
                            .toFormBundle(tradeFormParametersBuilderProvider.get(), moshi)
                        router.navigateToTrade(findNavController(), args, NewsType.COMPANY)
                    }
                }
            }
        )

        alertsSharedViewModel.alertWasSet.observe(
            viewLifecycleOwner,
            Observer { signal ->
                snackbarUtil.showIfNotConsumed(signal)
            }
        )

        passInSymbol(viewModel, sharedViewModel)

        lifecycleObserverViewDelegate.observe(viewLifecycleOwner)
    }

    // Modify Option Symbol to Equity symbol since company news always associated with Equity type.
    private fun SearchResultItem.Symbol.toAdjustedSymbol(): SearchResultItem.Symbol =
        if (type.isOption) {
            this.copy(
                title = underlyerSymbol,
                type = InstrumentType.EQ,
                displaySymbol = underlyerSymbol
            )
        } else {
            this
        }

    private fun passInSymbol(
        viewModel: CompanyNewsViewModel,
        sharedViewModel: NewsSharedViewModel
    ) {
        arguments?.let {
            router.companyNewsSymbolArg(it)
        }?.let {
            viewModel.passedInSymbol(it)
            viewModel.onBecomeActive()
        } ?: viewModel.passedInSymbol(sharedViewModel.passedInSymbol)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        addToWatchListDelegate.clearUp()
    }

    override val recyclerDividerPositions: List<Int> = Collections.singletonList(0)
}
