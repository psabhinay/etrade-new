package com.etrade.mobilepro.util.android.view

import android.content.res.ColorStateList
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.etrade.mobilepro.util.android.R
import com.google.android.material.textfield.TextInputLayout

fun TextInputLayout.toggleStyle(
    isParentEnabled: Boolean
) {
    isEnabled = isParentEnabled
    @ColorRes val layoutColor: Int
    @ColorRes val textColor: Int
    if (isParentEnabled) {
        layoutColor = R.color.hint_color
        textColor = R.color.textColorPrimary
    } else {
        layoutColor = R.color.hint_color_disabled
        textColor = R.color.hint_color_disabled
    }
    defaultHintTextColor = ColorStateList.valueOf(ContextCompat.getColor(context, layoutColor))
    editText?.setTextColor(ColorStateList.valueOf(ContextCompat.getColor(context, textColor)))
}
