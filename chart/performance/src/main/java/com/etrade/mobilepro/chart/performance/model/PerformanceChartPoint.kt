package com.etrade.mobilepro.chart.performance.model

import com.etrade.mobilepro.chart.api.ChartPoint
import com.etrade.mobilepro.chartengine.TimedChartPoint
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

class PerformanceChartPoint(
    point: ChartPoint,
    defaultValue: Double
) : TimedChartPoint {
    override val x: LocalDateTime = LocalDate.parse(point.rawX, DateTimeFormatter.BASIC_ISO_DATE).atStartOfDay()
    override val y: Double = point.rawY.toDoubleOrNull() ?: defaultValue
}

fun ChartPoint.toPerformanceChartPoint(defaultValue: Double) = PerformanceChartPoint(this, defaultValue)
