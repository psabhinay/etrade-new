package com.etrade.mobilepro.login.presentation

interface BiometricAuthenticationHelperHolder {
    val biometricAuthenticationHelper: BiometricAuthenticationHelper
}
