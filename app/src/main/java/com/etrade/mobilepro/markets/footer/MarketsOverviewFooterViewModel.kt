package com.etrade.mobilepro.markets.footer

import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.TimeStampReporter
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import javax.inject.Inject

class MarketsOverviewFooterViewModel @Inject constructor(
    private val timeStampReporter: TimeStampReporter,
    val customizeViewVisible: Boolean
) : DynamicViewModel(R.layout.item_market_overview_footer), TimeStampReporter by timeStampReporter {
    override val variableId: Int = BR.obj

    fun customizeViewClicked() {
        clickEvents.value = CtaEvent.CustomizeEvent
    }
}
