package com.etrade.mobilepro.portfolio.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

abstract class SingleSourceLiveData<T> : MediatorLiveData<T>() {

    private var previousSource: LiveData<T>? = null

    fun fetchData(ids: List<String>) {
        previousSource?.let { removeSource(it) }
        previousSource = getData(ids).also { source ->
            addSource(source) {
                if (shouldEmit(it)) {
                    value = it
                }
            }
        }
    }

    protected abstract fun getData(ids: List<String>): LiveData<T>

    protected open fun shouldEmit(value: T): Boolean = true
}
