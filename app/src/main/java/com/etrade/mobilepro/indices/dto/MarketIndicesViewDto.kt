package com.etrade.mobilepro.indices.dto

import androidx.annotation.Keep
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MarketIndicesViewDto(
    @Json(name = "data")
    override val data: Any?,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?,
    val behaviourOptions: BehaviourOptions = BehaviourOptions(),
    @Json(name = "useMarketChangeColor")
    val useMarketChangeColor: Boolean = false,
    @Json(name = "showFutures")
    val showFutures: Boolean = true
) : BaseScreenView()

@Keep
enum class Action {
    DO_NOTHING,
    OPEN_DETAILS,
    OPEN_INDEX_CHART
}

data class BehaviourOptions(
    val clickAction: Action = Action.OPEN_DETAILS
)
