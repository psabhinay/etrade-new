package com.etrade.mobilepro.screener.presentation.stock.technicals.percentinput

import com.etrade.mobilepro.inputvalueview.InputFieldManager

internal class PercentFormatter(private val dailyPercentFormat: String) : InputFieldManager.Formatter {
    override fun formatRawContent(rawContent: String): String {
        return if (!rawContent.endsWith("%") && rawContent.isNotBlank()) {
            String.format(dailyPercentFormat, rawContent)
        } else {
            rawContent
        }
    }
}
