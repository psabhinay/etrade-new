package com.etrade.mobilepro.chat.data.setup.dto.contactId

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChatContactIdMessagesDto(

    @Json(name = "items")
    val items: List<ChatItemDto> = emptyList()
)
