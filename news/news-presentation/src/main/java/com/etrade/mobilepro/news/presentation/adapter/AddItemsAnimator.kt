package com.etrade.mobilepro.news.presentation.adapter

import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView

class AddItemsAnimator : DefaultItemAnimator(), RecyclerView.ItemAnimator.ItemAnimatorFinishedListener {

    var executeWhenAnimationFinished: (() -> Unit)? = null

    override fun onAnimationsFinished() {
        executeWhenAnimationFinished?.invoke()
        executeWhenAnimationFinished = null
    }

    override fun onAddFinished(item: RecyclerView.ViewHolder?) {
        isRunning(this)
    }
}
