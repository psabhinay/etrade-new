package com.etrade.completeview.dto

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto

data class NetSummaryViewDto(
    val netAssetsDto: NetAssetsDto,
    val dayGainDto: DayGainDto,
    val accountId: String? = null
) : BaseScreenView() {
    override val data = NetSummaryDto(netAssetsDto, dayGainDto)
    override val ctaList: List<CallToActionDto>? = null
}
