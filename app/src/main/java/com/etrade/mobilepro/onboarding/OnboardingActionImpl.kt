package com.etrade.mobilepro.onboarding

import android.app.Activity
import android.content.Intent
import com.etrade.mobilepro.home.MainActivity

const val EXTRA_SHOW_OPEN_AN_ACCOUNT_PAGE = "EXTRA_SHOW_OPEN_AN_ACCOUNT_PAGE"

class OnboardingActionImpl : OnboardingAction {

    override fun logonAction(activity: Activity) {
        activity.apply {
            startActivity(Intent(this, MainActivity::class.java))

            finish()
        }
    }

    override fun openAnAccountAction(activity: Activity) {
        activity.apply {
            startActivity(
                Intent(this, MainActivity::class.java).apply {
                    putExtra(EXTRA_SHOW_OPEN_AN_ACCOUNT_PAGE, true)
                }
            )

            finish()
        }
    }
}
