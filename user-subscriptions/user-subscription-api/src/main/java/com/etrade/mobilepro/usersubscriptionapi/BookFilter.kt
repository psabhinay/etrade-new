package com.etrade.mobilepro.usersubscriptionapi

import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2BookType

data class BookFilter(val bookType: Level2BookType, val requiresSubscription: Boolean)
