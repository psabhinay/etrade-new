package com.etrade.mobilepro.earnings.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.earnings.EarningsResponseResult

interface EarningsRepo {
    suspend fun loadEarnings(symbol: String, periodType: String, periodCount: Int): ETResult<EarningsResponseResult>
}
