package com.etrade.mobilepro.common

import android.content.res.Resources
import androidx.lifecycle.LiveData
import javax.inject.Inject

const val MENU_GRAPH_RESOURCE_NAME = "menuGraph"

interface RestorableViewModelDelegate {

    fun init(clazz: Class<*>, restorable: LiveData<*>, restoreState: ((Restorable) -> Unit)?)

    fun getRestoredState(clazz: Class<*>): Restorable?

    fun dispose(clazz: Class<*>)

    fun clearAll()

    fun onBottomNavigationItemSelected(id: Int)
}

class RestorableViewModelDelegateImpl @Inject constructor(
    private val restorableState: RestorableState,
    private val resources: Resources
) : RestorableViewModelDelegate {

    private val restorableMap
        get() = restorableState.stateMap

    /**
     * Sets up the ViewState observer and restores the state
     */
    override fun init(clazz: Class<*>, restorable: LiveData<*>, restoreState: ((Restorable) -> Unit)?) {
        if (restorableState.selectedBottomNavButton.isMenuButton(resources)) {
            restorable.observeForever {
                if (it is Restorable) {
                    restorableMap[clazz] = it
                } else {
                    throw IllegalArgumentException("RestorableViewModelDelegate init() must be called with type LiveData<Restorable>")
                }
            }

            restorableMap[clazz]?.let { restoreState?.invoke(it) }
        }
    }

    /**
     * View saved state without restoring it
     */
    override fun getRestoredState(clazz: Class<*>): Restorable? {
        return restorableMap[clazz]
    }

    override fun dispose(clazz: Class<*>) {
        if (restorableState.selectedBottomNavButton.isMenuButton(resources)) {
            restorableMap[clazz] = null
        }
    }

    override fun clearAll() {
        restorableMap.clear()
        restorableState.tabPositionMap.clear()
    }

    override fun onBottomNavigationItemSelected(id: Int) {
        restorableState.selectedBottomNavButton = id
    }
}

/**
 * There's something really weird going on with R.id.menuGraph.
 * It seems to have two distinct int values so the check
 * selectedNavButton == R.id.menuGraph always fails
 *
 * This is a workaround but does the job
 */
fun Int.isMenuButton(resources: Resources) =
    runCatching {
        resources.getResourceName(this).endsWith(MENU_GRAPH_RESOURCE_NAME)
    }.getOrDefault(false)
