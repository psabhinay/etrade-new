package com.etrade.mobilepro.login.api.repo

/**
 * Enum class to indicate the type of the login error that happened.
 * Most of them are mapped from login error codes returned from login service
 */
enum class LoginResultCode {
    SUCCESS,
    WARNING,
    /**
     * Indicates network is not available
     */
    NETWORK_ERROR,
    CYOTA_CHALLENGE,
    /**
     * Indicates invalid credentials or user not logged in
     */
    INVALID_CREDENTIALS,
    /**
     * Indicates biometric enabled and invalid credentials
     */
    INVALID_STORED_PASSWORD,
    /**
     * Indicates force upgrade
     */
    FORCE_UPGRADE,
    /**
     * Indicates access denied or system under maintenance
     */
    ACCESS_DENIED,
    /**
     * Indicates five consecutive wrong password
     */
    LOCKED_OUT,
    /**
     * Indicates password expired
     */
    PASSWORD_EXPIRED,
    CONFLICTED_USER,
    CAP1_CONFLICTED_USER,
    CAP1_PASSWORD_CHANGE,
    CAP1_PREVIEW_USER,
    /**
     * Indicates wrong password
     */
    WRONG_PASSWORD,
    UNKNOWN
}
