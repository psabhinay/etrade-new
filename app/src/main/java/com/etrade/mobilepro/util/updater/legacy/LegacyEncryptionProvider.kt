package com.etrade.mobilepro.util.updater.legacy

import com.etrade.mobilepro.encryption.api.Base64Processor
import com.etrade.mobilepro.encryption.api.SymmetricAlgorithmSpec
import com.etrade.mobilepro.encryption.api.transformation
import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.GCMParameterSpec
import javax.inject.Inject

private const val AUTHENTICATION_TAG_LENGTH = 128
private const val IV_BUFFER_SIZE = 12

class LegacyEncryptionProvider @Inject constructor(
    symmetricAlgorithmSpec: SymmetricAlgorithmSpec,
    private val base64Processor: Base64Processor
) {

    private val cipher = Cipher.getInstance(symmetricAlgorithmSpec.transformation)

    private val keyAlias = "EtradeKey"
    private val keyStore: KeyStore = KeyStore.getInstance("AndroidKeyStore").apply { load(null) }

    private val secretKey: SecretKey? by lazy {
        keyStore.getKey(keyAlias, null) as? SecretKey
    }

    fun decrypt(encrypted: String): String? {
        return secretKey?.let {
            runCatching {
                cipher.init(Cipher.DECRYPT_MODE, it, GCMParameterSpec(AUTHENTICATION_TAG_LENGTH, ByteArray(IV_BUFFER_SIZE)))
                String(cipher.doFinal(base64Processor.decode(encrypted)))
            }.getOrNull()
        }
    }

    fun removeCredentials() = keyStore.deleteEntry(keyAlias)
}
