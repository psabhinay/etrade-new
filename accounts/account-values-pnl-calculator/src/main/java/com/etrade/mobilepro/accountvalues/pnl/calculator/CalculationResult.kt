package com.etrade.mobilepro.accountvalues.pnl.calculator

import com.etrade.eo.accountvalues.api.AccountValuesVO
import com.etrade.eo.accountvalues.api.NetOutstandingCall
import io.reactivex.Observer
import java.math.BigDecimal

sealed class CalculationResult {

    fun checkIfNotEmptyResultAndPost(observer: Observer<AccountValuesVO>) {
        if (this is CalculatedAccountValuesVO) {
            observer.onNext(this)
        }
    }

    data class CalculatedAccountValuesVO(
        override val accountNumber: String = "",
        override val bondValue: BigDecimal? = null,
        override val buyingPower: BigDecimal? = null,
        override val cash: BigDecimal? = null,
        override val cashAvailableToWithdraw: BigDecimal? = null,
        override val cashMovement: BigDecimal? = null,
        override val committedOpenOrdersSum: BigDecimal? = null,
        override val dayTradingBuyingPower: BigDecimal? = null,
        override val dtcash: BigDecimal? = null,
        override val equityRequirement: BigDecimal? = null,
        override val intradaySMA: BigDecimal? = null,
        override val maintenanceExcess: BigDecimal? = null,
        override val marginBalance: BigDecimal? = null,
        override val marginBuyingPower: BigDecimal? = null,
        override val marginDebitBalance: BigDecimal? = null,
        val marketValue: BigDecimal? = null,
        override val moneyMarketValue: BigDecimal? = null,
        override val mutualFundValue: BigDecimal? = null,
        override val netLiquidationValue: BigDecimal? = null,
        val netAssets: BigDecimal? = null,
        override val netOptionsValue: BigDecimal? = null,
        override val netOutstandingCallsAmount: BigDecimal = BigDecimal.ZERO,
        override val netOutstandingCallsDetails: List<NetOutstandingCall> = emptyList(),
        override val numOfDayTrades: Int? = null,
        override val openOrderReserve: BigDecimal? = null,
        override val openTradeEquity: BigDecimal? = null,
        override val optionRequirement: BigDecimal? = null,
        override val optionsValueLong: BigDecimal? = null,
        override val optionsValueShort: BigDecimal? = null,
        override val pendingWithdrawals: BigDecimal? = null,
        override val plDay: BigDecimal? = null,
        override val plDayClosed: BigDecimal? = null,
        val plDayPercent: BigDecimal? = null,
        override val plYTD: BigDecimal? = null,
        override val portfolioMarginBuyingPower: BigDecimal? = null,
        override val portfolioMarginRequirement: BigDecimal? = null,
        override val purchasingPower: BigDecimal? = null,
        override val recentDepositHold: BigDecimal? = null,
        override val settledCash: BigDecimal? = null,
        override val startingCATW: BigDecimal? = null,
        override val stockValueLong: BigDecimal? = null,
        override val stockValueShort: BigDecimal? = null,
        val totalGain: BigDecimal? = null,
        val totalGainPercent: BigDecimal? = null,

        // The total sum of price paid or cost basis incurred for all positions within an account
        // Used to evaluate the Account's total gain percent.
        val totalCost: BigDecimal? = null,
        override val unsettledCashAvailable: BigDecimal? = null,
        override val unsettledCashUnavailable: BigDecimal? = null
    ) : AccountValuesVO, CalculationResult()

    object EmptyCalculationResult : CalculationResult()
}
