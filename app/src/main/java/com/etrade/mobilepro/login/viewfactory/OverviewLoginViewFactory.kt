package com.etrade.mobilepro.login.viewfactory

import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.login.data.dto.LoginViewDto
import com.etrade.mobilepro.login.presentation.LoginViewDelegate
import javax.inject.Inject

class OverviewLoginViewFactory @Inject constructor(
    private val loginViewDelegate: LoginViewDelegate
) : DynamicUiViewFactory {

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is LoginViewDto) {
            return loginViewDelegate
        } else {
            throw DynamicUiViewFactoryException("LoginViewFactory registered to unknown type")
        }
    }
}
