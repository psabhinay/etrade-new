package com.etrade.mobilepro.transfermoney.presentation.contributiondetail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.transfermoney.api.TransferIraContribution
import com.etrade.mobilepro.transfermoney.presentation.R
import com.etrade.mobilepro.transfermoney.presentation.databinding.TransferFragmentContributionDetailBinding
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.HeaderSectionItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import org.threeten.bp.format.DateTimeFormatter

@RequireLogin
class TransferIraContributionDetailFragment(
    private val viewModelProvider: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val deadlineDateFormatter: DateTimeFormatter
) : OverlayBaseFragment() {
    override val layoutRes: Int = R.layout.transfer_fragment_contribution_detail
    private val binding by viewBinding(TransferFragmentContributionDetailBinding::bind)
    private val viewModel by navGraphViewModels<TransferIraContributionDetailViewModel>(R.id.transfer_money_nav_graph) { viewModelProvider }
    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeIraDetails()
    }

    private fun observeIraDetails() {
        viewModel.iraContributionDetails.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    TransferIraContributionDetailViewModel.ViewState.Loading -> binding.iraContributionLoading.show()
                    is TransferIraContributionDetailViewModel.ViewState.Success -> {
                        binding.iraContributionLoading.hide()
                        binding.iraContributionTable.setContent(
                            generateTableSections(it.contributionDetails)
                        )
                    }
                    is TransferIraContributionDetailViewModel.ViewState.Error -> {
                        binding.iraContributionLoading.hide()
                        snackbarUtil.retrySnackbar { it.retry() }
                    }
                }
            }
        )
    }

    private fun generateTableSections(contributionDetails: TransferIraContribution): List<TableSectionItemLayout> {
        val currentYear = generateContributionYear(contributionDetails.currentYearDetails)
        val previousYear = contributionDetails
            .takeIf { contributionDetails.eligibleYears.size > 1 }
            ?.previousYearDetails
            ?.let {
                generateContributionYear(it)
            }.orEmpty()
        return currentYear + previousYear
    }

    private fun generateContributionYear(contributionDetails: TransferIraContribution.Details?): List<TableSectionItemLayout> {
        if (contributionDetails == null) {
            return emptyList()
        }
        return listOf(
            HeaderSectionItem(getString(R.string.ira_contribution_year, contributionDetails.taxYear.toString())),
            createSummarySection(getString(R.string.ira_ytd_contribution_text), '$' + contributionDetails.amount),
            createSummarySection(getString(R.string.ira_contribution_limit_text), '$' + contributionDetails.maxLimit),
            createSummarySection(getString(R.string.ira_still_contribute_text), '$' + contributionDetails.amountLeft),
            createSummarySection(getString(R.string.ira_contribution_deadline_text), contributionDetails.taxDueDate.format(deadlineDateFormatter))
        )
    }
}
