package com.etrade.mobilepro.markets.common

import android.os.Bundle
import android.util.TypedValue
import android.view.View
import androidx.annotation.CallSuper
import androidx.annotation.StringRes
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.extension.screenWidthPixels
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.swiperefresh.MultiSwipeRefreshLayout
import com.etrade.mobilepro.tableviewutils.presentation.RestrictedTableView
import com.etrade.mobilepro.tableviewutils.presentation.adapter.SymbolTableViewAdapter
import com.etrade.mobilepro.tableviewutils.presentation.adapter.TableViewAdapter
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar

internal abstract class MarketsBaseFragment<T : MarketsBaseViewModel> constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    layoutRes: Int
) : Fragment(layoutRes) {

    open val displaySymbolDescription = false

    protected abstract val marketsRowHeaderWidth: Int
    protected abstract val loading: ContentLoadingProgressBar
    protected abstract val table: RestrictedTableView
    protected abstract val emptyQuotesText: View
    protected abstract val swipeRefresh: MultiSwipeRefreshLayout

    protected abstract val viewModelClassType: Class<T>

    protected val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(
            viewModelClassType
        )
    }

    private var snackBar: Snackbar? = null
    private val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViewModels()

        swipeRefresh.setOnRefreshListener {
            refresh()
        }
    }

    protected open fun onDestroyBinding() {
        swipeRefresh.isEnabled = false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        table.adapter = null
    }

    @CallSuper
    protected open fun setUpViewModels() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                observeViewState(it)
            }
        )
    }

    @CallSuper
    protected open fun handleLoading() {
        if (!swipeRefresh.isRefreshing) {
            loading.show()
        }
        snackBar?.dismiss()
        emptyQuotesText.visibility = View.GONE
    }

    @CallSuper
    protected open fun handleError() {
        handleComplete()
        snackBar = snackbarUtil.retrySnackbar(dismissOnPause = true) { refresh() }
    }

    @CallSuper
    protected open fun handleEmpty() {
        handleComplete()
    }

    @CallSuper
    protected open fun handleSuccess(success: MarketsBaseViewModel.ViewState.Success) {
        loading.hide()
        snackBar?.dismiss()
        swipeRefresh.isRefreshing = false
        setupTableView(viewModel.columnHeaderList, success.rowHeaders, success.cellItems)
    }

    protected fun getTimeStampText(@StringRes timeStampResId: Int, timeStamp: String?) = if (timeStamp.isNullOrEmpty()) {
        ""
    } else {
        getString(timeStampResId, timeStamp)
    }

    private fun observeViewState(it: MarketsBaseViewModel.ViewState) {
        when (it) {
            is MarketsBaseViewModel.ViewState.Loading -> handleLoading()
            is MarketsBaseViewModel.ViewState.Error -> handleError()
            is MarketsBaseViewModel.ViewState.Success -> handleSuccess(it)
            is MarketsBaseViewModel.ViewState.Empty -> handleEmpty()
        }
    }

    private fun setupTableView(columnHeaderList: List<Cell>, rowHeaders: List<SymbolCell>, cellItems: List<List<ResultDataCell>>) {
        val adapter = table.adapter as? TableViewAdapter
        if (adapter != null) {
            adapter.updateAllItems(columnHeaderList, rowHeaders, cellItems)
        } else {
            table.initTableView(columnHeaderList, rowHeaders, cellItems)
        }
    }

    private fun refresh() {
        viewModel.refresh()
    }

    private fun handleComplete() {
        loading.hide()
        snackBar?.dismiss()
        table.visibility = View.GONE
        swipeRefresh.isRefreshing = false
        emptyQuotesText.visibility = View.VISIBLE
    }

    private fun RestrictedTableView.initTableView(columnHeaderList: List<Cell>, rowHeaders: List<SymbolCell>, cellItems: List<List<ResultDataCell>>) {
        val activity = activity ?: return
        val outValue = TypedValue().apply {
            resources.getValue(marketsRowHeaderWidth, this, true)
        }

        val tableAdapter = context?.let {
            SymbolTableViewAdapter(
                it,
                displaySymbolDescription
            )
        }

        adapter = tableAdapter
        isNestedScrollingEnabled = false
        isIgnoreSelectionColors = true
        setHasFixedWidth(true)

        rowHeaderWidth = (activity.screenWidthPixels * outValue.float).toInt()
        adapter.apply {
            setAllItems(columnHeaderList, rowHeaders, cellItems)
            swipeRefresh.setSwipeableChildren(table.cellRecyclerView)
        }
    }
}
