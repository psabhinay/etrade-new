package com.etrade.mobilepro.billpay.data.rest.schedulepayment

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
class PaymentRequestBodyDto(
    @Json(name = "value")
    val content: PaymentRequestDto
)

@JsonClass(generateAdapter = true)
class PaymentRequestDto(
    @Json(name = "sessionId")
    val sessionId: String,
    @Json(name = "paymentInfo")
    val payment: PaymentInfoDto,
    @Json(name = "payeeInfo")
    val payee: PayeeInfoDto
)

@JsonClass(generateAdapter = true)
class PaymentInfoDto(
    @Json(name = "paymentId")
    val paymentId: String? = null,
    @Json(name = "amount")
    val amount: BigDecimal,
    @Json(name = "scheduleDate")
    val scheduleDate: String,
    @Json(name = "memo")
    val memo: String? = null,
    @Json(name = "accountId")
    val originAccountId: String,
    @Json(name = "instNo")
    val originAccountInstNo: String
)

@JsonClass(generateAdapter = true)
class PayeeInfoDto(
    @Json(name = "payeeId")
    val payeeId: String,
    @Json(name = "leadDays")
    val leadDays: Int
)

@JsonClass(generateAdapter = true)
class PaymentResponseBodyDto(
    @Json(name = "paymentDetails")
    val content: List<PaymentResponseDto?>?
) : BaseDataDto()

@JsonClass(generateAdapter = true)
class PaymentResponseDto(
    @Json(name = "paymentId")
    val paymentId: String?,
    @Json(name = "confirmationNumber")
    val confirmationNumber: String?,
    @Json(name = "deliveryDate")
    val deliveryDate: String?
)
