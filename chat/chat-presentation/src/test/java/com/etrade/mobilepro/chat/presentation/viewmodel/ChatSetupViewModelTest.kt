package com.etrade.mobilepro.chat.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.chat.api.ChatParamsModel
import com.etrade.mobilepro.chat.api.setup.models.ChatActiveStatusModel
import com.etrade.mobilepro.chat.api.setup.models.ChatProductItemModel
import com.etrade.mobilepro.chat.api.setup.models.ChatProductItemsModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatAccountModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatCustomerAccountModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatCustomerDetailsModel
import com.etrade.mobilepro.chat.api.setup.repo.ChatSetupRepo
import com.etrade.mobilepro.chat.api.setup.repo.ChatSetupResult
import com.etrade.mobilepro.chat.presentation.TEST_ACC_DESCRIPTION
import com.etrade.mobilepro.chat.presentation.TEST_ACC_NUM
import com.etrade.mobilepro.chat.presentation.TEST_CHAT_CONTACT_ID
import com.etrade.mobilepro.chat.presentation.TEST_ERROR_MESSAGE
import com.etrade.mobilepro.chat.presentation.TEST_FIRST_NAME
import com.etrade.mobilepro.chat.presentation.TEST_ID
import com.etrade.mobilepro.chat.presentation.TEST_LAST_NAME
import com.etrade.mobilepro.chat.presentation.TEST_USER_NAME
import com.etrade.mobilepro.chat.presentation.chatTestGeolocationModel
import com.etrade.mobilepro.chat.presentation.chatTestMetadataModel
import com.etrade.mobilepro.chat.presentation.chatTestRequestParams
import com.etrade.mobilepro.chat.presentation.setup.ChatSetupViewModel
import com.etrade.mobilepro.common.result.ETResult
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
internal class ChatSetupViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    private val chatSetupRepo: ChatSetupRepo = mock()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()
    private val chatCustomerAccountModel = ChatCustomerAccountModel(
        accNum = TEST_ACC_NUM, instDescription = TEST_ACC_DESCRIPTION,
        accounts = listOf(
            ChatAccountModel(firstname = TEST_FIRST_NAME, lastname = TEST_LAST_NAME)
        )
    )
    private val chatCustomerDetailsModel = ChatCustomerDetailsModel(
        customerAccounts = listOf(chatCustomerAccountModel),
        userGeolocation = chatTestGeolocationModel
    )
    private val chatProductItemModel = ChatProductItemModel(id = TEST_ID)
    private val chatProductItemsModel = ChatProductItemsModel(listOf(chatProductItemModel))

    private lateinit var paramsModel: ChatParamsModel
    private lateinit var sut: ChatSetupViewModel

    @Before
    fun setup() {
        Dispatchers.setMain(testCoroutineDispatcher)
        paramsModel = ChatParamsModel(
            chatContactId = TEST_CHAT_CONTACT_ID,
            chatCustomerDetailsModel = chatCustomerDetailsModel,
            chatProductItems = chatProductItemsModel,
            metadata = chatTestMetadataModel
        )
        sut = ChatSetupViewModel(chatSetupRepo, paramsModel)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun testChatRefreshError() {
        val isActive = true
        val isError = true
        setupLaunchExpectations(isActive, isError)

        verifyLaunchExpectations(isActive, isError)
    }

    @Test
    fun testChatIsOnHoliday() {
        val isActive = false
        val isError = false
        setupLaunchExpectations(isActive, isError)

        verifyLaunchExpectations(isActive, isError)
    }

    @Test
    fun testChatButtonClick() {
        setupLaunchExpectations(isActive = true, isError = false)
        with(sut) {
            refresh(true)
            onChatButtonClick()
            viewState.test().awaitValue()
                .assertValue { ChatSetupViewModel.ViewState.Status.GoToChatMessages == it.status }
                .assertValue { chatCustomerAccountModel == it.selectedAccount }
                .assertValue { TEST_USER_NAME == it.selectedName }
                .assertValue { chatProductItemModel == it.selectedSubject }
            Assert.assertEquals(chatParamsRequest, chatTestRequestParams)
        }
    }

    @Test
    fun testChatRefreshSuccess() {
        val isActive = true
        val isError = false
        setupLaunchExpectations(isActive, isError)

        verifyLaunchExpectations(isActive, isError)
    }

    private fun verifyLaunchExpectations(isActive: Boolean, isError: Boolean) {
        runBlocking(testCoroutineDispatcher) {
            sut.refresh()
            verify(chatSetupRepo).getChatActiveStatus()
        }

        sut.viewState.test()
            .assertValue { getTestStatus(getTestState(isActive, isError)).javaClass == it.status.javaClass }
            .assertValue { getTestAccountModel(isActive, isError) == it.selectedAccount }
            .assertValue { getTestUsername(isActive, isError) == it.selectedName }
            .assertValue { getTestProductItemModel(isActive, isError) == it.selectedSubject }
    }

    private fun getTestState(isActive: Boolean = false, isError: Boolean = false) = if (isError) {
        ChatSetupResult.Failure(TEST_ERROR_MESSAGE)
    } else {
        if (isActive) {
            ChatSetupResult.Success
        } else {
            ChatSetupResult.NonOperatingHour(isHoliday = true)
        }
    }

    private fun setupLaunchExpectations(isActive: Boolean = false, isError: Boolean = false) {
        runBlocking(testCoroutineDispatcher) {
            val chatActiveStatusModel = ETResult.success(ChatActiveStatusModel(isActive = isActive, metadata = chatTestMetadataModel))
            whenever(chatSetupRepo.getChatActiveStatus()).thenReturn(chatActiveStatusModel)
            whenever(chatSetupRepo.setupChat(paramsModel)).thenReturn(getTestState(isActive, isError))
        }
    }

    private fun getTestProductItemModel(isActive: Boolean, isError: Boolean) = if (!isError && isActive) {
        chatProductItemModel
    } else {
        null
    }

    private fun getTestUsername(isActive: Boolean, isError: Boolean) = if (!isError && isActive) {
        TEST_USER_NAME
    } else {
        null
    }

    private fun getTestAccountModel(isActive: Boolean, isError: Boolean) = if (!isError && isActive) {
        chatCustomerAccountModel
    } else {
        null
    }

    private fun getTestStatus(testState: ChatSetupResult) =
        when (testState) {
            ChatSetupResult.Success -> ChatSetupViewModel.ViewState.Status.Idle
            is ChatSetupResult.Failure -> ChatSetupViewModel.ViewState.Status.Error(null)
            is ChatSetupResult.NonOperatingHour -> ChatSetupViewModel.ViewState.Status.Unavailable(isHoliday = true)
        }
}
