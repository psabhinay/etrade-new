package com.etrade.mobilepro.compose.etdialogs.elements

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.FontSizes
import com.etrade.mobilepro.compose.etdialogs.R

private const val MIN_ROW_ELEMENTS = 3

@Composable
internal fun <T> DialogButtonRow(
    state: T,
    buttonRow: List<DialogButton<T>>
) {
    Row(
        modifier = Modifier.padding(horizontal = Dimens.Size16dp, vertical = Dimens.Size10dp).fillMaxWidth(),
    ) {
        buttonRow.takeIf { it.size < MIN_ROW_ELEMENTS }?.let {
            repeat(MIN_ROW_ELEMENTS + 1 - it.size) {
                Spacer(modifier = Modifier.weight(1f))
            }
        }
        buttonRow.forEachIndexed { index, buttonData ->
            OutlinedButton(
                buttonData.onClick,
                enabled = buttonData.isEnabled(state),
                colors = ButtonDefaults.outlinedButtonColors(
                    backgroundColor = colorResource(R.color.colorBackground),
                    contentColor = colorResource(R.color.purple)
                ),
                border = null,
            ) {
                Text(
                    text = buttonData.title,
                    style = MaterialTheme.typography.button.copy(fontSize = FontSizes.Size14sp, fontWeight = FontWeight.W500)
                )
            }
            if (index < buttonRow.size - 1) {
                Spacer(modifier = Modifier.weight(1f))
            }
        }
    }
}
