package com.etrade.mobilepro.biometric.presentation

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.fragment.app.commitNow
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import javax.crypto.Cipher

private const val BIOMETRIC_AUTHENTICATION_FRAGMENT_TAG = "BIOMETRIC_AUTHENTICATION_FRAGMENT_TAG"

fun FragmentManager.bindCallbacksToExistingBiometricAuthenticationFragment(bindCallbacks: BiometricAuthenticationFragment.() -> Unit) {
    restoreBiometricAuthenticationFragment()?.bindCallbacks()
}

fun FragmentManager.removeBiometricAuthenticationFragment() {
    restoreBiometricAuthenticationFragment()?.let { fragment ->
        commit(allowStateLoss = true) {
            remove(fragment)
        }
    }
}

fun FragmentManager.restoreBiometricAuthenticationFragment(): BiometricAuthenticationFragment? {
    return findFragmentByTag(BIOMETRIC_AUTHENTICATION_FRAGMENT_TAG) as BiometricAuthenticationFragment?
}

fun FragmentManager.showBiometricAuthenticationFragment(
    cipher: Cipher,
    type: BiometricPromptType = BiometricPromptType.AUTHENTICATE,
    isUserInitiated: Boolean = BiometricAuthenticator.USER_INITIATED_BY_DEFAULT,
    bindCallbacks: BiometricAuthenticationFragment.() -> Unit
) {
    fragmentFactory
        .instantiateFragment(BiometricAuthenticationFragment::class.java)
        .let { it as BiometricAuthenticationFragment }
        .apply(bindCallbacks)
        .also { fragment ->
            commitNow(allowStateLoss = true) {
                add(fragment, BIOMETRIC_AUTHENTICATION_FRAGMENT_TAG)
            }
        }
        .authenticate(cipher, type, isUserInitiated)
}
