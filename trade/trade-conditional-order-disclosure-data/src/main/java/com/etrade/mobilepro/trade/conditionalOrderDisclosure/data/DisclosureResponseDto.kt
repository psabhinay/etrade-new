package com.etrade.mobilepro.trade.conditionalOrderDisclosure.data

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.backends.neo.MessageDto
import com.etrade.mobilepro.backends.neo.toServerMessage
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class DisclosureResponseDto(
    @field:Json(name = "invest.trading.errors")
    val errorList: List<MessageDto>?,
    @field:Json(name = "invest.trading.warnings")
    val warningsList: List<MessageDto>?
) {
    val serverMessages: List<ServerMessage> = (warningsList.orEmpty() + errorList.orEmpty()).map { it.toServerMessage() }
}

@JsonClass(generateAdapter = true)
internal data class AcknowledgeConfirmationDto(
    @Json(name = "value")
    val requestValueDto: UserDecisionDto
)

@JsonClass(generateAdapter = true)
internal class UserDecisionDto(
    @Json(name = "userDecission")
    val userDecision: String = ""
)
