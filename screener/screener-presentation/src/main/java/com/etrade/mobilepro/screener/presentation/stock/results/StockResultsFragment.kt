package com.etrade.mobilepro.screener.presentation.stock.results

import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.screener.api.ScreenerDisclosure
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseScreenerResultFragment
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class StockResultsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    router: MainNavigation,
    screenersRouter: ScreenersRouter
) : BaseScreenerResultFragment(snackbarUtilFactory, router) {
    override val viewModel by viewModels<StockResultsViewModel> { viewModelFactory }

    override val resultsDisclosureClickListener: () -> Unit = {
        screenersRouter.actionResultsToDisclosure(findNavController(), ScreenerDisclosure.STOCK_RESULTS)
    }
}
