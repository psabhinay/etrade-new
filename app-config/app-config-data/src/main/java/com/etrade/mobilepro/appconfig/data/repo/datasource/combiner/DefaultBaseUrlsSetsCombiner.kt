package com.etrade.mobilepro.appconfig.data.repo.datasource.combiner

import com.etrade.mobilepro.appconfig.api.BaseUrls
import javax.inject.Inject

class DefaultBaseUrlsSetsCombiner @Inject constructor() : BaseUrlSetsCombiner {
    override fun combine(incomingBaseUrls: BaseUrls, defaultBaseUrls: BaseUrls): BaseUrls =
        BaseUrls(
            web = incomingBaseUrls.web.takeIfNotBlankOrSetDefault(defaultBaseUrls.web),
            eDocWeb = incomingBaseUrls.eDocWeb.takeIfNotBlankOrSetDefault(defaultBaseUrls.eDocWeb),
            express = incomingBaseUrls.express.takeIfNotBlankOrSetDefault(defaultBaseUrls.express),
            mobileTrade = incomingBaseUrls.mobileTrade.takeIfNotBlankOrSetDefault(defaultBaseUrls.mobileTrade),
            mobileETrade = incomingBaseUrls.mobileETrade.takeIfNotBlankOrSetDefault(defaultBaseUrls.mobileETrade),
            marketOnDemand = incomingBaseUrls.marketOnDemand.takeIfNotBlankOrSetDefault(defaultBaseUrls.marketOnDemand),
            api = incomingBaseUrls.api.takeIfNotBlankOrSetDefault(defaultBaseUrls.api),
            level1StreamerUrl = incomingBaseUrls.level1StreamerUrl.takeIfNotBlankOrSetDefault(defaultBaseUrls.level1StreamerUrl),
            level2StreamerUrl = incomingBaseUrls.level2StreamerUrl.takeIfNotBlankOrSetDefault(defaultBaseUrls.level2StreamerUrl),
            environmentChange = incomingBaseUrls.environmentChange.takeIfNotBlankOrSetDefault(defaultBaseUrls.environmentChange)
        )
}

private fun String.takeIfNotBlankOrSetDefault(default: String): String = this.takeIf { it.isNotBlank() } ?: default
