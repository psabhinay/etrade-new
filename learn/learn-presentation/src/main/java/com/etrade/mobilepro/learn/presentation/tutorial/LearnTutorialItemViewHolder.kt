package com.etrade.mobilepro.learn.presentation.tutorial

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.learn.presentation.R

class LearnTutorialItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val tutorial_icon = itemView.findViewById<ImageView>(R.id.tutorial_icon)
    private val tutorial_text = itemView.findViewById<TextView>(R.id.tutorial_text)
    private val tutorial_layout = itemView.findViewById<View>(R.id.tutorial_layout)

    fun bind(tutorialItem: LearnTutorialItem, tutorialWalkThroughRouting: (LearnTutorialItem) -> Unit) {
        tutorial_icon.setImageResource(tutorialItem.tutorialIconId)
        tutorial_text.text = tutorialItem.tutorialText
        tutorial_layout.setOnClickListener {
            tutorialWalkThroughRouting(tutorialItem)
        }
    }
}
