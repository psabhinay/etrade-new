package com.etrade.mobilepro.marketsapi

import com.etrade.mobilepro.instrument.InstrumentType
import java.math.BigDecimal

data class Index(
    val ticker: String,
    val displayName: String,
    val price: BigDecimal,
    val volume: BigDecimal,
    val change: BigDecimal,
    val percentChange: BigDecimal,
    val instrumentType: InstrumentType,
    val timeStamp: String
)
