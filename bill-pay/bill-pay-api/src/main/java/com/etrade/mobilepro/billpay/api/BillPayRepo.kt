package com.etrade.mobilepro.billpay.api

import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.common.result.ETResult
import org.threeten.bp.Instant

interface BillPayRepo {

    suspend fun cancelPayment(sessionId: String, paymentId: String): ETResult<Unit>

    suspend fun getAccounts(): ETResult<List<AccountDetails>>

    suspend fun getPayeesList(sessionId: String): ETResult<List<Payee>>

    suspend fun getPaymentList(sessionId: String, status: PaymentStatus, numOfDays: Int, startDate: Instant): ETResult<List<Payment>>

    suspend fun getBillPaySessionId(): ETResult<String>

    suspend fun getStateList(): ETResult<StatesOfUS>

    suspend fun savePayee(sessionId: String, payee: Payee): ETResult<SavePayeeResult>

    suspend fun deletePayee(sessionId: String, payeeId: String): ETResult<Boolean>

    suspend fun schedulePayment(sessionId: String, request: PaymentRequest): ETResult<PaymentResult>

    suspend fun searchPayee(sessionId: String, name: String): ETResult<SearchPayeeResult>
}
