package com.etrade.mobilepro.util

/**
 * Implementation of this interface format values to a human readable format.
 */
interface ValueFormatter<T> {

    /**
     * Formats a [value].
     *
     * @param value The value to format.
     *
     * @return Human readable text that represents the [value].
     */
    fun format(value: T): CharSequence
}
