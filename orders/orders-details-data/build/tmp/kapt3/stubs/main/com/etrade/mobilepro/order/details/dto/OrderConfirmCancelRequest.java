package com.etrade.mobilepro.order.details.dto;

import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0017\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b5\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\u00fb\u0001\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0003\u0010\b\u001a\u00020\u0003\u0012\b\b\u0003\u0010\t\u001a\u00020\u0003\u0012\b\b\u0003\u0010\n\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u000b\u001a\u00020\u0003\u0012\b\b\u0003\u0010\f\u001a\u00020\u0003\u0012\b\b\u0003\u0010\r\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u000e\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u000f\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0010\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0011\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0012\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0013\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0014\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0015\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0016\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0017\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0018\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0019\u001a\u00020\u0003\u0012\u000e\b\u0001\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b\u00a2\u0006\u0002\u0010\u001dJ\t\u00108\u001a\u00020\u0003H\u00c6\u0003J\t\u00109\u001a\u00020\u0003H\u00c6\u0003J\t\u0010:\u001a\u00020\u0003H\u00c6\u0003J\t\u0010;\u001a\u00020\u0003H\u00c6\u0003J\t\u0010<\u001a\u00020\u0003H\u00c6\u0003J\t\u0010=\u001a\u00020\u0003H\u00c6\u0003J\t\u0010>\u001a\u00020\u0003H\u00c6\u0003J\t\u0010?\u001a\u00020\u0003H\u00c6\u0003J\t\u0010@\u001a\u00020\u0003H\u00c6\u0003J\t\u0010A\u001a\u00020\u0003H\u00c6\u0003J\t\u0010B\u001a\u00020\u0003H\u00c6\u0003J\t\u0010C\u001a\u00020\u0003H\u00c6\u0003J\t\u0010D\u001a\u00020\u0003H\u00c6\u0003J\t\u0010E\u001a\u00020\u0003H\u00c6\u0003J\t\u0010F\u001a\u00020\u0003H\u00c6\u0003J\t\u0010G\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010H\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001bH\u00c6\u0003J\t\u0010I\u001a\u00020\u0003H\u00c6\u0003J\t\u0010J\u001a\u00020\u0003H\u00c6\u0003J\t\u0010K\u001a\u00020\u0003H\u00c6\u0003J\t\u0010L\u001a\u00020\u0003H\u00c6\u0003J\t\u0010M\u001a\u00020\u0003H\u00c6\u0003J\t\u0010N\u001a\u00020\u0003H\u00c6\u0003J\t\u0010O\u001a\u00020\u0003H\u00c6\u0003J\u00ff\u0001\u0010P\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00032\b\b\u0003\u0010\u0006\u001a\u00020\u00032\b\b\u0003\u0010\u0007\u001a\u00020\u00032\b\b\u0003\u0010\b\u001a\u00020\u00032\b\b\u0003\u0010\t\u001a\u00020\u00032\b\b\u0003\u0010\n\u001a\u00020\u00032\b\b\u0003\u0010\u000b\u001a\u00020\u00032\b\b\u0003\u0010\f\u001a\u00020\u00032\b\b\u0003\u0010\r\u001a\u00020\u00032\b\b\u0003\u0010\u000e\u001a\u00020\u00032\b\b\u0003\u0010\u000f\u001a\u00020\u00032\b\b\u0003\u0010\u0010\u001a\u00020\u00032\b\b\u0003\u0010\u0011\u001a\u00020\u00032\b\b\u0003\u0010\u0012\u001a\u00020\u00032\b\b\u0003\u0010\u0013\u001a\u00020\u00032\b\b\u0003\u0010\u0014\u001a\u00020\u00032\b\b\u0003\u0010\u0015\u001a\u00020\u00032\b\b\u0003\u0010\u0016\u001a\u00020\u00032\b\b\u0003\u0010\u0017\u001a\u00020\u00032\b\b\u0003\u0010\u0018\u001a\u00020\u00032\b\b\u0003\u0010\u0019\u001a\u00020\u00032\u000e\b\u0003\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001bH\u00c6\u0001J\u0013\u0010Q\u001a\u00020R2\b\u0010S\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010T\u001a\u00020UH\u00d6\u0001J\t\u0010V\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u001fR\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\u001fR\u0011\u0010\u0019\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u001fR\u0011\u0010\u0016\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001fR\u0011\u0010\u0017\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u001fR\u0011\u0010\u0013\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u001fR\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\'R\u0011\u0010\u0011\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001fR\u0011\u0010\u0010\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001fR\u0011\u0010\u0012\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001fR\u0011\u0010\u0015\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b+\u0010\u001fR\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b,\u0010\u001fR\u0011\u0010\f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010\u001fR\u0011\u0010\u0014\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010\u001fR\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b/\u0010\u001fR\u0011\u0010\u0018\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b0\u0010\u001fR\u0011\u0010\b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u0010\u001fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b2\u0010\u001fR\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b3\u0010\u001fR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b4\u0010\u001fR\u0011\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b5\u0010\u001fR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b6\u0010\u001fR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b7\u0010\u001f\u00a8\u0006W"}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/OrderConfirmCancelRequest;", "", "previewId", "", "underlier", "strategyType", "requestType", "originalOrderNo", "overrideRestrictedCd", "priceType", "allOrNone", "accountId", "orderType", "orderTerm", "acctDesc", "stopPrice", "optionLevelCd", "limitPrice", "orderNumber", "destMktCode", "origSysCodeReq", "orderSource", "dayTradingFlag", "dayTradingVer", "overnightIndicatorFlag", "clearanceCode", "legRequestList", "", "Lcom/etrade/mobilepro/trade/data/common/LegDetailsCommonDto;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "getAccountId", "()Ljava/lang/String;", "getAcctDesc", "getAllOrNone", "getClearanceCode", "getDayTradingFlag", "getDayTradingVer", "getDestMktCode", "getLegRequestList", "()Ljava/util/List;", "getLimitPrice", "getOptionLevelCd", "getOrderNumber", "getOrderSource", "getOrderTerm", "getOrderType", "getOrigSysCodeReq", "getOriginalOrderNo", "getOvernightIndicatorFlag", "getOverrideRestrictedCd", "getPreviewId", "getPriceType", "getRequestType", "getStopPrice", "getStrategyType", "getUnderlier", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "orders-details-data"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class OrderConfirmCancelRequest {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String previewId = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String underlier = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String strategyType = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String requestType = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String originalOrderNo = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String overrideRestrictedCd = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String priceType = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String allOrNone = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String accountId = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String orderType = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String orderTerm = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String acctDesc = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String stopPrice = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String optionLevelCd = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String limitPrice = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String orderNumber = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String destMktCode = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String origSysCodeReq = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String orderSource = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String dayTradingFlag = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String dayTradingVer = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String overnightIndicatorFlag = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String clearanceCode = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto> legRequestList = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequest copy(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "previewId")
    java.lang.String previewId, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "underlier")
    java.lang.String underlier, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "strategyType")
    java.lang.String strategyType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "requestType")
    java.lang.String requestType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "originalOrderNo")
    java.lang.String originalOrderNo, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "overrideRestrictedCd")
    java.lang.String overrideRestrictedCd, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "priceType")
    java.lang.String priceType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "allOrNone")
    java.lang.String allOrNone, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "accountId")
    java.lang.String accountId, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderType")
    java.lang.String orderType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderTerm")
    java.lang.String orderTerm, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "acctDesc")
    java.lang.String acctDesc, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "stopPrice")
    java.lang.String stopPrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optionLevelCd")
    java.lang.String optionLevelCd, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "limitPrice")
    java.lang.String limitPrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderNumber")
    java.lang.String orderNumber, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "destMktCode")
    java.lang.String destMktCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "origSysCodeReq")
    java.lang.String origSysCodeReq, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderSource")
    java.lang.String orderSource, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "dayTradingFlag")
    java.lang.String dayTradingFlag, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "dayTradingVer")
    java.lang.String dayTradingVer, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "overnightIndicatorFlag")
    java.lang.String overnightIndicatorFlag, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "clearancecode")
    java.lang.String clearanceCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "legRequestList")
    java.util.List<? extends com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto> legRequestList) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public OrderConfirmCancelRequest(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "previewId")
    java.lang.String previewId, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "underlier")
    java.lang.String underlier, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "strategyType")
    java.lang.String strategyType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "requestType")
    java.lang.String requestType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "originalOrderNo")
    java.lang.String originalOrderNo, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "overrideRestrictedCd")
    java.lang.String overrideRestrictedCd, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "priceType")
    java.lang.String priceType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "allOrNone")
    java.lang.String allOrNone, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "accountId")
    java.lang.String accountId, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderType")
    java.lang.String orderType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderTerm")
    java.lang.String orderTerm, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "acctDesc")
    java.lang.String acctDesc, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "stopPrice")
    java.lang.String stopPrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optionLevelCd")
    java.lang.String optionLevelCd, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "limitPrice")
    java.lang.String limitPrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderNumber")
    java.lang.String orderNumber, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "destMktCode")
    java.lang.String destMktCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "origSysCodeReq")
    java.lang.String origSysCodeReq, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderSource")
    java.lang.String orderSource, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "dayTradingFlag")
    java.lang.String dayTradingFlag, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "dayTradingVer")
    java.lang.String dayTradingVer, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "overnightIndicatorFlag")
    java.lang.String overnightIndicatorFlag, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "clearancecode")
    java.lang.String clearanceCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "legRequestList")
    java.util.List<? extends com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto> legRequestList) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPreviewId() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUnderlier() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStrategyType() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getRequestType() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOriginalOrderNo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOverrideRestrictedCd() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPriceType() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAllOrNone() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAccountId() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component10() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOrderType() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOrderTerm() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component12() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAcctDesc() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component13() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStopPrice() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component14() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOptionLevelCd() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component15() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLimitPrice() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component16() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOrderNumber() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component17() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDestMktCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component18() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOrigSysCodeReq() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component19() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOrderSource() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component20() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDayTradingFlag() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component21() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDayTradingVer() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component22() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOvernightIndicatorFlag() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component23() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getClearanceCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto> component24() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto> getLegRequestList() {
        return null;
    }
}