package com.etrade.mobilepro.mod

import com.etrade.mobilepro.mod.api.CompanyOverview
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CompanyAddress(
    val street1: String?,
    val street2: String?,
    val street3: String?,
    val city: String?,
    val state: String?,
    val postalCode: String?
)

@JsonClass(generateAdapter = true)
data class CompanyOverviewDto(
    @Json(name = "Overview")
    override val overview: String?,
    @Json(name = "CompanyAddress")
    val companyAddress: CompanyAddress?,
    @Json(name = "WebAddress")
    override val webAddress: String?,
    @Json(name = "Sector")
    override val sector: String?,
    @Json(name = "SectorCode")
    val sectorCode: String?,
    @Json(name = "Industry")
    override val industry: String?,
    @Json(name = "IndustryCode")
    val industryCode: String?
) : CompanyOverview
