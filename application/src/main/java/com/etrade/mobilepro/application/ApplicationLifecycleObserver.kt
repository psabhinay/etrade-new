package com.etrade.mobilepro.application

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.etrade.mobilepro.login.usecase.DisableBiometricLoginIfRequiredUseCase
import com.etrade.mobilepro.user.session.manager.SessionTimeoutManager
import com.etrade.mobilepro.util.executeBlocking
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApplicationLifecycleObserver @Inject constructor(
    private val disableBiometricLoginIfRequired: DisableBiometricLoginIfRequiredUseCase,
    private val sessionTimeoutManager: SessionTimeoutManager
) : ApplicationLifecycle, Application.ActivityLifecycleCallbacks {

    override fun onUserInteraction(activity: Activity) {
        sessionTimeoutManager.touchSession()
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
    }

    override fun onActivityPaused(activity: Activity) = Unit

    override fun onActivityResumed(activity: Activity) {
        sessionTimeoutManager.touchSession()
        disableBiometricLoginIfRequired.executeBlocking()
    }

    override fun onActivityStarted(activity: Activity) {
    }

    override fun onActivityStopped(activity: Activity) {
    }

    override fun onActivitySaveInstanceState(activity: Activity, instanceState: Bundle) {
    }

    override fun onActivityDestroyed(activity: Activity) {
    }
}
