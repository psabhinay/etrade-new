package com.etrade.mobilepro.chat.api.setup

import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatAccountModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatCustomerAccountModel

class ChatCustomerMaps(customerAccounts: List<ChatCustomerAccountModel>) {
    val customerAccountsMap: HashMap<String, ChatCustomerAccountModel>

    val customerUserNameMap: HashMap<String, ChatAccountModel>

    val customerUserNameWithAccountsMap: HashMap<String, List<String>>

    init {
        val mapAccountToUserName = HashMap<String, List<String>>()
        val mapAccounts = HashMap<String, ChatCustomerAccountModel>()
        val mapUsernames = HashMap<String, ChatAccountModel>()
        val size = customerAccounts.size
        for (i in 0 until size) {
            val chatAccountListObject = customerAccounts[i]
            val accountHolderList = chatAccountListObject.accounts
            val accHolderListSize = accountHolderList.size
            val listUserName = ArrayList<String>()
            for (j in 0 until accHolderListSize) {
                val accountHolderList1 = accountHolderList[j]
                val name = accountHolderList1.firstname + " " + accountHolderList1.lastname
                listUserName.add(name)
                mapUsernames[name] = accountHolderList1
            }
            val accNum = chatAccountListObject.accNum
            mapAccounts[accNum] = chatAccountListObject
            mapAccountToUserName[accNum] = listUserName
        }

        this.customerAccountsMap = mapAccounts
        this.customerUserNameMap = mapUsernames
        this.customerUserNameWithAccountsMap = mapAccountToUserName
    }
}
