package com.etrade.mobilepro.mod.auth.api

import androidx.annotation.WorkerThread
import java.io.IOException

interface AuthTokenProvider {
    class ModAuthTokenException(message: String) : Exception(message)

    /**
     * Synchronous call to obtain a temporary auth token for market-on-demand service calls.
     * The symbol does not restrict the token, you can use the resulting token for looking up
     * any MOD data for any symbol.
     *
     * @throws ModAuthTokenException Thrown if the response could not be parsed or token could
     * not be extracted for various reasons. Error message not safe for users.
     */
    @Throws(IOException::class, ModAuthTokenException::class)
    @WorkerThread
    fun getModAuthToken(ticker: String = "AAPL"): String
}
