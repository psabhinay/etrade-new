package com.etrade.mobilepro.util.debuglog

class DebugLog(throwable: Throwable, additionalInfo: String = "") : Exception(throwable) {
    override val message: String = super.message + " $additionalInfo"
}
