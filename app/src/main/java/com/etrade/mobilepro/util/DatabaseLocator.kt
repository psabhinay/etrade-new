package com.etrade.mobilepro.util

import java.io.File

/**
 * Resolves paths for databases.
 */
interface DatabaseLocator {

    /**
     * Gets absolute path by database [name]
     *
     * @param name name of a database
     *
     * @return an absolute path to the given database
     */
    fun getDatabasePath(name: String): File
}
