package com.etrade.mobilepro.streaming.lightstreamer.level2

import com.etrade.eo.streaming.SubscriptionMode
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.Level2DataEvent
import com.etrade.mobilepro.streaming.api.StreamIdFactory
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

internal const val ASK_SIDE = "ASK"
internal const val BID_SIDE = "BID"
private const val THROTTLE_MS = 1000L
internal const val PULSE_RATE_MS = 5000L

/**
 * Streaming provider for level 2 quotes.
 *
 * @param streamIdFactory The factory to create stream ids for level 2 quotes.
 * @param subscriptionManagerHelper The helper for a subscription manager.
 */
class Level2StreamingQuoteProvider @Inject constructor(
    private val streamIdFactory: StreamIdFactory<Level2BookType>,
    private val subscriptionManagerHelper: SubscriptionManagerHelper<Level2DataEvent>,
    private val streamFactoryParams: Level2StreamIdFactory.Parameters,
    private val pulseBeatScheduler: Scheduler? = null
) : StreamingQuoteProvider<String, Level2Data, Level2BookType> {

    override fun getQuote(symbol: String, instrumentType: InstrumentType, fields: Set<String>, payload: Level2BookType?): Observable<Level2Data> {
        /*
        The method subscribes to L2 event source (subscriptionManagerHelper).

        Incoming flow may be intense and for that .throttleLatest(..) is used.
        Incoming flow may be slow and we would want to show some partial info on the screen before
        we collect all required items of {streamFactoryParams.bookCount * 2} number,
        for that we use pulseBeat Observable which "flushes" our buffer down the chain.

        The number of events we want to collect is {streamFactoryParams.bookCount * 2} and
        multiplier = 2 is there because we show two columns, with BID and ASK,
        and one row there under the hood is made of two events. In COMMAND mode this was handled on
        the backend side with the usage of END OF BOOK command

        Scan Rx operator has following logic:
        It accumulates incoming data events that contain BID or ASK data. If number of accumulated
        events becomes equal to the threshold they are allowed to pass down the chain
        and the accumulator clears itself on the next event.
        Once in PULSE_RATE_MS time interval Scan receives a special "PulseBeat" Leve2DataEvent
        implementation, which results in flag update which allows to pass accumulated data down
        the chain

        Events snapshotting is required to allow correct functioning of throttleLatest operator
        (otherwise events will be rewritten, as their references would be available in the Scan
        operator)
         */
        val pulseBeat = Observable
            .interval(PULSE_RATE_MS, TimeUnit.MILLISECONDS, pulseBeatScheduler ?: Schedulers.computation())
            .map { PulseDataEvent() }
        val dataSource = subscriptionManagerHelper
            .subscribeTo(streamIdFactory.createStreamId(symbol, instrumentType, payload), fields, SubscriptionMode.MERGE)
        return dataSource
            .mergeWith(pulseBeat)
            .scan(DataEventAccumulator()) { accumulator, newItem ->
                accumulator.gotPulseBeat = false
                if (newItem.side == BID_SIDE || newItem.side == ASK_SIDE) {
                    if (accumulator.events.size == streamFactoryParams.bookCount * 2) {
                        accumulator.events.clear()
                    }
                    accumulator.events.add(newItem)
                }
                if (newItem is PulseDataEvent && accumulator.events.isNotEmpty()) {
                    accumulator.gotPulseBeat = true
                }
                accumulator
            }
            .filter { it.gotPulseBeat || it.events.size == streamFactoryParams.bookCount * 2 }
            .map { it.snapshotEvents() }
            .throttleLatest(THROTTLE_MS, TimeUnit.MILLISECONDS)
            .map(::mapEvents)
    }

    private fun mapEvents(events: List<Level2DataEvent>): Level2Data {
        val bidList = mutableListOf<Level2DataEvent>()
        val askList = mutableListOf<Level2DataEvent>()
        for (level2DataEvent in events) {
            when (level2DataEvent.side) {
                BID_SIDE -> bidList.add(level2DataEvent)
                ASK_SIDE -> askList.add(level2DataEvent)
            }
        }
        return Level2Data(bidList, askList)
    }

    private class PulseDataEvent : Level2DataEvent {
        override val key: String = ""
        override val side: String = ""
        override val price: Double = 0.0
        override val size: Long = 0
        override val consolidatedSize: Long = 0
        override val marketMakerId: String = ""
        override val time: Long = 0
        override val consolidateCount: Long = 0

        override fun copy(): Level2DataEvent = this
    }

    private class DataEventAccumulator {
        val events = mutableListOf<Level2DataEvent>()
        var gotPulseBeat = false

        fun snapshotEvents(): List<Level2DataEvent> = events.map { it.copy() }
    }
}
