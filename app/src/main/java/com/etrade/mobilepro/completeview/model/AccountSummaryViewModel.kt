package com.etrade.mobilepro.completeview.model

import android.content.res.Resources
import androidx.annotation.LayoutRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobile.accounts.dto.InstitutionType
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.Diffable
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.inboxmessages.dynamicui.api.lowcash.LowCashPrompt
import com.etrade.mobilepro.livedata.SingleSourceLiveDataDelegate
import com.etrade.mobilepro.navigation.appendUuidQueryParam
import com.etrade.mobilepro.navigation.getNavDirections
import com.etrade.mobilepro.streaming.api.StreamingText
import com.etrade.mobilepro.util.ClickAction
import com.etrade.mobilepro.util.android.CallToAction

class AccountSummaryViewModel(
    override val showName: Boolean = true,
    override val clickAction: ClickAction? = null,
    override val accountSourceLabel: String?,
    accountSummaryData: AccountSummaryData,
    initialCallToAction: List<CallToAction>?,
    additionSectionBuilder: (Account?) -> List<AccountSummarySection>,
    @LayoutRes layoutRes: Int,
    private val daoProvider: CachedDaoProvider,
    private val resources: Resources
) : DynamicViewModel(layoutRes), AccountSummary, Diffable {

    lateinit var initialDetailValueSource: LiveData<StreamingText>

    private val detailValueDelegate by lazy {
        SingleSourceLiveDataDelegate(initialDetailValueSource)
    }

    val account: Account?
        get() = uuid?.let {
            daoProvider.getAccountDao().getAccountWithUuid(it)
        }

    override val uuid: String? = accountSummaryData.accountUuid

    override val name: String? = accountSummaryData.accountName

    override var lowCashPrompt: LowCashPrompt? = accountSummaryData.cashPrompt

    override val additionalSections: List<AccountSummarySection> by lazy {
        additionSectionBuilder(account)
    }

    override val showFdicLogo: Boolean
        get() = account?.institutionType == InstitutionType.TELEBANK

    private val isBankAccount: Boolean
        get() = account?.isBankAccount() ?: false

    override val callToAction: LiveData<List<CallToAction>?>
        get() = _callToAction

    override val detailLabel: LiveData<String>
        get() = _detailLabel

    override val detailValue: LiveData<StreamingText>
        get() = detailValueDelegate.value

    override val variableId: Int = BR.obj

    override fun onAugmentationClicked() {
        lowCashPrompt?.deeplink?.let { clickEvents.value = CtaEvent.UriEvent(it) }
    }

    private val _callToAction = MutableLiveData(initialCallToAction)

    private val _detailLabel = MutableLiveData(accountSummaryData.label)

    init {
        daoProvider.open()
    }

    override fun onCleared() {
        super.onCleared()
        daoProvider.close()
    }

    override fun onCtaClicked(clickAction: ClickAction?) {
        val ctaEvent = clickAction?.appUrl?.let {
            getNavDirections(appendUuidQueryParam(it, uuid), isBankAccount)
        }
            ?: clickAction?.webViewUrl?.let { CtaEvent.UriEvent(it) }
            ?: clickAction?.webUrl?.let { CtaEvent.UriEvent(it) }

        ctaEvent?.let { clickEvents.postValue(it) }
    }

    override fun onInfoClicked() {
        clickEvents.postValue(
            CtaEvent.InfoDialogEvent(
                resources.getString(R.string.complete_view_account_summary_info_title),
                resources.getString(R.string.complete_view_account_summary_info_content)
            )
        )
    }

    fun updateDetails(label: String, valueSource: LiveData<StreamingText>) {
        _detailLabel.value = label
        detailValueDelegate.updateSource(valueSource)
    }

    fun updateAdditionalSections(sections: List<AccountSummarySection>) {
        sections.forEachIndexed { index, summary ->
            additionalSections[index].update(summary.title.value, summary.value)
        }
    }

    fun updateActions(actions: List<CallToAction>?) {
        _callToAction.value = actions
    }

    fun updatePrompt(lowCashPrompt: LowCashPrompt?) {
        this.lowCashPrompt = lowCashPrompt
    }

    override fun isSameItem(other: Any): Boolean =
        other.javaClass == javaClass && uuid == (other as AccountSummaryViewModel).uuid

    override fun hasSameContents(other: Any): Boolean = false
}

data class AccountSummaryData(
    val accountUuid: String?,
    val accountName: String?,
    val label: String,
    val cashPrompt: LowCashPrompt?
)
