package com.etrade.mobilepro.compose.etdialogs.info

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.window.DialogProperties
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.FontSizes
import com.etrade.mobilepro.common.compose.theme.etColors
import com.etrade.mobilepro.compose.etdialogs.R
import com.etrade.mobilepro.compose.etdialogs.elements.DialogButtonRow
import com.etrade.mobilepro.compose.etdialogs.elements.DialogContainer

@Composable
fun InfoDialog(state: InfoDialogManager.ViewState, dialogProperties: DialogProperties = remember { DialogProperties(dismissOnClickOutside = false) }) {
    when (state.status) {
        InfoDialogManager.ViewState.Status.Gone -> { /* no op */ }
        InfoDialogManager.ViewState.Status.Visible -> {
            DialogContainer(properties = dialogProperties, onDismiss = state.settings.onDismiss) {
                Column {
                    Text(
                        text = state.settings.title,
                        style = MaterialTheme.typography.h6.copy(
                            fontSize = FontSizes.Size20sp,
                            fontWeight = FontWeight.W500,
                            color = colorResource(R.color.black)
                        ),
                        modifier = Modifier.padding(Dimens.Size16dp)
                    )
                    Text(
                        text = state.settings.message,
                        style = MaterialTheme.typography.body1.copy(
                            fontSize = FontSizes.Size16sp,
                            fontWeight = FontWeight.Normal,
                            lineHeight = FontSizes.Size24sp,
                            color = MaterialTheme.etColors.infoDialogMessage
                        ),
                        modifier = Modifier.padding(horizontal = Dimens.Size16dp)
                    )
                    Spacer(modifier = Modifier.height(Dimens.Size8dp))
                    DialogButtonRow(state, state.settings.buttonRow)
                }
            }
        }
    }
}
