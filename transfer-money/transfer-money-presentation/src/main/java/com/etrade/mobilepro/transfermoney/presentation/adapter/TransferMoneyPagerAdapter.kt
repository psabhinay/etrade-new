package com.etrade.mobilepro.transfermoney.presentation.adapter

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.transfermoney.presentation.R
import com.etrade.mobilepro.transfermoney.presentation.activity.TransferActivityFragment
import com.etrade.mobilepro.transfermoney.presentation.transfer.TransferFragment
import com.etrade.mobilepro.util.android.adapter.DefaultFragmentPagerAdapter
import com.etrade.mobilepro.util.android.adapter.DefaultFragmentPagerAdapter.FragmentPagerItem

private val transferFragments = listOf(
    FragmentPagerItem(TransferFragment::class.java, R.string.transfer_transfer_title),
    FragmentPagerItem(TransferActivityFragment::class.java, R.string.transfer_activity_title)
)

internal class TransferMoneyPagerAdapter(
    fragment: Fragment
) : DefaultFragmentPagerAdapter(fragment, transferFragments)

internal fun getActivityTabIndex() =
    transferFragments.indexOfFirst { it.fragmentClass == TransferActivityFragment::class.java }
