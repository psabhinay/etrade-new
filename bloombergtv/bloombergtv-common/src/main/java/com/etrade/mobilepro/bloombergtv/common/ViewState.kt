package com.etrade.mobilepro.bloombergtv.common

sealed class ViewState {
    object Loading : ViewState()
    data class Success(val url: String) : ViewState()
    data class Failure(val error: String) : ViewState()
}
