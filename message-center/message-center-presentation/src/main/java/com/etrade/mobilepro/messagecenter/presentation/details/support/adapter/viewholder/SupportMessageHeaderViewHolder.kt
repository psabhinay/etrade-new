package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.messagecenter.presentation.R
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterItemSupportMessageHeaderBinding
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.Header
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.binding.resources

internal class SupportMessageHeaderViewHolder(private val binding: MessageCenterItemSupportMessageHeaderBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bindTo(item: Header) {
        val resources = binding.resources
        binding.sectionView.setContent(
            listOf(
                createSummarySection(
                    label = resources.getString(R.string.message_center_ref_number),
                    value = item.referenceNumber
                ),
                createSummarySection(
                    label = resources.getString(R.string.message_center_status),
                    value = item.status
                ),
                createSummarySection(
                    label = resources.getString(R.string.title_account),
                    value = item.accountName
                ),
                createSummarySection(
                    label = resources.getString(R.string.message_center_subject),
                    value = item.subject
                ),
                createSummarySection(
                    label = resources.getString(R.string.message_center_topic),
                    value = item.topic
                )
            )
        )
    }
}
