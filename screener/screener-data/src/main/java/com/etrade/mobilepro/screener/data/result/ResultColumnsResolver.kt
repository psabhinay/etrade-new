package com.etrade.mobilepro.screener.data.result

import com.etrade.mobilepro.screener.api.ScreenerFilter

interface ResultColumnsResolver {
    fun getColumns(filter: ScreenerFilter): Set<ResultColumn>
}
