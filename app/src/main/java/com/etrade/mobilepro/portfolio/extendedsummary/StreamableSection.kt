package com.etrade.mobilepro.portfolio.extendedsummary

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.streaming.api.StreamingText
import com.etrade.mobilepro.util.formatAccessibilityDescription
import com.etrade.mobilepro.util.formatCurrencyAmount

class StreamableSection(val label: String, val streamingText: LiveData<StreamingText>) {
    val valueContentDescription: LiveData<String?>
        get() = streamingText.map {
            it.text.formatCurrencyAmount()
        }

    val labelContentDescription: String
        get() = label.formatAccessibilityDescription()
}
