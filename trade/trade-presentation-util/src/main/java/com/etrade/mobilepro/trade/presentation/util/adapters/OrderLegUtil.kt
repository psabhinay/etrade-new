package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import com.etrade.mobilepro.instrument.displaySymbol
import com.etrade.mobilepro.trade.api.OrderLeg
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.presentation.util.formatLegDetails
import com.etrade.mobilepro.util.formatters.injectAMLabel

internal fun createLabel(
    resources: Resources,
    orderLegs: Iterable<OrderLeg>,
    amLegDetails: List<TradeLeg>? = emptyList()
): CharSequence {
    return orderLegs.joinToString(separator = "\n") { orderLeg ->
        val isAMOption = amLegDetails?.firstOrNull {
            it.symbol == orderLeg.symbol
        }?.isAMOption ?: false
        formatOrderLeg(resources, orderLeg, isAMOption)
    }
}

private fun formatOrderLeg(resources: Resources, leg: OrderLeg, isAMOption: Boolean): CharSequence {
    return resources
        .formatLegDetails(leg.action, leg.displaySymbol, leg.quantity?.toPlainString())
        .injectAMLabel(isAMOption)
}
