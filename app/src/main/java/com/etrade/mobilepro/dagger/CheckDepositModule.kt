package com.etrade.mobilepro.dagger

import android.content.Context
import android.text.format.Formatter
import android.util.Base64
import com.etrade.mobilepro.checkdeposit.activity.DepositActivityRepository
import com.etrade.mobilepro.checkdeposit.activity.service.DepositActivityService
import com.etrade.mobilepro.checkdeposit.deposit.service.RemoteCheckDepositService
import com.etrade.mobilepro.checkdeposit.usecases.AcceptServiceAgreementUseCase
import com.etrade.mobilepro.checkdeposit.usecases.AcceptServiceAgreementUseCaseImpl
import com.etrade.mobilepro.checkdeposit.usecases.accounts.LoadEligibleAccountsUseCase
import com.etrade.mobilepro.checkdeposit.usecases.api.DepositImageUseCase
import com.etrade.mobilepro.checkdeposit.usecases.api.GetCheckDepositAccountsUseCase
import com.etrade.mobilepro.checkdeposit.usecases.api.UploadCheckImageUseCase
import com.etrade.mobilepro.checkdeposit.usecases.deposit.DepositImageUseCaseImpl
import com.etrade.mobilepro.checkdeposit.usecases.uploadcheckimage.UploadTransformedCheckImageUseCase
import com.etrade.mobilepro.checkdeposit.usecases.validation.ValidateDepositBusinessRulesUseCase
import com.etrade.mobilepro.checkdeposit.usecases.validation.ValidateDepositBusinessRulesUseCaseImpl
import com.etrade.mobilepro.checkdeposit.util.prepareCheckImageForUpload
import com.etrade.mobilepro.common.di.Api
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.depositactivity.repository.DefaultDepositActivityRepository
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.session.api.UserConsumerKeyInterface
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
abstract class CheckDepositModule {

    companion object {

        @Provides
        fun provideUploadCheckImageUseCase(context: Context, checkDepositService: RemoteCheckDepositService): UploadCheckImageUseCase {
            val encodeToBase64 = { byteArray: ByteArray -> Base64.encodeToString(byteArray, Base64.DEFAULT) }
            val prettyPrintByteSize = { size: Int -> Formatter.formatFileSize(context, size.toLong()) }
            val checkTransform = { bytes: ByteArray -> prepareCheckImageForUpload(bytes) }
            return UploadTransformedCheckImageUseCase(checkDepositService, encodeToBase64, prettyPrintByteSize, checkTransform)
        }

        @Provides
        @Singleton
        internal fun provideCheckDepositService(
            @MobileTrade retrofit: Retrofit
        ): RemoteCheckDepositService = retrofit.create(RemoteCheckDepositService::class.java)

        @Provides
        @Singleton
        internal fun provideCheckDepositActivityService(
            @Api retrofit: Retrofit
        ): DepositActivityService = retrofit.create(DepositActivityService::class.java)

        @Provides
        @Singleton
        internal fun provideLoadEligibleAccountsUseCase(
            remoteCheckDepositService: RemoteCheckDepositService
        ): GetCheckDepositAccountsUseCase {
            return LoadEligibleAccountsUseCase(remoteCheckDepositService)
        }

        @Provides
        @Singleton
        internal fun provideAcceptServiceAgreementUseCase(
            remoteCheckDepositService: RemoteCheckDepositService
        ): AcceptServiceAgreementUseCase {
            return AcceptServiceAgreementUseCaseImpl(remoteCheckDepositService)
        }

        @Provides
        @Singleton
        internal fun provideDepositActivityRepository(
            depositActivityService: DepositActivityService,
            user: UserConsumerKeyInterface
        ): DepositActivityRepository {
            return DefaultDepositActivityRepository(depositActivityService, user::getConsumerKey)
        }

        @Provides
        @Singleton
        internal fun provideValidateDepositBusinessRulesUseCase(
            remoteCheckDepositService: RemoteCheckDepositService
        ): ValidateDepositBusinessRulesUseCase =
            ValidateDepositBusinessRulesUseCaseImpl(remoteCheckDepositService)

        @Provides
        internal fun provideDepositImageUseCase(user: User, remoteCheckDepositService: RemoteCheckDepositService): DepositImageUseCase =
            DepositImageUseCaseImpl(user.getUserId().toString(), remoteCheckDepositService)
    }
}
