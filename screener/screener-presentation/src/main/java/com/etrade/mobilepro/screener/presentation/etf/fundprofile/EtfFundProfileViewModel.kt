package com.etrade.mobilepro.screener.presentation.etf.fundprofile

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.screener.api.EtfFundGroup
import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.FundFamily
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.datasource.EtfFundDataSource
import com.etrade.mobilepro.screener.api.filter.etf.EtfFundProfileFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Etf
import com.etrade.mobilepro.screener.api.repo.FundCategoriesRepo
import com.etrade.mobilepro.screener.api.toggle
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import com.etrade.mobilepro.screener.presentation.common.SelectionFlowHandler
import javax.inject.Inject

class EtfFundProfileViewModel @Inject constructor(
    private val fundDataSource: EtfFundDataSource,
    private val fundCategoriesRepo: FundCategoriesRepo,
    private val fundGroupSelectionFlowHandler: SelectionFlowHandler<EtfFundGroup>,
    aggregator: ScreenerFilterAggregator,
    @Etf screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<EtfFundProfileFilter, EtfFundProfileViewModel.ViewState>(
    aggregator,
    ScreenerType.Etf.FundProfile,
    screenerInteractor
),
    SelectionFlowHandler<EtfFundGroup> by fundGroupSelectionFlowHandler {
    internal var prevCategorySelection: Set<FundCategory>? = null

    override fun provideInitialState(initialFilter: EtfFundProfileFilter): ViewState {
        if (initialFilter.isEmpty()) {
            preloadItems(fundDataSource.getAllFundGroupsItem())
        }
        return ViewState(filter = initialFilter)
    }

    override fun onCleared() {
        clearSubscriptions()
        super.onCleared()
    }

    internal val fundFamilyData: LiveData<List<DropDownMultiSelector.Selectable<FundFamily>>> = Transformations.map(viewState) {
        fundDataSource.getFundFamilies().map { fundFamily ->
            DropDownMultiSelector.Selectable(
                title = fundFamily.title,
                value = fundFamily,
                selected = it.filter.fundFamily.id == fundFamily.id
            )
        }
    }

    internal val etfFundGroupData: LiveData<List<DropDownMultiSelector.Selectable<EtfFundGroup>>> = Transformations.map(viewState) {
        fundDataSource.getFundGroups().map { fundGroup ->
            DropDownMultiSelector.Selectable(
                title = fundGroup.title,
                value = fundGroup,
                selected = fundGroup.id == it.filter.etfFundGroup.id
            )
        }
    }

    internal val fundCategoryData: LiveData<List<DropDownMultiSelector.Selectable<FundCategory>>> = Transformations.map(viewState) {
        fundCategoriesRepo.getLoadedEtfFundCategories(it.filter.etfFundGroup).map { fundCategory ->
            DropDownMultiSelector.Selectable(
                title = fundCategory.title,
                value = fundCategory,
                selected = it.filter.fundCategories.contains(fundCategory)
            )
        }
    }

    internal fun fundFamilySelected(fundFamily: FundFamily) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    fundFamily = fundFamily
                )
            )
        }
    }

    internal fun fundGroupSelected(etfFundGroup: EtfFundGroup) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    etfFundGroup = etfFundGroup,
                    fundCategories = setOf(fundDataSource.getAllFundCategoriesItem())
                )
            )
        }
    }

    internal fun fundCategorySelected(fundCategory: FundCategory) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    fundCategories = it.filter.fundCategories.toggle(fundCategory, fundDataSource.getAllFundCategoriesItem())
                )
            )
        }
    }

    internal fun fundCategoryCleared() {
        updateState {
            it.copy(filter = it.filter.copy(fundCategories = setOf(fundDataSource.getAllFundCategoriesItem())))
        }
    }

    internal fun morningStarRatingToggled(rating: Int, isChecked: Boolean) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    morningStarRating = if (isChecked) { it.filter.morningStarRating + rating } else { it.filter.morningStarRating - rating }
                )
            )
        }
    }

    internal fun categorySelectionCancelled(previousValues: Set<FundCategory>?) {
        previousValues?.let { previousSelection ->
            updateState {
                it.copy(
                    filter = it.filter.copy(
                        fundCategories = previousSelection
                    )
                )
            }
        }
    }

    internal fun backupFundCategorySelection() {
        this.prevCategorySelection = viewState.value?.filter?.fundCategories?.toSet() // copy it
    }

    data class ViewState(
        override val filter: EtfFundProfileFilter
    ) : BaseFilterScreenViewModel.ViewState<EtfFundProfileFilter>
}
