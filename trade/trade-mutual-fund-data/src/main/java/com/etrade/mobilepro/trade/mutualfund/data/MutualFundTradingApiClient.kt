package com.etrade.mobilepro.trade.mutualfund.data

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.POST

internal interface MutualFundTradingApiClient {
    @JsonMoshi
    @POST("app/mftrading/mobilemfsellentry.json")
    suspend fun mutualFundSellEntry(@JsonMoshi @Body body: MutualFundFundsRequestDto): ServerResponseDto<MutualFundFundsResponseDto>

    @JsonMoshi
    @POST("app/mftrading/mobilemfexchangesell.json")
    suspend fun mutualFundExchangeSellEntry(@JsonMoshi @Body body: MutualFundFundsRequestDto): ServerResponseDto<MutualFundFundsResponseDto>

    @JsonMoshi
    @POST("app/mftrading/mobilemfexchangebuy.json")
    suspend fun mutualFundExchangeBuyEntry(@JsonMoshi @Body body: MutualFundFundsRequestDto): ServerResponseDto<MutualFundFundsResponseDto>
}
