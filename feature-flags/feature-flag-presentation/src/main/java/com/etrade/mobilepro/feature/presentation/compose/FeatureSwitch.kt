package com.etrade.mobilepro.feature.presentation.compose

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.Surface
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.etrade.mobilepro.common.compose.theme.EtradeTheme
import com.etrade.mobilepro.feature.presentation.viewmodel.FeatureFlagItemViewModel
import com.etrade.mobilepro.feature.presentation.viewmodel.yesOrNoRes
import kotlinx.coroutines.launch

@Composable
internal fun FeatureSwitch(viewModel: FeatureFlagItemViewModel, modifier: Modifier = Modifier) {
    Column(
        modifier = modifier.wrapContentSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val isEnabled: Boolean by viewModel.isEnabled
            .collectAsState(
                initial = viewModel.defaultValue
            )
        val enabledName = stringResource(isEnabled.yesOrNoRes()).uppercase()
        val scope = rememberCoroutineScope()
        Switch(
            checked = isEnabled,
            enabled = viewModel.isSettable,
            onCheckedChange = { enabled -> scope.launch { viewModel.setEnabled(enabled) } }
        )
        Text(text = enabledName)
    }
}

@Composable
private fun FeatureSwitchPreview(isFlagEnabled: Boolean, isSettable: Boolean = true) {
    Surface(
        modifier = Modifier
            .wrapContentSize()
    ) {
        FeatureSwitch(itemViewModel("", isFlagEnabled, isSettable), Modifier.padding(20.dp))
    }
}

@Preview
@Composable
private fun FeatureSwitchFlagEnabled() {
    EtradeTheme(darkTheme = false) {
        FeatureSwitchPreview(true)
    }
}

@Preview
@Composable
private fun FeatureSwitchFlagDisabled() {
    EtradeTheme(darkTheme = false) {
        FeatureSwitchPreview(false)
    }
}

@Preview
@Composable
private fun FeatureSwitchNotSettable() {
    EtradeTheme(darkTheme = false) {
        FeatureSwitchPreview(isFlagEnabled = false, isSettable = false)
    }
}

@Preview
@Composable
private fun FeatureSwitchFlagEnabledDark() {
    EtradeTheme(darkTheme = true) {
        FeatureSwitchPreview(true)
    }
}

@Preview
@Composable
private fun FeatureSwitchFlagDisabledDark() {
    EtradeTheme(darkTheme = true) {
        FeatureSwitchPreview(false)
    }
}

@Preview
@Composable
private fun FeatureSwitchNotSettableDark() {
    EtradeTheme(darkTheme = true) {
        FeatureSwitchPreview(isFlagEnabled = false, isSettable = false)
    }
}
