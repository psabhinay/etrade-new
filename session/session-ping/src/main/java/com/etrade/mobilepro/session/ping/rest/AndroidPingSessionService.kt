package com.etrade.mobilepro.session.ping.rest

import com.etrade.eo.rest.retrofit.Scalar
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

internal interface AndroidPingSessionService {

    @FormUrlEncoded
    @Scalar
    @POST("/e/t/mobile/AndroidPingSession")
    suspend fun mobilePingSession(
        @Field("UserId") userId: Long,
        @Field("UserDeviceNumber") deviceNumber: String,
        @Field("UserDeviceType") deviceType: String = "ANDROID"
    ): String
}
