package com.etrade.mobilepro.alerts.data.mapper

import com.etrade.mobilepro.alerts.api.model.SetNewsAlertResponse
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.alerts.data.dto.SetNewsAlertDetailXmlDto
import com.etrade.mobilepro.alerts.data.dto.SetNewsAlertResponseXmlDto
import com.etrade.mobilepro.util.json.coerceToBool

private const val DESCRIPTION_FORMAT = "%s: News"

internal class SetNewsAlertDtoMapper {
    private val detailMapper = SetNewsAlertDetailDtoMapper()
    fun map(xmlDto: SetNewsAlertResponseXmlDto): SetNewsAlertResponse =
        SetNewsAlertResponse(
            hasError = coerceToBool(xmlDto.hasError, false),
            errorMessage = xmlDto.errorMessage ?: "",
            hasAlerts = coerceToBool(xmlDto.hasAlerts, false),
            alerts = xmlDto.alerts?.mapNotNull { detailMapper.map(it) } ?: emptyList()
        )
}

internal class SetNewsAlertDetailDtoMapper {
    fun map(xmlDtoNullable: SetNewsAlertDetailXmlDto?): UserAlertItem.NewsAlert? {
        val xmlDto = xmlDtoNullable ?: return null
        return UserAlertItem.NewsAlert(
            symbol = xmlDto.symbol ?: "",
            content = xmlDto.content ?: "",
            delete = xmlDto.delete ?: "",
            deleteInfo = xmlDto.deleteInfo ?: "",
            edit = xmlDto.edit ?: "",
            lastEdited = xmlDto.lastEdited ?: "",
            upDownStatus = coerceToBool(xmlDto.upDownStatus, false),
            inPlayStatus = coerceToBool(xmlDto.inPlayStatus, false),
            earningsStatus = coerceToBool(xmlDto.earningsStatus, false),
            splitNoticeStatus = coerceToBool(xmlDto.splitNoticeStatus, false),
            description = String.format(DESCRIPTION_FORMAT, xmlDto.symbol ?: ""),
            isDeleteBtnVisible = false
        )
    }
}
