package com.etrade.mobilepro.viewdelegate

import androidx.lifecycle.LifecycleOwner

interface ViewDelegate {
    fun observe(owner: LifecycleOwner)
}
