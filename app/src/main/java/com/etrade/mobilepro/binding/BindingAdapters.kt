@file:Suppress("TooManyFunctions", "unused")

package com.etrade.mobilepro.binding

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.chart.model.ChartLineEntry
import com.etrade.mobilepro.chart.util.updateLineDataSetWithEntry
import com.etrade.mobilepro.dynamicui.DynamicDividerView
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.imageloader.loadFromUrlAndSetBackgroundColor
import com.etrade.mobilepro.inboxmessages.dynamicui.api.lowcash.LowCashPrompt
import com.etrade.mobilepro.indices.IndexDataWithColor
import com.etrade.mobilepro.indices.adapter.FuturesWidgetAdapter
import com.etrade.mobilepro.markets.earnings.MarketEventDetailsPayload
import com.etrade.mobilepro.markets.earnings.TodayEarningsWidgetRecyclerView
import com.etrade.mobilepro.movers.MoverGridLayout
import com.etrade.mobilepro.movers.MoverItem
import com.etrade.mobilepro.screener.presentation.common.TableViewInitializeParams
import com.etrade.mobilepro.screener.presentation.common.TableViewPayload
import com.etrade.mobilepro.settings.view.SettingTextWidget
import com.etrade.mobilepro.settings.viewmodel.SettingActionDynamicRefreshViewModel
import com.etrade.mobilepro.settings.viewmodel.SettingTextDynamicViewModel
import com.etrade.mobilepro.tableviewutils.presentation.BroaderIndicesTableView
import com.etrade.mobilepro.uiwidgets.widgetgridlayout.WidgetGridLayout
import com.etrade.mobilepro.uiwidgets.widgetgridlayout.WidgetGridLayoutItem
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.android.accessibility.processAccessibilityStreamingEvent
import com.etrade.mobilepro.util.android.clicklisteners.OnViewDebounceClickListener
import com.etrade.mobilepro.util.android.extension.addDividerItemDecoration
import com.etrade.mobilepro.util.android.extension.clearItemDecorations
import com.etrade.mobilepro.util.android.extension.setEnabledState
import com.etrade.mobilepro.util.android.goneUnless
import com.etrade.mobilepro.util.android.textutil.TextUtil
import com.etrade.mobilepro.util.android.textutil.updateTextPaintForUrlSpans
import com.etrade.mobilepro.util.formatCurrencyAmount
import com.github.mikephil.charting.charts.LineChart
import kotlinx.coroutines.CoroutineScope

@BindingAdapter("movers")
fun MoverGridLayout.setMovers(moverItems: List<MoverItem>?) {
    moverItems?.let { setData(it) }
}

@BindingAdapter("items")
fun WidgetGridLayout.setMovers(items: List<WidgetGridLayoutItem>?) {
    items?.let { submitItems(it) }
}

@BindingAdapter("viewAllButtonViewState", "showActionView")
fun View.viewAllButtonVisibility(viewState: LiveData<DynamicUiViewState>?, showActionView: Boolean) {
    goneUnless(showActionView && viewState?.value == DynamicUiViewState.Success)
}

@BindingAdapter("htmlTagsToSpannable")
fun TextView.htmlTagsToSpannable(text: String) {
    this.text = TextUtil.convertTextWithHmlTagsToSpannable(text)
        .updateTextPaintForUrlSpans(drawUnderlined = false)
}

@BindingAdapter("todayEarnings")
fun TodayEarningsWidgetRecyclerView.setTodayEarnings(items: List<MarketEventDetailsPayload>?) {
    items?.let(::setItems)
}

@BindingAdapter("showIfSuccess")
fun View.showIfSuccess(viewState: DynamicUiViewState?) {
    goneUnless(viewState is DynamicUiViewState.Success)
}

@BindingAdapter("entryData")
fun LineChart.updateLineData(chartLineEntry: ChartLineEntry?) {
    chartLineEntry?.let { updateLineDataSetWithEntry(it) }
}

@BindingAdapter("liveValue")
fun TextView.setNewValue(obj: SettingActionDynamicRefreshViewModel?) {
    text = obj?.refreshedValue
}

@BindingAdapter("layout_height")
fun DynamicDividerView.setHeight(height: Float) {
    layoutParams = layoutParams.apply { this.height = height.toInt() }
}

@BindingAdapter("layoutMarginTop")
fun setLayoutMarginTop(view: View, dimen: Float) {
    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
    layoutParams.topMargin = dimen.toInt()
    view.layoutParams = layoutParams
}

@BindingAdapter("settingsItem")
fun SettingTextWidget.setSettingItem(item: SettingTextDynamicViewModel?) {
    item?.also { setSettingItem(it) }
}

@BindingAdapter("enabledState")
fun View.setEnabledState(enabled: Boolean) {
    setEnabledState(enabled)
}

@BindingAdapter("lowcash_prompt")
fun setLowCashImage(view: ImageView, lowCashPrompt: LowCashPrompt?) {
    lowCashPrompt?.image?.let { view.loadFromUrlAndSetBackgroundColor(it) }
}

@BindingAdapter("formatIndex")
fun setIndexContentDescription(view: View, indexData: IndexDataWithColor?) {
    indexData?.let {
        val lastPointsLabel = view.context.getString(R.string.complete_view_index_last_points_content_description)
        val lastChangeLabel = view.context.getString(R.string.complete_view_index_last_change_content_description)
        view.contentDescription = "${indexData.displayName} $lastPointsLabel ${indexData.last} $lastChangeLabel ${indexData.change}"
    }
}

@BindingAdapter("processIndicesDescription")
fun View.processIndicesDescription(indicesData: IndexDataWithColor?) {
    indicesData?.let {
        val lastPointsLabel =
            context.getString(R.string.complete_view_index_last_points_content_description)
        val lastChangeLabel =
            context.getString(R.string.complete_view_index_last_change_content_description)
        processAccessibilityStreamingEvent(
            "${it.displayName.characterByCharacter()} $lastPointsLabel ${it.last} $lastChangeLabel ${it.change}"
        )
    }
}

@BindingAdapter("formatCurrencyAmount")
fun TextView.setCurrencyAmountContentDescription(value: String?) {
    contentDescription = value?.formatCurrencyAmount()
}

@BindingAdapter("scope")
fun BroaderIndicesTableView.setViewModelScope(viewModelScope: CoroutineScope?) {
    viewModelScope?.let { setScope(viewModelScope) }
}

@BindingAdapter("tableViewInitialParams")
fun BroaderIndicesTableView.setTableViewInitialParams(params: TableViewInitializeParams?) {
    params?.let { initTableViewParams(it.rowHeaderWidth, it.tableViewListener, it.adapterProvider) }
}

@BindingAdapter("tableViewPayload")
fun BroaderIndicesTableView.setTableViewPayload(payload: TableViewPayload?) {
    payload?.let { applyData(it.columnHeaderList, it.rowHeaders, it.cellItems) }
}

@BindingAdapter("timeStampText")
fun TextView.setTimeStampText(timestamp: String?) {
    text = timestamp?.let { context.resources.getString(R.string.as_of_time, it) }
}

@BindingAdapter("futureIndices")
fun setFutureIndices(recyclerView: RecyclerView, indicesData: List<IndexDataWithColor>?) {
    val adapter = recyclerView.adapter as? FuturesWidgetAdapter ?: FuturesWidgetAdapter().also {
        recyclerView.adapter = it
    }
    adapter.futureIndicesItems = indicesData ?: emptyList()
    recyclerView.clearItemDecorations()
    recyclerView.addDividerItemDecoration(R.drawable.medium_divider_horizontal, isVerticalOrientation = false)
}

@BindingAdapter("onDebouncedClick")
fun setOnClickListener(view: View, block: () -> Unit) {
    val clickListener = OnViewDebounceClickListener(
        View.OnClickListener {
            block()
        }
    )
    view.setOnClickListener(clickListener)
}
