package com.etrade.mobilepro.ui_demo.tickview

import android.graphics.drawable.AnimatedVectorDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.ui_demo.databinding.ActivityTickViewBinding
import com.etrade.mobilepro.util.android.binding.viewBinding

class TickViewActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityTickViewBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.play.setOnClickListener {
            playAnimations()
        }
    }

    override fun onResume() {
        super.onResume()
        playAnimations()
    }

    private fun playAnimations() {
        (binding.tickImageView.drawable as? AnimatedVectorDrawable)?.start()
    }
}

