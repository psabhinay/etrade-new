package com.etrade.mobilepro.checkdeposit.usecases.accounts

import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount
import java.math.BigInteger

internal data class DepositAccount(
    override val accountId: String,
    override val accountCategory: String,
    override val instNo: BigInteger,
    override val accountType: String,
    override val nickname: String?
) : FundsFlowAccount
