package com.etrade.mobilepro.appwidget.common

import android.content.Context
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import androidx.annotation.WorkerThread
import com.etrade.mobilepro.appwidget.R

@WorkerThread
internal abstract class BaseRemoteViewsAdapter<T>(
    val context: Context
) : RemoteViewsService.RemoteViewsFactory {

    protected var items: List<T> = listOf()
    protected abstract val remoteViewsFactory: BaseRemoteViewsFactory<T>
    private var cachedRemoteView: RemoteViews? = null

    protected abstract fun getUpdatedItems(): List<T>

    override fun getViewAt(position: Int): RemoteViews {
        return if (items.lastIndex >= position) {
            remoteViewsFactory.create(items[position])
                .also {
                    // Readme.md#Note-3
                    if (position == 0) {
                        cachedRemoteView = it
                    }
                }
        } else {
            createFakeRemoteView()
        }
    }

    override fun onDataSetChanged() {
        items = getUpdatedItems()
    }

    override fun getCount(): Int = items.size
    override fun getViewTypeCount(): Int = 1
    override fun getItemId(position: Int): Long = position.toLong()
    override fun hasStableIds(): Boolean = true

    override fun getLoadingView(): RemoteViews? {
        // Readme.md#Note-3
        return if (items.size == 1 && cachedRemoteView != null) {
            cachedRemoteView
        } else {
            createFakeRemoteView()
        }
    }

    override fun onCreate() { /* Not implemented */ }
    override fun onDestroy() { /* Not implemented */ }

    private fun createFakeRemoteView() = RemoteViews(context.packageName, R.layout.appwidget_item_fake_view)
}
