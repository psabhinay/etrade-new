package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.appwidget.indices.IndicesViewStateService
import com.etrade.mobilepro.appwidget.widgetcontainer.WidgetRemoteViewsService
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvService
import com.etrade.mobilepro.chat.presentation.service.ChatMessagesService
import com.etrade.mobilepro.pushnotification.presentation.EtMessagingService
import com.etrade.mobilepro.pushnotification.presentation.SmartAlertService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceModule {
    @ContributesAndroidInjector
    internal abstract fun contributeChatMessagesServiceInjector(): ChatMessagesService

    @ContributesAndroidInjector
    internal abstract fun contributeWidgetRemoteViewsService(): WidgetRemoteViewsService

    @ContributesAndroidInjector
    internal abstract fun contributeIndicesViewStateService(): IndicesViewStateService

    @ContributesAndroidInjector
    internal abstract fun contributeEtMessagingService(): EtMessagingService

    @ContributesAndroidInjector
    internal abstract fun contributeSmartAlertServiceInjector(): SmartAlertService

    @ContributesAndroidInjector
    internal abstract fun contributeBloombergTvServiceInjector(): BloombergTvService
}
