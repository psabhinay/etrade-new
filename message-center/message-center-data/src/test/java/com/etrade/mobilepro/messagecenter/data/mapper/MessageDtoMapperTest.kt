package com.etrade.mobilepro.messagecenter.data.mapper

import com.etrade.mobilepro.messagecenter.data.dto.MessageDto
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter

class MessageDtoMapperTest {
    private val formatter = DateTimeFormatter.ofPattern("MMM dd yyyy HH:mm:ss:SSSa").withZone(ZoneId.of("Z"))

    @Test
    fun testMapper() {
        val accountIdToDescMap = mapOf("id1" to "descId1")
        val creationDate = "Dec 24 2019 12:38:00:000PM"
        val input = MessageDto(
            msgId = "msgId1",
            accountId = "id1",
            creationTs = creationDate,
            unreadFlag = "1",
            readTs = creationDate
        )

        val output = input.toMessage(formatter, accountIdToDescMap)

        assertEquals("msgId1", output.id)
        assertEquals("descId1", output.accountShortName)
        assertEquals(true, output.isUnread)
        assertEquals(1577191080000, output.createdAt?.toEpochMilli())
        assertEquals(1577191080000, output.readAt?.toEpochMilli())
        assertEquals("12/24/2019", output.createdAtFormatted)
    }
}
