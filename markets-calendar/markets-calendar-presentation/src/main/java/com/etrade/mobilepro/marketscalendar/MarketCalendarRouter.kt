package com.etrade.mobilepro.marketscalendar

import android.os.Bundle
import androidx.navigation.NavController
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails

interface MarketCalendarRouter {
    fun navigateToEarningEventDetails(navController: NavController, item: EventDetails)
    fun navigateToEconomicEventDetails(navController: NavController, item: EventDetails)
    fun getEarningEventDetailsItem(arguments: Bundle?): EventDetails?
    fun getEconomicEventDetailsItem(arguments: Bundle?): EventDetails?
}
