package com.etrade.mobilepro.trade.presentation.disclosures

import android.webkit.WebView
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.di.DeviceType
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

private const val LIETF_JS_INTERFACE_NAME = "ETMobileInterface"

class LiEtfAgreementWebViewFragment @Inject constructor(
    @DeviceType deviceType: String
) : WebViewFragment(deviceType) {

    override fun setupWebView(webView: WebView) {
        super.setupWebView(webView)
        webView.addJavascriptInterface(
            OtcJsInterface(onAction = ::dismissFragment),
            LIETF_JS_INTERFACE_NAME
        )
    }

    private fun dismissFragment() {
        logger.debug("Closing the LIETF WebView Fragment")
        requireActivity().runOnUiThread {
            findNavController().popBackStack()
        }
    }
}
