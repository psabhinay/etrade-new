package com.etrade.mobilepro.portfolio

/**
 * Portfolio positions table mode.
 *
 * @param index index of a mode (must be in sync with: `R.array.portfolio_table_view` and unique across every enum items)
 */
enum class PortfolioTableMode(val index: Int) {

    /**
     * Options View
     */
    OPTIONS(1),

    /**
     * Standard View.
     */
    STANDARD(0);

    companion object {

        /**
         * Gets [PortfolioTableMode] from [index].
         *
         * @param index index of a mode
         *
         * @return table mode
         */
        fun from(index: Int): PortfolioTableMode {
            return values().find { it.index == index } ?: STANDARD
        }
    }
}
