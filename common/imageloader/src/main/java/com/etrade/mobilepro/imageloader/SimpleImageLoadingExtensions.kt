package com.etrade.mobilepro.imageloader

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.core.graphics.drawable.toBitmap
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

/**
 * Loads an image from the provided url, fills the imageview with the color provided
 * if no color is provided, uses the color of the bottom rightmost pixel of the loaded image
 */
fun ImageView.loadFromUrlAndSetBackgroundColor(url: String, color: Int? = null) {
    Glide.with(this)
        .load(url)
        .listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                val bitmap = resource?.toBitmap()
                setBackgroundColor(color ?: bitmap?.getPixel(bitmap.width - 1, bitmap.height - 1) ?: 0)
                return false
            }
        })
        .into(this)
}

fun ImageView.loadFromUrl(url: String) {
    Glide.with(this).load(url).into(this)
}
