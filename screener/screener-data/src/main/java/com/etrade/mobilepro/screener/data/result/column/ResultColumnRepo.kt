package com.etrade.mobilepro.screener.data.result.column

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.data.result.ResultColumn
import com.etrade.mobilepro.tableviewutils.api.ColumnRepo
import com.etrade.mobilepro.tableviewutils.presentation.data.BaseColumnRepo
import javax.inject.Inject

private val stockColumnsOrder = listOf(
    ResultColumn.LastPrice,
    ResultColumn.ChangePercent,
    ResultColumn.ChangeDollar,
    ResultColumn.Volume,
    ResultColumn.MarketCap,
    ResultColumn.Price4WeeksSP500,
    ResultColumn.Price13WeeksSP500,
    ResultColumn.Price52WeeksSP500,
    ResultColumn.Price4WeeksIndustry,
    ResultColumn.Price13WeeksIndustry,
    ResultColumn.Price52WeeksIndustry,
    ResultColumn.PeRatioTrailing12,
    ResultColumn.PeRatioCurrentYear,
    ResultColumn.PeRatioNextYear,
    ResultColumn.PriceToBook,
    ResultColumn.EpsGrowthAnnualCurrent,
    ResultColumn.EpsGrowthAnnualNext,
    ResultColumn.RevenueGrowthAnnualCurrent,
    ResultColumn.RevenueGrowthAnnualNext,
    ResultColumn.EpsGrowthQuarterly,
    ResultColumn.RevenueGrowthQuarterly,
    ResultColumn.DividendYield,
    ResultColumn.PriceChange52WeeksLow,
    ResultColumn.PriceChange52WeeksHigh
)

private val etfColumnOrder = listOf(
    ResultColumn.EtfDescription,
    ResultColumn.LastPrice,
    ResultColumn.ChangeDollar,
    ResultColumn.ChangePercent,
    ResultColumn.FundCategory,
    ResultColumn.Volume,
    ResultColumn.ExpenseRatio,
    ResultColumn.MorningStarRating,
    ResultColumn.MarketReturnCategory1Month,
    ResultColumn.MarketReturnCategory3Months,
    ResultColumn.MarketReturnCategory6Months,
    ResultColumn.MarketReturnCategoryYTD,
    ResultColumn.MarketReturnCategory1Year,
    ResultColumn.MarketReturnCategory3Years,
    ResultColumn.MarketReturnCategory5Years,
    ResultColumn.MarketReturnCategory10Years,
    ResultColumn.MarketReturnCategorySinceInception,
    ResultColumn.NavReturnCategory1Month,
    ResultColumn.NavReturnCategory3Months,
    ResultColumn.NavReturnCategory6Months,
    ResultColumn.NavReturnCategoryYTD,
    ResultColumn.NavReturnCategory1Year,
    ResultColumn.NavReturnCategory3Years,
    ResultColumn.NavReturnCategory5Years,
    ResultColumn.NavReturnCategory10Years,
    ResultColumn.NavReturnCategorySinceInception,
    ResultColumn.StandardDeviation,
    ResultColumn.TotalAssets,
    ResultColumn.Yield,
    ResultColumn.AverageMarketCap,
    ResultColumn.Volume30DaysAvg
)

private val mfColumnsOrder = listOf(
    ResultColumn.Description,
    ResultColumn.MorningStarRating,
    ResultColumn.FundCategory,
    ResultColumn.TotalAssets,
    ResultColumn.ExpenseRatio,
    ResultColumn.DistributionYield,
    ResultColumn.CategoryReturn1Year,
    ResultColumn.CategoryReturn5Years,
    ResultColumn.CategoryReturn10Years,
    ResultColumn.ThreeYearsAlpha,
    ResultColumn.ThreeYearsBeta,
    ResultColumn.ThreeYearsSharpeRatio,
    ResultColumn.ThreeYearsRSquare,
    ResultColumn.TurnOverRatio,
    ResultColumn.PortfolioConcentration,
    ResultColumn.AverageMarketCap,
    ResultColumn.FrontLoad,
    ResultColumn.DeferLoad,
    ResultColumn.RedemptionFee
)

class ResultColumnRepo @Inject constructor(
    storage: KeyValueStorage,
    group: ScreenerType.Group
) : BaseColumnRepo<ResultColumn>(storage), ColumnRepo<ResultColumn> {
    override val storageKey: String = "screenerResultColumns" + group.constantName

    override val allColumns: Map<String, ResultColumn> = when (group) {
        ScreenerType.Group.STOCK -> stockColumnsOrder
        ScreenerType.Group.MUTUAL_FUND -> mfColumnsOrder
        ScreenerType.Group.ETF -> etfColumnOrder
    }.associateBy({ it.storageKey }, { it })

    override val defaultColumnsOrder: List<String> = when (group) {
        ScreenerType.Group.STOCK -> stockColumnsOrder
        ScreenerType.Group.MUTUAL_FUND -> mfColumnsOrder
        ScreenerType.Group.ETF -> etfColumnOrder
    }.map {
        it.storageKey
    }
}
