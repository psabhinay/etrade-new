package com.etrade.mobilepro.positions.dao

import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.util.market.time.isMarketTime
import com.etrade.mobilepro.util.safeParseBigDecimal
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.math.BigDecimal

open class PositionRealmObject(
    @PrimaryKey
    override var positionId: String = "",
    var accountUuid: String = "",
    var accountId: String = "",
    var pricePaidRaw: String = "",
    var basisPrice: String = "",
    override var symbol: String = "",
    var commissionRaw: String = "",
    var quantityRaw: String = "",
    var displayQuantityRaw: String = "",
    var feesRaw: String = "",
    var todayQuantity: String = "",
    var todayPricePaid: String = "",
    var todayCommissions: String = "",
    var daysGainValue: String = "",
    var daysGainPercentageRaw: String = "",
    var totalGainValue: String = "",
    var totalGainPercentageRaw: String = "",
    var todaysClose: String = "",
    var daysQuantity: String? = null,
    var daysPurchase: Boolean = false,
    var hasLots: Boolean = false,
    override var inTheMoneyFlag: Boolean? = null,
    var optionUnderlier: String = "",
    var strikePrice: String = "",
    var markToMarket: String = "",
    var marketValueRaw: String = "",
    var baseSymbolPrice: String? = null
) : RealmObject(), Position {

    override val daysGain: BigDecimal?
        get() = daysGainValue.safeParseBigDecimal()

    override val totalGain: BigDecimal?
        get() = totalGainValue.safeParseBigDecimal()

    override val pricePaid: BigDecimal?
        get() = pricePaidRaw.safeParseBigDecimal()

    override val commission: BigDecimal?
        get() = commissionRaw.safeParseBigDecimal()

    override val fees: BigDecimal?
        get() = feesRaw.safeParseBigDecimal()

    override val quantity: BigDecimal?
        get() = quantityRaw.safeParseBigDecimal()

    override val displayQuantity: BigDecimal?
        get() = displayQuantityRaw.safeParseBigDecimal()

    override val daysGainPercentage: BigDecimal?
        get() = daysGainPercentageRaw.safeParseBigDecimal()

    override val totalGainPercentage: BigDecimal?
        get() = totalGainPercentageRaw.safeParseBigDecimal()

    override val todayClose: BigDecimal?
        get() = if (isMarketTime()) null else todaysClose.safeParseBigDecimal()

    override val baseSymbolAndPrice: String?
        get() = baseSymbolPrice

    override val mark: BigDecimal?
        get() = markToMarket.safeParseBigDecimal()

    override val marketValue: BigDecimal?
        get() = marketValueRaw.safeParseBigDecimal()

    @Suppress("LongParameterList", "LongMethod")
    fun copy(
        positionId: String = this.positionId,
        accountUuid: String = this.accountUuid,
        accountId: String = this.accountId,
        pricePaidRaw: String = this.pricePaidRaw,
        basisPrice: String = this.basisPrice,
        symbol: String = this.symbol,
        commissionRaw: String = this.commissionRaw,
        quantityRaw: String = this.quantityRaw,
        displayQuantityRaw: String = this.displayQuantityRaw,
        feesRaw: String = this.feesRaw,
        todayQuantity: String = this.todayQuantity,
        todayPricePaid: String = this.todayPricePaid,
        todayCommissions: String = this.todayCommissions,
        daysGainValue: String = this.daysGainValue,
        daysGainPercentageRaw: String = this.daysGainPercentageRaw,
        totalGainValue: String = this.totalGainValue,
        totalGainPercentageRaw: String = this.totalGainPercentageRaw,
        todaysClose: String = this.todaysClose,
        daysQuantity: String? = this.daysQuantity,
        daysPurchase: Boolean = this.daysPurchase,
        hasLots: Boolean = this.hasLots,
        inTheMoneyFlag: Boolean? = this.inTheMoneyFlag,
        optionUnderlier: String = this.optionUnderlier,
        strikePrice: String = this.strikePrice,
        markToMarket: String = this.markToMarket,
        marketValueRaw: String = this.marketValueRaw,
        baseSymbolPrice: String? = this.baseSymbolPrice
    ): PositionRealmObject {
        return PositionRealmObject(
            positionId = positionId,
            accountUuid = accountUuid,
            accountId = accountId,
            pricePaidRaw = pricePaidRaw,
            basisPrice = basisPrice,
            symbol = symbol,
            commissionRaw = commissionRaw,
            quantityRaw = quantityRaw,
            displayQuantityRaw = displayQuantityRaw,
            feesRaw = feesRaw,
            todayQuantity = todayQuantity,
            todayPricePaid = todayPricePaid,
            todayCommissions = todayCommissions,
            daysGainValue = daysGainValue,
            daysGainPercentageRaw = daysGainPercentageRaw,
            totalGainValue = totalGainValue,
            totalGainPercentageRaw = totalGainPercentageRaw,
            todaysClose = todaysClose,
            daysQuantity = daysQuantity,
            daysPurchase = daysPurchase,
            hasLots = hasLots,
            inTheMoneyFlag = inTheMoneyFlag,
            optionUnderlier = optionUnderlier,
            strikePrice = strikePrice,
            markToMarket = markToMarket,
            marketValueRaw = marketValueRaw,
            baseSymbolPrice = baseSymbolPrice
        )
    }

    fun updateWithStreamUpdatedObject(positionRealmObject: PositionRealmObject?) {
        positionRealmObject?.let {
            daysGainValue = it.daysGain.toString()
            daysGainPercentageRaw = it.daysGainPercentage.toString()
            totalGainValue = it.totalGain.toString()
            totalGainPercentageRaw = it.totalGainPercentage.toString()
            markToMarket = it.mark.toString()
            marketValueRaw = it.marketValue.toString()
        }
    }
}
