package com.etrade.mobilepro.bank;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0011\u001a\u00020\u0012H\u0002J\u0012\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\u000b\u001a\u00020\f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/etrade/mobilepro/bank/BankDateTransactionPickerFragment;", "Landroidx/fragment/app/DialogFragment;", "viewModelFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "(Landroidx/lifecycle/ViewModelProvider$Factory;)V", "args", "Lcom/etrade/mobilepro/bank/BankDateTransactionPickerFragmentArgs;", "getArgs", "()Lcom/etrade/mobilepro/bank/BankDateTransactionPickerFragmentArgs;", "args$delegate", "Landroidx/navigation/NavArgsLazy;", "bankHomeViewModel", "Lcom/etrade/mobilepro/bank/BankHomeViewModel;", "getBankHomeViewModel", "()Lcom/etrade/mobilepro/bank/BankHomeViewModel;", "bankHomeViewModel$delegate", "Lkotlin/Lazy;", "getDateForPicker", "Lorg/threeten/bp/LocalDate;", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "updateSelectableDateRange", "", "datePicker", "Landroid/widget/DatePicker;", "bank-dynamic_debug"})
@com.etrade.mobilepro.common.RequireLogin()
public final class BankDateTransactionPickerFragment extends androidx.fragment.app.DialogFragment {
    private final androidx.lifecycle.ViewModelProvider.Factory viewModelFactory = null;
    private final kotlin.Lazy bankHomeViewModel$delegate = null;
    private final androidx.navigation.NavArgsLazy args$delegate = null;
    
    @javax.inject.Inject()
    public BankDateTransactionPickerFragment(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.ViewModelProvider.Factory viewModelFactory) {
        super();
    }
    
    private final com.etrade.mobilepro.bank.BankHomeViewModel getBankHomeViewModel() {
        return null;
    }
    
    private final com.etrade.mobilepro.bank.BankDateTransactionPickerFragmentArgs getArgs() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.app.Dialog onCreateDialog(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void updateSelectableDateRange(android.widget.DatePicker datePicker) {
    }
    
    private final org.threeten.bp.LocalDate getDateForPicker() {
        return null;
    }
}