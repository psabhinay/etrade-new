package com.etrade.mobilepro.trade.form.api.input

interface BaseTradeFormInputId :
    TradeFormInputId,
    WithPriceTradeFormInputId,
    WithSymbolTradeFormInputId {
    val orderId: String
}
