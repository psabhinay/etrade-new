package com.etrade.mobilepro.caching

import androidx.annotation.VisibleForTesting
import org.threeten.bp.Duration
import org.threeten.bp.Instant

class TimeElapsedCacheController<T> @VisibleForTesting internal constructor(
    private val now: () -> Instant,
    private val maximumTimeElapsed: Duration,
    private val instantConverter: (T) -> Instant?
) : SimpleCachedResource.Controller<T> {

    constructor(maximumTimeElapsed: Duration, instantConverter: (T) -> Instant?) : this(
        now = { Instant.now() },
        maximumTimeElapsed = maximumTimeElapsed,
        instantConverter = instantConverter
    )

    override fun shouldRefresh(itemFromCache: T): Boolean {
        return instantConverter(itemFromCache)?.let {
            val cacheDuration = Duration.between(it, now())
            cacheDuration > maximumTimeElapsed
        } ?: true
    }
}
