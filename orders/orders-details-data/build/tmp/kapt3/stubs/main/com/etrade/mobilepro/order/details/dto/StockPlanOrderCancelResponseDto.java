package com.etrade.mobilepro.order.details.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\bb\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\u00a5\u0002\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u001bJ\u000b\u0010L\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010M\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010N\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010O\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010P\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010Q\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010R\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010S\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010T\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010U\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010V\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010W\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010X\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010Y\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010Z\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010[\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\\\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010]\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010^\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010_\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010`\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010a\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010c\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u00a9\u0002\u0010d\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010e\u001a\u00020f2\b\u0010g\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010h\u001a\u00020iH\u00d6\u0001J\t\u0010j\u001a\u00020\u0003H\u00d6\u0001R \u0010\f\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR \u0010\u0016\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u001d\"\u0004\b!\u0010\u001fR \u0010\u0006\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u001d\"\u0004\b#\u0010\u001fR \u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u001d\"\u0004\b%\u0010\u001fR \u0010\u0014\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u001d\"\u0004\b\'\u0010\u001fR \u0010\u0018\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u001d\"\u0004\b)\u0010\u001fR \u0010\u0015\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u001d\"\u0004\b+\u0010\u001fR \u0010\u000e\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u001d\"\u0004\b,\u0010\u001fR \u0010\u000b\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\u001d\"\u0004\b-\u0010\u001fR \u0010\u0013\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u001d\"\u0004\b/\u0010\u001fR \u0010\u0011\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b0\u0010\u001d\"\u0004\b1\u0010\u001fR \u0010\u0019\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\u001d\"\u0004\b3\u0010\u001fR \u0010\u0017\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u001d\"\u0004\b5\u0010\u001fR \u0010\u000f\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b6\u0010\u001d\"\u0004\b7\u0010\u001fR \u0010\u0005\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b8\u0010\u001d\"\u0004\b9\u0010\u001fR \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010\u001d\"\u0004\b;\u0010\u001fR \u0010\u0007\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b<\u0010\u001d\"\u0004\b=\u0010\u001fR \u0010\t\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b>\u0010\u001d\"\u0004\b?\u0010\u001fR \u0010\u0010\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010\u001d\"\u0004\bA\u0010\u001fR \u0010\r\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bB\u0010\u001d\"\u0004\bC\u0010\u001fR \u0010\b\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010\u001d\"\u0004\bE\u0010\u001fR \u0010\u001a\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bF\u0010\u001d\"\u0004\bG\u0010\u001fR \u0010\u0012\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bH\u0010\u001d\"\u0004\bI\u0010\u001fR \u0010\n\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bJ\u0010\u001d\"\u0004\bK\u0010\u001f\u00a8\u0006k"}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/StockPlanOrderCancelResponseDto;", "", "priceType", "", "emailAddress", "orderTime", "companyName", "sharesToExercise", "tickerName", "sharesToHold", "transactionType", "isEtca", "accountNumber", "term", "isAON", "orderNumber", "sharesToSell", "limitPrice", "tradeDesiredExpiration", "iscustomGtc", "emvPending", "hasEtslAcct", "accountTitle", "minorTitle", "etsl_acct", "majorTitle", "ticker_symbol", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccountNumber", "()Ljava/lang/String;", "setAccountNumber", "(Ljava/lang/String;)V", "getAccountTitle", "setAccountTitle", "getCompanyName", "setCompanyName", "getEmailAddress", "setEmailAddress", "getEmvPending", "setEmvPending", "getEtsl_acct", "setEtsl_acct", "getHasEtslAcct", "setHasEtslAcct", "setAON", "setEtca", "getIscustomGtc", "setIscustomGtc", "getLimitPrice", "setLimitPrice", "getMajorTitle", "setMajorTitle", "getMinorTitle", "setMinorTitle", "getOrderNumber", "setOrderNumber", "getOrderTime", "setOrderTime", "getPriceType", "setPriceType", "getSharesToExercise", "setSharesToExercise", "getSharesToHold", "setSharesToHold", "getSharesToSell", "setSharesToSell", "getTerm", "setTerm", "getTickerName", "setTickerName", "getTicker_symbol", "setTicker_symbol", "getTradeDesiredExpiration", "setTradeDesiredExpiration", "getTransactionType", "setTransactionType", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "orders-details-data"})
@org.simpleframework.xml.Root(name = "cancelorderresponse", strict = true)
public final class StockPlanOrderCancelResponseDto {
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "pricetype", required = false)
    private java.lang.String priceType;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "emailAddress", required = false)
    private java.lang.String emailAddress;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "orderTime", required = false)
    private java.lang.String orderTime;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "companyName", required = false)
    private java.lang.String companyName;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "sharesToExercise", required = false)
    private java.lang.String sharesToExercise;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "tickerName", required = false)
    private java.lang.String tickerName;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "sharesToHold", required = false)
    private java.lang.String sharesToHold;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "transactionType", required = false)
    private java.lang.String transactionType;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "is_etca", required = false)
    private java.lang.String isEtca;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "accountNumber", required = false)
    private java.lang.String accountNumber;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "term", required = false)
    private java.lang.String term;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "isAON", required = false)
    private java.lang.String isAON;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "orderNumber", required = false)
    private java.lang.String orderNumber;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "sharesToSell", required = false)
    private java.lang.String sharesToSell;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "limitPrice", required = false)
    private java.lang.String limitPrice;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "tradeDesiredExpiration", required = false)
    private java.lang.String tradeDesiredExpiration;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "iscustomgtc", required = false)
    private java.lang.String iscustomGtc;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "emvPending", required = false)
    private java.lang.String emvPending;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "hasEtslAcct", required = false)
    private java.lang.String hasEtslAcct;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "account_number", required = false)
    private java.lang.String accountTitle;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "minorTitle", required = false)
    private java.lang.String minorTitle;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "etsl_acct", required = false)
    private java.lang.String etsl_acct;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "majorTitle", required = false)
    private java.lang.String majorTitle;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.Element(name = "ticker_symbol", required = false)
    private java.lang.String ticker_symbol;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.StockPlanOrderCancelResponseDto copy(@org.jetbrains.annotations.Nullable()
    java.lang.String priceType, @org.jetbrains.annotations.Nullable()
    java.lang.String emailAddress, @org.jetbrains.annotations.Nullable()
    java.lang.String orderTime, @org.jetbrains.annotations.Nullable()
    java.lang.String companyName, @org.jetbrains.annotations.Nullable()
    java.lang.String sharesToExercise, @org.jetbrains.annotations.Nullable()
    java.lang.String tickerName, @org.jetbrains.annotations.Nullable()
    java.lang.String sharesToHold, @org.jetbrains.annotations.Nullable()
    java.lang.String transactionType, @org.jetbrains.annotations.Nullable()
    java.lang.String isEtca, @org.jetbrains.annotations.Nullable()
    java.lang.String accountNumber, @org.jetbrains.annotations.Nullable()
    java.lang.String term, @org.jetbrains.annotations.Nullable()
    java.lang.String isAON, @org.jetbrains.annotations.Nullable()
    java.lang.String orderNumber, @org.jetbrains.annotations.Nullable()
    java.lang.String sharesToSell, @org.jetbrains.annotations.Nullable()
    java.lang.String limitPrice, @org.jetbrains.annotations.Nullable()
    java.lang.String tradeDesiredExpiration, @org.jetbrains.annotations.Nullable()
    java.lang.String iscustomGtc, @org.jetbrains.annotations.Nullable()
    java.lang.String emvPending, @org.jetbrains.annotations.Nullable()
    java.lang.String hasEtslAcct, @org.jetbrains.annotations.Nullable()
    java.lang.String accountTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String minorTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String etsl_acct, @org.jetbrains.annotations.Nullable()
    java.lang.String majorTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String ticker_symbol) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public StockPlanOrderCancelResponseDto() {
        super();
    }
    
    public StockPlanOrderCancelResponseDto(@org.jetbrains.annotations.Nullable()
    java.lang.String priceType, @org.jetbrains.annotations.Nullable()
    java.lang.String emailAddress, @org.jetbrains.annotations.Nullable()
    java.lang.String orderTime, @org.jetbrains.annotations.Nullable()
    java.lang.String companyName, @org.jetbrains.annotations.Nullable()
    java.lang.String sharesToExercise, @org.jetbrains.annotations.Nullable()
    java.lang.String tickerName, @org.jetbrains.annotations.Nullable()
    java.lang.String sharesToHold, @org.jetbrains.annotations.Nullable()
    java.lang.String transactionType, @org.jetbrains.annotations.Nullable()
    java.lang.String isEtca, @org.jetbrains.annotations.Nullable()
    java.lang.String accountNumber, @org.jetbrains.annotations.Nullable()
    java.lang.String term, @org.jetbrains.annotations.Nullable()
    java.lang.String isAON, @org.jetbrains.annotations.Nullable()
    java.lang.String orderNumber, @org.jetbrains.annotations.Nullable()
    java.lang.String sharesToSell, @org.jetbrains.annotations.Nullable()
    java.lang.String limitPrice, @org.jetbrains.annotations.Nullable()
    java.lang.String tradeDesiredExpiration, @org.jetbrains.annotations.Nullable()
    java.lang.String iscustomGtc, @org.jetbrains.annotations.Nullable()
    java.lang.String emvPending, @org.jetbrains.annotations.Nullable()
    java.lang.String hasEtslAcct, @org.jetbrains.annotations.Nullable()
    java.lang.String accountTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String minorTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String etsl_acct, @org.jetbrains.annotations.Nullable()
    java.lang.String majorTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String ticker_symbol) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPriceType() {
        return null;
    }
    
    public final void setPriceType(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmailAddress() {
        return null;
    }
    
    public final void setEmailAddress(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOrderTime() {
        return null;
    }
    
    public final void setOrderTime(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCompanyName() {
        return null;
    }
    
    public final void setCompanyName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSharesToExercise() {
        return null;
    }
    
    public final void setSharesToExercise(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTickerName() {
        return null;
    }
    
    public final void setTickerName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSharesToHold() {
        return null;
    }
    
    public final void setSharesToHold(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTransactionType() {
        return null;
    }
    
    public final void setTransactionType(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String isEtca() {
        return null;
    }
    
    public final void setEtca(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component10() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccountNumber() {
        return null;
    }
    
    public final void setAccountNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTerm() {
        return null;
    }
    
    public final void setTerm(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component12() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String isAON() {
        return null;
    }
    
    public final void setAON(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component13() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOrderNumber() {
        return null;
    }
    
    public final void setOrderNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component14() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSharesToSell() {
        return null;
    }
    
    public final void setSharesToSell(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component15() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLimitPrice() {
        return null;
    }
    
    public final void setLimitPrice(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component16() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTradeDesiredExpiration() {
        return null;
    }
    
    public final void setTradeDesiredExpiration(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component17() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getIscustomGtc() {
        return null;
    }
    
    public final void setIscustomGtc(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component18() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmvPending() {
        return null;
    }
    
    public final void setEmvPending(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component19() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getHasEtslAcct() {
        return null;
    }
    
    public final void setHasEtslAcct(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component20() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccountTitle() {
        return null;
    }
    
    public final void setAccountTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component21() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMinorTitle() {
        return null;
    }
    
    public final void setMinorTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component22() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEtsl_acct() {
        return null;
    }
    
    public final void setEtsl_acct(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component23() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMajorTitle() {
        return null;
    }
    
    public final void setMajorTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component24() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTicker_symbol() {
        return null;
    }
    
    public final void setTicker_symbol(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
}