package com.etrade.mobilepro.screener.presentation.mutualfund.risk

import com.etrade.mobilepro.screener.api.filter.mutualfund.RiskFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class ThreeYearBeta : ScreenerFilterEvent<RiskFilter, RiskViewModel.ViewState, Boolean> {
    LESS_VOLATILE {
        override fun update(viewState: RiskViewModel.ViewState, eventValue: Boolean): RiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(lessVolatile = eventValue))
    },
    AS_VOLATILE {
        override fun update(viewState: RiskViewModel.ViewState, eventValue: Boolean): RiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(asVolatile = eventValue))
    },
    MORE_VOLATILE {
        override fun update(viewState: RiskViewModel.ViewState, eventValue: Boolean): RiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(moreVolatile = eventValue))
    }
}

internal enum class StandardDeviation : ScreenerFilterEvent<RiskFilter, RiskViewModel.ViewState, Boolean> {
    BOTTOM_20 {
        override fun update(viewState: RiskViewModel.ViewState, eventValue: Boolean): RiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(bottom20 = eventValue))
    },
    BELOW_MID_20 {
        override fun update(viewState: RiskViewModel.ViewState, eventValue: Boolean): RiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(belowMid20 = eventValue))
    },
    MID_20 {
        override fun update(viewState: RiskViewModel.ViewState, eventValue: Boolean): RiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(mid20 = eventValue))
    },
    ABOVE_MID_20 {
        override fun update(viewState: RiskViewModel.ViewState, eventValue: Boolean): RiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(aboveMid20 = eventValue))
    },
    TOP_20 {
        override fun update(viewState: RiskViewModel.ViewState, eventValue: Boolean): RiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(top20 = eventValue))
    }
}
