package com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemSectionBinding
import com.etrade.mobilepro.debugmenu.presentation.item.SectionItem

internal class SectionViewHolder(private val binding: DebugmenuItemSectionBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bindTo(item: SectionItem) {
        binding.header.setText(item.titleStringRes)
    }
}
