package com.etrade.mobilepro.documents.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.documents.Document
import com.etrade.mobilepro.documents.DocumentStorage
import com.etrade.mobilepro.documents.StatementDocument
import com.etrade.mobilepro.documents.TaxDocument
import com.etrade.mobilepro.util.guard
import javax.inject.Inject

class DocumentDetailsViewModel @Inject constructor(
    private val documentStorage: DocumentStorage
) : ViewModel() {
    private val _viewState = MutableLiveData<ViewState>()
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    internal fun fetchData(documentId: String) {
        val documentWithData = documentStorage.getCachedDocument(documentId) guard {
            _viewState.value = ViewState(status = ViewState.Status.Error { fetchData(documentId) })
            return
        }
        if (_viewState.value?.status is ViewState.Status.Success && _viewState.value?.documentId == documentId) { return }
        _viewState.value = ViewState(status = ViewState.Status.Success, document = documentWithData)
    }

    internal data class ViewState(
        val status: Status,
        private val document: Document? = null
    ) {
        val filename = document?.filename
        val title = when (document) {
            is TaxDocument -> "Tax document details"
            is StatementDocument -> "Statement details"
            else -> document?.title
        }
        val documentId = document?.documentId

        sealed class Status {
            object Success : Status()
            class Error(
                val retry: () -> Unit
            ) : Status()
        }
    }
}
