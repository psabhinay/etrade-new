package com.etrade.mobilepro.chartiq.api.repo

import com.etrade.mobilepro.common.result.ETResult

interface ChartIqDataRepo {
    suspend fun getChartData(chartQuery: String?): ETResult<String>

    suspend fun getAllViews(): ETResult<String>

    suspend fun getStudies(): ETResult<String>

    suspend fun getDrawing(symbol: String?): ETResult<String>

    suspend fun saveView(requestBody: String?): ETResult<String>

    suspend fun deleteView(requestBody: String?): ETResult<String>

    suspend fun saveDrawing(requestBody: String?): ETResult<String>

    suspend fun deleteDrawing(requestBody: String?): ETResult<String>

    suspend fun getOpenOrders(requestBody: String?): ETResult<String>
}
