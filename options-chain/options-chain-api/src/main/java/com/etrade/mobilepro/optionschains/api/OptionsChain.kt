package com.etrade.mobilepro.optionschains.api

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.instrument.ExpirationDate
import com.etrade.mobilepro.streaming.api.Level1Data
import java.math.BigDecimal

data class OptionsChain(
    val expirationDates: List<ExpirationDate>,
    val optionPairs: List<OptionsPair>
)

data class OptionsPair(
    val call: Option,
    val put: Option
)

interface Option {
    val displaySymbol: String
    val strikePrice: BigDecimal
    val symbol: String
    val isInTheMoney: Boolean
    val isAMOption: Boolean

    val streamBid: LiveData<BigDecimal>
    val streamBidSize: LiveData<String>
    val streamAsk: LiveData<BigDecimal>
    val streamAskSize: LiveData<String>
    val streamVolume: LiveData<String>
    val streamOpenInterest: LiveData<String>
    val streamLastPrice: LiveData<BigDecimal>
    val streamChange: LiveData<BigDecimal>

    fun updateItem(data: Level1Data) = Unit
}

val Option.id: String
    get() = symbol
