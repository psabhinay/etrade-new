package com.etrade.mobilepro.checkdeposit.usecases.deposit

import com.etrade.mobilepro.checkdeposit.deposit.service.DepositImageDto
import com.etrade.mobilepro.checkdeposit.deposit.service.DepositImageValueDto
import com.etrade.mobilepro.checkdeposit.deposit.service.RemoteCheckDepositService
import com.etrade.mobilepro.checkdeposit.usecases.api.DepositImageUseCase
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET

private const val FRONT_REAR_TOKENS = "Front_Rear_Tokens"
private const val FRONT_REAR_IMAGES = "Front_Rear_Images"
private const val REAR_TOKEN_FRONT_IMAGE = "RearToken_FrontImage"
private const val FRONT_TOKEN_REAR_IMAGE = "FrontToken_RearImage"

class DepositImageUseCaseImpl(
    private val userId: String,
    private val service: RemoteCheckDepositService
) : DepositImageUseCase {

    @SuppressWarnings("LongMethod")
    override suspend fun execute(parameter: DepositImageRequest): ETResult<CheckDepositResult> = runCatchingET {
        service
            .depositimage(
                DepositImageDto(
                    DepositImageValueDto(
                        userId = userId,
                        depositMode = getDepositMode(parameter),
                        amount = parameter.amount,
                        accountNo = parameter.accountNo,
                        instNo = parameter.instNo,
                        workflowId = parameter.workflowId,
                        frontImageToken = parameter.frontImageToken,
                        rearImageToken = parameter.rearImageToken,
                        frontImage = parameter.frontImage,
                        rearImage = parameter.rearImage,
                        iraDepositType = parameter.iraDepositType
                    )
                )
            ).doSuccessful {
                if (it.hasNoMessages()) {
                    CheckDepositResult.Valid(
                        workflowId = it.depositImageResultDto.workflowId!!,
                        accountNumber = it.depositImageResultDto.accountNumber!!,
                        amount = it.depositImageResultDto.amount!!
                    )
                } else {
                    CheckDepositResult.Invalid(
                        message = it.getFirstMessageText(),
                        code = it.getFirstMessageCode(),
                        frontImageFieldToFlag = it.depositImageResultDto.frontImageFieldToFlag,
                        rearImageFieldToFlag = it.depositImageResultDto.rearImageFieldToFlag,
                        amountFieldToFlag = it.depositImageResultDto.amountFieldToFlag
                    )
                }
            }
    }

    private fun getDepositMode(parameter: DepositImageRequest): String? = when {
        parameter.frontImageToken != null && parameter.rearImageToken != null -> FRONT_REAR_TOKENS
        parameter.frontImage != null && parameter.rearImage != null -> FRONT_REAR_IMAGES
        parameter.frontImage != null && parameter.rearImageToken != null -> REAR_TOKEN_FRONT_IMAGE
        parameter.frontImageToken != null && parameter.rearImage != null -> FRONT_TOKEN_REAR_IMAGE
        else -> null
    }
}
