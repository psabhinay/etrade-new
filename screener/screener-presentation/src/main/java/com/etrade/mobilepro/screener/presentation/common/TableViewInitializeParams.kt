package com.etrade.mobilepro.screener.presentation.common

import com.etrade.mobilepro.tableviewutils.R
import com.etrade.mobilepro.tableviewutils.presentation.TableViewListener
import com.etrade.mobilepro.tableviewutils.presentation.adapter.TableViewAdapter

class TableViewInitializeParams(
    val rowHeaderWidth: Int = R.dimen.default_tableview_row_header_width,
    val tableViewListener: TableViewListener? = null,
    val adapterProvider: () -> TableViewAdapter
)
