package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.data.CommonOrderConstants
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.etrade.mobilepro.trade.data.form.input.DefaultOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

private const val SYMBOL = "ETFC"

class StockSaveOrderRequestParametersProviderTest {

    private val stockSaveOrderRequestParametersProvider: SaveOrderRequestParametersProvider = StockSaveOrderRequestParametersProvider()

    private val generalTestCase: TestCase = object : TestCase {
        override val params: Map<String, String?> = mapOf(
            DefaultStockTradeFormInputId.accountId to "83906686",
            DefaultStockTradeFormInputId.securityType to DefaultStockTradeFormInputId.securityTypeValueId.stock,
            DefaultStockTradeFormInputId.symbol to SYMBOL
        )
        override val expected: Map<String, Any?> = mapOf(
            DefaultOptionTradeFormInputId.accountId to "83906686",
            DefaultOptionTradeFormInputId.symbol to SYMBOL,
            DefaultOptionTradeFormInputId.strategy to StrategyType.STOCK.typeCode
        )
    }

    private val legTestCases: List<TestCase> = listOf(
        createLegTestCase(10, TransactionType.BUY_OPEN) { buy },
        createLegTestCase(100, TransactionType.SELL_CLOSE) { sell }
    )

    private val priceTypeTestCases: List<TestCase> = listOf(
        createPriceTypeTestCase(orderType = OrderType.EQUITY, priceType = { market }, term = { goodForDay }),
        createPriceTypeTestCase(orderType = OrderType.EQUITY, limitPrice = "220", priceType = { limit }, term = { fillOrKill }),
        createPriceTypeTestCase(orderType = OrderType.EQUITY, stopPrice = "110", priceType = { stop }, term = { goodForDay }),
        createPriceTypeTestCase(orderType = OrderType.ADVANCE_EQUITY, stopPrice = "350", priceType = { trailingStopDollar }, term = { goodUntilCancel }),
        createPriceTypeTestCase(orderType = OrderType.ADVANCE_EQUITY, stopPrice = "50", priceType = { trailingStopPercent }, term = { goodForDay })
    )

    @Test
    fun `verify provider's result`() {
        legTestCases.forEach { legTestCase ->
            priceTypeTestCases.forEach { priceTypeTestCase ->
                val testCases = listOf(generalTestCase, legTestCase, priceTypeTestCase)
                val testCase = object : TestCase {
                    override val params: Map<String, String?> = merge(testCases) { params }
                    override val expected: Map<String, Any?> = mapOf("value" to merge(testCases) { expected })
                }
                checkEqual(testCase.expected, stockSaveOrderRequestParametersProvider.createBody(testCase.params).normalized)
            }
        }
    }

    private inline fun <reified T> merge(testCases: Iterable<TestCase>, property: TestCase.() -> Map<String, T?>): Map<String, T?> {
        return testCases.map { it.property() }.fold(mapOf()) { acc, value ->
            acc + value
        }
    }

    private fun checkEqual(expected: Map<*, *>, actual: Map<*, *>) {
        expected.forEach { (key, value) ->
            when (value) {
                is List<*> -> {
                    val list = actual[key] as List<*>
                    value.forEachIndexed { index, item ->
                        checkEqual(item as Map<*, *>, list[index] as Map<*, *>)
                    }
                }
                is Map<*, *> -> checkEqual(value, actual[key] as Map<*, *>)
                else -> assertEquals(value, actual[key], "key=$key")
            }
        }
    }

    private fun createLegTestCase(quantity: Int, expectedTransactionType: TransactionType, action: ActionValueId.() -> String): TestCase {
        return object : TestCase {
            override val params: Map<String, String?> = mapOf(
                DefaultStockTradeFormInputId.action to DefaultStockTradeFormInputId.actionValueId.action(),
                DefaultStockTradeFormInputId.quantity to quantity.toString()
            )
            override val expected: Map<String, Any?> = mapOf(
                DefaultOptionTradeFormInputId.tradeLegs to listOf(
                    mapOf(
                        "quantity" to quantity,
                        "symbol" to SYMBOL,
                        "transaction" to expectedTransactionType.typeCode.toString()
                    )
                )
            )
        }
    }

    private fun createPriceTypeTestCase(
        orderType: OrderType,
        limitPrice: String? = null,
        stopPrice: String? = null,
        priceType: PriceTypeValueId.() -> String,
        term: OrderTermValueId.() -> String
    ): TestCase {
        return object : TestCase {
            override val params: Map<String, String?> = mapOf(
                DefaultStockTradeFormInputId.priceType to DefaultStockTradeFormInputId.priceTypeValueId.priceType(),
                DefaultStockTradeFormInputId.limitPrice to limitPrice,
                DefaultStockTradeFormInputId.stopPrice to stopPrice,
                DefaultStockTradeFormInputId.term to DefaultStockTradeFormInputId.termValueId.term()
            )
            override val expected: Map<String, Any?> = mapOf(
                CommonOrderConstants.ORDER_TYPE to orderType.typeCode,
                DefaultOptionTradeFormInputId.priceType to DefaultOptionTradeFormInputId.priceTypeValueId.priceType(),
                DefaultOptionTradeFormInputId.limitPrice to limitPrice,
                DefaultOptionTradeFormInputId.stopPrice to stopPrice,
                DefaultOptionTradeFormInputId.term to DefaultOptionTradeFormInputId.termValueId.term()
            )
        }
    }

    private val Map<String, Any?>.normalized: Map<String, Any?>
        get() {
            val normalized = toMutableMap()
            @Suppress("UNCHECKED_CAST") val value = (normalized.remove("value") as Map<String, Any?>).toMutableMap().apply {
                val tradeLegs = (remove(DefaultOptionTradeFormInputId.tradeLegs) as List<LegDetailsCommonDto>).map {
                    mapOf(
                        "quantity" to it.quantity,
                        "symbol" to it.symbol,
                        "transaction" to it.transactionCode
                    )
                }
                put(DefaultOptionTradeFormInputId.tradeLegs, tradeLegs)
            }.toMap()
            normalized["value"] = value
            return normalized.toMap()
        }

    private interface TestCase {
        val params: Map<String, String?>
        val expected: Map<String, Any?>
    }
}
