package com.etrade.mobilepro.chart.netassets.data

import com.etrade.mobilepro.chart.netassets.model.NetAssetsChartData
import com.etrade.mobilepro.util.Resource

interface NetAssetsChartRepo {
    suspend fun getNetAssetsChart(accounts: List<String>): Resource<NetAssetsChartData>
}
