package com.etrade.mobilepro.chat.api.message.models

import com.etrade.mobilepro.chat.api.ChatMetadataModel

sealed class ChatRequestModel {

    class GetMessages(val sessionId: String, val metadata: ChatMetadataModel?) : ChatRequestModel()
    class TerminateChat(val sessionID: String, val metadata: ChatMetadataModel?) : ChatRequestModel()
    class PostMessage(val sessionID: String, val metadata: ChatMetadataModel?, val message: String) : ChatRequestModel()
}
