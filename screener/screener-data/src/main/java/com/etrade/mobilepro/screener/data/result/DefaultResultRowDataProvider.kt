package com.etrade.mobilepro.screener.data.result

import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.screener.api.result.PrescreenedQuote
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.util.color.DataColorResolver
import javax.inject.Inject

class DefaultResultRowDataProvider @Inject constructor(
    private val mfMapper: FundQuoteDetailItemsMapper,
    private val dataColorResolver: DataColorResolver
) : ResultRowDataProvider {
    override fun getRowData(
        columnOrder: Collection<ResultColumn>,
        entry: PrescreenedQuote?
    ): List<ResultDataCell> {
        val prescreenedQuote = entry ?: return emptyList()
        return columnOrder.map { it.createCell(prescreenedQuote, mfMapper, dataColorResolver) }
    }

    override fun updateRowData(row: List<ResultDataCell>, data: Level1Data, columns: List<ResultColumn>): List<ResultDataCell> {
        return columns.mapIndexed { index: Int, resultColumn: ResultColumn ->
            resultColumn.updateCell(row[index], data, dataColorResolver)
        }
    }
}
