package com.etrade.mobilepro.balance

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ScrollOnTopListener
import com.etrade.mobilepro.databinding.FragmentBalancesBinding
import com.etrade.mobilepro.dynamicui.GenericRecyclerAdapter
import com.etrade.mobilepro.dynamicui.api.CtaEvent.NavDirectionsNavigation
import com.etrade.mobilepro.dynamicui.api.CtaEvent.RetryEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.toDynamicViewModels
import com.etrade.mobilepro.home.AccountsTabDataDelegate
import com.etrade.mobilepro.home.AccountsTabHostFragmentArgs
import com.etrade.mobilepro.home.OnAccountsTabSelectionListener
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@RequireLogin
internal class BalancesFragment @Inject constructor(
    val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.fragment_balances), ScrollOnTopListener, OnAccountsTabSelectionListener {

    private val binding: FragmentBalancesBinding by viewBinding(FragmentBalancesBinding::bind) { onDestroyBinding() }

    private val accountId =
        lazy { arguments?.let { AccountsTabHostFragmentArgs.fromBundle(it).accountId } }

    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    private val balancesViewModel: BalancesViewModel by viewModels { viewModelFactory }

    private val viewAdapter: GenericRecyclerAdapter
        get() = binding.balancesRecyclerView.adapter as GenericRecyclerAdapter

    private var snackBar: Snackbar? = null

    private val dataDelegate = AccountsTabDataDelegate {
        accountId.value?.let {
            balancesViewModel.fetchBalances(it)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
        binding.swipeRefresh.setOnRefreshListener {
            balancesViewModel.refresh()
        }

        dataDelegate.initialize(savedInstanceState != null)
    }

    private fun FragmentBalancesBinding.onDestroyBinding() {
        viewAdapter.disposeAll()
        balancesRecyclerView.adapter = null
        binding.swipeRefresh.setOnRefreshListener(null)
    }

    private fun setupFragment() {
        with(binding.balancesRecyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = GenericRecyclerAdapter(this@BalancesFragment.viewLifecycleOwner, emptyList())
        }
    }

    private fun bindViewModel() {
        balancesViewModel.viewState.observe(
            this@BalancesFragment.viewLifecycleOwner,
            Observer {
                when (it) {
                    is BalancesViewModel.ViewState.Loading -> {
                        binding.balancesLoadingIndicator.show()
                    }
                    is BalancesViewModel.ViewState.Error -> {
                        binding.balancesLoadingIndicator.hide()
                        binding.swipeRefresh.isRefreshing = false
                        snackBar = snackBarUtil.retrySnackbar { balancesViewModel.refresh() }
                    }

                    is BalancesViewModel.ViewState.Success -> {
                        binding.balancesLoadingIndicator.hide()
                        snackBar?.dismiss()
                        binding.swipeRefresh.isRefreshing = false
                        viewAdapter.values = it.views.toDynamicViewModels(
                            this,
                            binding.swipeRefresh.isRefreshing
                        ) { viewModel ->
                            observeClickEvents(viewModel)
                        }
                    }
                }
            }
        )
    }

    private fun observeClickEvents(viewModel: DynamicViewModel) {
        viewModel.clickEvents.observe(
            this@BalancesFragment.viewLifecycleOwner,
            Observer { ctaEvent ->
                when (ctaEvent) {
                    is NavDirectionsNavigation -> {
                        findNavController().navigate(ctaEvent.ctaDirections)
                    }
                    is RetryEvent -> {
                        snackBar = snackBarUtil.retrySnackbar {
                            // retry balances service call
                            viewModel.onCtaClicked(null)
                        }
                    }
                }
            }
        )
    }

    override val isOnTop get() = binding.balancesRecyclerView.computeVerticalScrollOffset() == 0

    override fun scrollUp() {
        binding.balancesRecyclerView.apply {
            stopScroll()
            smoothScrollToPosition(0)
        }
    }

    override fun onTabSelected() {
        val isFragmentReady = view != null
        dataDelegate.fetchData(isFragmentReady)
        if (isFragmentReady) {
            binding.balancesRecyclerView.bringToFront()
        }
    }
}
