package com.etrade.mobilepro.tier

import com.etrade.mobilepro.common.result.ETResult

interface CustomerTierRepo {

    suspend fun getTier(): ETResult<CustomerTier>
}
