package com.etrade.mobilepro.screener.presentation.stock.technicals

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.stock.TechnicalsFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.databinding.FragmentTechnicalsBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.hideSoftKeyboard
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

private const val MAX_PERCENT_VALUE = 100

class TechnicalsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<TechnicalsFilter, TechnicalsViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {

    private val binding by viewBinding(FragmentTechnicalsBinding::bind)
    override val layoutRes: Int = R.layout.fragment_technicals
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.STOCK
    override val viewModel by viewModels<TechnicalsViewModel> { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
    }

    @Suppress("LongMethod")
    private fun setupFragment() {
        binding.percentLow.apply {
            setupView(
                dependencies = viewModel.lowPercentViewDependencies,
                rawContentData = viewModel.lowPercentMutableData,
                maxValue = MAX_PERCENT_VALUE
            ) {
                it.hideSoftKeyboard()
                it.clearFocus()
            }

            lastCompleteValue.observe(
                viewLifecycleOwner,
                Observer {
                    viewModel.lowPercentChanged(it)
                }
            )
        }

        binding.percentHigh.apply {
            setupView(
                dependencies = viewModel.highPercentViewDependencies,
                rawContentData = viewModel.highPercentMutableData,
                maxValue = MAX_PERCENT_VALUE
            ) {
                it.hideSoftKeyboard()
                it.clearFocus()
            }

            lastCompleteValue.observe(
                viewLifecycleOwner,
                Observer {
                    viewModel.highPercentChanged(it)
                }
            )
        }
    }
}
