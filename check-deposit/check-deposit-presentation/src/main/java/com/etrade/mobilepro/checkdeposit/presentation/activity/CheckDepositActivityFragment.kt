package com.etrade.mobilepro.checkdeposit.presentation.activity

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.checkdeposit.activity.service.DepositActivityItemDto
import com.etrade.mobilepro.checkdeposit.presentation.CheckDepositLandingFragmentDirections
import com.etrade.mobilepro.checkdeposit.presentation.CheckDepositNavigationFlowViewModel
import com.etrade.mobilepro.checkdeposit.presentation.R
import com.etrade.mobilepro.checkdeposit.presentation.databinding.FragmentCheckDepositActivityBinding
import com.etrade.mobilepro.checkdeposit.presentation.deposit.CheckDepositViewModel
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.disposeOnDestinationChanged
import com.etrade.mobilepro.livedata.concatCombine
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.addDividerItemDecoration
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

@RequireLogin
class CheckDepositActivityFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.fragment_check_deposit_activity) {

    private val binding by viewBinding(FragmentCheckDepositActivityBinding::bind)

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)
    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { binding.depositActivityContainer }) }
    private val viewModel by viewModels<CheckDepositActivityViewModel> { viewModelFactory }
    private val navigationFlowViewModel: CheckDepositNavigationFlowViewModel by navGraphViewModels(R.id.check_deposit_graph)
    private val checkDepositViewModel: CheckDepositViewModel by navGraphViewModels(R.id.check_deposit_graph) { viewModelFactory }
    private val checkDepositActivityAdapter = CheckDepositActivityAdapter(
        depositActivityItemListener = { deposit, accountName ->
            openDetails(deposit, accountName)
        }
    )

    private val checkDepositActivityObserver = Observer<CheckDepositActivityViewModel.ViewState> {
        when (it) {
            CheckDepositActivityViewModel.ViewState.Loading -> {
                showLoadingState()
            }
            is CheckDepositActivityViewModel.ViewState.Error -> {
                showErrorState(it)
            }
        }
    }

    private val disposeListener = disposeOnDestinationChanged { checkDepositViewModel.disposeState() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentSetup()
        bindViewModels()
    }

    override fun onResume() {
        super.onResume()
        findNavController().addOnDestinationChangedListener(disposeListener)
    }

    override fun onPause() {
        super.onPause()
        findNavController().removeOnDestinationChangedListener(disposeListener)
    }

    private fun fragmentSetup() {
        binding.swipeToRefreshDepositActivity.apply {
            applyColorScheme()
            setOnRefreshListener {
                viewModel.loadDepositActivityItems()
            }
        }
        with(binding.checkDepositActivityRecyclerView) {
            layoutManager = LinearLayoutManager(activity).apply { orientation = RecyclerView.VERTICAL }
            addDividerItemDecoration(R.drawable.thin_divider)
            adapter = checkDepositActivityAdapter
        }
    }

    private fun bindViewModels() {
        viewModel.viewState.observe(viewLifecycleOwner, checkDepositActivityObserver)

        val activityResult = checkDepositViewModel.accounts.concatCombine(viewModel.viewState) { accounts, state ->
            if (state is CheckDepositActivityViewModel.ViewState.Success && accounts.isNotEmpty()) {
                CombinedDepositActivityResult(state.depositActivityItems.addAccounts(accounts), true)
            } else {
                CombinedDepositActivityResult(emptyList(), false)
            }
        }

        activityResult.observe(
            viewLifecycleOwner,
            Observer { result ->
                if (result.isSuccessfullyReceived) {
                    showSuccessState(result.items)

                    navigationFlowViewModel.checkDepositRequestId?.also { requestId ->
                        result.items
                            .firstOrNull { it.depositActivityItem.requestId == requestId }
                            ?.also { openDetails(it.depositActivityItem, it.uiDepositAccount) }
                    }
                }
            }
        )

        navigationFlowViewModel.refreshDepositActivity.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.run { viewModel.loadDepositActivityItems() }
            }
        )
    }

    private fun showLoadingState() {
        with(binding) {
            noDepositActivity.visibility = View.GONE
            if (!swipeToRefreshDepositActivity.isRefreshing) {
                loadingIndicator.visibility = View.VISIBLE
                checkDepositActivityRecyclerView.visibility = View.GONE
            }
        }
    }

    private fun showErrorState(viewState: CheckDepositActivityViewModel.ViewState.Error) {
        with(binding) {
            noDepositActivity.visibility = View.GONE
            checkDepositActivityRecyclerView.visibility = View.GONE
            loadingIndicator.visibility = View.GONE

            snackbarUtil
                .retrySnackbar {
                    viewModel.loadDepositActivityItems()
                }

            logger.error(viewState.message)
            swipeToRefreshDepositActivity.isRefreshing = false
        }
    }

    private fun showSuccessState(resultItems: List<DepositActivityResultItem>) {
        with(binding) {
            if (resultItems.isEmpty()) {
                noDepositActivity.visibility = View.VISIBLE
                checkDepositActivityRecyclerView.visibility = View.GONE
                loadingIndicator.visibility = View.GONE
            } else {
                noDepositActivity.visibility = View.GONE
                checkDepositActivityRecyclerView.visibility = View.VISIBLE
                loadingIndicator.visibility = View.GONE
            }

            checkDepositActivityAdapter.addItems(resultItems)

            if (swipeToRefreshDepositActivity.isRefreshing) {
                checkDepositActivityRecyclerView.scrollToPosition(0)
                swipeToRefreshDepositActivity.isRefreshing = false
            }
        }
    }

    private fun openDetails(deposit: DepositActivityItemDto, accountName: String) {
        navigate(CheckDepositLandingFragmentDirections.openDepositDetails(deposit, accountName))
    }

    fun navigate(destination: NavDirections) = with(findNavController()) {
        currentDestination?.getAction(destination.actionId)
            ?.let { navigate(destination) }
    }

    data class CombinedDepositActivityResult(
        val items: List<DepositActivityResultItem>,
        val isSuccessfullyReceived: Boolean
    )
}
