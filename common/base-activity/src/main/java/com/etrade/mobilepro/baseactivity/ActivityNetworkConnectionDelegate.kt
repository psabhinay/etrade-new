package com.etrade.mobilepro.baseactivity

import androidx.fragment.app.FragmentActivity

interface ActivityNetworkConnectionDelegate {
    fun initNetworkConnectivityBanner(activity: FragmentActivity)
}
