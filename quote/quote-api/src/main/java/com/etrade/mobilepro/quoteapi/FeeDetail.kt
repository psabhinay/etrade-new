package com.etrade.mobilepro.quoteapi

data class FeeDetail(
    val percent: String,
    val range: String
)
