package com.etrade.mobilepro.barcode

import android.app.Activity
import android.os.Handler
import android.os.Message
import androidx.camera.core.AspectRatio
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageCapture
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.common.extension.screenHeightPixels
import com.etrade.mobilepro.common.extension.screenWidthPixels
import com.google.mlkit.vision.barcode.Barcode
import java.util.concurrent.ExecutorService
import kotlin.math.abs
import kotlin.math.ln
import kotlin.math.max
import kotlin.math.min

private const val RATIO_4_3_VALUE = 4.0 / 3.0
private const val RATIO_16_9_VALUE = 16.0 / 9.0
private const val MESSAGE_ID = 4242

class BarcodeReaderHandler(
    private val previewView: PreviewView,
    private val scanCallback: (Barcode) -> Unit,
    private val activity: Activity
) {
    private val handler: Handler = Handler(activity.mainLooper)
    private lateinit var message: Message

    @Suppress("LongMethod")
    private fun setUpCamera(executor: ExecutorService) {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(activity)
        val r = Runnable {
            cameraProviderFuture.addListener(
                {
                    val cameraProvider = cameraProviderFuture.get()
                    val screenAspectRatio = aspectRatio(
                        activity.screenWidthPixels,
                        activity.screenHeightPixels,
                    )
                    val rotation = previewView.display.rotation
                    val cameraSelector = CameraSelector.Builder().requireLensFacing(CameraSelector.LENS_FACING_BACK).build()
                    val preview = Preview.Builder()
                        .setTargetAspectRatio(screenAspectRatio)
                        .setTargetRotation(rotation)
                        .build()
                    val imageCapture = ImageCapture.Builder()
                        .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                        .setTargetAspectRatio(screenAspectRatio)
                        .setTargetRotation(rotation)
                        .build()
                    val imageAnalyzer = ImageAnalysis.Builder()
                        .setTargetAspectRatio(screenAspectRatio)
                        .setTargetRotation(rotation)
                        .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                        .build()
                        .also { it.setAnalyzer(executor, BarCodeAnalyzer(scanCallback)) }
                    cameraProvider.unbindAll()
                    preview.setSurfaceProvider(previewView.surfaceProvider)
                    cameraProvider.bindToLifecycle(activity as LifecycleOwner, cameraSelector, preview, imageCapture, imageAnalyzer)
                },
                ContextCompat.getMainExecutor(activity)
            )
        }
        message = Message.obtain(handler, r)
        message.what = MESSAGE_ID
        handler.sendMessageDelayed(message, 0)
    }

    private fun aspectRatio(width: Int, height: Int): Int {
        val previewRatio = ln(max(width, height).toDouble() / min(width, height))
        if (abs(previewRatio - ln(RATIO_4_3_VALUE))
            <= abs(previewRatio - ln(RATIO_16_9_VALUE))
        ) {
            return AspectRatio.RATIO_4_3
        }
        return AspectRatio.RATIO_16_9
    }

    fun start(executor: ExecutorService) {
        setUpCamera(executor)
    }

    fun stop() {
        handler.removeCallbacksAndMessages(null)
    }
}
