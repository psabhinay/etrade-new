package com.etrade.mobilepro.trade.data.contingent

import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrder
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderPreviewResponse
import com.etrade.mobilepro.trade.data.TradeOrderArguments
import com.etrade.mobilepro.trade.data.TradeRequestParamsUpdater
import com.etrade.mobilepro.trade.data.advanced.AdvancedTradingApiClient
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithContingentOrderInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.squareup.moshi.Moshi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest

@ExperimentalCoroutinesApi
internal abstract class AbstractContingentOrderTradeDelegateTest<T> where T : WithContingentOrderInputId,
                                                                          T : WithAdvancedTradeFormInputId,
                                                                          T : WithPriceTradeFormInputId,
                                                                          T : WithActionTradeFormInputId {

    private val fakeClient: AdvancedTradingApiClient = mock {
        runBlocking {
            whenever(it.previewOrder(any(), any())).thenReturn(mock())
            whenever(it.placeOrder(any(), any())).thenReturn(mock())
        }
    }

    abstract val inputId: T

    abstract val requestParamUpdater: TradeRequestParamsUpdater

    abstract val orderType: OrderType

    protected fun comparePreviewInputAndOutput(input: Map<String, String>, output: Map<String, String>) {
        runBlockingTest {
            createDelegate().previewOrder(input, MarketDestination.AUTO)
        }
        runBlockingTest {
            verify(fakeClient).previewOrder(AdvancedTradingApiClient.PATH_CONTINGENT, output)
        }
    }

    protected fun comparePlaceInputAndOutput(input: Map<String, String>, output: Map<String, String>, previewId: String) {
        runBlockingTest {
            val argument = TradeOrderArguments(input, MarketDestination.AUTO)
            createDelegate().placeOrder(argument, createTradeOrder(previewId))
        }
        runBlockingTest {
            verify(fakeClient).placeOrder(AdvancedTradingApiClient.PATH_CONTINGENT, output)
        }
    }

    private fun createDelegate() = ContingentOrderTradeDelegate(inputId, fakeClient, requestParamUpdater, Moshi.Builder().build())

    private fun createTradeOrder(previewId: String): TradeOrder {
        val response = mock<AdvancedOrderPreviewResponse>()
        whenever(response.previewId).thenReturn(previewId)
        val order = mock<AdvancedOrder>()
        whenever(order.orderType).thenReturn(orderType)
        whenever(response.orders).thenReturn(listOf(order))
        return TradeOrder(previewId, "", response)
    }
}
