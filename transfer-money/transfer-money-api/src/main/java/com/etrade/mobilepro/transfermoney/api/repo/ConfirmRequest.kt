package com.etrade.mobilepro.transfermoney.api.repo

import com.etrade.mobilepro.transfermoney.api.TransferRepeat

data class ConfirmRequest(
    val userId: String,
    val scheduleDate: String,
    val fromAcctId: String,
    val toAcctId: String,
    val amount: String,
    val frequency: TransferRepeat,
    val platform: String,
    val iRAContribYear: String?
)
