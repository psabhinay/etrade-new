package com.etrade.mobilepro.tooltip

import android.graphics.Rect
import android.os.Build
import android.text.format.DateUtils
import android.view.View
import it.sephiroth.android.library.xtooltip.ClosePolicy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import it.sephiroth.android.library.xtooltip.Tooltip as ThirdPartyTooltip

private const val TOOLTIP_SHOW_DURATION = 10 * DateUtils.SECOND_IN_MILLIS
private const val TOOLTIP_DISMISS_DELAY = 100L
const val TOOLTIP_INDETERMINATE_DURATION = -1L
const val TOOLTIP_SHOW_DURATION_3SEC = 3 * DateUtils.SECOND_IN_MILLIS

class Tooltip {

    private var tooltip: ThirdPartyTooltip? = null
    private var timeOutJob: Job? = null

    fun show(block: Builder.() -> Unit) = Builder().apply(block).run { show(this) }

    private fun show(builder: Builder) {
        if (!builder.isInitialized) { return }
        with(builder) {
            tooltip = ThirdPartyTooltip.Builder(target.context)
                .anchor(target, xOffset, yOffset, true)
                .maxWidth(target.context.resources.displayMetrics.widthPixels - (target.context.resources.displayMetrics.density * 2).toInt())
                .text(text)
                .closePolicy(getClosePolicy())
                .overlay(false)
                .styleId(R.style.ToolTipStyle)
                .also { setupDuration() }
                .create().apply {
                    doOnTouchInside { onDismiss(DismissType.TouchInside) }
                    doOnTouchOutside { onDismiss(DismissType.TouchOutside) }
                    doOnHidden { onDismiss(DismissType.Unknown) }
                    doOnShown {
                        val tooltipView = it.contentView
                        tooltipView?.announceForAccessibility("${target.contentDescription} tooltip, ${this@with.text.replace("\n", " ")}")
                        // ETAND-17333, ETAND-17531
                        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.R) {
                            (tooltipView?.parent as? View)?.fitsSystemWindows = false
                        }
                    }
                    show(target, this@with.gravity.toXTooltipGravity())
                }
        }
    }

    fun dismiss() {
        tooltip?.dismiss()
    }

    fun containsTooltip(x: Float, y: Float): Boolean {
        val outRect = Rect()
        tooltip?.contentView?.getGlobalVisibleRect(outRect)

        return outRect.contains(x.toInt(), y.toInt())
    }

    private fun TooltipGravity.toXTooltipGravity() = when (this) {
        TooltipGravity.TOP -> ThirdPartyTooltip.Gravity.TOP
        TooltipGravity.BOTTOM -> ThirdPartyTooltip.Gravity.BOTTOM
        TooltipGravity.LEFT -> ThirdPartyTooltip.Gravity.LEFT
        TooltipGravity.RIGHT -> ThirdPartyTooltip.Gravity.RIGHT
    }

    private fun Builder.onDismiss(type: DismissType) {
        timeOutJob?.cancel()
        tooltip?.dismiss(TOOLTIP_DISMISS_DELAY)
        when {
            onDismiss != null -> { onDismiss?.invoke() }
            type == DismissType.TouchInside -> { onExplicitDismiss?.invoke() }
            type == DismissType.TimeOut ||
                type == DismissType.TouchOutside ||
                type == DismissType.Unknown -> { onImplicitDismiss?.invoke() }
        }
        dispose()
    }

    private fun Builder.getClosePolicy(): ClosePolicy {
        return when {
            shouldConsumeDismissEvent -> { ClosePolicy.TOUCH_INSIDE_CONSUME }
            else -> { ClosePolicy.TOUCH_NONE }
        }
    }

    private fun Builder.setupDuration() {
        if (durationParams?.duration == TOOLTIP_INDETERMINATE_DURATION) { return }

        durationParams?.let { params ->
            timeOutJob = params.coroutineScope.launch {
                delay(params.duration)
                onDismiss(DismissType.TimeOut)
            }
        }
    }

    private fun Builder.dispose() {
        tooltip = null
        timeOutJob = null
        onDismiss = null
        onExplicitDismiss = null
        onImplicitDismiss = null
    }

    class Builder {
        lateinit var target: View
        var text: String = ""
        var gravity: TooltipGravity = TooltipGravity.BOTTOM
        var xOffset: Int = 0
        var yOffset: Int = 0
        var durationParams: DurationParams? = null

        /**
         * If true, the tooltip will consume a touch event through its dismiss event. It will prevent
         * other views or components to receive a touch event unless the tooltip is already dismissed.
         */
        var shouldConsumeDismissEvent: Boolean = true

        /**
         * Called when the user explicitly dismisses the tooltip.
         */
        var onExplicitDismiss: (() -> Unit)? = null
        /**
         * Call when dismissing the tooltip without user interaction.
         */
        var onImplicitDismiss: (() -> Unit)? = null
        /**
         * Called whenever [onImplicitDismiss] or [onExplicitDismiss] is called.
         */
        var onDismiss: (() -> Unit)? = null
        val isInitialized: Boolean get() { return ::target.isInitialized }
    }

    class DurationParams(
        val coroutineScope: CoroutineScope,
        val duration: Long = TOOLTIP_SHOW_DURATION
    )
}

fun showTooltip(block: Tooltip.Builder.() -> Unit): Tooltip {
    return Tooltip().apply { show(block) }
}

sealed class TooltipGravity {
    object TOP : TooltipGravity()
    object BOTTOM : TooltipGravity()
    object LEFT : TooltipGravity()
    object RIGHT : TooltipGravity()
}

sealed class DismissType {
    object TouchInside : DismissType()
    object TouchOutside : DismissType()
    object TimeOut : DismissType()
    /**
     * May be triggered by pressing back button from software or hardware input.
     */
    object Unknown : DismissType()
}
