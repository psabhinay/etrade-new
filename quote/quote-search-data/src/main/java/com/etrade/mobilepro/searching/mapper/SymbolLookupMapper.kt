package com.etrade.mobilepro.searching.mapper

import com.etrade.mobilepro.instrument.ExpirationDate
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.SettlementSessionCode
import com.etrade.mobilepro.searching.dto.SymbolLookupResponseDto
import com.etrade.mobilepro.searchingapi.items.ErrorInfo
import com.etrade.mobilepro.searchingapi.items.OptionDate
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SymbolLookUpResult

class SymbolLookupMapper {
    private val months = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec")

    fun map(symbolLookupResponse: SymbolLookupResponseDto?, symbol: SearchQuery): SymbolLookUpResult {
        val symbolList = symbolLookupResponse?.symbolLookupList?.map { item ->
            SearchResultItem.Symbol(
                title = item.symbol ?: "",
                instrumentId = "",
                description = symbol.description,
                type = item.productType?.let { InstrumentType.valueOf(it) } ?: InstrumentType.UNKNOWN,
                cachedAt = System.currentTimeMillis(),
                queryValue = symbol.value,
                expire = "${item.expire?.month} ${item.expire?.day} '${item.expire?.year?.substring(2)}",
                strike = item.strike ?: "",
                settlementSessionCd = SettlementSessionCode.from(item.settlementSessionCd),
                displaySymbol = item.symbolDisplay ?: ""
            )
        }

        val optionMonths = symbolLookupResponse?.optionMonthsList
        val dateList = optionMonths?.dateList?.mapNotNull { item ->
            val year = item.year
            val month = item.month
            val day = item.day
            if (year == null || month == null || day == null) {
                null
            } else {
                OptionDate(
                    ExpirationDate(year = year, month = month, day = day, settlementSessionCode = SettlementSessionCode.from(item.settlementSessionCd)),
                    formattedDate(month, day, year)
                )
            }
        }
        val errorDto = symbolLookupResponse?.result?.fault?.errors

        val errorInfo = errorDto?.run {
            getOrNull(0)?.run {
                ErrorInfo(
                    errorCode,
                    errorTypeInt,
                    errorDescription
                )
            }
        }

        return SymbolLookUpResult(
            symbolList ?: emptyList(),
            dateList ?: emptyList(),
            errorInfo
        )
    }

    private fun formattedDate(month: Int, day: Int, year: Int): String {
        val formatterMonth = months[month - 1]
        val formatterYear = year.toString().substring(2)
        return "$formatterMonth $day '$formatterYear"
    }
}
