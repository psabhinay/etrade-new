package com.etrade.mobilepro.marketscalendar.viewmodel

import android.content.res.Resources
import com.etrade.mobilepro.marketscalendar.api.model.EventsEarnings
import com.etrade.mobilepro.marketscalendar.api.model.EventsRequest
import com.etrade.mobilepro.marketscalendar.api.model.EventsResponse
import com.etrade.mobilepro.marketscalendar.api.repo.MarketsCalendarRepo
import com.etrade.mobilepro.marketscalendar.data.dto.EARNINGS_EVENT
import com.etrade.mobilepro.marketscalendar.util.getCalendarEventTitle
import io.reactivex.Single
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter
import java.text.DecimalFormat
import javax.inject.Inject

class EarningsEventsMarketsCalendarViewModel @Inject constructor(
    marketsCalendarRepo: MarketsCalendarRepo,
    sharedCalendarState: SharedCalendarState,
    private val resources: Resources
) : MarketsCalendarResultViewModel<EventsEarnings>(marketsCalendarRepo, sharedCalendarState) {

    override val eventType: String = EARNINGS_EVENT

    override val eventFilterEnable: String? = null

    override val marketsEventsRequest: MarketsCalendarRepo.(EventsRequest) -> Single<EventsResponse<EventsEarnings>> = MarketsCalendarRepo::getEventsEarningsRx

    override fun mapEventItem(item: EventsEarnings): EventDetails {
        return EventDetails(
            item.symbol?.let(resources::getCalendarEventTitle),
            getEventDate(item, EVENTS_DATE_TIME_FORMATTER),
            item.epsestMean,
            item.epsactualValue,
            getEventSurprise(item, bigDecimalValueTemplate),
            eventType,
            symbol = item.symbol
        )
    }

    private fun getEventDate(item: EventsEarnings, formatter: DateTimeFormatter): String? {
        return if (selectedDate.isBefore(currentDate)) {
            item.epsannounceDate?.let { epsannounceDate ->
                item.epsannounceTime?.let { epsannounceTime ->
                    val date: LocalDate = LocalDate.parse(epsannounceDate, DateTimeFormatter.BASIC_ISO_DATE)
                    val time: LocalTime = LocalTime.parse(epsannounceTime, DateTimeFormatter.ISO_LOCAL_TIME)

                    val dateTime: LocalDateTime = LocalDateTime.of(date, time)

                    dateTime.atZone(nyTimeZone).format(formatter)
                }
            }
        } else {
            item.dateTime?.let {
                val time: LocalDateTime = LocalDateTime.parse(it, EVENTS_FULL_DATE_TIME_FORMATTER)

                return time.atZone(nyTimeZone).format(formatter)
            }
        }
    }

    private fun getEventSurprise(item: EventsEarnings, doubleValueTemplate: DecimalFormat): String? {
        return item.epsactualValue?.let { actual ->
            item.epsestMean?.let { estimate ->
                doubleValueTemplate.format(actual.toBigDecimal() - estimate.toBigDecimal())
            }
        }
    }
}
