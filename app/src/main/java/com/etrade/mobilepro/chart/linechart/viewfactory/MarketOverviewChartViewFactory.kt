package com.etrade.mobilepro.chart.linechart.viewfactory

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.chart.data.ChartParams
import com.etrade.mobilepro.chart.data.GetChartLineDataUseCase
import com.etrade.mobilepro.chart.linechart.dto.MarketChartViewDto
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class MarketOverviewChartViewFactory @Inject constructor(
    private val getChartLineDataUseCase: GetChartLineDataUseCase,
    private val markets: List<ChartParams>,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController
) : DynamicUiViewFactory {

    val logger: Logger = LoggerFactory.getLogger(MarketOverviewChartViewFactory::class.java)

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is MarketChartViewDto) {
            return object : GenericLayout {

                override val viewModelClass = MarketOverviewChartViewModel::class.java

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    toMarketChartOverview()
                }

                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is MarketOverviewChartViewModel) {
                        viewModel.retry()
                    } else {
                        logger.error("view model is not of type MarketOverviewChartViewModel")
                    }
                }

                override val uniqueIdentifier: String = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("MarketOverviewChartViewFactory registered to unknown type")
        }
    }

    private fun toMarketChartOverview(): MarketOverviewChartViewModel {
        return MarketOverviewChartViewModel(getChartLineDataUseCase, markets, streamingQuoteProvider, streamingStatusController)
    }
}
