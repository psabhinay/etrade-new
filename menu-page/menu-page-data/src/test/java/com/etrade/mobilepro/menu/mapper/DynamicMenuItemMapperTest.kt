package com.etrade.mobilepro.menu.mapper

import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.menu.dto.MenuDto
import com.etrade.mobilepro.util.Resource
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class DynamicMenuItemMapperTest {

    private val baseUrl = "https://us.uat.etrade.com"
    private lateinit var dynamicMenuApiResponse: Resource<BaseMobileResponse>
    private lateinit var initialMenuDto: List<MenuDto>

    @Before
    fun setup() {
        dynamicMenuApiResponse = getDynamicMenuResponse()
        @Suppress("UNCHECKED_CAST")
        initialMenuDto = (dynamicMenuApiResponse.data?.screen?.views as? List<MenuDto>).orEmpty()
    }

    @Test
    fun `map base mobile response to dynamic menu item list`() {
        val dynamicMenuApiResponseResult = ETResult.success(dynamicMenuApiResponse)
        val dynamicMenuItemResult = dynamicMenuApiResponseResult.mapToDynamicMenuItemListResult(baseUrl)
        dynamicMenuItemResult.onSuccess {
            initialMenuDto.verifyDynamicMenuItems(it)
        }
    }

    @Test
    fun `map To Dynamic MenuItems`() {
        @Suppress("UNCHECKED_CAST")
        val menuDtos: List<MenuDto> = (dynamicMenuApiResponse.data?.screen?.views as? List<MenuDto>).orEmpty()
        val dynamicMenuItems = menuDtos.mapToDynamicMenuItems(baseUrl)
        initialMenuDto.verifyDynamicMenuItems(dynamicMenuItems)
    }

    @Test
    fun `map to deeplink`() {
        val appUrl = "etrade://menu/transfer"
        val appUrlAction = ClickActionDto(appUrl = appUrl)
        assertEquals(appUrl, appUrlAction.mapToDeepLink(baseUrl, null))

        val webViewTitle = "Knowledge Base"
        val webViewUrl = "/knowledge/investing-basics"
        val webViewUrlActionDto = ClickActionDto(webViewUrl = webViewUrl)
        val webViewDeeplink = "etrade://webview?title=$webViewTitle&url=${baseUrl.plus(webViewUrl)}"
        assertEquals(
            webViewDeeplink,
            webViewUrlActionDto.mapToDeepLink(
                baseUrl = baseUrl,
                defaultTitle = webViewTitle
            )
        )

        val webUrl = "https://us.uat.etrade.com"
        val webUrlActionDto = ClickActionDto(webUrl = webUrl)
        val webDeepLink = "etrade://web/$webUrl"
        assertEquals(webDeepLink, webUrlActionDto.mapToDeepLink(baseUrl, null))

        val invalidActionDto = ClickActionDto()
        assertEquals(null, invalidActionDto.mapToDeepLink(baseUrl, null))
    }
}
