package com.etrade.mobilepro

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.quote.dto.QuoteStockDto
import com.etrade.mobilepro.quote.getFormattedLastPrice
import com.etrade.mobilepro.quote.mapper.MobileQuoteMapper
import com.etrade.mobilepro.quote.mapper.MobileQuoteWidgetItemMapper
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import org.junit.Assert
import org.junit.Test

internal class QuoteUtilTest {

    @Test
    fun `test formatted last price`() {
        val quotes = MobileQuoteMapper().map(getMockQuotes()!!)
        Assert.assertEquals(
            MarketDataFormatter.formatMarketDataCustom(quotes.quotePrice.lastPrice.toBigDecimalOrNull()),
            quotes.getFormattedLastPrice()
        )
    }

    @Test
    fun `test formatted last price during extended hours`() {
        val quotes = MobileQuoteMapper().map(getMockExtendedHoursQuotes()!!)
        Assert.assertEquals(
            MarketDataFormatter.formatMarketDataCustom(quotes.extendedQuoteHrDetail!!.lastPrice.toBigDecimalOrNull()),
            quotes.getFormattedLastPrice()
        )
    }

    private fun getMockQuotes(): QuoteStockDto? {
        return MobileQuoteWidgetItemMapper::class.getObjectFromJson<QuoteStockDto>("quotes.json")
    }

    private fun getMockExtendedHoursQuotes(): QuoteStockDto? {
        return MobileQuoteWidgetItemMapper::class.getObjectFromJson<QuoteStockDto>("extended_hours_quotes.json")
    }
}
