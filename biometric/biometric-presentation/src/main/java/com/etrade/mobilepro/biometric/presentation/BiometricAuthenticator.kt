package com.etrade.mobilepro.biometric.presentation

import javax.crypto.Cipher

interface BiometricAuthenticator {

    var onAuthenticationCancel: (() -> Unit)?
    var onAuthenticationFail: ((reason: Int?) -> Unit)?
    var onAuthenticationSuccess: ((Cipher) -> Unit)?

    fun authenticate(cipher: Cipher, type: BiometricPromptType, isUserInitiated: Boolean = USER_INITIATED_BY_DEFAULT)

    companion object {
        const val USER_INITIATED_BY_DEFAULT = true
    }
}
