**Caching library module**

General idea comes from [this guide](https://developer.android.com/jetpack/docs/guide#addendum)

CachedResource class delegates its data manipulating(fetching, storing, mapping) tasks implementation to external entity, 
represented by CachedResource.DataSourceDelegate interface. Resource refresh decision logic is delegated to external entity,
represented by CachedResource.DataSourceDelegate interface

Sample usage can be as follows:
```kotlin
class ArticlePresenter: CachedResource.DataSourceDelegate<ArticleDto, ArticleJsonDto, ArticleDbDto>, CachedResource.DataSourceDelegate<ArticleDto> {
    private val articleId = "sdfg-erg4-sfsfv-sfbvtrwas"
    private val cacheExpirationMs = 36000
    private val cachedArticle = CachedResource(articleId, this, this)
    
    ...
    
    fun getArticle(): Observable<Resource<ArticleDto>> = cachedArticle
        .asObservable()
        .subscribeOn(... IO)
        .observeOn(... UI)
    
    ...
    override fun mapNetworkResponse(itemFromNetwork: ArticleJsonDto): ArticleDto {
        return ArticleDto(content = itemFromNetwork.content)
    }
    
    override fun mapCacheResponse(itemFromCache: ArticleDbDto): ArticleDto {
        return ArticleDto(content = itemFromCache.content)
    }
    
    override fun fetchFromNetwork(id: Any): Single<ArticleJsonDto> {
        return retrofitApiClient.loadArticle(id as? String)
    }
    
    override fun fetchFromDb(id: Any): Single<ArticleDbDto> {
        return Single.fromCallable {
            return articleDao.getById(id as? String).firstOrNull() ?: throw NoSuchElementException()
        }
    }
    
    override fun putToCache(fetchedResult: ArticleDto): Single<ArticleDto> {
        return Single.fromCallable {
            return articleDao.upsert(fetchedResult)
        }
    }
        
    fun shouldRefresh(itemFromCache: ArticleDto): Boolean {
        return Date().time - itemFromCache.createdAt > cacheExpirationMs
    }
           
}

class ArticleFragment {
    @Inject
    private lateinit var presenter: ArticlePresenter

    ...
    
    fun showArticle() {
        presenter
            .getArticle()
            .subscribe({ articleResource ->
                when(articleResource) {
                    is Resource.Loading -> ...
                    is Resource.Failed -> ...
                    is Resource.Success -> ...
                }
            }, Throwable::printStackTrace)
    }
    
    ...
}
```
