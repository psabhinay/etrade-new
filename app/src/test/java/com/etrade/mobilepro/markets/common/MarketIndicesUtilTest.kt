package com.etrade.mobilepro.markets.common

import com.etrade.mobilepro.streaming.mock.streamer.MockOptionData
import com.etrade.mobilepro.streaming.mock.streamer.MockStockData
import org.junit.Assert
import org.junit.Test

class MarketIndicesUtilTest {

    @Test
    fun `test getSymbolRowInfo for stock data`() {
        val quote = MockStockData()
        val symbolRowInfo = getSymbolRowInfo(
            quote,
            "symbol",
            "22.22"
        )
        Assert.assertEquals(quote.dayChangePercent, symbolRowInfo.percentChange)
        Assert.assertEquals(quote.dayChange, symbolRowInfo.change)
    }

    @Test
    fun `test buildSymbolRowItem for options data`() {
        val quote = MockOptionData()
        val symbolRowInfo = getSymbolRowInfo(
            quote,
            "symbol",
            "22.22"
        )
        Assert.assertEquals(quote.changePercent, symbolRowInfo.percentChange)
        Assert.assertEquals(quote.change, symbolRowInfo.change)
    }
}
