package com.etrade.mobilepro.login.data

import android.security.keystore.KeyPermanentlyInvalidatedException
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.encryption.api.CipherAuthenticator
import com.etrade.mobilepro.encryption.api.EncryptionProvider
import com.etrade.mobilepro.encryption.api.SecretKeyMaterial
import com.etrade.mobilepro.encryption.api.SecretKeyRepo
import com.etrade.mobilepro.encryption.api.SymmetricAlgorithmSpec
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.suspendCancellableCoroutine
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import javax.crypto.Cipher
import javax.crypto.SecretKey
import kotlin.coroutines.resume

class LoginPreferencesRepoTest {

    @Test
    fun `check empty state`() {
        withLoginPreferencesRepo { repo, _ ->
            val username = repo.getUserName()
            assertTrue(username.isFailure)
            assertThrows<NoSuchElementException> { username.getOrThrow() }

            val password = repo.getUserPassword { it }
            assertTrue(password.isFailure)
            assertThrows<NoSuchElementException> { password.getOrThrow() }
        }
    }

    @Test
    fun `check setUserName encrypts the value`() {
        withLoginPreferencesRepo { repo, storage ->
            val username = "username"
            assertTrue(repo.setUserName(username).isSuccess)
            val encryptedUsername = storage.getStringValue(KEY_ENCRYPTED_USER_NAME)
            assertEquals(username.reversed(), encryptedUsername)
        }
    }

    @Test
    fun `check username lifecycle`() {
        withLoginPreferencesRepo { repo, _ ->
            val username = "username"
            assertTrue(repo.setUserName(username).isSuccess)
            val usernameResult = repo.getUserName()
            assertTrue(usernameResult.isSuccess)
            assertEquals(username, usernameResult.getOrThrow())
        }
    }

    @Test
    fun `check setPassword encrypts the value and saves hash`() {
        withLoginPreferencesRepo { repo, storage ->
            val password = "password"
            val result = repo.passwordOperation(false, { resume(it) }) {
                setUserPassword(password, it)
            }
            result.exceptionOrNull()?.printStackTrace()
            assertTrue(result.isSuccess)
            assertEquals("$password!", storage.getStringValue(KEY_PASSWORD_HASH))
            assertEquals(password.reversed(), storage.getStringValue(KEY_ENCRYPTED_PASSWORD))
        }
    }

    @Test
    fun `check authentication cancel interrupt operation`() {
        checkOperationInterruption<CancellationException> {
            cancel()
        }
    }

    @Test
    fun `check authentication error interrupt operation`() {
        checkOperationInterruption<CustomException> {
            cancel(CustomException())
        }
    }

    @Test
    fun `check password lifecycle`() {
        withLoginPreferencesRepo { repo, _ ->
            val password = "password"
            val setResult = repo.passwordOperation(false, { resume(it) }) {
                setUserPassword(password, it)
            }
            assertTrue(setResult.isSuccess)
            val getResult = repo.passwordOperation(false, { resume(it) }) {
                getUserPassword(it)
            }
            assertTrue(getResult.isSuccess)
            assertEquals(password, getResult.getOrThrow())
        }
    }

    @Test
    fun `check update password does not create new password if repo is empty`() {
        withLoginPreferencesRepo { repo, _ ->
            val result = repo.passwordOperation(false, { resume(it) }) {
                updateUserPasswordIfChanged("password", it)
            }
            assertTrue(result.isFailure)
            assertThrows<NoSuchElementException> { result.getOrThrow() }
        }
    }

    @Test
    fun `check update password does not save password if same`() {
        withLoginPreferencesRepo { repo, storage ->
            val password = "password"
            storage.putStringValue(KEY_PASSWORD_HASH, "$password!")
            storage.putStringValue(KEY_ENCRYPTED_PASSWORD, password.reversed())

            val result = repo.passwordOperation(true, { cancel() }) {
                updateUserPasswordIfChanged(password, it)
            }
            assertTrue(result.isSuccess)
        }
    }

    @Test
    fun `check update password saves new password`() {
        withLoginPreferencesRepo { repo, storage ->
            val oldPassword = "oldPassword"
            storage.putStringValue(KEY_PASSWORD_HASH, "$oldPassword!")
            storage.putStringValue(KEY_ENCRYPTED_PASSWORD, oldPassword.reversed())

            val newPassword = "newPassword"
            val result = repo.passwordOperation(false, { resume(it) }) {
                updateUserPasswordIfChanged(newPassword, it)
            }
            assertTrue(result.isSuccess)

            assertEquals("$newPassword!", storage.getStringValue(KEY_PASSWORD_HASH))
            assertEquals(newPassword.reversed(), storage.getStringValue(KEY_ENCRYPTED_PASSWORD))
        }
    }

    @Test
    fun `check switching of biometric clears encrypted password and its hash`() {
        withLoginPreferencesRepo { repo, storage ->
            val oldPassword = "oldPassword"
            storage.putStringValue(KEY_PASSWORD_HASH, "$oldPassword!")
            storage.putStringValue(KEY_ENCRYPTED_PASSWORD, oldPassword.reversed())

            repo.disableBiometricAuthentication()

            assertNull(storage.getStringValue(KEY_PASSWORD_HASH, null))
            assertNull(storage.getStringValue(KEY_ENCRYPTED_PASSWORD, null))
        }
    }

    @Test
    fun `check reset biometrics when key invalidated`() {
        val password = "password"

        withLoginPreferencesRepo { repo, storage ->
            storage.putStringValue(KEY_PASSWORD_HASH, "$password!")
            storage.putStringValue(KEY_ENCRYPTED_PASSWORD, password.reversed())

            val result = repo.passwordOperation(true, { cancel(KeyPermanentlyInvalidatedException()) }) {
                getUserPassword(it)
            }

            assertEquals(storage.getStringValue(KEY_PASSWORD_HASH), "")
            assertEquals(storage.getStringValue(KEY_ENCRYPTED_PASSWORD), "")

            assertTrue(result.isFailure)
            assertThrows<KeyPermanentlyInvalidatedException> { result.getOrThrow() }
        }
    }

    private inline fun <reified T : Throwable> checkOperationInterruption(noinline interruptWith: CancellableContinuation<Cipher>.(Cipher) -> Unit) {
        val password = "password"

        withLoginPreferencesRepo { repo, _ ->
            val result = repo.passwordOperation(true, interruptWith) {
                setUserPassword(password, it)
            }
            assertTrue(result.isFailure)
            assertThrows<T> { result.getOrThrow() }
        }

        withLoginPreferencesRepo { repo, storage ->
            storage.putStringValue(KEY_PASSWORD_HASH, "$password!")
            storage.putStringValue(KEY_ENCRYPTED_PASSWORD, password.reversed())

            val result = repo.passwordOperation(true, interruptWith) {
                getUserPassword(it)
            }

            assertTrue(result.isFailure)
            assertThrows<T> { result.getOrThrow() }
        }

        withLoginPreferencesRepo { repo, storage ->
            storage.putStringValue(KEY_PASSWORD_HASH, "$password!")
            storage.putStringValue(KEY_ENCRYPTED_PASSWORD, password.reversed())

            val result = repo.passwordOperation(true, interruptWith) {
                updateUserPasswordIfChanged("newPassword", it)
            }

            assertTrue(result.isFailure)
            assertThrows<T> { result.getOrThrow() }
        }
    }

    private fun withLoginPreferencesRepo(test: suspend (LoginPreferencesRepo, KeyValueStorage) -> Unit) {
        runBlocking {
            val keyValueStorage = InMemoryKeyValueStorage()
            val loginPreferencesRepo = LoginPreferencesRepoImp(
                keyValueStorage = keyValueStorage,
                encryptionProvider = DummyEncryptionProvider(),
                secretKeyRepo = DummySecretKeyRepo()
            )
            test(loginPreferencesRepo, keyValueStorage)
        }
    }

    private suspend fun <T> LoginPreferencesRepo.passwordOperation(
        locked: Boolean,
        proceed: CancellableContinuation<Cipher>.(Cipher) -> Unit,
        operation: suspend LoginPreferencesRepo.(CipherAuthenticator) -> ETResult<T>
    ): ETResult<T> {
        return operation { cipher ->
            suspendCancellableCoroutine { continuation ->
                (cipherToKeyMap[cipher] as? DummySecretKey)?.locked = locked
                continuation.proceed(cipher)
            }
        }
    }

    private class DummyEncryptionProvider : EncryptionProvider {

        override fun isSecureEncryptionSupported(): Boolean = true

        override suspend fun encrypt(plaintext: String, keyMaterial: SecretKeyMaterial, authenticate: CipherAuthenticator): String {
            return decrypt(plaintext, keyMaterial, authenticate)
        }

        override suspend fun decrypt(encrypted: String, keyMaterial: SecretKeyMaterial, authenticate: CipherAuthenticator): String {
            val dummySecretKey = keyMaterial.dummySecretKey
            val isInitiallyLocked = dummySecretKey.locked
            val cipher = Cipher.getInstance("AES")
            cipherToKeyMap[cipher] = dummySecretKey
            if (keyMaterial.isUserAuthenticationRequired) {
                authenticate(cipher)
            }
            cipherToKeyMap.remove(cipher)
            if (dummySecretKey.locked) {
                throw Exception("Key is locked")
            }
            return encrypted.reversed().also {
                dummySecretKey.locked = isInitiallyLocked
            }
        }

        override suspend fun hash(value: String): String = "$value!"

        private val SecretKeyMaterial.dummySecretKey: DummySecretKey
            get() = secretKey as DummySecretKey
    }

    private class CustomException : Exception()

    private class DummySecretKeyRepo : SecretKeyRepo {
        override val algorithmSpec: SymmetricAlgorithmSpec = mock()
        override val commonKey: SecretKeyMaterial = SecretKeyMaterial(DummySecretKey(false), false)
        override val secureKey: SecretKeyMaterial = SecretKeyMaterial(DummySecretKey(true), true)
        override fun recreateSecretKey() { }
    }

    private class DummySecretKey(var locked: Boolean = true) : SecretKey {
        override fun getAlgorithm(): String = mock()
        override fun getEncoded(): ByteArray = mock()
        override fun getFormat(): String = mock()
    }

    private class InMemoryKeyValueStorage : KeyValueStorage {

        val map: MutableMap<String, Any?> = mutableMapOf()

        override fun getBooleanValue(key: String, defaultValue: Boolean): Boolean = map[key] as Boolean? ?: defaultValue

        override fun getIntValue(key: String, defaultValue: Int): Int = map[key] as Int? ?: defaultValue

        override fun getStringValue(key: String, defaultValue: String?): String? = map[key] as String? ?: defaultValue

        override fun putBooleanValue(key: String, value: Boolean?) {
            map[key] = value
        }

        override fun putIntValue(key: String, value: Int) {
            map[key] = value
        }

        override fun putStringValue(key: String, value: String?) {
            map[key] = value
        }
    }

    companion object {
        private val cipherToKeyMap: MutableMap<Cipher, SecretKey> = mutableMapOf()
    }
}
