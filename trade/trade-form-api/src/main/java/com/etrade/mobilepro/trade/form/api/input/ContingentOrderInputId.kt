package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.trade.form.api.value.ConditionValueId
import com.etrade.mobilepro.trade.form.api.value.QualifierValueId

interface ContingentOrderInputId : WithSymbolTradeFormInputId {
    val contingentSymbol: String
    val condition: String
    val conditionValue: String
    val conditionValueId: ConditionValueId
    val qualifier: String
    val qualifierValueId: QualifierValueId
}
