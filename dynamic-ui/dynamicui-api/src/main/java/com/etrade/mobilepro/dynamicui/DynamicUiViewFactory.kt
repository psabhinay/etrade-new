package com.etrade.mobilepro.dynamicui

import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.ScreenRequest

class DynamicUiViewFactoryException(message: String) : Exception(message)

interface DynamicUiViewFactory {
    fun createView(dto: Any, meta: List<Any> = emptyList()): GenericLayout

    /**
     * Call this method before createView to ensure the certain request values are passed to the GenericLayout
     */
    fun setupValuesFromRequest(screenRequest: ScreenRequest) {
        /* implementation not needed for most requests */
    }
}
