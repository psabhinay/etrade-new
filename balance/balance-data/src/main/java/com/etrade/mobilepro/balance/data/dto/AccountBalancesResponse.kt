package com.etrade.mobilepro.balance.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root
import java.math.BigInteger

@Root(strict = false, name = "AccountBalancesResponse")
data class AccountBalancesResponse constructor(
    @field:Element(name = "BALANCE")
    var balances: BalanceDto? = null,

    @field:Element(name = "LoginUserSSOResponse", required = false)
    var error: BalancesErrorResponse? = null
)

@Root(strict = false, name = "BALANCE")
data class BalanceDto constructor(

    @field:Element(name = "ACCT_TYPE", required = false)
    var balancesAccountType: String? = null,

    @field:Element(name = "TRD_RESTRICTION_CODE", required = false)
    var trdRestrictionCode: BigInteger = BigInteger.ZERO,

    @field:Element(name = "PREV_TIME", required = false)
    var previousTime: String? = null,

    @field:Element(name = "BAL_PREV_CLOSE", required = false)
    var balancePreviousClose: BalancesDetailsDto? = null,

    @field:Element(name = "BAL_REAL_TIME", required = false)
    var balanceRealTime: BalancesDetailsDto? = null
)
