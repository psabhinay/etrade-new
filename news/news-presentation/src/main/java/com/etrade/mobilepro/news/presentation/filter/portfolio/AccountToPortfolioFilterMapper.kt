package com.etrade.mobilepro.news.presentation.filter.portfolio

import android.content.Context
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.news.presentation.R
import javax.inject.Inject

const val ALL_PORTFOLIOS_RESOURCE_ID = "all"

class AccountToPortfolioFilterMapper @Inject constructor(private val context: Context) {
    fun map(source: List<Account>): List<PortfolioFilterItem> {
        val baseList = mutableListOf(
            PortfolioFilterItem(
                title = context.getString(R.string.all),
                filterResourceId = ALL_PORTFOLIOS_RESOURCE_ID,
                isBaseAllPortfoliosItem = true,
                accountId = ALL_PORTFOLIOS_RESOURCE_ID
            )
        )
        val mapped = source.map {
            PortfolioFilterItem(title = it.accountShortName, filterResourceId = it.encAccountId ?: "", isStockPlan = it.isStockPlan(), accountId = it.accountId)
        }
        baseList.addAll(mapped)
        return baseList
    }
}
