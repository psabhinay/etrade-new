package com.etrade.mobilepro.screener.data.interactor

import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterRequest
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.repo.ScreenersRepo
import com.etrade.mobilepro.screener.api.result.ScreenerResult
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val REQUEST_DEBOUNCE_MS = 500L

class DefaultScreenerInteractor @Inject constructor(
    val aggregator: ScreenerFilterAggregator,
    private val mobileQuoteRepo: MobileQuoteRepo,
    private val repo: ScreenersRepo,
    private val group: ScreenerType.Group
) : ScreenerInteractor {
    private val retriesSubject: Subject<ScreenerType.Group> = BehaviorSubject.createDefault(group)

    private val preScreenResultsSubject: Subject<Resource<Int>> = BehaviorSubject.create()

    private val requests = aggregator
        .screenerFilterRequests()
        .filter {
            it.fireNetworkRequest
        }
        .filter {
            it.type.belongsTo == group
        }
        .distinctUntilChanged()
        .debounce(REQUEST_DEBOUNCE_MS, TimeUnit.MILLISECONDS)
    private val retries = retriesSubject
        .filter {
            it == group
        }
    private val preScreenResultsFlow = Observable
        .combineLatest(
            requests,
            retries,
            BiFunction<ScreenerFilterRequest, ScreenerType.Group, ScreenerFilterRequest> { request, _ ->
                request
            }
        )
        .switchMap(this::prescreenRequestFlow)
        .scan(Resource.Success<Int>() as Resource<Int>) { was, now ->
            if (now is Resource.Loading && now.data == null && was.data != null) {
                Resource.Loading(data = was.data)
            } else {
                now
            }
        }
        .share()

    override fun preScreenResults(): Observable<Resource<Int>> {
        preScreenResultsFlow.subscribe(preScreenResultsSubject)
        return preScreenResultsSubject
    }

    override fun retryPrescreen() = retriesSubject.onNext(group)

    override fun getFiltersSnapshot(): List<ScreenerFilter> = aggregator.getFilterSnapshot(group)

    override fun combinedScreenersResult(): Single<ScreenerResult> =
        repo
            .resultingPrescreenData(group, aggregator.generateRequestStringRepresentation(group))
            .toObservable()
            .flatMap { prescreenData ->
                mobileQuoteRepo
                    .loadMobileQuotes(symbols = prescreenData.joinToString(",") { it.symbol })
                    .map { quotes ->
                        ScreenerResult(
                            mobileQuotes = quotes,
                            prescreenData = prescreenData
                        )
                    }
            }
            .firstOrError()

    private fun prescreenRequestFlow(request: ScreenerFilterRequest): Observable<Resource<Int>> =
        Single
            .concat(
                Single.just(Resource.Loading()),
                repo
                    .preScreenResults(group = request.type.belongsTo, requestData = request.filter)
                    .map<Resource<Int>> {
                        Resource.Success(data = it)
                    }
                    .onErrorReturnItem(Resource.Failed())
            )
            .toObservable()
}
