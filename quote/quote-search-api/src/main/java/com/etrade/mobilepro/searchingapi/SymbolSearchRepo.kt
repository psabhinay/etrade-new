package com.etrade.mobilepro.searchingapi

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable

interface SymbolSearchRepo {
    fun searchAsLiveData(query: SearchQuery): LiveData<Resource<List<SearchResultItem.Symbol>>>
    fun searchAsObservable(query: SearchQuery): Observable<Resource<List<SearchResultItem.Symbol>>>
}
