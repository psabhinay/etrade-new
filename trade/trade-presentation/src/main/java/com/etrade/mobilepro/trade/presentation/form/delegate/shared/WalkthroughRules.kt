package com.etrade.mobilepro.trade.presentation.form.delegate.shared

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import com.etrade.mobilepro.balance.viewmodel.AccountBalanceViewModel
import com.etrade.mobilepro.dynamic.form.api.HiddenInput
import com.etrade.mobilepro.dynamic.form.api.InputHolder
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.TextInput
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.livedata.combineWith
import com.etrade.mobilepro.livedata.doOnNext
import com.etrade.mobilepro.livedata.filter
import com.etrade.mobilepro.livedata.filterNotNull
import com.etrade.mobilepro.livedata.first
import com.etrade.mobilepro.livedata.mergeLatest
import com.etrade.mobilepro.livedata.observeForever
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.FormActionTooltip
import com.etrade.mobilepro.trade.presentation.form.FormTooltip
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter
import com.etrade.mobilepro.tradingdefaults.preferences.DEFAULT_WALKTHROUGH_COMPLETED_FLAG
import com.etrade.mobilepro.walkthrough.api.IdleState
import com.etrade.mobilepro.walkthrough.api.TradePlacingAction
import com.etrade.mobilepro.walkthrough.api.TradePlacingState
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel

internal fun RuleListBuilder.walkthroughRules(
    parameter: CreateTradeFormParameter,
    inputId: StockTradeFormInputId,
    resources: Resources
) {
    val currentState = parameter.walkthroughViewModel.state.value
    if (currentState == null || currentState is IdleState) {
        skipWalkthrough(inputId)
        return
    }

    val ruleParams = WalkthroughRulesParams(
        inputId,
        parameter.onActionClickListener,
        parameter.walkthroughViewModel,
        parameter.balanceViewModel,
        resources
    )
    notifyTradePageWasDisplayed(ruleParams)
    highlightTradePlacingActions(ruleParams)
    notifySymbolHighlightedOnSymbolSelected(ruleParams)
    notifyOrderTypeHighlightedOnOrderTypeSelected(ruleParams)
    requestToMonitorQuantityKeyboardVisibility(ruleParams)
    notifyQuantityHighlightedOnKeyboardEvent(ruleParams)
    notifyPriceTypeHighlighted(ruleParams)
    notifyTermHighlighted(ruleParams)
    warnBeforeChangingSecurityType(ruleParams)
    displayPriceTypeDescriptions(ruleParams)
    displayPriceTypes(ruleParams)
}

private fun RuleListBuilder.skipWalkthrough(inputId: StockTradeFormInputId) {
    rule {
        requireInput<HiddenInput>(inputId.walkthroughFormCompleted)
            .setValue(DEFAULT_WALKTHROUGH_COMPLETED_FLAG)
    }
}

private fun RuleListBuilder.notifyTradePageWasDisplayed(
    params: WalkthroughRulesParams
) {
    rule {
        // Assume that the trade page was displayed after success or failure of the
        // account balance request.
        params.balanceViewModel.accountBalance
            .filter { data -> data != null }
            .mergeLatest(
                params.balanceViewModel.accountBalanceFailed
                    .filter { isFailure -> isFailure }
            )
            .first()
            .observeForever {
                params.onAction(TradePlacingAction.TradePageDisplayed)
            }
    }
}

private fun RuleListBuilder.highlightTradePlacingActions(
    params: WalkthroughRulesParams
) {
    rule {
        params.state
            .filter { it is TradePlacingState }
            .observeForever { state, stopObserving ->
                when (state) {
                    is IdleState -> stopObserving()
                    TradePlacingState.HighlightSymbol -> highlightSymbolInput(params)
                    TradePlacingState.HighlightOrderType -> highlightOrderTypeInput(params)
                    TradePlacingState.HighlightQuantity -> highlightQuantityInput(params)
                    TradePlacingState.HighlightPriceType -> highlightPriceTypeInput(params)
                    TradePlacingState.HighlightTerm -> highlightTermInput(params)
                    TradePlacingState.HighlightPreview -> highlightPreviewAction(params)
                }
            }
    }
}

private fun RuleListBuilder.notifySymbolHighlightedOnSymbolSelected(
    params: WalkthroughRulesParams
) {
    rule {
        requireAnyInput(params.symbol).value
            .filter { it != null }
            .first()
            .switchMap { params.state }
            .observeForever { state, stopObserving ->
                when (state) {
                    is IdleState -> stopObserving()
                    is TradePlacingState.HighlightSymbol -> {
                        params.onAction(TradePlacingAction.SymbolHighlighted)
                    }
                }
            }
    }
}

private fun RuleListBuilder.notifyOrderTypeHighlightedOnOrderTypeSelected(
    params: WalkthroughRulesParams
) {
    rule {
        requireAnyInput(params.action).value
            .filter { it != null }
            .first()
            .switchMap { params.state }
            .observeForever { state, stopObserving ->
                when (state) {
                    is IdleState -> stopObserving()
                    is TradePlacingState.HighlightOrderType -> {
                        params.onAction(TradePlacingAction.OrderTypeHighlighted)
                    }
                }
            }
    }
}

/**
 * Consider price type as "Highlighted" when the user stops interacting (entered a valid value and
 * dismissed the keyboard) with price type related fields.
 */
private fun RuleListBuilder.notifyPriceTypeHighlighted(params: WalkthroughRulesParams) {
    rule {
        var previousPriceType: String? = null
        params.state
            .filter { it is TradePlacingState.EnterPriceType }
            .switchMap { requireInput<SelectInput>(params.priceType).value }
            .doOnNext { stopMonitoringPriceTypeIme(params) }
            .map { it?.id }
            .filterNotNull()
            .doOnNext { selectedPriceType ->
                if (previousPriceType != null && selectedPriceType == params.priceTypeValueId.market) {
                    onPriceEntered(params)
                }
            }
            .filter { it != params.priceTypeValueId.market }
            .switchMap { selectedPriceType ->
                previousPriceType = selectedPriceType
                getInputInteraction(selectedPriceType, params)
            }
            .observeForever { isPriceEntered ->
                if (isPriceEntered) { onPriceEntered(params) }
            }
    }
}

/**
 * Price is considered as "entered" when the user enters a valid price value and
 * dismisses the keyboard.
 *
 * @return A [LiveData] which emits true when the user has entered a valid price valued
 * and dismissed the keyboard.
 */
private fun InputHolder.observeInputInteraction(priceTypeId: String): LiveData<Boolean> {
    return requireInput<TextInput>(priceTypeId)
        .run {
            requestToMonitorKeyboardVisibility(true)
            isKeyboardVisible.combineWith(value) { isVisible, price ->
                isVisible to price
            }
        }
        .map { (isKeyboardVisible, value) ->
            !isKeyboardVisible && !value.isNullOrEmpty()
        }
}

private fun InputHolder.stopMonitoringPriceTypeIme(params: WalkthroughRulesParams) {
    setOf<TextInput>(
        requireInput(params.limitPrice),
        requireInput(params.stopPrice),
        requireInput(params.limitPriceForStopLimit),
        requireInput(params.stopPriceForStopLimit)
    ).forEach { it.requestToMonitorKeyboardVisibility(false) }
}

private fun InputHolder.onPriceEntered(params: WalkthroughRulesParams) {
    stopMonitoringPriceTypeIme(params)
    params.onAction(TradePlacingAction.PriceTypeEntered)
}

/**
 * Gets a [LiveData] that represents input interaction for a given price type. The returned
 * [LiveData] indicates when the user has finished interacting with a price type input.
 */
private fun InputHolder.getInputInteraction(
    priceTypeId: String,
    params: WalkthroughRulesParams
): LiveData<Boolean> {
    return when (priceTypeId) {
        params.priceTypeValueId.limit -> { observeInputInteraction(params.limitPrice) }
        params.priceTypeValueId.stop,
        params.priceTypeValueId.trailingStopDollar,
        params.priceTypeValueId.trailingStopPercent -> { observeInputInteraction(params.stopPrice) }
        params.priceTypeValueId.stopLimit -> {
            observeInputInteraction(params.limitPriceForStopLimit)
                .combineLatest(
                    observeInputInteraction(params.stopPriceForStopLimit)
                )
                .map { (isLimitPriceEntered, isStopPriceEntered) ->
                    isLimitPriceEntered && isStopPriceEntered
                }
        }
        else -> liveData { /* intentionally blank */ }
    }
}

private fun RuleListBuilder.notifyTermHighlighted(params: WalkthroughRulesParams) {
    rule {
        requireInput<SelectInput>(params.priceType)
            .value
            .map { it?.id }
            .filterNotNull()
            .filter { it != params.priceTypeValueId.market }
            .switchMap {
                requireInput<SelectInput>(params.term).value
            }
            .filterNotNull()
            .observeForever { onTermHighlighted(params) }
    }
}

private fun RuleListBuilder.warnBeforeChangingSecurityType(
    params: WalkthroughRulesParams
) {
    rule {
        requireInput<SelectInput>(params.securityType).clickInterceptedSignal.observeForever {
            params.showDialog {
                message = params.resources.getString(R.string.trade_walkthrough_security_type_warning_message)
                positiveAction {
                    label = params.resources.getString(R.string.ok)
                    action = {
                        params.onAction(TradePlacingAction.Canceled)
                        requireInput<SelectInput>(params.securityType).requestClick()
                    }
                }
                negativeAction {
                    label = params.resources.getString(R.string.cancel)
                }
            }
        }
    }
}

private fun RuleListBuilder.displayPriceTypeDescriptions(params: WalkthroughRulesParams) {
    rule {
        requireInput<SelectInput>(params.priceType).clickInterceptedSignal
            .observeForever {
                if (params.state.value is TradePlacingState.HighlightPriceType) {
                    params.onAction(TradePlacingAction.PriceTypeHighlighted)
                    params.onActionClick(TradeFormAction.DisplayPriceTypeDescriptions)
                }
            }
    }
}

private fun RuleListBuilder.displayPriceTypes(params: WalkthroughRulesParams) {
    rule {
        requireInput<SelectInput>(params.priceType).clickInterceptedSignal
            .observeForever {
                val statesPriorToPriceType = setOf(
                    TradePlacingState.HighlightSymbol,
                    TradePlacingState.HighlightOrderType,
                    TradePlacingState.HighlightQuantity,
                    TradePlacingState.HighlightPriceType,
                    TradePlacingState.DisplayPriceTypes
                )
                if (params.state.value !in statesPriorToPriceType) {
                    requireInput<SelectInput>(params.priceType).requestClick()
                }
            }
    }
}

/**
 * Start monitoring the keyboard visibility when we highlight the quantity input,
 * and stop monitoring it when the next field is highlighted.
 */
private fun RuleListBuilder.requestToMonitorQuantityKeyboardVisibility(
    params: WalkthroughRulesParams
) {
    rule {
        params.state
            .observeForever { state ->
                when (state) {
                    is TradePlacingState.HighlightQuantity -> {
                        requireInput<TextInput>(params.quantity)
                            .requestToMonitorKeyboardVisibility(true)
                    }
                    is TradePlacingState.HighlightPriceType -> {
                        requireInput<TextInput>(params.quantity)
                            .requestToMonitorKeyboardVisibility(false)
                    }
                }
            }
    }
}

/**
 * Request to highlight the next field after the quantity field has a valid value and the keyboard
 * has been dismissed.
 */
private fun RuleListBuilder.notifyQuantityHighlightedOnKeyboardEvent(params: WalkthroughRulesParams) {
    rule {
        requireInput<TextInput>(params.quantity).isKeyboardVisible
            .observeForever { isKeyboardVisible ->
                val isHighlightingQuantity = params.state.value is TradePlacingState.HighlightQuantity
                val quantityIsNotEmpty = !requireInput<TextInput>(params.quantity)
                    .value.value
                    .isNullOrEmpty()

                if (!isKeyboardVisible && isHighlightingQuantity && quantityIsNotEmpty) {
                    params.onAction(TradePlacingAction.QuantityHighlighted)
                }
            }
    }
}

private fun InputHolder.onTermHighlighted(params: WalkthroughRulesParams) {
    requireInput<HiddenInput>(params.walkthroughFormCompleted).setValue(DEFAULT_WALKTHROUGH_COMPLETED_FLAG)
    params.onAction(TradePlacingAction.TermHighlighted)
}

private fun highlightSymbolInput(params: WalkthroughRulesParams) {
    FormTooltip(
        targetInputId = params.symbol,
        message = params.resources.getString(R.string.trade_walkthrough_symbol_tooltip)
    ).let(TradeFormAction::Tooltip)
        .run(params::onActionClick)
}

private fun highlightOrderTypeInput(params: WalkthroughRulesParams) {
    FormTooltip(
        targetInputId = params.action,
        message = params.resources.getString(R.string.trade_walkthrough_order_type_tooltip)
    ).let(TradeFormAction::Tooltip)
        .run(params::onActionClick)
}

private fun highlightQuantityInput(params: WalkthroughRulesParams) {
    FormTooltip(
        targetInputId = params.quantity,
        message = params.resources.getString(R.string.trade_walkthrough_quantity_tooltip),
        shouldConsumeDismissEvent = false,
    ).let(TradeFormAction::Tooltip)
        .run(params::onActionClick)
}

private fun highlightPriceTypeInput(params: WalkthroughRulesParams) {
    FormTooltip(
        targetInputId = params.priceType,
        message = params.resources.getString(R.string.trade_walkthrough_price_type_tooltip)
    ).let(TradeFormAction::Tooltip)
        .run(params::onActionClick)
}

private fun InputHolder.highlightTermInput(params: WalkthroughRulesParams) {
    val onTermTooltipDismissed = {
        if (requireInput<SelectInput>(params.priceType).value.value?.id == params.priceTypeValueId.market) {
            onTermHighlighted(params)
        }
    }

    FormTooltip(
        targetInputId = params.term,
        message = params.resources.getString(R.string.trade_walkthrough_term_tooltip),
        onExplicitDismiss = onTermTooltipDismissed,
        onImplicitDismiss = onTermTooltipDismissed
    ).let(TradeFormAction::Tooltip)
        .run(params::onActionClick)
}

private fun highlightPreviewAction(params: WalkthroughRulesParams) {
    FormActionTooltip(
        targetActionTitle = params.resources.getString(R.string.trade_action_preview),
        message = params.resources.getString(R.string.trade_walkthrough_preview_tooltip),
        onExplicitDismiss = { params.onAction(TradePlacingAction.PreviewHighlighted) },
        onImplicitDismiss = { params.onAction(TradePlacingAction.PreviewHighlighted) }
    ).let(TradeFormAction::Tooltip)
        .run(params::onActionClick)
}

class WalkthroughRulesParams(
    private val inputId: StockTradeFormInputId,
    private val onActionClickListener: OnActionClickListener,
    private val walkthroughViewModel: WalkthroughStatesViewModel,
    val balanceViewModel: AccountBalanceViewModel,
    val resources: Resources
) : StockTradeFormInputId by inputId,
    OnActionClickListener by onActionClickListener,
    WalkthroughStatesViewModel by walkthroughViewModel
