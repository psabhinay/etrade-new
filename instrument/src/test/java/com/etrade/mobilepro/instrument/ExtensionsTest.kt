package com.etrade.mobilepro.instrument

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class ExtensionsTest {

    @Test
    fun testGetConstructedOsiSymbol() {
        assertEquals("GOU---100116P00110000", getConstructedOsiSymbol("GOU Jan 16 '10 $110 Put"))
        assertEquals("DLY AM JAN-10 $11 CALL", getConstructedOsiSymbol("DLY AM JAN-10 $11 CALL"))
        assertEquals("APPL", getConstructedOsiSymbol("APPL"))
    }

    @Test
    fun testgetDisplaySymbolFromOsiKey() {
        assertEquals("GOU Jan 16 '10 $110 Put", getDisplaySymbolFromOsiKey("GOU---100116P00110000"))
        assertEquals(null, getDisplaySymbolFromOsiKey("DLY AM JAN-10 $11 CALL"))
    }

    @Test
    fun testGetOptionType() {
        assertEquals(OptionType.CALL, getOptionType("GOU---100116C00110000"))
        assertEquals(OptionType.PUT, getOptionType("GOU---100116P00110000"))
        assertEquals(OptionType.UNKNOWN, getOptionType("GOU---100116X00110000"))
        assertNull(getOptionType("ABCDDDDD"))
        assertNull(getOptionType(""))
    }

    @Test
    fun testGetCallPutPart() {
        assertEquals("Call", getCallPutPartFromOsiKey("GOU---100116C00110000"))
        assertEquals("Put", getCallPutPartFromOsiKey("GOU---100116P00110000"))
        assertNull(getCallPutPartFromOsiKey("GOU---100116X00110000"))
        assertNull(getCallPutPartFromOsiKey("ABCDDDDD"))
        assertNull(getCallPutPartFromOsiKey(""))
    }

    @Test
    fun testGetExpirationDate() {
        assertEquals(
            ExpirationDate(2010, 1, 16, SettlementSessionCode.PM),
            getExpirationDateFromOsiKey("GOU---100116C00110000")
        )
    }

    @Test
    fun testIsLongSymbol() {
        assertTrue(isSymbolLongOption("GOU---100116C00110000"))
    }

    @Test
    fun testGetStrikePrice() {
        assertNull(getStrikePrice(""))
        assertNull(getStrikePrice("AAPL"))
        assertEquals(BigDecimal(110), getStrikePrice("GOU---100116C00110000"))
    }
}
