package com.etrade.mobilepro.quoteapi

import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import io.reactivex.Observable

fun <F, T, P> StreamingQuoteProvider<F, T, P>.getQuote(symbol: SearchResultItem.Symbol, fields: Set<F>, payload: P? = null): Observable<T> {
    return getQuote(symbol.title, symbol.type, fields, payload)
}
