package com.etrade.mobilepro.appwidget.common

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.widgetcontainer.AdapterType
import com.etrade.mobilepro.appwidget.widgetcontainer.DefaultAppWidgetProvider
import com.etrade.mobilepro.appwidget.widgetcontainer.WidgetRemoteViewsService
import org.slf4j.LoggerFactory

private const val EXTRA_BUNDLE = "IndexRemoteViewsService.extra.bundle"
private const val EXTRA_BUNDLE_HASH_CODE = "CustomWidgetProvider.extra.bundle_hash_code"
private const val EXTRA_ADAPTER_TYPE = "IndexRemoteViewsService.extra.required_adapter"
private const val EXTRA_LAYOUT_TYPE = "IndexRemoteViewsService.extra.layout_type"

private val logger = LoggerFactory.getLogger("appwidget.common.UtilIntent")

internal fun getAdapterIntent(context: Context, adapterType: AdapterType, layout: WidgetSettings.Layout): Intent {
    val bundle = Bundle().apply {
        putSerializable(EXTRA_ADAPTER_TYPE, adapterType)
        putSerializable(EXTRA_LAYOUT_TYPE, layout)
    }

    return Intent(context, WidgetRemoteViewsService::class.java).apply {
        putExtra(EXTRA_BUNDLE, bundle)
        putExtra(EXTRA_BUNDLE_HASH_CODE, bundle.hashCode())
        data = Uri.parse(toUri(Intent.URI_INTENT_SCHEME))
    }
}

@Suppress("TooGenericExceptionCaught")
internal fun Intent.getAdapterTypeOrThrow(): AdapterType {
    return try {
        getBundleExtra(EXTRA_BUNDLE)
            ?.getSerializable(EXTRA_ADAPTER_TYPE)
            ?.let { it as? AdapterType }
            .let { requireNotNull(it) { "Could not extract adapter type from Intent" } }
    } catch (ex: RuntimeException) {
        // RuntimeException#cause is ClassNotFoundException
        logger.error("Could not extract adapter type from Intent", ex)
        AdapterType.Watchlist
    }
}

internal fun Intent.getLayoutTypeOrThrow(): WidgetSettings.Layout {
    return getBundleExtra(EXTRA_BUNDLE)
        ?.getSerializable(EXTRA_LAYOUT_TYPE)
        ?.let { it as? WidgetSettings.Layout }
        ?: WidgetSettings.Layout.TILES
}

internal fun Context.getBroadcastIntentWith(isMutable: Boolean = false, setProperties: Intent.() -> Unit): PendingIntent =
    Intent(this, DefaultAppWidgetProvider::class.java).let {
        it.setProperties()
        val mutabilityFlag = if (isMutable) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent.FLAG_MUTABLE
            } else {
                0
            }
        } else {
            PendingIntent.FLAG_IMMUTABLE
        }
        val flags = PendingIntent.FLAG_UPDATE_CURRENT or mutabilityFlag
        PendingIntent.getBroadcast(this, 0, it, flags)
    }
