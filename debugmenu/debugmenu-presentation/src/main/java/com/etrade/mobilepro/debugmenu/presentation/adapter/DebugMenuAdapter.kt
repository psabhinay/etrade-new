package com.etrade.mobilepro.debugmenu.presentation.adapter

import com.etrade.mobilepro.debugmenu.presentation.adapter.delegate.CircuitBreakerAdapterDelegate
import com.etrade.mobilepro.debugmenu.presentation.adapter.delegate.EnvironmentAdapterDelegate
import com.etrade.mobilepro.debugmenu.presentation.adapter.delegate.FeatureFlagsAdapterDelegate
import com.etrade.mobilepro.debugmenu.presentation.adapter.delegate.ForceCrashAdapterDelegate
import com.etrade.mobilepro.debugmenu.presentation.adapter.delegate.SectionAdapterDelegate
import com.etrade.mobilepro.debugmenu.presentation.adapter.delegate.TimeoutsAdapterDelegate
import com.etrade.mobilepro.debugmenu.presentation.item.DebugMenuItem
import com.etrade.mobilepro.debugmenu.presentation.item.EnvironmentItem
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

internal class DebugMenuAdapter(
    environmentSelectedListener: (EnvironmentItem) -> Unit,
    timeoutListener: (sessionLimitEnabled: Boolean) -> Unit,
    circuitBreakerListener: (circuitBreakerEnabled: Boolean) -> Unit,
    featureFlagsListener: () -> Unit
) : ListDelegationAdapter<MutableList<DebugMenuItem>>() {
    init {
        delegatesManager.addDelegate(CircuitBreakerAdapterDelegate(circuitBreakerListener))
        delegatesManager.addDelegate(EnvironmentAdapterDelegate(environmentSelectedListener))
        delegatesManager.addDelegate(SectionAdapterDelegate())
        delegatesManager.addDelegate(TimeoutsAdapterDelegate(timeoutListener))
        delegatesManager.addDelegate(ForceCrashAdapterDelegate())
        delegatesManager.addDelegate(FeatureFlagsAdapterDelegate(featureFlagsListener))
        items = mutableListOf()
    }

    fun submitItems(newItems: List<DebugMenuItem>) {
        dispatchUpdates(items, newItems)
        items.clear()
        items.addAll(newItems)
    }
}
