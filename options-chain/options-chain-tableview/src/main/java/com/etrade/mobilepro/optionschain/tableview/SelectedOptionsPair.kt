package com.etrade.mobilepro.optionschain.tableview

class SelectedOptionsPair(
    val put: SelectedOption,
    val call: SelectedOption
)
