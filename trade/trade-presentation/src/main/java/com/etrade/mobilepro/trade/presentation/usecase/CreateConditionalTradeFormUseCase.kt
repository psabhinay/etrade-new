package com.etrade.mobilepro.trade.presentation.usecase

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.viewmodel.CreateFormUseCase
import com.etrade.mobilepro.dynamic.form.viewmodel.CreateFormUseCaseParameter
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.trade.presentation.TradeConditionalOrderDisclosureViewModel
import com.etrade.mobilepro.trade.presentation.form.ConditionalOrderFormFactory
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import javax.inject.Inject

interface CreateConditionalTradeFormUseCase : CreateFormUseCase

class CreateConditionalTradeFormUseCaseImpl @Inject constructor(
    private val conditionalOrderFormFactory: ConditionalOrderFormFactory
) : CreateConditionalTradeFormUseCase {

    override suspend fun execute(parameter: CreateFormUseCaseParameter): ETResult<LiveData<Form>> {
        if (parameter !is CreateConditionalTradeFormParameter) {
            return ETResult.failure(IllegalArgumentException("Invalid parameter instance."))
        }

        return conditionalOrderFormFactory.createConditionalOrderForm(parameter)
    }
}

class CreateConditionalTradeFormParameter(
    override val conditionalOrderDisclosureViewModel: TradeConditionalOrderDisclosureViewModel,
    override val onActionClickListener: OnActionClickListener,
    override val quoteWidgetViewModel: QuotesWidgetViewModel,
    override val defaultValues: InputValuesSnapshot,
    override val tradeOrderType: TradeOrderType,
    override val walkthroughViewModel: WalkthroughStatesViewModel,
    val accountId: String,
    val optionsQuoteBidAskViewModel: QuotesWidgetViewModel,
    val orderNumber: Int
) : TradeFormParameter
