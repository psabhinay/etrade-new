package com.etrade.mobilepro.menu.mapper

import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.repo.DynamicMenuItem

interface DynamicMenuItemMapper {
    fun mapDynamicMenuItemsToPage(dynamicMenuItems: List<DynamicMenuItem>): List<MenuItem>
}
