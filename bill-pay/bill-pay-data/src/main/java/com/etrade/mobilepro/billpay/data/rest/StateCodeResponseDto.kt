package com.etrade.mobilepro.billpay.data.rest

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class StateCodeResponseDto(
    @Json(name = "StateList")
    val stateList: Map<String, String>? = null
) : BaseDataDto()
