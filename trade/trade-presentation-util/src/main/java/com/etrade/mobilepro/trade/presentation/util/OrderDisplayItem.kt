package com.etrade.mobilepro.trade.presentation.util

import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

data class OrderDisplayItem(val label: CharSequence, val sections: List<TableSectionItemLayout>)
