package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterItemSupportMessageLetterBinding
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.Letter
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.SupportMessageItem
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.viewholder.SupportMessageLetterViewHolder
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class SupportMessageLetterDelegate : AbsListItemAdapterDelegate<Letter, SupportMessageItem, SupportMessageLetterViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): SupportMessageLetterViewHolder =
        SupportMessageLetterViewHolder(
            parent.viewBinding(
                MessageCenterItemSupportMessageLetterBinding::inflate
            )
        )

    override fun isForViewType(item: SupportMessageItem, items: MutableList<SupportMessageItem>, position: Int): Boolean =
        item is Letter

    override fun onBindViewHolder(item: Letter, holder: SupportMessageLetterViewHolder, payloads: MutableList<Any>) =
        holder.bindTo(item)
}
