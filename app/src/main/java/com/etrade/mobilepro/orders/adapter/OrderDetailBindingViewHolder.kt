package com.etrade.mobilepro.orders.adapter

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.presentation.AffiliatedProductAction

class OrderDetailBindingViewHolder(
    private val binding: ViewDataBinding,
    private val variableId: Int
) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(item: Any?) {
        binding.setVariable(variableId, item)
        binding.executePendingBindings()
    }

    fun bindWithAffiliatedAction(
        item: Any?,
        affiliatedVariable: Int,
        onAffiliatedProductClickListener: AffiliatedProductAction
    ) {
        binding.setVariable(affiliatedVariable, onAffiliatedProductClickListener)
        bindTo(item)
    }
}
