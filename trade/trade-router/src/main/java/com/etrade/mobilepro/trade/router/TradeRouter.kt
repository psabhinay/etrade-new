package com.etrade.mobilepro.trade.router

import android.app.Activity
import androidx.annotation.IdRes
import androidx.navigation.NavController
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.order.details.router.OrderConfirmationAction
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.trade.api.TradeOptionInfoType

interface TradeRouter {

    @get:IdRes val navGraphId: Int

    fun openConfirmation(navController: NavController, orderId: String, action: OrderConfirmationAction)

    fun openPreview(navController: NavController, argument: OrderDetailsArgument)

    fun openPortfolio(activity: Activity, accountId: String)

    fun openCalculator(navController: NavController)

    fun openPriceTypeDescriptions(navController: NavController)

    fun editConditionalOrder(navController: NavController, orderInputId: String, orderNumber: Int, accountId: String?, snapshot: InputValuesSnapshot?)

    fun exitConfirmation(activity: Activity)

    fun openExtendedHoursDisclosure(navController: NavController, title: String)

    fun openOtcDisclosure(navController: NavController, title: String)

    fun openLiEtfDisclosure(navController: NavController, title: String)

    fun openMRBDisclosure(navController: NavController, title: String)

    fun openHelp(navController: NavController, tradeOptionInfoType: TradeOptionInfoType, learnOtherPriceTypes: Boolean = false)
}
