package com.etrade.mobilepro.earnings.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
data class EarningsResponse(
    @Json(name = "EarningsResponse")
    val response: EarningsDetails?
)

@JsonClass(generateAdapter = true)
data class EarningsDetails(
    @Json(name = "EarningsDetail")
    val detail: List<EarningsDetail>?
)

@JsonClass(generateAdapter = true)
data class EarningsDetail(
    @Json(name = "Period")
    val period: List<MarketPeriod>?
)

@JsonClass(generateAdapter = true)
data class MarketPeriod(
    @Json(name = "high")
    val high: BigDecimal?,
    @Json(name = "low")
    val low: BigDecimal?,
    @Json(name = "estMean")
    val estMean: BigDecimal?,
    @Json(name = "fiscalQuarter")
    val quarter: String?,
    @Json(name = "qtr1ExpectedDate")
    val expectedDate: String?,
    @Json(name = "announceDate")
    val announceDate: String?,
    @Json(name = "announceTime")
    val announceTime: String?,
    @Json(name = "actualValue")
    val actualValue: BigDecimal?,
    @Json(name = "surpriseMean")
    val surpriseMean: BigDecimal?,
    @Json(name = "fiscalPeriodYear")
    val fiscalPeriodYear: String?,
    @Json(name = "dateTime")
    val dateTime: BigDecimal?
)
