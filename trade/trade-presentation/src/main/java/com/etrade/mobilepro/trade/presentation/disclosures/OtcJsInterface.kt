package com.etrade.mobilepro.trade.presentation.disclosures

import android.webkit.JavascriptInterface
import org.slf4j.LoggerFactory

class OtcJsInterface(private val onAction: (() -> Unit)) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @JavascriptInterface
    fun closeWebView() {
        logger.debug("JS Output - closeWebView")
        onAction.invoke()
    }

    @Deprecated(
        message = "Keep the misspelled version available for web view pages that use it",
        replaceWith = ReplaceWith("closeWebView()")
    )
    @JavascriptInterface
    fun closeWebview() {
        closeWebView()
    }
}
