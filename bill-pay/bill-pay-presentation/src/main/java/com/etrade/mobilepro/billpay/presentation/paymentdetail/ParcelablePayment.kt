package com.etrade.mobilepro.billpay.presentation.paymentdetail

import android.content.res.Resources
import android.os.Parcelable
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.billpay.api.Payment
import com.etrade.mobilepro.billpay.api.PaymentStatus
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.billpay.presentation.common.formatAmount
import com.etrade.mobilepro.billpay.presentation.getLabel
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.formatters.americanSimpleDateFormatter
import com.etrade.mobilepro.util.formatters.formatAccountName
import com.etrade.mobilepro.util.formatters.formatPayeeName
import kotlinx.parcelize.Parcelize
import org.threeten.bp.LocalDate
import java.math.BigDecimal

@Parcelize
class ParcelablePayment(
    val id: String?,
    val status: PaymentStatus,
    val amount: BigDecimal?,
    val payeeName: String?,
    val payeeNickname: String?,
    val payeeAccountNumber: String?,
    val deliveryDate: LocalDate?,
    val scheduleDate: LocalDate?,
    val memo: String?,
    val confirmationNumber: String?,
    val accountId: String?,
    val accountName: String?,
    val isActionButtonsVisible: Boolean = status == PaymentStatus.SCHEDULED
) : Parcelable

fun ParcelablePayment.toListOfTableSectionItemLayout(resources: Resources): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    sections.add(createSummarySection(resources.getString(R.string.status), resources.getString(status.getLabel())))
    setConfirmationSection(sections, resources, confirmationNumber)
    setDateSections(sections, resources)
    setPayToSection(sections, resources)
    setPayFromSection(sections, resources, accountName, accountId)
    setAmountSection(sections, resources)
    setMemoSection(sections, resources, memo)
    return sections
}

private fun setMemoSection(
    sections: MutableList<TableSectionItemLayout>,
    resources: Resources,
    memo: String?
) {
    sections.add(createSummarySection(resources.getString(R.string.bill_pay_payment_details_memo), memo ?: resources.getString(R.string.empty_default)))
}

private fun setPayFromSection(
    sections: MutableList<TableSectionItemLayout>,
    resources: Resources,
    accountName: String?,
    accountNumber: String?
) = when {
    accountName != null && accountNumber != null -> formatAccountName(accountName, accountNumber)
    accountName != null -> accountName
    else -> resources.getString(R.string.empty_default)
}
    .let {
        val key = resources.getString(R.string.bill_pay_payment_details_pay_from)
        createSummarySection(
            label = key,
            value = it,
            valueDescription = resources.accountNameContentDescription(it) ?: ""
        )
    }
    .let(sections::add)

private fun setConfirmationSection(
    sections: MutableList<TableSectionItemLayout>,
    resources: Resources,
    confirmationNumber: String?
) {
    sections.add(
        createSummarySection(
            resources.getString(R.string.bill_pay_payment_details_confirmation_number),
            confirmationNumber ?: resources.getString(R.string.empty_default)
        )
    )
}

private fun ParcelablePayment.setPayToSection(
    sections: MutableList<TableSectionItemLayout>,
    resources: Resources
) {
    val formattedPayeeName = formatPayeeName(payeeName, payeeAccountNumber, payeeNickname) ?: resources.getString(R.string.empty_default)
    val key = resources.getString(R.string.bill_pay_payment_details_pay_to)
    sections.add(
        createSummarySection(
            label = key,
            value = formattedPayeeName,
            valueDescription = resources.accountNameContentDescription(formattedPayeeName) ?: ""
        )
    )
}

private fun ParcelablePayment.setAmountSection(
    sections: MutableList<TableSectionItemLayout>,
    resources: Resources
) {
    val formattedAmount = amount?.let { MarketDataFormatter.formatCurrencyValue(it) } ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
    sections.add(createSummarySection(resources.getString(R.string.amount), formattedAmount))
}

private fun ParcelablePayment.setDateSections(
    sections: MutableList<TableSectionItemLayout>,
    resources: Resources
) {
    val formattedScheduleDate = scheduleDate?.format(americanSimpleDateFormatter)?.toString() ?: resources.getString(R.string.empty_default)
    sections.add(createSummarySection(resources.getString(R.string.bill_pay_payment_details_send_date), formattedScheduleDate))

    val formattedDeliveryDate = deliveryDate?.format(americanSimpleDateFormatter)?.toString() ?: resources.getString(R.string.empty_default)
    sections.add(createSummarySection(resources.getString(R.string.bill_pay_payment_details_delivery_date), formattedDeliveryDate))
}

internal fun Payment.toParcelable(
    accountName: String?,
    payeeAccountNumber: String?
): ParcelablePayment = ParcelablePayment(
    id = paymentId,
    status = status,
    amount = amount.formatAmount(),
    payeeName = payeeName,
    payeeNickname = payeeNickname,
    payeeAccountNumber = payeeAccountNumber,
    deliveryDate = deliveryDate,
    scheduleDate = scheduleDate,
    memo = memo,
    confirmationNumber = confirmationNumber,
    accountId = accountId,
    accountName = accountName
)
