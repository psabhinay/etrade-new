package com.etrade.mobilepro.screener.api.filter.stock

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class FundamentalsFilter(
    val peRatio0to15x: Boolean = false,
    val peRatio15to25x: Boolean = false,
    val peRatio25to50x: Boolean = false,
    val peRatio50to100x: Boolean = false,
    val peRatioBelowIndustryAvg: Boolean = false,
    val peRatioAboveIndustryAvg: Boolean = false,

    val peRatioOverTrailing12Months: Boolean = false,
    val peRatioOverCurrentFiscalYear: Boolean = false,
    val peRatioOverNextFiscalYear: Boolean = false,

    val priceBookRatio0to1x: Boolean = false,
    val priceBookRatio1to2x: Boolean = false,
    val priceBookRatio2to3x: Boolean = false,
    val priceBookRatioGreaterThan3x: Boolean = false
) : ScreenerFilter {

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.STOCK

    fun isRatioXSelected(): Boolean = peRatio0to15x || peRatio15to25x || peRatio25to50x || peRatio50to100x

    fun isOverPeriodSelected(): Boolean = peRatioOverTrailing12Months || peRatioOverCurrentFiscalYear || peRatioOverNextFiscalYear

    fun isPriceBookRatioSelected(): Boolean = priceBookRatio0to1x || priceBookRatio1to2x || priceBookRatio2to3x || priceBookRatioGreaterThan3x

    override fun isEmpty(): Boolean = !isRatioXSelected() && !peRatioBelowIndustryAvg && !peRatioAboveIndustryAvg &&
        !isOverPeriodSelected() && !isPriceBookRatioSelected()

    fun periodButtonsAreChecked(): Boolean =
        peRatioOverTrailing12Months || peRatioOverCurrentFiscalYear || peRatioOverNextFiscalYear

    fun peRatioButtonsAreChecked(): Boolean =
        peRatio0to15x || peRatio15to25x || peRatio25to50x || peRatio50to100x || peRatioBelowIndustryAvg || peRatioAboveIndustryAvg

    fun peRatioRangeButtonsAreChecked(): Boolean =
        peRatio0to15x || peRatio15to25x || peRatio25to50x || peRatio50to100x

    fun peRatioBlockIsEmpty(): Boolean = !periodButtonsAreChecked() && !peRatioButtonsAreChecked()
}
