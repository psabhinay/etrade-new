package com.etrade.mobilepro.common

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class CommonRealmModule
