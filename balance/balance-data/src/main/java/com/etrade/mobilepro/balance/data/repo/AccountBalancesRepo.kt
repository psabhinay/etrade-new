package com.etrade.mobilepro.balance.data.repo

import com.etrade.mobilepro.balance.data.AccountBalances
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable

/**
 * Repository provides balance details for a given account.
 */
interface AccountBalancesRepo {

    /**
     * @return AccountBalances
     */
    fun getBalances(request: BalanceRequest, cacheExpiration: Long?): Observable<Resource<AccountBalances>>
}
