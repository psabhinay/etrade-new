package com.etrade.mobilepro.user.session.manager

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.session.api.SessionEvents

interface SessionTimeoutManager {

    /**
     * Core logic that checks whether a warning dialog box should be shown, or whether the session
     * should be terminated. Also handles the alarm creation/cancellation and message passing to
     * the lifecycle observer in order to show the appropriate dialog boxes to the user.
     *
     * @return True if the session was deemed to be terminated, False if otherwise.
     */
    fun checkAndTerminateSession(): Boolean

    /**
     * Indicates user activity that should reset the elapsed idle time
     */
    fun touchSession()

    fun initSessionTimeout(timeOutDuration: Long)

    fun shouldTimeoutUser(): Boolean

    fun cancelSessionTimeout()

    val sessionEvent: LiveData<SessionEvents>
}
