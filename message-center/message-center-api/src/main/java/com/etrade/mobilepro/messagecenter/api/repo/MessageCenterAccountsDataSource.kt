package com.etrade.mobilepro.messagecenter.api.repo

import com.etrade.mobilepro.common.result.ETResult

interface MessageCenterAccountsDataSource {
    suspend fun getAccountIdToShortDescriptionMap(): ETResult<Map<String, String>>
}
