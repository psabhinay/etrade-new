package com.etrade.mobilepro.util

interface AuthenticationStatusProvider {

    fun isAuthenticated(): Boolean
}
