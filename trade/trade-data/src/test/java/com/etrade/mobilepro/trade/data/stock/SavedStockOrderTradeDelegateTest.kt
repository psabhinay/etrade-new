package com.etrade.mobilepro.trade.data.stock

import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.trade.data.DAY_TRADING_FLAG
import com.etrade.mobilepro.trade.data.DAY_TRADING_VER
import com.etrade.mobilepro.trade.data.form.input.DefaultStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
internal class SavedStockOrderTradeDelegateTest {

    private val inputId: StockTradeFormInputId = DefaultStockTradeFormInputId

    private val fakeApiClient: StockTradingApiClient = mock {
        runBlocking {
            whenever(it.makeStockOrder(any(), any())).then(mock())
        }
    }

    private val mockUser: User = mock {
        whenever(it.getUserId()).thenReturn(12345)
    }

    @Test
    fun `test default stock change preview market order has orderId and originalQuantity`() {
        val sut = SavedStockOrderTradeDelegate(fakeApiClient, false, mockUser)
        val inputMap = mapOf(
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.allOrNone to "0",
            inputId.stopPrice to "",
            inputId.limitPrice to "",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to "day",
            inputId.originalQuantity to "10",
            inputId.orderId to "23456"
        )

        runBlockingTest {
            sut.previewOrder(inputMap, MarketDestination.AUTO)
        }

        val expectedMap = mapOf(
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.allOrNone to "0",
            inputId.stopPrice to "",
            inputId.limitPrice to "",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to "day",
            inputId.originalQuantity to "10",
            inputId.orderId to "23456",
            StockOrderConstants.PLATFORM to "android",
            StockOrderConstants.USER_ID to "12345",
            StockOrderConstants.LOT_SUPPORT to "false",
            StockOrderConstants.MARKET_SESSION to "1",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        runBlockingTest {
            verify(fakeApiClient).makeStockOrder("PreviewChangeStockOrder", expectedMap)
        }
    }
}
