package com.etrade.mobilepro.transfermoney

import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.transfermoney.api.repo.TransferStockAccountsDataSource
import javax.inject.Inject

class DefaultTransferStockAccountsDataSource @Inject constructor(
    private val daoProvider: AccountDaoProvider
) : TransferStockAccountsDataSource {
    override suspend fun getStockAccountIds(): Set<String> {
        val accounts = daoProvider.getAccountDao().getAllAccountsNonAsync()
        return findStockPlanAccountIds(accounts)
    }

    private fun findStockPlanAccountIds(accounts: List<Account>): Set<String> {
        val candidates = accounts.filter { it.isStockPlan() }
        val result = mutableSetOf<String>()
        candidates.forEach { candidate ->
            if (accounts.count { it.accountId == candidate.accountId } == 1) {
                result.add(candidate.accountId)
            }
        }
        return result
    }
}
