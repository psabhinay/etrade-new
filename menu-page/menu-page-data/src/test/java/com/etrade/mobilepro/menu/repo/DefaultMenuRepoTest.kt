package com.etrade.mobilepro.menu.repo

import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.menu.dto.MenuDto
import com.etrade.mobilepro.menu.mapper.getDynamicMenuResponse
import com.etrade.mobilepro.menu.mapper.verifyDynamicMenuItems
import com.etrade.mobilepro.util.Resource
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import kotlin.Long.Companion.MAX_VALUE

@ExperimentalCoroutinesApi
class DefaultMenuRepoTest {

    private val baseUrl = "https://us.uat.etrade.com"
    private lateinit var dynamicMenuServiceResponseObservable: Observable<Resource<BaseMobileResponse>>
    private lateinit var dynamicMenuApiResponse: Resource<BaseMobileResponse>
    private lateinit var menuRepo: DefaultMenuRepo

    @Before
    fun setUp() {
        dynamicMenuApiResponse = getDynamicMenuResponse()
        dynamicMenuServiceResponseObservable = Observable.just(dynamicMenuApiResponse)
        menuRepo = mockDefaultMenuRepo()
    }

    @Test
    fun `get dynamic menu items`() {
        runBlockingTest {
            menuRepo.getDynamicMenuItems().onSuccess {
                @Suppress("UNCHECKED_CAST")
                val initialDynamicMenuDtos = (dynamicMenuApiResponse.data?.screen?.views as? List<MenuDto>).orEmpty()
                initialDynamicMenuDtos.verifyDynamicMenuItems(it)
            }
        }
    }

    private fun mockDefaultMenuRepo(): DefaultMenuRepo {
        return DefaultMenuRepo(
            screenDataSource = mockScreenDataSource(),
            baseUrl = baseUrl
        )
    }

    private fun mockScreenDataSource(): ScreenDataSource {
        return mock {
            on {
                getScreen(
                    request = ServicePath.DynamicMenu,
                    showCachedData = true,
                    cacheExpiration = MAX_VALUE
                )
            } doReturn dynamicMenuServiceResponseObservable
        }
    }
}
