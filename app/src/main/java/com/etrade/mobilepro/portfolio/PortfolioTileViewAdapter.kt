package com.etrade.mobilepro.portfolio

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.extension.getUsableScreenWidth
import com.etrade.mobilepro.navigation.getParentActivity
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableRow
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

const val TILES_PER_ROW = 3

class PortfolioTileViewAdapter(context: Context) : PortfolioNonTableViewAdapter(context) {

    override val layoutId: Int
        get() = R.layout.movers_item

    override fun getItems(): List<List<PortfolioRecordsTableRow>> = data.rows.withIndex()
        .groupBy { it.index / TILES_PER_ROW }
        .map { entry -> entry.value.map { it.value } }

    override fun onCreateCellViewHolder(parent: ViewGroup, viewType: Int) =
        PortfolioTileDataCellViewHolder(
            LayoutInflater.from(parent.context).inflate(layoutId, parent, false).apply {
                val usableWidth = getParentActivity()!!.getUsableScreenWidth
                val padding = resources.getDimensionPixelSize(R.dimen.spacing_small12) * 2
                layoutParams = ViewGroup.LayoutParams(
                    (usableWidth - padding) / TILES_PER_ROW,
                    WRAP_CONTENT
                )
            }
        )

    override fun onBindCellViewHolder(
        holder: AbstractViewHolder?,
        cellItemModel: Any?,
        columnPosition: Int,
        rowPosition: Int
    ) {
        val index = rowPosition * TILES_PER_ROW + columnPosition
        val row = data.rows[index]
        (holder as PortfolioTileDataCellViewHolder).bind(data.columns.toHeaders(row), row, columnMap)
    }
}
