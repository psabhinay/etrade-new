package com.etrade.mobilepro.portfolio.lightstreamer

import com.etrade.eo.streaming.UpdateInfo

val createPortfolioUpdateEvent: (UpdateInfo) -> PortfolioUpdateEvent = { info ->
    object : PortfolioUpdateEvent {
        override val symbol: String? = info.getNewValue(SYMBOL)
        override val instrumentId: String? = info.getNewValue(INSTRUMENT_ID)
        override val instrumentType: String? = info.getNewValue(INSTRUMENT_TYPE)
        override val accountId: String? = info.getNewValue(ACCOUNT_NUMBER)
    }
}
