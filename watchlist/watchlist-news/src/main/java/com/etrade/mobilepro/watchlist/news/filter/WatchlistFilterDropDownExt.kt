package com.etrade.mobilepro.watchlist.news.filter

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.news.api.filter.FilterDataSource
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.watchlist.news.R
import com.etrade.mobilepro.watchlistapi.Watchlist

fun Fragment.initFilterDropDown(
    filterDropDownTitle: String = getString(R.string.watchlist_select_title),
    filterDataSource: FilterDataSource<Watchlist>,
    dividerPosition: Int? = null,
    onFilterClickedUnAuth: () -> Unit,
    isAuth: UserAuthenticationState
): LiveData<() -> Unit> {
    val listener = MutableLiveData<(() -> Unit)>().apply { value = { } }
    val dropDownManager = BottomSheetSelector<Watchlist>(parentFragmentManager, resources)
    filterDataSource.items.observe(
        viewLifecycleOwner,
        { filterItems ->
            if (isAuth().not()) {
                listener.value = {
                    onFilterClickedUnAuth()
                }
            } else {
                dropDownManager.init(
                    tag = this::class.java.name,
                    title = filterDropDownTitle,
                    items = filterItems,
                    initialSelectedPosition = filterDataSource.getSelectedItemIndex(),
                    dividerPosition = dividerPosition
                )
                dropDownManager.setListener { _, selected ->
                    filterDataSource.setSelectedItem(selected)
                }
                listener.value = {
                    dropDownManager.updateInitialSelectedPosition(filterDataSource.getSelectedItemIndex())
                    dropDownManager.openDropDown()
                }
            }
        }
    )
    return listener
}
