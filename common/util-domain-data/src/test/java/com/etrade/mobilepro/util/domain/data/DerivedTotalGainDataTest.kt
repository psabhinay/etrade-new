package com.etrade.mobilepro.util.domain.data

import org.junit.jupiter.api.Test
import java.math.BigDecimal

class DerivedTotalGainDataTest {

    @Test
    fun `test deriveTotalGain`() {
        listOf<TestParams<BigDecimal, BigDecimal>>(
            TestParams(
                inputData = listOf(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO),
                expectedResult = BigDecimal.ZERO
            ),
            TestParams(
                inputData = listOf(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal("2"), BigDecimal("0.01"), BigDecimal("0.03")),
                expectedResult = BigDecimal("-0.04")
            ),
            TestParams(
                inputData = listOf(BigDecimal("15.02"), BigDecimal("13.00"), BigDecimal.ZERO, BigDecimal("0.01"), BigDecimal("0.03")),
                expectedResult = BigDecimal("-0.04")
            ),
            TestParams(
                inputData = listOf(BigDecimal("15.02"), BigDecimal("13.00"), BigDecimal("2"), BigDecimal("0.01"), BigDecimal("0.03")),
                expectedResult = BigDecimal("4")
            )
        ).forEach { testParams ->
            val result = deriveTotalGain(
                currentPrice = testParams.inputData[0],
                pricePaid = testParams.inputData[1],
                quantity = testParams.inputData[2],
                commission = testParams.inputData[3],
                fees = testParams.inputData[4]
            )
            assertTestResult(result, testParams.expectedResult)
        }
    }

    @Test
    fun `test deriveTotalGainPercentage`() {
        listOf<TestParams<BigDecimal, BigDecimal?>>(
            TestParams(
                inputData = listOf(BigDecimal.ZERO, BigDecimal("2")),
                expectedResult = BigDecimal.ZERO
            ),
            TestParams(
                inputData = listOf(BigDecimal("2"), BigDecimal.ZERO),
                expectedResult = null
            ),
            TestParams(
                inputData = listOf(BigDecimal("2"), BigDecimal("0.5")),
                expectedResult = BigDecimal("400")
            ),
            TestParams(
                inputData = listOf(BigDecimal("2"), BigDecimal("4")),
                expectedResult = BigDecimal("50")
            )
        ).forEach { testParams ->
            val result = deriveTotalGainPercentage(
                totalGain = testParams.inputData[0],
                totalCost = testParams.inputData[1]
            )
            assertTestResult(result, testParams.expectedResult)
        }
    }

    @Test
    fun `test deriveCommonTotalCost`() {
        listOf<TestParams<BigDecimal, BigDecimal?>>(
            TestParams(
                inputData = listOf(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO),
                expectedResult = BigDecimal.ZERO
            ),
            TestParams(
                inputData = listOf(BigDecimal("2"), BigDecimal.ZERO, BigDecimal("0.02"), BigDecimal("0.01")),
                expectedResult = BigDecimal("0.03")
            ),
            TestParams(
                inputData = listOf(BigDecimal("2"), BigDecimal("-3"), BigDecimal("0.02"), BigDecimal("0.01")),
                expectedResult = BigDecimal("6.03")
            ),
            TestParams(
                inputData = listOf(BigDecimal("2"), BigDecimal("3"), BigDecimal("0.02"), BigDecimal("0.01")),
                expectedResult = BigDecimal("6.03")
            )
        ).forEach { testParams ->
            val result = deriveCommonTotalCost(
                pricePaid = testParams.inputData[0],
                quantity = testParams.inputData[1],
                commission = testParams.inputData[2],
                fees = testParams.inputData[3]
            )
            assertTestResult(result, testParams.expectedResult)
        }
    }

    @Test
    fun `test deriveOptionTotalCost`() {
        listOf<TestParams<BigDecimal, BigDecimal?>>(
            TestParams(
                inputData = listOf(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ONE),
                expectedResult = BigDecimal.ZERO
            ),
            TestParams(
                inputData = listOf(BigDecimal("2"), BigDecimal.ZERO, BigDecimal("0.02"), BigDecimal("0.01"), BigDecimal.ONE),
                expectedResult = BigDecimal("0.03")
            ),
            TestParams(
                inputData = listOf(BigDecimal("2"), BigDecimal("-3"), BigDecimal("0.02"), BigDecimal("0.01"), BigDecimal.ONE),
                expectedResult = BigDecimal("6.03")
            ),
            TestParams(
                inputData = listOf(BigDecimal("2"), BigDecimal("3"), BigDecimal("0.02"), BigDecimal("0.01"), BigDecimal("2")),
                expectedResult = BigDecimal("12.03")
            )
        ).forEach { testParams ->
            val result = deriveOptionTotalCost(
                pricePaid = testParams.inputData[0],
                quantity = testParams.inputData[1],
                commission = testParams.inputData[2],
                fees = testParams.inputData[3],
                optionMultiplier = testParams.inputData[4]
            )
            assertTestResult(result, testParams.expectedResult)
        }
    }

    @Test
    fun `test deriveBondTotalCost`() {
        listOf<TestParams<BigDecimal, BigDecimal?>>(
            TestParams(
                inputData = listOf(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO),
                expectedResult = BigDecimal.ZERO
            ),
            TestParams(
                inputData = listOf(BigDecimal("2"), BigDecimal.ZERO, BigDecimal("0.02"), BigDecimal("0.01")),
                expectedResult = BigDecimal("0.03")
            ),
            TestParams(
                inputData = listOf(BigDecimal("200"), BigDecimal("-3"), BigDecimal("0.02"), BigDecimal("0.01")),
                expectedResult = BigDecimal("6.03")
            ),
            TestParams(
                inputData = listOf(BigDecimal("2"), BigDecimal("300"), BigDecimal("0.02"), BigDecimal("0.01")),
                expectedResult = BigDecimal("6.03")
            )
        ).forEach { testParams ->
            val result = deriveBondTotalCost(
                pricePaid = testParams.inputData[0],
                quantity = testParams.inputData[1],
                commission = testParams.inputData[2],
                fees = testParams.inputData[3]
            )
            assertTestResult(result, testParams.expectedResult)
        }
    }

    @Test
    fun `test deriveAccountTotalGainPercentage`() {
        listOf<TestParams<BigDecimal, BigDecimal?>>(
            TestParams(
                inputData = listOf(BigDecimal.ZERO, BigDecimal.ZERO),
                expectedResult = null
            ),
            TestParams(
                inputData = listOf(BigDecimal.ZERO, BigDecimal("2")),
                expectedResult = BigDecimal.ZERO
            ),
            TestParams(
                inputData = listOf(BigDecimal("20"), BigDecimal("10")),
                expectedResult = BigDecimal("200")
            )
        ).forEach { testParams ->
            val result = deriveAccountTotalGainPercentage(
                totalGain = testParams.inputData[0],
                totalPricePaid = testParams.inputData[1]
            )
            assertTestResult(result, testParams.expectedResult)
        }
    }

    private fun assertTestResult(result: BigDecimal?, expectedResult: BigDecimal?) {
        println("Test result = $result, expected result = $expectedResult")
        assert(
            result == expectedResult || expectedResult?.compareTo(result) == 0
        )
    }
}

private data class TestParams<T, R>(
    val inputData: List<T>,
    val expectedResult: R
)
