package com.etrade.mobilepro.valuepicker

import androidx.databinding.BindingAdapter

@BindingAdapter("valuePickerWidgetItem")
fun setValuePickerWidgetItem(lv: ValuePickerWidget, item: ValuePickerWidgetItem?) {
    item?.also {
        lv.hint = it.title
        lv.value = it.value
        lv.contentDescription = it.contentDescription
        lv.setOnClickListener { _ -> it.showValuePickerDropDown() }
        it.onDropDownSelectedListener = { title, description ->
            lv.value = title
            lv.contentDescription = description
        }
    }
}
