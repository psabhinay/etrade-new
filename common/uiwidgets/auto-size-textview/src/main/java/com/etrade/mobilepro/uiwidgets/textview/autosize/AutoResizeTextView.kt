package com.etrade.mobilepro.uiwidgets.textview.autosize

import android.content.Context
import android.content.res.Resources
import android.graphics.RectF
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint
import android.util.AttributeSet
import android.util.SparseIntArray
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatTextView
import com.etrade.mobilepro.uiwidgets.autosize.textview.R
import kotlin.math.abs

/**
 * This TextView dynamically and efficiently search the best font size
 * to fit a text to the available space. LAYOUT_WIDTH should be MATCH_PARENT
 * to get a maximum space and correct behaviour. If WRAP_CONTENT get set to
 * LAYOUT_WIDTH then you might get unexpected result like text size always be minimum size
 * If you are using constraint layout be sure that you set all the constraints
 * or set the minHeight if you don't give any constraint on bottom or top.
 * None of the android:autoSize*** method should use with this view since
 * they will contradict and introduce unexpected behavior.
 *
 * @property textRect the area for a specific font size
 * @property availableSpaceRect current available area
 * @property textCachedSizes cache for the text size and font
 * @property textPaint measuring the area for a text with specific font
 * @property autoMaxTextSize upper limit for the font size
 * @property autoMinTextSize lower limit for the font size
 * @property spacingMult when calculating line spacing it is multiplied by {@code spacingMult}
 * @property spacingAdd when calculating line spacing {@code spacingAdd} is added to result
 * @property widthLimit current calculated width of the view
 * @property heightLimit current calculated height of the view
 * @property maxLines the height of the TextView to be at most {@code maxLines} tall.
 * @property autoEnableCache switch to control enable cache for calculated font size by default this is true
 * @property initialized control to show if this view is initialized correctly
 *
 */
class AutoResizeTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = android.R.attr.textViewStyle
) : AppCompatTextView(context, attrs, defStyle) {

    private val textRect: RectF = RectF()
    private val availableSpaceRect: RectF = RectF()
    private val textCachedSizes: SparseIntArray = SparseIntArray()
    private val textPaint: TextPaint = TextPaint(this.paint)
    private var autoMaxTextSize = FLOAT_ZERO
    private var autoMinTextSize = FLOAT_TEN
    private var spacingMult = FLOAT_ONE
    private var spacingAdd = FLOAT_ZERO
    private var maxLines = INT_ONE
    private var autoEnableCache = true
    private var initialized = false
    private val noSpaceRect = RectF(FLOAT_ZERO, FLOAT_ZERO, FLOAT_ZERO, FLOAT_ZERO)

    private val widthLimit
        get() = abs(measuredWidth - compoundPaddingLeft - compoundPaddingRight)

    private val heightLimit
        get() = measuredHeight - compoundPaddingBottom - compoundPaddingTop

    private val Int.isSingleLine
        get() = this == INT_ONE

    private val Float.isSameTextSize
        get() = this == textSize

    init {
        initialize(context, attrs, defStyle)
    }

    /**
     * Initialize the view gets called inside constructors
     */
    private fun initialize(context: Context, attrs: AttributeSet?, defStyle: Int) {
        setStyleables(context, attrs, defStyle)
        initialized = true
    }

    private fun setStyleables(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int
    ) {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.AutoSizeTextView,
            defStyle, INT_ZERO
        ).apply {
            try {
                autoMinTextSize = getDimensionPixelSize(
                    R.styleable.AutoSizeTextView_auto_size_minTextSize,
                    context.resources.getDimensionPixelSize(R.dimen.font_size_xxxsmall)
                ).toFloat()
                autoMaxTextSize = getDimensionPixelSize(
                    R.styleable.AutoSizeTextView_auto_size_maxTextSize,
                    INT_ZERO
                ).toFloat()
                if (autoMaxTextSize == FLOAT_ZERO) {
                    autoMaxTextSize = this@AutoResizeTextView.textSize
                }
                autoEnableCache =
                    getBoolean(R.styleable.AutoSizeTextView_auto_size_enableCache, true)
            } finally {
                recycle()
            }
        }
    }

    /**
     * Sets the [text] to be displayed and adjust the current font size
     */
    override fun setText(text: CharSequence, type: BufferType) {
        super.setText(text, type)
        adjustTextSize()
    }

    /**
     * Sets a [size] as max font size and adjust the current
     */
    override fun setTextSize(size: Float) {
        autoMaxTextSize = size
        textCachedSizes.clear()
        adjustTextSize()
    }

    /**
     * Sets the height of the TextView to be at most [maxLines] tall.
     * and adjust the current font size
     */
    override fun setMaxLines(maxlines: Int) {
        super.setMaxLines(maxlines)
        maxLines = maxlines
        adjustTextSize()
    }

    override fun getMaxLines(): Int {
        return maxLines
    }

    /**
     * Sets the height of the TextView to be at most 1 tall
     * and adjust the current font size
     */
    override fun setSingleLine() {
        super.setSingleLine()
        maxLines = INT_ONE
        adjustTextSize()
    }

    /**
     * Sets the height of the TextView to be at most 1 tall if true
     * otherwise it can be any
     * and adjust the current font size
     */
    override fun setSingleLine(singleLine: Boolean) {
        super.setSingleLine(singleLine)
        maxLines = if (singleLine) { INT_ONE } else { INT_NEGATIVE_ONE }
        adjustTextSize()
    }

    /**
     * Sets the height of the TextView to be exactly [lines] tall.
     * and adjust the current font size
     */
    override fun setLines(lines: Int) {
        super.setLines(lines)
        maxLines = lines
        adjustTextSize()
    }

    /**
     * Sets [size] as max font size.
     * and adjust the current font size
     */
    override fun setTextSize(unit: Int, size: Float) {
        val c = this.context
        val r = if (c == null) {
            Resources.getSystem()
        } else {
            c.resources
        }
        autoMaxTextSize = TypedValue.applyDimension(unit, size, r.displayMetrics)
        textCachedSizes.clear()
        adjustTextSize()
    }

    override fun setLineSpacing(add: Float, mult: Float) {
        super.setLineSpacing(add, mult)
        spacingMult = mult
        spacingAdd = add
    }

    /**
     * Sets [minTextSize] as a [autoMinTextSize].
     * and adjust the current font size
     */
    fun setMinTextSize(minTextSize: Float) {
        autoMinTextSize = minTextSize
        adjustTextSize()
    }

    /**
     * Sets [maxTextSize] as a [autoMaxTextSize].
     * and adjust the current font size
     */
    fun setMaxTextSize(maxTextSize: Float) {
        autoMaxTextSize = maxTextSize
        adjustTextSize()
    }

    /**
     * Calculate the available space and find the best text size and set it as current
     */
    private fun adjustTextSize() {
        if (initialized) {
            availableSpaceRect.right = widthLimit.toFloat()
            availableSpaceRect.bottom = heightLimit.toFloat()

            if (availableSpaceRect == noSpaceRect) return
            val adjustedTextSize = efficientTextSizeSearch(
                autoMinTextSize.toInt(),
                autoMaxTextSize.toInt(),
                availableSpaceRect
            ).toFloat()

            if (adjustedTextSize.isSameTextSize) return
            super.setTextSize(
                TypedValue.COMPLEX_UNIT_PX,
                adjustedTextSize
            )
        }
    }

    /**
     * Enable font size caching if [enable] is true
     * otherwise do a search each time to find the best fit
     * and adjust the current font size
     */
    fun enableSizeCache(enable: Boolean) {
        autoEnableCache = enable
        textCachedSizes.clear()
        adjustTextSize()
    }

    /**
     * Gets the font size from cache if it is cached otherwise search for the best fit
     */
    private fun efficientTextSizeSearch(
        start: Int,
        end: Int,
        availableSpace: RectF
    ): Int = if (!autoEnableCache) {
        val size = binarySearch(start, end, availableSpace)
        size
    } else {
        val text = this.text.toString()
        val key = text.length
        if (key == INT_ZERO) {
            textSize.toInt()
        } else {
            var size = textCachedSizes[key]
            if (size != INT_ZERO) {
                size
            } else {
                size = binarySearch(start, end, availableSpace)
                textCachedSizes.put(key, size)
                size
            }
        }
    }

    /**
     * Adjust the current font size when text change
     */
    override fun onTextChanged(text: CharSequence, start: Int, before: Int, after: Int) {
        super.onTextChanged(text, start, before, after)
        adjustTextSize()
    }

    /**
     * Adjust the current font size if width or height change
     */
    override fun onSizeChanged(width: Int, height: Int, oldwidth: Int, oldheight: Int) {
        textCachedSizes.clear()
        super.onSizeChanged(width, height, oldwidth, oldheight)
        if (width != oldwidth || height != oldheight) {
            adjustTextSize()
        }
    }

    /**
     * Finds if suggested font size fits to available space
     *
     * @param suggestedSize possible candidate for the font size
     * @param availableSpace current available space for the text
     *
     * @return [INT_NEGATIVE_ONE] if [availableSpace] is too big for text
     * otherwise it returns [INT_ONE]
     */
    fun onTestSize(suggestedSize: Int, availableSpace: RectF): Int {
        textPaint.textSize = suggestedSize.toFloat()
        val text = text.toString()
        if (maxLines.isSingleLine) {
            textRect.bottom = textPaint.fontSpacing
            textRect.right = textPaint.measureText(text)
        } else {
            val layout =
                StaticLayout.Builder.obtain(text, INT_ZERO, text.length, textPaint, widthLimit)
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                    .setLineSpacing(spacingMult, spacingAdd)
                    .setIncludePad(true)
                    .build()
            if (maxLines != INT_NEGATIVE_ONE && layout.lineCount > maxLines) {
                return INT_ONE
            }
            textRect.bottom = layout.height.toFloat()
            var maxWidth = INT_NEGATIVE_ONE
            for (i in 0 until layout.lineCount) {
                val calculatedWidth = layout.getLineRight(i) - layout.getLineLeft(i)
                if (maxWidth < calculatedWidth) {
                    maxWidth = calculatedWidth.toInt()
                }
            }
            textRect.right = maxWidth.toFloat()
        }
        textRect.offsetTo(FLOAT_ZERO, FLOAT_ZERO)
        return if (availableSpace.contains(textRect)) INT_NEGATIVE_ONE else INT_ONE
    }

    /**
     * Do the binary search to find the best fit font size
     *
     * @param start lower font size limit of the search range
     * @param end upper font size limit of the search range
     * @param availableSpace current available space for the text
     */
    private fun binarySearch(
        start: Int,
        end: Int,
        availableSpace: RectF
    ): Int {
        var lastBest = start
        var lo = start
        var hi = end - 1
        while (lo <= hi) {
            val mid = lo + hi ushr 1
            val midValCmp = onTestSize(mid, availableSpace)
            if (midValCmp < 0) {
                lastBest = lo
                lo = mid + 1
            } else {
                if (midValCmp <= 0) {
                    return mid
                }
                hi = mid - 1
                lastBest = hi
            }
        }
        return lastBest
    }

    companion object {
        private const val FLOAT_ZERO = 0.0f
        private const val FLOAT_ONE = 1.0f
        private const val FLOAT_TEN = 10.0f
        private const val INT_ZERO = 0
        private const val INT_ONE = 1
        private const val INT_NEGATIVE_ONE = -1
    }
}
