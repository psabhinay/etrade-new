package com.etrade.mobilepro.marketscalendar.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class MarketsCalendarPagerAdapter(
    private val fragment: Fragment,
    private val items: List<MarketsCalendarTab>
) : FragmentStateAdapter(fragment) {

    override fun createFragment(position: Int): Fragment {
        val fragmentClass = items[position].fragmentClass

        return fragment.childFragmentManager
            .fragmentFactory
            .instantiate(fragmentClass.classLoader!!, fragmentClass.name)
    }

    fun getPageTitle(position: Int): String {
        return fragment.getString(items[position].title)
    }

    override fun getItemCount(): Int = items.size
}
