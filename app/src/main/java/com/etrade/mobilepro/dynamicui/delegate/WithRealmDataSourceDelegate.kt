package com.etrade.mobilepro.dynamicui.delegate

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.dynamicui.CachedResponseModel
import com.etrade.mobilepro.dynamicui.DynamicScreenRpcService
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import io.reactivex.Single
import io.realm.Realm

abstract class WithRealmDataSourceDelegate constructor(
    private val dynamicScreenRpcService: DynamicScreenRpcService
) : SimpleCachedResource.DataSourceDelegate<CachedResponseModel, ScreenRequest> {

    override fun fetchFromNetwork(id: ScreenRequest): Single<CachedResponseModel> {
        val requestDto = id.createRequestBody()
        return dynamicScreenRpcService.getScreen(id.url, requestDto)
            .map {
                val cachedModel = CachedResponseModel(
                    id = id.contentKey,
                    retrievedTimestamp = System.currentTimeMillis(),
                    response = it
                )
                cachedModel
            }
    }

    override fun putToCache(fetchedResult: CachedResponseModel): Single<CachedResponseModel> = Single.fromCallable {
        getRealmInstance().use { realm ->
            realm.executeTransaction { realmTxn ->
                realmTxn.insertOrUpdate(fetchedResult)
            }
        }
        fetchedResult
    }

    override fun fetchFromDb(id: ScreenRequest): Single<CachedResponseModel> = Single.fromCallable {
        getRealmInstance().use { realm ->
            val cachedResponse = realm
                .where(CachedResponseModel::class.java)
                .equalTo("id", id.contentKey)
                .findFirst() ?: throw NoSuchElementException()
            realm.copyFromRealm(cachedResponse)
        }
    }

    abstract fun getRealmInstance(): Realm
}
