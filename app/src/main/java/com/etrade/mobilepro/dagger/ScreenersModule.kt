package com.etrade.mobilepro.dagger

import android.content.res.Resources
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.common.di.Mod
import com.etrade.mobilepro.markets.DefaultMarketScreenersRouter
import com.etrade.mobilepro.mod.auth.api.AuthTokenProvider
import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.screener.api.EtfFundGroup
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.MutualFundGroup
import com.etrade.mobilepro.screener.api.ScreenerFilterJsonGenerator
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.datasource.CountryDataSource
import com.etrade.mobilepro.screener.api.datasource.EtfFundDataSource
import com.etrade.mobilepro.screener.api.datasource.IndustryTypeDataSource
import com.etrade.mobilepro.screener.api.datasource.MovingAveragesDataSource
import com.etrade.mobilepro.screener.api.datasource.MutualFundDataSource
import com.etrade.mobilepro.screener.api.datasource.PercentDataSource
import com.etrade.mobilepro.screener.api.datasource.RegionDataSource
import com.etrade.mobilepro.screener.api.datasource.ScreenerSetsDataSource
import com.etrade.mobilepro.screener.api.datasource.SectorDataSource
import com.etrade.mobilepro.screener.api.filter.ScreenerFilterFactory
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Etf
import com.etrade.mobilepro.screener.api.qualifier.MutualFund
import com.etrade.mobilepro.screener.api.qualifier.Stock
import com.etrade.mobilepro.screener.api.repo.FundCategoriesRepo
import com.etrade.mobilepro.screener.api.repo.ScreenersRepo
import com.etrade.mobilepro.screener.api.result.PrescreenData
import com.etrade.mobilepro.screener.api.result.RequestToResultParameterMapper
import com.etrade.mobilepro.screener.data.DefaultScreenerSetsDataSource
import com.etrade.mobilepro.screener.data.aggregator.DefaultScreenerFilterAggregator
import com.etrade.mobilepro.screener.data.datasource.DefaultCountryDataSource
import com.etrade.mobilepro.screener.data.datasource.DefaultEtfFundDataSource
import com.etrade.mobilepro.screener.data.datasource.DefaultIndustryTypeDataSource
import com.etrade.mobilepro.screener.data.datasource.DefaultMovingAveragesDataSource
import com.etrade.mobilepro.screener.data.datasource.DefaultMutualFundDataSource
import com.etrade.mobilepro.screener.data.datasource.DefaultPercentDataSource
import com.etrade.mobilepro.screener.data.datasource.DefaultRegionDataSource
import com.etrade.mobilepro.screener.data.datasource.DefaultSectorDataSource
import com.etrade.mobilepro.screener.data.filter.DefaultScreenerFilterFactory
import com.etrade.mobilepro.screener.data.interactor.DefaultScreenerInteractor
import com.etrade.mobilepro.screener.data.json.DefaultRequestToResultParameterMapper
import com.etrade.mobilepro.screener.data.json.DefaultScreenerFilterJsonGenerator
import com.etrade.mobilepro.screener.data.repo.DefaultFundCategoriesRepo
import com.etrade.mobilepro.screener.data.repo.DefaultScreenersRepo
import com.etrade.mobilepro.screener.data.repo.mapper.FundCategoryResponseMapper
import com.etrade.mobilepro.screener.data.repo.mapper.ResultResponseMapper
import com.etrade.mobilepro.screener.data.rest.ResultResponse
import com.etrade.mobilepro.screener.data.rest.ScreenerApiClient
import com.etrade.mobilepro.screener.data.result.DefaultResultColumnTitleProvider
import com.etrade.mobilepro.screener.data.result.DefaultResultColumnsProvider
import com.etrade.mobilepro.screener.data.result.DefaultResultRowDataProvider
import com.etrade.mobilepro.screener.data.result.ResultColumn
import com.etrade.mobilepro.screener.data.result.ResultColumnTitleProvider
import com.etrade.mobilepro.screener.data.result.ResultColumnsProvider
import com.etrade.mobilepro.screener.data.result.ResultColumnsResolver
import com.etrade.mobilepro.screener.data.result.ResultRowDataProvider
import com.etrade.mobilepro.screener.data.result.column.EtfResultColumnsResolver
import com.etrade.mobilepro.screener.data.result.column.MutualFundResultColumnsResolver
import com.etrade.mobilepro.screener.data.result.column.ResultColumnRepo
import com.etrade.mobilepro.screener.data.result.column.StockResultColumnsResolver
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.common.DefaultScreenersPillViewHelper
import com.etrade.mobilepro.screener.presentation.common.FundGroupSelectionFlowHandler
import com.etrade.mobilepro.screener.presentation.common.ScreenersPillViewHelper
import com.etrade.mobilepro.screener.presentation.common.SelectionFlowHandler
import com.etrade.mobilepro.screener.presentation.stock.technicals.DefaultSeekerInputDependenciesFactory
import com.etrade.mobilepro.screener.presentation.stock.technicals.SeekerInputDependenciesFactory
import com.etrade.mobilepro.tableviewutils.api.ColumnRepo
import com.etrade.mobilepro.util.color.DataColorResolver
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Provider
import javax.inject.Singleton

@Suppress("TooManyFunctions")
@Module
class ScreenersModule {

    @Provides
    @Singleton
    internal fun provideScreenerSetsDataSource(
        resources: Resources,
        aggregator: ScreenerFilterAggregator
    ): ScreenerSetsDataSource = DefaultScreenerSetsDataSource(resources, aggregator)

    @Provides
    @Singleton
    internal fun provideSeekerInputDepsFactory(resources: Resources): SeekerInputDependenciesFactory =
        DefaultSeekerInputDependenciesFactory(resources)

    @Provides
    internal fun provideSectorsData(resources: Resources): SectorDataSource = DefaultSectorDataSource(resources)

    @Provides
    internal fun provideRegionData(resources: Resources): RegionDataSource = DefaultRegionDataSource(resources)

    @Provides
    internal fun provideCountryData(resources: Resources): CountryDataSource = DefaultCountryDataSource(resources)

    @Provides
    internal fun providePercentData(resources: Resources): PercentDataSource = DefaultPercentDataSource(resources)

    @Provides
    internal fun provideIndustryTypeData(resources: Resources): IndustryTypeDataSource =
        DefaultIndustryTypeDataSource(resources)

    @Provides
    internal fun provideFundData(resources: Resources): MutualFundDataSource = DefaultMutualFundDataSource(resources)

    @Provides
    internal fun provideEtfFundData(
        resources: Resources
    ): EtfFundDataSource = DefaultEtfFundDataSource(resources)

    @Provides
    internal fun provideMovingAveragesData(resources: Resources): MovingAveragesDataSource =
        DefaultMovingAveragesDataSource(resources)

    @Suppress("LongParameterList")
    @Provides
    internal fun provideScreenerFilterFactory(
        sectorDataSource: SectorDataSource,
        fundDataSource: MutualFundDataSource,
        etfFundDataSource: EtfFundDataSource,
        regionDataSource: RegionDataSource,
        countryDataSource: CountryDataSource,
        percentDataSource: PercentDataSource,
        industryTypeDataSource: IndustryTypeDataSource,
        movingAveragesDataSource: MovingAveragesDataSource
    ): ScreenerFilterFactory =
        DefaultScreenerFilterFactory(
            sectorDataSource, fundDataSource, etfFundDataSource, regionDataSource, percentDataSource, countryDataSource,
            industryTypeDataSource, movingAveragesDataSource
        )

    @Provides
    @Singleton
    internal fun provideScreenerFilterJsonGenerator(
        filterGenerators: Map<Class<*>, @JvmSuppressWildcards Provider<FilterModelGenerator>>,
        mapper: RequestToResultParameterMapper,
        moshi: Moshi
    ): ScreenerFilterJsonGenerator =
        DefaultScreenerFilterJsonGenerator(moshi, filterGenerators, mapper)

    @Provides
    @Singleton
    internal fun provideScreenerFilterAggregator(
        screenerFilterFactory: ScreenerFilterFactory,
        screenerFilterJsonGenerator: ScreenerFilterJsonGenerator
    ): ScreenerFilterAggregator =
        DefaultScreenerFilterAggregator(screenerFilterFactory, screenerFilterJsonGenerator)

    @Provides
    @Singleton
    internal fun provideScreenerApiClient(@Mod retrofit: Retrofit): ScreenerApiClient = retrofit.create(ScreenerApiClient::class.java)

    @Provides
    @Singleton
    internal fun provideScreenerRepo(
        apiClient: ScreenerApiClient,
        tokenApiProvider: AuthTokenProvider,
        mapper: (@JvmSuppressWildcards ResultResponse) -> @JvmSuppressWildcards List<PrescreenData>,
        moshi: Moshi
    ): ScreenersRepo =
        DefaultScreenersRepo(apiClient, tokenApiProvider, mapper, moshi)

    @Provides
    @Singleton
    internal fun provideFundCategoriesRepo(
        apiClient: ScreenerApiClient,
        fundDataSource: EtfFundDataSource,
        mfFundDataSource: MutualFundDataSource,
        mapper: (@JvmSuppressWildcards String) -> FundCategory
    ): FundCategoriesRepo = DefaultFundCategoriesRepo(apiClient, fundDataSource, mfFundDataSource, mapper)

    @Provides
    internal fun provideFundCategoryResponseMapper(): ((String) -> FundCategory) = FundCategoryResponseMapper()::map

    @Provides
    internal fun providePrescreenResponseMapper(): ((ResultResponse) -> List<PrescreenData>) = ResultResponseMapper()::map

    @Provides
    internal fun provideEtfFundGroupSelectionFlowHandler(fundCategoriesRepo: FundCategoriesRepo): SelectionFlowHandler<EtfFundGroup> =
        FundGroupSelectionFlowHandler(fundCategoriesRepo)

    @Provides
    internal fun provideMfundGroupSelectionFlowHandler(fundCategoriesRepo: FundCategoriesRepo): SelectionFlowHandler<MutualFundGroup> =
        FundGroupSelectionFlowHandler(fundCategoriesRepo)

    @Provides
    internal fun provideScreenersPillViewHelper(resources: Resources): ScreenersPillViewHelper =
        DefaultScreenersPillViewHelper(resources)

    @Provides
    @Singleton
    @Stock
    internal fun provideStockScreenerInteractor(
        aggregator: ScreenerFilterAggregator,
        repo: ScreenersRepo,
        mobileQuoteRepo: MobileQuoteRepo
    ): ScreenerInteractor =
        DefaultScreenerInteractor(aggregator, mobileQuoteRepo, repo, ScreenerType.Group.STOCK)

    @Provides
    @Singleton
    @MutualFund
    internal fun provideMFScreenerInteractor(aggregator: ScreenerFilterAggregator, repo: ScreenersRepo, mobileQuoteRepo: MobileQuoteRepo): ScreenerInteractor =
        DefaultScreenerInteractor(aggregator, mobileQuoteRepo, repo, ScreenerType.Group.MUTUAL_FUND)

    @Provides
    @Singleton
    @Etf
    internal fun provideETFScreenerInteractor(aggregator: ScreenerFilterAggregator, repo: ScreenersRepo, mobileQuoteRepo: MobileQuoteRepo): ScreenerInteractor =
        DefaultScreenerInteractor(aggregator, mobileQuoteRepo, repo, ScreenerType.Group.ETF)

    @Provides
    internal fun provideResultColumnTitleProvider(resources: Resources): ResultColumnTitleProvider =
        DefaultResultColumnTitleProvider(resources)

    @Provides
    internal fun provideResultRowDataProvider(mfMapper: FundQuoteDetailItemsMapper, dataColorResolver: DataColorResolver): ResultRowDataProvider =
        DefaultResultRowDataProvider(mfMapper, dataColorResolver)

    @Suppress("LongParameterList")
    @Provides
    internal fun provideResultColumnsProvider(
        @Stock stockResolver: ResultColumnsResolver,
        @MutualFund mfResolver: ResultColumnsResolver,
        @Etf etfResolver: ResultColumnsResolver,
        @Stock stockColumnRepo: ColumnRepo<ResultColumn>,
        @MutualFund mfColumnRepo: ColumnRepo<ResultColumn>,
        @Etf etfColumnRepo: ColumnRepo<ResultColumn>
    ): ResultColumnsProvider = DefaultResultColumnsProvider(
        stockResolver,
        mfResolver,
        etfResolver,
        stockColumnRepo,
        mfColumnRepo,
        etfColumnRepo
    )

    @Provides
    @Stock
    internal fun provideStockResultColumnsResolver(): ResultColumnsResolver = StockResultColumnsResolver()

    @Provides
    @MutualFund
    internal fun provideMutualFundResultColumnsResolver(): ResultColumnsResolver = MutualFundResultColumnsResolver()

    @Provides
    @Etf
    internal fun provideEtfResultColumnsResolver(): ResultColumnsResolver = EtfResultColumnsResolver()

    @Provides
    @Stock
    internal fun provideStockColumnsRepo(storage: KeyValueStorage): ColumnRepo<ResultColumn> = ResultColumnRepo(storage, ScreenerType.Group.STOCK)

    @Provides
    @MutualFund
    internal fun provideMfColumnsRepo(storage: KeyValueStorage): ColumnRepo<ResultColumn> = ResultColumnRepo(storage, ScreenerType.Group.MUTUAL_FUND)

    @Provides
    @Etf
    internal fun provideEtfColumnsRepo(storage: KeyValueStorage): ColumnRepo<ResultColumn> = ResultColumnRepo(storage, ScreenerType.Group.ETF)

    @Provides
    internal fun provideRequestToResultParameterMapper(): RequestToResultParameterMapper = DefaultRequestToResultParameterMapper()

    @Provides
    internal fun provideScreenersRouter(): ScreenersRouter = DefaultMarketScreenersRouter()
}
