package com.etrade.mobilepro.landing.presentation.adapter.delegate

import android.view.LayoutInflater
import android.view.ViewGroup
import com.etrade.mobilepro.landing.presentation.adapter.LandingPageItem
import com.etrade.mobilepro.landing.presentation.adapter.LandingPageSelectionItem
import com.etrade.mobilepro.landing.presentation.adapter.viewholder.LandingPageItemViewHolder
import com.etrade.mobilepro.landing.presentation.databinding.LandingPageSelectionItemBinding
import com.etrade.mobilepro.util.android.extension.inflate
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class LandingPageItemDelegate(
    private val listener: (LandingPageItem) -> Unit
) : AbsListItemAdapterDelegate<LandingPageItem, LandingPageSelectionItem, LandingPageItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): LandingPageItemViewHolder {
        val binding = LandingPageSelectionItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LandingPageItemViewHolder(binding, listener)
    }

    override fun isForViewType(item: LandingPageSelectionItem, items: MutableList<LandingPageSelectionItem>, position: Int): Boolean =
        item is LandingPageItem

    override fun onBindViewHolder(item: LandingPageItem, holder: LandingPageItemViewHolder, payloads: MutableList<Any>) =
        holder.bindTo(item)
}
