package com.etrade.mobilepro.trade.optionlegselect

import android.content.res.Resources
import com.etrade.mobilepro.optionschains.api.Option
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.util.formatters.injectAMLabel

fun TradeLeg.toOptionLegItem(resources: Resources): OptionLegItem {
    return OptionLegItem(
        quantity = quantity.toString(),
        displayName = formatDisplayName(resources).injectAMLabel(isAMOption),
        isDeletePending = if (this is OptionTradeLeg) {
            isDeletePending
        } else {
            false
        }
    )
}

internal data class StockTradeLeg(
    override val symbol: String,
    override val transactionType: TransactionType,
    override val quantity: Int,
    override val isAMOption: Boolean = false
) : TradeLeg {
    override val displaySymbol: String
        get() = symbol
}

internal class OptionTradeLegWithOption(
    val option: Option,
    transactionType: TransactionType,
    quantity: Int,
    symbol: String = option.symbol,
    displaySymbol: String = option.displaySymbol,
    isAMOption: Boolean = option.isAMOption,
    isDeletePending: Boolean
) : OptionTradeLeg(transactionType, quantity, symbol, displaySymbol, isAMOption, isDeletePending)

internal open class OptionTradeLeg(
    override val transactionType: TransactionType,
    override val quantity: Int,
    override val symbol: String,
    override val displaySymbol: String,
    override val isAMOption: Boolean,
    val isDeletePending: Boolean
) : TradeLeg

internal fun TradeLeg.formatDisplayName(resources: Resources): String = "${formatAction(resources)} $displaySymbol"

internal fun TradeLeg.formatAction(resources: Resources): String = resources.getString(transactionType.getLabel())
