package com.etrade.mobilepro.compose.components

import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

@Composable
fun <T> LiveDataObserve(liveData: LiveData<T>, observer: Observer<T>) {
    DisposableEffect(Unit) {
        liveData.observeForever(observer)
        onDispose {
            liveData.removeObserver(observer)
        }
    }
}
