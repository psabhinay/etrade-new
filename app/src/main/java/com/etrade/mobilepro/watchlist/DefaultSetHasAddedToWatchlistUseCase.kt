package com.etrade.mobilepro.watchlist

import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.watchlist.repo.SetHasAddedToWatchlistUseCase
import javax.inject.Inject

class DefaultSetHasAddedToWatchlistUseCase @Inject constructor(
    private val userPreferences: UserPreferences
) : SetHasAddedToWatchlistUseCase {
    override suspend fun execute(parameter: Boolean) {
        userPreferences.setHasAddedToWatchlist(parameter)
    }
}
