package com.etrade.mobilepro.orders.list.api

import com.etrade.mobilepro.orders.api.OrderStatus

/**
 * Order item specific to stock plan orders
 */
data class StockPlanOrderItem(
    override val orderNumber: String,
    override val orderStatus: OrderStatus,
    override val symbols: List<String>,
    val date: String,
    val planType: String,
    val orderType: String,
    val priceType: String,
    val executionPrice: String,
    val sharesToExercise: String,
    val exercised: String,
    val sharesToSell: String,
    val sold: String,
    val accountIndex: String
) : OrderItem
