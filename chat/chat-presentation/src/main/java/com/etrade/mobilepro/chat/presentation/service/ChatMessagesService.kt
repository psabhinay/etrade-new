package com.etrade.mobilepro.chat.presentation.service

import android.app.Activity
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Binder
import android.provider.Settings
import androidx.annotation.MainThread
import androidx.core.app.TaskStackBuilder
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.api.ChatParamsRequestModel
import com.etrade.mobilepro.chat.api.ChatState
import com.etrade.mobilepro.chat.api.ChatState.Error
import com.etrade.mobilepro.chat.api.ChatState.Loading
import com.etrade.mobilepro.chat.api.ChatState.MessagesShown
import com.etrade.mobilepro.chat.api.ChatState.TerminatedByAgent
import com.etrade.mobilepro.chat.api.ChatState.TerminatedByUser
import com.etrade.mobilepro.chat.api.message.models.ChatRequestModel
import com.etrade.mobilepro.chat.presentation.R
import com.etrade.mobilepro.chat.presentation.common.ChatConstants
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.ACTION_START_MESSAGES_SERVICE
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.ACTION_STOP_MESSAGES_SERVICE
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_ACTIVITY_CLASS
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_EXTRA_OPEN_MESSAGES
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_NOTIFICATION_ID
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_PARAMS_REQUEST_BODY
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_SESSION_ID
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_START_POINT_MESSAGE
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.IS_CHAT_LAUNCHED_FROM_MESSAGES_CENTER
import com.etrade.mobilepro.chat.presentation.common.ChatEndListener
import com.etrade.mobilepro.chat.presentation.common.ChatEndReceiver
import com.etrade.mobilepro.chat.presentation.common.ChatFloatingButton
import com.etrade.mobilepro.chat.presentation.common.OnFloatingButtonTouchListener
import com.etrade.mobilepro.chat.usecase.ChatRequestUseCase
import com.etrade.mobilepro.common.createNotification
import com.etrade.mobilepro.dialog.viewmodel.DialogViewModel
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.util.runWithRetries
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import javax.inject.Inject

@Suppress("TooManyFunctions")
class ChatMessagesService : Service(), HasAndroidInjector, OnFloatingButtonTouchListener, ChatEndListener {

    private val logger by lazy { LoggerFactory.getLogger(ChatMessagesService::class.java) }

    val chatState: LiveData<ChatState> get() = _chatState

    private val scope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    @Inject
    lateinit var chatMessagesUseCase: ChatRequestUseCase

    @Inject
    lateinit var user: UserSessionHandler

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var dialogViewModel: DialogViewModel

    private val _chatState = MutableLiveData<ChatState>()
    private var isInQueue = false
    private val endChatReceiver = ChatEndReceiver()
    private lateinit var chatFloatingButton: ChatFloatingButton
    private lateinit var intent: Intent
    private lateinit var sessionID: String
    private lateinit var activityClass: Class<out Activity>
    private lateinit var chatParamsRequestBody: ChatParamsRequestModel
    private var estimatedAwaitTime = -1

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onBind(intent: Intent) = MessagesBinder()

    override fun onCreate() {
        AndroidInjection.inject(this)
        chatFloatingButton = ChatFloatingButton(this, this)
        chatFloatingButton.setupFloatingButton()
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        when (intent.action) {
            ACTION_START_MESSAGES_SERVICE -> start(intent)
            ACTION_STOP_MESSAGES_SERVICE -> terminate(true)
        }
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        isRunning = false
        endChatReceiver.unsubscribe(this)
        removeFloatingButton()
        stopForeground(true)
        scope.cancel()
        super.onDestroy()
    }

    override fun onTouchFloatingButton() {
        removeFloatingButton()
        startActivity(createStartActivityIntent())
    }

    override fun onChatEnded() {
        terminate(false)
    }

    // Invoked when the app was closed by clear task option when the chat service was in the background
    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        terminate(false)
        user.unAuthenticateSession()
    }

    fun sendMessage(message: String) {
        (_chatState.value as? MessagesShown)?.messages?.toMutableList()?.let {
            it.add(ChatMessage.Loading)
            _chatState.value = MessagesShown(it)
        }
        scope.launch { postMessage(message) }
    }

    fun showFloatingButton() {
        if (isRunning && Settings.canDrawOverlays(this)) {
            chatFloatingButton.show()
        }
    }

    fun removeFloatingButton() {
        chatFloatingButton.remove()
        if (isRunning) {
            updateNotification()
        }
    }

    private fun initParamsFromIntent(intent: Intent) {
        @Suppress("UNCHECKED_CAST")
        with(requireNotNull(intent.extras)) {
            activityClass = getSerializable(CHAT_ACTIVITY_CLASS) as Class<out Activity>
            chatParamsRequestBody = getSerializable(CHAT_PARAMS_REQUEST_BODY) as ChatParamsRequestModel
            sessionID = requireNotNull(getString(CHAT_SESSION_ID))
        }
    }

    private fun updateIntentDestination(intent: Intent) {
        this.intent.apply {
            putExtra(IS_CHAT_LAUNCHED_FROM_MESSAGES_CENTER, intent.getBooleanExtra(IS_CHAT_LAUNCHED_FROM_MESSAGES_CENTER, false))
            putExtra(CHAT_START_POINT_MESSAGE, intent.getStringExtra(CHAT_START_POINT_MESSAGE))
        }
    }

    private fun createStartActivityIntent() = Intent(this, activityClass).apply {
        putExtras(requireNotNull(intent.extras))
        putExtra(CHAT_EXTRA_OPEN_MESSAGES, true).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    }

    private fun createChatNotification(unread: Int = 0): Notification {
        val intent = createStartActivityIntent()
        val stackBuilder = TaskStackBuilder.create(this).apply {
            addParentStack(activityClass)
            addNextIntent(intent)
        }

        val resultIntentFlags = PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        val resultPendingIntent = stackBuilder.getPendingIntent(0, resultIntentFlags)
        val endChatPendingIntent = PendingIntent.getBroadcast(
            this,
            0,
            Intent(this, ChatEndReceiver::class.java).apply { action = ChatConstants.ACTION_CHAT_TERMINATE_BY_USER },
            PendingIntent.FLAG_IMMUTABLE
        )
        return createNotification(this)
            .setSmallIcon(android.R.drawable.sym_action_chat)
            .setContentTitle("E*TRADE")
            .setContentText(getChatNotificationMessage(unread))
            .setOngoing(true)
            .addAction(android.R.drawable.ic_delete, "End Chat", endChatPendingIntent)
            .setContentIntent(resultPendingIntent)
            .setAutoCancel(false)
            .build()
    }

    fun start(intent: Intent) {
        if (user.isUserLoggedIn() && !isRunning) {
            this.intent = intent
            initParamsFromIntent(intent)
            endChatReceiver.subscribe(this)
            _chatState.value = Loading
            isRunning = true
            isInQueue = true
            chatMessagesUseCase.clearMessages()
            startForeground(CHAT_NOTIFICATION_ID, createChatNotification())
            scope.launch { getMessages() }
        } else if (isRunning) {
            updateIntentDestination(intent)
            val messages = chatMessagesUseCase.getMessages()
            when {
                messages.isNotEmpty() -> _chatState.value = MessagesShown(messages)
                estimatedAwaitTime >= 0 -> _chatState.value = ChatState.QueueShown(estimatedAwaitTime)
                else -> _chatState.value = Error(resources.getString(R.string.error_message_general))
            }
        }
    }

    private suspend fun getMessages() {
        chatMessagesUseCase.execute(ChatRequestModel.GetMessages(sessionID, chatParamsRequestBody.value.metadata))
            .fold(
                onSuccess = { state ->
                    withContext(Dispatchers.Main) {
                        when (state) {
                            is TerminatedByUser -> stopSelf()
                            is Error -> doOnError(state.message)
                            is MessagesShown -> updateUnreadMessages(state.unread)
                            is ChatState.QueueShown -> estimatedAwaitTime = state.estimatedWaitTime
                            is TerminatedByAgent -> {
                                showTerminatedByAgentDialog()
                                stopSelf()
                            }
                        }
                        _chatState.value = state
                    }
                /*
                * As soon as we receive messages response from the server we do send another request again.
                * */
                    if (isRunning) {
                        runWithRetries { getMessages() }
                    }
                },
                onFailure = {
                    withContext(Dispatchers.Main) {
                        doOnError(it.localizedMessage)
                    }
                }
            )
    }

    private fun doOnError(message: String?) {
        logger.debug("Failed to retrieve messages", message)
        _chatState.value = Error(message)
        stopSelf()
    }

    private fun updateUnreadMessages(unread: Int) {
        chatFloatingButton.updateUnreadMessages(unread)
        isInQueue = false
        updateNotification()
    }

    private fun updateNotification() {
        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(
            CHAT_NOTIFICATION_ID, createChatNotification(unread = chatFloatingButton.unreadMessages)
        )
    }

    private suspend fun terminateChat() {
        chatMessagesUseCase.execute(ChatRequestModel.TerminateChat(sessionID, chatParamsRequestBody.value.metadata)).fold(
            onSuccess = {
                withContext(Dispatchers.Main) {
                    stopSelf()
                    _chatState.value = it
                }
            },
            onFailure = { logger.debug("Failed to terminate chat", it) }
        )
    }

    private suspend fun postMessage(message: String) {
        chatMessagesUseCase.execute(
            ChatRequestModel.PostMessage(
                sessionID,
                chatParamsRequestBody.value.metadata,
                message
            )
        ).fold(
            onSuccess = {
                withContext(Dispatchers.Main) {
                    _chatState.value = it
                }
            },
            onFailure = {
                logger.debug("Failed to post message", it)
                _chatState.value = ChatState.SendMessageError(chatMessagesUseCase.getMessages(), message)
            }
        )
    }

    private fun showTerminatedByAgentDialog() {
        dialogViewModel.postDialog(
            titleResId = R.string.chat_end_dialog_title,
            messageResId = R.string.chat_ended_by_agent_message
        )
    }

    private fun terminate(forced: Boolean) {
        if (forced || isRunning) {
            isRunning = false
            endChatReceiver.unsubscribe(this)
            removeFloatingButton()
            scope.launch { terminateChat() }
        }
    }

    companion object {

        var isRunning = false
            private set

        @MainThread
        fun stop(context: Context) {
            if (isRunning) {
                isRunning = false
                startServiceSafely(context, getIntent(context).apply { action = ACTION_STOP_MESSAGES_SERVICE })
            }
        }

        @MainThread
        fun startAndBound(context: Context, intent: Intent, serviceConnection: ServiceConnection) {
            startServiceSafely(context, intent)
            context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        }

        internal fun getIntent(context: Context) = Intent(context, ChatMessagesService::class.java)

        private fun startServiceSafely(context: Context, intent: Intent) {
            ContextCompat.startForegroundService(context, intent)
        }
    }

    private fun getChatNotificationMessage(unread: Int) = if (isInQueue) {
        getString(R.string.chat_is_in_queue)
    } else {
        resources.getQuantityString(R.plurals.chat_notification_session_progress_text, unread, unread)
    }

    inner class MessagesBinder : Binder() {
        val service: ChatMessagesService
            get() = this@ChatMessagesService
    }
}
