package com.etrade.mobilepro.trade.presentation.form.delegate.shared

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.map
import com.etrade.mobilepro.dynamic.form.api.InputHolder
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.SwitchInput
import com.etrade.mobilepro.dynamic.form.api.TextInput
import com.etrade.mobilepro.dynamic.form.api.inputParameterValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.livedata.skip
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.toSecurityType
import com.etrade.mobilepro.trade.presentation.TradeConditionalOrderDisclosureViewModel
import com.etrade.mobilepro.trade.presentation.form.delegate.TradeFormFactoryDelegate
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume

internal fun RuleListBuilder.liveForm(
    inputId: TradeFormInputId,
    currentSecurityType: SecurityType,
    listener: TradeFormFactoryDelegate.OnFormChangeListener,
    disclosureViewModel: TradeConditionalOrderDisclosureViewModel,
    accountDelegate: SelectedAccountDelegate
) {
    rule {
        requireInput<SelectInput>(inputId.accountId).value.skip(1).observeForever { option ->
            option?.id?.let { accountId ->
                accountDelegate.setUserSelectedAccountId(
                    currentSecurityType,
                    accountId
                )
            }
        }
        var previousSecurityType: SecurityType = currentSecurityType
        securityType(inputId).observeForever { securityType ->
            if (securityType != null && securityType != previousSecurityType) {
                if (securityType == SecurityType.CONDITIONAL) {
                    disclosureViewModel.onConditionalOrderSelected(previousSecurityType)
                }
                previousSecurityType = securityType
                listener.onSecurityType(securityType)
            }
        }
    }
}

internal fun RuleListBuilder.resetPricesOnVisibilityChange(inputId: WithPriceTradeFormInputId, includeLimitPrice: Boolean = true) {
    inputId.getPriceTypeList(includeLimitPrice).forEach {
        rule {
            requireInput<TextInput>(it).let { priceTextInput ->
                priceTextInput.isVisible.observeForever {
                    priceTextInput.setParameterValue(null)
                }
            }
        }
    }
}

internal fun RuleListBuilder.resetPricesOnSymbolChange(lookup: String, inputId: WithPriceTradeFormInputId, includeLimitPrice: Boolean = true) {
    rule {
        var prevSymbol: String? = null
        requireAnyInput(lookup).parameterValue.observeForever { newSymbol ->

            // Retain prices on same Symbol selection.
            if (prevSymbol != newSymbol.toString()) {
                prevSymbol = newSymbol.toString()
                inputId.getPriceTypeList(includeLimitPrice).forEach { priceInput ->
                    requireInput<TextInput>(priceInput).resetValue()
                }
            }
        }
    }
}

private fun WithPriceTradeFormInputId.getPriceTypeList(
    includeLimitPrice: Boolean
) = listOfNotNull(
    if (includeLimitPrice) {
        limitPrice
    } else {
        null
    },
    limitPriceForStopLimit,
    stopPrice,
    stopPriceForStopLimit
)

internal fun RuleListBuilder.resetTermAndAllOrNoneOnPriceTypeChange(inputId: WithPriceTradeFormInputId) {
    rule {
        requireInput<SelectInput>(inputId.priceType).value.observeForever {
            if (it?.id == inputId.priceTypeValueId.market) {
                requireAnyInput(inputId.term).setParameterValue(inputId.termValueId.goodForDay)
            } else {
                requireAnyInput(inputId.term).resetValue()
            }
            requireInput<SwitchInput>(inputId.allOrNone).setValue(false)
        }
    }
}

internal fun RuleListBuilder.resetPriceTypeIfDisclosureRejected(
    inputId: WithPriceTradeFormInputId,
    disclosureViewModel: TradeConditionalOrderDisclosureViewModel
) {
    rule {
        var previousPriceType: String? = null
        requireInput<SelectInput>(inputId.priceType).value.observeForever {
            if (it?.id == inputId.priceTypeValueId.trailingStopDollar ||
                it?.id == inputId.priceTypeValueId.trailingStopPercent
            ) {
                disclosureViewModel.resetPriceType.observeForever(object : Observer<ConsumableLiveEvent<String>> {
                    override fun onChanged(event: ConsumableLiveEvent<String>?) {
                        event?.consume { priceTypeToSet ->
                            disclosureViewModel.resetPriceType.removeObserver(this)
                            requireInput<SelectInput>(inputId.priceType).setParameterValue(
                                priceTypeToSet
                            )
                        }
                    }
                })
                previousPriceType?.let { type ->
                    disclosureViewModel.onTrailingPriceTypeSelected(type)
                }
            }
            previousPriceType = it?.id
        }
    }
}

private fun InputHolder.securityType(inputId: TradeFormInputId): LiveData<SecurityType?> {
    return inputParameterValue(inputId.securityType).map { parameterValue ->
        inputId.toSecurityType(parameterValue)
    }
}
