package com.etrade.mobilepro.bank;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u00dc\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002:\u0001eB[\b\u0007\u0012\b\b\u0001\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0017J\b\u0010E\u001a\u00020\u001bH\u0002J \u0010F\u001a\u00020G2\u0006\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\u00042\b\b\u0002\u0010H\u001a\u00020IJ\u0018\u0010J\u001a\u00020G2\u0006\u0010#\u001a\u00020\u00042\u0006\u0010\"\u001a\u00020\u0004H\u0002J\u001e\u0010K\u001a\u00020G2\u0006\u0010\"\u001a\u00020L2\u0006\u0010M\u001a\u00020\u001d2\u0006\u0010N\u001a\u00020\u001dJ*\u0010O\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a2\f\u0010P\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a2\f\u0010Q\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001aH\u0002J\u001c\u0010R\u001a\u00020G2\u0006\u0010S\u001a\u00020\u00042\n\b\u0002\u0010T\u001a\u0004\u0018\u00010UH\u0002J\u0010\u0010V\u001a\u00020G2\u0006\u0010W\u001a\u00020XH\u0002J\"\u0010Y\u001a\u00020Z2\u0018\u0010[\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001b0\u001a0]0\\H\u0002J\u0010\u0010^\u001a\u00020_2\u0006\u0010`\u001a\u00020aH\u0002J\u000e\u0010b\u001a\u00020G2\u0006\u0010c\u001a\u00020\u001dJ\u000e\u0010d\u001a\u00020G2\u0006\u0010c\u001a\u00020\u001dR\u001a\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001b0\u001a0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001d0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001d0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001b0\u001a0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010 \u001a\b\u0012\u0004\u0012\u00020!0\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\"\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010#\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010$\u001a\u00020%8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b(\u0010)\u001a\u0004\b&\u0010\'R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R-\u0010,\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001d0/0.j\u0002`00-\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u00102R\u001b\u00103\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b6\u0010)\u001a\u0004\b4\u00105R\u001b\u00107\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b9\u0010)\u001a\u0004\b8\u00105R\u0014\u0010:\u001a\b\u0012\u0004\u0012\u00020;0-X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010<\u001a\n >*\u0004\u0018\u00010=0=X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010?\u001a\u00020\u0004X\u0096D\u00a2\u0006\b\n\u0000\u001a\u0004\b@\u00105R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010A\u001a\b\u0012\u0004\u0012\u00020!0-8F\u00a2\u0006\u0006\u001a\u0004\bB\u00102R\u001d\u0010C\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001b0\u001a0-\u00a2\u0006\b\n\u0000\u001a\u0004\bD\u00102\u00a8\u0006f"}, d2 = {"Lcom/etrade/mobilepro/bank/BankHomeViewModel;", "Landroidx/lifecycle/ViewModel;", "Lcom/etrade/mobilepro/tracking/ScreenViewModel;", "baseUrl", "", "cachedDynamicScreenRepo", "Lcom/etrade/mobilepro/dynamicui/api/DynamicScreenRepo;", "imageLoader", "Lcom/etrade/mobilepro/imageloader/ImageLoader;", "transactionsServiceProvider", "Lcom/etrade/mobilepro/transactions/TransactionsServiceProvider;", "resources", "Landroid/content/res/Resources;", "user", "Lcom/etrade/mobilepro/session/api/User;", "darkModeChecker", "Lcom/etrade/mobilepro/util/android/darkmode/DarkModeChecker;", "inboxMessagesRepository", "Lcom/etrade/mobilepro/inboxmessages/InboxMessagesRepository;", "dispatchers", "Lcom/etrade/mobilepro/util/android/coroutine/DispatcherProvider;", "context", "Landroid/content/Context;", "(Ljava/lang/String;Lcom/etrade/mobilepro/dynamicui/api/DynamicScreenRepo;Lcom/etrade/mobilepro/imageloader/ImageLoader;Lcom/etrade/mobilepro/transactions/TransactionsServiceProvider;Landroid/content/res/Resources;Lcom/etrade/mobilepro/session/api/User;Lcom/etrade/mobilepro/util/android/darkmode/DarkModeChecker;Lcom/etrade/mobilepro/inboxmessages/InboxMessagesRepository;Lcom/etrade/mobilepro/util/android/coroutine/DispatcherProvider;Landroid/content/Context;)V", "_dynamicWidgets", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/etrade/mobilepro/dynamicui/api/GenericLayout;", "_endDate", "Lorg/threeten/bp/LocalDate;", "_startDate", "_transactionsWidgets", "_viewStates", "Lcom/etrade/mobilepro/bank/BankHomeViewModel$ViewState;", "accountId", "accountUuId", "bankDateRangeSelector", "Lcom/etrade/mobilepro/bank/daterangeselector/BankDateRangeSelector;", "getBankDateRangeSelector", "()Lcom/etrade/mobilepro/bank/daterangeselector/BankDateRangeSelector;", "bankDateRangeSelector$delegate", "Lkotlin/Lazy;", "compositeDisposable", "Lio/reactivex/disposables/CompositeDisposable;", "dateRangeSelection", "Landroidx/lifecycle/LiveData;", "Lcom/etrade/mobilepro/util/android/ConsumableLiveEvent;", "Lkotlin/Pair;", "Lcom/etrade/mobilepro/bank/DateRange;", "getDateRangeSelection", "()Landroidx/lifecycle/LiveData;", "emptyDate", "getEmptyDate", "()Ljava/lang/String;", "emptyDate$delegate", "emptyValue", "getEmptyValue", "emptyValue$delegate", "inboxMessages", "Lcom/etrade/mobilepro/inboxmessages/InboxMessagesDynamicPayload;", "logger", "Lorg/slf4j/Logger;", "kotlin.jvm.PlatformType", "screenName", "getScreenName", "viewState", "getViewState", "widgets", "getWidgets", "createPlaceHolderBankCtaView", "fetchScreen", "", "forceRefresh", "", "getBankAccountDynamicScreen", "getTransactions", "", "startDate", "endDate", "getWidgetsWithDateRangeSelector", "dynamicScreens", "transactions", "onError", "errorMsg", "throwable", "", "onSuccess", "response", "Lcom/etrade/mobilepro/transactions/TransactionsResponseInfo;", "subscribeToScreenChanges", "Lio/reactivex/disposables/Disposable;", "screen", "Lio/reactivex/Observable;", "Lcom/etrade/mobilepro/util/Resource;", "toBankTransactionsView", "Lcom/etrade/mobilepro/bank/transactions/BankTransactionGenericLayout;", "it", "Lcom/etrade/mobilepro/transactions/TransactionDetailInfo;", "updateEndDate", "date", "updateStartDate", "ViewState", "bank-dynamic_debug"})
public final class BankHomeViewModel extends androidx.lifecycle.ViewModel implements com.etrade.mobilepro.tracking.ScreenViewModel {
    private final java.lang.String baseUrl = null;
    private final com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo cachedDynamicScreenRepo = null;
    private final com.etrade.mobilepro.transactions.TransactionsServiceProvider transactionsServiceProvider = null;
    private final android.content.res.Resources resources = null;
    private final com.etrade.mobilepro.session.api.User user = null;
    private final com.etrade.mobilepro.util.android.darkmode.DarkModeChecker darkModeChecker = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String screenName = "Overview";
    private final org.slf4j.Logger logger = null;
    private final kotlin.Lazy emptyValue$delegate = null;
    private final kotlin.Lazy emptyDate$delegate = null;
    private final androidx.lifecycle.MutableLiveData<com.etrade.mobilepro.bank.BankHomeViewModel.ViewState> _viewStates = null;
    private final io.reactivex.disposables.CompositeDisposable compositeDisposable = null;
    private java.lang.String accountUuId;
    private java.lang.String accountId;
    private final androidx.lifecycle.MutableLiveData<org.threeten.bp.LocalDate> _startDate = null;
    private final androidx.lifecycle.MutableLiveData<org.threeten.bp.LocalDate> _endDate = null;
    private final kotlin.Lazy bankDateRangeSelector$delegate = null;
    private final androidx.lifecycle.LiveData<com.etrade.mobilepro.inboxmessages.InboxMessagesDynamicPayload> inboxMessages = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.etrade.mobilepro.dynamicui.api.GenericLayout>> _dynamicWidgets = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.etrade.mobilepro.dynamicui.api.GenericLayout>> _transactionsWidgets = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<com.etrade.mobilepro.util.android.ConsumableLiveEvent<kotlin.Pair<org.threeten.bp.LocalDate, org.threeten.bp.LocalDate>>> dateRangeSelection = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.List<com.etrade.mobilepro.dynamicui.api.GenericLayout>> widgets = null;
    
    @javax.inject.Inject()
    public BankHomeViewModel(@org.jetbrains.annotations.NotNull()
    @com.etrade.mobilepro.common.di.Web()
    java.lang.String baseUrl, @org.jetbrains.annotations.NotNull()
    @com.etrade.mobilepro.common.di.RemoteDynamicScreen()
    com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo cachedDynamicScreenRepo, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.imageloader.ImageLoader imageLoader, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.transactions.TransactionsServiceProvider transactionsServiceProvider, @org.jetbrains.annotations.NotNull()
    android.content.res.Resources resources, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.session.api.User user, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.util.android.darkmode.DarkModeChecker darkModeChecker, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.inboxmessages.InboxMessagesRepository inboxMessagesRepository, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.util.android.coroutine.DispatcherProvider dispatchers, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getScreenName() {
        return null;
    }
    
    private final java.lang.String getEmptyValue() {
        return null;
    }
    
    private final java.lang.String getEmptyDate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.etrade.mobilepro.bank.BankHomeViewModel.ViewState> getViewState() {
        return null;
    }
    
    private final com.etrade.mobilepro.bank.daterangeselector.BankDateRangeSelector getBankDateRangeSelector() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.etrade.mobilepro.util.android.ConsumableLiveEvent<kotlin.Pair<org.threeten.bp.LocalDate, org.threeten.bp.LocalDate>>> getDateRangeSelection() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.etrade.mobilepro.dynamicui.api.GenericLayout>> getWidgets() {
        return null;
    }
    
    private final void getBankAccountDynamicScreen(java.lang.String accountUuId, java.lang.String accountId) {
    }
    
    private final io.reactivex.disposables.Disposable subscribeToScreenChanges(io.reactivex.Observable<com.etrade.mobilepro.util.Resource<java.util.List<com.etrade.mobilepro.dynamicui.api.GenericLayout>>> screen) {
        return null;
    }
    
    public final void fetchScreen(@org.jetbrains.annotations.NotNull()
    java.lang.String accountId, @org.jetbrains.annotations.NotNull()
    java.lang.String accountUuId, boolean forceRefresh) {
    }
    
    public final void updateStartDate(@org.jetbrains.annotations.NotNull()
    org.threeten.bp.LocalDate date) {
    }
    
    public final void updateEndDate(@org.jetbrains.annotations.NotNull()
    org.threeten.bp.LocalDate date) {
    }
    
    public final void getTransactions(long accountId, @org.jetbrains.annotations.NotNull()
    org.threeten.bp.LocalDate startDate, @org.jetbrains.annotations.NotNull()
    org.threeten.bp.LocalDate endDate) {
    }
    
    private final void onSuccess(com.etrade.mobilepro.transactions.TransactionsResponseInfo response) {
    }
    
    private final void onError(java.lang.String errorMsg, java.lang.Throwable throwable) {
    }
    
    private final com.etrade.mobilepro.bank.transactions.BankTransactionGenericLayout toBankTransactionsView(com.etrade.mobilepro.transactions.TransactionDetailInfo it) {
        return null;
    }
    
    private final java.util.List<com.etrade.mobilepro.dynamicui.api.GenericLayout> getWidgetsWithDateRangeSelector(java.util.List<? extends com.etrade.mobilepro.dynamicui.api.GenericLayout> dynamicScreens, java.util.List<? extends com.etrade.mobilepro.dynamicui.api.GenericLayout> transactions) {
        return null;
    }
    
    private final com.etrade.mobilepro.dynamicui.api.GenericLayout createPlaceHolderBankCtaView() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b\u00a8\u0006\t"}, d2 = {"Lcom/etrade/mobilepro/bank/BankHomeViewModel$ViewState;", "", "()V", "Error", "Loading", "Success", "Lcom/etrade/mobilepro/bank/BankHomeViewModel$ViewState$Loading;", "Lcom/etrade/mobilepro/bank/BankHomeViewModel$ViewState$Error;", "Lcom/etrade/mobilepro/bank/BankHomeViewModel$ViewState$Success;", "bank-dynamic_debug"})
    public static abstract class ViewState {
        
        private ViewState() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/etrade/mobilepro/bank/BankHomeViewModel$ViewState$Loading;", "Lcom/etrade/mobilepro/bank/BankHomeViewModel$ViewState;", "()V", "bank-dynamic_debug"})
        public static final class Loading extends com.etrade.mobilepro.bank.BankHomeViewModel.ViewState {
            @org.jetbrains.annotations.NotNull()
            public static final com.etrade.mobilepro.bank.BankHomeViewModel.ViewState.Loading INSTANCE = null;
            
            private Loading() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/etrade/mobilepro/bank/BankHomeViewModel$ViewState$Error;", "Lcom/etrade/mobilepro/bank/BankHomeViewModel$ViewState;", "()V", "bank-dynamic_debug"})
        public static final class Error extends com.etrade.mobilepro.bank.BankHomeViewModel.ViewState {
            @org.jetbrains.annotations.NotNull()
            public static final com.etrade.mobilepro.bank.BankHomeViewModel.ViewState.Error INSTANCE = null;
            
            private Error() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0010"}, d2 = {"Lcom/etrade/mobilepro/bank/BankHomeViewModel$ViewState$Success;", "Lcom/etrade/mobilepro/bank/BankHomeViewModel$ViewState;", "uiMode", "", "(I)V", "getUiMode", "()I", "component1", "copy", "equals", "", "other", "", "hashCode", "toString", "", "bank-dynamic_debug"})
        public static final class Success extends com.etrade.mobilepro.bank.BankHomeViewModel.ViewState {
            private final int uiMode = 0;
            
            @org.jetbrains.annotations.NotNull()
            public final com.etrade.mobilepro.bank.BankHomeViewModel.ViewState.Success copy(int uiMode) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Success(int uiMode) {
                super();
            }
            
            public final int component1() {
                return 0;
            }
            
            public final int getUiMode() {
                return 0;
            }
        }
    }
}