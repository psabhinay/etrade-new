package com.etrade.mobilepro.dummy

import com.etrade.eo.accountvalues.api.AccountValuesVO
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.accountvalues.api.AccountValuesProvider
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult
import com.etrade.mobilepro.common.di.CompleteView
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingText
import io.reactivex.Observable
import javax.inject.Inject

class CompleteViewStreamingTextManager @Inject constructor(
    @CompleteView accountValuesProvider: AccountValuesProvider,
    streamingStatusController: StreamingStatusController
) : BaseStreamingTextManager(accountValuesProvider, streamingStatusController) {

    override fun Observable<AccountValuesVO>.getNetAssetsAccountValues(): Observable<StreamingText> =
        map {
            val netAssets = (it as CalculationResult.CalculatedAccountValuesVO).netAssets
            val formattedValue = MarketDataFormatter.formatMoneyValueWithCurrency(netAssets)
            createStreamingText(netAssets, formattedValue)
        }
}
