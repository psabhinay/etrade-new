-if class com.etrade.mobilepro.bank.transactions.BankTransactionDataDto
-keepnames class com.etrade.mobilepro.bank.transactions.BankTransactionDataDto
-if class com.etrade.mobilepro.bank.transactions.BankTransactionDataDto
-keep class com.etrade.mobilepro.bank.transactions.BankTransactionDataDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
