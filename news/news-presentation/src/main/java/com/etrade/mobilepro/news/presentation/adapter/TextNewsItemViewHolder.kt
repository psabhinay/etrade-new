package com.etrade.mobilepro.news.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.presentation.databinding.ItemTextNewsBinding

private const val TITLE_MAX_LINES = 3

class TextNewsItemViewHolder(
    private val binding: ItemTextNewsBinding,
    var selectNewsItemListener: ((NewsItem.Text, Int) -> Unit)?,
    var symbolSelectListener: ((String) -> Unit)?
) : RecyclerView.ViewHolder(binding.root) {

    private var newsItem: NewsItem.Text? = null

    init {
        binding.textNewsItemRootLayout.setOnClickListener {
            newsItem?.let { item ->
                selectNewsItemListener?.invoke(item, bindingAdapterPosition)
            }
        }
    }

    fun bindTo(item: NewsItem?, wasRead: Boolean?, isSelected: Boolean) {
        this.newsItem = item as? NewsItem.Text
        with(binding) {
            textNewsCell.bindTo(
                item, wasRead,
                TITLE_MAX_LINES
            )
            textNewsCell.symbolSelectListener = symbolSelectListener
            textNewsItemRootLayout.isSelected = isSelected
        }
    }
}
