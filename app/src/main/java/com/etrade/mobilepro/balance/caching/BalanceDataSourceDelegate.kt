package com.etrade.mobilepro.balance.caching

import com.etrade.mobilepro.balance.data.repo.BalanceRequest
import com.etrade.mobilepro.balance.data.rest.BALANCE_PATH
import com.etrade.mobilepro.balance.data.rest.BalancesService
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.dynamicui.CachedResponseModel
import io.reactivex.Single
import io.realm.Realm
import retrofit2.Retrofit
import javax.inject.Inject

class BalanceDataSourceDelegate @Inject constructor(
    @MobileTrade retrofit: Retrofit
) : SimpleCachedResource.DataSourceDelegate<CachedResponseModel, BalanceRequest> {
    private val apiClient = retrofit.create(BalancesService::class.java)

    override fun fetchFromNetwork(id: BalanceRequest): Single<CachedResponseModel> =
        apiClient
            .getBalances(id.userId, id.accountId)
            .map {
                val cachedModel = CachedResponseModel(
                    id = BALANCE_PATH + id.accountId,
                    retrievedTimestamp = System.currentTimeMillis(),
                    response = it
                )
                cachedModel
            }

    override fun putToCache(fetchedResult: CachedResponseModel): Single<CachedResponseModel> = Single.fromCallable {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { realmTxn ->
                realmTxn.insertOrUpdate(fetchedResult)
            }
        }
        fetchedResult
    }

    override fun fetchFromDb(id: BalanceRequest): Single<CachedResponseModel> = Single.fromCallable {
        Realm.getDefaultInstance().use { realm ->
            val cachedResponse = realm
                .where(CachedResponseModel::class.java)
                .equalTo("id", BALANCE_PATH + id.accountId)
                .findFirst() ?: throw NoSuchElementException()
            realm.copyFromRealm(cachedResponse)
        }
    }
}
