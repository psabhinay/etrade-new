package com.etrade.mobilepro.chart.performance.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B!\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0006R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\b\u00a8\u0006\n"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/model/PerformanceChartData;", "", "accounts", "", "Lcom/etrade/mobilepro/chart/api/ChartLine;", "indexes", "(Ljava/util/List;Ljava/util/List;)V", "getAccounts", "()Ljava/util/List;", "getIndexes", "performance_debug"})
public final class PerformanceChartData {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.etrade.mobilepro.chart.api.ChartLine> accounts = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.etrade.mobilepro.chart.api.ChartLine> indexes = null;
    
    public PerformanceChartData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.etrade.mobilepro.chart.api.ChartLine> accounts, @org.jetbrains.annotations.NotNull()
    java.util.List<com.etrade.mobilepro.chart.api.ChartLine> indexes) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.chart.api.ChartLine> getAccounts() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.chart.api.ChartLine> getIndexes() {
        return null;
    }
}