package com.etrade.mobilepro.quote.mapper

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.dto.MutualFundsTradeQuoteDetailsDto
import com.etrade.mobilepro.quote.dto.MutualFundsTradeQuoteDetailsResponseDto
import com.etrade.mobilepro.quoteapi.QuoteChangeWidgetItem
import com.etrade.mobilepro.quoteapi.SalesChargeType
import com.etrade.mobilepro.testutil.getObjectFromJson
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

private const val GREEN = 0xFF00FF00.toInt()
private const val RED = 0xFFFF0000.toInt()

internal class MutualFundsTradeQuoteMapperTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var sut: MutualFundsTradeQuoteMapper

    private val mapperResources = getQuoteResources()

    @Test
    fun mapMutualFundsTradeQuote() {
        sut = MutualFundsTradeQuoteMapper(mapperResources)
        val mutualFundsTradeQuoteDetailsResponseDto = MutualFundsTradeQuoteDetailsResponseDto(fundDetailsMutual = getMockQuotes()!!, selectedFundDetails = null)
        val result = sut.map(mutualFundsTradeQuoteDetailsResponseDto)
        assertNotNull(result)
        assertNotNull(result.headerItem)
        assertNotNull(result.summaryItem)
        assertEquals(result.instrumentType, InstrumentType.MF)

        with(result.headerItem) {
            assertEquals("PRXCX - T. ROWE PRICE CALIFORNIA TAX-FREE BOND", title)
            assertEquals(QuoteChangeWidgetItem("0.050000", "0.44"), change.value)
            assertEquals("NASDAQ as of 04/08/2020", asOfDateTime.value)
            assertEquals("11.360000", lastPrice.value?.price)
            assertEquals(GREEN, changeColor.value)
        }

        with(result.summaryItem) {
            assertEquals("None", salesCharge)
            assertEquals("90 days or less - \$49.99", etradeEarlyRedemptionFee)
            assertEquals("None", fundRedemptionFee)
            assertEquals("T. ROWE PRICE CALIFORNIA TAX-FREE BOND", fundName)
            assertEquals("\$2,500.00", initialInvestment)
            assertEquals("\$11.36", netAssetValue)
            assertEquals("04:00 PM ET", ordCutOffTime)
            assertEquals("0.00", sevenDayCurrentYield)
            assertEquals("\$100.00", subsequentInvestment)

            assertEquals("08:00 PM EDT", lastTradeTime)
            assertEquals("None", transactionFee)

            assertEquals(false, isFundOwned)
        }
    }

    @Test
    fun mapMutualFundsOwnedQuote() {
        sut = MutualFundsTradeQuoteMapper(mapperResources)
        val mutualFundsTradeQuoteDetailsResponseDto = getOwnedMockQuotes()!!
        val result = sut.map(mutualFundsTradeQuoteDetailsResponseDto)
        assertNotNull(result)
        assertNotNull(result.headerItem)
        assertNotNull(result.summaryItem)
        assertEquals(result.instrumentType, InstrumentType.MF)

        with(result.headerItem) {
            assertEquals("AAAAX - DWS RREEF REAL ASSETS A", title)
            assertEquals(QuoteChangeWidgetItem("0.070000", "0.77"), change.value)
            assertEquals("NASDAQ as of 05/05/2020", asOfDateTime.value)
            assertEquals("9.170000", lastPrice.value?.price)
            assertEquals(GREEN, changeColor.value)
        }

        with(result.summaryItem) {
            assertEquals("--", salesCharge)
            assertEquals("90 days or less - \$49.99", etradeEarlyRedemptionFee)
            assertEquals("None", fundRedemptionFee)
            assertEquals("DWS RREEF REAL ASSETS A", fundName)
            assertEquals("\$1,000.00", initialInvestment)
            assertEquals("\$9.17", netAssetValue)
            assertEquals("04:00 PM ET", ordCutOffTime)
            assertEquals("0.00", sevenDayCurrentYield)
            assertEquals("\$0.00", subsequentInvestment)

            assertEquals("08:05 AM EDT", lastTradeTime)
            assertEquals("None", transactionFee)
            assertEquals(100, positions.first().quantity.toInt())
            assertEquals("\$917.00", positions.first().marketValue)
            assertEquals(true, isFundOwned)
            assertEquals(SalesChargeType.FRONT_END_LOAD, salesChargeType)
        }
    }

    private fun getMockQuotes(): MutualFundsTradeQuoteDetailsDto? {
        return MutualFundsTradeQuoteMapper::class.getObjectFromJson<MutualFundsTradeQuoteDetailsResponseDto>("mutual_fund_trade_quotes.json").fundDetailsMutual
    }

    private fun getOwnedMockQuotes(): MutualFundsTradeQuoteDetailsResponseDto? {
        return MutualFundsTradeQuoteMapper::class.getObjectFromJson<MutualFundsTradeQuoteDetailsResponseDto>("mutual_fund_owned_quote.json")
    }
}
