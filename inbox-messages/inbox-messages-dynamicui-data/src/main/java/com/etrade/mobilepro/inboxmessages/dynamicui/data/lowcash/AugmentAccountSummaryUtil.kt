package com.etrade.mobilepro.inboxmessages.dynamicui.data.lowcash

import com.etrade.completeview.dto.AccountSummaryCardViewDto
import com.etrade.completeview.dto.AccountSummaryDto
import com.etrade.completeview.dto.AccountSummaryViewDto
import com.etrade.mobilepro.inboxmessages.dynamicui.api.lowcash.LowCashPrompt

internal fun AccountSummaryCardViewDto.augmentWithLowCashPrompt(prompt: LowCashPrompt): AccountSummaryCardViewDto =
    this.copy(data = this.data.augmentWithLowCashPrompt(prompt))

internal fun AccountSummaryViewDto.augmentWithLowCashPrompt(prompt: LowCashPrompt): AccountSummaryViewDto =
    this.copy(data = this.data.augmentWithLowCashPrompt(prompt))

private fun AccountSummaryDto.augmentWithLowCashPrompt(prompt: LowCashPrompt): AccountSummaryDto =
    this.copy(extraAugmentationInfo = this.extraAugmentationInfo + prompt)
