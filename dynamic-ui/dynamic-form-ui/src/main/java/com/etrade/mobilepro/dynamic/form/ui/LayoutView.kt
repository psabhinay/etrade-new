package com.etrade.mobilepro.dynamic.form.ui

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.dynamic.form.api.Layout

internal class LayoutView : FrameLayout, ComponentView<Layout> {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val componentViewDelegate: ComponentViewDelegate = ComponentViewDelegate(this)

    private var layout: Layout? = null

    override fun render(component: Layout, owner: LifecycleOwner) {
        componentViewDelegate.render(component, owner)

        removeAllViews()

        DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(context), component.layoutResId, this, true).apply {
            lifecycleOwner = owner
            component.bindings.forEach {
                setVariable(it.variableId, it.variable)
            }
            executePendingBindings()
        }

        layout = component
    }
}
