package com.etrade.mobilepro.quote.search.presentation

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.search.presentation.databinding.QuoteItemSearchResultSymbolBinding
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.etrade.mobilepro.util.android.textutil.setSymbolSpan
import com.etrade.mobilepro.util.toSentence
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class SymbolItemAdapterDelegate(internal var listener: OnSearchResultItemClickListener?) :
    AbsListItemAdapterDelegate<SearchResultItem.Symbol, SearchResultItem, SymbolItemAdapterDelegate.SymbolItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): SymbolItemViewHolder {
        return SymbolItemViewHolder(parent.viewBinding(QuoteItemSearchResultSymbolBinding::inflate))
    }

    override fun isForViewType(item: SearchResultItem, items: MutableList<SearchResultItem>, position: Int): Boolean {
        return item is SearchResultItem.Symbol
    }

    override fun onBindViewHolder(item: SearchResultItem.Symbol, holder: SymbolItemViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }

    inner class SymbolItemViewHolder(private val binding: QuoteItemSearchResultSymbolBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private var item: SearchResultItem.Symbol? = null

        init {
            binding.symbolRootLayout.setOnClickListener {
                item?.let {
                    listener?.onSearchResultItemClicked(it)
                }
            }
        }

        fun bind(item: SearchResultItem.Symbol) {
            this.item = item

            binding.symbolTitle.setSymbolSpan(item.title, item.type, item.displaySymbol)
            binding.symbolDescription.text = item.description.toSentence()
        }
    }
}
