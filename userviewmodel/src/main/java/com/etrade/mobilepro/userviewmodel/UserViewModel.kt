package com.etrade.mobilepro.userviewmodel

import android.content.res.Resources
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobile.accounts.dto.InstitutionType
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.accounts.model.toAccountDropDownModel
import com.etrade.mobilepro.accounts.util.android.view.AccountViewType
import com.etrade.mobilepro.accounts.util.android.view.AccountsSpinnerModel
import com.etrade.mobilepro.accounts.util.android.view.getAccountViewType
import com.etrade.mobilepro.common.RestorableViewModelDelegate
import com.etrade.mobilepro.common.navigation.NavRouterSignal
import com.etrade.mobilepro.common.navigation.NavRouterSignalViewModel
import com.etrade.mobilepro.common.navigation.navigate
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.timeout.state.TimeoutState
import com.etrade.mobilepro.usersession.api.LoginSignalHandler
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

/**
 * Holds the Account list, CurrentSelected account view type and selected list item index.
 */

class UserViewModel @Inject constructor(
    private val accountListRepo: AccountListRepo,
    private val resources: Resources,
    private val user: User,
    private val userViewModelRouter: UserViewModelRouter,
    private val timeoutState: TimeoutState,
    private val restorableViewModelDelegate: RestorableViewModelDelegate
) : NavRouterSignalViewModel {

    val currentSelectedAccountType: String
        get() = currentSelectedAccount.value?.account?.accountType?.name ?: AccountType.Unknown.name

    override val navRouterSignal: LiveData<ConsumableLiveEvent<NavRouterSignal>>
        get() = _navRouterSignal
    private val _navRouterSignal: MutableLiveData<ConsumableLiveEvent<NavRouterSignal>> = MutableLiveData()

    var previousSessionState: SessionStateChange? = SessionStateChange.UNAUTHENTICATED
        private set

    /**
     * Peeks at content of [userSessionState].
     */
    val peekUserSessionState: SessionStateChange?
        get() = userSessionState.value?.peekContent()

    /**
     * Checks if user is authenticated or not.
     */
    val isAuthenticated: Boolean
        get() = peekUserSessionState is SessionStateChange.AUTHENTICATED

    /**
     * Checks if user has new or unread personal notifications.
     */
    var hasNewOrUnreadPersonalNotifications: Boolean = false
    var userSeenNotificationsInCurrentSession: Boolean = false

    /**
     * Checks if user have more than one account
     */
    val hasMoreThanOneAccount: Boolean
        get() = accountList.size > 1

    val defaultAccountId: String?
        get() = defaultAccount?.account?.accountId ?: user.defaultAccountId

    val defaultAccountUuid: String?
        get() = defaultAccount?.account?.accountUuid ?: tradeEligibleAccounts.find { it.account?.accountId == user.defaultAccountId }?.account?.accountUuid

    var lastSelectedAccountViewType = AccountViewType.UNAUTHENTICATED_VIEW

    @Volatile
    private lateinit var _accountList: MutableList<AccountsSpinnerModel>

    private var _currentSelectedAccount: MutableLiveData<AccountsSpinnerModel?> = MutableLiveData()
    val currentSelectedAccount: LiveData<AccountsSpinnerModel?>
        get() = _currentSelectedAccount

    private val _accountListWithoutView = mutableListOf<AccountsSpinnerModel>()

    var defaultAccount: AccountsSpinnerModel? = null

    val accountList: List<AccountsSpinnerModel>
        get() = if (isAuthenticated) {
            _accountList
        } else {
            emptyList()
        }

    val loginAbortedSignal: LiveData<ConsumableLiveEvent<Unit>> get() = _loginAbortedSignal
    private val _loginAbortedSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val onLoginAbortedListenerSet = mutableSetOf<(() -> Boolean)>()

    // Account options that are displayed for Trade. Includes Brokerage and Managed account types.
    val tradeEligibleAccounts: List<AccountsSpinnerModel>
        get() = accountListRepo.getAccountList().filter { it.institutionType == InstitutionType.ADP }.toAccountDropDownModel()

    // Account options that are displayed for portfolio widgets. Includes only brokerage account types.
    val brokerageAccounts: List<AccountsSpinnerModel>
        get() = accountListRepo.getAccountList().filter { it.isBrokerageAccount() }.toAccountDropDownModel()

    val hasMoreThanOneBrokerageAccount: Boolean
        get() = brokerageAccounts.size > 1

    fun onLoginAborted() {
        notifyOnLoginAbortedListeners()
        _loginAbortedSignal.value = ConsumableLiveEvent(Unit)
    }

    fun selectMultipleAccountView(accountList: List<AccountsSpinnerModel>? = this.accountList) {
        accountList?.firstOrNull()?.let {
            if (it.accountViewType == AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW) {
                updateSelectedAccount(it)
            }
        }
    }

    fun selectAllBrokerageAccountView() {
        accountList.firstOrNull { it.accountViewType == AccountViewType.ALL_BROKERAGE_ACCOUNT_VIEW }?.let {
            updateSelectedAccount(it)
        }
    }

    fun updateSelectedAccount(accountUuid: String) {
        accountList.forEach { spinnerItem ->
            if (spinnerItem.account?.accountUuid == accountUuid) {
                updateSelectedAccount(spinnerItem)
            }
        }
    }

    fun updateSelectedAccountByAccountId(accountId: String) {
        accountList.forEach { spinnerItem ->
            if (spinnerItem.account?.accountId == accountId) {
                updateSelectedAccount(spinnerItem)
            }
        }
    }

    var isNavigationToAccount = false
    fun updateSelectedAccount(account: AccountsSpinnerModel) {
        isNavigationToAccount = account.accountViewType != AccountViewType.UNAUTHENTICATED_VIEW
        lastSelectedAccountViewType = account.accountViewType
        _currentSelectedAccount.value = account
    }

    sealed class ViewState {
        object Loading : ViewState()
        object Success : ViewState()
        data class Error(val message: ConsumableLiveEvent<String?> = ConsumableLiveEvent("Something went wrong!")) : ViewState()
    }

    private var _userSessionState: MutableLiveData<ConsumableLiveEvent<SessionStateChange>> = MutableLiveData()
    val userSessionState: LiveData<ConsumableLiveEvent<SessionStateChange>>
        get() = _userSessionState

    private val _streamingToggleState: MutableLiveData<ConsumableLiveEvent<Boolean>> = MutableLiveData()
    val streamingToggleState: LiveData<ConsumableLiveEvent<Boolean>>
        get() = _streamingToggleState

    fun streamingToggleStateChange(event: Boolean) {
        _streamingToggleState.value = ConsumableLiveEvent(event)
    }

    /**
     * method to notify the App about change in authentication state
     *
     * @param userStateChangePreparation Function to execute before notifying observers about a
     * user state change.
     */
    fun userSessionStateChange(
        event: SessionStateChange,
        shouldShowSarLogoutDialog: Boolean = false,
        userStateChangePreparation: (() -> Unit)? = null
    ) {
        if (event == _userSessionState.value?.peekContent()) return

        when (event) {
            is SessionStateChange.UNAUTHENTICATED -> {
                cleanupOnUnAuthenticateState(shouldShowSarLogoutDialog)
            }
            is SessionStateChange.AUTHENTICATED -> {
                timeoutState.shouldShowSarLogoutDialog = false

                // fetch realm cached accounts  and update the accounts dropdown model
                val accounts = accountListRepo.getAccountList()
                updateAccountsDropDownModel(accounts)
            }
        }
        // Give other components the opportunity to prepare other actions (e.x. pending landing
        // navigation) before UserViewModel notifies observers about a user state change.
        userStateChangePreparation?.invoke()
        previousSessionState = _userSessionState.value?.peekContent()
        _userSessionState.value = ConsumableLiveEvent(event)
    }

    private fun cleanupOnUnAuthenticateState(shouldShowSarLogoutDialog: Boolean) {
        _accountList.clear()
        _accountListWithoutView.clear()
        defaultAccount = null
        restorableViewModelDelegate.clearAll()
        _currentSelectedAccount.value = null
        lastSelectedAccountViewType = AccountViewType.UNAUTHENTICATED_VIEW
        timeoutState.shouldShowSarLogoutDialog = shouldShowSarLogoutDialog
        userSeenNotificationsInCurrentSession = false
        isNavigationToAccount = false
    }

    private val _userInternationalState = MutableLiveData<Boolean>().apply { value = false }
    val userInternationalState: LiveData<Boolean> = _userInternationalState

    fun showMarketDataAgreement(title: String, message: String) {
        _navRouterSignal.navigate(userViewModelRouter.provideMarketDataAgreementDirection(title, message))
    }

    fun navigateToSelectedAccountTabByTitle(
        accountViewType: AccountViewType,
        account: Account?
    ) {
        _navRouterSignal.navigate(
            userViewModelRouter.provideActionToSelectedAccountNavDirections(
                account,
                accountViewType,
                lastSelectedAccountViewType
            )
        )
    }

    fun navigateToPortfolio(accountId: String, accountUuid: String) {
        _navRouterSignal.navigate(userViewModelRouter.providePortfolioTabDirection(accountId, accountUuid))
    }

    fun updateInternationalState(newState: Boolean) {
        _userInternationalState.value = newState
    }

    fun getAccountType(accountUuid: String): AccountViewType? {
        return accountList.find { it.account?.accountUuid == accountUuid }?.accountViewType
    }

    fun observeLoginState(viewLifecycleOwner: LifecycleOwner, loginSignalHandler: LoginSignalHandler? = null, block: () -> Unit) {
        userSessionState.observe(
            viewLifecycleOwner,
            Observer {
                if (it.peekContent() == SessionStateChange.AUTHENTICATED) {
                    if (loginSignalHandler != null) {
                        if (loginSignalHandler.allowOnLoginCallback) {
                            loginSignalHandler.allowOnLoginCallback = false
                            block.invoke()
                        }
                    } else {
                        block.invoke()
                    }
                }
            }
        )
    }

    private fun updateAccountsDropDownModel(accounts: List<Account>) {
        _accountList = accounts.map { AccountsSpinnerModel(it.accountShortName, it.getAccountViewType(), it) }.toMutableList()

        _accountListWithoutView.clear()
        _accountListWithoutView.addAll(0, _accountList)

        when {
            _accountList.size > 1 -> {
                _accountList.add(0, AccountsSpinnerModel(resources.getString(R.string.title_complete_view), AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW))
                if (accounts.count { it.accountType == AccountType.Brokerage } > 1) {
                    _accountList.add(1, AccountsSpinnerModel(resources.getString(R.string.all_brokerage_title), AccountViewType.ALL_BROKERAGE_ACCOUNT_VIEW))
                }

                if (lastSelectedAccountViewType == AccountViewType.UNAUTHENTICATED_VIEW) {
                    selectMultipleAccountView(_accountList)
                }
            }
            _accountList.size == 1 -> {
                lastSelectedAccountViewType = _accountList[0].accountViewType
                _currentSelectedAccount.value = _accountList[0]
            }
            else -> lastSelectedAccountViewType = AccountViewType.UNAUTHENTICATED_VIEW
        }
    }

    init {
        _userSessionState.value = ConsumableLiveEvent(SessionStateChange.UNAUTHENTICATED)
    }

    private fun notifyOnLoginAbortedListeners() {
        val iterator = onLoginAbortedListenerSet.iterator()
        while (iterator.hasNext()) {
            if (iterator.next().invoke()) {
                iterator.remove()
            }
        }
    }
}
