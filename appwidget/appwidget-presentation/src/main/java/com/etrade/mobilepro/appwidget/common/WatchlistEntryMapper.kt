package com.etrade.mobilepro.appwidget.common

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.etrade.mobilepro.watchlistapi.WatchlistEntry

internal fun WatchlistEntry.copyWith(data: Level1Data): WatchlistEntry? {
    data.timestampSinceEpoch ?: return null
    val price = data.lastPrice?.safeParseBigDecimal()
    val change = if (data is StockData) {
        data.dayChange?.safeParseBigDecimal()
    } else {
        data.change?.safeParseBigDecimal()
    }
    val changePercent = if (data is StockData) {
        data.dayChangePercent
    } else { data.changePercent }
        ?.dropLast(1)?.safeParseBigDecimal()
        ?.let { MarketDataFormatter.formatMarketPercData(it) }
        ?.safeParseBigDecimal()

    if (price == null || change == null || changePercent == null) { return null }

    return this.copy(
        lastPrice = price,
        change = change,
        changePercent = changePercent
    )
}
