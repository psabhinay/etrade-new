package com.etrade.mobilepro.screener.presentation.etf

import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.screener.api.ScreenerDisclosure
import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseScreenersFragment
import com.etrade.mobilepro.tracking.params.EtfMarketsScreenerResultsAnalyticsParams
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class EtfScreenersFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter
) : BaseScreenersFragment(snackbarUtilFactory, screenersRouter) {

    override val viewModel by viewModels<EtfScreenersViewModel>({ requireParentFragment() }) { viewModelFactory }

    override val disclosureType: ScreenerDisclosure = ScreenerDisclosure.ETF_PRIMARY

    override val disclosuresText get() = getString(R.string.disclosure_etf_primary)

    override val resultNavListener: () -> Unit = {
        screenersRouter.actionToEtfResultsFragment(findNavController())
        viewModel.trackScreenerResultPage(EtfMarketsScreenerResultsAnalyticsParams())
    }

    override val screenerSetSelectedListener: (ScreenerItem.ScreenerSet) -> Unit = {
        when (it.type) {
            ScreenerType.Etf.FundProfile -> screenersRouter.actionScreenersContainerToEtfFundProfileFragment(findNavController())
            ScreenerType.Etf.Performance -> screenersRouter.actionScreenersContainerToEtfPerformanceFragment(findNavController())
            ScreenerType.Etf.Risk -> screenersRouter.actionScreenersContainerToEtfRiskFragment(findNavController())
            ScreenerType.Etf.Portfolio -> screenersRouter.actionScreenersContainerToEtfPortfolioFragment(findNavController())
            ScreenerType.Etf.Technicals -> screenersRouter.actionScreenersContainerToEtfTechnicalsFragment(findNavController())
            else -> throw IllegalArgumentException("Wrong tab for ${it.type}")
        }
    }
}
