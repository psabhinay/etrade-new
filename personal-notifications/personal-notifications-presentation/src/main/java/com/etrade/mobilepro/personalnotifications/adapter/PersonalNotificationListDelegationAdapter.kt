package com.etrade.mobilepro.personalnotifications.adapter

import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.personalnotifications.adapter.delegate.BannerNotificationViewDelegate
import com.etrade.mobilepro.personalnotifications.adapter.delegate.EbmNotificationViewDelegate
import com.etrade.mobilepro.personalnotifications.adapter.delegate.TextNotificationViewDelegate
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class PersonalNotificationListDelegationAdapter(
    imageLoader: ImageLoader,
    readStatusChecker: ((PersonalNotification) -> Boolean),
    clickListener: ((PersonalNotification) -> Unit),
    itemDismissListener: ((PersonalNotification.Localytics) -> Unit)
) : ListDelegationAdapter<MutableList<PersonalNotification>>() {

    init {
        delegatesManager.addDelegate(TextNotificationViewDelegate(readStatusChecker, clickListener))
        delegatesManager.addDelegate(BannerNotificationViewDelegate(imageLoader, clickListener))
        delegatesManager.addDelegate(EbmNotificationViewDelegate(clickListener, itemDismissListener))

        items = mutableListOf()
    }

    fun updateItems(items: List<PersonalNotification>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }
}
