package com.etrade.mobilepro.trade.form.api.value

interface ConditionalPriceTypeValueId : PriceTypeValueId {
    val hiddenStop: String
}
