package com.etrade.mobilepro.personalnotifications.viewmodel

import android.content.res.Resources
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.etrade.mobilepro.accountslist.api.AccountsListRepo
import com.etrade.mobilepro.common.di.PersonalNotifications
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.common.readstatus.ReadableType
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.inboxmessages.InboxMessagesPopupFragmentArgs
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.getDeepLinkCtaEvent
import com.etrade.mobilepro.personalnotifications.R
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification
import com.etrade.mobilepro.personalnotifications.fragment.PersonalNotificationsFragmentDirections
import com.etrade.mobilepro.personalnotifications.fragment.VERIFY_EXTERNAL_ACCOUNT_URL
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import kotlinx.coroutines.launch
import kotlinx.coroutines.rx2.awaitLast
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val PERSON_NOTIFICATION_ENTITY_EXPIRY = 30L

class PersonalNotificationsViewModel @Inject constructor(
    private val res: Resources,
    private val accountsListRepo: AccountsListRepo,
    @PersonalNotifications private val readStatusRepo: ReadStatusRepo,
    @Web private val baseUrl: String,
    private val inboxMessagesRepository: InboxMessagesRepository
) : ViewModel() {

    private val logger = LoggerFactory.getLogger(PersonalNotificationsViewModel::class.java)

    private val _viewState = MutableLiveData<ViewState>()
    val viewState
        get() = _viewState

    private val _clickEvents = MutableLiveData<ConsumableLiveEvent<CtaEvent>>()
    val clickEvents
        get() = _clickEvents

    init {
        getPersonalNotifications()
    }

    fun getPersonalNotifications() {
        viewModelScope.launch {
            accountsListRepo
                .getScreen(cacheExpiration = Long.MAX_VALUE)
                .runCatching { awaitLast() }
                .onSuccess {
                    when (it) {
                        is Resource.Success -> {
                            val data = it.data?.personalNotifications.orEmpty()
                            _viewState.value = ViewState.Success(data)
                        }
                        is Resource.Failed -> _viewState.value = ViewState.Failure
                        is Resource.Loading -> _viewState.value = ViewState.Loading
                    }
                }
                .onFailure {
                    logger.error("Failed to retrieve personal notifications", it)
                    _viewState.value = ViewState.Failure
                }
        }
    }

    fun wasRead(notification: PersonalNotification): Boolean {
        return readStatusRepo.wasRead(notification.identifier)
    }

    fun selectNotification(notification: PersonalNotification) {
        readStatusRepo.markAsRead(notification.identifier, ReadableType.PERSONAL_NOTIFICATION)
        when (notification) {
            is PersonalNotification.ExternalAccountVerification -> {
                _clickEvents.value = CtaEvent.NavDirectionsNavigation(
                    PersonalNotificationsFragmentDirections.openVerifyExternalAccountWebview(
                        res.getString(R.string.verify_external_account_web_view_title),
                        "$baseUrl$VERIFY_EXTERNAL_ACCOUNT_URL"
                    )
                ).consumable()
            }
            is PersonalNotification.Localytics -> {
                val inboxMessage = notification.message
                _clickEvents.value = if (!inboxMessage.deepLink.isNullOrEmpty()) {
                    inboxMessage.deepLink!!.getDeepLinkCtaEvent(baseUrl).consumable()
                } else if (inboxMessage.message.isNotEmpty()) {
                    CtaEvent.NavDirectionsNavigation(
                        object : NavDirections {
                            override fun getArguments(): Bundle =
                                InboxMessagesPopupFragmentArgs(notification.message.message).toBundle()

                            override fun getActionId(): Int = R.id.inbox_messages_nav_graph
                        }
                    ).consumable()
                } else {
                    null
                }
                viewModelScope.launch { inboxMessagesRepository.setMessageOpen(notification.message) }
            }
        }
    }

    fun markNotificationsAsRead(notifications: List<PersonalNotification>) =
        notifications.forEach { readStatusRepo.markAsRead(it.identifier, ReadableType.PERSONAL_NOTIFICATION) }

    fun tagLocalyticsMessage(notifications: List<PersonalNotification.Localytics>) {
        viewModelScope.launch {
            notifications.forEach {
                inboxMessagesRepository.setMessageSend(it.message)
                inboxMessagesRepository.setMessageViewed(it.message)
            }
        }
    }

    fun dismissInboxMessage(notification: PersonalNotification.Localytics) {
        viewModelScope.launch {
            inboxMessagesRepository.dismissMessage(notification.message)
        }
        val oldViewState = viewState.value as ViewState.Success
        _viewState.value = oldViewState.copy(
            notifications = oldViewState.notifications.toMutableList().let {
                it.remove(notification)
                it
            }
        )
    }

    override fun onCleared() {
        super.onCleared()
        readStatusRepo.clearExpiredReadableEntities(
            elapsedTimeInMillis = TimeUnit.DAYS.toMillis(PERSON_NOTIFICATION_ENTITY_EXPIRY),
            type = ReadableType.PERSONAL_NOTIFICATION
        )
    }

    sealed class ViewState {
        data class Success(val notifications: List<PersonalNotification>) : ViewState()
        object Failure : ViewState()
        object Loading : ViewState()
    }
}
