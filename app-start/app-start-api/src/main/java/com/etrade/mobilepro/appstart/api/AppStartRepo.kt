package com.etrade.mobilepro.appstart.api

import com.etrade.mobilepro.common.result.ETResult

interface AppStartRepo {
    suspend fun getPingSessionCallInterval(): ETResult<Long?>
    suspend fun getGDPRStatus(): ETResult<Boolean>

    /**
     * Default GDPR Status (location). True means the prospect is not in US.
     */
    companion object {
        const val IS_SUBJECT_TO_GDPR_DEFAULT: Boolean = true
    }
}
