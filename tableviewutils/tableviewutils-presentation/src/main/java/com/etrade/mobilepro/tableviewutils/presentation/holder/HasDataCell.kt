package com.etrade.mobilepro.tableviewutils.presentation.holder

import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell

interface HasDataCell {
    var cell: DataCell?
}
