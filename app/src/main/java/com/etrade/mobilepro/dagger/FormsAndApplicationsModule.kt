package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.formsandapplications.api.FormsAndApplicationsRepository
import com.etrade.mobilepro.formsandapplications.data.DefaultFormsAndApplicationsRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class FormsAndApplicationsModule {

    @Binds
    @Singleton
    internal abstract fun bindFormsAndApplicationsRepository(repository: DefaultFormsAndApplicationsRepository): FormsAndApplicationsRepository
}
