package com.etrade.mobilepro.msaccounts.presentation

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory

class MsAccountsErrorViewLayout : GenericLayout {
    override val viewModelFactory: ViewModelProvider.Factory
        get() = viewModelFactory {
            MsAccountsErrorViewModel()
        }
    override val viewModelClass: Class<out DynamicViewModel>
        get() = MsAccountsErrorViewModel::class.java
    override val uniqueIdentifier: String
        get() = javaClass.name
}
