package com.etrade.mobilepro.chartiq.data.dto

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = false)
internal enum class Toggle {
    ON,
    OFF
}
