package com.etrade.mobilepro.util

import android.content.res.Resources
import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.R

fun getTimestampString(resources: Resources): String {
    val timestamp = System.currentTimeMillis().let {
        "${DateFormattingUtils.formatTimeWithoutSecondsWithShowingTimezone(it)} ${DateFormattingUtils.formatToShortDate(it)}"
    }
    return resources.getString(R.string.as_of_time, timestamp)
}
