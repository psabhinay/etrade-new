package com.etrade.mobilepro.hidecontent

import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.etrade.mobilepro.util.android.extension.isApplicationDebuggable
import java.lang.ref.WeakReference
import javax.inject.Inject

class HideContentOverlayDelegateImpl @Inject constructor() : HideContentOverlayDelegate, LifecycleObserver {
    private lateinit var activityRef: WeakReference<AppCompatActivity>

    override fun init(activity: AppCompatActivity) {
        activityRef = WeakReference(activity)
        activity.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        if (activityRef.get()?.applicationContext?.isApplicationDebuggable() != true) {
            activityRef.get()?.window?.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        activityRef.get()?.window?.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
    }
}
