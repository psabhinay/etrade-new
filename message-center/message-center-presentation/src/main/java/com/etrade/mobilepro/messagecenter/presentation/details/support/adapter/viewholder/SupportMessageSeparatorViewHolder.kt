package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer

internal class SupportMessageSeparatorViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer
