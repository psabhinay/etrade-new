package com.etrade.mobilepro.trade.presentation.form.delegate.shared

import android.content.res.Resources
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.AppDialogBuilder
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.dynamic.form.api.ActionListBuilder
import com.etrade.mobilepro.dynamic.form.api.ActionType
import com.etrade.mobilepro.dynamic.form.api.FormBuilder
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter

internal fun FormBuilder.saveAndPreview(resources: Resources, parameter: CreateTradeFormParameter, requiredInputs: Collection<String>) {
    actions(parameter.onActionClickListener) {
        if (parameter.tradeOrderType != TradeOrderType.EDIT_OPEN) {
            saveOrder(resources, requiredInputs, parameter.disableIfNoChange, parameter.defaultValues)
        }
        previewOrder(resources, requiredInputs)
    }
}

internal fun ActionListBuilder.submitOrder(actionTitle: String, dependsOnInputIds: Collection<String>) {
    action(TradeFormAction.SubmitOrder) {
        actionType = ActionType.PRIMARY
        title = actionTitle
        dependsOnValuesOf(dependsOnInputIds, false, emptyMap())
    }
}

private fun ActionListBuilder.previewOrder(
    resources: Resources,
    dependsOnInputIds: Collection<String>
) {
    submitOrder(
        actionTitle = resources.getString(R.string.trade_action_preview),
        dependsOnInputIds = dependsOnInputIds
    )
}

private fun ActionListBuilder.saveOrder(
    resources: Resources,
    dependsOnInputIds: Collection<String>,
    disableIfNoChange: Boolean,
    defaultValues: InputValuesSnapshot
) {
    action(TradeFormAction.SaveOrder) {
        actionType = ActionType.SECONDARY
        title = resources.getString(R.string.trade_action_save_order)
        dependsOnValuesOf(dependsOnInputIds, disableIfNoChange, defaultValues)
    }
}

internal fun OnActionClickListener.showDialog(init: AppDialogBuilder.() -> Unit) {
    showDialog(
        appDialog {
            init()
            dismissAction = ::hideDialog
        }
    )
}

internal fun OnActionClickListener.showDialog(appDialog: AppDialog) = onActionClick(TradeFormAction.Dialog(appDialog))

internal fun OnActionClickListener.hideDialog() = onActionClick(TradeFormAction.Dialog())
