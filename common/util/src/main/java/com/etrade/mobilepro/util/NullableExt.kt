package com.etrade.mobilepro.util

inline infix fun <T> T?.guard(block: () -> Nothing): T {
    return this ?: block()
}
