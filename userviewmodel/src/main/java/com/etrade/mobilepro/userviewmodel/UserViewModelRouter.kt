package com.etrade.mobilepro.userviewmodel

import androidx.navigation.NavDirections
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.accounts.util.android.view.AccountViewType

interface UserViewModelRouter {
    fun provideMarketDataAgreementDirection(title: String, message: String): NavDirections
    fun providePortfolioTabDirection(accountId: String, accountUuid: String): NavDirections
    fun provideActionToSelectedAccountNavDirections(
        account: Account?,
        selectedAccountViewType: AccountViewType,
        currentAccountViewType: AccountViewType
    ): NavDirections
}
