package com.etrade.mobilepro.chat.api.setup.models

import com.etrade.mobilepro.chat.api.ChatMetadataModel

data class ChatActiveStatusModel(val isActive: Boolean?, val metadata: ChatMetadataModel)
