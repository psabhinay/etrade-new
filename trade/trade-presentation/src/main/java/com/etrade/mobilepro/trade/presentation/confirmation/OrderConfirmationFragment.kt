package com.etrade.mobilepro.trade.presentation.confirmation

import android.animation.Animator
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.activity.addCallback
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.order.details.router.OrderConfirmationAction
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.databinding.TradeFragmentOrderConfirmationBinding
import com.etrade.mobilepro.trade.presentation.util.SharedTradeViewModel
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import javax.inject.Inject

private const val KEY_ORDER_ID = "orderId"
const val KEY_CONFIRMATION_ACTION = "placeOrderConfirmationAction"

private const val ORDER_DETAILS_VIEW_ALPHA_NORMAL: Float = 1f

private const val TICK_VIEW_PROPORTION: Float = 3.5f
private const val TICK_VIEW_SCALE_INITIAL: Float = 3f
private const val TICK_VIEW_SCALE_NORMAL: Float = 1f

@RequireLogin
class OrderConfirmationFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    private val tradeRouter: TradeRouter
) : Fragment(R.layout.trade_fragment_order_confirmation) {

    private val binding by viewBinding(TradeFragmentOrderConfirmationBinding::bind)
    private val viewAdapter: OrderConfirmationListAdapter
        get() = binding.orderDetailsListView.adapter as OrderConfirmationListAdapter

    private val viewModel: OrderConfirmationViewModel by viewModels { viewModelFactory }
    private val sharedTradeViewModel: SharedTradeViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }

    private val confirmationAction: OrderConfirmationAction
        get() = (arguments?.get(KEY_CONFIRMATION_ACTION) as? OrderConfirmationAction) ?: OrderConfirmationAction.VIEW_PORTFOLIO

    private val orderId: String
        get() = requireNotNull(requireArguments().getString(KEY_ORDER_ID))

    private val hasContent: Boolean
        get() = viewModel.contentAvailableIndicator.value == true

    private val onDestinationChangedListener: NavController.OnDestinationChangedListener = NavController.OnDestinationChangedListener { _, newDestination, _ ->
        if (destination?.id != newDestination.id) {
            // It means that a user leaves the fragment and we can clear the order. We will not need it anymore.
            sharedTradeViewModel.clearOrder(orderId)
        }
    }

    private var navController: NavController? = null
    private var destination: NavDestination? = null

    private var isTickViewAnimating: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.fetch(orderId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            initViewState(tickView, confirmationView, orderDetailsListView, confirmationButton, savedInstanceState == null)
        }

        navController = findNavController().apply {
            destination = currentDestination
            addOnDestinationChangedListener(onDestinationChangedListener)
        }

        // We want to be sure that sharedTradeViewModel exists before we may leave the navigation graph associated with this nav controller. For example: when
        // a user opens `View Portfolio` the navigation graph may be destroyed before we are able to clear the order.
        touchSharedTradeViewModel()

        with(viewModel) {
            contentAvailableIndicator.observe(viewLifecycleOwner, { onLoadingIndicator(it) })
            openPortfolioSignal.observe(viewLifecycleOwner, { onOpenPortfolioSignal(it) })
            confirmationItems.observe(viewLifecycleOwner, { onSectionItems(it) })
            exitConfirmationSignal.observe(viewLifecycleOwner, { onExitConfirmationSignal(it) })
        }

        binding.orderDetailsListView.apply {
            setupRecyclerViewDivider(R.drawable.thin_divider)
            adapter = OrderConfirmationListAdapter()
        }

        setUpConfirmationButton()
    }

    private fun setUpConfirmationButton() {
        when (confirmationAction) {
            OrderConfirmationAction.VIEW_PORTFOLIO -> {
                setUpConfirmationButtonParams(R.string.view_portfolio) {
                    viewModel.openPortfolio()
                }
            }
            OrderConfirmationAction.CLOSE -> {
                setUpConfirmationButtonParams(R.string.close) {
                    viewModel.exitConfirmation()
                }
                activity?.onBackPressedDispatcher?.addCallback {
                    viewModel.exitConfirmation()
                }
            }
        }
    }

    private fun setUpConfirmationButtonParams(@StringRes textRes: Int, onClick: () -> Unit) {
        binding.apply {
            confirmationButton.setText(textRes)
            confirmationButton.setOnClickListener {
                onClick()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        navController?.removeOnDestinationChangedListener(onDestinationChangedListener)
        navController = null
    }

    /**
     * Ensures that sharedTradeViewModel property is initialized.
     */
    private fun touchSharedTradeViewModel() {
        sharedTradeViewModel
    }

    private fun onLoadingIndicator(contentAvailable: Boolean) {
        if (contentAvailable && !isTickViewAnimating) {
            binding.apply {
                revealContent(tickView, confirmationView, orderDetailsListView, confirmationButton)
            }
        }
    }

    private fun onOpenPortfolioSignal(event: ConsumableLiveEvent<String>) {
        event.consume { accountId ->
            activity?.let { tradeRouter.openPortfolio(it, accountId) }
        }
    }

    private fun onSectionItems(items: List<OrderConfirmationItem>) {
        viewAdapter.values = items
    }

    private fun onExitConfirmationSignal(event: ConsumableLiveEvent<Unit>) {
        event.consume {
            activity?.let { tradeRouter.exitConfirmation(it) }
        }
    }

    private fun initViewState(tickView: ImageView, confirmationView: View, orderDetailsView: View, openPortfolioView: View, isAnimationRequired: Boolean) {
        when {
            isAnimationRequired -> startAnimation(tickView, confirmationView, orderDetailsView, openPortfolioView)
            hasContent -> {
                tickView.setImageResource(R.drawable.ic_check)
                orderDetailsView.alpha = ORDER_DETAILS_VIEW_ALPHA_NORMAL
                openPortfolioView.alpha = ORDER_DETAILS_VIEW_ALPHA_NORMAL
            }
            else -> {
                tickView.setImageResource(R.drawable.ic_check)
                ensurePreAnimationState(tickView, confirmationView, orderDetailsView, openPortfolioView)
            }
        }
    }

    private fun startAnimation(tickView: ImageView, confirmationView: View, orderDetailsView: View, openPortfolioView: View) {
        ensurePreAnimationState(tickView, confirmationView, orderDetailsView, openPortfolioView)

        tickView.apply {
            context.createAnimatedVectorDrawable()
                .apply {
                    registerAnimationCallback(object : Animatable2Compat.AnimationCallback() {
                        override fun onAnimationEnd(drawable: Drawable) {
                            isTickViewAnimating = false
                            if (hasContent) {
                                revealContent(tickView, confirmationView, orderDetailsView, openPortfolioView)
                            }
                        }
                    })

                    isTickViewAnimating = true
                    start()
                }
                .also { setImageDrawable(it) }
        }
    }

    private fun ensurePreAnimationState(tickView: ImageView, confirmationView: View, orderDetailsView: View, openPortfolioView: View) {
        val tickSize = resources.getDimensionPixelSize(R.dimen.trade_confirmation_tick_size)
        val tickInitialSize = tickSize * TICK_VIEW_SCALE_INITIAL
        val screenHeight = resources.displayMetrics.heightPixels

        val tickerTranslation = (screenHeight - tickInitialSize - resources.getDimensionPixelSize(R.dimen.spacing_medium)) / TICK_VIEW_PROPORTION

        confirmationView.translationY = tickerTranslation + (tickInitialSize - tickSize) / 2

        tickView.apply {
            translationY = tickerTranslation
            scaleX = TICK_VIEW_SCALE_INITIAL
            scaleY = TICK_VIEW_SCALE_INITIAL
        }

        orderDetailsView.alpha = 0f
        openPortfolioView.alpha = 0f
    }

    private fun revealContent(tickView: View, confirmationView: View, orderDetailsView: View, openPortfolioView: View) {
        val tickViewAnimator = tickView
            .animate()
            .translationY(0f)
            .scaleX(TICK_VIEW_SCALE_NORMAL)
            .scaleY(TICK_VIEW_SCALE_NORMAL)

        val confirmationViewAnimator = confirmationView
            .animate()
            .translationY(0f)
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator) = Unit
                override fun onAnimationCancel(animation: Animator) = Unit
                override fun onAnimationStart(animation: Animator) = Unit
                override fun onAnimationEnd(animation: Animator) {
                    orderDetailsView.animate()
                        .alpha(ORDER_DETAILS_VIEW_ALPHA_NORMAL)
                        .start()
                    openPortfolioView.animate()
                        .alpha(ORDER_DETAILS_VIEW_ALPHA_NORMAL)
                        .start()
                }
            })

        tickViewAnimator.start()
        confirmationViewAnimator.start()
    }

    private fun Context.createAnimatedVectorDrawable(): AnimatedVectorDrawableCompat {
        return requireNotNull(AnimatedVectorDrawableCompat.create(this, R.drawable.ic_check_animated))
    }
}
