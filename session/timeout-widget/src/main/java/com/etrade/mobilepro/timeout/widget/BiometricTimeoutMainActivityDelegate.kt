package com.etrade.mobilepro.timeout.widget

import androidx.appcompat.app.AppCompatActivity

interface BiometricTimeoutMainActivityDelegate {
    fun init(activity: AppCompatActivity)
    fun showSessionTerminateDialog()
    fun authenticate()
}
