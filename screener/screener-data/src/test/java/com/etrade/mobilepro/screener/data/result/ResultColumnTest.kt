package com.etrade.mobilepro.screener.data.result

import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.QuotePrice
import com.etrade.mobilepro.screener.api.result.PrescreenedQuote
import com.etrade.mobilepro.util.color.DataColor
import com.etrade.mobilepro.util.color.DataColorResolver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.any
import org.mockito.Mockito.mock

class ResultColumnTest {
    private val resultColumn: ResultColumn = ResultColumn.ChangeDollar
    private val mockFundQuoteDetailItemsMapper = mock(FundQuoteDetailItemsMapper::class.java)
    private val mockDataColorResolver = mock(DataColorResolver::class.java).also {
        `when`(it.resolveDataColor(any())).thenReturn(DataColor.NEUTRAL)
    }

    @Test
    fun testChangeGreaterThanOne() {
        val result = resultColumn.createCell(getPrescreenedQuote("1.2355678"), mockFundQuoteDetailItemsMapper, mockDataColorResolver)
        assertEquals("1.24", result.displayText)
    }

    @Test
    fun testChangeBetweenZeroAndOne() {
        val result = resultColumn.createCell(getPrescreenedQuote("0.3747382992"), mockFundQuoteDetailItemsMapper, mockDataColorResolver)
        assertEquals("0.3747", result.displayText)
    }

    @Test
    fun testChangeBetweenMinusOneAndZero() {
        val result = resultColumn.createCell(getPrescreenedQuote("-0.39485739"), mockFundQuoteDetailItemsMapper, mockDataColorResolver)
        assertEquals("-0.3949", result.displayText)
    }

    @Test
    fun testChangeLessThanMinusOne() {
        val result = resultColumn.createCell(getPrescreenedQuote("-2.39574834"), mockFundQuoteDetailItemsMapper, mockDataColorResolver)
        assertEquals("-2.40", result.displayText)
    }

    private fun getPrescreenedQuote(price: String): PrescreenedQuote = PrescreenedQuote(
        quote = MobileQuote(quotePrice = QuotePrice(change = price)),
        prescreenData = null
    )
}
