package com.etrade.mobilepro.walkthrough.presentation.symbollookup

import com.etrade.mobilepro.statemachine.EtStateMachine
import com.etrade.mobilepro.walkthrough.api.SymbolLookupAction
import com.etrade.mobilepro.walkthrough.api.SymbolLookupState
import com.tinder.StateMachine
import javax.inject.Inject

class SymbolLookupStateMachine @Inject constructor() : EtStateMachine<SymbolLookupState, SymbolLookupAction> {

    private val stateMachine = StateMachine.create<SymbolLookupState, SymbolLookupAction, SymbolLookupState> {
        initialState(SymbolLookupState.Idle)
        state<SymbolLookupState.Idle> {
            on<SymbolLookupAction.StartWalkthrough> {
                transitionTo(SymbolLookupState.DisplayLookupScreen, SymbolLookupState.DisplayLookupScreen)
            }
            on<SymbolLookupAction.Canceled> {
                transitionTo(SymbolLookupState.Idle, SymbolLookupState.Idle)
            }
        }
        state<SymbolLookupState.DisplayLookupScreen> {
            on<SymbolLookupAction.SymbolSelected> {
                transitionTo(SymbolLookupState.DisplayDialogOverQuote, SymbolLookupState.DisplayDialogOverQuote)
            }
            on<SymbolLookupAction.Canceled> {
                transitionTo(SymbolLookupState.Idle, SymbolLookupState.Idle)
            }
        }
        state<SymbolLookupState.DisplayDialogOverQuote> {
            on<SymbolLookupAction.DismissQuoteDialog> {
                transitionTo(SymbolLookupState.DisplayQuoteOnly, SymbolLookupState.DisplayQuoteOnly)
            }
            on<SymbolLookupAction.Canceled> {
                transitionTo(SymbolLookupState.Idle, SymbolLookupState.Idle)
            }
        }
        state<SymbolLookupState.DisplayQuoteOnly> {
            on<SymbolLookupAction.QuoteClosed> {
                transitionTo(SymbolLookupState.DisplaySearchTooltip, SymbolLookupState.DisplaySearchTooltip)
            }
            on<SymbolLookupAction.Canceled> {
                transitionTo(SymbolLookupState.Idle, SymbolLookupState.Idle)
            }
        }
        state<SymbolLookupState.DisplaySearchTooltip> {
            on<SymbolLookupAction.LookupActionHighlighted> {
                transitionTo(SymbolLookupState.Idle, SymbolLookupState.Idle)
            }
            on<SymbolLookupAction.Canceled> {
                transitionTo(SymbolLookupState.Idle, SymbolLookupState.Idle)
            }
        }
        onTransition {
            val validTransition = it as? StateMachine.Transition.Valid ?: return@onTransition
            val newState = validTransition.sideEffect ?: return@onTransition
            onStateChange?.invoke(newState)
        }
    }

    override val currentState: SymbolLookupState
        get() = stateMachine.state

    override var onStateChange: ((SymbolLookupState) -> Unit)? = null

    override fun transition(action: SymbolLookupAction) {
        stateMachine.transition(action)
    }
}
