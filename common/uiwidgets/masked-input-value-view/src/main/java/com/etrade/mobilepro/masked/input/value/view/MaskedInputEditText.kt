package com.etrade.mobilepro.masked.input.value.view

import android.content.Context
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.AttributeSet
import androidx.core.content.withStyledAttributes
import com.etrade.mobilepro.uiwidgets.maskedinputvalueview.R
import com.google.android.material.textfield.TextInputEditText
import org.slf4j.Logger
import org.slf4j.LoggerFactory

private const val DEFAULT_MASK_PLACEHOLDER_CHAR = '*'

/**
 * [TextInputEditText] view that allows to get an input text as a value of specified type [T] and apply a mask to inputting text.
 *
 * To enable a mask you have to specify it in a layout file as `app:mask` property (e.g.app:mask="+1 (***) ***-**-**").
 * Default value of mask's placeholder is '*'
 * but it is possible to change it by `app:placeholderChar` property (e.g. app:placeholderChar="#" and app:mask="+1 (###) ###-##-##").
 *
 * This is an abstract class and to take advantage of it you must override [convertTextToValue] method
 * where the logic of converting input text into specified type [T] should be implemented.
 */
abstract class MaskedInputEditText<T> : TextInputEditText {

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    protected val logger: Logger by lazy { LoggerFactory.getLogger(this.javaClass) }

    private val rawValueBuilder = StringBuilder()
    private var mask: String? = null
    private var maskPlaceholderChar: Char = DEFAULT_MASK_PLACEHOLDER_CHAR

    private val textWatcher = object : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            // unused
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            mask?.let { mask ->
                if (count > 0) {
                    rawValueBuilder.handleOnTextAdded(s, mask)
                }

                if (before > 0) {
                    rawValueBuilder.handleOnTextRemoved(start, before, mask, maskPlaceholderChar)
                }
            }
        }

        override fun afterTextChanged(s: Editable) {
            mask?.let { mask ->
                var maskPosition = 0
                var rawValuePosition = 0
                var hasNext = true
                val resultToDisplay = StringBuilder()
                var hasMaskCharToBeAppended = mask.hasMaskCharToBeAppended(maskPosition)

                while (hasNext) {
                    if (hasMaskCharToBeAppended) {
                        resultToDisplay.append(mask[maskPosition])
                    } else {
                        resultToDisplay.append(rawValueBuilder[rawValuePosition])
                        rawValuePosition++
                    }

                    maskPosition++
                    hasMaskCharToBeAppended = mask.hasMaskCharToBeAppended(maskPosition)
                    hasNext = rawValuePosition < rawValueBuilder.length || hasMaskCharToBeAppended
                }

                removeTextChangedListener(this)
                s.clear()
                s.append(resultToDisplay.toString())
                addTextChangedListener(this)
            }
        }
    }

    /**
     * Provides pure input text as [CharSequence] without mask effect.
     * E.g. mask is "+1 (###) ###-##-##" and input text is "+1 (234) 5", so [rawText] should return pure value that is "2345".
     */
    var rawText: CharSequence
        get() = rawValueBuilder.toString()
        set(value) {
            rawValueBuilder.clear()
            setText(value)
        }

    private fun init(attributeSet: AttributeSet?) {
        context.withStyledAttributes(attributeSet, R.styleable.MaskedInputEditText) {
            mask = getString(R.styleable.MaskedInputEditText_mask)
            getString(R.styleable.MaskedInputEditText_placeholderChar)?.let {
                maskPlaceholderChar = it.first()
            }
        }

        mask?.let {
            filters = arrayOf(InputFilter.LengthFilter(it.length))
            addTextChangedListener(textWatcher)
            rawText = ""
        }
    }

    abstract fun convertTextToValue(text: CharSequence): T

    /**
     * Provides a value of [rawText] converted into specified type [T].
     */
    fun getValue(): T = convertTextToValue(rawText)

    private fun String.hasMaskCharToBeAppended(maskPosition: Int) = maskPosition < length &&
        this[maskPosition] != maskPlaceholderChar
}
