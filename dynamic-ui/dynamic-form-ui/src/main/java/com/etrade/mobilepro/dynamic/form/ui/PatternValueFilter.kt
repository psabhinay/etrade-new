package com.etrade.mobilepro.dynamic.form.ui

import android.text.InputFilter
import android.text.Spanned
import java.util.regex.Pattern

internal class PatternValueFilter constructor(private val pattern: Pattern) : InputFilter {

    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
        val replacement = source.subSequence(start, end)
        val candidate = dest.replaceRange(dstart, dend, replacement)
        return when {
            dest.isEmpty() && replacement == "." -> if (candidate.toString() == ".") { "0." } else { null }
            replacement.isEmpty() -> if (candidate.startsWith('.')) { "0" } else { null }
            pattern.matcher(candidate).matches() -> null
            else -> filter(source, start, middleValue(start, end), start, end, dest, dstart, dend)
        }
    }

    @Suppress("LongParameterList")
    private fun filter(source: CharSequence, start: Int, end: Int, minEnd: Int, maxEnd: Int, dest: CharSequence, dstart: Int, dend: Int): CharSequence {
        val candidate = source.subSequence(start, end)
        return if (pattern.matcher(dest.replaceRange(dstart, dend, candidate)).matches()) {
            val newEnd = middleValue(end, maxEnd)
            if (end == newEnd) {
                candidate
            } else {
                filter(source = source, start = start, end = newEnd, minEnd = end, maxEnd = maxEnd, dest = dest, dstart = dstart, dend = dend)
            }
        } else {
            filter(source = source, start = start, end = middleValue(minEnd, end), minEnd = minEnd, maxEnd = end, dest = dest, dstart = dstart, dend = dend)
        }
    }
}

internal fun middleValue(min: Int, max: Int): Int {
    require(min <= max) { "Invalid values: min=$min;max=$max" }
    return min + (max - min) / 2
}

internal fun createDecimalPattern(maxFractionDigits: Int): Pattern {
    return Pattern.compile("([0-9]*(\\.[0-9]{0,$maxFractionDigits})?)?")
}

internal fun createCurrencyDecimalPattern(maxFractionDigits: Int): Pattern {
    return Pattern.compile("\\$?(([0-9]+|[0-9]{1,3}(,[0-9]{3})*)(\\.[0-9]{0,$maxFractionDigits})?)?")
}
