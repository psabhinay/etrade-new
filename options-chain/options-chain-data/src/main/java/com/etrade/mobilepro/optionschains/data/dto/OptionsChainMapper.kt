package com.etrade.mobilepro.optionschains.data.dto

import com.etrade.mobilepro.instrument.ExpirationDate
import com.etrade.mobilepro.instrument.SettlementSessionCode
import com.etrade.mobilepro.optionschains.api.OptionsChain
import com.etrade.mobilepro.optionschains.api.OptionsPair

internal fun OptionChainsResponseDto.toOptionsChain(isAMOption: Boolean): OptionsChain {
    return OptionsChain(
        expirationDates = optionsMonthsList.expirationDateList.mapNotNull { it.toExpirationDate() },
        optionPairs = filterOptions(isAMOption)
    )
}

private fun OptionChainsResponseDto.filterOptions(isAMOption: Boolean): List<OptionsPair> {
    val groupList = optionsByMonth?.optionPairGroup?.entries?.toMutableList()
    return if (isAMOption) {
        groupList?.firstOrNull()
    } else {
        groupList?.lastOrNull()
    }?.value
        ?.mapNotNull { it.toOptionsPair(isAMOption) }
        .orEmpty()
}

private fun OptionPairCallsAndPutsDto.toOptionsPair(isAMOption: Boolean): OptionsPair? {
    return OptionsPair(
        call = call?.copy(isAMOption = isAMOption) ?: return null,
        put = put?.copy(isAMOption = isAMOption) ?: return null
    )
}

private fun ExpirationDateDto.toExpirationDate(): ExpirationDate? {
    return ExpirationDate(
        year = year ?: return null,
        month = month ?: return null,
        day = day ?: return null,
        settlementSessionCode = SettlementSessionCode.from(settlementSessionCode)
    )
}
