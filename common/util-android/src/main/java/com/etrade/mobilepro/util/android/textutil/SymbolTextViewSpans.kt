package com.etrade.mobilepro.util.android.textutil

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.RelativeSizeSpan
import android.widget.TextView
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.toShortDate

private const val OPTION_SYMBOL_MIN_LINE_LIMIT = 3
private const val OPTION_SYMBOL_MAX_LINE_LIMIT = 5
private const val DEFAULT_LINE_LIMIT = 1
private const val RELATIVE_SIZE = 0.6f

fun TextView.setSymbolSpan(ticker: String, instrumentType: InstrumentType, description: String) {
    val preparedText = prepareSymbolSpan(ticker, instrumentType, description)
    if (ticker == preparedText) {
        text = ticker
        contentDescription = ticker.characterByCharacter()
        maxLines = DEFAULT_LINE_LIMIT
        minLines = DEFAULT_LINE_LIMIT
    } else {
        text = preparedText
        maxLines = OPTION_SYMBOL_MAX_LINE_LIMIT
        minLines = OPTION_SYMBOL_MIN_LINE_LIMIT
        contentDescription = preparedText
    }
}

@Suppress("MagicNumber")
fun prepareSymbolSpan(ticker: String, instrumentType: InstrumentType, description: String): CharSequence {
    if (!instrumentType.isOption) return ticker

    // options description SPX Dec 17 '21 $4000 Put, we want to shrink everything after the underlyer ticker
    // watch list entries returns option descriptions with trailing characters w or q that we need to omit
    val trimmedDesc = description.trimEnd('w', 'q', ' ')
    val descriptions = trimmedDesc.split(" ")
    if (descriptions.isEmpty()) return ticker

    val spannableStringBuilder = SpannableStringBuilder()
    val indexToStart = descriptions[0].length
    for (index in descriptions.indices) {
        spannableStringBuilder.append(descriptions[index]).append(" ")
        if (index == 0 || index == 3) {
            // append newlines after underlyer ticker and after date
            spannableStringBuilder.append("\n")
        }
    }

    return spannableStringBuilder.apply {
        setSpan(RelativeSizeSpan(RELATIVE_SIZE), indexToStart, spannableStringBuilder.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
}

fun prepareWidgetTicker(
    ticker: String,
    instrumentType: InstrumentType?,
    description: String?
): Pair<String, String?> =
    description
        ?.takeIf { instrumentType?.isOption == true }
        // watch list entries returns option descriptions with trailing characters w or q that we need to omit
        ?.trimEnd('w', 'q', ' ')
        ?.replace(" Call", "C")
        ?.replace(" Put", "P")
        ?.split(" ".toRegex(), 2)
        ?.let { (symbol, optionData) ->
            val date = optionData.substringBeforeLast(' ').toShortDate()
            val price = optionData.substringAfterLast(' ')
            symbol to "$date $price"
        } ?: ticker to null

fun prepareOptionTicketDatePrice(
    ticker: String,
    instrumentType: InstrumentType?,
    description: String?
) = prepareWidgetTicker(ticker, instrumentType, description).second?.split(" ")?.let {
    Pair(it.firstOrNull(), it.lastOrNull())
} ?: Pair(null, null)

/**
 * Extracts underlying symbols from a description string that looks like `SPX Dec 17 '21 $4000 Put`
 * Expects InstrumentType to be an option
 */
// is based on prepareWidgetTicker function from above
fun extractUnderlyingSymbol(description: String?, instrumentType: InstrumentType?): String? =
    description
        ?.takeIf { instrumentType?.isOption == true }
        // watch list entries returns option descriptions with trailing characters w or q that we need to omit
        ?.trimEnd('w', 'q', ' ')
        ?.split(" ".toRegex(), 2)
        ?.let { (symbol, _) ->
            symbol
        }
