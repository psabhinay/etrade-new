package com.etrade.mobilepro.optionschains.data.repo

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class OptionsChainInputTest {

    private val map: Map<OptionsChainInput, String> = mapOf(
        OptionsChainInput("AAPL", 2) to "AAPL",
        OptionsChainInput("AAPL", 1, 2020, 12, 31) to "AAPL\$123120",
        OptionsChainInput("TSLA", 0, 1970, 1, 1) to "TSLA\$010170",
        OptionsChainInput("AAPL--191115C00260000", 0, 70, 1, 1) to "AAPL\$010170"
    )

    @Test
    fun `check path creation`() {
        map.forEach { (input, path) ->
            assertEquals(path, input.createPath())
        }
    }
}
