package com.etrade.mobilepro.documents

import org.threeten.bp.LocalDate

data class StatementDocumentsRequest(
    val accountUuid: String,
    val startDate: LocalDate,
    val endDate: LocalDate
)
