package com.etrade.mobilepro.markets.futures.widget

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.indices.Futures
import com.etrade.mobilepro.indices.IndexDataWithColor
import com.etrade.mobilepro.uiwidgets.widgetgridlayout.WidgetGridLayoutItem
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import javax.inject.Inject

class MarketsFuturesWidgetViewModel @Inject constructor(
    private val futureIndicesLoader: FutureIndicesLoader
) : DynamicViewModel(R.layout.widget_futures) {
    override val variableId: Int = com.etrade.mobilepro.BR.obj
    private val _futuresTemporalDescription = MutableLiveData<Futures>()
    private val _futuresData = MutableLiveData<List<IndexDataWithColor>>()
    private val compositeDisposable = CompositeDisposable()
    val contractDate: LiveData<String>
        get() = Transformations.map(_futuresTemporalDescription) { temporalDescription ->
            temporalDescription.futuresExpiry
        }
    val items: LiveData<List<WidgetGridLayoutItem>>
        get() = Transformations.map(_futuresData) { list ->
            list.map { indexDataWithColor ->
                val formattedPrice: String = MarketDataFormatter.formatMarketDataCustom(indexDataWithColor.last.toBigDecimalOrNull())
                WidgetGridLayoutItem(
                    ticker = indexDataWithColor.ticker,
                    displaySymbol = indexDataWithColor.displayName,
                    price = formattedPrice,
                    gain = indexDataWithColor.change,
                    gainPercentage = indexDataWithColor.changePercentage,
                    decimalPercentChange = null,
                    isPositiveGain = indexDataWithColor.isPositiveChange,
                    shouldDisplayOptionsData = false,
                )
            }
        }

    private val _viewState = MutableLiveData<DynamicUiViewState>()
    val viewState: LiveData<DynamicUiViewState>
        get() = _viewState

    fun getFutureIndices() {
        if (viewState.value != DynamicUiViewState.Success) {
            _viewState.value = DynamicUiViewState.Loading
        }
        viewModelScope.launch {
            futureIndicesLoader.fetchFutureIndices(
                futuresTemporalDescriptionConsumer = _futuresTemporalDescription,
                futuresDataConsumer = _futuresData,
                alsoDoOnSuccess = {
                    _viewState.value = DynamicUiViewState.Success
                },
                alsoDoOnError = { throwable ->
                    logger.error("Markets futures widget fetch failed", throwable)
                    _viewState.value = DynamicUiViewState.Error()
                }
            )
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}
