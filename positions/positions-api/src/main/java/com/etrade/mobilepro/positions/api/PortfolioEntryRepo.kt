package com.etrade.mobilepro.positions.api

import io.reactivex.Single

interface PortfolioEntryRepo {
    fun getPortfolioEntry(userId: String, accountId: String, extendedHourFlag: Boolean): Single<PortfolioEntry>
}
