package com.etrade.mobilepro.editlist.intent

import android.content.Intent

/**
 * Provides list action details.
 *
 * @param intent intent that is being prepared
 */
@ListEditDslMarker
class ListActionBuilder(private val intent: Intent) {

    /**
     * Text resource ID that is displayed on the action button.
     */
    var textResId: Int
        get() = intent.listActionTextResId
        set(value) {
            intent.listActionTextResId = value
        }

    /**
     * Set `true` if a list action should be displayed, `false` otherwise.
     *
     * It is gone by default, if not explicitly set to `true`.
     */
    var isVisible: Boolean
        get() = intent.listActionIsVisible
        set(value) {
            intent.listActionIsVisible = value
        }

    /**
     * Set `true` if a list action should be enabled, `false` otherwise.
     *
     * It is enabled by default, if not explicitly set to `false`.
     */
    var isEnabled: Boolean
        get() = intent.listActionIsEnabled
        set(value) {
            intent.listActionIsEnabled = value
        }

    /**
     * Adds a confirmation dialog to a list action.
     */
    fun confirmation(init: ConfirmationBuilder.() -> Unit) {
        ConfirmationBuilder(intent).apply(init).build()
    }

    /**
     * A list action may require a confirmation to be displayed before performing an actual action. This builder lets you define attributes of the
     * confirmation, such as title and message.
     *
     * @param intent intent that is being prepared
     */
    @ListEditDslMarker
    class ConfirmationBuilder(private val intent: Intent) {

        /**
         * Set `true` if an action confirmation is required. Default is `false`.
         */
        var isRequired: Boolean = false

        /**
         * Title of a confirmation dialog.
         */
        var titleResId: Int = 0

        /**
         * Message of a confirmation dialog.
         */
        var messageResId: Int = 0

        internal fun build() {
            if (isRequired && (titleResId == 0 || messageResId == 0)) {
                throw IllegalArgumentException("No title or message provided for a confirmation dialog")
            }
            intent.apply {
                listActionConfirmationIsRequired = isRequired
                listActionConfirmationTitleResId = titleResId
                listActionConfirmationMessageResId = messageResId
            }
        }
    }
}
