package com.etrade.mobilepro.caching.coroutine

import com.etrade.eo.core.coroutines.safeRetry
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.recover

/**
 * Represents a primary data source, a single source of truth that can be requested to get fresh data.
 */
interface DataSource<Resource, Id> {
    suspend fun fetch(id: Id): ETResult<Resource>
}

/**
 * Represents a secondary data source, it may or not contain the data that a consumer is looking for.
 */
interface Cache<Resource, Id> {
    suspend fun get(id: Id): ETResult<Resource>
    suspend fun isCached(id: Id): Boolean
    suspend fun put(resource: Resource, id: Id): ETResult<Unit>
    suspend fun remove(id: Id): ETResult<Unit>
}

/**
 * A data controller which coordinates when to the data sources, it will always try to fetch data from a cache source, but if it is not available
 * it will use use the primary source as fallback.
 *
 * Note: Parallel fetches that race each other are possible in this current implementation.
 *
 * @param id The id type use by this controller to query data sources.
 * @param primarySource Main data source used to request fresh data.
 * @param cacheSource Secondary data source used to save and provide previously fetched data.
 */
class CacheController<Resource, Id> (
    private val id: Id,
    private val primarySource: DataSource<Resource, Id>,
    private val cacheSource: Cache<Resource, Id>
) {
    suspend fun fetch(): ETResult<Resource> {
        return safeRetry { fetchFromCacheWithFallback() }
    }

    private suspend fun fetchFromCacheWithFallback(): ETResult<Resource> {
        return if (cacheSource.isCached(id)) {
            cacheSource
                .get(id)
                .recover { fetchAndCache().getOrThrow() }
        } else {
            fetchAndCache()
        }
    }

    private suspend fun fetchAndCache(): ETResult<Resource> {
        return primarySource.fetch(id)
            .fold(
                onSuccess = { item ->
                    cacheSource
                        .put(item, id)
                        .map { item }
                },
                onFailure = { ETResult.failure(it) }
            )
    }
}
