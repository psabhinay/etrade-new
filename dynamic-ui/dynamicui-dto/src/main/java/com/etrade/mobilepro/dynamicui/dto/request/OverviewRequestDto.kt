package com.etrade.mobilepro.dynamicui.dto.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class OverviewRequestDto(
    @Json(name = "overview")
    val overview: String = ""
)
