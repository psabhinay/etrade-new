package com.etrade.mobilepro.transactions.api

import io.realm.RealmObject

open class CachedTransactionDetail(
    var date: String = "",
    var type: String = "",
    var description: String = "",
    var amount: String = ""
) : RealmObject()
