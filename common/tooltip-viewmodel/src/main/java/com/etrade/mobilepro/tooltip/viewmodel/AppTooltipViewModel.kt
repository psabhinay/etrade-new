package com.etrade.mobilepro.tooltip.viewmodel

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.tooltip.AppTooltip
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

interface AppTooltipViewModel {
    val tooltip: LiveData<ConsumableLiveEvent<AppTooltip>>
}
