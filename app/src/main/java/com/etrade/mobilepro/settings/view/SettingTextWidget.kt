package com.etrade.mobilepro.settings.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import androidx.appcompat.widget.AppCompatTextView
import com.etrade.mobilepro.settings.viewmodel.SettingTextDynamicViewModel

@SuppressLint("SetTextI18n")
class SettingTextWidget : AppCompatTextView {

    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    private lateinit var item: SettingTextDynamicViewModel

    private val detector = GestureDetector(
        context,
        object : GestureDetector.SimpleOnGestureListener() {
            override fun onDoubleTap(e: MotionEvent?): Boolean {
                text = item.title + item.alterValue
                item.hasExpandedText = true
                return true
            }

            override fun onDoubleTapEvent(e: MotionEvent?): Boolean = true

            override fun onDown(e: MotionEvent?): Boolean = true
        }
    )

    fun setSettingItem(item: SettingTextDynamicViewModel) {
        this.item = item

        text = if (item.hasExpandedText) { item.title + item.alterValue } else { item.title + item.value }
    }

    init {
        @Suppress("ClickableViewAccessibility")
        setOnTouchListener { _, event ->
            item.alterValue?.also {
                return@setOnTouchListener detector.onTouchEvent(event)
            }

            false
        }
    }
}
