package com.etrade.mobilepro.optionschains.api

import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.livedata.DistinctLiveData
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.OptionData
import java.math.BigDecimal

abstract class BaseOption : Option {

    final override val streamBid: MutableLiveData<BigDecimal> by lazy { MutableLiveData(bid) }
    final override val streamBidSize: MutableLiveData<String> by lazy { MutableLiveData(bidSize) }
    final override val streamAsk: MutableLiveData<BigDecimal> by lazy { MutableLiveData(ask) }
    final override val streamAskSize: MutableLiveData<String> by lazy { MutableLiveData(askSize) }
    final override val streamVolume: MutableLiveData<String> by lazy { DistinctLiveData(volume) }
    final override val streamOpenInterest: MutableLiveData<String> by lazy { DistinctLiveData(openInterest) }
    final override val streamLastPrice: MutableLiveData<BigDecimal> by lazy { DistinctLiveData(lastPrice) }
    final override val streamChange: MutableLiveData<BigDecimal> by lazy { DistinctLiveData(change) }

    protected abstract val bid: BigDecimal
    protected abstract val bidSize: String
    protected abstract val ask: BigDecimal
    protected abstract val askSize: String
    protected abstract val volume: String
    protected abstract val openInterest: String
    protected abstract val lastPrice: BigDecimal
    protected abstract val change: BigDecimal

    @Suppress("ComplexMethod")
    // not really complex just using too many lets
    final override fun updateItem(data: Level1Data) {
        data.bid?.toBigDecimalOrNull()?.let { streamBid.postValue(it) }
        data.bidSize?.let { streamBidSize.postValue(it) }
        data.ask?.toBigDecimalOrNull()?.let { streamAsk.postValue(it) }
        data.askSize?.let { streamAskSize.postValue(it) }
        data.volume?.let { streamVolume.postValue(it) }
        data.lastPrice?.toBigDecimalOrNull()?.let { streamLastPrice.postValue(it) }
        data.change?.toBigDecimalOrNull()?.let { streamChange.postValue(it) }

        if (data is OptionData) {
            data.openInterest?.let { streamOpenInterest.postValue(it) }
        }
    }
}
