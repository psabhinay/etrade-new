package com.etrade.mobilepro.alerts.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.alerts.api.model.AlertGroup
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.alerts.presentation.AlertParcelableContent
import com.etrade.mobilepro.alerts.presentation.base.AlertsDeleter
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.android.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class AlertDetailViewModel @Inject constructor(
    private val alertsRepo: AlertsRepo,
    private val user: User,
    private val alertsDeleter: AlertsDeleter
) : ViewModel() {
    private val _viewState = MutableLiveData<ViewState>()
    private val _viewEffects = SingleLiveEvent<ViewEffects>()
    val viewEffects: LiveData<ViewEffects> = _viewEffects
    val viewState: LiveData<ViewState> = _viewState
    private val compositeDisposable = CompositeDisposable()
    private var passedAlertContent: AlertParcelableContent? = null

    fun loadAlert(alertParcelableContent: AlertParcelableContent) {
        passedAlertContent = alertParcelableContent
        val userId = user.getUserId()
        if (userId == null) {
            _viewState.value = ViewState.Error.LoadingError
            return
        }
        _viewState.value = ViewState.Loading(alertParcelableContent)
        compositeDisposable.add(
            alertsRepo
                .loadAlertContent(userId.toString(), alertParcelableContent.id)
                .subscribeBy(
                    onSuccess = {
                        val updatedScreenContent = AlertParcelableContent(
                            id = it.id,
                            subject = alertParcelableContent.subject,
                            occurredAtFormatted = alertParcelableContent.occurredAtFormatted,
                            content = it.content,
                            group = alertParcelableContent.group
                        )
                        _viewState.postValue(ViewState.Success(updatedScreenContent))
                    },
                    onError = {
                        _viewState.postValue(ViewState.Error.LoadingError)
                    }
                )
        )
    }

    fun deleteAlert() {
        val userId = user.getUserId()?.toString() ?: return
        val alertId = passedAlertContent?.id ?: return
        val group = passedAlertContent?.group ?: return
        _viewState.value = ViewState.Loading()
        alertsDeleter.delete(
            userId, alertId,
            onSuccess = { deletedAlertId ->
                _viewEffects.value = ViewEffects.AlertWasDeleted(deletedAlertId, group)
            },
            onError = { _, _ ->
                _viewState.value = ViewState.Error.DeletionError
            }
        )
    }

    fun askConfirmDelete() {
        with(viewState.value) {
            if (this is ViewState.Success) {
                _viewState.value = ViewState.ConfirmDelete(alert)
            }
        }
    }

    fun cancelDelete() {
        with(viewState.value) {
            if (this is ViewState.ConfirmDelete) {
                _viewState.value = ViewState.Success(alert)
            }
        }
    }

    override fun onCleared() {
        compositeDisposable.clear()
        alertsDeleter.unsubscribeAll()
        super.onCleared()
    }

    sealed class ViewState {
        class Loading(
            val preloadedAlertInfo: AlertParcelableContent? = null
        ) : ViewState()

        class Success(
            val alert: AlertParcelableContent
        ) : ViewState()

        class ConfirmDelete(
            val alert: AlertParcelableContent
        ) : ViewState()

        sealed class Error : ViewState() {
            object LoadingError : Error()
            object DeletionError : Error()
        }
    }

    sealed class ViewEffects {
        data class AlertWasDeleted(val alertId: String, val group: AlertGroup) : ViewEffects()
    }
}
