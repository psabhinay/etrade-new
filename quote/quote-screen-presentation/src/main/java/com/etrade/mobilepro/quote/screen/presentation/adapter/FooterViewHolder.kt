package com.etrade.mobilepro.quote.screen.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemFooterBinding
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.util.android.accessibility.bindButtonAccessibility

@Suppress("LongParameterList")
class FooterViewHolder(private val binding: QuoteItemFooterBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bindTo(
        item: QuoteItem.QuoteFooterItem?,
        companyOverviewClickListener: (() -> Unit)? = null,
        level2ClickListener: (() -> Unit)? = null,
        reportClickListener: (() -> Unit)? = null
    ) {
        item ?: return
        with(binding) {
            level2Title.bindButtonAccessibility()
            level2Title.setOnClickListener { level2ClickListener?.invoke() }

            companyOverviewTitle.bindButtonAccessibility()
            companyOverviewTitle.setOnClickListener {
                companyOverviewClickListener?.invoke()
            }

            analystResearchTitle.bindButtonAccessibility()
            analystResearchTitle.setOnClickListener {
                reportClickListener?.invoke()
            }
        }
    }
}
