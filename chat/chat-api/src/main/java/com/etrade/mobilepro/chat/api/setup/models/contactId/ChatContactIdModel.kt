package com.etrade.mobilepro.chat.api.setup.models.contactId

import com.etrade.mobilepro.chat.api.ChatMetadataModel

data class ChatGetContactIdModel(val data: ChatGetContactIdDataModel?)

data class ChatGetContactIdDataModel(val messages: ChatGetContactIdMessagesModel, val metadata: ChatMetadataModel?)

data class ChatGetContactIdMessagesModel(val chatItems: List<ChatItemModel>)

data class ChatItemModel(var id: String? = null)
