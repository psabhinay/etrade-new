package com.etrade.mobilepro.util

import com.google.gson.reflect.TypeToken
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import org.simpleframework.xml.convert.AnnotationStrategy
import org.simpleframework.xml.core.Persister
import java.io.IOException
import javax.inject.Inject

interface Serializer<T> {
    fun serialize(target: T): String
    fun deserialize(target: String): T
}

class JsonSerializer<T> @Inject constructor(
    val moshi: Moshi
) : Serializer<T> {

    lateinit var adapter: JsonAdapter<T>

    @Suppress("UNCHECKED_CAST")
    inline fun <reified T2 : T> init() {
        adapter = moshi.adapter<T2>(object : TypeToken<T2>() {}.type) as JsonAdapter<T>
    }

    override fun serialize(target: T): String {
        checkInit()
        return adapter.toJson(target)
    }

    override fun deserialize(target: String): T {
        checkInit()
        return adapter.fromJson(target)
            ?: throw IOException("${JsonSerializer::class.java.canonicalName} could not deserialize $target")
    }

    private fun checkInit() {
        check(::adapter.isInitialized) { "${JsonSerializer::class.java.canonicalName} is not initialized, call its init() method before using it" }
    }
}

inline fun <reified T> String.parseXml(): T? {
    val persister = Persister(AnnotationStrategy())
    return persister.read(T::class.java, this, false)
}
