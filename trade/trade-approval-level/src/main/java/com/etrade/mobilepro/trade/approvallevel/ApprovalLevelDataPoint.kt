package com.etrade.mobilepro.trade.approvallevel

import com.etrade.mobilepro.trade.api.AccountMode

class ApprovalLevelDataPoint(
    val approvalLinkLabel: String,
    val accountMode: AccountMode = AccountMode.UNKNOWN
)
