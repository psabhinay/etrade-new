package com.etrade.mobilepro.chart.performance.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b`\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\u0007"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/data/PerformanceChartApiClient;", "", "getPerformanceChart", "Lio/reactivex/Observable;", "Lcom/etrade/mobilepro/chart/performance/data/PerformanceChartResponseDto;", "request", "Lcom/etrade/mobilepro/chart/data/ChartRequestDto;", "performance_debug"})
public abstract interface PerformanceChartApiClient {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "/app/apg/perfChart.json")
    @com.etrade.mobilepro.util.json.JsonMoshi()
    public abstract io.reactivex.Observable<com.etrade.mobilepro.chart.performance.data.PerformanceChartResponseDto> getPerformanceChart(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    @com.etrade.mobilepro.util.json.JsonMoshi()
    com.etrade.mobilepro.chart.data.ChartRequestDto request);
}