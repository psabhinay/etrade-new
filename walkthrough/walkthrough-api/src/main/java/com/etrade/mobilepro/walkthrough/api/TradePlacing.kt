package com.etrade.mobilepro.walkthrough.api

enum class NovicePriceType {
    MARKET,
    LIMIT,
    DEFAULT
}

sealed class TradePlacingState : State {
    object Idle : TradePlacingState(), IdleState
    object DisplayInfoMessage : TradePlacingState()
    object DisplayTradePage : TradePlacingState()
    object HighlightSymbol : TradePlacingState()
    object HighlightOrderType : TradePlacingState()
    object HighlightQuantity : TradePlacingState()
    object HighlightPriceType : TradePlacingState()
    object DisplayPriceTypes : TradePlacingState()
    object EnterPriceType : TradePlacingState()
    object HighlightTerm : TradePlacingState()
    object HighlightPreview : TradePlacingState()
    object HighlightTrade : TradePlacingState()
}

sealed class TradePlacingAction : Action {
    object StartWalkthrough : TradePlacingAction()
    object InfoMessageDisplayed : TradePlacingAction()
    object TradePageDisplayed : TradePlacingAction()
    object SymbolHighlighted : TradePlacingAction()
    object OrderTypeHighlighted : TradePlacingAction()
    object QuantityHighlighted : TradePlacingAction()
    object PriceTypeHighlighted : TradePlacingAction()
    object PriceTypesDisplayed : TradePlacingAction()
    object PriceTypeEntered : TradePlacingAction()
    object TermHighlighted : TradePlacingAction()
    object PreviewHighlighted : TradePlacingAction()
    object TradeHighlighted : TradePlacingAction()
    object Canceled : TradePlacingAction()
}
