package com.etrade.mobilepro.orders

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.quote.presentation.AffiliatedProductAction
import com.etrade.mobilepro.quote.presentation.updateAffiliatedImage
import com.etrade.mobilepro.quoteapi.QuoteHeaderViewItem

@BindingAdapter(value = ["imageSrc", "quoteItem", "affiliatedProductAction"])
fun TextView.updateAffiliatedImageForOrder(
    imageSrc: Int,
    quoteItem: QuoteHeaderViewItem?,
    onAffiliatedProductAction: AffiliatedProductAction? = null
) = updateAffiliatedImage(
    affiliatedImage = imageSrc,
    quoteItem = quoteItem,
    onAffiliatedProductAction = onAffiliatedProductAction,
    affiliatedProductTitleAction = null
)
