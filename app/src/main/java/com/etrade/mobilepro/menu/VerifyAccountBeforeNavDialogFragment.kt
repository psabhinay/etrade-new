package com.etrade.mobilepro.menu

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.viewmodel.EtNavigationViewModel
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder

abstract class VerifyAccountBeforeNavDialogFragment(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val userViewModel: UserViewModel
) : DialogFragment() {

    private val etNavigationViewModel: EtNavigationViewModel by activityViewModels { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (userViewModel.isAuthenticated) {
            dismiss()
            etNavigationViewModel.navigate(getDirections(false))
        }
    }

    protected abstract fun getDirections(withLogin: Boolean): NavDirections

    protected fun getWebViewDirections(withLogin: Boolean, webViewTitle: String, webViewUrl: String): NavDirections {
        return if (withLogin) {
            VerifyAccountBeforeWebViewDialogFragmentDirections.actionMenuLoginRequiredWebView(webViewTitle, webViewUrl)
        } else {
            VerifyAccountBeforeWebViewDialogFragmentDirections.actionMenuWebView(webViewTitle, webViewUrl)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return context?.let { ctx ->
            val builder = MaterialAlertDialogBuilder(ctx)
                .setMessage(R.string.already_e_trade_customer_prompt)
                .setPositiveButton(R.string.yes) { _, _ -> etNavigationViewModel.navigate(getDirections(true)) }
                .setNegativeButton(R.string.no) { _, _ ->
                    etNavigationViewModel.navigate(getDirections(false))
                }
            builder.create()
        } ?: super.onCreateDialog(savedInstanceState)
    }
}
