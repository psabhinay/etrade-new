package com.etrade.mobilepro.pushnotification.data.rest

import com.etrade.eo.rest.retrofit.Xml
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface PushSettingsApiClient {
    @POST("e/t/mobile/SetAndroidProfileItem")
    @Xml
    @FormUrlEncoded
    suspend fun setAlertPushSettings(
        @Field("deviceId") deviceId: String,
        @Field("acctAlert") isAccountAlertEnabled: String,
        @Field("stockAlert") isStockAlertEnabled: String,
        @Field("regId") firebaseCloudMessagingToken: String?
    ): AlertPushSettingsResponseDto

    @POST("e/t/mobile/UpdateAndroidRegId")
    @Xml
    @FormUrlEncoded
    suspend fun subscribeToSmartAlerts(
        @Field("deviceId") deviceId: String,
        @Field("regId") firebaseCloudMessagingToken: String?
    ): AlertPushSettingsResponseDto
}
