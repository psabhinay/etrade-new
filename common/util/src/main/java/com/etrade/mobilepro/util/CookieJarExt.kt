package com.etrade.mobilepro.util

import okhttp3.CookieJar
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull

const val SESSION_COOKIE_NAME = "SMSESSION"

fun CookieJar.getSessionCookieStringOrEmpty(urlString: String) = urlString.toHttpUrlOrNull()
    ?.let { loadForRequest(it) }
    ?.lastOrNull { SESSION_COOKIE_NAME.equals(it.name, ignoreCase = true) }
    ?.run { "$name=$value" }
    .orEmpty()
