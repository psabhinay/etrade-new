package com.etrade.mobilepro.screener.presentation.mutualfund.performance

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.mutualfund.PerformanceFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.MutualFund
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class PerformanceViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @MutualFund screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<PerformanceFilter, PerformanceViewModel.ViewState>(
    aggregator,
    ScreenerType.MutualFund.Performance,
    screenerInteractor
) {
    override fun provideInitialState(initialFilter: PerformanceFilter): ViewState = ViewState(filter = initialFilter)

    data class ViewState(
        override val filter: PerformanceFilter
    ) : BaseFilterScreenViewModel.ViewState<PerformanceFilter>
}
