package com.etrade.mobilepro.dynamic.form.api

import androidx.lifecycle.LiveData

class Action(
    val id: Any,
    val title: String,
    val actionType: ActionType,
    val isEnabled: LiveData<Boolean>,
    val execute: OnActionClickListener
)

/**
 * Primary buttons are filled with purple, whereas secondary buttons are blank
 */
enum class ActionType {
    PRIMARY,
    SECONDARY
}

interface OnActionClickListener {
    fun onActionClick(actionId: Any)
}

operator fun Action.invoke() = execute.onActionClick(id)
