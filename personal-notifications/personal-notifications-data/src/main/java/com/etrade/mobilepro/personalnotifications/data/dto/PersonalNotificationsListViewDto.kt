package com.etrade.mobilepro.personalnotifications.data.dto

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal
import java.math.BigInteger

@JsonClass(generateAdapter = true)
data class PersonalNotificationsListViewDto(
    @Json(name = "data")
    override val data: PersonalNotificationsDto?,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?
) : BaseScreenView()

@JsonClass(generateAdapter = true)
data class PersonalNotificationsDto(
    @Json(name = "external_account_verification")
    val externalAccountVerification: List<ExternalAccountVerificationDto>?,
    @Json(name = "pending_transactions")
    val pendingTransactions: List<PendingTransactionsDto>?
)

@JsonClass(generateAdapter = true)
class ExternalAccountVerificationDto(
    @Json(name = "accountUuid")
    val accountUuid: String,
    @Json(name = "accountId")
    val accountId: String
)

@JsonClass(generateAdapter = true)
class PendingTransactionsDto(
    @Json(name = "accountUuid")
    val accountUuid: String,
    @Json(name = "transactions")
    val transactions: List<TransactionDto>?
)

@JsonClass(generateAdapter = true)
class TransactionDto(
    @Json(name = "fundAvailabilityDate")
    val fundAvailabilityDate: String?,
    @Json(name = "amount")
    val amount: BigDecimal?,
    @Json(name = "activityId")
    val activityId: BigInteger?
)
