package com.etrade.mobilepro.watchlist.presentation

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.util.TypedValue
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModelProvider
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.eo.corelibandroid.createViewModel
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.Styles
import com.etrade.mobilepro.common.compose.theme.etColors
import com.etrade.mobilepro.common.extension.screenWidthPixels
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.compose.components.ComposeTableView
import com.etrade.mobilepro.compose.components.DisclosuresItem
import com.etrade.mobilepro.compose.components.EtRetrySnackbar
import com.etrade.mobilepro.compose.components.LoadingIndicator
import com.etrade.mobilepro.compose.components.TableViewCell
import com.etrade.mobilepro.compose.components.TableViewColumn
import com.etrade.mobilepro.compose.components.TableViewRowHeader
import com.etrade.mobilepro.compose.components.TableViewSortState
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.dropdown.DropDownManager
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.etcompose.tabhorizontalpager.PagerContent
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.movers.MoverItem
import com.etrade.mobilepro.navdestinations.openDisclosures
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.tableviewutils.presentation.cell.getRowHeaderTextAndSubText
import com.etrade.mobilepro.util.SortOrder
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.textutil.prepareOptionTicketDatePrice
import com.etrade.mobilepro.util.formatCurrencyAmount
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.formatters.formatMarketPercentDataCustom
import com.etrade.mobilepro.util.removePercent
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingAction
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.etrade.mobilepro.watchlist.overview.listview.WatchlistListView
import com.etrade.mobilepro.watchlist.overview.tileview.WatchlistTileView
import com.etrade.mobilepro.watchlist.overview.viewmodel.ViewState
import com.etrade.mobilepro.watchlist.overview.viewmodel.WatchlistEntries
import com.etrade.mobilepro.watchlist.overview.viewmodel.WatchlistOverviewViewModel
import com.etrade.mobilepro.watchlist.presentation.compose.ControlBar
import com.etrade.mobilepro.watchlist.presentation.tableview.WatchlistTableViewControlBarViewModel
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.evrencoskun.tableview.sort.SortState
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import org.slf4j.Logger
import org.slf4j.LoggerFactory

private const val TABLE_VIEW_PADDING = 112

@Suppress("LargeClass")
class WatchlistOverviewTabDelegate(
    private val fragment: Fragment,
    private val walkthroughStatesViewModel: WalkthroughStatesViewModel,
    private val mainNavigation: MainNavigation,
    private val onUnAuthFilterClickAction: () -> Unit,
    private val containerViewModel: () -> WatchlistContainerViewModel,
    viewModelFactory: ViewModelProvider.Factory,
    watchlistOverviewViewModelFactory: WatchlistOverviewViewModel.Factory,
    watchlistId: String?
) : LifecycleObserver {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    init {
        fragment.lifecycle.addObserver(this)
    }

    private val onScroll: (Int, Int) -> Unit = { fromIndex, toIndex ->
        if (overviewViewModel.isStreamingToggleEnabled) {
            overviewViewModel.viewState.value?.let {
                if (it is ViewState.FetchEntries.Success) {
                    val tableTypeSelectedIndex = controlBarViewModel.tableTypeSelectedIndex.value
                    var updatedToIndex = if (tableTypeSelectedIndex == 0) {
                        toIndex - 1
                    } else {
                        toIndex
                    }
                    val updatedSymbols = if (tableTypeSelectedIndex == 0) {
                        updatedToIndex = if (it.tableViewData.rowHeaders.size < updatedToIndex) {
                            it.tableViewData.rowHeaders.size
                        } else {
                            updatedToIndex
                        }
                        it.tableViewData.rowHeaders.subList(fromIndex, updatedToIndex).map { withSymbolInfo ->
                            Symbol(withSymbolInfo)
                        }.toSet()
                    } else {
                        updatedToIndex = if (it.watchlistEntryList.size < updatedToIndex) {
                            it.watchlistEntryList.size
                        } else {
                            updatedToIndex
                        }
                        it.watchlistEntryList.subList(fromIndex, updatedToIndex).map { watchlistEntry ->
                            Symbol(watchlistEntry.ticker, watchlistEntry.typeCode ?: InstrumentType.UNKNOWN)
                        }.toSet()
                    }
                    overviewViewModel.notifyVisibleItemsChanged(updatedSymbols)
                }
            }
        }
    }
    private val onSort: (Int?, Int?) -> Unit = { columnIndex, visibleItemSize ->
        overviewViewModel.sortTable(columnIndex, visibleItemSize)
    }

    private val onRowHeaderClick: ((Int) -> Unit) = {
        navigateToQuoteDetails(it)
    }

    private val onTileClick: ((MoverItem, Int) -> Unit) = { _, index ->
        navigateToQuoteDetails(index, false)
    }

    private val onListClick: ((Int) -> Unit) = { index ->
        navigateToQuoteDetails(index, false)
    }

    private val overviewViewModel: WatchlistOverviewViewModel by fragment.createViewModel {
        watchlistOverviewViewModelFactory.create(
            watchlistId
        )
    }

    private val controlBarViewModel by lazy {
        ViewModelProvider(fragment, viewModelFactory).get(
            WatchlistTableViewControlBarViewModel::class.java
        )
    }

    private var dropDownManager: DropDownManager<Watchlist>? = null
    private val editWatchlistRequest = fragment.registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        onEditWatchlist(result.resultCode, result.data)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onResume() {
        overviewViewModel.resumeStreaming()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onPause() {
        overviewViewModel.pauseStreaming()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onDestroy() {
        fragment.lifecycle.removeObserver(this)
    }

    @ExperimentalFoundationApi
    fun watchlistOverview(): PagerContent = {
        WatchlistOverview()
    }

    private fun onEditWatchlist(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val watchlistId = data?.getStringExtra(EditWatchlistActivity.EXTRA_WATCHLIST_ID)
            if (watchlistId != null) {
                containerViewModel().deleteWatchlist(watchlistId)
            } else {
                overviewViewModel.refreshCurrentWatchList()
            }
        }
    }

    @ExperimentalFoundationApi
    @Composable
    fun WatchlistOverview() {
        val viewState: ViewState? by overviewViewModel.viewState.observeAsState()
        val tableTypeSelectedIndex: Int by controlBarViewModel.tableTypeSelectedIndex.observeAsState(0)

        Column(Modifier.fillMaxSize()) {
            when (viewState) {
                is ViewState.FetchWatchlist.Loading -> {
                    CenteredFullSizeBox {
                        LoadingIndicator()
                    }
                }
                else -> {
                    SwipeRefresh(rememberSwipeRefreshState(false), { overviewViewModel.refreshCurrentWatchList() }) {
                        Column {
                            ControlBar(
                                overviewViewModel,
                                controlBarViewModel,
                                tableTypeSelectedIndex,
                                onDropDownClick = remember { { controlBarViewModel.onDropDownClick(onUnAuthFilterClickAction) } },
                                controlBarViewModel::onTableTypeChange,
                                controlBarViewModel::onCustomizeViewClick,
                                controlBarViewModel::dismissTooltip
                            )
                            TableViewPart(viewState, tableTypeSelectedIndex)
                        }
                    }
                }
            }
        }
    }

    @ExperimentalFoundationApi
    @Composable
    private fun TableViewPart(
        viewState: ViewState?,
        tableTypeSelectedIndex: Int
    ) {
        when (viewState) {
            is ViewState.FetchEntries.NoItem -> {
                NoItemView()
            }
            is ViewState.FetchEntries.Success, is ViewState.FetchEntries.Error, is ViewState.FetchWatchlist.Error -> {
                TableViewSuccessAndError(viewState, tableTypeSelectedIndex)
            }
            ViewState.FetchEntries.Loading -> CenteredFullSizeBox {
                LoadingIndicator()
            }
            else -> {
            }
        }
    }

    @ExperimentalFoundationApi
    @SuppressWarnings("ComplexMethod")
    @Composable
    private fun TableViewSuccessAndError(
        viewState: ViewState,
        tableTypeSelectedIndex: Int
    ) {
        Box(modifier = Modifier.fillMaxSize()) {
            val entries = (viewState as ViewState.WithEntries).tableViewData
            if (entries != null) {
                when (tableTypeSelectedIndex) {
                    0 -> {
                        WatchlistGridView(entries)
                    }
                    1 -> {
                        WatchlistListView(viewState.watchlistEntryList, onScroll, onListClick) { WatchlistFooter(entries) }
                    }
                    2 -> {
                        WatchlistTileView(
                            entryList = viewState.watchlistEntryList.mapToMoverItems(),
                            onItemTap = onTileClick,
                            footer = { WatchlistFooter(entries) }
                        )
                    }
                    else -> {}
                }
                walkthroughStatesViewModel.onAction(SymbolTrackingAction.WatchlistPageDisplayed)
            } else {
                NoItemView()
            }

            TableViewError(viewState)
        }
    }

    private fun List<WatchlistEntry>?.mapToMoverItems() = this?.map {
        val decimalChange = it.change
        val isPositiveColor = decimalChange?.signum()?.let { num -> num >= 0 } ?: false
        val formattedPercentChange: String = MarketDataFormatter.formatMarketPercentDataCustom(it.changePercent)
        val formattedChange: String = MarketDataFormatter.formatMarketDataCustom(decimalChange)
        val formattedPrice: String = MarketDataFormatter.formatMarketDataCustom(it.lastPrice)
        val priceDescription = fragment.resources.getString(R.string.label_last_price, formattedPrice.formatCurrencyAmount())
        val gainDescription = fragment.resources.getString(
            com.etrade.mobilepro.movers.R.string.label_days_change_dollars_and_percent,
            formattedChange.formatCurrencyAmount(),
            formattedPercentChange.formatCurrencyAmount().removePercent()
        )
        val (optionsExpiryDate, optionsPrice) = prepareOptionTicketDatePrice(it.ticker, it.typeCode, it.description)
        val displaySymbol = it.getDisplaySymbol()
        MoverItem(
            ticker = it.ticker,
            displaySymbol = displaySymbol,
            price = formattedPrice,
            gain = formattedChange,
            gainPercentage = formattedPercentChange,
            decimalPercentChange = it.changePercent,
            priceDescription = priceDescription,
            gainDescription = gainDescription,
            isPositiveGain = isPositiveColor,
            shouldDisplayOptionsData = true,
            optionsExpiryDate = optionsExpiryDate,
            optionsStrikePricePlusType = optionsPrice
        )
    } ?: emptyList()

    fun WatchlistEntry.getDisplaySymbol() = if (typeCode?.isOption == true) {
        ticker.substringBefore('-')
    } else {
        ticker
    }

    @ExperimentalFoundationApi
    @Composable
    private fun WatchlistGridView(entries: WatchlistEntries) {
        val sortInfo by overviewViewModel.sortState.observeAsState()
        val paddingValues = PaddingValues(bottom = TABLE_VIEW_PADDING.dp)

        val columns = entries.columnHeaders.map {
            TableViewColumn(it.displayText)
        }

        val rowHeaders = entries.rowHeaders.map {
            val (text, subText) = it.getRowHeaderTextAndSubText()
            TableViewRowHeader(text?.toString() ?: "", subText?.toString())
        }
        val cellItems = entries.cellItems.map {
            it.map { cell ->
                TableViewCell(
                    cell.displayText,
                    cell.dataColor
                )
            }
        }
        val sortState = TableViewSortState(
            sortInfo?.sortState.toSortOrder(),
            sortInfo?.sortedColumnPosition
        )

        ComposeTableView(
            columns, rowHeaders, cellItems, sortState, onScroll, onSort, onRowHeaderClick, paddingValues
        ) { WatchlistFooter(entries) }
    }

    private fun LazyListScope.WatchlistFooter(entries: WatchlistEntries) {
        val asOfTime = entries.timestamp?.let {
            fragment.getString(R.string.as_of_time, it)
        } ?: ""
        val mfTimestamp = entries.mfTimestamp

        DateTimeFooter(asOfTime, mfTimestamp)
        DisclosuresItem { startActivityDisclosure() }
    }

    private fun LazyListScope.DateTimeFooter(asOfTime: String, mfTimestamp: String?) = item {
        Column(
            modifier = Modifier.fillMaxWidth().padding(top = Dimens.Size16dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            if (mfTimestamp != null) {
                Text(text = mfTimestamp, style = Styles.Regular14sp)
            }
            Text(
                modifier = Modifier.padding(bottom = Dimens.Size16dp),
                text = asOfTime,
                style = Styles.Regular14sp,
                color = MaterialTheme.etColors.footerDateTime
            )
        }
    }

    private fun SortState?.toSortOrder(): SortOrder = when (this) {
        SortState.ASCENDING -> SortOrder.ASCENDING
        SortState.DESCENDING -> SortOrder.DESCENDING
        SortState.UNSORTED -> SortOrder.UNDEFINED
        else -> SortOrder.UNDEFINED
    }

    @Composable
    private fun TableViewError(viewState: ViewState) {
        when (viewState) {
            is ViewState.FetchWatchlist.Error.RetryError -> EtRetrySnackbar {
                overviewViewModel.getUserWatchlists()
            }
            is ViewState.FetchEntries.Error.RetryError -> EtRetrySnackbar {
                overviewViewModel.refreshCurrentWatchList()
            }
            is ViewState.FetchWatchlist.Error.NoRetryError,
            is ViewState.FetchEntries.Error.NoRetryError -> EtRetrySnackbar()
            else -> {
            }
        }
    }

    @Composable
    private fun NoItemView() {
        CenteredFullSizeBox {
            Text(text = fragment.getString(R.string.watchlist_no_items_in))
        }
    }

    @Composable
    private fun CenteredFullSizeBox(content: @Composable BoxScope.() -> Unit) {
        Box(Modifier.fillMaxSize(), Alignment.Center, content = content)
    }

    private fun navigateToQuoteDetails(index: Int, isGridView: Boolean = true) = with(fragment) {
        overviewViewModel.getSymbolCell(index, isGridView)?.let {
            val searchResultItem = overviewViewModel.getSymbolFromCell(it)
            mainNavigation.navigateToQuoteDetails(activity, searchResultItem.toBundle())
        }
    }

    private fun calculateNewRowHeaderWidth(): Int = with(fragment) {
        val outValue = TypedValue()
        resources.getValue(R.dimen.default_tableview_row_header_width, outValue, true)
        (requireActivity().screenWidthPixels * outValue.float).toInt()
    }

    private fun startActivityDisclosure() {
        with(fragment) {
            startActivity(activity?.openDisclosures())
        }
    }

    fun onViewCreated() {
        overviewViewModel.watchlists.observe(
            fragment.viewLifecycleOwner,
            { listOfWatchLists ->
                initDropDown(listOfWatchLists)
            }
        )
        overviewViewModel.needRefresh.observe(
            fragment.viewLifecycleOwner,
            {
                it.consume { needRefresh ->
                    if (needRefresh) {
                        overviewViewModel.refreshCurrentWatchList()
                    }
                }
            }
        )
    }

    @SuppressLint("DefaultLocale")
    private fun initDropDown(listOfWatchLists: List<Watchlist>) {
        dropDownManager =
            BottomSheetSelector<Watchlist>(fragment.childFragmentManager, fragment.resources).apply {
                init(
                    tag = WL_DROPDOWN_TAG,
                    title = fragment.getString(R.string.watchlist_select_title),
                    items = listOfWatchLists,
                    initialSelectedPosition = overviewViewModel.getIndexOfSelectedWatchlist()
                )
                setListener { position, selectedWatchList ->
                    controlBarViewModel.dropdownSelectedIndex = position
                    overviewViewModel.getWatchlistEntries(selectedWatchList)
                }
                setDismissAction {
                    overviewViewModel.resumeStreaming()
                }
            }
        controlBarViewModel.init(listOfWatchLists.mapNotNull { it.name }.toSet())
        controlBarViewModel.clickEvents.observe(
            fragment.viewLifecycleOwner,
            {
                when (it) {
                    CtaEvent.LaunchCustomizeColumns -> handleCustomizeColumnSignal()
                    CtaEvent.ShowDropdownSelector -> {
                        dropDownManager?.run {
                            updateInitialSelectedPosition(overviewViewModel.getIndexOfSelectedWatchlist())
                            openDropDown()
                            overviewViewModel.pauseStreaming()
                        }
                    }
                    else -> {
                    }
                }
            }
        )
    }

    private fun handleCustomizeColumnSignal() {
        fragment.context?.also {
            val intent = EditWatchlistColumnsActivity.intent(it) {
                isDeletableItems = false
            }
            editWatchlistRequest.launch(intent)
        }
    }
}
