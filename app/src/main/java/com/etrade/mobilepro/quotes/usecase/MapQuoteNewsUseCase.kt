package com.etrade.mobilepro.quotes.usecase

import android.content.res.Resources
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.getOrElse
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.quote.mapper.MobileQuotePageItemMapper
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.quoteapi.QuoteItemInterface
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.isEtfSymbol
import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.util.android.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface MapQuoteNewsUseCase : UseCase<MapQuoteNewsParameter, List<QuoteItemInterface>>

class MapQuoteNewsUseCaseImpl @Inject constructor(
    private val mapper: MobileQuotePageItemMapper,
    private val resources: Resources
) : MapQuoteNewsUseCase {

    override suspend fun execute(parameter: MapQuoteNewsParameter): List<QuoteItemInterface> = withContext(Dispatchers.IO) {
        with(parameter) {
            result.map { newsItems ->
                when {
                    symbol.type.isMutualFundOrMoneyMarketFund -> newsItems
                    symbol.isEtfSymbol() -> {
                        mapper.getBaseQuoteDetails(newsItems) + mapper.getEtfFooterDetails(inInDualPane)
                    }
                    else -> {
                        val footerItems = if (!symbol.type.isIndex && !parameter.inInDualPane) {
                            mapper.getFooterDetails()
                        } else {
                            emptyList()
                        }
                        footerItems + listOf(QuoteItem.ThickGutter) + mapper.getBaseQuoteDetails(newsItems)
                    }
                }
            }.getOrElse { mapper.map(resources.getString(R.string.error_message_general), symbol.type.isIndex || parameter.inInDualPane) }
        }
    }
}

class MapQuoteNewsParameter(
    val symbol: SearchResultItem.Symbol,
    val result: ETResult<List<NewsItem.Text>>,
    val inInDualPane: Boolean
)
