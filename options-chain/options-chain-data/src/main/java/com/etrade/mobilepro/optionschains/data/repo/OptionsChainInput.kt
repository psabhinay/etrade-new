package com.etrade.mobilepro.optionschains.data.repo

/**
 * Data class to reference the input for OptionsChain
 */
internal class OptionsChainInput(
    val symbol: String,
    val expiryTypeCode: Int,
    val year: Int? = null,
    val month: Int? = null,
    val day: Int? = null
) {

    init {
        require(symbol.isNotBlank())
    }
}

private const val YEAR_LAST_DIGITS = 100
private const val INT_THRESHOLD = 10

internal fun OptionsChainInput.createPath(): String {
    var trimmedSymbol = symbol.trim()
    return if (day == null || month == null || year == null) {
        trimmedSymbol
    } else {
        if (trimmedSymbol.contains('-')) {
            trimmedSymbol = trimmedSymbol.split("-")[0]
        }

        val formattedYear = (year % YEAR_LAST_DIGITS).toString()
        val formattedMonth = month.prependWithZero()
        val formattedDay = day.prependWithZero()
        "$trimmedSymbol$$formattedMonth$formattedDay$formattedYear"
    }
}

private fun Int.prependWithZero(): String = if (this < INT_THRESHOLD) "0$this" else "$this"
