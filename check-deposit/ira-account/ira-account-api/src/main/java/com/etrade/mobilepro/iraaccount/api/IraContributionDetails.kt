package com.etrade.mobilepro.iraaccount.api

import org.threeten.bp.LocalDate

interface IraContributionDetails {
    val availableDepositTypes: List<String>

    val currentYearContributionDetails: Details
    val previousYearContributionDetails: Details?
    val shouldShowPreviousYear: Boolean

    interface Details {
        val taxYear: String
        val yearToDateContribution: String
        val contributionLimit: String
        val contributionAmountLeft: String
        val contributionDeadline: LocalDate?
    }
}
