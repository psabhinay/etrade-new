package com.etrade.mobilepro.marketscalendar.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.marketscalendar.R
import com.etrade.mobilepro.marketscalendar.adapter.MarketsEventDetailsAdapter
import com.etrade.mobilepro.marketscalendar.databinding.FragmentCalendarEventDetailsBinding
import com.etrade.mobilepro.marketscalendar.viewmodel.CalendarEventDetailsViewModel
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.util.android.addToCalendar
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate

abstract class CalendarEventDetailsFragment(
    private val viewModelFactory: ViewModelProvider.Factory
) : OverlayBaseFragment() {

    abstract fun createAdapterDelegate(layoutInflater: LayoutInflater, noValue: String): AdapterDelegate<MutableList<EventDetails>>

    abstract val item: EventDetails?

    override val layoutRes: Int = R.layout.fragment_calendar_event_details

    private val viewModel: CalendarEventDetailsViewModel by viewModels { viewModelFactory }

    private val binding by viewBinding(FragmentCalendarEventDetailsBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutInflater: LayoutInflater = LayoutInflater.from(context)
        val noValue: String = getString(R.string.empty_default)

        binding.itemsRv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = item?.let { MarketsEventDetailsAdapter(it, createAdapterDelegate(layoutInflater, noValue)) }
        }

        initViewModel()
    }

    private fun initViewModel() {
        viewModel.addToCalendarActionVisibility.observe(
            viewLifecycleOwner,
            Observer { isVisible ->
                binding.btnAddToCalendar.visibility = if (isVisible) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
            }
        )

        viewModel.getCalendarEvent().observe(
            viewLifecycleOwner,
            Observer {
                binding.btnAddToCalendar.setOnClickListener {
                    addEventToCalendar()
                }
            }
        )

        viewModel.item.value = item
    }

    private fun addEventToCalendar() {
        viewModel.getCalendarEvent().value?.also { event ->
            context?.also { addToCalendar(it, event) }
        }
    }
}
