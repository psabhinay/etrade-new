package com.etrade.mobilepro.screener.data.repo.mapper

import com.etrade.mobilepro.screener.api.FundCategory

class FundCategoryResponseMapper {
    fun map(jsonDto: String): FundCategory = FundCategory(
        id = jsonDto,
        title = jsonDto,
        isAllItemsElement = false
    )
}
