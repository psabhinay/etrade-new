package com.etrade.mobilepro.chartiq.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class AccountDto(

    @Json(name = "value")
    val id: String,

    @Json(name = "displayText")
    val displayName: String
)
