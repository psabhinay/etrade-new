package com.etrade.mobilepro.transfermoney.presentation.transfer

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.map
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.eo.core.util.isZero
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.disposeOnDestinationChanged
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.etrade.mobilepro.transfermoney.api.TransferRepeat
import com.etrade.mobilepro.transfermoney.presentation.R
import com.etrade.mobilepro.transfermoney.presentation.TransferMoneyContainerFragmentArgs
import com.etrade.mobilepro.transfermoney.presentation.TransferMoneyContainerFragmentDirections
import com.etrade.mobilepro.transfermoney.presentation.common.getTitleId
import com.etrade.mobilepro.transfermoney.presentation.contributiondetail.TransferIraContributionDetailViewModel
import com.etrade.mobilepro.transfermoney.presentation.databinding.TransferFragmentTransferBinding
import com.etrade.mobilepro.transfermoney.presentation.external.ExternalAccountVerificationMethodActionSheetViewModel
import com.etrade.mobilepro.transfermoney.widget.CALENDAR_REQUEST_KEY
import com.etrade.mobilepro.transfermoney.widget.TransferMoneyCalendarDialogFragment
import com.etrade.mobilepro.transfermoney.widget.extractCalendarDialogResult
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetClicksViewModel
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetStyle
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetViewDelegate
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.accessibility.bindTextInputButtonAccessibility
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.clearFocusSafely
import com.etrade.mobilepro.util.android.extension.setTextSafely
import com.etrade.mobilepro.util.android.hideSoftKeyboard
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.android.textutil.CurrencyTextWatcher
import com.google.android.material.snackbar.Snackbar
import java.math.BigDecimal
import java.math.BigDecimal.ROUND_HALF_UP
import javax.inject.Inject

private const val TRANSFER_REPEAT_TAG = "transfer_repeat_tag"
private const val TRANSFER_FROM_TAG = "transfer_from_tag"
private const val TRANSFER_TO_TAG = "transfer_to_tag"
private const val EMPTY_STRING = ""

@Suppress("LargeClass")
@RequireLogin
class TransferFragment @Inject constructor(
    private val factory: TransferViewModelFactoryFactory,
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.transfer_fragment_transfer) {

    private val binding by viewBinding(TransferFragmentTransferBinding::bind)
    private val args: TransferMoneyContainerFragmentArgs by navArgs()
    private val viewModel: TransferViewModel by navGraphViewModels(R.id.transfer_money_nav_graph) {
        factory.create(args.fromAccount?.ifBlank { null }, args.amount?.ifBlank { null })
    }
    private val iraDetailsViewModel by navGraphViewModels<TransferIraContributionDetailViewModel>(R.id.transfer_money_nav_graph) { viewModelFactory }
    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private lateinit var transferRepeatSelector: DropDownMultiSelector<TransferRepeat>
    private lateinit var transferFromSelector: DropDownMultiSelector<TransferMoneyAccount>
    private lateinit var transferToSelector: DropDownMultiSelector<TransferMoneyAccount>
    private lateinit var currencyTextWatcher: CurrencyTextWatcher
    private var iraRetrySnackBar: Snackbar? = null

    private val disposeListener = disposeOnDestinationChanged { viewModel.disposeState() }
    private var savedAmount: String? = null

    private val actionSheetViewModel by viewModels<ExternalAccountVerificationMethodActionSheetViewModel> { viewModelFactory }
    private val actionSheetClicksViewModel: ActionSheetClicksViewModel by viewModels { viewModelFactory }

    private val actionSheetDelegate: ActionSheetViewDelegate by lazy {
        ActionSheetViewDelegate(
            actionSheetViewModel,
            actionSheetClicksViewModel,
            childFragmentManager,
            ActionSheetStyle.ExternalAccountVerification
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionSheetDelegate.onCreate(savedInstanceState)

        setFragmentResultListener(CALENDAR_REQUEST_KEY) { _, bundle ->
            bundle.extractCalendarDialogResult()
                .let {
                    viewModel.updateSelectedDate(it.selectedDate)
                    viewModel.currentCalendarPage = it.selectedPage
                }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
        savedInstanceState?.let { getCalendarBuilder().restore(this) }
    }

    override fun onResume() {
        super.onResume()
        findNavController().addOnDestinationChangedListener(disposeListener)
    }

    override fun onPause() {
        super.onPause()
        findNavController().removeOnDestinationChangedListener(disposeListener)
    }

    private fun setupFragment() {
        setupDropDownLists()
        setupClickListeners()
        setupAmountField()
        setupContributionDetailsButton()
        viewModel.restoreTransferAmountSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { amount -> setFormattedAmount(amount, false) }
            }
        )
        viewModel.scrollUpSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { binding.content.smoothScrollTo(0, 0) }
            }
        )
        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Boolean>(ADD_EXTERNAL_FRAGMENT_RESULT_KEY)
            ?.observe(
                viewLifecycleOwner,
                Observer { doneClicked ->
                    if (doneClicked) {
                        viewModel.refreshAccounts()
                    }
                }
            )
    }

    private fun setFormattedAmount(amountValue: BigDecimal, shouldSetIncognito: Boolean) {
        val resultText = if (amountValue.isZero()) {
            ""
        } else {
            val formattedAmount = MarketDataFormatter.formatCurrencyValue(amountValue)
            savedAmount = formattedAmount
            formattedAmount
        }
        binding.transferMoneyForm.apply {
            if (shouldSetIncognito) {
                amount.setTextSafely(resultText, currencyTextWatcher)
            } else {
                amount.setText(resultText)
            }
        }
    }

    private fun bindViewModel() {
        actionSheetDelegate.observe(viewLifecycleOwner)
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                handleVisibilityState(it)
                handleTransferMoneyViewState(it)
                notifyIraContributionViewModel(it)
            }
        )
        iraDetailsViewModel.iraContributionDetails.observe(
            viewLifecycleOwner,
            Observer {
                if (it is TransferIraContributionDetailViewModel.ViewState.Error) {
                    iraRetrySnackBar = snackBarUtil.retrySnackbar(getString(R.string.could_not_load_ira_details)) { it.retry() }
                } else {
                    iraRetrySnackBar?.dismiss()
                }
            }
        )
        bindTransferCalendarViewStateModel()
        bindTransferMoneyPreviewState()
        bindMessagesFromPreviewPage()
        viewModel.updatePreSelectedAmountIfNeeded(args.amount)
        bindActionSheetViewModel()
    }

    private fun bindMessagesFromPreviewPage() {
        viewModel.snackbarMessage.observe(
            viewLifecycleOwner,
            Observer {
                /**
                 * For fragment inside viewPager with behaviour = BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
                 * onResume and onPause called in that way - onResume => onPause => onResume
                 * that's broke our snackbar with installAutoDismissOnPause
                 */
                // Use parent viewLifecycleOwner to show snackbar
                it.getNonConsumedContent()?.let {
                    snackbarUtilFactory.createSnackbarUtil(
                        { requireParentFragment().viewLifecycleOwner },
                        { view }
                    ).errorSnackbar()
                }
            }
        )
        viewModel.popupErrorMessage.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.let { error ->
                    findNavController().navigate(TransferMoneyContainerFragmentDirections.openPopupDialog(error.code, error.description))
                }
            }
        )
    }

    private fun bindTransferMoneyPreviewState() {
        viewModel.previewState.observe(
            viewLifecycleOwner,
            Observer { state ->
                when (state) {
                    is TransferMoneyPreviewButtonState.Disabled ->
                        binding.previewTransfer.isEnabled =
                            false
                    is TransferMoneyPreviewButtonState.Enabled -> {
                        binding.previewTransfer.isEnabled = true
                        binding.previewTransfer.setOnClickListener {
                            findNavController().navigate(
                                TransferMoneyContainerFragmentDirections.openTransferMoneyPreview(
                                    state.request
                                )
                            )
                            binding.transferMoneyForm.amount.text = null
                        }
                    }
                }
            }
        )
    }

    private fun bindTransferCalendarViewStateModel() {
        viewModel.dateSelectorEnableState.observe(
            viewLifecycleOwner,
            Observer {
                binding.transferMoneyForm.scheduleLayout.isEnabled = it
            }
        )
    }

    private fun bindActionSheetViewModel() {
        actionSheetViewModel.addExternalAccountNavSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { method ->
                    findNavController().navigate(
                        TransferMoneyContainerFragmentDirections.openAddExternalAccount(
                            verificationMethod = method
                        )
                    )
                }
            }
        )
    }

    private fun notifyIraContributionViewModel(viewState: TransferViewModel.ViewState) {
        if (viewState is TransferViewModel.ViewState.Success && viewState.selectedTo != null) {
            iraDetailsViewModel.setSelectedAccount(viewState.selectedTo)
        }
    }

    private fun handleTransferMoneyViewState(viewState: TransferViewModel.ViewState?) {
        when (viewState) {
            TransferViewModel.ViewState.Loading -> binding.transferPb.show()
            is TransferViewModel.ViewState.Success -> {
                binding.transferPb.hide()
                binding.transferMoneyForm.apply {
                    repeat.setText(viewState.selectedRepeat.getTitleId())
                    from.setText(viewState.selectedFrom?.title)
                    transferTo.setText(viewState.selectedTo?.title)
                    toLayout.isEnabled = viewState.isToSelectedEnabled()
                    schedule.setText(viewState.formattedSelectedScheduleDate())

                    val amountValue = viewState.amount
                    if (amountValue != null) {
                        setFormattedAmount(amountValue, true)
                    } else {
                        savedAmount ?: amount.text?.clear()
                    }
                }

                savedAmount = null
                bindTransferMoneyAccessibility(viewState)
            }
            TransferViewModel.ViewState.Error -> {
                binding.transferPb.hide()
                snackBarUtil.retrySnackbar { viewModel.refreshAccounts() }
            }
        }
    }

    private fun bindTransferMoneyAccessibility(viewState: TransferViewModel.ViewState.Success) {
        // Accessibility
        binding.transferMoneyForm.apply {
            fromLayout.bindTextInputButtonAccessibility(
                getString(
                    R.string.accessibility_label_from,
                    context?.accountNameContentDescription(viewState.selectedFrom?.title ?: "")
                )
            )
            toLayout.bindTextInputButtonAccessibility(
                getString(
                    R.string.accessibility_label_to,
                    context?.accountNameContentDescription(viewState.selectedTo?.title ?: "")
                )
            )
            scheduleLayout.bindTextInputButtonAccessibility(
                "${getString(R.string.schedule_date)} ${viewState.formattedSelectedScheduleDate() ?: ""}"
            )
            repeatLayout.bindTextInputButtonAccessibility(
                "${getString(R.string.transfer_repeat)} ${getString(viewState.selectedRepeat.getTitleId())}"
            )
        }
    }

    private fun handleVisibilityState(viewState: TransferViewModel.ViewState) {
        val contributionDetailVisibility =
            if (viewState is TransferViewModel.ViewState.Success && viewState.iraAccountSelected()) {
                View.VISIBLE
            } else {
                View.GONE
            }

        binding.transferMoneyForm.apply {
            iraContributionDetailsButton.visibility = contributionDetailVisibility
            contributionDetailsBorderTop.visibility = contributionDetailVisibility
            contributionDetailsBorderBottom.visibility = contributionDetailVisibility
        }

        binding.content.visibility = if (viewState is TransferViewModel.ViewState.Success) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    @Suppress("LongMethod") // nothing special
    private fun setupDropDownLists() {
        transferRepeatSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = TRANSFER_REPEAT_TAG,
                multiSelectEnabled = false,
                title = resources.getString(R.string.transfer_select_a_recurring_option),
                itemsData = viewModel.transferRepeatData,
                selectListener = { _, selected ->
                    viewModel.transferRepeatSelected(selected.value)
                }
            )
        )
        transferFromSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = TRANSFER_FROM_TAG,
                multiSelectEnabled = false,
                title = resources.getString(R.string.transfer_select_an_account),
                itemsData = viewModel.transferSource.from,
                selectListener = { _, selected ->
                    viewModel.transferFromSelected(selected.value)
                },
                actionItemSettings = DropDownMultiSelector.ActionItemSettings(
                    title = getString(R.string.transfer_add_an_external_account),
                    startIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_circle_add),
                    endIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_arrow_right)?.apply {
                        setTint(ContextCompat.getColor(requireContext(), R.color.purple))
                    },
                    action = {
                        actionSheetViewModel.showExternalVerificationMethodSelector()
                    }
                ),
                loadingState = viewModel.viewState.map<TransferViewModel.ViewState, Resource<TransferMoneyAccount>> {
                    if (it is TransferViewModel.ViewState.Loading) {
                        Resource.Loading()
                    } else {
                        Resource.Success()
                    }
                }
            )
        )
        transferToSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = TRANSFER_TO_TAG,
                multiSelectEnabled = false,
                title = resources.getString(R.string.transfer_select_an_account),
                itemsData = viewModel.transferSource.to,
                selectListener = { _, selected ->
                    viewModel.transferToSelected(selected.value)
                }
            )
        )
    }

    @Suppress("LongMethod")
    private fun setupClickListeners() {
        binding.transferMoneyForm.apply {
            schedule.setOnClickListener {
                getCalendarBuilder().show(this@TransferFragment)
            }
            repeat.setOnClickListener {
                transferRepeatSelector.open()
            }
            from.setOnClickListener {
                if (viewModel.hasFromAccounts()) {
                    transferFromSelector.open()
                } else {
                    snackBarUtil.snackbar(
                        getString(R.string.transfer_no_accounts),
                        Snackbar.LENGTH_SHORT
                    )
                }
            }
            transferTo.setOnClickListener {
                if (viewModel.hasToAccounts()) {
                    transferToSelector.open()
                } else {
                    snackBarUtil.snackbar(
                        getString(R.string.transfer_no_accounts),
                        Snackbar.LENGTH_SHORT
                    )
                }
            }
        }

        binding.addExternalAccount.setOnClickListener {
            actionSheetViewModel.showExternalVerificationMethodSelector()
        }
    }

    private fun setupAmountField() {
        binding.transferMoneyForm.apply {
            amount.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (scheduleLayout.isEnabled) {
                        amount.clearFocusSafely()
                        activity?.hideSoftKeyboard()
                        schedule.callOnClick()
                    }
                }
                false
            }
            currencyTextWatcher =
                CurrencyTextWatcher(
                    editText = amount,
                    initialAmountInDollar = viewModel.formattedAmountWithTwoDecimalPlace
                ) {
                    viewModel.amountEntered(it)
                }
            amount.setOnFocusChangeListener { _, hasFocus ->
                amountLayout.error =
                    if (!hasFocus && currencyTextWatcher.rawValue <= BigDecimal.ZERO && !screenIsEmpty()) {
                        resources.getString(R.string.format_error_desc)
                    } else {
                        EMPTY_STRING
                    }
            }
        }
    }

    private fun screenIsEmpty(): Boolean =
        viewModel.hasEmptySuccessState() && binding.transferMoneyForm.amount.text.isNullOrEmpty()

    private fun setupContributionDetailsButton() {
        binding.transferMoneyForm.iraContributionDetailsButton.setOnClickListener {
            findNavController().navigate(R.id.action_transfer_money_container_to_transfer_ira_contribution_details)
        }
    }

    private fun getCalendarBuilder(): TransferMoneyCalendarDialogFragment.Builder {
        return TransferMoneyCalendarDialogFragment.Builder(
            isOriginAccountExternal = viewModel.isFromAccountExternal,
            isDestinationAccountExternal = viewModel.isToAccountExternal,
            selectedDate = viewModel.selectedScheduleDate,
            selectedPage = viewModel.currentCalendarPage
        )
    }
}

private val TransferViewModel.formattedAmountWithTwoDecimalPlace: BigDecimal?
    get() = getAmount()?.setScale(2, ROUND_HALF_UP)
