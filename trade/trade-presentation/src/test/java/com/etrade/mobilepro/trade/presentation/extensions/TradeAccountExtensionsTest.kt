package com.etrade.mobilepro.trade.presentation.extensions

import android.content.res.Resources
import com.etrade.mobilepro.balance.api.AccountBalance
import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.presentation.AccountDataPoint
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.mockAccountBalance
import com.etrade.mobilepro.trade.presentation.mockTradeAccount
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertEquals
import org.junit.Test

class TradeAccountExtensionsTest {

    private val resources: Resources = mock<Resources>().apply {
        whenever(getString(R.string.trade_label_account_value)).thenReturn("trade_label_account_value")
        whenever(getString(R.string.trade_label_available_excess_equity)).thenReturn("trade_label_available_excess_equity")
        whenever(getString(R.string.trade_label_cash_available_for_investment)).thenReturn("trade_label_cash_available_for_investment")
        whenever(getString(R.string.trade_label_intraday_marginable)).thenReturn("trade_label_intraday_marginable")
        whenever(getString(R.string.trade_label_intraday_non_marginable)).thenReturn("trade_label_intraday_non_marginable")
        whenever(getString(R.string.trade_label_margin_purchasing_power)).thenReturn("trade_label_margin_purchasing_power")
        whenever(getString(R.string.trade_label_non_margin_purchasing_power)).thenReturn("trade_label_non_margin_purchasing_power")
        whenever(getString(R.string.trade_label_overnight_marginable)).thenReturn("trade_label_overnight_marginable")
        whenever(getString(R.string.trade_label_overnight_non_marginable)).thenReturn("trade_label_overnight_non_marginable")
    }

    @Test
    fun `check data points for margin account`() {
        checkDataPoints(
            mockTradeAccount(mode = AccountMode.MARGIN),
            mockAccountBalance(isPdt = false),
            listOf(
                AccountDataPoint("trade_label_account_value", "--"),
                AccountDataPoint("trade_label_margin_purchasing_power", "--"),
                AccountDataPoint("trade_label_non_margin_purchasing_power", "--")
            )
        )
    }

    @Test
    fun `check data points for margin account and pattern day trader`() {
        checkDataPoints(
            mockTradeAccount(mode = AccountMode.MARGIN),
            mockAccountBalance(isPdt = true),
            listOf(
                AccountDataPoint("trade_label_account_value", "--"),
                AccountDataPoint("trade_label_intraday_marginable", "--"),
                AccountDataPoint("trade_label_intraday_non_marginable", "--"),
                AccountDataPoint("trade_label_overnight_marginable", "--"),
                AccountDataPoint("trade_label_overnight_non_marginable", "--")
            )
        )
    }

    @Test
    fun `check data points for portfolio margin account`() {
        checkDataPoints(
            mockTradeAccount(mode = AccountMode.PORTFOLIO_MARGIN),
            mockAccountBalance(isPdt = false),
            listOf(
                AccountDataPoint("trade_label_account_value", "--"),
                AccountDataPoint("trade_label_available_excess_equity", "--")
            )
        )
    }

    @Test
    fun `check data points for portfolio margin account and pattern day trader`() {
        checkDataPoints(
            mockTradeAccount(mode = AccountMode.PORTFOLIO_MARGIN),
            mockAccountBalance(isPdt = true),
            listOf(
                AccountDataPoint("trade_label_account_value", "--"),
                AccountDataPoint("trade_label_available_excess_equity", "--")
            )
        )
    }

    @Test
    fun `check data points for other account`() {
        checkDataPoints(
            mockTradeAccount(mode = AccountMode.UNKNOWN),
            mockAccountBalance(isPdt = false),
            listOf(
                AccountDataPoint("trade_label_account_value", "--"),
                AccountDataPoint("trade_label_cash_available_for_investment", "--")
            )
        )
    }

    @Test
    fun `check data points for other account and pattern day trader`() {
        checkDataPoints(
            mockTradeAccount(mode = AccountMode.UNKNOWN),
            mockAccountBalance(isPdt = true),
            listOf(
                AccountDataPoint("trade_label_account_value", "--"),
                AccountDataPoint("trade_label_cash_available_for_investment", "--")
            )
        )
    }

    private fun checkDataPoints(account: TradeAccount, balance: AccountBalance, expected: List<AccountDataPoint>) {
        val actual = balance.toDataPoints(resources, account.accountMode)
        assertEquals(expected.size, actual.size)
        expected.forEachIndexed { index, expectedDataPoint ->
            val actualDataPoint = actual[index]
            assertEquals(expectedDataPoint.label, actualDataPoint.label)
            assertEquals(expectedDataPoint.value, actualDataPoint.value)
        }
    }

    @Test
    fun `check drop down option`() {
        val actual = mockTradeAccount("id", "name").toDropDownOption()
        assertEquals("id", actual.id)
        assertEquals("name", actual.name)
    }
}
