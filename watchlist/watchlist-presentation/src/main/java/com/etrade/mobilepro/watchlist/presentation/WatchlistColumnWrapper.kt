package com.etrade.mobilepro.watchlist.presentation

import android.content.res.Resources
import com.etrade.mobilepro.editlist.EditListItem
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.WatchlistColumn

/**
 * Wrapper [WatchlistColumn] to make it [EditListItem].
 *
 * @param column a column to wrap
 * @param resources provide android resources
 */
class WatchlistColumnWrapper(
    val column: WatchlistColumn,
    private val resources: Resources
) : EditListItem {

    override val displayText: CharSequence
        get() = resources.getString(column.reorderTextRes)

    override val isChecked: Boolean = true

    override val contentDescription: String
        get() = resources.getString(column.contentDescriptionRes)
}
