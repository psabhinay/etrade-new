package com.etrade.mobilepro.quote.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.dto.MutualFundOwnedDetailRequestDto
import com.etrade.mobilepro.quote.dto.MutualFundsTradeQuoteRequestDto
import com.etrade.mobilepro.quote.dto.MutualFundsTradeQuoteRequestValueDto
import com.etrade.mobilepro.quote.mapper.MutualFundsTradeQuoteMapper
import com.etrade.mobilepro.quote.restapi.MutualFundsTradeQuoteApiClient
import com.etrade.mobilepro.quoteapi.InvalidMutualFunds
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesRepo
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesResult
import com.etrade.mobilepro.quoteapi.NonSupportedFunds
import com.etrade.mobilepro.quoteapi.QuoteContentLoadingFailed
import javax.inject.Inject

private const val NON_MUTUAL_FUND_CODE = "53"
private const val NON_SUPPORTED_FUND_CODE = "9004"

/**
 * Performs a request for a mutual fund quote.
 * NOTE: Fetches Mutual fund details only and for other symbols throws error code - 53
 */
class DefaultMutualFundsTradeQuotesRepo @Inject constructor(
    private val apiClient: MutualFundsTradeQuoteApiClient,
    private val mapper: MutualFundsTradeQuoteMapper
) : MutualFundsTradeQuotesRepo {
    override suspend fun loadFundsQuote(accountId: String, symbol: String): ETResult<MutualFundsTradeQuotesResult> = runCatchingET {
        val requestJson = MutualFundsTradeQuoteRequestValueDto(
            MutualFundsTradeQuoteRequestDto(
                accountId = accountId,
                symbol = symbol
            )
        )

        val responseData = apiClient.getFundDetails(requestJson).data
        if (responseData?.hasNoMessages() == true) {
            mapper.map(responseData)
        } else {
            when (responseData?.getFirstMessageCode()) {
                NON_MUTUAL_FUND_CODE -> InvalidMutualFunds
                NON_SUPPORTED_FUND_CODE -> NonSupportedFunds(responseData.getFirstMessageText() ?: "Not supported")
                else -> {
                    throw QuoteContentLoadingFailed(responseData?.getFirstMessageText() ?: "Empty Payload")
                }
            }
        }
    }

    override suspend fun loadOwnedFundsQuote(accountId: String, symbol: String, instrumentType: InstrumentType): ETResult<MutualFundsTradeQuotesResult> =
        runCatchingET {
            val responseData = apiClient.mutualFundEntryDetail(
                MutualFundOwnedDetailRequestDto.toMutualFundSellRequest(accountId = accountId, symbol = symbol, typeCode = instrumentType.typeCode)
            ).data

            if (responseData?.hasNoMessages() == true) {
                mapper.map(responseData)
            } else {
                when (responseData?.getFirstMessageCode()) {
                    NON_MUTUAL_FUND_CODE -> InvalidMutualFunds
                    NON_SUPPORTED_FUND_CODE -> NonSupportedFunds(responseData.getFirstMessageText() ?: "Not supported")
                    else -> {
                        throw QuoteContentLoadingFailed(responseData?.getFirstMessageText() ?: "Empty Payload")
                    }
                }
            }
        }
}
