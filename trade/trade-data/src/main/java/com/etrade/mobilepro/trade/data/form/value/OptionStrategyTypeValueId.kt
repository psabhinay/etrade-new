package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.trade.form.api.value.StrategyTypeValueId
import com.etrade.mobilepro.util.MutualConverter

object OptionStrategyTypeValueId : StrategyTypeValueId {

    override val butterfly: String = StrategyType.BUTTERFLY.typeCode
    override val buyWrite: String = StrategyType.BUY_WRITE.typeCode
    override val calendar: String = StrategyType.CALENDAR.typeCode
    override val call: String = StrategyType.CALL.typeCode
    override val callSpread: String = StrategyType.CALL_SPREAD.typeCode
    override val collars: String = StrategyType.COLLARS.typeCode
    override val combo: String = StrategyType.COMBO.typeCode
    override val condor: String = StrategyType.CONDOR.typeCode
    override val custom: String = StrategyType.CUSTOM.typeCode
    override val diagonal: String = StrategyType.DIAGONAL.typeCode
    override val ironButterfly: String = StrategyType.IRON_BUTTERFLY.typeCode
    override val ironCondor: String = StrategyType.IRON_CONDOR.typeCode
    override val marriedPut: String = StrategyType.MARRIED_PUT.typeCode
    override val put: String = StrategyType.PUT.typeCode
    override val putSpread: String = StrategyType.PUT_SPREAD.typeCode
    override val stock: String = StrategyType.STOCK.typeCode
    override val straddle: String = StrategyType.STRADDLE.typeCode
    override val strangle: String = StrategyType.STRANGLE.typeCode

    override val converter: MutualConverter<String, StrategyType> by lazy { createConverter() }
}
