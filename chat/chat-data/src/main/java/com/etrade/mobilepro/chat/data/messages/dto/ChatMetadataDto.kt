package com.etrade.mobilepro.chat.data.messages.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class ChatMetadataDto(
    @Json(name = "affinity")
    var affinity: String? = null,
    @Json(name = "buttonId")
    var buttonId: String? = null,
    @Json(name = "buttonName")
    var buttonName: String? = null,
    @Json(name = "sessionKey")
    var sessionKey: String? = null,
    @Json(name = "messagesAck")
    var messagesAck: Int? = null
) : Serializable
