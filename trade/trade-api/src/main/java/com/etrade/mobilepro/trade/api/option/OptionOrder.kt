package com.etrade.mobilepro.trade.api.option

import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.WithCostOrder
import com.etrade.mobilepro.trade.api.WithPriceOrder

interface OptionOrder : Order, WithPriceOrder, WithCostOrder {
    val strategy: StrategyType
}
