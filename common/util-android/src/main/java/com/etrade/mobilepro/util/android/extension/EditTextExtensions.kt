package com.etrade.mobilepro.util.android.extension

import android.text.TextWatcher
import android.widget.EditText

fun EditText.setTextSafely(text: String?, watcher: TextWatcher) {
    removeTextChangedListener(watcher)
    setText(text)
    setSelection(text?.length ?: 0)
    addTextChangedListener(watcher)
}
