-dontwarn javax.xml.stream.**

-keep class org.simpleframework.xml.** { *; }

# Retrofit uses reflection to grab the type adapter
# for Simple XML models wrapped by reactivex classes
-keep @org.simpleframework.xml.Root class *

-keepclassmembers class * {
    @org.simpleframework.xml.Element *;
}