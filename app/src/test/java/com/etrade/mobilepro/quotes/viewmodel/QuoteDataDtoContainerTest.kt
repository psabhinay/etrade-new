package com.etrade.mobilepro.quotes.viewmodel

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.quote.dto.BaseQuoteViewType
import com.etrade.mobilepro.quote.dto.FundQuoteResponseDto
import com.etrade.mobilepro.quote.dto.QuoteDataContainer
import com.etrade.mobilepro.quote.dto.QuoteResponse
import com.etrade.mobilepro.quote.dto.QuoteStockDto
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import junit.framework.TestCase.assertNotNull
import org.junit.Test

class QuoteDataDtoContainerTest {

    @Test
    fun `test correct quote dto types`() {
        assertNotNull(getMockMFQuotes())
        assertNotNull(getMockQuotes())
    }

    private fun getMockQuotes(): QuoteStockDto? {
        return this::class.getObjectFromJson<ServerResponseDto<QuoteDataContainer>>(
            "/responses/quotes.json",
            ServerResponseDto::class.java,
            QuoteDataContainer::class.java,
            listOf(getPolyMorphicAdapterFactory())
        ).data?.quotes?.firstOrNull() as? QuoteStockDto
    }

    private fun getMockMFQuotes(): FundQuoteResponseDto? {
        return this::class.getObjectFromJson<ServerResponseDto<QuoteDataContainer>>(
            "/responses/quotes_mutual_funds.json",
            ServerResponseDto::class.java,
            QuoteDataContainer::class.java,
            listOf(getPolyMorphicAdapterFactory())
        ).data?.quotes?.firstOrNull() as? FundQuoteResponseDto
    }

    private fun getPolyMorphicAdapterFactory(): PolymorphicJsonAdapterFactory<QuoteResponse> {
        var factory = PolymorphicJsonAdapterFactory.of(QuoteResponse::class.java, "typeCode")
        for (enum in BaseQuoteViewType.values()) {
            factory = factory.withSubtype(enum.dtoClass, enum.dtoName)
        }
        return factory
    }
}
