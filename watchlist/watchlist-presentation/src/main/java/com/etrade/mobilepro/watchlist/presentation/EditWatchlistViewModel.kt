package com.etrade.mobilepro.watchlist.presentation

import android.content.Intent
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.tracking.SCREEN_TITLE_EDIT_WATCH_LISTS
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.watchlist.presentation.EditWatchlistActivity.Companion.EXTRA_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

/**
 * Implementation of [EditListViewModel] for editing a watchlist.
 *
 * Before usage you may want to call [prepareViewModel] method.
 */
class EditWatchlistViewModel @Inject constructor(
    private val watchlistRepo: WatchlistRepo
) : EditListViewModel<WatchlistEntryWrapper>(), ScreenViewModel {

    private val disposable = CompositeDisposable()

    private var watchlistId: String = DEVICE_WATCHLIST_ID

    override val screenName: String? = SCREEN_TITLE_EDIT_WATCH_LISTS

    override fun onFetchItems(viewState: MutableLiveData<ViewState>) {
        viewState.value = ViewState.InProgress
        disposable.clear()
        disposable.addAll(
            watchlistRepo.getWatchlistEntries(watchlistId).subscribeBy(
                onSuccess = { items ->
                    provideItems(items.map { entry -> WatchlistEntryWrapper(entry) })
                    viewState.postValue(ViewState.Idle)
                },
                onError = {
                    viewState.postValue(ViewState.Error)
                }
            )
        )
    }

    override fun onApplyChanges(
        items: List<WatchlistEntryWrapper>,
        removedItems: List<WatchlistEntryWrapper>,
        viewState: MutableLiveData<ViewState>
    ) {
        viewState.value = ViewState.InProgress
        disposable.clear()
        disposable.add(
            watchlistRepo.saveWatchList(watchlistId, items.toWatchlistEntries(), removedItems.toWatchlistEntries()).subscribeBy(
                onSuccess = { isSuccessful ->
                    viewState.postValue(if (isSuccessful) ViewState.Success() else ViewState.Error)
                },
                onError = {
                    viewState.postValue(ViewState.Error)
                }
            )
        )
    }

    override fun onPerformListAction(viewState: MutableLiveData<ViewState>) {
        viewState.value = ViewState.InProgress
        disposable.clear()
        disposable.add(
            watchlistRepo.deleteWatchList(watchlistId).subscribeBy(
                onSuccess = { isSuccessful ->
                    viewState.postValue(
                        if (isSuccessful) {
                            ViewState.Success(
                                intent = Intent().apply {
                                    putExtra(EXTRA_WATCHLIST_ID, watchlistId)
                                }
                            )
                        } else {
                            ViewState.Error
                        }
                    )
                },
                onError = {
                    viewState.postValue(ViewState.Error)
                }
            )
        )
    }

    override fun onCleared() {
        disposable.clear()
    }

    /**
     * Prepares this view model before use.
     *
     * @param watchlistId watchlist ID
     */
    fun prepareViewModel(watchlistId: String) {
        this.watchlistId = watchlistId
    }

    private fun List<WatchlistEntryWrapper>.toWatchlistEntries(): List<WatchlistEntry> = map { it.entry }
}
