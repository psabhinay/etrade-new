package com.etrade.mobilepro.common

import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.goneUnless

// Market Status Event can be shared across multiple overlays.
fun MarketTradeStatusDelegate.observeWithTextView(textView: TextView?, lifecycleOwner: LifecycleOwner) {
    marketStatus.observe(
        lifecycleOwner,
        Observer {
            textView?.goneUnless(it.peekContent().isMarketHalted)
        }
    )
}

// Used by top overlay page such as Quotes.
fun MarketTradeStatusDelegate.consumeWithTextView(textView: TextView?, lifecycleOwner: LifecycleOwner) {
    marketStatus.observe(
        lifecycleOwner,
        Observer {
            it.consume { status ->
                textView?.goneUnless(status.isMarketHalted)
            }
        }
    )
}

fun MarketTradeStatusDelegate.removeObservers(owner: LifecycleOwner) {
    marketStatus.removeObservers(owner)
}
