package com.etrade.mobilepro.screener.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

const val OPERATOR_EQUALS = 0
const val OPERATOR_GREATER_THAN_OR_EQUAL_TO = 1
const val OPERATOR_GREATER_THAN = 2
const val OPERATOR_LIKE = 4
const val OPERATOR_LESS_THAN = 5
const val OPERATOR_NOT_EQUAL_TO = 6

@JsonClass(generateAdapter = true)
data class ScreenerParams(
    @Json(name = "Arguments")
    val arguments: List<Argument>,
    @Json(name = "FirstRow")
    val firstRow: Int,
    @Json(name = "RowCount")
    val rowCount: Int,
    @Json(name = "ResultFields")
    val resultFields: List<String>,
    @Json(name = "SortArguments")
    val sortArguments: List<SortArgument>
)

data class PartialScreenerParams(
    val arguments: List<Argument> = listOf()
)

@JsonClass(generateAdapter = true)
data class Argument(
    @Json(name = "Clauses")
    val clauses: List<Clause>,
    @Json(name = "Field")
    val field: String,
    @Json(name = "ClausesGroups")
    val clausesGroups: List<Clause>? = null // default value for part of results number requests
)

@JsonClass(generateAdapter = true)
data class Clause(
    @Json(name = "Operator")
    val operator: Int? = null,
    @Json(name = "Values")
    val values: List<String>
)

@JsonClass(generateAdapter = true)
data class SortArgument(
    @Json(name = "Field")
    val field: String,
    @Json(name = "Direction")
    val direction: String
)
