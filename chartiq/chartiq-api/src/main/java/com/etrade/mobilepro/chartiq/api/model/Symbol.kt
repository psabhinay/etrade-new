package com.etrade.mobilepro.chartiq.api.model

import com.etrade.mobilepro.instrument.InstrumentType

data class Symbol(
    val ticker: String,
    val instrumentType: InstrumentType
)
