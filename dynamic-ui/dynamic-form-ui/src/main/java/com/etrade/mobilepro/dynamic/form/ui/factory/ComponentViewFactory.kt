package com.etrade.mobilepro.dynamic.form.ui.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.api.Component
import com.etrade.mobilepro.dynamic.form.ui.ComponentView

interface ComponentViewFactory<C : Component, out V : ComponentView<C>> {
    fun createView(context: Context): V
}
