package com.etrade.mobilepro.orders.data.dto

import com.etrade.mobilepro.orders.api.OrderStatus

fun parseOrderStatusForStockPlan(status: String?): OrderStatus {
    return when (status) {
        "C", "Cancelled" -> OrderStatus.CANCELLED
        "E", "Executed" -> OrderStatus.EXECUTED
        "O", "Open" -> OrderStatus.OPEN
        "U", "Open & Unsettled" -> OrderStatus.OPEN_AND_UNSETTLED
        "S", "Settled" -> OrderStatus.SETTLED
        else -> OrderStatus.UNKNOWN
    }
}
