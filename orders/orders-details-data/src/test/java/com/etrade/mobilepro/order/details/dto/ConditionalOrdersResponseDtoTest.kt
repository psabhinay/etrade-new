package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.mapper.OrderDetailDescriptionMapper
import com.etrade.mobilepro.order.details.mapper.OrderDetailsMapper
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.testutil.getObjectFromJson
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class ConditionalOrdersResponseDtoTest {
    @Test
    fun `test expired conditional orders mapper`() {
        val response = getExpiredOrderDetailsResponse()
        val orderDetailsDto = response.data?.viewOrderEvents
        orderDetailsDto?.apply {

            val orders = orderDetails.orders
            assertEquals(2, orders.size)

            val orderDetailsItems = OrderDetailsMapper(OrderDetailDescriptionMapper()::map).map(this)
            assertEquals(2, orderDetailsItems.size)
            assertTrue(orderDetailsItems.first() is BaseOrderDetailsItem.OrderDetailsItem)

            val firstOrderDetail = orderDetailsItems.first() as BaseOrderDetailsItem.OrderDetailsItem
            assertEquals(BigDecimal.valueOf(15016.23), firstOrderDetail.estimatedTotalCost)
            assertEquals(BigDecimal.valueOf(4.95), firstOrderDetail.commission)
            assertEquals(OrderStatus.EXPIRED, firstOrderDetail.orderStatus)
            assertEquals(BigDecimal.valueOf(10), firstOrderDetail.priceExecuted)
            assertNull(firstOrderDetail.limitPrice)
            assertEquals("10/12", firstOrderDetail.quantity)
            assertEquals(PriceType.MARKET, firstOrderDetail.priceType)
            assertEquals(OrderType.EQUITY, firstOrderDetail.orderType)
            assertEquals(OrderTerm.GOOD_FOR_DAY, firstOrderDetail.term)
            assertNull(firstOrderDetail.strategyType)
            assertEquals(1, firstOrderDetail.orderDescriptions.size)
            val orderDescFirst = firstOrderDetail.orderDescriptions.getOrNull(0)
            orderDescFirst?.apply {
                assertEquals("10/12", displayQuantity)
                assertEquals("10", fillQuantity)
                assertEquals("12", quantity)
                assertEquals(TransactionType.BUY_OPEN, transactionType)
                assertEquals("GOOG", displaySymbol)
                assertEquals("GOOG", originalSymbol)
                assertEquals("", underlyingProductId)
            }

            assertTrue(orderDetailsItems[1] is BaseOrderDetailsItem.OrderDetailsItem)
            val secondOrderDetail = orderDetailsItems[1] as BaseOrderDetailsItem.OrderDetailsItem
            assertEquals(BigDecimal.valueOf(2499.51), secondOrderDetail.estimatedTotalCost)
            assertEquals(BigDecimal.valueOf(4.95), secondOrderDetail.commission)
            assertEquals(OrderStatus.EXPIRED, secondOrderDetail.orderStatus)
            assertEquals(BigDecimal(0), secondOrderDetail.priceExecuted)
            assertNull(secondOrderDetail.limitPrice)
            assertEquals("12", secondOrderDetail.quantity)
            assertEquals(PriceType.MARKET, secondOrderDetail.priceType)
            assertEquals(OrderType.EQUITY, secondOrderDetail.orderType)
            assertEquals(OrderTerm.GOOD_FOR_DAY, secondOrderDetail.term)
            assertNull(secondOrderDetail.strategyType)
            assertEquals(1, secondOrderDetail.orderDescriptions.size)
            val orderDescSecond = secondOrderDetail.orderDescriptions.getOrNull(0)
            orderDescSecond?.apply {
                assertEquals("12", displayQuantity)
                assertEquals("0", fillQuantity)
                assertEquals("12", quantity)
                assertEquals(TransactionType.BUY_OPEN, transactionType)
                assertEquals("AAPL", displaySymbol)
                assertEquals("AAPL", originalSymbol)
                assertEquals("", underlyingProductId)
            }
            secondOrderDetail.tradeLegs.apply {
                assertEquals(1, size)
                val firstLeg = first()
                assertEquals("AAPL", firstLeg.symbol)
                assertEquals("AAPL", firstLeg.displaySymbol)
                assertEquals(TransactionType.BUY_OPEN, firstLeg.transactionType)
                assertEquals(12, firstLeg.quantity)
            }
        }
    }

    @Test
    fun `test conditional orders mapper`() {
        val response = getSavedOrderDetailsResponse()
        val orderDetailsDto = response.data?.preparedOrderDetails
        orderDetailsDto?.apply {

            val orders = orderDetails.orders
            assertEquals(3, orders.size)

            val orderDetailsItems = OrderDetailsMapper(OrderDetailDescriptionMapper()::map).map(this)
            assertEquals(3, orderDetailsItems.size)

            assertTrue(orderDetailsItems.first() is BaseOrderDetailsItem.OrderDetailsItem)
            val firstOrderDetail = orderDetailsItems.first() as BaseOrderDetailsItem.OrderDetailsItem
            assertEquals(BigDecimal(0), firstOrderDetail.estimatedTotalCost)
            assertEquals(BigDecimal(0), firstOrderDetail.commission)
            assertEquals(OrderStatus.SAVED, firstOrderDetail.orderStatus)
            assertEquals(BigDecimal(0), firstOrderDetail.priceExecuted)
            assertNull(firstOrderDetail.limitPrice)
            assertEquals("4", firstOrderDetail.quantity)
            assertEquals(PriceType.MARKET, firstOrderDetail.priceType)
            assertEquals(OrderType.SIMPLE_OPTION, firstOrderDetail.orderType)
            assertEquals(OrderTerm.GOOD_FOR_DAY, firstOrderDetail.term)
            assertNull(firstOrderDetail.strategyType)
            assertEquals(1, firstOrderDetail.orderDescriptions.size)
            val orderDescFirst = firstOrderDetail.orderDescriptions.getOrNull(0)
            orderDescFirst?.apply {
                assertEquals("4", displayQuantity)
                assertEquals("0", fillQuantity)
                assertEquals("4", quantity)
                assertEquals(TransactionType.BUY_OPEN_OPT, transactionType)
                assertEquals("OEX Mar 22 '19 \$1090 Call", displaySymbol)
                assertEquals("OEX---190322C01090000", originalSymbol)
                assertEquals("OEX", underlyingProductId)
            }

            assertTrue(orderDetailsItems[1] is BaseOrderDetailsItem.OrderDetailsItem)
            val secondOrderDetail = orderDetailsItems[1] as BaseOrderDetailsItem.OrderDetailsItem
            assertEquals(BigDecimal(0), secondOrderDetail.estimatedTotalCost)
            assertEquals(BigDecimal(0), secondOrderDetail.commission)
            assertEquals(OrderStatus.SAVED, secondOrderDetail.orderStatus)
            assertEquals(BigDecimal(0), secondOrderDetail.priceExecuted)
            assertNull(secondOrderDetail.limitPrice)
            assertEquals("4", secondOrderDetail.quantity)
            assertEquals(PriceType.MARKET, secondOrderDetail.priceType)
            assertEquals(OrderType.SIMPLE_OPTION, secondOrderDetail.orderType)
            assertEquals(OrderTerm.GOOD_FOR_DAY, secondOrderDetail.term)
            assertNull(secondOrderDetail.strategyType)
            assertEquals(1, secondOrderDetail.orderDescriptions.size)
            val orderDescSecond = secondOrderDetail.orderDescriptions.getOrNull(0)
            orderDescSecond?.apply {
                assertEquals("4", displayQuantity)
                assertEquals("0", fillQuantity)
                assertEquals("4", quantity)
                assertEquals(TransactionType.BUY_OPEN_OPT, transactionType)
                assertEquals("NDXP Mar 20 '19 \$4800 Put", displaySymbol)
                assertEquals("NDXP--190320P04800000", originalSymbol)
                assertEquals("NDX", underlyingProductId)
            }

            assertTrue(orderDetailsItems[2] is BaseOrderDetailsItem.OrderDetailsItem)
            val thirdOrderDetail = orderDetailsItems[2] as BaseOrderDetailsItem.OrderDetailsItem
            assertEquals(BigDecimal(0), thirdOrderDetail.estimatedTotalCost)
            assertEquals(BigDecimal(0), thirdOrderDetail.commission)
            assertEquals(OrderStatus.SAVED, thirdOrderDetail.orderStatus)
            assertEquals(BigDecimal(0), thirdOrderDetail.priceExecuted)
            assertNull(thirdOrderDetail.limitPrice)
            assertEquals("4", thirdOrderDetail.quantity)
            assertEquals(PriceType.TRAILING_STOP_DOLLAR, thirdOrderDetail.priceType)
            assertEquals(OrderType.ADVANCE_EQUITY, thirdOrderDetail.orderType)
            assertEquals(OrderTerm.GOOD_FOR_DAY, thirdOrderDetail.term)
            assertNull(thirdOrderDetail.strategyType)
            assertEquals(1, thirdOrderDetail.orderDescriptions.size)
            val orderDescThird = thirdOrderDetail.orderDescriptions.getOrNull(0)
            orderDescThird?.apply {
                assertEquals("4", displayQuantity)
                assertEquals("0", fillQuantity)
                assertEquals("4", quantity)
                assertEquals(TransactionType.BUY_OPEN, transactionType)
                assertEquals("AMZN", displaySymbol)
                assertEquals("AMZN", originalSymbol)
                assertEquals("", underlyingProductId)
            }
        }
    }

    private fun getSavedOrderDetailsResponse(): ServerResponseDto<OrderDetailsResponseDto> =
        ConditionalOrdersResponseDtoTest::class.getObjectFromJson(
            "saved_conditional_order_details.json",
            ServerResponseDto::class.java,
            OrderDetailsResponseDto::class.java
        )

    private fun getExpiredOrderDetailsResponse(): ServerResponseDto<OrderDetailsResponseDto> =
        ConditionalOrdersResponseDtoTest::class.getObjectFromJson(
            "expired_conditional_order_detail.json",
            ServerResponseDto::class.java,
            OrderDetailsResponseDto::class.java
        )
}
