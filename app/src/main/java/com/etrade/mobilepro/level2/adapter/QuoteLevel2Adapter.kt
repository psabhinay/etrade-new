package com.etrade.mobilepro.level2.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.databinding.QuoteLevel2RowBinding
import com.etrade.mobilepro.level2.QuoteRowData
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.etrade.mobilepro.util.android.extension.viewBinding

class QuoteLevel2Adapter : RecyclerView.Adapter<QuoteLevel2ViewHolder>() {

    private var quoteRowDataList = listOf<QuoteRowData>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuoteLevel2ViewHolder {
        return QuoteLevel2ViewHolder(parent.viewBinding(QuoteLevel2RowBinding::inflate))
    }

    override fun getItemCount() = quoteRowDataList.size

    override fun onBindViewHolder(holder: QuoteLevel2ViewHolder, position: Int) {
        holder.bindTo(quoteRowDataList[position])
    }

    override fun getItemId(position: Int): Long = quoteRowDataList[position].hashCode().toLong()

    fun submitQuotes(quotes: List<QuoteRowData>) {
        dispatchUpdates(quoteRowDataList, quotes, QuoteDiffCallback(quoteRowDataList, quotes))
        quoteRowDataList = quotes
    }
}
