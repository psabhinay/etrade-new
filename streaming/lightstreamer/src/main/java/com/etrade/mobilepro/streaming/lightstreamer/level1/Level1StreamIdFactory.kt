package com.etrade.mobilepro.streaming.lightstreamer.level1

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.StreamIdFactory
import javax.inject.Inject

/**
 * LightStreamer implementation of [StreamIdFactory] for Level 1 quotes.
 */
class Level1StreamIdFactory @Inject constructor(private val parameters: Parameters) : StreamIdFactory<Any> {

    override fun createStreamId(symbol: String, instrumentType: InstrumentType, payload: Any?): String {
        val prefix = if (instrumentType.isOption) { "OPTION" } else { "STOCK" }
        val suffix = if (parameters.isRealtimeQuotes) { "REALTIME" } else { "DELAYED" }
        return "$prefix:$symbol:$suffix"
    }

    /**
     * Parameters for creation of stream ids of LightStreamer Level 2 quotes.
     */
    interface Parameters {
        val isRealtimeQuotes: Boolean
    }
}
