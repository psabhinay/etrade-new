package com.etrade.mobilepro.quoteapi

import androidx.lifecycle.LiveData

interface QuoteBidAskItem {
    val bidPrice: LiveData<QuotePriceWidgetItem>
    val askPrice: LiveData<QuotePriceWidgetItem>
}
