package com.etrade.mobilepro.inputvalueview

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.os.Parcelable
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.inputvalueview.databinding.ViewSeekerInputValueBinding
import com.etrade.mobilepro.util.android.extension.hideSoftKeyboard
import com.etrade.mobilepro.util.android.extension.inflater
import com.etrade.mobilepro.util.android.extension.showSoftKeyboard
import com.etrade.mobilepro.util.android.extension.updateValue
import kotlinx.parcelize.Parcelize

private const val KEYBOARD_DELAY_MS = 300L

class SeekbarInputValueView : LinearLayout {
    constructor(ctx: Context) : super(ctx)
    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)
    private val kbShowingHandler = Handler(Looper.getMainLooper())
    private var _binding: ViewSeekerInputValueBinding? = ViewSeekerInputValueBinding.inflate(inflater, this, true)
    val binding: ViewSeekerInputValueBinding
        get() = _binding!!

    private var prevValue = 0

    private val _lastCompleteValue = MutableLiveData<String>()
    private val inputFieldManager = InputFieldManager(_lastCompleteValue)
    private var pendingActionOnAttach: (() -> Unit)? = null

    val lastCompleteValue: LiveData<String>
        get() = _lastCompleteValue

    @SuppressWarnings("LongParameterList")
    fun setupView(
        dependencies: InputFieldManager.Dependencies,
        rawContentData: MutableLiveData<String>,
        maxValue: Int = 100,
        validatedData: MutableLiveData<InputFieldManager.Value> = MutableLiveData(),
        onDoneTappedListener: ((TextView) -> Unit)? = null
    ) {
        prevValue = rawContentData.value?.toIntOrNull() ?: 0
        inputFieldManager.setupView(binding.editText, null, dependencies, rawContentData, validatedData)
        inputFieldManager.rawTextChangeListener = { rawString ->
            val rawValue = rawString.replace("%", "").toIntOrNull() ?: prevValue
            if (rawValue > maxValue || rawValue < 0) {
                inputFieldManager.updateTextValueWithoutNotification(prevValue.toString())
            } else {
                val progress = getSeekerProgress(rawString, maxValue)
                binding.seeker.progress = progress
                prevValue = progress
                inputFieldManager.updateTextValueWithoutNotification(progress.toString())
            }
        }
        binding.seeker.max = maxValue
        val progress = getSeekerProgress(rawContentData.value, maxValue)
        binding.seeker.progress = progress
        binding.editText.text.updateValue(progress.toString())
        formatContents()
        setupSeekerListeners(rawContentData)
        setupEditTextListeners(onDoneTappedListener)
    }

    fun getValue(): InputFieldManager.Value = inputFieldManager.getValue()

    fun formatContents() {
        inputFieldManager.formatContents()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        pendingActionOnAttach?.invoke()
        pendingActionOnAttach = null
    }

    override fun onDetachedFromWindow() {
        kbShowingHandler.removeCallbacksAndMessages(null)
        super.onDetachedFromWindow()
        _binding = null
    }

    override fun onSaveInstanceState(): Parcelable? = State(
        parentState = super.onSaveInstanceState(),
        isFocused = binding.editText.hasFocus()
    )

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is State) {
            super.onRestoreInstanceState(state.parentState)
            if (state.isFocused) {
                pendingActionOnAttach = {
                    binding.editText.requestFocus()
                    kbShowingHandler.postDelayed(
                        {
                            binding.editText.showSoftKeyboard(forced = true)
                        },
                        KEYBOARD_DELAY_MS
                    )
                }
            }
        } else {
            super.onRestoreInstanceState(state)
        }
    }
    private fun getSeekerProgress(rawString: String?, maxValue: Int): Int {
        val value = rawString?.replace("%", "")?.toIntOrNull() ?: 0
        return when {
            value <= 0 -> 0
            value >= maxValue -> maxValue
            else -> value
        }
    }

    private fun setupSeekerListeners(rawContentData: MutableLiveData<String>) {
        binding.seeker.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    rawContentData.value = progress.toString()
                    _lastCompleteValue.value = progress.toString()
                    formatContents()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                binding.editText.clearFocus()
                seekBar?.hideSoftKeyboard()
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                // nop
            }
        })
    }

    private fun setupEditTextListeners(onDoneTappedListener: ((TextView) -> Unit)?) {
        binding.editText.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE || event.keyCode == KeyEvent.KEYCODE_ENTER) {
                onDoneTappedListener?.invoke(v)
                return@setOnEditorActionListener true
            }
            false
        }
    }

    @Parcelize
    private class State(
        val parentState: Parcelable?,
        val isFocused: Boolean
    ) : Parcelable
}
