package com.etrade.mobilepro.chat.data.common

import com.etrade.mobilepro.chat.api.ChatGeolocationModel
import com.etrade.mobilepro.chat.api.ChatMetadataModel
import com.etrade.mobilepro.chat.api.message.models.ChatForMobileModel
import com.etrade.mobilepro.chat.api.message.models.ChatInfoModel
import com.etrade.mobilepro.chat.api.message.models.ChatRetrieveMessageItemModel
import com.etrade.mobilepro.chat.api.message.models.ChatRetrievedMessageDataModel
import com.etrade.mobilepro.chat.api.message.models.ChatRetrievedMessageModel
import com.etrade.mobilepro.chat.api.message.models.MessageModel
import com.etrade.mobilepro.chat.api.message.models.MobileChatDataModel
import com.etrade.mobilepro.chat.api.setup.models.ChatActiveStatusModel
import com.etrade.mobilepro.chat.api.setup.models.ChatOperatingHoursDataModel
import com.etrade.mobilepro.chat.api.setup.models.ChatProductItemModel
import com.etrade.mobilepro.chat.api.setup.models.ChatProductItemsModel
import com.etrade.mobilepro.chat.api.setup.models.GetChatOperatingHoursModel
import com.etrade.mobilepro.chat.api.setup.models.contactId.ChatCreateContactIdModel
import com.etrade.mobilepro.chat.api.setup.models.contactId.ChatGetContactIdDataModel
import com.etrade.mobilepro.chat.api.setup.models.contactId.ChatGetContactIdMessagesModel
import com.etrade.mobilepro.chat.api.setup.models.contactId.ChatGetContactIdModel
import com.etrade.mobilepro.chat.api.setup.models.contactId.ChatItemModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatAccountModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatCustomerAccountModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatCustomerDetailsModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.CustomerDetailsDataModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.GetCustomerDetailsForMobileModel
import com.etrade.mobilepro.chat.data.common.dto.ChatMetaDataDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatForMobileResponseDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatInfoDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatRetrieveMessageDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatRetrievedMessageDataDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatRetrievedMessageDetailsDto
import com.etrade.mobilepro.chat.data.messages.dto.MobileChatDataDto
import com.etrade.mobilepro.chat.data.setup.dto.chatActiveStatus.ChatActiveStatusDto
import com.etrade.mobilepro.chat.data.setup.dto.chatActiveStatus.ChatActiveStatusMetadataDto
import com.etrade.mobilepro.chat.data.setup.dto.contactId.ChatContactIdDataDto
import com.etrade.mobilepro.chat.data.setup.dto.contactId.ChatContactIdDto
import com.etrade.mobilepro.chat.data.setup.dto.contactId.ChatContactIdMessagesDto
import com.etrade.mobilepro.chat.data.setup.dto.contactId.ChatCreateContactIdDto
import com.etrade.mobilepro.chat.data.setup.dto.contactId.ChatItemDto
import com.etrade.mobilepro.chat.data.setup.dto.getChatOperatingHours.ChatOperatingHoursData
import com.etrade.mobilepro.chat.data.setup.dto.getChatOperatingHours.ChatOperatingHoursDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatAccountHolderListDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatCustomerAccountListDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatCustomerDetailsDataDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatCustomerDetailsDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatCustomerDetailsForMobileDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatGeoLocationDto
import com.etrade.mobilepro.chat.data.setup.dto.getProducts.ChatProductsDto
import com.squareup.moshi.JsonDataException

fun ChatProductsDto.toChatProductItemsModel() =
    ChatProductItemsModel(
        mutableListOf<ChatProductItemModel>().apply {
            data?.messages?.forEach { add(ChatProductItemModel(it.id, it.lookupName)) }
        }
    )

fun ChatActiveStatusDto.toGetChatActiveStatusModel() =
    ChatActiveStatusModel(data.isActive, getChatActiveStatusMetadata(data.metadata))

fun getChatActiveStatusMetadata(metadata: ChatActiveStatusMetadataDto) =
    ChatMetadataModel(buttonName = metadata.buttonName, buttonId = metadata.buttonId)

fun ChatOperatingHoursDto.toGetChatOperatingHoursModel() =
    GetChatOperatingHoursModel(data?.let { getChatOperatingHoursDataModel(data) })

fun ChatCustomerDetailsForMobileDto.toGetCustomerDetailsForMobileModel() =
    GetCustomerDetailsForMobileModel(data?.let { getCustomerDetailsDataModel(it) })

fun ChatCreateContactIdDto.toCreateChatContactIdModel() =
    data?.messages?.items?.let {
        ChatCreateContactIdModel(data = ChatGetContactIdMessagesModel(getChatItemModels(it)))
    } ?: ChatCreateContactIdModel()

fun ChatContactIdDto.toGetChatContactIdModel() = ChatGetContactIdModel(getContactIdDataModel(data))

fun getChatMetadata(metadata: ChatMetaDataDto?) =
    ChatMetadataModel(
        sessionKey = metadata?.sessionKey,
        affinity = metadata?.affinity,
        buttonId = metadata?.buttonId,
        buttonName = metadata?.buttonName
    )

fun getMobileChatDataModel(data: MobileChatDataDto?) = MobileChatDataModel(
    chatInfoModel = getChatInfoModel(data?.chatInfoModel),
    messages = data?.messages
)

fun getChatInfoModel(chatInfoModel: ChatInfoDto?) = ChatInfoModel(sessionID = chatInfoModel?.sessionID)

fun ChatRetrieveMessageDto.toRetrievedMessageModel() = ChatRetrievedMessageModel(
    data = getRetrievedMessageDataModel(data),
    success = success,
    sequence = data.sequence
)

private fun getCustomerDetailsDataModel(customerDetailsDataDto: ChatCustomerDetailsDataDto) =
    CustomerDetailsDataModel(
        customerDetailsDataDto.chatCustomerDetails?.let {
            getCustomerDetailsModel(it)
        }
    )

private fun getCustomerDetailsModel(chatCustomerDetailsDto: ChatCustomerDetailsDto) =
    ChatCustomerDetailsModel(
        userId = chatCustomerDetailsDto.userId,
        customerAccounts = getCustomerAccounts(chatCustomerDetailsDto.chatCustomerAccountList),
        userGeolocation = getChatUserGeolocationModel(chatCustomerDetailsDto.chatGeoLocationDto)
    )

private fun getChatOperatingHoursDataModel(data: ChatOperatingHoursData) =
    ChatOperatingHoursDataModel(data.isHoliday, data.inOperatingHours)

private fun getChatUserGeolocationModel(chatGeoLocationDto: ChatGeoLocationDto?) =
    chatGeoLocationDto?.let {
        ChatGeolocationModel(
            city = it.city,
            cityCode = it.cityCode,
            connectionSpeed = it.connectionSpeed,
            continentCode = it.continentCode,
            countryCode = it.countryCode,
            country = it.country,
            domain = it.domain,
            ipAddress = it.ipAddress,
            isp = it.isp,
            latitude = it.latitude,
            longitude = it.longitude,
            metroCode = it.metroCode,
            postalCode = it.postalCode,
            proxyType = it.proxyType,
            regionCode = it.regionCode,
            region = it.region,
            twoLetterCountry = it.twoLetterCountry
        )
    }

private fun getCustomerAccounts(chatCustomerAccountListDto: List<ChatCustomerAccountListDto?>): List<ChatCustomerAccountModel> {
    val list = mutableListOf<ChatCustomerAccountModel>()
    chatCustomerAccountListDto.forEach {
        it?.let {
            list.add(
                ChatCustomerAccountModel(
                    accNum = it.accNum,
                    accountType = it.accountType,
                    accounts = getAccHolderList(it.acctHolderList),
                    instDescription = it.instDescription,
                    instNo = it.instNo,
                    instName = it.instName,
                    accountHolderCount = it.acctHolderCount,
                    isOH = it.isOH
                )
            )
        }
    }
    return list
}

private fun getAccHolderList(accountHolderListDto: List<ChatAccountHolderListDto>): List<ChatAccountModel> {
    if (accountHolderListDto.isEmpty()) {
        throw JsonDataException("Account holder list must not be empty!")
    }
    return accountHolderListDto.map {
        ChatAccountModel(
            firstname = it.firstname,
            lastname = it.lastname,
            cid = it.cid,
            tier = it.tier
        )
    }
}

private fun getContactIdDataModel(contactIdMessagesDto: ChatContactIdDataDto?) =
    contactIdMessagesDto?.let {
        ChatGetContactIdDataModel(
            getContactIdMessagesModel(it.messages),
            getChatMetadata(it.metadata)
        )
    }

private fun getContactIdMessagesModel(messages: ChatContactIdMessagesDto) =
    ChatGetContactIdMessagesModel(
        mutableListOf<ChatItemModel>().apply {
            messages.items.forEach { chatItemDto ->
                add(getChatItemModel(chatItemDto))
            }
        }
    )

private fun getChatItemModel(chatItemDto: ChatItemDto) = ChatItemModel(chatItemDto.id)

private fun getChatItemModels(dtoItems: List<ChatItemDto>) =
    mutableListOf<ChatItemModel>().apply {
        dtoItems.forEach { chatItemDto ->
            add(getChatItemModel(chatItemDto))
        }
    }

fun ChatForMobileResponseDto.toChatForMobileModel() = ChatForMobileModel(
    data = getMobileChatDataModel(data),
    success = success
)

private fun getRetrievedMessageDataModel(data: ChatRetrievedMessageDataDto?) =
    ChatRetrievedMessageDataModel(
        mutableListOf<ChatRetrieveMessageItemModel>().apply {
            data?.items?.forEach {
                add(ChatRetrieveMessageItemModel(type = it.type, message = getMessageModel(it.message)))
            }
        }
    )

private fun getMessageModel(message: ChatRetrievedMessageDetailsDto) =
    with(message) {
        MessageModel(
            agentId = agentId,
            connectionTimeout = connectionTimeout,
            name = name,
            queuePosition = queuePosition,
            sneakPeekEnabled = sneakPeekEnabled,
            text = text,
            transcriptSaveEnabled = transcriptSaveEnabled,
            url = url,
            userId = userId,
            visitorId = visitorId,
            estimatedWaitTime = estimatedWaitTime,
            position = position,
            reason = reason
        )
    }
