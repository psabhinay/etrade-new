package com.etrade.mobilepro.uiwidgets.tablesectionsview.model

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import com.etrade.mobilepro.uiwidgets.tablesectionsview.R

class SummarySectionItemWithActionIcon(
    label: String,
    value: String,
    @ColorInt textColor: Int? = null,
    labelDescription: String? = null,
    valueDescription: String? = null,
    @DrawableRes private val actionIconDrawable: Int,
    private val actionIconDescription: String,
    private val onActionIconClick: () -> Unit
) : SummarySectionItem(label, value, textColor, labelDescription, valueDescription) {

    override fun getRowView(linearParams: LinearLayout.LayoutParams, parent: ViewGroup): View {
        val view = super.getRowView(linearParams, parent)
        view.findViewById<ImageView>(R.id.actionIcon).apply {
            setImageResource(actionIconDrawable)
            visibility = View.VISIBLE
            contentDescription = actionIconDescription
            setOnClickListener { onActionIconClick.invoke() }
        }

        return view
    }
}
