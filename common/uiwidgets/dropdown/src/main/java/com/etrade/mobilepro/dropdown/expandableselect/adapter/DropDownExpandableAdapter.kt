package com.etrade.mobilepro.dropdown.expandableselect.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.dropdown.databinding.BottomSheetSelectorRowBinding
import com.etrade.mobilepro.dropdown.expandableselect.DropDownExpandableSelector
import com.etrade.mobilepro.dropdown.expandableselect.ExpandableItemState
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.viewBinding

internal class DropDownExpandableAdapter<T>(
    private val lastExpandedItem: ExpandableItemState?,
    listener: (index: Int, item: DropDownExpandableSelector.Selectable<T>, shouldDismiss: Boolean) -> Unit,
    private val updateListener: ((index: Int, length: Int, newIndex: Int) -> Unit)
) : RecyclerView.Adapter<ExpandableViewHolder<T>>(), UpdatesConsumer<T> {
    private val items = mutableListOf<DropDownExpandableSelector.Selectable<T>>()
    private lateinit var expandablesHandler: ExpandablesHandler<T>

    private val innerListener: (Int, DropDownExpandableSelector.Selectable<T>) -> Unit = { index, item ->
        expandablesHandler.itemWasClicked(index, item)
        listener.invoke(index, item, item.subItems == null)
    }

    fun submitItems(items: List<DropDownExpandableSelector.Selectable<T>>) {
        if (::expandablesHandler.isInitialized) {
            expandablesHandler.applyNewSelected(items)
        } else {
            expandablesHandler = ExpandablesHandler(lastExpandedItem, items, this, updateListener)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpandableViewHolder<T> =
        ExpandableViewHolder(parent.viewBinding(BottomSheetSelectorRowBinding::inflate), innerListener)

    override fun getItemCount(): Int = this.items.size

    override fun onBindViewHolder(holder: ExpandableViewHolder<T>, position: Int) {
        holder.bindTo(items[position])
    }

    override fun dispatchUpdates(renderedItems: List<DropDownExpandableSelector.Selectable<T>>) {
        dispatchUpdates(items, renderedItems)
        items.clear()
        items.addAll(renderedItems)
    }
}
