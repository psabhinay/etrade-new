package com.etrade.mobilepro.momentumbar

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.Style
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter

/**
 * These streaming constants should be inside streaming api library
 * TODO use streaming constants
 */
const val STREAMING_PRICE_CHANGE_UP: Short = 1
const val STREAMING_PRICE_CHANGE_DOWN: Short = -1
const val STREAMING_PRICE_CHANGE_UNCHANGED: Short = 0

class MomentumBar : View {
    private var momentumBarData: MutableList<MomentumBarBlock> = mutableListOf()
    private var lastChange: Short = STREAMING_PRICE_CHANGE_UNCHANGED
    private var currentLeft: Float = 0f

    private val pillSize = resources.getDimension(R.dimen.momentum_bar_width)
    private val innerPadding = resources.getDimension(R.dimen.momentum_bar_inner_padding)
    private val topPadding = resources.getDimension(R.dimen.momentum_bar_top_padding)
    private val roundCorners = resources.getDimension(R.dimen.pill_round_corners)
    private val strokeWidth = resources.getDimension(R.dimen.pill_stroke_width)
    private val halfStrokeWidth = strokeWidth / 2

    private val paint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val rectangle: RectF = RectF()

    var maxBarSizeListener: MaxBarSizeListener? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attr: AttributeSet) : super(context, attr) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        setBackgroundColor(ContextCompat.getColor(context, R.color.white))
    }

    @SuppressWarnings("MagicNumber")
    override fun onDraw(canvas: Canvas) {
        currentLeft = width - pillSize

        for (index in momentumBarData.indices) {
            if (currentLeft >= 0) {
                rectangle.left = currentLeft + halfStrokeWidth
                rectangle.top = topPadding + halfStrokeWidth
                rectangle.right = currentLeft + pillSize - halfStrokeWidth
                rectangle.bottom = layoutParams.height - topPadding - halfStrokeWidth

                currentLeft -= pillSize + innerPadding
                drawBlock(canvas, momentumBarData[index])
            } else {
                maxBarSizeListener?.onMaxSizeReached(index)
                break
            }
        }
    }

    private fun drawBlock(canvas: Canvas, block: MomentumBarBlock) {
        if (block.isGreen) {
            paint.color = ContextCompat.getColor(context, R.color.green)
        } else {
            paint.color = ContextCompat.getColor(context, R.color.red)
        }

        if (block.isFilled) {
            paint.style = Style.FILL_AND_STROKE
        } else {
            paint.style = Style.STROKE
        }
        paint.strokeWidth = strokeWidth

        canvas.drawRoundRect(rectangle, roundCorners, roundCorners, paint)
    }

    private data class MomentumBarBlock(val isGreen: Boolean, val isFilled: Boolean)

    private fun getBlock(streamingPriceChange: Short): MomentumBarBlock {
        val isGreen: Boolean
        val isFilled: Boolean

        when (streamingPriceChange) {
            STREAMING_PRICE_CHANGE_UP -> {
                isGreen = true
                isFilled = true
                lastChange = STREAMING_PRICE_CHANGE_UP
            }
            STREAMING_PRICE_CHANGE_DOWN -> {
                isGreen = false
                isFilled = true
                lastChange = STREAMING_PRICE_CHANGE_DOWN
            }
            else -> {
                isFilled = false
                isGreen = lastChange != STREAMING_PRICE_CHANGE_DOWN
            }
        }

        return MomentumBarBlock(isGreen, isFilled)
    }

    fun updateBarWithList(streamingPriceChanges: List<Short>) {
        momentumBarData.clear()
        lastChange = STREAMING_PRICE_CHANGE_UNCHANGED
        for (change in streamingPriceChanges) {
            momentumBarData.add(0, getBlock(change))
        }

        this.postInvalidate()
    }
}

interface MaxBarSizeListener {
    fun onMaxSizeReached(size: Int)
}

@BindingAdapter("updateMomentumBar")
fun updateMomentumBar(momentumBar: MomentumBar, data: List<Short>?) {
    data?.let {
        momentumBar.updateBarWithList(data)
    }
}

@BindingAdapter("maxSizeListener")
fun setMaxSizeListener(momentumBar: MomentumBar, listener: MaxBarSizeListener?) {
    listener?.let {
        momentumBar.maxBarSizeListener = listener
    }
}
