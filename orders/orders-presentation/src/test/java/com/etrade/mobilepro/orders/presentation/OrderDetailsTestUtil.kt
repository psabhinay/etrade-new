package com.etrade.mobilepro.orders.presentation

import android.content.res.Resources
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.orders.presentation.section.OrderLabels
import com.etrade.mobilepro.orders.presentation.section.OrderResources

internal fun generateMockOrderDetailLabelResources(resources: Resources): OrderDetailLabelResources {
    return OrderDetailLabelResources(
        OrderLabels(
            "All or None",
            "Order Number",
            "Order Placed",
            "Order Status",
            "Executed",
            "Order Type",
            "Quantity",
            "Term",
            "Price Type",
            "Limit Price",
            "Stop Price",
            "Price Executed",
            "Commission/Fees",
            "Cancel Requested",
            "Expired",
            "System Rejected",
            "Cancelled",
            "Order Saved",
            "Account",
            "Strategy",
            "Estimated Commission",
            "Estimated Total Cost",
            "Estimated Total Process",
            "Quantity (Executed/Entered)",
            "GTD %1$",
            "Extended Hrs Day(7am-8pm ET)",
            "Extended Hrs Night(8pm-4am ET)",
            "Destination",
            "Order executed",
            "Qualifier",
            "Condition",
            "Value",
            "Stop Val $",
            "Stop Val %",
            "Price",
            "Limit Offset",
            "Estimated Commission/Fees",
            "Description",
            "Exercise Value",
            "Shares Exercised",
            "Shares to Exercise",
            "Shares to Issue",
            "Est. Gross Proceeds",
            "Total Price",
            "Sec. Fee",
            "Broker Assist Fee",
            "Disbursement Fee",
            "Est. Tax Withheld",
            "Est. Shares WithHeld/Value",
            "Est. Net Proceeds",
            "Shares to Exercise",
            "Shares to Issue",
            "Shares WithHeld/Value"
        ),
        OrderResources(
            "$",
            "--",
            "%",
            122323,
            resources
        )
    )
}
