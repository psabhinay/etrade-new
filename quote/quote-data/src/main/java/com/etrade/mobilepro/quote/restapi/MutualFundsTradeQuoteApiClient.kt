package com.etrade.mobilepro.quote.restapi

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.quote.dto.MutualFundOwnedDetailRequestDto
import com.etrade.mobilepro.quote.dto.MutualFundsTradeQuoteDetailsResponseDto
import com.etrade.mobilepro.quote.dto.MutualFundsTradeQuoteRequestValueDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.POST

interface MutualFundsTradeQuoteApiClient {

    @JsonMoshi
    @POST("/app/quote/getmobileMFDetails.json")
    suspend fun getFundDetails(
        @Body @JsonMoshi body: MutualFundsTradeQuoteRequestValueDto
    ): ServerResponseDto<MutualFundsTradeQuoteDetailsResponseDto>

    @JsonMoshi
    @POST("app/mobilemftrading/mobileGetSellDetails.json")
    suspend fun mutualFundEntryDetail(
        @JsonMoshi @Body body: MutualFundOwnedDetailRequestDto
    ): ServerResponseDto<MutualFundsTradeQuoteDetailsResponseDto>
}
