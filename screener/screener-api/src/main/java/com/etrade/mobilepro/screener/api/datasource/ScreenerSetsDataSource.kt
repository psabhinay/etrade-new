package com.etrade.mobilepro.screener.api.datasource

import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.api.ScreenerType
import io.reactivex.Observable

interface ScreenerSetsDataSource {
    fun screeners(group: ScreenerType.Group): Observable<List<ScreenerItem>>
    fun clearScreenerSet(screenerSet: ScreenerItem.ScreenerSet)
}
