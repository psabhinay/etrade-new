package com.etrade.mobilepro.learn.presentation.tutorials

import com.etrade.mobilepro.walkthrough.api.Action
import com.etrade.mobilepro.walkthrough.api.SymbolLookupAction
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingAction
import com.etrade.mobilepro.walkthrough.api.TradePlacingAction

class ActionsMap {

    companion object {

        const val SYMBOL_LOOKUP_DISMISS_DIALOG = "SymbolLookupDismissDialog"
        const val SYMBOL_TRACKING_DISMISS_DIALOG = "SymbolTrackingDismissDialog"
        const val TRADE_PLACING_DISMISS_DIALOG = "TradePlacingDismissDialog"

        fun get(key: String): Action? {
            return when (key) {
                SYMBOL_LOOKUP_DISMISS_DIALOG -> SymbolLookupAction.DismissQuoteDialog
                SYMBOL_TRACKING_DISMISS_DIALOG -> SymbolTrackingAction.InfoMessageDisplayed
                TRADE_PLACING_DISMISS_DIALOG -> TradePlacingAction.InfoMessageDisplayed
                else -> null
            }
        }
    }
}
