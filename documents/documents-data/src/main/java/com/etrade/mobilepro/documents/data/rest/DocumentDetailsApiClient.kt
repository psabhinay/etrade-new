package com.etrade.mobilepro.documents.data.rest

import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.POST

internal interface DocumentDetailsApiClient {
    @JsonMoshi
    @POST("/phx/etm/services/v1/account/documents/pdfDocument")
    suspend fun getDocumentDetails(@JsonMoshi @Body requestDto: DocumentRequestDto): DocumentResponseDto
}
