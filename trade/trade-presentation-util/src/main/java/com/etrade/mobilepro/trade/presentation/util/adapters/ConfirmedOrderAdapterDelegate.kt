package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import com.etrade.mobilepro.trade.api.ConfirmedOrder
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.presentation.util.R
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

class ConfirmedOrderAdapterDelegate(private val resources: Resources) : OrderAdapterDelegate {

    override fun createOrderSectionItems(order: Order): List<TableSectionItemLayout> {
        return if (order is ConfirmedOrder) {
            listOfNotNull(
                sectionItem(resources, R.string.trade_label_confirmation_number, order.confirmationOrderId),
                sectionItem(resources, R.string.trade_label_timestamp, order.placedDate)
            )
        } else {
            emptyList()
        }
    }
}
