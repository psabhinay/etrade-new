package com.etrade.mobilepro.transfermoney.usecase.calendar

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.transfermoney.api.TransferCalendarHolidays
import com.etrade.mobilepro.transfermoney.api.repo.TransferCalendarRepo
import com.etrade.mobilepro.util.UseCase
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

private const val DATE_WITH_THREE_SYMBOLS = 3
private const val DATE_WITH_FOUR_SYMBOLS = 4
private const val DATE_WITH_EIGHT_SYMBOLS = 8
private const val FROM_INST_TYPE_EXTERNAL = "EXTERNAL"
private const val TO_INST_TYPE_TELEBANK = "TELEBANK"
private val DATE_FORMATTER: DateTimeFormatter = DateTimeFormatter.BASIC_ISO_DATE

interface GetTransferCalendarUseCase : UseCase<GetTransferCalendarRequest, ETResult<Set<LocalDate>>>

class GetTransferCalendarUseCaseImpl @Inject constructor(
    private val repo: TransferCalendarRepo
) : GetTransferCalendarUseCase {

    /**
     * @param parameter Current year
     */
    override suspend fun execute(parameter: GetTransferCalendarRequest): ETResult<Set<LocalDate>> {
        return runCatchingET {
            handleSuccess(repo.getCalendar().getOrThrow(), parameter.year)
        }
    }

    private fun handleSuccess(items: List<TransferCalendarHolidays>, year: Int): Set<LocalDate> {
        return items.asSequence()
            .filter(::filterByFromInstAndToInstType)
            .map { item -> mapRepoModelToViewModel(year, item) }
            .flatten()
            .toSet()
    }

    private fun filterByFromInstAndToInstType(item: TransferCalendarHolidays): Boolean {
        return item.fromInstType.equals(FROM_INST_TYPE_EXTERNAL, true) && item.toInstType.equals(TO_INST_TYPE_TELEBANK, true)
    }

    /**
     * Current logic ported from existing app:
     * Holiday could be returned from server in three different formats
     * - three symbols like: 111, 123
     * - four symbols like: 1111, 1220
     * - eight symbols like: 20190112, 20201123
     *
     * For three symbols case we should use current year with 0 and rest of symbols: 2019_0_111 => 20190111
     * For four symbols case we should use current year and rest of symbols: 2019_1112 => 20191112
     * Eight symbols we directly pass to [DATE_FORMATTER] to get [LocalDate]
     */
    private fun mapRepoModelToViewModel(year: Int, item: TransferCalendarHolidays): List<LocalDate> {
        val result = mutableListOf<LocalDate>()

        item.holidays.forEach {
            when (it.length) {
                DATE_WITH_THREE_SYMBOLS -> {
                    result.add(LocalDate.parse("${year}0$it", DATE_FORMATTER))
                    result.add(LocalDate.parse("${year + 1}0$it", DATE_FORMATTER))
                }
                DATE_WITH_FOUR_SYMBOLS -> {
                    result.add(LocalDate.parse("${year}$it", DATE_FORMATTER))
                    result.add(LocalDate.parse("${year + 1}$it", DATE_FORMATTER))
                }
                DATE_WITH_EIGHT_SYMBOLS -> result.add(
                    LocalDate.parse(
                        it,
                        DATE_FORMATTER
                    )
                )
            }
        }

        return result
    }
}
