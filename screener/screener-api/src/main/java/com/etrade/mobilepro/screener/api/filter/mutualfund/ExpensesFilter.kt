package com.etrade.mobilepro.screener.api.filter.mutualfund

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

private val customResultFields = listOf(
    "DistributionYield",
    "CatReturn1year",
    "CatReturn5year",
    "CatReturn10year",
    "FrontLoad",
    "DeferLoad",
    "RedemptionFee"
)

data class ExpensesFilter(
    val expenseRatioLessThan5: Boolean = false,
    val expenseRatio5to1: Boolean = false,
    val expenseRatio1to15: Boolean = false,
    val expenseRatio15to2: Boolean = false,
    val expenseRatioGreaterThan2: Boolean = false
) : ScreenerFilter {
    override fun isEmpty(): Boolean = !expenseRatioLessThan5 && !expenseRatio5to1 && !expenseRatio1to15 && !expenseRatio15to2 && !expenseRatioGreaterThan2

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND

    override fun customResultFields(): List<String> = customResultFields
}
