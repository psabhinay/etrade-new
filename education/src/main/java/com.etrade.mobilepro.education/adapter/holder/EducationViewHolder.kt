package com.etrade.mobilepro.education.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.education.EducationItem
import com.etrade.mobilepro.education.databinding.AdapterEducationBinding

class EducationViewHolder(private val binding: AdapterEducationBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(
        item: EducationItem,
        clickListener: View.OnClickListener
    ) {
        with(binding) {
            root.tag = item
            root.setOnClickListener(clickListener)
            title.text = item.title
            author.text = item.author
            length.text = item.length
            description.text = item.description
        }
    }
}
