package com.etrade.mobilepro.landing.presentation

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.common.setupBlurView
import com.etrade.mobilepro.landing.presentation.adapter.LandingPageSelectionAdapter
import com.etrade.mobilepro.landing.presentation.databinding.LandingPageFragmentSelectionBinding
import com.etrade.mobilepro.livedata.observeBy
import javax.inject.Inject

class LandingPageSelectionFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory
) : Fragment() {

    object Const {
        const val TAG = "LandingPageSelectionFragment"
    }

    private val viewModel by activityViewModels<LandingPageSelectionViewModel> { viewModelFactory }
    private var binding: LandingPageFragmentSelectionBinding? = null
    private val adapter = LandingPageSelectionAdapter {
        viewModel.itemSelected(it.value)
    }
    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            viewModel.onBackPressed()
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = LandingPageFragmentSelectionBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
        viewModel.fetchData()
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    private fun setupFragment() = binding?.apply {
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter

        setupBlurView(blurView)

        backArrow.setOnClickListener {
            viewModel.onBackPressed()
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, onBackPressedCallback)
    }

    private fun bindViewModel() {
        viewModel.viewState.observeBy(
            viewLifecycleOwner
        ) {
            binding?.apply {
                backArrow.visibility = if (it.hasBackArrow) {
                    View.VISIBLE
                } else {
                    View.INVISIBLE
                }
            }
            adapter.submitState(it)
        }

        viewModel.hideSignal.observeBy(viewLifecycleOwner) {
            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_USER
        }
    }

    companion object {

        const val TAG = "LandingPageSelectionFragment"
    }
}
