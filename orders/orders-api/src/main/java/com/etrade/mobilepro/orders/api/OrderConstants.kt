package com.etrade.mobilepro.orders.api

import com.etrade.mobilepro.orders.api.OrderAction.BUY_ACTION
import com.etrade.mobilepro.orders.api.OrderAction.SELL_ACTION

private const val ORDER_ACTION_BUY = "2"
private const val ORDER_ACTION_SELL = "3"
const val OFFSET_OPEN = "1"
const val OFFSET_CLOSED = "2"
const val NOT_SUPPORTED = "NOT_SUPPORTED"
const val USER_SELECTED_DNR_FLAG = "2"
const val ORDER_TRIGGER_MARKET = "1"
const val ORDER_TRIGGER_LIMIT = "2"

fun extractOrderStatus(orderStatusList: List<OrderStatus>): OrderStatus {
    return when {
        orderStatusList.contains(OrderStatus.OPEN) -> OrderStatus.OPEN
        orderStatusList.contains(OrderStatus.EXECUTED) -> OrderStatus.EXECUTED
        orderStatusList.contains(OrderStatus.CANCELLED) -> OrderStatus.CANCELLED
        orderStatusList.contains(OrderStatus.REJECTED) -> OrderStatus.REJECTED
        orderStatusList.contains(OrderStatus.EXPIRED) -> OrderStatus.EXPIRED
        else -> orderStatusList[0]
    }
}

enum class OrderRequestType {
    ORDER_REQTYPE_UNSPECIFIED,
    ORDER_REQTYPE_PLACEPREVIEW,
    ORDER_REQTYPE_PLACE,
    ORDER_REQTYPE_PLACE_WITHLOT,
    ORDER_REQTYPE_MODIFYLOOKUP,
    ORDER_REQTYPE_MODIFYPREVIEW,
    ORDER_REQTYPE_MODIFY,
    ORDER_REQTYPE_MODIFY_WITHLOT,
    ORDER_REQTYPE_CANCELPREVIEW,
    ORDER_REQTYPE_CANCEL,
}

/**
 * enum class used to map order statuses.
 * @property requestCode used to fetch orders ranging from 0 to 6
 */
@Suppress("MagicNumber")
enum class OrderStatus(
    val requestCode: Int,
    val responseCode: Int = -1,
    val espStatusCode: String = ""
) {
    ALL(0),
    ALL_BY_DATE(0),
    ALL_BY_SYMBOL(0),
    SAVED(0, 0),
    OPEN(1, 1, "O"),
    CANCEL_REQUESTED(1, 5),
    CANCEL_REJECTED(1, 6),
    REJECT_REQUESTED(1, 7),
    REQUEST_SENT_TO_MARKET(1, 8),
    CANCELLED(3, 10, "C"),
    EXECUTED(2, 11, "E"),
    REJECTED(6, 12),
    EXPIRED(5, 13),
    OPEN_AND_UNSETTLED(0, -1, "U"),
    SETTLED(requestCode = 0, espStatusCode = "S"),
    UNKNOWN(-1);

    companion object {
        /**
         * Creates an order status from a [statusCode] from orders response.
         * @param statusCode statusCode ranging from 0-13.
         */
        fun fromStatusCode(statusCode: String?): OrderStatus {
            if (statusCode == null) {
                return UNKNOWN
            }
            var intStatusCode = statusCode.toIntOrNull() ?: -1
            if (intStatusCode > OPEN.responseCode && intStatusCode < CANCEL_REQUESTED.responseCode) {
                intStatusCode = OPEN.responseCode
            }
            return values().firstOrNull { intStatusCode == it.responseCode } ?: UNKNOWN
        }
    }
}

enum class TradeOrderType {
    NEW,
    EDIT_OPEN,
    EDIT_SAVED;

    fun isEdit() = this != NEW
    fun isOpen() = this == EDIT_OPEN
}

enum class AdvancedOrderType(val typeCode: String) {
    AO_BRACKETED_FIXED("1"),
    AO_BRACKETED_TRAIL("2"),
    AO_SIMPLE_STOP("3"),
    AO_TRAIL_STOP("4"),
    AO_LIMIT_PRESENTABLE("5"),
    AO_LIMIT_EXECUTABLE("6"),
    AO_CONTINGENT("7"),
    AO_CONTINGENT_CANCEL("8"),
    AO_TRAIL_STOP_LIMIT("9"),
    UNKNOWN("0");

    companion object {
        val bracketedOrderTypes = listOf(
            AO_BRACKETED_FIXED,
            AO_BRACKETED_TRAIL
        )
        val contingentOrderTypes = listOf(
            AO_CONTINGENT,
            AO_CONTINGENT_CANCEL
        )

        val trailOrderTypes = listOf(
            AO_TRAIL_STOP,
            AO_TRAIL_STOP_LIMIT
        )

        val orderTypesForPriceType = listOf(
            AO_BRACKETED_FIXED,
            AO_BRACKETED_TRAIL,
            AO_SIMPLE_STOP,
            AO_TRAIL_STOP,
            AO_LIMIT_PRESENTABLE,
            AO_LIMIT_EXECUTABLE
        )

        /**
         * Creates an advanced order type from a [typeCode] from orders response.
         * @param typeCode statusCode ranging from 0-9
         */
        fun fromTypeCode(typeCode: String?) = typeCode?.let { type ->
            values().firstOrNull { type == it.typeCode }
        } ?: UNKNOWN
    }
}

enum class OrderType(val typeCode: String) {
    EQUITY("1"),
    ADVANCE_EQUITY("2"),
    SIMPLE_OPTION("3"),
    SPREADS("4"),
    BUY_WRITES("5"),
    OPTION_EXERCISED("6"),
    OPTION_ASSIGNED("7"),
    OPTION_EXPIRED("8"),
    MUTUAL_FUND("9"),
    MONEY_MARKET_FUND("10"),
    BOND("11"),
    ADVANCE_OPTION("12"),
    OPTION_DO_NOT_EXERCISE("13"),
    BUTTERFLY("14"),
    CONDOR("15"),
    IRON_CONDOR("16"),
    IRON_BUTTERFLY("17"),
    COLLARS("18"),
    CUSTOM("19"),
    CONTINGENT("25"),
    CONDITIONAL_ONE_CANCEL_OTHER("26"),
    CONDITIONAL_ONE_TRIGGER_OTHER("27"),
    CONDITIONAL_OTOCO("28"),
    OTHER("29"),
    UNKNOWN("-1");

    fun isMutualFund(): Boolean {
        return this == MUTUAL_FUND || this == MONEY_MARKET_FUND
    }

    fun isConditional(): Boolean {
        return this == CONDITIONAL_ONE_CANCEL_OTHER || this == CONDITIONAL_ONE_TRIGGER_OTHER || this == CONDITIONAL_OTOCO
    }

    companion object {
        val conditionalOrderTypes = listOf(
            CONDITIONAL_ONE_CANCEL_OTHER,
            CONDITIONAL_ONE_TRIGGER_OTHER,
            CONDITIONAL_OTOCO,
            OTHER
        )

        /**
         * Creates an order type from a [typeCode] from orders response.
         * @param typeCode ranging from 1-29
         */
        fun fromTypeCode(typeCode: String?) = typeCode?.let { type ->
            values().firstOrNull { type == it.typeCode }
        } ?: UNKNOWN
    }
}

enum class OrderAction(val value: String, val isBuy: Boolean, val multiplier: Int) {
    BUY_ACTION(ORDER_ACTION_BUY, true, 1),
    SELL_ACTION(ORDER_ACTION_SELL, false, -1),
    EXCHANGE_ACTION("", false, 1),
    UNKNOWN("", false, -1);

    companion object {
        fun fromString(code: String): OrderAction {
            return when (code) {
                ORDER_ACTION_BUY -> BUY_ACTION
                ORDER_ACTION_SELL -> SELL_ACTION
                else -> UNKNOWN
            }
        }

        fun fromBoolean(isBuy: Boolean): OrderAction {
            return if (isBuy) {
                BUY_ACTION
            } else {
                SELL_ACTION
            }
        }
    }
}

/**
 * enum used to map transactions types for orders.
 *
 * @property typeCode is used by trading requests
 */
@Suppress("MagicNumber")
enum class TransactionType(
    val typeCode: Int,
    val orderAction: OrderAction,
    val positionEffect: String,
    val isOption: Boolean = false
) {
    BUY_OPEN(1, BUY_ACTION, OFFSET_OPEN),
    SELL_CLOSE(2, SELL_ACTION, OFFSET_CLOSED),
    SELL_OPEN(3, SELL_ACTION, OFFSET_OPEN),
    BUY_CLOSE(4, BUY_ACTION, OFFSET_CLOSED),

    BUY_OPEN_OPT(1, BUY_ACTION, OFFSET_OPEN, true),
    SELL_CLOSE_OPT(2, SELL_ACTION, OFFSET_CLOSED, true),
    SELL_OPEN_OPT(3, SELL_ACTION, OFFSET_OPEN, true),
    BUY_CLOSE_OPT(4, BUY_ACTION, OFFSET_CLOSED, true),

    UNKNOWN(-1, OrderAction.UNKNOWN, "");

    companion object {

        /**
         * Creates TransactionType from a [orderAction], [positionEffect], and [isOption]. Returns [UNKNOWN] if nothing is matched.
         * @param orderAction 2 for buy 3 for sell
         * @param positionEffect 1 or 2 used to determine whether the transaction is to open or to close a position
         * @param isOption boolean for is this for a symbol that is an option or not
         */
        fun from(
            orderAction: OrderAction,
            positionEffect: String,
            isOption: Boolean
        ): TransactionType =
            values().firstOrNull { orderAction == it.orderAction && positionEffect == it.positionEffect && isOption == it.isOption }
                ?: UNKNOWN

        /**
         * Creates TransactionType from a [typeCode], and [isOption]. Returns [UNKNOWN] if nothing is matched.
         * @param typeCode 1, 2, 3, 4 for respective transaction type
         * @param isOption boolean for is this for a symbol that is an option or not
         */
        fun fromTypeCode(typeCode: Int?, isOption: Boolean): TransactionType =
            values().firstOrNull { typeCode == it.typeCode && isOption == it.isOption } ?: UNKNOWN
    }
}

fun TransactionType.signedQuantity(quantity: Int): Int = when (this.orderAction) {
    BUY_ACTION -> quantity
    else -> quantity.times(-1)
}

enum class PriceType(
    val typeCode: String
) {
    UNKNOWN("-1"),
    MARKET("1"),
    LIMIT("2"),
    STOP("3"),
    STOP_LIMIT("4"),
    NET_DEBIT("6"),
    NET_CREDIT("7"),
    EVEN("8"),
    BRACKETED("9"),
    HIDDEN_STOP("5"),
    RESERVE_LIMIT("11"),
    MARKET_ON_OPEN("12"),
    MARKET_ON_CLOSE("13"),
    LIMIT_ON_OPEN("14"),
    LIMIT_ON_CLOSE("15"),
    FIRM("16"),
    SUBJECT("17"),
    TRAILING_STOP_PERCENT("18"),
    TRAILING_STOP_DOLLAR("19"),

    HIDDEN_LIMIT(NOT_SUPPORTED),
    TRAILING_STOP_LIMIT(NOT_SUPPORTED),

    OH_STOP_DEBIT("9"),
    OH_STOP_CREDIT("10"),
    OH_STOP_EVEN("11"),
    OH_STOP_DEBIT_LIMIT_CREDIT("20"),
    OH_STOP_CREDIT_LIMIT_DEBIT("21"),
    OH_STOP_LIMIT_DEBIT("22"),
    OH_STOP_LIMIT_CREDIT("23"),

    H_STOP_LOWER_TRIGGER("25"),
    UPPER_TRIGGER_H_STOP("26"),
    T_STOP_PERCENT_LOWER_TRIGGER("27"),
    T_STOP_DOLLAR_LOWER_TRIGGER("28"),
    UPPER_TRIGGER_H_STOP_PERCENT("29"),
    UPPER_TRIGGER_H_STOP_DOLLAR("30"),
    H_STOP("31"),
    LIMIT_PRESENTABLE("32");

    fun isStopPriceVisible(): Boolean {
        return this == STOP || this == STOP_LIMIT || this == TRAILING_STOP_LIMIT
    }

    fun showStopLossDisclosure(): Boolean =
        this == STOP || this == STOP_LIMIT || this == TRAILING_STOP_DOLLAR || this == TRAILING_STOP_PERCENT

    companion object {
        private val limitPriceTypes =
            listOf(LIMIT, NET_DEBIT, NET_CREDIT, LIMIT_ON_OPEN, LIMIT_ON_CLOSE, FIRM, SUBJECT)
        private val marketPriceTypes = listOf(MARKET, MARKET_ON_OPEN, MARKET_ON_CLOSE)
        private val stopPriceTypes = listOf(
            OH_STOP_DEBIT,
            OH_STOP_CREDIT,
            OH_STOP_EVEN,
            TRAILING_STOP_PERCENT,
            TRAILING_STOP_DOLLAR,
            OH_STOP_DEBIT_LIMIT_CREDIT,
            OH_STOP_CREDIT_LIMIT_DEBIT
        )

        fun isLimitPrice(typeCode: String): Boolean {
            return limitPriceTypes.firstOrNull { it.typeCode == typeCode } != null
        }

        fun isMarketPrice(typeCode: String): Boolean {
            return marketPriceTypes.firstOrNull { it.typeCode == typeCode } != null
        }

        fun isStopPrice(typeCode: String): Boolean {
            return stopPriceTypes.firstOrNull { it.typeCode == typeCode } != null
        }

        fun getPriceType(
            typeCode: String,
            isHiddenLimit: Boolean,
            isReserveLimit: Boolean,
            isLimitOffsetPositive: Boolean
        ): PriceType {
            return when {
                isHiddenLimit -> HIDDEN_LIMIT
                isLimitOffsetPositive -> TRAILING_STOP_LIMIT
                isReserveLimit -> RESERVE_LIMIT
                else -> mapPriceType(typeCode)
            }
        }

        fun mapPriceType(typeCode: String): PriceType {
            return when (typeCode) {
                "9" -> NET_DEBIT
                "10" -> NET_CREDIT
                "11" -> EVEN
                else -> values().firstOrNull { typeCode == it.typeCode } ?: UNKNOWN
            }
        }

        @Suppress("LongMethod", "ComplexMethod") // long 'when'
        fun getPriceTypeForAdvancedOrderType(
            aoType: AdvancedOrderType,
            offSetType: String,
            orderAction: String,
            orderTrigger: String,
        ): PriceType {
            return when (aoType) {
                AdvancedOrderType.AO_BRACKETED_FIXED -> {
                    if (ORDER_ACTION_BUY == orderAction) {
                        H_STOP_LOWER_TRIGGER
                    } else {
                        UPPER_TRIGGER_H_STOP
                    }
                }
                AdvancedOrderType.AO_BRACKETED_TRAIL -> {
                    if (ORDER_ACTION_BUY == orderAction) {
                        if (OFFSET_CLOSED == offSetType) {
                            T_STOP_PERCENT_LOWER_TRIGGER
                        } else {
                            T_STOP_DOLLAR_LOWER_TRIGGER
                        }
                    } else {
                        if (OFFSET_CLOSED == offSetType) {
                            UPPER_TRIGGER_H_STOP_PERCENT
                        } else {
                            UPPER_TRIGGER_H_STOP_DOLLAR
                        }
                    }
                }
                AdvancedOrderType.AO_SIMPLE_STOP -> {
                    when {
                        ORDER_TRIGGER_MARKET == orderTrigger -> HIDDEN_STOP
                        ORDER_TRIGGER_LIMIT == orderTrigger -> STOP_LIMIT
                        else -> H_STOP
                    }
                }
                AdvancedOrderType.AO_TRAIL_STOP -> {
                    if (OFFSET_CLOSED == offSetType) {
                        TRAILING_STOP_PERCENT
                    } else {
                        TRAILING_STOP_DOLLAR
                    }
                }
                AdvancedOrderType.AO_LIMIT_PRESENTABLE -> {
                    LIMIT_PRESENTABLE
                }
                AdvancedOrderType.AO_LIMIT_EXECUTABLE -> {
                    LIMIT
                }
                else -> UNKNOWN
            }
        }
    }
}

enum class StrategyType(
    val typeCode: String,
    val strategyName: String
) {
    STOCK("1", "STGY_STOCK"),
    CALL("2", "STGY_CALL"),
    PUT("3", "STGY_PUT"),
    CALL_SPREAD("4", "STGY_CALL_SPREAD"),
    PUT_SPREAD("5", "STGY_PUT_SPREAD"),
    BUY_WRITE("6", "STGY_BUY_WRITE"),
    MARRIED_PUT("7", "STGY_MARRIED_PUT"),
    STRADDLE("8", "STGY_STRADDLE"),
    STRANGLE("9", "STGY_STRANGLE"),
    CALENDAR("10", "STGY_CALENDAR"),
    DIAGONAL("11", "STGY_DIAGONAL"),
    BUTTERFLY("12", "STGY_BUTTERFLY"),
    IRON_BUTTERFLY("13", "STGY_IRON_BUTTERFLY"),
    CONDOR("14", "STGY_CONDOR"),
    IRON_CONDOR("15", "STGY_IRON_CONDOR"),
    COMBO("16", "STGY_COMBO"),
    COLLARS("17", "STGY_COLLARS"),
    CUSTOM("18", "STGY_CUSTOM"),
    UNKNOWN("-1", "");

    companion object {

        fun fromName(name: String?) = from(name) { strategyName }

        /**
         * Creates an StrategyType from a [typeCode] from orders response.
         * @param typeCode statusCode ranging from 1-18
         */
        fun fromTypeCode(typeCode: String?) = from(typeCode) { this.typeCode }

        private inline fun from(
            candidate: String?,
            property: StrategyType.() -> String
        ): StrategyType {
            return candidate?.let {
                values().firstOrNull { type ->
                    it == type.property()
                }
            } ?: UNKNOWN
        }
    }
}

enum class EventType(
    val typeCode: String
) {
    PLACED("1"),
    SENT_TO_CMS("2"),
    SENT_TO_MARKET("3"),
    SENT_ACKNOWLEDGED("4"),
    CANCEL_REQUESTED("5"),
    MODIFIED("6"),
    BROKER_RELEASE("7"),
    SYSTEM_REJECTED("8"),
    MARKET_REJECTED("9"),
    CANCEL_CONFIRMED("10"),
    CANCEL_REJECTED("11"),
    EXPIRED("12"),
    EXECUTED("13"),
    EXECUTION_ADJUSTED("14"),
    EXECUTION_REVERSED("15"),
    CANCELLATION_REVERSED("16"),
    EXPIRATION_REVERSED("17"),
    OPT_POSITION_ASSIGNED("18"),
    OPT_POSITION_EXPIRED("19"),
    OPEN_ORDER_ADJUSTED("20"),
    OPT_POSITION_EXERCISED("21"),
    CA_CANCELLED("22"),
    CA_BOOKED("23"),
    IPO_ALLOCATED("24"),
    DONE_TRADE_EXECUTED("25"),
    REJECTION_REVERSED("26"),
    OPT_POSITION_DNE("27"),
    UNKNOWN("-1");

    companion object {

        fun getEventType(typeCode: String): EventType {
            return values().firstOrNull { typeCode == it.typeCode } ?: UNKNOWN
        }
    }
}

enum class OrderTerm(
    val typeCode: String
) {
    GOOD_TILL_DATE("4"),
    GOOD_UNTIL_CANCEL("2"),
    GOOD_FOR_DAY("3"),
    IMMEDIATE_OR_CANCEL("5"),
    FILL_OR_KILL("6"),
    EXTENDED_HOUR_DAY("7"),
    EXTENDED_HOUR_IOC("8"), // IMMEDIATE_OR_CANCEL
    GOOD_UNTIL_CANCEL_DNI("9"),
    GOOD_UNTIL_CANCEL_DNR("10"),
    GOOD_TILL_DATE_DNI("11"),
    GOOD_TILL_DATE_DNR("12"),
    UNKNOWN("-1");

    companion object {
        fun getOrderTerm(
            typeCode: String,
            marketSession: MarketSession,
            isGoodTillDate: Boolean,
            execInstruction: ExecInstruction,
            orderSpecialAttribCd: String
        ): OrderTerm {
            var term = getOrderTerm(typeCode, execInstruction, orderSpecialAttribCd, isGoodTillDate)
            if (MarketSession.MARKET_AFTERHOUR == marketSession) {
                if (GOOD_FOR_DAY == term) {
                    term = EXTENDED_HOUR_DAY
                }
                if (IMMEDIATE_OR_CANCEL == term) {
                    term = EXTENDED_HOUR_IOC
                }
            }
            return term
        }

        fun mapOrderTerm(typeCode: String): OrderTerm =
            values().firstOrNull { it.typeCode == typeCode } ?: UNKNOWN

        private fun getOrderTerm(
            typeCode: String,
            execInstruction: ExecInstruction,
            orderSpecialAttribCd: String,
            isGoodTillDate: Boolean
        ): OrderTerm {
            var localTypeCode = typeCode
            if (localTypeCode == GOOD_UNTIL_CANCEL.typeCode && isGoodTillDate) {
                localTypeCode = GOOD_TILL_DATE.typeCode
            }
            return when (localTypeCode) {
                GOOD_UNTIL_CANCEL.typeCode -> {
                    if (execInstruction.doNotIncreaseFlag) {
                        GOOD_UNTIL_CANCEL_DNI
                    } else if (orderSpecialAttribCd != USER_SELECTED_DNR_FLAG && execInstruction.doNotReduceFlag) {
                        GOOD_UNTIL_CANCEL_DNR
                    } else {
                        GOOD_UNTIL_CANCEL
                    }
                }
                GOOD_TILL_DATE.typeCode -> {
                    if (execInstruction.doNotIncreaseFlag) {
                        GOOD_TILL_DATE_DNI
                    } else if (orderSpecialAttribCd != USER_SELECTED_DNR_FLAG && execInstruction.doNotReduceFlag) {
                        GOOD_TILL_DATE_DNR
                    } else {
                        GOOD_TILL_DATE
                    }
                }
                else -> values().firstOrNull { typeCode == it.typeCode } ?: UNKNOWN
            }
        }
    }
}

enum class OrigSysCode(
    val numberCode: String,
    val textCode: String
) {
    ORIG_SYS_UNSPECIFIED("0", ""),
    ORIG_SYS_CODE_ABWT("1", "ORIG_SYS_CODE_ABWT"),
    ORIG_SYS_CODE_DETW("2", "ORIG_SYS_CODE_DETW"),
    ORIG_SYS_CODE_FONE("3", "ORIG_SYS_CODE_FONE"),
    ORIG_SYS_CODE_DRCT("4", "ORIG_SYS_CODE_DRCT"),
    ORIG_SYS_CODE_EINV("5", "ORIG_SYS_CODE_EINV"),
    ORIG_SYS_CODE_PVTA("6", "ORIG_SYS_CODE_PVTA"),
    ORIG_SYS_CODE_ETS("7", "ORIG_SYS_CODE_ETS"),
    ORIG_SYS_CODE_OL("8", "ORIG_SYS_CODE_OL"),
    ORIG_SYS_CODE_EGLC("9", "ORIG_SYS_CODE_EGLC"),
    ORIG_SYS_CODE_WBST("10", "ORIG_SYS_CODE_WBST"),
    ORIG_SYS_CODE_WBSO("11", "ORIG_SYS_CODE_WBSO"),
    ORIG_SYS_CODE_WBSI("12", "ORIG_SYS_CODE_WBSI"),
    ORIG_SYS_CODE_WBSG("13", "ORIG_SYS_CODE_WBSG"),
    ORIG_SYS_CODE_TDCP("14", "ORIG_SYS_CODE_TDCP"),
    ORIG_SYS_CODE_EGNO("15", "ORIG_SYS_CODE_EGNO"),
    ORIG_SYS_CODE_IVRI("16", "ORIG_SYS_CODE_IVRI"),
    ORIG_SYS_CODE_EGSE("17", "ORIG_SYS_CODE_EGSE"),
    ORIG_SYS_CODE_EGDE("18", "ORIG_SYS_CODE_EGDE"),
    ORIG_SYS_CODE_EGIL("19", "ORIG_SYS_CODE_EGIL"),
    ORIG_SYS_CODE_BRKR("20", "ORIG_SYS_CODE_BRKR"),
    ORIG_SYS_CODE_PET("21", "ORIG_SYS_CODE_PET"),
    ORIG_SYS_CODE_TDSK("22", "ORIG_SYS_CODE_TDSK"),
    ORIG_SYS_CODE_WIRE("23", "ORIG_SYS_CODE_WIRE"),
    ORIG_SYS_CODE_BRDT("24", "ORIG_SYS_CODE_BRDT"),
    ORIG_SYS_CODE_DEUS("25", "ORIG_SYS_CODE_DEUS"),
    ORIG_SYS_CODE_IPOF("26", "ORIG_SYS_CODE_IPOF"),
    ORIG_SYS_CODE_WBSD("27", "ORIG_SYS_CODE_WBSD"),
    ORIG_SYS_CODE_MFAD("28", "ORIG_SYS_CODE_MFAD"),
    ORIG_SYS_CODE_COE("29", "ORIG_SYS_CODE_COE"),
    ORIG_SYS_CODE_MKCT("30", "ORIG_SYS_CODE_MKCT"),
    ORIG_SYS_CODE_AUTO("31", "ORIG_SYS_CODE_AUTO"),
    ORIG_SYS_CODE_BALI("32", "ORIG_SYS_CODE_BALI"),
    ORIG_SYS_CODE_OADV("33", "ORIG_SYS_CODE_OADV"),
    ORIG_SYS_CODE_WMAD("34", "ORIG_SYS_CODE_WMAD"),
    ORIG_SYS_CODE_WMFB("35", "ORIG_SYS_CODE_WMFB"),
    ORIG_SYS_CODE_WMRT("36", "ORIG_SYS_CODE_WMRT"),
    ORIG_SYS_CODE_WMAT("37", "ORIG_SYS_CODE_WMAT"),
    ORIG_SYS_CODE_WMRB("38", "ORIG_SYS_CODE_WMRB"),
    ORIG_SYS_CODE_MPRO("39", "ORIG_SYS_CODE_MPRO"),
    ORIG_SYS_CODE_PROXL("40", "ORIG_SYS_CODE_PROXL"),
    ORIG_SYS_CODE_ETKO("41", "ORIG_SYS_CODE_ETKO"),
    ORIG_SYS_CODE_ETCA("42", "ORIG_SYS_CODE_ETCA"),
    ORIG_SYS_CODE_BT("43", "ORIG_SYS_CODE_BT"),
    ORIG_SYS_CODE_BTDT("44", "ORIG_SYS_CODE_BTDT"),
    ORIG_SYS_CODE_IPHN("45", "ORIG_SYS_CODE_IPHN"),
    ORIG_SYS_CODE_OLAQ("46", "ORIG_SYS_CODE_OLAQ"),
    ORIG_SYS_CODE_ETDE("47", "ORIG_SYS_CODE_ETDE"),
    ORIG_SYS_CODE_ETJP("48", "ORIG_SYS_CODE_ETJP"),
    ORIG_SYS_CODE_ETNO("49", "ORIG_SYS_CODE_ETNO"),
    ORIG_SYS_CODE_IPO("50", "ORIG_SYS_CODE_IPO"),
    ORIG_SYS_CODE_TRAPI("51", "ORIG_SYS_CODE_TRAPI"),
    ORIG_SYS_CODE_APL("52", "ORIG_SYS_CODE_APL"),
    ORIG_SYS_CODE_BDSK("53", "ORIG_SYS_CODE_BDSK"),
    ORIG_SYS_CODE_ETSE("54", "ORIG_SYS_CODE_ETSE"),
    ORIG_SYS_CODE_IPAD("55", "ORIG_SYS_CODE_IPAD"),
    ORIG_SYS_CODE_MYET("56", "ORIG_SYS_CODE_MYET"),
    ORIG_SYS_CODE_ANDR("57", "ORIG_SYS_CODE_ANDR"),
    ORIG_SYS_CODE_AIP("58", "ORIG_SYS_CODE_AIP"),
    ORIG_SYS_CODE_OH("59", "ORIG_SYS_CODE_OH");

    companion object {
        const val origSysCodeKey: String = "origSysCode"

        fun fromOrderListResponse(code: String): OrigSysCode {
            return values().firstOrNull { code == it.numberCode } ?: ORIG_SYS_UNSPECIFIED
        }

        fun fromOrderDetailResponse(code: String): OrigSysCode {
            return values().firstOrNull { code == it.textCode } ?: ORIG_SYS_UNSPECIFIED
        }
    }
}

enum class AdvancedOrderSecurityType(
    val numberCode: String,
    val textCode: String
) {
    EQ("1", "EQ"),
    OPTN("2", "OPTN"),
    INDX("3", "INDX"),
    UNKNOWN("", "");

    companion object {
        fun getSecurityType(code: String): AdvancedOrderSecurityType {
            return values().firstOrNull { code == it.textCode || code == it.numberCode } ?: UNKNOWN
        }
    }
}

@Suppress("MagicNumber")
enum class MarketSession(val code: Int) {
    MARKET_UNSPECIFIED(0),
    MARKET_REGULAR(1),
    MARKET_AFTERHOUR(2),
    MARKET_OVERNIGHT(3);

    companion object {
        fun getMarketSession(code: Int?): MarketSession {
            code?.let {
                return values().firstOrNull { code == it.code } ?: MARKET_UNSPECIFIED
            }
            return MARKET_UNSPECIFIED
        }
    }
}

enum class OrderIdentifier {
    CONDITIONAL, STOCK, CONTINGENT, OPTION, BRACKETED
}

enum class ContingentQualifier(
    val followPrice: String
) {
    ASK("1"),
    BID("2"),
    LAST_PRICE("3"),
    UNKNOWN("");

    companion object {
        fun fromFollowPrice(followPrice: String?) = followPrice?.let { type ->
            values().firstOrNull { type == it.followPrice }
        } ?: UNKNOWN
    }
}

enum class ContingentCondition(
    val offsetType: String
) {
    GREATER_THAN_OR_EQUAL("3"),
    LESS_THAN_OR_EQUAL("4"),
    UNKNOWN("");

    companion object {
        fun fromOffsetType(offsetType: String?) = offsetType?.let { type ->
            values().firstOrNull { type == it.offsetType }
        } ?: UNKNOWN
    }
}

enum class TrailingStopType(
    val offsetType: String
) {
    TRAILING_STOP_PERCENT("2"),
    TRAILING_STOP_DOLLAR("");

    companion object {
        fun fromOffsetType(offsetType: String?) = offsetType?.let { type ->
            values().firstOrNull { type == it.offsetType }
        } ?: TRAILING_STOP_DOLLAR
    }
}

@Suppress("MagicNumber")
enum class MarketDestination(val code: Int, val codeString: String, val symbol: String) {

    AUTO(1, "AUTO", "AUTO"),
    AMEX(2, "NYSE MKT", "AMEX"),
    ARCA(3, "ARCA (7am - 8pm ET)", "ARCA"),
    BOX(4, "BOX", "BOX"),
    CBOE(5, "CBOE", "CBOE"),
    ISE(6, "ISE", "ISE"),
    NOM(7, "NOM", "NOM"),
    NSDQ(8, "NSDQ (7am - 8pm ET)", "NSDQ"),
    NYSE(9, "NYSE", "NYSE"),
    PCX(10, "NYSE", "PCX"), // used for option NYSE
    PHX(11, "PHX", "PHX"),
    INET(12, "INET", "INET"),
    ISLD(13, "ISLD", "ISLD"),
    INCA(14, "INCA", "INCA"),
    UNKNOWN(-1, "", "");

    companion object {
        fun getMarketDestination(codeString: String): MarketDestination {
            return values().firstOrNull { codeString == it.codeString || codeString == it.symbol }
                ?: UNKNOWN
        }
    }
}
