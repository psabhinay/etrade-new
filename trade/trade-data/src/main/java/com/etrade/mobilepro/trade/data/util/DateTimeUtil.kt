package com.etrade.mobilepro.trade.data.util

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Locale
import java.util.TimeZone

private val orderPlacedFormat: ThreadLocal<DateFormat> = object : ThreadLocal<DateFormat>() {
    override fun initialValue(): DateFormat {
        return SimpleDateFormat("MM/dd/yyyy hh:mm:ss a z", Locale.US).apply {
            timeZone = TimeZone.getTimeZone("EST5EDT")
        }
    }
}

internal fun formatOrderPlacedTime(timeMs: Long?): String? = timeMs?.let { orderPlacedFormat.get().format(it) }
