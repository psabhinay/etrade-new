package com.etrade.mobilepro.util.domain.data

import com.etrade.mobilepro.util.safeParseBigDecimal
import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Calculates P/E based on EPS and Price.
 */
fun derivePriceToEarnings(earningsPerShare: String?, price: String?): BigDecimal? {
    val earningsPerShareDecimal = earningsPerShare?.safeParseBigDecimal() ?: return null
    val decimalPrice = price?.safeParseBigDecimal() ?: return null
    return if (earningsPerShareDecimal > BigDecimal.ZERO) {
        decimalPrice.divide(earningsPerShareDecimal, 2, RoundingMode.HALF_UP)
    } else {
        BigDecimal.ZERO
    }
}

fun deriveInitialPrice(lastPrice: String?, change: String?): String? = safeSubtract(lastPrice, change)

fun deriveChange(initialPrice: String?, lastPrice: String?): String? = safeSubtract(lastPrice, initialPrice)

fun deriveChangePercent(initialPrice: String?, change: String?): String? {
    if (initialPrice == null || change == null) {
        return null
    }

    val divisor = initialPrice.safeParseBigDecimal() ?: return null

    return if (divisor.signum() == 0) {
        divisor.toPlainString()
    } else runCatching {
        BigDecimal(change)
            .times(BigDecimal(PERCENT_MULTIPLIER))
            .divide(divisor, DIVIDE_SCALE, RoundingMode.HALF_UP)
            .toPlainString()
    }.getOrNull()
}

fun deriveDayChange(
    extendedHourLast: String?,
    closingPrice: String?,
    nonExtendedHoursChange: String?
): String? = deriveDayChange(
    extendedHourChange = deriveChange(initialPrice = closingPrice, lastPrice = extendedHourLast),
    nonExtendedHoursChange = nonExtendedHoursChange
)

fun deriveDayChange(
    extendedHourChange: String?,
    nonExtendedHoursChange: String?
): String? = safeAdd(nonExtendedHoursChange, extendedHourChange)

fun deriveDayChangePercent(
    dayChange: String?,
    closingPrice: String?,
    nonExtendedHoursChange: String?
): String? = deriveChangePercent(
    initialPrice = deriveInitialPrice(lastPrice = closingPrice, change = nonExtendedHoursChange),
    change = dayChange
)

private fun safeSubtract(first: String?, second: String?): String? {
    if (first == null || second == null) {
        return null
    }
    return runCatching {
        BigDecimal(first)
            .subtract(BigDecimal(second))
            .toPlainString()
    }.getOrNull()
}

private fun safeAdd(first: String?, second: String?): String? {
    if (first == null || second == null) {
        return null
    }
    return runCatching {
        BigDecimal(first)
            .add(BigDecimal(second))
            .toPlainString()
    }.getOrNull()
}
