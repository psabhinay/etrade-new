package com.etrade.mobilepro.order.details.presentation.delegate

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dialog.confirmationDialog
import com.etrade.mobilepro.dynamic.form.api.Action
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.getSymbolUnderlier
import com.etrade.mobilepro.instrument.isSymbolLongOption
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderCancelRequest
import com.etrade.mobilepro.order.details.api.OrderData
import com.etrade.mobilepro.order.details.api.OrderDetailDescriptionMapperType
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.api.OrderDetailsRepo
import com.etrade.mobilepro.order.details.api.OrderDetailsRequestParams
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem
import com.etrade.mobilepro.order.details.presentation.OrderDetailItemInterfaceMapperType
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel.ActionsArgument
import com.etrade.mobilepro.order.details.presentation.OrderOptionsRatio
import com.etrade.mobilepro.order.details.presentation.R
import com.etrade.mobilepro.order.details.presentation.helper.cancelOrderAction
import com.etrade.mobilepro.order.details.presentation.helper.deleteOrderAction
import com.etrade.mobilepro.order.details.presentation.helper.editOrderAction
import com.etrade.mobilepro.order.details.presentation.helper.getDefaultStaticBidAskDelegate
import com.etrade.mobilepro.order.details.presentation.helper.getStaticBidAskDelegate
import com.etrade.mobilepro.order.details.presentation.helper.mergeResult
import com.etrade.mobilepro.order.details.presentation.helper.needToFetchStrategy
import com.etrade.mobilepro.order.details.presentation.helper.newOrderAction
import com.etrade.mobilepro.order.details.presentation.helper.previewOrderAction
import com.etrade.mobilepro.order.details.router.GeneralOrderDetailsArgument
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.order.details.router.PreviewOrderDetailsArgument
import com.etrade.mobilepro.order.details.router.isOpen
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.orders.api.extractOrderStatus
import com.etrade.mobilepro.orders.extensions.addContingentOrderValues
import com.etrade.mobilepro.orders.extensions.android.asSingleMessage
import com.etrade.mobilepro.orders.extensions.android.cancelOrderDialog
import com.etrade.mobilepro.orders.extensions.android.cancelSuccessDialog
import com.etrade.mobilepro.orders.extensions.applyOrderDetailValues
import com.etrade.mobilepro.orders.extensions.cancelOrder
import com.etrade.mobilepro.orders.extensions.getDisabledInputFields
import com.etrade.mobilepro.orders.extensions.getSecurityType
import com.etrade.mobilepro.orders.presentation.DefaultOrderButtonVisibilityManager
import com.etrade.mobilepro.orders.presentation.OrderButtonVisibilityManager
import com.etrade.mobilepro.orders.presentation.orderButtonVisibilityManager
import com.etrade.mobilepro.quote.mapper.MobileQuoteWidgetItemMapper
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.api.option.OptionTradeRepo
import com.etrade.mobilepro.trade.api.option.OptionTradeStrategyResponse
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormInputIdMap
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.input.WithContingentOrderInputId
import com.etrade.mobilepro.trade.optionlegselect.TradeLegBidAskViewModel
import com.etrade.mobilepro.trade.presentation.usecase.PreviewOrderUseCase
import com.etrade.mobilepro.trade.presentation.usecase.PreviewOrderUseCaseParams
import com.etrade.mobilepro.trade.presentation.util.TradeExceptionViewModelDelegate
import com.etrade.mobilepro.trade.presentation.util.TradePreviewViewModelDelegate
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.invoke
import com.etrade.mobilepro.util.safeLet
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.rx2.await
import kotlinx.coroutines.rx2.awaitLast
import java.math.BigDecimal
import javax.inject.Inject
import javax.inject.Provider

@Suppress("LargeClass", "TooManyFunctions")
class GeneralOrderDetailsDelegate @Inject constructor(
    private val orderDetailsRepo: OrderDetailsRepo,
    private val orderMapper: OrderDetailItemInterfaceMapperType,
    private val orderDetailDescriptionMapper: OrderDetailDescriptionMapperType,
    private val optionTradeRepo: OptionTradeRepo,
    private val previewOrderUseCase: PreviewOrderUseCase,
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    private val tradeFormInputIdMap: TradeFormInputIdMap,
    private val resource: Resources,
    private val quoteService: MobileQuoteRepo,
    private val quoteMapper: MobileQuoteWidgetItemMapper,
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase,
    private val user: User,
    @Web
    private val baseUrl: String
) : OrderDetailsViewModel.BaseOrderDetailsDelegate() {

    override val noActionsMessage: CharSequence
        get() = resource.getString(R.string.orders_message_not_supported_order)

    override val loadingIndicators: Set<LiveData<Boolean>>
        get() = setOf(cancelLoadingIndicator, deleteLoadingIndicator, previewLoadingIndicator)

    private val cancelLoadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)
    private val deleteLoadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)
    private val previewLoadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)

    private var firstOrderDetailsItem: BaseOrderDetailsItem.OrderDetailsItem? = null
    private var strategyType: StrategyType? = null

    private val tradeExceptionDelegate by lazy { TradeExceptionViewModelDelegate(resource, baseUrl, mutableDialog, emptyMap(), onOptionsApprovalUpgrade) }

    override suspend fun loadItems(
        argument: OrderDetailsArgument,
        quotes: Deferred<Map<String, OrderDetailItemInterface>>
    ): ETResult<OrderDetailsViewModel.OrderDetails> {

        require(argument is GeneralOrderDetailsArgument)

        val accountId = argument.accountId
        val orderNumber = argument.orderNumber
        val observable = if (argument.orderStatus == OrderStatus.SAVED) {
            orderDetailsRepo.getSavedOrderDetails(OrderDetailsRequestParams(accountId, orderNumber, OrderStatus.SAVED))
        } else {
            orderDetailsRepo.getOrderDetails(OrderDetailsRequestParams(accountId, orderNumber, OrderStatus.ALL))
        }

        return observable
            .runCatchingET { awaitLast() }
            .mapCatching { orderItemDetailList -> toOrderDetails(argument, orderItemDetailList) }
    }

    private suspend fun toOrderDetails(
        argument: GeneralOrderDetailsArgument,
        orderItemDetailList: List<BaseOrderDetailsItem>
    ): OrderDetailsViewModel.OrderDetails {
        val first = orderItemDetailList[0] as BaseOrderDetailsItem.OrderDetailsItem
        firstOrderDetailsItem = first
        val legs = first.tradeLegs
        val optionLegs = legs.filter { it.transactionType.isOption }
        val hasOption = optionLegs.isNotEmpty()
        val (quoteWidgetViewModel, tradeLegBidAskViewModel) = getQuoteDetailsViewModels()
        if (hasOption) {
            tradeLegBidAskViewModel.subscribeToBidAsk(legs, streamOptions = false, shouldRunBlocking = true)
        }
        val hasStock = legs.any { !it.transactionType.isOption }
        strategyType = resolveOrderStrategy(first)

        val quotesMap = orderItemDetailList.getQuotes()

        val items = transformDetailsList(orderItemDetailList, argument.orderType, hasStock, hasOption, tradeLegBidAskViewModel)

        return OrderDetailsViewModel.OrderDetails(
            combine(CombineArguments(argument, hasOption, hasStock, quoteWidgetViewModel), items, quotesMap),
            orderItemDetailList.getOrderStatus(),
            firstOrderDetailsItem,
            orderItemDetailList.filterIsInstance<BaseOrderDetailsItem.OrderDetailsItem>()
        )
    }

    private suspend fun resolveOrderStrategy(item: BaseOrderDetailsItem.OrderDetailsItem): StrategyType? {
        return if (item.needToFetchStrategy()) {
            fetchOrderStrategy(item)?.strategyType
        } else {
            item.strategyType
        }
    }

    private suspend fun fetchOrderStrategy(item: BaseOrderDetailsItem.OrderDetailsItem): OptionTradeStrategyResponse? {
        val tradeLegs = item.orderDescriptions.map { orderDetailDescriptionMapper(it, it.isPartialOrder) }
        val symbol = item.orderDescriptions[0].underlyingProductId
        return optionTradeRepo.getStrategyIdentifier(symbol, tradeLegs, item.account, item.orderType).getOrNull()
    }

    private fun getBidAskDelegate(
        detailsItem: BaseOrderDetailsItem,
        hasOption: Boolean,
        tradeLegBidAskViewModel: TradeLegBidAskViewModel
    ): OrderDetailDisplayItem.OrderBidAskItem =
        when (detailsItem) {
            is BaseOrderDetailsItem.OrderDetailsItem -> {
                if (hasOption && detailsItem.hasMultipleLegs) {
                    OrderDetailDisplayItem.OrderBidAskItem.DelegatedOrderBidAskItem(tradeLegBidAskViewModel.quoteBidAskItem)
                } else {
                    if (!detailsItem.hasMultipleLegs) {
                        OrderDetailDisplayItem.OrderBidAskItem.DynamicOrderBidAskItemWithSymbol(detailsItem.orderDescriptions[0].originalSymbol)
                    } else {
                        getStaticBidAskDelegate(detailsItem)
                    }
                }
            }
            is BaseOrderDetailsItem.AdvancedOrderItem -> OrderDetailDisplayItem.OrderBidAskItem.DynamicOrderBidAskItemWithSymbol(detailsItem.symbol)
            is BaseOrderDetailsItem.StockPlanOrderDetailsItem,
            is BaseOrderDetailsItem.PreviewOrderDetailsItem -> getDefaultStaticBidAskDelegate()
        }

    private fun transformDetailsList(
        items: List<BaseOrderDetailsItem>,
        order: OrderType,
        hasStock: Boolean,
        hasOption: Boolean,
        tradeLegBidAskViewModel: TradeLegBidAskViewModel
    ): List<List<OrderDetailItemInterface>> {
        val formattedRatio = if (hasStock && hasOption) {
            OrderOptionsRatio(tradeLegBidAskViewModel.normalizedRatio)
        } else {
            null
        }
        return items.mapIndexed { index: Int, orderDetailsItem: BaseOrderDetailsItem ->
            val detailsItem = (orderDetailsItem as? BaseOrderDetailsItem.OrderDetailsItem)?.let { item ->
                strategyType?.let { type -> item.copy(strategyType = type) }
            } ?: orderDetailsItem
            orderMapper(detailsItem, index, order, getBidAskDelegate(detailsItem, hasOption, tradeLegBidAskViewModel), formattedRatio)
        }
    }

    private fun combine(
        arguments: CombineArguments,
        details: List<List<OrderDetailItemInterface>>,
        quotes: Map<String, OrderDetailItemInterface>
    ): List<OrderDetailItemInterface> {

        val results = mutableListOf<OrderDetailItemInterface>()

        details.forEach { orderItems ->
            val hasHeaderSection = orderItems.any { it is OrderDetailDisplayItem.OrderSectionHeader }
            if (hasHeaderSection) {
                results.add(orderItems.filterIsInstance<OrderDetailDisplayItem.OrderSectionHeader>().first())
            }
            addQuoteDetails(arguments, orderItems, quotes, results)
            if (hasHeaderSection) {
                results.addAll(orderItems.subList(1, orderItems.size))
            } else {
                results.addAll(orderItems)
            }
        }

        return results
    }

    private suspend fun List<BaseOrderDetailsItem>.getQuotes(): Map<String, OrderDetailItemInterface> {
        val symbols: String = mapNotNull { item ->
            when (item) {
                is BaseOrderDetailsItem.OrderDetailsItem -> item.getSymbol()
                is BaseOrderDetailsItem.AdvancedOrderItem -> item.getSymbol()
                else -> null
            }
        }.distinct().joinToString(separator = ",")

        return quoteService.loadMobileQuotes(symbols = symbols, extendedHours = isExtendedHoursOnUseCase())
            .runCatching { awaitLast() }
            .map {
                quoteMapper.map(it)
            }
            .getOrDefault(emptyMap())
    }

    private fun BaseOrderDetailsItem.OrderDetailsItem.getSymbol() = orderDescriptions.joinToString(separator = ",") {
        if (it.underlyingProductId.isNotBlank()) {
            "${it.originalSymbol},${it.underlyingProductId}"
        } else {
            it.originalSymbol
        }
    }

    private fun BaseOrderDetailsItem.AdvancedOrderItem.getSymbol(): String {
        val resolvedSymbol = if (symbol.isNotBlank()) {
            symbol
        } else {
            displaySymbol
        }
        return if (isSymbolLongOption(resolvedSymbol)) {
            "$resolvedSymbol,${getSymbolUnderlier(resolvedSymbol, InstrumentType.OPTN)}"
        } else {
            resolvedSymbol
        }
    }

    private fun addQuoteDetails(
        arguments: CombineArguments,
        orderItems: List<OrderDetailItemInterface>,
        quotes: Map<String, OrderDetailItemInterface>,
        results: MutableList<OrderDetailItemInterface>
    ) {
        val orderLegItems = orderItems.filterIsInstance<OrderDetailDisplayItem.OrderLegItem>()
        val orderLegItem = orderLegItems.first()
        checkAdvancedOrderSymbol(arguments.argument, orderLegItem)
        val quote = quotes[orderLegItem.originalSymbol] ?: quotes[orderLegItem.underlyingProductId]
        quote?.also { quoteWidget ->
            results.add(quoteWidget)
            if (arguments.hasStock) {
                val quoteItem = quoteWidget as QuoteWidgetItem
                if (arguments.hasOption) {
                    arguments.quoteWidgetViewModel.updateQuotes(quoteItem)
                } else {
                    mergeResult(orderItems, quoteItem)
                }
            }
        }
    }

    override fun createActions(argument: ActionsArgument, manager: OrderButtonVisibilityManager): List<Action> {
        val orderDetailsArgument = argument.orderDetailsArgument
        require(orderDetailsArgument is GeneralOrderDetailsArgument)

        return with(manager) {
            listOfNotNull(
                newOrderAction(resource) {
                    openTradeSignal(formOrderData(orderDetailsArgument, false))
                },
                cancelOrderAction(resource) {
                    confirmCancel(orderDetailsArgument, manager)
                },
                deleteOrderAction(resource) {
                    confirmDelete(orderDetailsArgument)
                },
                editOrderAction(resource, orderDetailsArgument) {
                    openTradeSignal(formOrderData(orderDetailsArgument, true))
                },
                previewOrderAction(resource) {
                    previewOrder(orderDetailsArgument, argument.orderDetailsItems)
                }
            )
        }
    }

    override fun createButtonVisibilityManager(argument: OrderDetailsArgument): OrderButtonVisibilityManager {
        require(argument is GeneralOrderDetailsArgument)

        val item = firstOrderDetailsItem ?: return orderButtonVisibilityManager()

        return DefaultOrderButtonVisibilityManager(
            orderType = argument.orderType,
            orderStatus = argument.orderStatus,
            advancedOrderType = item.advancedItem?.orderType ?: AdvancedOrderType.UNKNOWN,
            priceType = item.priceType,
            orderTerm = item.term,
            origSysCode = item.origSysCode,
            hasDependentFlag = item.hasDependentFlag,
            dependentOrderNo = item.dependentOrderNumber,
            isMultiLeg = item.hasMultipleLegs,
            qtyReserveShow = item.orderDescriptions[0].qtyReserveShow ?: BigDecimal.ZERO,
            hiddenFlag = item.hiddenFlag,
            goodThroughDateFlag = item.goodTillDateFlag,
            instrumentType = item.orderDescriptions[0].typeCode,
            marketSession = item.marketSession,
            stopPrice = item.stopPrice ?: BigDecimal.ZERO,
            execInstruction = item.execInstruction,
            orderSpecialAttribCd = item.orderSpecialAttribCd
        )
    }

    // Some orders we get empty symbol and display symbol, and we have to get this information from order list and update the value here
    private fun checkAdvancedOrderSymbol(argument: GeneralOrderDetailsArgument, orderLegItem: OrderDetailDisplayItem.OrderLegItem) {
        if (orderLegItem.isAdvancedOrder) {
            if (orderLegItem.originalSymbol.isEmpty()) {
                orderLegItem.originalSymbol = argument.advancedOrderSymbol ?: ""
            }
            if (orderLegItem.displaySymbol.isEmpty()) {
                orderLegItem.displaySymbol = argument.advancedOrderDisplaySymbol ?: ""
            }
        }
    }

    private fun createOrderCancelRequest(argument: GeneralOrderDetailsArgument): OrderCancelRequest? {
        return firstOrderDetailsItem?.let {
            argument.run {
                OrderCancelRequest(
                    accountId,
                    user.getUserId().toString(),
                    "android",
                    it.orderDescriptions[0].typeCode,
                    it.term.typeCode,
                    it.origSysCode,
                    it
                )
            }
        }
    }

    private fun confirmCancel(argument: GeneralOrderDetailsArgument, manager: OrderButtonVisibilityManager) {
        mutableDialog.value = resource.cancelOrderDialog(
            positiveAction = {
                cancelOrder(argument, manager)
                resetActionStates()
            },
            negativeAction = ::resetActionStates
        )
    }

    private fun cancelOrder(argument: GeneralOrderDetailsArgument, manager: OrderButtonVisibilityManager) {
        val request = createOrderCancelRequest(argument) ?: return

        val buttonManager = manager as DefaultOrderButtonVisibilityManager

        executeOperation(
            loadingIndicator = cancelLoadingIndicator,
            operation = {
                executeCancel(argument, buttonManager, request)
            },
            onSuccess = {
                sendOrderCanceledSignal(argument.orderId)
                onSuccessCancelOrder(argument, buttonManager, it)
                refresh()
            }
        )
    }

    private fun onSuccessCancelOrder(
        argument: GeneralOrderDetailsArgument,
        manager: DefaultOrderButtonVisibilityManager,
        serverMessages: List<ServerMessage>
    ) {
        val error = serverMessages.firstOrNull { it.type == ServerMessage.Type.ERROR }
        if (error != null) {
            postError(ErrorMessage(error.text) { cancelOrder(argument, manager) })
        } else {
            val dialogMessage = if (serverMessages.isEmpty()) {
                resource.getString(R.string.orders_dialog_cancel_order_success_message)
            } else {
                serverMessages.asSingleMessage()
            }
            mutableDialog.value = resource.cancelSuccessDialog(dialogMessage, ::resetActionStates)
        }
    }

    private suspend fun executeCancel(
        argument: GeneralOrderDetailsArgument,
        manager: DefaultOrderButtonVisibilityManager,
        orderCancelRequest: OrderCancelRequest
    ): ETResult<List<ServerMessage>> {
        return orderDetailsRepo.cancelOrder(
            orderIdentifier = manager.orderIdentifier,
            request = orderCancelRequest,
            orderType = argument.orderType,
            advancedOrderType = firstOrderDetailsItem?.advancedItem?.orderType
        ).runCatchingET { await() }
    }

    private fun confirmDelete(argument: GeneralOrderDetailsArgument) {
        mutableDialog.value = resource.confirmationDialog(
            title = R.string.orders_action_delete_order,
            message = R.string.orders_dialog_delete_order_confirm_message,
            positiveAction = {
                deleteOrder(argument)
                resetActionStates()
            },
            negativeAction = ::resetActionStates
        )
    }

    private fun deleteOrder(argument: GeneralOrderDetailsArgument) {
        executeOperation(
            loadingIndicator = deleteLoadingIndicator,
            operation = {
                orderDetailsRepo.deleteOrder(
                    accountId = argument.accountId,
                    orderId = argument.orderId
                )
            },
            onSuccess = {
                sendOrderDeletedSignal(argument.orderNumber)
            }
        )
    }

    private fun previewOrder(orderDetailsArgument: GeneralOrderDetailsArgument, orderDetailsItems: List<BaseOrderDetailsItem.OrderDetailsItem>?) {
        executeOperation(
            loadingIndicator = previewLoadingIndicator,
            operation = {
                previewOrderUseCase(
                    PreviewOrderUseCaseParams(
                        buildInputValuesSnapshot(
                            argument = orderDetailsArgument,
                            securityType = orderDetailsArgument.resolveSecurityType(),
                            orderDetailsItems = orderDetailsItems
                        ),
                        changeOrder = false
                    )
                )
            },
            onSuccess = {
                TradePreviewViewModelDelegate(resource, baseUrl, mutableDialog) { argument ->
                    (argument as? PreviewOrderDetailsArgument)?.let { previewOrderDetailsArgument ->
                        openPreviewSignal(previewOrderDetailsArgument.copy(allowCancel = false))
                    }
                }.handlePreview(it)
            },
            interceptFailure = {
                tradeExceptionDelegate.handleTradeOrderException(it, orderDetailsArgument.accountId)
            }
        )
    }

    private fun List<BaseOrderDetailsItem>.getOrderStatus(): OrderStatus {
        val allStatusList = filterIsInstance<BaseOrderDetailsItem.OrderDetailsItem>()
            .map {
                it.orderStatus
            }
        return extractOrderStatus(allStatusList)
    }

    private fun buildInputValuesSnapshot(
        argument: GeneralOrderDetailsArgument,
        securityType: SecurityType,
        orderDetailsItems: List<BaseOrderDetailsItem.OrderDetailsItem>?
    ): InputValuesSnapshot {
        return if (orderDetailsItems != null && securityType == SecurityType.CONDITIONAL) {
            createConditionalOrderSnapshot(argument, orderDetailsItems)
        } else {
            createSimpleOrderSnapshot(argument, securityType, includeOrderValues = true).toMutableMap().apply {
                if (orderDetailsItems != null && argument.orderType == OrderType.CONTINGENT) {
                    val item = orderDetailsItems.firstOrNull()?.advancedItem
                    val inputId = tradeFormInputIdMap.getInputId(TradeFormType(securityType, AdvancedTradeType.Contingent)) as? WithContingentOrderInputId
                    safeLet(item, inputId) { advancedOrderItem, withContingentOrderId ->
                        addContingentOrderValues(withContingentOrderId.contingentOrderInputId, advancedOrderItem, argument.advancedOrderSymbol)
                    }
                }
            }
        }
    }

    private fun createConditionalOrderSnapshot(
        argument: GeneralOrderDetailsArgument,
        orderDetailsItems: List<BaseOrderDetailsItem.OrderDetailsItem>
    ): InputValuesSnapshot {
        val snapshot = mutableMapOf<String, Any?>()
        snapshot.putAll(
            tradeFormParametersBuilderProvider.get().apply {
                accountId = argument.accountId
            }.create(SecurityType.CONDITIONAL)
        )
        orderDetailsItems.forEachIndexed { index, detailsItem ->
            snapshot[index.toString()] = tradeFormParametersBuilderProvider.get().apply {
                accountId = argument.accountId
                detailsItem.orderDescriptions.firstOrNull()?.let { description ->
                    symbol = object : WithSymbolInfo {
                        override val symbol = description.originalSymbol
                        override val instrumentType = description.typeCode
                    }
                }

                applyOrderDetailValues(detailsItem, strategyType)
            }.create(SecurityType.CONDITIONAL)
        }
        return snapshot
    }

    private fun createSimpleOrderSnapshot(
        argument: GeneralOrderDetailsArgument,
        securityType: SecurityType,
        includeOrderValues: Boolean
    ): InputValuesSnapshot {
        return tradeFormParametersBuilderProvider.get().apply {
            accountId = argument.accountId
            if (includeOrderValues) {
                val orderDetails = viewModel.orderDetailsItem as? BaseOrderDetailsItem.OrderDetailsItem
                val originalSymbol = orderDetails?.orderDescriptions?.firstOrNull()?.originalSymbol
                symbol = object : WithSymbolInfo {
                    override val symbol = originalSymbol ?: argument.symbols.first()
                    override val instrumentType = argument.instrumentType
                }
                orderId = argument.orderId
                originalQuantity = argument.originalQuantity

                // Stop-loss order type is not handled here because it is a Conditional Order
                advancedTradeType = if (argument.orderType == OrderType.CONTINGENT) {
                    AdvancedTradeType.Contingent
                } else {
                    AdvancedTradeType.None
                }

                when (val detailsItem = viewModel.orderDetailsItem) {
                    is BaseOrderDetailsItem.OrderDetailsItem -> applyOrderDetailValues(detailsItem, strategyType)
                }
            }
        }.create(securityType)
    }

    private fun formOrderData(argument: GeneralOrderDetailsArgument, isEditOrder: Boolean): OrderData {
        val securityType = viewModel.orderDetailsItem.getSecurityType(argument.instrumentType)
        val formParams = createSimpleOrderSnapshot(argument, securityType, includeOrderValues = isEditOrder)
        val inputsDisabled = if (isEditOrder) {
            tradeFormInputIdMap.getDisabledInputFields(argument.isOpen(), securityType, formParams)
        } else {
            emptyArray()
        }
        return OrderData(
            formParams,
            inputsDisabled,
            getTradeOrderType(isEditOrder, argument.orderStatus)
        )
    }

    private fun GeneralOrderDetailsArgument.resolveSecurityType() = if (orderType.isConditional()) {
        SecurityType.CONDITIONAL
    } else {
        viewModel.orderDetailsItem.getSecurityType(instrumentType)
    }
}

private fun getTradeOrderType(isEditOrder: Boolean, orderStatus: OrderStatus) = if (isEditOrder) {
    if (orderStatus == OrderStatus.SAVED) {
        TradeOrderType.EDIT_SAVED
    } else {
        TradeOrderType.EDIT_OPEN
    }
} else {
    TradeOrderType.NEW
}

private class CombineArguments(
    val argument: GeneralOrderDetailsArgument,
    val hasOption: Boolean,
    val hasStock: Boolean,
    val quoteWidgetViewModel: QuotesWidgetViewModel
)
