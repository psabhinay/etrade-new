package com.etrade.mobilepro.order.details.presentation.delegate

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderDetailDescription
import com.etrade.mobilepro.orders.extensions.getSecurityTypeForMultipleItems
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class UtilityTest {

    @Test
    fun getSecurityTypeForMultipleItems() {
        val testMap: Map<SecurityType, List<InstrumentType>> = mapOf(
            SecurityType.STOCK_AND_OPTIONS to listOf(InstrumentType.OPTNC, InstrumentType.ETF),
            SecurityType.STOCK_AND_OPTIONS to listOf(InstrumentType.OPTNP, InstrumentType.OPTNC, InstrumentType.EQ),
            SecurityType.STOCK to listOf(InstrumentType.ETF, InstrumentType.EQ),
            SecurityType.STOCK to listOf(InstrumentType.ETF),
            SecurityType.STOCK to listOf(InstrumentType.EQ),
            SecurityType.OPTION to listOf(InstrumentType.OPTN),
            SecurityType.OPTION to listOf(InstrumentType.OPTNC),
            SecurityType.OPTION to listOf(InstrumentType.OPTNP)
        )

        testMap.forEach { (expected, params) ->
            val descriptions = params.map { instrumentType ->
                val orderDetailsDescription = mock<OrderDetailDescription>()
                whenever(orderDetailsDescription.typeCode).thenReturn(instrumentType)
                orderDetailsDescription
            }
            val item = mock<BaseOrderDetailsItem.OrderDetailsItem>()
            whenever(item.orderDescriptions).thenReturn(descriptions)

            assertEquals(expected, item.getSecurityTypeForMultipleItems())
        }
    }
}
