package com.etrade.mobilepro.timeout.widget

import androidx.fragment.app.FragmentManager

interface SessionDialogFragment {

    var onButtonClickListener: (() -> Unit)?

    companion object {

        /**
         * Restores a dialog from [fragmentManager] if it is present.
         *
         * @param fragmentManager fragment manager to look into
         *
         * @return a dialog if available
         */
        fun restore(fragmentManager: FragmentManager, callbacks: () -> Unit) {
            (fragmentManager.findFragmentByTag(TAG) as? SessionDialogFragment)?.onButtonClickListener = callbacks
        }

        const val TAG = "SessionDialogFragment"
    }
}
