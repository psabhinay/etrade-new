package com.etrade.mobilepro.alerts.presentation.manage.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.alerts.api.model.AlertAction
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.alerts.presentation.databinding.ItemSimpleAlertBinding
import com.etrade.mobilepro.common.customview.SwipeRevealLayout

class UserAlertViewHolder(
    private val binding: ItemSimpleAlertBinding,
    private val alertListener: ((UserAlertItem, AlertAction) -> Unit)?,
    private val swipeRevealListener: ((UserAlertItem, SwipeRevealLayout.Action) -> Unit)?,
    dragLocker: (() -> Boolean)?
) : RecyclerView.ViewHolder(binding.root) {
    private var item: UserAlertItem? = null

    init {
        binding.alertTitle.setOnClickListener {
            item?.let { item ->
                alertListener?.invoke(item, AlertAction.SELECT)
            }
        }
        binding.swipeReveal.actionListener = { action ->
            item?.let {
                swipeRevealListener?.invoke(it, action)
            }
        }
        binding.deleteAlertBtn.setOnClickListener {
            item?.let { alert ->
                alertListener?.invoke(alert, AlertAction.DELETE)
            }
        }
        binding.swipeReveal.dragLocker = dragLocker
    }

    fun bindTo(item: UserAlertItem) {
        this.item = item
        binding.alertTitle.text = item.description
        if (item.isDeleteBtnVisible) {
            binding.swipeReveal.open(animation = false)
        } else {
            binding.swipeReveal.close(animation = false)
        }
    }
}
