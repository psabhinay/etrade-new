package com.etrade.mobilepro.bloombergtv.presentation.common

internal const val AUDIO_URL = "audioUrl"
internal const val VIDEO_URL = "videoUrl"
internal const val START_TIME = "startTime"
internal const val ACTION_PLAY = "com.etrade.mobilepro.ACTION_PLAY"
internal const val ACTION_EXIT = "com.etrade.mobilepro.ACTION_EXIT"
internal const val ACTION_OPEN_IN_APP = "com.etrade.mobilepro.ACTION_OPEN_IN_APP"
