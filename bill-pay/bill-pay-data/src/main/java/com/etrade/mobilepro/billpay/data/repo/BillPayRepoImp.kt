package com.etrade.mobilepro.billpay.data.repo

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.Payment
import com.etrade.mobilepro.billpay.api.PaymentRequest
import com.etrade.mobilepro.billpay.api.PaymentResult
import com.etrade.mobilepro.billpay.api.PaymentStatus
import com.etrade.mobilepro.billpay.api.SavePayeeResult
import com.etrade.mobilepro.billpay.api.SearchPayeeResult
import com.etrade.mobilepro.billpay.api.StatesOfUS
import com.etrade.mobilepro.billpay.data.rest.BillPayApiClient
import com.etrade.mobilepro.billpay.data.rest.PayeeRequestDto
import com.etrade.mobilepro.billpay.data.rest.PaymentListRequestDto
import com.etrade.mobilepro.billpay.data.rest.PaymentListRequestValueDto
import com.etrade.mobilepro.billpay.data.rest.SavePayeeRequestDto
import com.etrade.mobilepro.billpay.data.rest.cancelpayment.createCancelPaymentRequest
import com.etrade.mobilepro.billpay.data.rest.getSchedulePaymentPath
import com.etrade.mobilepro.billpay.data.rest.searchpayee.SearchPayeeRequestBodyDto
import com.etrade.mobilepro.billpay.data.rest.searchpayee.SearchRequestDto
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import org.threeten.bp.Instant
import javax.inject.Inject

private const val CANCEL_PAYMENT_SUCCESS_CODE = "SUCCESS"

class BillPayRepoImp @Inject constructor(
    private val apiClient: BillPayApiClient
) : BillPayRepo {

    override suspend fun cancelPayment(sessionId: String, paymentId: String): ETResult<Unit> {
        return runCatchingET {
            createCancelPaymentRequest(sessionId, paymentId)
                .let { apiClient.cancelPayment(it) }
                .doSuccessful { it.content.status == CANCEL_PAYMENT_SUCCESS_CODE }
                .let(::require)
                .let { Unit }
        }
    }

    override suspend fun getBillPaySessionId(): ETResult<String> {
        return runCatchingET {
            apiClient.getBillpaySessionId()
                .doIfSuccessfulOrThrow {
                    it.sessionId
                }
        }
    }

    override suspend fun getAccounts(): ETResult<List<AccountDetails>> {
        return runCatchingET {
            apiClient.getEnrollmentDetails().toAccountList()
        }
    }

    override suspend fun getPayeesList(sessionId: String): ETResult<List<Payee>> {
        return runCatchingET {
            apiClient.getPayeesList(PayeeRequestDto.toPayeeRequest(sessionId))
                .doIfSuccessfulOrThrow {
                    it.toPayeeList()
                }
        }
    }

    override suspend fun getPaymentList(sessionId: String, status: PaymentStatus, numOfDays: Int, startDate: Instant): ETResult<List<Payment>> {
        return runCatchingET {
            apiClient.getPaymentList(createPaymentListRequest(sessionId, status, numOfDays, startDate))
                .doIfSuccessfulOrThrow {
                    it.toPaymentEntry()
                }
        }
    }

    private fun createPaymentListRequest(sessionId: String, paymentStatus: PaymentStatus, numOfDays: Int, startDate: Instant): PaymentListRequestDto {
        return PaymentListRequestDto(
            PaymentListRequestValueDto(
                sessionId,
                paymentStatus.status,
                numOfDays,
                DateFormattingUtils.formatToShortDate(startDate.toEpochMilli())
            )
        )
    }

    override suspend fun getStateList(): ETResult<StatesOfUS> {
        return runCatchingET {
            apiClient.getStateList().doSuccessful {
                StatesOfUS(it.toAddressStateList().sortedBy { stateAddress -> stateAddress.code })
            }
        }
    }

    private suspend fun addPayee(sessionId: String, payee: Payee): ETResult<SavePayeeResult> {
        return runCatchingET {
            apiClient.addPayee(SavePayeeRequestDto.fromPayee(payee, sessionId)).toSavePayeeResult()
        }
    }

    private suspend fun updatePayee(sessionId: String, payee: Payee): ETResult<SavePayeeResult> {
        return runCatchingET {
            apiClient.updatePayee(SavePayeeRequestDto.fromPayee(payee, sessionId)).toSavePayeeResult()
        }
    }

    override suspend fun savePayee(sessionId: String, payee: Payee): ETResult<SavePayeeResult> {
        return if (payee.isExistingPayee) {
            updatePayee(sessionId, payee)
        } else {
            addPayee(sessionId, payee)
        }
    }

    override suspend fun deletePayee(sessionId: String, payeeId: String): ETResult<Boolean> {
        return runCatchingET {
            apiClient.deletePayee(PayeeRequestDto.toDeletePayeeRequest(sessionId, payeeId))
                .doIfSuccessfulOrThrow {
                    it.successResult?.status == SavePayeeResult.Successful.code
                }
        }
    }

    override suspend fun schedulePayment(sessionId: String, request: PaymentRequest): ETResult<PaymentResult> {
        return runCatchingET {
            apiClient
                .schedulePayment(request.toDto(sessionId), getSchedulePaymentPath(request.isNewPayment))
                .toPaymentResult()
        }
    }

    override suspend fun searchPayee(sessionId: String, name: String): ETResult<SearchPayeeResult> {
        return runCatchingET {
            apiClient
                .searchPayee(
                    SearchPayeeRequestBodyDto(
                        SearchRequestDto(
                            name,
                            sessionId
                        )
                    )
                ).toSearchResult()
        }
    }
}
