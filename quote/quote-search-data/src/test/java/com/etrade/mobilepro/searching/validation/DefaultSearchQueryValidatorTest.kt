package com.etrade.mobilepro.searching.validation

import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchType
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class DefaultSearchQueryValidatorTest {
    private val sut = DefaultSearchQueryValidator()

    @Test
    fun `should be transformed to uppercase and trimmed`() {
        val input = SearchQuery(SearchType.SYMBOL, " abc  ")

        val output = sut.makeValidOrEmpty(input)

        assertTrue("to uppercase and trim", output.value == "ABC")
    }

    @Test
    fun `empty remains empty`() {
        val input = SearchQuery(SearchType.SYMBOL, "")

        val output = sut.makeValidOrEmpty(input)

        assertTrue(output.isEmpty())
    }

    @Test
    fun `valid symbols are ok`() {
        val input = SearchQuery(
            SearchType.SYMBOL,
            "abcdifghijklmnopqrstuvwxyzABCDIFGHIJKLMNOPQRSTUVWXYZ."
        )

        val output = sut.makeValidOrEmpty(input)

        assertTrue("valid symbols", output.value == "ABCDIFGHIJKLMNOPQRSTUVWXYZABCDIFGHIJKLMNOPQRSTUVWXYZ.")
    }

    @Test
    fun `only valid symbols are allowed`() {
        val all = CharArray(65535) {
            it.toChar()
        }
        val input = SearchQuery(SearchType.SYMBOL, String(all))

        val output = sut.makeValidOrEmpty(input)

        assertTrue(output.isNotEmpty())

        assertTrue("only valid chars remain", output.value == " .0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ")
    }

    @Test
    fun `should not skip a dot`() {
        val input = "A.AB"
        val query = SearchQuery(SearchType.SYMBOL, input)
        val result = sut.makeValidOrEmpty(query)
        assertEquals("Dot should remain in the output", input, result.value)
    }
}
