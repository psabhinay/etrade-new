package com.etrade.mobilepro.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.viewmodel.ActivitySignal
import com.etrade.mobilepro.viewmodel.ActivitySignalViewModel

operator fun ActivitySignalViewModel.plus(second: ActivitySignalViewModel): ActivitySignalViewModel {
    val first = this
    return object : ActivitySignalViewModel {
        override val activitySignal: LiveData<ConsumableLiveEvent<ActivitySignal>>
            get() = MediatorLiveData<ConsumableLiveEvent<ActivitySignal>>().apply {
                addSource(first.activitySignal) { value = it }
                addSource(second.activitySignal) { value = it }
            }
    }
}
