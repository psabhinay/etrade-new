package com.etrade.mobilepro.trade.api

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.MarketDestination

interface TradeRepo {

    suspend fun previewOrder(
        map: InputValuesSnapshot,
        marketDestination: MarketDestination = MarketDestination.AUTO,
        changeOrder: Boolean
    ): ETResult<TradeOrder>

    suspend fun placeOrder(orderId: String): ETResult<TradeOrder>

    suspend fun loadOrder(orderId: String): ETResult<TradeOrder>

    suspend fun clearOrder(orderId: String): ETResult<Unit>
}
