package com.etrade.mobilepro.screener.presentation.stock.opinions

import com.etrade.mobilepro.screener.api.filter.stock.OpinionsFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class Equity : ScreenerFilterEvent<OpinionsFilter, OpinionsViewModel.ViewState, Boolean> {
    DOWNGRADED {
        override fun update(viewState: OpinionsViewModel.ViewState, eventValue: Boolean): OpinionsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(downgraded = eventValue, upgraded = false))
    },
    UPGRADED {
        override fun update(viewState: OpinionsViewModel.ViewState, eventValue: Boolean): OpinionsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(upgraded = eventValue, downgraded = false))
    }
}
