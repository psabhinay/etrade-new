package com.etrade.mobilepro.ui.bottomnavigationview

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import kotlinx.parcelize.Parcelize

class BottomNavigationView : com.google.android.material.bottomnavigation.BottomNavigationView {

    constructor(ctx: Context) : super(ctx)

    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet)

    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    override fun onSaveInstanceState(): Parcelable = State(
        parentState = super.onSaveInstanceState(),
        visibility = visibility
    )

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is State) {
            visibility = state.visibility

            super.onRestoreInstanceState(state.parentState)
        } else {
            super.onRestoreInstanceState(state)
        }
    }
}

@Parcelize
private class State(
    val parentState: Parcelable?,
    val visibility: Int
) : Parcelable
