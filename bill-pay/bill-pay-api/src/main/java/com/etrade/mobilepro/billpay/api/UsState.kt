package com.etrade.mobilepro.billpay.api

data class UsState(val code: String, val description: String? = null)

@JvmInline
value class StatesOfUS(val states: List<UsState>)
