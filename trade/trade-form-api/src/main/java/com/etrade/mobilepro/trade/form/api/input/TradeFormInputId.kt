package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.value.SecurityTypeValueId

interface TradeFormInputId {
    val accountId: String
    val securityType: String
    val securityTypeValueId: SecurityTypeValueId
}

fun TradeFormInputId.toSecurityType(value: Any?): SecurityType? {
    return (value as? String)?.let {
        securityTypeValueId.converter.toValue(it)
    }
}
