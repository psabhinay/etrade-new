package com.etrade.mobilepro.landing.api.repo

import com.etrade.mobilepro.landing.api.LandingPage
import com.etrade.mobilepro.landing.api.LandingState

interface LandingRepo {
    fun getLandingState(): LandingState
    fun setSelectedPage(page: LandingPage)
}
