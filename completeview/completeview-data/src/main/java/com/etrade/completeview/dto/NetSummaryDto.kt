package com.etrade.completeview.dto

data class NetSummaryDto(
    val netAssetsDto: NetAssetsDto,
    val dayGainDto: DayGainDto
)
