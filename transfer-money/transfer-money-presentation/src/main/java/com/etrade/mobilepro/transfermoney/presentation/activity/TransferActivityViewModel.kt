package com.etrade.mobilepro.transfermoney.presentation.activity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.RestorableViewModelDelegate
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.transfermoney.api.TransferMoneyActivity
import com.etrade.mobilepro.transfermoney.api.repo.TransferMoneyRepo
import com.etrade.mobilepro.transfermoney.presentation.transfer.TransferViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import kotlinx.coroutines.launch
import javax.inject.Inject

class TransferActivityViewModel @Inject constructor(
    private val user: User,
    private val repo: TransferMoneyRepo,
    private val dispatchers: DispatcherProvider,
    private val restorableDelegate: RestorableViewModelDelegate
) : ViewModel() {
    private val _viewState = MutableLiveData<ViewState>()
    private val _transferCancelledSignal = MutableLiveData<ConsumableLiveEvent<Boolean>>()
    internal val viewState: LiveData<ViewState>
        get() = _viewState
    internal val transferCancelledSignal: LiveData<ConsumableLiveEvent<Boolean>>
        get() = _transferCancelledSignal
    init {
        fetchData(isInitial = true)
    }

    internal fun refresh() {
        fetchData(isInitial = false)
    }

    internal fun onTransferCancelled(onlyOneWasCancelled: Boolean) {
        _transferCancelledSignal.value = ConsumableLiveEvent(onlyOneWasCancelled)
    }

    private fun fetchData(isInitial: Boolean) {
        _viewState.value = if (isInitial) { ViewState.Loading.Initial } else { ViewState.Loading.Refreshing }
        viewModelScope.launch(dispatchers.IO) {
            repo.getActivity(user.getUserId().toString()).fold(
                onSuccess = { items ->
                    _viewState.postValue(ViewState.Success(items))
                },
                onFailure = {
                    _viewState.postValue(
                        ViewState.Error {
                            fetchData(isInitial)
                        }
                    )
                }
            )
        }
    }

    fun disposeState() {
        restorableDelegate.dispose(TransferViewModel::class.java)
    }

    internal sealed class ViewState {
        sealed class Loading : ViewState() {
            object Initial : Loading()
            object Refreshing : Loading()
        }
        data class Success(
            val items: List<TransferMoneyActivity>
        ) : ViewState()
        class Error(
            val retry: () -> Unit
        ) : ViewState()
    }
}
