package com.etrade.mobilepro.portfolio.model

import com.etrade.mobilepro.backends.mgs.BaseReference
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class TaxLotReferenceListDto(
    @Json(name = "data")
    override val data: List<TaxLotReferenceDto>
) : BaseReference<List<TaxLotDto>>() {

    override fun convertToModel(): List<TaxLotDto> = data.map {
        TaxLotDto(
            taxLotId = it.taxLotId,
            positionId = it.positionId,
            symbol = it.symbol,
            acquiredDateMs = it.acquiredDateMs,
            price = it.price,
            daysGainRaw = it.daysGainValue,
            daysGainPct = it.daysGainPct,
            marketValueRaw = it.marketValueRaw,
            totalCostForGainPct = it.totalCostForGainPct,
            totalGainRaw = it.totalGainValue,
            remainingQty = it.remainingQty,
            commPerShare = it.commPerShare,
            feesPerShare = it.feesPerShare
        )
    }
}
