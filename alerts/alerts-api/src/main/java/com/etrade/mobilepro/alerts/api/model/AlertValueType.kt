package com.etrade.mobilepro.alerts.api.model

enum class AlertValueType {
    HIGH_TARGET_PRICE,
    LOW_TARGET_PRICE,
    HIGH_DAILY_PERCENT,
    LOW_DAILY_PERCENT,
    HIGH_PE_TARGET,
    LOW_PE_TARGET,
    DAILY_VOLUME
}

enum class NewsAlertValueType(val requestParamName: String) {
    UP_DOWN("editrat0"),
    IN_PLAY("editipl0"),
    EARNINGS("editern0"),
    SPLIT_NOTICE("editspl0")
}
