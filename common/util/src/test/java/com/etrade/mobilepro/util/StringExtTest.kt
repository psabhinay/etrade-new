package com.etrade.mobilepro.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

class StringExtTest {

    @Test
    fun allLowerCaseTest() {
        val testString = "hello world"
        val resultString = testString.toSentence()
        assertEquals("Hello World", resultString)
    }

    @Test
    fun allUpperCaseTest() {
        val testString = "HELLO WORLD"
        val resultString = testString.toSentence()
        assertEquals("Hello World", resultString)
    }

    @Test
    fun otherAllUpperCaseTest() {
        val testString = "GOOD HARBOR TACTICAL CORE US A"
        val resultString = testString.toSentence()
        assertEquals("Good Harbor Tactical Core Us A", resultString)
    }

    @Test
    fun allLowerCaseWithSpaceTest() {
        val testString = "hello world         "
        val resultString = testString.toSentence()
        assertEquals("Hello World", resultString)
    }

    @Test
    fun `check toNullIfBlank if null`() {
        val string: String? = null
        assertNull(string.toNullIfBlank())
    }

    @ParameterizedTest
    @ValueSource(strings = [" ", "   ", "\t", "\n", "\n\n\n", " \n \t \n \t "])
    fun `check toNullIfBlank`(testInput: String) {
        assertNull(testInput.toNullIfBlank())
    }

    @Test
    fun splitOnFirstSpace_ifNoSpaces_returnsOriginalString() {
        assertEquals("test".splitOnFirstSpace(), "test" to null)
    }

    @Test
    fun splitOnFirstSpace_ifOneSpace_returnsTwoParts() {
        assertEquals("test1 test2".splitOnFirstSpace(), "test1" to "test2")
    }

    @Test
    fun splitOnFirstSpace_ifSeveralSpaces_splitsOnlyOnFirst() {
        assertEquals("test1 test2 test3".splitOnFirstSpace(), "test1" to "test2 test3")
    }

    @Test
    fun toShortDate_whenTwoDigitDate_returnsCorrectShortDate() {
        val date = "Aug 20 '21"
        val result = date.toShortDate()
        assertEquals("08/20/21", result)
    }

    @Test
    fun toShortDate_whenSingleDigitDateWithLeadingZero_returnsCorrectShortDate() {
        val date = "Sep 01 '21"
        val result = date.toShortDate()
        assertEquals("09/01/21", result)
    }

    @Test
    fun toShortDate_whenInvalidDate_returnsSelf() {
        val date = "Sept 1 '21"
        val result = date.toShortDate()
        assertEquals(date, result)
    }

    @Test
    fun addLeadingZerosToDate_addsZeros() {
        val dateString = "1/1/1"
        val result = dateString.addLeadingZerosToDate()
        assertEquals("01/01/01", result)
    }

    @Test
    fun addLeadingZerosToDate_addsOneZero() {
        val dateString = "1/01/01"
        val result = dateString.addLeadingZerosToDate()
        assertEquals("01/01/01", result)
    }

    @Test
    fun addLeadingZerosToDate_addsNothing() {
        val dateString = "01/01/01"
        val result = dateString.addLeadingZerosToDate()
        assertEquals("01/01/01", result)
    }
}
