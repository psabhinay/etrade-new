package com.etrade.mobilepro.inboxmessages

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.localytics.androidx.InAppCampaign
import com.localytics.androidx.InboxCampaign
import com.localytics.androidx.Localytics
import com.localytics.androidx.MessagingListenerV2Adapter
import org.slf4j.LoggerFactory

private const val INBOX_MESSAGE_VIEWED = "Viewed"

class LocalyticsInboxMessagesRepository : InboxMessagesRepository {

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    override suspend fun getMessages(type: MessageType, place: MessagePlace?, userType: UserType?): ETResult<List<InboxMessage>> =
        getMessagesBlocking(type, place, userType)

    override fun getMessagesBlocking(type: MessageType, place: MessagePlace?, userType: UserType?): ETResult<List<InboxMessage>> = runCatchingET {
        Localytics.getDisplayableInboxCampaigns()
            .filter { campaign ->
                campaign.shouldBeShown().also { shouldBeShown ->
                    if (!shouldBeShown) {
                        tagInboxImpressionAsSend(campaign)
                    }
                }
            }
            .map { LocalyticsInboxMessage(it) }
            .filter {
                it.type == type &&
                    place?.let { place -> it.place == place } ?: true &&
                    userType?.let { type -> it.userType?.contains(type) } ?: true
            }
    }

    override suspend fun dismissMessage(message: InboxMessage) {
        message.let { it as? LocalyticsInboxMessage }?.campaign?.delete()
    }

    override suspend fun setMessageSend(message: InboxMessage) {
        message.let { it as? LocalyticsInboxMessage }?.campaign?.let { tagInboxImpressionAsSend(it) }
    }

    override suspend fun setMessageOpen(message: InboxMessage) {
        message.let { it as? LocalyticsInboxMessage }?.campaign?.let { Localytics.tagInboxImpression(it, Localytics.ImpressionType.CLICK) }
    }

    override suspend fun setMessageViewed(message: InboxMessage) {
        message.let { it as? LocalyticsInboxMessage }?.campaign?.let { Localytics.tagInboxImpression(it, INBOX_MESSAGE_VIEWED) }
    }

    override fun triggerMessage(triggerName: String, onMessageClose: (() -> Unit)?) {
        Localytics.triggerInAppMessage(triggerName)
        Localytics.setMessagingListener(object : MessagingListenerV2Adapter() {
            override fun localyticsDidDismissInAppMessage() {
                super.localyticsDidDismissInAppMessage()
                onMessageClose?.invoke()
            }
        })
    }

    override fun triggerMessageCustomUi(
        triggerName: String,
        onMessageTriggered: (InAppMessage) -> Unit
    ) {
        Localytics.setMessagingListener(object : MessagingListenerV2Adapter() {
            override fun localyticsShouldShowInAppMessage(campaign: InAppCampaign): Boolean {
                logger.debug("triggerMessageCustomUi localyticsShouldShowInAppMessage ${campaign.eventName} ${campaign.attributes}")
                if (campaign.eventName == triggerName) {
                    onMessageTriggered(LocalyticsInAppMessage(campaign))
                    return false
                }
                return super.localyticsShouldShowInAppMessage(campaign)
            }
        })
        logger.debug("triggerMessageCustomUi triggering $triggerName")
        Localytics.triggerInAppMessage(triggerName)
    }

    // tagImpression with dismiss type counts the inbox message as sends. That is reflected on Localytics messaging dashboard
    private fun tagInboxImpressionAsSend(campaign: InboxCampaign) {
        Localytics.tagInboxImpression(campaign, Localytics.ImpressionType.DISMISS)
    }
}
