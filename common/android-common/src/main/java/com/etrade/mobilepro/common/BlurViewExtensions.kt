package com.etrade.mobilepro.common

import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import eightbitlab.com.blurview.BlurView
import eightbitlab.com.blurview.RenderScriptBlur

private const val BLUR_RADIUS = 8F

fun Fragment.setupBlurView(blurView: BlurView, @ColorRes overlayColor: Int = R.color.lighter_grey_opacity_50, root: ViewGroup? = null) {
    val decorView = requireActivity().window.decorView
    val rootView = root ?: decorView.findViewById(android.R.id.content) as ViewGroup
    val windowBackground = root?.background ?: decorView.background

    blurView.setupWith(rootView)
        .setBlurAutoUpdate(true)
        .setFrameClearDrawable(windowBackground)
        .setBlurRadius(BLUR_RADIUS)
        .setBlurAlgorithm(RenderScriptBlur(requireContext()))
        .setHasFixedTransformationMatrix(true)
        .setOverlayColor(ContextCompat.getColor(requireContext(), overlayColor))
}
