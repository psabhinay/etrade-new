package com.etrade.mobilepro.billpay.data.rest

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class BillpaySessionResponseDto(
    @Json(name = "sessionId")
    val sessionId: String
) : BaseDataDto()
