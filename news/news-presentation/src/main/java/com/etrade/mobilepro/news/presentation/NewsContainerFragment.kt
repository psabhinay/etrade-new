package com.etrade.mobilepro.news.presentation

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.RestoreTabPositionDelegate
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.presentation.adapter.NewsPagerAdapter
import com.etrade.mobilepro.news.presentation.databinding.FragmentNewsContainerBinding
import com.etrade.mobilepro.tracking.SCREEN_TITLE_NEWS
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.google.android.material.tabs.TabLayoutMediator
import javax.inject.Inject

class NewsContainerFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    private val restoreTabPositionDelegate: RestoreTabPositionDelegate,
    private val router: NewsRouter,
    private val setupResolver: NewsSetupResolver,
) : Fragment(R.layout.fragment_news_container) {

    private val binding by viewBinding(FragmentNewsContainerBinding::bind) { onDestroyBinding() }

    private val viewModel: NewsContainerViewModel by viewModels { viewModelFactory }
    private val sharedViewModel: NewsSharedViewModel by activityViewModels { viewModelFactory }

    private val args: Params
        get() = router.newsContainerArgs(arguments) ?: Params()

    private lateinit var pageSelectionListener: NewsPagerAdapter.TabLayoutListener

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        bindViewModel(hasNoSavedState = savedInstanceState == null)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        if (setupResolver.containerShouldInflateCommonMenu(activity)) {
            inflater.inflate(R.menu.news_common_menu, menu)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_company_lookup -> {
                startSearchForCurrentTab()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun onDestroyBinding() {
        binding.tabContainer.tabLayout.removeOnTabSelectedListener(pageSelectionListener)
    }

    @Suppress("LongMethod")
    private fun bindViewModel(hasNoSavedState: Boolean) {

        sharedViewModel.authState.observe(
            viewLifecycleOwner,
            Observer { isAuth ->
                val adapter = NewsPagerAdapter(this, isAuth = isAuth)

                with(binding) {
                    newsListViewPager.adapter = adapter
                    TabLayoutMediator(tabContainer.tabLayout, newsListViewPager) { tab, position ->
                        tab.text = adapter.getPageTitle(position)
                    }.attach()

                    pageSelectionListener = adapter.TabLayoutListener {
                        sharedViewModel.didSelectTab(it)
                    }
                    tabContainer.tabLayout.addOnTabSelectedListener(pageSelectionListener)

                    restoreTabPositionDelegate
                        .init(this@NewsContainerFragment, newsListViewPager)

                    // if a specific tab has been passed in the args, override the restored tab position
                    adapter.resolveTabIndex(args.tabType).takeIf { it >= 0 }?.let {
                        newsListViewPager.setCurrentItem(it, false)
                    }
                }
            }
        )

        viewModel.clearLiveData.observe(
            viewLifecycleOwner,
            Observer {
                sharedViewModel.clear()
            }
        )

        if (hasNoSavedState) {
            sharedViewModel.watchlistFilterDataSource.refresh()
            args.accountId?.let {
                sharedViewModel.portfolioFilterDataSource.findAndSelectItemByAccountId(it, args.isStockPlan)
            }
        }

        args.quoteTicker?.let {
            sharedViewModel.passedInSymbol = it
        }
    }

    override fun onResume() {
        super.onResume()

        // When the company tab is opened for the first time there is a race condition when opening
        // QuoteLookup Search Screen. So Manually navigating to company tab.
        if (sharedViewModel.isCompanyTabOpenedFirstTime) {
            sharedViewModel.isCompanyTabOpenedFirstTime = false
            binding.newsListViewPager.setCurrentItem(2, false)
        }
    }

    private fun startSearchForCurrentTab() {
        sharedViewModel.activeTab.value?.let {
            router.openSearch(false, SCREEN_TITLE_NEWS)
        }
    }

    class Params(
        val accountId: String? = null,
        val quoteTicker: String? = null,
        val isStockPlan: Boolean? = null,
        val tabType: NewsType = NewsType.UNDEFINED
    )
}
