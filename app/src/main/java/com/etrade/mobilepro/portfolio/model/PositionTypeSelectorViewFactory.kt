package com.etrade.mobilepro.portfolio.model

import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.portfolio.PortfolioSettingsRepository
import com.etrade.mobilepro.portfolio.PositionTypeSelectorViewDto
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

class PositionTypeSelectorViewFactory @Inject constructor(
    private val resources: Resources,
    private val portfolioSettingsRepository: PortfolioSettingsRepository
) : DynamicUiViewFactory {

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is PositionTypeSelectorViewDto) {
            return object : GenericLayout {

                override val viewModelClass = PositionTypeSelectorViewModel::class.java

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    val mode = if (dto.isPortfolio) {
                        PositionTableMode.PORTFOLIO_VIEW
                    } else {
                        PositionTableMode.ALL_BROKERAGE_VIEW
                    }
                    PositionTypeSelectorViewModel(resources, portfolioSettingsRepository, mode)
                }

                override fun refresh(viewModel: DynamicViewModel) {
                    // do nothing
                }

                override val uniqueIdentifier: String = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("PositionTypeSelectorViewFactory registered to unknown type")
        }
    }
}
