package com.etrade.mobilepro.quoteapi

class QuoteTemplateResources constructor(
    val valueMissing: String,
    val titleFormat: String,
    val positiveChangeColorRes: Int,
    val negativeChangeColorRes: Int,
    val neutralChangeColorRes: Int,
    val asOfDateTimeFormat: String,
    val nasdaqAsOfDateTimeFormat: String,
    val dateFormat: String,
    val peFormat: String,
    val changeFormat: String,
    val closingPrice: String,
    val underlyerPrice: String,
    val underlyerClosingPrice: String,
    val fundClassTitle: String,
    val fundDescriptionFormat: String,
    val fundDescriptionNotFound: String,
    val fundPerformanceCategoryFormat: String,
    val priceFormat: String,
    val sizeFormat: String,
    val contentDescriptionFormat: String
)
