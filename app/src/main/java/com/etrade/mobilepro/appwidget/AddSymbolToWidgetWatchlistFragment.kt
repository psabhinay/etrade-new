package com.etrade.mobilepro.appwidget

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import com.etrade.mobilepro.R
import com.etrade.mobilepro.appwidget.widgetcontainer.DefaultAppWidgetProvider
import com.etrade.mobilepro.common.toolbarView
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.quotelookup.getSearchIntentAndOptions
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.searchingapi.searchResultSymbol
import com.etrade.mobilepro.util.safeLet
import javax.inject.Inject

class AddSymbolToWidgetWatchlistFragment @Inject constructor() : OverlayBaseFragment() {

    override val layoutRes: Int = R.layout.fragment_blank_page

    private val symbolLookupRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            handleSymbolSearch(result.resultCode, result.data)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            searchSymbol()
        }
    }

    private fun searchSymbol() {
        activity?.let {
            val params = StartSearchParams(
                query = SearchQuery(SearchType.SYMBOL),
                stockQuoteSearchOnly = false,
                finishOnSearch = true
            )
            val (intent, options) = it.toolbarView.getSearchIntentAndOptions(it, params)
            symbolLookupRequest.launch(intent, options)
        }
    }

    private fun handleSymbolSearch(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            safeLet(context, data?.searchResultSymbol) { context, symbol ->
                DefaultAppWidgetProvider.addSymbolToSelectedWatchlist(context, symbol)
            }
        }
        activity?.finishAffinity()
    }
}
