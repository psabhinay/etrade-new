package com.etrade.mobilepro.appwidget.api.settings

import androidx.annotation.Keep
import java.util.concurrent.TimeUnit

private const val FULLY_OPAQUE = 255

@Keep
data class WidgetSettings(
    val refreshRate: RefreshRate = RefreshRate.MINUTES_15,
    val layout: Layout = Layout.TILES,
    private val opacity: Int = FULLY_OPAQUE,
    val watchListId: String
) {
    val opacityAlpha: Int = getSafeOpacityAlpha()

    private fun getSafeOpacityAlpha(): Int = when {
        opacity < 0 -> 0
        opacity > FULLY_OPAQUE -> FULLY_OPAQUE
        else -> opacity
    }

    @Keep
    @Suppress("MagicNumber")
    enum class RefreshRate(val inMillis: Long) {
        MINUTES_15(inMillis = TimeUnit.MINUTES.toMillis(15)),
        MINUTES_30(inMillis = TimeUnit.MINUTES.toMillis(30)),
        HOURS_1(inMillis = TimeUnit.HOURS.toMillis(1)),
        HOURS_2(inMillis = TimeUnit.HOURS.toMillis(2)),
        MANUAL(inMillis = 0);

        companion object {
            fun fromInt(input: Int): RefreshRate? = values().find { it.ordinal == input }
        }
    }

    @Keep
    enum class Layout {
        TILES, LIST;

        companion object {
            fun fromInt(input: Int): Layout? = values().find { it.ordinal == input }
        }
    }
}
