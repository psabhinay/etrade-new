package com.etrade.mobilepro.dynamic.form.ui

import android.content.Context
import android.util.AttributeSet
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.dropdown.DropDownItemConverter
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.SelectInput

class SelectInputView : AbstractSelectInputView<SelectInput> {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun createDropDownManager(
        itemConverter: DropDownItemConverter<DropDownOption>,
        input: SelectInput
    ) = BottomSheetSelector(findFragment<Fragment>().childFragmentManager, resources, itemConverter).apply {
        init(
            "${SelectInputView::class.java.name}/${input.id}",
            bottomSheetSelectorTitle,
            dropDownOptions,
            selectedOptionIndex,
            toolbarActionEnd = input.toolBarActionEnd
        )
    }
}
