package com.etrade.mobilepro.menu.builder

import com.etrade.mobilepro.menu.DividerMenuItem
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.NewMenuItemValidator
import com.etrade.mobilepro.menu.TextMenuItem

class MenuPageBuilder(val newMenuItemValidator: NewMenuItemValidator? = null) {
    internal val items = mutableListOf<MenuItem>()

    private fun item(id: Int? = null, type: TextMenuItem.Type, setup: TextMenuItemBuilder.() -> Unit = {}) {
        val itemBuilder = TextMenuItemBuilder(newMenuItemValidator)
        itemBuilder.id = id
        itemBuilder.type = type
        itemBuilder.setup()
        items += itemBuilder.build()
    }

    fun textItem(id: Int? = null, setup: TextMenuItemBuilder.() -> Unit = {}) {
        item(id, TextMenuItem.Type.Text, setup)
    }

    fun textWithArrowItem(id: Int? = null, setup: TextMenuItemBuilder.() -> Unit = {}) {
        item(id, TextMenuItem.Type.TextWithArrow, setup)
    }

    fun textWithArrowAndSubtitle(id: Int? = null, setup: TextMenuItemBuilder.() -> Unit = {}) {
        item(id, TextMenuItem.Type.TextWithArrowAndSubtitle, setup)
    }

    fun divider() {
        items += DividerMenuItem()
    }

    fun count(setup: TextMenuItemBuilder.() -> Unit = {}) {
        item(type = TextMenuItem.Type.Count, setup = setup)
    }

    fun highlight(setup: TextMenuItemBuilder.() -> Unit = {}) {
        item(type = TextMenuItem.Type.Highlight, setup = setup)
    }

    fun build(): List<MenuItem> {
        return items.toList()
    }
}

fun menuPage(newMenuItemValidator: NewMenuItemValidator? = null, setup: MenuPageBuilder.() -> Unit): List<MenuItem> {
    val pageBuilder = MenuPageBuilder(newMenuItemValidator)
    pageBuilder.setup()
    return pageBuilder.build()
}
