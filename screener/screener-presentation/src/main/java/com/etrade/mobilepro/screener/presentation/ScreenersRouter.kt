package com.etrade.mobilepro.screener.presentation

import androidx.navigation.NavController
import com.etrade.mobilepro.screener.api.ScreenerDisclosure

@Suppress("TooManyFunctions")
interface ScreenersRouter {

    fun actionScreenersContainerToStockMarketSegment(navController: NavController)

    fun actionScreenersContainerToStockPriceVolume(navController: NavController)

    fun actionScreenersContainerToMutualFundPortfolioFragment(navController: NavController)

    fun actionScreenersContainerToOpinionsFragment(navController: NavController)

    fun actionScreenersContainerToFundamentalsFragment(navController: NavController)

    fun actionScreenersContainerToTechnicalsFragment(navController: NavController)

    fun actionScreenersContainerToEtfRiskFragment(navController: NavController)

    fun actionScreenersContainerToRiskFragment(navController: NavController)

    fun actionScreenersContainerToEarningsAndDividendsFragment(navController: NavController)

    fun actionScreenersContainerToFeesFragment(navController: NavController)

    fun actionScreenersContainerToEtfPerformanceFragment(navController: NavController)

    fun actionScreenersContainerToPerformanceFragment(navController: NavController)

    fun actionScreenersContainerToExpensesFragment(navController: NavController)

    fun actionScreenersContainerToEtfPortfolioFragment(navController: NavController)

    fun actionScreenersContainerToEtfFundProfileFragment(navController: NavController)

    fun actionScreenersContainerToFundProfileFragment(navController: NavController)

    fun actionScreenersContainerToEtfTechnicalsFragment(navController: NavController)

    fun actionToStockResultsFragment(navController: NavController)

    fun actionToMfResultsFragment(navController: NavController)

    fun actionToEtfResultsFragment(navController: NavController)

    fun actionShowPopup(navController: NavController, popupText: String)

    fun actionResultsToDisclosure(navController: NavController, disclosureType: ScreenerDisclosure)
}
