package com.etrade.mobilepro.portfolio.edit

import android.content.Context
import android.content.Intent
import com.etrade.mobilepro.editlist.EditListActivity
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.editlist.intent.BaseIntentBuilder
import com.etrade.mobilepro.editlist.intent.ListActionBuilder
import com.etrade.mobilepro.editlist.intent.ListEditDslMarker
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.util.delegate.getValue
import com.etrade.mobilepro.util.delegate.setValue

/**
 * UI: https://etrade.invisionapp.com/d/#/console/15411437/320299692/preview
 */
class EditPortfolioTableActivity : EditListActivity<PortfolioColumnWrapper>() {

    override val viewModelClass: Class<out EditListViewModel<PortfolioColumnWrapper>>
        get() = EditPortfolioTableViewModel::class.java

    override fun onCreate(viewModel: EditListViewModel<PortfolioColumnWrapper>) {
        super.onCreate(viewModel)
        (viewModel as EditPortfolioTableViewModel).prepareViewModel(intent.positionTableMode, intent.portfolioTableViewType)
    }

    companion object {

        /**
         * Creates an intent to start [EditPortfolioTableActivity].
         *
         * @param context a context of the application package implementing this class
         * @param init initialization block
         *
         * @return prepared intent
         */
        fun intent(context: Context, init: IntentBuilder.() -> Unit): Intent = IntentBuilder(context).apply(init).intent
    }

    /**
     * Builds an [Intent] to start [EditPortfolioTableActivity].
     *
     * @param context a context of the application package implementing this class
     */
    @ListEditDslMarker
    class IntentBuilder(context: Context) : BaseIntentBuilder(Intent(context, EditPortfolioTableActivity::class.java)) {

        init {
            isDeletableItems = false
            listAction {
                isVisible = false
            }
        }

        /**
         * Determines a type of view.
         */
        var positionTableMode: PositionTableMode by intent::positionTableMode

        /**
         * Table view type to use.
         */
        var portfolioTableViewType: PortfolioTableViewType by intent::portfolioTableViewType

        /**
         * Provide a list's action parameters.
         */
        private fun listAction(init: ListActionBuilder.() -> Unit) = ListActionBuilder(intent).init()
    }
}
