package com.etrade.mobilepro.dagger

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.movers.widget.MoversWidgetViewModel
import com.etrade.mobilepro.watchlist.news.filter.WatchlistFilterViewModel
import com.etrade.mobilepro.watchlist.presentation.WatchlistContainerFragment
import com.etrade.mobilepro.watchlist.presentation.WatchlistContainerViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface Watchlist2Module {

    @Binds
    @IntoMap
    @FragmentKey(WatchlistContainerFragment::class)
    fun bindWatchlistNewsFragment(fragment: WatchlistContainerFragment): Fragment

    @Binds
    @IntoMap
    @ViewModelKey(MoversWidgetViewModel::class)
    fun bindMoversWidgetVM(vm: MoversWidgetViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WatchlistFilterViewModel::class)
    fun bindWatchlistFilterVM(vm: WatchlistFilterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WatchlistContainerViewModel::class)
    fun bindWatchlistContainerViewModel(vm: WatchlistContainerViewModel): ViewModel
}
