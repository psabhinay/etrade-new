package com.etrade.mobilepro.positions.data.mapper

import com.etrade.mobilepro.positions.data.dto.PortfolioEntryDto
import com.etrade.mobilepro.positions.data.dto.PortfolioPositionDto
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class PortfolioEntryJsonMapperTest {
    private val testPosition1 = PortfolioPositionDto("A")
    private val testPosition2 = PortfolioPositionDto("B")
    private val nullSymbolPosition = PortfolioPositionDto(symbol = null)
    private val emptySymbolPosition = PortfolioPositionDto(symbol = "")

    private val testEntry1 = PortfolioEntryDto(accountId = "acc1", positions = listOf(testPosition1, testPosition2))
    private val testEntry2 = PortfolioEntryDto(accountId = null, positions = null)
    private val testEntry3 =
        PortfolioEntryDto(accountId = null, positions = listOf(nullSymbolPosition, emptySymbolPosition))
    private val testEntry4 =
        PortfolioEntryDto(accountId = null, positions = listOf(testPosition1, testPosition2))
    private val sut = PortfolioEntryJsonMapper()

    @Test
    fun `success case`() {
        val output = sut.map(testEntry1)

        assertTrue(testEntry1.positions?.size == output.positions.size, "position sizes match")
        assertTrue(output.positions[0].recordId == "A", "symbol0 name matches")
        assertTrue(output.positions[1].recordId == "B", "symbol1 name matches")
        assertTrue(output.accountId == "acc1", "account Id matches")
    }

    @Test
    fun nulls() {
        val nullsOutput = sut.map(testEntry2)

        assertTrue(nullsOutput.accountId.isEmpty(), "null account id mapped to empty string")
        assertTrue(nullsOutput.positions.isEmpty(), "positions list is empty")
    }

    @Test
    fun `empties and nulls - throws if portfolio position does not have an id`() {
        assertThrows<IllegalArgumentException> { sut.map(testEntry3) }
    }

    @Test
    fun `empties and nulls`() {
        val output4 = sut.map(testEntry4)
        assertTrue(output4.accountId.isEmpty(), "null account id mapped to empty string")
        assertTrue(testEntry3.positions?.size == output4.positions.size, "positions list size")
        assertEquals("--", output4.positions[0].symbol, "null symbol name maps to empty name")
        assertEquals("--", output4.positions[1].symbol, "empty symbol name maps to empty name")
    }
}
