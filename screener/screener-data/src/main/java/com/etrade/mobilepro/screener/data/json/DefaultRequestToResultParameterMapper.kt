package com.etrade.mobilepro.screener.data.json

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.result.RequestToResultParameterMapper
import javax.inject.Inject

private val etfPerformanceRequestFields = setOf(
    "Mkt_1Month_Ptile",
    "Mkt_3Month_Ptile",
    "Mkt_6Month_Ptile",
    "Mkt_YTD_Ptile",
    "Mkt_1Year_Ptile",
    "Mkt_3Year_Ptile",
    "Mkt_5Year_Ptile",
    "Mkt_10Year_Ptile",
    "NavRetCat1Month",
    "NavRetCat3Month",
    "NavRetCat6Month",
    "NavRetCatYTD",
    "NavRetCat1Year",
    "NavRetCat3Year",
    "NavRetCat5Year",
    "NavRetCat10Year"
)

private val etfPerformanceResultFields = setOf(
    "QuarterEndMarketReturnM1",
    "QuarterEndMarketReturnY1",
    "QuarterEndMarketReturnY5",
    "QuarterEndMarketReturnY10",
    "QuarterEndMarketReturnSinceInception",
    "QuarterEndReturnY1",
    "QuarterEndReturnY5",
    "QuarterEndReturnY10",
    "QuarterEndReturnSinceInception"
)

private val mfPerformanceRequestFields = setOf(
    "1Month_TR_Ptile",
    "3Month_TR_Ptile",
    "6Month_TR_Ptile",
    "YTD_TR_Ptile",
    "1Year_TR_Ptile",
    "3Year_TR_Ptile",
    "5Year_TR_Ptile",
    "10Year_TR_Ptile"
)

private val mfPerformanceResultFields = setOf(
    "DistributionYield",
    "CatReturn1year",
    "CatReturn5year",
    "CatReturn10year"
)

private val etfMappings = mapOf(
    etfPerformanceRequestFields to etfPerformanceResultFields
)

private val mfMappings = mapOf(
    mfPerformanceRequestFields to mfPerformanceResultFields
)

class DefaultRequestToResultParameterMapper @Inject constructor() : RequestToResultParameterMapper {
    override fun map(group: ScreenerType.Group, requestParameters: List<String>): List<String> {
        return when (group) {
            ScreenerType.Group.STOCK -> requestParameters
            ScreenerType.Group.MUTUAL_FUND -> mapRequestParameters(requestParameters, mfMappings)
            ScreenerType.Group.ETF -> mapRequestParameters(requestParameters, etfMappings)
        }
    }

    private fun mapRequestParameters(requestParameters: List<String>, mappings: Map<Set<String>, Set<String>>): List<String> {
        val result = requestParameters.toMutableSet()
        mappings.entries.forEach { entry ->
            val oldSize = result.size
            result.removeAll(entry.key)
            if (result.size != oldSize) {
                result.addAll(entry.value)
            }
        }
        return result.toList()
    }
}
