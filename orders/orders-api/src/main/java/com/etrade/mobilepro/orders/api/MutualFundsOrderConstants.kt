package com.etrade.mobilepro.orders.api

enum class DividendReInvestmentType {
    DEPOSIT_FUND,
    HOLDING,
    RE_INVEST
}

enum class FundsOrderActionType {
    BUY,
    SELL,
    EXCHANGE
}

enum class FundsQuantityType {
    DOLLARS,
    ENTIRE_CASH_POSITION,
    ENTIRE_MARGIN_POSITION,
    ENTIRE_POSITION,
    SHARES,
    UNKNOWN
}
