package com.etrade.mobilepro.portfolio.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ParcelablePortfolioSummaryItemDto(
    val title: String,
    val initialValue: String?,
    val streamId: String?,
    val movementType: String?
) : Parcelable
