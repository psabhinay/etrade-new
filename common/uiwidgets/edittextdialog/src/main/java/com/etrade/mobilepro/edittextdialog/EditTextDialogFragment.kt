package com.etrade.mobilepro.edittextdialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.res.Configuration
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ScrollView
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.etrade.mobilepro.edittextdialog.databinding.FragmentEditTextDialogBinding
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.util.android.accessibility.bindTextInputButtonAccessibility
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory

private val logger: Logger = LoggerFactory.getLogger(EditTextDialogFragment::class.java)
private const val RESOURCE_ID_NULL = 0
/**
 * This is a dialog with a single input field.
 */
class EditTextDialogFragment : DialogFragment() {

    private var _binding: FragmentEditTextDialogBinding? = null
    private val binding
        get() = _binding!!

    var onPositiveButtonClickListener: ((String) -> Unit)? = null
    var onNegativeButtonClickListener: (() -> Unit)? = null

    var validator: InputFieldManager.Validator? = null

    @get:StringRes
    private val titleResId: Int
        get() = arguments?.getInt(KEY_TITLE_RES_ID) ?: RESOURCE_ID_NULL

    @get:StringRes
    private val hintResId: Int
        get() = arguments?.getInt(KEY_HINT_RES_ID) ?: RESOURCE_ID_NULL

    @get:StringRes
    private val messageResId: Int
        get() = arguments?.getInt(KEY_MESSAGE_RES_ID) ?: RESOURCE_ID_NULL

    @get:StringRes
    private val positiveButtonTitleResId: Int
        get() = arguments?.getInt(KEY_POSITIVE_BUTTON_LABEL_RES_ID, R.string.ok) ?: R.string.ok

    @get:StringRes
    private val negativeButtonTitleResId: Int
        get() = arguments?.getInt(KEY_NEGATIVE_BUTTON_LABEL_RES_ID, R.string.cancel) ?: R.string.cancel

    @get:StringRes
    private val actionButton1TitleResId: Int
        get() = arguments?.getInt(KEY_ACTION_BUTTON1_LABEL_RES_ID) ?: RESOURCE_ID_NULL
    private var actionButton1ClickListener: (() -> Unit)? = null

    @get:StringRes
    private val actionButton2TitleResId: Int
        get() = arguments?.getInt(KEY_ACTION_BUTTON2_LABEL_RES_ID) ?: RESOURCE_ID_NULL
    private var actionButton2ClickListener: (() -> Unit)? = null

    private var dismissListener: (() -> Unit)? = null
    private var showListener: (() -> Unit)? = null
    private var dialogContentView: View? = null
    private var inputFilters: Array<InputFilter> = emptyArray()
    private var inputType: Int = InputType.TYPE_CLASS_TEXT

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val context = requireContext()
        inflateDialogContent()
        logger.debug("onCreateDialog is get called")
        showListener?.invoke()
        return createAndSetupMaterialDialog(context)
    }

    @SuppressLint("InflateParams") // parent for a view provided by alert dialog
    private fun inflateDialogContent() {
        _binding = FragmentEditTextDialogBinding.inflate(layoutInflater).apply {
            setupInputEditText()
            setupActionButtons()
            dialogContentView = root
        }
    }

    private fun FragmentEditTextDialogBinding.setupActionButtons() {
        if (actionButton1TitleResId != RESOURCE_ID_NULL) {
            actionButton1.visibility = View.VISIBLE
            actionButton1.text = resources.getString(actionButton1TitleResId)
            actionButton1.setOnClickListener { actionButton1ClickListener?.invoke() }
        }
        if (actionButton2TitleResId != RESOURCE_ID_NULL) {
            actionButton2.visibility = View.VISIBLE
            actionButton2.text = resources.getString(actionButton2TitleResId)
            actionButton2.setOnClickListener { actionButton2ClickListener?.invoke() }
        }
    }

    private fun getEmptyDescription(hintText: String) = getString(R.string.label_edit_box_description, hintText)

    private fun getDescriptionWithEditableText(hintText: String, editableText: String?): String {
        val emptyDescriptionText = getEmptyDescription(hintText)
        return if (editableText == null) {
            emptyDescriptionText
        } else {
            "${getString(R.string.label_edit_box_editing_description, editableText)}, $emptyDescriptionText"
        }
    }

    private fun getHintText() = if (hintResId != RESOURCE_ID_NULL) {
        getString(hintResId)
    } else {
        ""
    }

    private fun FragmentEditTextDialogBinding.setupInputEditText() {
        val hintText = getHintText()
        textInputLayout.apply {
            hint = hintText
            bindTextInputButtonAccessibility(getEmptyDescription(hintText))
        }

        textInputEditText.apply {
            filters = inputFilters
            inputType = this@EditTextDialogFragment.inputType
            addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

                override fun afterTextChanged(s: Editable?) {
                    textInputLayout.apply {
                        error = null
                        bindTextInputButtonAccessibility(getDescriptionWithEditableText(hintText, s?.toString()))
                    }
                }
            })
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    onPositiveButtonClick()
                    true
                } else {
                    false
                }
            }
        }
    }

    private fun createAndSetupMaterialDialog(context: Context): AlertDialog {
        return MaterialAlertDialogBuilder(context).apply {
            if (titleResId != RESOURCE_ID_NULL) {
                setTitle(titleResId)
            }
            if (messageResId != RESOURCE_ID_NULL) {
                setMessage(messageResId)
            }
            setView(dialogContentView)
            setPositiveButton(positiveButtonTitleResId, null)
            setNegativeButton(negativeButtonTitleResId) { _, _ ->
                onNegativeButtonClickListener?.invoke()
            }
        }.create().apply {
            val currentOrientation = context.resources.configuration.orientation
            if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                setupScrollViewHeight()
            }
            setOnShowListener { dialog ->
                (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                    onPositiveButtonClick()
                }
            }
        }
    }

    private fun setupScrollViewHeight() {
        if (actionButton1TitleResId != RESOURCE_ID_NULL && actionButton2TitleResId != RESOURCE_ID_NULL) {
            val scrollView = dialogContentView?.findViewById<ScrollView>(R.id.editTextDialogScrollView)
            scrollView?.let {
                val params: ViewGroup.LayoutParams = it.layoutParams
                params.height = resources.getDimension(R.dimen.editTextDialogHeight).toInt()
                it.layoutParams = params
            }
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        logger.debug("onDismiss is get called")
        dismissListener?.invoke()
        super.onDismiss(dialog)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textInputLayout.error = savedInstanceState?.getCharSequence(KEY_ERROR)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putCharSequence(KEY_ERROR, binding.textInputLayout.error)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun onPositiveButtonClick() {
        val text = binding.textInputEditText.text.toString()
        when (val value = validator?.validate(text) ?: InputFieldManager.Value.Valid(text)) {
            is InputFieldManager.Value.Valid -> {
                onPositiveButtonClickListener?.invoke(value.content)
                dismissAllowingStateLoss()
            }
            is InputFieldManager.Value.Invalid -> binding.textInputLayout.error = value.reason
        }
    }

    class Builder(private val fragmentTag: String) {
        @StringRes private var titleResId: Int? = null
        @StringRes private var hintResId: Int? = null
        @StringRes private var messageResId: Int? = null
        @StringRes private var positiveButtonTitleResId: Int? = null
        private var positiveButtonClickListener: ((String) -> Unit)? = null
        @StringRes private var negativeButtonTitleResId: Int? = null
        private var negativeButtonClickListener: (() -> Unit)? = null
        private var filters: Array<InputFilter>? = null
        private var inputType: Int? = null
        private var validator: InputFieldManager.Validator? = null
        private var isCancelable: Boolean = true
        private var dismissListener: (() -> Unit)? = null
        private var showListener: (() -> Unit)? = null

        @StringRes private var actionButton1TitleResId: Int? = null
        private var actionButton1ClickListener: (() -> Unit)? = null
        @StringRes private var actionButton2TitleResId: Int? = null
        private var actionButton2ClickListener: (() -> Unit)? = null

        fun cancellable(isCancelable: Boolean) = apply { this.isCancelable = isCancelable }
        fun title(titleResId: Int) = apply { this.titleResId = titleResId }
        fun hint(hintResId: Int) = apply { this.hintResId = hintResId }
        fun message(messageResId: Int) = apply { this.messageResId = messageResId }
        fun positiveButton(labelResId: Int? = null, listener: ((String) -> Unit)? = null) = apply {
            this.positiveButtonTitleResId = labelResId
            this.positiveButtonClickListener = listener
        }
        fun negativeButton(labelResId: Int? = null, listener: (() -> Unit)? = null) = apply {
            this.negativeButtonTitleResId = labelResId
            this.negativeButtonClickListener = listener
        }
        fun setInputFilters(filters: Array<InputFilter>) = apply { this.filters = filters }
        fun setInputType(inputType: Int) = apply { this.inputType = inputType }
        fun setValidator(validator: InputFieldManager.Validator) = apply { this.validator = validator }
        fun actionButton1(labelResId: Int, listener: (() -> Unit)) = apply {
            this.actionButton1TitleResId = labelResId
            this.actionButton1ClickListener = listener
        }
        fun actionButton2(labelResId: Int, listener: (() -> Unit)) = apply {
            this.actionButton2TitleResId = labelResId
            this.actionButton2ClickListener = listener
        }
        fun dismissListener(listener: (() -> Unit)) = apply {
            this.dismissListener = listener
        }
        fun showListener(listener: (() -> Unit)) = apply {
            this.showListener = listener
        }

        fun show(fragmentManager: FragmentManager) {
            EditTextDialogFragment().apply {
                prepareDialog()
            }.show(fragmentManager, fragmentTag)
        }

        fun restore(fragmentManager: FragmentManager) {
            (fragmentManager.findFragmentByTag(fragmentTag) as? EditTextDialogFragment)?.prepareDialog()
        }

        fun dismiss(fragmentManager: FragmentManager) {
            if (fragmentManager.isStateSaved) { return }
            (fragmentManager.findFragmentByTag(fragmentTag) as? EditTextDialogFragment)?.dismiss()
        }

        @SuppressWarnings("ComplexMethod")
        private fun EditTextDialogFragment.prepareDialog() {
            arguments = Bundle().apply {
                this@Builder.titleResId?.let { putInt(KEY_TITLE_RES_ID, it) }
                this@Builder.hintResId?.let { putInt(KEY_HINT_RES_ID, it) }
                this@Builder.messageResId?.let { putInt(KEY_MESSAGE_RES_ID, it) }
                this@Builder.positiveButtonTitleResId?.let { putInt(KEY_POSITIVE_BUTTON_LABEL_RES_ID, it) }
                this@Builder.negativeButtonTitleResId?.let { putInt(KEY_NEGATIVE_BUTTON_LABEL_RES_ID, it) }
                this@Builder.actionButton1TitleResId?.let { putInt(KEY_ACTION_BUTTON1_LABEL_RES_ID, it) }
                this@Builder.actionButton2TitleResId?.let { putInt(KEY_ACTION_BUTTON2_LABEL_RES_ID, it) }
            }
            isCancelable = this@Builder.isCancelable
            this@Builder.validator?.let { validator = it }
            this@Builder.filters?.let { inputFilters = it }
            this@Builder.inputType?.let { inputType = it }
            this@Builder.positiveButtonClickListener?.let { onPositiveButtonClickListener = it }
            this@Builder.negativeButtonClickListener?.let { onNegativeButtonClickListener = it }
            this@Builder.actionButton1ClickListener?.let { actionButton1ClickListener = it }
            this@Builder.actionButton2ClickListener?.let { actionButton2ClickListener = it }
            this@Builder.dismissListener?.let { dismissListener = it }
            this@Builder.showListener?.let { showListener = it }
        }
    }

    companion object {
        private const val KEY_ERROR = "KEY_ERROR"
        private const val KEY_TITLE_RES_ID = "KEY_TITLE_RES_ID"
        private const val KEY_HINT_RES_ID = "KEY_HINT_RES_ID"
        private const val KEY_MESSAGE_RES_ID = "KEY_MESSAGE_RES_ID"
        private const val KEY_POSITIVE_BUTTON_LABEL_RES_ID = "KEY_POSITIVE_BUTTON_LABEL_RES_ID"
        private const val KEY_NEGATIVE_BUTTON_LABEL_RES_ID = "KEY_NEGATIVE_BUTTON_LABEL_RES_ID"
        private const val KEY_ACTION_BUTTON1_LABEL_RES_ID = "KEY_ACTION_BUTTON1_LABEL_RES_ID"
        private const val KEY_ACTION_BUTTON2_LABEL_RES_ID = "KEY_ACTION_BUTTON2_LABEL_RES_ID"
    }
}
