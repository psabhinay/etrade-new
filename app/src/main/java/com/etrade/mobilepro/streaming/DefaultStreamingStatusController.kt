package com.etrade.mobilepro.streaming

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.appwidget.AppWidgetHandler
import com.etrade.mobilepro.session.api.RealtimeQuotesStatusUpdateListener
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate.StreamingSubscriptionUpdate
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate.StreamingToggleUpdate
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdateListener
import java.util.concurrent.CopyOnWriteArrayList
import javax.inject.Inject

private const val STREAMING_ON = "streamingOn"

class DefaultStreamingStatusController @Inject constructor(
    private val appWidgetHandler: AppWidgetHandler,
    private val user: User,
    private val keyValueStorage: KeyValueStorage
) : StreamingStatusController {

    private val streamingStatusListeners = CopyOnWriteArrayList<StreamingStatusUpdateListener>()

    private val realtimeUserStatusUpdateListener: RealtimeQuotesStatusUpdateListener = {
        notifyListeners(StreamingSubscriptionUpdate(user.isRealtimeUser))
    }

    private val streamingPreferenceValue: Boolean
        get() = keyValueStorage.getBooleanValue(STREAMING_ON, false)

    override var isStreamingToggleEnabled: Boolean
        get() = user.isRealtimeUser && streamingPreferenceValue
        set(value) {
            if (streamingPreferenceValue == value) {
                return
            } else if (value && user.isRealtimeUser) {
                keyValueStorage.putBooleanValue(STREAMING_ON, value)
                notifyListeners(StreamingToggleUpdate(true))
                appWidgetHandler.refresh()
            } else if (!value) {
                keyValueStorage.putBooleanValue(STREAMING_ON, value)
                notifyListeners(StreamingToggleUpdate(false))
            }
        }

    override fun subscribeForStreamingStatusUpdates(listener: StreamingStatusUpdateListener) {
        if (streamingStatusListeners.isEmpty()) {
            user.subscribeForRealtimeQuotesStatusUpdates(realtimeUserStatusUpdateListener)
        }
        streamingStatusListeners.add(listener)
    }

    override fun unsubscribeFromStreamingStatusUpdates(listener: StreamingStatusUpdateListener) {
        streamingStatusListeners.remove(listener)
        if (streamingStatusListeners.isEmpty()) {
            user.unsubscribeFromRealtimeQuotesStatusUpdates(realtimeUserStatusUpdateListener)
        }
    }

    private fun notifyListeners(streamingStatusUpdate: StreamingStatusUpdate) {
        streamingStatusListeners.forEach { it(streamingStatusUpdate) }
    }
}
