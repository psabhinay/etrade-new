package com.etrade.mobilepro.inboxmessages

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import kotlin.coroutines.CoroutineContext

fun fetchInboxMessagesStaticViewLiveData(
    type: MessageType,
    coroutineContext: CoroutineContext,
    repository: InboxMessagesRepository,
    place: MessagePlace? = null,
    userType: UserType? = null
): LiveData<InboxMessagesStaticPayload> {
    val messages = MutableLiveData<List<InboxMessage>>(emptyList())
    return liveData(coroutineContext) {
        repository.getMessages(type, place, userType)
            .onSuccess { emit(it) }
            .onFailure { emit(emptyList<InboxMessage>()) }
    }
        .switchMap { messages.apply { value = it } }
        .map {
            if (it.isEmpty()) {
                InboxMessagesStaticPayload()
            } else {
                InboxMessagesStaticPayload(it)
            }
        }
}

data class InboxMessagesStaticPayload(val messages: List<InboxMessage>? = null)
