package com.etrade.mobilepro.searchingapi

import com.etrade.mobilepro.searchingapi.items.SearchResultItem

interface SelectionHistoryRepo {
    fun addToHistory(selectedItem: SearchResultItem)
    fun symbolSelectionHistory(): List<SearchResultItem.Symbol>
}
