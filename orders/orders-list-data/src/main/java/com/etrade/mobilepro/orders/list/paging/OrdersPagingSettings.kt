package com.etrade.mobilepro.orders.list.paging

import com.etrade.mobilepro.orders.api.OrderStatus

class OrdersPagingSettings private constructor(
    val pageSize: Int,
    val prefetchDistance: Int,
    val initialLoadCount: Int
) {
    companion object {
        private val defaultSettings = OrdersPagingSettings(
            pageSize = 20,
            prefetchDistance = 10,
            initialLoadCount = 20
        )

        private val allBySymbolSettings = OrdersPagingSettings(
            pageSize = 100,
            prefetchDistance = 15,
            initialLoadCount = 100
        )

        fun getOrdersPageSetting(orderStatus: OrderStatus): OrdersPagingSettings {
            return if (orderStatus == OrderStatus.ALL_BY_SYMBOL) {
                allBySymbolSettings
            } else {
                defaultSettings
            }
        }
    }
}
