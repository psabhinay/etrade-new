package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.displaySymbol
import com.etrade.mobilepro.instrument.isOption
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.common.AdvancedSavedOrderDto
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.etrade.mobilepro.trade.data.form.toLegDetailsDto
import com.etrade.mobilepro.trade.data.isTrailingStop
import com.etrade.mobilepro.trade.data.stock.StockOrderConstants
import com.etrade.mobilepro.trade.form.api.getTransactionType
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithQuantity
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractAction
import com.etrade.mobilepro.trade.form.api.input.extractPriceType
import com.etrade.mobilepro.trade.form.api.input.extractTerm

internal abstract class BaseAdvancedSaveOrderRequestBodyProvider : SaveOrderRequestParametersProvider {

    override val path: String = "savepreparedgrouporder"

    protected fun <T> createOrder(
        snapshot: InputValuesSnapshot,
        inputId: T,
        withSymbolInfo: WithSymbolInfo? = null
    ): AdvancedSavedOrderDto
            where T : WithActionTradeFormInputId, T : WithPriceTradeFormInputId, T : WithSymbolTradeFormInputId, T : WithQuantity {
        val symbol = withSymbolInfo ?: inputId.extractSymbolInfo(snapshot)
        return AdvancedSavedOrderDto(
            priceType = inputId.extractPriceType(snapshot)?.typeCode,
            orderType = snapshot.getOrderType(inputId, symbol).typeCode,
            limitPrice = snapshot[inputId.limitPrice] as? String,
            stopPrice = (snapshot[inputId.stopPrice] ?: snapshot[StockOrderConstants.OFFSET_VALUE]) as? String,
            orderTerm = inputId.extractTerm(snapshot)?.typeCode,
            allOrNone = snapshot[inputId.allOrNone] as? String ?: "0",
            legRequestList = snapshot.getTradeLegList(symbol, inputId.quantity, inputId)
        )
    }

    private fun InputValuesSnapshot.getTradeLegList(
        symbol: WithSymbolInfo?,
        inputQtyId: String,
        inputActionId: WithActionTradeFormInputId
    ): List<LegDetailsCommonDto> {
        val quantity = (this[inputQtyId] as? String)?.toIntOrNull() ?: 0
        val action = inputActionId.extractAction(this)
        return listOf(
            object : TradeLeg {
                override val symbol: String = symbol?.symbol.orEmpty()
                override val transactionType: TransactionType = action?.getTransactionType(symbol?.isOption ?: false) ?: TransactionType.UNKNOWN
                override val quantity: Int = quantity
                override val displaySymbol: String = symbol?.displaySymbol.orEmpty()
                override val isAMOption: Boolean = false
            }.toLegDetailsDto(withProductType = true)
        )
    }

    protected fun WithSymbolInfo.getProductType(): String {
        return if (isOption) {
            InstrumentType.OPTN
        } else {
            instrumentType
        }.typeCode
    }

    private fun InputValuesSnapshot.getOrderType(inputId: WithPriceTradeFormInputId, symbol: WithSymbolInfo?): OrderType {
        return when {
            symbol?.isOption ?: false -> OrderType.SIMPLE_OPTION
            isTrailingStop(inputId) -> OrderType.ADVANCE_EQUITY
            else -> OrderType.EQUITY
        }
    }
}
