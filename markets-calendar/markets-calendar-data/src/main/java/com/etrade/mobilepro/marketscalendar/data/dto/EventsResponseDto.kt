package com.etrade.mobilepro.marketscalendar.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DataDto<T>(
    @Json(name = "eventsResponseDM")
    val eventsResponseDm: EventsResponseDmDto<T>?
)

@JsonClass(generateAdapter = true)
data class EventsResponseDmDto<T>(
    @Json(name = "eventsDataList")
    val eventsDataList: List<T>?
)

@JsonClass(generateAdapter = true)
data class EventsResponseDto<T>(
    @Json(name = "data")
    val dataDto: DataDto<T>?
)
