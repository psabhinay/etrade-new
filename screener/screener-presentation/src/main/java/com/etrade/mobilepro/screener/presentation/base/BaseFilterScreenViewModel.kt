package com.etrade.mobilepro.screener.presentation.base

import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.util.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

abstract class BaseFilterScreenViewModel<F : ScreenerFilter, T : BaseFilterScreenViewModel.ViewState<F>>(
    private val aggregator: ScreenerFilterAggregator,
    private val screenerType: ScreenerType,
    private val screenerInteractor: ScreenerInteractor
) : ViewModel() {
    internal val viewState: LiveData<T>
        get() = _viewState
    internal val resultsState: LiveData<Resource<Int>>
        get() = _resultsState
    private val _viewState = MutableLiveData<T>()
    private val _resultsState = MutableLiveData<Resource<Int>>()
    private val compositeDisposable = CompositeDisposable()

    interface ViewState<F : ScreenerFilter> {
        val filter: F
    }

    abstract fun provideInitialState(initialFilter: F): T

    internal fun initialize() {
        _viewState.value = provideInitialState(aggregator.get(screenerType))
        subscribeToPreScreenResults()
    }

    protected fun updateState(transform: (T) -> T) {
        _viewState.value?.let { state ->
            val newState = transform(state)
            doUpdateState(state, newState, true)
        }
    }

    protected fun conditionalUpdateState(transform: (T) -> ScreenerFilterEvent.Result<T>) {
        _viewState.value?.let { state ->
            val result = transform(state)
            doUpdateState(state, result.value, result.shouldTriggerRequest)
        }
    }

    internal fun <V> consumeEvent(event: ScreenerFilterEvent<F, T, V>, value: V) = conditionalUpdateState {
        event.conditionalUpdate(it, value)
    }

    internal fun retryPrescreen() {
        screenerInteractor.retryPrescreen()
    }

    @CallSuper
    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    private fun subscribeToPreScreenResults() =
        compositeDisposable.add(
            screenerInteractor
                .preScreenResults()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = {
                        _resultsState.value = it
                    },
                    onComplete = {
                        throw IllegalStateException("Screener interactor at ($screenerType) has emitted onComplete")
                    },
                    onError = {
                        throw IllegalStateException("Screener interactor at ($screenerType) has emitted onError with ${it.message}", it)
                    }
                )
        )

    private fun doUpdateState(oldState: T, newState: T, shouldTriggerRequest: Boolean) {
        if (newState != oldState) {
            _viewState.value = newState
            aggregator.put(screenerType, newState.filter, shouldTriggerRequest)
        }
    }
}
