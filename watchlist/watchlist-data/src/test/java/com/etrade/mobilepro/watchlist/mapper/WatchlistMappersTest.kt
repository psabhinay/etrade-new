package com.etrade.mobilepro.watchlist.mapper

import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.backends.mgs.adapters.getBaseMetaAdapterFactory
import com.etrade.mobilepro.backends.mgs.adapters.getBaseReferenceAdapterFactory
import com.etrade.mobilepro.backends.mgs.adapters.getBaseScreenViewAdapterFactory
import com.etrade.mobilepro.dynamicui.DynamicEnum
import com.etrade.mobilepro.dynamicui.addSubtypes
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.watchlist.dto.AddWatchListEntryResponseDto
import com.etrade.mobilepro.watchlist.dto.CreateWatchlistResponseDataDto
import com.etrade.mobilepro.watchlist.dto.CreateWatchlistResponseDto
import com.etrade.mobilepro.watchlist.dto.DeleteWatchlistResponseDto
import com.etrade.mobilepro.watchlist.dto.SaveWatchlistEntriesResponseDto
import com.etrade.mobilepro.watchlist.dto.WatchlistDto
import com.etrade.mobilepro.watchlist.dto.WatchlistEntriesResponseDto
import com.etrade.mobilepro.watchlist.dto.WatchlistEntryDto
import com.etrade.mobilepro.watchlist.dto.WatchlistResponseDto
import com.squareup.moshi.JsonDataException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.math.BigDecimal

private const val ENTRY_ID = "22050318"
private const val WATCHLIST_ID = "1161254318"
private const val WATCHLIST_NAME = "createnew_v"
private const val WATCHLIST_UUID = "MxyzNxAwMTk5MB=="

internal class WatchlistMappersTest {
    @Test
    fun `test map stock entry`() {
        val response: WatchlistEntriesResponseDto = WatchlistMappersTest::class.getObjectFromJson("get_watchlist_entries.json")
        assertNotNull(response.data.watchListEntries)

        val entriesDto = response.data.watchListEntries
        assertEquals(WATCHLIST_ID, entriesDto?.watchlistId)
        assertEquals(15, entriesDto?.entries?.size)

        val firstEntryDto = entriesDto?.entries?.get(0)
        assertNotNull(firstEntryDto)
        val mappedEntry = WatchlistEntryDtoMapper().map(firstEntryDto ?: WatchlistEntryDto(), WATCHLIST_ID)
        assertTrue("4951382318" == mappedEntry.entryID, "fetched entry id was 495138318")
        assertTrue("QQQ" == mappedEntry.ticker, "ticker was QQQ")
        assertTrue("INVESCO QQQ TR UNIT SER 1" == mappedEntry.description)
        assertTrue(mappedEntry.underlyingSymbol == null, "no underlyer for stock entry")
        assertTrue("EQ" == mappedEntry.typeCode?.typeCode)
        assertEquals(1552593600L, mappedEntry.lastTradeTimeStamp)
        assertTrue("04:00 PM ET 03/14/19" == mappedEntry.lastTradeTime)
        assertTrue(BigDecimal.valueOf(176.71) == mappedEntry.lastPrice)
        assertTrue(BigDecimal.valueOf(-0.3) == mappedEntry.change)
        assertTrue(BigDecimal.valueOf(-0.16948195017230666) == mappedEntry.changePercent)
        assertTrue(BigDecimal.valueOf(23726436) == mappedEntry.volume)
        assertTrue(BigDecimal.valueOf(176.84) == mappedEntry.bid)
        assertTrue(BigDecimal.valueOf(176.85) == mappedEntry.ask)
        assertTrue(BigDecimal.valueOf(177.34) == mappedEntry.dayHigh)
        assertTrue(BigDecimal.valueOf(176.66) == mappedEntry.dayLow)
        assertTrue(BigDecimal.valueOf(187.53) == mappedEntry.week52High)
        assertTrue(BigDecimal.valueOf(143.46) == mappedEntry.week52Low)
        assertTrue(BigDecimal.valueOf(0.0) == mappedEntry.priceEarningsRatio)
        assertTrue(BigDecimal.valueOf(0.0) == mappedEntry.earningsPerShare)
    }

    @Test
    fun `test map user watch lists`() {
        val getWatchListResponse: WatchlistResponseDto = WatchlistMappersTest::class.getObjectFromJson("watchlist_ids_v1.json")
        assertNotNull(getWatchListResponse)
        val watchLists = getWatchListResponse.mobileResponse.references?.get(0)?.data
        assertNotNull(watchLists)
        val investToWatch = WatchlistDtoMapper().map(watchLists?.get(0) ?: WatchlistDto("", null, null))
        assertNotNull(investToWatch)
        assertEquals("338004990", investToWatch.id)
        assertEquals("1", investToWatch.name)
        assertEquals("MzM4MDA0OTkw", investToWatch.uuid)
    }

    @Test
    fun `test json exception error data returned null`() {
        assertThrows(JsonDataException::class.java) {
            WatchlistMappersTest::class.getObjectFromJson<WatchlistResponseDto>("get_watchlists_error.json")
        }
    }

    @Test
    fun `test edit watch list success parsing`() {
        val response: SaveWatchlistEntriesResponseDto = WatchlistMappersTest::class.getObjectFromJson("save_watchlist_response.json")
        assertEquals("SUCCESS", response.data?.message?.code)
    }

    @Test
    fun `test add watch list success parsing`() {
        val response: AddWatchListEntryResponseDto = WatchlistMappersTest::class.getObjectFromJson("add_watchlist_response.json")
        assertEquals(ENTRY_ID, response.data?.watchlistEntry?.entries?.get(0)?.entryID)
        assertEquals(WATCHLIST_ID, response.data?.watchlistEntry?.addedWatchlistId)
    }

    @Test
    fun `test add watch list invalid watchlist id parsing`() {
        assertThrows(JsonDataException::class.java) {
            WatchlistMappersTest::class.getObjectFromJson<AddWatchListEntryResponseDto>("add_watchlist_invalid_watchlist_id_error.json")
        }
    }

    @Test
    fun `test create watch list success parsing`() {
        val response: BaseMobileResponse = getWatchlistsResponse("create_new_watchlist_response.json")
        val newWatchlistDto = response.screen.references?.firstOrNull()?.data as? CreateWatchlistResponseDataDto
        assertEquals(WATCHLIST_ID, newWatchlistDto?.watchListId)
        assertEquals(WATCHLIST_NAME, newWatchlistDto?.watchListName)
        assertEquals(WATCHLIST_UUID, newWatchlistDto?.watchListUuid)
    }

    @Test
    fun `test delete watch list success parsing`() {
        val response: DeleteWatchlistResponseDto = WatchlistMappersTest::class.getObjectFromJson("delete_watchlist_response.json")
        assertEquals("SUCCESS", response.data?.message?.code)
    }

    private fun getWatchlistsResponse(resourcePath: String): BaseMobileResponse =
        this::class.getObjectFromJson(
            resourcePath = resourcePath,
            adapters = listOf(
                addSubtypes(
                    getBaseReferenceAdapterFactory(),
                    WatchlistReferenceType.values().toList()
                ),
                getBaseScreenViewAdapterFactory(),
                getBaseMetaAdapterFactory(),
            )
        )

    private enum class WatchlistReferenceType(
        override val dtoName: String,
        override val dtoClass: Class<out BaseReference<*>>
    ) : DynamicEnum<BaseReference<*>> {
        WATCHLIST_CREATE("watchList_Create", CreateWatchlistResponseDto::class.java)
    }
}
