-if class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelResponseDto
-keepnames class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelResponseDto
-if class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelResponseDto
-keep class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelResponseDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelResponseDto
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelResponseDto
-keepclassmembers class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelResponseDto {
    public synthetic <init>(com.etrade.mobilepro.order.details.dto.CancelOrderResponse,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
