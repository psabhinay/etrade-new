package com.etrade.mobilepro.trade.presentation.util

import android.content.res.Resources
import androidx.annotation.StringRes
import com.etrade.mobilepro.quoteapi.FundsFee
import com.etrade.mobilepro.quoteapi.FundsTradeSummaryItem
import com.etrade.mobilepro.quoteapi.MutualFundPosition
import com.etrade.mobilepro.quoteapi.MutualPositionType.MARGIN
import com.etrade.mobilepro.quoteapi.SalesChargeType
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.HeaderSectionItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection

fun toMutualFundsQuoteSummaryItems(res: Resources, item: FundsTradeSummaryItem): List<TableSectionItemLayout> = with(item) {
    val deferredSalesChargeItems = deferredSalesCharge?.let {
        getSectionsForCharges(res.getString(R.string.trade_label_deferred_charge), it)
    }.orEmpty()

    val frontEndSalesChargeItems =
        frontEndSalesCharge?.let { getSectionsForCharges(res.getString(R.string.trade_label_front_end_charge), it, item.frontLoadText) }.orEmpty()

    val items = listOfNotNull(
        res.toItem(R.string.trade_label_last_trade_timestamp, lastTradeTime)
    ) + if (isFundOwned) {
        item.positions.toPositionSections(res)
    } else {
        initialInvestment(res) + subsequentInvestment(res)
    } + listOfNotNull(
        res.toItem(R.string.trade_label_cut_off_time, ordCutOffTime),
        res.toItem(R.string.trade_label_sales_charge, res.getString(salesChargeType.getStringResId())),
        res.toItem(R.string.trade_label_fund_redemption_fee, fundRedemptionFee),
        res.toItem(R.string.trade_label_transaction_fee, transactionFee),
        res.toItem(R.string.trade_label_early_redemption_fee, etradeEarlyRedemptionFee)
    )

    return items + deferredSalesChargeItems + frontEndSalesChargeItems
}

private fun List<MutualFundPosition>.toPositionSections(res: Resources): Collection<TableSectionItemLayout> {
    return listOfNotNull(HeaderSectionItem(res.getString(R.string.trade_label_shares_information))) + this.map {
        val quantity = it.quantity.toIntOrNull()
        listOfNotNull(
            quantity?.let { qty ->
                res.toItem(
                    labelId = if (it.type == MARGIN) {
                        R.string.trade_label_margin_positions
                    } else {
                        R.string.trade_label_cash_position
                    },
                    value = res.getQuantityString(R.plurals.trade_label_share_information_shares, qty, qty)
                )
            },
            res.toItem(
                labelId = if (it.type == MARGIN) {
                    R.string.trade_label_margin_positions_market_value
                } else {
                    R.string.trade_label_cash_position_market_value
                },
                value = it.marketValue
            )
        )
    }.flatten()
}

private fun FundsTradeSummaryItem.subsequentInvestment(res: Resources) = listOfNotNull(
    if (iraSubsequentInvestment.isNullOrBlank()) {
        res.toItem(R.string.trade_label_sub_min_investment, subsequentInvestment)
    } else {
        res.toItem(R.string.trade_label_ira_sub_min_investment, iraSubsequentInvestment ?: res.getString(R.string.empty_default))
    }
)

private fun FundsTradeSummaryItem.initialInvestment(res: Resources) = listOfNotNull(
    if (iraInitialInvestment.isNullOrBlank()) {
        res.toItem(R.string.trade_label_init_min_investment, initialInvestment)
    } else {
        res.toItem(R.string.trade_label_ira_init_min_investment, iraInitialInvestment ?: res.getString(R.string.empty_default))
    }
)

private fun MutableList<TableSectionItemLayout>.toItem(label: String, value: String) {
    this.add(createSummarySection(label, value))
}

private fun Resources.toItem(@StringRes labelId: Int, value: String) = createSummarySection(getString(labelId), value)

private fun getSectionsForCharges(label: String, charge: List<FundsFee>, subTitle: String? = null): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    sections.add(HeaderSectionItem(label))
    subTitle?.let { sections.add(createSummarySection(it, "")) }
    sections.addAll(
        charge.map {
            createSummarySection(it.range, it.percent)
        }
    )
    return sections
}

private fun SalesChargeType.getStringResId(): Int = when (this) {
    SalesChargeType.FRONT_END -> R.string.trade_sales_charge_front_end
    SalesChargeType.FRONT_END_LOAD -> R.string.trade_sales_charge_front_end_load
    SalesChargeType.BACK_END -> R.string.trade_sales_charge_back_end
    SalesChargeType.NONE -> R.string.trade_sales_charge_none
    else -> R.string.trade_sales_charge_none
}
