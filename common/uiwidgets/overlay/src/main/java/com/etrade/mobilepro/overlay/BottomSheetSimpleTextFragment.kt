package com.etrade.mobilepro.overlay

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.overlay.databinding.FragmentBottomSheetSimpleTextBinding
import com.etrade.mobilepro.util.android.binding.viewBinding

abstract class BottomSheetSimpleTextFragment :
    Fragment(R.layout.fragment_bottom_sheet_simple_text) {

    protected val binding by viewBinding(FragmentBottomSheetSimpleTextBinding::bind)

    abstract val text: CharSequence

    override fun onStart() {
        super.onStart()
        binding.bottomSheetSimpleText.text = text
    }
}
