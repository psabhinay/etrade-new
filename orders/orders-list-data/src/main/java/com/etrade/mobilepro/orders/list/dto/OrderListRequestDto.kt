package com.etrade.mobilepro.orders.list.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OrderListRequestDto(
    @Json(name = "value")
    val orderRequest: RequestValueDto
)

@JsonClass(generateAdapter = true)
data class RequestValueDto(
    @Json(name = "accountId")
    var accountId: String,
    @Json(name = "recordsPerPage")
    var recordsPerPage: Int,
    @Json(name = "fromDate")
    var fromDate: String = "",
    @Json(name = "toDate")
    var toDate: String = "",
    @Json(name = "orderStatus")
    var orderStatus: Int = 0,
    @Json(name = "end")
    var end: String,
    @Json(name = "sortByExpDateTs")
    var sortByExpDateTs: String = "0",
    /**
     * 2 is for descending order on placed dates
     */
    @Json(name = "sortByPlacedTs")
    var sortByPlacedTs: String = "2",
    @Json(name = "countOnly")
    var countOnly: Boolean = false
)
