package com.etrade.mobilepro.markets.data

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.marketsapi.Index
import com.etrade.mobilepro.marketsapi.MoverRepo
import kotlinx.coroutines.rx2.await
import javax.inject.Inject

class MoverRepoImpl @Inject constructor(
    private val apiClient: MoverService
) : MoverRepo {
    override suspend fun getMarketIndices(): ETResult<List<Index>> {
        return apiClient.runCatchingET {
            getMarketIndices()
                .await()
                .let { requireNotNull(it.quotes) }
                .mapNotNull { it.toIndex() }
        }
    }
}
