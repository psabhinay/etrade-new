package com.etrade.mobilepro.chat.presentation

import android.app.Activity
import android.os.Bundle
import androidx.navigation.NavController

interface ChatRouter {

    fun openMenu(navController: NavController)

    fun openChatMessages(navController: NavController, chatSessionData: Bundle?)

    fun openChatSetup(navController: NavController)

    fun popChat(activity: Activity?)

    fun openMessageCenter(navController: NavController, bundle: Bundle)
}
