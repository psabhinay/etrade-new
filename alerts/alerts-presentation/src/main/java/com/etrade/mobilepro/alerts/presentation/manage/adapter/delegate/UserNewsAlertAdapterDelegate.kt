package com.etrade.mobilepro.alerts.presentation.manage.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.alerts.api.model.AlertAction
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.alerts.presentation.databinding.ItemSimpleAlertBinding
import com.etrade.mobilepro.alerts.presentation.manage.adapter.viewholder.UserAlertViewHolder
import com.etrade.mobilepro.common.customview.SwipeRevealLayout
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class UserNewsAlertAdapterDelegate(
    private val alertListener: ((UserAlertItem, AlertAction) -> Unit)?,
    private val swipeRevealListener: ((UserAlertItem, SwipeRevealLayout.Action) -> Unit)?,
    private val dragLocker: (() -> Boolean)?
) : AbsListItemAdapterDelegate<UserAlertItem.NewsAlert, UserAlertItem, UserAlertViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): UserAlertViewHolder {
        return UserAlertViewHolder(parent.viewBinding(ItemSimpleAlertBinding::inflate), alertListener, swipeRevealListener, dragLocker)
    }

    override fun isForViewType(item: UserAlertItem, items: MutableList<UserAlertItem>, position: Int): Boolean {
        return item is UserAlertItem.NewsAlert
    }

    override fun onBindViewHolder(item: UserAlertItem.NewsAlert, holder: UserAlertViewHolder, payloads: MutableList<Any>) {
        holder.bindTo(item)
    }
}
