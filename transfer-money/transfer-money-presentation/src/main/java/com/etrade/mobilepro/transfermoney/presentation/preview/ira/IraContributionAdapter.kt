package com.etrade.mobilepro.transfermoney.presentation.preview.ira

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.livedata.concatCombine
import com.etrade.mobilepro.transfermoney.presentation.contributiondetail.TransferIraContributionDetailViewModel
import com.etrade.mobilepro.transfermoney.presentation.preview.TransferMoneyPreviewViewModel
import com.etrade.mobilepro.util.Resource
import javax.inject.Inject

interface IraContributionAdapter {
    class IraPreviewData(
        val taxYearData: LiveData<List<DropDownMultiSelector.Selectable<String>>>,
        val iraInfoState: LiveData<Resource<String>>
    )
    fun adaptIraContributionToPreview(
        iraContributionDetailViewModel: TransferIraContributionDetailViewModel,
        transferMoneyPreviewViewModel: TransferMoneyPreviewViewModel
    ): IraPreviewData
}

class DefaultIraContributionAdapter @Inject constructor() : IraContributionAdapter {

    override fun adaptIraContributionToPreview(
        iraContributionDetailViewModel: TransferIraContributionDetailViewModel,
        transferMoneyPreviewViewModel: TransferMoneyPreviewViewModel
    ): IraContributionAdapter.IraPreviewData {
        val taxYearData = toTaxYearData(iraContributionDetailViewModel, transferMoneyPreviewViewModel)
        val iraInfoState = toIraInfoState(iraContributionDetailViewModel)
        return IraContributionAdapter.IraPreviewData(taxYearData, iraInfoState)
    }

    private fun toIraInfoState(iraContributionDetailViewModel: TransferIraContributionDetailViewModel): LiveData<Resource<String>> =
        Transformations.map(iraContributionDetailViewModel.iraContributionDetails) {
            when (it) {
                TransferIraContributionDetailViewModel.ViewState.Loading -> Resource.Loading(data = null)
                is TransferIraContributionDetailViewModel.ViewState.Error -> Resource.Failed(data = null, retry = it.retry)
                is TransferIraContributionDetailViewModel.ViewState.Success -> Resource.Success(data = null)
            }
        }

    private fun toTaxYearData(
        iraContributionDetailViewModel: TransferIraContributionDetailViewModel,
        transferMoneyPreviewViewModel: TransferMoneyPreviewViewModel
    ): LiveData<List<DropDownMultiSelector.Selectable<String>>> =
        iraContributionDetailViewModel.iraContributionDetails.concatCombine(transferMoneyPreviewViewModel.selectedIraYear) { viewState, year ->
            if (viewState is TransferIraContributionDetailViewModel.ViewState.Success) {
                viewState.contributionDetails.eligibleYears
                    .filter { it > 0 }
                    .map { yearItem ->
                        val yearAsString = yearItem.toString()
                        DropDownMultiSelector.Selectable(
                            title = yearAsString,
                            value = yearAsString,
                            selected = yearAsString == year
                        )
                    }
            } else {
                emptyList()
            }
        }
}
