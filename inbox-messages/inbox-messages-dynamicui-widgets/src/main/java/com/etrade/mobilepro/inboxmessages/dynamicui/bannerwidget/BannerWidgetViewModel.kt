package com.etrade.mobilepro.inboxmessages.dynamicui.bannerwidget

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.Diffable
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.dynamicui.BR
import com.etrade.mobilepro.inboxmessages.dynamicui.R
import kotlinx.coroutines.launch

class BannerWidgetViewModel(
    val dto: BannerWidgetViewDto,
    val inboxMessagesRepository: InboxMessagesRepository,
    context: Context,
) : DynamicViewModel(R.layout.inbox_message_banner_card), Diffable {
    override val variableId: Int = BR.obj
    val message = dto.data as InboxMessage
    val backgroundColor =
        message.backgroundColor?.let {
            Color.parseColor(it)
        } ?: ContextCompat.getColor(context, R.color.purple)

    init {
        viewModelScope.launch {
            inboxMessagesRepository.setMessageViewed(dto.data as InboxMessage)
        }
    }

    fun onBannerClicked() {
        dto.clickActionDto?.appUrl?.let { CtaEvent.UriEvent(it) }?.let { clickEvents.postValue(it) }
        viewModelScope.launch {
            inboxMessagesRepository.setMessageOpen(message)
        }
    }

    fun onDismissClicked() {
        clickEvents.postValue(CtaEvent.DismissInboxMessage(message))
        viewModelScope.launch {
            inboxMessagesRepository.setMessageSend(message)
        }
    }

    override fun hasSameContents(other: Any): Boolean =
        message.id == (other as BannerWidgetViewModel).message.id

    override fun isSameItem(other: Any): Boolean =
        other.javaClass == javaClass
}
