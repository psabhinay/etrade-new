package com.etrade.mobilepro.tracking

import com.etrade.mobilepro.tracking.params.AnalyticsParams

interface Tracker {

    fun logIn(customerId: String, paramsTrack: AnalyticsParams)

    fun event(paramsTrack: AnalyticsParams)

    fun screen(screen: String, paramsTrack: AnalyticsParams)
}
