package com.etrade.mobilepro.personalnotifications.adapter.holder

import android.view.View
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.personalnotifications.R
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.ExternalAccountVerification
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.Localytics
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.PendingTransaction
import com.etrade.mobilepro.personalnotifications.databinding.ViewTextNotificationItemBinding
import com.etrade.mobilepro.util.formatters.parseCustomDateFromString

private val PENDING_TRANSACTION_INPUT_DATE_FORMAT = listOf(
    "yyyy-MM-dd HH:mm:ss 'EDT'",
    "yyyy-MM-dd HH:mm:ss 'EST'",
    "yyyy-MM-dd HH:mm:ss 'ET'"
)
private const val PENDING_TRANSACTION_OUTPUT_DATE_FORMAT = "MM/dd/yyyy"

class TextNotificationViewHolder(
    val binding: ViewTextNotificationItemBinding,
    val clickListener: ((PersonalNotification) -> Unit)
) : RecyclerView.ViewHolder(binding.root) {
    private val context = binding.root.context
    private lateinit var item: PersonalNotification

    fun bind(item: PersonalNotification, wasRead: Boolean) {
        this.item = item
        setTextStyle(wasRead)
        setClickListener()
        when (item) {
            is ExternalAccountVerification -> {
                setText(
                    context.getString(R.string.verify_external_account_title),
                    context.getString(R.string.verify_external_account_subtitle),
                    true
                )
            }
            is PendingTransaction -> {
                setText(
                    context.getString(R.string.pending_transaction_title),
                    context.getString(
                        R.string.pending_transaction_subtitle,
                        item.amount,
                        item.fundAvailabilityDate.formatDate()
                    )
                )
            }
            is Localytics.Inline -> {
                setText(item.message.subtitle, item.message.message, item.message.deepLink?.isNotEmpty() ?: false)
            }
        }
    }

    private fun setClickListener() {
        binding.root.setOnClickListener {
            this.item.let {
                clickListener.invoke(it)
                TextViewCompat.setTextAppearance(
                    binding.title,
                    R.style.PersonalNotification_Title_Read
                )
            }
        }
    }

    private fun setTextStyle(wasRead: Boolean) {
        wasRead.let {
            val titleStyle = if (it) {
                R.style.PersonalNotification_Title_Read
            } else {
                R.style.PersonalNotification_Title
            }
            TextViewCompat.setTextAppearance(binding.title, titleStyle)
        }
    }

    private fun setText(titleStr: String, subTitleStr: String, showArrow: Boolean = false) {
        with(binding) {
            title.text = titleStr
            subTitle.text = subTitleStr
            arrow.visibility = if (showArrow) {
                View.VISIBLE
            } else {
                View.INVISIBLE
            }
        }
    }

    private fun String.formatDate() =
        PENDING_TRANSACTION_INPUT_DATE_FORMAT.mapNotNull { format ->
            parseCustomDateFromString(
                this,
                format,
                PENDING_TRANSACTION_OUTPUT_DATE_FORMAT
            )
        }.firstOrNull { it.isNotEmpty() } ?: ""
}
