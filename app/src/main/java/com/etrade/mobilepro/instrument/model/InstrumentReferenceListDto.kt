package com.etrade.mobilepro.instrument.model

import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.instrument.dao.InstrumentRealmObject
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class InstrumentReferenceListDto(
    @Json(name = "data")
    override val data: List<InstrumentReferenceDto>
) : BaseReference<List<InstrumentRealmObject>>() {

    override fun convertToModel(): List<InstrumentRealmObject> = data.map {
        InstrumentRealmObject(
            instrumentId = it.instrumentId,
            symbol = it.symbol,
            symbolDescription = it.symbolDescription,
            displaySymbol = it.displaySymbol,
            typeCodeRaw = it.typeCodeRaw,
            exchangeCode = it.exchangeCode,
            bidRaw = it.bidRaw,
            askRaw = it.askRaw,
            daysChangeVal = it.daysChangeVal ?: "",
            daysChangePer = it.daysChangePer ?: "",
            volumeRaw = it.volumeRaw,
            marketCapRaw = it.marketCapRaw,
            pe = it.pe,
            eps = it.eps,
            lastPriceRaw = it.lastPriceRaw,
            week52High = it.week52High,
            week52Low = it.week52Low,
            impliedVolatilityPct = it.impliedVolatilityPct,
            openInterest = it.openInterest,
            deltaRaw = it.deltaRaw,
            premium = it.premium,
            gammaRaw = it.gammaRaw,
            vegaRaw = it.vegaRaw,
            thetaRaw = it.thetaRaw,
            expirationDate = it.expirationDate,
            underlyingTypeCode = it.underlyingTypeCode,
            underlyingExchangeCode = it.underlyingExchangeCode,
            underlyingSymbol = it.underlyingSymbol,
            daysExpiration = it.daysExpiration
        )
    }
}
