package com.etrade.mobilepro.mediaplayer

import android.app.AppOpsManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Pair
import android.view.View
import com.brightcove.player.captioning.BrightcoveCaptionFormat
import com.brightcove.player.edge.Catalog
import com.brightcove.player.edge.VideoListener
import com.brightcove.player.event.EventEmitter
import com.brightcove.player.model.Video
import com.brightcove.player.view.BrightcovePlayer
import com.etrade.mobilepro.mediaplayer.databinding.ActivityEtCloudMediaPlayerBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import org.slf4j.LoggerFactory
import java.util.ArrayList

private const val EXTRA_ID = "EXTRA_ID"
private const val HTTP = "http"
private const val PIP_EVENT = "enterPictureInPictureMode"
private const val PIP_SETTINGS_ACTION = "android.settings.PICTURE_IN_PICTURE_SETTINGS"

class EtCloudMediaPlayer : BrightcovePlayer() {

    private val binding by viewBinding(ActivityEtCloudMediaPlayerBinding::inflate)

    private val logger = LoggerFactory.getLogger(EtCloudMediaPlayer::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(binding.root)

        brightcoveVideoView = binding.videoView
        setupEventListener()

        super.onCreate(savedInstanceState)

        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        brightcoveVideoView.clear()
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        intent.extras?.getString(EXTRA_ID)?.also { videoId ->
            val eventEmitter: EventEmitter = brightcoveVideoView.eventEmitter

            brightcoveVideoView.setClosedCaptioningEnabled(true)
            brightcoveVideoView.setBackgroundResource(R.color.black_icon)

            val catalog = Catalog(eventEmitter, getString(R.string.brightcove_account_key), getString(R.string.brightcove_policy_key))
            catalog.findVideoByID(
                videoId,
                object : VideoListener() {

                    fun fixMissingProtocolForCaptions(video: Video?) {
                        video
                            ?.properties
                            ?.get(Video.Fields.CAPTION_SOURCES)
                            ?.let {
                                @Suppress("UNCHECKED_CAST")
                                it as? ArrayList<Pair<*, *>>
                            }
                            ?.also { list ->
                                val uri: String? = list.getOrNull(0)?.first?.toString()

                                uri
                                    ?.takeUnless { it.startsWith(HTTP) }
                                    ?.also {
                                        val pair = Pair.create(
                                            Uri.parse("https:$it"),
                                            BrightcoveCaptionFormat.createCaptionFormat(
                                                "text/vtt",
                                                "en"
                                            )
                                        )

                                        list[0] = pair
                                    }
                            }
                    }

                    override fun onVideo(video: Video?) {
                        fixMissingProtocolForCaptions(video)

                        brightcoveVideoView.add(video)
                        brightcoveVideoView.start()
                    }

                    override fun onError(error: String?) {
                        logger.error("BrightCove error: $error")
                        binding.videoErrorTextView.visibility = View.VISIBLE
                    }
                }
            )
        }
    }

    private fun setupEventListener() {
        brightcoveVideoView.eventEmitter
            .on(PIP_EVENT) {
                if (!isPipAllowed()) { openPipSettings() }
            }
    }

    private fun isPipAllowed(): Boolean {
        val manager = getSystemService(Context.APP_OPS_SERVICE)
            .let { it as AppOpsManager }
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            manager.unsafeCheckOpNoThrow(AppOpsManager.OPSTR_PICTURE_IN_PICTURE, android.os.Process.myUid(), packageName)
        } else {
            @Suppress("DEPRECATION")
            manager.checkOpNoThrow(AppOpsManager.OPSTR_PICTURE_IN_PICTURE, android.os.Process.myUid(), packageName)
        }.let { it == AppOpsManager.MODE_ALLOWED }
    }

    private fun openPipSettings() {
        val uri = Uri.parse("package:$packageName")
        val intent = Intent(PIP_SETTINGS_ACTION, uri)
        startActivity(intent)
    }

    companion object {

        fun start(context: Context, id: String) {
            val intent = Intent(context, EtCloudMediaPlayer::class.java)

            intent.putExtra(EXTRA_ID, id)

            context.startActivity(intent)
        }
    }
}
