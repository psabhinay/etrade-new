package com.etrade.mobilepro.orders.adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.R
import com.etrade.mobilepro.databinding.ItemOrderBinding
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.getLabel
import com.etrade.mobilepro.orders.list.api.DefaultOrderItem
import com.etrade.mobilepro.orders.list.api.OrderDetailsDescription
import com.etrade.mobilepro.orders.list.api.OrderItem
import com.etrade.mobilepro.orders.list.api.StockPlanOrderItem
import com.etrade.mobilepro.trade.presentation.util.formatLegDetails

class OrderViewHolder(
    private val binding: ItemOrderBinding,
    private var selectItemListener: ((OrderItem, Int) -> Unit)?
) : RecyclerView.ViewHolder(binding.root) {

    private val context = itemView.context

    fun bindTo(order: OrderItem?, isSelected: Boolean) {
        binding.layoutRow.setOnClickListener {
            order?.let { item ->
                selectItemListener?.invoke(item, bindingAdapterPosition)
            }
        }
        if (order is DefaultOrderItem) {
            bindDefaultOrder(order, isSelected)
        }
        if (order is StockPlanOrderItem) {
            bindStockOrder(order, isSelected)
        }
    }

    private fun bindDefaultOrder(order: DefaultOrderItem, isSelected: Boolean) {
        with(binding) {
            textOrderstatus.text = order.orderStatus.getStatus(
                context,
                order.advancedOrderType.getLabel(order.orderType, context.resources)
            )
            textOrderdate.text =
                order.date?.toEpochMilli()?.let { DateFormattingUtils.formatToShortDate(it) }
                ?: context.resources.getString(R.string.not_available)
            order.orderDescriptions.joinToString(separator = System.lineSeparator()) {
                getDescriptionString(context, it)
            }.let {
                textOrderDesc.text = it
                textOrderDesc.contentDescription = "${order.orderType} $it".replace("_", " ")
            }
            layoutRow.isSelected = isSelected
        }
    }

    private fun bindStockOrder(order: StockPlanOrderItem, isSelected: Boolean) {
        with(binding) {
            textOrderstatus.text = order.orderStatus.getStatus(context)
            textOrderdate.text = order.date
            val descText = if (order.orderType.contains(order.planType)) {
                order.orderType
            } else {
                "${order.orderType} ${order.planType}"
            }
            textOrderDesc.text = descText
            textOrderDesc.contentDescription = descText
            layoutRow.isSelected = isSelected
        }
    }

    private fun getDescriptionString(
        context: Context,
        desc: OrderDetailsDescription
    ): CharSequence {
        return context.resources.formatLegDetails(
            desc.transactionType,
            desc.displaySymbol,
            desc.fillQuantityValue
        )
    }

    private fun OrderStatus.getStatus(context: Context, label: String? = null): String {
        val statusString = context.getString(getLabel())
        return if (label == null) {
            statusString
        } else {
            context.getString(R.string.order_status_format, statusString, label)
        }
    }
}
