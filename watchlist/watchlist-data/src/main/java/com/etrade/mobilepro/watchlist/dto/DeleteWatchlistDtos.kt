package com.etrade.mobilepro.watchlist.dto

import com.etrade.mobile.power.backends.neo.BaseUsData
import com.etrade.mobile.power.backends.neo.BaseUsResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DeleteWatchlistResponseDto(
    @Json(name = "data")
    override val data: DeleteWatchlistDataDto?
) : BaseUsResponse()

@JsonClass(generateAdapter = true)
data class DeleteWatchlistDataDto(
    @Json(name = "Messages")
    var message: WatchlistMessage
) : BaseUsData()

@JsonClass(generateAdapter = true)
class DeleteWatchlistRequestDto(
    @Json(name = "value")
    val deleteWatchlistDetail: DeleteWatchlistDetailDto
)

@JsonClass(generateAdapter = true)
data class DeleteWatchlistDetailDto(
    @Json(name = "watchlistID")
    val watchlistId: String = ""
)
