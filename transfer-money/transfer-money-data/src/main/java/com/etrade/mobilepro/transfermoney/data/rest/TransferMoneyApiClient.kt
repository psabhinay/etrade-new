package com.etrade.mobilepro.transfermoney.data.rest

import com.etrade.eo.rest.retrofit.Xml
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface TransferMoneyApiClient {
    @Xml
    @POST("e/t/mobile/QuickTransfer")
    @FormUrlEncoded
    suspend fun getQuickTransferAccounts(@Field("UserId") userId: String): QuickTransferResponseDto

    @JsonMoshi
    @POST("/phx/etm/services/v1/moveMoney/transferActivities")
    suspend fun getTransferActivities(
        @JsonMoshi @Body request: TransferActivitiesRequestDto = TransferActivitiesRequestDto()
    ): TransferActivitiesResponseDto

    @Suppress("LongParameterList")
    @Xml
    @POST("e/t/mobile/QuickTransferPreview")
    @FormUrlEncoded
    suspend fun getQuickTransferPreview(
        @Field("UserId") userId: String,
        @Field("platform") platform: String,
        @Field("ScheduledDate") scheduleDate: String,
        @Field("FromAcctId") fromAcctId: String,
        @Field("FromAcctType") fromAcctType: String,
        @Field("ToAcctId") toAcctId: String,
        @Field("ToAcctType") toAcctType: String,
        @Field("Amount") amount: String,
        @Field("Frequency") frequency: String
    ): QuickTransferPreviewResponse

    @Xml
    @POST("/e/t/mobile/QuickTransferDelete")
    @FormUrlEncoded
    suspend fun cancelTransfer(
        @Field("UserId") userId: String,
        @Field("ReferenceNumber") referenceNumber: String,
        @Field("DeleteSeries") deleteSeries: Boolean
    ): TransferCancelDto

    @Suppress("LongParameterList")
    @Xml
    @POST("e/t/mobile/QuickTransferConfirm")
    @FormUrlEncoded
    suspend fun getQuickTransferConfirm(
        @Field("UserId") userId: String,
        @Field("ScheduledDate") scheduledDate: String,
        @Field("FromAccount") fromAccount: String,
        @Field("ToAccount") toAccount: String,
        @Field("Amount") amount: String,
        @Field("Frequency") frequency: String,
        @Field("platform") platform: String,
        @Field("IRAContribYear") iRAContribYear: String?
    ): QuickTransferConfirmResponse
}
