package com.etrade.mobilepro.tradingdefaults

sealed class TradingDefaultsViewState {

    data class Idle(val items: List<TradingDefaultsItem>) : TradingDefaultsViewState()

    object Saving : TradingDefaultsViewState()

    object Saved : TradingDefaultsViewState()
}
