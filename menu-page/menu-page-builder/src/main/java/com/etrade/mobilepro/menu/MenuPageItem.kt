package com.etrade.mobilepro.menu

import android.net.Uri
import androidx.annotation.DrawableRes
import androidx.navigation.NavDirections
import com.etrade.mobilepro.inboxmessages.InboxMessagesDynamicView
import com.etrade.mobilepro.viewmodel.ActivitySignal

/**
 * Descriptor for items in a Menu Page
 */

interface MenuItem

open class TextMenuItem(
    val type: Type? = null,
    val id: Int? = null,
    val text: CharSequence? = null,
    var subtext: CharSequence? = null,
    val contentDescription: CharSequence? = null,
    @DrawableRes
    var icon: Int? = null,
    var localyticsTag: CharSequence? = null,
    var showNewPill: Boolean = false,
    var showRightIcon: Boolean = false,
    val shouldSaveDirections: Boolean = false,
    val clickable: Boolean = false,
    val menuActions: List<MenuAction> = emptyList()
) : MenuItem {
    sealed class MenuAction {
        class MenuNavDirectionsAction(val navDirections: NavDirections) : MenuAction()
        class MenuActivityAction(val action: ActivitySignal) : MenuAction()
        class MenuGenericAction(val action: () -> Unit) : MenuAction()
        class MenuAppAction(val uri: Uri) : MenuAction()
    }

    sealed class Type {
        object Text : Type()
        object TextWithArrow : Type()
        object TextWithArrowAndSubtitle : Type()
        object Count : Type()
        object Highlight : Type()
    }
}

class MessagesMenuItem(val messages: InboxMessagesDynamicView) : MenuItem

class DividerMenuItem : MenuItem
