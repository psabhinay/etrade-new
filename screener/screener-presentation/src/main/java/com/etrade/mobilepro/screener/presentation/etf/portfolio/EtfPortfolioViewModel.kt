package com.etrade.mobilepro.screener.presentation.etf.portfolio

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.screener.api.Country
import com.etrade.mobilepro.screener.api.Industry
import com.etrade.mobilepro.screener.api.IndustryType
import com.etrade.mobilepro.screener.api.Percent
import com.etrade.mobilepro.screener.api.Region
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.datasource.CountryDataSource
import com.etrade.mobilepro.screener.api.datasource.IndustryTypeDataSource
import com.etrade.mobilepro.screener.api.datasource.PercentDataSource
import com.etrade.mobilepro.screener.api.datasource.RegionDataSource
import com.etrade.mobilepro.screener.api.filter.etf.EtfPortfolioFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Etf
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class EtfPortfolioViewModel @Inject constructor(
    private val regionDataSource: RegionDataSource,
    private val countryDataSource: CountryDataSource,
    private val percentDataSource: PercentDataSource,
    private val industryTypeDataSource: IndustryTypeDataSource,
    aggregator: ScreenerFilterAggregator,
    @Etf screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<EtfPortfolioFilter, EtfPortfolioViewModel.ViewState>(aggregator, ScreenerType.Etf.Portfolio, screenerInteractor) {

    override fun provideInitialState(initialFilter: EtfPortfolioFilter): ViewState = ViewState(filter = initialFilter)

    internal val regionsDropDownListData: LiveData<List<DropDownMultiSelector.Selectable<Region>>> = Transformations.map(viewState) {
        regionDataSource.getRegions().map { region ->
            DropDownMultiSelector.Selectable(
                title = region.title,
                value = region,
                selected = region.id == it.filter.region.id
            )
        }
    }

    internal val regionPercentDropDownListData: LiveData<List<DropDownMultiSelector.Selectable<Percent>>> = Transformations.map(viewState) {
        percentDataSource.getExposurePercentList().map { percent ->
            DropDownMultiSelector.Selectable(
                title = percent.title,
                value = percent,
                selected = percent.id == it.filter.regionPercentInvested.id
            )
        }
    }

    internal val countriesDropDownListData: LiveData<List<DropDownMultiSelector.Selectable<Country>>> = Transformations.map(viewState) {
        countryDataSource.getCountries().map { country ->
            DropDownMultiSelector.Selectable(
                title = country.title,
                value = country,
                selected = country.id == it.filter.country.id
            )
        }
    }

    internal val countryPercentDropDownListData: LiveData<List<DropDownMultiSelector.Selectable<Percent>>> = Transformations.map(viewState) {
        percentDataSource.getExposurePercentList().map { percent ->
            DropDownMultiSelector.Selectable(
                title = percent.title,
                value = percent,
                selected = percent.id == it.filter.countryPercentInvested.id
            )
        }
    }

    internal val industryTypeDropDownListData: LiveData<List<DropDownMultiSelector.Selectable<IndustryType>>> = Transformations.map(viewState) {
        industryTypeDataSource.getIndustryTypes().map { industry ->
            DropDownMultiSelector.Selectable(
                title = industry.title,
                value = industry,
                selected = industry.id == it.filter.industryType.id
            )
        }
    }

    internal val industryTypePercentDropDownListData: LiveData<List<DropDownMultiSelector.Selectable<Percent>>> = Transformations.map(viewState) {
        percentDataSource.getExposurePercentList().map { percent ->
            DropDownMultiSelector.Selectable(
                title = percent.title,
                value = percent,
                selected = percent.id == it.filter.industryTypePercentInvested.id
            )
        }
    }

    internal val industryDropDownListData: LiveData<List<DropDownMultiSelector.Selectable<Industry>>> = Transformations.map(viewState) {
        industryTypeDataSource.getIndustriesOfType(it.filter.industryType).map { industry ->
            DropDownMultiSelector.Selectable(
                title = industry.title,
                value = industry,
                selected = industry.id == it.filter.industry.id
            )
        }
    }

    internal fun regionSelected(region: Region) {
        updateState {
            if (region.isAllItemsElement) {
                it.copy(
                    filter = it.filter.copy(
                        region = region,
                        regionPercentInvested = percentDataSource.getEmptyPercent()
                    )
                )
            } else {
                it.copy(filter = it.filter.copy(region = region))
            }
        }
    }

    internal fun regionPercentSelected(percent: Percent) {
        updateState {
            it.copy(filter = it.filter.copy(regionPercentInvested = percent))
        }
    }

    internal fun countrySelected(country: Country) {
        updateState {
            if (country.isAllItemsElement) {
                it.copy(
                    filter = it.filter.copy(
                        country = country,
                        countryPercentInvested = percentDataSource.getEmptyPercent()
                    )
                )
            } else {
                it.copy(filter = it.filter.copy(country = country))
            }
        }
    }

    internal fun countryPercentSelected(percent: Percent) {
        updateState {
            it.copy(filter = it.filter.copy(countryPercentInvested = percent))
        }
    }

    internal fun industryTypeSelected(industryType: IndustryType) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    industryType = industryType,
                    industry = industryTypeDataSource.getAllIndustriesItem(),
                    industryTypePercentInvested = percentDataSource.getEmptyPercent()
                )
            )
        }
    }

    internal fun industrySelected(industry: Industry) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    industry = industry,
                    industryTypePercentInvested = percentDataSource.getEmptyPercent()
                )
            )
        }
    }

    internal fun industryTypePercentSelected(percent: Percent) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    industryTypePercentInvested = percent
                )
            )
        }
    }

    data class ViewState(
        override val filter: EtfPortfolioFilter
    ) : BaseFilterScreenViewModel.ViewState<EtfPortfolioFilter> {
        fun regionPercentEnabled(): Boolean = !filter.region.isAllItemsElement
        fun countryPercentEnabled(): Boolean = !filter.country.isAllItemsElement
        fun industryTypePercentEnabled(): Boolean = !filter.industry.isAllItemsElement
        fun industryEnabled(): Boolean = !filter.industryType.isAllItemsElement
    }
}
