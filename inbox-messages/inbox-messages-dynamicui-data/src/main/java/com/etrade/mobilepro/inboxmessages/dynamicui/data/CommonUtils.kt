package com.etrade.mobilepro.inboxmessages.dynamicui.data

import com.etrade.completeview.dto.AccountSummaryDto
import com.etrade.mobile.accounts.dto.AccountDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.inboxmessages.InboxAttributes
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.util.guard

internal fun findCorrespondingInboxMessage(messages: ETResult<List<InboxMessage>>, promptCode: String, isSystemInDarkMode: Boolean): InboxMessage? {
    val promptMessages = messages.getOrNull().orEmpty().filter { it.rawAttributes[InboxAttributes.USER_TYPE].orEmpty().contains(promptCode) }
    return promptMessages.find { message ->
        if (isSystemInDarkMode) {
            message.isForDarkMode()
        } else {
            message.isForDarkMode().not()
        }
    }
}

internal fun findAccountDto(view: BaseScreenView, accounts: List<AccountDto>): AccountDto? {
    val accountViewSummaryData = view.data as? AccountSummaryDto guard {
        return null
    }
    return accounts.find { it.accountUuid == accountViewSummaryData.accountUuid }
}

private const val DARK_MODE_FLAG = "D"
internal fun InboxMessage.isForDarkMode(): Boolean = this.rawAttributes[InboxAttributes.USER_TYPE].orEmpty().contains(DARK_MODE_FLAG)
