package com.etrade.mobilepro.quotes

import com.etrade.mobilepro.common.di.DeviceType
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

class PerformanceFragment @Inject constructor(@DeviceType deviceType: String) : WebViewFragment(deviceType)
