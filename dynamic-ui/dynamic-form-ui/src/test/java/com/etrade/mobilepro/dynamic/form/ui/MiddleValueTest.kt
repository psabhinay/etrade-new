package com.etrade.mobilepro.dynamic.form.ui

import org.junit.Assert.assertEquals
import org.junit.Test

class MiddleValueTest {

    // Tests do not cover negative values because there are no requirements for them yet.

    @Test(expected = IllegalArgumentException::class)
    fun `check invalid values`() {
        middleValue(1, 0)
    }

    @Test
    fun `check middle of the same values`() {
        0.let { assertEquals(it, middleValue(it, it)) }
        1.let { assertEquals(it, middleValue(it, it)) }
        100.let { assertEquals(it, middleValue(it, it)) }
        12345.let { assertEquals(it, middleValue(it, it)) }
    }

    @Test
    fun `check middle values`() {
        assertEquals(1, middleValue(0, 2))
        assertEquals(1, middleValue(1, 2))
        assertEquals(5, middleValue(4, 6))
        assertEquals(5, middleValue(5, 6))
        assertEquals(100, middleValue(0, 200))
        assertEquals(100, middleValue(1, 200))
        assertEquals(150, middleValue(100, 200))
    }
}
