// Code generated by moshi-kotlin-codegen. Do not edit.
@file:Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION",
    "RedundantExplicitType", "LocalVariableName", "RedundantVisibilityModifier",
    "PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package com.etrade.mobilepro.order.details.dto

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.`internal`.Util
import java.lang.NullPointerException
import kotlin.String
import kotlin.Suppress
import kotlin.Unit
import kotlin.collections.emptySet
import kotlin.text.buildString

public class CancelOrderResponseJsonAdapter(
  moshi: Moshi
) : JsonAdapter<CancelOrderResponse>() {
  private val options: JsonReader.Options = JsonReader.Options.of("orderId")

  private val stringAdapter: JsonAdapter<String> = moshi.adapter(String::class.java, emptySet(),
      "orderId")

  public override fun toString(): String = buildString(41) {
      append("GeneratedJsonAdapter(").append("CancelOrderResponse").append(')') }

  public override fun fromJson(reader: JsonReader): CancelOrderResponse {
    var orderId: String? = null
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> orderId = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("orderId",
            "orderId", reader)
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    return CancelOrderResponse(
        orderId = orderId ?: throw Util.missingProperty("orderId", "orderId", reader)
    )
  }

  public override fun toJson(writer: JsonWriter, value_: CancelOrderResponse?): Unit {
    if (value_ == null) {
      throw NullPointerException("value_ was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("orderId")
    stringAdapter.toJson(writer, value_.orderId)
    writer.endObject()
  }
}
