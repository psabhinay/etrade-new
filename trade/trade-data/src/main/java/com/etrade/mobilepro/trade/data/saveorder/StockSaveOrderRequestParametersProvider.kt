package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.data.CommonOrderConstants.ORDER_TYPE
import com.etrade.mobilepro.trade.data.CommonOrderConstants.PREPARED_ID
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.etrade.mobilepro.trade.data.form.input.DefaultOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultStockTradeFormInputId
import com.etrade.mobilepro.trade.data.isTrailingStop
import com.etrade.mobilepro.trade.data.stock.StockOrderConstants
import com.etrade.mobilepro.trade.data.stock.resolvePrices
import com.etrade.mobilepro.trade.form.api.getTransactionType
import com.etrade.mobilepro.trade.form.api.input.extractAction
import com.etrade.mobilepro.trade.form.api.input.extractPriceType
import com.etrade.mobilepro.trade.form.api.input.extractTerm
import com.etrade.mobilepro.util.safeParseBigDecimal

internal class StockSaveOrderRequestParametersProvider : SaveOrderRequestParametersProvider {

    override val path: String = "savepreparedorder"

    override fun createBody(params: InputValuesSnapshot): InputValuesSnapshot {
        val body: MutableMap<String, Any?> = mutableMapOf()
        params.toMutableMap().apply {
            resolvePrices(this)
            val symbol = DefaultStockTradeFormInputId.extractSymbol(this)
            body[DefaultOptionTradeFormInputId.accountId] = this[DefaultStockTradeFormInputId.accountId]
            body[DefaultOptionTradeFormInputId.allOrNone] = this[DefaultStockTradeFormInputId.allOrNone]
            body[DefaultOptionTradeFormInputId.tradeLegs] = getTradeLegList(symbol ?: "")
            body[DefaultOptionTradeFormInputId.limitPrice] = this[DefaultStockTradeFormInputId.limitPrice]
            body[DefaultOptionTradeFormInputId.term] = DefaultStockTradeFormInputId.extractTerm(this)?.typeCode
            body[ORDER_TYPE] = getStockOrderType()?.typeCode
            body[PREPARED_ID] = this[DefaultStockTradeFormInputId.orderId]
            body[DefaultOptionTradeFormInputId.priceType] = DefaultStockTradeFormInputId.extractPriceType(this)?.typeCode
            body[DefaultOptionTradeFormInputId.strategy] = StrategyType.STOCK.typeCode
            body[DefaultOptionTradeFormInputId.stopPrice] = this[DefaultStockTradeFormInputId.stopPrice] ?: this[StockOrderConstants.OFFSET_VALUE]
            body[DefaultOptionTradeFormInputId.symbol] = symbol
        }
        return mapOf("value" to body)
    }

    private fun InputValuesSnapshot.getTradeLegList(symbol: String): List<LegDetailsCommonDto> {
        val quantity = (this[DefaultStockTradeFormInputId.quantity] as? String)?.safeParseBigDecimal()?.toInt() ?: 0
        return listOf(
            LegDetailsCommonDto().also {
                it.symbol = symbol
                it.transactionCode = getTransactionType().typeCode.toString()
                it.quantity = quantity
            }
        )
    }

    private fun InputValuesSnapshot.getTransactionType(): TransactionType {
        val tradeAction = DefaultStockTradeFormInputId.extractAction(this)
        return tradeAction?.getTransactionType(false) ?: TransactionType.UNKNOWN
    }

    private fun InputValuesSnapshot.getStockOrderType(): OrderType? =
        when {
            isTrailingStop(DefaultStockTradeFormInputId) -> OrderType.ADVANCE_EQUITY
            else -> OrderType.EQUITY
        }
}
