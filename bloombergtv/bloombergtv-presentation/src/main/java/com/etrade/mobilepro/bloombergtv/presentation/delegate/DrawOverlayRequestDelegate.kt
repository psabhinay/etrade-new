package com.etrade.mobilepro.bloombergtv.presentation.delegate

import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvActivity
import com.etrade.mobilepro.bloombergtv.presentation.R
import com.google.android.material.snackbar.Snackbar

internal class DrawOverlayRequestDelegate : BloombergTvActivityDelegate {
    private lateinit var activity: BloombergTvActivity
    private lateinit var settingsRequest: ActivityResultLauncher<Intent>
    private var onPermissionGranted: (() -> Unit)? = null

    fun hasDrawOverlayPermission(onPermissionGranted: () -> Unit): Boolean {
        this.onPermissionGranted = onPermissionGranted
        /** check if we already  have permission to draw over other apps  */
        if (!Settings.canDrawOverlays(activity)) {
            /** if not construct intent to request permission  */
            val intent = Intent(
                Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:${activity.packageName}")
            )
            settingsRequest.launch(intent)
        } else {
            return true
        }
        return false
    }

    override fun init(activity: BloombergTvActivity) {
        this.activity = activity
        /** request permission via start activity for result  */
        settingsRequest = activity.registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (Settings.canDrawOverlays(activity)) {
                onPermissionGranted?.invoke()
            } else {
                // permission not granted
                activity.snackbarUtil
                    .snackbar(activity.getString(R.string.bloomberg_pip_not_granted_error_message), Snackbar.LENGTH_SHORT)
                    ?.show()
            }
        }
    }
}
