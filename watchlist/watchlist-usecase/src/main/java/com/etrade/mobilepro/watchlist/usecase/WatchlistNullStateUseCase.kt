package com.etrade.mobilepro.watchlist.usecase

import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.INVESTMENTS_TO_WATCH_NAME
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.qualifiers.DefaultWatchlistRepository
import com.etrade.mobilepro.watchlistapi.qualifiers.DeviceWatchlistRepository
import com.etrade.mobilepro.watchlistapi.qualifiers.HasAddedToWatchlist
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import kotlinx.coroutines.rx2.await
import javax.inject.Inject

interface WatchlistNullStateUseCase : UseCase<List<Watchlist>, Boolean>

class DefaultWatchlistNullStateUseCase @Inject constructor(
    @HasAddedToWatchlist private val hasAddedToWatchlist: Boolean,
    @DeviceWatchlistRepository private val deviceWatchlistRepo: WatchlistRepo,
    @DefaultWatchlistRepository private val defaultWatchlistRepo: WatchlistRepo
) : WatchlistNullStateUseCase {

    /**
     * Requirements:
     * * User only has the default device watch list & Investments to Watch on their app
     * * User has never added any symbols to the device watch list or Investments to Watch
     *
     * [HAS_ADDED_TO_WATCHLIST] is used to set a user preference when a symbol
     * is added to a watch list. If that preference has not been set, then we check whether
     * additional watch lists have been added. If not, then we check whether the device watchlist
     * or investments to watch has any entries. We can't directly check whether the user has ever added a symbol to
     * Device Watchlist on other devices, but the preference let's us know whether one has been
     * added on this device.
     **/
    override suspend fun execute(parameter: List<Watchlist>): Boolean {
        if (hasAddedToWatchlist || parameter.size > AUTHENTICATED_MAX_EMPTY_WATCHLISTS) {
            return false
        }

        return parameter.find { it.name == INVESTMENTS_TO_WATCH_NAME }?.let { investmentToWatch ->
            deviceWatchlistRepo.getWatchlistSymbols(DEVICE_WATCHLIST_ID).await().isEmpty() &&
                defaultWatchlistRepo.getWatchlistSymbols(investmentToWatch.id).await().isEmpty()
        } ?: deviceWatchlistRepo.getWatchlistSymbols(DEVICE_WATCHLIST_ID).await().isEmpty()
    }

    companion object {
        // Device Watchlist and Investments to Watch
        const val AUTHENTICATED_MAX_EMPTY_WATCHLISTS = 2
    }
}
