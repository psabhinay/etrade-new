package com.etrade.mobilepro.optionschain.tableview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.optionschain.tableview.OptionItemClickListener
import com.etrade.mobilepro.optionschain.tableview.SelectedOption
import com.etrade.mobilepro.optionschain.tableview.viewholder.OptionsListViewHolder
import com.etrade.mobilepro.util.android.extension.dispatchUpdates

class OptionsListAdapter(
    private val lifecycleOwner: LifecycleOwner,
    @LayoutRes
    private val layoutId: Int,
    context: Context
) : AbstractOptionChainAdapter<SelectedOption, OptionsListViewHolder>(context) {

    var optionClickListener: OptionItemClickListener? = null

    private var options: List<SelectedOption> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OptionsListViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            layoutId,
            parent,
            false
        )
        binding.lifecycleOwner = lifecycleOwner
        return OptionsListViewHolder(binding)
    }

    override fun OptionsListViewHolder.bindWithData(data: SelectedOption) {
        bind(data)
        itemView.setOnClickListener {
            optionClickListener?.onOptionClicked(data.option)
        }
    }

    override fun getItemCount(): Int = options.size

    override fun OptionsListViewHolder.bind(position: Int) {
        bindWithData(options[position])
    }

    fun setOptionsData(data: List<SelectedOption>) {
        val oldOptions = options
        options = data
        dispatchUpdates(oldOptions, options)
    }
}
