package com.etrade.mobilepro.earnings

import android.content.res.Resources
import com.etrade.mobilepro.earnings.dto.EarningsResponse
import com.etrade.mobilepro.earnings.viewModel.MarketEarnings
import com.etrade.mobilepro.earnings.viewModel.MarketEarningsType
import com.etrade.mobilepro.earnings.viewModel.createMarketEarnings
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.util.json.BigDecimalAdapter
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.threeten.bp.LocalDateTime
import org.threeten.bp.Month

class MarketEarningsTest {

    private val resources: Resources = mockResourcesString()

    private fun testEarningsCreation(path: String, currentDateTime: LocalDateTime, expected: MarketEarnings) {
        val response: EarningsResponse = MarketEarningsTest::class.getObjectFromJson(
            path,
            adapters = listOf(BigDecimalAdapter)
        )

        assertNotNull(response)
        assertNotNull(response.response?.detail?.first()?.period)

        val result: MarketEarnings = createMarketEarnings(
            earningsDetailData = createMarkerEarningsResponseResult(response.response!!.detail!!.first().period!!),
            resources = resources,
            currentDateTime = currentDateTime
        )

        assertEquals(expected.type, result.type)
        assertEquals(expected.title, result.title)
        assertEquals(expected.message, result.message)
        assertEquals(expected.high, result.high)
        assertEquals(expected.mean, result.mean)
        assertEquals(expected.low, result.low)
        assertEquals(expected.dateTimeForCalendar, result.dateTimeForCalendar)
    }

    @Test
    fun `test earnings creation`() {
        testEarningsCreation(
            "for_earnings_0.json",
            LocalDateTime.of(2019, Month.OCTOBER, 4, 17, 30),
            MarketEarnings(
                MarketEarningsType.EARNINGS,
                "Earnings",
                "Next earnings date 12/11/2019 est.",
                null,
                null,
                null,
                LocalDateTime.of(2019, Month.DECEMBER, 11, 6, 6)
            )
        )

        testEarningsCreation(
            "for_earnings_1.json",
            LocalDateTime.of(2019, Month.OCTOBER, 4, 17, 30),
            MarketEarnings(
                MarketEarningsType.EARNINGS,
                "Earnings",
                "Next earnings date 10/24/2019 est.",
                null,
                null,
                null,
                LocalDateTime.of(2019, Month.OCTOBER, 24, 1, 0)
            )
        )
    }

    @Test
    fun `test earnings release creation`() {
        testEarningsCreation(
            "for_earnings_release_0.json",
            LocalDateTime.of(2019, Month.OCTOBER, 4, 17, 30),
            MarketEarnings(
                MarketEarningsType.EARNINGS_RELEASE,
                "Earnings Release",
                "Q4 earnings to be released on 10/04/2019",
                "-0.61",
                "-0.625",
                "-0.64",
                LocalDateTime.of(2019, Month.OCTOBER, 4, 7, 0)
            )
        )

        testEarningsCreation(
            "for_earnings_release_1.json",
            LocalDateTime.of(2019, Month.OCTOBER, 8, 17, 30),
            MarketEarnings(
                MarketEarningsType.EARNINGS_RELEASE,
                "Earnings Release",
                "Q4 earnings to be released on 10/09/2019",
                "0.06",
                "0.042",
                "0.03",
                LocalDateTime.of(2019, Month.OCTOBER, 9, 16, 30)
            )
        )
    }

    @Test
    fun `test earnings announcement creation`() {
        testEarningsCreation(
            "for_earnings_announcement_0.json",
            LocalDateTime.of(2019, Month.OCTOBER, 8, 16, 57),
            MarketEarnings(
                MarketEarningsType.EARNINGS_ANNOUNCEMENT,
                "Earnings Announcement",
                "Q2 earnings of $0.09 were announced on 10/04/2019, missing analyst estimate by $0.09",
                null,
                null,
                null,
                null
            )
        )

        testEarningsCreation(
            "for_earnings_announcement_1.json",
            LocalDateTime.of(2019, Month.OCTOBER, 8, 16, 57),
            MarketEarnings(
                MarketEarningsType.EARNINGS_ANNOUNCEMENT,
                "Earnings Announcement",
                "Q2 earnings of $0.0 were announced on 10/07/2019, matches analyst estimate",
                null,
                null,
                null,
                null
            )
        )
    }

    @Test
    fun `sets earnings announcement creation message to beating when eps actual greater than mean estimate`() {
        testEarningsCreation(
            "for_earnings_announcement_2.json",
            LocalDateTime.of(2019, Month.OCTOBER, 8, 16, 57),
            MarketEarnings(
                MarketEarningsType.EARNINGS_ANNOUNCEMENT,
                "Earnings Announcement",
                "Q2 earnings of $2.02 were announced on 10/07/2019, beating analyst estimate by $1.01",
                null,
                null,
                null,
                null
            )
        )
    }

    @Test
    fun `sets earnings creation with empty date when earnings size is less than two`() {
        testEarningsCreation(
            "for_earnings_2.json",
            LocalDateTime.of(2019, Month.OCTOBER, 4, 17, 30),
            MarketEarnings(
                type = MarketEarningsType.EARNINGS,
                title = resources.getString(R.string.earnings),
                message = resources.getString(R.string.earnings_date).format(resources.getString(R.string.empty_date_default))
            )
        )
    }

    @Test
    fun `sets next earning date null when current and earnings date is two days past`() {
        testEarningsCreation(
            "for_earnings_2.json",
            LocalDateTime.of(2019, Month.OCTOBER, 1, 17, 30),
            MarketEarnings(
                MarketEarningsType.EARNINGS,
                "Earnings",
                resources.getString(R.string.earnings_date).format(resources.getString(R.string.empty_date_default)),
                null,
                null,
                null,
                null
            )
        )
    }
}
