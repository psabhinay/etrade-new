package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee

import com.etrade.mobilepro.common.result.ETResult

data class ActionState<T>(
    val isLoading: Boolean = false,
    val isActive: Boolean? = null,
    val result: ETResult<T>? = null,
    val retry: (() -> Unit)? = null
)
