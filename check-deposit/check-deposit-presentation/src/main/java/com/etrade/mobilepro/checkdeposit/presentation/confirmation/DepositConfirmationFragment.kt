package com.etrade.mobilepro.checkdeposit.presentation.confirmation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.checkdeposit.presentation.CheckDepositNavigationFlowViewModel
import com.etrade.mobilepro.checkdeposit.presentation.R
import com.etrade.mobilepro.checkdeposit.presentation.databinding.FragmentDepositConfirmationBinding
import com.etrade.mobilepro.checkdeposit.presentation.deposit.CheckDepositViewModel
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

@RequireLogin
class DepositConfirmationFragment @Inject constructor(
    private val viewModelProvider: ViewModelProvider.Factory
) : Fragment(R.layout.fragment_deposit_confirmation) {

    private val binding by viewBinding(FragmentDepositConfirmationBinding::bind)

    private val viewModel: CheckDepositViewModel by navGraphViewModels(R.id.check_deposit_graph) { viewModelProvider }
    private val navigationFlowViewModel: CheckDepositNavigationFlowViewModel by navGraphViewModels(R.id.check_deposit_graph)
    private val args: DepositConfirmationFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_deposit_confirmation, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setConfirmationView()
        if (savedInstanceState == null) { binding.confirmationView.playAnimation() }

        viewModels<CheckDepositConfirmationViewModel> { viewModelProvider }.value
    }

    override fun onResume() {
        super.onResume()
        viewModel.resetDetails()
    }

    private fun setConfirmationView() = binding.confirmationView.apply {
        setAmountText(args.amount)
        setConfirmationText(getString(R.string.check_deposit_posting_details))

        setDetailsTable(
            listOf(
                createSummarySection(
                    getString(R.string.check_deposit_confirmation_number), args.confirmationNumber,
                    valueDescription = args.confirmationNumber.characterByCharacter()
                ),
                createSummarySection(
                    getString(R.string.check_deposit_account), args.account,
                    valueDescription = args.account.characterByCharacter()
                )
            )
        )

        setActionButton(
            getString(R.string.check_deposit_view_deposit),
            View.OnClickListener {
                navigationFlowViewModel.switchToDepositActivity()
                navigationFlowViewModel.checkDepositRequestId = args.confirmationNumber

                findNavController().popBackStack()
            }
        )
    }
}
