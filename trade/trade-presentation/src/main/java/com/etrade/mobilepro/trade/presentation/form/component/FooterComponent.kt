package com.etrade.mobilepro.trade.presentation.form.component

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.dynamic.form.api.Component

internal class FooterComponent(
    val links: LiveData<List<FooterLink>>,
    override val isVisible: LiveData<Boolean> = MutableLiveData(true)
) : Component

internal class FooterLink(
    val title: String,
    val action: () -> Unit
)
