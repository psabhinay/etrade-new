package com.etrade.mobilepro.portfolio.model

import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.data.baseValueExtractors
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.util.SortOrder

data class SortInfo(
    val column: PortfolioRecordColumn,
    val order: SortOrder
)

fun SortInfo.createComparator(): Comparator<PortfolioRecord> = column.createComparator(order)

private fun PortfolioRecordColumn.createComparator(sortOrder: SortOrder): Comparator<PortfolioRecord> {
    return when (sortOrder) {
        SortOrder.ASCENDING -> Comparator { o1, o2 -> compareValues(getComparableProperty(o1), getComparableProperty(o2)) }
        SortOrder.DESCENDING -> Comparator { o1, o2 -> compareValues(getComparableProperty(o2), getComparableProperty(o1)) }
        SortOrder.UNDEFINED -> Comparator { _, _ -> 0 }
    }
}

private fun PortfolioRecordColumn.getComparableProperty(record: PortfolioRecord): Comparable<*>? = baseValueExtractors.getValue(this)(record)
