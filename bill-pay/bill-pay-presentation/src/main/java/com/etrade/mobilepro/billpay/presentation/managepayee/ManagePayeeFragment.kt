package com.etrade.mobilepro.billpay.presentation.managepayee

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.billpay.presentation.BillPayContainerFragmentDirections
import com.etrade.mobilepro.billpay.presentation.BillPayViewModel
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.billpay.presentation.common.toParcelable
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayFragmentPayeesListBinding
import com.etrade.mobilepro.billpay.presentation.managepayee.adapter.PayeesListAdapter
import com.etrade.mobilepro.billpay.presentation.viewmodel.ManagePayeeViewModelFactoryFactory
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.disposeOnDestinationChanged
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import javax.inject.Inject

@RequireLogin
class ManagePayeeFragment @Inject constructor(
    private val assistedFactory: ManagePayeeViewModelFactoryFactory,
    private val viewModelFactory: ViewModelProvider.Factory
) : Fragment(R.layout.bill_pay_fragment_payees_list) {

    private val binding by viewBinding(BillPayFragmentPayeesListBinding::bind)

    private val sharedViewModel: BillPayViewModel by navGraphViewModels(R.id.bill_pay_nav_graph) {
        viewModelFactory
    }

    private val viewModel: ManagePayeeViewModel by navGraphViewModels(R.id.bill_pay_nav_graph) {
        assistedFactory.create(
            sharedViewModel.payees.value ?: emptyList()
        )
    }

    private val payeeAdapter: PayeesListAdapter = PayeesListAdapter {
        findNavController().navigate(
            BillPayContainerFragmentDirections.actionPayeeListToPayeeDetails(it.toParcelable())
        )
    }

    private val disposeListener = disposeOnDestinationChanged { sharedViewModel.disposeState() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModels()
    }

    override fun onResume() {
        super.onResume()
        findNavController().addOnDestinationChangedListener(disposeListener)
    }

    override fun onPause() {
        super.onPause()
        findNavController().removeOnDestinationChangedListener(disposeListener)
    }

    private fun setupFragment() {
        binding.swipeRefresh.setOnRefreshListener { sharedViewModel.refreshPayees() }
        binding.swipeRefresh.isRefreshing = false
        with(binding.recyclerView) {
            layoutManager = LinearLayoutManager(requireContext())
            setupRecyclerViewDivider(R.drawable.thin_divider)
            adapter = payeeAdapter
        }

        binding.buttonAddPayee.setOnClickListener {
            sharedViewModel.viewState.value?.let {
                requireNotNull(it.billPaySessionId) { "No Bill Pay Session Id" }
                findNavController().navigate(
                    BillPayContainerFragmentDirections.actionManagePayeeToPayeeForm(it.billPaySessionId)
                )
            }
        }
    }

    @Suppress("LongMethod")
    private fun bindViewModels() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer { state ->
                binding.noContent.visibility = if (state.items?.isEmpty() == true) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

                state.items?.let { payeeAdapter.items = it }
            }
        )

        sharedViewModel.viewState.observe(
            viewLifecycleOwner,
            Observer { state ->
                binding.swipeRefresh.isRefreshing = state.isRefreshing
            }
        )

        sharedViewModel.payees.observe(
            viewLifecycleOwner,
            Observer { payeeList ->
                viewModel.setPayees(payeeList)
            }
        )

        sharedViewModel.addedPayee.observe(
            viewLifecycleOwner,
            Observer {
                // In case a payee gets added we need to consume the event to avoid show it on payment
                it.getNonConsumedContent()?.let { newPayee ->
                    val navController = findNavController()
                    if (navController.currentDestination?.id == R.id.billPayContainerFragment) {
                        navController.navigate(BillPayContainerFragmentDirections.actionPayeeListToPayeeDetails(newPayee.toParcelable()))
                    }
                }
            }
        )
    }
}
