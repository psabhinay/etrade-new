package com.etrade.mobilepro.orders.presentation

import android.content.res.Resources
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderDetailDescription
import com.etrade.mobilepro.orders.api.ExecInstruction
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.orders.api.MarketSession
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.orders.presentation.section.viewfactory.MutualOrderSectionViewFactory
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.SummarySectionItem
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.math.BigDecimal

class MutualFundOrderDetailSectionTest {

    @Mock
    private lateinit var resources: Resources

    private lateinit var orderDetailLabelResources: OrderDetailLabelResources

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        orderDetailLabelResources = generateMockOrderDetailLabelResources(resources)
        Mockito.`when`(resources.getString(anyInt())).thenReturn(" ")
    }

    @Test
    fun `test mutual fund order sell or buy`() {
        val mutualOrderSectionViewFactory = MutualOrderSectionViewFactory(orderDetailLabelResources)
        val order = getFundsOrderDetail(listOf(mockTradeLeg()))

        val orderInfoItem = mutualOrderSectionViewFactory.createOrderSectionView(order).filter {
            (it as SummarySectionItem).label == "Order Number" ||
                it.label == "Order Placed" ||
                it.label == "Order Status"
        }

        val quantityItem = mutualOrderSectionViewFactory.createOrderLegSectionView(order.orderDescriptions.first()).filter {
            (it as SummarySectionItem).label == "Quantity"
        }
        val commissionFees = mutualOrderSectionViewFactory.mutualFundFeesSection(order).filter {
            (it as SummarySectionItem).label == "Estimated Commission/Fees"
        }
        assertTrue(orderInfoItem.size == 3)
        assertTrue(quantityItem.isNotEmpty())
        assertTrue(commissionFees.isNotEmpty())
    }

    @Test
    fun `test mutual fund exchange order with sell and buy leg`() {
        val mutualOrderSectionViewFactory = MutualOrderSectionViewFactory(orderDetailLabelResources)
        val order = getFundsOrderDetail(listOf(mockTradeLeg(), mockTradeLeg()))

        val orderInfoItem = mutualOrderSectionViewFactory.createOrderSectionView(order).filter {
            (it as SummarySectionItem).label == "Order Number" ||
                it.label == "Order Placed" ||
                it.label == "Order Status"
        }

        val quantityItem = order.orderDescriptions.map {
            mutualOrderSectionViewFactory.createOrderLegSectionView(it).filter { sections ->
                (sections as SummarySectionItem).label == "Quantity"
            }
        }

        val commissionFees = mutualOrderSectionViewFactory.mutualFundFeesSection(order).filter {
            (it as SummarySectionItem).label == "Estimated Commission/Fees"
        }
        assertTrue(orderInfoItem.size == 3)
        assertTrue(quantityItem.size == 2)
        assertTrue(commissionFees.isNotEmpty())
    }

    private fun getFundsOrderDetail(legs: List<OrderDetailDescription>): BaseOrderDetailsItem.OrderDetailsItem = BaseOrderDetailsItem.OrderDetailsItem(
        OrderStatus.OPEN, OrderType.EQUITY, PriceType.MARKET, "200", "12345",
        "09/01/2019", "09/01/2019", OrderTerm.GOOD_UNTIL_CANCEL, null, BigDecimal("10.95"), BigDecimal("5.95"), BigDecimal.ZERO, null,
        null, "12345", legs, null, null, TransactionType.BUY_OPEN, BigDecimal.ZERO, BigDecimal.ZERO, null,
        null, null, null, false, null, false, false, null, MarketSession.MARKET_REGULAR,
        MarketDestination.AUTO, OrigSysCode.ORIG_SYS_CODE_ANDR, ExecInstruction(false, false, false, false), null, null,
        false, 0, false, "1", tradeLegs = emptyList()
    )

    private fun mockTradeLeg(): OrderDetailDescription = OrderDetailDescription(
        displaySymbol = "DNJXX",
        transactionType = TransactionType.BUY_OPEN,
        displayQuantity = "123.456",
        originalSymbol = "DNJXX",
        fillQuantity = "123",
        quantity = "123"
    )
}
