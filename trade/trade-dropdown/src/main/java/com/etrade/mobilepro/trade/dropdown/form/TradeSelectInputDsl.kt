package com.etrade.mobilepro.trade.dropdown.form

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.dynamic.form.api.ContentDescriptionFormatter
import com.etrade.mobilepro.dynamic.form.api.DropDown
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.InputHolder
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.SelectInputBuilder
import com.etrade.mobilepro.trade.dropdown.help.TradeHelpParams

@Suppress("LongParameterList")
class TradeSelectInput(
    id: String,
    title: LiveData<CharSequence?>,
    customContentDescription: CharSequence? = null,
    contentDescriptionFormatter: ContentDescriptionFormatter?,
    isVisible: LiveData<Boolean>,
    isEnabled: LiveData<Boolean> = MutableLiveData(true),
    shouldInterceptClick: LiveData<Boolean> = MutableLiveData(false),
    dropDown: DropDown,
    toolBarActionEnd: ToolbarActionEnd? = null,
    val helpParams: TradeHelpParams? = null
) : SelectInput(
    id,
    title,
    customContentDescription,
    contentDescriptionFormatter,
    isVisible,
    isEnabled,
    shouldInterceptClick,
    dropDown,
    toolBarActionEnd

)

class TradeSelectInputBuilder internal constructor(id: String, inputHolder: InputHolder) : SelectInputBuilder(id, inputHolder) {

    var helpParams: TradeHelpParams? = null

    override fun create() = TradeSelectInput(
        id,
        dynamicTitle,
        contentDescription,
        contentDescriptionFormatter,
        isVisible,
        isEnabled,
        shouldInterceptClick,
        options,
        toolbarActionEnd,
        helpParams
    )
}

fun GroupBuilder.tradeSelect(id: String, init: TradeSelectInputBuilder.() -> Unit) {
    custom {
        TradeSelectInputBuilder(id, this).apply(init).create()
    }
}
