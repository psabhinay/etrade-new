package com.etrade.mobilepro.screener.presentation.stock.pricevolume

import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.stock.PriceVolumeFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Stock
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class PriceVolumeViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @Stock screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<PriceVolumeFilter, PriceVolumeViewModel.ViewState>(
    aggregator,
    ScreenerType.Stock.PriceAndVolume,
    screenerInteractor
) {
    internal val greaterThanSharePriceData = MutableLiveData<String>()
    internal val lessThanSharePriceData = MutableLiveData<String>()

    override fun provideInitialState(initialFilter: PriceVolumeFilter): ViewState {
        greaterThanSharePriceData.value = initialFilter.sharePriceGreaterThan
        lessThanSharePriceData.value = initialFilter.sharePriceLessThan
        return ViewState(filter = initialFilter)
    }

    data class ViewState(
        override val filter: PriceVolumeFilter
    ) : BaseFilterScreenViewModel.ViewState<PriceVolumeFilter>
}
