package com.etrade.mobilepro.bloombergtv.widget

import android.graphics.Bitmap
import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("setThumbnail")
fun setThumbnail(iv: ImageView, thumbnail: Bitmap?) {
    thumbnail?.let {
        iv.setImageBitmap(it)
    }
}
