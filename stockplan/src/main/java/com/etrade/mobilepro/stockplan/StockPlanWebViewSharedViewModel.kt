package com.etrade.mobilepro.stockplan

import android.webkit.WebResourceRequest
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import java.util.WeakHashMap
import javax.inject.Inject

const val SWITCH_LANGUAGE_REQUEST_ENDPOINT = "switchlanguage.json"

class StockPlanWebViewSharedViewModel @Inject constructor() : ViewModel() {
    private val map = WeakHashMap<StockPlanWebViewFragment, () -> Unit>()

    internal fun onWebViewRequest(fragmentClass: Class<StockPlanWebViewFragment>, fragmentState: Lifecycle.State, request: WebResourceRequest) {
        if (fragmentState.isAtLeast(Lifecycle.State.RESUMED) && request.url.path?.contains(SWITCH_LANGUAGE_REQUEST_ENDPOINT) == true) {
            // to break a cycle listeners are notified only if the signal came from a resumed fragment
            map.entries.forEach { entry ->
                if (entry.key.javaClass.name != fragmentClass.name) {
                    entry.value?.invoke()
                }
            }
        }
    }

    internal fun addLanguageChangeListener(fragment: StockPlanWebViewFragment, listener: () -> Unit) {
        map[fragment] = listener
    }

    internal fun removeLanguageChangeListener(fragment: StockPlanWebViewFragment) {
        map.remove(fragment)
    }
}
