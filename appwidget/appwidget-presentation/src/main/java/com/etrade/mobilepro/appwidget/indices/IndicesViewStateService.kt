package com.etrade.mobilepro.appwidget.indices

import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.distinctUntilChanged
import com.etrade.mobilepro.appwidget.R
import com.etrade.mobilepro.appwidget.api.isUninitialized
import com.etrade.mobilepro.appwidget.common.getPlacedWidgetIds
import com.etrade.mobilepro.appwidget.widgetcontainer.WidgetViewStateHandler
import com.etrade.mobilepro.common.AppNotificationChannel
import com.etrade.mobilepro.common.createNotification
import com.etrade.mobilepro.common.di.EntryPointActivity
import com.etrade.mobilepro.searchingapi.items.EXTRA_QUOTE_TICKER
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.util.debounce
import dagger.android.AndroidInjection
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val ACTION_INIT_LAYOUT = "action.init_layout"
private const val ACTION_LOAD = "action_load"
private const val ACTION_ADD_SYMBOL_TO_SELECTED_WATCHLIST = "IndicesViewStateService.action.add_symbol_to_selected_watchlist"

private const val EXTRA_APP_WIDGET_IDS = "extra_app_widget_ids"
private const val EXTRA_FORCE_REFRESH = "extra.force_refresh"

private const val NOTIFICATION_ID = 345
private const val LAYOUT_INIT_DEBOUNCE_IN_MS = 1000L
private const val SERVICE_RELEASE_DEBOUNCE_IN_MS = 2000L

internal fun getLayoutInitializationIntent(
    context: Context,
    appWidgetIds: IntArray,
    forceRefresh: Boolean = false
): Intent {
    return Intent(context, IndicesViewStateService::class.java).apply {
        action = ACTION_INIT_LAYOUT
        putExtra(EXTRA_APP_WIDGET_IDS, appWidgetIds)
        putExtra(EXTRA_FORCE_REFRESH, forceRefresh)
    }
}

internal fun getWidgetIndicesRefreshIntent(context: Context): Intent {
    return Intent(context, IndicesViewStateService::class.java).apply {
        action = ACTION_LOAD
    }
}

internal fun getWidgetIndicesRefreshPendingIntent(context: Context): PendingIntent {
    return getWidgetIndicesRefreshIntent(context)
        .run {
            PendingIntent.getForegroundService(context, 0, this, PendingIntent.FLAG_IMMUTABLE)
        }
}

internal fun getAddSymbolToWatchlistPendingIntent(context: Context, symbol: SearchResultItem.Symbol): Intent {
    return Intent(context, IndicesViewStateService::class.java).apply {
        action = ACTION_ADD_SYMBOL_TO_SELECTED_WATCHLIST
        putExtras(symbol.toBundle())
    }
}

class IndicesViewStateService : LifecycleService() {

    @Inject
    @EntryPointActivity
    internal lateinit var activityClass: Class<out Activity>

    @Inject
    internal lateinit var viewModel: IndicesViewModel

    @Inject
    internal lateinit var viewStateHandler: WidgetViewStateHandler

    private val mainScope: CoroutineScope by lazy { MainScope() }
    private val debounceLayoutInitialization = debounce(LAYOUT_INIT_DEBOUNCE_IN_MS, mainScope, ::initLayout)

    private var pendingServiceRelease: Job? = null

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
        // Readme.md#Note-6
        setupForegroundExecution()
        setupViewModel()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        // Readme.md#Note-7
        setupForegroundExecution()
        pendingServiceRelease?.cancel()
        if (!viewModel.isWidgetPlaced) {
            scheduleServiceRelease()
        } else {
            when (intent?.action) {
                ACTION_ADD_SYMBOL_TO_SELECTED_WATCHLIST -> {
                    intent.getParcelableExtra<SearchResultItem.Symbol>(EXTRA_QUOTE_TICKER)
                        ?.let(viewModel::addSymbolToWatchlist)
                }
                ACTION_LOAD -> viewModel.onDataRequested()
                ACTION_INIT_LAYOUT -> debounceLayoutInitialization(intent)
                else -> scheduleServiceRelease()
            }
        }

        return START_NOT_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        mainScope.cancel()
        viewModel.dispose()
    }

    private fun setupForegroundExecution() {
        val pendingIntent = if (::activityClass.isInitialized) {
            getRealPendingIntent()
        } else {
            getDummyPendingIntent()
        }

        val notification = createNotification(this, AppNotificationChannel.Silent)
            .setSmallIcon(R.drawable.ic_generic_notification)
            .setContentTitle(getString(R.string.product_name))
            .setContentText(getString(R.string.appwidget_notification_message))
            .setContentIntent(pendingIntent)
            .build()

        startForeground(NOTIFICATION_ID, notification)
    }

    private fun scheduleServiceRelease() {
        pendingServiceRelease = mainScope.launch {
            delay(SERVICE_RELEASE_DEBOUNCE_IN_MS)
            stopSelf()
        }
    }

    private fun setupViewModel() {
        viewModel.emissionsCompletedSignal.observe(this) { scheduleServiceRelease() }
        viewModel.viewState.distinctUntilChanged().observe(this) { viewState ->
            viewStateHandler.handleViewState(viewState, getPlacedWidgetIds())
        }
    }

    private fun getRealPendingIntent() = Intent(applicationContext, activityClass).run {
        flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        PendingIntent.getActivity(applicationContext, 0, this, PendingIntent.FLAG_IMMUTABLE)
    }

    private fun getDummyPendingIntent() = Intent("${this::class.java.name}.dummy.action").let {
        PendingIntent.getBroadcast(applicationContext, 0, it, PendingIntent.FLAG_IMMUTABLE)
    }

    private fun initLayout(intent: Intent) {
        val appWidgetIds = intent.getIntArrayExtra(EXTRA_APP_WIDGET_IDS) ?: return
        val forceRefresh = intent.getBooleanExtra(EXTRA_FORCE_REFRESH, false)

        val currentViewState = viewModel.viewState.value

        viewStateHandler.initViewState(appWidgetIds)
        currentViewState?.let {
            viewStateHandler.handleViewState(currentViewState, appWidgetIds)
        }

        if (currentViewState.isUninitialized || forceRefresh) {
            viewModel.onDataRequested()
        } else if (!viewModel.isStreaming) {
            scheduleServiceRelease()
        }
    }
}
