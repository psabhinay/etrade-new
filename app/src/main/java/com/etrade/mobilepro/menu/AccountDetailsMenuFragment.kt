package com.etrade.mobilepro.menu

import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.menu.viewmodel.AccountDetailsMenuViewModel
import com.etrade.mobilepro.userviewmodel.UserViewModel
import javax.inject.Inject

class AccountDetailsMenuFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    userViewModel: UserViewModel
) : AppBaseMenuFragment(viewModelFactory, userViewModel) {
    override val menuPageViewModel by viewModels<AccountDetailsMenuViewModel> { viewModelFactory }
}
