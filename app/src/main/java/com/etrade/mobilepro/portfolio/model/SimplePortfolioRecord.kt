package com.etrade.mobilepro.portfolio.model

import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.api.Position

data class SimplePortfolioRecord(
    override val recordId: String,
    override val displayName: String,
    override val isTaxLot: Boolean,
    override val position: Position,
    override val instrument: Instrument
) : PortfolioRecord
