package com.etrade.mobilepro.dao.realm

import io.realm.log.LogLevel
import io.realm.log.RealmLogger
import org.slf4j.LoggerFactory
import org.slf4j.MarkerFactory

class RealmSlf4jLogger : RealmLogger {
    private val logger = LoggerFactory.getLogger(RealmSlf4jLogger::class.java)

    override fun log(level: Int, tag: String, throwable: Throwable?, message: String?) {
        val marker = MarkerFactory.getMarker(tag)
        when (level) {
            LogLevel.ALL, LogLevel.TRACE -> logger.trace(marker, message)
            LogLevel.DEBUG -> logger.debug(marker, message)
            LogLevel.INFO -> logger.info(marker, message)
            LogLevel.WARN -> logger.warn(marker, message)
            LogLevel.ERROR, LogLevel.FATAL -> logger.error(marker, message, throwable)
        }
    }
}
