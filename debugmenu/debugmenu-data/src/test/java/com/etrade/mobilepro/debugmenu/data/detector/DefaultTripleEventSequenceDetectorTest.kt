package com.etrade.mobilepro.debugmenu.data.detector

import com.etrade.mobilepro.util.android.timer.TimeoutTimer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.threeten.bp.Duration

internal class DefaultTripleEventSequenceDetectorTest {

    @Test
    fun successScenario() {
        val timerStub = getTimeoutTimerStub()
        val sut = DefaultTripleEventSequenceDetector(timerStub)
        var triggerCount = 0
        sut.setup { triggerCount++ }
        sut.onEvent()
        sut.onEvent()
        sut.onEvent()
        timerStub.stop()
        assertEquals(1, triggerCount)
    }

    @Test
    fun negativeScenarioTimeoutOccursEarlier() {
        val timerStub = getTimeoutTimerStub()
        val sut = DefaultTripleEventSequenceDetector(timerStub)
        var triggerCount = 0
        sut.setup { triggerCount++ }
        sut.onEvent()
        sut.onEvent()
        timerStub.stop()
        sut.onEvent()
        assertEquals(0, triggerCount)
    }

    private fun getTimeoutTimerStub(): TimeoutTimer = object : TimeoutTimer {
        private var actionOnStop: (() -> Unit)? = null

        override fun start(timeout: Duration, actionOnStop: (() -> Unit)?) {
            this.actionOnStop = actionOnStop
        }

        override fun stop() {
            this.actionOnStop?.invoke()
            this.actionOnStop = null
        }

        override fun isActive(): Boolean = this.actionOnStop != null
    }
}
