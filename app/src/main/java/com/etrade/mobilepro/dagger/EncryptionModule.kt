package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.encryption.api.Base64Processor
import com.etrade.mobilepro.encryption.api.EncryptionProvider
import com.etrade.mobilepro.encryption.api.SecretKeyRepo
import com.etrade.mobilepro.encryption.api.SymmetricAlgorithmSpec
import com.etrade.mobilepro.encryption.data.AesSymmetricAlgorithmSpec
import com.etrade.mobilepro.encryption.data.AndroidSecretKeyRepo
import com.etrade.mobilepro.encryption.data.DefaultBase64Processor
import com.etrade.mobilepro.encryption.data.DefaultEncryptionProvider
import com.etrade.mobilepro.encryption.viewmodel.CipherAuthenticatorViewModel
import com.etrade.mobilepro.encryption.viewmodel.CipherAuthenticatorViewModelDelegate
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP EVERYTHING IN THIS FILE ORGANIZED ALPHABETICALLY */

@Module
abstract class EncryptionModule {

    @Binds
    @Singleton
    abstract fun bindBase64Processor(provider: DefaultBase64Processor): Base64Processor

    @Binds
    abstract fun bindCipherAuthenticatorViewModel(delegate: CipherAuthenticatorViewModelDelegate): CipherAuthenticatorViewModel

    @Binds
    @Singleton
    abstract fun bindEncryptionProvider(provider: DefaultEncryptionProvider): EncryptionProvider

    @Binds
    @Singleton
    abstract fun bindSecretKeyRepo(repo: AndroidSecretKeyRepo): SecretKeyRepo

    @Binds
    @Singleton
    abstract fun bindSymmetricAlgorithm(spec: AesSymmetricAlgorithmSpec): SymmetricAlgorithmSpec
}
