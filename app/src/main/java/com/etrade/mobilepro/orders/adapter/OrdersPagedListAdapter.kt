package com.etrade.mobilepro.orders.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.databinding.ItemOrderBinding
import com.etrade.mobilepro.news.presentation.adapter.NetworkStateItemViewHolder
import com.etrade.mobilepro.news.presentation.databinding.ItemNetworkStateLoadingBinding
import com.etrade.mobilepro.orders.list.api.OrderItem
import com.etrade.mobilepro.util.android.extension.viewBinding

private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<OrderItem>() {
    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: OrderItem, newItem: OrderItem): Boolean =
        oldItem == newItem

    override fun areItemsTheSame(oldItem: OrderItem, newItem: OrderItem): Boolean =
        oldItem.orderNumber == newItem.orderNumber

    override fun getChangePayload(oldItem: OrderItem, newItem: OrderItem): Any? {
        return null
    }
}

class OrdersPagingDataAdapter(
    private val selectItemListener: ((OrderItem, Int) -> Unit)? = null,
    private val selectionChecker: ((Int) -> Boolean)? = null
) : PagingDataAdapter<OrderItem, RecyclerView.ViewHolder>(DIFF_CALLBACK) {
    lateinit var context: Context
    var isLoading: Boolean = false
        set(value) {
            field = value
            if (value) {
                notifyItemInserted(super.getItemCount())
            } else {
                notifyItemRemoved(super.getItemCount())
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return when (viewType) {
            R.layout.item_order -> {
                OrderViewHolder(parent.viewBinding(ItemOrderBinding::inflate), selectItemListener)
            }
            R.layout.item_network_state_loading -> {
                NetworkStateItemViewHolder(parent.viewBinding(ItemNetworkStateLoadingBinding::inflate))
            }
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_order -> {
                (holder as OrderViewHolder).bindTo(
                    getItem(position),
                    isSelected = selectionChecker?.invoke(position) ?: false
                )
            }
            R.layout.item_network_state_loading -> (holder as NetworkStateItemViewHolder).bindTo(isLoading)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (isLoading && position == itemCount - 1) {
            R.layout.item_network_state_loading
        } else {
            R.layout.item_order
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (isLoading) 1 else 0
    }
}
