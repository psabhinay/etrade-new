package com.etrade.mobilepro.chart

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.R
import com.etrade.mobilepro.chart.netassets.NetAssetsChartFragment
import com.etrade.mobilepro.chart.performance.PerformanceChartFragment
import com.etrade.mobilepro.util.android.adapter.DefaultFragmentPagerAdapter
import com.etrade.mobilepro.util.android.adapter.DefaultFragmentPagerAdapter.FragmentPagerItem

private val chartFragments = listOf(
    FragmentPagerItem(PerformanceChartFragment::class.java, R.string.chart_performance_title),
    FragmentPagerItem(NetAssetsChartFragment::class.java, R.string.chart_net_assets_title)
)

class ChartPagerAdapter(fragment: Fragment) : DefaultFragmentPagerAdapter(fragment, chartFragments)
