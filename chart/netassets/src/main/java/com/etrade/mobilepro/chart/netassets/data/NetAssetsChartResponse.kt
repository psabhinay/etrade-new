package com.etrade.mobilepro.chart.netassets.data

import com.etrade.mobilepro.chart.data.ChartDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class NetAssetsChartResponseDto(
    @Json(name = "data")
    val data: NetAssetsChartResponseDataDto?
)

@JsonClass(generateAdapter = true)
internal class NetAssetsChartResponseDataDto(
    @Json(name = "overview")
    val overview: ChartDto
)
