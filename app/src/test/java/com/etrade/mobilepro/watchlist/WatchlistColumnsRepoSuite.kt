package com.etrade.mobilepro.watchlist

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.WatchlistColumn
import com.etrade.mobilepro.watchlist.presentation.WatchlistColumnRepoImpl
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito

class WatchlistColumnsRepoSuite {

    @Test
    fun `test default columns order`() {
        checkGettingColumns(
            null,
            listOf(
                WatchlistColumn.LastPrice,
                WatchlistColumn.ChangeDollar,
                WatchlistColumn.ChangePercent,
                WatchlistColumn.Volume,
                WatchlistColumn.Bid,
                WatchlistColumn.Ask,
                WatchlistColumn.DayHigh,
                WatchlistColumn.DayLow,
                WatchlistColumn.FiftyTwoWeekHigh,
                WatchlistColumn.FiftyTwoWeekLow,
                WatchlistColumn.PriceEarningRatio,
                WatchlistColumn.EarningsPerShare
            )
        )
    }

    @Test
    fun `test repo contains different order`() {
        checkGettingColumns(
            "Ask,Bid,ChangeDollar,ChangePercent,DayHigh,DayLow,EarningsPerShare,FiftyTwoWeekHigh,FiftyTwoWeekLow,LastPrice,PriceEarningRatio,Volume",
            listOf(
                WatchlistColumn.Ask,
                WatchlistColumn.Bid,
                WatchlistColumn.ChangeDollar,
                WatchlistColumn.ChangePercent,
                WatchlistColumn.DayHigh,
                WatchlistColumn.DayLow,
                WatchlistColumn.EarningsPerShare,
                WatchlistColumn.FiftyTwoWeekHigh,
                WatchlistColumn.FiftyTwoWeekLow,
                WatchlistColumn.LastPrice,
                WatchlistColumn.PriceEarningRatio,
                WatchlistColumn.Volume
            )
        )
    }

    @Test
    fun `test repo contains wrong item`() {
        // this may be the case when we remove some new columns in a newer version of the app
        checkGettingColumns(
            "Ask,Bart,Bid,ChangeDollar,ChangePercent,DayHigh,DayLow,EarningsPerShare,FiftyTwoWeekHigh,FiftyTwoWeekLow,Homer,LastPrice,PriceEarningRatio,Volume",
            listOf(
                WatchlistColumn.Ask,
                WatchlistColumn.Bid,
                WatchlistColumn.ChangeDollar,
                WatchlistColumn.ChangePercent,
                WatchlistColumn.DayHigh,
                WatchlistColumn.DayLow,
                WatchlistColumn.EarningsPerShare,
                WatchlistColumn.FiftyTwoWeekHigh,
                WatchlistColumn.FiftyTwoWeekLow,
                WatchlistColumn.LastPrice,
                WatchlistColumn.PriceEarningRatio,
                WatchlistColumn.Volume
            )
        )
    }

    @Test
    fun `test repo contains less items`() {
        // this may be the case when we add some new columns in a newer version of the app
        checkGettingColumns(
            "Ask,ChangeDollar,DayHigh,EarningsPerShare,FiftyTwoWeekLow,PriceEarningRatio",
            listOf(
                WatchlistColumn.Ask,
                WatchlistColumn.ChangeDollar,
                WatchlistColumn.DayHigh,
                WatchlistColumn.EarningsPerShare,
                WatchlistColumn.FiftyTwoWeekLow,
                WatchlistColumn.PriceEarningRatio,
                WatchlistColumn.LastPrice,
                WatchlistColumn.ChangePercent,
                WatchlistColumn.Volume,
                WatchlistColumn.Bid,
                WatchlistColumn.DayLow,
                WatchlistColumn.FiftyTwoWeekHigh
            )
        )
    }

    @Test
    fun `test repo saving columns`() {
        checkSavingColumns(
            listOf(
                WatchlistColumn.Ask,
                WatchlistColumn.Bid,
                WatchlistColumn.ChangeDollar,
                WatchlistColumn.ChangePercent,
                WatchlistColumn.DayHigh,
                WatchlistColumn.DayLow,
                WatchlistColumn.EarningsPerShare,
                WatchlistColumn.FiftyTwoWeekHigh,
                WatchlistColumn.FiftyTwoWeekLow,
                WatchlistColumn.LastPrice,
                WatchlistColumn.PriceEarningRatio,
                WatchlistColumn.Volume
            )
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `test repo saving columns failed`() {
        checkSavingColumns(
            listOf(
                WatchlistColumn.Ask,
                WatchlistColumn.ChangeDollar,
                WatchlistColumn.DayHigh,
                WatchlistColumn.EarningsPerShare,
                WatchlistColumn.FiftyTwoWeekLow,
                WatchlistColumn.PriceEarningRatio
            )
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `test repo saving columns with duplicates`() {
        checkSavingColumns(
            listOf(
                WatchlistColumn.Ask,
                WatchlistColumn.Bid,
                WatchlistColumn.ChangeDollar,
                WatchlistColumn.ChangePercent,
                WatchlistColumn.Ask,
                WatchlistColumn.DayLow,
                WatchlistColumn.EarningsPerShare,
                WatchlistColumn.FiftyTwoWeekHigh,
                WatchlistColumn.FiftyTwoWeekLow,
                WatchlistColumn.LastPrice,
                WatchlistColumn.PriceEarningRatio,
                WatchlistColumn.Bid
            )
        )
    }

    private fun checkGettingColumns(storageValue: String?, expectedColumns: List<WatchlistColumn>) {
        val storage = Mockito.mock(KeyValueStorage::class.java)
        val repo = WatchlistColumnRepoImpl(storage)

        whenever(storage.getStringValue(any(), anyOrNull())).thenReturn(storageValue)
        assertEquals(expectedColumns, runBlocking { repo.getColumns() })
    }

    private fun checkSavingColumns(newColumns: List<WatchlistColumn>) {
        val storage = Mockito.mock(KeyValueStorage::class.java)
        val repo = WatchlistColumnRepoImpl(storage)
        runBlocking { repo.saveColumns(newColumns) }
    }
}
