package com.etrade.mobilepro.screener.api.filter.mutualfund

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

private val customResultFields = listOf(
    "FrontLoad", "DeferLoad", "RedemptionFee"
)

data class FeesFilter(
    val noLoadTransaction: Boolean = false,
    val transactionFee: Boolean = false,
    val frontEndLoad: Boolean = false,
    val deferredLoad: Boolean = false
) : ScreenerFilter {
    override fun isEmpty(): Boolean = !noLoadTransaction && !transactionFee && !frontEndLoad && !deferredLoad

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND

    override fun customResultFields(): List<String> = customResultFields
}
