package com.etrade.mobilepro.debugmenu.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.etrade.mobilepro.appconfig.api.AppConfig
import com.etrade.mobilepro.appconfig.api.AppEnvironment
import com.etrade.mobilepro.appconfig.api.BaseUrls
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.debugmenu.api.EnvChange
import com.etrade.mobilepro.debugmenu.api.repo.DebugMenuRepo
import com.etrade.mobilepro.debugmenu.presentation.item.CircuitBreakerItem
import com.etrade.mobilepro.debugmenu.presentation.item.EnvironmentItem
import com.etrade.mobilepro.debugmenu.presentation.item.FeatureFlagsItem
import com.etrade.mobilepro.debugmenu.presentation.item.ForceCrashItem
import com.etrade.mobilepro.debugmenu.presentation.item.SectionItem
import com.etrade.mobilepro.debugmenu.presentation.item.TimeoutsItem
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.guard
import kotlinx.coroutines.launch
import javax.inject.Inject

interface DebugMenuNavDirections {
    val featureFlags: NavDirections
}

class DebugMenuViewModel @Inject constructor(
    private val appConfigRepo: AppConfigRepo,
    private val debugMenuRepo: DebugMenuRepo,
    private val directions: DebugMenuNavDirections
) : ViewModel() {
    private val _viewState = MutableLiveData<ViewState>()
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    private val _navigateActions = MutableLiveData<ConsumableLiveEvent<NavDirections>>()
    internal val navigateActions: LiveData<ConsumableLiveEvent<NavDirections>>
        get() = _navigateActions

    internal fun loadData() {
        if (_viewState.value == null) {
            _viewState.value = ViewState(
                appConfig = appConfigRepo.getAppConfig(),
                status = ViewState.Status.Idle,
            )
        }
    }

    internal fun toggleCircuitBreaker(enabled: Boolean) {
        _viewState.updateValue {
            it.copy(appConfig = it.appConfig.copy(isCircuitBreakEnabled = enabled), status = ViewState.Status.Restart)
        }
        saveAppConfig()
    }

    internal fun updateTimeout(limitSession: Boolean) {
        _viewState.updateValue {
            it.copy(appConfig = it.appConfig.copy(timeouts = it.appConfig.timeouts.copy(limitSessionTo30Sec = limitSession)), status = ViewState.Status.Restart)
        }
        saveAppConfig()
    }

    internal fun navigateToFeatureFlags() {
        _navigateActions.value = ConsumableLiveEvent(directions.featureFlags)
    }

    internal fun updateAppEnvironment(appEnvironment: AppEnvironment) {
        when (appEnvironment) {
            _viewState.value?.appConfig?.environment -> _viewState.updateValue {
                it.copy(status = ViewState.Status.ExitScreen)
            }
            AppEnvironment.DR,
            AppEnvironment.DR_EXTERNAL,
            AppEnvironment.M5,
            AppEnvironment.MOCK,
            AppEnvironment.PLT,
            AppEnvironment.PROD_M3_ONLY,
            AppEnvironment.PROD_M5_ONLY,
            AppEnvironment.SIT,
            AppEnvironment.UAT -> _viewState.updateValue {
                it.copy(status = ViewState.Status.EnterCredentials.Idle, appEnvCandidate = appEnvironment)
            }
            AppEnvironment.PROD -> {
                _viewState.updateValue {
                    it.copy(status = ViewState.Status.Restart, appConfig = it.appConfig.copy(environment = appEnvironment))
                }
                saveAppConfig(newBaseUrls = BaseUrls())
            }
        }
    }

    internal fun updateEnvironment(name: String?, pwd: String?) {
        val appEnvironment = _viewState.value?.appEnvCandidate guard {
            _viewState.updateValue {
                it.copy(status = ViewState.Status.EnterCredentials.CredError.NoAppEnvSelected)
            }
            return
        }
        if (name.isNullOrBlank() || pwd.isNullOrBlank()) {
            _viewState.updateValue {
                it.copy(status = ViewState.Status.EnterCredentials.CredError.AuthError(EnvChange.AuthFailed))
            }
            return
        }
        _viewState.updateValue {
            it.copy(status = ViewState.Status.EnterCredentials.Loading)
        }
        performEnvChange(appEnvironment, name, pwd)
    }

    internal fun cancelEnvironmentUpdate() = _viewState.updateValue {
        it.copy(status = ViewState.Status.Idle)
    }

    private fun performEnvChange(appEnvironment: AppEnvironment, name: String, pwd: String) {
        viewModelScope.launch {
            debugMenuRepo.requestEnvChange(appEnvironment, name, pwd).fold(
                onSuccess = { envChange ->
                    when (envChange) {
                        is EnvChange.Success -> {
                            _viewState.updateValue {
                                it.copy(status = ViewState.Status.Restart, appConfig = it.appConfig.copy(environment = appEnvironment))
                            }
                            saveAppConfig(envChange.urls)
                        }
                        EnvChange.AuthFailed,
                        is EnvChange.ServerError -> _viewState.updateValue {
                            it.copy(status = ViewState.Status.EnterCredentials.CredError.AuthError(envChange))
                        }
                    }
                },
                onFailure = {
                    _viewState.updateValue {
                        it.copy(status = ViewState.Status.EnterCredentials.CredError.NetworkError)
                    }
                }
            )
        }
    }

    private fun saveAppConfig(newBaseUrls: BaseUrls? = null) = _viewState.value?.let {
        if (appConfigRepo.getAppConfig() != it.appConfig) {
            appConfigRepo.setEnvironment(it.appConfig.environment, newBaseUrls ?: it.appConfig.baseUrls)
            appConfigRepo.setTimeouts(it.appConfig.timeouts)
            appConfigRepo.setMarketCircuitBreaker(it.appConfig.isCircuitBreakEnabled)
        }
    }

    internal data class ViewState(
        val appConfig: AppConfig,
        val status: Status,
        val appEnvCandidate: AppEnvironment? = null
    ) {
        val items = listOf(
            SectionItem(R.string.debugmenu_section_env)
        ) + AppEnvironment.values().map { EnvironmentItem(it, isChecked = it == appConfig.environment) } + listOf(
            SectionItem(R.string.debugmenu_section_timeouts),
            TimeoutsItem(appConfig.timeouts),
            SectionItem(R.string.debugmenu_section_other),
            FeatureFlagsItem(),
            CircuitBreakerItem(appConfig.isCircuitBreakEnabled),
            ForceCrashItem()
        )

        sealed class Status {
            object Idle : Status()
            object Restart : Status()
            object ExitScreen : Status()
            sealed class EnterCredentials : Status() {
                object Idle : EnterCredentials()
                sealed class CredError : EnterCredentials() {
                    data class AuthError(val value: EnvChange) : CredError()
                    object NetworkError : CredError()
                    object NoAppEnvSelected : CredError()
                }

                object Loading : EnterCredentials()
            }
        }
    }
}
