package com.etrade.mobilepro.balance

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.balance.BalancesViewModel.ViewState
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.Resource
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class BalancesViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private fun mockDynamicScreenService(views: List<GenericLayout> = listOf()): DynamicScreenRepo {
        val viewMocks: Observable<Resource<List<GenericLayout>>> = Observable.just(Resource.Success(views))
        return mock {
            on { getScreen(any(), any(), anyOrNull()) } doReturn viewMocks
        }
    }

    private fun mockDynamicScreenServiceForError(): DynamicScreenRepo = mock {
        on { getScreen(any(), any(), anyOrNull()) } doReturn Observable.error(RuntimeException(ViewState.Error().message))
    }

    @Test
    fun `returns view state with generic views on success`() {
        val mockObserver = mock<Observer<ViewState>>()
        val mockDynamicService = mockDynamicScreenService()
        val vm = BalancesViewModel(mockDynamicService, User())
        vm.viewState.observeForever(mockObserver)
        vm.fetchBalances("1234567")
        verify(mockObserver).onChanged(eq(ViewState.Success(emptyList())))
    }

    @Test
    fun `returns error view state when fetchBalances call is not successful`() {
        val mockObserver = mock<Observer<ViewState>>()
        val mockDynamicService = mockDynamicScreenServiceForError()
        val vm = BalancesViewModel(mockDynamicService, User())
        vm.viewState.observeForever(mockObserver)
        vm.fetchBalances("1234567")
        verify(mockObserver).onChanged(eq(ViewState.Error()))
    }
}
