package com.etrade.mobilepro.dagger

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.PayeeFormFragment
import com.etrade.mobilepro.billpay.presentation.paymentpreview.PaymentPreviewFragment
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface BillPayFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(PayeeFormFragment::class)
    fun bindPayeeFormFragment(fragment: PayeeFormFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PaymentPreviewFragment::class)
    fun bindPaymentPreviewFragment(fragment: PaymentPreviewFragment): Fragment
}
