package com.etrade.mobilepro.screener.presentation.common

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import com.etrade.mobilepro.common.extension.screenWidthPixels
import com.etrade.mobilepro.tableviewutils.R
import com.etrade.mobilepro.tableviewutils.presentation.RestrictedTableView
import com.etrade.mobilepro.tableviewutils.presentation.TableViewListener
import com.etrade.mobilepro.tableviewutils.presentation.adapter.TableViewAdapter
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.util.android.extension.getActivity

/**
 * TableView with restricted height.
 */
class ScreenerResultTableView : RestrictedTableView {
    private var adapterProvider: (() -> TableViewAdapter)? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun applyData(columnHeaderList: List<Cell>, rowHeaders: List<SymbolCell>, cellItems: List<List<DataCell>>) {
        val settingAdapterFirstTime = adapter == null
        if (settingAdapterFirstTime) {
            adapter = adapterProvider?.invoke()
            /**
             * https://medium.com/@dmstocking/how-one-linearlayoutmanager-bug-makes-diffutil-worthless-372a5b887dec.
             * We're disabling animations to prevent automatic scrolling when sorting items
             */
            columnSortHandler.isEnableAnimation = false
        }

        val tableAdapter: TableViewAdapter = adapter as TableViewAdapter
        val oldColumns = adapter.columnHeaderRecyclerViewAdapter.items
        val oldRowHeaders = adapter.rowHeaderRecyclerViewAdapter.items

        when {
            settingAdapterFirstTime -> {
                adapter.setAllItems(columnHeaderList, rowHeaders, cellItems)
            }
            columnHeaderList == oldColumns &&
                rowHeaders == oldRowHeaders -> {
                tableAdapter.updateAllItems(columnHeaderList, rowHeaders, cellItems)
            }
            else -> {
                adapter.setAllItems(columnHeaderList, rowHeaders, cellItems)
                rowHeaderLayoutManager.scrollToPosition(0)
                cellLayoutManager.scrollToPosition(0)
            }
        }
    }

    fun initTableViewParams(
        rowHeaderWidth: Int = R.dimen.default_tableview_row_header_width,
        tableViewListener: TableViewListener? = null,
        adapterProvider: () -> TableViewAdapter
    ) {
        val activity = context.getActivity() ?: return
        val outValue = TypedValue().apply {
            resources.getValue(rowHeaderWidth, this, true)
        }

        isIgnoreSelectionColors = true
        isSortable = true
        this.tableViewListener = tableViewListener
        this.setHasFixedWidth(true)
        this.rowHeaderWidth = (activity.screenWidthPixels * outValue.float).toInt()
        this.adapterProvider = adapterProvider
    }
}
