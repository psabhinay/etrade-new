package com.etrade.mobilepro.appwidget.settings

import androidx.navigation.NavController
import com.etrade.mobilepro.appwidget.api.settings.WidgetId

interface WidgetSettingsRouter {
    fun openWatchlistSelection(navController: NavController, widgetId: WidgetId)
}
