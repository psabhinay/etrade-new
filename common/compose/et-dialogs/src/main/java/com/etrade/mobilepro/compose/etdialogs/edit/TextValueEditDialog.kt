package com.etrade.mobilepro.compose.etdialogs.edit

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.FontSizes
import com.etrade.mobilepro.compose.etdialogs.R
import com.etrade.mobilepro.compose.etdialogs.elements.DialogButtonRow
import com.etrade.mobilepro.compose.etdialogs.elements.DialogContainer

@OptIn(ExperimentalComposeUiApi::class)
@Composable
@Suppress("LongMethod") // compose
fun TextValueEditDialog(
    state: TextValueEditor.ViewState,
    dialogProperties: DialogProperties = remember { DialogProperties(dismissOnClickOutside = false, usePlatformDefaultWidth = false) }
) {
    when (state.status) {
        TextValueEditor.ViewState.Status.Gone -> { /* no op */ }
        TextValueEditor.ViewState.Status.Input -> {
            val kbController = LocalSoftwareKeyboardController.current
            val kbOptions = remember {
                KeyboardOptions.Default.copy(
                    imeAction = ImeAction.Done,
                    capitalization = KeyboardCapitalization.Sentences,
                    keyboardType = KeyboardType.Text
                )
            }
            val kbActions = remember {
                KeyboardActions(
                    onDone = {
                        state.settings.onDone()
                        kbController?.hide()
                    }
                )
            }
            DialogContainer(properties = dialogProperties, onDismiss = state.settings.onDismiss) {
                Column {
                    Text(
                        text = state.settings.title,
                        style = MaterialTheme.typography.h6.copy(
                            fontSize = FontSizes.Size20sp,
                            fontWeight = FontWeight.W500,
                            color = colorResource(R.color.black)
                        ),
                        modifier = Modifier.padding(Dimens.Size16dp)
                    )
                    TextField(
                        value = state.value,
                        onValueChange = state.settings.onUpdateValue,
                        maxLines = 1,
                        singleLine = true,
                        keyboardOptions = kbOptions,
                        keyboardActions = kbActions,
                        label = state.settings.hint.takeIf { it.isNotBlank() }?.let { { Text(it) } },
                        textStyle = MaterialTheme.typography.body1.copy(fontSize = FontSizes.Size16sp),
                        modifier = Modifier.fillMaxWidth(),
                        shape = RoundedCornerShape(0.dp),
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = colorResource(R.color.colorBackground),
                            textColor = colorResource(R.color.purple),
                            focusedIndicatorColor = Color.Transparent,
                            unfocusedIndicatorColor = Color.Transparent,
                            disabledIndicatorColor = Color.Transparent,
                            errorIndicatorColor = Color.Transparent,
                            cursorColor = colorResource(R.color.black),
                            focusedLabelColor = colorResource(R.color.purple)
                        )
                    )
                    Divider(
                        modifier = Modifier.fillMaxWidth().padding(horizontal = Dimens.Size16dp),
                        color = colorResource(R.color.light_grey)
                    )
                    state.errorMsg?.let {
                        Text(it, color = colorResource(R.color.red), fontSize = FontSizes.Size14sp, modifier = Modifier.padding(horizontal = Dimens.Size16dp))
                    }
                    Spacer(modifier = Modifier.height(Dimens.Size8dp))
                    DialogButtonRow(state, state.settings.buttonRow)
                }
            }
        }
    }
}
