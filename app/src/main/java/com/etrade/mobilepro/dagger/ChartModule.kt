package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.chart.data.GetChartLineDataUseCase
import com.etrade.mobilepro.chart.linechart.GetSymbolChartLineDataUseCase
import com.etrade.mobilepro.chart.netassets.GetNetAssetsChartDataEntriesUseCase
import com.etrade.mobilepro.chart.netassets.GetNetAssetsChartDataEntriesUseCaseImpl
import com.etrade.mobilepro.chart.netassets.data.DefaultNetAssetsChartRepo
import com.etrade.mobilepro.chart.netassets.data.NetAssetsChartRepo
import com.etrade.mobilepro.chart.performance.GetPerformanceChartLineDataUseCase
import com.etrade.mobilepro.chart.performance.data.DefaultPerformanceChartRepo
import com.etrade.mobilepro.chart.performance.data.PerformanceChartRepo
import dagger.Binds
import dagger.Module
import javax.inject.Named
import javax.inject.Singleton

@Module
abstract class ChartModule {

    /* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */

    @Binds
    @Singleton
    abstract fun bindGetNetAssetsChartDataEntriesUseCase(useCase: GetNetAssetsChartDataEntriesUseCaseImpl): GetNetAssetsChartDataEntriesUseCase

    @Binds
    @Singleton
    @Named("Performance")
    abstract fun bindGetPerformanceChartLineDataUseCase(useCase: GetPerformanceChartLineDataUseCase): GetChartLineDataUseCase

    @Binds
    @Singleton
    @Named("Symbol")
    abstract fun bindGetSymbolChartLineDataUseCase(useCase: GetSymbolChartLineDataUseCase): GetChartLineDataUseCase

    @Binds
    @Singleton
    abstract fun bindNetAssetsChartRepo(netAssetsChartRepo: DefaultNetAssetsChartRepo): NetAssetsChartRepo

    @Binds
    @Singleton
    abstract fun bindPerformanceChartRepo(performanceChartRepo: DefaultPerformanceChartRepo): PerformanceChartRepo
}
