package com.etrade.mobilepro.watchlistapi

// This constant let us determine if a watchlist we get from server is deletable or not by comparing its name against this string. This is a horrible hack but
// unfortunately there is no other way to do this now. See https://jira.corp.etradegrp.com/browse/ETAND-2965 for future references.
const val INVESTMENTS_TO_WATCH_NAME = "Investments to Watch"
