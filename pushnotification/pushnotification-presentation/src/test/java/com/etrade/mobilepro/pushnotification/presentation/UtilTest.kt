package com.etrade.mobilepro.pushnotification.presentation

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.pushnotification.api.SmartAlert
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class UtilTest {

    @Test
    fun `maps representing invalid alerts can not be converted into a smart alerts`() {
        listOf(
            mapOf("" to "", "" to ""),
            mapOf("payload" to ""),
            mapOf("payload" to "", "type" to ""),
            mapOf("payload" to "Message"),
            mapOf("payload" to "Message", "type" to ""),
            mapOf("type" to "Account"),
            mapOf("payload" to "", "type" to "Account"),
            mapOf("type" to "Stock"),
            mapOf("payload" to "", "type" to "Stock")
        ).forEach {
            assertNull(it.toSmartAlert())
        }
    }

    @Test
    fun `map representing account alert can be converted into a smart alert`() {
        val map = mapOf("payload" to "Any message", "type" to "Account")
        val alert = map.toSmartAlert()

        assert(alert is SmartAlert.Account)
        assertEquals(alert!!.message, "Any message")
    }

    @Test
    fun `map representing an stock alert can be converted into a smart alert`() {
        val map = mapOf("payload" to "AAPL up by at least 0.0200%", "type" to "Stock")
        val alert = map.toSmartAlert()

        assert(alert is SmartAlert.Stock)
        (alert as? SmartAlert.Stock)?.apply {
            assertEquals(message, "AAPL up by at least 0.0200%")
            assertEquals(symbolTitle, "AAPL")
        }
    }

    @Test
    fun foo() {
        val alert = SmartAlert.Stock("AAPL up by at least 0.0200%", "AAPL")
        val symbol = alert.toSymbol()

        assertEquals(symbol.title, "AAPL")
        assertEquals(symbol.type, InstrumentType.EQ)
    }
}
