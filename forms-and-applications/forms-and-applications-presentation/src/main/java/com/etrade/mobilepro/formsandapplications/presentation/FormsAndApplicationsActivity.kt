package com.etrade.mobilepro.formsandapplications.presentation

import android.os.Bundle
import android.view.MenuItem
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.baseactivity.ActivityNetworkConnectionDelegate
import com.etrade.mobilepro.baseactivity.ActivitySessionDelegate
import com.etrade.mobilepro.common.LoginChecker
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.setupToolbarWithUpButton
import com.etrade.mobilepro.formsandapplications.api.FormsAndApplicationsReference
import com.etrade.mobilepro.formsandapplications.presentation.databinding.ActivityFormsAndApplicationsBinding
import com.etrade.mobilepro.menuitem.MenuItemViewWithArrow
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.addDividerItemDecoration
import com.etrade.mobilepro.util.android.extension.inflate
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

/**
 * See designs [here](https://etrade.invisionapp.com/d/main#/console/15411917/320309458/preview).
 */
@RequireLogin
class FormsAndApplicationsActivity : AppCompatActivity(), HasAndroidInjector {

    private val binding by viewBinding(ActivityFormsAndApplicationsBinding::inflate)

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var activitySessionDelegate: ActivitySessionDelegate

    @Inject
    lateinit var activityNetworkConnectionDelegate: ActivityNetworkConnectionDelegate

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var loginChecker: LoginChecker

    private val viewModel: FormsAndApplicationsViewModel by viewModels { viewModelFactory }

    private val itemAdapter: Adapter by lazy {
        Adapter { item ->
            startActivity(AccountServicesWebViewActivity.intent(this, item.url, item.shortTitle))

            item.analyticsTitle?.let { viewModel.trackPage(it) }
        }
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(binding.root)

        setupToolbarWithUpButton(R.string.forms_and_applications_title)

        binding.list.apply {
            adapter = itemAdapter
            addDividerItemDecoration(R.drawable.thin_divider)
        }

        if (savedInstanceState == null) {
            viewModel.refreshItems()
        }

        viewModel.listItems.observe(
            this,
            Observer {
                itemAdapter.items = it
            }
        )
        activityNetworkConnectionDelegate.initNetworkConnectivityBanner(this)

        activitySessionDelegate.observeTimeoutEvent(this)
    }

    override fun onStart() {
        super.onStart()
        loginChecker.check(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        activitySessionDelegate.onUserInteraction(this)
    }

    private class Adapter(private val onItemClickListener: (FormsAndApplicationsReference) -> Unit) : RecyclerView.Adapter<ViewHolder>() {

        /**
         * Property to set/get adapter's items.
         */
        var items: List<FormsAndApplicationsReference> = emptyList()
            set(value) {
                if (field == value) return
                field = value
                notifyDataSetChanged()
            }

        override fun getItemCount(): Int = items.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(onItemClickListener, parent)

        override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bindTo(items[position])
    }

    private class ViewHolder(
        private val onItemClickListener: (FormsAndApplicationsReference) -> Unit,
        parent: ViewGroup
    ) : RecyclerView.ViewHolder(parent.inflate(R.layout.item_menu_item_view_with_arrow)) {

        /**
         * Binds an [item] to this view holder.
         *
         * @param item an item to bind
         */
        fun bindTo(item: FormsAndApplicationsReference) {
            (itemView as MenuItemViewWithArrow).apply {
                menuTitleText = context.getString(item.title)
                setOnClickListener {
                    onItemClickListener(item)
                }
            }
        }
    }
}
