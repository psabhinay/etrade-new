package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot

internal interface SaveOrderRequestParametersProvider {
    val path: String
    fun createBody(params: InputValuesSnapshot): InputValuesSnapshot
}
