package com.etrade.mobilepro.barchartview

interface DataEntry {

    val x: Float
    val y: Float
    val barColor: Int
    val valueColor: Int
    val valueDescriptionColor: Int?
}
