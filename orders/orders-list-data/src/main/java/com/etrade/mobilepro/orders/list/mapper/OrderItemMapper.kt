package com.etrade.mobilepro.orders.list.mapper

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.checkAndRectifyIndexUnderlyingSymbol
import com.etrade.mobilepro.instrument.getDisplaySymbolFromOsiKey
import com.etrade.mobilepro.orders.api.AdvancedOrderSecurityType
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.ExecInstruction
import com.etrade.mobilepro.orders.api.MarketSession
import com.etrade.mobilepro.orders.api.OrderAction
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.orders.api.extractOrderStatus
import com.etrade.mobilepro.orders.data.dto.HeaderDto
import com.etrade.mobilepro.orders.data.dto.LegDetailDto
import com.etrade.mobilepro.orders.data.dto.OrderDto
import com.etrade.mobilepro.orders.data.dto.OrderItemDto
import com.etrade.mobilepro.orders.data.dto.StockPlanOrdersResponseDto
import com.etrade.mobilepro.orders.data.dto.parseOrderStatusForStockPlan
import com.etrade.mobilepro.orders.list.api.DefaultOrderItem
import com.etrade.mobilepro.orders.list.api.OrderDetailsDescription
import com.etrade.mobilepro.orders.list.api.StockPlanOrderItem
import com.etrade.mobilepro.util.safeParseBigDecimal
import org.threeten.bp.Instant
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

class OrderItemMapper {
    fun map(orderItemDto: OrderItemDto): DefaultOrderItem {

        /**
         * if order status is missing from the entire order item, we extract it from header of the first order
         */
        val wholeHeader = orderItemDto.wholeOrderHeader
        val firstValidOrder = getFirstValidOrder(orderItemDto)
        val orderType = OrderType.fromTypeCode(wholeHeader.orderType)
        val orderStatus = wholeHeader.orderStatus.takeIf { it.isNotBlank() }
            ?.let { OrderStatus.fromStatusCode(it) } ?: getOrderStatus(orderItemDto.order)
        val hasGroupOrders = orderItemDto.groupOrderCount.toIntOrNull() ?: 0 > 1
        val date: Instant? = getCreatedAt(orderStatus, firstValidOrder, hasGroupOrders, orderItemDto)
        val advancedOrderType = AdvancedOrderType.fromTypeCode(firstValidOrder.advancedOrder.aoType)
        val orderDetailDescriptionList = getBriefOrderDescriptions(
            orderItemDto.order.flatMap {
                it.legDetails
            }
        )
        val marketSession = MarketSession.getMarketSession(wholeHeader.marketSession.toIntOrNull())
        val execInstruction = wholeHeader.execInstruction.run { ExecInstruction(aonFlag == 1, nhFlag == 1, dnrFlag == 1, dniFlag == 1) }
        val isGoodTillDate = wholeHeader.gtdFlag == 1
        val displaySymbol = firstValidOrder.legDetails[0].displaySymbol
        val orderSymbolSet = getOrderSymbolSet(orderItemDto.order)
        val advancedOrderSymbolSet = getAdvancedOrderSymbolSet(orderItemDto.order, advancedOrderType)
        val advancedOrderSymbol = orderItemDto.order.firstOrNull()?.advancedOrder?.aoCondSymbol
        val advancedOrderDisplaySymbol = advancedOrderSymbol?.let {
            getDisplaySymbolFromOsiKey(it)
        } ?: advancedOrderSymbolSet.firstOrNull()

        return DefaultOrderItem(
            orderNumber = orderItemDto.orderNumber,
            displaySymbol = displaySymbol,
            symbols = orderSymbolSet.union(advancedOrderSymbolSet).filter { it.isNotBlank() }.toList(),
            date = date,
            orderStatus = orderStatus,
            orderDescriptions = orderDetailDescriptionList,
            advancedOrderType = AdvancedOrderType.fromTypeCode(firstValidOrder.advancedOrder.aoType),
            orderType = orderType,
            origSysCode = OrigSysCode.fromOrderListResponse(wholeHeader.origSysCode),
            hasDependentFlag = wholeHeader.hasDependentFlag.toIntOrNull() == 1,
            dependentOrderNumber = wholeHeader.dependentOrderNo.toIntOrNull() ?: Int.MIN_VALUE,
            orderTerm = OrderTerm.getOrderTerm(
                wholeHeader.orderTerm,
                marketSession,
                isGoodTillDate,
                execInstruction,
                wholeHeader.orderSpecialAttribCd
            ),
            goodThroughDateFlag = isGoodTillDate,
            stopPrice = wholeHeader.stopPrice.safeParseBigDecimal() ?: BigDecimal.ZERO,
            hiddenFlag = wholeHeader.hiddenFlag.toIntOrNull() == 1,
            qtyReserveShow = firstValidOrder.legDetails[0].qtyReserveShow,
            instrumentType = InstrumentType.from(firstValidOrder.legDetails[0].typeCode),
            priceType = getPriceType(wholeHeader, firstValidOrder.legDetails[0].qtyReserveShow),
            execInstruction = execInstruction,
            orderSpecialAttribCd = wholeHeader.orderSpecialAttribCd,
            marketSession = marketSession,
            advancedOrderSymbol = advancedOrderSymbol,
            advancedOrderDisplaySymbol = advancedOrderDisplaySymbol

        )
    }

    // Sometimes orderItemDto can have 2 order items where first item doesn't have a valid leg with data, but second does have
    private fun getFirstValidOrder(orderItemDto: OrderItemDto): OrderDto {
        val firstOrder = orderItemDto.order[0]
        return if (firstOrder.legDetails.isEmpty()) {
            val secondOrder = orderItemDto.order[1]
            if (secondOrder.legDetails.isEmpty()) {
                throw IllegalArgumentException("Invalid order data")
            }
            secondOrder
        } else {
            firstOrder
        }
    }

    fun mapStockPlanOrders(serverResponse: StockPlanOrdersResponseDto): List<StockPlanOrderItem> {
        val items = serverResponse.orders ?: emptyList()
        val symbol = getStockPlanSymbol(serverResponse)
        return items.map { dto ->
            StockPlanOrderItem(
                date = dto.date ?: "",
                planType = dto.planType ?: "",
                orderType = dto.orderType ?: "",
                priceType = dto.priceType ?: "",
                executionPrice = dto.executionPrice ?: "",
                sharesToExercise = dto.sharesToExercise ?: "",
                exercised = dto.exercised ?: "",
                sharesToSell = dto.sharesToSell ?: "",
                sold = dto.sold ?: "",
                orderNumber = dto.orderNumber ?: "",
                accountIndex = dto.accountIndex ?: "",
                orderStatus = parseOrderStatusForStockPlan(dto.orderStatus),
                symbols = symbol.split(",")
            )
        }
    }

    private fun getStockPlanSymbol(response: StockPlanOrdersResponseDto): String {
        return response.accountList?.firstOrNull {
            it.isDefault?.toBoolean() == true
        }?.symbol ?: ""
    }

    private fun getOrderStatus(orders: List<OrderDto>): OrderStatus {
        val allStatusList = orders.map {
            OrderStatus.fromStatusCode(it.header.orderStatus)
        }
        return extractOrderStatus(allStatusList)
    }

    private fun getPriceType(wholeHeader: HeaderDto, qtyReserveShow: Int): PriceType {
        val typeCode = wholeHeader.orderTrigger
        val isReserveLimit = qtyReserveShow > 0
        val isLimitOffsetPositive = wholeHeader.limitOffsetValue.toDoubleOrNull() ?: Double.MIN_VALUE > 0
        val isHiddenLimit = wholeHeader.hiddenFlag.run {
            when {
                isBlank() -> false
                else -> wholeHeader.hiddenFlag.toIntOrNull() ?: Int.MIN_VALUE > 0
            }
        }
        return PriceType.getPriceType(typeCode, isHiddenLimit, isReserveLimit, isLimitOffsetPositive)
    }

    private fun getAdvancedOrderSymbolSet(orderDtoList: List<OrderDto>, advancedOrderType: AdvancedOrderType = AdvancedOrderType.UNKNOWN): Set<String> {
        val symbolsSet = mutableSetOf<String>()
        if (AdvancedOrderType.UNKNOWN == advancedOrderType) {
            return symbolsSet
        }
        orderDtoList.forEach {
            when (AdvancedOrderSecurityType.getSecurityType(it.advancedOrder.aoCondSecurityType)) {
                AdvancedOrderSecurityType.OPTN -> {
                    it.advancedOrder.aoCondSymbol.split("-").getOrNull(0)?.let { symbol ->
                        symbolsSet.add(symbol)
                    }
                    symbolsSet.add(it.advancedOrder.aoCondSymbol)
                }
                else -> symbolsSet.add(it.advancedOrder.aoCondSymbol)
            }
        }

        return symbolsSet
    }

    private fun getOrderSymbolSet(orderDtoList: List<OrderDto>): Set<String> {
        val symbolsSet = mutableSetOf<String>()
        orderDtoList.forEach {
            it.legDetails.forEach { legDetail ->
                if (InstrumentType.from(legDetail.typeCode).isOption) {
                    val underlyingSymbol = legDetail.displaySymbol.split(" ").getOrElse(0) {
                        legDetail.symbol
                    }
                    symbolsSet.add(checkAndRectifyIndexUnderlyingSymbol(underlyingSymbol))
                    symbolsSet.add(legDetail.symbol)
                } else {
                    symbolsSet.add(legDetail.symbol)
                }
            }
        }
        return symbolsSet
    }

    private fun getCreatedAt(
        orderStatus: OrderStatus,
        firstOrder: OrderDto,
        hasGroupOrders: Boolean,
        orderItemDto: OrderItemDto
    ): Instant? {
        return when {
            orderStatus == OrderStatus.SAVED -> firstOrder.auditInfo.createUtc.toLongOrNull()?.let { TimeUnit.SECONDS.toMillis(it) }
            hasGroupOrders -> firstOrder.header.timePlaced.toLongOrNull()
            orderItemDto.groupOrderCount.toIntOrNull() ?: 0 > 0 -> firstOrder.header.timePlaced.toLongOrNull()
            else -> orderItemDto.wholeOrderHeader.timePlaced.toLongOrNull()
        }?.let { Instant.ofEpochMilli(it) }
    }

    private fun getBriefOrderDescriptions(legDetails: List<LegDetailDto>): List<OrderDetailsDescription> {
        return legDetails.map { legDetail: LegDetailDto ->
            val quantityValue = if (legDetail.filledQuantity != legDetail.quantityValue && legDetail.filledQuantity != "0") {
                String.format("%s/%s", legDetail.filledQuantity, legDetail.quantityValue)
            } else {
                legDetail.quantityValue
            }
            val orderAction: OrderAction = OrderAction.fromString(legDetail.orderAction)
            val transactionType = TransactionType.from(orderAction, legDetail.positionEffect, InstrumentType.valueOf(legDetail.typeCode).isOption)
            OrderDetailsDescription(quantityValue, transactionType, legDetail.displaySymbol)
        }
    }
}
