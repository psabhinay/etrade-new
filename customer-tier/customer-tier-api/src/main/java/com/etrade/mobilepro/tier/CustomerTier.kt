package com.etrade.mobilepro.tier

sealed class CustomerTier(val asString: String) {

    object Elite : CustomerTier("Elite")
    object Premium : CustomerTier("Premium")
    object StockPlanElite : CustomerTier("Stock Plan Elite")
    object Gold : CustomerTier("Gold")
    object Silver : CustomerTier("Silver")
    object Bronze : CustomerTier("Bronze")
    object Tin : CustomerTier("Tin")

    class Unrecognized(name: String) : CustomerTier(name)

    companion object {

        fun fromString(input: String): CustomerTier = when {
            input.isTier(Elite) -> Elite
            input.isTier(Premium) -> Premium
            input.isTier(StockPlanElite) -> StockPlanElite
            input.isTier(Gold) -> Gold
            input.isTier(Silver) -> Silver
            input.isTier(Bronze) -> Bronze
            input.isTier(Tin) -> Tin
            else -> Unrecognized(input)
        }

        private fun String.isTier(tier: CustomerTier) = equals(tier.asString, ignoreCase = true)
    }
}
