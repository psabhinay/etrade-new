package com.etrade.mobilepro.completeview.model

import androidx.annotation.ColorRes
import com.etrade.mobilepro.R
import com.etrade.mobilepro.streaming.api.StreamingTextManager

@ColorRes
fun StreamingTextManager.MovementType.getColorRes(): Int = when (this) {
    StreamingTextManager.MovementType.GAIN -> R.color.green_light
    StreamingTextManager.MovementType.LOSS -> R.color.red_light
    StreamingTextManager.MovementType.NEUTRAL -> R.color.complete_view_toolbar_default_text_color
}
