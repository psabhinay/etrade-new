package com.etrade.mobilepro.viewdelegate

import android.app.Activity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumeIfNot
import com.etrade.mobilepro.viewmodel.ActivitySignal
import com.etrade.mobilepro.viewmodel.ActivitySignalViewModel

class ActivitySignalViewDelegate(
    private val viewModel: ActivitySignalViewModel,
    private val activity: Activity
) : ViewDelegate {

    override fun observe(owner: LifecycleOwner) {
        viewModel.activitySignal.observe(owner, Observer { onActivitySignal(it) })
    }

    private fun onActivitySignal(event: ConsumableLiveEvent<ActivitySignal>) {
        event.consumeIfNot(activity.isFinishing) { signal ->
            signal(activity)
        }
    }
}
