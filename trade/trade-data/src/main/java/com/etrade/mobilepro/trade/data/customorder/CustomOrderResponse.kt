package com.etrade.mobilepro.trade.data.customorder

import com.etrade.mobilepro.backends.neo.BaseDataDto

internal abstract class CustomOrderResponse : BaseDataDto() {
    abstract val order: CustomOrderDto?
}
