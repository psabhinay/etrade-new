package com.etrade.mobilepro.markets.data

import com.etrade.eo.rest.retrofit.Xml
import com.etrade.mobilepro.caching.okhttp.CACHE_CONTROL_DEFAULT_EXPIRATION
import com.etrade.mobilepro.markets.data.response.MarketDollarTopGainerResponse
import com.etrade.mobilepro.markets.data.response.MarketDollarTopLoserResponse
import com.etrade.mobilepro.markets.data.response.MarketFutureResponse
import com.etrade.mobilepro.markets.data.response.MarketIndexResponse
import com.etrade.mobilepro.markets.data.response.MarketMostActiveResponse
import com.etrade.mobilepro.markets.data.response.MarketMoverResponse
import com.etrade.mobilepro.markets.data.response.MarketPctTopGainerResponse
import com.etrade.mobilepro.markets.data.response.MarketPctTopLoserResponse
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface MoverService {
    @JsonMoshi
    @Headers(CACHE_CONTROL_DEFAULT_EXPIRATION)
    @GET("/e/t/mobile/GetMostActives.json")
    fun getMarketMovers(): Single<MarketMoverResponse>

    @Xml
    @Headers(CACHE_CONTROL_DEFAULT_EXPIRATION)
    @GET("/e/t/mobile/GetMarketIndices")
    fun getMarketIndices(): Single<MarketIndexResponse>

    @Xml
    @Headers(CACHE_CONTROL_DEFAULT_EXPIRATION)
    @GET("/e/t/mobile/getfuturequotes")
    fun getMarketFutures(): Single<MarketFutureResponse>

    @Xml
    @FormUrlEncoded
    @POST("/e/t/mobile/GetPctTopGainer")
    fun getPctTopGainer(@Field("MoverExchange") exchangeField: String): Single<MarketPctTopGainerResponse>

    @Xml
    @FormUrlEncoded
    @POST("/e/t/mobile/GetPctTopLoser")
    fun getPctTopLoser(@Field("MoverExchange") exchangeField: String): Single<MarketPctTopLoserResponse>

    @Xml
    @FormUrlEncoded
    @POST("/e/t/mobile/GetDollarTopGainer")
    fun getDollarTopGainer(@Field("MoverExchange") exchangeField: String): Single<MarketDollarTopGainerResponse>

    @Xml
    @FormUrlEncoded
    @POST("/e/t/mobile/GetDollarTopLoser")
    fun getDollarTopLoser(@Field("MoverExchange") exchangeField: String): Single<MarketDollarTopLoserResponse>

    @Xml
    @FormUrlEncoded
    @POST("/e/t/mobile/GetMostActives")
    fun getMostActives(@Field("MoverExchange") exchangeField: String): Single<MarketMostActiveResponse>
}
