package com.etrade.mobilepro.backends.neo

import com.etrade.mobilepro.backends.api.HasWarnings
import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.backends.api.ServerMessageException
import com.etrade.mobilepro.backends.api.ServerResponseException
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
open class ServerResponseDto<out T : BaseDataDto>(
    @Json(name = "smUser")
    val smUser: String?,

    @Json(name = "sysdate")
    val sysdate: String?,

    @Json(name = "hostName")
    val hostName: String?,

    @Json(name = "message_type")
    val messageType: String?,

    @Json(name = "message_info")
    val messageInfo: String?,

    @Json(name = "success")
    val isSuccess: Boolean?,

    @Json(name = "data")
    val data: T?
) {
    fun extractError(): ServerError = when (data) {
        null -> ServerError.NullResponse
        else -> data.extractError()
    }

    fun <R> doIfSuccessfulOrThrow(block: (T) -> R): R {
        val error = extractError()
        if (error != ServerError.NoError) {
            throw ServerResponseException(error)
        } else {
            return block(data as T)
        }
    }

    fun throwIfHasErrors() {
        val error = extractError()
        if (error != ServerError.NoError) {
            throw ServerResponseException(error)
        }
    }

    fun <R> doSuccessful(block: (T) -> R): R {
        return block(data as T)
    }
}

/**
 * Converts an instance of [ServerResponseDto] to a response object.
 *
 * @param T The type of data inside the instance of [ServerResponseDto]
 * @param R The type of response object.
 * @param convert Converts data object and warnings into an instance of [R].
 *
 * @throws ServerMessageException If there are errors in [ServerResponseDto].
 */
fun <T : BaseDataDto, R : HasWarnings> ServerResponseDto<T>.toResponse(convert: (data: T, warnings: List<ServerMessage>) -> R): R {
    return data?.let { result ->
        val messages = result.getAllMessages()
        val errors = messages.filter { it.type == ServerMessage.Type.ERROR }
        if (errors.isEmpty()) {
            convert(result, messages.filter { it.type == ServerMessage.Type.WARN })
        } else {
            throw ServerMessageException(errors)
        }
    } ?: throw ServerMessageException(ServerMessage.error(messageType.orEmpty()))
}
