package com.etrade.mobilepro.screener.api

interface ScreenerFilter {
    fun isEmpty(): Boolean
    fun isNotEmpty(): Boolean = !isEmpty()
    fun belongsTo(): ScreenerType.Group
    fun customResultFields(): List<String> = emptyList()
}
