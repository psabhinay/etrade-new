package com.etrade.mobilepro.chat.data.setup.service

import com.etrade.mobilepro.chat.data.setup.dto.chatActiveStatus.ChatActiveStatusDto
import com.etrade.mobilepro.chat.data.setup.dto.contactId.ChatContactIdDto
import com.etrade.mobilepro.chat.data.setup.dto.contactId.ChatCreateContactIdDto
import com.etrade.mobilepro.chat.data.setup.dto.getChatOperatingHours.ChatOperatingHoursDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatCustomerDetailsForMobileDto
import com.etrade.mobilepro.chat.data.setup.dto.getProducts.ChatProductsDto
import com.etrade.mobilepro.chat.data.setup.dto.request.ChatSetupRequestBodyDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

private const val CHAT_API_PATH = "/app/chat/v2/"
internal const val CHAT_PLATFORM_HEADER = "Platform: Android"

interface ChatSetupApiService {

    @JsonMoshi
    @GET(CHAT_API_PATH + "getChatActiveStatus.json")
    @Headers(CHAT_PLATFORM_HEADER)
    suspend fun getChatActiveStatus(): ChatActiveStatusDto

    @JsonMoshi
    @GET(CHAT_API_PATH + "createChatContactId.json")
    @Headers(CHAT_PLATFORM_HEADER)
    suspend fun createChatContactId(): ChatCreateContactIdDto

    @JsonMoshi
    @GET(CHAT_API_PATH + "getChatOperatingHours.json")
    @Headers(CHAT_PLATFORM_HEADER)
    suspend fun getChatOperatingHours(): ChatOperatingHoursDto

    @JsonMoshi
    @GET(CHAT_API_PATH + "getChatProducts.json")
    @Headers(CHAT_PLATFORM_HEADER)
    suspend fun getChatProducts(): ChatProductsDto

    @JsonMoshi
    @GET(CHAT_API_PATH + "getChatContactId.json")
    @Headers(CHAT_PLATFORM_HEADER)
    suspend fun getChatContactId(): ChatContactIdDto

    @JsonMoshi
    @POST("/app/chatxml/getCustomerDetailsForMobile.json")
    @Headers(CHAT_PLATFORM_HEADER)
    suspend fun getCustomerDetailsForMobile(@Body @JsonMoshi request: ChatSetupRequestBodyDto): ChatCustomerDetailsForMobileDto
}
