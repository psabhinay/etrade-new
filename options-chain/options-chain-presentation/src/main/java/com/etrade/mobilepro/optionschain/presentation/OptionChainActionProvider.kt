package com.etrade.mobilepro.optionschain.presentation

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.optionschain.tableview.R
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import javax.inject.Inject

interface OptionChainActionProvider {

    /**
     * Provides list of action items for option chain
     */
    fun getActions(
        type: InstrumentType,
        underlyingTypeCode: InstrumentType = InstrumentType.UNKNOWN,
        isFromQuotes: Boolean = false
    ): List<ActionItem>

    fun getOptionChainType(id: Int): OptionChainType
}

enum class OptionChainType {
    QUOTE_DETAILS,
    TRADE_OPTIONS,
    TRADE_STOCKS_AND_OPTIONS,
    TRADE_CONDITIONAL
}

fun OptionChainType.toSecurityType(): SecurityType {
    return when (this) {
        OptionChainType.TRADE_OPTIONS -> SecurityType.OPTION
        OptionChainType.TRADE_STOCKS_AND_OPTIONS -> SecurityType.STOCK_AND_OPTIONS
        OptionChainType.TRADE_CONDITIONAL -> SecurityType.CONDITIONAL
        else -> throw IllegalStateException("Option Chain Type is not valid")
    }
}

class OptionChainActionProviderImpl @Inject constructor() : OptionChainActionProvider {

    override fun getActions(type: InstrumentType, underlyingTypeCode: InstrumentType, isFromQuotes: Boolean): List<ActionItem> {
        if (type.isIndex || underlyingTypeCode.isIndex) {
            return getActionsForIndex(isFromQuotes)
        }
        return getDefaultActions(isFromQuotes)
    }

    override fun getOptionChainType(id: Int) =
        actionOptionChainTypeMap[id] ?: throw IllegalArgumentException("Invalid action item")

    private fun getDefaultActions(isFromQuotes: Boolean) =
        listOfNotNull(
            getQuoteDetailsOrNull(isFromQuotes),
            createTradeOptionsAction(isFromQuotes),
            createTradeStocksAndOptionsAction(isFromQuotes),
            createTradeConditionalAction(isFromQuotes)
        )

    private fun getActionsForIndex(isFromQuotes: Boolean): List<ActionItem> =
        listOfNotNull(
            getQuoteDetailsOrNull(isFromQuotes),
            createTradeOptionsAction(isFromQuotes)
        )

    private fun getQuoteDetailsOrNull(isFromQuotes: Boolean) =
        if (isFromQuotes) {
            null
        } else {
            createQuoteDetailsAction()
        }

    private fun createQuoteDetailsAction(): ActionItem =
        ActionItem(R.id.options_chain_action_quote_details, R.string.options_chain_quote_details, R.drawable.ic_tableview_quote)

    private fun createTradeOptionsAction(isFromQuotes: Boolean): ActionItem =
        ActionItem(
            id = R.id.options_chain_action_trade_options,
            displayNameId = if (isFromQuotes) {
                R.string.options_chain_trade_options_from_quotes
            } else {
                R.string.options_chain_trade_options
            },
            iconId = R.drawable.ic_trade_options
        )

    private fun createTradeStocksAndOptionsAction(isFromQuotes: Boolean): ActionItem =
        ActionItem(
            id = R.id.options_chain_action_trade_stocks_and_options,
            displayNameId = if (isFromQuotes) {
                R.string.options_chain_trade_stock_and_options_from_quotes
            } else {
                R.string.options_chain_trade_stock_and_options
            },
            iconId = R.drawable.ic_trade_stocks_and_options
        )

    private fun createTradeConditionalAction(isFromQuotes: Boolean): ActionItem =
        ActionItem(
            id = R.id.options_chain_action_trade_conditional,
            displayNameId = if (isFromQuotes) {
                R.string.options_chain_trade_conditional_from_quotes
            } else {
                R.string.options_chain_trade_conditional
            },
            iconId = R.drawable.ic_trade_conditional
        )

    private val actionOptionChainTypeMap = mapOf(
        R.id.options_chain_action_quote_details to OptionChainType.QUOTE_DETAILS,
        R.id.options_chain_action_trade_options to OptionChainType.TRADE_OPTIONS,
        R.id.options_chain_action_trade_stocks_and_options to OptionChainType.TRADE_STOCKS_AND_OPTIONS,
        R.id.options_chain_action_trade_conditional to OptionChainType.TRADE_CONDITIONAL
    )
}
