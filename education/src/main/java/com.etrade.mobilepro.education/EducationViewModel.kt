package com.etrade.mobilepro.education

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.tracking.SCREEN_TITLE_EDUCATION_VIDEOS
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen
import javax.inject.Inject

private const val INDEX_ALL_TOPIC = 0

class EducationViewModel @Inject constructor(
    val resources: Resources,
    val tracker: Tracker
) : ViewModel(), ScreenViewModel {

    // add "All" topic that not present in existing app
    private val _topics: MutableList<String> = mutableListOf(resources.getString(R.string.education_topic_all))
    private val _articles: MutableList<EducationItem> = mutableListOf()

    val topics: LiveData<List<String>>

    val selectedTopicIndex: MutableLiveData<Int> = MutableLiveData()

    val selectedTopic: LiveData<String> = Transformations.map(selectedTopicIndex) { _topics[it] }

    val articles: LiveData<List<EducationItem>> = Transformations.map(selectedTopicIndex) {
        if (it == INDEX_ALL_TOPIC) {
            _articles
        } else {
            val topic: String = _topics[it]

            tracker.screen(topic)

            _articles.filter { item -> item.topic == topic }
        }.sorted()
    }

    override val screenName: String? = SCREEN_TITLE_EDUCATION_VIDEOS

    init {
        val lineRegex = Regex(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)")

        resources
            .openRawResource(R.raw.education_videos)
            .bufferedReader()
            .useLines {
                it.forEach { line ->
                    val item: EducationItem = EducationItem().map(line, lineRegex)

                    _topics.takeUnless { list -> list.contains(item.topic) }?.add(item.topic)

                    _articles.add(item)
                }
            }

        topics = MutableLiveData<List<String>>().apply { value = _topics }
        selectedTopicIndex.value = 0
    }
}
