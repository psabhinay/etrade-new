package com.etrade.mobilepro.documents.data.decoding

import android.util.Base64
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.util.zip.GZIPInputStream
import javax.inject.Inject

class DefaultDocumentDecoder @Inject constructor(
    private val cacheDir: String
) : DocumentDecoder {
    override suspend fun decodeToFile(fileName: String, base64gZippedData: String): ETResult<File> = runCatchingET {
        withContext(Dispatchers.IO) {
            val gzipped = Base64.decode(base64gZippedData, Base64.DEFAULT)
            val unzippedStream = GZIPInputStream(gzipped.inputStream())
            ensureSubDirExists()
            val file = createFile(fileName)
            file.outputStream().use { unzippedStream.copyTo(it) }
            file
        }
    }

    private fun createFile(name: String): File = File("$cacheDir/$name.pdf").apply { createNewFile() }
    private fun ensureSubDirExists() = File(cacheDir).mkdir()
}
