package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee

import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.telephony.PhoneNumberUtils.formatNumber
import android.view.Menu
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.billpay.api.SearchPayeeResultItem
import com.etrade.mobilepro.billpay.api.StatesOfUS
import com.etrade.mobilepro.billpay.api.UsState
import com.etrade.mobilepro.billpay.presentation.BillPayViewModel
import com.etrade.mobilepro.billpay.presentation.MANAGE_PAYEE_TAB_INDEX
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.billpay.presentation.common.NavigationViewModel
import com.etrade.mobilepro.billpay.presentation.common.toParcelable
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayFragmentAddPayeeBinding
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input.NoOperationFormatter
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input.UsPhoneLengthValidator
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input.UsPhoneValidator
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input.ZipValidator
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.search.SearchPayeeAdapter
import com.etrade.mobilepro.billpay.presentation.viewmodel.PayeeFormViewModelFactoryFactory
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.result.getOrDefault
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.inputvalueview.validators.AlwaysValidValidator
import com.etrade.mobilepro.inputvalueview.validators.ComposedValidator
import com.etrade.mobilepro.inputvalueview.validators.LengthInRangeValidator
import com.etrade.mobilepro.inputvalueview.validators.RequiredFieldValidator
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.accessibility.bindTextEditAccessibility
import com.etrade.mobilepro.util.android.accessibility.bindTextEditAccessibilityListeners
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.clearFocusSafely
import com.etrade.mobilepro.util.android.extension.hideSoftKeyboard
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import java.util.Locale
import javax.inject.Inject

private val TAG_STATE = "${PayeeFormFragment::class.java.canonicalName}.drop_down_address_state"
private val TAG_DIALOG = "${PayeeFormFragment::class.java.canonicalName}.delete_payee_dialog"

private const val TEXT_INPUT_MAX_LENGTH = 32
private const val ADDRESS_INPUT_MAX_LENGTH = 40
private const val MAX_ZIP_CODE_LENGTH = 5
internal const val DELETE_PAYEE_DIALOG_CODE = 305
private const val DISABLED_ALPHA: Float = 0.5f
private const val ENABLED_ALPHA: Float = 1f

@RequireLogin
class PayeeFormFragment @Inject constructor(
    private val assistedFactory: PayeeFormViewModelFactoryFactory,
    private val viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : OverlayBaseFragment() {

    private val binding by viewBinding(BillPayFragmentAddPayeeBinding::bind)

    override val layoutRes: Int = R.layout.bill_pay_fragment_add_payee

    private val args: PayeeFormFragmentArgs by navArgs()
    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.bill_pay_nav_graph)
    private val sharedViewModel: BillPayViewModel by navGraphViewModels(R.id.bill_pay_nav_graph) {
        viewModelFactory
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private val viewModel: PayeeFormViewModel by viewModels {
        assistedFactory.create(
            args.billPaySessionId,
            args.payeeDetails,
            args.isEditMode
        )
    }

    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private val parentSnackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { activity },
            { view }
        )
    }

    private lateinit var stateSelector: DropDownMultiSelector<UsState>

    private val inputs by lazy {
        listOf(
            binding.payeeForm.name,
            binding.payeeForm.nickname,
            binding.payeeForm.accountNumber,
            binding.payeeForm.address1,
            binding.payeeForm.address2,
            binding.payeeForm.city,
            binding.payeeForm.state,
            binding.payeeForm.zipCode
        )
    }

    private val defaultEditorActionListener = TextView.OnEditorActionListener { v, actionId, _ ->
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            v.hideSoftKeyboard()
            binding.dummy.requestFocus()
            true
        } else {
            false
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private val searchAdapter: SearchPayeeAdapter =
        SearchPayeeAdapter {
            viewModel.selectSearchResult(it)
        }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindPayeeFormAccessibilityListeners()
        setupDropDowns()

        setupViewsWithArgs()
        setupChangeListeners()
        setupViews()
        subscribeToDataChanges()
        setupClickListeners()
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                DELETE_PAYEE_DIALOG_CODE -> {
                    viewModel.deletePayee()
                }
            }
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        if (args.payeeDetails != null) {
            activity?.apply {
                toolbarActionEndView?.apply {
                    setToolbarActionTextAndContentDescription(R.string.delete)
                    setOnClickListener { showDialog() }
                    visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        activity?.run {
            currentFocus?.also { view ->
                view.hideSoftKeyboard()
            }
        }
    }

    private fun showDialog() {
        CustomDialogFragment.newInstance(
            message = getString(R.string.bill_pay_payee_delete_dialog_message),
            okTitle = getString(R.string.yes),
            cancelTitle = getString(R.string.no),
            cancelable = false,
            requestCode = DELETE_PAYEE_DIALOG_CODE
        ).let { dialog ->
            dialog.show(parentFragmentManager, TAG_DIALOG)
        }
    }

    private fun setupViewsWithArgs() {
        args.payeeDetails?.also {
            binding.payeeForm.name.setText(it.name, TextView.BufferType.EDITABLE)
            binding.payeeForm.nickname.setText(it.nickName, TextView.BufferType.EDITABLE)
            binding.payeeForm.accountNumber.setText(it.accountNumber, TextView.BufferType.EDITABLE)
            binding.payeeForm.address1.setText(it.address?.address1, TextView.BufferType.EDITABLE)
            binding.payeeForm.address2.setText(it.address?.address2, TextView.BufferType.EDITABLE)
            binding.payeeForm.city.setText(it.address?.city, TextView.BufferType.EDITABLE)
            binding.payeeForm.zipCode.setText(it.address?.zipCode, TextView.BufferType.EDITABLE)
            binding.payeeForm.state.setText(it.address?.state, TextView.BufferType.EDITABLE)
            binding.payeeForm.phoneNumber.setText(
                formatNumber(it.phoneNumber, Locale.US.country),
                TextView.BufferType.EDITABLE
            )
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupClickListeners() {
        binding.savePayee.setOnClickListener {
            if (viewModel.isValidPayee(::validateUsStateField)) {
                viewModel.savePayee()
            }
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun subscribeToDataChanges() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer { render(it) })
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    @Suppress("LongMethod")
    private fun render(viewState: ViewState) {
        displayLoading(viewState.isLoading)
        displaySearchView(viewState.displaySearchMode)
        displayPayeeForm(viewState.search)
        displaySearchResults(viewState.search)
        displayStateList(viewState.loadUsStates)
        if (viewState.isEditMode) {
            createEditableForm(viewState.selectedCompanyData)
        } else {
            displayManagePayeeFields(viewState.selectedCompanyData)
        }
        bindPayeeFormAccessibility()

        binding.savePayee.isEnabled = viewState.payeeValid
        binding.savePayee.contentDescription = if (viewState.payeeValid) {
            null
        } else {
            getString(R.string.accessibility_disabled_button, getString(R.string.save))
        }

        viewState.selectedState?.let {
            binding.payeeForm.state.setText(it.code)
            binding.payeeForm.stateLayout.error = null
        }

        handleError(viewState.savePayeeError)

        viewState.isSaveSuccess?.getNonConsumedContent()?.let {
            refreshPayees()
        }
        viewState.isDeleteSuccess?.getNonConsumedContent()?.let {
            parentSnackBarUtil.snackbar(
                getString(R.string.bill_pay_payee_deleted),
                duration = Snackbar.LENGTH_LONG
            )
            navigationViewModel.refreshPayeeList()
            findNavController().navigate(
                PayeeFormFragmentDirections.actionPayeeFormToPayeeList(
                    tabIndex = MANAGE_PAYEE_TAB_INDEX
                )
            )
        }
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    private fun createEditableForm(selectedCompanyData: SearchPayeeResultItem?) {
        if (selectedCompanyData != null && selectedCompanyData.merchantId != 0) {
            createEditableManageForm()
        } else {
            createEditableManualForm()
        }
    }

    private fun createEditableManualForm() {
        listOf(
            binding.payeeForm.nameLayout,
            binding.payeeForm.name,
            binding.payeeForm.nicknameLayout,
            binding.payeeForm.nickname,
            binding.payeeForm.accountNumberLayout,
            binding.payeeForm.accountNumber,
            binding.payeeForm.address1Layout,
            binding.payeeForm.address1,
            binding.payeeForm.address2Layout,
            binding.payeeForm.address2,
            binding.payeeForm.cityLayout,
            binding.payeeForm.city,
            binding.payeeForm.stateLayout,
            binding.payeeForm.state,
            binding.payeeForm.zipCodeLayout,
            binding.payeeForm.zipCode,
            binding.payeeForm.phoneNumberLayout,
            binding.payeeForm.phoneNumber
        ).forEach {
            it.visibility = View.VISIBLE
            it.isEnabled = true
        }
    }

    @Suppress("LongMethod")
    private fun createEditableManageForm() {
        listOf(
            binding.payeeForm.nameLayout,
            binding.payeeForm.name,
            binding.payeeForm.address1Layout,
            binding.payeeForm.address1,
            binding.payeeForm.address2Layout,
            binding.payeeForm.address2,
            binding.payeeForm.cityLayout,
            binding.payeeForm.city,
            binding.payeeForm.stateLayout,
            binding.payeeForm.state,
            binding.payeeForm.zipCodeLayout,
            binding.payeeForm.zipCode,
            binding.payeeForm.phoneNumberLayout,
            binding.payeeForm.phoneNumber
        ).forEach {
            it.visibility = View.VISIBLE
            it.isEnabled = false
            it.alpha = DISABLED_ALPHA
        }

        listOf(
            binding.payeeForm.nicknameLayout,
            binding.payeeForm.nickname,
            binding.payeeForm.accountNumberLayout,
            binding.payeeForm.accountNumber
        ).forEach {
            it.visibility = View.VISIBLE
            it.isEnabled = true
            it.alpha = ENABLED_ALPHA
        }

        binding.payeeForm.nickname.nextFocusDownId = binding.payeeForm.accountNumber.id
        binding.payeeForm.zipCode.setOnEditorActionListener(null)
        binding.payeeForm.accountNumber.setOnEditorActionListener(null)
    }

    private fun displayManagePayeeFields(selectedManagePayee: SearchPayeeResultItem?) {
        if (selectedManagePayee != null) {
            showManagePayeeForm(selectedManagePayee)
        } else {
            showManualPayeeForm()
        }
    }

    private fun showManualPayeeForm() {
        binding.payeeForm.name.isEnabled = true
        binding.payeeForm.name.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_AUTO
        binding.payeeForm.accountNumber.nextFocusDownId = binding.payeeForm.address1.id
        binding.payeeForm.zipCode.setOnEditorActionListener(null)
        binding.payeeForm.accountNumber.setOnEditorActionListener(null)
        listOf(
            binding.payeeForm.nameLayout,
            binding.payeeForm.accountNumberLayout,
            binding.payeeForm.nicknameLayout,
            binding.payeeForm.address1Layout,
            binding.payeeForm.address2Layout,
            binding.payeeForm.cityLayout,
            binding.payeeForm.stateLayout,
            binding.payeeForm.zipCodeLayout,
            binding.payeeForm.phoneNumberLayout
        ).forEach { it.visibility = View.VISIBLE }
    }

    @Suppress("LongMethod") // only setting up properties
    private fun showManagePayeeForm(selectedManagePayee: SearchPayeeResultItem) {
        binding.payeeForm.nameLayout.visibility = View.VISIBLE
        binding.payeeForm.nameLayout.error = null
        binding.payeeForm.name.setText(selectedManagePayee.name, TextView.BufferType.EDITABLE)
        binding.payeeForm.name.isEnabled = false
        binding.payeeForm.name.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
        binding.payeeForm.nameLayout.importantForAccessibility =
            View.IMPORTANT_FOR_ACCESSIBILITY_YES
        binding.payeeForm.nameLayout.contentDescription =
            getString(R.string.label_name, selectedManagePayee.name)
        binding.payeeForm.accountNumberLayout.visibility = View.VISIBLE
        if (selectedManagePayee.merchantZipRequired) {
            binding.payeeForm.accountNumber.imeOptions = EditorInfo.IME_ACTION_NEXT
            binding.payeeForm.accountNumber.nextFocusDownId = binding.payeeForm.zipCode.id
            binding.payeeForm.zipCodeLayout.visibility = View.VISIBLE
            binding.payeeForm.zipCode.imeOptions = EditorInfo.IME_ACTION_DONE
            binding.payeeForm.zipCode.setOnEditorActionListener(defaultEditorActionListener)
        } else {
            binding.payeeForm.zipCodeLayout.visibility = View.GONE
            binding.payeeForm.zipCode.setOnEditorActionListener(null)
            binding.payeeForm.accountNumber.imeOptions = EditorInfo.IME_ACTION_DONE
            binding.payeeForm.accountNumber.setOnEditorActionListener(defaultEditorActionListener)
        }

        listOf(
            binding.payeeForm.nicknameLayout,
            binding.payeeForm.address1Layout,
            binding.payeeForm.address2Layout,
            binding.payeeForm.cityLayout,
            binding.payeeForm.stateLayout,
            binding.payeeForm.phoneNumberLayout
        ).forEach { it.visibility = View.GONE }
    }

    private fun displayPayeeForm(search: ActionState<List<SearchPayeeResultItem>>?) {
        search?.let { (_, isActive, _, _) ->
            if (isActive == true) {
                binding.searchPayee.visibility = View.VISIBLE
                binding.dividerSearch.visibility = View.VISIBLE
                binding.payeeForm.root.visibility = View.GONE
                binding.searchResultsContainer.visibility = View.VISIBLE
            } else {
                binding.searchResultsContainer.visibility = View.GONE
                binding.payeeForm.root.visibility = View.VISIBLE
            }
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun displaySearchResults(search: ActionState<List<SearchPayeeResultItem>>?) {
        search?.let { (isLoading, isActive, result, _) ->
            if (isActive == true) {
                binding.searchInProgress.isVisible = isLoading
                if (result != null) {
                    displaySearchResult(result.getOrNull(), isLoading)
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    private fun displaySearchResult(items: List<SearchPayeeResultItem>?, isLoading: Boolean) {
        if (items != null) {
            binding.noItemsTv.isVisible = !isLoading && items.isEmpty()
            binding.searchRecycler.isVisible = !isLoading && items.isNotEmpty()
            updateAdapter(items)
        } else {
            binding.noItemsTv.isVisible = !isLoading
        }
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    private fun updateAdapter(items: List<SearchPayeeResultItem>) {
        if (items.isNotEmpty()) {
            searchAdapter.items = items
            binding.searchRecycler.layoutManager?.scrollToPosition(0)
        }
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    private fun displayStateList(loadUsState: ActionState<StatesOfUS>?) {
        loadUsState?.let { (isLoading, _, _, retry) ->
            displayLoading(isLoading)
            if (retry != null) {
                snackBarUtil.retrySnackbar { retry() }
            }
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun refreshPayees() {
        sharedViewModel.viewState.observeBy(this) {
            if (!it.isRefreshing && it.isEditableOnRefresh) {
                notifyPayeeUpdated()
            }
        }
        sharedViewModel.refreshPayees(fromEdition = true)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun notifyPayeeUpdated() {
        viewModel.payee?.also {
            if (it.isExistingPayee) {
                findNavController().navigate(
                    PayeeFormFragmentDirections.actionPayeeFormToPayeeDetail(
                        it.toParcelable()
                    )
                )
            } else {
                navigationViewModel.payeeAdded(it)
                findNavController().popBackStack(R.id.payeeFormFragment, true)
            }
        }
    }

    private fun handleError(event: ConsumableLiveEvent<Throwable>?) {
        event?.getNonConsumedContent()?.let {
            snackBarUtil.errorSnackbar(message = it.message, duration = Snackbar.LENGTH_SHORT)
        }
    }

    private fun displayLoading(loading: Boolean) {
        if (loading) {
            binding.progressBar.show()
        } else {
            binding.progressBar.hide()
        }
        binding.contentScroll.isVisible = !loading
    }

    private fun displaySearchView(isShow: Boolean) {
        if (isShow) {
            binding.searchPayee.visibility = View.VISIBLE
            binding.dividerSearch.visibility = View.VISIBLE
        } else {
            binding.searchPayee.visibility = View.GONE
            binding.dividerSearch.visibility = View.GONE
        }
    }

    @Suppress("LongMethod")
    private fun bindPayeeFormAccessibilityListeners() {
        binding.payeeForm.nameLayout.bindTextEditAccessibilityListeners(
            binding.payeeForm.name,
            getString(R.string.name),
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.nicknameLayout.bindTextEditAccessibilityListeners(
            binding.payeeForm.nickname,
            getString(R.string.nickname),
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.accountNumberLayout.bindTextEditAccessibilityListeners(
            binding.payeeForm.accountNumber,
            getString(R.string.account_number),
            true,
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.address1Layout.bindTextEditAccessibilityListeners(
            binding.payeeForm.address1,
            getString(R.string.address1),
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.address2Layout.bindTextEditAccessibilityListeners(
            binding.payeeForm.address2,
            getString(R.string.address2),
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.cityLayout.bindTextEditAccessibilityListeners(
            binding.payeeForm.city,
            getString(R.string.city),
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.stateLayout.bindTextEditAccessibilityListeners(
            binding.payeeForm.state,
            getString(R.string.state)
        )
        binding.payeeForm.zipCodeLayout.bindTextEditAccessibilityListeners(
            binding.payeeForm.zipCode,
            getString(R.string.zip_code),
            true,
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.phoneNumberLayout.bindTextEditAccessibilityListeners(
            binding.payeeForm.phoneNumber,
            getString(R.string.phone_number),
            actionLabel = getString(R.string.edit_action)
        )
    }

    @Suppress("LongMethod")
    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun bindPayeeFormAccessibility() {
        binding.payeeForm.nameLayout.bindTextEditAccessibility(
            getString(R.string.name),
            viewModel.rawName.value,
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.nicknameLayout.bindTextEditAccessibility(
            getString(R.string.nickname),
            viewModel.rawNickname.value,
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.accountNumberLayout.bindTextEditAccessibility(
            getString(R.string.account_number),
            viewModel.rawAccountNumber.value,
            true,
            getString(R.string.edit_action)
        )
        binding.payeeForm.address1Layout.bindTextEditAccessibility(
            getString(R.string.address1),
            viewModel.rawAddress1.value,
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.address2Layout.bindTextEditAccessibility(
            getString(R.string.address2),
            viewModel.rawAddress2.value,
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.cityLayout.bindTextEditAccessibility(
            getString(R.string.city),
            viewModel.rawCity.value,
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.stateLayout.bindTextEditAccessibility(
            getString(R.string.state),
            viewModel.rawState.value
        )
        binding.payeeForm.zipCodeLayout.bindTextEditAccessibility(
            getString(R.string.zip_code),
            viewModel.rawZipCode.value,
            true,
            actionLabel = getString(R.string.edit_action)
        )
        binding.payeeForm.phoneNumberLayout.bindTextEditAccessibility(
            getString(R.string.phone_number),
            viewModel.rawPhoneNumber.value,
            actionLabel = getString(R.string.edit_action)
        )
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupViews() {
        setupSearch()
        setupName()
        setupNickname()
        setupAccountNumber()
        setupAddress()
        setupCity()
        setupStateInput()
        setupZipCode()
        setupPhoneNumber()
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupSearch() {
        binding.searchPayee.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.searchPayee(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                viewModel.searchPayee(newText)
                return true
            }
        })

        with(binding.searchRecycler) {
            layoutManager = LinearLayoutManager(requireContext())
            setupRecyclerViewDivider(R.drawable.thin_divider)
            adapter = searchAdapter
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupPhoneNumber() {
        binding.payeeForm.phoneNumberLayout.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = NoOperationFormatter(),
                validator = ComposedValidator(
                    RequiredFieldValidator(
                        getString(R.string.bill_pay_empty_payee_phone)
                    ),
                    UsPhoneLengthValidator(
                        getString(R.string.bill_pay_invalid_payee_phone_length)
                    ),
                    UsPhoneValidator(
                        getString(R.string.bill_pay_invalid_payee_phone),
                        requireActivity().applicationContext
                    )
                )
            ),
            validatedData = viewModel.phoneNumberInputState,
            rawContentData = viewModel.rawPhoneNumber
        )
        binding.payeeForm.phoneNumber.addTextChangedListener(PhoneNumberFormattingTextWatcher(Locale.US.country))
        binding.payeeForm.phoneNumber.setOnEditorActionListener(defaultEditorActionListener)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupZipCode() {
        binding.payeeForm.zipCodeLayout.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = NoOperationFormatter(),
                validator = RequiredFieldValidator(
                    getString(R.string.bill_pay_required_payee_zip_code)
                ) + LengthInRangeValidator(
                    MAX_ZIP_CODE_LENGTH..MAX_ZIP_CODE_LENGTH,
                    getString(R.string.bill_pay_invalid_payee_zip_code)
                ) + ZipValidator(
                    getString(R.string.bill_pay_invalid_payee_zip_code_zero)
                )
            ),
            validatedData = viewModel.zipCodeInputState,
            rawContentData = viewModel.rawZipCode
        )
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupStateInput() {
        binding.payeeForm.stateLayout.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = NoOperationFormatter(),
                validator = RequiredFieldValidator(getString(R.string.bill_pay_invalid_payee_state))
            ),
            rawContentData = viewModel.rawState
        )
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupCity() {
        binding.payeeForm.cityLayout.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = NoOperationFormatter(),
                validator = RequiredFieldValidator(
                    getString(R.string.bill_pay_required_payee_city)
                ) + LengthInRangeValidator(
                    1..TEXT_INPUT_MAX_LENGTH,
                    getString(R.string.bill_pay_invalid_payee_city)
                )
            ),
            validatedData = viewModel.cityInputState,
            rawContentData = viewModel.rawCity
        )
        binding.payeeForm.city.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                stateSelector.open()
                binding.payeeForm.city.clearFocusSafely()
                true
            } else {
                false
            }
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupAddress() {
        binding.payeeForm.address1Layout.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = NoOperationFormatter(),
                validator = RequiredFieldValidator(
                    getString(R.string.bill_pay_required_payee_address1)
                ) + LengthInRangeValidator(
                    1..ADDRESS_INPUT_MAX_LENGTH,
                    getString(R.string.bill_pay_invalid_payee_address1)
                )
            ),
            validatedData = viewModel.address1InputState,
            rawContentData = viewModel.rawAddress1
        )
        binding.payeeForm.address2Layout.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = NoOperationFormatter(),
                validator = AlwaysValidValidator()
            ),
            validatedData = viewModel.address2InputState,
            rawContentData = viewModel.rawAddress2
        )
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupAccountNumber() {
        binding.payeeForm.accountNumberLayout.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = NoOperationFormatter(),
                validator = RequiredFieldValidator(
                    getString(R.string.bill_pay_required_payee_account)
                ) + LengthInRangeValidator(
                    1..TEXT_INPUT_MAX_LENGTH,
                    getString(R.string.bill_pay_invalid_payee_account_number)
                )
            ),
            validatedData = viewModel.accountNumberInputState,
            rawContentData = viewModel.rawAccountNumber
        )
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupNickname() {
        binding.payeeForm.nicknameLayout.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = NoOperationFormatter(),
                validator = AlwaysValidValidator()
            ),
            validatedData = viewModel.nickNameInputState,
            rawContentData = viewModel.rawNickname
        )
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupName() {
        binding.payeeForm.nameLayout.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = NoOperationFormatter(),
                validator = RequiredFieldValidator(
                    getString(R.string.bill_pay_required_payee_name)
                ) + LengthInRangeValidator(
                    2..TEXT_INPUT_MAX_LENGTH,
                    getString(R.string.bill_pay_invalid_payee_name)
                )
            ),
            validatedData = viewModel.nameInputState,
            rawContentData = viewModel.rawName
        )
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun setupDropDowns() {
        stateSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = TAG_STATE,
                multiSelectEnabled = false,
                title = getString(R.string.state),
                itemsData = getDropDownStates(),
                selectListener = { _, item ->
                    viewModel.onStateSelected(item.value)
                    binding.payeeForm.zipCode.requestFocus()
                },
                onCancelListener = {
                    validateUsStateField()
                    binding.payeeForm.zipCode.requestFocus()
                }
            )
        )
        binding.payeeForm.state.setOnClickListener { stateSelector.open() }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun getDropDownStates(): LiveData<List<DropDownMultiSelector.Selectable<UsState>>> {
        return Transformations.map(viewModel.viewState) {
            val usStateList = if (it.loadUsStates?.result != null) {
                it.loadUsStates.result.getOrDefault(StatesOfUS(emptyList()))
            } else {
                StatesOfUS(emptyList())
            }

            usStateList.states.map { usState ->
                if (usState.description != null) {
                    buildSelectableWithDescription(usState)
                } else {
                    buildSelectable(usState)
                }
            }
        }
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    private fun buildSelectable(usState: UsState): DropDownMultiSelector.Selectable<UsState> {
        return DropDownMultiSelector.Selectable(
            title = "",
            titleFormatter = { item -> item.code },
            value = usState,
            selected = usState == viewModel.viewState.value?.selectedState,
            titleColor = ContextCompat.getColor(requireContext(), R.color.colorAccent)
        )
    }

    @ExperimentalCoroutinesApi
    @FlowPreview
    private fun buildSelectableWithDescription(usState: UsState): DropDownMultiSelector.Selectable<UsState> {
        return DropDownMultiSelector.Selectable(
            title = "",
            titleFormatter = { item -> item.code },
            descriptionFormatter = { item -> item.description.orEmpty() },
            value = usState,
            selected = usState == viewModel.viewState.value?.selectedState,
            titleColor = ContextCompat.getColor(requireContext(), R.color.colorAccent)
        )
    }

    private fun validateUsStateField(): Boolean {
        if (binding.payeeForm.stateLayout.isVisible) {
            binding.payeeForm.stateLayout.formatContents()
            return binding.payeeForm.stateLayout.getValue() is InputFieldManager.Value.Valid
        }
        return true
    }

    private fun setupChangeListeners() =
        inputs.forEach { it.doOnTextChanged { _, _, _, _ -> removeErrorIfFocused(it) } }

    private fun removeErrorIfFocused(input: TextInputEditText?) {
        val target = input ?: return
        if (target.isFocused) {
            clearError(input)
        }
    }

    private fun clearError(target: TextInputEditText) {
        target.error = null
    }
}
