package com.etrade.mobilepro.watchlist.delegates

import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

interface AddToWatchListDelegate {
    val newSymbolAddedToWatchList: LiveData<ConsumableLiveEvent<Unit>>

    fun init(fragment: Fragment, loadingIndicator: ContentLoadingProgressBar?)
    fun showWatchList()
    fun setSymbol(symbol: SearchResultItem.Symbol)
    fun clearUp()
    fun getWatchList()
}
