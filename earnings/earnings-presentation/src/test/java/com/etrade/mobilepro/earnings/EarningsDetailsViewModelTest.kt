package com.etrade.mobilepro.earnings

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.earnings.dto.EarningsResponse
import com.etrade.mobilepro.earnings.repo.EarningsRepo
import com.etrade.mobilepro.earnings.viewModel.EarningsDetailsViewModel
import com.etrade.mobilepro.quote.dto.QuoteDto
import com.etrade.mobilepro.quote.dto.QuoteStockDto
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.coroutine.TestContextProvider
import com.etrade.mobilepro.util.json.BigDecimalAdapter
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import java.math.BigDecimal

class EarningsDetailsViewModelTest {
    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private val earningsRepo = mock<EarningsRepo>()
    private val resources: Resources = mockResourcesString()

    @ExperimentalCoroutinesApi
    private val testDispatcherProvider = TestContextProvider()

    lateinit var sut: EarningsDetailsViewModel

    private fun getMockQuotes(): QuoteDto =
        this::class.getObjectFromJson<QuoteStockDto>("quotes.json")

    private inline fun <reified T : Any> getMockEarnings(path: String = "market_earnings.json"): T {
        return this::class.getObjectFromJson(
            path,
            adapters = listOf(BigDecimalAdapter)
        )
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `test create earnings details with 3 periods`() {
        runBlockingTest {
            whenever(earningsRepo.loadEarnings(any(), any(), any())).thenReturn(ETResult.success(createMapping(getMockEarnings("market_earnings_3.json"))))
            whenever(resources.getString(R.string.empty_date_default)).thenReturn("--")

            sut =
                EarningsDetailsViewModel(earningsRepo, testDispatcherProvider, resources)

            sut.selectedGraphIndex.observeForever {
                Assert.assertEquals(0, it)
            }
            sut.loadGraphDetails("AAPL", false)

            sut.marketEarningsQuarterly.observeForever {
                Assert.assertTrue(it is Resource.Success)
                Assert.assertNotNull(it.data?.message)
            }

            sut.marketEarningsAnnual.observeForever {
                Assert.assertTrue(it is Resource.Success)
                Assert.assertFalse(it.isEmpty())
            }

            sut.selectedQuarterEarningsDetails.observeForever {
                Assert.assertNotNull(it?.actual)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `returns error message when earnings call is not successful`() {
        runBlockingTest {
            whenever(earningsRepo.loadEarnings(any(), any(), any())).thenReturn(ETResult.failure(Throwable("bad things")))

            val sut =
                EarningsDetailsViewModel(earningsRepo, testDispatcherProvider, resources)

            whenever(resources.getString(R.string.empty_date_default)).thenReturn("--")
            sut.loadGraphDetails("AAPL", false)

            sut.marketEarningsQuarterly.observeForever {
                Assert.assertTrue(it is Resource.Failed)
            }

            sut.marketEarningsAnnual.observeForever {
                Assert.assertTrue(it is Resource.Failed)
            }

            sut.selectedQuarterEarningsDetails.observeForever {
                Assert.assertNull(it)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets calendar earnings event estimate date to 3 months from announcement when expected date is null`() {
        runBlockingTest {
            whenever(earningsRepo.loadEarnings(any(), any(), any())).thenReturn(ETResult.success(createMapping(getMockEarnings("market_earnings_3.json"))))

            sut = EarningsDetailsViewModel(earningsRepo, testDispatcherProvider, resources)
            sut.selectedGraphIndex.observeForever {
                Assert.assertEquals(0, it)
            }

            sut.loadGraphDetails("AAPL", false)
            sut.marketEarningsQuarterly.observeForever {
                val expectedDate = LocalDateTime.of(2021, 11, 22, 0, 0)
                    .atZone(ZoneId.systemDefault())
                    .plusWeeks(12L)
                    .toLocalDateTime()

                val expectedDateInMillis =
                    expectedDate.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()

                sut.symbolTitle = "AAPL"
                val result = sut.getCalendarEvent()

                Assert.assertEquals("AAPL", result?.title)
                Assert.assertEquals(expectedDateInMillis, result?.startTimeMillis)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets calendar earnings event estimate date to expected date when expected date is not 2 day past or before current date`() {
        runBlockingTest {
            whenever(earningsRepo.loadEarnings(any(), any(), any())).thenReturn(ETResult.success(createMapping(getMockEarnings("market_earnings_5.json"))))

            sut = EarningsDetailsViewModel(earningsRepo, testDispatcherProvider, resources)
            sut.selectedGraphIndex.observeForever {
                Assert.assertEquals(0, it)
            }
            sut.loadGraphDetails("AAPL", false)

            sut.marketEarningsQuarterly.observeForever {
                val expectedDate = LocalDateTime.of(2999, 11, 22, 0, 0)
                    .atZone(ZoneId.systemDefault())
                    .toLocalDateTime()

                val expectedDateInMillis =
                    expectedDate.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()

                sut.symbolTitle = "AAPL"
                val result = sut.getCalendarEvent()

                Assert.assertEquals("AAPL", result?.title)
                Assert.assertEquals(expectedDateInMillis, result?.startTimeMillis)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets calendar earnings event estimate date to null when announce date is null`() {
        runBlockingTest {
            whenever(earningsRepo.loadEarnings(any(), any(), any())).thenReturn(ETResult.success(createMapping(getMockEarnings("market_earnings_4.json"))))

            sut = EarningsDetailsViewModel(earningsRepo, testDispatcherProvider, resources)
            sut.selectedGraphIndex.observeForever {
                Assert.assertEquals(0, it)
            }
            sut.loadGraphDetails("AAPL", false)

            sut.marketEarningsQuarterly.observeForever {
                sut.symbolTitle = "AAPL"
                val result = sut.getCalendarEvent()

                Assert.assertEquals(null, result?.title)
                Assert.assertEquals(null, result?.startTimeMillis)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets calendar earnings event estimate date to null when symbol title is null`() {
        runBlockingTest {
            whenever(earningsRepo.loadEarnings(any(), any(), any())).thenReturn(ETResult.success(createMapping(getMockEarnings("market_earnings_3.json"))))

            sut = EarningsDetailsViewModel(earningsRepo, testDispatcherProvider, resources)
            sut.selectedGraphIndex.observeForever {
                Assert.assertEquals(0, it)
            }
            sut.loadGraphDetails("AAPL", false)

            sut.marketEarningsQuarterly.observeForever {
                val result = sut.getCalendarEvent()

                Assert.assertEquals(null, result?.title)
                Assert.assertEquals(null, result?.startTimeMillis)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets selected graph data when earnings quarterly is successful`() {
        runBlockingTest {
            whenever(earningsRepo.loadEarnings(any(), any(), any())).thenReturn(ETResult.success(createMapping(getMockEarnings("market_earnings_5.json"))))

            val sut = EarningsDetailsViewModel(earningsRepo, testDispatcherProvider, resources)
            sut.loadGraphDetails("AAPL", false)
            sut.graphIndex = 0
            sut.selectedGraphData.observeForever {
                val result = sut.getGraphDataItem(0)
                Assert.assertEquals(0.0f, result?.x)
                Assert.assertEquals(0.0f, result?.y)
                Assert.assertEquals(2131099910, result?.barColor)
                Assert.assertEquals(2131099789, result?.valueColor)
                Assert.assertEquals(2131099917, result?.valueDescriptionColor)
                Assert.assertEquals("Q1\\'19", result?.xFormatted)
                Assert.assertEquals("", result?.valueDescriptionFormatted)
                Assert.assertEquals("1", result?.quarter)
                Assert.assertEquals(false, result?.isFuturePeriod)

                val resultEarningsDetailData = result?.earningsDetailData

                Assert.assertEquals(BigDecimal("0.0"), resultEarningsDetailData?.high)
                Assert.assertEquals(BigDecimal("0.0"), resultEarningsDetailData?.low)
                Assert.assertEquals(BigDecimal("0.0"), resultEarningsDetailData?.estMean)
                Assert.assertEquals("1", resultEarningsDetailData?.quarter)
                Assert.assertEquals("0", resultEarningsDetailData?.expectedDate)
                Assert.assertEquals("0", resultEarningsDetailData?.announceDate)
                Assert.assertEquals("", resultEarningsDetailData?.announceTime)
                Assert.assertEquals(BigDecimal("0.0"), resultEarningsDetailData?.actualValue)
                Assert.assertEquals(BigDecimal("0.0"), resultEarningsDetailData?.surpriseMean)
                Assert.assertEquals("2019", resultEarningsDetailData?.fiscalPeriodYear)
                Assert.assertEquals(BigDecimal("0"), resultEarningsDetailData?.dateTime)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets selected graph data to null when earnings quarterly is successful but index is not found`() {
        runBlockingTest {
            whenever(earningsRepo.loadEarnings(any(), any(), any())).thenReturn(ETResult.success(createMapping(getMockEarnings("market_earnings_5.json"))))

            val sut = EarningsDetailsViewModel(earningsRepo, testDispatcherProvider, resources)
            sut.loadGraphDetails("AAPL", false)
            sut.selectedGraphData.observeForever {
                val result = sut.getGraphDataItem(1000)
                Assert.assertEquals(null, result)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets last history quarter index when earnings quarterly is successful`() {
        runBlockingTest {
            whenever(earningsRepo.loadEarnings(any(), any(), any())).thenReturn(ETResult.success(createMapping(getMockEarnings("market_earnings_5.json"))))

            val sut = EarningsDetailsViewModel(earningsRepo, testDispatcherProvider, resources)
            sut.loadGraphDetails("AAPL", false)
            sut.selectedGraphData.observeForever {
                val result = sut.getLastHistoryQuarterIndex()
                Assert.assertEquals(1, result)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets highlight items when earnings quarterly is successful`() {
        runBlockingTest {
            whenever(earningsRepo.loadEarnings(any(), any(), any())).thenReturn(ETResult.success(createMapping(getMockEarnings("market_earnings_5.json"))))

            val sut = EarningsDetailsViewModel(earningsRepo, testDispatcherProvider, resources)
            sut.loadGraphDetails("AAPL", false)
            sut.selectedGraphData.observeForever {
                val result = sut.getHighlightItems(0)
                Assert.assertNotNull(result)
            }
        }
    }

    private fun createMapping(mockEarnings: EarningsResponse): EarningsResponseResult =
        EarningsResponseResult(createMarkerEarningsResponseResult(mockEarnings.response?.detail?.firstOrNull()?.period))
}
