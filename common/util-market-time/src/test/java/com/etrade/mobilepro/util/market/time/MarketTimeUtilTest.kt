package com.etrade.mobilepro.util.market.time

import io.mockk.every
import io.mockk.mockkStatic
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.threeten.bp.LocalDateTime

private const val MARKET_TIME_UTIL_PACKAGE = "com.etrade.mobilepro.util.market.time.MarketTimeUtilKt"

internal class MarketTimeUtilTest {

    @Test
    fun `test isMarketDay`() {
        mockkStatic(MARKET_TIME_UTIL_PACKAGE)
        every { getCurrentTimeNewYork() } returns (LocalDateTime.of(2020, 7, 8, 0, 0).atZone(NEW_YORK_ZONE_ID))
        assertTrue(isMarketDay())

        every { getCurrentTimeNewYork() } returns (LocalDateTime.of(2020, 7, 4, 0, 0).atZone(NEW_YORK_ZONE_ID))
        assertFalse(isMarketDay())

        every { getCurrentTimeNewYork() } returns (LocalDateTime.of(2020, 6, 27, 0, 0).atZone(NEW_YORK_ZONE_ID))
        assertFalse(isMarketDay())
    }

    @Test
    fun `test isPreMarketTime`() {
        mockkStatic(MARKET_TIME_UTIL_PACKAGE)
        every { getCurrentTimeNewYork() } returns (LocalDateTime.of(2020, 7, 8, 0, 0).atZone(NEW_YORK_ZONE_ID))
        assertTrue(isPreMarketTime())

        every { getCurrentTimeNewYork() } returns (LocalDateTime.of(2020, 7, 8, 8, 0).atZone(NEW_YORK_ZONE_ID))
        assertTrue(isPreMarketTime())

        every { getCurrentTimeNewYork() } returns (LocalDateTime.of(2020, 7, 8, 10, 0).atZone(NEW_YORK_ZONE_ID))
        assertFalse(isPreMarketTime())

        every { getCurrentTimeNewYork() } returns (LocalDateTime.of(2020, 7, 18, 0, 0).atZone(NEW_YORK_ZONE_ID))
        assertFalse(isPreMarketTime())
    }

    @Test
    fun `test isMarketTime`() {
        mockkStatic(MARKET_TIME_UTIL_PACKAGE)
        every { getCurrentTimeNewYork() } returns (LocalDateTime.of(2020, 7, 8, 0, 0).atZone(NEW_YORK_ZONE_ID))
        assertFalse(isMarketTime())

        every { getCurrentTimeNewYork() } returns (LocalDateTime.of(2020, 7, 8, 9, 30).atZone(NEW_YORK_ZONE_ID))
        assertTrue(isMarketTime())

        every { getCurrentTimeNewYork() } returns (LocalDateTime.of(2020, 7, 8, 15, 59).atZone(NEW_YORK_ZONE_ID))
        assertTrue(isMarketTime())

        every { getCurrentTimeNewYork() } returns (LocalDateTime.of(2020, 7, 8, 16, 1).atZone(NEW_YORK_ZONE_ID))
        assertFalse(isMarketTime())
    }
}
