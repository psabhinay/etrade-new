package com.etrade.mobilepro.markets

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.R
import com.etrade.mobilepro.markets.overview.MarketsOverviewFragment
import com.etrade.mobilepro.marketscalendar.MarketsCalendarFragment
import com.etrade.mobilepro.screener.presentation.ScreenersContainerFragment
import com.etrade.mobilepro.util.android.adapter.DefaultFragmentPagerAdapter

class MarketsPagerAdapter(fragment: Fragment) : DefaultFragmentPagerAdapter(
    fragment,
    listOf(
        FragmentPagerItem(MarketsOverviewFragment::class.java, R.string.overview),
        FragmentPagerItem(MarketsCalendarFragment::class.java, R.string.calendar),
        FragmentPagerItem(ScreenersContainerFragment::class.java, R.string.screeners)
    )
)
