# Multi Swipe Refresh

## Description

A SwipeRefreshLayout that checks on a list of views' canScrollVertically up to help with multiple scrollable views.

## Usage

Sample usage with a layout that has a list view inside a scrollview and also delegate usage that we no longer use

```Kotlin
   const val REFRESH_DURATION = 5000L
   const val ITEMS_COUNT = 1000

   class SampleScrollViewPullDownFragment : Fragment() {

       override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
           val fragmentView: View by SwipeRefreshDelegate(R.layout.fragment_sample_scrollview, inflater, container)
           return fragmentView
       }

       override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
           super.onViewCreated(view, savedInstanceState)
           swipeRefreshLayout?.setOnRefreshListener {
               swipeRefreshLayout?.postDelayed({
                   swipeRefreshLayout?.isRefreshing = false
               }, REFRESH_DURATION)
           }

           populateListView()

           sampleListView.setOnTouchListener { touchedListView, _ ->
               if (swipeRefreshLayout?.isRefreshing == false) {
                   swipeRefreshLayout?.isEnabled = sampleListView?.firstVisiblePosition == 0
               }
               touchedListView.parent.requestDisallowInterceptTouchEvent(true)
               return@setOnTouchListener false
           }

           sampleListView.setOnScrollListener(object : AbsListView.OnScrollListener {
               override fun onScroll(view: AbsListView?, firstVisibleItem: Int, visibleItemCount: Int, totalItemCount: Int) {
                   // NOP
               }

               override fun onScrollStateChanged(view: AbsListView?, scrollState: Int) {
                   if (swipeRefreshLayout?.isRefreshing == false) {
                       swipeRefreshLayout?.isEnabled = scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                   }
               }
           })
       }

       private fun populateListView() {
           val numArray = mutableListOf<Int>()
           val random = Random()
           repeat(ITEMS_COUNT) { numArray.add(random.nextInt()) }
           sampleListView.adapter = ArrayAdapter(context!!, android.R.layout.simple_list_item_1, numArray)
       }
   }
```

```kotlin
class SwipeRefreshDelegate(
    private val innerLayoutId: Int,
    private val inflater: LayoutInflater,
    private val container: ViewGroup?
) : ReadOnlyProperty<Any?, View> {

    override fun getValue(thisRef: Any?, property: KProperty<*>): View {
        val fragmentView = inflater.inflate(R.layout.layout_pulltorefresh, container, false)
        val swipeRefreshLayout = fragmentView.findViewById<MultiSwipeRefreshLayout>(R.id.swipeRefreshLayout)
        inflater.inflate(innerLayoutId, swipeRefreshLayout, true)
        return fragmentView
    }
}
```

```xml
<?xml version="1.0" encoding="utf-8"?>
<ScrollView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:focusableInTouchMode="true"
    android:fillViewport="true">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">

        <TextView
            android:id="@+id/refreshText"
            android:layout_width="match_parent"
            android:layout_height="371dp"
            android:gravity="center"
            android:text="@string/title_account"/>

        <ListView
            android:id="@+id/sampleListView"
            android:layout_width="wrap_content"
            android:layout_height="300dp"
            tools:ignore="NestedScrolling">
        </ListView>
    </LinearLayout>
</ScrollView>
```
