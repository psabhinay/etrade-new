package com.etrade.mobilepro.markets

import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequiredLoginProvider
import com.etrade.mobilepro.common.isRequireLoginAnnotationPresent
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.toolbarActionStartView
import com.etrade.mobilepro.common.viewpager.FragmentPageListener
import com.etrade.mobilepro.common.viewpager.setupFragmentPageListener
import com.etrade.mobilepro.databinding.FragmentMarketsBinding
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.navigation.EtNavController
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.tooltip.Tooltip
import com.etrade.mobilepro.tooltip.TooltipGravity
import com.etrade.mobilepro.tooltip.TooltipViewModel
import com.etrade.mobilepro.tracking.initTabsTracking
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.viewCoroutineScope
import javax.inject.Inject

private const val MARKETS_TAB_INDEX: String = "tabIndex"

class MarketsFragment @Inject constructor(
    private val applicationPreferences: ApplicationPreferences,
    private val viewModelFactory: ViewModelProvider.Factory,
    private val userViewModel: UserViewModel
) : Fragment(R.layout.fragment_markets), RequiredLoginProvider {

    private val binding by viewBinding(FragmentMarketsBinding::bind) { onDestroyBinding() }

    private val tooltipViewModel: TooltipViewModel by activityViewModels()
    private val viewModel: MarketsViewModel by viewModels { viewModelFactory }
    private var tooltip: Tooltip? = null

    private var pageListener: FragmentPageListener? = null

    private val etNavController: EtNavController by lazy { EtNavController(findNavController(), userViewModel) }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        handleTooltip()
        bindViewModel()
    }

    private fun setupFragment() {
        MarketsPagerAdapter(this).attach(binding.tabLayout, binding.viewpager)
        with(binding.viewpager) {
            initTabsTracking(viewModel)
            offscreenPageLimit = 1
        }
        pageListener = binding.viewpager.setupFragmentPageListener(this)
        binding.viewpager.setCurrentItem(calculateTabToOpen(), false)
        clearTabNavigationArguments()
    }

    private fun bindViewModel() {
        viewModel.loginSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { tab ->
                    navigateToLogin(tabToReturnTo = tab.index)
                }
            }
        )
        viewModel.openTabSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { tab ->
                    binding.viewpager.setCurrentItem(tab.index, false)
                }
            }
        )
        userViewModel.userSessionState.observeBy(viewLifecycleOwner) {
            setupToolbarActionStart(it.peekContent() == SessionStateChange.AUTHENTICATED)
        }
    }

    private fun FragmentMarketsBinding.onDestroyBinding() {
        tooltipViewModel.dismiss(tooltip)
        tooltip = null
        viewpager.adapter = null
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        setupToolbarActionStart(userViewModel.isAuthenticated)
    }

    private fun setupToolbarActionStart(isAuthenticated: Boolean) {
        requireActivity().toolbarActionStartView?.apply {
            if (isAuthenticated) {
                visibility = View.GONE
                text = null
                setOnClickListener(null)
            } else {
                visibility = View.VISIBLE
                setToolbarActionTextAndContentDescription(R.string.button_text_logon)
                setOnClickListener {
                    navigateToLogin()
                }
            }
        }
    }

    private fun navigateToLogin(tabToReturnTo: Int = binding.viewpager.currentItem) {
        etNavController.navigate(object : NavDirections {
            override fun getActionId(): Int = R.id.action_market_with_login

            override fun getArguments(): Bundle = Bundle().apply {
                putInt(MARKETS_TAB_INDEX, tabToReturnTo)
            }
        })
    }

    private fun handleTooltip() {
        if (applicationPreferences.shouldDisplayMarketsTooltip) {
            (activity as? MainActivity)
                ?.binding
                ?.bottomNavigation
                ?.findViewById<View>(R.id.menuGraph)
                ?.let {
                    tooltip = tooltipViewModel.show {
                        target = it
                        text = getString(R.string.tooltip_markets)
                        gravity = TooltipGravity.TOP
                        yOffset = resources.getDimensionPixelOffset(R.dimen.spacing_large)
                        durationParams = Tooltip.DurationParams(viewCoroutineScope)
                    }
                }
        }
    }

    private fun calculateTabToOpen(): Int {
        val fromArgs = arguments?.getInt(MARKETS_TAB_INDEX) ?: 0
        return if (viewModel.tabRequiresAuth(fromArgs) && !userViewModel.isAuthenticated) {
            0
        } else {
            fromArgs
        }
    }

    private fun clearTabNavigationArguments() {
        if (userViewModel.isAuthenticated) {
            arguments?.remove(MARKETS_TAB_INDEX)
        }
    }

    override fun isLoginRequired(): Boolean = pageListener?.selectedFragment?.isRequireLoginAnnotationPresent() ?: false
}
