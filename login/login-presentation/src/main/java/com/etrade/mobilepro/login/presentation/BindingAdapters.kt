package com.etrade.mobilepro.login.presentation

import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.login.presentation.widget.CardLoginWidget

@BindingAdapter("loginViewModel")
fun bindLoginViewModel(view: CardLoginWidget, loginViewModel: LoginViewModel?) {
    view.bindViewModel(loginViewModel)
}
