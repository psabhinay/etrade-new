package com.etrade.mobilepro.trade.presentation.form.delegate.dependencies

import android.content.res.Resources
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.ConditionalPriceTypeValueId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.presentation.R

internal class ConditionalFactoryDelegateDependenciesImpl(
    inputId: ConditionalOrderTradeFormInputId,
    resources: Resources
) : StockFactoryDelegateDependenciesImpl<ConditionalOrderTradeFormInputId>(inputId, resources) {

    private val priceTypeValueId: ConditionalPriceTypeValueId = inputId.priceTypeValueId
    private val termValueId: OrderTermValueId = inputId.termValueId

    override val stopPriceValidPriceTypes = listOf(
        priceTypeValueId.stop,
        priceTypeValueId.trailingStopPercent,
        priceTypeValueId.trailingStopDollar,
        priceTypeValueId.hiddenStop
    )

    override val priceTypePriceLabelMap = mapOf(
        priceTypeValueId.limit to resources.getString(R.string.trade_hint_limit_price),
        priceTypeValueId.stop to resources.getString(R.string.trade_hint_stop_price),
        priceTypeValueId.stopLimit to resources.getString(R.string.trade_hint_stop_price),
        priceTypeValueId.trailingStopDollar to resources.getString(R.string.trade_hint_trailing_stop_dollar),
        priceTypeValueId.trailingStopPercent to resources.getString(R.string.trade_hint_trailing_stop_percent),
        priceTypeValueId.hiddenStop to resources.getString(R.string.trade_hint_trailing_stop_dollar)
    )

    override val limitOrderTermLabelMap = mapOf(
        termValueId.goodUntilCancel to resources.getString(R.string.trade_term_gtc),
        termValueId.fillOrKill to resources.getString(R.string.trade_term_fok),
        termValueId.immediateOrCancel to resources.getString(R.string.trade_term_ioc)
    )

    override val saveAndPreviewActionRequiredInputs = setOf(
        inputId.action,
        inputId.priceType,
        inputId.quantity,
        inputId.symbol,
        inputId.term,
        inputId.limitPrice,
        inputId.limitPriceForStopLimit,
        inputId.stopPrice,
        inputId.stopPriceForStopLimit
    )

    override val allowShortSellUpdatedTerms: Boolean = false
}
