package com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemTimeoutsBinding
import com.etrade.mobilepro.debugmenu.presentation.item.TimeoutsItem

internal class TimeoutsViewHolder(
    private val binding: DebugmenuItemTimeoutsBinding,
    listener: (sessionLimitEnabled: Boolean) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    private var item: TimeoutsItem? = null

    init {
        binding.limitSession.setOnCheckedChangeListener { _, isChecked ->
            if (item?.value?.limitSessionTo30Sec != isChecked) {
                listener.invoke(isChecked)
            }
        }
    }

    fun bindTo(item: TimeoutsItem) {
        this.item = item
        binding.limitSession.isChecked = item.value.limitSessionTo30Sec
    }
}
