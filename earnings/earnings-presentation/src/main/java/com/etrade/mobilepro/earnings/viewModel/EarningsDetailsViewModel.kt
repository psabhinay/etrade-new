package com.etrade.mobilepro.earnings.viewModel

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.earnings.EarningsDetailData
import com.etrade.mobilepro.earnings.R
import com.etrade.mobilepro.earnings.repo.EarningsRepo
import com.etrade.mobilepro.earnings.rest.PERIOD_ANNUAL
import com.etrade.mobilepro.earnings.rest.PERIOD_QUARTERLY
import com.etrade.mobilepro.tracking.SCREEN_TITLE_EARNINGS_DETAILS
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.CalendarEvent
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import com.github.mikephil.charting.highlight.Highlight
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import org.threeten.bp.ZoneId
import javax.inject.Inject

private const val QUARTERLY_GRAPH_INDEX = 0

class EarningsDetailsViewModel @Inject constructor(
    private val earningsRepo: EarningsRepo,
    private val dispatchers: DispatcherProvider,
    private val resources: Resources
) : ViewModel(), ScreenViewModel {

    override val screenName: String = SCREEN_TITLE_EARNINGS_DETAILS

    private val compositeDisposable = CompositeDisposable()
    private val _marketEarningsQuarterly: MutableLiveData<Resource<MarketEarnings>> = MutableLiveData()
    private val _quarterlyGraphData: MutableLiveData<List<EarningsDetailData>> = MutableLiveData()
    private val _annualGraphData: MutableLiveData<Resource<List<EarningsDetailData>>> = MutableLiveData()
    private val _loadingStatus: MediatorLiveData<Boolean> = MediatorLiveData()
    private val annualDisplayCount: Int
        get() {
            return resources.getInteger(R.integer.annual_display_count)
        }
    private val quarterlyDisplayCount: Int
        get() {
            return resources.getInteger(R.integer.quarterly_display_count)
        }
    private val graphDataResources: GraphDataResources by lazy {
        GraphDataResources(
            R.color.purple,
            R.color.green,
            R.color.red,
            R.color.purple_opacity_50,
            R.color.light_black,
            resources.getString(R.string.market_graph_x_template)
        )
    }
    private val earningsDetailsResources: EarningDetailsResources by lazy {
        EarningDetailsResources(
            resources.getString(R.string.empty_default),
            resources.getString(R.string.estimate),
            resources.getString(R.string.high_est),
            resources.getString(R.string.actual),
            resources.getString(R.string.mean_est),
            resources.getString(R.string.surprise),
            resources.getString(R.string.low_est),
            R.color.light_black,
            R.color.red,
            R.color.green,
            resources.getString(R.string.quarters_period_template),
            resources.getString(R.string.earnings_selected_quarter_description)
        )
    }
    private val _selectedGraphIndex: MutableLiveData<Int> = MutableLiveData()
    private val quarterlyGraphData: LiveData<List<GraphData>> = Transformations.map(_quarterlyGraphData) {
        val graphList = getSafeDisplayList(it, quarterlyDisplayCount)
        mapMarketPeriodToGraphData(graphList, it.size / 2)
    }

    private val annualGraphData: LiveData<List<GraphData>> = Transformations.map(Transformations.map(_annualGraphData) { it.data ?: emptyList() }) {
        val graphList = getSafeDisplayList(it, annualDisplayCount)
        mapMarketPeriodToGraphData(graphList, it.size / 2)
    }

    val marketEarningsQuarterly: LiveData<Resource<MarketEarnings>> = _marketEarningsQuarterly
    val selectedGraphIndex: LiveData<Int> = _selectedGraphIndex
    val selectedGraphData: LiveData<List<GraphData>> = Transformations.switchMap(_selectedGraphIndex) {
        when (it) {
            QUARTERLY_GRAPH_INDEX -> quarterlyGraphData
            else -> annualGraphData
        }
    }
    val loadingStatus: LiveData<Boolean> = _loadingStatus
    val marketEarningsAnnual: LiveData<Resource<List<EarningsDetailData>>> = _annualGraphData

    var graphIndex: Int
        get() = _selectedGraphIndex.value ?: QUARTERLY_GRAPH_INDEX
        set(value) {
            _selectedGraphIndex.value = value
        }
    var selectedGraphBarIndex: MutableLiveData<Int> = MutableLiveData()

    val selectedQuarterEarningsDetails: LiveData<EarningDetails?> = selectedGraphBarIndex.map { index ->
        quarterlyGraphData.value?.let { createEarningDetails(index, it, earningsDetailsResources) }
    }

    var symbolTitle: String? = null

    init {
        _loadingStatus.addSource(_marketEarningsQuarterly) {
            _loadingStatus.postValue(it is Resource.Loading)
        }

        graphIndex = QUARTERLY_GRAPH_INDEX
    }

    fun getCalendarEvent(): CalendarEvent? {
        val marketEarnings: MarketEarnings? = _marketEarningsQuarterly.value?.data
        val title = symbolTitle

        return if (title != null) {
            marketEarnings
                ?.dateTimeForCalendar
                ?.atZone(ZoneId.systemDefault())
                ?.toInstant()
                ?.let { instant -> CalendarEvent(title, instant.toEpochMilli()) }
        } else {
            null
        }
    }

    fun loadGraphDetails(symbol: String, isInLandscapeMode: Boolean) {
        if (isInLandscapeMode) {
            when (graphIndex) {
                QUARTERLY_GRAPH_INDEX -> loadMarketEarningsQuarterly(symbol)
                else -> loadMarketEarningsAnnual(symbol)
            }
        } else {
            loadMarketEarningsQuarterly(symbol)
            loadMarketEarningsAnnual(symbol)
        }
    }

    fun getGraphDataItem(index: Int): GraphData? = selectedGraphData.value?.getOrNull(index)

    fun getHighlightItems(index: Int): Array<Highlight>? {
        return getGraphDataItem(index)
            ?.xFormatted
            ?.let { selectedGraphData.value?.filter { item -> item.xFormatted == it } }
            ?.map { Highlight(it.x, it.y, 0) }
            ?.let {
                // moving selected item to head of list, because barchart use first item from array as last selected item
                val selectedIndex = it.indexOfFirst { item -> item.x.toInt() == index }
                val item = it[selectedIndex]
                val list = it.toMutableList()

                list.removeAt(selectedIndex)
                list.add(0, item)

                list
            }
            ?.toTypedArray()
    }

    fun getLastHistoryQuarterIndex(): Int? {
        return selectedGraphData.value?.indexOfLast { !it.isFuturePeriod }
    }

    private fun getSafeDisplayList(it: List<EarningsDetailData>, displayCount: Int): List<EarningsDetailData> {
        return if (it.isNotEmpty()) {
            val safeDisplayCount = if (it.size < displayCount) {
                it.size
            } else {
                displayCount
            }

            it.subList(0, safeDisplayCount)
        } else {

            emptyList()
        }
    }

    private fun loadMarketEarningsQuarterly(symbol: String) {
        _marketEarningsQuarterly.value = Resource.Loading()
        viewModelScope.launch(dispatchers.IO) {
            earningsRepo.loadEarnings(symbol, PERIOD_QUARTERLY, resources.getInteger(R.integer.quarterly_period_count)).fold(
                onSuccess = {
                    it.earningsDetailData
                        ?.run {
                            _quarterlyGraphData.postValue(this)
                            _marketEarningsQuarterly.postValue(Resource.Success(createMarketEarnings(this, resources)))
                        } ?: _marketEarningsQuarterly.postValue(Resource.Success(createMarketEarnings(emptyList(), resources)))
                },
                onFailure = {
                    _marketEarningsQuarterly.postValue(Resource.Failed(createMarketEarnings(emptyList(), resources)))
                }
            )
        }
    }

    private fun loadMarketEarningsAnnual(symbol: String) {
        viewModelScope.launch(dispatchers.IO) {
            earningsRepo.loadEarnings(symbol, PERIOD_ANNUAL, resources.getInteger(R.integer.annual_period_count)).fold(
                onSuccess = {
                    it.earningsDetailData
                        ?.run {
                            _annualGraphData.postValue(Resource.Success(this))
                        }
                },
                onFailure = {
                    _annualGraphData.postValue(Resource.Failed(emptyList()))
                }
            )
        }
    }

    private fun mapMarketPeriodToGraphData(items: List<EarningsDetailData>, halfSize: Int): List<GraphData> {
        return items
            .mapIndexed { index, item ->
                val isFuturePeriod: Boolean = index >= halfSize
                createGraphData(index, item, isFuturePeriod, graphDataResources)
            }
    }

    override fun onCleared() {
        compositeDisposable.clear()

        super.onCleared()
    }
}
