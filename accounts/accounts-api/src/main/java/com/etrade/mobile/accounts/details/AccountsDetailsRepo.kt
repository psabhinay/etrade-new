package com.etrade.mobile.accounts.details

interface AccountsDetailsRepo {
    suspend fun getAccountsDetailedList(consumerKey: String): AccountsDetailedList
}
