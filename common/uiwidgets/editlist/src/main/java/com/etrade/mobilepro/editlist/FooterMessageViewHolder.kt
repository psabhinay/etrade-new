package com.etrade.mobilepro.editlist

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.editlist.databinding.ItemEditListBottomMessageBinding

class FooterMessageViewHolder(private val binding: ItemEditListBottomMessageBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(message: Int) {
        binding.message.setText(message)
    }
}
