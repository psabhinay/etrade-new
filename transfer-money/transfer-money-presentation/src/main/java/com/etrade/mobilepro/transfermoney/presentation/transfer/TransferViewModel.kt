package com.etrade.mobilepro.transfermoney.presentation.transfer

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.calendar.MIDDLE_OF_CALENDAR_RANGE
import com.etrade.mobilepro.common.Restorable
import com.etrade.mobilepro.common.RestorableViewModelDelegate
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.etrade.mobilepro.transfermoney.api.TransferRepeat
import com.etrade.mobilepro.transfermoney.api.repo.TransferMoneyRepo
import com.etrade.mobilepro.transfermoney.api.repo.TransferRepeatDataSource
import com.etrade.mobilepro.transfermoney.presentation.common.getTitleId
import com.etrade.mobilepro.transfermoney.presentation.preview.ParcelableGetQuickTransferPreviewRequest
import com.etrade.mobilepro.transfermoney.presentation.preview.TransferError
import com.etrade.mobilepro.transfermoney.presentation.transfer.source.TransferSourceAdapter
import com.etrade.mobilepro.transfermoney.presentation.transfer.source.TransferSourceFilterer
import com.etrade.mobilepro.transfermoney.presentation.transfer.source.TransferSourceHolder
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.delegate.consumable
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import com.etrade.mobilepro.util.guard
import com.etrade.mobilepro.util.safeParseBigDecimal
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.threeten.bp.LocalDate
import java.math.BigDecimal
import java.math.RoundingMode

private val logger = LoggerFactory.getLogger(TransferViewModel::class.java)

class TransferViewModel @AssistedInject constructor(
    private val user: User,
    private val transferMoneyRepo: TransferMoneyRepo,
    private val transferRepeatDataSource: TransferRepeatDataSource,
    private val resources: Resources,
    @DeviceType private val deviceType: String,
    private val filterer: TransferSourceFilterer,
    transferSourceAdapter: TransferSourceAdapter,
    @Assisted("preSelectedFromAccountId") preSelectedFromAccountId: String?,
    @Assisted("preFilledAmount") private var preFilledAmount: String?,
    private val restorableDelegate: RestorableViewModelDelegate
) : ViewModel() {

    @AssistedFactory
    interface Factory {
        fun create(
            @Assisted("preSelectedFromAccountId") preSelectedFromAccountId: String?,
            @Assisted("preFilledAmount") preFilledAmount: String?
        ): TransferViewModel
    }

    private val _restoreTransferAmountSignal: MutableLiveData<ConsumableLiveEvent<BigDecimal>> = MutableLiveData()
    val restoreTransferAmountSignal: LiveData<ConsumableLiveEvent<BigDecimal>>
        get() = _restoreTransferAmountSignal

    private val _scrollUpSignal: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()
    val scrollUpSignal: LiveData<ConsumableLiveEvent<Unit>> get() = _scrollUpSignal

    private val _snackbarMessage: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()
    val snackbarMessage: LiveData<ConsumableLiveEvent<Unit>>
        get() = _snackbarMessage

    private val _popupErrorMessage: MutableLiveData<ConsumableLiveEvent<TransferError>> = MutableLiveData()
    val popupErrorMessage: LiveData<ConsumableLiveEvent<TransferError>>
        get() = _popupErrorMessage

    // Consumable as to not re-select the account when a refresh is triggered.
    private val accountIdToPreselect: String? by consumable(preSelectedFromAccountId)

    private val _viewState = MediatorLiveData<ViewState>().apply {
        value = restorableDelegate.getRestoredState(this@TransferViewModel::class.java) as? ViewState ?: ViewState.Loading
    }

    private var savedViewState: ViewState? = null

    internal val viewState: LiveData<ViewState>
        get() = _viewState

    internal val transferRepeatData: LiveData<List<DropDownMultiSelector.Selectable<TransferRepeat>>> = Transformations.map(viewState) {
        (it as? ViewState.Success)?.let {
            runBlocking {
                transferRepeatDataSource.getTransferRepeats().map { transferRepeat ->
                    DropDownMultiSelector.Selectable(
                        title = resources.getString(transferRepeat.getTitleId()),
                        value = transferRepeat,
                        selected = transferRepeat == it.selectedRepeat
                    )
                }
            }
        }
    }

    internal val transferSource: TransferSourceAdapter.SelectableTransferSource =
        transferSourceAdapter.adaptTransferSourceHolderToSelectable(
            Transformations.map(viewState) {
                it as? ViewState.Success ?: ViewState.Success()
            }
        ) { holder ->
            _viewState.updateValue {
                if (it is ViewState.Success) {
                    it.copy(
                        accounts = holder.accounts,
                        selectedFrom = holder.selectedFrom,
                        selectedTo = holder.selectedTo
                    )
                } else {
                    it
                }
            }
        }

    val selectedScheduleDate: LocalDate?
        get() = (_viewState.value as? ViewState.Success)?.selectedScheduleDate

    val isFromAccountExternal: Boolean
        get() = (_viewState.value as? ViewState.Success)?.selectedFrom?.isExternalAccount ?: false

    val isToAccountExternal: Boolean
        get() = (_viewState.value as? ViewState.Success)?.selectedTo?.isExternalAccount ?: false

    var currentCalendarPage: Int = MIDDLE_OF_CALENDAR_RANGE

    val previewState: LiveData<TransferMoneyPreviewButtonState> = _viewState.map {
        if (it is ViewState.Success && it.readyForPreview()) {
            TransferMoneyPreviewButtonState.Enabled(
                ParcelableGetQuickTransferPreviewRequest(
                    platform = deviceType,
                    userId = requireNotNull(user.getUserId()).toString(),
                    amount = requireNotNull(it.amount.toString()),
                    scheduleDate = requireNotNull(it.selectedScheduleDate),
                    fromAccount = requireNotNull(it.selectedFrom),
                    toAccount = requireNotNull(it.selectedTo),
                    frequency = requireNotNull(it.selectedRepeat)
                )
            )
        } else {
            TransferMoneyPreviewButtonState.Disabled
        }
    }

    private val _dateSelectorEnableState = MediatorLiveData<Boolean>().apply {
        value = false

        addSource(_viewState) {
            value = it is ViewState.Success && it.readyForDateSelection()
        }
    }
    val dateSelectorEnableState: LiveData<Boolean>
        get() = _dateSelectorEnableState

    init {
        restorableDelegate.init(this::class.java, viewState, ::restoreState)
        _viewState.value = restorableDelegate.getRestoredState(this::class.java) as? ViewState ?: ViewState.Loading
        refreshAccounts()
    }

    internal fun updatePreSelectedAmountIfNeeded(amount: String?) {
        if (preFilledAmount == null && !amount.isNullOrEmpty()) {
            val currentState = _viewState.value
            if (currentState is ViewState.Success) {
                _viewState.value = currentState.copy(amount = amount.safeParseBigDecimal())
            } else {
                preFilledAmount = amount
                refreshAccounts()
            }
        }
    }

    internal fun refreshAccounts() {
        _viewState.value = ViewState.Loading
        viewModelScope.launch {
            transferMoneyRepo.getAccounts(userId = user.getUserId().toString()).fold(
                onSuccess = { retrievedAccounts ->
                    val amount = preFilledAmount?.safeParseBigDecimal()?.setScale(2, RoundingMode.HALF_EVEN)
                    amount?.let { preFilledAmount = null }
                    val viewState = ViewState.Success(accounts = retrievedAccounts, amount = amount)
                    val fromAccounts = filterer.filterFromAccounts(viewState.copyAsHolder())
                    _viewState.value = selectFromAccount(accountIdToPreselect, fromAccounts)?.let {
                        viewState.copy(selectedFrom = it)
                    } ?: viewState

                    (savedViewState as? ViewState.Success).let { it ->
                        it?.amount?.let { amount -> amountEntered(amount) }
                        it?.selectedFrom?.let { fromAccount -> transferFromSelected(fromAccount) }
                        it?.selectedTo?.let { toAccount -> transferToSelected(toAccount) }
                        it?.selectedRepeat?.let { frequency -> transferRepeatSelected(frequency) }
                        it?.selectedScheduleDate?.let { scheduleDate -> updateSelectedDate(scheduleDate) }
                    }

                    savedViewState = null
                },
                onFailure = {
                    logger.error(it.localizedMessage)
                    _viewState.postValue(ViewState.Error)
                }
            )
        }
    }

    internal fun hasFromAccounts(): Boolean =
        (viewState.value as? ViewState.Success)?.run {
            filterer.filterFromAccounts(this.copyAsHolder()).isNotEmpty()
        } ?: false

    internal fun hasToAccounts(): Boolean =
        (viewState.value as? ViewState.Success)?.run {
            filterer.filterToAccounts(this.copyAsHolder()).isNotEmpty()
        } ?: false

    internal fun transferRepeatSelected(selectedRepeat: TransferRepeat) {
        (_viewState.value as? ViewState.Success)?.let {
            _viewState.value = it.copy(selectedRepeat = selectedRepeat)
        }
    }

    internal fun transferFromSelected(selectedFrom: TransferMoneyAccount) {
        (_viewState.value as? ViewState.Success)?.let {
            if (selectedFrom.isExternalAccount) {
                _viewState.value = it.copy(selectedFrom = selectedFrom, selectedScheduleDate = null)
            } else {
                _viewState.value = it.copy(selectedFrom = selectedFrom)
            }
        }
    }

    internal fun transferToSelected(selectedTo: TransferMoneyAccount) {
        (_viewState.value as? ViewState.Success)?.let {
            if (selectedTo.isExternalAccount) {
                _viewState.value = it.copy(selectedTo = selectedTo, selectedScheduleDate = null)
            } else {
                _viewState.value = it.copy(selectedTo = selectedTo)
            }
        }
    }

    internal fun amountEntered(amount: BigDecimal) {
        (_viewState.value as? ViewState.Success)?.let {
            _viewState.value = it.copy(amount = amount)
        }
    }

    internal fun updateSelectedDate(date: LocalDate?) {
        (_viewState.value as? ViewState.Success)?.let {
            _viewState.value = it.copy(selectedScheduleDate = date)
        }
    }

    internal fun resetTransferMoneyData() {
        (_viewState.value as? ViewState.Success)?.let {
            _viewState.value = ViewState.Success(accounts = it.accounts)
            _restoreTransferAmountSignal.value = ConsumableLiveEvent(BigDecimal.ZERO)
            _scrollUpSignal.value = ConsumableLiveEvent(Unit)
        }
    }

    internal fun showPopupErrorMessage(error: TransferError) {
        _popupErrorMessage.value = ConsumableLiveEvent(error)
    }

    internal fun showSnackbarMessage() {
        _snackbarMessage.value = ConsumableLiveEvent(Unit)
    }

    internal fun hasEmptySuccessState(): Boolean {
        val state = viewState.value as? ViewState.Success guard {
            return true
        }
        return state.selectedRepeat == TransferRepeat.None && state.selectedScheduleDate == null &&
            state.selectedFrom == null && state.selectedTo == null
    }

    internal fun getAmount(): BigDecimal? = (_viewState.value as? ViewState.Success)?.amount

    fun disposeState() {
        restorableDelegate.dispose(this::class.java)
    }

    private fun restoreState(restoredState: Restorable) {
        if (restoredState is ViewState) {
            restoredState.apply {
                _viewState.value = this

                (this as? ViewState.Success)?.amount?.let {
                    _restoreTransferAmountSignal.value = ConsumableLiveEvent(it)
                }

                savedViewState = this
            }
        }
    }

    internal sealed class ViewState : Restorable {
        object Loading : ViewState()
        data class Success(
            override val accounts: List<TransferMoneyAccount> = emptyList(),
            val selectedRepeat: TransferRepeat = TransferRepeat.None,
            override val selectedFrom: TransferMoneyAccount? = null,
            override val selectedTo: TransferMoneyAccount? = null,
            val selectedScheduleDate: LocalDate? = null,
            val amount: BigDecimal? = null
        ) : ViewState(), TransferSourceHolder {
            fun iraAccountSelected(): Boolean = selectedTo?.isIraAccount == true

            fun formattedSelectedScheduleDate(): String? = selectedScheduleDate?.format(createAmericanSimpleDateFormatter())

            fun readyForPreview(): Boolean = selectedFrom != null && selectedTo != null && selectedScheduleDate != null &&
                amount ?: BigDecimal.ZERO > BigDecimal.ZERO

            fun readyForDateSelection(): Boolean = selectedFrom != null && selectedTo != null

            fun isToSelectedEnabled(): Boolean = selectedFrom != null
        }

        object Error : ViewState()
    }
}
