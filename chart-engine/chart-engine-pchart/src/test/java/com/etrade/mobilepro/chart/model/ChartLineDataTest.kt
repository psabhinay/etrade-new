package com.etrade.mobilepro.chart.model

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.chart.util.getChartLineDataWithDisplayDto
import com.etrade.mobilepro.chartengine.ChartOHLCData
import com.etrade.mobilepro.chartengine.SymbolChartData
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.mock

@RunWith(AndroidJUnit4::class)
class ChartLineDataTest {

    private lateinit var fakeColorResolver: ColorResolver

    @Before
    fun initColorResolver() {
        fakeColorResolver = mock(ColorResolver::class.java)
        Mockito.`when`(fakeColorResolver.getColorInt(Mockito.anyInt())).thenAnswer { 0 }
    }

    @Test
    fun `verify one day chart add lineSets`() {
        val chartLineData = ChartLineData(ChartTimeFrame.Day, true)
        // Add fake DJIND chart line set

        val dowLineSet = getChartLineDataWithDisplayDto("DJIND", 0, getDowChartDataForDay(), ChartTimeFrame.Day)
        chartLineData.addLineSet(dowLineSet)
        assertEquals("DJIND", dowLineSet.label)
        assertEquals(1550241540000, dowLineSet.highestTimeInMillis)
        assertEquals(3, dowLineSet.getLineSet().entryCount)
        assertEquals(1, chartLineData.getLineData(fakeColorResolver).dataSetCount)

        // Add fake NASDAQ chart line set
        val nasdaqLineSet = getChartLineDataWithDisplayDto("COMP.IDX", 0, getNasdaqChartDataForDay(), ChartTimeFrame.Day)
        chartLineData.addLineSet(nasdaqLineSet)
        assertEquals("COMP.IDX", nasdaqLineSet.label)
        assertEquals(1550241540000, nasdaqLineSet.highestTimeInMillis)
        assertEquals(3, dowLineSet.getLineSet().entryCount)
        assertEquals(2, chartLineData.getLineData(fakeColorResolver).dataSetCount)

        // final configuration checks after two lines added
        val configuration = chartLineData.getConfiguration()
        assertEquals(ChartTimeFrame.Day, configuration.timeFrame)
        assertEquals(-8.0, configuration.yAxisMin, 0.01)
        assertEquals(5.0, configuration.yAxisMax, 0.01)
        assertEquals(1550241540000, configuration.maxTimeInMillis)
    }

    @Test
    fun `verify weekly chart add lineSets`() {
        val chartLineData = ChartLineData(ChartTimeFrame.OneWeek, true)
        // Add fake DJIND chart line set
        val dowLineSet = getChartLineDataWithDisplayDto("DJIND", 0, getDowChartDataForWeekly(), ChartTimeFrame.OneWeek)
        chartLineData.addLineSet(dowLineSet)
        assertEquals("DJIND", dowLineSet.label)
        assertEquals(6, dowLineSet.getLineSet().entryCount)
        assertEquals(6, dowLineSet.highestTimeInMillis)
        assertEquals(6, dowLineSet.listOfLabels.size)
        assertEquals(1, chartLineData.getLineData(fakeColorResolver).dataSetCount)

        // Add fake NASDAQ chart line set
        val nasdaqLineSet = getChartLineDataWithDisplayDto("COMP.IDX", 0, getNasdaqChartDataForWeekly(), ChartTimeFrame.OneWeek)
        chartLineData.addLineSet(nasdaqLineSet)
        assertEquals("COMP.IDX", nasdaqLineSet.label)
        assertEquals(6, nasdaqLineSet.highestTimeInMillis)
        assertEquals(6, dowLineSet.getLineSet().entryCount)
        assertEquals(6, dowLineSet.listOfLabels.size)
        assertEquals(2, chartLineData.getLineData(fakeColorResolver).dataSetCount)

        // final configuration checks after two lines added
        val configuration = chartLineData.getConfiguration()
        assertEquals(ChartTimeFrame.OneWeek, configuration.timeFrame)
        assertEquals(-5.0, configuration.yAxisMin, 0.01)
        assertEquals(20.0, configuration.yAxisMax, 0.01)
        assertEquals(6, configuration.listOfLabels?.size)
        assertEquals(6, configuration.maxTimeInMillis)
    }

    // Since we're supporting only line charts right now, open, high, and low and volume are not used, we are only using closing prices of each point.

    private fun getDowChartDataForDay(): SymbolChartData {
        // three points of change percents -4%, -8%, +4%
        val mockChartOHLCDataList = listOf(
            ChartOHLCData("02/15/2019 09:33", 0.0, 0.0, 0.0, 24000.0, 0),
            ChartOHLCData("02/15/2019 09:36", 0.0, 0.0, 0.0, 23000.0, 0),
            ChartOHLCData("02/15/2019 09:39", 0.0, 0.0, 0.0, 26000.0, 0)
        )
        return SymbolChartData("", mockChartOHLCDataList, 25000.0, 0, 0)
    }

    private fun getNasdaqChartDataForDay(): SymbolChartData {
        // three data points with change percents -5%, 0%, +5%
        val mockChartOHLCDataList = listOf(
            ChartOHLCData("02/15/2019 09:33", 0.0, 0.0, 0.0, 6650.0, 0),
            ChartOHLCData("02/15/2019 09:36", 0.0, 0.0, 0.0, 7000.0, 0),
            ChartOHLCData("02/15/2019 09:39", 0.0, 0.0, 0.0, 7350.0, 0)
        )
        return SymbolChartData("", mockChartOHLCDataList, 7000.0, 0, 0)
    }

    private fun getDowChartDataForWeekly(): SymbolChartData {
        // week worth of data points, change % calculated from first items as base, 0%, 4%, 8%, 0%, 12%, 20%
        val mockChartOHLCDataList = listOf(
            ChartOHLCData("02/08/2019 09:45", 0.0, 0.0, 0.0, 25000.0, 0),
            ChartOHLCData("02/11/2019 09:45", 0.0, 0.0, 0.0, 26000.0, 0),
            ChartOHLCData("02/12/2019 09:45", 0.0, 0.0, 0.0, 27000.0, 0),
            ChartOHLCData("02/13/2019 09:45", 0.0, 0.0, 0.0, 25000.0, 0),
            ChartOHLCData("02/14/2019 09:45", 0.0, 0.0, 0.0, 28000.0, 0),
            ChartOHLCData("02/15/2019 09:45", 0.0, 0.0, 0.0, 30000.0, 0)
        )
        return SymbolChartData("", mockChartOHLCDataList, 0.0, 0, 0)
    }

    private fun getNasdaqChartDataForWeekly(): SymbolChartData {
        // week worth of data points, change % calculated from first item as base, 0%, -5%, 3%, 5%, 10%, 15%
        val mockChartOHLCDataList = listOf(
            ChartOHLCData("02/08/2019 09:45", 0.0, 0.0, 0.0, 7000.0, 0),
            ChartOHLCData("02/11/2019 09:45", 0.0, 0.0, 0.0, 6650.0, 0),
            ChartOHLCData("02/12/2019 09:45", 0.0, 0.0, 0.0, 7210.0, 0),
            ChartOHLCData("02/13/2019 09:45", 0.0, 0.0, 0.0, 7350.0, 0),
            ChartOHLCData("02/14/2019 09:45", 0.0, 0.0, 0.0, 7700.0, 0),
            ChartOHLCData("02/15/2019 09:45", 0.0, 0.0, 0.0, 8050.0, 0)
        )
        return SymbolChartData("", mockChartOHLCDataList, 0.0, 0, 0)
    }
}
