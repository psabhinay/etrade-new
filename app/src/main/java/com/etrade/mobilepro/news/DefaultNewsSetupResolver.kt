package com.etrade.mobilepro.news

import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.news.presentation.NewsContainerFragment
import com.etrade.mobilepro.news.presentation.NewsSetupResolver
import com.etrade.mobilepro.util.android.fragment.isChildOfBottomSheet
import javax.inject.Inject

class DefaultNewsSetupResolver @Inject constructor() : NewsSetupResolver {
    override fun showSearch(activity: FragmentActivity?, fragment: Fragment): Boolean {
        return activity is MainActivity && !fragment.isChildOfBottomSheet(fragment)
    }

    override fun hasOptionsMenu(detailsContainer: View?, parentFragment: Fragment?): Boolean {
        return detailsContainer != null || parentFragment !is NewsContainerFragment
    }

    override fun shouldHaveOptionsMenuWithDetails(
        isDualPane: Boolean,
        parentFragment: Fragment?
    ): Boolean {
        return isDualPane && parentFragment is NewsContainerFragment
    }

    override fun containerShouldInflateCommonMenu(activity: FragmentActivity?): Boolean =
        activity is MainActivity
}
