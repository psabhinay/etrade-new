package com.etrade.mobilepro.orders.extensions

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.OrderIdentifier
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode

fun extractOrderIdentifier(
    orderType: OrderType,
    advancedOrderType: AdvancedOrderType?,
    origSysCode: OrigSysCode,
    instrumentType: InstrumentType,
    isMultiLeg: Boolean
): OrderIdentifier {
    return when {
        orderType.isConditional() -> OrderIdentifier.CONDITIONAL

        advancedOrderType == AdvancedOrderType.AO_CONTINGENT ||
            (origSysCode == OrigSysCode.ORIG_SYS_CODE_OH && advancedOrderType == AdvancedOrderType.AO_CONTINGENT_CANCEL)
        -> OrderIdentifier.CONTINGENT

        AdvancedOrderType.bracketedOrderTypes.contains(advancedOrderType) -> OrderIdentifier.BRACKETED
        InstrumentType.EQ == instrumentType && !isMultiLeg -> OrderIdentifier.STOCK
        else -> OrderIdentifier.OPTION
    }
}
