package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.util.MutualConverter

object CommonOrderTermValueId : OrderTermValueId {
    override val extendedHoursDay: String = OrderTerm.EXTENDED_HOUR_DAY.typeCode
    override val extendedHoursImmediateOrCancel: String = OrderTerm.EXTENDED_HOUR_IOC.typeCode
    override val fillOrKill: String = OrderTerm.FILL_OR_KILL.typeCode
    override val goodForDay: String = OrderTerm.GOOD_FOR_DAY.typeCode
    override val goodUntilCancel: String = OrderTerm.GOOD_UNTIL_CANCEL.typeCode
    override val immediateOrCancel: String = OrderTerm.IMMEDIATE_OR_CANCEL.typeCode
    override val converter: MutualConverter<String, OrderTerm> by lazy { createConverter() }
}
