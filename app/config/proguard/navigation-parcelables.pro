# Needed for when using a Parcelable type as a navigation safeArg
# https://issuetracker.google.com/issues/122634648
-keepnames class * implements android.os.Parcelable

# The following enum classes are used as argType parameters in navigation graphs and should be kept
-keepnames class com.etrade.mobilepro.order.details.router.OrderConfirmationAction
-keepnames class com.etrade.mobilepro.orders.api.OrderStatus
-keepnames class com.etrade.mobilepro.orders.api.TradeOrderType
-keepnames class com.etrade.mobilepro.orders.api.TransactionType
-keepnames class com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
-keepnames class com.etrade.mobilepro.positions.presentation.PositionTableMode
-keepnames class com.etrade.mobilepro.quotes.MutualFundViewType
-keepnames class com.etrade.mobilepro.screener.api.ScreenerDisclosure
-keepnames class com.etrade.mobilepro.trade.api.AccountMode
-keepnames class com.etrade.mobilepro.trade.mutualfund.presentation.MutualFundTradeType
-keepnames class org.threeten.bp.LocalDate
