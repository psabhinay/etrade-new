package com.etrade.mobilepro.movers

import android.content.Context
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.navigation.getParentActivity
import com.etrade.mobilepro.navigation.getTabDestination
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import javax.inject.Inject

class DefaultMoversRouter @Inject constructor(
    private val mainNavigation: MainNavigation,
) : MoversRouter {
    override fun navigateToQuoteDetails(view: View, symbol: String, instrumentType: InstrumentType) {
        navigateToQuoteDetails(view.getParentActivity(), symbol, instrumentType)
    }

    override fun navigateToQuoteDetails(
        context: Context,
        symbol: String,
        instrumentType: InstrumentType
    ) {
        navigateToQuoteDetails(context.getParentActivity(), symbol, instrumentType)
    }

    override fun getTabDest(ctaString: String): CtaEvent? = getTabDestination(ctaString)

    private fun navigateToQuoteDetails(
        activity: AppCompatActivity?,
        symbol: String,
        instrumentType: InstrumentType
    ) {
        mainNavigation.navigateToQuoteDetails(
            activity,
            SearchResultItem.Symbol(title = symbol, type = instrumentType).toBundle()
        )
    }
}
