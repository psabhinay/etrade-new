package com.etrade.mobilepro.trade.presentation.form.component.view

import android.content.Context
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.ui.ComponentView
import com.etrade.mobilepro.dynamic.form.ui.ComponentViewDelegate
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.component.FooterComponent
import com.etrade.mobilepro.trade.presentation.form.component.FooterLink

internal class FooterComponentView(context: Context) : LinearLayout(context), ComponentView<FooterComponent> {

    private val componentViewDelegate: ComponentViewDelegate = ComponentViewDelegate(this)

    private val linkObserver: Observer<List<FooterLink>> = Observer {
        removeAllViews()

        val links = it.takeIf { it.isNotEmpty() } ?: return@Observer

        addView(createTextView(links.iterator()))
    }

    private val linkVerticalPadding: Int = context.resources.getDimensionPixelSize(R.dimen.spacing_small)

    private var footerComponent: FooterComponent? = null

    init {
        gravity = Gravity.CENTER_HORIZONTAL
        orientation = HORIZONTAL
        showDividers = SHOW_DIVIDER_MIDDLE
        dividerDrawable = ContextCompat.getDrawable(context, R.drawable.divider_spacing_xsmall)
    }

    override fun render(component: FooterComponent, owner: LifecycleOwner) {
        componentViewDelegate.render(component, owner)

        footerComponent?.links?.removeObserver(linkObserver)

        footerComponent = component

        component.links.observe(owner, linkObserver)
    }

    private fun createTextView(iterator: Iterator<FooterLink>): TextView {
        val spannable = SpannableStringBuilder()
        iterator.forEach { link ->
            if (spannable.isNotEmpty()) {
                spannable.append(" - ")
            }
            spannable.append(link.title)
            spannable.setSpan(
                object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        link.action()
                    }

                    override fun updateDrawState(textPaint: TextPaint) {
                        super.updateDrawState(textPaint)
                        textPaint.isUnderlineText = false
                    }
                },
                spannable.length - link.title.length,
                spannable.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }

        return TextView(context).apply {
            layoutParams = LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            gravity = Gravity.CENTER_HORIZONTAL
            TextViewCompat.setTextAppearance(this, R.style.TextAppearance_Etrade_Text_Regular_Medium_Link)
            setPadding(0, linkVerticalPadding, 0, linkVerticalPadding)
            movementMethod = LinkMovementMethod.getInstance()
            text = spannable
            importantForAccessibility = IMPORTANT_FOR_ACCESSIBILITY_YES
        }
    }
}
