package com.etrade.mobilepro.marketscalendar.api.model

class EventsSummaryResponse(
    val eventsSummaryResponseData: EventsSummaryResponseData?
) {
    data class EventsSummaryResponseData(
        val eventsSummaryResponseDm: EventsSummaryResponseDm?
    )

    data class EventsSummaryResponseDm(
        val eventsSummaryList: List<EventsSummary>?
    )

    data class EventsSummary(
        val dateTime: String?
    )
}
