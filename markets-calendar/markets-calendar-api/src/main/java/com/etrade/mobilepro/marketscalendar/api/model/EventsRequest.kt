package com.etrade.mobilepro.marketscalendar.api.model
import org.threeten.bp.LocalDate

class EventsRequest(
    val eventsRequestValue: EventsRequestValue
) {
    data class EventsRequestValue(
        val eventsRequestDateRange: EventsRequestDateRange,
        val symbolSet: List<String>,
        val eventFilterEnable: String?,
        val eventTypeSet: List<String>
    )

    data class EventsRequestDateRange(
        val startDate: LocalDate,
        val endDate: LocalDate
    )
}
