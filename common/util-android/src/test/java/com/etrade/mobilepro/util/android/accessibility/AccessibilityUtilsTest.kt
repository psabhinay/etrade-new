package com.etrade.mobilepro.util.android.accessibility

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.emptyString
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class AccessibilityUtilsTest {

    lateinit var context: Context

    private val resources
        get() = context.resources

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
    }

    @Test
    fun accountNameContentDescription_nullInput() {
        assertThat(
            context.accountNameContentDescription(null),
            nullValue()
        )
    }

    @Test
    fun accountNameContentDescription_doesNotMatch() {
        assertThat(
            context.accountNameContentDescription("a random account name"),
            `is`(equalTo("a random account name"))
        )
    }

    @Test
    fun accountNameContentDescription_matchesNoPrefix() {
        assertThat(
            context.accountNameContentDescription("-9876 $85000"),
            `is`(equalTo(" dash 9 8 7 6 $85000"))
        )
    }

    @Test
    fun accountNameContentDescription_matchesNoSuffix() {
        assertThat(
            context.accountNameContentDescription("Checking -9876"),
            `is`(equalTo("Checking dash 9 8 7 6"))
        )
    }

    @Test
    fun accountNameContentDescription_matchesAll() {
        assertThat(
            context.accountNameContentDescription("Checking -9876 $1000.00"),
            `is`(equalTo("Checking dash 9 8 7 6 $1000.00"))
        )
    }

    @Test
    fun accountNameContentDescription_matchesWithSpaces() {
        assertThat(
            context.accountNameContentDescription("Checking 1234 - 9876 $1000.00"),
            `is`(equalTo("Checking 1 2 3 4 dash 9 8 7 6 $1000.00"))
        )
    }

    @Test
    fun accountNameContentDescription_matchesLongAccountNumber() {
        assertThat(
            context.accountNameContentDescription("Checking 12345678-9876 $1000.00"),
            `is`(equalTo("Checking 1 2 3 4 5 6 7 8 dash 9 8 7 6 $1000.00"))
        )
    }

    @Test
    fun listDescription_emptyList() {
        assertThat(
            emptyList<String>().listDescription(resources),
            `is`(emptyString())
        )
    }

    @Test
    fun listDescription_singleItem() {
        assertThat(
            listOf("first").listDescription(resources),
            `is`(equalTo("Item first"))
        )
    }

    @Test
    fun listDescription_twoItems() {
        assertThat(
            listOf("first", "second").listDescription(resources),
            `is`(equalTo("Items first and second"))
        )
    }

    @Test
    fun listDescription_threeItems() {
        assertThat(
            listOf("first", "second", "third").listDescription(resources),
            `is`(equalTo("Items first, second and third"))
        )
    }

    @Test
    fun listDescription_manyItems() {
        println("testing listDescription_manyItems")
        assertThat(
            listOf(
                "first",
                "second",
                "third",
                "fourth",
                "fifth",
                "sixth"
            ).listDescription(resources),
            `is`(equalTo("Items first, second, third, fourth, fifth and sixth"))
        )
    }
}
