package com.etrade.mobilepro.billpay.presentation.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.presentation.paymentdetail.ParcelablePayment
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

internal class NavigationViewModel : ViewModel() {

    private val _paymentPreviewCleared: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()
    val paymentPreviewCleared: LiveData<ConsumableLiveEvent<Unit>> get() = _paymentPreviewCleared

    private val _refreshPayments: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()
    val refreshPayments: LiveData<ConsumableLiveEvent<Unit>> get() = _refreshPayments

    private val _paymentScheduled: MutableLiveData<ConsumableLiveEvent<ParcelablePayment>> = MutableLiveData()
    val paymentScheduled: LiveData<ConsumableLiveEvent<ParcelablePayment>> get() = _paymentScheduled

    private val _refreshPayees: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()
    val refreshPayees: LiveData<ConsumableLiveEvent<Unit>> get() = _refreshPayees

    private val _addedPayee: MutableLiveData<ConsumableLiveEvent<Payee>> = MutableLiveData()
    val addedPayee: LiveData<ConsumableLiveEvent<Payee>> get() = _addedPayee

    internal fun clearPaymentPreview() {
        _paymentPreviewCleared.value = ConsumableLiveEvent(Unit)
    }

    internal fun refreshPayments() {
        _refreshPayments.value = ConsumableLiveEvent(Unit)
    }

    internal fun paymentScheduled(payment: ParcelablePayment) {
        _paymentScheduled.value = ConsumableLiveEvent(payment)
    }

    internal fun refreshPayeeList() {
        _refreshPayees.value = ConsumableLiveEvent(Unit)
    }

    internal fun payeeAdded(newPayee: Payee) {
        _addedPayee.value = ConsumableLiveEvent(newPayee)
    }
}
