package com.etrade.mobilepro.trade.help

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.getToolbarTitleContentDescription
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.trade.api.TradeOptionInfoType
import com.etrade.mobilepro.trade.help.databinding.TradeHelpFragmentBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import javax.inject.Inject

const val ARGUMENT_KEY_TRADE_OPTION_INFO_TYPE = "tradeOptionInfoType"
const val ARGUMENT_KEY_LEARN_OTHER_PRICE_TYPES = "learnOtherPriceTypes"
const val ARGUMENT_KEY_SKIP_TITLE_UPDATE = "skipTitleUpdate"

class TradeHelpFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory
) : Fragment(R.layout.trade_help_fragment) {

    private val binding by viewBinding(TradeHelpFragmentBinding::bind)
    private val viewModel: TradeHelpViewModel by viewModels { viewModelFactory }
    private val helpListAdapter by lazy { TradeHelpListAdapter() }
    private val tradeOptionInfoType: TradeOptionInfoType
        get() = requireNotNull(requireArguments().get(ARGUMENT_KEY_TRADE_OPTION_INFO_TYPE)) as TradeOptionInfoType
    private val learnOtherPriceTypes: Boolean
        get() = requireNotNull(requireArguments().getBoolean(ARGUMENT_KEY_LEARN_OTHER_PRICE_TYPES))
    private val skipTitleUpdate: Boolean
        get() = requireNotNull(requireArguments().getBoolean(ARGUMENT_KEY_SKIP_TITLE_UPDATE))
    private var pageTitle: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState == null) {
            viewModel.setInformationType(tradeOptionInfoType, learnOtherPriceTypes)
        }

        binding.informationList.apply {
            adapter = helpListAdapter
            setupRecyclerViewDivider(R.drawable.thin_divider, countOfLastItemsWithoutDivider = 0)
        }

        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel.title.observe(
            viewLifecycleOwner,
            {
                pageTitle = it
            }
        )

        viewModel.listItems.observe(
            viewLifecycleOwner,
            {
                helpListAdapter.items = it
            }
        )
    }

    fun setupToolBarTitle(title: String) {
        if (skipTitleUpdate) { return }
        toolbarTitleView?.text = title
        toolbarTitleView?.contentDescription = getToolbarTitleContentDescription(title, requireContext())
    }

    override fun onResume() {
        super.onResume()
        pageTitle?.let { setupToolBarTitle(it) }
    }
}
