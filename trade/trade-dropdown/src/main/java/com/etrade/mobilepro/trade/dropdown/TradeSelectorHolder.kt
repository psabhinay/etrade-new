package com.etrade.mobilepro.trade.dropdown

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.dropdown.DropDownItem
import com.etrade.mobilepro.trade.dropdown.help.TradeHelpParams

data class TradeSelectorHolder(
    val title: String,
    val dropDownItems: List<DropDownItem>,
    val helpParams: TradeHelpParams? = null,
    var listener: ((Int) -> Unit)? = null
) {
    private val _selectedPosition = MutableLiveData<Int?>()
    val selectedPosition: LiveData<Int?>
        get() = _selectedPosition

    fun updateSelectedPosition(position: Int?) {
        _selectedPosition.postValue(position)
    }
}
