package com.etrade.mobilepro.optionschain.tableview.adapter

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.optionschain.tableview.RowHeightCache

abstract class AbstractOptionChainAdapter<Data, VH : RecyclerView.ViewHolder>(
    context: Context
) : RecyclerView.Adapter<VH>() {

    private var rowHeightCache: RowHeightCache? = null

    private val defaultRowWidth: Int = context.resources.getDimensionPixelSize(com.etrade.mobilepro.optionschain.tableview.R.dimen.options_chain_item_width)

    private var parentRecyclerView: RecyclerView? = null

    abstract fun VH.bind(position: Int)

    abstract fun VH.bindWithData(data: Data)

    override fun onBindViewHolder(holder: VH, position: Int) {
        rowHeightCache?.let { controller ->
            holder.itemView.layoutParams.height = controller.getRowHeight(position)
        }
        holder.bind(position)
    }

    fun setRowHeightCache(cache: RowHeightCache?) {
        rowHeightCache = cache
    }

    fun measureRowHeights(items: Iterable<Data>) {
        parentRecyclerView?.let {
            onCreateViewHolder(it, 0).apply {
                items.forEachIndexed { index, data ->
                    bindWithData(data)
                    val measuredHeight = measureHolderHeight()
                    rowHeightCache?.updateRowHeight(measuredHeight, index)
                }
            }
        }
    }

    private fun VH.measureHolderHeight(): Int {
        itemView.measure(
            View.MeasureSpec.makeMeasureSpec(defaultRowWidth, View.MeasureSpec.EXACTLY),
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        return itemView.measuredHeight
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        parentRecyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        parentRecyclerView = null
        super.onDetachedFromRecyclerView(recyclerView)
    }
}
