package com.etrade.mobilepro.tracking.params

private const val EVENT_NAME = "Quote Lookup"
private const val EVENT_ATTRIBUTE_LOCATION = "Location"

class QuoteLookupAnalyticsParams(
    private val location: String
) : AnalyticsParams {

    override val name: String = EVENT_NAME

    override val params: Map<String, String> = mapOf(
        EVENT_ATTRIBUTE_LOCATION to location
    )
}
