package com.etrade.mobilepro.watchlistapi.repo

import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import io.reactivex.Single

/**
 * Repository provides access to device watch lists and user watch lists
 */

interface WatchlistRepo {

    /**
     * @returns a list of user's watch lists
     */
    fun getUserWatchlists(): Single<List<Watchlist>>

    /**
     * @return a list of watchlist entries with watchlist id
     */
    fun getWatchlistEntries(watchlistId: String, isExtendedHours: Boolean = false): Single<List<WatchlistEntry>>

    /**
     * Gets a list of symbols for a specific watchlist.
     *
     * @param watchlistId the watchlist id
     *
     * @return a list of watchlist symbols
     */
    fun getWatchlistSymbols(watchlistId: String): Single<List<String>>

    /**
     * Adds a symbol to a watchlist.
     *
     * @param watchlistId watchlist ID
     * @param ticker symbol to add
     *
     * @return `true` if successful, `false` otherwise
     */
    fun addToWatchList(watchlistId: String, ticker: String): Single<Boolean>

    /**
     * Saves a watchlist to a repo.
     *
     * @param watchlistId watchlist ID
     * @param entries watchlist entries (ordered list) to save
     * @param removedEntries list of watchlist entries removed from a watchlist
     *
     * @return `true` if successful, `false` otherwise
     */
    fun saveWatchList(watchlistId: String, entries: List<WatchlistEntry>, removedEntries: List<WatchlistEntry>): Single<Boolean>

    /**
     * Creates a watchlist.
     *
     * @param watchlistName watchlist name
     *
     * @return newly created watchlist
     */
    fun createWatchlist(watchlistName: String): Single<Watchlist>

    fun deleteWatchList(watchlistId: String): Single<Boolean>

    fun getAllWatchlistsEntries(isExtendedHours: Boolean = false): Single<List<WatchlistEntry>>
}
