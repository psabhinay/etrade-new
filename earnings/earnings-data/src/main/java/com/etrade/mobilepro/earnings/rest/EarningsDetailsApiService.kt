package com.etrade.mobilepro.earnings.rest

import com.etrade.mobilepro.earnings.dto.EarningsDetailResponse
import com.etrade.mobilepro.earnings.dto.EarningsDetailsRequest
import com.etrade.mobilepro.earnings.dto.EarningsResponse
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

const val PERIOD_QUARTERLY = "QTR"
const val PERIOD_ANNUAL = "ANN"

internal interface EarningsDetailsApiService {

    @JsonMoshi
    @GET("/v1/market/earnings/{symbol}.json")
    suspend fun getMarketEarning(
        @Header("DataToken") consumerKey: String?,
        @Path("symbol") symbol: String,
        @Query("periodType") periodType: String,
        @Query("periodCount") periodCount: Int
    ): EarningsResponse

    /**
     * Note: This endpoint is not fully implemented yet. It only works for quarterly period type
     * requests.
     *
     * It'll be addressed in [https://jira.corp.etradegrp.com/browse/ETAND-19863].
     */
    @JsonMoshi
    @POST("/phx/market/etm/services/v2/market/earnings")
    suspend fun getMarketQuarterlyEarning(@JsonMoshi @Body requestBody: EarningsDetailsRequest): EarningsDetailResponse
}
