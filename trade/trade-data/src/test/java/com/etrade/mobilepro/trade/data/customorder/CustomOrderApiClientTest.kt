package com.etrade.mobilepro.trade.data.customorder

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.testutil.getObjectFromJson
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class CustomOrderApiClientTest {

    @Test
    fun `test Custom Order Strategy api call`() {
        val response: ServerResponseDto<CustomOrderStrategyResponseBodyDto> = getCustomOrderStrategyResponseBodyDto()
        Assertions.assertNotNull(response.data)
        Assertions.assertNotNull(response.data?.content)
        Assertions.assertNotNull(response.data?.content?.legRequestList)
    }

    @Test
    fun `test Custom Order Preview api call`() {
        val response: ServerResponseDto<CustomOrderPreviewResponseDto> = getCustomOrderPreviewResponseDto()
        Assertions.assertNotNull(response.data)
        Assertions.assertNotNull(response.data?.order)
        Assertions.assertNotNull(response.data?.order?.legDetails)
    }

    @Test
    fun `test Custom Order Place api call`() {
        val response: ServerResponseDto<CustomOrderPlaceResponseDto> = getCustomOrderPlaceResponseDto()
        Assertions.assertNotNull(response.data)
        Assertions.assertNotNull(response.data?.order)
        Assertions.assertNotNull(response.data?.order?.legDetails)
    }

    @Test
    fun `test Custom Order Edit Preview api call`() {
        val response: ServerResponseDto<CustomOrderEditPreviewResponseDto> = getCustomOrderEditPreviewResponseDto()
        Assertions.assertNotNull(response.data)
        Assertions.assertNotNull(response.data?.order)
        Assertions.assertNotNull(response.data?.order?.legDetails)
    }

    @Test
    fun `test Custom Order Edit Place api call`() {
        val response: ServerResponseDto<CustomOrderEditPlaceResponseDto> = getCustomOrderEditPlaceResponseDto()
        Assertions.assertNotNull(response.data)
        Assertions.assertNotNull(response.data?.order?.legDetails)
    }

    private fun getCustomOrderStrategyResponseBodyDto(): ServerResponseDto<CustomOrderStrategyResponseBodyDto> =
        CustomOrderApiClientTest::class.getObjectFromJson(
            "strategyidentifier.json", ServerResponseDto::class.java, CustomOrderStrategyResponseBodyDto::class.java
        )

    private fun getCustomOrderPreviewResponseDto(): ServerResponseDto<CustomOrderPreviewResponseDto> =
        CustomOrderApiClientTest::class.getObjectFromJson(
            "preview.json", ServerResponseDto::class.java, CustomOrderPreviewResponseDto::class.java
        )

    private fun getCustomOrderPlaceResponseDto(): ServerResponseDto<CustomOrderPlaceResponseDto> =
        CustomOrderApiClientTest::class.getObjectFromJson(
            "place.json", ServerResponseDto::class.java, CustomOrderPlaceResponseDto::class.java
        )

    private fun getCustomOrderEditPreviewResponseDto(): ServerResponseDto<CustomOrderEditPreviewResponseDto> =
        CustomOrderApiClientTest::class.getObjectFromJson(
            "change_preview.json", ServerResponseDto::class.java, CustomOrderEditPreviewResponseDto::class.java
        )

    private fun getCustomOrderEditPlaceResponseDto(): ServerResponseDto<CustomOrderEditPlaceResponseDto> =
        CustomOrderApiClientTest::class.getObjectFromJson(
            "change.json", ServerResponseDto::class.java, CustomOrderEditPlaceResponseDto::class.java
        )
}
