package com.etrade.mobilepro.chat.data.setup.dto.getChatOperatingHours

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChatOperatingHoursDto(

    @Json(name = "data")
    val data: ChatOperatingHoursData? = null
)

@JsonClass(generateAdapter = true)
data class ChatOperatingHoursData(

    @Json(name = "isInHoliday")
    val isHoliday: Boolean = false,

    @Json(name = "inOperatingHours")
    val inOperatingHours: Boolean = false
)
