package com.etrade.mobilepro.chart.performance.data

import com.etrade.mobilepro.chart.data.ChartRequestDto
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

internal interface PerformanceChartApiClient {

    @JsonMoshi
    @POST("/app/apg/perfChart.json")
    fun getPerformanceChart(@JsonMoshi @Body request: ChartRequestDto): Observable<PerformanceChartResponseDto>
}
