package com.etrade.mobilepro.watchlist.overview.viewmodel.tableview

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithInstrumentType
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell
import com.etrade.mobilepro.util.color.DataColor
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.formatters.formatMarketPercentDataCustom
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnInterface
import java.math.BigDecimal

class WatchlistDataCell(
    rowId: String,
    dataColor: DataColor,
    val ticker: String?,
    val initialValue: Any?,
    override val instrumentType: InstrumentType,
    displayText: String = ""
) : DataCell(rowId, displayText, BigDecimal.ZERO, dataColor = dataColor),
    WithInstrumentType

@Suppress("LongParameterList")
fun createWatchlistDataCell(
    rowId: String,
    ticker: String?,
    initialValue: BigDecimal?,
    column: WatchlistColumn,
    type: InstrumentType,
    dataColorResolver: DataColorResolver,
    tickIndicator: Short? = null
): WatchlistDataCell {
    val cellValue = initialValue ?: BigDecimal.ZERO
    return WatchlistDataCell(
        rowId = rowId,
        ticker = ticker,
        initialValue = initialValue,
        dataColor = getWatchlistCellDataColor(cellValue, column, dataColorResolver, tickIndicator),
        instrumentType = type,
        displayText = getWatchlistCellDisplayText(initialValue, column)
    ).apply {
        this.value = cellValue
    }
}

fun WatchlistEntry.mapToWatchlistDataCellParams(): Map<WatchlistColumn, BigDecimal?> = mapOf(
    WatchlistColumn.Ask to ask,
    WatchlistColumn.Bid to bid,
    WatchlistColumn.ChangeDollar to change,
    WatchlistColumn.ChangePercent to changePercent,
    WatchlistColumn.DayHigh to dayHigh,
    WatchlistColumn.DayLow to dayLow,
    WatchlistColumn.EarningsPerShare to earningsPerShare,
    WatchlistColumn.LastPrice to lastPrice,
    WatchlistColumn.PriceEarningRatio to priceEarningsRatio,
    WatchlistColumn.Volume to volume,
    WatchlistColumn.FiftyTwoWeekHigh to week52High,
    WatchlistColumn.FiftyTwoWeekLow to week52Low
)

fun Level1Data.mapToWatchlistDataCellParams(): Map<WatchlistColumn, BigDecimal?> {
    val dayChange = if (this is StockData) dayChange?.safeParseBigDecimal() else change?.safeParseBigDecimal()
    val dayChangePercent = if (this is StockData) dayChangePercent?.safeParseBigDecimal() else changePercent?.safeParseBigDecimal()

    return mapOf(
        WatchlistColumn.Ask to ask?.safeParseBigDecimal(),
        WatchlistColumn.Bid to bid?.safeParseBigDecimal(),
        WatchlistColumn.ChangeDollar to dayChange,
        WatchlistColumn.ChangePercent to dayChangePercent,
        WatchlistColumn.DayHigh to dayHigh?.safeParseBigDecimal(),
        WatchlistColumn.DayLow to dayLow?.safeParseBigDecimal(),
        WatchlistColumn.EarningsPerShare to earningsPerShare?.safeParseBigDecimal(),
        WatchlistColumn.LastPrice to lastPrice?.safeParseBigDecimal(),
        WatchlistColumn.PriceEarningRatio to priceToEarnings?.safeParseBigDecimal(),
        WatchlistColumn.Volume to volume?.safeParseBigDecimal(),
        WatchlistColumn.FiftyTwoWeekHigh to yearHigh?.safeParseBigDecimal(),
        WatchlistColumn.FiftyTwoWeekLow to yearLow?.safeParseBigDecimal()
    )
}

fun getRowDataCellItems(
    entryID: String,
    columnOrder: Collection<WatchlistColumnInterface>,
    entry: WatchlistEntry,
    dataColorResolver: DataColorResolver
): List<WatchlistDataCell> {
    val watchlistCellData = entry.mapToWatchlistDataCellParams()
    return mutableListOf<WatchlistDataCell>().also { list ->
        columnOrder.forEach { column ->
            list.add(
                createWatchlistDataCell(
                    rowId = entryID,
                    ticker = entry.ticker,
                    initialValue = watchlistCellData[column],
                    column = column as WatchlistColumn,
                    type = entry.typeCode ?: InstrumentType.UNKNOWN,
                    dataColorResolver = dataColorResolver
                )
            )
        }
    }
}

fun updateWatchlistDataCell(
    cell: WatchlistDataCell,
    column: WatchlistColumn,
    value: BigDecimal?,
    dataColorResolver: DataColorResolver,
    tickIndicator: Short? = null
): WatchlistDataCell = if (value == null || value == cell.initialValue) {
    cell
} else {
    createWatchlistDataCell(
        rowId = cell.id,
        ticker = cell.ticker,
        initialValue = value,
        column = column,
        type = cell.instrumentType,
        dataColorResolver = dataColorResolver,
        tickIndicator = tickIndicator
    )
}

private fun getWatchlistCellDataColor(
    value: Any,
    column: WatchlistColumn,
    dataColorResolver: DataColorResolver,
    tickIndicator: Short? = null
) = if (column == WatchlistColumn.ChangeDollar || column == WatchlistColumn.ChangePercent) {
    dataColorResolver.resolveDataColor(value)
} else if (column == WatchlistColumn.LastPrice) {
    dataColorResolver.resolveFilledDataColor(tickIndicator)
} else {
    DataColor.NEUTRAL
}

fun getWatchlistCellDisplayText(
    initialValue: BigDecimal?,
    column: WatchlistColumn
) = when (column) {
    WatchlistColumn.ChangeDollar,
    WatchlistColumn.Bid,
    WatchlistColumn.Ask,
    WatchlistColumn.DayHigh,
    WatchlistColumn.DayLow,
    WatchlistColumn.FiftyTwoWeekHigh,
    WatchlistColumn.FiftyTwoWeekLow,
    WatchlistColumn.LastPrice -> MarketDataFormatter.formatMarketDataCustom(initialValue)
    WatchlistColumn.ChangePercent -> MarketDataFormatter.formatMarketPercentDataCustom(initialValue)
    WatchlistColumn.Volume -> MarketDataFormatter.formatVolumeTwoDecimals(initialValue)
    WatchlistColumn.PriceEarningRatio -> MarketDataFormatter.formatPriceToBeUpdated(initialValue).plus("x")
    WatchlistColumn.EarningsPerShare -> MarketDataFormatter.formatPriceToBeUpdated(initialValue)
}
