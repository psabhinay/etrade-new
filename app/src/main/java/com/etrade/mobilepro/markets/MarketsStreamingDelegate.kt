package com.etrade.mobilepro.markets

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import io.reactivex.Observable

class MarketsStreamingDelegateImpl(
    val streamingController: StreamingSubscriptionController,
    val streamingStatusController: StreamingStatusController,
    private val setOfFields: Set<Level1Field>
) : MarketsStreamingDelegate {

    override fun isStreamingEnabled() = streamingStatusController.isStreamingToggleEnabled

    override fun cancelSubscription() {
        streamingController.cancelSubscription()
    }

    override fun pauseStreaming() {
        streamingController.pauseSubscription()
    }

    override fun resumeStreaming() {
        if (isStreamingEnabled()) {
            streamingController.resumeSubscription()
        }
    }

    override fun initSubscription(symbols: Observable<Set<Symbol>>, block: (String, InstrumentType, Level1Data) -> Unit) {
        streamingController.initSubscription({ setOfFields }, symbols) { symbol, instrumentType, data ->
            block.invoke(symbol, instrumentType, data)
        }
        if (isStreamingEnabled()) {
            streamingController.resumeSubscription()
        }
    }
}

interface MarketsStreamingDelegate {
    fun isStreamingEnabled(): Boolean
    fun resumeStreaming()
    fun pauseStreaming()
    fun cancelSubscription()
    fun initSubscription(symbols: Observable<Set<Symbol>>, block: (String, InstrumentType, Level1Data) -> Unit)
}
