package com.etrade.mobilepro.checkdeposit.presentation.details.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.checkdeposit.presentation.databinding.AdapterDepositDetailDividerBinding
import com.etrade.mobilepro.checkdeposit.presentation.details.DepositDetail
import com.etrade.mobilepro.checkdeposit.presentation.details.holder.DepositDetailDividerViewHolder
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class DepositDividerAdapterDelegate : AbsListItemAdapterDelegate<DepositDetail.Divider, DepositDetail, DepositDetailDividerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): DepositDetailDividerViewHolder {
        return DepositDetailDividerViewHolder(parent.viewBinding(AdapterDepositDetailDividerBinding::inflate))
    }

    override fun isForViewType(item: DepositDetail, items: MutableList<DepositDetail>, position: Int): Boolean = item is DepositDetail.Divider

    override fun onBindViewHolder(item: DepositDetail.Divider, holder: DepositDetailDividerViewHolder, payloads: MutableList<Any>) {
        // nothing to bind
    }
}
