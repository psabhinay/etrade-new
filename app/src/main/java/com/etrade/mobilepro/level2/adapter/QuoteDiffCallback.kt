package com.etrade.mobilepro.level2.adapter

import androidx.recyclerview.widget.DiffUtil
import com.etrade.mobilepro.level2.QuoteRowData

class QuoteDiffCallback(
    private val oldItems: List<QuoteRowData>,
    private val newItems: List<QuoteRowData>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]
        return oldItem.javaClass == newItem.javaClass &&
            oldItem.askItem == newItem.askItem &&
            oldItem.bidItem == newItem.bidItem
    }
}
