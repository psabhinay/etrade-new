package com.etrade.mobilepro.ui.nestedrecyclerview

/**
 * Marks a view that has restricted height.
 */
interface RestrictedHeightView {

    /**
     * Top coordinate of a view.
     */
    val currentTop: Int

    /**
     * Bottom coordinate of a view.
     */
    val currentBottom: Int

    /**
     * Checks if a view can scroll up the list (towards the first item).
     */
    val canScrollUp: Boolean
        get() = checkScrollableVertically(-1)

    /**
     * Checks if a view can scroll down the list (towards the last item).
     */
    val canScrollDown: Boolean
        get() = checkScrollableVertically(1)

    /**
     * Max view's height.
     */
    var viewMaxHeight: Int

    /**
     * Checks if a view can scroll in specified [direction].
     *
     * @param direction direction of scroll
     *
     * @return `true` if scrollable, `false` otherwise
     */
    fun checkScrollableVertically(direction: Int): Boolean
}
