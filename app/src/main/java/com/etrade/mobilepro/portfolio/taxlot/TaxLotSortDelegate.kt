package com.etrade.mobilepro.portfolio.taxlot

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.livedata.DistinctLiveData
import com.etrade.mobilepro.portfolio.data.PortfolioColumnRepo
import com.etrade.mobilepro.portfolio.delegate.BasePositionSortDelegate
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.util.SortOrder
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject

class TaxLotSortDelegate @AssistedInject constructor(
    portfolioColumnRepo: PortfolioColumnRepo,
    @Assisted override val tableViewType: PortfolioTableViewType,
    @Assisted override val positionTableMode: PositionTableMode
) : BasePositionSortDelegate(portfolioColumnRepo) {

    override val sortInfo: LiveData<SortInfo>
        get() = _sortInfo

    private val _sortInfo: MutableLiveData<SortInfo> =
        DistinctLiveData(SortInfo(PortfolioRecordColumn.SYMBOL, SortOrder.ASCENDING))

    override val sortViewState: LiveData<SortStateInfo>
        get() = _sortInfo.map { info ->
            getSortStateInfo(info)
        }

    override fun sortColumn(newState: SortInfo) {
        _sortInfo.value = newState
    }

    override fun getOldSortInfo(): SortInfo? = _sortInfo.value
}
