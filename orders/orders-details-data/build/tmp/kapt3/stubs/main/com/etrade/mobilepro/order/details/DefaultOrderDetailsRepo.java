package com.etrade.mobilepro.order.details;

import com.etrade.mobilepro.backends.api.ServerError;
import com.etrade.mobilepro.backends.api.ServerMessage;
import com.etrade.mobilepro.backends.api.ServerResponseException;
import com.etrade.mobilepro.common.result.ETResult;
import com.etrade.mobilepro.instrument.InstrumentType;
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem;
import com.etrade.mobilepro.order.details.api.OrderCancelRequest;
import com.etrade.mobilepro.order.details.api.OrderDetailDescription;
import com.etrade.mobilepro.order.details.api.OrderDetailsRepo;
import com.etrade.mobilepro.order.details.api.OrderDetailsRequestParams;
import com.etrade.mobilepro.order.details.api.StockPlanOrderCancelResponse;
import com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequest;
import com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequestDto;
import com.etrade.mobilepro.order.details.dto.OrderDeleteRequestDto;
import com.etrade.mobilepro.order.details.dto.OrderDeleteRequestValueDto;
import com.etrade.mobilepro.order.details.dto.OrderDetailsRequestDto;
import com.etrade.mobilepro.order.details.dto.OrderDetailsRequestValueDto;
import com.etrade.mobilepro.order.details.mapper.OrderDetailsMapper;
import com.etrade.mobilepro.order.details.rest.OrderDetailsApiClient;
import com.etrade.mobilepro.orders.api.OrderRequestType;
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto;
import io.reactivex.Observable;
import io.reactivex.Single;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u001c\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013J\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J&\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\f2\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00150\tH\u0002J\'\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001e0\u000f2\u0006\u0010\u001f\u001a\u00020\u00122\u0006\u0010 \u001a\u00020\u0012H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010!J\u001c\u0010\"\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020$0\t0#2\u0006\u0010\u000b\u001a\u00020%H\u0016J\u001c\u0010&\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020$0\t0#2\u0006\u0010\u000b\u001a\u00020%H\u0016J\'\u0010\'\u001a\b\u0012\u0004\u0012\u00020(0\u000f2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u0012H\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010!R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006)"}, d2 = {"Lcom/etrade/mobilepro/order/details/DefaultOrderDetailsRepo;", "Lcom/etrade/mobilepro/order/details/api/OrderDetailsRepo;", "orderDetailsApiClient", "Lcom/etrade/mobilepro/order/details/rest/OrderDetailsApiClient;", "mapper", "Lcom/etrade/mobilepro/order/details/mapper/OrderDetailsMapper;", "(Lcom/etrade/mobilepro/order/details/rest/OrderDetailsApiClient;Lcom/etrade/mobilepro/order/details/mapper/OrderDetailsMapper;)V", "cancelGroupOrder", "Lio/reactivex/Single;", "", "Lcom/etrade/mobilepro/backends/api/ServerMessage;", "requestParams", "Lcom/etrade/mobilepro/order/details/api/OrderCancelRequest;", "cancelOrder", "cancelStockPlanOrder", "Lcom/etrade/mobilepro/common/result/ETResult;", "Lcom/etrade/mobilepro/order/details/api/StockPlanOrderCancelResponse;", "orderNumber", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "createLegDetailsCommonDto", "Lcom/etrade/mobilepro/trade/data/common/LegDetailsCommonDto;", "symbol", "description", "Lcom/etrade/mobilepro/order/details/api/OrderDetailDescription;", "createOrderConfirmCancelRequest", "Lcom/etrade/mobilepro/order/details/dto/OrderConfirmCancelRequest;", "underlier", "legRequestList", "deleteOrder", "", "accountId", "orderId", "(Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getOrderDetails", "Lio/reactivex/Observable;", "Lcom/etrade/mobilepro/order/details/api/BaseOrderDetailsItem;", "Lcom/etrade/mobilepro/order/details/api/OrderDetailsRequestParams;", "getSavedOrderDetails", "getStockPlanOrderDetails", "Lcom/etrade/mobilepro/order/details/api/BaseOrderDetailsItem$StockPlanOrderDetailsItem;", "orders-details-data"})
public final class DefaultOrderDetailsRepo implements com.etrade.mobilepro.order.details.api.OrderDetailsRepo {
    private final com.etrade.mobilepro.order.details.rest.OrderDetailsApiClient orderDetailsApiClient = null;
    private final com.etrade.mobilepro.order.details.mapper.OrderDetailsMapper mapper = null;
    
    @javax.inject.Inject()
    public DefaultOrderDetailsRepo(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.order.details.rest.OrderDetailsApiClient orderDetailsApiClient, @org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.order.details.mapper.OrderDetailsMapper mapper) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Observable<java.util.List<com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem>> getSavedOrderDetails(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.order.details.api.OrderDetailsRequestParams requestParams) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Observable<java.util.List<com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem>> getOrderDetails(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.order.details.api.OrderDetailsRequestParams requestParams) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getStockPlanOrderDetails(@org.jetbrains.annotations.NotNull()
    java.lang.String orderNumber, @org.jetbrains.annotations.NotNull()
    java.lang.String symbol, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.etrade.mobilepro.common.result.ETResult<com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem.StockPlanOrderDetailsItem>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Single<java.util.List<com.etrade.mobilepro.backends.api.ServerMessage>> cancelOrder(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.order.details.api.OrderCancelRequest requestParams) {
        return null;
    }
    
    private final com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequest createOrderConfirmCancelRequest(java.lang.String underlier, com.etrade.mobilepro.order.details.api.OrderCancelRequest requestParams, java.util.List<? extends com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto> legRequestList) {
        return null;
    }
    
    private final com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto createLegDetailsCommonDto(java.lang.String symbol, com.etrade.mobilepro.order.details.api.OrderDetailDescription description) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object cancelStockPlanOrder(@org.jetbrains.annotations.NotNull()
    java.lang.String orderNumber, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.etrade.mobilepro.common.result.ETResult<com.etrade.mobilepro.order.details.api.StockPlanOrderCancelResponse>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.reactivex.Single<java.util.List<com.etrade.mobilepro.backends.api.ServerMessage>> cancelGroupOrder(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.order.details.api.OrderCancelRequest requestParams) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object deleteOrder(@org.jetbrains.annotations.NotNull()
    java.lang.String accountId, @org.jetbrains.annotations.NotNull()
    java.lang.String orderId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.etrade.mobilepro.common.result.ETResult<kotlin.Unit>> continuation) {
        return null;
    }
}