package com.etrade.mobilepro.tracking.params

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.tracking.EVENT_ATTRIBUTE_CUSTOMER_ID
import com.etrade.mobilepro.tracking.EVENT_ATTRIBUTE_SECURITY_TYPE
import com.etrade.mobilepro.tracking.EVENT_ATTRIBUTE_SYMBOL
import com.etrade.mobilepro.tracking.UNSPECIFIED
import com.etrade.mobilepro.tracking.trackerValue

private const val EVENT_NAME = "Add to Watch List"
private const val EVENT_ATTRIBUTE_DEVICE_WATCH_LIST = "Device Watch List"

class AddToWatchListAnalyticsParams(
    private val customerId: String?,
    private val deviceWatchList: Boolean,
    private val symbol: String,
    private val type: InstrumentType,
    private val securityType: String
) : AnalyticsParams {

    override val name: String = EVENT_NAME

    override val params: Map<String, String>
        get() = mapOf(
            EVENT_ATTRIBUTE_SYMBOL to symbol,
            EVENT_ATTRIBUTE_SECURITY_TYPE to (getSecurityType(type, securityType) ?: UNSPECIFIED),
            EVENT_ATTRIBUTE_DEVICE_WATCH_LIST to deviceWatchList.trackerValue,
            EVENT_ATTRIBUTE_CUSTOMER_ID to (customerId ?: UNSPECIFIED)
        )

    @Suppress("ComplexMethod") // obvious method to get security type
    private fun getSecurityType(type: InstrumentType, securityType: String): String? {
        return if (securityType.isNotEmpty()) {
            securityType
        } else {
            when (type) {
                InstrumentType.INDX -> "Index"
                InstrumentType.EQ -> "Stock"
                InstrumentType.OPTN, InstrumentType.OPTNC, InstrumentType.OPTNP -> "Option"
                InstrumentType.MF -> "Mutual Fund"
                InstrumentType.MMF -> "Mutual Market Fund"
                InstrumentType.BOND -> "Bond"
                InstrumentType.ETF -> "ETF"

                else -> null
            }
        }
    }
}
