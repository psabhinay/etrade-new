package com.etrade.mobilepro.trade.data.advanced

import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderPreviewResponse
import com.etrade.mobilepro.trade.api.advanced.TrailingStopOrderDetails
import com.etrade.mobilepro.trade.api.advanced.TrailingStopOrderResponse

internal class AdvancedOrderPreviewResponseWrapper(
    responseDto: AdvancedOrderPreviewResponseDto
) : TrailingStopOrderResponse, AdvancedOrderPreviewResponse by responseDto {
    override val previewId: String? = (responseDto as? AdvancedOrderPreviewResponse)?.previewId
    override val trailingStopOrderDetails: List<TrailingStopOrderDetails> = createTrailingStopOrders(responseDto)
}

private fun createTrailingStopOrders(responseDto: BaseAdvancedOrderResponseDto): List<TrailingStopOrderDetails> = responseDto.advancedOrders?.map {
    object : TrailingStopOrderDetails {
        override val orderTerm: String = it.aoTerm
        override val orderType: String = it.aoOrderType
        override val offsetType = it.offsetType
        override val offsetValue = it.offsetValue
        override val initialStopPrice: String = it.initialStopPrice
        override val followPrice = it.followPrice
    }
}.orEmpty()
