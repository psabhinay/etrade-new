package com.etrade.mobilepro.markets.futures.widget

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MarketsFuturesViewDto(
    @Json(name = "data")
    override val data: Any?,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?
) : BaseScreenView()
