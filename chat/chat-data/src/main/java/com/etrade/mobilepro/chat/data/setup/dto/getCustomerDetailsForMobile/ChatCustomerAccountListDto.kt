package com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChatCustomerAccountListDto(

    @Json(name = "accNum")
    val accNum: String,

    @Json(name = "instNo")
    val instNo: String? = "",

    @Json(name = "instName")
    val instName: String? = null,

    @Json(name = "accountType")
    val accountType: String? = null,

    @Json(name = "acctHolderCount")
    val acctHolderCount: String? = null,

    @Json(name = "acctHolderList")
    val acctHolderList: List<ChatAccountHolderListDto> = emptyList(),

    @Json(name = "instDescription")
    val instDescription: String,

    @Json(name = "isOH")
    val isOH: Boolean = false
)

@JsonClass(generateAdapter = true)
data class ChatAccountHolderListDto(

    @Json(name = "firstname")
    val firstname: String? = null,

    @Json(name = "lastname")
    val lastname: String? = null,

    @Json(name = "cid")
    val cid: String? = null,

    @Json(name = "tier")
    val tier: String? = null
)
