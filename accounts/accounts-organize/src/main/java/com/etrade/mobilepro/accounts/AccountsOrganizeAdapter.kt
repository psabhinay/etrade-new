package com.etrade.mobilepro.accounts

import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.editlist.ItemAdapter

class AccountsOrganizeAdapter(
    viewModel: EditListViewModel<*>,
    itemTouchHelper: ItemTouchHelper,
    isDragFinished: () -> Boolean
) : ItemAdapter(viewModel, itemTouchHelper, false, false, isDragFinished) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return AccountOrganizeViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        bindItemType(holder as AccountOrganizeViewHolder, position)
    }
}
