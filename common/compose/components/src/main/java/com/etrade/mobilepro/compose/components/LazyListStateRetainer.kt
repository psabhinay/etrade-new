package com.etrade.mobilepro.compose.components

import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.saveable.Saver
import androidx.compose.runtime.saveable.SaverScope
import androidx.compose.runtime.saveable.rememberSaveable
import org.slf4j.LoggerFactory

/**
 * Retains and restores lazyListState. Does not use bundles, instead it is intended to be used in VMs or similar classes that outlive config changes
 * Can be used to restore lazyListState in a deferred manner when a certain condition is met. This is useful when dealing with paged lists
 */
class LazyListStateRetainer : Saver<LazyListState, Any> {
    private val logger = LoggerFactory.getLogger(this::class.java)
    var firstVisibleItemIndex = 0
    var firstVisibleItemScrollOffset = 0
    private var hasSaved = false
    private var currentListState: LazyListState? = null

    private fun getStoredState(): LazyListState {
        logger.debug("getStored: $firstVisibleItemIndex $firstVisibleItemScrollOffset")
        return LazyListState(firstVisibleItemIndex, firstVisibleItemScrollOffset)
    }

    override fun restore(value: Any): LazyListState = getStoredState()

    override fun SaverScope.save(value: LazyListState): Any? {
        firstVisibleItemIndex = value.firstVisibleItemIndex
        firstVisibleItemScrollOffset = value.firstVisibleItemScrollOffset
        logger.debug("save: $firstVisibleItemIndex $firstVisibleItemScrollOffset")
        hasSaved = true
        return null
    }

    @Composable
    fun rememberLazyListState(): LazyListState = rememberSaveable(saver = this) {
        getStoredState()
    }.also {
        currentListState = it
    }

    @Composable
    fun <T> RestoreScrollStateWhen(data: T, condition: (T) -> Boolean) {
        if (hasSaved && condition(data)) {
            logger.debug("restoreWhen hit")
            hasSaved = false
            LaunchedEffect(data) { currentListState?.scrollToItem(firstVisibleItemIndex, firstVisibleItemScrollOffset) }
        }
    }
}
