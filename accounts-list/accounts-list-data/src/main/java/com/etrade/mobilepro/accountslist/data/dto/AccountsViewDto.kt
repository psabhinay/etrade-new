package com.etrade.mobilepro.accountslist.data.dto

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class AccountsViewDto(
    @Json(name = "data")
    override val data: AccountsListDto,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?
) : BaseScreenView()
