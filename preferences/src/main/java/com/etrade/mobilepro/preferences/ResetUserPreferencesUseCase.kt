package com.etrade.mobilepro.preferences

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.util.UseCase
import javax.inject.Inject

interface ResetUserPreferencesUseCase : UseCase<Unit, ETResult<Unit>>

class ResetUserPreferencesUseCaseImp @Inject constructor(
    private val userPreferences: UserPreferences,
    private val loginPreferences: LoginPreferencesRepo,
    private val streamingStatusController: StreamingStatusController
) : ResetUserPreferencesUseCase {
    override suspend fun execute(parameter: Unit): ETResult<Unit> {
        return runCatchingET {
            userPreferences.apply {
                watchlistTableViewSelectedIndex = 0
                isSweepActivityEnabled = false
                pfMoversWidgetAccountUuid = null
                pfNewsWidgetAccountUuid = null
                setDefaultLandingPage(LandingPageParams())
                setRecentWatchlist("", null)
                setAccountsOptions(emptyList())
            }
            loginPreferences.disableBiometricAuthentication()
            loginPreferences.isLoginForSameUser = false
            loginPreferences.isPushNotificationPopupShown = false
            streamingStatusController.isStreamingToggleEnabled = false
        }
    }
}
