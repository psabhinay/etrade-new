package com.etrade.mobilepro.messagecenter.presentation.newmessage

import android.os.Bundle
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.messagecenter.api.model.ReferenceBook
import com.etrade.mobilepro.messagecenter.presentation.R
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterFragmentNewMessageBinding
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterPartialNewMessageFormBinding
import com.etrade.mobilepro.messagecenter.presentation.messagelist.MessageCenterListViewModel
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.accessibility.bindTextInputButtonAccessibility
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import javax.inject.Inject

private val ACCOUNT_TAG = "${NewMessageFragment::class.java.canonicalName}_account_selector"
private val SUBJECT_TAG = "${NewMessageFragment::class.java.canonicalName}_subject_selector"
private val TOPIC_TAG = "${NewMessageFragment::class.java.canonicalName}_topic_selector"
private const val IS_FROM_OUT_OF_MESSAGE_CENTER: String = "isFromOutOfMessageCenter"

@RequireLogin
class NewMessageFragment @Inject constructor(
    viewModelProvider: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : OverlayBaseFragment() {

    private val binding by viewBinding(MessageCenterFragmentNewMessageBinding::bind)
    private val partialForm by viewBinding(MessageCenterPartialNewMessageFormBinding::bind)

    override val layoutRes: Int = R.layout.message_center_fragment_new_message

    private val viewModel by viewModels<NewMessageViewModel> { viewModelProvider }
    private val messageListViewModel by navGraphViewModels<MessageCenterListViewModel>(R.id.message_center_nav_graph) { viewModelProvider }
    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private var snackbar: Snackbar? = null
    private lateinit var accountSelector: DropDownMultiSelector<ReferenceBook.Account>
    private lateinit var subjectSelector: DropDownMultiSelector<ReferenceBook.Subject>
    private lateinit var topicSelector: DropDownMultiSelector<ReferenceBook.Topic>
    private val isFromOutOfMessageCenter by lazy {
        arguments?.getBoolean(
            IS_FROM_OUT_OF_MESSAGE_CENTER
        ) ?: false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
        viewModel.refresh(forced = false)
    }

    private fun setupFragment() {
        setupSelectors()
        setupListeners()
    }

    private fun setupListeners() {
        with(partialForm) {
            account.setOnClickListener {
                accountSelector.open()
            }
            subject.setOnClickListener {
                subjectSelector.open()
            }
            topic.setOnClickListener {
                topicSelector.open()
            }
            newMessageText.doAfterTextChanged { viewModel.messageTextChanged(it?.toString()) }
        }
        binding.send.setOnClickListener {
            viewModel.sendNewMessage()
        }
    }

    @Suppress("LongMethod") // no way to shorten
    private fun setupSelectors() {
        accountSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = ACCOUNT_TAG,
                multiSelectEnabled = false,
                title = resources.getString(R.string.title_account),
                itemsData = viewModel.accountsData,
                selectListener = { _, selected ->
                    viewModel.accountSelected(selected.value)
                }
            )
        )
        subjectSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = SUBJECT_TAG,
                multiSelectEnabled = false,
                title = resources.getString(R.string.message_center_subject),
                itemsData = viewModel.subjectsData,
                selectListener = { _, selected ->
                    viewModel.subjectSelected(selected.value)
                }
            )
        )
        topicSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = TOPIC_TAG,
                multiSelectEnabled = false,
                title = resources.getString(R.string.message_center_topic),
                itemsData = viewModel.topicsData,
                selectListener = { _, selected ->
                    viewModel.topicSelected(selected.value)
                }
            )
        )
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                handleStatus(it)
                handleSelections(it)
                handleEnabledState(it)
            }
        )
    }

    private fun setTextAndDescription(
        textInput: TextInputEditText,
        textLayout: TextInputLayout,
        text: String?,
        description: String? = null
    ) {
        textInput.setText(text)
        textLayout.bindTextInputButtonAccessibility(description ?: text)
    }

    private fun handleSelections(state: NewMessageViewModel.ViewState) {
        val accountText = state.selectedAccount?.shortDescription
        with(partialForm) {
            setTextAndDescription(
                account,
                accountLayout,
                accountText,
                context?.accountNameContentDescription(accountText)
            )
            setTextAndDescription(
                subject,
                subjectLayout,
                state.selectedSubject?.desc
            )
            setTextAndDescription(
                topic,
                topicLayout,
                state.selectedTopic?.desc
            )
        }
    }

    private fun handleEnabledState(state: NewMessageViewModel.ViewState) {
        partialForm.subjectLayout.isEnabled = state.isSubjectEnabled()
        partialForm.topicLayout.isEnabled = state.isTopicEnabled()
        binding.send.isEnabled = state.isSendButtonEnabled()
    }

    private fun handleStatus(state: NewMessageViewModel.ViewState) {
        when (state.status) {
            NewMessageViewModel.ViewState.Status.Idle -> {
                snackbar?.dismiss()
                toggleViewState(isLoading = false)
            }
            NewMessageViewModel.ViewState.Status.Finished -> {
                handleStatusFinished()
            }
            NewMessageViewModel.ViewState.Status.Loading -> {
                snackbar?.dismiss()
                toggleViewState(isLoading = true)
            }
            is NewMessageViewModel.ViewState.Status.Error -> {
                toggleViewState(isLoading = false)
                snackbar = snackBarUtil.retrySnackbar { state.status.retry.invoke() }
            }
            is NewMessageViewModel.ViewState.Status.SendNewMessageFailed -> {
                toggleViewState(isLoading = false)

                if (state.status.consumableSnackBarMessage != null) {
                    snackbar = snackBarUtil.errorSnackbar()
                }
            }
        }
    }

    private fun handleStatusFinished() {
        if (!isFromOutOfMessageCenter) {
            messageListViewModel.scrollMessagesToTop()
            messageListViewModel.refresh(forced = true)
            messageListViewModel.showMessage(getString(R.string.message_center_message_has_been_sent))
        }
        snackbar?.dismiss()
        findNavController().popBackStack()
    }

    private fun toggleViewState(isLoading: Boolean) {
        binding.scrollView.visibility = if (isLoading) {
            View.GONE
        } else {
            View.VISIBLE
        }
        if (isLoading) {
            binding.newMessagePb.show()
        } else {
            binding.newMessagePb.hide()
        }
    }
}
