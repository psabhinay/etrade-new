package com.etrade.mobilepro.ui_demo.snackbar

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.ui_demo.R
import com.etrade.mobilepro.ui_demo.databinding.ActivitySnackbarBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory


class SnackbarActivity : AppCompatActivity() {
    private val binding by viewBinding(ActivitySnackbarBinding::inflate)
    private lateinit var snackBarUtil: SnackBarUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        snackBarUtil = SnackbarUtilFactory(ConnectionNotifier(this), this).createSnackbarUtil({ this }, { binding.container })
        binding.withAction.setOnClickListener {
            val s = snackBarUtil.snackbarWithAction("I have action", "ACTION") { Toast.makeText(this, "Action", Toast.LENGTH_SHORT).show() }
            s?.show()
        }
        binding.withDefaultCustomView.setOnClickListener {
            val s = snackBarUtil.snackbarWithCustomViewAction("I have custom view", R.string.set_alert_lowcase, { Toast.makeText(this, "CustomViewAction", Toast.LENGTH_SHORT).show()})
            s?.show()
        }
    }
}
