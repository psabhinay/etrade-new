package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.WithHardToBorrowOrder
import com.etrade.mobilepro.trade.presentation.util.R
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.util.safeParseBigDecimal

private const val EMPTY_STRING = ""

class WithHardToBorrowOrderAdapterDelegate(private val resources: Resources) : OrderAdapterDelegate {

    override fun createOrderSectionItems(order: Order): List<TableSectionItemLayout> {
        require(order is WithHardToBorrowOrder)
        return createHardToBorrowSectionItems(order)
    }

    private fun createHardToBorrowSectionItems(response: WithHardToBorrowOrder): List<TableSectionItemLayout> {
        return with(response) {
            if (estDailyBorrowInterest.isNullOrEmpty() || borrowRate.isNullOrEmpty()) {
                return emptyList()
            }
            val mutableList = mutableListOf<TableSectionItemLayout?>()
            mutableList.add(sectionItem(resources, R.string.trade_label_htb, EMPTY_STRING))
            mutableList.add(
                sectionItem(
                    resources,
                    R.string.trade_label_estimated_borrow_rate,
                    borrowRate?.formatWithPercentage()
                )
            )
            mutableList.add(
                sectionItem(
                    resources,
                    R.string.trade_label_estimated_daily_interest,
                    MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(estDailyBorrowInterest?.safeParseBigDecimal())
                )
            )
            mutableList.filterNotNull()
        }
    }

    private fun String.formatWithPercentage() = "$this%"
}
