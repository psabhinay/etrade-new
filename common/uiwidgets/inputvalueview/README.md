***Custom views for manual value input module***

## InputValueView
This class is a TextInputLayout inheritor and is actually a TextField with extra logic of formatting, 
validating and customizing input filters depending on different events or by demand. 
Please look at InputFieldManager Javadoc for this logic description, as all of these is delegated to the InputFieldManager 

## SeekbarInputValueView
This is a compound view consisting of a SeekBar and an EditText. Same logic and features are applied to its EditText as in
InputValueView. Seekbar events update value on EditText and vice versa 

Both views internally depend on InputFieldManager class. 
Both views use external dependencies for input value handling logic
Both views use external value holder(MutableLiveData) for storing their values
For details please currently kindly look at InputFieldManager class Javadoc
