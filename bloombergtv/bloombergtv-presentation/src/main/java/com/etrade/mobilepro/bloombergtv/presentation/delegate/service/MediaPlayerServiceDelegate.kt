package com.etrade.mobilepro.bloombergtv.presentation.delegate.service

import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.view.SurfaceHolder
import android.view.ViewGroup
import com.etrade.mobilepro.bloombergtv.presentation.common.mute
import com.etrade.mobilepro.bloombergtv.presentation.common.playing
import com.etrade.mobilepro.bloombergtv.presentation.common.safeCall
import com.etrade.mobilepro.bloombergtv.presentation.common.unmute
import com.etrade.mobilepro.bloombergtv.presentation.pip.PipVideoControllerView
import com.etrade.mobilepro.common.result.runCatchingET
import java.util.concurrent.atomic.AtomicBoolean

internal class MediaPlayerServiceDelegate {
    private var player: MediaPlayer? = null
    private val isPrepared = AtomicBoolean(false)

    fun play() = player.safeCall(isPrepared.get()) { start() }
    fun pause() = player.safeCall(isPrepared.get()) { pause() }
    fun mute() = player.mute()
    fun unmute() = player.unmute()
    fun isPlaying(): Boolean = player.playing

    @Suppress("LongParameterList") // optional parameters to reuse this class in two services
    fun start(
        url: String,
        holder: SurfaceHolder? = null,
        videoContainerProvider: (() -> ViewGroup?)? = null,
        hasOngoingCall: () -> Boolean,
        onError: () -> Unit,
        onCompletion: (() -> Unit)? = null
    ) {
        isPrepared.set(false)
        val mediaPlayerCallbacks = object : MediaPlayerCallbacks {
            override fun onPrepared(mp: MediaPlayer) {
                videoContainerProvider?.invoke()?.let { container ->
                    PipVideoControllerView.createAndAttachTo(
                        view = container,
                        startPlay = { mp.start() },
                        isPlaying = { mp.isPlaying }
                    )
                }
                mp.start()
                if (hasOngoingCall()) {
                    mp.pause()
                }
                isPrepared.set(true)
            }

            override fun onError(mp: MediaPlayer, what: Int, extra: Int): Boolean {
                dismantlePlayer()
                onError()
                return true
            }

            override fun onCompletion(mp: MediaPlayer?) = onCompletion?.invoke() ?: Unit
        }
        player = runCatchingET {
            MediaPlayer().apply {
                setCallbacks(mediaPlayerCallbacks)
                setAudioAttributes(AudioAttributes.Builder().setLegacyStreamType(AudioManager.STREAM_MUSIC).build())
                setDisplay(holder)
                setDataSource(url)
                prepareAsync()
            }
        }.getOrNull()
    }

    fun dismantlePlayer() {
        player?.apply {
            setCallbacks(null)
            release()
        }
        player = null
    }

    private fun MediaPlayer.setCallbacks(callbacks: MediaPlayerCallbacks?) {
        setOnPreparedListener(callbacks)
        setOnErrorListener(callbacks)
        setOnCompletionListener(callbacks)
    }

    private interface MediaPlayerCallbacks :
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnCompletionListener
}
