package com.etrade.mobilepro.chat.data.messages.repo

import com.etrade.mobilepro.chat.api.ChatParamsRequestModel
import com.etrade.mobilepro.chat.api.message.models.ChatRequestModel
import com.etrade.mobilepro.chat.data.common.toRequestBody
import com.etrade.mobilepro.chat.data.messages.dto.ChatGetMessagesRequestDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatParamsRequestDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatPostMessageRequestDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatTerminateRequestDto
import com.etrade.mobilepro.util.Serializer
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import javax.inject.Inject

interface ChatRequestBodyManager {
    fun createRetrievedMessageBody(model: ChatRequestModel.GetMessages, ackMessage: Int): RequestBody
    fun createTerminateChatBody(model: ChatRequestModel.TerminateChat): RequestBody
    fun createPostMessageBody(model: ChatRequestModel.PostMessage): RequestBody
    fun createMobileChatBody(model: ChatParamsRequestModel): RequestBody
}

class ChatRequestBodyManagerImpl @Inject constructor(
    private val getMessageSerializer: Serializer<ChatGetMessagesRequestDto>,
    private val postMessageSerializer: Serializer<ChatPostMessageRequestDto>,
    private val terminateChatSerializer: Serializer<ChatTerminateRequestDto>,
    private val chatParamsSerializer: Serializer<ChatParamsRequestDto>
) : ChatRequestBodyManager {

    override fun createRetrievedMessageBody(model: ChatRequestModel.GetMessages, ackMessage: Int): RequestBody =
        getMessageSerializer.serialize(model.toRequestBody(ackMessage)).toRequestBody(getContentType())

    override fun createTerminateChatBody(model: ChatRequestModel.TerminateChat): RequestBody =
        terminateChatSerializer.serialize(model.toRequestBody()).toRequestBody(getContentType())

    override fun createPostMessageBody(model: ChatRequestModel.PostMessage): RequestBody =
        postMessageSerializer.serialize(model.toRequestBody()).toRequestBody(getContentType())

    override fun createMobileChatBody(model: ChatParamsRequestModel): RequestBody =
        chatParamsSerializer.serialize(model.toRequestBody()).toRequestBody(getContentType())

    private fun getContentType() = "application/json; charset=utf-8".toMediaTypeOrNull()
}
