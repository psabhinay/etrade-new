package com.etrade.mobilepro.home

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.postDelayed
import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.etrade.mobilepro.R
import com.etrade.mobilepro.accessvalidation.AppAccessProvider
import com.etrade.mobilepro.accounts.AccountsOrganizeBoardingViewModel
import com.etrade.mobilepro.accounts.AccountsOrganizeOnboardingFragment
import com.etrade.mobilepro.accounts.util.android.view.AccountsSpinnerModel
import com.etrade.mobilepro.alerts.presentation.AlertsSharedViewModel
import com.etrade.mobilepro.application.ApplicationLifecycle
import com.etrade.mobilepro.baseactivity.ActivityNetworkConnectionDelegate
import com.etrade.mobilepro.common.LoginChecker
import com.etrade.mobilepro.common.OnAccountsClickListener
import com.etrade.mobilepro.common.OverlayFragmentManagerHolder
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.extension.clearFocusOnTouchOutside
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.common.navigation.NavRouter
import com.etrade.mobilepro.common.navigation.NavRouterSignalViewModel
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.common.toolbarActionStartView
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.common.toolbarView
import com.etrade.mobilepro.common.viewmodel.EtNavigationViewModel
import com.etrade.mobilepro.common.viewmodel.SnackbarViewModel
import com.etrade.mobilepro.common.viewmodel.pendingNavDirections
import com.etrade.mobilepro.databinding.ActivityMainBinding
import com.etrade.mobilepro.dialog.showDialog
import com.etrade.mobilepro.dialog.viewmodel.AppDialogViewDelegate
import com.etrade.mobilepro.dialog.viewmodel.DialogViewModel
import com.etrade.mobilepro.dialog.viewmodel.plus
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.dropdown.DropDownManager
import com.etrade.mobilepro.dropdown.viewmodel.DropDownViewDelegate
import com.etrade.mobilepro.hidecontent.HideContentOverlayDelegate
import com.etrade.mobilepro.home.helper.AlertsCountBadgeHelper
import com.etrade.mobilepro.home.model.AppActionViewModel
import com.etrade.mobilepro.home.model.MainActivityViewModel
import com.etrade.mobilepro.home.model.PromptInDialogsHelper
import com.etrade.mobilepro.landing.api.LandingRouter
import com.etrade.mobilepro.landing.api.repo.LandingRepo
import com.etrade.mobilepro.landing.presentation.LandingPageSelectionFragment
import com.etrade.mobilepro.landing.presentation.LandingPageSelectionViewModel
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.login.presentation.BiometricAuthenticationHelper
import com.etrade.mobilepro.login.presentation.BiometricAuthenticationHelperHolder
import com.etrade.mobilepro.navigation.AppBarDestinationChangedListener
import com.etrade.mobilepro.navigation.EtNavController
import com.etrade.mobilepro.navigation.pendingNavUri
import com.etrade.mobilepro.review.AppReviewViewModel
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.streaming.StreamingSessionDelegateHelper
import com.etrade.mobilepro.timeout.widget.BiometricTimeoutMainActivityDelegate
import com.etrade.mobilepro.timeout.widget.SessionDialogFragment
import com.etrade.mobilepro.timeout.widget.SessionTerminatedFragment
import com.etrade.mobilepro.timeout.widget.SharedSessionTerminatedViewModel
import com.etrade.mobilepro.tooltip.TooltipViewModel
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.consumeIfNot
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import com.etrade.mobilepro.util.android.extension.isApplicationDebuggable
import com.etrade.mobilepro.util.android.fragment.NavScopedStorageRegisterer
import com.etrade.mobilepro.util.android.fragment.NavScopedStorageRegistererHolder
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.plus
import com.etrade.mobilepro.viewdelegate.ActivitySignalViewDelegate
import com.etrade.mobilepro.viewdelegate.AppMessageViewDelegate
import com.etrade.mobilepro.viewdelegate.ViewDelegate
import com.etrade.mobilepro.viewmodel.NavigationSignalViewModel
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

private const val BOTTOM_BAR_HIDE_DELAY_MS = 50L

internal const val KEY_HIDE_BOTTOM_BAR = "keyHideBottomBar"

@Suppress("LargeClass", "TooManyFunctions")
class MainActivity : AppCompatActivity(), HasAndroidInjector, BiometricAuthenticationHelperHolder, NavScopedStorageRegistererHolder {

    val binding by viewBinding(ActivityMainBinding::inflate)

    override val biometricAuthenticationHelper: BiometricAuthenticationHelper = BiometricAuthenticationHelper()

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var fragmentInjectionFactory: FragmentFactory

    @Inject
    lateinit var snackbarUtilFactory: SnackbarUtilFactory

    @Inject
    lateinit var activityNetworkConnectionDelegate: ActivityNetworkConnectionDelegate

    @Inject
    lateinit var mainNavigation: MainNavigation

    @Inject
    lateinit var loginChecker: LoginChecker

    @Inject
    lateinit var applicationLifecycleObserver: ApplicationLifecycle

    @Inject
    lateinit var biometricTimeoutMainActivityDelegate: BiometricTimeoutMainActivityDelegate

    @Inject
    lateinit var hideContentOverlayDelegate: HideContentOverlayDelegate

    @Inject
    lateinit var streamingSessionDelegateHelper: StreamingSessionDelegateHelper

    @Inject
    lateinit var userViewModel: UserViewModel

    @Inject
    lateinit var snackbarViewModel: SnackbarViewModel

    @Inject
    lateinit var dialogViewModel: DialogViewModel

    @Inject
    lateinit var dialogsHelper: PromptInDialogsHelper

    @Inject
    lateinit var landingRouter: LandingRouter

    @Inject
    lateinit var landingRepo: LandingRepo

    @Inject
    lateinit var alertsCountBadgeHelper: AlertsCountBadgeHelper

    @Inject
    lateinit var appAccessProvider: AppAccessProvider

    @Inject
    lateinit var overlayHolder: OverlayFragmentManagerHolder

    @Inject
    lateinit var darkModeChecker: DarkModeChecker

    @Inject
    @Web
    lateinit var baseUrl: String

    @Inject
    lateinit var navScopedStorageRegisterer: NavScopedStorageRegisterer
    override val navScopedStorage: NavScopedStorageRegisterer
        get() = navScopedStorageRegisterer

    private val appActionViewModel: AppActionViewModel by viewModels { viewModelFactory }
    private val bottomNavigationViewModel: BottomNavigationViewModel by viewModels { viewModelFactory }
    private val viewModel: MainActivityViewModel by viewModels { viewModelFactory }
    private val toolbarViewModel: ToolbarViewModel by viewModels { viewModelFactory }
    private val etNavigationViewModel: EtNavigationViewModel by viewModels()
    private val tooltipViewModel: TooltipViewModel by viewModels()
    private val accountsOrganizeViewModel: AccountsOrganizeBoardingViewModel by viewModels { viewModelFactory }
    private val landingSelectionViewModel: LandingPageSelectionViewModel by viewModels { viewModelFactory }
    private val appReviewViewModel: AppReviewViewModel by viewModels { viewModelFactory }
    private val sharedSessionTerminatedViewModel: SharedSessionTerminatedViewModel by viewModels { viewModelFactory }
    private val alertsSharedViewModel: AlertsSharedViewModel by viewModels { viewModelFactory }

    private val accountsDropDownManager: DropDownManager<AccountsSpinnerModel> by lazy {
        BottomSheetSelector<AccountsSpinnerModel>(supportFragmentManager, resources)
    }

    private val viewModelDelegates: Iterable<ViewDelegate> by lazy {
        setOf(
            ActivitySignalViewDelegate(viewModel + appAccessProvider, this),
            AppDialogViewDelegate(dialogViewModel + appAccessProvider) { showDialog(it) },
            AppMessageViewDelegate(snackbarViewModel, snackbarUtilFactory.createSnackbarUtil({ this }, { binding.container })),
            DropDownViewDelegate(toolbarViewModel, accountsDropDownManager),
            NavigationSignalViewDelegate(appActionViewModel),
            NavigationSignalViewDelegate(landingSelectionViewModel),
            NavRouterSignalViewDelegate(appActionViewModel),
            NavRouterSignalViewDelegate(bottomNavigationViewModel),
            NavRouterSignalViewDelegate(etNavigationViewModel),
            NavRouterSignalViewDelegate(userViewModel),
            NavRouterSignalViewDelegate(viewModel)
        )
    }

    private val mainActivityIntentHelper: MainActivityIntentHelper by lazy {
        MainActivityIntentHelper(this, appActionViewModel, etNavController, mainNavigation, baseUrl)
    }

    private val mainActivityMenuHelper: MainActivityMenuHelper by lazy {
        MainActivityMenuHelper(
            menuInflater,
            navController,
            viewModel,
            supportFragmentManager
        ) {
            userViewModel.isNavigationToAccount = false
        }
    }

    private val mainActivityToolbarHelper: MainActivityToolbarHelper by lazy {
        MainActivityToolbarHelper(
            toolbarView,
            accountsDropDownManager,
            toolbarViewModel,
            this@MainActivity,
            mainActivityMenuHelper,
            toolbarTitleView,
            supportFragmentManager
        ) {
            userViewModel.isAuthenticated
        }
    }

    private val overlayFragmentHelper: OverlayFragmentHelper by lazy {
        OverlayFragmentHelper(supportFragmentManager, dialogsHelper)
    }

    private val destinationChangedListener = NavController.OnDestinationChangedListener { _, destination, arguments ->
        appActionViewModel.updateDestination(destination)
        bottomNavigationViewModel.updateDestination(destination, arguments)
        toolbarViewModel.updateDestination(destination, arguments)
    }

    private val handler: Handler = Handler(Looper.getMainLooper())
    private var pendingAction: Runnable? = null

    private lateinit var navController: NavController
    private lateinit var etNavController: EtNavController

    override fun onCreate(savedInstanceState: Bundle?) {
        if (!isTaskRoot && intent.action == null) {
            finish() // this is a workaround for a bug when launcher creates new activity on top of the existing one
        }
        AndroidInjection.inject(this)
        supportFragmentManager.fragmentFactory = fragmentInjectionFactory
        setTheme(R.style.BaseAppTheme)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setSupportActionBar(toolbarView)
        supportActionBar?.setDisplayShowTitleEnabled(false) // Disable the default tool bar title text.
        setupNavigation()
        activityNetworkConnectionDelegate.initNetworkConnectivityBanner(this)
        biometricTimeoutMainActivityDelegate.init(this)
        overlayHolder.overlayManager = overlayFragmentHelper.toOverlayFragmentManager(binding.overlayContainer)
        streamingSessionDelegateHelper.init(this)
        if (!applicationContext.isApplicationDebuggable()) {
            hideContentOverlayDelegate.init(this)
        }
        darkModeChecker.registerWith(this)
        bindViewModels()
        if ((savedInstanceState == null) && intent.isNewIntent()) {
            appAccessProvider.requestAccess(lifecycleScope) {
                handler.post { mainActivityIntentHelper.onNewIntent(intent) }
            }
        }
        alertsCountBadgeHelper.registerWith(this, binding.bottomNavigation)
    }

    override fun onStart() {
        super.onStart()
        loginChecker.check(this)
    }

    override fun onResume() {
        super.onResume()
        appAccessProvider.requestAccess(lifecycleScope) {
            mainActivityIntentHelper.onIntent(intent)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cancelPendingAction()
    }

    override fun onNewIntent(newIntent: Intent?) {
        super.onNewIntent(newIntent)
        appAccessProvider.requestAccess(lifecycleScope) {
            newIntent?.let(mainActivityIntentHelper::onNewIntent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean = mainActivityMenuHelper.inflate(menu)

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        toolbarActionStartView?.clearTextAndListener()
        toolbarActionEndView?.clearTextAndListener()
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return mainActivityMenuHelper.onOptionsItemSelected(item) {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean = navController.navigateUp()

    private fun setupNavigation() {
        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        etNavController = EtNavController(navController, userViewModel, landingRepo, landingRouter, appActionViewModel)
        addNavControllerDestinationListeners()
        NavigationUI.setupWithNavController(binding.bottomNavigation, navController)
        binding.bottomNavigation.apply {
            setOnItemSelectedListener {
                bottomNavigationViewModel.selectItem(selectedItemId, it, navController)
            }
        }
        binding.bottomNavigation.setOnItemReselectedListener {
            bottomNavigationViewModel.reselectItem(it)
        }
    }

    private fun addNavControllerDestinationListeners() {
        navController.apply {
            addOnDestinationChangedListener(AppBarDestinationChangedListener(this@MainActivity, toolbarViewModel))
            addOnDestinationChangedListener(destinationChangedListener)
            (landingRouter as? NavController.OnDestinationChangedListener)?.let {
                addOnDestinationChangedListener(it)
            }
            addOnDestinationChangedListener(navScopedStorageRegisterer)
        }
    }

    private fun bindViewModels() {
        bindAccountsOrganizeViewModel()
        bindBottomNavigationViewModel()
        bindLandingSelectionViewModel()
        bindSessionTerminatedViewModel()
        mainActivityToolbarHelper.bindToolbarViewModel()
        bindUserViewModel()

        viewModelDelegates.forEach {
            it.observe(this)
        }

        appReviewViewModel.showAppReview.observe(this, Observer { it.consume { appReviewViewModel.launchReview(this) } })

        alertsSharedViewModel.inEditMode.observeBy(this) {
            binding.bottomNavigation.visibility =
                if (it) {
                    View.GONE
                } else {
                    View.VISIBLE
                }
        }
    }

    private fun bindSessionTerminatedViewModel() {
        sharedSessionTerminatedViewModel.showSessionTerminatedPage.observe { (show, callbacks) ->
            overlayFragmentHelper.onOverlayFragment(
                show, binding.overlayContainer,
                {
                    supportFragmentManager.fragmentFactory
                        .instantiateFragment(SessionTerminatedFragment::class.java)
                        .also { (it as SessionDialogFragment).onButtonClickListener = callbacks }
                },
                tag = SessionDialogFragment.TAG
            )
        }
    }

    private fun bindAccountsOrganizeViewModel() {
        accountsOrganizeViewModel.showAccountsOrganizeBoarding.observe {
            if (it.show || overlayFragmentHelper.isCurrentFragment(AccountsOrganizeOnboardingFragment.TAG)) {
                overlayFragmentHelper.onOverlayFragment(
                    it.show, binding.overlayContainer,
                    {
                        supportFragmentManager.fragmentFactory
                            .instantiateFragment(AccountsOrganizeOnboardingFragment::class.java)
                    },
                    it.showDialogs,
                    tag = AccountsOrganizeOnboardingFragment.Const.TAG
                )
            }
        }
    }

    private fun bindBottomNavigationViewModel() {
        bottomNavigationViewModel.apply {
            accountsItemSignal.observe { event ->
                event.consumeIfNot(isFinishing) {
                    val currentFragment = supportFragmentManager.getCurrentNavigationFragment()
                    if (currentFragment is OnAccountsClickListener) {
                        currentFragment.onAccountsClick()
                    }
                }
            }
            checkedItem.observe { resourceId ->
                resourceId?.let { binding.bottomNavigation.menu.findItem(it) }?.isChecked = true
            }
            isVisible.observe { visible ->
                cancelPendingAction()
                if (visible) {
                    binding.bottomNavigation.visibility = View.VISIBLE
                } else {
                    // Slight delay is needed to prevent jump of content on a previous destination, waiting for the current destination to be displayed.
                    pendingAction = handler.postDelayed(BOTTOM_BAR_HIDE_DELAY_MS) {
                        binding.bottomNavigation.visibility = View.GONE
                        pendingAction = null
                    }
                }
            }
        }
    }

    private fun bindLandingSelectionViewModel() {
        landingSelectionViewModel.apply {
            showLandingPageSelection.observe {
                if (it || overlayFragmentHelper.isCurrentFragment(LandingPageSelectionFragment.TAG)) {
                    overlayFragmentHelper.onOverlayFragment(
                        it, binding.overlayContainer,
                        {
                            supportFragmentManager.fragmentFactory
                                .instantiateFragment(LandingPageSelectionFragment::class.java)
                        },
                        tag = LandingPageSelectionFragment.Const.TAG
                    )
                }
            }
        }
    }

    private fun bindUserViewModel() {
        userViewModel.apply {
            loginAbortedSignal.observe(::onLoginAbortedSignal)
            userSessionState.observe(::onUserSessionState)
        }
    }

    private fun onLoginAbortedSignal(signal: ConsumableLiveEvent<Unit>) {
        signal.consumeIfNot(isFinishing) {
            if (etNavController.isIsolatedLoginRequiringDestination(pendingNavDirections?.navDirections)) {
                finish()
            }
            mainActivityIntentHelper.handleCanceledAppAction()
            etNavController.apply {
                clearPendingDirections()
                clearPendingAppActions()
            }
        }
    }

    private fun onUserSessionState(signal: ConsumableLiveEvent<SessionStateChange>) {
        signal.consumeIfNot(isFinishing) { state ->
            onUserSessionStateChange(state)
        }
    }

    private fun onUserSessionStateChange(nonConsumedContent: SessionStateChange) {
        viewModel.userSessionStateChange(nonConsumedContent)

        if (nonConsumedContent == SessionStateChange.UNAUTHENTICATED) {
            biometricTimeoutMainActivityDelegate.init(this)
            biometricTimeoutMainActivityDelegate.authenticate()
        }

        accountsOrganizeViewModel.userSessionStateChange(nonConsumedContent)
        landingSelectionViewModel.userSessionStateChange(nonConsumedContent)
    }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        clearFocusOnTouchOutside(ev)
        return tooltipViewModel.dispatchTouchEvent(ev) ?: super.dispatchTouchEvent(ev)
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        applicationLifecycleObserver.onUserInteraction(this)
    }

    private fun cancelPendingAction() {
        pendingAction?.let {
            handler.removeCallbacks(it)
            pendingAction = null
        }
    }

    private fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) = observe(this@MainActivity, Observer { onChanged(it) })

    private inner class NavRouterSignalViewDelegate(
        navRouterSignalViewModel: NavRouterSignalViewModel
    ) : NavigationViewDelegate<NavRouter>(etNavController, navRouterSignalViewModel.navRouterSignal)

    private inner class NavigationSignalViewDelegate(
        navigationSignalViewModel: NavigationSignalViewModel
    ) : NavigationViewDelegate<NavController>(navController, navigationSignalViewModel.navigationSignal)

    private open inner class NavigationViewDelegate<T>(
        private val router: T,
        private val navigationSignal: LiveData<ConsumableLiveEvent<(T) -> Unit>>
    ) : ViewDelegate {

        final override fun observe(owner: LifecycleOwner) {
            navigationSignal.observe(
                owner,
                Observer { event ->
                    event.consumeIfNot(isFinishing) { navigate ->
                        navigate(router)
                        pendingNavUri = null
                    }
                }
            )
        }
    }
}

private fun Intent.isNewIntent(): Boolean =
    (flags and Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY != 0) or (!action.isNullOrEmpty())

private fun TextView.clearTextAndListener() {
    text = null
    setOnClickListener(null)
    visibility = View.GONE
}
