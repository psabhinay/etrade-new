package com.etrade.mobilepro.messagecenter.data.mapper

import com.etrade.mobilepro.messagecenter.api.model.ReferenceBook
import com.etrade.mobilepro.messagecenter.data.dto.ReferenceBookResponseDto
import com.etrade.mobilepro.util.json.coerceToBool
import kotlinx.collections.immutable.toImmutableList

private const val EMPTY_VALUE = "--"

@Suppress("LongMethod") // it is simple and straightforward
internal fun ReferenceBookResponseDto.toReferenceBook(accountIdToDescMap: Map<String, String>): ReferenceBook =
    ReferenceBook(
        accounts = (
            this.accounts
                ?.filterNotNull()
                ?.filter { it.accountId != null }
                ?.map {
                    val shortDesc = accountIdToDescMap[it.accountId] ?: it.name ?: EMPTY_VALUE
                    ReferenceBook.Account(
                        id = it.accountId ?: "",
                        name = it.name ?: EMPTY_VALUE,
                        shortDescription = shortDesc,
                        acctNo = it.accountId ?: EMPTY_VALUE,
                        instNo = it.accountInstNo ?: EMPTY_VALUE,
                        instName = it.productCd ?: EMPTY_VALUE
                    )
                } ?: emptyList()
            ).toImmutableList(),
        subjects = (
            this.subjects
                ?.filterNotNull()
                ?.filter {
                    it.id != null
                }
                ?.map { subjectDto ->
                    ReferenceBook.Subject(
                        id = subjectDto.id ?: "",
                        desc = subjectDto.desc ?: EMPTY_VALUE,
                        isActive = coerceToBool(subjectDto.isActive),
                        topics = (
                            subjectDto.topics
                                ?.filterNotNull()
                                ?.filter {
                                    it.topicId != null
                                }
                                ?.map { topicDto ->
                                    ReferenceBook.Topic(
                                        id = topicDto.topicId ?: "",
                                        subjectId = subjectDto.id ?: "",
                                        desc = topicDto.desc ?: EMPTY_VALUE,
                                        isActive = coerceToBool(topicDto.isActive)
                                    )
                                } ?: emptyList()
                            ).toImmutableList()
                    )
                } ?: emptyList()
            ).toImmutableList()
    )
