package com.etrade.mobilepro.alerts

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.alerts.presentation.adapter.AlertsAdapter
import com.etrade.mobilepro.alerts.presentation.base.AlertTab
import com.etrade.mobilepro.alerts.presentation.base.BaseAlertsFragment
import com.etrade.mobilepro.alerts.presentation.base.BaseAlertsViewModel.ViewEffects
import com.etrade.mobilepro.alerts.presentation.market.MarketAlertsViewModel
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.databinding.FragmentAlertsMarketBinding
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.android.snackbar.showIfNotConsumed
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@RequireLogin
class MarketAlertsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    readStatusRepo: ReadStatusRepo
) : BaseAlertsFragment(viewModelFactory, R.layout.fragment_alerts_market, readStatusRepo) {

    private val binding by viewBinding(FragmentAlertsMarketBinding::bind)

    override val alertTab: AlertTab = AlertTab.MARKET
    override val viewModel: MarketAlertsViewModel by navGraphViewModels(R.id.mainGraph) { viewModelFactory }
    override val snackbarUtil: SnackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { activity?.findViewById(R.id.nav_host_fragment) }
        )
    }
    private var snackBar: Snackbar? = null
    override fun provideAlertsRecyclerView(): RecyclerView = binding.alertsContainer.alertsRv

    override fun provideDeletionLiveEvent(): LiveData<String> = sharedViewModel.deletedMarketAlert

    override fun setupFragment() {
        binding.setAlertBtn.setOnClickListener {
            findNavController().navigate(R.id.setAlertFragment)
        }
        binding.manageAlertsBtn.setOnClickListener {
            findNavController().navigate(R.id.manageAlertFragment)
        }
        binding.alertsContainer.root.applyColorScheme()
    }

    override fun bindViewModel(adapter: AlertsAdapter) {
        binding.alertsContainer.root.setOnRefreshListener {
            adapter.markDirty()
            viewModel.loadMore(clearAlreadyLoadedItems = true)
        }
        viewModel.viewState.observeBy(viewLifecycleOwner) {
            handleViewState(it)
        }
        viewModel.viewEffects.observeBy(viewLifecycleOwner) {
            handleViewEffects(it)
        }

        with(sharedViewModel) {
            alertWasSet.observeBy(viewLifecycleOwner) {
                snackbarUtil.showIfNotConsumed(it)
            }
            inEditMode.observeBy(viewLifecycleOwner) {
                updateForEditableMode(it)
            }
        }
    }

    override fun updateForEditableMode(isEditable: Boolean) {
        super.updateForEditableMode(isEditable)
        with(binding) {
            viewModel.updateHeaderItems(!isEditable)

            if (isEditable) {
                manageAlertsBtn.visibility = View.GONE
                btnDivider.visibility = View.GONE
                setAlertBtn.visibility = View.GONE
                marketAlertsHeaderLayoutDivider.visibility = View.GONE
            } else {
                manageAlertsBtn.visibility = View.VISIBLE
                btnDivider.visibility = View.VISIBLE
                setAlertBtn.visibility = View.VISIBLE
                marketAlertsHeaderLayoutDivider.visibility = View.VISIBLE
            }
        }
    }

    @Suppress("LongMethod")
    private fun handleViewState(it: MarketAlertsViewModel.ViewState) = when (it) {
        is MarketAlertsViewModel.ViewState.Loading -> {
            if (!binding.alertsContainer.root.isRefreshing) {
                setLoadingVisibility(true)
            }
            snackBar?.dismiss()
        }
        is MarketAlertsViewModel.ViewState.Success -> {
            binding.alertsContainer.root.isRefreshing = false
            setLoadingVisibility(false)
            sharedViewModel.inEditMode.value?.let { updateForEditableMode(it) }
            updateAlertsAdapter(it.alerts)
            binding.alertsContainer.noAlerts.visibility = if (it.showNoAlertsWarning) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
        is MarketAlertsViewModel.ViewState.Error.LoadingError -> {
            binding.alertsContainer.root.isRefreshing = false
            setLoadingVisibility(false)
            viewModel.loadMore(clearAlreadyLoadedItems = it.wasIntendedToClearItems)
        }
        is MarketAlertsViewModel.ViewState.Error.DeletionError -> {
            setLoadingVisibility(false)
            viewModel.deleteAlert(it.alert)
            updateAlertsAdapter(it.alerts)
        }
    }

    private fun setLoadingVisibility(isVisible: Boolean) {
        if (isVisible) {
            binding.manageableAlertsLoadingPb.show()
        } else {
            binding.manageableAlertsLoadingPb.hide()
        }
    }

    private fun handleViewEffects(effect: ViewEffects) {
        when (effect) {
            ViewEffects.AlertsWereDeleted -> {
                snackBar = snackbarUtil.snackbar(
                    getString(R.string.alerts_were_removed), Snackbar.LENGTH_SHORT
                )?.apply { show() }
            }
            ViewEffects.AlertDeleteFailed -> {
                snackBar = snackbarUtil.snackbar(
                    getString(R.string.alert_delete_failure), Snackbar.LENGTH_SHORT
                )?.apply { show() }
            }
            ViewEffects.LoadingFailed -> {
                snackBar = snackbarUtil.snackbar(
                    getString(R.string.loading_alerts_failed), Snackbar.LENGTH_SHORT
                )?.apply { show() }
            }
        }
    }
}
