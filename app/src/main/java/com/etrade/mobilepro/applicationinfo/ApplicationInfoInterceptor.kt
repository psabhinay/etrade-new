package com.etrade.mobilepro.applicationinfo

import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import okhttp3.Interceptor
import okhttp3.Response

class ApplicationInfoInterceptor(private val applicationInfo: ApplicationInfo) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()

        val userAgentName = "User-Agent"
        var existingUserAgentContent = originalRequest.header(userAgentName)
        if (!existingUserAgentContent.isNullOrBlank()) {
            existingUserAgentContent = existingUserAgentContent.plus(" ")
        } else {
            existingUserAgentContent = ""
        }

        return chain.proceed(
            originalRequest.newBuilder()
                // Bill-pay requires appversion and deviceId in header
                .addHeader("AppVersion", applicationInfo.appVersionName())
                // Check Deposit needs DevicePlatform for IRA
                .addHeader("DevicePlatform", applicationInfo.deviceType)
                .addHeader("DeviceID", applicationInfo.deviceId())
                .addHeader(userAgentName, existingUserAgentContent.plus(applicationInfo.toString()))
                .build()
        )
    }
}
