package com.etrade.mobilepro.appwidget.widgetcontainer

import android.content.Context
import android.graphics.Color
import android.widget.RemoteViews
import com.etrade.mobilepro.appwidget.R
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.common.getAdapterIntent
import com.etrade.mobilepro.appwidget.common.getBroadcastIntentWith
import com.etrade.mobilepro.appwidget.common.getLayoutResId

private const val WIDGET_BG_COLOR = "#131a2c"
private const val HEX = 16

internal class WidgetRemoteViewsFactory(
    private val context: Context,
    private val widgetSettings: WidgetSettings
) {
    fun create() = RemoteViews(context.packageName, R.layout.appwidget_widget_layout).apply {
        setupIndicesAdapter()
        setupWatchlistAdapter()
        setupWidgetTransparency()
    }

    private fun RemoteViews.setupIndicesAdapter() {
        val adapterIntent = getAdapterIntent(context, AdapterType.Indices, widgetSettings.layout)
        setRemoteAdapter(R.id.lvIndices, adapterIntent)
    }

    private fun RemoteViews.setupWatchlistAdapter() {
        val adapterIntent = getAdapterIntent(context, AdapterType.Watchlist, widgetSettings.layout)
        setRemoteAdapter(getLayoutResId(widgetSettings.layout), adapterIntent)

        val pendingIntent = context.getBroadcastIntentWith(isMutable = true) {
            action = ACTION_QUOTE_LOOKUP
        }
        setPendingIntentTemplate(getLayoutResId(widgetSettings.layout), pendingIntent)
    }

    private fun RemoteViews.setupWidgetTransparency() {
        val alpha = widgetSettings.opacityAlpha.toString(HEX).run { if (length == 1) { "0$this" } else { this } }
        val color = WIDGET_BG_COLOR.replace("#", "#$alpha")
        setInt(R.id.widgetContainer, "setBackgroundColor", Color.parseColor(color))
    }
}
