package com.etrade.mobilepro.billpay.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class EnrollmentDetailsResponseDto(
    @Json(name = "data")
    val dataDto: DataDto
)

@JsonClass(generateAdapter = true)
class DataDto(
    @Json(name = "billPayEnrollmentDisplay")
    val enrollmentDisplayDto: EnrollmentDisplayDto
)

@JsonClass(generateAdapter = true)
class EnrollmentDisplayDto(
    @Json(name = "accountsList")
    val accountList: List<AccountDto>
)
