package com.etrade.mobilepro.checkdeposit.presentation

import androidx.annotation.StringRes

enum class DepositStatus(val value: String, @StringRes val label: Int) {

    IN_PROCESS("INPROCESS", R.string.deposit_status_in_process),
    COMPLETED("COMPLETED", R.string.deposit_status_complete),
    REJECTED("", R.string.deposit_status_rejected);

    companion object {
        @StringRes
        fun labelFromValue(value: String): Int {
            return (values().find { it.value == value } ?: REJECTED).label
        }
    }
}
