package com.etrade.mobilepro.portfolio

import android.view.View
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.core.content.ContextCompat
import com.etrade.mobilepro.R
import com.etrade.mobilepro.instrument.formatToShortSymbol
import com.etrade.mobilepro.instrument.getExpirationDateFromOsiKey
import com.etrade.mobilepro.instrument.getOptionType
import com.etrade.mobilepro.instrument.getStrikePrice
import com.etrade.mobilepro.instrument.isSymbolLongOption
import com.etrade.mobilepro.instrument.toLocalDate
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableRow
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.android.accessibility.processAccessibilityStreamingEvent
import com.etrade.mobilepro.util.formatCurrencyAmount
import com.etrade.mobilepro.util.formatters.SHORT_DATE_FORMAT
import com.etrade.mobilepro.util.removePercent
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.etrade.mobilepro.util.toMonthAccessibility
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

class PortfolioTileDataCellViewHolder(itemView: View) : AbstractViewHolder(itemView) {

    private val context get() = itemView.context

    private val symbol = textView(R.id.movers_symbol)
    private val price = textView(R.id.movers_price)
    private val gain = textView(R.id.movers_gain)
    private val gainPercentage = textView(R.id.movers_gain_percentage)
    private val optionExpirationDate = textView(R.id.movers_option_date)
    private val optionsStrikePrice = textView(R.id.movers_option_type_strikeprice)
    private val backgroundContainer = itemView.findViewById<View>(R.id.movers_background)
    private val accessibilityFocusedView: View = itemView.apply {
        // for accessibility purpose
        id = View.generateViewId()
    }

    @Suppress("LongMethod")
    fun bind(values: List<String>, row: PortfolioRecordsTableRow, columnMap: Map<PortfolioColumn, Int>) {
        val symbolText = formatToShortSymbol(row.rowHeader.symbol, row.rowHeader.displayText)
        symbol.text = symbolText

        val lastPrice = values[columnMap[PortfolioColumn.LAST_PRICE]!!]
        price.text = lastPrice

        val daysGainValueDollar = values[columnMap[PortfolioColumn.DAYS_GAIN_DOLLAR]!!]
        gain.text = daysGainValueDollar

        val daysGainValuePercent = values[columnMap[PortfolioColumn.DAYS_GAIN_PERCENT]!!]
        gainPercentage.text = daysGainValuePercent

        var contentDescription = "${symbolText.characterByCharacter()}, "
        if (isSymbolLongOption(row.rowHeader.symbol)) {
            setOptionsText(row.rowHeader.symbol)
            contentDescription +=
                "${optionExpirationDate.contentDescription}, " +
                "${optionsStrikePrice.contentDescription}, "
        } else {
            optionExpirationDate.text = ""
            optionsStrikePrice.text = ""
        }
        contentDescription += "${context.getString(R.string.label_last_price, lastPrice)}, " +
            "${context.getString(
                R.string.label_days_change_dollars_and_percent,
                daysGainValueDollar.formatCurrencyAmount(),
                daysGainValuePercent.removePercent()
            )}, button"

        accessibilityFocusedView.processAccessibilityStreamingEvent(
            accessibilityText = contentDescription,
            accessibilityUserTouchText = contentDescription
        )
        applyBackgroundColor(daysGainValueDollar)
    }

    private fun setOptionsText(symbol: String) {
        val expirationDate = getExpirationDateFromOsiKey(symbol)
        optionExpirationDate.text = SHORT_DATE_FORMAT.format(expirationDate.toLocalDate())
        optionExpirationDate.contentDescription = expirationDate.let {
            "${it.month.toMonthAccessibility()} ${it.day} ${it.year}"
        }
        val strikePrice = "$${getStrikePrice(symbol)}"
        val optionType = getOptionType(symbol)
        val strikePriceAndType = "$strikePrice${optionType?.responseCode}"
        optionsStrikePrice.text = strikePriceAndType
        optionsStrikePrice.contentDescription = "$strikePrice ${optionType?.name}"
    }

    private fun applyBackgroundColor(value: String) {
        val bigDecimal = value.safeParseBigDecimal() ?: return
        val drawable = when (bigDecimal.signum()) {
            -1 -> ContextCompat.getDrawable(context, R.drawable.mover_item_bg_red)
            else -> ContextCompat.getDrawable(context, R.drawable.mover_item_bg_green)
        }
        backgroundContainer.background = drawable
    }

    private fun textView(@IdRes resId: Int) = itemView.findViewById<TextView>(resId)
}
