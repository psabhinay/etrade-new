package com.etrade.mobilepro.ui_demo.footnote

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.bold
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import androidx.core.text.italic
import androidx.core.text.strikeThrough
import androidx.core.text.underline
import com.etrade.mobilepro.ui_demo.R
import com.etrade.mobilepro.ui_demo.databinding.ActivityFootnoteBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.footnotespan.FootnoteSectionContext
import com.etrade.mobilepro.util.android.footnotespan.buildFootnote
import com.etrade.mobilepro.util.android.footnotespan.footnoteSection
import com.etrade.mobilepro.util.android.textutil.scaleSuperscript

class FootnoteActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityFootnoteBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)

        binding.footnotes = footnoteSection {
            footnote("a") {
                append("Footnotes now support spannable sections such as ")
                bold { append("bold, ") }
                italic { append("italic, ") }
                bold { italic { append("bold italic, ") } }
                strikeThrough { append("strikeThrough, ") }
                append("superscript")
                scaleSuperscript { append("viii") }
                append(", ")
                underline { append("etc.") }
            }
            footnote("i") { append(getString(R.string.lorem1)) }
            footnote("ii") { append(getString(R.string.lorem2)) }
            footnote("iii") { append(getString(R.string.lorem3)) }
            footnote("iv") { append(getString(R.string.lorem4)) }
            footnote("v") { append(getString(R.string.lorem1)) }
            footnote("vi") { append(getString(R.string.lorem2)) }
            footnote("vii") { append(getString(R.string.lorem3)) }
            footnote("viii") { append(getString(R.string.lorem4)) }
        }

        val footnoteContext = FootnoteSectionContext(") ")
        with(binding.footView1) {
            text = footnoteContext.buildFootnote(
                this,
                "A",
                "Footnotes can also be added to different TextViews."
            )
        }
        with(binding.footView2) {
            text = footnoteContext.buildFootnote(
                this,
                "BB",
                "To be aligned, they must be created with the same FootnoteSectionContext."
            )
        }
        with(binding.footView3) {
            text = footnoteContext.buildFootnote(
                this,
                "CC",
                "The TextViews can even have different sizes."
            )
        }
        with(binding.footView4) {
            text = footnoteContext.buildFootnote(
                this,
                "DDD",
                getString(R.string.lorem4)
            )
        }
    }
}
