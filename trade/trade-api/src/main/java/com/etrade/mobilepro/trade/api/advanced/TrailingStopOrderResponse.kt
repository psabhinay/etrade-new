package com.etrade.mobilepro.trade.api.advanced

interface TrailingStopOrderResponse {
    val trailingStopOrderDetails: List<TrailingStopOrderDetails>
}
