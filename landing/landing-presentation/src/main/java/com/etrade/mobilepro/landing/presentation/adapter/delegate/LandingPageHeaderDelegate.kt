package com.etrade.mobilepro.landing.presentation.adapter.delegate

import android.view.LayoutInflater
import android.view.ViewGroup
import com.etrade.mobilepro.landing.presentation.adapter.LandingPageHeader
import com.etrade.mobilepro.landing.presentation.adapter.LandingPageSelectionItem
import com.etrade.mobilepro.landing.presentation.adapter.viewholder.LandingPageHeaderViewHolder
import com.etrade.mobilepro.landing.presentation.databinding.LandingPageSelectionItemHeaderBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class LandingPageHeaderDelegate : AbsListItemAdapterDelegate<LandingPageHeader, LandingPageSelectionItem, LandingPageHeaderViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): LandingPageHeaderViewHolder {
        val binding = LandingPageSelectionItemHeaderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LandingPageHeaderViewHolder(binding)
    }

    override fun isForViewType(item: LandingPageSelectionItem, items: MutableList<LandingPageSelectionItem>, position: Int): Boolean =
        item is LandingPageHeader

    override fun onBindViewHolder(item: LandingPageHeader, holder: LandingPageHeaderViewHolder, payloads: MutableList<Any>) =
        holder.bindTo(item)
}
