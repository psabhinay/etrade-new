package com.etrade.mobilepro.login.api.repo

sealed class LoginResult

data class LoginSuccess(
    val userId: String,
    val isInternationalUser: Boolean,
    val isRsaUser: Boolean,
    val isRealTimeUser: Boolean,
    val sessionResultMessage: String? = null
) : LoginResult()

data class LoginFailure(
    val loginResultCode: LoginResultCode = LoginResultCode.UNKNOWN,
    val sessionResultMessage: String? = null
) : LoginResult()
