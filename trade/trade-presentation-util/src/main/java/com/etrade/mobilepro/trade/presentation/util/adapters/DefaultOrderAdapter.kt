package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrder
import com.etrade.mobilepro.trade.api.option.OptionOrder
import com.etrade.mobilepro.trade.api.stock.StockOrder
import com.etrade.mobilepro.trade.presentation.util.OrderDisplayItem
import javax.inject.Inject

class DefaultOrderAdapter @Inject constructor(val resources: Resources) : OrderAdapter {

    private val confirmedOrderAdapterDelegate: OrderAdapterDelegate = ConfirmedOrderAdapterDelegate(resources)
    private val withCostOrderAdapterDelegate: OrderAdapterDelegate = WithCostOrderAdapterDelegate(resources)
    private val withPriceOrderAdapterDelegate: OrderAdapterDelegate = WithPriceOrderAdapterDelegate(resources)
    private val withOtcTierOrderAdapterDelegate: OrderAdapterDelegate = WithOtcTierOrderAdapterDelegate(resources)

    private val mutualFundOrderAdapter: OrderAdapter = MutualFundOrderAdapter(resources, confirmedOrderAdapterDelegate)
    private val optionOrderAdapter: OrderAdapter =
        OptionOrderAdapter(
            resources = resources,
            confirmedOrderAdapterDelegate = confirmedOrderAdapterDelegate,
            withCostOrderAdapterDelegate = withCostOrderAdapterDelegate,
            withPriceOrderAdapterDelegate = withPriceOrderAdapterDelegate
        )
    private val stockOrderAdapter: OrderAdapter =
        StockOrderAdapter(
            resources = resources,
            confirmedOrderAdapterDelegate = confirmedOrderAdapterDelegate,
            withCostOrderAdapterDelegate = withCostOrderAdapterDelegate,
            withPriceOrderAdapterDelegate = withPriceOrderAdapterDelegate,
            withOtcTierAdapterDelegate = withOtcTierOrderAdapterDelegate
        )

    override fun createOrderDisplayItem(order: Order): OrderDisplayItem {
        return when (order) {
            is MutualFundsOrder -> mutualFundOrderAdapter
            is OptionOrder -> optionOrderAdapter
            is StockOrder -> stockOrderAdapter
            else -> throw UnsupportedOperationException("Order type is not supported: $order")
        }.createOrderDisplayItem(order)
    }
}
