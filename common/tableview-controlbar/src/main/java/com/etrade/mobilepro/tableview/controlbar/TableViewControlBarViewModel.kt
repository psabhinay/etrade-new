package com.etrade.mobilepro.tableview.controlbar

import androidx.lifecycle.LiveData

interface TableViewControlBarViewModel {
    val enableDropDown: Boolean
    val enableTableTypeSelection: Boolean
    val dropdownSelectedText: LiveData<String?>
    val tableTypeSelectedIndex: LiveData<Int>
    val enableCustomizeSettings: LiveData<Boolean>

    fun onCustomizeViewClick()
    fun onDropDownClick(onUnAuthFilterClickAction: (() -> Unit)?)
    fun onDropDownClick() = onDropDownClick(null) // view binding just won't consume the one above
    fun onTableTypeChange(index: Int)
    fun setDropdownSelectedText(selectedText: String?)
}
