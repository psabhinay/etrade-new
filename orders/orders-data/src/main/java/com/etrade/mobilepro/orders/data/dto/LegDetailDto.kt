package com.etrade.mobilepro.orders.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LegDetailDto(
    @Json(name = "averageFillPrice")
    val averageFillPrice: String,
    @Json(name = "cusip")
    val cusip: String,
    @Json(name = "displaySymbol")
    val displaySymbol: String,
    @Json(name = "fillCount")
    val fillCount: String,
    @Json(name = "filledQuantity")
    val filledQuantity: String,
    @Json(name = "orderAction")
    val orderAction: String,
    @Json(name = "positionEffect")
    val positionEffect: String,
    @Json(name = "qtyReserveShow")
    val qtyReserveShow: Int,
    @Json(name = "quantityType")
    val quantityType: String,
    @Json(name = "quantityValue")
    val quantityValue: String,
    @Json(name = "quoteAsk")
    val quoteAsk: String,
    @Json(name = "quoteBid")
    val quoteBid: String,
    @Json(name = "quotePrice")
    val quotePrice: String,
    @Json(name = "remainingQuantity")
    val remainingQuantity: String,
    @Json(name = "shortSaleAuthID")
    val shortSaleAuthID: String,
    @Json(name = "shortSaleCd")
    val shortSaleCd: String,
    @Json(name = "symbol")
    val symbol: String,
    @Json(name = "typeCode")
    val typeCode: String
)
