package com.etrade.mobilepro.trade.form.api.input

interface WithContingentOrderInputId : WithSymbolTradeFormInputId {
    val contingentOrderInputId: ContingentOrderInputId
}
