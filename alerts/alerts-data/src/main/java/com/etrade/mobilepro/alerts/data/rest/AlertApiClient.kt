package com.etrade.mobilepro.alerts.data.rest

import com.etrade.eo.rest.retrofit.Xml
import com.etrade.mobilepro.alerts.data.dto.AlertContentResponseDto
import com.etrade.mobilepro.alerts.data.dto.AlertsCountDto
import com.etrade.mobilepro.alerts.data.dto.AlertsCountRequest
import com.etrade.mobilepro.alerts.data.dto.DeleteAlertResponseXmlDto
import com.etrade.mobilepro.alerts.data.dto.InboxAlertsResponseDto
import com.etrade.mobilepro.alerts.data.dto.SetAlertResponseXmlDto
import com.etrade.mobilepro.alerts.data.dto.SetNewsAlertResponseXmlDto
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AlertApiClient {
    @JsonMoshi
    @FormUrlEncoded
    @POST("e/t/mobile/GetInboxAlerts.json")
    fun getInboxAlerts(
        @Field("UserId") userId: String,
        @Field("EventId") fromEventId: String,
        @Field("EventOccurredDt") occurredBefore: Long,
        @Field("RowsReq") maxCount: Int,
        @Field("Direction") direction: Int
    ): Single<InboxAlertsResponseDto>

    // TODO Rx version should be removed in scope of ETAND-5561 along with other refactorings required here
    @JsonMoshi
    @FormUrlEncoded
    @POST("e/t/mobile/GetInboxAlerts.json")
    suspend fun getInboxAlertsSuspend(
        @Field("UserId") userId: String,
        @Field("EventId") fromEventId: String,
        @Field("EventOccurredDt") occurredBefore: Long,
        @Field("RowsReq") maxCount: Int,
        @Field("Direction") direction: Int
    ): InboxAlertsResponseDto

    @Xml
    @FormUrlEncoded
    @POST("e/t/mobile/GetAlerts")
    fun getAlertContent(
        @Field("UserId") userId: String,
        @Field("MessageId") alertId: String,
        @Field("isWebView") isWebView: Boolean
    ): Single<AlertContentResponseDto>

    @Xml
    @FormUrlEncoded
    @POST("e/t/mobile/DeleteAlert")
    fun deleteAlert(@Field("UserId") userId: String, @Field("MessageId") alertId: String): Single<DeleteAlertResponseXmlDto>

    /**
     * First four params have unknown meanings and seem to be constant values.
     */
    @Xml
    @FormUrlEncoded
    @POST("e/t/mobile/setmobilealerts")
    fun setAlert(
        @Field("_formtarget") formTarget: String = "setmobilealerts",
        @Field("_formtarget_all_children") allChildren: String = "TRUE",
        @Field("addbutton.x") addButtonX: String = "19",
        @Field("sizefield") sizeField: String = "1",
        @Field("stk1") sessionToken: String,
        @Field("editsymbol0") symbol: String,
        @Field("editpvl0") targetPriceLow: String,
        @Field("editpvh0") targetPriceHigh: String,
        @Field("editpcl0") dailyPercentLow: String,
        @Field("editpch0") dailyPercentHigh: String,
        @Field("editpel0") peTargetLow: String,
        @Field("editpeh0") peTargetHigh: String,
        @Field("editvol0") dailyVolume: String
    ): Single<SetAlertResponseXmlDto>

    /**
     * Sets news alert.
     * Params are boolean values in Int representation (1 for true, 0 for false). They determine, which switches for news alert were set.
     */
    @Xml
    @FormUrlEncoded
    @POST("e/t/mobile/setmobilenewsalerts")
    fun setNewsAlert(
        @Field("_formtarget") formTarget: String = "setmobilenewsalerts",
        @Field("_formtarget_all_children") allChildren: String = "TRUE",
        @Field("addbutton.x") addButtonX: String = "9",
        @Field("sizefield") sizeField: String = "1",
        @Field("stk1") sessionToken: String,
        @Field("editsymbol0") symbol: String,
        @FieldMap switchParams: Map<String, Int>
    ): Single<SetNewsAlertResponseXmlDto>

    @Xml
    @FormUrlEncoded
    @POST("e/t/mobile/setmobilealerts")
    fun getAlerts(@Field("Send") send: String = "Send"): Single<SetAlertResponseXmlDto>

    @Xml
    @FormUrlEncoded
    @POST("e/t/mobile/setmobilenewsalerts")
    fun getNewsAlerts(@Field("Send") send: String = "Send"): Single<SetNewsAlertResponseXmlDto>

    /**
     * First five params have unknown meanings and seem to be constant values.
     */
    @Xml
    @FormUrlEncoded
    @POST("e/t/mobile/setmobilealerts")
    fun deleteUserAlert(
        @Field("_formtarget") formTarget: String = "setmobilealerts",
        @Field("_formtarget_all_children") allChildren: String = "TRUE",
        @Field("deletebutton.x") deleteButtonX: String = "8",
        @Field("deletebutton.y") deleteButtonY: String = "23",
        @Field("sizefield") sizeField: String = "1",
        @Field("chkbox") deleteInfo: String
    ): Single<SetAlertResponseXmlDto>

    /**
     * First five params have unknown meanings and seem to be constant values.
     */
    @Xml
    @FormUrlEncoded
    @POST("e/t/mobile/setmobilenewsalerts")
    fun deleteUserNewsAlert(
        @Field("_formtarget") formTarget: String = "setmobilenewsalerts",
        @Field("_formtarget_all_children") allChildren: String = "TRUE",
        @Field("deletebutton.x") deleteButtonX: String = "8",
        @Field("deletebutton.y") deleteButtonY: String = "23",
        @Field("sizefield") sizeField: String = "1",
        @Field("chkbox") deleteInfo: String
    ): Single<SetNewsAlertResponseXmlDto>

    /**
     * First five params have unknown meanings and seem to be constant values.
     */
    @Xml
    @FormUrlEncoded
    @POST("e/t/mobile/setmobilealerts")
    fun updateUserAlert(
        @Field("_formtarget") formTarget: String = "setmobilealerts",
        @Field("_formtarget_all_children") allChildren: String = "TRUE",
        @Field("addbutton.x") deleteButtonX: String = "13",
        @Field("addebutton.y") deleteButtonY: String = "9",
        @Field("sizefield") sizeField: String = "1",
        @Field("stk1") sessionToken: String,
        @Field("delete_info0") deleteInfo: String,
        @Field("edit_string0") edit: String,
        @Field("lastedited0") lastEdited: String,
        @Field("editsymbol0") symbol: String,
        @Field("editpvl0") targetPriceLow: String,
        @Field("editpvh0") targetPriceHigh: String,
        @Field("editpcl0") dailyPercentLow: String,
        @Field("editpch0") dailyPercentHigh: String,
        @Field("editpel0") peTargetLow: String,
        @Field("editpeh0") peTargetHigh: String,
        @Field("editvol0") dailyVolume: String
    ): Single<SetAlertResponseXmlDto>

    /**
     * First four params and pdFlag have unknown meanings and seem to be constant values.
     * Params are boolean values in nullable Int representation (1 for true, null for false). They determine, which switches for news alert were set
     */
    @Xml
    @FormUrlEncoded
    @POST("e/t/mobile/setmobilenewsalerts")
    fun updateUserNewsAlert(
        @Field("_formtarget") formTarget: String = "setmobilenewsalerts",
        @Field("_formtarget_all_children") allChildren: String = "TRUE",
        @Field("addbutton.x") deleteButtonX: String = "19",
        @Field("sizefield") sizeField: String = "1",
        @Field("stk1") sessionToken: String,
        @Field("delete_info0") deleteInfo: String,
        @Field("edit_string0") edit: String,
        @Field("lastedited0") lastEdited: String,
        @Field("editsymbol0") symbol: String,
        @Field("editrat0") upDown: Int?,
        @Field("editipl0") inPlay: Int?,
        @Field("editern0") earning: Int?,
        @Field("editspl0") splitNotice: Int?,
        @Field("pd_flag0") pdFlag: Int = 0
    ): Single<SetNewsAlertResponseXmlDto>

    @JsonMoshi
    @POST("app/alerts/alertinbox/UNREAD.json")
    fun getAlertsCount(@JsonMoshi @Body request: AlertsCountRequest): Single<AlertsCountDto>
}
