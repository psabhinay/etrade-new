package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderHistoryItem
import com.etrade.mobilepro.order.details.mapper.OrderDetailDescriptionMapper
import com.etrade.mobilepro.order.details.mapper.OrderDetailsMapper
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.testutil.XmlDeserializer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class StockPlanOrderResponseMapperTest {

    @Test
    fun `test stock order mapper`() {
        val response = XmlDeserializer.getObjectFromXml("stock_plan_order_details.xml", StockPlanOrderDetailsResponseDto::class.java)
        val actual = OrderDetailsMapper(OrderDetailDescriptionMapper()::map).mapStockPlanOrderDetails(response, "AAPL")
        val expected = BaseOrderDetailsItem.StockPlanOrderDetailsItem(
            orderHistoryList = listOf(
                OrderHistoryItem(
                    price = "\$190.00",
                    transaction = "Order Placed",
                    sellQuantity = "4",
                    date = "01/02/2019 05:43:49 PM ET"
                )
            ),
            orderNumber = "4221810",
            orderType = "Sell Exercised Shares",
            orderStatus = OrderStatus.OPEN,
            description = "APPLE CORPORATION(AAPL)",
            term = "GTC",
            sharesExercised = "0",
            sharesSold = "4",
            sharesToExercise = "0",
            sharesToSell = "4",
            estSharesToSell = "4",
            gtdExpiration = "--",
            priceType = "Limit",
            limitPrice = "$190.00",
            grossProceeds = "\$760.00(USD)",
            estGrossProceeds = "\$760.00(USD)",
            totalPrice = "\$0.00",
            commission = "\$10.95",
            secFee = "\$0.00",
            brokerAssistFee = "\$0.00",
            disbursementFee = "\$0.00",
            taxesWithheld = "\$0.00",
            estNetProceeds = "(\$10.95)(USD)",
            netProceeds = "(\$10.95)(USD)",
            exerciseValue = "\$0.00",
            sharesToKeep = "-4",
            estSharesToKeep = "-4",
            sharesIssued = "-4",
            sharesToIssue = "-4",
            appreciationSharesValue = "0/\$0.00",
            estAppreciationSharesValue = "0/\$0.00",
            taxSharesValue = "0/\$0.00",
            estTaxSharesValue = "0/\$0.00",
            planPrice = "\$400.00",
            grantType = "Nonqual",
            planNumber = "OP07302015",
            planDate = "05/21/2012",
            vestPeriod = "0",
            costBasis = "105.005",
            planType = "D",
            symbol = "AAPL"
        )
        assertEquals(expected, actual)
    }
}
