package com.etrade.mobilepro.util.json;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;
import com.squareup.moshi.adapters.EnumJsonAdapter;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that the annotated enum has a fallback value. The fallback must be set via
 * {@link #name()}. If no enum constant with the provided name is declared in the annotated
 * enum type an {@linkplain AssertionError assertion error} will be thrown.
 *
 * <p>To leverage from {@link FallbackEnum} {@link FallbackEnum#ADAPTER_FACTORY} must be added to
 * your {@linkplain Moshi moshi instance}:
 *
 * <pre><code>
 *   Moshi moshi = new Moshi.Builder()
 *      .add(FallbackEnum.ADAPTER_FACTORY)
 *      .build();
 * </code></pre>
 */
@SuppressWarnings("unchecked")
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface FallbackEnum {
    String name();

    /** Builds an adapter that can process enums annotated with {@link FallbackEnum}. */
    JsonAdapter.Factory ADAPTER_FACTORY = (type, annotations, moshi) -> {
        if (!annotations.isEmpty()) return null;

        Class<?> rawType = Types.getRawType(type);
        if (rawType.isEnum()) {
            FallbackEnum annotation = rawType.getAnnotation(FallbackEnum.class);
            if (annotation == null) return null;

            Class<? extends Enum> enumType = (Class<? extends Enum>) rawType;

            return EnumJsonAdapter.create(enumType).withUnknownFallback(Enum.valueOf(enumType, annotation.name()));
        }

        return null;
    };
}
