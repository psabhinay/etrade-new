package com.etrade.mobilepro.trade.presentation.usecase

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.TradeRepo
import com.etrade.mobilepro.util.UseCase
import javax.inject.Inject

interface PreviewOrderUseCase : UseCase<PreviewOrderUseCaseParams, ETResult<TradeOrder>>

class PreviewOrderUseCaseImpl @Inject constructor(
    private val tradeRepo: TradeRepo
) : PreviewOrderUseCase {

    override suspend fun execute(parameter: PreviewOrderUseCaseParams): ETResult<TradeOrder> {
        return tradeRepo.previewOrder(parameter.values, changeOrder = parameter.changeOrder)
    }
}

data class PreviewOrderUseCaseParams(val values: InputValuesSnapshot, val changeOrder: Boolean = false)
