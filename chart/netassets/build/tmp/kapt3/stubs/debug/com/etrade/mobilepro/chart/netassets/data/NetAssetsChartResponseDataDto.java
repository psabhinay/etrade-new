package com.etrade.mobilepro.chart.netassets.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0001\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/data/NetAssetsChartResponseDataDto;", "", "overview", "Lcom/etrade/mobilepro/chart/data/ChartDto;", "(Lcom/etrade/mobilepro/chart/data/ChartDto;)V", "getOverview", "()Lcom/etrade/mobilepro/chart/data/ChartDto;", "netassets_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class NetAssetsChartResponseDataDto {
    @org.jetbrains.annotations.NotNull()
    private final com.etrade.mobilepro.chart.data.ChartDto overview = null;
    
    public NetAssetsChartResponseDataDto(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "overview")
    com.etrade.mobilepro.chart.data.ChartDto overview) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.chart.data.ChartDto getOverview() {
        return null;
    }
}