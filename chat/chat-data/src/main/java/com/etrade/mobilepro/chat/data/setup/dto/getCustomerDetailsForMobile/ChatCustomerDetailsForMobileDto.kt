package com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChatCustomerDetailsForMobileDto(

    @Json(name = "data")
    val data: ChatCustomerDetailsDataDto? = null
)

@JsonClass(generateAdapter = true)
data class ChatCustomerDetailsDataDto(
    @Json(name = "ChatCustomerDetails")
    val chatCustomerDetails: ChatCustomerDetailsDto? = null
)

@JsonClass(generateAdapter = true)
data class ChatCustomerDetailsDto(

    @Json(name = "userId")
    val userId: String? = "",

    @Json(name = "chatGeoLoc")
    val chatGeoLocationDto: ChatGeoLocationDto? = null,

    @Json(name = "chatCustAClist")
    val chatCustomerAccountList: List<ChatCustomerAccountListDto?> = emptyList()
)

@JsonClass(generateAdapter = true)
data class ChatGeoLocationDto(

    @Json(name = "city")
    val city: String? = null,

    @Json(name = "cityCode")
    val cityCode: String? = null,

    @Json(name = "connectionSpeed")
    val connectionSpeed: String? = null,

    @Json(name = "continentCode")
    val continentCode: String? = null,

    @Json(name = "country")
    val country: String? = null,

    @Json(name = "countryCode")
    val countryCode: String? = null,

    @Json(name = "domain")
    val domain: String? = null,

    @Json(name = "ipAddress")
    val ipAddress: String? = null,

    @Json(name = "isp")
    val isp: String? = null,

    @Json(name = "latitude")
    val latitude: String? = null,

    @Json(name = "longitude")
    val longitude: String? = null,

    @Json(name = "metroCode")
    val metroCode: String? = null,

    @Json(name = "postalCode")
    val postalCode: String? = null,

    @Json(name = "proxyType")
    val proxyType: String? = null,

    @Json(name = "region")
    val region: String? = null,

    @Json(name = "regionCode")
    val regionCode: String? = null,

    @Json(name = "twoLetterCountry")
    val twoLetterCountry: String? = null

)
