package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.OrderType.CONDITIONAL_ONE_TRIGGER_OTHER
import com.etrade.mobilepro.trade.data.fixLimitStopPriceParam
import com.etrade.mobilepro.trade.data.stock.resolvePrices
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithQuantity
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractPriceType
import com.etrade.mobilepro.trade.form.api.input.shared.WithStopLossInputId

internal class StopLossSaveOrderRequestBodyProvider<T>(
    private val inputId: T
) : BaseAdvancedSaveOrderRequestBodyProvider() where T : WithActionTradeFormInputId,
                                                     T : WithStopLossInputId,
                                                     T : WithSymbolTradeFormInputId,
                                                     T : WithPriceTradeFormInputId,
                                                     T : WithQuantity {

    override fun createBody(params: InputValuesSnapshot): InputValuesSnapshot {
        val body: MutableMap<String, Any?> = mutableMapOf()
        params.toMutableMap().apply {
            fixLimitStopPriceParam(inputId, inputId.extractPriceType(this))
            fixLimitStopPriceParam(inputId.stopLossInputId, inputId.stopLossInputId.extractPriceType(this))
            resolvePrices(this)
            body["orderGroupType"] = CONDITIONAL_ONE_TRIGGER_OTHER.typeCode
            body["accountId"] = this[inputId.accountId]
            body["orderSvcReqList"] = listOf(
                createOrder(this, inputId),
                createOrder(this, inputId.stopLossInputId)
            )
        }
        return mapOf("value" to body)
    }
}
