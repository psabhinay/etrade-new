package com.etrade.mobilepro.screener.data.datasource

import android.content.res.Resources
import com.etrade.mobilepro.screener.api.Region
import com.etrade.mobilepro.screener.api.datasource.RegionDataSource
import com.etrade.mobilepro.screener.data.R
import javax.inject.Inject

class DefaultRegionDataSource @Inject constructor(private val resources: Resources) : RegionDataSource {
    private val allRegions by lazy {
        Region(
            id = "",
            title = resources.getString(R.string.all_regions),
            isAllItemsElement = true
        )
    }

    private val regionsData by lazy {
        listOf(
            allRegions,
            Region(
                id = "AfricaMiddleEast",
                title = resources.getString(R.string.region_africa_middle_east)
            ),
            Region(
                id = "Australasia",
                title = resources.getString(R.string.region_australasia)
            ),
            Region(
                id = "AsiaDeveloped",
                title = resources.getString(R.string.region_asia_developed)
            ),
            Region(
                id = "AsiaEmerging",
                title = resources.getString(R.string.region_asia_emerging)
            ),
            Region(
                id = "EuropeDeveloped",
                title = resources.getString(R.string.region_europe_developed)
            ),
            Region(
                id = "EuropeEmerging",
                title = resources.getString(R.string.region_europe_emerging)
            ),
            Region(
                id = "JapanRegion",
                title = resources.getString(R.string.region_japan)
            ),
            Region(
                id = "LatinAmerica",
                title = resources.getString(R.string.region_latin_america)
            ),
            Region(
                id = "NorthAmerica",
                title = resources.getString(R.string.region_north_america)
            ),
            Region(
                id = "UnitedKingdomRegion",
                title = resources.getString(R.string.region_united_kingdom)
            )
        )
    }

    override fun getAllRegionsItem(): Region = allRegions

    override fun getRegions(): List<Region> = regionsData
}
