package com.etrade.mobilepro.billpay.presentation.paymentpreview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.PaymentResult
import com.etrade.mobilepro.tracking.SCREEN_TITLE_BILL_PAY_CONFIRMATION
import com.etrade.mobilepro.tracking.SCREEN_TITLE_BILL_PAY_PREVIEW
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.delegate.consumable
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch

class PaymentPreviewViewModel @AssistedInject constructor(
    private val repo: BillPayRepo,
    @Assisted private val sessionId: String,
    @Assisted private val paymentPreview: PaymentPreview,
    private val tracker: Tracker
) : ViewModel() {

    private val _viewState = MutableLiveData(PaymentPreviewViewState(isLoading = false))
    internal val viewState: LiveData<PaymentPreviewViewState> get() = _viewState

    @AssistedFactory
    interface Factory {
        fun create(
            sessionId: String,
            paymentPreview: PaymentPreview
        ): PaymentPreviewViewModel
    }

    init {
        tracker.screen(SCREEN_TITLE_BILL_PAY_PREVIEW)
    }

    internal fun schedulePayment() {
        if (viewState.value?.isLoading == true) return

        _viewState.value = PaymentPreviewViewState(isLoading = true)
        viewModelScope.launch {
            repo.schedulePayment(sessionId, paymentPreview.toRequest())
                .fold(
                    onSuccess = {
                        _viewState.value = PaymentPreviewViewState(
                            paymentResult = it
                        )

                        trackSubmitScreenIfApplicable(it)
                    },
                    onFailure = {
                        _viewState.value = PaymentPreviewViewState(
                            error = ConsumableLiveEvent(Unit)
                        )
                    }
                )
        }
    }

    internal fun isPaymentSuccessful(): Boolean = viewState.value?.paymentSucceed == true

    internal fun getSuccessOrNull(): PaymentResult.Success? = viewState.value?.paymentResult as? PaymentResult.Success

    private fun trackSubmitScreenIfApplicable(paymentResult: PaymentResult) {
        if (paymentResult is PaymentResult.Success) {
            tracker.screen(SCREEN_TITLE_BILL_PAY_CONFIRMATION)
        }
    }
}

internal data class PaymentPreviewViewState(
    val isLoading: Boolean = false,
    val error: ConsumableLiveEvent<Unit>? = null,
    val paymentResult: PaymentResult? = null
) {
    val paymentSucceed: Boolean get() = paymentResult is PaymentResult.Success
    val consumablePaymentResult: PaymentResult? by consumable(paymentResult)
}
