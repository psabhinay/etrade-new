package com.etrade.mobilepro.portfolio.taxlot

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDirections
import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dynamic.form.fragment.createFormValuesArgument
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.instrument.dao.InstrumentDao
import com.etrade.mobilepro.instrument.isSymbolLongOption
import com.etrade.mobilepro.livedata.concatCombineCoroutine
import com.etrade.mobilepro.orders.api.OrderAction
import com.etrade.mobilepro.portfolio.GetPortfolioTaxLotsParameter
import com.etrade.mobilepro.portfolio.GetPortfolioTaxLotsUseCase
import com.etrade.mobilepro.portfolio.PortfolioTableViewListener
import com.etrade.mobilepro.portfolio.PortfolioViewActionListener
import com.etrade.mobilepro.portfolio.applySort
import com.etrade.mobilepro.portfolio.data.merge
import com.etrade.mobilepro.portfolio.delegate.PositionSortDelegate
import com.etrade.mobilepro.portfolio.delegate.PositionSortDelegateFactory
import com.etrade.mobilepro.portfolio.extractBaseSymbolAndPrice
import com.etrade.mobilepro.portfolio.getTradeFormParams
import com.etrade.mobilepro.portfolio.getUpdatedInstrument
import com.etrade.mobilepro.portfolio.livedata.InstrumentsLiveData
import com.etrade.mobilepro.portfolio.livedata.PositionsLiveData
import com.etrade.mobilepro.portfolio.model.PositionId
import com.etrade.mobilepro.portfolio.optionFields
import com.etrade.mobilepro.portfolio.postClickEvent
import com.etrade.mobilepro.portfolio.stockFields
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableData
import com.etrade.mobilepro.portfolio.updateTableRows
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.positions.api.TaxLotPosition
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.SingleLiveEvent
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.invoke
import com.etrade.mobilepro.util.throttleLast
import com.evrencoskun.tableview.listener.ITableViewListener
import com.squareup.moshi.Moshi
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.concurrent.Executor
import javax.inject.Provider

const val MESSAGE_TAX_LOT_TRADE = 2

/**
 * Here is the flow that is implemented by this class in Mermaid (https://mermaidjs.github.io/) notation:
 * ```mermaid
 * graph LR
 * A -- Update --> taxLotsLiveData
 * A -- Update --> localSortInfo
 * taxLotsLiveData -- Update --> instrumentLiveData
 * instrumentLiveData -- Merge in --> positionRecordsLiveData
 * taxLotsLiveData -- Map to --> taxLotRecords
 * localSortInfo -- Apply to --> orderedPositionRecordsLiveData
 * tableData -- Merge in --> tableData (first record is a portfolio position, followed by Tax lots)
 * ```
 *  You may use StackEdit (https://stackedit.io/) to preview it.
 */

@Suppress("LongParameterList")
class PortfolioTaxLotRecordsViewModel @AssistedInject constructor(
    private val daoProvider: CachedDaoProvider,
    private val dataColorResolver: DataColorResolver,
    private val getPortfolioTaxLotsUseCase: GetPortfolioTaxLotsUseCase,
    private val mainThreadExecutor: Executor,
    private val moshi: Moshi,
    private val streamingController: StreamingSubscriptionController,
    private val streamingStatusController: StreamingStatusController,
    private val tradingDefaultsPreferences: TradingDefaultsPreferences,
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    private val accountRepo: DefaultAccountRepo,
    @Assisted("accountUuid") private val accountUuid: String,
    @Assisted("symbol") private val symbol: String,
    @Assisted("positionId") private val positionId: String,
    @Assisted private val portfolioTableViewType: PortfolioTableViewType,
    @Assisted private val positionTableMode: PositionTableMode,
    private val sortDelegateFactory: PositionSortDelegateFactory
) : ViewModel(), PortfolioViewActionListener {

    @AssistedFactory
    interface Factory {
        fun create(
            @Assisted("accountUuid") accountUuid: String,
            @Assisted("symbol") symbol: String,
            @Assisted("positionId") positionId: String,
            @Assisted portfolioTableViewType: PortfolioTableViewType,
            @Assisted positionTableMode: PositionTableMode
        ): PortfolioTaxLotRecordsViewModel
    }

    val clickEvents: MutableLiveData<CtaEvent> = SingleLiveEvent()

    private val _viewEffects = MutableLiveData<ViewEffects>().apply { value = ViewEffects.Loading }
    val viewEffects: LiveData<ViewEffects> = _viewEffects

    private lateinit var instrumentsToWrite: Instrument

    private val instrumentDao: InstrumentDao by lazy { daoProvider.getInstrumentDao() }
    private val positionsLiveData: PositionsLiveData by lazy { PositionsLiveData(daoProvider.getPositionsDao()) }

    private val instrumentType: InstrumentType = if (isSymbolLongOption(symbol)) {
        InstrumentType.OPTN
    } else {
        InstrumentType.EQ
    }

    val sortDelegate: PositionSortDelegate by lazy {
        sortDelegateFactory.create(portfolioTableViewType, positionTableMode)
    }

    private val streamingSymbolSet = setOf(Symbol(symbol, instrumentType))

    private val taxLotsLiveData: MutableLiveData<List<Position>> =
        MutableLiveData<List<Position>>().apply { value = emptyList() }

    private val instrumentLiveData: InstrumentsLiveData by lazy {
        InstrumentsLiveData(instrumentDao)
    }

    private val positionRecordsLiveData: LiveData<List<PortfolioRecord>> by lazy {
        taxLotsLiveData.concatCombineCoroutine(instrumentLiveData, ::combine)
    }

    private val orderedPositionRecordsLiveData: LiveData<List<PortfolioRecord>> by lazy {
        positionRecordsLiveData.concatCombineCoroutine(sortDelegate.sortInfo, ::applySort)
    }

    val tableData: LiveData<PortfolioRecordsTableData> =
        MediatorLiveData<PortfolioRecordsTableData>().apply {

            // Call this method to Open Realm instance before the accessing Realm persisted data objects
            daoProvider.open()

            val updateItems = {
                runBlocking {
                    val columns = sortDelegate.columns.value
                    val records = mergeRecords()
                    if (columns != null && orderedPositionRecordsLiveData.hasLotPositions()) {
                        val updatedTable = withContext(Dispatchers.Default) {
                            updateTableRows(columns, records, value, dataColorResolver)
                        }
                        value = updatedTable
                    }
                }
            }
            addSource(orderedPositionRecordsLiveData) { updateItems() }
            addSource(sortDelegate.columns) { updateItems() }
        }

    /**
     * Contains all records ( Sorted and included Portfolio Position).
     */
    val records: LiveData<List<PortfolioRecord>>
        get() = orderedPositionRecordsLiveData

    private val subscriptionFieldsProvider: (InstrumentType) -> Set<Level1Field> =
        { type -> if (type.isOption) optionFields else stockFields }

    private val visibleSymbolsSubject = BehaviorSubject.create<Set<Symbol>>()

    private val throttleUpdate = throttleLast<Unit>(
        coroutineScope = viewModelScope,
        destinationFunction = {
            instrumentDao.updateInstruments(listOf(instrumentsToWrite))
        }
    )

    private val updateCallback: (symbol: String, instrumentType: InstrumentType, data: Level1Data) -> Unit =
        { symbol, _, data ->
            val updatedInstrument = instrumentLiveData.getUpdatedInstrument(symbol, data)
            mainThreadExecutor.execute {
                if (updatedInstrument != null) {
                    instrumentsToWrite = updatedInstrument
                    throttleUpdate(Unit)
                }
            }
        }

    init {
        sortDelegate.updateColumns()

        initStreaming()

        fetchInstrument()

        refreshTaxLots()

        // Retrieves portfolio position from RealmDB fetched from previous portfolio page.
        fetchPortfolioPosition()
    }

    private fun fetchPortfolioPosition() {
        positionsLiveData.apply {
            observeForever { /* Intentionally blank. */ }
            fetchData(listOf(positionId))
        }
    }

    private fun initStreaming() {
        visibleSymbolsSubject.onNext(streamingSymbolSet)
        streamingController.initSubscription(
            subscriptionFieldsProvider,
            visibleSymbolsSubject,
            updateCallback
        )

        if (streamingStatusController.isStreamingToggleEnabled) {
            streamingController.resumeSubscription()
        }
    }

    override fun onCleared() {
        super.onCleared()
        daoProvider.close()
        streamingController.cancelSubscription()
        sortDelegate.onCleared()
    }

    override fun requestActions(position: Int) {
        val actualRecords = records.value ?: return

        // Total No.of position is actual records (Tax lot) + portfolio position
        if (position > actualRecords.size) {
            return
        }
        clickEvents.postClickEvent(MESSAGE_TAX_LOT_TRADE, position)
    }

    fun peekRecord(position: Int): PortfolioRecord? = if (position <= 0) {
        positionsLiveData.getPortfolioRecord()
    } else {
        records.value?.getOrNull(position - 1)
    }

    fun getTradeDirections(
        portfolioRecord: PortfolioRecord,
        orderAction: OrderAction
    ): NavDirections {

        val accountId =
            daoProvider.getAccountDao().getAccountWithUuid(accountUuid)?.accountId ?: accountUuid

        viewModelScope.launch {
            accountRepo.setAccountId(accountId)
        }

        val formParams = getTradeFormParams(
            portfolioRecord,
            accountId,
            orderAction,
            tradeFormParametersBuilderProvider,
            tradingDefaultsPreferences.getDefaultQuantityWithInstrumentType(instrumentType)
        )
        val formBundle = createFormValuesArgument(moshi, formParams)

        return PortfolioTaxLotFragmentDirections.actionTaxLotsLaunchTrade(formBundle)
    }

    fun createTableViewListener(): ITableViewListener =
        PortfolioTableViewListener(sortDelegate, this)

    private fun fetchInstrument() {
        val symbols = listOf(symbol)
        runBlocking {
            instrumentLiveData.fetchData(symbols)
        }

        visibleSymbolsSubject.onNext(streamingSymbolSet)
    }

    fun pauseStreaming() {
        streamingController.pauseSubscription()
    }

    fun resumeStreaming() {
        if (streamingStatusController.isStreamingToggleEnabled) {
            streamingController.resumeSubscription()
        }
    }

    fun refreshTaxLots() {
        _viewEffects.value = ViewEffects.Loading
        viewModelScope.launch {
            val resource = getPortfolioTaxLotsUseCase(
                GetPortfolioTaxLotsParameter(
                    accountUuid,
                    PositionId(positionId)
                )
            )
            when (resource) {
                is Resource.Success -> {
                    if (resource.data.isNullOrEmpty()) {
                        _viewEffects.value = ViewEffects.Error("Failed to retrieve lots")
                    } else {
                        taxLotsLiveData.value = resource.data
                        _viewEffects.value = ViewEffects.Success
                    }
                }
                else -> _viewEffects.value = ViewEffects.Error()
            }
        }
    }

    private suspend fun combine(
        positions: List<Position>,
        instruments: Map<String, Instrument>
    ): List<PortfolioRecord> {
        return withContext(Dispatchers.Default) {
            positions.map {
                val instrument = instruments.values.first()
                val baseSymbolAndPrice = extractBaseSymbolAndPrice(instrument, it, instruments)
                val (uniqueId, position) = if (it is TaxLotPosition) {
                    it.taxLotId to it.updateWithInstrument(instrument)
                } else {
                    it.positionId to it
                }
                merge(uniqueId, position, instrument, baseSymbolAndPrice, isTaxLot = true)
            }
        }
    }

    private fun mergeRecords(): List<PortfolioRecord> {
        val records = mutableListOf<PortfolioRecord>()

        // First Position in the table of record is a portfolio position
        // Record is fetched from Realm DB
        positionsLiveData.getPortfolioRecord()?.let { records.add(it) }

        // Sorted Tax lots records fetched by a network call
        records.addAll(orderedPositionRecordsLiveData.value?.toList().orEmpty())
        return records
    }

    private fun PositionsLiveData.getPortfolioRecord(): PortfolioRecord? {
        return this.value?.firstOrNull()?.let { position ->
            val instruments = instrumentLiveData.value.orEmpty()
            val instrument = instruments.values.firstOrNull()
            val baseSymbolAndPrice =
                instrument?.let { extractBaseSymbolAndPrice(it, position, instruments) }
            merge(
                position.positionId,
                position,
                instrument,
                baseSymbolAndPrice
            )
        }
    }

    sealed class ViewEffects {
        object Loading : ViewEffects()

        data class Error(val message: String? = "Something went wrong!") : ViewEffects()

        object Success : ViewEffects()
    }
}

private fun LiveData<List<PortfolioRecord>>.hasLotPositions(): Boolean {
    return value?.isNotEmpty() == true
}
