package com.etrade.mobilepro.markets.earnings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.marketscalendar.data.dto.EARNINGS_EVENT
import com.etrade.mobilepro.marketscalendar.util.createRequestForMarketsEvents
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.MarketsTodayTopEarningsEventAnalyticsParams
import kotlinx.coroutines.launch
import org.threeten.bp.LocalDate
import javax.inject.Inject

class TodayEarningsViewModel @Inject constructor(
    private val getTodayEarningsUseCase: GetTodayEarningsUseCase,
    private val tracker: Tracker
) : DynamicViewModel(R.layout.widget_today_earnings) {

    override val variableId: Int = BR.obj

    private val _items: MutableLiveData<List<MarketEventDetailsPayload>> = MutableLiveData()
    val items: LiveData<List<MarketEventDetailsPayload>>
        get() = _items

    private val _viewState = MutableLiveData<DynamicUiViewState>()
    val viewState: LiveData<DynamicUiViewState>
        get() = _viewState

    private val eventClickListener: (String, NavController) -> Unit = { symbol, navController ->
        tracker.event(MarketsTodayTopEarningsEventAnalyticsParams())
        navController.navigate(MainGraphDirections.openEarningsDetailsOverlay(symbol))
    }

    fun loadTodayEarnings() {
        if (viewState.value != DynamicUiViewState.Success) {
            _viewState.value = DynamicUiViewState.Loading
        }
        val currentDate = LocalDate.now()
        val request = createRequestForMarketsEvents(
            startDate = currentDate,
            endDate = currentDate,
            eventType = EARNINGS_EVENT,
            eventFilterEnable = null
        )
        viewModelScope.launch {
            getTodayEarningsUseCase
                .execute(request)
                .onSuccess {
                    handleSuccessResponse(it)
                }
                .onFailure {
                    _viewState.value = DynamicUiViewState.Error()
                    logger.error("Today's earnings fetch error", it)
                }
        }
    }

    private fun handleSuccessResponse(it: List<EventDetails>?) {
        logger.debug("handleSuccessResponse start")
        it
            ?.map { item -> MarketEventDetailsPayload(item, eventClickListener) }
            ?.also { items ->
                if (items.isEmpty()) {
                    _viewState.value = DynamicUiViewState.Error()
                } else {
                    _viewState.value = DynamicUiViewState.Success
                    _items.value = items
                    logger.debug("handleSuccessResponse end")
                }
            } ?: let { _viewState.value = DynamicUiViewState.Error() }
    }
}
