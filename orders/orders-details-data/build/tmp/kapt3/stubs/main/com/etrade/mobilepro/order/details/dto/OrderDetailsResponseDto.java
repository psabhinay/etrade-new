package com.etrade.mobilepro.order.details.dto;

import com.etrade.mobilepro.backends.neo.BaseDataDto;
import com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto;
import com.etrade.mobilepro.orders.data.dto.AuditInfoDto;
import com.etrade.mobilepro.orders.data.dto.HeaderDto;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0087\b\u0018\u00002\u00020\u0001B\u001d\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0005J\u000b\u0010\t\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\n\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J!\u0010\u000b\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007\u00a8\u0006\u0014"}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/OrderDetailsResponseDto;", "Lcom/etrade/mobilepro/backends/neo/BaseDataDto;", "viewOrderEvents", "Lcom/etrade/mobilepro/order/details/dto/OrderDetailsDto;", "preparedOrderDetails", "(Lcom/etrade/mobilepro/order/details/dto/OrderDetailsDto;Lcom/etrade/mobilepro/order/details/dto/OrderDetailsDto;)V", "getPreparedOrderDetails", "()Lcom/etrade/mobilepro/order/details/dto/OrderDetailsDto;", "getViewOrderEvents", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "orders-details-data"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class OrderDetailsResponseDto extends com.etrade.mobilepro.backends.neo.BaseDataDto {
    @org.jetbrains.annotations.Nullable()
    private final com.etrade.mobilepro.order.details.dto.OrderDetailsDto viewOrderEvents = null;
    @org.jetbrains.annotations.Nullable()
    private final com.etrade.mobilepro.order.details.dto.OrderDetailsDto preparedOrderDetails = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDto copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "viewOrderEvents")
    com.etrade.mobilepro.order.details.dto.OrderDetailsDto viewOrderEvents, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "PreparedOrderDetails")
    com.etrade.mobilepro.order.details.dto.OrderDetailsDto preparedOrderDetails) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public OrderDetailsResponseDto(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "viewOrderEvents")
    com.etrade.mobilepro.order.details.dto.OrderDetailsDto viewOrderEvents, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "PreparedOrderDetails")
    com.etrade.mobilepro.order.details.dto.OrderDetailsDto preparedOrderDetails) {
        super(null, null);
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.etrade.mobilepro.order.details.dto.OrderDetailsDto component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.etrade.mobilepro.order.details.dto.OrderDetailsDto getViewOrderEvents() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.etrade.mobilepro.order.details.dto.OrderDetailsDto component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.etrade.mobilepro.order.details.dto.OrderDetailsDto getPreparedOrderDetails() {
        return null;
    }
}