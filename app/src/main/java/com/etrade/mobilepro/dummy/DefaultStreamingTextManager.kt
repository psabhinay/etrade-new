package com.etrade.mobilepro.dummy

import com.etrade.eo.accountvalues.api.AccountValuesVO
import com.etrade.mobilepro.accountvalues.api.AccountValuesProvider
import com.etrade.mobilepro.common.di.Default
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingText
import io.reactivex.Observable
import javax.inject.Inject

class DefaultStreamingTextManager @Inject constructor(
    @Default accountValuesProvider: AccountValuesProvider,
    streamingStatusController: StreamingStatusController
) : BaseStreamingTextManager(accountValuesProvider, streamingStatusController) {

    override fun Observable<AccountValuesVO>.getNetAssetsAccountValues(): Observable<StreamingText> =
        getNetAccountValues()
}
