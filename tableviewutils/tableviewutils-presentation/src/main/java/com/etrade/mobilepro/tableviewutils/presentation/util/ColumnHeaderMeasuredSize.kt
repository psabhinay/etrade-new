package com.etrade.mobilepro.tableviewutils.presentation.util

data class ColumnHeaderMeasuredSize(
    val containerWidth: Int,
    val containerHeight: Int,
    val titleViewWidth: Int
)
