package com.etrade.mobilepro.util.debuglog

/**
 *  Interface to log the debug on remote location using firebase or a backend logging service.
 */

interface RemoteDebugLogger {
    fun log(debugLog: DebugLog)
}
