package com.etrade.mobilepro.walkthrough.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.map
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.livedata.filter
import com.etrade.mobilepro.livedata.first
import com.etrade.mobilepro.statemachine.EtStateMachine
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.walkthrough.api.Action
import com.etrade.mobilepro.walkthrough.api.IdleState
import com.etrade.mobilepro.walkthrough.api.State
import com.etrade.mobilepro.walkthrough.api.SymbolLookupAction
import com.etrade.mobilepro.walkthrough.api.SymbolLookupState
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingAction
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingState
import com.etrade.mobilepro.walkthrough.api.TradePlacingAction
import com.etrade.mobilepro.walkthrough.api.TradePlacingState
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

private const val TIME_OUT_IN_MILLIS = 60_000L

interface WalkthroughStatesViewModel {
    var state: LiveData<State>
    fun onAction(action: Action)
    fun dispose()
}

@Singleton
class SharedWalkthroughStatesViewModel @Inject constructor(
    private val symbolLookupStateMachine: EtStateMachine<SymbolLookupState, SymbolLookupAction>,
    private val symbolTrackingStateMachine: EtStateMachine<SymbolTrackingState, SymbolTrackingAction>,
    private val tradePlacingStateMachine: EtStateMachine<TradePlacingState, TradePlacingAction>
) : WalkthroughStatesViewModel {

    private val mainScope = MainScope()
    private var _state = MutableLiveData<State>()
    private var timeOutJob: Job? = null

    override var state: LiveData<State> = _state

    init {
        symbolLookupStateMachine.onStateChange = {
            onStateChange(it)
        }
        symbolTrackingStateMachine.onStateChange = {
            onStateChange(it)
        }
        tradePlacingStateMachine.onStateChange = {
            onStateChange(it)
        }
    }

    private fun onStateChange(state: State) {
        scheduleTimeOut(state)
        _state.value = state
    }

    override fun onAction(action: Action) {
        when (action) {
            is SymbolLookupAction -> symbolLookupStateMachine.transition(action)
            is SymbolTrackingAction -> symbolTrackingStateMachine.transition(action)
            is TradePlacingAction -> tradePlacingStateMachine.transition(action)
        }
    }

    override fun dispose() {
        onAction(SymbolLookupAction.Canceled)
        onAction(SymbolTrackingAction.Canceled)
        onAction(TradePlacingAction.Canceled)
        timeOutJob?.cancel()
    }

    private fun scheduleTimeOut(newState: State) {
        timeOutJob?.cancel()
        if (newState !is IdleState) {
            timeOutJob = mainScope.launch {
                delay(TIME_OUT_IN_MILLIS)
                dispose()
            }
        }
    }
}

@Suppress("UNCHECKED_CAST")
inline fun <S : State, V, reified SUCCESS : V> WalkthroughStatesViewModel.createConditionalSignal(
    viewState: LiveData<V>,
    goalState: State,
    noinline successStatePredicate: ((SUCCESS) -> Boolean)? = null
): LiveData<ConsumableLiveEvent<S>> {
    return Transformations.map(this.state) {
        it
    }.filter {
        it == goalState
    }.combineLatest(
        viewState.filter {
            it is SUCCESS && successStatePredicate?.invoke(it) ?: true
        }.first()
    ).map { (state, _) ->
        state.consumable() as ConsumableLiveEvent<S>
    }
}
