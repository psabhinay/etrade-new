package com.etrade.mobilepro.quote.search.presentation

import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.etrade.mobilepro.util.delegate.getValue
import com.etrade.mobilepro.util.delegate.setValue
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class SearchResultsAdapter(symbolSelectListener: OnSearchResultItemClickListener? = null) : ListDelegationAdapter<MutableList<SearchResultItem>>() {

    private val symbolItemAdapterDelegate: SymbolItemAdapterDelegate = SymbolItemAdapterDelegate(symbolSelectListener)

    var onSearchResultItemClickListener: OnSearchResultItemClickListener? by symbolItemAdapterDelegate::listener

    init {
        delegatesManager.addDelegate(symbolItemAdapterDelegate)
        delegatesManager.addDelegate(SectionTitleItemAdapterDelegate())
        delegatesManager.addDelegate(DisclosureItemAdapterDelegate())
        items = mutableListOf()
    }

    fun updateData(newList: List<SearchResultItem>) {
        dispatchUpdates(items, newList)
        items.clear()
        items.addAll(newList)
    }
}
