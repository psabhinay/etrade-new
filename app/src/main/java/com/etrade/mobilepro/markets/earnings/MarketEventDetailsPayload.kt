package com.etrade.mobilepro.markets.earnings

import androidx.navigation.NavController
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails

data class MarketEventDetailsPayload(
    val event: EventDetails,
    val clickListener: (String, NavController) -> Unit
)
