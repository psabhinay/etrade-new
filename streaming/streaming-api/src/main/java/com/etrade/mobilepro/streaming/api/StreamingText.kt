package com.etrade.mobilepro.streaming.api

import androidx.annotation.ColorRes

data class StreamingText constructor(val text: String, @ColorRes val color: Int)
