package com.etrade.mobilepro.login.presentation.cta

import android.os.Message
import android.os.Parcelable
import com.etrade.mobilepro.login.presentation.MESSAGE_LOGIN_ACTION
import kotlinx.parcelize.Parcelize

@Parcelize
class LoginCtaItem(val action: LoginCtaAction, val isChecked: Boolean? = null) : Parcelable {

    fun getEventMessage(): Message = Message.obtain().apply {
        what = MESSAGE_LOGIN_ACTION
        obj = this@LoginCtaItem
    }
}

enum class LoginCtaAction {
    SIGN_UP,
    FORGOT_USERNAME,
    FORGOT_PASSWORD,
    PRIVACY_POLICY,
    CHECK_BOX_REMEMBER_USERNAME,
    CALL_ETRADE_SUPPORT,
    CHECK_BOX_USE_CODE,
    VIEW_EULA
}
