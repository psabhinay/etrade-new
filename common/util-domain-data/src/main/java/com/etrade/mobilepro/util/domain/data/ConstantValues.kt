package com.etrade.mobilepro.util.domain.data

import java.math.BigDecimal

internal const val PERCENT_MULTIPLIER = 100
internal const val DIVIDE_SCALE = 4
internal const val BOND_MULTIPLIER = 100

internal val NEGATIVE_ONE = BigDecimal(-1)
