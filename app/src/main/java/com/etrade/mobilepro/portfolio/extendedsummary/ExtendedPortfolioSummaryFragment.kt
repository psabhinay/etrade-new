package com.etrade.mobilepro.portfolio.extendedsummary

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.databinding.ViewPortfolioSummaryDetailsBinding
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import com.etrade.mobilepro.util.android.binding.dataBinding
import javax.inject.Inject

@RequireLogin
class ExtendedPortfolioSummaryFragment @Inject constructor(
    private val defaultTextManager: StreamingTextManager
) : Fragment(R.layout.view_portfolio_summary_details) {
    private val args: ExtendedPortfolioSummaryFragmentArgs by navArgs()
    private val binding by dataBinding(ViewPortfolioSummaryDetailsBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.obj = args.info

        val sections =
            args.info?.toAccountSummarySections(defaultTextManager).orEmpty()
        binding.streamingSections = sections

        args.info?.clickEvents?.observe(
            viewLifecycleOwner,
            Observer {
                if (it is CtaEvent.UriEvent) {
                    findNavController().navigate(it.uri)
                }
            }
        )
    }
}
