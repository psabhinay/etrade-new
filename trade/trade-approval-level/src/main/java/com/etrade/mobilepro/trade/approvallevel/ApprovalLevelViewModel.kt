package com.etrade.mobilepro.trade.approvallevel

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.confirmationDialog
import com.etrade.mobilepro.dialog.viewmodel.AppDialogViewModel
import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class ApprovalLevelViewModel @AssistedInject constructor(
    val resources: Resources,
    @Assisted val accountMode: AccountMode
) : ViewModel(), AppDialogViewModel {

    @AssistedFactory
    interface Factory {
        fun create(
            accountMode: AccountMode
        ): ApprovalLevelViewModel
    }

    private val _dialogSignal: MutableLiveData<ConsumableLiveEvent<AppDialog?>?> = MutableLiveData()
    override val dialog: LiveData<ConsumableLiveEvent<AppDialog?>?> = _dialogSignal

    fun showCallUsConfirmDialog(positiveAction: () -> Unit, negativeAction: () -> Unit) {
        _dialogSignal.value = resources.confirmationDialog(
            R.string.call_us,
            R.string.approval_level_call_us_description,
            positiveAction,
            negativeAction
        ).consumable()
    }
}
