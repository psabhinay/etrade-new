package com.etrade.mobilepro.alert.base

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.alerts.api.model.AlertGroup
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.alerts.presentation.base.AlertPaginationInfo
import com.etrade.mobilepro.alerts.presentation.base.AlertsDeleter
import com.etrade.mobilepro.alerts.presentation.base.AlertsHolder
import com.etrade.mobilepro.alerts.presentation.base.BaseAlertsViewModel
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.rx.test.RxImmediateSchedulerRule
import com.etrade.mobilepro.session.api.User
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.util.concurrent.CountDownLatch

private const val USER_ID = 1L
private const val MAX_COUNT = 100

internal class BaseAlertsViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    private lateinit var sut: Sut
    private lateinit var alerts: List<AlertItem>

    @Mock
    private lateinit var user: User

    @Mock
    lateinit var alertsRepo: AlertsRepo

    @Mock
    private lateinit var alertsHolder: AlertsHolder

    @Mock
    private lateinit var alertsDeleter: AlertsDeleter

    @Mock
    private lateinit var readStatusRepo: ReadStatusRepo

    private val startingEventId = argumentCaptor<String>()
    private val startingEventOccurredAt = argumentCaptor<Long>()

    @Before
    fun setupCommonMocks() {
        MockitoAnnotations.openMocks(this)
        whenever(user.getUserId()).thenReturn(USER_ID)
        whenever(readStatusRepo.wasRead(any())).thenReturn(false)
        whenever(alertsDeleter.isDeleting(any())).thenReturn(false)

        alerts = mutableListOf<AlertItem>(AlertItem.Divider(true)).apply {
            addAll(
                generateChunkOfAlerts(
                    group = AlertGroup.MARKET,
                    lastLoadedId = "15893",
                    lastLoadedOccurredAt = 3298923L,
                    maxCount = MAX_COUNT
                ).filter {
                    it.belongsTo() == AlertGroup.MARKET
                }
            )
        }.toList()
    }

    @Test
    fun `if there are more alerts on server, but we have zero after filtering, load next chunk, until end is reached, No needed alerts are met at all`() {
        val lastEventId = "247"
        whenever(
            alertsRepo.loadNext(
                eq(USER_ID.toString()),
                startingEventId.capture(),
                startingEventOccurredAt.capture(),
                eq(MAX_COUNT)
            )
        ).thenReturn(
            Single.fromCallable {
                generateChunkOfAlerts(
                    group = AlertGroup.ACCOUNT,
                    lastLoadedId = startingEventId.lastValue,
                    lastLoadedOccurredAt = startingEventOccurredAt.lastValue,
                    stopAtId = lastEventId,
                    differentAt = null,
                    maxCount = MAX_COUNT
                )
            }
        )
        whenever(alertsHolder.lastAlertInfo(any())).thenReturn(AlertPaginationInfo("0", 0L))

        val latch = CountDownLatch(1)
        sut = object : Sut(alertsRepo, user, alertsHolder, alertsDeleter) {
            override fun onAlertsChanged(alerts: List<AlertItem>) {
                super.onAlertsChanged(alerts)
                assertTrue(alerts.isEmpty())
            }
            override fun onAlertChunkLoaded(hasMore: Boolean) {
                super.onAlertChunkLoaded(hasMore)
                if (!hasMore) {
                    latch.countDown()
                }
            }
        }

        sut.loadMore(clearAlreadyLoadedItems = true)
        latch.await()
        assertTrue("should load 3 chunks (100, 100, 47)", sut.chunksLoaded == 3)
        assertTrue("alerts were changed 3 times", sut.alertChangedCount == 3)
        assertTrue("loading started once", sut.loadingStarted == 1)
    }

    @Test
    fun `if there are more alerts on server, but we have zero after filtering, load next chunk, until end is reached, Needed alert is met in second chunk`() {
        val lastEventId = "247"
        val differentAlertAt = "154"
        whenever(
            alertsRepo.loadNext(
                eq(USER_ID.toString()),
                startingEventId.capture(),
                startingEventOccurredAt.capture(),
                eq(MAX_COUNT)
            )
        ).thenReturn(
            Single.fromCallable {
                generateChunkOfAlerts(
                    group = AlertGroup.ACCOUNT,
                    lastLoadedId = startingEventId.lastValue,
                    lastLoadedOccurredAt = startingEventOccurredAt.lastValue,
                    stopAtId = lastEventId,
                    differentAt = differentAlertAt,
                    maxCount = MAX_COUNT
                )
            }
        )
        val chunkCaptor = argumentCaptor<List<AlertItem.Alert>>()
        whenever(alertsHolder.lastAlertInfo(any())).thenReturn(AlertPaginationInfo("0", 0L))
        val latch = CountDownLatch(1)
        sut = object : Sut(alertsRepo, user, alertsHolder, alertsDeleter) {
            override fun onAlertChunkLoaded(hasMore: Boolean) {
                super.onAlertChunkLoaded(hasMore)
                if (hasMore && chunksLoaded == 2) {
                    latch.countDown()
                }
            }
        }

        sut.loadMore(clearAlreadyLoadedItems = true)
        verify(alertsHolder, times(2)).add(chunkCaptor.capture())
        latch.await()
        assertTrue("should load 2 chunks (100, 100)", sut.chunksLoaded == 2)
        assertTrue("alerts were changed 2 times", sut.alertChangedCount == 2)
        assertTrue("loading started once", sut.loadingStarted == 1)
        assertTrue("has one value", chunkCaptor.lastValue.size == 1)
        assertTrue("value", chunkCaptor.lastValue[0].id == differentAlertAt)
    }

    @Test
    fun `no alerts, deleteMarkedAlerts, does not call deleteAlert`() {
        sut = Mockito.spy(Sut(alertsRepo, user, alertsHolder, alertsDeleter))

        alerts = listOf<AlertItem.Alert>()
        doReturn(alerts).whenever(alertsHolder).alerts()

        sut.deleteMarkedAlerts()

        verify(sut, never()).deleteAlert(any())
    }

    @Test
    fun `4 of more than 4 market alerts marked, deleteMarkedAlerts, only marked alerts deleted`() {
        sut = Mockito.spy(Sut(alertsRepo, user, alertsHolder, alertsDeleter))

        val markedIndices = mutableListOf(0, 4, 5, 21)
        testMarkAlerts(markedIndices)

        val resultList = mutableListOf<AlertItem>().apply { addAll(alerts) }
        doReturn(resultList).whenever(alertsHolder).alerts()

        val deletedAlerts = mutableListOf<AlertItem>()

        doAnswer {
            (it.arguments[0] as AlertItem.Alert).let { alert ->
                markedIndices.forEach { i ->
                    if (resultList[i] == alert) {
                        deletedAlerts.add(resultList[i])
                        resultList.removeAt(i)
                    }
                }
            }
        }.whenever(sut).deleteAlert(any())

        sut.deleteMarkedAlerts()

        alerts.minus(resultList)
            .filterIsInstance<AlertItem.Alert>()
            .apply {
                assertTrue(
                    "did not delete the correct number of alerts",
                    size == deletedAlerts.size
                )
                forEach {
                    assertTrue("$it was not deleted", deletedAlerts.contains(it))
                }
            }
    }

    @Test
    fun `no alerts, markAllAlerts, does not call markAndCountAlert`() {
        sut = Mockito.spy(Sut(alertsRepo, user, alertsHolder, alertsDeleter))

        alerts = listOf<AlertItem.Alert>()
        doReturn(alerts).whenever(alertsHolder).alerts()

        sut.markAllAlerts()

        verify(sut, never()).markAndCountAlert(any())
    }

    @Test
    fun `3 out of more than 3 market alerts marked, markAllAlerts, marks the rest`() {
        sut = Mockito.spy(Sut(alertsRepo, user, alertsHolder, alertsDeleter))

        testMarkAlerts(listOf(2, 7, 16))
        doReturn(alerts).whenever(alertsHolder).alerts()

        doAnswer {
            (it.arguments[0] as AlertItem.Alert).status.let { stat ->
                if (!stat.isMarked) { stat.isMarked = true }
            }
        }.whenever(alertsHolder).toggleMarkedStatus(any())

        sut.markAllAlerts()

        verify(
            sut,
            times(MAX_COUNT - 3)
                .description("marked alerts not counted correctly")
        ).markAndCountAlert(any())

        alerts
            .filterIsInstance<AlertItem.Alert>()
            .forEachIndexed { index, it ->
                assertTrue("alert at $index is not marked", it.status.isMarked)
            }
        assertTrue(
            "onAlertsChanged not called exactly once",
            sut.alertChangedCount == 1
        )
    }

    @Test
    fun `All market alerts unmarked, markAllAlerts twice, marks then unmarks all alerts`() {
        sut = Sut(alertsRepo, user, alertsHolder, alertsDeleter)

        doReturn(alerts).whenever(alertsHolder).alerts()
        doAnswer {
            (it.arguments[0] as AlertItem.Alert).status.isMarked = true
        }.whenever(alertsHolder)
            .toggleMarkedStatus(any())

        sut.markAllAlerts()

        alerts
            .filterIsInstance<AlertItem.Alert>()
            .forEach {
                assertTrue("first call to markAllAlerts failed", it.status.isMarked)
            }

        doAnswer {
            (it.arguments[0] as AlertItem.Alert).status.isMarked = false
        }.whenever(alertsHolder)
            .toggleMarkedStatus(any())

        sut.markAllAlerts()

        alerts
            .filterIsInstance<AlertItem.Alert>()
            .forEach {
                assertFalse("second call to markAllAlerts failed", it.status.isMarked)
            }
        assertTrue(
            "onAlertsChanged not called exactly twice",
            sut.alertChangedCount == 2
        )
    }

    private fun generateChunkOfAlerts(
        group: AlertGroup,
        lastLoadedId: String,
        lastLoadedOccurredAt: Long,
        stopAtId: String? = null,
        differentAt: String? = null,
        maxCount: Int
    ): List<AlertItem.Alert> {
        val differentIndex = differentAt?.toInt()
        val differentGroup = if (group == AlertGroup.ACCOUNT) {
            AlertGroup.MARKET
        } else {
            AlertGroup.ACCOUNT
        }
        var index = lastLoadedId.toInt()
        var lastOccurredAt = lastLoadedOccurredAt
        val lastIndex = index + maxCount
        val stopIndex = stopAtId?.toInt()
        val result = mutableListOf<AlertItem.Alert>()
        while (index != stopIndex && index < lastIndex) {
            index++
            lastOccurredAt++
            if (index == differentIndex) {
                result.add(createAlert(index, lastOccurredAt, differentGroup))
            } else {
                result.add(createAlert(index, lastOccurredAt, group))
            }
        }
        return result
    }

    private fun createAlert(id: Int, occurredAt: Long, group: AlertGroup): AlertItem.Alert =
        AlertItem.Alert(
            id = id.toString(),
            occurredAt = occurredAt,
            subject = "",
            occurredAtFormatted = "",
            code = if (group == AlertGroup.ACCOUNT) {
                950
            } else {
                650
            },
            status = AlertItem.Status(),
            isDeleteButtonVisible = false
        )

    private fun testMarkAlerts(markedIndices: List<Int>) {
        alerts
            .filterIsInstance<AlertItem.Alert>()
            .apply {
                markedIndices.onEach {
                    this[it].status.isMarked = true
                }
            }
    }

    private open class Sut(
        alertsRepo: AlertsRepo,
        user: User,
        alertsHolder: AlertsHolder,
        alertsDeleter: AlertsDeleter
    ) : BaseAlertsViewModel(alertsRepo, user, alertsHolder, alertsDeleter) {
        var chunksLoaded = 0
        var alertChangedCount = 0
        var loadingStarted = 0
        override val alertGroup: AlertGroup = AlertGroup.MARKET
        override fun onLoadingStart() { loadingStarted++ }
        override fun onAlertsChanged(alerts: List<AlertItem>) { alertChangedCount++ }
        override fun onLoadingFailed(it: Throwable, clearLoadedItems: Boolean) {
            assertTrue("wtf", false)
        }
        override fun onDeletionFailed(it: Throwable, alert: AlertItem.Alert, actualAlerts: List<AlertItem>) {
            assertTrue("wtf", false)
        }
        override fun onDeletionSuccess(alert: AlertItem.Alert) {
            assertTrue("wtf", false)
        }
        override fun onAlertChunkLoaded(hasMore: Boolean) { chunksLoaded++ }
        override fun hasErrorState(): Boolean = false
    }
}
