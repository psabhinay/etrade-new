package com.etrade.mobilepro.order.details.presentation

import com.etrade.mobilepro.instrument.isSymbolLongOption
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.WithPriceOrder
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrder
import com.etrade.mobilepro.trade.api.option.OptionOrder
import com.etrade.mobilepro.trade.api.stock.StockOrder
import com.etrade.mobilepro.trade.form.api.SecurityType

internal val Order.priceType: PriceType?
    get() = (this as? WithPriceOrder)?.priceType

internal val Order.securityType: SecurityType?
    get() {
        return when (this) {
            is MutualFundsOrder -> SecurityType.MUTUAL_FUND
            is OptionOrder -> {
                if (isSymbolLongOption(legs.first().symbol)) {
                    SecurityType.OPTION
                } else {
                    SecurityType.STOCK_AND_OPTIONS
                }
            }
            is StockOrder -> SecurityType.STOCK
            else -> null
        }
    }
