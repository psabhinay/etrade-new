package com.etrade.mobilepro.markets.movers

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.livedata.postUpdateValue
import com.etrade.mobilepro.markets.MarketsStreamingDelegate
import com.etrade.mobilepro.markets.MarketsStreamingDelegateImpl
import com.etrade.mobilepro.markets.common.MarketsBaseViewModel
import com.etrade.mobilepro.markets.common.SymbolRowInfo
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.data.response.MarketMoversResponse
import com.etrade.mobilepro.markets.data.response.Quote
import com.etrade.mobilepro.quote.deriveDayChange
import com.etrade.mobilepro.quote.deriveDayChangePercent
import com.etrade.mobilepro.quote.getFormattedLastPrice
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.getTimeStamp
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.sorting.TableDataSortable
import com.etrade.mobilepro.tableviewutils.presentation.sorting.TableDataSortingHandler
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.domain.data.resolveTimeStamp
import com.etrade.mobilepro.util.executeBlocking
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.rx.awaitResult
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.evrencoskun.tableview.sort.SortState
import io.reactivex.Observable
import io.reactivex.rxkotlin.zipWith
import io.reactivex.subjects.BehaviorSubject
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MarketsMoversViewModel @Inject constructor(
    dataColorResolver: DataColorResolver,
    marketsService: MoverService,
    streamingController: StreamingSubscriptionController,
    streamingStatusController: StreamingStatusController,
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase,
    private val quoteRepo: MobileQuoteRepo
) : MarketsBaseViewModel(marketsService, dataColorResolver), TableDataSortable {

    override val columnHeaderList: List<Cell> = ArrayList<Cell>().apply {
        add(Cell(1.toString(), "Last"))
        add(Cell(2.toString(), "Change $"))
        add(Cell(3.toString(), "Change %"))
        add(Cell(4.toString(), "Volume"))
    }

    override val viewModelType = MarketsMoversViewModel::class.java

    val exchangeList = listOf(ExchangeType.NASDAQ, ExchangeType.NYSE)
    val activitiesList = listOf(
        ActivityType.MostActive,
        ActivityType.TopPctGainer,
        ActivityType.TopPctLoser,
        ActivityType.TopDollarGainer,
        ActivityType.TopDollarLoser
    )

    val sortState: LiveData<SortStateInfo?> get() = _sortState
    val selectedExchange: LiveData<ExchangeType> get() = _selectedExchange
    val selectedActivity: LiveData<ActivityType> get() = _selectedActivity

    val selectedExchangeIndex: Int
        get() = exchangeList.indexOf(_selectedExchange.value ?: throw IllegalStateException("No selected exchange"))

    val selectedActivityIndex: Int
        get() = activitiesList.indexOf(_selectedActivity.value ?: throw IllegalStateException("No selected activity"))

    private val streamingDelegate: MarketsStreamingDelegate by lazy {
        MarketsStreamingDelegateImpl(
            streamingController, streamingStatusController,
            setOf(
                Level1Field.CHANGE,
                Level1Field.CHANGE_PERCENT,
                Level1Field.LAST_PRICE,
                Level1Field.TIMESTAMP_SINCE_EPOCH,
                Level1Field.VOLUME,
                Level1Field.TICK_INDICATOR
            )
        )
    }

    private val visibleSymbolsSubject = BehaviorSubject.create<Set<Symbol>>()
    private val sortingHandler = TableDataSortingHandler<ResultDataCell>()
    private val _sortState = MutableLiveData<SortStateInfo?>()
    private var _selectedExchange: MutableLiveData<ExchangeType> = MutableLiveData()
    private var _selectedActivity: MutableLiveData<ActivityType> = MutableLiveData()

    init {
        _selectedExchange.value = ExchangeType.NASDAQ
        _selectedActivity.value = ActivityType.MostActive
        fetch()
    }

    override suspend fun fetchDataFromServer(): ViewState {
        val exchangeSelected: ExchangeType = _selectedExchange.value ?: return ViewState.Error("Selected exchange is null")
        val activitySelected: ActivityType = _selectedActivity.value ?: return ViewState.Error("Selected activity is null")
        return getMarketsRequest(activitySelected, exchangeSelected).toObservable()
            .map { mapQuotes(it) }
            .concatMap { quotes ->
                val symbols = quotes.filter { it.symbol != null }.joinToString(",") { it.symbol ?: "" }
                quoteRepo.loadMobileQuotes(symbols, extendedHours = isExtendedHoursOnUseCase.executeBlocking())
                    .zipWith(Observable.just(quotes))
            }
            .map { it.first.zip(it.second) }
            .awaitResult().fold(onSuccess = ::onEmptyOrSuccess, onFailure = ::onEmptyOrError)
    }

    override fun onCleared() {
        streamingDelegate.cancelSubscription()
        super.onCleared()
    }

    override fun buildSymbolRowItem(symbolInfo: SymbolRowInfo, fromMoversResponse: Boolean, instrumentType: InstrumentType) =
        super.buildSymbolRowItem(symbolInfo, fromMoversResponse, instrumentType).toMutableList().apply {
            val volumeValue = symbolInfo.volume?.safeParseBigDecimal()
            val formattedVolume = symbolInfo.volume?.let { MarketDataFormatter.formatVolumeTwoDecimals(it.safeParseBigDecimal()) } ?: ""
            add(ResultDataCell(volumeValue ?: BigDecimal.ZERO, formattedVolume, instrumentType, symbolInfo.symbol))
        }

    override fun onSuccess(result: ViewState.Success) {
        super.onSuccess(result)
        streamingDelegate.initSubscription(visibleSymbolsSubject, ::handleSymbolUpdate)
    }

    override fun beforeRefresh() {
        super.beforeRefresh()
        streamingDelegate.cancelSubscription()
    }

    override fun sortTable(column: Int?) {
        val oldSortState = _sortState.value
        val newSortState = sortingHandler.resolveSortState(oldSortState, column)

        if (newSortState != oldSortState) {
            val tableModel = (viewState.value as? ViewState.Success)
            tableModel?.let {
                val isTableModelEmpty = it.rowHeaders.isEmpty() && it.cellItems.isEmpty()
                if (!isTableModelEmpty) {
                    val sortedData = sortingHandler.sortTableData(it.cellItems, it.rowHeaders, newSortState)
                    val updatedModel = tableModel.copy(
                        rowHeaders = sortedData.first,
                        cellItems = sortedData.second
                    )
                    mutableViewState.postValue(updatedModel)
                }

                _sortState.value = newSortState
            }
        }
    }

    fun pauseStreaming() {
        streamingDelegate.pauseStreaming()
    }

    fun resumeStreaming() {
        streamingDelegate.resumeStreaming()
    }

    fun exchangeItemSelected(item: ExchangeType) {
        _selectedExchange.value = item
        refresh()
    }

    fun activityItemSelected(item: ActivityType) {
        _selectedActivity.value = item
        refresh()
    }

    fun notifyVisibleItemsChanged(updatedSymbols: Set<Symbol>) {
        if (streamingDelegate.isStreamingEnabled()) {
            visibleSymbolsSubject.onNext(updatedSymbols)
        }
    }

    private fun mapQuotes(it: MarketMoversResponse): List<Quote> {
        val quotes = it.quotes
        if (quotes.isNullOrEmpty()) {
            logger.error("Market movers response contains no quotes")
            throw EmptyQuotesException()
        }
        return quotes
    }

    private fun handleSymbolUpdate(symbol: String, instrumentType: InstrumentType, data: Level1Data) {
        val lastPrice = data.lastPrice ?: return
        val symbolCells = buildSymbolCells(symbol, instrumentType, data, MarketDataFormatter.formatMarketDataCustom(lastPrice.toBigDecimal()))

        mutableViewState.postUpdateValue { model ->
            if (model is ViewState.Success) {

                val updatedTimeStamp = data.timestampSinceEpoch?.let { timeStamp -> formatTimeStamp(timeStamp) } ?: model.timeStamp
                var updatedModel: ViewState.Success

                model.run {
                    val isUnsorted = (_sortState.value?.equals(SortState.UNSORTED) ?: true)

                    updatedModel = if (isUnsorted) {
                        updateViewStateForSymbol(model, symbolCells, symbol, updatedTimeStamp)
                    } else {
                        val sortedData = sortingHandler.sortTableData(cellItems, rowHeaders, _sortState.value)
                        val sortedModel = model.copy(
                            rowHeaders = sortedData.first,
                            cellItems = sortedData.second
                        )

                        updateViewStateForSymbol(sortedModel, symbolCells, symbol, updatedTimeStamp)
                    }
                }

                updatedModel
            } else {
                model
            }
        }
    }

    private fun resolveTimeStamp(isExtendedHours: Boolean, quotes: List<Pair<MobileQuote, Quote>>): String? {
        return quotes
            .resolveTimeStamp(
                transform = { first.getTimeStamp(isExtendedHours) },
                timeStampFormatter = { formatTimeStamp(TimeUnit.SECONDS.toMillis(it)) }
            )
    }

    private fun onEmptyOrSuccess(quotes: List<Pair<MobileQuote, Quote>>): ViewState =
        if (quotes.isEmpty()) {
            ViewState.Empty
        } else {
            _sortState.postValue(null) // Reset sort order for new data from server

            val listOfSymbols = mutableListOf<SymbolCell>()
            val listOfCells = mutableListOf<List<ResultDataCell>>()
            quotes.forEach { (mobileQuote, quote) ->
                val instrumentType = InstrumentType.from(quote.productType)
                listOfSymbols.add(
                    SymbolCell(
                        mobileQuote.symbol,
                        mobileQuote.symbol,
                        mobileQuote.symbol,
                        instrumentType,
                        ""
                    )
                )
                val symbolCells = buildSymbolRowItem(
                    SymbolRowInfo(
                        symbol = mobileQuote.symbol,
                        price = mobileQuote.getFormattedLastPrice(),
                        change = mobileQuote.deriveDayChange(),
                        percentChange = mobileQuote.deriveDayChangePercent(),
                        volume = quote.volume,
                        tickIndicator = null
                    ),
                    fromMoversResponse = true,
                    instrumentType = instrumentType
                )
                listOfCells.add(symbolCells)
            }
            val timestamp = resolveTimeStamp(isExtendedHoursOnUseCase.executeBlocking(), quotes)
            ViewState.Success(listOfSymbols, listOfCells, timestamp)
        }

    private fun onEmptyOrError(error: Throwable): ViewState {
        return if (error is EmptyQuotesException) {
            ViewState.Empty
        } else {
            logger.error("Markets movers fetch failed: ${error.localizedMessage}")
            ViewState.Error("Markets movers fetch failed", error)
        }
    }

    private fun getMarketsRequest(activityType: ActivityType, exchangeType: ExchangeType) =
        when (activityType) {
            ActivityType.MostActive -> marketsService.getMostActives(exchangeType.value)
            ActivityType.TopPctGainer -> marketsService.getPctTopGainer(exchangeType.value)
            ActivityType.TopPctLoser -> marketsService.getPctTopLoser(exchangeType.value)
            ActivityType.TopDollarGainer -> marketsService.getDollarTopGainer(exchangeType.value)
            ActivityType.TopDollarLoser -> marketsService.getDollarTopLoser(exchangeType.value)
        }

    enum class ExchangeType(val displayName: String, val value: String) {
        NASDAQ("NASDAQ", "NSDQ") {
            override fun toString(): String {
                return displayName
            }
        },
        NYSE("NYSE", "NYSE") {
            override fun toString(): String {
                return displayName
            }
        }
    }

    sealed class ActivityType(val name: String) {
        object MostActive : ActivityType("Most Active")
        object TopPctGainer : ActivityType("Top % Gainers")
        object TopPctLoser : ActivityType("Top % Losers")
        object TopDollarGainer : ActivityType("Top $ Gainers")
        object TopDollarLoser : ActivityType("Top $ Losers")

        override fun toString(): String {
            return name
        }
    }
}
