package com.etrade.mobilepro.tableview

import android.view.View
import androidx.lifecycle.LiveData

/**
 * This goes under a table view.
 */
interface TableFooter {

    /**
     * Switches footer visibility.
     */
    val isFooterVisible: LiveData<Boolean>

    /**
     * Primary timestamp to display.
     */
    val primaryTimestamp: LiveData<CharSequence>

    /**
     * Secondary timestamp to display.
     */
    val secondaryTimestamp: LiveData<CharSequence>

    /**
     * Called when a disclosures view is clicked.
     */
    fun onDisclosuresViewClicked(v: View)
}
