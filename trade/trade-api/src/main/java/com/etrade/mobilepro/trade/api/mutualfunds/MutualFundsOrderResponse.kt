package com.etrade.mobilepro.trade.api.mutualfunds

import com.etrade.mobilepro.trade.api.OrderResponse

interface MutualFundsOrderResponse : OrderResponse {
    override val orders: List<MutualFundsOrder>
}

val MutualFundsOrderResponse.orderIds: List<String>
    get() = orders.map { it.orderId }

val MutualFundsOrderResponse.hasExchangeLeg: Boolean
    get() = orders.size > 1
