package com.etrade.mobilepro.markets.common

data class SymbolRowInfo(
    val symbol: String,
    val price: String,
    val change: String?,
    val percentChange: String?,
    val volume: String?,
    val tickIndicator: Short?
)
