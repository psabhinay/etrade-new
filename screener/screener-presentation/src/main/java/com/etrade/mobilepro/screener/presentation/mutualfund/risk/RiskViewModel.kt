package com.etrade.mobilepro.screener.presentation.mutualfund.risk

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.mutualfund.RiskFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.MutualFund
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class RiskViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @MutualFund screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<RiskFilter, RiskViewModel.ViewState>(aggregator, ScreenerType.MutualFund.Risks, screenerInteractor) {
    override fun provideInitialState(initialFilter: RiskFilter): ViewState = ViewState(filter = initialFilter)

    data class ViewState(
        override val filter: RiskFilter
    ) : BaseFilterScreenViewModel.ViewState<RiskFilter>
}
