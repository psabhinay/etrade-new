package com.etrade.mobilepro.accounts.util.android

import android.os.Parcel
import com.etrade.mobile.accounts.details.AccountDetails
import kotlinx.parcelize.Parceler
import java.math.BigDecimal

object ContributionDetailsParceler : Parceler<AccountDetails.ContributionDetails> {

    override fun create(parcel: Parcel): AccountDetails.ContributionDetails = parcel.run {
        object : AccountDetails.ContributionDetails {
            override val contributionAmount: BigDecimal? = readSerializable() as? BigDecimal
            override val taxYear: Int = readInt()
        }
    }

    override fun AccountDetails.ContributionDetails.write(parcel: Parcel, flags: Int) {
        parcel.apply {
            writeSerializable(contributionAmount)
            writeInt(taxYear)
        }
    }
}
