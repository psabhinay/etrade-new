package com.etrade.mobilepro.news.presentation.market

import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.api.repo.NewsRepo
import com.etrade.mobilepro.news.presentation.base.BaseNewsViewModel
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentLoader
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsFontAdjustHandler
import com.etrade.mobilepro.util.android.recyclerviewutil.SelectionTracker
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class MarketNewsViewModel @Inject constructor(
    newsRepo: NewsRepo,
    statusRepo: ReadStatusRepo,
    contentLoader: TextNewsContentLoader,
    fontAdjustHandler: TextNewsFontAdjustHandler,
    selectionTracker: SelectionTracker
) : BaseNewsViewModel(newsRepo, statusRepo, contentLoader, fontAdjustHandler, selectionTracker) {
    override fun getNewsType(): NewsType = NewsType.MARKET

    @ExperimentalCoroutinesApi
    override fun onBecomeActive() {
        refresh(false)
    }
}
