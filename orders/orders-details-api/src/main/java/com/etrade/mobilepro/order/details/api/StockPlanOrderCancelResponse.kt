package com.etrade.mobilepro.order.details.api

data class StockPlanOrderCancelResponse(
    val accountNumber: String
)
