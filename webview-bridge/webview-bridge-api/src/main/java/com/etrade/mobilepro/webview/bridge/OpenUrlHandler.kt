package com.etrade.mobilepro.webview.bridge

interface OpenUrlHandler {
    fun openUrl(url: String, isPdf: Boolean)
}
