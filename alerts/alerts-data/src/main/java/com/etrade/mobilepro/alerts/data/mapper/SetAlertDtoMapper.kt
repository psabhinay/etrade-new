package com.etrade.mobilepro.alerts.data.mapper

import com.etrade.mobilepro.alerts.api.model.SetAlertResponse
import com.etrade.mobilepro.alerts.api.model.StatusValue
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.alerts.api.model.UserAlertStringResources
import com.etrade.mobilepro.alerts.data.dto.SetAlertDetailXmlDto
import com.etrade.mobilepro.alerts.data.dto.SetAlertResponseXmlDto
import com.etrade.mobilepro.util.json.coerceToBool
import java.util.Locale

private const val DESCRIPTION_START_FORMAT = "%s: "

internal class SetAlertDtoMapper(strings: UserAlertStringResources) {
    private val detailMapper = SetAlertDetailDtoMapper(strings)
    fun map(xmlDto: SetAlertResponseXmlDto): SetAlertResponse =
        SetAlertResponse(
            hasError = coerceToBool(xmlDto.hasError, false),
            errorMessage = xmlDto.errorMessage ?: "",
            hasAlerts = coerceToBool(xmlDto.hasAlerts, false),
            alerts = xmlDto.alerts?.mapNotNull { detailMapper.map(it) } ?: emptyList()
        )
}

internal class SetAlertDetailDtoMapper(private val strings: UserAlertStringResources) {
    fun map(xmlDtoNullable: SetAlertDetailXmlDto?): UserAlertItem.Alert? {
        val xmlDto = xmlDtoNullable ?: return null
        val targetPriceHigh = StatusValue(
            value = xmlDto.targetPriceHigh ?: "",
            alertStatus = coerceToAlertStatus(xmlDto.targetPriceHighStatus)
        )
        val targetPriceLow = StatusValue(
            value = xmlDto.targetPriceLow ?: "",
            alertStatus = coerceToAlertStatus(xmlDto.targetPriceLowStatus)
        )
        val dailyPercentHigh = StatusValue(
            value = xmlDto.dailyPercentHigh ?: "",
            alertStatus = coerceToAlertStatus(xmlDto.dailyPercentHighStatus)
        )
        val dailyPercentLow = StatusValue(
            value = xmlDto.dailyPercentLow ?: "",
            alertStatus = coerceToAlertStatus(xmlDto.dailyPercentLowStatus)
        )
        val peTargetHigh = StatusValue(
            value = xmlDto.peTargetHigh ?: "",
            alertStatus = coerceToAlertStatus(xmlDto.peTargetHighStatus)
        )
        val peTargetLow = StatusValue(
            value = xmlDto.peTargetLow ?: "",
            alertStatus = coerceToAlertStatus(xmlDto.peTargetLowStatus)
        )
        val dailyVolume = StatusValue(
            value = xmlDto.dailyVolume ?: "",
            alertStatus = coerceToAlertStatus(xmlDto.dailyVolumeStatus)
        )
        val description = String.format(DESCRIPTION_START_FORMAT, xmlDto.symbol ?: "")
        val params = mutableListOf<String>()
        if (targetPriceLow.status || targetPriceHigh.status) {
            params.add(strings.targetPrice)
        }
        if (dailyPercentLow.status || dailyPercentHigh.status) {
            params.add(strings.dailyPercent)
        }
        if (peTargetLow.status || peTargetHigh.status) {
            params.add(strings.peTarget)
        }
        if (dailyVolume.status) {
            params.add(strings.dailyVolume)
        }
        return UserAlertItem.Alert(
            symbol = xmlDto.symbol ?: "",
            content = xmlDto.content ?: "",
            delete = xmlDto.delete ?: "",
            deleteInfo = xmlDto.deleteInfo ?: "",
            edit = xmlDto.edit ?: "",
            lastEdited = xmlDto.lastEdited ?: "",
            targetPriceHigh = targetPriceHigh,
            targetPriceLow = targetPriceLow,
            dailyPercentHigh = dailyPercentHigh,
            dailyPercentLow = dailyPercentLow,
            peTargetHigh = peTargetHigh,
            peTargetLow = peTargetLow,
            dailyVolume = dailyVolume,
            description = description + params.joinToString(", "),
            isDeleteBtnVisible = false
        )
    }
}

private fun coerceToAlertStatus(input: CharSequence?, default: StatusValue.AlertStatus = StatusValue.AlertStatus.NONE): StatusValue.AlertStatus {
    val normalized = input?.toString()?.lowercase(Locale.getDefault())?.trim() ?: return default
    return when (normalized) {
        "none" -> StatusValue.AlertStatus.NONE
        "disabled" -> StatusValue.AlertStatus.DISABLED
        "enabled" -> StatusValue.AlertStatus.ENABLED
        else -> default
    }
}
