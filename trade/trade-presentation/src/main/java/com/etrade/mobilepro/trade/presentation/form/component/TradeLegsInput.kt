package com.etrade.mobilepro.trade.presentation.form.component

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.dynamic.form.api.BaseInput
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.util.ValueCoder

internal class TradeLegsInput(id: String, valueCoder: ValueCoder<List<TradeLeg>>) :
    BaseInput<List<TradeLeg>>(id, valueCoder) {

    override val isVisible: LiveData<Boolean> = value.map { !it.isNullOrEmpty() }

    fun updateQuantity(index: Int, quantity: String) {
        val quantityCandidate = quantity.toIntOrNull()
        val items = value.value ?: return
        val leg = items.getOrNull(index)?.takeIf { it.quantity != quantityCandidate } ?: return

        // If quantity is 0, negative or invalid, return without setting any value.
        val newQuantity = quantityCandidate?.takeIf { it >= 0 } ?: return

        setValue(
            items.toMutableList()
                .apply { set(index, withUpdatedQuantity(leg, newQuantity)) }
                .toList()
        )
    }

    private fun withUpdatedQuantity(leg: TradeLeg, newQuantity: Int): TradeLeg {
        return object : TradeLeg {
            override val symbol: String = leg.symbol
            override val transactionType: TransactionType = leg.transactionType
            override val quantity: Int = newQuantity
            override val displaySymbol: String = leg.displaySymbol
            override val isAMOption: Boolean = leg.isAMOption
        }
    }
}
