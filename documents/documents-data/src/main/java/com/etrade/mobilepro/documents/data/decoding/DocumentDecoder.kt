package com.etrade.mobilepro.documents.data.decoding

import com.etrade.mobilepro.common.result.ETResult
import java.io.File

interface DocumentDecoder {
    suspend fun decodeToFile(fileName: String, base64gZippedData: String): ETResult<File>
}
