package com.etrade.mobilepro.inboxmessages

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.inboxmessages.binding.DashboardInformationBinding
import com.etrade.mobilepro.inboxmessages.binding.DynamicNavigationBinding
import com.etrade.mobilepro.inboxmessages.binding.InboxMessageBinding
import com.etrade.mobilepro.inboxmessages.binding.InboxMessageThumbnailImageBinding
import com.etrade.mobilepro.inboxmessages.binding.InboxMessageTitleViewBinding
import com.etrade.mobilepro.inboxmessages.binding.TradeInformationBinding
import com.etrade.mobilepro.util.android.extension.inflate

class InboxMessageViewPagerAdapter : PagerAdapter() {

    var onItemDismissListener: ((InboxMessage) -> Unit)? = null
    var onItemClickListener: ((InboxMessage) -> Unit)? = null
    var imageLoader: ImageLoader? = null

    private var items: MutableList<InboxMessage> = mutableListOf()

    override fun isViewFromObject(view: View, obj: Any): Boolean = view == obj

    override fun getCount(): Int = items.size

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun getItemPosition(obj: Any): Int = POSITION_NONE

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val item = items[position]
        val binder = getMessageBinder(item)

        return container.inflate(binder.layoutRes).also {
            binder.bind(item, it)
            container.addView(it)
        }
    }

    private fun getMessageBinder(item: InboxMessage): InboxMessageBinding = when (item.type) {
        MessageType.DASHBOARD_INFORMATION -> DashboardInformationBinding(
            onItemDismissListener,
            onItemClickListener
        )
        MessageType.DYNAMIC_NAVIGATION -> {
            if (item.hasThumbnail) {
                InboxMessageThumbnailImageBinding(
                    imageLoader = requireNotNull(imageLoader),
                    onItemClickListener = onItemClickListener
                )
            } else {
                if (item.isSimpleInboxMessage) {
                    InboxMessageTitleViewBinding(onItemClickListener)
                } else {
                    DynamicNavigationBinding(onItemClickListener)
                }
            }
        }
        MessageType.TRADE_INFORMATION -> TradeInformationBinding()
        else -> throw UnsupportedOperationException("Unsupported ${item.type}")
    }

    fun updateItems(payload: List<InboxMessage>) {
        items.clear()
        items.addAll(payload)

        notifyDataSetChanged()
    }
}
