package com.etrade.msaccounts.dto

enum class MsAccountSummarySectionDtoType(val labelTitle: String) {
    TOTAL_ASSETS("Total Assets"),
    TOTAL_LIABILITIES("Total Liabilities")
}
