package com.etrade.mobilepro.level2

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.R
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.streaming.api.Level2DataEvent
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2BookType
import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2Data
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class QuoteLevel2ViewModel @Inject constructor(
    private val streamingQuoteProvider: StreamingQuoteProvider<String, Level2Data, Level2BookType>,
    private val resources: Resources
) : ViewModel() {

    private val mutableViewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState>
        get() = mutableViewState

    private val compositeDisposable = CompositeDisposable()

    private var symbol: SearchResultItem.Symbol? = null

    fun setSymbol(symbol: SearchResultItem.Symbol) {
        this.symbol = symbol
    }

    fun streamLevel2(bookType: Level2BookType) {
        compositeDisposable.clear()
        mutableViewState.value = ViewState.Loading
        symbol?.let {
            compositeDisposable.add(
                streamingQuoteProvider.getQuote(it.title, it.type, setOf("L2_BOOK"), bookType).subscribeBy(
                    onNext = { data ->
                        val bidList = data.bidList
                        val askList = data.askList

                        if (askList.isNotEmpty() || bidList.isNotEmpty()) {
                            val quoteRowDataList = getQuoteRowDataList(bidList, askList)
                            mutableViewState.postValue(ViewState.Success(quoteRowDataList))
                        } else {
                            mutableViewState.postValue(ViewState.Error(resources.getString(R.string.level_2_no_data_error_msg)))
                        }
                    },
                    onError = {
                        mutableViewState.postValue(ViewState.Error(resources.getString(R.string.error_message_general)))
                    }
                )
            )
        }
    }

    fun haltStreaming() {
        compositeDisposable.clear()
        mutableViewState.postValue(ViewState.Error(resources.getString(R.string.level_2_no_data)))
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    private fun getQuoteRowDataList(
        bidList: List<Level2DataEvent>,
        askList: List<Level2DataEvent>
    ): List<QuoteRowData> {
        val quoteRowDataList = mutableListOf<QuoteRowData>()
        if (bidList.size > askList.size) {
            bidList.addToQuoteRow(isBid = true, second = askList, quoteRowDataList = quoteRowDataList)
        } else {
            askList.addToQuoteRow(isBid = false, second = bidList, quoteRowDataList = quoteRowDataList)
        }
        return quoteRowDataList
    }

    sealed class ViewState {
        object Loading : ViewState()
        data class Success(val result: List<QuoteRowData>) : ViewState()
        data class Error(val message: String) : ViewState()
    }
}

private fun List<Level2DataEvent>.addToQuoteRow(isBid: Boolean, second: List<Level2DataEvent>, quoteRowDataList: MutableList<QuoteRowData>) {
    forEachIndexed { index, level2DataEvent ->
        val rowData = if (isBid) {
            QuoteRowData(level2DataEvent, second.getEventValue(index))
        } else {
            QuoteRowData(second.getEventValue(index), level2DataEvent)
        }
        quoteRowDataList.add(rowData)
    }
}

private fun List<Level2DataEvent>.getEventValue(index: Int) =
    if (index < size) {
        this[index]
    } else {
        null
    }
