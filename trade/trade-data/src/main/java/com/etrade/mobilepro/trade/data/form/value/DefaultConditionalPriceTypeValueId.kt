package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.trade.form.api.value.ConditionalPriceTypeValueId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.util.MutualConverter

object DefaultConditionalPriceTypeValueId : ConditionalPriceTypeValueId, PriceTypeValueId by CommonAdvancedPriceTypeValueId {
    override val hiddenStop: String = "5"
    override val converter: MutualConverter<String, PriceType> by lazy { createConverter() }
}
