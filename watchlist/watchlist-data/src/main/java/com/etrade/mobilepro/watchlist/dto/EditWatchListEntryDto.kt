package com.etrade.mobilepro.watchlist.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EditWatchListEntryDto(
    @Json(name = "watchlistID")
    val watchlistID: String,
    @Json(name = "entryID")
    val entryID: String? = null,
    @Json(name = "symbol")
    val symbol: String,
    @Json(name = "typeCode")
    val typeCode: String? = null,
    @Json(name = "action")
    val action: String? = null,
    @Json(name = "newIndexID")
    val newIndexID: Long? = null,
    @Json(name = "positionType")
    val positionType: Int = 1
)
