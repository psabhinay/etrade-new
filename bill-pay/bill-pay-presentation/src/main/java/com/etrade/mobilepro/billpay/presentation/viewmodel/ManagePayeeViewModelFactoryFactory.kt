package com.etrade.mobilepro.billpay.presentation.viewmodel

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.presentation.managepayee.ManagePayeeViewModel
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

interface ManagePayeeViewModelFactoryFactory {
    fun create(
        payees: List<Payee>
    ): ViewModelProvider.Factory
}

class ManagePayeeViewModelFactoryFactoryImp @Inject constructor(
    private val factory: ManagePayeeViewModel.Factory
) : ManagePayeeViewModelFactoryFactory {

    override fun create(
        payees: List<Payee>
    ): ViewModelProvider.Factory =
        viewModelFactory { factory.create(payees) }
}
