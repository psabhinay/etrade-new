package com.etrade.mobilepro.inboxmessages.binding

import androidx.core.view.updateLayoutParams
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.InboxMessagesView

@BindingAdapter("imageLoader")
internal fun setImageLoader(view: InboxMessagesView, imageLoader: ImageLoader?) {
    imageLoader?.let {
        view.imageLoader = imageLoader
    }
}

@BindingAdapter("inboxMessages")
internal fun setInboxMessages(view: InboxMessagesView, messages: List<InboxMessage>?) {
    messages?.let {
        view.setMessages(messages)
    }
}

@BindingAdapter("dismissListener")
internal fun onItemDismissListener(view: InboxMessagesView, callback: ((InboxMessage) -> Unit)?) {
    callback?.let {
        view.onItemDismissListener = callback
    }
}

@BindingAdapter("clickListener")
internal fun onItemClickListener(view: InboxMessagesView, callback: ((InboxMessage) -> Unit)?) {
    callback?.let {
        view.onItemClickListener = callback
    }
}

@BindingAdapter("layout_height")
internal fun setLayoutHeight(view: InboxMessagesView, height: Int?) {
    height?.let {
        view.updateLayoutParams { this.height = it }
    }
}
