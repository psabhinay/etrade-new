package com.etrade.mobilepro.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class SafeEnumsKtTest {

    private enum class TestEnum {
        DEFAULT,
        FOOBAR
    }

    @Test
    fun `test enumValueOrDefault() returning valid enum for valid string`() {
        assertEquals(enumValueOrDefault("FOOBAR", TestEnum.DEFAULT), TestEnum.FOOBAR)
    }

    @Test
    fun `test enumValueOrDefault() returning default enum for invalid string`() {
        val defaultEnum = TestEnum.DEFAULT
        assertEquals(enumValueOrDefault("INVALID", defaultEnum), defaultEnum)
    }
}
