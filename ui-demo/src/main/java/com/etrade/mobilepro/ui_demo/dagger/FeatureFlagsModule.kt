package com.etrade.mobilepro.ui_demo.dagger

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.etrade.mobilepro.feature.DefaultDelegatingFeatureFlagRepo
import com.etrade.mobilepro.feature.Feature
import com.etrade.mobilepro.feature.FeatureFlagProvider
import com.etrade.mobilepro.feature.FeatureFlagRepo
import com.etrade.mobilepro.feature.firebase.FirebaseFeatureFlagProvider
import com.etrade.mobilepro.feature.local.FLAG_STORE_NAME
import com.etrade.mobilepro.feature.local.LocalFeatureFlagStore
import com.etrade.mobilepro.feature.local.qualifier.FeatureFlagDataStore
import com.etrade.mobilepro.feature.presentation.viewmodel.DefaultFeatureFlagsViewModel
import com.etrade.mobilepro.feature.presentation.viewmodel.FeatureFlagsViewModel
import com.etrade.mobilepro.feature.qualifiers.AvailableFeatureFlagProviders
import com.etrade.mobilepro.feature.qualifiers.AvailableFeatures
import com.etrade.mobilepro.feature.qualifiers.RemoteEnabledFeatures
import com.etrade.mobilepro.ui_demo.featureflags.DemoFeatureFlags
import dagger.Binds
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton

@Module
interface FeatureFlagsModule {

    @ExperimentalCoroutinesApi
    @Binds
    fun bindFeatureFlagsViewModel(model: DefaultFeatureFlagsViewModel): FeatureFlagsViewModel

    @ExperimentalCoroutinesApi
    @Binds
    @Singleton
    fun bindFeatureFlagRepo(provider: DefaultDelegatingFeatureFlagRepo): FeatureFlagRepo

    companion object {

        private val Context.featureFlagDataStore: DataStore<Preferences> by preferencesDataStore(
            FLAG_STORE_NAME
        )

        @Provides
        @AvailableFeatures
        fun provideAvailableFeatures(): List<Feature> = DemoFeatureFlags.values().toList()

        @Provides
        @RemoteEnabledFeatures
        fun provideRemoteEnabledFeatures(): Set<Feature> =
            DemoFeatureFlags.getRemoteEnabledFeatures()

        @Provides
        @Singleton
        @FeatureFlagDataStore
        fun provideFeatureFlagDataStore(context: Context): DataStore<Preferences> =
            context.featureFlagDataStore

        @Provides
        @AvailableFeatureFlagProviders
        fun provideFeatureFlagProviders(
            localStore: LocalFeatureFlagStore,
            firebaseProvider: FirebaseFeatureFlagProvider
        ): List<FeatureFlagProvider> =
            listOf(localStore, firebaseProvider)
    }
}
