package com.etrade.mobilepro.trade.api

import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import java.math.BigDecimal

interface WithPriceOrder {
    val priceType: PriceType
    val term: OrderTerm
    val allOrNone: Boolean
    val limitPrice: BigDecimal?
    val stopPrice: BigDecimal?
    val yourPrice: String?
}
