package com.etrade.mobilepro.pnl.calculations

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.util.domain.data.Gain
import com.etrade.mobilepro.util.domain.data.deriveBondTotalCost
import com.etrade.mobilepro.util.domain.data.deriveCommonTotalCost
import com.etrade.mobilepro.util.domain.data.deriveDaysGain
import com.etrade.mobilepro.util.domain.data.deriveOptionTotalCost
import com.etrade.mobilepro.util.domain.data.deriveTotalGain
import com.etrade.mobilepro.util.domain.data.deriveTotalGainPercentage
import com.etrade.mobilepro.util.math.BOND_MULTIPLIER
import java.math.BigDecimal

@Suppress("LongParameterList", "LongMethod", "ComplexCondition")
fun calculateTotalGain(
    instrumentType: InstrumentType,
    currentPriceDecimal: BigDecimal?,
    pricePaidDecimal: BigDecimal?,
    quantityDecimal: BigDecimal?,
    commissionDecimal: BigDecimal?,
    feesDecimal: BigDecimal?,
    instrumentMultiplier: BigDecimal? = BigDecimal.ONE
): Gain? {
    if (currentPriceDecimal == null || pricePaidDecimal == null || quantityDecimal == null || commissionDecimal == null || feesDecimal == null) {
        return null
    }

    val totalGain = deriveTotalGain(
        currentPrice = currentPriceDecimal,
        pricePaid = pricePaidDecimal,
        quantity = quantityDecimal,
        commission = commissionDecimal,
        fees = feesDecimal
    )
    val totalCost = calculateTotalCost(instrumentType, pricePaidDecimal, quantityDecimal, commissionDecimal, feesDecimal, instrumentMultiplier)
    val totalGainPercentage = deriveTotalGainPercentage(totalGain, totalCost)

    return if (totalGainPercentage == null) null else Gain(totalGain, totalGainPercentage)
}

@Suppress("LongParameterList")
fun calculateTotalCost(
    instrumentType: InstrumentType,
    pricePaid: BigDecimal,
    quantity: BigDecimal,
    commission: BigDecimal,
    fees: BigDecimal,
    instrumentMultiplier: BigDecimal? = BigDecimal.ONE
): BigDecimal {
    return when {
        instrumentType.isOption -> deriveOptionTotalCost(pricePaid, quantity, commission, fees, instrumentMultiplier)
        instrumentType == InstrumentType.BOND -> deriveBondTotalCost(pricePaid, quantity, commission, fees)
        else -> deriveCommonTotalCost(pricePaid, quantity, commission, fees)
    }
}

@Suppress("LongParameterList", "LongMethod")
fun calculateDaysGain(
    instrumentType: InstrumentType,
    lastPriceDecimal: BigDecimal?,
    previousCloseDecimal: BigDecimal?,
    quantityDecimal: BigDecimal?,
    todayQuantityDecimal: BigDecimal?,
    todayPricePaidDecimal: BigDecimal?,
    todayCommissionDecimal: BigDecimal?,
    feesDecimal: BigDecimal?,
    basisPrice: BigDecimal?
): Gain? {
    if (lastPriceDecimal == null || previousCloseDecimal == null || quantityDecimal == null) {
        return null
    }

    if (instrumentType == InstrumentType.BOND) {
        val bondMultiplier = BigDecimal(BOND_MULTIPLIER)
        var bondTodayPricePaidDecimal: BigDecimal? = null
        val bondLastPriceDecimal = lastPriceDecimal.divide(bondMultiplier)
        val bondPreviousCloseDecimal = previousCloseDecimal.divide(bondMultiplier)
        if (todayPricePaidDecimal != null && todayPricePaidDecimal.compareTo(BigDecimal.ZERO) != 0) {
            bondTodayPricePaidDecimal = todayPricePaidDecimal.divide(bondMultiplier)
        }
        return deriveDaysGain(
            bondLastPriceDecimal, bondPreviousCloseDecimal, quantityDecimal,
            todayQuantityDecimal, bondTodayPricePaidDecimal, todayCommissionDecimal, feesDecimal
        )
    }

    return deriveDaysGain(
        lastPriceDecimal, previousCloseDecimal, quantityDecimal,
        todayQuantityDecimal, todayPricePaidDecimal, todayCommissionDecimal, feesDecimal, basisPrice
    )
}
