-if class com.etrade.mobilepro.chart.data.ChartRequestValueDto
-keepnames class com.etrade.mobilepro.chart.data.ChartRequestValueDto
-if class com.etrade.mobilepro.chart.data.ChartRequestValueDto
-keep class com.etrade.mobilepro.chart.data.ChartRequestValueDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.etrade.mobilepro.chart.data.ChartRequestValueDto
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.etrade.mobilepro.chart.data.ChartRequestValueDto
-keepclassmembers class com.etrade.mobilepro.chart.data.ChartRequestValueDto {
    public synthetic <init>(java.util.List,java.util.List,java.lang.String,java.lang.String,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
