package com.etrade.mobilepro.screener.api.result

import com.etrade.mobilepro.quoteapi.MobileQuote

data class ScreenerResult(
    val mobileQuotes: List<MobileQuote> = emptyList(),
    val prescreenData: List<PrescreenData> = emptyList()
) {
    val asMap: Map<String, PrescreenedQuote>

    init {
        asMap = mutableMapOf()
        mobileQuotes.forEach { quote ->
            val data = prescreenData.find { it.symbol == quote.symbol }
            asMap[quote.symbol] = PrescreenedQuote(
                quote,
                data
            )
        }
    }
}

class PrescreenedQuote(
    val quote: MobileQuote,
    val prescreenData: PrescreenData?
)

data class PrescreenData(
    val symbol: String,
    val price4WeeksSP500: String,
    val price13WeeksSP500: String,
    val price52WeeksSP500: String,
    val price4WeeksIndustry: String,
    val price13WeeksIndustry: String,
    val price52WeeksIndustry: String,
    val peRatioTrailing12: String,
    val peRatioCurrentYear: String,
    val peRatioNextYear: String,
    val priceToBook: String,
    val epsGrowthAnnualCurrent: String,
    val epsGrowthAnnualNext: String,
    val revenueGrowthAnnualCurrent: String,
    val revenueGrowthAnnualNext: String,
    val epsGrowthQuarterly: String,
    val revenueGrowthQuarterly: String,
    val dividendYield: String,
    val priceChange52WeeksLow: String,
    val priceChange52WeeksHigh: String,
    val marketReturnCategory1Month: String,
    val marketReturnCategory3Months: String,
    val marketReturnCategory6Months: String,
    val marketReturnCategoryYTD: String,
    val marketReturnCategory1Year: String,
    val marketReturnCategory3Years: String,
    val marketReturnCategory5Years: String,
    val marketReturnCategory10Years: String,
    val marketReturnCategorySinceInception: String,
    val navReturnCategory1Month: String,
    val navReturnCategory3Months: String,
    val navReturnCategory6Months: String,
    val navReturnCategoryYTD: String,
    val navReturnCategory1Year: String,
    val navReturnCategory3Years: String,
    val navReturnCategory5Years: String,
    val navReturnCategory10Years: String,
    val navReturnCategorySinceInception: String,
    val standardDeviation: String,
    val yieldValue: String,
    val distributionYield: String,
    val categoryReturn1Year: String,
    val categoryReturn5Years: String,
    val categoryReturn10Years: String,
    val turnOverRatio: String,
    val portfolioConcentration: String,
    val averageMarketCap: String,
    val frontLoad: String,
    val deferredLoad: String,
    val fundName: String,
    val morningstarRatingSinceIncep: String,
    val fundCategory: String,
    val morningstarAsOfDate: String,
    val morningstarRatingOutOfFunds3Year: String,
    val totalAssets: String,
    val expenseRatio: String,
    val alpha: String,
    val beta: String,
    val sharpeRatio: String,
    val rSquared: String,
    val volume30daysAvg: String,
    var redemptionFee: String
)

data class MorningstarRatingResult(
    val starsCount: Int,
    val fundsCount: Int
)
