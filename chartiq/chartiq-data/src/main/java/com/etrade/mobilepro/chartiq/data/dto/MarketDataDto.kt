package com.etrade.mobilepro.chartiq.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class MarketDataDto(

    @Json(name = "price")
    val price: Double = 0.0,

    @Json(name = "volume")
    val volume: Long = 0,

    @Json(name = "timestamp")
    val timestamp: Long = 0,

    @Json(name = "symbol")
    val ticker: String? = null
)
