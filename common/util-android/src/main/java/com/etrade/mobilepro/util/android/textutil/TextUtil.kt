package com.etrade.mobilepro.util.android.textutil

import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.text.style.URLSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.TextView
import androidx.core.text.scale
import androidx.core.text.superscript
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.util.android.clicklisteners.DebounceClickableSpan

const val SCALE_SUPERSCRIPT_DEFAULT_PROPORTION = 0.625F

class TextUtil {
    companion object {
        @Suppress("DEPRECATION")
        fun convertTextWithHmlTagsToSpannable(charSequence: CharSequence): Spannable {
            return Html.fromHtml(charSequence.toString(), Html.FROM_HTML_MODE_LEGACY) as Spannable
        }
    }
}

fun Spannable.updateTextPaintForUrlSpans(drawUnderlined: Boolean): Spannable {
    for (u in this.getSpans(0, this.length, URLSpan::class.java)) {
        this.setSpan(UnderlineSpanWithParam(underlineText = drawUnderlined), this.getSpanStart(u), this.getSpanEnd(u), 0)
    }
    return this
}

class UnderlineSpanWithParam(private val underlineText: Boolean) : UnderlineSpan() {

    fun textIsUnderlined() = underlineText

    override fun updateDrawState(ds: TextPaint) {
        ds.isUnderlineText = underlineText
    }
}

fun Spannable.setLinkClickListener(listener: (url: String) -> Unit): Spannable {
    for (span in this.getSpans(0, this.length, URLSpan::class.java)) {
        this.setSpan(
            object : ClickableSpan() {
                override fun onClick(widget: View) {
                    listener.invoke(span.url)
                }
            },
            this.getSpanStart(span), this.getSpanEnd(span), 0
        )
    }
    return this
}

fun SpannableString.setClickableSpan(start: Int, end: Int, action: () -> Unit) {
    setSpan(DebounceClickableSpan(action), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
}

@BindingAdapter(value = ["actualText", "defaultText"], requireAll = false)
fun TextView.setTextWithFallback(actualText: String?, defaultText: String) {
    this.text = if (actualText.isNullOrEmpty()) {
        defaultText
    } else {
        actualText
    }
}

inline fun SpannableStringBuilder.scaleSuperscript(
    proportion: Float = SCALE_SUPERSCRIPT_DEFAULT_PROPORTION,
    builderAction: SpannableStringBuilder.() -> Unit
): SpannableStringBuilder =
    scale(proportion) { superscript(builderAction) }
