package com.etrade.mobilepro.screener.data.result

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType
import io.reactivex.Completable
import io.reactivex.Single

interface ResultColumnsProvider {
    fun getColumns(filterSnapshot: List<ScreenerFilter>): Single<List<ResultColumn>>
    fun saveColumns(group: ScreenerType.Group, columns: List<ResultColumn>): Completable
}
