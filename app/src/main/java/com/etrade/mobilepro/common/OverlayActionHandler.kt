package com.etrade.mobilepro.common

import android.content.Context
import android.content.Intent
import com.etrade.mobilepro.R
import com.etrade.mobilepro.navdestinations.openDisclosures
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity

const val EULA_URL = "etrade://overlay/eula"
const val EULA_TITLE = "End-User License Agreement"
const val ALERT_TERMS_AND_CONDITIONS_URL = "etrade://overlay/alert_terms"
const val DISCLOSURES = "etrade://overlay/disclosures"
private const val EULA = "eula"
private const val ALERT_TERMS = "alert_terms"
private const val PRIVACY_POLICY = "privacy_policy"

fun getOverlayIntentForUrlAction(url: String, title: String, context: Context): Intent {
    val text = when {
        url.contains(EULA, true) -> context.resources.getString(R.string.eula)
        url.contains(PRIVACY_POLICY, true) -> context.resources.getString(R.string.privacy_policy)
        url.contains(ALERT_TERMS, true) -> context.resources.getString(R.string.alert_terms_and_conditions_content)
        url.contains(DISCLOSURES, true) -> return context.openDisclosures()
        else -> ""
    }
    return SpannableTextOverlayActivity.intent(
        context,
        title,
        text,
        com.etrade.mobilepro.overlay.R.drawable.ic_arrow_back
    )
}
