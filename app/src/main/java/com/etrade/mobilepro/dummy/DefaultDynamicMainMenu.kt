package com.etrade.mobilepro.dummy

import android.content.res.Resources
import com.etrade.mobilepro.R
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.menu.MenuDeepLink
import com.etrade.mobilepro.menu.dto.DataDetail
import com.etrade.mobilepro.menu.dto.MenuDto
import com.etrade.mobilepro.menu.dto.MenuItem
import com.etrade.mobilepro.menu.mapper.mapToDynamicMenuItems
import com.etrade.mobilepro.menu.repo.DynamicMenuItem
import javax.inject.Inject

class DefaultDynamicMainMenu @Inject constructor(
    @Web val webBaseUrl: String,
    val resources: Resources
) {

    fun createDynamicMenuItems(): List<DynamicMenuItem> {
        return createDynamicMenuItemDtos().mapToDynamicMenuItems(webBaseUrl)
    }

    @Suppress("LongMethod")
    fun createDynamicMenuItemDtos(): List<MenuDto> {
        val views = mutableListOf<MenuDto>()
        val type = "menu_section"
        // Alert
        val alertsMenuDetail = DataDetail(
            listOf(
                MenuItem(
                    menuType = "MenuItemViewAlertCount",
                    label = resources.getString(R.string.alerts),
                    icon = "ic_alerts",
                    ctaAction = ClickActionDto(appUrl = MenuDeepLink.ALERTS.appUrl)
                )
            )
        )
        views.add(MenuDto(data = alertsMenuDetail, type = type))
        // Check Deposit
        val checkDepositMenuDetail = DataDetail(
            listOf(
                MenuItem(
                    menuType = "MenuItemView",
                    label = resources.getString(R.string.check_deposit),
                    icon = "ic_check_deposit",
                    ctaAction = ClickActionDto(appUrl = MenuDeepLink.CHECK_DEPOSIT.appUrl)
                ),
                MenuItem(
                    menuType = "MenuItemView",
                    label = resources.getString(R.string.transfer),
                    icon = "ic_transfer",
                    ctaAction = ClickActionDto(appUrl = MenuDeepLink.TRANSFER.appUrl)
                )
            )
        )
        views.add(MenuDto(data = checkDepositMenuDetail, type = type))
        // My Profile
        val myProfileMenuDetail = DataDetail(
            listOf(
                MenuItem(
                    menuType = "MenuItemView",
                    label = resources.getString(R.string.my_profile),
                    icon = "ic_profile",
                    ctaAction = ClickActionDto(appUrl = MenuDeepLink.USER_PROFILE.appUrl)
                ),
                MenuItem(
                    menuType = "MenuItemView",
                    label = resources.getString(R.string.settings),
                    icon = "ic_settings",
                    ctaAction = ClickActionDto(appUrl = MenuDeepLink.SETTINGS.appUrl)
                ),
                MenuItem(
                    menuType = "MenuItemView",
                    label = resources.getString(R.string.customer_service),
                    icon = "ic_customer_service",
                    ctaAction = ClickActionDto(appUrl = MenuDeepLink.CUSTOMER_SERVICE.appUrl)
                )
            )
        )
        views.add(MenuDto(data = myProfileMenuDetail, type = type))

        // Quotes
        val quotesMenuDetail = DataDetail(
            listOf(
                MenuItem(
                    menuType = "MenuItemView",
                    label = resources.getString(R.string.quotes),
                    icon = "ic_quotes",
                    ctaAction = ClickActionDto(appUrl = MenuDeepLink.QUOTES.appUrl)
                ),
                MenuItem(
                    menuType = "MenuItemView",
                    label = resources.getString(R.string.news),
                    icon = "ic_news",
                    ctaAction = ClickActionDto(appUrl = MenuDeepLink.NEWS.appUrl)
                ),
                MenuItem(
                    menuType = "MenuItemView",
                    label = resources.getString(R.string.bloombergtv),
                    icon = "ic_bloombergtv",
                    ctaAction = ClickActionDto(appUrl = MenuDeepLink.BLOOMBERG_TV.appUrl)
                ),
                MenuItem(
                    menuType = "MenuItemView",
                    label = resources.getString(R.string.open_new_account),
                    icon = "ic_open_account",
                    ctaAction = ClickActionDto(appUrl = MenuDeepLink.OPEN_ACCOUNT.appUrl)
                )
            )
        )
        views.add(MenuDto(data = quotesMenuDetail, type = type))
        return views
    }
}
