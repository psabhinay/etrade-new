package com.etrade.mobilepro.accounts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import kotlinx.coroutines.launch
import javax.inject.Inject

const val COUNT_TO_SHOW_ACCOUNT_ORGANIZATION = 2

class AccountsOrganizeBoardingViewModel @Inject constructor(
    private val accountOrderRepo: AccountOrderRepo,
    private val accountRepo: AccountListRepo,
    private val applicationPreferences: ApplicationPreferences
) : EditListViewModel<AccountOrderItem>() {

    private val _sessionState = MutableLiveData<SessionStateChange>()

    private val accounts: List<AccountOrderItem>
        get() = accountRepo.getAccountList().toOrderAccountList()

    private val _showAccountsOrganizeBoarding = MediatorLiveData<AccountOrganizeOnboardState>().apply {
        fun isValidLoginState(sessionsState: SessionStateChange) =
            applicationPreferences.homeLandingCount == COUNT_TO_SHOW_ACCOUNT_ORGANIZATION && sessionsState is SessionStateChange.AUTHENTICATED

        fun merge(accountsSize: Int, sessionsState: SessionStateChange) {
            value = AccountOrganizeOnboardState(
                show = applicationPreferences.shouldShowAccountsOrganizeBoarding &&
                    isValidLoginState(sessionsState) &&
                    accountsSize > 1,
                showDialogs = isValidLoginState(sessionsState) && accountsSize == 1
            )
        }

        if (applicationPreferences.shouldShowAccountsOrganizeBoarding) {
            addSource(_sessionState) {
                merge(accounts.size, it)
            }
        } else {
            merge(0, _sessionState.value ?: SessionStateChange.UNAUTHENTICATED)
        }
    }
    val showAccountsOrganizeBoarding: LiveData<AccountOrganizeOnboardState>
        get() = _showAccountsOrganizeBoarding.distinctUntilChanged()

    private val _accountsOrderingChanged: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()
    val accountsOrderingChanged: LiveData<ConsumableLiveEvent<Unit>>
        get() = _accountsOrderingChanged

    fun userSessionStateChange(event: SessionStateChange) {
        _sessionState.value = event
    }

    override fun onFetchItems(viewState: MutableLiveData<ViewState>) {
        viewModelScope.launch {
            provideItems(accountOrderRepo.getAccounts())
        }
    }

    override fun onApplyChanges(items: List<AccountOrderItem>, removedItems: List<AccountOrderItem>, viewState: MutableLiveData<ViewState>) {
        viewModelScope.launch {
            accountOrderRepo.saveAccounts(items)
            _accountsOrderingChanged.postValue(ConsumableLiveEvent(Unit))
            viewState.value = ViewState.Success()
        }
    }

    override fun createFrom(item: AccountOrderItem, value: Boolean): AccountOrderItem {
        return AccountOrderItem(item.displayText, item.uuid, value)
    }

    fun hideAccountsOrganizeBoarding(showDialogs: Boolean = false) {
        applicationPreferences.shouldShowAccountsOrganizeBoarding = false
        _showAccountsOrganizeBoarding.value = AccountOrganizeOnboardState(
            show = false,
            showDialogs = showDialogs
        )
    }
}

internal fun List<Account>.toOrderAccountList(): List<AccountOrderItem> =
    this.map { AccountOrderItem(it.accountShortName, it.accountUuid) }

data class AccountOrganizeOnboardState(
    val show: Boolean,
    val showDialogs: Boolean
)
