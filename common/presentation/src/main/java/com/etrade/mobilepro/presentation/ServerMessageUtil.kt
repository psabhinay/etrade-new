package com.etrade.mobilepro.presentation

import android.content.res.Resources
import android.text.SpannableStringBuilder
import androidx.core.text.HtmlCompat
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.backends.api.EXTENDED_HOURS_DISCLOSURE_ERROR_CODE
import com.etrade.mobilepro.backends.api.HasWarnings
import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.backends.api.ServerResponseException
import com.etrade.mobilepro.backends.api.disclosureErrors
import com.etrade.mobilepro.backends.api.warningTitleDisclosures
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.appDialog

/**
 * Handles a response. If a response is successful it's handling is delegated to [handleWarnings] method proceeding with [onSuccess]. If a response has failed
 * with [ServerResponseException] it handles all [ServerMessage]s from [ServerResponseException.message], otherwise it invokes [onUnhandledFailure].
 *
 * Essentially this will push one or more dialogs to [dialog] object for a user to see and, if needed, will invoke [onSuccess] or [onUnhandledFailure] actions.
 *
 * @param resources The app's resources.
 * @param dialog The [MutableLiveData] object to initiate a dialog with a user in case there are some warnings or errors.
 * @param onSuccess The callback to be invoked if a result was overall successful and we can proceed with the next phase of an operation.
 * @param onUnhandledFailure The callback to be invoked of a result has failed with errors and the error can not be handled by this method.
 *
 * @see handleServerMessages
 * @see HasWarnings
 */
fun <T : HasWarnings> ETResult<T>.handleResult(
    resources: Resources,
    baseUrl: String,
    dialog: MutableLiveData<AppDialog?>,
    onSuccess: (T) -> Unit,
    onUnhandledFailure: (Throwable) -> Unit
): ETResult<T> {
    return onSuccess { response ->
        response.handleWarnings(resources, baseUrl, dialog, onSuccess)
    }.onFailure { exception ->
        if (exception is ServerResponseException) {
            val error = exception.error
            if (error is ServerError.Known) {
                handleServerMessages(resources, baseUrl, error.messages, dialog, emptyMap())
                return@onFailure
            }
        }
        onUnhandledFailure(exception)
    }
}

/**
 * Handles warnings and then invokes [afterLastWarning] when the last warning is pushed to [dialog]. If there are no warnings [afterLastWarning] is invoked
 * immediately.
 *
 * @param resources The app's resources.
 * @param dialog The [MutableLiveData] object to initiate a dialog with a user in case there are some warnings.
 * @param afterLastWarning The callback to be invoked after last warning is consumed.
 *
 * @see handleServerMessages
 * @see HasWarnings
 */
fun <T : HasWarnings> T.handleWarnings(
    resources: Resources,
    baseUrl: String,
    dialog: MutableLiveData<AppDialog?>,
    afterLastWarning: (T) -> Unit
) {
    val warnings = warnings
    if (warnings.isNotEmpty()) {
        handleServerMessages(resources, baseUrl, warnings, dialog, emptyMap()) {
            afterLastWarning(this)
        }
    } else {
        afterLastWarning(this)
    }
}

/**
 * Handles server messages by pushing dialogs into [dialog] object.
 *
 * There are two levels of severity for [ServerMessage]: warnings and errors. Errors always have precedence over warnings, so they will be pushed first to
 * [dialog] object. Also there is [ServerMessage.priority] for each message. The method will group messages by priorities (e.g. it will show them as a single
 * dialog) concatenating messages with 2 new lines between them. Lower priority values have precedence.
 *
 * For example consider the following list of messages (pseudo code):
 *
 * ```
 * warning(message = "Some Warning 1", priority = 1)
 * warning(message = "Some Warning 2", priority = 2)
 * error(message = "Some Error 1", priority = 2)
 * error(message = "Some Error 2", priority = 2)
 * warning(message = "Some Warning 3", priority = 1)
 * error(message = "Some Error 3", priority = 1)
 * ```
 *
 * This will result to 4 dialogs in this specific order (pseudo code):
 *
 * ```
 * dialog(message = "Some Error 3")
 * dialog(message = "Some Error 1\n\nSome Error 2")
 * dialog(message = "Some Warning 1\n\nSome Warning 3")
 * dialog(message = "Some Warning 2")
 * ```
 *
 * If there are no messages this method will invoke [afterLastMessage] immediately.
 *
 * @param resources The app's resources.
 * @param messages The collection of messages to show.
 * @param dialog The [MutableLiveData] object to initiate a dialog with a user.
 * @param navigationMap The [Map] that contains actions to be invoked for specific server messages error codes.
 * @param afterLastMessage The callback to be invoked after the last dialog is consumed.
 */
@Suppress("LongParameterList")
fun handleServerMessages(
    resources: Resources,
    baseUrl: String,
    messages: Collection<ServerMessage>,
    dialog: MutableLiveData<AppDialog?>,
    navigationMap: Map<String, () -> Unit>,
    afterLastMessage: () -> Unit = { /* intentionally blank */ }
) {
    if (messages.isEmpty()) {
        afterLastMessage()
    } else {
        val groups = messages.groupBy { GroupKey(it.type, it.priority) }
        val groupNumbers = groups.keys.sorted()
        showDialog(resources, baseUrl, groups, groupNumbers, 0, dialog, navigationMap) {
            dialog.value = null
            afterLastMessage()
        }
    }
}

@Suppress("LongParameterList")
private fun showDialog(
    resources: Resources,
    baseUrl: String,
    groups: Map<GroupKey, List<ServerMessage>>,
    groupKeys: List<GroupKey>,
    groupIndex: Int,
    dialog: MutableLiveData<AppDialog?>,
    navigationMap: Map<String, () -> Unit>,
    onLastDialogPositiveAction: () -> Unit
) {
    dialog.value = appDialog {
        val group = groups[groupKeys[groupIndex]]
        val serverMessage = group?.firstOrNull()
        title = serverMessage.getDialogTitle(resources)
        message = group?.toMessageText(resources, baseUrl) ?: ""
        positiveAction {
            label = serverMessage.getPositiveLabel(resources)
            val index = groupIndex + 1
            action = getActionForMessage(groupKeys, index, serverMessage, resources, baseUrl, groups, dialog, navigationMap, onLastDialogPositiveAction)
        }
        if (serverMessage?.type == ServerMessage.Type.DISCLOSURE_ERROR) {
            negativeAction { label = resources.getString(R.string.trade_dialog_not_now) }
        } else {
            cancelAction = { dialog.value = null }
        }
    }
}

@Suppress("LongParameterList")
private fun getActionForMessage(
    groupKeys: List<GroupKey>,
    index: Int,
    message: ServerMessage?,
    resources: Resources,
    baseUrl: String,
    groups: Map<GroupKey, List<ServerMessage>>,
    dialog: MutableLiveData<AppDialog?>,
    navigationMap: Map<String, () -> Unit>,
    onLastDialogPositiveAction: () -> Unit
): (() -> Unit) {
    return if (groupKeys.size > index) {
        {
            navigationMap[message?.code]?.invoke()
            showDialog(resources, baseUrl, groups, groupKeys, index, dialog, navigationMap, onLastDialogPositiveAction)
        }
    } else {
        navigationMap[message?.code] ?: onLastDialogPositiveAction
    }
}

private fun ServerMessage?.getDialogTitle(resources: Resources): String {
    return if (this != null) {
        val stringRes = if (code == EXTENDED_HOURS_DISCLOSURE_ERROR_CODE.toString()) {
            R.string.trade_title_extended_hours_error
        } else {
            when (type) {
                ServerMessage.Type.ERROR -> R.string.title_error
                ServerMessage.Type.WARN -> R.string.please_note
                ServerMessage.Type.DISCLOSURE_ERROR -> getDisclosureTitle()
                else -> R.string.please_note
            }
        }
        resources.getString(stringRes)
    } else {
        resources.getString(R.string.please_note)
    }
}

private fun ServerMessage.getDisclosureTitle() =
    if (warningTitleDisclosures.contains(code)) {
        R.string.title_warning
    } else {
        R.string.please_note
    }

private fun ServerMessage?.getPositiveLabel(resources: Resources) =
    if (disclosureErrors.contains(this?.code)) {
        resources.getString(R.string.trade_sign_disclosure)
    } else {
        resources.getString(R.string.ok)
    }

internal fun String.fixHref(baseUrl: String): String {
    val regex = """(<a\s+(?:[^>]*?\s+)?href=)(["'])(/.*?)\2""".toRegex()
    return replace(regex) { match ->
        val (tagStart, quote, path) = match.destructured
        "$tagStart$quote$baseUrl$path$quote"
    }
}

private fun List<ServerMessage>.toMessageText(resources: Resources, baseUrl: String): CharSequence {
    return map { it.text }
        .map {
            it.takeIf {
                !resources.getString(R.string.trade_sign_disclosure_inadequate_message).contentEquals(it)
            } ?: resources.getString(R.string.trade_sign_disclosure_adequate_message)
        }
        .map { it.fixHref(baseUrl) }
        .joinTo(SpannableStringBuilder(), "\n\n") {
            HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_LEGACY)
        }
}

private data class GroupKey(
    val type: ServerMessage.Type,
    val priority: Int
) : Comparable<GroupKey> {

    override fun compareTo(other: GroupKey): Int {
        return when (type) {
            other.type -> priority.compareTo(other.priority)
            ServerMessage.Type.ERROR -> -1
            else -> 1
        }
    }
}
