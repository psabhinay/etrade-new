package com.etrade.mobilepro.screener.api.filter

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

interface ScreenerFilterFactory {
    fun createDefault(type: ScreenerType): ScreenerFilter
}
