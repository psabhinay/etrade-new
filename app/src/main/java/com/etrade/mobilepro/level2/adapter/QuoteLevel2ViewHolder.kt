package com.etrade.mobilepro.level2.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.R
import com.etrade.mobilepro.databinding.QuoteLevel2RowBinding
import com.etrade.mobilepro.level2.QuoteRowData
import com.etrade.mobilepro.util.android.accessibility.processAccessibilityStreamingEvent
import com.etrade.mobilepro.util.android.binding.resources
import com.etrade.mobilepro.util.formatters.formatVolumeCustom

class QuoteLevel2ViewHolder(private val binding: QuoteLevel2RowBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bindTo(quoteRowData: QuoteRowData) {
        val bidItem = quoteRowData.bidItem
        val askItem = quoteRowData.askItem
        with(binding) {
            val emptyDefault = resources.getString(R.string.empty_default)

            marketMakerBid.text = bidItem?.marketMakerId ?: emptyDefault
            bid.text = MarketDataFormatter.formatMarketData(bidItem?.price)
            bidSize.text = getSizeString(bidItem?.size)

            marketMakerAsk.text = askItem?.marketMakerId ?: emptyDefault
            ask.text = MarketDataFormatter.formatMarketData(askItem?.price)
            askSize.text = getSizeString(askItem?.size)

            val bidContentDescription = itemView.resources.getString(
                R.string.quote_content_description_ask_bid_price,
                itemView.resources.getString(R.string.bid),
                bid.text,
                bidSize.text
            )
            val askContentDescription = itemView.resources.getString(
                R.string.quote_content_description_ask_bid_price,
                itemView.resources.getString(R.string.ask),
                ask.text,
                askSize.text
            )
            root.contentDescription =
                "${marketMakerBid.text}, $bidContentDescription, $askContentDescription"
            root.processAccessibilityStreamingEvent(accessibilityText = root.contentDescription.toString())
        }
    }

    private fun getSizeString(size: Long?): String {
        return MarketDataFormatter.formatVolumeCustom(size?.toBigDecimal())
    }
}
