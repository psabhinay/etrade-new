package com.etrade.mobilepro.ui_demo.dropdown

import com.etrade.mobilepro.dropdown.expandableselect.DropDownExpandableSelector
import java.util.UUID
import kotlin.random.Random

sealed class DummyItem {
    abstract val title: String
    open val subItems: List<DummyItem>? = null

    data class SimpleTextItem(
        val data: String = UUID.randomUUID().toString()
    ): DummyItem() {
        override val title: String
            get() = data
    }

    data class ExpandableTextItem(
        val data: Int,
        override val subItems: List<DummyItem> = (0..5).map { SimpleTextItem(data = "SubItem $data - $it") }
    ): DummyItem() {
        override val title: String
            get() = "Expandable $data"
    }

    data class DoubleItem(
        val payload: Double = Random.nextDouble(999999.0)
    ): DummyItem() {
        override val title: String
            get() = payload.toString()
    }

    data class SingleExpandableTextItem(
        val data: Int,
        override val subItems: List<DummyItem> = listOf( SimpleTextItem(data = "SubItem 1") )
    ): DummyItem() {
        override val title: String
            get() = "Expandable $data"
    }
}

internal fun List<DummyItem>?.mapToSelectable(level: Int = 0, selectedValue: DummyItem?): List<DropDownExpandableSelector.Selectable<DummyItem>>? =
    this?.map { item ->
        DropDownExpandableSelector.Selectable(
            title = item.title,
            selected = item == selectedValue,
            value = item,
            level = level,
            subItems = item.subItems.mapToSelectable(level + 1, selectedValue)
        )
    }
