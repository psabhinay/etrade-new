package com.etrade.mobilepro.transfermoney.presentation.preview

import com.etrade.mobilepro.transfermoney.presentation.confirmation.ParcelableTransferConfirmation
import com.etrade.mobilepro.util.delegate.consumable

sealed class ConfirmationViewState {

    object Loading : ConfirmationViewState()

    data class Success(val confirmation: ParcelableTransferConfirmation) : ConfirmationViewState()

    data class Failed(val errorDescription: String? = null) : ConfirmationViewState() {

        val consumableErrorDescription: String? by consumable(errorDescription ?: "")
    }
}
