package com.etrade.mobilepro.trade.form.api.value

interface AmountToSellValueId {
    val entirePosition: String
    val cashPosition: String
    val marginPosition: String
    val dollars: String
    val shares: String
}
