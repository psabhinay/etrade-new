package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.trade.form.api.value.StrategyTypeValueId

interface WithTradeLegsTradeFormInputId : TradeFormInputId {
    val strategy: String
    val strategyValueId: StrategyTypeValueId
    val tradeLegs: String
}
