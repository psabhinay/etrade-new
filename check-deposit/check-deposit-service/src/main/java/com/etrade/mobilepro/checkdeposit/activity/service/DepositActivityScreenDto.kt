package com.etrade.mobilepro.checkdeposit.activity.service

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class DepositActivityScreenDto(
    @Json(name = "CheckDepositActivityResponse")
    val checkDepositActivityResponse: CheckDepositActivityResponse?
)

@JsonClass(generateAdapter = true)
data class CheckDepositActivityResponse(
    @Json(name = "DepositActivity")
    val depositActivity: List<DepositActivityItemDto>?
)

@JsonClass(generateAdapter = true)
data class DepositActivityItemDto(
    @Json(name = "status")
    val status: String?,
    @Json(name = "requestId")
    val requestId: String?,
    @Json(name = "amount")
    val amount: String?,
    @Json(name = "accountId")
    val accountId: String?,
    @Json(name = "depositDate")
    val depositDate: String?,
    @Json(name = "accountIdKey")
    val accountIdKey: String?,
    @Json(name = "frontCheckId")
    val frontCheckId: String?,
    @Json(name = "backCheckId")
    val backCheckId: String?,
    @Json(name = "contributionYear")
    val contributionYear: String?
) : Serializable
