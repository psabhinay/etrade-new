package com.etrade.mobilepro.menuitem

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.getDrawableOrThrow
import androidx.core.content.res.getStringOrThrow
import androidx.core.content.withStyledAttributes
import androidx.core.graphics.drawable.updateBounds
import com.etrade.mobilepro.util.android.extension.inflate

/**
 * Class implements custom view which has icon on the left,text which is 16dp apart from this icon and a text with oval bg anchored on the right parent edge.
 * The icon width and height depends on the size of image.Currently it is used for alert menu item.
 */
class MenuItemViewWithCount : ConstraintLayout {

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private fun init(attributeSet: AttributeSet?) {
        inflate(R.layout.menu_item_with_count, true)

        context.withStyledAttributes(attributeSet, R.styleable.MenuItemViewWithCount) {
            val messageCountTextView: TextView = findViewById(R.id.menu_message_count)
            messageCountTextView.text = getStringOrThrow(R.styleable.MenuItemViewWithCount_menu_count)

            val iconLeft = getDrawableOrThrow(R.styleable.MenuItemViewWithCount_menu_icon_left)
            iconLeft.updateBounds(right = iconLeft.intrinsicWidth, bottom = iconLeft.intrinsicHeight)
            val menuTitleView = findViewById<MenuItemTitleView>(R.id.menuTitleView)
            menuTitleView.titleTextView.compoundDrawablePadding = resources.getDimension(R.dimen.spacing_medium).toInt()
            menuTitleView.titleTextView.setCompoundDrawables(iconLeft, null, null, null)
            menuTitleView.text = getString(R.styleable.MenuItemViewWithCount_menu_title).orEmpty()
        }
    }
}
