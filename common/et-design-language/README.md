# E*TRADE Design Language Components

## Required Watching
[https://www.youtube.com/watch?v=6o3KqyX_tEA]

## Typography.xml
This file includes all the base textappearance styles. Every text styles in the app should be inherited from these styles instead of using font_size.
Here are couple of things we should be aware of:
    1. Attributes on views shouldn't be used. All style attributes should define under a style.
    2. supported attributes by TextAppearance <attr name="textColor" />
                                              <attr name="textSize" />
                                              <attr name="textStyle" />
                                              <attr name="typeface" />
                                              <attr name="fontFamily" />
                                              <attr name="textColorHighlight" />
                                              <attr name="textColorHint" />
                                              <attr name="textColorLink" />
                                              <attr name="textAllCaps" format="boolean" />
                                              <attr name="shadowColor" format="color" />
                                              <attr name="shadowDx" format="float" />
                                              <attr name="shadowDy" format="float" />
                                              <attr name="shadowRadius" format="float" />
                                              <attr name="elegantTextHeight" format="boolean" />
                                              <attr name="letterSpacing" format="float" />
                                              <attr name="fontFeatureSettings" format="string" />

    3. Precedence order in styling: Span > Setters > View > Style > Default Style > Theme > TextAppearance

## TextInputLayout
<com.google.android.material.textfield.TextInputLayout
            android:id="@+id/test"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:hint="@string/high_target_price">

    <com.google.android.material.textfield.TextInputEditText
        android:layout_width="match_parent"
        android:layout_height="wrap_content"/>

</com.google.android.material.textfield.TextInputLayout>

## Text Button With Icon
<com.google.android.material.button.MaterialButton
    android:id="@+id/material_text_button"
    style="@style/Etrade.TextButton"
    android:layout_width="250dp"
    android:layout_height="wrap_content"
    app:icon="@drawable/ic_circle_add"
    app:iconGravity="textStart"
    android:text="@string/add_to_watch_list"/>

## Filled, unelevated Button
<com.google.android.material.button.MaterialButton
    android:id="@+id/material_unelevated_filled_button"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:text="@string/button_text_logon"/>

## Outlined, unelevated Button
<com.google.android.material.button.MaterialButton
    android:id="@+id/material_unelevated_outlined_button"
    style="@style/Widget.Etrade.OutlinedButton"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:text="@string/button_text_logon"/>

## AlertDialog
MaterialAlertDialogBuilder(this)
            .setTitle(R.string.delete_watch_list)
            .setMessage(R.string.delete_watch_list_confirmation_message)
            .setPositiveButton("Yes", null)
            .setNegativeButton("No", null)
            .show()