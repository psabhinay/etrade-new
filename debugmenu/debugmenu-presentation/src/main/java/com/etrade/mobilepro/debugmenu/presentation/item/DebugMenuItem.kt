package com.etrade.mobilepro.debugmenu.presentation.item

import androidx.annotation.StringRes
import com.etrade.mobilepro.appconfig.api.AppEnvironment
import com.etrade.mobilepro.appconfig.api.Timeouts

internal interface DebugMenuItem
@JvmInline
internal value class SectionItem(@StringRes val titleStringRes: Int) : DebugMenuItem

internal data class EnvironmentItem(val value: AppEnvironment, val isChecked: Boolean) : DebugMenuItem
@JvmInline
internal value class TimeoutsItem(val value: Timeouts) : DebugMenuItem
@JvmInline
internal value class CircuitBreakerItem(val enabled: Boolean) : DebugMenuItem

internal class FeatureFlagsItem : DebugMenuItem

internal class ForceCrashItem : DebugMenuItem
