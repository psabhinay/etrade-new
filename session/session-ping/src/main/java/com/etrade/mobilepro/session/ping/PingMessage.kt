package com.etrade.mobilepro.session.ping

internal const val DEFAULT_PING_SESSION_INTERVAL_MILLIS = 60000L

data class PingMessage(
    val isSessionValid: Boolean = true,
    val errorMsg: String? = null,
    val nextCheckInterval: Long = DEFAULT_PING_SESSION_INTERVAL_MILLIS
)
