package com.etrade.mobilepro.portfolio.lightstreamer

import io.reactivex.Observable

interface AccountPortfolioDataProvider {

    fun getAccountPortfolioEvents(accountId: String): Observable<PortfolioUpdateEvent>
}
