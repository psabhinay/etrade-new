package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.barcode.BarcodeReaderActivity
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvActivity
import com.etrade.mobilepro.chartiq.ChartIqActivity
import com.etrade.mobilepro.completeview.edit.EditCompleteViewActivity
import com.etrade.mobilepro.feedback.presentation.FeedbackActivity
import com.etrade.mobilepro.formsandapplications.presentation.AccountServicesWebViewActivity
import com.etrade.mobilepro.formsandapplications.presentation.FormsAndApplicationsActivity
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.home.overview.edit.EditOverviewActivity
import com.etrade.mobilepro.launcher.AppLauncherActivity
import com.etrade.mobilepro.login.LoginActivity
import com.etrade.mobilepro.markets.edit.EditMarketOverviewActivity
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsWebViewActivity
import com.etrade.mobilepro.onboarding.OnboardingActivity
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.pdf.presentation.PdfActivity
import com.etrade.mobilepro.portfolio.edit.EditPortfolioTableActivity
import com.etrade.mobilepro.quotelookup.QuoteLookupActivity
import com.etrade.mobilepro.trade.StopLossDisclosuresActivity
import com.etrade.mobilepro.usersubscriptionpresentation.UserSubscriptionsWebViewActivity
import com.etrade.mobilepro.watchlist.presentation.EditWatchlistActivity
import com.etrade.mobilepro.watchlist.presentation.EditWatchlistColumnsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector
import org.chromium.customtabsclient.shared.WebViewProxyActivity
import org.chromium.customtabsclient.shared.WebviewActivity

@Module
abstract class ActivityModule {

    /* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */

    @ContributesAndroidInjector
    internal abstract fun contributeAppLauncherActivityInjector(): AppLauncherActivity

    @ContributesAndroidInjector
    internal abstract fun contributeBarcodeReaderActivity(): BarcodeReaderActivity

    @ContributesAndroidInjector
    internal abstract fun contributeBloombergTvActivityInjector(): BloombergTvActivity

    @ContributesAndroidInjector
    internal abstract fun contributeChartIqActivityInjector(): ChartIqActivity

    @ContributesAndroidInjector
    internal abstract fun contributeEditCompleteViewActivity(): EditCompleteViewActivity

    @ContributesAndroidInjector
    internal abstract fun contributeEditMarketsOverviewActivity(): EditMarketOverviewActivity

    @ContributesAndroidInjector
    internal abstract fun contributeEditOverviewActivity(): EditOverviewActivity

    @ContributesAndroidInjector
    internal abstract fun contributeEditPortfolioTableActivity(): EditPortfolioTableActivity

    @ContributesAndroidInjector
    internal abstract fun contributeEditWatchlistActivity(): EditWatchlistActivity

    @ContributesAndroidInjector
    internal abstract fun contributeEditWatchlistColumnsActivity(): EditWatchlistColumnsActivity

    @ContributesAndroidInjector
    internal abstract fun contributeFeedbackActivityInjector(): FeedbackActivity

    @ContributesAndroidInjector
    internal abstract fun contributeFormsAndApplicationsActivity(): FormsAndApplicationsActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLoginActivityInjector(): LoginActivity

    @ContributesAndroidInjector
    internal abstract fun contributeMainActivityInjector(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun contributeMsAccountsWebViewActivity(): MsAccountsWebViewActivity

    @ContributesAndroidInjector
    internal abstract fun contributeOnboardingActivityInjector(): OnboardingActivity

    @ContributesAndroidInjector
    internal abstract fun contributeQuoteLookupActivityInjector(): QuoteLookupActivity

    @ContributesAndroidInjector
    internal abstract fun contributeWebviewActivity(): WebviewActivity

    @ContributesAndroidInjector
    internal abstract fun contributeUserSubscriptionsWebViewActivity(): UserSubscriptionsWebViewActivity

    @ContributesAndroidInjector
    internal abstract fun contributeAccountServicesWebViewActivity(): AccountServicesWebViewActivity

    @ContributesAndroidInjector
    internal abstract fun contributeWebViewProxyActivity(): WebViewProxyActivity

    @ContributesAndroidInjector
    internal abstract fun contributeSpannableTextOverlayActivity(): SpannableTextOverlayActivity

    @ContributesAndroidInjector
    internal abstract fun contributeStopLossDisclosureActivity(): StopLossDisclosuresActivity

    @ContributesAndroidInjector
    internal abstract fun contributePdfActivity(): PdfActivity
}
