package com.etrade.mobilepro.portfolio.model

import com.etrade.mobilepro.positions.api.Position
import java.math.BigDecimal

data class SimplePosition(
    override val positionId: String,
    override val symbol: String,
    override val daysGain: BigDecimal? = null,
    override val totalGain: BigDecimal? = null,
    override val pricePaid: BigDecimal? = null,
    override val commission: BigDecimal? = null,
    override val fees: BigDecimal? = null,
    override val quantity: BigDecimal? = null,
    override val displayQuantity: BigDecimal? = null,
    override val daysGainPercentage: BigDecimal? = null,
    override val totalGainPercentage: BigDecimal? = null,
    override val todayClose: BigDecimal? = null,
    override val baseSymbolAndPrice: String? = null,
    override val mark: BigDecimal? = null,
    override val marketValue: BigDecimal? = null,
    override val inTheMoneyFlag: Boolean? = null
) : Position {

    constructor(position: Position, baseSymbolAndPrice: String? = null) : this(
        positionId = position.positionId,
        symbol = position.symbol,
        daysGain = position.daysGain,
        totalGain = position.totalGain,
        pricePaid = position.pricePaid,
        commission = position.commission,
        fees = position.fees,
        quantity = position.quantity,
        displayQuantity = position.displayQuantity,
        daysGainPercentage = position.daysGainPercentage,
        totalGainPercentage = position.totalGainPercentage,
        todayClose = position.todayClose,
        baseSymbolAndPrice = baseSymbolAndPrice ?: position.baseSymbolAndPrice,
        mark = position.mark,
        marketValue = position.marketValue,
        inTheMoneyFlag = position.inTheMoneyFlag
    )
}
