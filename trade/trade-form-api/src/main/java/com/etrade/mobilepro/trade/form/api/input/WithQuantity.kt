package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot

interface WithQuantity {
    val quantity: String
}

fun WithQuantity.extractQuantity(snapshot: InputValuesSnapshot): String? = snapshot[quantity] as? String
