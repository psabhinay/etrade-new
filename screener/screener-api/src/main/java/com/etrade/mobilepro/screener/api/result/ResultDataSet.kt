package com.etrade.mobilepro.screener.api.result

import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell

data class ResultDataSet(
    val columnHeaders: List<Cell>,
    val rowHeaders: List<SymbolCell>,
    val cellItems: List<List<ResultDataCell>>
) {
    val isEmpty: Boolean
        get() = rowHeaders.isEmpty() && cellItems.isEmpty()
}
