package com.etrade.mobilepro.login.data

import android.security.keystore.KeyPermanentlyInvalidatedException
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.encryption.api.CipherAuthenticator
import com.etrade.mobilepro.encryption.api.EncryptionProvider
import com.etrade.mobilepro.encryption.api.SecretKeyMaterial
import com.etrade.mobilepro.encryption.api.SecretKeyRepo
import com.etrade.mobilepro.login.api.repo.LoginPreferencesError
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.inject.Inject
import kotlin.coroutines.cancellation.CancellationException

internal const val KEY_ENCRYPTED_USER_NAME = "encryptedUsername"
internal const val KEY_ENCRYPTED_PASSWORD = "encryptedPassword"
internal const val KEY_PASSWORD_HASH = "passwordHash"

private const val KEY_IS_FIRST_LOGIN = "isFirstLogin"
private const val KEY_IS_LOGIN_MDA_POPUP_SHOWN = "loginMDAPopupShown"
private const val KEY_IS_PASSWORD_HIDDEN_STATE = "isPasswordHiddenState"
private const val KEY_IS_HIDE_PASSWORD_ENABLED = "isHidePasswordEnabled"
private const val KEY_IS_REMEMBER_USERNAME_ENABLED = "rememberUser"
private const val KEY_IS_SECURITY_CODE_ENABLED = "isSecurityCodeEnabled"
private const val KEY_IS_RSA_USER = "U_PWD_RSA"
private const val KEY_VERSION = "version"
private const val KEY_IS_LOGIN_FOR_SAME_USER = "loginForSameUser"
private const val KEY_IS_PUSH_NOTIFICATION_SHOWN = "pushNotificationShown"

private const val NO_ENROLLED_BIOMETRIC_CAUSE = "At least one biometric must be enrolled to create keys requiring user authentication for every use"
private const val KEY_USER_NOT_AUTHENTICATED_CAUSE = "Key user not authenticated"

class LoginPreferencesRepoImp @Inject constructor(
    private val keyValueStorage: KeyValueStorage,
    private val encryptionProvider: EncryptionProvider,
    private val secretKeyRepo: SecretKeyRepo
) : LoginPreferencesRepo {

    override val isBiometricAuthenticationEnabled: Boolean
        get() = keyValueStorage.getStringValue(KEY_PASSWORD_HASH, null) != null

    override val isFirstLogin: Boolean
        get() = keyValueStorage.getBooleanValue(KEY_IS_FIRST_LOGIN, true)

    override var isPasswordHidden: Boolean
        get() = keyValueStorage.getBooleanValue(KEY_IS_PASSWORD_HIDDEN_STATE, true)
        set(value) = keyValueStorage.putBooleanValue(KEY_IS_PASSWORD_HIDDEN_STATE, value)

    override var isHidePasswordEnabled: Boolean
        get() = keyValueStorage.getBooleanValue(KEY_IS_HIDE_PASSWORD_ENABLED, false)
        set(value) = keyValueStorage.putBooleanValue(KEY_IS_HIDE_PASSWORD_ENABLED, value)

    override var isRememberUserNameEnabled: Boolean
        get() { return keyValueStorage.getBooleanValue(KEY_IS_REMEMBER_USERNAME_ENABLED) }
        set(value) { keyValueStorage.putBooleanValue(KEY_IS_REMEMBER_USERNAME_ENABLED, value) }

    override var isSecurityCodeEnabled: Boolean
        get() { return keyValueStorage.getBooleanValue(KEY_IS_SECURITY_CODE_ENABLED, false) }
        set(value) { keyValueStorage.putBooleanValue(KEY_IS_SECURITY_CODE_ENABLED, value) }

    override var isRsaUser: Boolean
        get() { return keyValueStorage.getBooleanValue(KEY_IS_RSA_USER) }
        set(value) { keyValueStorage.putBooleanValue(KEY_IS_RSA_USER, value) }

    override var isLoginMdaPopupShown: Boolean
        get() { return keyValueStorage.getBooleanValue(KEY_IS_LOGIN_MDA_POPUP_SHOWN) }
        set(value) { keyValueStorage.putBooleanValue(KEY_IS_LOGIN_MDA_POPUP_SHOWN, value) }

    override var version: Int
        get() = keyValueStorage.getIntValue(KEY_VERSION)
        set(value) = keyValueStorage.putIntValue(KEY_VERSION, value)

    private val defaultAuthenticator: suspend (Cipher) -> Cipher = { it }

    override fun disableBiometricAuthentication() {
        keyValueStorage.apply {
            putStringValue(KEY_ENCRYPTED_PASSWORD, null)
            putStringValue(KEY_PASSWORD_HASH, null)
        }
    }

    override var isLoginForSameUser: Boolean
        get() {
            return keyValueStorage.getBooleanValue(KEY_IS_LOGIN_FOR_SAME_USER)
        }
        set(value) {
            keyValueStorage.putBooleanValue(KEY_IS_LOGIN_FOR_SAME_USER, value)
        }

    override var isPushNotificationPopupShown: Boolean
        get() {
            return keyValueStorage.getBooleanValue(KEY_IS_PUSH_NOTIFICATION_SHOWN, false)
        }
        set(value) {
            keyValueStorage.putBooleanValue(KEY_IS_PUSH_NOTIFICATION_SHOWN, value)
        }

    override fun registerLoginEvent() = keyValueStorage.putBooleanValue(KEY_IS_FIRST_LOGIN, false)

    override suspend fun setUserName(userName: String): ETResult<Unit> = storeEncrypted(KEY_ENCRYPTED_USER_NAME, secretKeyRepo.commonKey, userName)

    override suspend fun getUserName(): ETResult<String> = getDecrypted(KEY_ENCRYPTED_USER_NAME, secretKeyRepo.commonKey)

    override suspend fun setUserPassword(password: String, authenticator: CipherAuthenticator): ETResult<Unit> = savePassword(password, null, authenticator)

    override suspend fun getUserPassword(authenticator: CipherAuthenticator): ETResult<String> {
        return runCatchingET { getDecrypted(KEY_ENCRYPTED_PASSWORD, secretKeyRepo.secureKey, authenticator).getOrThrow() }.onFailure {
            checkException(it)
        }
    }

    override suspend fun updateUserPasswordIfChanged(password: String, authenticator: CipherAuthenticator): ETResult<Unit> {
        return runCatchingET {
            val savedPasswordHash = getPasswordHash()
            val passwordHash = password.hash()
            if (savedPasswordHash != passwordHash) {
                savePassword(password, passwordHash, authenticator).getOrThrow()
            }
        }
    }

    private suspend fun storeEncrypted(
        key: String,
        secretKeyMaterial: SecretKeyMaterial,
        value: String,
        authenticator: CipherAuthenticator = defaultAuthenticator
    ): ETResult<Unit> {
        return runCatchingET {
            if (!BuildConfig.DEBUG && !encryptionProvider.isSecureEncryptionSupported()) {
                throw UnsupportedOperationException("Secure encryption not supported")
            }

            val encryptedPassword = encryptionProvider.encrypt(value, secretKeyMaterial, authenticator)
            keyValueStorage.putStringValue(key, encryptedPassword)
        }.onFailure { checkException(it) }
    }

    private suspend fun getDecrypted(
        key: String,
        secretKeyMaterial: SecretKeyMaterial,
        authenticator: CipherAuthenticator = defaultAuthenticator
    ): ETResult<String> {
        return runCatchingET {
            val value = keyValueStorage.getValueOrThrow(key)
            encryptionProvider.decrypt(value, secretKeyMaterial, authenticator)
        }
    }

    private suspend fun savePassword(password: String, passwordHash: String?, authenticator: CipherAuthenticator): ETResult<Unit> {
        return runCatchingET {
            storeEncrypted(KEY_ENCRYPTED_PASSWORD, secretKeyRepo.secureKey, password, authenticator)
                .onSuccess { savePasswordHash(password, passwordHash) }.getOrThrow()
        }
    }

    override fun checkException(exception: Throwable): LoginPreferencesError {
        return when (exception) {
            is CancellationException -> LoginPreferencesError.LOGIN_CANCELED
            is InvalidKeyException,
            is KeyPermanentlyInvalidatedException -> {
                resetBiometric()
                LoginPreferencesError.SECRET_KEY_INVALIDATED
            }
            is IllegalBlockSizeException -> {
                if (exception.messageContains(KEY_USER_NOT_AUTHENTICATED_CAUSE)) {
                    resetBiometric()
                }
                LoginPreferencesError.GENERIC_ERROR
            }
            is InvalidAlgorithmParameterException -> {
                if (exception.messageContains(NO_ENROLLED_BIOMETRIC_CAUSE)) {
                    resetBiometric()
                    LoginPreferencesError.NO_VALID_BIOMETRIC
                } else {
                    LoginPreferencesError.GENERIC_ERROR
                }
            }
            else -> LoginPreferencesError.GENERIC_ERROR
        }
    }

    private fun resetBiometric() {
        secretKeyRepo.recreateSecretKey()
        disableBiometricAuthentication()
    }

    private suspend fun savePasswordHash(password: String, passwordHash: String?) {
        runCatching {
            keyValueStorage.putStringValue(KEY_PASSWORD_HASH, passwordHash ?: password.hash())
        }
    }

    private fun getPasswordHash(): String = keyValueStorage.getValueOrThrow(KEY_PASSWORD_HASH)

    private suspend fun String.hash(): String = encryptionProvider.hash(this).trimEnd()

    private fun KeyValueStorage.getValueOrThrow(key: String): String {
        return getStringValue(key, null) ?: throw NoSuchElementException("Value not found for key $key")
    }

    private fun Throwable.messageContains(text: String) = cause?.message?.contains(text) == true
}
