-if class com.etrade.mobilepro.chart.data.ChartRequestDto
-keepnames class com.etrade.mobilepro.chart.data.ChartRequestDto
-if class com.etrade.mobilepro.chart.data.ChartRequestDto
-keep class com.etrade.mobilepro.chart.data.ChartRequestDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
