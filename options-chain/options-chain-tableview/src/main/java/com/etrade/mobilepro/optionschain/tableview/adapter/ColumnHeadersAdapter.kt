package com.etrade.mobilepro.optionschain.tableview.adapter

import android.content.Context
import android.view.ViewGroup
import com.etrade.mobilepro.optionschain.tableview.OptionsChainColumn
import com.etrade.mobilepro.optionschain.tableview.databinding.OptionsChainColumnHeaderBinding
import com.etrade.mobilepro.optionschain.tableview.viewholder.ColumnHeaderViewHolder
import com.etrade.mobilepro.util.android.extension.viewBinding

class ColumnHeadersAdapter(
    context: Context
) : AbstractOptionChainAdapter<OptionsChainColumn, ColumnHeaderViewHolder>(context) {

    var columnHeaders: List<OptionsChainColumn> = emptyList()

    override fun ColumnHeaderViewHolder.bind(position: Int) {
        bindWithData(columnHeaders[position])
    }

    override fun ColumnHeaderViewHolder.bindWithData(data: OptionsChainColumn) {
        bindTo(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColumnHeaderViewHolder =
        ColumnHeaderViewHolder(parent.viewBinding(OptionsChainColumnHeaderBinding::inflate))

    override fun getItemCount(): Int = columnHeaders.size

    override fun onBindViewHolder(holder: ColumnHeaderViewHolder, position: Int) {
        holder.bindTo(columnHeaders[position])
    }
}
