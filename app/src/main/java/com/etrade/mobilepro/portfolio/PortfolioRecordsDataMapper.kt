package com.etrade.mobilepro.portfolio

import com.etrade.mobilepro.R
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordDataCell
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableData
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableRow
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.data.baseValueExtractors
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.util.color.DataColorResolver

@Suppress("ComplexCondition")
internal fun updateTableRows(
    columns: List<PortfolioColumn>,
    records: List<PortfolioRecord>,
    previousData: PortfolioRecordsTableData?,
    dataColorResolver: DataColorResolver
): PortfolioRecordsTableData {
    return PortfolioRecordsTableData(
        columns = columns,
        rows = records.mapIndexed { index, record ->
            val previousRow = previousData?.rows?.getOrNull(index)
            if (previousData != null &&
                previousData.columns == columns &&
                previousRow != null &&
                previousRow.recordId == record.recordId &&
                previousRow.rowHeader.displayText == record.displayName
            ) {
                previousRow.copy(
                    cells = columns.updateRowCells(record, previousRow, dataColorResolver)
                )
            } else {
                record.toPortfolioRecordsTableRow(columns, dataColorResolver)
            }
        }
    )
}

private fun List<PortfolioColumn>.updateRowCells(
    record: PortfolioRecord,
    previousRow: PortfolioRecordsTableRow,
    dataColorResolver: DataColorResolver
): List<PortfolioRecordDataCell> {
    return mapIndexed { cellIndex, column ->
        val baseValueExtractor = baseValueExtractors.getValue(column.column)
        val baseValue = baseValueExtractor(record)
        val previousCell = previousRow.cells[cellIndex]
        if (baseValue == null || baseValue == previousCell.baseValue) {
            previousCell
        } else {
            column.createCell(record, dataColorResolver)
        }
    }
}

private fun PortfolioRecord.toPortfolioRecordsTableRow(
    columns: List<PortfolioColumn>,
    dataColorResolver: DataColorResolver
): PortfolioRecordsTableRow {
    val (backgroundColor, backgroundResource) = resolveBackgroundColorAndResource()
    return PortfolioRecordsTableRow(
        rowHeader = toRowHeaderCell(),
        cells = toDataCells(columns, dataColorResolver),
        isTaxLot = isTaxLot,
        recordId = recordId,
        backgroundColor = backgroundColor,
        backgroundResource = backgroundResource
    )
}

private fun PortfolioRecord.resolveBackgroundColorAndResource() = if (position.inTheMoneyFlag == true) {
    Pair(R.color.symbol_cell_highlighted_background_color, R.drawable.bg_tableview_row_header_purple)
} else {
    Pair(R.color.symbol_cell_default_background_color, R.drawable.bg_tableview_row_header)
}

private fun PortfolioRecord.toRowHeaderCell(): SymbolCell {
    val description = if (isTaxLot) {
        null
    } else {
        instrument.symbolDescription
    }
    return SymbolCell(
        recordId,
        displayName,
        instrument.symbol,
        instrument.instrumentType,
        description
    )
}

private fun PortfolioRecord.toDataCells(columns: List<PortfolioColumn>, dataColorResolver: DataColorResolver): List<PortfolioRecordDataCell> =
    columns.map { it.createCell(this, dataColorResolver) }
