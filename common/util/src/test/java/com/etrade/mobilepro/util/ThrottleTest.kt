package com.etrade.mobilepro.util

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class ThrottleTest {
    @ExperimentalCoroutinesApi
    @Test
    fun `test throttle last 10ms`() {
        var counter = 0
        runBlockingTest {
            val throttleMethod = throttleLast<Unit>(10, this) { counter++ }

            for (i in 0 until 500) {
                throttleMethod(Unit)
                delay(10)
            }
        }

        assertTrue(counter == 500)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test throttle last 0ms`() {
        var counter = 0
        val throttleMethod = throttleLast<Unit>(0, TestCoroutineScope()) { counter++ }

        for (i in 0 until 500) {
            throttleMethod(Unit)
        }

        assertTrue(counter == 500)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `debounce 500 events results and receive only one event`() {
        var counter = 0

        runBlockingTest {
            val debounceMethod = debounce<Unit>(20, this) { counter++ }

            for (i in 0 until 500) {
                debounceMethod(Unit)
                delay(10)
            }

            advanceTimeBy(20)
            assertTrue(counter == 1)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `debounce 500 events and receive half of them`() {
        var counter = 0

        runBlockingTest {
            val debounceMethod = debounce<Unit>(20, this) { counter++ }

            for (i in 0 until 500) {
                debounceMethod(Unit)
                if (i % 2 == 0) {
                    delay(10)
                } else {
                    delay(20)
                }
            }
            assertTrue(counter == 250)
        }
    }
}
