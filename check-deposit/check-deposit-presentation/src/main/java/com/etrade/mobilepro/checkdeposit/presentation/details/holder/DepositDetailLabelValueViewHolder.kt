package com.etrade.mobilepro.checkdeposit.presentation.details.holder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.checkdeposit.presentation.databinding.AdapterDepositDetailLabelValueBinding
import com.etrade.mobilepro.checkdeposit.presentation.details.DepositDetail

class DepositDetailLabelValueViewHolder(
    private val binding: AdapterDepositDetailLabelValueBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: DepositDetail.LabelValue) {
        with(binding) {
            label.text = item.label
            value.text = item.value
            if (item.contentDescription != null) {
                value.contentDescription = item.contentDescription
            }
        }
    }
}
