package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asFlow
import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.SearchPayeeResult
import com.etrade.mobilepro.billpay.api.SearchPayeeResultItem
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.ActionState
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.livedata.postUpdateValue
import com.etrade.mobilepro.livedata.updateValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow

private const val SEARCH_DELAY = 300L
private const val EMPTY_SEARCH_TERM = ""

@FlowPreview
@ExperimentalCoroutinesApi
internal class SearchPayeeDelegate constructor(
    private val repo: BillPayRepo,
    private val billPaySessionId: String
) {

    private val _searchTerm = MutableLiveData<String>(EMPTY_SEARCH_TERM)
    internal val searchActionState: MutableLiveData<ActionState<List<SearchPayeeResultItem>>> =
        MutableLiveData(ActionState(isLoading = false, isActive = false))

    internal suspend fun bindSearch() {
        _searchTerm
            .asFlow()
            .debounce(SEARCH_DELAY)
            .distinctUntilChanged()
            .flatMapLatest { searchTerm ->
                flow {
                    setSearchActionLoading()
                    emit(repo.searchPayee(billPaySessionId, searchTerm))
                }
            }
            .collect {
                it.fold(
                    onSuccess = ::onSearchPayeeSuccess,
                    onFailure = ::onSearchPayeeFail
                )
            }
    }

    internal fun searchPayee(searchTerm: String?) {
        setSearchAsActive()
        _searchTerm.value = searchTerm ?: EMPTY_SEARCH_TERM
    }

    internal fun deactivateSearch() {
        setSearchActionActive(false)
    }

    private fun onSearchPayeeSuccess(result: SearchPayeeResult) {
        when (result) {
            is SearchPayeeResult.Success -> {
                setSearchActionResult(result.payeeSearchResults)
            }
            else -> {
                showNoSearchResults()
            }
        }
    }

    private fun onSearchPayeeFail(cause: Throwable) {
        setSearchActionFail(cause)
    }

    private fun showNoSearchResults() {
        setSearchActionResult(emptyList())
    }

    private fun setSearchActionLoading() =
        searchActionState.updateValue { it.copy(isLoading = true) }

    private fun setSearchAsActive() = setSearchActionActive(true)

    private fun setSearchActionActive(isActive: Boolean) {
        searchActionState.updateValue { it.copy(isActive = isActive) }
    }

    private fun setSearchActionResult(searchResult: List<SearchPayeeResultItem>) =
        searchActionState.postUpdateValue { it.copy(isLoading = false, result = ETResult.success(searchResult)) }

    private fun setSearchActionFail(cause: Throwable) {
        searchActionState.postUpdateValue { it.copy(isLoading = false, result = ETResult.failure(cause)) }
    }
}
