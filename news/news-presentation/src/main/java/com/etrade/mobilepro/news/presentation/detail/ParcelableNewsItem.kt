package com.etrade.mobilepro.news.presentation.detail

import android.os.Parcelable
import com.etrade.mobilepro.news.api.model.NewsItem
import kotlinx.parcelize.Parcelize

@Parcelize
class ParcelableNewsItem(
    val title: String,
    val content: String,
    val dateSource: String,
    val docId: String
) : Parcelable {
    fun toNewsItem(): NewsItem.Text = NewsItem.Text(
        newsDateTimeSource = dateSource,
        title = title,
        content = content,
        symbols = emptyList(),
        url = "",
        docId = docId
    )
}

fun NewsItem.Text.toParcelable(): ParcelableNewsItem = ParcelableNewsItem(
    title = title,
    content = content,
    dateSource = newsDateTimeSource,
    docId = docId
)
