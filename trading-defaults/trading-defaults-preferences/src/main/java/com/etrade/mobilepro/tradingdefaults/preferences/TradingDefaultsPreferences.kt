@file:Suppress("TopLevelPropertyNaming")
package com.etrade.mobilepro.tradingdefaults.preferences

import android.content.res.Resources
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.preferences.preference
import java.math.BigDecimal
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

val DEFAULT_STOCKS_PRICE_TYPE = PriceType.MARKET
val DEFAULT_STOCKS_TERM = OrderTerm.GOOD_FOR_DAY
val DEFAULT_OPTIONS_TERM = OrderTerm.GOOD_FOR_DAY

private val DEFAULT_OPTIONS_PRICE_TYPE = PriceType.LIMIT

const val DEFAULT_STOCKS_QUANTITY = ""
const val DEFAULT_OPTIONS_CONTRACTS = 1
const val DEFAULT_STOCK_AND_OPTIONS_STOCK_QUANTITY = 100
const val DEFAULT_WALKTHROUGH_COMPLETED_FLAG = "FormCompleted"

class TradingDefaultsPreferences(
    private val resources: Resources,
    private val keyValueStorage: KeyValueStorage
) {

    var stocksAccountId: String by keyValueStorage.preference(key = "stocksAccountId")

    var stocksQuantity: String by keyValueStorage.preference(key = "sharesKey", default = DEFAULT_STOCKS_QUANTITY)

    var stocksPriceType: PriceType? by delegatePriceType(default = DEFAULT_STOCKS_PRICE_TYPE, key = "stockPriceKey")

    var stocksTerm: OrderTerm? by delegateOrderTerm(default = DEFAULT_STOCKS_TERM, key = "stockTermKey")

    var optionsAccountId: String by keyValueStorage.preference(key = "optionsAccountId")

    var optionsContracts: Int by keyValueStorage.preference(key = "optionContractsKey", default = DEFAULT_OPTIONS_CONTRACTS)

    var optionsPriceType: PriceType? by delegatePriceType(default = DEFAULT_OPTIONS_PRICE_TYPE, key = "optionPriceKey")

    var optionsTerm: OrderTerm? by delegateOrderTerm(default = DEFAULT_OPTIONS_TERM, key = "optionTermKey")

    fun getDefaultQuantityWithInstrumentType(instrumentType: InstrumentType): BigDecimal {
        return if (instrumentType.isOption) {
            optionsContracts.toString()
        } else {
            stocksQuantity
        }.takeIf { it.isNotBlank() }
            ?.toBigDecimalOrNull()
            ?: BigDecimal.ONE
    }

    private fun delegatePriceType(default: PriceType? = null, key: String): ReadWriteProperty<Any, PriceType?> = object : ReadWriteProperty<Any, PriceType?> {
        override fun getValue(thisRef: Any, property: KProperty<*>): PriceType? {
            val value = keyValueStorage.getStringValue(key)

            return value
                ?.let { PriceType.mapPriceType(it) }
                ?.let {
                    if (it == PriceType.UNKNOWN) {
                        fallback(value, PriceType.values(), PriceType::getLabel)
                            .takeUnless { item -> item == PriceType.UNKNOWN }
                            ?: default
                    } else {
                        it
                    }
                }
        }

        override fun setValue(thisRef: Any, property: KProperty<*>, value: PriceType?) {
            keyValueStorage.putStringValue(key, value?.typeCode)
        }
    }

    private fun delegateOrderTerm(default: OrderTerm? = null, key: String): ReadWriteProperty<Any, OrderTerm?> = object : ReadWriteProperty<Any, OrderTerm?> {
        override fun getValue(thisRef: Any, property: KProperty<*>): OrderTerm? {
            val value = keyValueStorage.getStringValue(key)

            return value
                ?.let { OrderTerm.mapOrderTerm(it) }
                ?.let {
                    if (it == OrderTerm.UNKNOWN) {
                        fallback(value, OrderTerm.values(), OrderTerm::getLabel)
                            .takeUnless { item -> item == OrderTerm.UNKNOWN }
                            ?: default
                    } else {
                        it
                    }
                }
        }

        override fun setValue(thisRef: Any, property: KProperty<*>, value: OrderTerm?) {
            keyValueStorage.putStringValue(key, value?.typeCode)
        }
    }

    private fun <T> fallback(value: String, items: Array<T>, label: T.() -> Int): T? = items.firstOrNull { value == resources.getString(label(it)) }
}
