package com.etrade.mobilepro.dynamic.form.api

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.dynamic.form.api.util.toDropDownOption
import com.jraska.livedata.test
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert.assertNull
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class DropDownTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val resources: Resources = mockResources()

    @Test
    fun `check defaults`() {
        val options: List<List<Int>> = listOf(
            emptyList(),
            listOf(0),
            listOf(1, 2, 3),
            listOf(Int.MIN_VALUE, Int.MAX_VALUE, 0, 100)
        )

        options.forEach { optionCase ->
            checkDefaults(optionCase, null)
            checkDefaults(optionCase, 10)
            optionCase.forEach { value -> checkDefaults(optionCase, value) }
        }
    }

    private fun checkDefaults(options: List<Int>, selectedOption: Int?) {
        val delegate = createDelegate(options, selectedOption)
        checkDelegateState(delegate, options, options.indexOf(selectedOption))
    }

    private fun checkDelegateState(delegate: DropDown, expected: List<Int>, expectedDefaultIndex: Int?) {
        val optionListObserver = delegate.optionList
            .test()
            .assertHasValue()
            .assertValue { value -> value.selectedOptionIndex == expectedDefaultIndex }
            .assertValue { value -> value.dropDownOptions.size == expected.size }

        expected.forEachIndexed { index, option ->
            optionListObserver.assertValue { value ->
                checkDropDownOption(option, value.dropDownOptions[index])
            }
        }
    }

    @Test
    fun `check selection of order type`() {
        checkOptionSelection((1..3).toList())
    }

    @Test
    fun `check selection of non existed order type`() {
        val expectedSelectedIndex = 1
        val options = (1..3).toList()
        val expectedSelectedOption = options[expectedSelectedIndex]
        val delegate = createDelegate(options, expectedSelectedOption)
        delegate.selectOption(0.toDropDownOption(resources))
        checkOrderType(delegate, expectedSelectedIndex, expectedSelectedOption)
    }

    @Test
    fun `initial value not preserved after reset`() {
        val options = (1..5).map { DropDownOption(it.toString(), it.toString()) }
        val tested = DropDown(options, initialOptionId = "2", defaultSelectedOptionId = null, doNotReset = false)
        tested.selectedOption.test().assertValue { it?.id == "2" }
        tested.resetValue()
        assertNull(tested.selectedOption.value)
    }

    private fun checkDropDownOption(expectedResource: Int, actual: DropDownOption): Boolean {
        val expected = expectedResource.toString()
        return expected == actual.id && expected == actual.name
    }

    private fun checkOptionSelection(options: List<Int>) {
        val delegate = createDelegate(options, null)
        options.forEachIndexed { index, option ->
            delegate.selectOption(option.toDropDownOption(resources))
            checkOrderType(delegate, index, option)
        }
    }

    private fun checkOrderType(delegate: DropDown, index: Int, option: Int) {
        delegate.optionList.test().assertValue { it.selectedOptionIndex == index }
        delegate.selectedOption.test().assertValue { checkDropDownOption(option, it!!) }
    }

    private fun createDelegate(options: List<Int>, defaultOption: Int?): DropDown = DropDown(options, null, defaultOption, resources)
}
