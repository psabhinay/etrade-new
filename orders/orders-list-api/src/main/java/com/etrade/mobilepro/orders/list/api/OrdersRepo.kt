package com.etrade.mobilepro.orders.list.api

interface OrdersRepo {
    suspend fun getOrders(orderParamsLoadSizeAndKey: OrderParamsLoadSizeAndKey, cacheExpiration: Long?): OrderItemsAndKey
}
