package com.etrade.mobilepro.marketscalendar.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.marketscalendar.adapter.holder.EconomicEventDetailsViewHolder
import com.etrade.mobilepro.marketscalendar.data.dto.ECONOMIC_EVENT
import com.etrade.mobilepro.marketscalendar.databinding.AdapterEconomicDetailsBinding
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class EconomicEventDetailsAdapterDelegate(
    private val noValue: String
) : AbsListItemAdapterDelegate<EventDetails, EventDetails, EconomicEventDetailsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): EconomicEventDetailsViewHolder {
        return EconomicEventDetailsViewHolder(parent.viewBinding(AdapterEconomicDetailsBinding::inflate), noValue)
    }

    override fun isForViewType(item: EventDetails, items: MutableList<EventDetails>, position: Int): Boolean = items[position].eventType == ECONOMIC_EVENT

    override fun onBindViewHolder(item: EventDetails, holder: EconomicEventDetailsViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }
}
