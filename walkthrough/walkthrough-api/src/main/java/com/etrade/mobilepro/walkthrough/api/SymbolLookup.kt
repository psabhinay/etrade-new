package com.etrade.mobilepro.walkthrough.api

sealed class SymbolLookupState : State {
    object Idle : SymbolLookupState(), IdleState
    object DisplayLookupScreen : SymbolLookupState()
    object DisplayDialogOverQuote : SymbolLookupState()
    object DisplayQuoteOnly : SymbolLookupState()
    object DisplaySearchTooltip : SymbolLookupState()
}

sealed class SymbolLookupAction : Action {
    object StartWalkthrough : SymbolLookupAction()
    object SymbolSelected : SymbolLookupAction()
    object DismissQuoteDialog : SymbolLookupAction()
    object QuoteClosed : SymbolLookupAction()
    object LookupActionHighlighted : SymbolLookupAction()
    object Canceled : SymbolLookupAction(), CancelAction
}
