package com.etrade.mobilepro.transactions.view

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ScrollOnTopListener
import com.etrade.mobilepro.databinding.FragmentTransactionsBinding
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.home.AccountsTabDataDelegate
import com.etrade.mobilepro.home.AccountsTabHostFragmentArgs
import com.etrade.mobilepro.home.OnAccountsTabSelectionListener
import com.etrade.mobilepro.transactions.view.adapter.TransactionsListAdapter
import com.etrade.mobilepro.transactions.viewmodel.TransactionsViewModel
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.formatters.americanSimpleDateFormatter
import com.google.android.material.snackbar.Snackbar
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import javax.inject.Inject

private const val SWEEP_ON_INDEX = 0
private const val SWEEP_OFF_INDEX = 1
private const val SWEEP_DROPDOWN_TAG = "SWEEP ACTIVITY"

private const val KEY_END_DATE = "endDate"
private const val KEY_START_DATE = "startDate"

@RequireLogin
class TransactionsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.fragment_transactions), ScrollOnTopListener, OnAccountsTabSelectionListener {

    private val binding by viewBinding(FragmentTransactionsBinding::bind)

    private val viewModel: TransactionsViewModel by viewModels { viewModelFactory }

    private val adapter: TransactionsListAdapter
        get() = binding.transactionsListView.adapter as TransactionsListAdapter

    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private var snackBar: Snackbar? = null

    private val sweepBottomSheetSelector: BottomSheetSelector<String> by lazy {
        BottomSheetSelector<String>(
            childFragmentManager,
            resources
        )
    }

    private val dataDelegate = AccountsTabDataDelegate {
        viewModel.fetch()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.apply {
            viewModel.startDate = LocalDate.ofEpochDay(getLong(KEY_START_DATE))
            viewModel.endDate = LocalDate.ofEpochDay(getLong(KEY_END_DATE))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupStartDate()
        setupEndDate()

        with(binding.transactionsListView) {
            setupRecyclerViewDivider(R.drawable.thin_divider)
            layoutManager = LinearLayoutManager(context)
            adapter = TransactionsListAdapter(getString(R.string.empty_default))
        }

        setupViewModel()
        initDropdown()

        binding.dateRangeSelector.dateRangeStartDate.text = formatDateString(viewModel.startDate)
        binding.dateRangeSelector.dateRangeEndDate.text = formatDateString(viewModel.endDate)
        updateSweepActivityValue(viewModel.isSweepActivityEnabled)

        binding.swipeRefresh.setOnRefreshListener {
            refreshTransactions()
        }
        binding.sweepActivityValue.setOnClickListener {
            if (viewModel.viewState.value != TransactionsViewModel.ViewState.Loading) {
                sweepBottomSheetSelector.openDropDown()
            }
        }

        dataDelegate.initialize(savedInstanceState != null)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(KEY_START_DATE, viewModel.startDate.toEpochDay())
        outState.putLong(KEY_END_DATE, viewModel.endDate.toEpochDay())
    }

    private fun refreshTransactions() {
        viewModel.refresh()
        binding.transactionsListView.scrollToPosition(0)
    }

    private fun formatDateString(date: LocalDate) = americanSimpleDateFormatter.format(date)

    @Suppress("LongMethod")
    private fun setupViewModel() {

        arguments?.let {
            viewModel.accountId = AccountsTabHostFragmentArgs.fromBundle(it).accountId?.toLong()
        }

        viewModel.run {
            viewState.observe(
                viewLifecycleOwner,
                Observer {
                    when (it) {
                        is TransactionsViewModel.ViewState.Loading -> {
                            if (!binding.swipeRefresh.isRefreshing) {
                                binding.loadingIndicator.show()
                            }
                            snackBar?.dismiss()
                            binding.transactionsListView.visibility = View.GONE
                            binding.emptyTransactionsText.visibility = View.GONE
                        }
                        is TransactionsViewModel.ViewState.Error -> {
                            binding.loadingIndicator.hide()
                            binding.swipeRefresh.isRefreshing = false
                            snackBar = snackBarUtil.retrySnackbar {
                                viewModel.refresh()
                            }
                        }
                        is TransactionsViewModel.ViewState.Success -> {
                            binding.loadingIndicator.hide()
                            snackBar?.dismiss()
                            binding.swipeRefresh.isRefreshing = false
                            adapter.transactionsList = it.transactions
                            adapter.notifyDataSetChanged()
                            when {
                                adapter.transactionsList.isEmpty() -> {
                                    binding.transactionsListView.visibility = View.GONE
                                    binding.emptyTransactionsText.visibility = View.VISIBLE
                                }
                                else -> {
                                    binding.transactionsListView.visibility = View.VISIBLE
                                    binding.emptyTransactionsText.visibility = View.GONE
                                }
                            }
                        }
                    }
                }
            )

            showStartDate.observe(viewLifecycleOwner) {
                if (it) this@TransactionsFragment.showStartDate()
            }

            showEndDate.observe(viewLifecycleOwner) {
                if (it) this@TransactionsFragment.showEndDate()
            }
        }
    }

    private fun setupEndDate() {
        binding.dateRangeSelector.dateRangeEndDate.setOnClickListener {
            viewModel.setShowEndDate()
        }
    }

    private fun showEndDate() {
        context?.let {
            val endDate = viewModel.endDate
            val datePickerDialog = DatePickerDialog(
                it,
                R.style.MaterialDatePicker,
                { _, year, monthOfYear, dayOfMonth ->
                    val pickedEndDate = LocalDate.of(year, monthOfYear + 1, dayOfMonth)
                    binding.dateRangeSelector.dateRangeEndDate.text =
                        formatDateString(pickedEndDate)
                    viewModel.endDate = pickedEndDate

                    refreshTransactions()
                    viewModel.setShowEndDate(false)
                },
                endDate.year, endDate.monthValue - 1, endDate.dayOfMonth
            )
            updateSelectableDateRange(datePickerDialog.datePicker, false)
            datePickerDialog.setOnCancelListener { viewModel.setShowEndDate(false) }
            datePickerDialog.show()
        }
    }

    private fun setupStartDate() {
        binding.dateRangeSelector.dateRangeStartDate.setOnClickListener {
            viewModel.setShowStartDate()
        }
    }

    private fun showStartDate() {
        context?.let {
            val startDate = viewModel.startDate
            val datePickerDialog = DatePickerDialog(
                it,
                R.style.MaterialDatePicker,
                { _, year, monthOfYear, dayOfMonth ->
                    val pickedStartDate = LocalDate.of(year, monthOfYear + 1, dayOfMonth)
                    binding.dateRangeSelector.dateRangeStartDate.text =
                        formatDateString(pickedStartDate)
                    viewModel.startDate = pickedStartDate

                    refreshTransactions()
                    viewModel.setShowStartDate(false)
                },
                startDate.year, startDate.monthValue - 1, startDate.dayOfMonth
            )
            updateSelectableDateRange(datePickerDialog.datePicker, true)
            datePickerDialog.setOnCancelListener { viewModel.setShowStartDate(false) }
            datePickerDialog.show()
        }
    }

    private fun updateSweepActivityValue(sweepOn: Boolean) {
        val valueText = if (sweepOn) R.string.sweep_on else R.string.sweep_off
        binding.sweepActivityValue.setText(valueText)
        binding.sweepActivityValue.contentDescription = getString(valueText)
    }

    private fun updateSelectableDateRange(datePicker: DatePicker, isStart: Boolean) {
        if (isStart) {
            val endDate = viewModel.endDate
            datePicker.maxDate = endDate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
            val minDate = LocalDate.of(endDate.year, endDate.monthValue, endDate.dayOfMonth).minusYears(2)
            datePicker.minDate = minDate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
        } else {
            val startDate = viewModel.startDate
            datePicker.minDate = startDate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
            datePicker.maxDate = LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
        }
    }

    private fun initDropdown() {
        sweepBottomSheetSelector.apply {
            init(
                SWEEP_DROPDOWN_TAG,
                getString(R.string.sweep_activity),
                listOf(getString(R.string.sweep_on), getString(R.string.sweep_off)),
                if (viewModel.isSweepActivityEnabled) SWEEP_ON_INDEX else SWEEP_OFF_INDEX,
                null,
                getString(R.string.sweep_description)
            )
            setListener { position, _ ->
                val sweepOn = position == SWEEP_ON_INDEX

                viewModel.isSweepActivityEnabled = sweepOn
                updateSweepActivityValue(sweepOn)
            }
        }
    }

    override val isOnTop get() = binding.transactionsListView.computeVerticalScrollOffset() == 0

    override fun scrollUp() {
        binding.transactionsListView.apply {
            stopScroll()
            smoothScrollToPosition(0)
        }
    }

    override fun onTabSelected() {
        val isFragmentReady = view != null
        dataDelegate.fetchData(isFragmentReady)
        if (isFragmentReady) {
            binding.transactionsListView.bringToFront()
        }
    }

    override fun onDestroyView() {
        binding.swipeRefresh.setOnRefreshListener(null)
        binding.sweepActivityValue.setOnClickListener(null)
        binding.transactionsListView.adapter = null
        super.onDestroyView()
    }
}
