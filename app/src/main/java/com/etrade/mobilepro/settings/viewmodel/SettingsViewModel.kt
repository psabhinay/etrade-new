package com.etrade.mobilepro.settings.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.common.di.LocalDynamicScreen
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.tracking.SCREEN_TITLE_SETTINGS
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class SettingsViewModel @Inject constructor(
    @LocalDynamicScreen
    private val dynamicScreenRepo: DynamicScreenRepo
) : ViewModel(), ScreenViewModel {

    private val viewStates = MutableLiveData<ViewState>()
    private var disposable: Disposable? = null

    override val screenName: String = SCREEN_TITLE_SETTINGS

    private fun disposeCurrent() {
        disposable?.takeUnless { it.isDisposed }?.dispose()
    }

    fun loadSettings(userSessionState: SessionStateChange) {
        viewStates.value = ViewState.Loading

        disposeCurrent()

        val screen: Observable<Resource<List<GenericLayout>>> = when (userSessionState) {
            SessionStateChange.AUTHENTICATED -> {
                dynamicScreenRepo.getScreen(ServicePath.AuthenticatedSettings)
            }
            SessionStateChange.UNAUTHENTICATED -> {
                dynamicScreenRepo.getScreen(ServicePath.Settings)
            }
        }

        disposable = screen.subscribeBy(
            onNext = { response ->
                when (response) {
                    is Resource.Success, is Resource.Loading -> {
                        viewStates.postValue(ViewState.Success(response.data ?: emptyList()))
                    }
                    is Resource.Failed -> {
                        viewStates.postValue(ViewState.Error)
                    }
                }
            },
            onError = {
                viewStates.postValue(ViewState.Error)
            }
        )
    }

    fun getViewState(): LiveData<ViewState> = viewStates

    override fun onCleared() {
        disposeCurrent()

        super.onCleared()
    }

    sealed class ViewState {

        object Loading : ViewState()

        object Error : ViewState()

        data class Success(val result: List<GenericLayout>) : ViewState()
    }
}
