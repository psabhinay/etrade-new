package com.etrade.mobilepro.portfolio.taxlot

import com.etrade.mobile.math.MathHelper.HUNDRED
import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.pnl.calculations.calculateDaysGain
import com.etrade.mobilepro.pnl.calculations.calculateTotalGain
import com.etrade.mobilepro.positions.api.TaxLotPosition
import com.etrade.mobilepro.util.domain.data.deriveMarketValue
import com.etrade.mobilepro.util.safeLet
import com.etrade.mobilepro.util.safeParseBigDecimal
import org.threeten.bp.Instant
import org.threeten.bp.temporal.ChronoUnit
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

@SuppressWarnings("LongMethod")
internal fun TaxLotPosition.updateWithInstrument(
    data: Instrument
): TaxLotPosition {
    val updatedPrice: String
    val quantityDecimal: BigDecimal?

    if (data.instrumentType.isOption) {
        updatedPrice = data.lastPrice?.toPlainString().orEmpty()
        quantityDecimal = quantity?.multiply(HUNDRED)
    } else {
        updatedPrice = data.lastPrice?.toPlainString().orEmpty()
        quantityDecimal = quantity
    }

    val lastPriceDecimal = updatedPrice.safeParseBigDecimal()

    val feesDecimal = safeLet(fees, quantityDecimal) { fee, quantity ->
        (fee * quantity).abs()
    }

    val todayPositionInfo = createTodayPositionInfo()

    val daysGain = calculateDaysGain(
        data.instrumentType,
        lastPriceDecimal,
        data.previousClose,
        quantityDecimal,
        todayPositionInfo.quantity,
        todayPositionInfo.pricePaid,
        todayPositionInfo.commissions,
        feesDecimal,
        pricePaid
    )

    val totalGain = calculateTotalGain(
        data.instrumentType,
        lastPriceDecimal,
        pricePaid,
        quantityDecimal,
        commission,
        feesDecimal
    )

    val marketValue = safeLet(lastPriceDecimal, quantityDecimal) { last, quantity ->
        deriveMarketValue(last, quantity)
    }

    return object : TaxLotPosition {
        override val taxLotId: String = this@updateWithInstrument.taxLotId
        override val acquiredDateMs: Long = this@updateWithInstrument.acquiredDateMs
        override val positionId: String
            get() = this@updateWithInstrument.positionId
        override val daysGain: BigDecimal?
            get() = daysGain?.dollar ?: this@updateWithInstrument.daysGain
        override val totalGain: BigDecimal?
            get() = totalGain?.dollar ?: this@updateWithInstrument.totalGain
        override val pricePaid: BigDecimal?
            get() = this@updateWithInstrument.pricePaid
        override val commission: BigDecimal?
            get() = this@updateWithInstrument.commission
        override val fees: BigDecimal?
            get() = this@updateWithInstrument.fees
        override val quantity: BigDecimal?
            get() = this@updateWithInstrument.quantity
        override val displayQuantity: BigDecimal?
            get() = this@updateWithInstrument.displayQuantity
        override val daysGainPercentage: BigDecimal?
            get() = daysGain?.percent ?: this@updateWithInstrument.daysGainPercentage
        override val marketValue: BigDecimal?
            get() = marketValue
        override val totalGainPercentage: BigDecimal?
            get() = totalGain?.percent ?: this@updateWithInstrument.totalGainPercentage
        override val todayClose: BigDecimal?
            get() = this@updateWithInstrument.todayClose
        override val baseSymbolAndPrice: String?
            get() = this@updateWithInstrument.baseSymbolAndPrice
        override val mark: BigDecimal?
            get() = this@updateWithInstrument.mark
        override val inTheMoneyFlag: Boolean?
            get() = this@updateWithInstrument.inTheMoneyFlag
        override val symbol: String
            get() = this@updateWithInstrument.symbol
    }
}

/**
 *  Constructs extended Position data points required for Day's gain calculation
 *  if the position was bought today
 */
internal fun TaxLotPosition.createTodayPositionInfo(): TodayPositionInfo =
    if (isLotTradedToday()) {
        TodayPositionInfo(
            quantity ?: ZERO,
            pricePaid ?: ZERO,
            commission ?: ZERO
        )
    } else {
        // default with ZERO's
        TodayPositionInfo()
    }

private fun TaxLotPosition.isLotTradedToday(): Boolean {
    val today12AmUtc = Instant.now().truncatedTo(ChronoUnit.DAYS).epochSecond
    return acquiredDateMs > today12AmUtc
}

data class TodayPositionInfo(
    val quantity: BigDecimal = ZERO,
    val pricePaid: BigDecimal = ZERO,
    val commissions: BigDecimal = ZERO
)
