package com.etrade.mobilepro.news.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.common.readstatus.ReadableType
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.api.model.createEmptyTextNewsItem
import com.etrade.mobilepro.news.api.repo.NewsRepo
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentLoader
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsFontAdjustHandler
import com.etrade.mobilepro.util.android.SingleLiveEvent
import com.etrade.mobilepro.util.android.recyclerviewutil.SelectionTracker
import io.reactivex.Observable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

@Suppress("TooManyFunctions")
abstract class BaseNewsViewModel(
    private val newsRepo: NewsRepo,
    private val statusRepo: ReadStatusRepo,
    val contentLoader: TextNewsContentLoader,
    val fontAdjustHandler: TextNewsFontAdjustHandler,
    val selectionTracker: SelectionTracker
) : ViewModel() {
    private val mutableNavigateLocation = MutableLiveData<Int>()
    private val _refreshItemAtIndexesEvent = SingleLiveEvent<List<Int>>()
    private val _selectedTextNewsItem = MutableLiveData<NewsItem.Text?>()

    val selectedNewsItem: LiveData<NewsItem.Text?> = _selectedTextNewsItem
    private val requestParamsFlow = MutableSharedFlow<NewsRequest?>(replay = 1)

    @ExperimentalCoroutinesApi
    val pagingDataFlow: StateFlow<PagingData<NewsItem>> = requestParamsFlow.flatMapLatest {
        newsRepo.news(
            type = getNewsType(),
            requestParams = it,
            headerItemsSource = provideHeaderItemsSource()
        ).flow.cachedIn(viewModelScope)
    }.stateIn(
        viewModelScope,
        SharingStarted.WhileSubscribed(stopTimeoutMillis = 5000),
        PagingData.empty()
    )

    val navigateLocationData: LiveData<Int> = mutableNavigateLocation
    val refreshItemAtIndexesEvent: LiveData<List<Int>> = _refreshItemAtIndexesEvent
    var isInDualPane: Boolean = false
        set(value) {
            lastSelectedItem?.let { lastItem ->
                lastSelectedPos?.let { lastPos ->
                    if (!field && value) { // we were in Single Pane mode, and have already visited some article, and now are going to Dual Pane
                        field = value
                        newsItemSelected(lastItem, lastPos)
                        return
                    }
                }
            }
            firstItem?.let {
                if (!field && value) { // we were in Single Pane mode and have not visited any article yet, and now are going to Dual Pane
                    field = value
                    newsItemSelected(it, provideHeaderItemsCount())
                    return
                }
            }
            field = value
        }

    private var firstItem: NewsItem.Text? = null
    private var lastSelectedItem: NewsItem.Text? = null
    private var lastSelectedPos: Int? = null

    init {
        selectionTracker.selectionChangedListener = { oldPos, newPos ->
            _refreshItemAtIndexesEvent.postValue(listOf(oldPos, newPos))
        }
    }

    abstract fun getNewsType(): NewsType

    open fun onBecomeActive() { // todo revert to internal
        // this is called when corresponding tab is selected
    }

    protected open fun provideHeaderItemsSource(): Observable<List<NewsItem>>? = null

    protected open fun provideHeaderItemsCount(): Int = 0

    @ExperimentalCoroutinesApi
    fun refresh(forceRefresh: Boolean) {
        if (forceRefresh || pagingDataFlow.value == PagingData.empty<NewsItem>()) {
            viewModelScope.launch {
                resetParamsAndLoad(requestParamsFlow.replayCache.lastOrNull())
            }
        }
    }

    @ExperimentalCoroutinesApi
    fun refreshWithParams(requestParams: NewsRequest) {
        viewModelScope.launch {
            resetParamsAndLoad(requestParams)
        }
    }

    fun newsItemSelected(item: NewsItem.Text, position: Int) {
        lastSelectedItem = item
        lastSelectedPos = position
        statusRepo.markAsRead(item.docId, type = ReadableType.NEWS_ARTICLE)
        if (isInDualPane) {
            contentLoader.load(item.docId)
            if (_selectedTextNewsItem.value != item) {
                _selectedTextNewsItem.value = item
            }
            selectionTracker.itemWasSelectedAt(position)
        }
    }

    fun wasRead(articleId: String): Boolean {
        return statusRepo.wasRead(articleId)
    }

    private suspend fun resetParamsAndLoad(requestParams: NewsRequest?) {
        lastSelectedPos = null
        lastSelectedItem = null
        firstItem = null
        _selectedTextNewsItem.value = null

        requestParamsFlow.emit(requestParams)
    }

    private fun applyAsFirstNewsItem(item: NewsItem.Text) {
        if (firstItem == null) {
            this.firstItem = item
            if (isInDualPane) {
                statusRepo.markAsRead(item.docId, type = ReadableType.NEWS_ARTICLE)
                contentLoader.load(item.docId)
                _selectedTextNewsItem.value = item
                selectionTracker.itemWasSelectedAt(provideHeaderItemsCount())
            }
        }
    }

    fun handleFirstLoadedItem(firstItem: NewsItem?) {
        when (firstItem) {
            is NewsItem.Text -> applyAsFirstNewsItem(firstItem)
            is NewsItem.CompanyHeader -> firstItem.subsequentFirstItem?.let {
                handleFirstLoadedItem(it)
            }
            else -> {
                if (isInDualPane) {
                    contentLoader.loadEmpty()
                    _selectedTextNewsItem.value = createEmptyTextNewsItem()
                }
            }
        }
    }
}
