package com.etrade.mobilepro.uiwidgets.actionsheet

import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import kotlinx.parcelize.Parcelize

@Parcelize
data class ActionItem(
    val id: Int,
    @StringRes val displayNameId: Int,
    @DrawableRes val iconId: Int = 0,
    val displayNameArgs: List<String> = emptyList(),
    @StringRes val descriptionId: Int = 0
) : Parcelable
