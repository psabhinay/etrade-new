package com.etrade.mobilepro.trade.data.stock

import com.etrade.mobilepro.trade.api.stock.ConfirmedStockOrder
import com.etrade.mobilepro.trade.api.stock.StockOrder
import com.etrade.mobilepro.trade.api.stock.StockOrderPlaceResponse
import com.etrade.mobilepro.trade.api.stock.StockOrderResponse

internal class StockOrderPlaceResponseWrapper(private val dto: StockOrderResponseDto) : StockOrderPlaceResponse, StockOrderResponse by dto {
    override val orders: List<ConfirmedStockOrder> = listOfNotNull(dto.order?.first()?.confirmedOrder)
}

private val OrderDto.confirmedOrder: ConfirmedStockOrder
    get() = object : ConfirmedStockOrder, StockOrder by this {
        override val confirmationOrderId: String? = orderId
        override val placedDate: String? = date.takeIf { it != "0" }
    }
