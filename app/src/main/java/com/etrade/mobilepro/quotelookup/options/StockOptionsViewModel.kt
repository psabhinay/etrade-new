package com.etrade.mobilepro.quotelookup.options

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.searchingapi.SearchInteractor
import com.etrade.mobilepro.searchingapi.SelectionHistoryRepo
import com.etrade.mobilepro.searchingapi.SymbolLookupRepo
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.searchingapi.items.SymbolLookUpResult
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(StockOptionsViewModel::class.java)
/*
    ErrorCode 20052 stands for no result found with
    <ErrorDescription>No Options were found for that symbol.</ErrorDescription>
 */
private const val EMPTY_ERROR_CODE = 20052

class StockOptionsViewModel @Inject constructor(
    private val symbolLookupRepo: SymbolLookupRepo,
    private val selectionHistoryRepo: SelectionHistoryRepo,
    private val searchInteractor: SearchInteractor
) : ViewModel() {
    private val compositeDisposable = CompositeDisposable()
    private val querySubject: Subject<SearchQuery> = PublishSubject.create<SearchQuery>()
    private val _results = MutableLiveData<ViewState>()
    val results: LiveData<ViewState> = _results
    var searchQuery: SearchQuery? = null
    var selectedPosition = 0

    init {
        compositeDisposable.add(
            querySubject
                .switchMap { searchQuery ->
                    symbolLookupRepo.loadSymbolLookup(searchQuery)
                }
                .subscribeBy(
                    onNext = { result ->
                        result?.let {
                            val state = result.errorInfo?.run {
                                if (errorCode == EMPTY_ERROR_CODE) {
                                    ViewState.Success(
                                        SymbolLookUpResult(
                                            emptyList(),
                                            emptyList(),
                                            result.errorInfo
                                        )
                                    )
                                } else {
                                    ViewState.Error("")
                                }
                            } ?: ViewState.Success(result)

                            _results.postValue(state)
                        }
                    },
                    onError = {
                        _results.postValue(ViewState.Error(it.localizedMessage))
                        logger.error(it.message)
                    }
                )
        )
    }

    fun search(query: SearchQuery) {
        _results.value = ViewState.Loading
        searchQuery = query
        querySubject.onNext(query)
    }

    fun addItemSelected(item: SearchResultItem) {
        selectionHistoryRepo.addToHistory(item)
    }

    fun itemSelected(searchType: SearchType, item: SearchResultItem) {
        searchInteractor.searchResultItemSelected(searchType, item)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    sealed class ViewState {
        object Loading : ViewState()
        data class Error(val message: String?) : ViewState()
        data class Success(val results: SymbolLookUpResult) : ViewState()
    }
}
