package com.etrade.mobilepro.screener.api

import android.text.Spannable

sealed class ScreenerItem {
    data class ScreenerSet(
        val type: ScreenerType,
        val title: String,
        val filter: ScreenerFilter
    ) : ScreenerItem() {
        fun isEmpty(): Boolean = filter.isEmpty()
    }

    data class DisclosuresText(val text: Spannable) : ScreenerItem()

    object Footer : ScreenerItem()
}
