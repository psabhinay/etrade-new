package com.etrade.mobilepro.timeout.setting.service.dto

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResponseDataDto(
    @Json(name = "text")
    val text: String
) : BaseDataDto()
