package com.etrade.mobilepro.widgets.quote

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.uiwidgets.quote.QuotePriceTextView
import com.etrade.mobilepro.uiwidgets.quotetextview.R
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(application = android.app.Application::class)
class QuotePriceTextViewTest {

    private var sut: QuotePriceTextView? = null

    private val context = ApplicationProvider.getApplicationContext<Context>()

    @Before
    fun setUp() {
        sut = QuotePriceTextView(context)
    }

    @Test
    fun testQuotePriceTradeView() {
        assertNotNull(sut)

        sut?.let {
            // when
            it.setTradeViewFullPriceText("79.55", "200", "Q")
            // then
            assertEquals(it.text.toString(), "79.55 X 200Q")

            // when
            it.setTradeViewShortPriceText("79.55")
            // then
            assertEquals(it.text.toString(), "79.55")
        }
    }

    @Test
    fun testDetailsViewFormattedPrice() {
        assertNotNull(sut)

        sut?.let {
            // when
            it.setDetailsViewFormattedPrice("79.55", "200", context.resources.getString(R.string.quote_widget_bid_format))
            // then
            assertEquals(it.text.toString(), "Bid: 79.55 x 200")

            // when
            it.setDetailsViewFormattedPrice("79.55", "200", context.resources.getString(R.string.quote_widget_ask_format))

            // then
            assertEquals(it.text.toString(), "Ask: 79.55 x 200")
        }
    }
}
