package com.etrade.mobilepro.screener.data.aggregator

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerFilterJsonGenerator
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterRequest
import com.etrade.mobilepro.screener.api.filter.ScreenerFilterFactory
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject

// Not thread safe
class DefaultScreenerFilterAggregator @Inject constructor(
    private val screenerFilterFactory: ScreenerFilterFactory,
    private val screenerFilterJsonGenerator: ScreenerFilterJsonGenerator
) : ScreenerFilterAggregator {
    private val requestsSubject: Subject<ScreenerFilterRequest> = PublishSubject.create()
    private val requestsFlow = requestsSubject.share()
    private val storage = mapOf(
        ScreenerType.Group.ETF to mutableMapOf<ScreenerType, ScreenerFilter>(),
        ScreenerType.Group.MUTUAL_FUND to mutableMapOf(),
        ScreenerType.Group.STOCK to mutableMapOf()
    )

    override fun init() {
        ScreenerType.Group.values().forEach { group ->
            group.contents.forEach { screenerType ->
                storage.getValue(group)[screenerType] = screenerFilterFactory.createDefault(screenerType)
            }
        }
    }

    override fun put(type: ScreenerType, filter: ScreenerFilter, triggerRequest: Boolean) {
        storage.getValue(type.belongsTo)[type] = filter
        emitUpdate(type, triggerRequest)
    }

    @Suppress("UNCHECKED_CAST")
    override fun <T : ScreenerFilter> get(type: ScreenerType): T =
        storage.getValue(type.belongsTo).getOrPut(type) { screenerFilterFactory.createDefault(type) } as T

    override fun clear(type: ScreenerType) {
        storage.getValue(type.belongsTo)[type] = screenerFilterFactory.createDefault(type)
        emitUpdate(type, true)
    }

    private fun emitUpdate(type: ScreenerType, fireNetworkRequest: Boolean) =
        requestsSubject.onNext(ScreenerFilterRequest(type, generateRequestStringRepresentation(type.belongsTo), fireNetworkRequest))

    override fun generateRequestStringRepresentation(group: ScreenerType.Group): String =
        screenerFilterJsonGenerator.generateFiltersJson(storage.getValue(group).values, group)

    override fun screenerFilterRequests(): Observable<ScreenerFilterRequest> = requestsFlow

    override fun getFilterSnapshot(group: ScreenerType.Group): List<ScreenerFilter> = storage.getValue(group).values.toList()
}
