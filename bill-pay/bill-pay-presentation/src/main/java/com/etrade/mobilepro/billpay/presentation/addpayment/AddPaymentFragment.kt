package com.etrade.mobilepro.billpay.presentation.addpayment

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.presentation.BillPayContainerFragmentArgs
import com.etrade.mobilepro.billpay.presentation.BillPayContainerFragmentDirections
import com.etrade.mobilepro.billpay.presentation.BillPayViewModel
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.billpay.presentation.common.EMPTY_STRING
import com.etrade.mobilepro.billpay.presentation.common.NavigationViewModel
import com.etrade.mobilepro.billpay.presentation.common.accountDetailsContentDescriptionFormatter
import com.etrade.mobilepro.billpay.presentation.common.formatAccountName
import com.etrade.mobilepro.billpay.presentation.common.formatCurrency
import com.etrade.mobilepro.billpay.presentation.common.getAmountError
import com.etrade.mobilepro.billpay.presentation.common.payeeContentDescriptionFormatter
import com.etrade.mobilepro.billpay.presentation.common.payeeNameFormatter
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayFragmentAddPaymentBinding
import com.etrade.mobilepro.billpay.presentation.paymentpreview.PaymentPreview
import com.etrade.mobilepro.billpay.presentation.viewmodel.AddPaymentViewModelFactoryFactory
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.disposeOnDestinationChanged
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.transfermoney.widget.CALENDAR_REQUEST_KEY
import com.etrade.mobilepro.transfermoney.widget.TransferMoneyCalendarDialogFragment
import com.etrade.mobilepro.transfermoney.widget.extractCalendarDialogResult
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.accessibility.bindTextEditAccessibilityListeners
import com.etrade.mobilepro.util.android.accessibility.bindTextInputButtonAccessibility
import com.etrade.mobilepro.util.android.accessibility.toUSCurrency
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.clearFocusSafely
import com.etrade.mobilepro.util.android.hideSoftKeyboard
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.android.textutil.CurrencyTextWatcher
import com.etrade.mobilepro.util.formatters.americanSimpleDateFormatter
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import com.google.android.material.snackbar.Snackbar
import org.threeten.bp.LocalDate
import java.math.BigDecimal
import javax.inject.Inject

private const val TAG_TO = "drop_down_to"
private const val TAG_FROM = "drop_down_from"
private const val ADD_PAYEE_DIALOG_TAG = "add_payee_tag"

private const val ADD_PAYEE_DIALOG_CODE = 302

@Suppress("LargeClass", "TooManyFunctions")
@RequireLogin
class AddPaymentFragment @Inject constructor(
    private val assistedViewModelFactory: AddPaymentViewModelFactoryFactory,
    private val sharedViewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.bill_pay_fragment_add_payment) {
    private val binding by viewBinding(BillPayFragmentAddPaymentBinding::bind)

    private val sharedViewModel: BillPayViewModel by navGraphViewModels(R.id.bill_pay_nav_graph) { sharedViewModelFactory }
    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.bill_pay_nav_graph)

    private val args: BillPayContainerFragmentArgs by navArgs()

    private val viewModel: AddPaymentViewModel by viewModels {
        assistedViewModelFactory.create(
            validAccountList = sharedViewModel.viewState.value?.validAccounts ?: emptyList(),
            payeeList = sharedViewModel.payees.value ?: emptyList(),
            originAccount = args.fromAccount?.ifBlank { null },
            destinationPayeeId = args.toPayee?.ifBlank { null }
        )
    }

    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    private val disposeListener = disposeOnDestinationChanged { sharedViewModel.disposeState() }

    private lateinit var transferFromSelector: DropDownMultiSelector<AccountDetails>

    private lateinit var transferToSelector: DropDownMultiSelector<Payee>

    private lateinit var currencyTextWatcher: CurrencyTextWatcher

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupDialogResults()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupTextLinks()
        setupViewModels()
        setupDropDowns()
        setupListeners()

        if (savedInstanceState != null) {
            getCalendarBuilder()?.restore(this)
        }
    }

    private val restoreTransferSignalObserver: Observer<ConsumableLiveEvent<BigDecimal>> =
        Observer {
            it.consume { amountBigDecimal ->
                binding.paymentForm.amount.setText(formatCurrency(amountBigDecimal))
                binding.paymentForm.amountLayout.error =
                    resources.getAmountError(currencyTextWatcher.rawValue)
            }
        }

    override fun onResume() {
        super.onResume()
        findNavController().addOnDestinationChangedListener(disposeListener)
        setupAmountField()
        viewModel.restoreTransferAmountSignal.observe(
            viewLifecycleOwner,
            restoreTransferSignalObserver
        )
    }

    override fun onPause() {
        super.onPause()
        viewModel.restoreTransferAmountSignal.removeObserver(restoreTransferSignalObserver)
        findNavController().removeOnDestinationChangedListener(disposeListener)
        binding.paymentForm.amount.removeTextChangedListener(currencyTextWatcher)
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                ADD_PAYEE_DIALOG_CODE -> {
                    val billPaySessionId = sharedViewModel.viewState.value?.billPaySessionId
                    requireNotNull(billPaySessionId) { "No Bill Pay Session Id" }
                    findNavController().navigate(
                        BillPayContainerFragmentDirections.actionManagePayeeToPayeeForm(
                            billPaySessionId
                        )
                    )
                }
            }
        }
        setFragmentResultListener(CALENDAR_REQUEST_KEY) { _, bundle ->
            bundle.extractCalendarDialogResult().let {
                viewModel.onScheduleDateSelected(it.selectedDate, it.selectedPage)
                binding.paymentForm.memoEditText.requestFocus()
            }
        }
    }

    private fun render(viewState: ViewState) = with(viewState) {
        binding.paymentForm.scheduleLayout.isEnabled =
            selectedPayee != null && selectedAccount != null
        binding.preview.isEnabled = viewModel.canCollectData()

        renderLoading(isLoading)
        renderScheduleDate(selectedScheduleDate)
        renderSelectedPayee(selectedPayee)
        renderSelectedAccount(selectedAccount)
        bindAddPaymentAccessibility(viewState)

        if (selectedAccount == null && validAccounts.size == 1) {
            viewModel.onAccountSelected(validAccounts.first())
        }

        binding.paymentForm.memoEditText.setText(memo)

        payeesNotFound?.getNonConsumedContent()?.let { displayAddPayeeDialog() }
        error?.getNonConsumedContent()
            ?.let { snackBarUtil.errorSnackbar(duration = Snackbar.LENGTH_INDEFINITE) }
    }

    private fun renderLoading(isLoading: Boolean) {
        if (isLoading) {
            binding.progressBar.show()
            binding.content.visibility = View.INVISIBLE
        } else {
            binding.progressBar.hide()
            binding.content.visibility = View.VISIBLE
        }
    }

    private fun renderScheduleDate(date: LocalDate?) {
        if (date == null) {
            binding.paymentForm.schedule.setText(EMPTY_STRING)
        } else {
            binding.paymentForm.schedule.setText(createAmericanSimpleDateFormatter().format(date))
        }
    }

    private fun renderSelectedPayee(payee: Payee?) {
        if (payee == null) {
            binding.paymentForm.transferTo.setText(EMPTY_STRING)
        } else {
            binding.paymentForm.transferTo.setText(resources.payeeNameFormatter.invoke(payee))
        }
    }

    private fun renderSelectedAccount(account: AccountDetails?) {
        if (account == null) {
            binding.paymentForm.transferFrom.setText(EMPTY_STRING)
        } else {
            binding.paymentForm.transferFrom.setText(account.formatAccountName())
        }
    }

    private fun setupViewModels() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer { render(it) })
        sharedViewModel.payees.observe(viewLifecycleOwner, Observer { viewModel.setPayees(it) })
        sharedViewModel.addedPayee.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.also { payee ->
                    viewModel.onPayeeSelected(payee)
                }
            }
        )
        navigationViewModel.paymentPreviewCleared.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.let { resetState() }
            }
        )
    }

    private fun setupDropDowns() {
        setupPayeesDropDown()
        setupFromAccountDropDown()
    }

    private fun setupFromAccountDropDown() {
        transferFromSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = TAG_FROM,
                multiSelectEnabled = false,
                title = getString(R.string.from),
                itemsData = getDropDownAccounts(),
                selectListener = { _, item -> viewModel.onAccountSelected(item.value) }
            )
        )
    }

    private fun setupPayeesDropDown() {
        transferToSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = TAG_TO,
                multiSelectEnabled = false,
                title = getString(R.string.to),
                itemsData = getDropDownPayees(),
                selectListener = { _, item -> viewModel.onPayeeSelected(item.value) },
                actionButtonSettings = DropDownMultiSelector.ActionButtonSettings(
                    buttonTitle = getString(
                        R.string.bill_pay_add_payee
                    )
                ) {
                    transferToSelector.close()
                    sharedViewModel.viewState.value?.let {
                        requireNotNull(it.billPaySessionId) { "No Bill Pay Session Id" }
                        findNavController().navigate(
                            BillPayContainerFragmentDirections.actionManagePayeeToPayeeForm(it.billPaySessionId)
                        )
                    }
                }
            )
        )
    }

    private fun setupListeners() {
        binding.paymentForm.memoEditText.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                viewModel.setMemo(binding.paymentForm.memoEditText.text.toString())
            }
        }
        binding.paymentForm.memoLayout.bindTextEditAccessibilityListeners(
            binding.paymentForm.memoEditText,
            getString(R.string.bill_pay_memo_hint)
        )

        binding.paymentForm.transferTo.setOnClickListener { transferToSelector.open() }
        binding.paymentForm.transferFrom.setOnClickListener { transferFromSelector.open() }
        binding.paymentForm.amount.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                binding.paymentForm.amount.clearFocusSafely()
                activity?.hideSoftKeyboard()
                binding.paymentForm.schedule.callOnClick()
            }
            false
        }
        binding.paymentForm.schedule.setOnClickListener {
            getCalendarBuilder()?.show(this)
        }
        binding.preview.setOnClickListener {
            if (viewModel.canCollectData()) {
                viewModel.collectData()?.let(::navigateToPreview)
            }
        }
    }

    private fun setupTextLinks() {
        binding.billPayLinkDisclosures.setOnClickListener {
            showSpannableText(R.string.disclosures, R.string.bill_pay_disclosure_content)
        }

        binding.billPayLinkFaq.setOnClickListener {
            showSpannableText(R.string.faq, R.string.bill_pay_faq_content)
        }
    }

    private fun setupAmountField() {
        currencyTextWatcher = CurrencyTextWatcher(
            editText = binding.paymentForm.amount,
            initialAmountInDollar = viewModel.viewState.value?.amount
        ) {
            viewModel.onAmountEntered(it)
        }
        binding.paymentForm.amount.setOnFocusChangeListener { _, hasFocus ->
            binding.paymentForm.amountLayout.error = if (!hasFocus) {
                resources.getAmountError(currencyTextWatcher.rawValue)
            } else {
                EMPTY_STRING
            }
        }
    }

    @Suppress("LongMethod")
    private fun bindAddPaymentAccessibility(viewState: ViewState) {
        binding.paymentForm.toLayout.bindTextInputButtonAccessibility(
            getString(
                R.string.accessibility_label_to,
                context?.accountNameContentDescription(
                    viewState.selectedPayee?.let {
                        resources.payeeNameFormatter.invoke(
                            it
                        )
                    } ?: ""
                )
            )
        )
        binding.paymentForm.fromLayout.bindTextInputButtonAccessibility(
            getString(
                R.string.accessibility_label_from,
                context?.accountNameContentDescription(
                    viewState.selectedAccount?.accountShortName ?: ""
                )
            )
        )
        binding.paymentForm.amountLayout.apply {
            val label = getString(R.string.amount)
            val description = viewState.amount?.toUSCurrency()
            bindTextInputButtonAccessibility(
                description = "$label ${description ?: ""}",
                actionLabel = context.getString(R.string.edit_action)
            )
        }
        binding.paymentForm.scheduleLayout.apply {
            val label = getString(R.string.schedule_date)
            val description =
                viewState.selectedScheduleDate?.let(americanSimpleDateFormatter::format) ?: ""
            bindTextInputButtonAccessibility("$label $description")
        }
        binding.paymentForm.memoLayout.apply {
            val label = getString(R.string.bill_pay_memo_hint)
            val description = viewState.memo ?: ""
            bindTextInputButtonAccessibility("$label $description")
        }
    }

    private fun getDropDownPayees(): LiveData<List<DropDownMultiSelector.Selectable<Payee>>> {
        return Transformations.map(viewModel.viewState) {
            it.payees.map { payee ->
                DropDownMultiSelector.Selectable(
                    title = "",
                    titleFormatter = resources.payeeNameFormatter,
                    descriptionFormatter = { item ->
                        resources.getQuantityString(
                            R.plurals.bill_pay_business_days,
                            item.leadDays ?: 0,
                            item.leadDays ?: 0
                        )
                    },
                    value = payee,
                    selected = payee == viewModel.viewState.value?.selectedPayee,
                    titleColor = ContextCompat.getColor(requireContext(), R.color.colorAccent),
                    contentDescriptionFormatter = resources::payeeContentDescriptionFormatter
                )
            }
        }
    }

    private fun getDropDownAccounts(): LiveData<List<DropDownMultiSelector.Selectable<AccountDetails>>> {
        return Transformations.map(viewModel.viewState) {
            it.validAccounts.map { account ->
                DropDownMultiSelector.Selectable(
                    title = account.formatAccountName(),
                    value = account,
                    selected = account == viewModel.viewState.value?.selectedAccount,
                    descriptionFormatter = { item ->
                        getString(
                            R.string.bill_pay_available_cash_placeholder,
                            formatAvailableWithdrawal(item)
                        )
                    },
                    titleColor = ContextCompat.getColor(requireContext(), R.color.colorAccent),
                    contentDescriptionFormatter = resources::accountDetailsContentDescriptionFormatter
                )
            }
        }
    }

    private fun formatAvailableWithdrawal(accountDetails: AccountDetails): String {
        return accountDetails.brokerage?.cashAvailableForWithdrawal?.let {
            MarketDataFormatter.formatCurrencyValue(it)
        } ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
    }

    private fun displayAddPayeeDialog() {
        CustomDialogFragment.newInstance(
            title = getString(R.string.please_note),
            message = getString(R.string.bill_pay_add_payee_dialog_message),
            okTitle = getString(R.string.bill_pay_add_payee),
            cancelTitle = getString(R.string.cancel),
            cancelable = false,
            requestCode = ADD_PAYEE_DIALOG_CODE
        ).let { dialog ->
            dialog.show(parentFragmentManager, ADD_PAYEE_DIALOG_TAG)
        }
    }

    private fun showSpannableText(@StringRes title: Int, @StringRes message: Int) {
        activity?.let { activity ->
            startActivity(
                SpannableTextOverlayActivity.intent(
                    context = activity,
                    title = getString(title),
                    text = getString(message),
                    negative = R.drawable.ic_close
                )
            )
        }
    }

    private fun getCalendarBuilder(): TransferMoneyCalendarDialogFragment.Builder? {
        return viewModel.viewState.value?.let { state ->
            if (state.selectedAccount != null && state.selectedPayee != null) {
                TransferMoneyCalendarDialogFragment.Builder(
                    isOriginAccountExternal = viewModel.isSelectedAccountExternal,
                    isDestinationAccountExternal = viewModel.isPayeeAccountExternal,
                    leadDays = state.selectedPayee.leadDays,
                    selectedDate = state.selectedScheduleDate,
                    selectedPage = state.selectedCalendarPage
                )
            } else {
                null
            }
        }
    }

    private fun resetState() {
        binding.content.smoothScrollTo(0, 0)
        if (binding.paymentForm.amount.text?.isEmpty() == false) {
            binding.paymentForm.amount.setText(EMPTY_STRING)
        }
        viewModel.reset()
    }

    private fun navigateToPreview(paymentPreview: PaymentPreview) {
        findNavController().navigate(
            BillPayContainerFragmentDirections.actionAddPaymentToPreview(paymentPreview)
        )
    }
}
