package com.etrade.mobilepro.screener.data.json.stock

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.stock.TechnicalsFilter
import javax.inject.Inject

private const val WEEK_HIGH_FIELD = "PriceChange52WeekHigh"
private const val WEEK_LOW_FIELD = "PriceChange52WeekLow"

class TechnicalsFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val technicalsFilter = screenerFilter as TechnicalsFilter
        val arguments = mutableListOf<Argument>()

        if (!technicalsFilter.stock52WeekLowNotAvailable) {
            arguments.add(
                Argument(
                    listOf(getTechnicalsClause(technicalsFilter.stock52WeekLow)),
                    WEEK_LOW_FIELD
                )
            )
        }

        if (!technicalsFilter.stock52WeekHighNotAvailable) {
            arguments.add(
                Argument(
                    listOf(getTechnicalsClause(technicalsFilter.stock52WeekHigh)),
                    WEEK_HIGH_FIELD
                )
            )
        }

        return PartialScreenerParams(arguments)
    }

    private fun getTechnicalsClause(value: String): Clause {
        return Clause(OPERATOR_LESS_THAN, listOf(value))
    }
}
