package com.etrade.mobilepro.screener.presentation.tableview

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.etrade.mobilepro.screener.api.result.MorningstarRatingResult
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.HasDataCell
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

class ResultDataCellWithStarsViewHolder(itemView: View) :
    AbstractViewHolder(itemView), HasDataCell {

    private val fundsCount: TextView = itemView.findViewById<View>(R.id.funds_count) as TextView
    private val ratings = listOf(
        itemView.findViewById<ImageView>(R.id.rating_1),
        itemView.findViewById<ImageView>(R.id.rating_2),
        itemView.findViewById<ImageView>(R.id.rating_3),
        itemView.findViewById<ImageView>(R.id.rating_4),
        itemView.findViewById<ImageView>(R.id.rating_5)
    )
    private val grayColor = ContextCompat.getColor(fundsCount.context, R.color.greyThirtyTransparent)
    private val purpleColor = ContextCompat.getColor(fundsCount.context, R.color.primaryColor)
    override var cell: DataCell? = null
        set(value) {
            field = value

            (value?.value as? MorningstarRatingResult)?.let {
                if (it.fundsCount > 0) {
                    fundsCount.visibility = View.VISIBLE
                    fundsCount.text = fundsCount.context.resources.getQuantityString(R.plurals.number_of_funds, it.fundsCount, it.fundsCount)
                } else {
                    fundsCount.visibility = View.GONE
                }
                updateRatings(it.starsCount)
            }
        }

    private fun updateRatings(rating: Int) {
        ratings.mapIndexed { index: Int, imageView: ImageView? ->
            val color = if (rating > index) {
                purpleColor
            } else {
                grayColor
            }
            imageView?.setColorFilter(color)
        }
    }
}
