package com.etrade.mobilepro.trade.data.form

import com.etrade.mobilepro.util.ValueCoder

object CommonAllOrNoneValueCoder : ValueCoder<Boolean> {

    private const val encodedFalse: String = "0"
    private const val encodedTrue: String = "1"

    override fun encode(value: Boolean): Any = if (value) { encodedTrue } else { encodedFalse }

    override fun decode(encoded: Any): Boolean? {
        return when (encoded) {
            encodedFalse -> false
            encodedTrue -> true
            else -> null
        }
    }
}
