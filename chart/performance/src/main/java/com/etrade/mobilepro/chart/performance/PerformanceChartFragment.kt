package com.etrade.mobilepro.chart.performance

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.chart.model.ChartLineData
import com.etrade.mobilepro.chart.performance.databinding.FragmentPerformanceChartBinding
import com.etrade.mobilepro.chart.util.updateLineData
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.ui.multitoggle.Toggle
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.contentView
import com.etrade.mobilepro.util.android.extension.dpToPx
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@RequireLogin
class PerformanceChartFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.fragment_performance_chart) {

    private val binding by viewBinding(FragmentPerformanceChartBinding::bind)

    private val viewModel: PerformanceChartViewModel by viewModels { viewModelFactory }

    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { contentView }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.accountId = arguments?.getString("accountId")
    }

    @Suppress("LongMethod")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.marketsToggleView.onToggledListener = ::onMarketSelectionChanged
        binding.timeIntervalsToggleView.onToggledListener = ::onTimeIntervalSelectionChanged

        viewModel.marketNames.observe(viewLifecycleOwner) {
            binding.marketsToggleView.setTextItems(it, context?.dpToPx(PADDING_IN_PD) ?: 0)
        }

        viewModel.timeFrameNames.observe(viewLifecycleOwner) {
            binding.timeIntervalsToggleView.setTextItems(it, context?.dpToPx(PADDING_IN_PD) ?: 0)
        }

        viewModel.chartState.observe(viewLifecycleOwner) {
            onChartState(it)
        }

        viewModel.selectedMarkets.observe(viewLifecycleOwner) {
            it.forEachIndexed { id, selected -> binding.marketsToggleView.setToggled(id, selected) }
        }

        viewModel.selectedTimeInterval.observe(viewLifecycleOwner) {
            binding.timeIntervalsToggleView.setToggled(it, true)
        }
    }

    private fun onChartState(state: PerformanceChartViewModel.ChartState) {
        when (state) {
            is PerformanceChartViewModel.ChartState.Idle -> displayChart(state.lineData)
            is PerformanceChartViewModel.ChartState.Loading -> displayChartLoading(state.lineData)
            is PerformanceChartViewModel.ChartState.Error -> displayChartError(state.lineData)
        }
    }

    private fun onMarketSelectionChanged(toggle: Toggle, selected: Boolean) {
        viewModel.updateMarketSelection(toggle.id, selected)
    }

    private fun onTimeIntervalSelectionChanged(toggle: Toggle, @Suppress("UNUSED_PARAMETER") selected: Boolean) {
        viewModel.updateSelectedTimeInterval(toggle.id)
    }

    private fun displayChart(lineData: ChartLineData?) = view?.apply {
        binding.chartView.apply {
            updateLineData(lineData)
            visibility = View.VISIBLE
        }
        binding.chartLoadingView.visibility = View.GONE
    }

    private fun displayChartLoading(lineData: ChartLineData?) = view?.apply {
        if (lineData != null) {
            displayChart(lineData)
        } else {
            binding.chartView.visibility = View.GONE
        }
        binding.chartLoadingView.visibility = View.VISIBLE
    }

    private fun displayChartError(lineData: ChartLineData?) = view?.apply {
        displayChart(lineData)
        snackBarUtil.snackbar(getString(R.string.error_message_general), Snackbar.LENGTH_SHORT)?.show()
    }

    companion object {
        private const val PADDING_IN_PD = 8
    }
}
