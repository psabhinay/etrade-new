package com.etrade.mobilepro.order.details.router

import androidx.navigation.NavController

interface OrderDetailsRouter {
    fun openOrderDetails(navController: NavController, argument: OrderDetailsArgument)
}
