package com.etrade.mobilepro.devtemplate

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.markets.MarketsGenericLayout
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.data.response.MarketFutureResponse
import com.etrade.mobilepro.testutil.XmlDeserializer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

internal class DevTemplateViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private fun mockMoverService(marketMoverResponse: MarketFutureResponse = MarketFutureResponse()) = mock<MoverService> {
        on { getMarketFutures() } doReturn Single.just(marketMoverResponse)
    }

    @Test
    fun `fetches service upon creation`() {
        val mockDynamicService = mockMoverService()

        DevTemplateViewModel(mockDynamicService)

        verify(mockDynamicService).getMarketFutures()
    }

    @Test
    fun `returns error view state when service call is not successful`() {
        val mockObserver = mock<Observer<ViewModelInterface.ViewState>>()
        val mockMoverService = mock<MoverService> {
            on { getMarketFutures() } doReturn Single.error(RuntimeException("Something went wrong"))
        }

        val vm = DevTemplateViewModel(mockMoverService)
        vm.viewState.observeForever(mockObserver)
        verify(mockObserver).onChanged(eq(ViewModelInterface.ViewState.Error("")))
    }

    @Test
    fun `returns view state with empty service response on success`() {
        val mockObserver = mock<Observer<ViewModelInterface.ViewState>>()
        val marketMoverResponse = MarketFutureResponse()
        val mockMoverService = mockMoverService(marketMoverResponse)
        val vm = DevTemplateViewModel(mockMoverService)
        vm.viewState.observeForever(mockObserver)
        val listOfQuotes = mutableListOf<DynamicViewModel>()
        verify(mockObserver).onChanged(eq(ViewModelInterface.ViewState.Success(listOfQuotes)))
    }

    @Test
    fun `returns view state with service success`() {
        val mockObserver = mock<Observer<ViewModelInterface.ViewState>>()

        val marketFutureResponse = XmlDeserializer.getObjectFromXml("markets_future.xml", MarketFutureResponse::class.java)
        val mockMoverService = mockMoverService(marketFutureResponse)

        val vm = DevTemplateViewModel(mockMoverService)
        vm.viewState.observeForever(mockObserver)

        val layoutList = vm.viewState.value as ViewModelInterface.ViewState.Success
        val overviewItem = (layoutList.results!![2] as MarketsGenericLayout.Quote).overviewItem

        Assert.assertEquals(3, layoutList.results?.size)
        Assert.assertEquals("ES", overviewItem.symbol)
    }
}
