package com.etrade.mobilepro.chat.presentation.setup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.chat.api.ChatParamsDataModel
import com.etrade.mobilepro.chat.api.ChatParamsModel
import com.etrade.mobilepro.chat.api.ChatParamsRequestModel
import com.etrade.mobilepro.chat.api.setup.ChatCustomerMaps
import com.etrade.mobilepro.chat.api.setup.models.ChatProductItemModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatAccountModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatCustomerAccountModel
import com.etrade.mobilepro.chat.api.setup.repo.ChatSetupRepo
import com.etrade.mobilepro.chat.api.setup.repo.ChatSetupResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.tracking.SCREEN_TITLE_CHAT_WITH_US
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.util.formatters.formatAccountName
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val logger: Logger = LoggerFactory.getLogger(ChatSetupViewModel::class.java)

class ChatSetupViewModel @Inject constructor(
    private val chatSetupRepo: ChatSetupRepo,
    private val paramsModel: ChatParamsModel
) : ViewModel(), ScreenViewModel {

    override val screenName: String = SCREEN_TITLE_CHAT_WITH_US

    private val subjectsList = mutableListOf<ChatProductItemModel>()
    private val accountsList = mutableListOf<ChatCustomerAccountModel>()
    private val namesList = mutableListOf<String>()

    private val _viewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState>
        get() = _viewState

    var chatParamsRequest: ChatParamsRequestModel? = null
    private var mapAccountToUserNames = HashMap<String, List<String>>()
    private var mapAccounts = HashMap<String, ChatCustomerAccountModel>()
    private var mapUserNames = HashMap<String, ChatAccountModel>()
    private var chatMessage: String? = null

    internal val accountsData: LiveData<List<DropDownMultiSelector.Selectable<ChatCustomerAccountModel>>> = Transformations.map(viewState) { viewState ->
        accountsList.map {
            DropDownMultiSelector.Selectable(
                title = formatAccountName(it.instDescription, it.accNum),
                value = it,
                selected = it == viewState.selectedAccount
            )
        }
    }

    internal val subjectsData: LiveData<List<DropDownMultiSelector.Selectable<ChatProductItemModel>>> = Transformations.map(viewState) { viewState ->
        subjectsList.map {
            DropDownMultiSelector.Selectable(title = it.lookupName!!, value = it, selected = it == viewState.selectedSubject)
        }
    }

    internal val nameData: LiveData<List<DropDownMultiSelector.Selectable<String>>> = Transformations.map(viewState) { viewState ->
        namesList.map {
            DropDownMultiSelector.Selectable(title = it, value = it, selected = it == viewState.selectedName)
        }
    }

    internal fun messageTextChanged(content: String?) {
        chatMessage = content
        _viewState.updateValue { it.copy(content = content) }
    }

    internal fun accountSelected(account: ChatCustomerAccountModel) {
        namesList.clear()
        addNames(account.accNum)
        if (namesList.size == 1) {
            _viewState.updateValue { it.copy(selectedAccount = account, selectedName = namesList.first()) }
        } else {
            _viewState.updateValue { it.copy(selectedAccount = account) }
        }
    }

    private fun addNames(accNum: String) {
        mapAccountToUserNames[accNum]?.let { namesList.addAll(it) }
    }

    internal fun subjectSelected(subject: ChatProductItemModel) = _viewState.updateValue { it.copy(selectedSubject = subject) }

    internal fun nameSelected(name: String) = _viewState.updateValue { it.copy(selectedName = name) }

    private fun selectInitialAccount(): ChatCustomerAccountModel? =
        accountsList.takeIf { it.size == 1 }?.first()

    private fun selectInitialSubject(): ChatProductItemModel? =
        subjectsList.takeIf { it.size == 1 }?.first()

    data class ViewState(
        val status: Status = Status.Idle,
        val selectedAccount: ChatCustomerAccountModel? = null,
        val selectedSubject: ChatProductItemModel? = null,
        val selectedName: String? = null,
        val content: String? = null
    ) {
        sealed class Status {
            enum class Popup {
                UNAVAILABLE, HOLIDAY
            }

            open val popup: Popup? = null

            object GoToChatMessages : Status()

            data class Unavailable(private val isHoliday: Boolean = false) : Status() {
                override val popup: Popup? = if (isHoliday) Popup.HOLIDAY else Popup.UNAVAILABLE
            }

            object Idle : Status()

            object Loading : Status()

            data class Error(val message: String?) : Status() {
                override val popup: Popup? = Popup.UNAVAILABLE
            }
        }

        fun isAccountEnabled() = isIdle()

        fun isSubjectEnabled() = isIdle()

        fun isChatButtonEnabled() = isIdle() && selectedName != null && !content.isNullOrBlank()

        fun isNameEnabled() = isIdle() && selectedAccount != null

        private fun isIdle() = status == Status.Idle
    }

    fun refresh(forced: Boolean = false) {
        if (_viewState.value == null || forced) {
            _viewState.value = ViewState(status = ViewState.Status.Loading)
            viewModelScope.launch {
                chatSetupRepo.getChatActiveStatus()
                    .onSuccess {
                        loadChatSetup()
                    }.onFailure { throwable ->
                        logger.error("Fail to get chat active status", throwable)
                        _viewState.updateValue { it.copy(status = ViewState.Status.Error(throwable.message)) }
                    }
            }
        }
    }

    private fun updateViewState() {
        subjectsList.clear()
        accountsList.clear()
        namesList.clear()
        paramsModel.chatCustomerDetailsModel?.customerAccounts?.forEach { accountsList.add(it) }
        paramsModel.chatProductItems?.items?.forEach { subjectsList.add(it) }

        if (accountsList.isNullOrEmpty() || subjectsList.isNullOrEmpty()) {
            _viewState.value = ViewState(ViewState.Status.Error(null), selectedAccount = selectInitialAccount())
        } else {
            initMaps()
            if (accountsList.size == 1) {
                addNames(accountsList.first().accNum)
            }
            _viewState.value = ViewState(
                ViewState.Status.Idle, selectedSubject = selectInitialSubject(),
                selectedAccount = selectInitialAccount(),
                selectedName = selectInitialName()
            )
        }
    }

    private fun initMaps() {
        val maps = ChatCustomerMaps(accountsList)
        mapUserNames = maps.customerUserNameMap
        mapAccounts = maps.customerAccountsMap
        mapAccountToUserNames = maps.customerUserNameWithAccountsMap
    }

    fun onChatButtonClick() {
        _viewState.updateValue { it.copy(status = ViewState.Status.Loading) }

        viewModelScope.launch {
            // Agent Queue will become stale if user waits for more than 2 min on the Setup page before initiating Chat Message
            // getChatActiveStatus Call refreshes the server side Chat Agent Queue.

            chatSetupRepo.getChatActiveStatus().onSuccess {
                launchChatMessageWindow()
            }.onFailure { throwable ->
                logger.error("Fail to get chat active status", throwable)
                _viewState.updateValue { it.copy(status = ViewState.Status.Error(throwable.message)) }
            }
        }
    }

    private fun launchChatMessageWindow() {
        _viewState.value?.apply {
            val holder = mapUserNames[selectedName]
            chatParamsRequest = ChatParamsRequestModel(
                ChatParamsDataModel(
                    isOH = if (selectedAccount?.isOH == true) "true" else "false",
                    isBusinessHrs = "true",
                    isStockPlan = if ("Stock Plan" == selectedAccount?.instDescription) "true" else "false",
                    acctInstName = selectedAccount?.instName,
                    accountNo = selectedAccount?.accNum,
                    accountHolderTier = holder?.tier,
                    metadata = paramsModel.metadata,
                    productID = selectedSubject?.id,
                    cid = holder?.cid,
                    contactID = paramsModel.chatContactId,
                    firstName = holder?.firstname,
                    lastName = holder?.lastname,
                    chatInfo = chatMessage,
                    chatGeoLoc = paramsModel.chatCustomerDetailsModel?.userGeolocation
                )
            )
        }
        _viewState.updateValue { it.copy(status = ViewState.Status.GoToChatMessages) }
    }

    private fun selectInitialName() = namesList.takeIf { it.size == 1 }?.first()

    private fun loadChatSetup() {
        _viewState.updateValue { it.copy(status = ViewState.Status.Loading) }
        viewModelScope.launch {
            when (val result = chatSetupRepo.setupChat(paramsModel)) {
                is ChatSetupResult.Success -> updateViewState()
                is ChatSetupResult.NonOperatingHour -> {
                    _viewState.updateValue { it.copy(status = ViewState.Status.Unavailable(result.isHoliday)) }
                }
                is ChatSetupResult.Failure -> {
                    logger.error("chat api call failed", result.errorMessage)
                    _viewState.updateValue { it.copy(status = ViewState.Status.Error(result.errorMessage)) }
                }
            }
        }
    }
}
