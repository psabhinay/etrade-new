package com.etrade.mobilepro.mod

import com.etrade.mobilepro.util.json.JsonMoshi
import okhttp3.HttpUrl
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Url

interface ModServiceClient {
    @JsonMoshi
    @POST
    fun companyLookupPost(@Url httpUrl: HttpUrl): Call<CompanyOverviewDto>

    @JsonMoshi
    @GET
    fun getFundReports(@Url httpUrl: HttpUrl): Call<FundReportsResponseDto>
}
