package com.etrade.mobilepro.home

import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.OnAccountsClickListener
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ScrollOnTopListener
import com.etrade.mobilepro.common.viewpager.FragmentPageListener
import com.etrade.mobilepro.common.viewpager.setupFragmentPageListener
import com.etrade.mobilepro.dynamicui.api.CtaEvent.AccountsTabNavigation
import com.etrade.mobilepro.home.adapter.AccountsFragmentPagerAdapter
import com.etrade.mobilepro.stockplan.StockPlanWebViewFragment
import com.etrade.mobilepro.stockplan.databinding.StockplanPagerViewBinding
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setContentDescriptionForTabs
import com.etrade.mobilepro.util.android.extension.setupInnerClickListenersForAccessibility
import com.etrade.mobilepro.util.android.recyclerviewutil.enforceSingleScrollDirection
import com.etrade.mobilepro.util.android.viewpager2.recyclerView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import javax.inject.Inject

const val TOOLTIP_SHOW_DELAY_MS = 300L

@RequireLogin
class AccountsTabHostFragment @Inject constructor(
    private val userViewModel: UserViewModel,
    private val selectedTabListener: AccountsSelectedTabListener,
    private val viewModelFactory: ViewModelProvider.Factory
) : Fragment(R.layout.stockplan_pager_view), NavigateTabs, StockPlanWebViewFragment.ActionListener, OnAccountsClickListener {
    private val binding: StockplanPagerViewBinding by viewBinding(StockplanPagerViewBinding::bind) { onDestroyBinding() }

    private fun onDestroyBinding() {
        handler.removeCallbacksAndMessages(null)
        tabLayoutMediator?.detach()
        tabLayoutMediator = null
        binding.stockPlanViewPager.adapter = null
        fragmentPageListener = null
        stockPlanTabLayout = null
    }

    private val viewModel: AccountsTabHostViewModel by viewModels { viewModelFactory }
    private val args: AccountsTabHostFragmentArgs by navArgs()

    private var stockPlanTabLayout: TabLayout? = null

    private var fragmentPageListener: FragmentPageListener? = null

    private val handler = Handler(Looper.getMainLooper())
    private val pagerAdapter: AccountsFragmentPagerAdapter
        get() = binding.stockPlanViewPager.adapter as AccountsFragmentPagerAdapter
    private var tabLayoutMediator: TabLayoutMediator? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findViews(view)
        handleFragmentArgs(savedInstanceState)
    }

    override fun onPause() {
        super.onPause()

        stockPlanTabLayout?.let {
            viewModel.storeSelectedTabIndex(it.selectedTabPosition)
        }
    }

    private fun handleFragmentArgs(savedInstanceState: Bundle?) = args.accountUuid?.let { accountUuid ->
        val accountViewType = userViewModel.getAccountType(accountUuid)

        if (accountViewType != null) {
            userViewModel.updateSelectedAccount(accountUuid)
            createAccountListByAccountId(accountViewType)?.let { accountList ->
                binding.stockPlanViewPager.adapter = AccountsFragmentPagerAdapter(this, accountList, arguments)
                binding.stockPlanViewPager.offscreenPageLimit = 1
                tabLayoutMediator = TabLayoutMediator(requireNotNull(stockPlanTabLayout), requireNotNull(binding.stockPlanViewPager)) { tab, position ->
                    tab.text = pagerAdapter.getPageTitle(position)
                }
                tabLayoutMediator?.attach()
                fragmentPageListener = binding.stockPlanViewPager.setupFragmentPageListener(this, this::onPageSelected)
                binding.stockPlanViewPager.recyclerView?.enforceSingleScrollDirection()
                stockPlanTabLayout?.setContentDescriptionForTabs()
                stockPlanTabLayout?.setupInnerClickListenersForAccessibility()
                initTabLayoutMode()
            }

            if (savedInstanceState == null) {
                navigateToArgTab()
            }
        }
    }

    private fun findViews(view: View) {
        stockPlanTabLayout = view.findViewById(R.id.tabLayout)
    }

    private fun navigateToArgTab() {
        val accountId = args.accountId
        val tabIndex = tabIndex(args.tabTitle)
        stockPlanTabLayout?.setContentDescriptionForTabs(forceSelectedIndex = tabIndex)
        viewModel.shouldUpdateTabContentDescriptions = false // we need to skip one time or onPageChangeListener will rewrite correct contentDescriptions
        if (accountId != null && tabIndex > 0) {
            navigateToTab(AccountsTabNavigation(accountId, args.tabTitle, args.tabArgs))
        } else {
            onPageSelected(0)
        }
    }

    private fun initTabLayoutMode() {
        val currentOrientation = context?.resources?.configuration?.orientation
        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            stockPlanTabLayout?.tabMode = TabLayout.MODE_FIXED
        } else {
            stockPlanTabLayout?.tabMode = TabLayout.MODE_SCROLLABLE
        }
    }

    override fun navigateToTab(destination: AccountsTabNavigation) {
        binding.stockPlanViewPager.setCurrentItem(tabIndex(destination.tabTitle), false)
        (fragmentPageListener?.selectedFragment as? AccountTabNavigationListener)?.onAccountTabNavigated(destination.tabArgs)
    }

    override fun onTradeButtonClicked() {
        binding.stockPlanViewPager.post {
            binding.stockPlanViewPager.setCurrentItem(pagerAdapter.getIndexOfAccountTab(AccountTab.Exercise), true)
        }
    }

    override fun onAccountsClick() {
        when (val currentFragment = fragmentPageListener?.selectedFragment) {
            is OnAccountsClickListener -> currentFragment.onAccountsClick()
            is ScrollOnTopListener -> {
                if (currentFragment.isOnTop) {
                    navigateToTab(AccountsTabNavigation(args.accountId ?: "", null, args.tabArgs))
                } else {
                    currentFragment.scrollUp()
                }
            }
        }
    }

    private fun onPageSelected(position: Int) {
        pagerAdapter.getPageTitle(position).let {
            viewModel.trackTab(position, it)
        }
        selectedTabListener.selectedTabIndex = position
        if (viewModel.shouldUpdateTabContentDescriptions) {
            stockPlanTabLayout?.setContentDescriptionForTabs()
        } else {
            viewModel.shouldUpdateTabContentDescriptions = true
        }

        (fragmentPageListener?.selectedFragment as? OnAccountsTabSelectionListener)?.onTabSelected()
    }

    private fun tabIndex(tabTitle: String?) = pagerAdapter.getIndexOf(tabTitle)
}
