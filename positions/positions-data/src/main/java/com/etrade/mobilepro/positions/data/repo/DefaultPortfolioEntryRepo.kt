package com.etrade.mobilepro.positions.data.repo

import com.etrade.mobilepro.positions.api.PortfolioEntry
import com.etrade.mobilepro.positions.api.PortfolioEntryRepo
import com.etrade.mobilepro.positions.data.mapper.PortfolioEntryJsonMapper
import com.etrade.mobilepro.positions.data.rest.PortfolioApiClient
import com.etrade.mobilepro.positions.data.rest.createPortfolioEntriesRequestBody
import io.reactivex.Single

class DefaultPortfolioEntryRepo(private val portfolioApiClient: PortfolioApiClient) : PortfolioEntryRepo {

    private val mapper = PortfolioEntryJsonMapper()

    override fun getPortfolioEntry(userId: String, accountId: String, extendedHourFlag: Boolean): Single<PortfolioEntry> {
        val request = createPortfolioEntriesRequestBody(userId, accountId, extendedHourFlag)
        return portfolioApiClient
            .getPortfolioEntries(request)
            .toObservable()
            .map(mapper::map)
            .firstOrError()
    }
}
