package com.etrade.mobilepro.pushnotification.api

data class AlertPushSettings(
    val stockAlertsEnabled: Boolean,
    val accountAlertsEnabled: Boolean
)
