package com.etrade.mobilepro.billpay.presentation

import android.os.Parcel
import com.etrade.mobilepro.billpay.api.PayeeAddress
import com.etrade.mobilepro.billpay.presentation.common.PayeeAddressParceler
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class PayeeAddressParcelerTest {

    @Test
    fun `data A can be serialized and deserialized`() {
        testData(dataA)
    }

    @Test
    fun `data B can be serialized and deserialized`() {
        testData(dataB)
    }

    private fun testData(data: PayeeAddress) {
        val parcel = Parcel.obtain()
        PayeeAddressParceler.run { data.write(parcel, 0) }
        parcel.setDataPosition(0)
        val result = PayeeAddressParceler.create(parcel)
        assertEquals(data, result)
    }

    private val dataA = PayeeAddress(
        address1 = "a1",
        address2 = "a2",
        city = "a3",
        state = "a4",
        zipCode = "a5"
    )

    private val dataB = PayeeAddress(
        address1 = null,
        address2 = null,
        city = "a3",
        state = null,
        zipCode = "a5"
    )
}
