package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class SaveOrderResponseDto(
    @Json(name = "PreparedOrder")
    val response: SaveOrderPreparedOrderDto?
) : BaseDataDto()
