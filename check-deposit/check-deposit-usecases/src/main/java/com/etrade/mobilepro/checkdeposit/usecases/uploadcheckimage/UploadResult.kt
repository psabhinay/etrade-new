package com.etrade.mobilepro.checkdeposit.usecases.uploadcheckimage

import com.etrade.mobilepro.checkdeposit.usecases.api.ImageUploadResult

internal data class UploadResult(override val tokenId: String) : ImageUploadResult
