package com.etrade.mobilepro.chart.data;

import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u000b\b\u0007\u0018\u00002\u00020\u0001B9\u0012\u000e\b\u0001\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000e\b\u0001\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0004\u0012\b\b\u0003\u0010\u0007\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\bR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\r\u00a8\u0006\u000f"}, d2 = {"Lcom/etrade/mobilepro/chart/data/ChartRequestValueDto;", "", "accounts", "", "", "indexes", "period", "optimized", "(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V", "getAccounts", "()Ljava/util/List;", "getIndexes", "getOptimized", "()Ljava/lang/String;", "getPeriod", "chartdata"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ChartRequestValueDto {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.String> accounts = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.String> indexes = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String period = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String optimized = null;
    
    public ChartRequestValueDto(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "accountList")
    java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "indexList")
    java.util.List<java.lang.String> indexes, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "period")
    java.lang.String period, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optimized")
    java.lang.String optimized) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getAccounts() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getIndexes() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPeriod() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOptimized() {
        return null;
    }
}