package com.etrade.mobilepro.home.overview.edit

import android.content.res.Resources
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamicui.DynamicViewType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Qualifier

private const val ENABLED_WIDGETS_STORAGE_KEY = "ENABLED_WIDGETS_STORAGE_KEY"
private const val SAVED_WIDGETS_STORAGE_KEY = "SAVED_WIDGETS_STORAGE_KEY"
private const val WIDGETS_SEPARATOR = ","

interface OverviewWidgetRepo {

    suspend fun getWidgets(): List<OverviewWidget>

    fun getWidgetsFlow(): Flow<List<OverviewWidget>>

    suspend fun saveWidgets(items: List<OverviewWidget>)
}

@Qualifier
annotation class Overview

class OverviewWidgetRepoImpl @Inject constructor(
    storage: KeyValueStorage,
    resources: Resources,
    override val enabledWidgetsStorageKey: String = ENABLED_WIDGETS_STORAGE_KEY,
    override val savedWidgetsStorageKey: String = SAVED_WIDGETS_STORAGE_KEY
) : BaseOverviewWidgetRepo(storage) {

    override val allWidgets: Flow<Map<String, OverviewWidget>> = flowOf(
        mapOf(
            DynamicViewType.ACCOUNT_SUMMARY.dtoName to OverviewWidget(
                DynamicViewType.ACCOUNT_SUMMARY.dtoName,
                resources.getString(R.string.account_summary)
            ),
            DynamicViewType.MARKET_INDICES.dtoName to OverviewWidget(
                DynamicViewType.MARKET_INDICES.dtoName,
                resources.getString(R.string.markets_overview)
            ),
            DynamicViewType.MARKET_CHART.dtoName to OverviewWidget(
                DynamicViewType.MARKET_CHART.dtoName,
                resources.getString(R.string.market_chart)
            ),
            DynamicViewType.NEWS_PORTFOLIO.dtoName to OverviewWidget(
                DynamicViewType.NEWS_PORTFOLIO.dtoName,
                resources.getString(R.string.portfolio_news)
            ),
            DynamicViewType.PORTFOLIO_MOVERS.dtoName to OverviewWidget(
                DynamicViewType.PORTFOLIO_MOVERS.dtoName,
                resources.getString(R.string.portfolio_movers)
            ),
            DynamicViewType.NEWS_BRIEFING.dtoName to OverviewWidget(
                DynamicViewType.NEWS_BRIEFING.dtoName,
                resources.getString(R.string.briefing_news)
            ),
            DynamicViewType.MARKET_MOVERS.dtoName to OverviewWidget(
                DynamicViewType.MARKET_MOVERS.dtoName,
                resources.getString(R.string.market_movers)
            )
        )
    )

    override val defaultWidgetsOrder: List<String> = listOf(
        DynamicViewType.ACCOUNT_SUMMARY.dtoName,
        DynamicViewType.MARKET_INDICES.dtoName,
        DynamicViewType.MARKET_CHART.dtoName,
        DynamicViewType.NEWS_PORTFOLIO.dtoName,
        DynamicViewType.PORTFOLIO_MOVERS.dtoName,
        DynamicViewType.NEWS_BRIEFING.dtoName,
        DynamicViewType.MARKET_MOVERS.dtoName
    )
}

abstract class BaseOverviewWidgetRepo(
    private val storage: KeyValueStorage
) : OverviewWidgetRepo {

    protected val logger: Logger by lazy { LoggerFactory.getLogger(javaClass) }

    abstract val enabledWidgetsStorageKey: String

    abstract val savedWidgetsStorageKey: String

    protected abstract val allWidgets: Flow<Map<String, OverviewWidget>>

    protected abstract val defaultWidgetsOrder: List<String>

    private var enabledWidgets: List<String>
        get() = storage.getStringValue(enabledWidgetsStorageKey, null)?.split(WIDGETS_SEPARATOR) ?: defaultWidgetsOrder
        set(value) {
            storage.putStringValue(enabledWidgetsStorageKey, value.joinToString(WIDGETS_SEPARATOR))
        }

    private var savedWidgets: List<String>
        get() = storage.getStringValue(savedWidgetsStorageKey, null)?.split(WIDGETS_SEPARATOR) ?: defaultWidgetsOrder
        set(value) {
            storage.putStringValue(savedWidgetsStorageKey, value.joinToString(WIDGETS_SEPARATOR))
        }

    override suspend fun saveWidgets(items: List<OverviewWidget>) = withContext(Dispatchers.IO) {
        enabledWidgets = items.asSequence().filter { it.isChecked }.map { it.widgetName }.toList()
        savedWidgets = items.map { it.widgetName }
    }

    override suspend fun getWidgets(): List<OverviewWidget> = getWidgetsFlow().first()

    override fun getWidgetsFlow(): Flow<List<OverviewWidget>> =
        allWidgets.map { availableWidgets ->
            val enabled = enabledWidgets
            val items = savedWidgets

            items.mapNotNull {
                availableWidgets[it]?.let { item ->
                    OverviewWidget(item.widgetName, item.displayText, enabled.contains(item.widgetName))
                }
            }
        }.flowOn(Dispatchers.IO)
}
