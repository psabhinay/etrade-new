package com.etrade.mobilepro.bank.databinding;
import com.etrade.mobilepro.bank.R;
import com.etrade.mobilepro.bank.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class BankActionsBindingImpl extends BankActionsBinding implements com.etrade.mobilepro.bank.generated.callback.OnClickListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.view, 4);
        sViewsWithIds.put(R.id.view2, 5);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback5;
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    @Nullable
    private final android.view.View.OnClickListener mCallback4;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public BankActionsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 6, sIncludes, sViewsWithIds));
    }
    private BankActionsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[1]
            , (android.view.View) bindings[4]
            , (android.view.View) bindings[5]
            );
        this.bankDeposit.setTag(null);
        this.bankPayBill.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.transferMoney.setTag(null);
        setRootTag(root);
        // listeners
        mCallback5 = new com.etrade.mobilepro.bank.generated.callback.OnClickListener(this, 3);
        mCallback3 = new com.etrade.mobilepro.bank.generated.callback.OnClickListener(this, 1);
        mCallback4 = new com.etrade.mobilepro.bank.generated.callback.OnClickListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.obj == variableId) {
            setObj((com.etrade.mobilepro.bank.actions.BankActionsViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setObj(@Nullable com.etrade.mobilepro.bank.actions.BankActionsViewModel Obj) {
        this.mObj = Obj;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.obj);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.etrade.mobilepro.bank.actions.BankActionsViewModel obj = mObj;
        // batch finished
        if ((dirtyFlags & 0x2L) != 0) {
            // api target 1

            this.bankDeposit.setOnClickListener(mCallback4);
            this.bankPayBill.setOnClickListener(mCallback5);
            this.transferMoney.setOnClickListener(mCallback3);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 3: {
                // localize variables for thread safety
                // obj != null
                boolean objJavaLangObjectNull = false;
                // obj
                com.etrade.mobilepro.bank.actions.BankActionsViewModel obj = mObj;



                objJavaLangObjectNull = (obj) != (null);
                if (objJavaLangObjectNull) {


                    obj.onPayBillClicked();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // obj != null
                boolean objJavaLangObjectNull = false;
                // obj
                com.etrade.mobilepro.bank.actions.BankActionsViewModel obj = mObj;



                objJavaLangObjectNull = (obj) != (null);
                if (objJavaLangObjectNull) {


                    obj.onTransferMoneyClicked();
                }
                break;
            }
            case 2: {
                // localize variables for thread safety
                // obj != null
                boolean objJavaLangObjectNull = false;
                // obj
                com.etrade.mobilepro.bank.actions.BankActionsViewModel obj = mObj;



                objJavaLangObjectNull = (obj) != (null);
                if (objJavaLangObjectNull) {


                    obj.onCheckDepositClicked();
                }
                break;
            }
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): obj
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}