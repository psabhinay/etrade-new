package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.common.di.AppsFlyer
import com.etrade.mobilepro.eventtracker.AppsFlyerHelper
import com.etrade.mobilepro.eventtracker.AppsFlyerRepoImpl
import com.etrade.mobilepro.eventtrackerapi.AppsFlyerRepo
import com.etrade.mobilepro.eventtrackerapi.AppsFlyerRequestApi
import com.etrade.mobilepro.eventtrackerapi.EventTrackerInterface
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
abstract class EventTrackerModule {

    @Binds
    @Singleton
    abstract fun bindAppsFlyerRepo(appsFlyerRepoImpl: AppsFlyerRepoImpl): AppsFlyerRepo

    companion object {
        @Provides
        @Singleton
        fun providesAppsFlyerHelper(): EventTrackerInterface = AppsFlyerHelper()

        @Provides
        @Singleton
        internal fun provideAppsFlyerRequestApi(@AppsFlyer retrofit: Retrofit): AppsFlyerRequestApi =
            retrofit.create(AppsFlyerRequestApi::class.java)
    }
}
