package com.etrade.mobilepro.order.details.dto;

import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B#\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b\u00a8\u0006\u0015"}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/OrderDeleteRequestValueDto;", "", "accountId", "", "preparedId", "origSysCodeReq", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccountId", "()Ljava/lang/String;", "getOrigSysCodeReq", "getPreparedId", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "orders-details-data"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class OrderDeleteRequestValueDto {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String accountId = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String preparedId = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String origSysCodeReq = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.OrderDeleteRequestValueDto copy(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "accountId")
    java.lang.String accountId, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "preparedId")
    java.lang.String preparedId, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "origSysCodeReq")
    java.lang.String origSysCodeReq) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public OrderDeleteRequestValueDto(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "accountId")
    java.lang.String accountId, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "preparedId")
    java.lang.String preparedId, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "origSysCodeReq")
    java.lang.String origSysCodeReq) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAccountId() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPreparedId() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOrigSysCodeReq() {
        return null;
    }
}