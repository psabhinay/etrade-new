package com.etrade.mobilepro.earnings

import android.content.res.Configuration
import android.view.MotionEvent
import android.view.View
import androidx.compose.runtime.Composable
import androidx.compose.ui.viewinterop.AndroidViewBinding
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.etrade.mobilepro.barchartview.BarChartView
import com.etrade.mobilepro.earnings.databinding.FragmentEarningsDetailsBinding
import com.etrade.mobilepro.earnings.viewModel.EarningsDetailsViewModel
import com.etrade.mobilepro.earnings.viewModel.GraphData
import com.etrade.mobilepro.earnings.viewModel.MarketEarnings
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quote.screen.api.TabVisibilityListener
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.addToCalendar
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.LifecycleObserverViewDelegate
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.BarLineChartTouchListener
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.google.android.material.snackbar.Snackbar
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import java.text.DecimalFormat

const val DEFAULT_VISIBLE_X_RANGE_MAXIMUM = 8F
val Y_AXIS_DECIMAL_FORMAT = DecimalFormat("#.###")

class EarningsDetailsViewDelegate @AssistedInject constructor(
    @Assisted private val fragment: Fragment,
    @Assisted private val symbol: SearchResultItem.Symbol,
    snackBarUtilFactory: SnackbarUtilFactory,
    viewModelFactory: ViewModelProvider.Factory
) : TabVisibilityListener {

    private val ticker: String = symbol.symbol
    private var snackBar: Snackbar? = null
    private val snackBarUtil by lazy { snackBarUtilFactory.createSnackbarUtil({ fragment.viewLifecycleOwner }, { fragment.view }) }
    private val quotesWidgetViewModel: QuotesWidgetViewModel by fragment.viewModels { viewModelFactory }
    private val viewModel: EarningsDetailsViewModel by fragment.viewModels { viewModelFactory }
    private var binding: FragmentEarningsDetailsBinding? = null
    private val graphDataObserver = Observer<List<GraphData>> { list ->
        binding?.barChart()?.apply {
            clear()
            add(list)

            if (isHighlightPerTapEnabled) {
                highlightLastHistoryQuarter()
            }

            contentDescription = getChartContentDescription(list)
        }
    }
    private val isInLandscapeMode: Boolean
        get() = fragment.resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE

    private val lifeCycleObserverViewDelegate by lazy { LifecycleObserverViewDelegate(listOf(quotesWidgetViewModel)) }

    @AssistedFactory
    interface Factory {
        fun create(
            fragment: Fragment,
            symbol: SearchResultItem.Symbol
        ): EarningsDetailsViewDelegate
    }

    @Composable
    fun EarningsDetailsViewCompose() {
        AndroidViewBinding(FragmentEarningsDetailsBinding::inflate) {
            if (binding != this) {
                binding = this
                lifecycleOwner = fragment.viewLifecycleOwner

                quotesWidgetViewModel = this@EarningsDetailsViewDelegate.quotesWidgetViewModel
                viewModel = this@EarningsDetailsViewDelegate.viewModel

                viewEarningsMarket?.addToCalendar?.setOnClickListener {
                    addEventToCalendar()
                }
                swipeRefresh?.setup()
                graphContainer.barChart.setup()

                lifeCycleObserverViewDelegate.observe(fragment.viewLifecycleOwner)
                setUpViewModel()
            }
        }
    }

    private fun SwipeRefreshLayout.setup() {
        applyColorScheme()
        setOnRefreshListener {
            loadDetails(ticker)
            snackBar?.dismiss()
        }
    }

    private fun BarChartView.setup() {
        defaultVisibleXRangeMaximum = DEFAULT_VISIBLE_X_RANGE_MAXIMUM
        highLightColor = ContextCompat.getColor(context, R.color.plum)
        xAxisValueFormatter = { index -> viewModel.getGraphDataItem(index.toInt())?.xFormatted ?: "" }
        yAxisValueFormatter = { value -> Y_AXIS_DECIMAL_FORMAT.format(value) }
        barValueFormatter = { value -> if (value == 0F) "" else value.toString() }
        barValueDescriptionFormatter = { index, _ -> viewModel.getGraphDataItem(index)?.valueDescriptionFormatted ?: "" }

        setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onNothingSelected() {
                // not used
            }

            override fun onValueSelected(e: Entry?, h: Highlight?) {
                e?.x?.toInt()?.run {
                    highlightValuesOnBarChart(this)
                    viewModel.selectedGraphBarIndex.value = this
                }
            }
        })
        onTouchListener = object : BarLineChartTouchListener(this, viewPortHandler.matrixTouch, 0F) {
            override fun performHighlight(h: Highlight?, e: MotionEvent?) {
                setLastHighlighted(null)

                super.performHighlight(h, e)
            }
        }
    }

    private fun getChartContentDescription(list: List<GraphData>): String {
        if (list.isEmpty() || list.isMissingQuarterInfo()) {
            return fragment.getString(R.string.no_chart_data_available)
        }
        val historyChart = list.toBarChartDescription(prefixText = fragment.getString(R.string.history), isForecast = false)
        val forecastChart = list.toBarChartDescription(prefixText = fragment.getString(R.string.forecast))

        return historyChart + "\n" + forecastChart
    }

    private fun List<GraphData>.toBarChartDescription(prefixText: String, isForecast: Boolean = true): String {
        return prefixText + "\n" + (
            this.filter { it.isFuturePeriod == isForecast && it.earningsDetailData.quarter != null }
                .map {
                    fragment.getString(
                        R.string.earnings_chart_bar_description,
                        it.earningsDetailData.quarter,
                        it.earningsDetailData.fiscalPeriodYear,
                        it.y.toString(),
                        it.valueDescriptionFormatted
                    )
                }.takeIf { it.isNotEmpty() }?.reduce { acc, s -> "$acc $s" } ?: ""
            )
    }

    @Suppress("LongMethod")
    private fun setUpViewModel() {
        setUpQuoteDetails()
        setUpMarketEarningsDetails()

        viewModel.marketEarningsAnnual.observe(fragment.viewLifecycleOwner) {
            when (it) {
                is Resource.Failed -> handleFailed()
            }
        }

        viewModel.selectedGraphIndex.observe(fragment.viewLifecycleOwner) {
            handleSelectedGraph(it)
        }

        viewModel.selectedGraphData.observe(fragment.viewLifecycleOwner, graphDataObserver)
        viewModel.loadingStatus.observe(fragment.viewLifecycleOwner) {
            binding?.progress?.visibility = if (it && binding?.swipeRefresh?.isRefreshing != true) {
                binding?.graphContainer?.barChart?.visibility = View.GONE
                View.VISIBLE
            } else {
                binding?.graphContainer?.barChart?.visibility = View.VISIBLE
                View.GONE
            }
        }
    }

    private fun handleSelectedGraph(it: Int?) {
        when (it) {
            0 -> {
                if (isInLandscapeMode) {
                    binding?.graphContainer?.barChart?.isHighlightPerTapEnabled = true
                    binding?.earningsDetailsContainer?.visibility = View.VISIBLE
                }
            }
            1 -> {
                binding?.graphContainer?.barChart?.isHighlightPerTapEnabled = false
            }
        }
    }

    private fun setUpMarketEarningsDetails() {
        viewModel.marketEarningsQuarterly.observe(
            fragment.viewLifecycleOwner,
            {
                when (it) {
                    is Resource.Success -> handleSuccessMarketEarnings(it.data)
                    is Resource.Failed -> {
                        handleFailed()
                        handleFailedMarketEarnings(it.data)
                    }
                }
            }
        )
        viewModel.selectedQuarterEarningsDetails.observe(
            fragment.viewLifecycleOwner,
            {
                binding?.earningDetails = it
            }
        )
    }

    private fun handleFailedMarketEarnings(data: MarketEarnings?) {
        binding?.marketEarningsContent?.visibility = View.VISIBLE
        binding?.graphContainer?.barChart?.visibility = View.VISIBLE
        binding?.marketItem = data
    }

    private fun setUpQuoteDetails() {
        quotesWidgetViewModel.quote.observe(
            fragment.viewLifecycleOwner,
            {
                viewModel.symbolTitle = it?.title
            }
        )
    }

    private fun handleSuccessMarketEarnings(item: MarketEarnings?) {
        binding?.marketEarningsContent?.visibility = View.VISIBLE
        handleSuccess()

        binding?.marketItem = item
    }

    private fun handleSuccess() {
        binding?.swipeRefresh?.isRefreshing = false
    }

    private fun handleFailed() {
        binding?.swipeRefresh?.isRefreshing = false

        if (snackBar?.isShown == true) {
            return
        }

        snackBar = snackBarUtil.retrySnackbar {
            loadDetails(ticker)
            snackBar?.dismiss()
        }
    }

    private fun addEventToCalendar() {
        viewModel.getCalendarEvent()?.run { fragment.context?.also { addToCalendar(it, this) } }
    }

    private fun highlightValuesOnBarChart(index: Int) {
        index
            .let { viewModel.getHighlightItems(it) }
            ?.also { binding?.barChart()?.highlightValues(it) }
    }

    private fun highlightLastHistoryQuarter() {
        viewModel
            .getLastHistoryQuarterIndex()
            ?.also { viewModel.selectedGraphBarIndex.value = it }
            ?.let { viewModel.getHighlightItems(it) }
            ?.also { binding?.barChart()?.highlightValues(it) }
    }

    private fun loadDetails(symbol: String) {
        viewModel.loadGraphDetails(symbol, isInLandscapeMode)
    }

    private fun FragmentEarningsDetailsBinding?.barChart(): BarChartView? = this?.graphContainer?.barChart

    override fun onVisibilityChange(isVisible: Boolean) {
        if (isVisible) {
            if (binding?.marketEarningsContent?.visibility != View.VISIBLE) {
                loadDetails(ticker)
            }
        } else {
            snackBar?.dismiss()
        }
    }
}
