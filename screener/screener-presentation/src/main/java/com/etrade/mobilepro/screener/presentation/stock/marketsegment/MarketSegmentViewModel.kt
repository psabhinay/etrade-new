package com.etrade.mobilepro.screener.presentation.stock.marketsegment

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.screener.api.Identifiable
import com.etrade.mobilepro.screener.api.Industry
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.Sector
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.datasource.SectorDataSource
import com.etrade.mobilepro.screener.api.filter.stock.MarketSegmentFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Stock
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import com.etrade.mobilepro.util.android.SingleLiveEvent
import javax.inject.Inject

internal const val BOTTOM_SECTOR_INDUSTRY_SELECTOR_TAG = "sector_industry"

class MarketSegmentViewModel @Inject constructor(
    private val sectorDataSource: SectorDataSource,
    aggregator: ScreenerFilterAggregator,
    @Stock screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<MarketSegmentFilter, MarketSegmentViewModel.ViewState>(
    aggregator,
    ScreenerType.Stock.MarketSegment,
    screenerInteractor
) {

    private val _viewEffects = SingleLiveEvent<ViewEffects>()
    internal val viewEffects: LiveData<ViewEffects> = _viewEffects
    private val selectedSectorPosition: Int
        get() = viewState.value?.filter?.selectedSectorPosition ?: 0
    private val selectedSectorId: String
        get() = viewState.value?.filter?.sector?.id ?: ""
    private val selectedIndustryPosition: Int
        get() = viewState.value?.filter?.selectedIndustryPosition ?: 0
    internal var lastBottomSheetData: ViewEffects.BottomSelectorWithData? = null

    override fun provideInitialState(initialFilter: MarketSegmentFilter): ViewState =
        ViewState(
            filter = initialFilter
        )

    internal fun chooseSectorTapped() {
        lastBottomSheetData = ViewEffects.BottomSelectorWithData(
            titleRes = R.string.sector,
            items = sectorDataSource.getSectors(),
            initialSelectedPosition = selectedSectorPosition
        )
        _viewEffects.value = lastBottomSheetData
    }

    internal fun chooseIndustryTapped() {
        lastBottomSheetData = ViewEffects.BottomSelectorWithData(
            titleRes = R.string.industry,
            items = sectorDataSource.getIndustriesBySectorId(selectedSectorId),
            initialSelectedPosition = selectedIndustryPosition
        )
        _viewEffects.value = lastBottomSheetData
    }

    internal fun dropDownItemSelected(position: Int, item: Identifiable) {
        when (item) {
            is Sector -> sectorSelected(position, item)
            is Industry -> industrySelected(position, item)
        }
    }

    @Suppress("ComplexMethod") // nothing special
    internal fun capToggled(cap: Cap, isChecked: Boolean) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    microCap = if (cap == Cap.MICRO) { isChecked } else { it.filter.microCap },
                    smallCap = if (cap == Cap.SMALL) { isChecked } else { it.filter.smallCap },
                    midCap = if (cap == Cap.MID) { isChecked } else { it.filter.midCap },
                    largeCap = if (cap == Cap.LARGE) { isChecked } else { it.filter.largeCap },
                    megaCap = if (cap == Cap.MEGA) { isChecked } else { it.filter.megaCap }
                )
            )
        }
    }

    private fun sectorSelected(position: Int, sector: Sector) {
        updateState {
            ViewState(
                filter = it.filter.copy(
                    sector = sector,
                    industry = sectorDataSource.getAllIndustriesItem(),
                    selectedSectorPosition = position,
                    selectedIndustryPosition = 0
                )
            )
        }
    }

    private fun industrySelected(position: Int, industry: Industry) {
        updateState {
            ViewState(
                filter = it.filter.copy(
                    industry = industry,
                    selectedIndustryPosition = position
                )
            )
        }
    }

    // Filter screens have only "Success" state, as handling network calls and errors is the container's responsibility
    data class ViewState(
        override val filter: MarketSegmentFilter
    ) : BaseFilterScreenViewModel.ViewState<MarketSegmentFilter>

    internal sealed class ViewEffects {
        class BottomSelectorWithData(
            val tag: String = BOTTOM_SECTOR_INDUSTRY_SELECTOR_TAG,
            @StringRes
            val titleRes: Int = R.string.sector,
            val items: List<Identifiable> = emptyList(),
            val initialSelectedPosition: Int = 0,
            val dividerAt: Int = 0
        ) : ViewEffects()
    }

    internal enum class Cap {
        MICRO, SMALL, MID, LARGE, MEGA
    }
}
