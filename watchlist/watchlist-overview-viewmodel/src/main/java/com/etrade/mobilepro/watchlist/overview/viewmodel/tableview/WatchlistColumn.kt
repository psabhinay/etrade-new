package com.etrade.mobilepro.watchlist.overview.viewmodel.tableview

import androidx.annotation.StringRes
import com.etrade.mobilepro.watchlist.overview.viewmodel.R
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnInterface

// Representation of the columns in watchlist
sealed class WatchlistColumn(
    @StringRes override val textRes: Int,
    @StringRes override val reorderTextRes: Int = textRes,
    @StringRes override val contentDescriptionRes: Int = reorderTextRes
) : WatchlistColumnInterface {
    object LastPrice : WatchlistColumn(R.string.last, R.string.last_price)
    object ChangeDollar : WatchlistColumn(R.string.change_dollar, R.string.days_change_dollar, R.string.label_days_change_dollar)
    object ChangePercent : WatchlistColumn(R.string.change_percent, R.string.days_change_percent, R.string.label_days_change_percent)
    object Volume : WatchlistColumn(R.string.volume)
    object Bid : WatchlistColumn(R.string.bid)
    object Ask : WatchlistColumn(R.string.ask)
    object DayHigh : WatchlistColumn(R.string.day_high_column)
    object DayLow : WatchlistColumn(R.string.day_low_column)
    object FiftyTwoWeekHigh : WatchlistColumn(R.string.watchlist_fifty_two_wk_high, R.string.fifty_two_week_high)
    object FiftyTwoWeekLow : WatchlistColumn(R.string.watchlist_fifty_two_wk_low, R.string.fifty_two_week_low)
    object PriceEarningRatio : WatchlistColumn(R.string.p_e, contentDescriptionRes = R.string.p_e_description)
    object EarningsPerShare : WatchlistColumn(R.string.eps, contentDescriptionRes = R.string.eps_description)
}
