package com.etrade.mobilepro.landing.presentation.adapter

import com.etrade.mobilepro.landing.api.LandingPage
import com.etrade.mobilepro.landing.presentation.LandingPageSelectionViewModel

internal interface LandingPageSelectionItem
@JvmInline
internal value class LandingPageItem(val value: LandingPage) : LandingPageSelectionItem

@JvmInline
internal value class LandingPageHeader(val status: LandingPageSelectionViewModel.ViewState.Status) : LandingPageSelectionItem
