package com.etrade.mobilepro.dynamic.form.viewmodel

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.util.UseCase

typealias CreateFormUseCase = UseCase<CreateFormUseCaseParameter, ETResult<LiveData<Form>>>

interface CreateFormUseCaseParameter {
    val onActionClickListener: OnActionClickListener
}
