package com.etrade.mobilepro.quote.screen.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemFundsLinksBinding

class FundsLinksViewHolder(private val binding: QuoteItemFundsLinksBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bindTo(
        disclosureClickListener: (() -> Unit)? = null,
        definitionsClickListener: (() -> Unit)? = null
    ) {
        binding.disclosureTitle.setOnClickListener {
            disclosureClickListener?.invoke()
        }
        binding.definitionsTitle.setOnClickListener {
            definitionsClickListener?.invoke()
        }
    }
}
