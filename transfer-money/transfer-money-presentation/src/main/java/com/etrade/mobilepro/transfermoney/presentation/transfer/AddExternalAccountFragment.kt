package com.etrade.mobilepro.transfermoney.presentation.transfer

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.webkit.WebView
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.common.di.AddExternalAccount
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.transfermoney.presentation.R
import com.etrade.mobilepro.util.android.extension.postUrl
import com.etrade.mobilepro.util.android.fragment.OrientationLock
import com.etrade.mobilepro.util.getSessionCookieStringOrEmpty
import okhttp3.Call
import okhttp3.CookieJar
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

internal const val ADD_EXTERNAL_FRAGMENT_RESULT_KEY = "ADD_EXTERNAL_FRAGMENT_RESULT_KEY"
class AddExternalAccountFragment @Inject constructor(
    @DeviceType deviceType: String,
    @AddExternalAccount private val url: String,
    private val orientationLock: OrientationLock,
    private val cookieJar: CookieJar
) : WebViewFragment(deviceType) {

    override fun getUrl(): String = url

    override val restoreState: Boolean = false

    private val navArgs: AddExternalAccountFragmentArgs by navArgs()

    private var webViewCall: Call? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        orientationLock.init(this, ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT, applyIfTablet = true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        activity?.apply {
            toolbarActionEndView?.apply {
                setToolbarActionTextAndContentDescription(R.string.done)
                setOnClickListener {
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(ADD_EXTERNAL_FRAGMENT_RESULT_KEY, true)
                    findNavController().navigateUp()
                }
                visibility = View.VISIBLE
            }

            (this as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            webViewReference.let {
                if (it.canGoBack()) {
                    it.goBack()
                } else {
                    findNavController().navigateUp()
                }
            }
        }
    }

    override fun onDestroyView() {
        webViewCall?.takeIf { !it.isCanceled() }?.cancel()
        super.onDestroyView()
    }

    override fun loadUrl(webView: WebView) {
        val data = "source=MOBILE&verificationType=${navArgs.verificationMethod ?: getString(R.string.transfer_instant_verification)}"
        val sessionCookie = cookieJar.getSessionCookieStringOrEmpty(getUrl())
        val headers = mapOf(
            "Cookie" to "document.cookie='isMobileApp=true'; path=/; domain=.etrade.com; secure; $sessionCookie;"
        )
        webViewCall = webView.postUrl(getUrl(), data, headers, logger = logger)
    }
}
