package com.etrade.mobilepro.quotes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

class QuoteBottomSheetFragmentViewModel @Inject constructor() : ViewModel() {
    private val _quoteDescription = MutableLiveData<Pair<String, String>>()

    val quoteDescription: LiveData<Pair<String, String>> = _quoteDescription

    private val _notifyWatchlistRefresh = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val notifyWatchlistRefresh: LiveData<ConsumableLiveEvent<Unit>> = _notifyWatchlistRefresh

    fun updateQuoteDesc(quote: String, quoteDesc: String) {
        _quoteDescription.value = quote to quoteDesc
    }

    fun refreshWatchlist() {
        _notifyWatchlistRefresh.value = ConsumableLiveEvent(Unit)
    }
}
