package com.etrade.mobilepro.news.presentation.filter.watchlist

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.livedata.combineWith
import com.etrade.mobilepro.livedata.filterNotNull
import com.etrade.mobilepro.news.api.filter.FilterDataSourceWithForcedUpdate
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.util.NetworkState
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.insertAllWatchlistsItem
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class WatchlistFilterDataSource @Inject constructor(
    private val watchlistRepo: WatchlistRepo,
    private val userPreferences: UserPreferences,
    private val resources: Resources,
) : FilterDataSourceWithForcedUpdate<Watchlist> {

    override val networkState: LiveData<NetworkState>
        get() = _networkState

    override val items: LiveData<List<Watchlist>>
        get() = _items

    override val selectedItem: LiveData<Watchlist>
        get() = items
            .combineWith(_selectedItemId) { items, selectedId ->
                items.firstOrNull { it.id == selectedId }
            }
            .filterNotNull()

    override val forcedUpdateWith: LiveData<ConsumableLiveEvent<Watchlist>>
        get() = _forcedUpdateWith

    private val _networkState: MutableLiveData<NetworkState> = MutableLiveData(NetworkState.Loading)
    private val _items: MutableLiveData<List<Watchlist>> = MutableLiveData(emptyList())

    private val _forcedUpdateWith = MutableLiveData<ConsumableLiveEvent<Watchlist>>()
    private val disposable: CompositeDisposable = CompositeDisposable()

    private val _selectedItemId = userPreferences.recentWatchlistData.map { it.first }

    override fun refresh(forceUpdate: Boolean) {
        if (_networkState.value == NetworkState.Success && !forceUpdate) { return }
        _networkState.value = NetworkState.Loading
        disposable.clear()
        disposable.add(
            watchlistRepo.getUserWatchlists()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = { list ->
                        val watchlists = list.insertAllWatchlistsItem(title = resources.getString(R.string.watchlist_title_all))
                        _items.value = watchlists
                        if (!watchlists.any { it.id == _selectedItemId.value }) {
                            userPreferences.setRecentWatchlist(DEVICE_WATCHLIST_ID, null)
                        }
                        if (forceUpdate) {
                            watchlists.firstOrNull { it.id == _selectedItemId.value }?.let { wl -> _forcedUpdateWith.value = wl.copy().consumable() }
                        }
                        _networkState.value = NetworkState.Success
                    },
                    onError = {
                        _networkState.value = NetworkState.Failed(it)
                    }
                )
        )
    }

    override fun getSelectedItemIndex(): Int = items.value?.indexOfFirst { it.id == _selectedItemId.value } ?: -1

    override fun setSelectedItem(item: Watchlist) {
        userPreferences.setRecentWatchlist(item.id, item.name)
    }

    override fun findAndSelectItemByAccountId(accountId: String, isStockPlan: Boolean?) =
        throw UnsupportedOperationException()

    override fun clear() {
        disposable.clear()
        if (_networkState.value == NetworkState.Loading) {
            _networkState.value = NetworkState.Success
        }
    }
}
