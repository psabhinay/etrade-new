package com.etrade.mobilepro.streaming.api

import com.etrade.mobilepro.instrument.InstrumentType
import io.reactivex.Observable

/**
 * An interface to a different quote providers.
 */
interface StreamingQuoteProvider<F, T, P> {

    /**
     * Gets a streaming quote values for a [symbol].
     *
     * @param symbol The symbol for which to get quotes.
     * @param instrumentType The instrument type of the symbol.
     * @param fields The collection of fields that a client is interested in.
     * @param payload The custom payload for a subscription.
     *
     * @return An observable object to listen for the changes in [fields] of a specific [symbol].
     */
    fun getQuote(symbol: String, instrumentType: InstrumentType, fields: Set<F>, payload: P? = null): Observable<T>
}
