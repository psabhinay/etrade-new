package com.etrade.mobilepro.feature.presentation.compose

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.etrade.mobilepro.feature.presentation.R
import com.etrade.mobilepro.feature.presentation.viewmodel.AsyncActionViewModel

@Composable
internal fun AsyncActionButtonItem(
    viewModel: AsyncActionViewModel,
    modifier: Modifier = Modifier
) {
    val refreshing: Boolean by viewModel.isExecuting.collectAsState()
    RefreshingButtonItem(
        onClick = viewModel::execute,
        text = viewModel.title,
        refreshing = refreshing,
        modifier = modifier
    )
}

@Composable
private fun AsyncActionButtonItemPreview(refreshing: Boolean) {
    AsyncActionButtonItem(
        asyncActionViewModel(
            title = stringResource(
                R.string.feature_flags_refresh_remote_title,
                "Firebase"
            ),
            refreshing = refreshing
        )
    )
}

@Preview
@Composable
private fun AsyncActionButtonItemExecuting() = AsyncActionButtonItemPreview(refreshing = true)

@Preview
@Composable
private fun AsyncActionButtonItemNotExecuting() = AsyncActionButtonItemPreview(refreshing = false)
