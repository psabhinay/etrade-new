package com.etrade.mobilepro.trade.data.stock

import com.etrade.mobilepro.trade.data.CommonOrderConstants

internal object StockOrderConstants {

    const val USER_ID = "UserId"
    const val PLATFORM = CommonOrderConstants.PLATFORM
    const val MARKET_SESSION = "MktSession"
    const val OFFSET_TYPE = "OffSetType"
    const val OFFSET_VALUE = "OffSetValue"
    const val PRICE = "Price"
    const val OVERNIGHT_INDICATOR_FLAG = "overnightIndicatorFlag"
    const val DESTINATION_MARKET_CODE = "destMktCode"
    const val LOT_QUANTITY = "lotqty"
    const val LOT_SUPPORT = "lotsupport"
}
