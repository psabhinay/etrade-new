package com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer

import androidx.fragment.app.Fragment

/**
 * @return a [BottomSheetNavContainer], if it exists as an ancestor of current [Fragment], @null otherwise.
 * If called on an instance of [BottomSheetNavContainer], returns self.
 */
tailrec fun Fragment.getBottomSheetNavContainer(): BottomSheetNavContainer? =
    this as? BottomSheetNavContainer ?: parentFragment?.getBottomSheetNavContainer()
