package com.etrade.mobilepro.appstart.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class AppStartResponseDto(
    @Json(name = "mobile_response")
    val screen: AppStartReferenceHolderDto
)

@JsonClass(generateAdapter = true)
internal class AppStartReferenceHolderDto(
    @Json(name = "references")
    val references: List<AppStartReferenceDto>
)

@JsonClass(generateAdapter = true)
internal class AppStartReferenceDto(
    @Json(name = "type")
    val type: String? = null,

    @Json(name = "data")
    val data: List<AppStartData>
)

@JsonClass(generateAdapter = true)
internal class AppStartData(
    @Json(name = "isSubjectToGDPR")
    val isSubjectToGDPR: Boolean,

    @Json(name = "pingSessionInterval")
    val pingSessionInterval: Long? = null
)
