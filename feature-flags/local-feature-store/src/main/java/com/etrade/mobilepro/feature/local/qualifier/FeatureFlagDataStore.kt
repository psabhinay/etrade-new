package com.etrade.mobilepro.feature.local.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class FeatureFlagDataStore
