package com.etrade.mobilepro.screener.data

import android.content.res.Resources
import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.datasource.ScreenerSetsDataSource
import io.reactivex.Observable
import javax.inject.Inject

class DefaultScreenerSetsDataSource @Inject constructor(
    private val resources: Resources,
    private val aggregator: ScreenerFilterAggregator
) : ScreenerSetsDataSource {
    override fun screeners(group: ScreenerType.Group): Observable<List<ScreenerItem>> =
        aggregator
            .screenerFilterRequests()
            .map {
                it.type
            }
            .startWith(group.contents.first())
            .filter {
                group.contentsAsSet.contains(it)
            }
            .map {
                group.contents
            }
            .map(this::mapToScreenerItem)

    override fun clearScreenerSet(screenerSet: ScreenerItem.ScreenerSet) = aggregator.clear(screenerSet.type)

    private fun mapToScreenerItem(screeners: List<ScreenerType>): List<ScreenerItem> =
        screeners.map {
            ScreenerItem.ScreenerSet(
                type = it,
                title = getDefaultTitles(it),
                filter = aggregator.get(it)
            )
        }
    @Suppress("ComplexMethod") // nothing special
    private fun getDefaultTitles(type: ScreenerType): String {
        val stringRes = when (type) {
            ScreenerType.Stock.MarketSegment -> R.string.stock_screener_set_market_segment
            ScreenerType.Stock.PriceAndVolume -> R.string.stock_screener_set_price_volume
            ScreenerType.Stock.Opinions -> R.string.stock_screener_set_opinions
            ScreenerType.Stock.Fundamentals -> R.string.stock_screener_set_fundamentals
            ScreenerType.Stock.EarningsAndDividends -> R.string.stock_screener_set_earnings_dividends
            ScreenerType.Stock.Technicals -> R.string.stock_screener_set_technicals
            ScreenerType.MutualFund.FundProfile -> R.string.mutual_fund_screener_set_fund_profile
            ScreenerType.MutualFund.Performance -> R.string.mutual_fund_screener_set_performance
            ScreenerType.MutualFund.Expenses -> R.string.mutual_fund_screener_set_expenses
            ScreenerType.MutualFund.Fees -> R.string.mutual_fund_screener_set_fees
            ScreenerType.MutualFund.Risks -> R.string.mutual_fund_screener_set_risks
            ScreenerType.MutualFund.Portfolio -> R.string.mutual_fund_screener_set_portfolio
            ScreenerType.Etf.FundProfile -> R.string.etf_screener_set_fund_profile
            ScreenerType.Etf.Performance -> R.string.etf_screener_set_performance
            ScreenerType.Etf.Risk -> R.string.etf_screener_set_risk
            ScreenerType.Etf.Portfolio -> R.string.etf_screener_set_portfolio
            ScreenerType.Etf.Technicals -> R.string.etf_screener_set_technicals
        }
        return resources.getString(stringRes)
    }
}
