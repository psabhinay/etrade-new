package com.etrade.mobilepro.billpay.presentation.editpayment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.PaymentRequest
import com.etrade.mobilepro.billpay.api.PaymentResult
import com.etrade.mobilepro.billpay.presentation.common.formatAmount
import com.etrade.mobilepro.billpay.presentation.common.isValidAmount
import com.etrade.mobilepro.billpay.presentation.paymentdetail.ParcelablePayment
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.util.delegate.consumable
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.threeten.bp.LocalDate
import java.math.BigDecimal

private val logger = LoggerFactory.getLogger(EditPaymentViewModel::class.java)

class EditPaymentViewModel @AssistedInject constructor(
    private val repo: BillPayRepo,
    @Assisted private val validAccountList: List<AccountDetails>,
    @Assisted private val selectedPayee: Payee,
    @Assisted private val originAccount: String?,
    @Assisted private val currentPayment: ParcelablePayment?
) : ViewModel() {

    internal val isPayeeAccountExternal: Boolean = true // All payee accounts are treated as external accounts
    internal val isSelectedAccountExternal: Boolean
        get() = requireNotNull(_viewState.value?.selectedAccount?.isExternalAccount) { "Selected account must not be null" }

    private val _viewState: MediatorLiveData<PaymentForm> = MediatorLiveData()
    internal val viewState: LiveData<PaymentForm> get() = _viewState

    @AssistedFactory
    interface Factory {
        fun create(
            validAccountList: List<AccountDetails>,
            selectedPayee: Payee?,
            originAccount: String?,
            currentPayment: ParcelablePayment?
        ): EditPaymentViewModel
    }

    private val initialForm: PaymentForm

    private fun isFormModified(): Boolean = _viewState.value != initialForm

    init {
        initialForm = PaymentForm(
            amount = currentPayment?.amount,
            validAccounts = validAccountList,
            selectedAccount = validAccountList.firstOrNull {
                it.accountId == originAccount
            },
            memo = currentPayment?.memo,
            selectedScheduleDate = currentPayment?.scheduleDate
        )
        _viewState.value = initialForm
    }

    internal fun canCollectData(): Boolean {
        return viewState.value?.run {
            isFormModified() && amount.isValidAmount()
        } ?: false
    }

    private fun collectData(): PaymentRequest? = _viewState.value?.run {
        PaymentRequest(
            paymentId = requireNotNull(currentPayment?.id),
            amount = requireNotNull(amount),
            scheduleDate = requireNotNull(selectedScheduleDate),
            originAccount = PaymentRequest.OriginAccount(
                accountId = requireNotNull(selectedAccount?.accountId),
                instNo = requireNotNull(selectedAccount?.instNo.toString())
            ),
            destinationAccount = PaymentRequest.DestinationAccount(
                payeeId = requireNotNull(selectedPayee.payeeId),
                leadDays = selectedPayee.leadDays ?: 0
            ),
            memo = memo
        )
    }

    internal fun onAmountEntered(amount: BigDecimal) {
        _viewState.updateValue { it.copy(amount = amount.formatAmount()) }
    }

    internal fun onAccountSelected(account: AccountDetails) {
        _viewState.updateValue {
            it.copy(selectedAccount = account)
        }
    }

    internal fun onScheduleDateSelected(date: LocalDate) {
        _viewState.updateValue {
            it.copy(selectedScheduleDate = date)
        }
    }

    internal fun setMemo(memo: String?) {
        _viewState.updateValue {
            it.copy(memo = if (memo.isNullOrEmpty()) { null } else { memo })
        }
    }

    internal fun updateScheduledPayment(sessionId: String) {
        if (_viewState.value?.isLoading == true) return

        collectData()?.let { request -> updatePayment(sessionId, request) }
    }

    private fun updatePayment(sessionId: String, request: PaymentRequest) {

        viewModelScope.launch {
            _viewState.updateValue { it.copy(isLoading = true) }

            repo.schedulePayment(sessionId, request)
                .fold(
                    onSuccess = { result ->
                        _viewState.updateValue { form ->
                            form.copy(isLoading = false, paymentResult = result)
                        }
                    },
                    onFailure = {
                        logger.error(it.localizedMessage)
                        _viewState.updateValue { form ->
                            form.copy(isLoading = false, paymentResult = PaymentResult.Error.Generic)
                        }
                    }
                )
        }
    }
}

internal data class PaymentForm(
    val validAccounts: List<AccountDetails> = emptyList(),
    val selectedAccount: AccountDetails? = null,
    val selectedScheduleDate: LocalDate? = null,
    val amount: BigDecimal? = null,
    val memo: String? = null,
    val isLoading: Boolean = false,
    val paymentResult: PaymentResult? = null
) {
    val paymentSucceed: Boolean get() = paymentResult is PaymentResult.Success
    val consumablePaymentResult: PaymentResult? by consumable(paymentResult)

    /**
     * Custom equals implementations is required because [BigDecimal.equals] implementation considers two objects
     * to be equal only if their value and scale are the same, so, here we have to use [BigDecimal.compareTo] to have
     * consistent comparison results.
     */
    @SuppressWarnings("ComplexMethod")
    override fun equals(other: Any?): Boolean {
        if (this === other) { return true }
        if (javaClass != other?.javaClass) { return false }

        other as PaymentForm

        if (validAccounts != other.validAccounts) { return false }
        if (selectedAccount != other.selectedAccount) { return false }
        if (selectedScheduleDate != other.selectedScheduleDate) { return false }
        if (amount?.compareTo(other.amount) != 0) { return false }
        if (memo != other.memo) { return false }
        if (isLoading != other.isLoading) { return false }
        if (paymentResult != other.paymentResult) { return false }

        return true
    }
}
