package com.etrade.mobilepro.alerts.api.repo

import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.api.model.AlertMessage
import com.etrade.mobilepro.alerts.api.model.AlertValueType
import com.etrade.mobilepro.alerts.api.model.NewsAlertValueType
import com.etrade.mobilepro.alerts.api.model.SetAlertResponse
import com.etrade.mobilepro.alerts.api.model.SetNewsAlertResponse
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.common.result.ETResult
import io.reactivex.Completable
import io.reactivex.Single

interface AlertsRepo {
    fun loadNext(userId: String, startingEventId: String, startingTimeStamp: Long, maxCount: Int): Single<List<AlertItem.Alert>>

    fun loadAlertsCount(): Single<Int>

    fun loadAlertContent(userId: String, alertId: String): Single<AlertMessage>

    fun deleteAlert(userId: String, alertId: String): Completable

    fun setAlert(sessionToken: String, symbol: String, values: Map<AlertValueType, String>): Single<SetAlertResponse>

    fun setNewsAlert(sessionToken: String, symbol: String, values: Map<NewsAlertValueType, Boolean>): Single<SetNewsAlertResponse>

    fun getUserAlerts(): Single<List<UserAlertItem>>

    fun deleteUserAlert(alert: UserAlertItem): Completable

    fun updateAlert(
        sessionToken: String,
        symbol: String,
        deleteInfo: String,
        edit: String,
        lastEdited: String,
        values: Map<AlertValueType, String>
    ): Single<SetAlertResponse>

    fun updateNewsAlert(
        sessionToken: String,
        symbol: String,
        deleteInfo: String,
        edit: String,
        lastEdited: String,
        values: Map<NewsAlertValueType, Boolean>
    ): Single<SetNewsAlertResponse>

    suspend fun findAlertByMessage(userId: Long?, alertMessage: String): ETResult<AlertItem.Alert?>
}
