package com.etrade.mobilepro.chart.netassets.data

import com.etrade.mobilepro.chart.data.ChartRequestDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.POST

internal interface NetAssetsChartApiClient {

    @JsonMoshi
    @POST("/app/apg/overviewChart.json")
    suspend fun getNetAssetsChart(@JsonMoshi @Body request: ChartRequestDto): NetAssetsChartResponseDto
}
