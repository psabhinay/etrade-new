package com.etrade.mobilepro.chat.presentation.message.viewHolder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.chat.presentation.databinding.ChatItemAgentIsTypingBinding

class ChatAgentTypingViewHolder(binding: ChatItemAgentIsTypingBinding) :
    RecyclerView.ViewHolder(binding.root)
