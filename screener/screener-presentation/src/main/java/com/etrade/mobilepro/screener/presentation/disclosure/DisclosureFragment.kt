package com.etrade.mobilepro.screener.presentation.disclosure

import android.os.Bundle
import android.view.View
import android.widget.ScrollView
import android.widget.TextView
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.screener.api.ScreenerDisclosure
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.databinding.FragmentDisclosureBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.restoreTextViewScrollPosition
import com.etrade.mobilepro.util.android.extension.saveLineStartOffset
import com.etrade.mobilepro.util.android.textutil.TextUtil
import com.etrade.mobilepro.util.android.textutil.setLinkClickListener
import com.etrade.mobilepro.util.safeLet
import org.chromium.customtabsclient.shared.WebviewActivity
import javax.inject.Inject

class DisclosureFragment @Inject constructor() : OverlayBaseFragment() {

    override val layoutRes: Int = R.layout.fragment_disclosure
    private val binding by viewBinding(FragmentDisclosureBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            binding.disclosureContent.text = TextUtil.convertTextWithHmlTagsToSpannable(
                getDisclosureContent(DisclosureFragmentArgs.fromBundle(it).disclosureType)
            ).setLinkClickListener { url ->
                requireContext().startActivity(WebviewActivity.intent(requireContext(), url, R.string.disclosures))
            }
        }

        binding.scrollView.restoreTextViewScrollPosition(binding.disclosureContent, savedInstanceState)
    }

    private fun getDisclosureContent(disclosureType: ScreenerDisclosure): String = when (disclosureType) {
        ScreenerDisclosure.STOCK_PRIMARY -> getString(R.string.disclosure_stock_primary)
        ScreenerDisclosure.STOCK_RESULTS -> getString(R.string.disclosure_stock_results)
        ScreenerDisclosure.MUTUAL_FUND_PRIMARY -> getString(R.string.disclosure_mutual_fund_primary_nested)
        ScreenerDisclosure.MUTUAL_FUND_RESULTS -> getString(R.string.disclosure_mutual_fund_result_nested)
        ScreenerDisclosure.ETF_PRIMARY -> getString(R.string.disclosure_etf_primary)
        ScreenerDisclosure.ETF_RESULTS -> getString(R.string.disclosure_etf_result_nested)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        safeLet(
            view?.findViewById<ScrollView>(R.id.scroll_view),
            view?.findViewById<TextView>(R.id.disclosure_content)
        ) { scrollView, disclosureContent ->
            scrollView.saveLineStartOffset(disclosureContent, outState)
        }
    }
}
