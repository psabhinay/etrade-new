package com.etrade.mobilepro.dagger

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.billpay.presentation.BillPayViewModel
import com.etrade.mobilepro.billpay.presentation.editpayment.EditPaymentViewModelFactoryFactory
import com.etrade.mobilepro.billpay.presentation.editpayment.EditPaymentViewModelFactoryFactoryImp
import com.etrade.mobilepro.billpay.presentation.paymentdetail.PaymentDetailViewModel
import com.etrade.mobilepro.billpay.presentation.viewmodel.AddPaymentViewModelFactoryFactory
import com.etrade.mobilepro.billpay.presentation.viewmodel.AddPaymentViewModelFactoryFactoryImp
import com.etrade.mobilepro.billpay.presentation.viewmodel.ManagePayeeViewModelFactoryFactory
import com.etrade.mobilepro.billpay.presentation.viewmodel.ManagePayeeViewModelFactoryFactoryImp
import com.etrade.mobilepro.billpay.presentation.viewmodel.PayeeFormViewModelFactoryFactory
import com.etrade.mobilepro.billpay.presentation.viewmodel.PayeeFormViewModelFactoryFactoryImp
import com.etrade.mobilepro.billpay.presentation.viewmodel.PaymentListViewModelFactoryFactory
import com.etrade.mobilepro.billpay.presentation.viewmodel.PaymentListViewModelFactoryFactoryImp
import com.etrade.mobilepro.billpay.presentation.viewmodel.PaymentPreviewAssistedFactory
import com.etrade.mobilepro.billpay.presentation.viewmodel.PaymentPreviewAssistedFactoryImpl
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@Module
interface BillPayViewModelModule {

    @Binds
    fun bindAddPaymentViewModelFactoryFactory(factory: AddPaymentViewModelFactoryFactoryImp): AddPaymentViewModelFactoryFactory

    @Binds
    fun bindEditPaymentViewModelFactoryFactory(factory: EditPaymentViewModelFactoryFactoryImp): EditPaymentViewModelFactoryFactory

    @Binds
    @FlowPreview
    @ExperimentalCoroutinesApi
    fun bindPayeeFormViewModelFactoryFactory(factory: PayeeFormViewModelFactoryFactoryImp): PayeeFormViewModelFactoryFactory

    @Binds
    @IntoMap
    @ViewModelKey(BillPayViewModel::class)
    fun bindBillPayViewModel(viewModel: BillPayViewModel): ViewModel

    @Binds
    fun bindManagePayeeViewModel(factory: ManagePayeeViewModelFactoryFactoryImp): ManagePayeeViewModelFactoryFactory

    @Binds
    @IntoMap
    @ViewModelKey(PaymentDetailViewModel::class)
    fun bindPaymentDetailViewModel(viewModel: PaymentDetailViewModel): ViewModel

    @Binds
    fun bindPaymentListViewModelFactoryFactory(factory: PaymentListViewModelFactoryFactoryImp): PaymentListViewModelFactoryFactory

    @Binds
    fun bindPaymentPreviewAssistedFactory(factory: PaymentPreviewAssistedFactoryImpl): PaymentPreviewAssistedFactory
}
