package com.etrade.mobilepro.trade.api.advanced

interface TrailingStopOrderDetails {
    val orderTerm: String
    val orderType: String
    val offsetType: String
    val offsetValue: String
    val initialStopPrice: String
    val followPrice: String
}
