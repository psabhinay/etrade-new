package com.etrade.mobilepro.streaming.lightstreamer.level1

import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.lightstreamer.LightstreamerFieldMapper
import javax.inject.Inject

class Level1LightstreamerFieldMapper @Inject constructor() : LightstreamerFieldMapper<Level1Field> {

    private val optionFieldMap: Map<Level1Field, Set<String>> = mutableMapOf(
        Level1Field.ASK to setOf(Level1Column.ASK),
        Level1Field.ASK_EXCHANGE to setOf(Level1Column.ASK_EXCHANGE),
        Level1Field.ASK_SIZE to setOf(Level1Column.ASK_SIZE),
        Level1Field.BID to setOf(Level1Column.BID),
        Level1Field.BID_EXCHANGE to setOf(Level1Column.BID_EXCHANGE),
        Level1Field.BID_SIZE to setOf(Level1Column.BID_SIZE),
        Level1Field.CHANGE to setOf(Level1Column.LAST_CHANGE),
        Level1Field.CHANGE_PERCENT to setOf(Level1Column.LAST_CHANGE_PCT),
        Level1Field.CLOSING_MARK to setOf(Level1Column.CLOSING_MARK),
        Level1Field.DAY_CHANGE to setOf(Level1Column.LAST_CHANGE),
        Level1Field.DAY_CHANGE_PERCENT to setOf(Level1Column.LAST_CHANGE_PCT),
        Level1Field.DAY_HIGH to setOf(Level1Column.HIGH),
        Level1Field.DAY_LOW to setOf(Level1Column.LOW),
        Level1Field.DELTA to setOf(Level1Column.DELTA),
        Level1Field.EPS to setOf(Level1Column.EPS),
        Level1Field.GAMMA to setOf(Level1Column.GAMMA),
        Level1Field.IMPLIED_VOLATILITY to setOf(Level1Column.IMPLIED_VOLATILITY),
        Level1Field.INTRINSIC_VALUE to setOf(Level1Column.INTRINSIC_VALUE),
        Level1Field.LAST_PRICE to setOf(Level1Column.LAST),
        Level1Field.LAST_PRICE_EXCHANGE to setOf(Level1Column.EXCHANGE_CODE),
        Level1Field.LAST_VOLUME to setOf(Level1Column.LAST_VOLUME),
        Level1Field.MARK to setOf(Level1Column.MARK),
        Level1Field.OPEN_INTEREST to setOf(Level1Column.OPEN_INTEREST),
        Level1Field.OPEN_PRICE to setOf(Level1Column.OPEN),
        Level1Field.RHO to setOf(Level1Column.RHO),
        Level1Field.PREVIOUS_CLOSE_PRICE to setOf(Level1Column.PREVIOUS_CLOSE),
        Level1Field.THETA to setOf(Level1Column.THETA),
        Level1Field.TRADE_HALT_REASON to setOf(Level1Column.TRADE_HALT_REASON),
        Level1Field.TRADE_STATUS to setOf(Level1Column.TRADE_STATUS),
        Level1Field.TICK_INDICATOR to setOf(
            Level1Column.ACCUM_VOLUME,
            Level1Column.LAST,
            Level1Column.TICK_INDICATOR
        ),
        Level1Field.TIMESTAMP_SINCE_EPOCH to setOf(Level1Column.SALE_TIME_MS),
        Level1Field.VEGA to setOf(Level1Column.VEGA),
        Level1Field.VOLUME to setOf(Level1Column.ACCUM_VOLUME),
        Level1Field.YEAR_HIGH to setOf(Level1Column.YEAR_HIGH),
        Level1Field.YEAR_HIGH_DATE to setOf(Level1Column.YEAR_HIGH_DATE),
        Level1Field.YEAR_LOW to setOf(Level1Column.YEAR_LOW),
        Level1Field.YEAR_LOW_DATE to setOf(Level1Column.YEAR_LOW_DATE)
    ).apply {
        addPriceToEarningFields()
    }

    private val stockFieldMap: Map<Level1Field, Set<String>> = mutableMapOf(
        Level1Field.ASK to setOf(Level1Column.ASK),
        Level1Field.ASK_EXCHANGE to setOf(Level1Column.ASK_EXCHANGE),
        Level1Field.ASK_SIZE to setOf(Level1Column.ASK_SIZE, Level1Column.PRIMARY_EXCHANGE),
        Level1Field.BETA to setOf(Level1Column.BETA),
        Level1Field.BID to setOf(Level1Column.BID),
        Level1Field.BID_EXCHANGE to setOf(Level1Column.BID_EXCHANGE),
        Level1Field.BID_SIZE to setOf(Level1Column.BID_SIZE, Level1Column.PRIMARY_EXCHANGE),
        Level1Field.CHANGE to setOf(
            Level1Column.EXT_HOURS_LAST,
            Level1Column.EXT_HOURS_LAST_CHANGE,
            Level1Column.EXT_HOURS_LAST_CHANGE_PCT,
            Level1Column.LAST,
            Level1Column.LAST_CHANGE,
            Level1Column.MARKET_STATUS
        ),
        Level1Field.CHANGE_PERCENT to setOf(
            Level1Column.EXT_HOURS_LAST,
            Level1Column.EXT_HOURS_LAST_CHANGE,
            Level1Column.EXT_HOURS_LAST_CHANGE_PCT,
            Level1Column.LAST,
            Level1Column.LAST_CHANGE,
            Level1Column.LAST_CHANGE_PCT,
            Level1Column.MARKET_STATUS
        ),
        Level1Field.CLOSING_MARK to setOf(Level1Column.CLOSING_MARK),
        Level1Field.DAY_CHANGE to setOf(
            Level1Column.EXT_HOURS_LAST,
            Level1Column.EXT_HOURS_LAST_CHANGE,
            Level1Column.EXT_HOURS_LAST_CHANGE_PCT,
            Level1Column.LAST,
            Level1Column.LAST_CHANGE,
            Level1Column.MARKET_STATUS
        ),
        Level1Field.DAY_CHANGE_PERCENT to setOf(
            Level1Column.EXT_HOURS_LAST,
            Level1Column.EXT_HOURS_LAST_CHANGE,
            Level1Column.EXT_HOURS_LAST_CHANGE_PCT,
            Level1Column.LAST,
            Level1Column.LAST_CHANGE,
            Level1Column.LAST_CHANGE_PCT,
            Level1Column.MARKET_STATUS
        ),
        Level1Field.DAY_HIGH to setOf(Level1Column.HIGH),
        Level1Field.DAY_LOW to setOf(Level1Column.LOW),
        Level1Field.DIVIDEND to setOf(Level1Column.DIVIDEND_RATE),
        Level1Field.DIVIDEND_YIELD to setOf(Level1Column.DIVIDEND_YIELD),
        Level1Field.DAY_LOW to setOf(Level1Column.LOW),
        Level1Field.EPS to setOf(Level1Column.EPS),
        Level1Field.LAST_PRICE to setOf(
            Level1Column.EXT_HOURS_LAST,
            Level1Column.LAST,
            Level1Column.MARKET_STATUS
        ),
        Level1Field.LAST_PRICE_EXCHANGE to setOf(Level1Column.EXCHANGE_CODE, Level1Column.EXT_HOURS_EXCHANGE_CODE),
        Level1Field.LAST_VOLUME to setOf(Level1Column.EXT_HOURS_VOL, Level1Column.LAST_VOLUME),
        Level1Field.MARK to setOf(Level1Column.MARK),
        Level1Field.MARKET_CAP to setOf(Level1Column.MARKET_CAP),
        Level1Field.OPEN_PRICE to setOf(Level1Column.OPEN),
        Level1Field.PREVIOUS_CLOSE_PRICE to setOf(Level1Column.PREVIOUS_CLOSE),
        Level1Field.TICK_INDICATOR to setOf(
            Level1Column.ACCUM_VOLUME,
            Level1Column.EXT_HOURS_LAST,
            Level1Column.LAST,
            Level1Column.MARKET_STATUS,
            Level1Column.TICK_INDICATOR
        ),
        Level1Field.TIMESTAMP_SINCE_EPOCH to setOf(
            Level1Column.LAST_TIME_MS,
            Level1Column.MARKET_STATUS,
            Level1Column.SALE_TIME_MS
        ),
        Level1Field.TRADE_HALT_REASON to setOf(Level1Column.TRADE_HALT_REASON),
        Level1Field.TRADE_STATUS to setOf(Level1Column.TRADE_STATUS),
        Level1Field.VOLUME to setOf(Level1Column.ACCUM_VOLUME),
        Level1Field.VOLUME_TEN_DAY_AVERAGE to setOf(Level1Column.VOLUME_TEN_DAY_AVERAGE),
        Level1Field.YEAR_HIGH to setOf(Level1Column.YEAR_HIGH),
        Level1Field.YEAR_HIGH_DATE to setOf(Level1Column.YEAR_HIGH_DATE),
        Level1Field.YEAR_LOW to setOf(Level1Column.YEAR_LOW),
        Level1Field.YEAR_LOW_DATE to setOf(Level1Column.YEAR_LOW_DATE)
    ).apply {
        addPriceToEarningFields()
    }

    override fun map(field: Level1Field, isOption: Boolean): Set<String> {
        return requireNotNull(
            if (isOption) {
                optionFieldMap
            } else {
                stockFieldMap
            }[field]
        )
    }

    private fun setOf(column: Level1Column): Set<String> = setOf(column.column)

    private fun setOf(vararg column: Level1Column): Set<String> = kotlin.collections.setOf(*column).map { it.column }.toSet()

    private fun MutableMap<Level1Field, Set<String>>.addPriceToEarningFields() {
        put(Level1Field.PRICE_TO_EARNINGS, getValue(Level1Field.EPS) + getValue(Level1Field.LAST_PRICE))
    }
}
