package com.etrade.mobilepro.order.details.mapper;

import com.etrade.eo.core.util.DateFormattingUtils;
import com.etrade.eo.core.util.MarketDataFormatter;
import com.etrade.mobilepro.instrument.InstrumentType;
import com.etrade.mobilepro.instrument.OptionType;
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem;
import com.etrade.mobilepro.order.details.api.OrderDetailDescription;
import com.etrade.mobilepro.order.details.api.OrderDetailDescriptionMapperType;
import com.etrade.mobilepro.order.details.api.OrderHistoryItem;
import com.etrade.mobilepro.order.details.api.StockPlanOrderCancelResponse;
import com.etrade.mobilepro.order.details.dto.EventDto;
import com.etrade.mobilepro.order.details.dto.LegDetailsDto;
import com.etrade.mobilepro.order.details.dto.OrderDetailsDto;
import com.etrade.mobilepro.order.details.dto.OrderHistory;
import com.etrade.mobilepro.order.details.dto.OrdersDto;
import com.etrade.mobilepro.order.details.dto.StockPlanOrderCancelResponseDto;
import com.etrade.mobilepro.order.details.dto.StockPlanOrderDetailsResponseDto;
import com.etrade.mobilepro.orders.api.AdvancedOrderType;
import com.etrade.mobilepro.orders.api.ContingentCondition;
import com.etrade.mobilepro.orders.api.ContingentQualifier;
import com.etrade.mobilepro.orders.api.EventType;
import com.etrade.mobilepro.orders.api.ExecInstruction;
import com.etrade.mobilepro.orders.api.MarketDestination;
import com.etrade.mobilepro.orders.api.MarketSession;
import com.etrade.mobilepro.orders.api.OrderAction;
import com.etrade.mobilepro.orders.api.OrderStatus;
import com.etrade.mobilepro.orders.api.OrderTerm;
import com.etrade.mobilepro.orders.api.OrderType;
import com.etrade.mobilepro.orders.api.OrigSysCode;
import com.etrade.mobilepro.orders.api.PriceType;
import com.etrade.mobilepro.orders.api.StrategyType;
import com.etrade.mobilepro.orders.api.TransactionType;
import com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto;
import com.etrade.mobilepro.trade.api.option.TradeLeg;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

@kotlin.Suppress(names = {"LargeClass"})
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u00a0\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B2\u0012+\u0010\u0002\u001a\'\u0012\t\u0012\u00070\u0004\u00a2\u0006\u0002\b\u0005\u0012\t\u0012\u00070\u0006\u00a2\u0006\u0002\b\u0005\u0012\t\u0012\u00070\u0007\u00a2\u0006\u0002\b\u00050\u0003j\u0002`\b\u00a2\u0006\u0002\u0010\tJ\u0016\u0010\n\u001a\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rH\u0002J$\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u00102\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\r2\u0006\u0010\u0012\u001a\u00020\u0006H\u0002J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0010\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u000eH\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J \u0010\u001d\u001a\u0004\u0018\u00010\u001a2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001f0\r2\u0006\u0010 \u001a\u00020!H\u0002J\u001e\u0010\"\u001a\u00020\u000b2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\u0006\u0010$\u001a\u00020\u001aH\u0002J*\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00040\r2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001f0\rH\u0002J\u001a\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020\u00162\b\u0010)\u001a\u0004\u0018\u00010\u0014H\u0002J\u001e\u0010*\u001a\u00020\u001a2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00040\r2\u0006\u0010+\u001a\u00020\'H\u0002J\u0019\u0010,\u001a\u0004\u0018\u00010\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0002\u00a2\u0006\u0002\u0010-J\u0014\u0010.\u001a\b\u0012\u0004\u0012\u00020/0\r2\u0006\u00100\u001a\u000201J\u0010\u00102\u001a\u0002032\u0006\u00104\u001a\u000205H\u0002J\u000e\u00106\u001a\u0002072\u0006\u00104\u001a\u000208J\u0016\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020<2\u0006\u0010=\u001a\u00020\u001aJ\u0012\u0010>\u001a\u00020\u001a2\b\u0010?\u001a\u0004\u0018\u00010\u001aH\u0002R3\u0010\u0002\u001a\'\u0012\t\u0012\u00070\u0004\u00a2\u0006\u0002\b\u0005\u0012\t\u0012\u00070\u0006\u00a2\u0006\u0002\b\u0005\u0012\t\u0012\u00070\u0007\u00a2\u0006\u0002\b\u00050\u0003j\u0002`\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006@"}, d2 = {"Lcom/etrade/mobilepro/order/details/mapper/OrderDetailsMapper;", "", "orderDetailDescriptionMapperType", "Lkotlin/Function2;", "Lcom/etrade/mobilepro/order/details/api/OrderDetailDescription;", "Lkotlin/jvm/JvmSuppressWildcards;", "", "Lcom/etrade/mobilepro/trade/api/option/TradeLeg;", "Lcom/etrade/mobilepro/order/details/api/OrderDetailDescriptionMapperType;", "(Lkotlin/jvm/functions/Function2;)V", "calculateCommissionFees", "Ljava/math/BigDecimal;", "legDetails", "", "Lcom/etrade/mobilepro/order/details/dto/LegDetailsDto;", "createLegs", "", "orderDescriptions", "hasPartialExecution", "getAdvancedOrderItem", "Lcom/etrade/mobilepro/order/details/api/BaseOrderDetailsItem$AdvancedOrderItem;", "order", "Lcom/etrade/mobilepro/order/details/dto/OrdersDto;", "getCommissionFee", "legDetail", "getDateString", "", "time", "", "getEventDate", "events", "Lcom/etrade/mobilepro/order/details/dto/EventDto;", "eventType", "Lcom/etrade/mobilepro/orders/api/EventType;", "getLatestExecutionPrice", "legs", "netPrice", "getOrderDescriptions", "getPriceType", "Lcom/etrade/mobilepro/orders/api/PriceType;", "firstOrder", "advancedOrderItem", "getQuantity", "priceType", "getTimeString", "(Ljava/lang/Long;)Ljava/lang/String;", "map", "Lcom/etrade/mobilepro/order/details/api/BaseOrderDetailsItem;", "orderDetailsDto", "Lcom/etrade/mobilepro/order/details/dto/OrderDetailsDto;", "mapOrderHistory", "Lcom/etrade/mobilepro/order/details/api/OrderHistoryItem;", "dto", "Lcom/etrade/mobilepro/order/details/dto/OrderHistory;", "mapStockOrderCancelResponse", "Lcom/etrade/mobilepro/order/details/api/StockPlanOrderCancelResponse;", "Lcom/etrade/mobilepro/order/details/dto/StockPlanOrderCancelResponseDto;", "mapStockPlanOrderDetails", "Lcom/etrade/mobilepro/order/details/api/BaseOrderDetailsItem$StockPlanOrderDetailsItem;", "detailsDto", "Lcom/etrade/mobilepro/order/details/dto/StockPlanOrderDetailsResponseDto;", "symbol", "trimUnwantedDateCharacters", "text", "orders-details-data"})
public final class OrderDetailsMapper {
    private final kotlin.jvm.functions.Function2<com.etrade.mobilepro.order.details.api.OrderDetailDescription, java.lang.Boolean, com.etrade.mobilepro.trade.api.option.TradeLeg> orderDetailDescriptionMapperType = null;
    
    public OrderDetailsMapper(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function2<com.etrade.mobilepro.order.details.api.OrderDetailDescription, java.lang.Boolean, com.etrade.mobilepro.trade.api.option.TradeLeg> orderDetailDescriptionMapperType) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem> map(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.order.details.dto.OrderDetailsDto orderDetailsDto) {
        return null;
    }
    
    private final java.util.List<com.etrade.mobilepro.trade.api.option.TradeLeg> createLegs(java.util.List<com.etrade.mobilepro.order.details.api.OrderDetailDescription> orderDescriptions, boolean hasPartialExecution) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"LongMethod"})
    public final com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem.StockPlanOrderDetailsItem mapStockPlanOrderDetails(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.order.details.dto.StockPlanOrderDetailsResponseDto detailsDto, @org.jetbrains.annotations.NotNull()
    java.lang.String symbol) {
        return null;
    }
    
    private final com.etrade.mobilepro.order.details.api.OrderHistoryItem mapOrderHistory(com.etrade.mobilepro.order.details.dto.OrderHistory dto) {
        return null;
    }
    
    private final java.lang.String trimUnwantedDateCharacters(java.lang.String text) {
        return null;
    }
    
    @java.lang.SuppressWarnings(value = {"LongMethod"})
    private final com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem.AdvancedOrderItem getAdvancedOrderItem(com.etrade.mobilepro.order.details.dto.OrdersDto order) {
        return null;
    }
    
    private final java.lang.String getQuantity(java.util.List<com.etrade.mobilepro.order.details.api.OrderDetailDescription> orderDescriptions, com.etrade.mobilepro.orders.api.PriceType priceType) {
        return null;
    }
    
    private final java.lang.String getEventDate(java.util.List<com.etrade.mobilepro.order.details.dto.EventDto> events, com.etrade.mobilepro.orders.api.EventType eventType) {
        return null;
    }
    
    private final java.math.BigDecimal getLatestExecutionPrice(java.util.List<com.etrade.mobilepro.order.details.dto.LegDetailsDto> legs, java.lang.String netPrice) {
        return null;
    }
    
    private final com.etrade.mobilepro.orders.api.PriceType getPriceType(com.etrade.mobilepro.order.details.dto.OrdersDto firstOrder, com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem.AdvancedOrderItem advancedOrderItem) {
        return null;
    }
    
    private final java.util.List<com.etrade.mobilepro.order.details.api.OrderDetailDescription> getOrderDescriptions(java.util.List<com.etrade.mobilepro.order.details.dto.LegDetailsDto> legDetails, java.util.List<com.etrade.mobilepro.order.details.dto.EventDto> events) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.api.StockPlanOrderCancelResponse mapStockOrderCancelResponse(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.order.details.dto.StockPlanOrderCancelResponseDto dto) {
        return null;
    }
    
    private final java.math.BigDecimal calculateCommissionFees(java.util.List<com.etrade.mobilepro.order.details.dto.LegDetailsDto> legDetails) {
        return null;
    }
    
    private final java.math.BigDecimal getCommissionFee(com.etrade.mobilepro.order.details.dto.LegDetailsDto legDetail) {
        return null;
    }
    
    private final java.lang.String getTimeString(java.lang.Long time) {
        return null;
    }
    
    private final java.lang.String getDateString(long time) {
        return null;
    }
}