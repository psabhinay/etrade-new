package com.etrade.mobilepro.screener.api.datasource

import com.etrade.mobilepro.screener.api.Percent

interface PercentDataSource {
    fun getEmptyPercent(): Percent
    fun getExposurePercentList(): List<Percent>
}
