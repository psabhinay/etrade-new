package com.etrade.mobilepro.documents.details

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ShareCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ToolbarUpdateBlocker
import com.etrade.mobilepro.common.ToolbarUpdateBlockerImpl
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.documents.databinding.DocumentsFragmentDetailBinding
import com.etrade.mobilepro.documents.router.DocumentArgumentsHelper
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import java.io.File
import javax.inject.Inject

@RequireLogin
class DocumentDetailsFragment @Inject constructor(
    private val argumentsHelper: DocumentArgumentsHelper,
    snackbarUtilFactory: SnackbarUtilFactory,
    viewModelFactory: ViewModelProvider.Factory
) : OverlayBaseFragment(), ToolbarUpdateBlocker by ToolbarUpdateBlockerImpl() {
    override val layoutRes: Int = com.etrade.mobilepro.documents.R.layout.documents_fragment_detail
    private val binding by viewBinding(DocumentsFragmentDetailBinding::bind)
    private val viewModel: DocumentDetailsViewModel by viewModels { viewModelFactory }
    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        blockTitleUpdates()
        setupFragment()
        bindViewModel()
        viewModel.fetchData(argumentsHelper.getDocumentId(arguments))
    }

    private fun setupFragment() {
        binding.download.setOnClickListener {
            val filePath = viewModel.viewState.value?.filename ?: return@setOnClickListener
            val uri = FileProvider.getUriForFile(requireContext(), "${requireActivity().packageName}.provider", File(filePath))
            val intent = ShareCompat.IntentBuilder(requireActivity())
                .setStream(uri)
                .setType("application/pdf")
                .createChooserIntent()
                .addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

            requireContext().startActivity(intent)
        }
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it.status) {
                    DocumentDetailsViewModel.ViewState.Status.Success -> {
                        it.filename?.let { filename ->
                            binding.pdfView.loadFile(File(filename))
                        }
                        binding.download.visibility = View.VISIBLE
                    }
                    is DocumentDetailsViewModel.ViewState.Status.Error -> {
                        binding.download.visibility = View.GONE
                        snackbarUtil.retrySnackbar { it.status.retry() }
                    }
                }
                toolbarTitleView?.apply {
                    val title = it.title
                    text = title
                    contentDescription = title
                }
            }
        )
    }
}
