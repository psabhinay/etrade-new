package com.etrade.mobilepro.compose.etdialogs.elements

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.compose.etdialogs.R

@Composable
internal fun DialogContainer(properties: DialogProperties, onDismiss: () -> Unit, content: @Composable () -> Unit) {
    Dialog(properties = properties, onDismissRequest = onDismiss) {
        Surface(
            modifier = Modifier.padding(horizontal = Dimens.Size48dp).wrapContentSize(),
            shape = MaterialTheme.shapes.medium.copy(all = CornerSize(Dimens.Size8dp)),
            elevation = Dimens.Size4dp,
            color = colorResource(R.color.colorBackground)
        ) {
            content()
        }
    }
}
