package com.etrade.mobilepro.news.presentation.detail

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentLoader
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsFontAdjustHandler
import javax.inject.Inject

class TextNewsViewModel @Inject constructor(
    val contentLoader: TextNewsContentLoader,
    val fontAdjustHandler: TextNewsFontAdjustHandler,
    val statusRepo: ReadStatusRepo
) : ViewModel() {

    override fun onCleared() {
        contentLoader.unsubscribe()
        super.onCleared()
    }
}
