package com.etrade.mobilepro.news.data.repo.datasource

import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.data.rest.NewsApiClient
import com.etrade.mobilepro.watchlist.repo.CombinedWatchlistRepo
import com.etrade.mobilepro.watchlist.repo.DeviceWatchlistRepo
import io.reactivex.Observable

internal class NewsDataSourceFactory(
    private val apiClient: NewsApiClient,
    private val deviceWatchlistRepo: DeviceWatchlistRepo,
    private val combinedWatchlistRepo: CombinedWatchlistRepo
) {

    fun getNewsDataSource(type: NewsType, requestParams: NewsRequest? = null, headerItemsSource: Observable<List<NewsItem>>?) =
        when (type) {
            NewsType.MARKET -> MarketNewsDataSource(apiClient)
            NewsType.WATCHLIST -> WatchlistNewsDataSource(apiClient, deviceWatchlistRepo, combinedWatchlistRepo, requestParams)
            NewsType.PORTFOLIO -> PortfolioNewsDataSource(apiClient, requestParams)
            NewsType.COMPANY -> CompanyNewsDataSource(apiClient, requestParams, headerItemsSource)
            else -> throw IllegalArgumentException("Unsupported news type: $this")
        }
}
