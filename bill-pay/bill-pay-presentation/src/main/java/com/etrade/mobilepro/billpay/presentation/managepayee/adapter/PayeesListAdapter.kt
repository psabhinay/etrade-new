package com.etrade.mobilepro.billpay.presentation.managepayee.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.util.android.extension.dispatchUpdates

internal class PayeesListAdapter(
    private val clickListener: (Payee) -> Unit
) : RecyclerView.Adapter<PayeesListViewHolder>() {
    var items: List<Payee> = emptyList()
        set(value) {
            val oldValue = field
            field = value
            dispatchUpdates(oldValue, value)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PayeesListViewHolder =
        PayeesListViewHolder(parent, clickListener)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PayeesListViewHolder, position: Int) = holder.bind(items[position])
}
