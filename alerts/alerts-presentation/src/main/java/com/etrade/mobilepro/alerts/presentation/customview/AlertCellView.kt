package com.etrade.mobilepro.alerts.presentation.customview

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import androidx.core.widget.TextViewCompat
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.presentation.AlertParcelableContent
import com.etrade.mobilepro.alerts.presentation.R
import com.etrade.mobilepro.alerts.presentation.databinding.ViewAlertCellBinding
import com.etrade.mobilepro.util.android.extension.inflater

class AlertCellView : RelativeLayout {

    private val binding = ViewAlertCellBinding.inflate(inflater, this, true)

    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    fun toggleDisclosureArrowVisibility(isVisible: Boolean) {
        binding.arrowRightIv.visibility = if (isVisible) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    fun setCheckboxVisibility(visibility: Int) {
        binding.alertCheckbox.visibility = visibility
    }

    fun bindTo(item: AlertItem.Alert, wasRead: Boolean? = null, titleMaxLinesLimit: Int = 1) {
        with(binding) {
            titleMaxLinesLimit.let {
                alertTitleTv.apply {
                    maxLines = it
                    ellipsize = TextUtils.TruncateAt.END
                }
            }
            alertTitleTv.text = item.subject
            dateTimeTv.text = item.occurredAtFormatted
            wasRead?.let { updateForReadState(wasRead) }

            with(alertCheckbox) {
                if (item.status.isEditable) {
                    visibility = VISIBLE
                    isChecked = item.status.isMarked
                } else {
                    visibility = GONE
                }
            }
        }
    }

    private fun updateForReadState(wasRead: Boolean) {
        val titleStyle =
            if (wasRead) {
                R.style.TextAppearance_Etrade_Title_Read30
            } else {
                R.style.TextAppearance_Etrade_Title
            }
        val dateTimeStyle =
            if (wasRead) {
                R.style.TextAppearance_Etrade_Text_Regular_Medium_Read
            } else {
                R.style.TextAppearance_Etrade_Text_Regular_Medium
            }
        TextViewCompat.setTextAppearance(binding.alertTitleTv, titleStyle)
        TextViewCompat.setTextAppearance(binding.dateTimeTv, dateTimeStyle)
    }

    fun bindTo(item: AlertParcelableContent?) {
        binding.apply {
            alertTitleTv.text = item?.subject
            dateTimeTv.text = item?.occurredAtFormatted
        }
    }
}
