package com.etrade.mobilepro.transfermoney.presentation.transfer.source

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.etrade.mobilepro.transfermoney.api.repo.TransferStockAccountsDataSource
import com.jraska.livedata.test
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import java.math.BigInteger

@RunWith(AndroidJUnit4::class)
internal class DefaultTransferSourceAdapterTest {
    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    lateinit var context: Context

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        context = getApplicationContext()
    }

    private val stockAccountsDataSource: TransferStockAccountsDataSource = object : TransferStockAccountsDataSource {
        override suspend fun getStockAccountIds(): Set<String> = setOf(testStockAccount1.accountId, testStockAccount2.accountId)
    }

    @Test
    fun `By default all NON-IRA and NON-STOCK accounts are available for selection in from and to fields`() {
        val sut = createSut()
        val testInput = MutableLiveData<TransferSourceHolder>()
        val result = sut.adaptTransferSourceHolderToSelectable(testInput) {}

        testInput.value = TestTransferSourceHolder(accounts = listOf(testAccount1, testAccount2))

        result.from
            .test()
            .awaitValue()
            .assertValue {
                it.size == 2
            }
        result.to
            .test()
            .awaitValue()
            .assertValue {
                it.size == 2
            }
    }

    @Test
    fun `Selecting account in From field makes it unavailable in To field`() {
        val sut = createSut()
        val testInput = MutableLiveData<TransferSourceHolder>()
        val result = sut.adaptTransferSourceHolderToSelectable(testInput) {}

        testInput.value = TestTransferSourceHolder(accounts = listOf(testAccount1, testAccount2), selectedFrom = testAccount1)
        result.from
            .test()
            .awaitValue()
            .assertValue {
                it.size == 2
            }
        result.to
            .test()
            .awaitValue()
            .assertValue {
                it.size == 1 && it[0].value == testAccount2
            }
    }

    @Test
    fun `Selecting account in To field makes it unavailable in From field`() {
        val sut = createSut()
        val testInput = MutableLiveData<TransferSourceHolder>()
        val result = sut.adaptTransferSourceHolderToSelectable(testInput) {}

        testInput.value = TestTransferSourceHolder(accounts = listOf(testAccount1, testAccount2), selectedTo = testAccount1)
        result.to
            .test()
            .awaitValue()
            .assertValue {
                it.size == 2
            }
        result.from
            .test()
            .awaitValue()
            .assertValue {
                it.size == 1 && it[0].value == testAccount2
            }
    }

    @Test
    fun `Stock accounts are not shown in From and To selection lists`() {
        val sut = createSut()
        val testInput = MutableLiveData<TransferSourceHolder>()
        val result = sut.adaptTransferSourceHolderToSelectable(testInput) {}

        testInput.value = TestTransferSourceHolder(accounts = listOf(testAccount1, testStockAccount1, testStockAccount2))

        result.to
            .test()
            .awaitValue()
            .assertValue {
                it.size == 1 && it[0].value == testAccount1
            }
        result.from
            .test()
            .awaitValue()
            .assertValue {
                it.size == 1 && it[0].value == testAccount1
            }
    }

    @Test
    fun `IRA accounts are not shown in From and shown in To lists`() {
        val sut = createSut()
        val testInput = MutableLiveData<TransferSourceHolder>()
        val result = sut.adaptTransferSourceHolderToSelectable(testInput) {}

        testInput.value = TestTransferSourceHolder(accounts = listOf(testAccount1, testIRAAccount1, testIRAAccount2))

        result.to
            .test()
            .awaitValue()
            .assertValue {
                it.size == 3
            }
        result.from
            .test()
            .awaitValue()
            .assertValue {
                it.size == 1 && it[0].value == testAccount1
            }
    }

    @Test
    fun `If in From field External account is set then selecting other External account in To field invokes callback with new holder state`() {
        val sut = createSut()
        val testInput = MutableLiveData<TransferSourceHolder>()
        var callCount = 0
        var newHolder: TransferSourceHolder? = null
        val result = sut.adaptTransferSourceHolderToSelectable(testInput) {
            callCount++
            newHolder = it
        }

        val initialState = TestTransferSourceHolder(
            accounts = listOf(testAccount1, testExternalAccount1, testExternalAccount2),
            selectedFrom = testExternalAccount1
        )

        result.from.test()

        testInput.value = initialState
        Assert.assertEquals(0, callCount)

        testInput.value = initialState.copyAsHolder(selectedTo = testExternalAccount2)

        Assert.assertEquals(1, callCount)
        Assert.assertEquals(true, newHolder != null)
        Assert.assertEquals(true, newHolder?.selectedFrom == null)
    }

    @Test
    fun `If in To field External account is set then selecting other External account in From field invokes callback with new holder state`() {
        val sut = createSut()
        val testInput = MutableLiveData<TransferSourceHolder>()
        var callCount = 0
        var newHolder: TransferSourceHolder? = null
        val result = sut.adaptTransferSourceHolderToSelectable(testInput) {
            callCount++
            newHolder = it
        }

        val initialState = TestTransferSourceHolder(
            accounts = listOf(testAccount1, testExternalAccount1, testExternalAccount2),
            selectedTo = testExternalAccount1
        )

        result.to.test()

        testInput.value = initialState
        Assert.assertEquals(0, callCount)

        testInput.value = initialState.copyAsHolder(selectedFrom = testExternalAccount2)

        Assert.assertEquals(1, callCount)
        Assert.assertEquals(true, newHolder != null)
        Assert.assertEquals(true, newHolder?.selectedTo == null)
    }

    @Test
    fun `Backend-set flags hide item from the correspoding lists - From`() {
        val sut = createSut()
        val testInput = MutableLiveData<TransferSourceHolder>()
        val result = sut.adaptTransferSourceHolderToSelectable(testInput) {}

        testInput.value = TestTransferSourceHolder(accounts = listOf(testAccount1, testAccount3NoFrom))

        result.to
            .test()
            .awaitValue()
            .assertValue {
                it.size == 2
            }
        result.from
            .test()
            .awaitValue()
            .assertValue {
                it.size == 1 && it[0].value == testAccount1
            }
    }

    @Test
    fun `Backend-set flags hide item from the corresponding lists - To`() {
        val sut = createSut()
        val testInput = MutableLiveData<TransferSourceHolder>()
        val result = sut.adaptTransferSourceHolderToSelectable(testInput) {}

        testInput.value = TestTransferSourceHolder(accounts = listOf(testAccount1, testAccount4NoTo))

        result.from
            .test()
            .awaitValue()
            .assertValue {
                it.size == 2
            }
        result.to
            .test()
            .awaitValue()
            .assertValue {
                it.size == 1 && it[0].value == testAccount1
            }
    }

    private fun createSut(): DefaultTransferSourceAdapter = DefaultTransferSourceAdapter(
        context.resources,
        DefaultTransferSourceFilterer(stockAccountsDataSource)
    )

    private class TestTransferSourceHolder(
        override val accounts: List<TransferMoneyAccount> = emptyList(),
        override val selectedFrom: TransferMoneyAccount? = null,
        override val selectedTo: TransferMoneyAccount? = null
    ) : TransferSourceHolder

    private val testAccount1 = TransferMoneyAccount(
        title = "title1",
        accountId = "id1",
        nickname = null,
        instNo = BigInteger.ZERO,
        accountType = "some type",
        accountCategory = "some category",
        canBeTo = true,
        canBeFrom = true
    )

    private val testAccount2 = TransferMoneyAccount(
        title = "title2",
        accountId = "id2",
        nickname = null,
        instNo = BigInteger.ZERO,
        accountType = "some type",
        accountCategory = "some category",
        canBeTo = true,
        canBeFrom = true
    )

    private val testAccount3NoFrom = TransferMoneyAccount(
        title = "title2",
        accountId = "id2",
        nickname = null,
        instNo = BigInteger.ZERO,
        accountType = "some type",
        accountCategory = "some category",
        canBeTo = true,
        canBeFrom = false
    )

    private val testAccount4NoTo = TransferMoneyAccount(
        title = "title2",
        accountId = "id2",
        nickname = null,
        instNo = BigInteger.ZERO,
        accountType = "some type",
        accountCategory = "some category",
        canBeTo = false,
        canBeFrom = true
    )

    private val testIRAAccount1 = TransferMoneyAccount(
        title = "title1IRA",
        accountId = "id1IRA",
        nickname = null,
        instNo = BigInteger.ZERO,
        accountType = "some type",
        accountCategory = "ira",
        canBeTo = true,
        canBeFrom = true
    )

    private val testIRAAccount2 = TransferMoneyAccount(
        title = "title2IRA",
        accountId = "id2IRA",
        nickname = null,
        instNo = BigInteger.ZERO,
        accountType = "some type",
        accountCategory = "ira",
        canBeTo = true,
        canBeFrom = true
    )

    private val testExternalAccount1 = TransferMoneyAccount(
        title = "title1EXT",
        accountId = "id1EXT",
        nickname = null,
        instNo = BigInteger.ZERO,
        accountType = "external",
        accountCategory = "some cat",
        canBeTo = true,
        canBeFrom = true
    )

    private val testExternalAccount2 = TransferMoneyAccount(
        title = "title2EXT",
        accountId = "id2EXT",
        nickname = null,
        instNo = BigInteger.ZERO,
        accountType = "external",
        accountCategory = "some cat",
        canBeTo = true,
        canBeFrom = true
    )

    private val testStockAccount1 = TransferMoneyAccount(
        title = "title1Stock",
        accountId = "id1StockAccount",
        nickname = null,
        instNo = BigInteger.ZERO,
        accountType = "some type",
        accountCategory = "some category", // it is the accountId that matters here
        canBeTo = true,
        canBeFrom = true
    )

    private val testStockAccount2 = TransferMoneyAccount(
        title = "title2Stock",
        accountId = "id2StockAccount",
        nickname = null,
        instNo = BigInteger.ZERO,
        accountType = "some type",
        accountCategory = "some category", // it is the accountId that matters here
        canBeTo = true,
        canBeFrom = true
    )
}
