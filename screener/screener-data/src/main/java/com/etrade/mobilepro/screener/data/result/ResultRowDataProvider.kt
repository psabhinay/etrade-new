package com.etrade.mobilepro.screener.data.result

import com.etrade.mobilepro.screener.api.result.PrescreenedQuote
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.streaming.api.Level1Data

interface ResultRowDataProvider {
    fun getRowData(
        columnOrder: Collection<ResultColumn>,
        entry: PrescreenedQuote?
    ): List<ResultDataCell>

    fun updateRowData(row: List<ResultDataCell>, data: Level1Data, columns: List<ResultColumn>): List<ResultDataCell>
}
