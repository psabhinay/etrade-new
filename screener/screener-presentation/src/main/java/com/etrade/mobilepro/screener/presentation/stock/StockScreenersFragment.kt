package com.etrade.mobilepro.screener.presentation.stock

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.screener.api.ScreenerDisclosure
import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseScreenersFragment
import com.etrade.mobilepro.tracking.params.StockMarketsScreenerResultsAnalyticsParams
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class StockScreenersFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter
) : BaseScreenersFragment(snackbarUtilFactory, screenersRouter) {
    override val viewModel by viewModels<StockScreenersViewModel>({ requireParentFragment() }) { viewModelFactory }

    override val disclosureType: ScreenerDisclosure = ScreenerDisclosure.STOCK_PRIMARY

    override val resultNavListener: () -> Unit = {
        screenersRouter.actionToStockResultsFragment(findNavController())
        viewModel.trackScreenerResultPage(StockMarketsScreenerResultsAnalyticsParams())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
    }

    override val screenerSetSelectedListener: (ScreenerItem.ScreenerSet) -> Unit = {
        when (it.type) {
            ScreenerType.Stock.MarketSegment -> screenersRouter.actionScreenersContainerToStockMarketSegment(findNavController())
            ScreenerType.Stock.PriceAndVolume -> screenersRouter.actionScreenersContainerToStockPriceVolume(findNavController())
            ScreenerType.Stock.Opinions -> screenersRouter.actionScreenersContainerToOpinionsFragment(findNavController())
            ScreenerType.Stock.Fundamentals -> screenersRouter.actionScreenersContainerToFundamentalsFragment(findNavController())
            ScreenerType.Stock.EarningsAndDividends -> screenersRouter.actionScreenersContainerToEarningsAndDividendsFragment(findNavController())
            ScreenerType.Stock.Technicals -> screenersRouter.actionScreenersContainerToTechnicalsFragment(findNavController())
            else -> throw IllegalArgumentException("Wrong tab for ${it.type}")
        }
    }

    private fun setupFragment() {
    }
}
