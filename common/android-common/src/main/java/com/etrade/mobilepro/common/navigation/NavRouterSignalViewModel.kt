package com.etrade.mobilepro.common.navigation

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable

typealias NavRouterSignal = (NavRouter) -> Unit

interface NavRouterSignalViewModel {
    val navRouterSignal: LiveData<ConsumableLiveEvent<NavRouterSignal>>
}

fun MutableLiveData<ConsumableLiveEvent<NavRouterSignal>>.navigate(directions: NavDirections) {
    sendSignal {
        navigate(directions)
    }
}

fun MutableLiveData<ConsumableLiveEvent<NavRouterSignal>>.navigate(uri: Uri) {
    sendSignal {
        navigate(uri)
    }
}

fun MutableLiveData<ConsumableLiveEvent<NavRouterSignal>>.navigate(action: () -> Unit) {
    sendSignal {
        navigate(action)
    }
}

fun MutableLiveData<ConsumableLiveEvent<NavRouterSignal>>.navigateToLandingPage() {
    sendSignal {
        navigateToLandingPage()
    }
}

private inline fun MutableLiveData<ConsumableLiveEvent<NavRouterSignal>>.sendSignal(crossinline route: NavRouter.() -> Unit) {
    value = { router: NavRouter ->
        router.route()
    }.consumable()
}
