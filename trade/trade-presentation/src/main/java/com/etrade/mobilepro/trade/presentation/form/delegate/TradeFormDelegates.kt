package com.etrade.mobilepro.trade.presentation.form.delegate

import android.content.Context
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ConditionalTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.MutualFundTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossStockTradeFormInputId
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.ContingentOptionFactoryDelegateDependenciesImpl
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.ContingentStockFactoryDelegateDependenciesImpl
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.StockFactoryDelegateDependenciesImpl
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.StockOptionsFactoryDelegateDependenciesImpl
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.StopLossOptionsFactoryDelegateDependenciesImpl
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.StopLossStockFactoryDelegateDependenciesImpl
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.util.ValueCoder
import javax.inject.Inject
import javax.inject.Provider

class TradeFormDelegates @Inject constructor(
    context: Context,
    searchIntentFactory: SearchIntentFactory,
    conditionalOrderTradeFormInputId: ConditionalOrderTradeFormInputId,
    conditionalTradeFormInputId: ConditionalTradeFormInputId,
    contingentStockTradeFormInputId: ContingentStockTradeFormInputId,
    mutualFundTradeFormInputId: MutualFundTradeFormInputId,
    optionTradeFormInputId: OptionTradeFormInputId,
    stockTradeFormInputId: StockTradeFormInputId,
    contingentOptionTradeFormInputId: ContingentOptionTradeFormInputId,
    stopLossStockTradeFormInputId: StopLossStockTradeFormInputId,
    stopLossOptionTradeFormInputId: StopLossOptionTradeFormInputId,
    tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    optionsTradeLegsCoder: ValueCoder<List<TradeLeg>>,
    tradingDefaultsPreferences: TradingDefaultsPreferences
) {

    fun getFormDelegate(tradeOrderType: TradeFormType): TradeFormFactoryDelegate? = delegates[tradeOrderType]

    private val delegates: Map<TradeFormType, TradeFormFactoryDelegate>

    init {
        fun createOptionTradeFormFactoryDelegate(securityType: SecurityType): OptionTradeFormFactoryDelegate {
            return OptionTradeFormFactoryDelegate(
                context = context,
                inputId = optionTradeFormInputId,
                securityType = securityType,
                tradeFormTypeUpdateDelegate = TradeFormTypeUpdateDelegate(tradeFormParametersBuilderProvider),
                tradeLegsCoder = optionsTradeLegsCoder,
                tradingDefaultsPreferences = tradingDefaultsPreferences,
                dependencies = StockOptionsFactoryDelegateDependenciesImpl(
                    optionTradeFormInputId,
                    context.resources
                )
            )
        }

        fun createStockTradeFormFactoryDelegate(): StockTradeFormFactoryDelegate {
            return StockTradeFormFactoryDelegate(
                context = context,
                inputId = stockTradeFormInputId,
                dependencies = StockFactoryDelegateDependenciesImpl(
                    stockTradeFormInputId,
                    context.resources
                ),
                searchIntentFactory = searchIntentFactory,
                tradeFormTypeUpdateDelegate = TradeFormTypeUpdateDelegate(tradeFormParametersBuilderProvider)
            )
        }

        fun createContingentStockTradeFormFactoryDelegate(): ContingentStockTradeFormFactoryDelegate {
            return ContingentStockTradeFormFactoryDelegate(
                context = context,
                inputId = contingentStockTradeFormInputId,
                priceTypeDependenciesProvider = ContingentStockFactoryDelegateDependenciesImpl(
                    contingentStockTradeFormInputId,
                    context.resources
                ),
                searchIntentFactory = searchIntentFactory,
                tradeFormTypeUpdateDelegate = TradeFormTypeUpdateDelegate(tradeFormParametersBuilderProvider)
            )
        }

        fun createContingentOptionTradeFormFactoryDelegate(securityType: SecurityType): ContingentOptionTradeFormFactoryDelegate {
            return ContingentOptionTradeFormFactoryDelegate(
                context,
                searchIntentFactory,
                contingentOptionTradeFormInputId,
                securityType = securityType,
                tradeLegsCoder = optionsTradeLegsCoder,
                formFactoryDelegateDependencies = ContingentOptionFactoryDelegateDependenciesImpl(
                    contingentOptionTradeFormInputId,
                    context.resources
                ),
                tradeFormTypeUpdateDelegate = TradeFormTypeUpdateDelegate(tradeFormParametersBuilderProvider),
                tradingDefaultsPreferences = tradingDefaultsPreferences
            )
        }

        fun createStopLossOptionsTradeFormFactoryDelegate(): StopLossOptionTradeFormFactoryDelegate {
            return StopLossOptionTradeFormFactoryDelegate(
                context = context,
                inputId = stopLossOptionTradeFormInputId,
                tradeFormTypeUpdateDelegate = TradeFormTypeUpdateDelegate(tradeFormParametersBuilderProvider),
                tradeLegsCoder = optionsTradeLegsCoder,
                tradingDefaultsPreferences = tradingDefaultsPreferences,
                priceTypeDependenciesProvider = StopLossOptionsFactoryDelegateDependenciesImpl(
                    stopLossOptionTradeFormInputId,
                    context.resources
                )
            )
        }

        fun createStockStopLossTradeFormFactoryDelegate(): StopLossStockTradeFormFactoryDelegate {
            return StopLossStockTradeFormFactoryDelegate(
                context = context,
                inputId = stopLossStockTradeFormInputId,
                delegateDependencies = StopLossStockFactoryDelegateDependenciesImpl(
                    stopLossStockTradeFormInputId,
                    context.resources
                ),
                searchIntentFactory = searchIntentFactory,
                tradeFormTypeUpdateDelegate = TradeFormTypeUpdateDelegate(tradeFormParametersBuilderProvider)
            )
        }

        delegates = mapOf(
            TradeFormType(SecurityType.CONDITIONAL) to ConditionalTradeFormFactoryDelegate(
                context.resources,
                conditionalTradeFormInputId,
                conditionalOrderTradeFormInputId
            ),
            TradeFormType(SecurityType.MUTUAL_FUND) to MutualFundTradeFormDelegate(
                context,
                SecurityType.MUTUAL_FUND,
                mutualFundTradeFormInputId,
                searchIntentFactory
            ),
            TradeFormType(SecurityType.OPTION) to createOptionTradeFormFactoryDelegate(SecurityType.OPTION),
            TradeFormType(SecurityType.STOCK) to createStockTradeFormFactoryDelegate(),
            TradeFormType(SecurityType.STOCK, AdvancedTradeType.Contingent) to createContingentStockTradeFormFactoryDelegate(),
            TradeFormType(SecurityType.OPTION, AdvancedTradeType.Contingent) to createContingentOptionTradeFormFactoryDelegate(SecurityType.OPTION),
            TradeFormType(SecurityType.STOCK, AdvancedTradeType.StopLoss) to createStockStopLossTradeFormFactoryDelegate(),
            TradeFormType(SecurityType.OPTION, AdvancedTradeType.StopLoss) to createStopLossOptionsTradeFormFactoryDelegate(),
            TradeFormType(SecurityType.STOCK_AND_OPTIONS) to createOptionTradeFormFactoryDelegate(SecurityType.STOCK_AND_OPTIONS)
        )
    }
}
