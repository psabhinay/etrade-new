package com.etrade.mobilepro.documents.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.documents.StatefulDocument
import com.etrade.mobilepro.documents.databinding.DocumentsItemAdapterBinding
import com.etrade.mobilepro.util.android.extension.viewBinding

internal class DocumentsRecyclerViewAdapter(private val listener: (StatefulDocument) -> Unit) : RecyclerView.Adapter<DocumentViewHolder>() {

    private val items: MutableList<StatefulDocument> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocumentViewHolder {
        return DocumentViewHolder(
            parent.viewBinding(DocumentsItemAdapterBinding::inflate),
            listener
        )
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: DocumentViewHolder, position: Int) {
        holder.bind(items[position], items.getOrNull(position - 1))
    }

    fun updateItems(items: List<StatefulDocument>) {
        DiffUtil.calculateDiff(
            StatefulDocumentDiffCallback(
                this.items,
                items
            )
        ).dispatchUpdatesTo(this)
        this.items.clear()
        this.items.addAll(items)
    }
}

private class StatefulDocumentDiffCallback(private val oldList: List<StatefulDocument>, private val newList: List<StatefulDocument>) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition].document.documentId == newList[newItemPosition].document.documentId

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition] == newList[newItemPosition]
}
