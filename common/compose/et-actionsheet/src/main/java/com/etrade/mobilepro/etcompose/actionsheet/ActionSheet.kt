@file:Suppress("MagicNumber")
package com.etrade.mobilepro.etcompose.actionsheet

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ModalBottomSheetLayout
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.etColors
import kotlinx.coroutines.launch

private const val SCROLLABLE_BREAKPOINT = 10
private val SCROLLABLE_MAX_HEIGHT = 525.dp

@ExperimentalComposeUiApi
@ExperimentalMaterialApi
@Composable
fun ActionSheet(
    bottomSheetItems: List<ActionSheetItem>,
    isActionSheetVisible: Boolean,
    onChangeVisibility: (Boolean) -> Unit,
) {
    if (isActionSheetVisible) {
        val updatedItems = bottomSheetItems.map {
            it.copy(
                onClick = {
                    onChangeVisibility(false)
                    it.onClick()
                }
            )
        }
        val sheetState = rememberModalBottomSheetState(
            initialValue = ModalBottomSheetValue.Expanded
        ) {
            onChangeVisibility(it != ModalBottomSheetValue.Hidden)
            true
        }
        val scope = rememberCoroutineScope()
        Dialog(
            onDismissRequest = {
                scope.launch {
                    onChangeVisibility(false)
                    sheetState.hide()
                }
            },
            properties = DialogProperties(usePlatformDefaultWidth = false)
        ) {
            ModalBottomSheetLayout(
                modifier = Modifier
                    .fillMaxWidth()
                    .clipToBounds(),
                sheetContent = {
                    if (updatedItems.size <= SCROLLABLE_BREAKPOINT) {
                        ColumnBottomSheet(bottomSheetItems = updatedItems)
                    } else {
                        ScrollableBottomSheet(updatedItems)
                    }
                },
                sheetShape = RoundedCornerShape(topStart = Dimens.Size14dp, topEnd = Dimens.Size14dp),
                sheetState = sheetState,
                scrimColor = Color.Transparent
            ) {}
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun ScrollableBottomSheet(bottomSheetItems: List<ActionSheetItem>) {
    LazyColumn(
        modifier = Modifier
            .fillMaxWidth()
            .height(SCROLLABLE_MAX_HEIGHT)
    ) {
        items(bottomSheetItems) { item ->
            BottomSheetListItem(item)
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun ColumnBottomSheet(bottomSheetItems: List<ActionSheetItem>) {
    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        bottomSheetItems.forEach { item ->
            BottomSheetListItem(item)
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun BottomSheetListItem(item: ActionSheetItem) {
    Surface(
        onClick = item.onClick,
        modifier = Modifier.fillMaxWidth(),
        color = MaterialTheme.etColors.bottomSheetSurface,
    ) {
        Text(
            text = item.title,
            modifier = Modifier.padding(Dimens.Size16dp),
            color = MaterialTheme.etColors.bottomSheetTextColor
        )
    }
    Divider(color = MaterialTheme.etColors.lightGrey)
}

@ExperimentalComposeUiApi
@ExperimentalMaterialApi
@Composable
@Preview
fun ColumnBottomSheetPreview() {
    val (isActionSheetVisible, setActionSheetVisibility) = remember { mutableStateOf(true) }
    val items: List<ActionSheetItem> = listOf(
        ActionSheetItem("Item A"),
        ActionSheetItem("Item B"),
        ActionSheetItem("Item C")
    )
    ActionSheet(items, isActionSheetVisible, setActionSheetVisibility)
}

@ExperimentalComposeUiApi
@ExperimentalMaterialApi
@Composable
@Preview
fun ScrollableBottomSheetPreview() {
    val (isActionSheetVisible, setActionSheetVisibility) = remember { mutableStateOf(true) }
    var items: List<ActionSheetItem> = mutableListOf()
    for (i in 1..20) {
        items += ActionSheetItem("Item $i")
    }
    ActionSheet(items, isActionSheetVisible, setActionSheetVisibility)
}
