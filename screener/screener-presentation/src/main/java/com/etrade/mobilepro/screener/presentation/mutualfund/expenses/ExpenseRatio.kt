package com.etrade.mobilepro.screener.presentation.mutualfund.expenses

import com.etrade.mobilepro.screener.api.filter.mutualfund.ExpensesFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class ExpenseRatio : ScreenerFilterEvent<ExpensesFilter, ExpensesViewModel.ViewState, Boolean> {
    LESS_THAN_DOT_5 {
        override fun update(viewState: ExpensesViewModel.ViewState, eventValue: Boolean): ExpensesViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(expenseRatioLessThan5 = eventValue))
    },
    DOT_5_TO_1 {
        override fun update(viewState: ExpensesViewModel.ViewState, eventValue: Boolean): ExpensesViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(expenseRatio5to1 = eventValue))
    },
    ONE_TO_1_DOT_5 {
        override fun update(viewState: ExpensesViewModel.ViewState, eventValue: Boolean): ExpensesViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(expenseRatio1to15 = eventValue))
    },
    ONE_DOT_5_TO_2 {
        override fun update(viewState: ExpensesViewModel.ViewState, eventValue: Boolean): ExpensesViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(expenseRatio15to2 = eventValue))
    },
    GREATER_THAN_2 {
        override fun update(viewState: ExpensesViewModel.ViewState, eventValue: Boolean): ExpensesViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(expenseRatioGreaterThan2 = eventValue))
    }
}
