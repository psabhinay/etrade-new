package com.etrade.mobilepro.orders.presentation.section.viewfactory

import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection

class StockOrderSectionViewFactory : OrderDetailSectionViewFactory {
    override fun createSectionView(
        detailsDto: BaseOrderDetailsItem.OrderDetailsItem,
        resources: OrderDetailLabelResources,
        isConditionalOrder: Boolean
    ): List<TableSectionItemLayout> {
        val sections = mutableListOf<TableSectionItemLayout>()
        val stockResources = resources.defaultResources.resource
        sections.addAll(getDatesSections(detailsDto, resources, false))
        sections.addAll(getSavedInfo(detailsDto, resources, false))
        if (!detailsDto.hasMultipleLegs) {
            sections.add(createSummarySection(resources.stock.orderType, stockResources.getString(detailsDto.orderAction.getLabel())))
        }
        if (isConditionalOrder || (!detailsDto.hasPartialExecution && !detailsDto.hasMultipleLegs)) {
            sections.add(
                getQuantity(
                    detailsDto.quantity, detailsDto.execInstruction, isConditionalOrder,
                    resources, detailsDto.advancedItem?.orderType, detailsDto.term
                )
            )
        }
        sections.addAll(getPriceSections(detailsDto, resources, isConditionalOrder))
        return sections
    }
}
