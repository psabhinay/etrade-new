package com.etrade.mobilepro.transfermoney.presentation

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.tracking.SCREEN_TITLE_TRANSFER_MONEY
import com.etrade.mobilepro.tracking.SCREEN_TITLE_TRANSFER_MONEY_ACTIVITY
import com.etrade.mobilepro.tracking.TabsHostScreenViewModel
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelper
import javax.inject.Inject

class TransferMoneyContainerViewModel @Inject constructor(
    override val viewPagerTabsTrackerHelper: ViewPagerTabsTrackerHelper
) : ViewModel(), TabsHostScreenViewModel {

    override val tabNames: Array<String> = arrayOf(SCREEN_TITLE_TRANSFER_MONEY, SCREEN_TITLE_TRANSFER_MONEY_ACTIVITY)
}
