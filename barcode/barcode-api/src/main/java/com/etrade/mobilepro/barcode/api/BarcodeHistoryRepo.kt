package com.etrade.mobilepro.barcode.api

import androidx.lifecycle.LiveData

interface BarcodeHistoryRepo {

    fun saveBarcodeToHistory(barcodeItem: BarcodeItem)

    fun deleteBarcodeFromHistory(barcodeItem: BarcodeItem)

    fun getBarcodeHistory(): LiveData<List<BarcodeItem>>
}
