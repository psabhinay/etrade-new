package com.etrade.mobilepro.checkdeposit.deposit.service

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.checkdeposit.validate.ValidateDepositBusinessRulesDto
import com.etrade.mobilepro.checkdeposit.validate.ValidateDepositBusinessRulesResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RemoteCheckDepositService {

    @JsonMoshi
    @GET("app/remotecheckdeposit/captureserviceagreement.json")
    suspend fun captureserviceagreement(
        @Query("tc_version") termsAndConditionsVersion: Int = 2
    ): ServerResponseDto<DepositCaptureServiceAgreementResponseDto>

    @JsonMoshi
    @GET("app/remotecheckdeposit/geteligibleaccounts.json")
    suspend fun getEligibleAccounts(
        @Query("phoneType") phoneType: String = "",
        @Query("tc_version") termsAndConditionsVersion: Int = 2
    ): ServerResponseDto<DepositEligibleAccountsResponseDto>

    @JsonMoshi
    @POST("app/remotecheckdeposit/uploadimage.json")
    suspend fun uploadImage(@JsonMoshi @Body request: UploadImageDto): ServerResponseDto<UploadImageResponseDto>

    @JsonMoshi
    @POST("app/remotecheckdeposit/validatedepositbusinessrules.json")
    suspend fun validateDepositBusinessRules(
        @JsonMoshi @Body request: ValidateDepositBusinessRulesDto
    ): ServerResponseDto<ValidateDepositBusinessRulesResponseDto>

    @JsonMoshi
    @POST("app/remotecheckdeposit/depositimage.json")
    suspend fun depositimage(
        @JsonMoshi @Body request: DepositImageDto
    ): ServerResponseDto<DepositImageResponseDto>
}
