package com.etrade.mobilepro.transactions

import com.etrade.eo.rest.RetrofitBuilderFacade
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.dagger.ViewModelModule
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.transactions.api.TransactionsRepo
import com.etrade.mobilepro.transactions.api.TransactionsRequest
import com.etrade.mobilepro.transactions.api.TransactionsResponse
import com.etrade.mobilepro.transactions.data.CachedTransactionDataSourceDelegate
import com.etrade.mobilepro.transactions.data.TransactionsRepoImpl
import com.etrade.mobilepro.util.delegate.getValue
import com.etrade.mobilepro.util.delegate.setValue
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
abstract class TransactionsModule {

    @Binds
    @Singleton
    abstract fun bindCachedTransactionDataSourceDelegate(
        delegate: CachedTransactionDataSourceDelegate
    ): SimpleCachedResource.DataSourceDelegate<TransactionsResponse, TransactionsRequest>

    @Binds
    @Singleton
    abstract fun provideTransactionRepo(transactionsRepoImpl: TransactionsRepoImpl): TransactionsRepo

    companion object {
        @Provides
        @Singleton
        fun provideTransactionServiceProvider(
            retrofitBuilderFacade: RetrofitBuilderFacade,
            appConfigRepo: AppConfigRepo
        ): TransactionsServiceProvider {
            return TransactionsLibrary.createMobileProTransactionsServiceProvider(
                retrofitBuilderFacade,
                appConfigRepo.getAppConfig().baseUrls.mobileTrade,
                AndroidSchedulers.mainThread()
            )
        }

        @Provides
        @Singleton
        fun provideSweepPreferences(userPreferences: UserPreferences): SweepPreferences {
            return object : SweepPreferences {
                override var isSweepActivityEnabled: Boolean by userPreferences::isSweepActivityEnabled
            }
        }
    }
}
