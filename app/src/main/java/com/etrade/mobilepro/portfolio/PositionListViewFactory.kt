package com.etrade.mobilepro.portfolio

import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.backends.mgs.PaginationMeta
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.portfolio.action.PortfolioRecordActionProvider
import com.etrade.mobilepro.portfolio.delegate.PortfolioSortDelegate
import com.etrade.mobilepro.positions.data.dto.PositionListViewDto
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.getTimestampString
import java.util.concurrent.Executor
import javax.inject.Inject
import javax.inject.Provider

@Suppress("LongParameterList")
class PositionListViewFactory @Inject constructor(
    private val accountRepo: DefaultAccountRepo,
    private val resources: Resources,
    private val dataColorResolver: DataColorResolver,
    private val getPortfolioNextPageUseCase: GetPortfolioNextPageUseCase,
    private val portfolioRecordActionProvider: PortfolioRecordActionProvider,
    private val daoProvider: CachedDaoProvider,
    private val mainThreadExecutor: Executor,
    private val marketTradeStatusDelegate: MarketTradeStatusDelegate,
    private val streamingControllerProvider: Provider<StreamingSubscriptionController>,
    private val streamingStatusController: StreamingStatusController,
    private val sortDelegate: PortfolioSortDelegate,
    private val portfolioSettingsRepository: PortfolioSettingsRepository
) : DynamicUiViewFactory {

    private val timestamp: String
        get() = getTimestampString(resources)

    @Suppress("LongMethod")
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is PositionListViewDto) {
            val initialPositions = dto.data?.positions.orEmpty().map { it.id }
            val nextPageToken = meta.filterIsInstance<PaginationMeta>().firstOrNull()?.nextPage?.continuationToken

            return object : GenericLayout {
                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    PortfolioRecordsViewModel(
                        dto.data?.accountUuid.orEmpty(),
                        accountRepo,
                        dataColorResolver,
                        getPortfolioNextPageUseCase,
                        portfolioRecordActionProvider,
                        daoProvider,
                        mainThreadExecutor,
                        marketTradeStatusDelegate,
                        streamingControllerProvider.get(),
                        streamingStatusController,
                        resources,
                        sortDelegate,
                        portfolioSettingsRepository
                    )
                }

                override val viewModelClass: Class<out DynamicViewModel> = PortfolioRecordsViewModel::class.java

                override val uniqueIdentifier: String = "${javaClass.name}_${dto.data?.accountUuid}"

                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is PortfolioRecordsViewModel) {
                        viewModel.refresh(initialPositions, timestamp, nextPageToken)
                    }
                }
            }
        } else {
            throw DynamicUiViewFactoryException("PositionListViewFactory registered to unknown type")
        }
    }

    override fun setupValuesFromRequest(screenRequest: ScreenRequest) {
        sortDelegate.setPositionsViewType(screenRequest is ServicePath.AllBrokeragePositions)
    }
}
