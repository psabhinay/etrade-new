package com.etrade.mobilepro.barcode.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.barcode.api.BarcodeHistoryRepo
import com.etrade.mobilepro.barcode.api.BarcodeItem
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

class BarcodeResultsViewModel @Inject constructor(
    private val barcodeHistoryRepo: BarcodeHistoryRepo
) : ViewModel() {

    private val _barcodeDeletedEvent: MutableLiveData<ConsumableLiveEvent<Any>> = MutableLiveData()

    val barcodeDeletedEvent: LiveData<ConsumableLiveEvent<Any>> = _barcodeDeletedEvent
    val result: LiveData<List<BarcodeItem>> = barcodeHistoryRepo.getBarcodeHistory()

    fun deleteBarcode(barcode: BarcodeItem) {
        barcodeHistoryRepo.deleteBarcodeFromHistory(barcode)

        _barcodeDeletedEvent.value = ConsumableLiveEvent(Any())
    }
}
