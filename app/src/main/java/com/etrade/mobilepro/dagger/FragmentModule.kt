package com.etrade.mobilepro.dagger

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.accounts.AccountsOrganizeOnboardingFragment
import com.etrade.mobilepro.alerts.AccountAlertsFragment
import com.etrade.mobilepro.alerts.AlertDetailFragment
import com.etrade.mobilepro.alerts.AlertsContainerFragment
import com.etrade.mobilepro.alerts.ManageAlertFragment
import com.etrade.mobilepro.alerts.MarketAlertsFragment
import com.etrade.mobilepro.alerts.SetAlertFragment
import com.etrade.mobilepro.alerts.UserAlertDetailFragment
import com.etrade.mobilepro.appwidget.AddSymbolToWidgetWatchlistFragment
import com.etrade.mobilepro.appwidget.watchlist.selection.WatchlistSelectionFragment
import com.etrade.mobilepro.balance.BalancesFragment
import com.etrade.mobilepro.bank.BankDateTransactionPickerFragment
import com.etrade.mobilepro.bank.BankHomeFragment
import com.etrade.mobilepro.barcode.BarcodeFragment
import com.etrade.mobilepro.barcode.fragment.BarcodeResultsFragment
import com.etrade.mobilepro.barcode.fragment.BarcodeScanFragment
import com.etrade.mobilepro.billpay.presentation.BillPayContainerFragment
import com.etrade.mobilepro.billpay.presentation.addpayment.AddPaymentFragment
import com.etrade.mobilepro.billpay.presentation.editpayment.EditPaymentFragment
import com.etrade.mobilepro.billpay.presentation.managepayee.ManagePayeeFragment
import com.etrade.mobilepro.billpay.presentation.managepayee.details.PayeeDetailFragment
import com.etrade.mobilepro.billpay.presentation.paymentdetail.PaymentDetailFragment
import com.etrade.mobilepro.billpay.presentation.paymentlist.PaymentListFragment
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticationFragment
import com.etrade.mobilepro.biometric.presentation.enabler.BiometricEnablerFragment
import com.etrade.mobilepro.brokerage.AllBrokerageAccountsFragment
import com.etrade.mobilepro.chart.ChartBottomSheetFragment
import com.etrade.mobilepro.chart.ChartsFragment
import com.etrade.mobilepro.chart.netassets.NetAssetsChartFragment
import com.etrade.mobilepro.chart.performance.PerformanceChartFragment
import com.etrade.mobilepro.chartiq.presentation.ChartIqFragment
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesFragment
import com.etrade.mobilepro.chat.presentation.setup.ChatSetupFragment
import com.etrade.mobilepro.checkdeposit.presentation.CheckDepositLandingFragment
import com.etrade.mobilepro.checkdeposit.presentation.activity.CheckDepositActivityFragment
import com.etrade.mobilepro.checkdeposit.presentation.confirmation.DepositConfirmationFragment
import com.etrade.mobilepro.checkdeposit.presentation.contributiondetails.IraContributionAmountsFragment
import com.etrade.mobilepro.checkdeposit.presentation.deposit.CheckDepositFragment
import com.etrade.mobilepro.checkdeposit.presentation.details.DepositDetailsFragment
import com.etrade.mobilepro.checkdeposit.presentation.serviceagreement.CheckDepositServiceAgreementFragment
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.companyoverview.CompanyOverviewFragment
import com.etrade.mobilepro.completeview.CompleteViewFragment
import com.etrade.mobilepro.dagger.factory.FragmentInjectionFactory
import com.etrade.mobilepro.dagger.qualifiers.AmericanDateOnly
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.devtemplate.DevTemplateFragment
import com.etrade.mobilepro.earnings.EarningsDetailsFragment
import com.etrade.mobilepro.earnings.EarningsDetailsOverlayFragment
import com.etrade.mobilepro.education.EducationFragment
import com.etrade.mobilepro.feature.presentation.FeatureFlagsFragment
import com.etrade.mobilepro.formsandapplications.presentation.AccountServicesWebViewFragment
import com.etrade.mobilepro.home.AccountsHomeFragment
import com.etrade.mobilepro.home.AccountsTabHostFragment
import com.etrade.mobilepro.home.ManagedOverviewFragment
import com.etrade.mobilepro.home.OverviewFragment
import com.etrade.mobilepro.home.TradeAppActionRedirectFragment
import com.etrade.mobilepro.home.TransferMoneyAppActionRedirectFragment
import com.etrade.mobilepro.level2.QuoteLevel2Fragment
import com.etrade.mobilepro.markets.MarketsFragment
import com.etrade.mobilepro.markets.futures.MarketsFuturesFragment
import com.etrade.mobilepro.markets.movers.MarketsMoversFragment
import com.etrade.mobilepro.markets.overview.MarketsOverviewFragment
import com.etrade.mobilepro.menu.AccountDetailsMenuFragment
import com.etrade.mobilepro.menu.CustomerServiceMenuFragment
import com.etrade.mobilepro.menu.ExtendedHoursDisclosureWebViewFragment
import com.etrade.mobilepro.menu.MainMenuFragment
import com.etrade.mobilepro.menu.MenuDataReplacementWebViewFragment
import com.etrade.mobilepro.menu.MenuWebViewFragment
import com.etrade.mobilepro.menu.ProfileMenuFragment
import com.etrade.mobilepro.menu.TransferMenuFragment
import com.etrade.mobilepro.menu.VerifyAccountBeforeOpenAccountDialogFragment
import com.etrade.mobilepro.menu.VerifyAccountBeforeWebViewDialogFragment
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsWebViewFragment
import com.etrade.mobilepro.newaccount.NewAccountWebViewFragment
import com.etrade.mobilepro.news.presentation.NewsContainerFragment
import com.etrade.mobilepro.news.presentation.company.CompanyNewsFragment
import com.etrade.mobilepro.news.presentation.detail.TextNewsDetailFragment
import com.etrade.mobilepro.news.presentation.market.MarketNewsFragment
import com.etrade.mobilepro.news.presentation.portfolio.PortfolioNewsFragment
import com.etrade.mobilepro.news.presentation.watchlist.WatchlistNewsFragment
import com.etrade.mobilepro.optionschain.presentation.OptionsChainDisclosureDialogFragment
import com.etrade.mobilepro.optionschain.presentation.OptionsChainFragment
import com.etrade.mobilepro.orders.OrderBottomSheetFragment
import com.etrade.mobilepro.orders.OrderDetailsFragment
import com.etrade.mobilepro.orders.OrdersFragment
import com.etrade.mobilepro.orders.OrdersListFragment
import com.etrade.mobilepro.orders.TradeOrderDetailsFragment
import com.etrade.mobilepro.pdf.presentation.PdfFragment
import com.etrade.mobilepro.personalnotifications.fragment.PersonalNotificationsFragment
import com.etrade.mobilepro.personalnotifications.fragment.VerifyExternalAccountWebViewFragment
import com.etrade.mobilepro.portfolio.PortfolioFragment
import com.etrade.mobilepro.portfolio.drip.DripWebViewFragment
import com.etrade.mobilepro.portfolio.extendedsummary.ExtendedPortfolioSummaryBottomSheetFragment
import com.etrade.mobilepro.portfolio.extendedsummary.ExtendedPortfolioSummaryFragment
import com.etrade.mobilepro.portfolio.taxlot.PortfolioTaxLotBottomSheetFragment
import com.etrade.mobilepro.portfolio.taxlot.PortfolioTaxLotFragment
import com.etrade.mobilepro.quotelookup.options.call.StockCallOptionLookupFragment
import com.etrade.mobilepro.quotelookup.options.put.StockPutOptionLookupFragment
import com.etrade.mobilepro.quotelookup.stock.StockQuoteLookupFragment
import com.etrade.mobilepro.quotelookup.stock.StockQuoteSearchFragment
import com.etrade.mobilepro.quotes.PerformanceFragment
import com.etrade.mobilepro.quotes.ProspectusFragment
import com.etrade.mobilepro.quotes.QuoteBottomSheetFragment
import com.etrade.mobilepro.quotes.QuoteFragment
import com.etrade.mobilepro.researchreport.ReportsFragment
import com.etrade.mobilepro.settings.SettingsFragment
import com.etrade.mobilepro.stockplan.StockPlanWebViewFragment
import com.etrade.mobilepro.stockplan.exercise.SellAndExerciseFragment
import com.etrade.mobilepro.stockplan.holdings.HoldingsFragment
import com.etrade.mobilepro.stockplan.orders.StockOrdersWebFragment
import com.etrade.mobilepro.themeselection.ThemeSelectionFragment
import com.etrade.mobilepro.timeout.widget.SessionTerminatedFragment
import com.etrade.mobilepro.trade.TradeBottomSheetFragment
import com.etrade.mobilepro.trade.TradeRouterFragment
import com.etrade.mobilepro.trade.approvallevel.ApprovalLevelFragment
import com.etrade.mobilepro.trade.calculator.TradeCalculatorFragment
import com.etrade.mobilepro.trade.dropdown.TradeSelectFragment
import com.etrade.mobilepro.trade.help.TradeHelpFragment
import com.etrade.mobilepro.trade.mutualfund.presentation.MutualFundTradeFragment
import com.etrade.mobilepro.trade.optionlegselect.OptionLegSelectFragment
import com.etrade.mobilepro.trade.presentation.TradeFragment
import com.etrade.mobilepro.trade.presentation.conditional.ConditionalOrderFormFragment
import com.etrade.mobilepro.trade.presentation.confirmation.OrderConfirmationFragment
import com.etrade.mobilepro.trade.presentation.disclosures.LiEtfAgreementWebViewFragment
import com.etrade.mobilepro.trade.presentation.disclosures.TradeAgreementWebViewFragment
import com.etrade.mobilepro.tradingdefaults.TradingDefaultsFragment
import com.etrade.mobilepro.transactions.view.TransactionsFragment
import com.etrade.mobilepro.transfermoney.presentation.TransferMoneyPopupDialogFragment
import com.etrade.mobilepro.transfermoney.presentation.transfer.ExternalAccountsWebViewFragment
import com.etrade.mobilepro.usersubscriptionpresentation.MarketDataSubscriptionPromptDialogFragment
import com.etrade.mobilepro.usersubscriptionpresentation.RealTimeQuoteStreamingPromptDialogFragment
import com.etrade.mobilepro.usersubscriptionpresentation.UserSubscriptionsWebViewFragment
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.chromium.customtabsclient.shared.NavigableWebViewFragment
import org.chromium.customtabsclient.shared.PdfWebviewFragment
import org.chromium.customtabsclient.shared.WebViewFragment
import org.chromium.customtabsclient.shared.WebViewHelper
import org.threeten.bp.format.DateTimeFormatter

@Suppress("LargeClass")
@Module(
    includes = [
        BillPayFragmentModule::class,
        MarketsCalendarFragmentModule::class,
        ScreenersFragmentModule::class,
        TransferMoneyFragmentModule::class,
        MessageCenterFragmentModule::class
    ]
)
abstract class FragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(ChatMessagesFragment::class)
    internal abstract fun bindChatMessagesFragment(fragment: ChatMessagesFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ChatSetupFragment::class)
    internal abstract fun bindChatSetupFragment(fragment: ChatSetupFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ConditionalOrderFormFragment::class)
    abstract fun bindConditionalOrderFormFragment(fragment: ConditionalOrderFormFragment): Fragment

    @Binds
    abstract fun bindFragmentFactory(factory: FragmentInjectionFactory): FragmentFactory

    @Binds
    @IntoMap
    @FragmentKey(AddPaymentFragment::class)
    internal abstract fun bindAddPaymentFragment(fragment: AddPaymentFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AddSymbolToWidgetWatchlistFragment::class)
    internal abstract fun bindAddSymbolToWidgetWatchlistFragment(fragment: AddSymbolToWidgetWatchlistFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EditPaymentFragment::class)
    internal abstract fun bindEditPaymentFragment(fragment: EditPaymentFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ExtendedPortfolioSummaryFragment::class)
    internal abstract fun bindExtendedPortfolioSummaryFragment(fragment: ExtendedPortfolioSummaryFragment): Fragment

    @ExperimentalCoroutinesApi
    @Binds
    @IntoMap
    @FragmentKey(FeatureFlagsFragment::class)
    abstract fun bindFeatureFlagFragment(fragment: FeatureFlagsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PaymentListFragment::class)
    internal abstract fun bindPaymentsFragment(fragment: PaymentListFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PaymentDetailFragment::class)
    internal abstract fun bindPaymentDetailFragment(fragment: PaymentDetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BarcodeResultsFragment::class)
    internal abstract fun bindBarcodeResultsFragment(fragment: BarcodeResultsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BarcodeScanFragment::class)
    internal abstract fun bindBarcodeScanFragment(fragment: BarcodeScanFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DripWebViewFragment::class)
    internal abstract fun bindDripWebViewFragment(fragment: DripWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ManagePayeeFragment::class)
    internal abstract fun bindManagePayeeFragment(fragment: ManagePayeeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PayeeDetailFragment::class)
    internal abstract fun bindPayeeDetailFragment(fragment: PayeeDetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BillPayContainerFragment::class)
    internal abstract fun bindBillPayContainerFragment(fragment: BillPayContainerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BiometricAuthenticationFragment::class)
    internal abstract fun bindBiometricAuthenticationFragment(fragment: BiometricAuthenticationFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BiometricEnablerFragment::class)
    internal abstract fun bindBiometricEnablerFragment(fragment: BiometricEnablerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ChartsFragment::class)
    abstract fun bindChartsFragment(fragment: ChartsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CheckDepositFragment::class)
    abstract fun bindCheckDepositFragment(checkDepositFragment: CheckDepositFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CheckDepositActivityFragment::class)
    abstract fun bindCheckDepositActivityFragment(checkDepositFragment: CheckDepositActivityFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CheckDepositLandingFragment::class)
    abstract fun bindCheckDepositLandingFragment(checkDepositFragment: CheckDepositLandingFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CheckDepositServiceAgreementFragment::class)
    abstract fun bindCheckDepositServiceAgreementFragment(checkDepositFragment: CheckDepositServiceAgreementFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DepositConfirmationFragment::class)
    abstract fun bindDepositConfirmationFragment(depositConfirmationFragment: DepositConfirmationFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DepositDetailsFragment::class)
    abstract fun bindDepositDetailsFragment(checkDepositFragment: DepositDetailsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(NetAssetsChartFragment::class)
    abstract fun bindNetAssetsChartFragment(fragment: NetAssetsChartFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PerformanceChartFragment::class)
    abstract fun bindPerformanceChartFragment(fragment: PerformanceChartFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MainMenuFragment::class)
    internal abstract fun bindMenuFragment(fragment: MainMenuFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MenuWebViewFragment::class)
    internal abstract fun bindMenuWebViewFragment(fragment: MenuWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(NewAccountWebViewFragment::class)
    internal abstract fun bindNewAccountWebViewFragment(fragment: NewAccountWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ProfileMenuFragment::class)
    internal abstract fun bindProfileMenuFragment(fragment: ProfileMenuFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AccountDetailsMenuFragment::class)
    internal abstract fun bindAccountDetailsMenuFragment(fragment: AccountDetailsMenuFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CustomerServiceMenuFragment::class)
    internal abstract fun bindCustomerServiceMenuFragment(fragment: CustomerServiceMenuFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransferMenuFragment::class)
    internal abstract fun contributeMoveMoneyMenuFragmentInjector(fragmentTransfer: TransferMenuFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MenuDataReplacementWebViewFragment::class)
    internal abstract fun contributeMenuWebViewFragmentInjector(fragmentMenuDataReplacementWebView: MenuDataReplacementWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MsAccountsWebViewFragment::class)
    internal abstract fun bindMsAccountsWebViewFragment(fragment: MsAccountsWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OrdersFragment::class)
    internal abstract fun bindOrderFragment(fragment: OrdersFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OrdersListFragment::class)
    internal abstract fun bindOpenOrderFragment(listFragment: OrdersListFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OrderDetailsFragment::class)
    internal abstract fun bindOptionOrderDetailsFragment(fragment: OrderDetailsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OrderBottomSheetFragment::class)
    internal abstract fun bindBottomSheetOrdersFragment(fragment: OrderBottomSheetFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransactionsFragment::class)
    internal abstract fun bindTransactionsFragment(fragment: TransactionsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TradeFragment::class)
    internal abstract fun bindTradeFragment(fragment: TradeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TradeRouterFragment::class)
    internal abstract fun bindTradeRouterFragment(fragment: TradeRouterFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MutualFundTradeFragment::class)
    internal abstract fun bindMutualFundTradeFragment(fragment: MutualFundTradeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TradeBottomSheetFragment::class)
    internal abstract fun bindTradeBottomSheetFragment(fragment: TradeBottomSheetFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TradeCalculatorFragment::class)
    internal abstract fun bindTradeCalculatorFragment(fragment: TradeCalculatorFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TradeAppActionRedirectFragment::class)
    internal abstract fun bindTradeAppActionRedirectFragment(fragment: TradeAppActionRedirectFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TradeHelpFragment::class)
    internal abstract fun bindTradeHelpFragment(fragment: TradeHelpFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransferMoneyAppActionRedirectFragment::class)
    internal abstract fun bindTransferMoneyAppActionRedirectFragment(fragment: TransferMoneyAppActionRedirectFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransferMoneyPopupDialogFragment::class)
    internal abstract fun bindTransferMoneyPopupDialogFragment(fragment: TransferMoneyPopupDialogFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ExtendedPortfolioSummaryBottomSheetFragment::class)
    internal abstract fun bindExtendedPortfolioBottomSheetFragment(fragment: ExtendedPortfolioSummaryBottomSheetFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PortfolioTaxLotBottomSheetFragment::class)
    internal abstract fun bindPortfolioTaxLotBottomSheetFragment(fragment: PortfolioTaxLotBottomSheetFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PortfolioTaxLotFragment::class)
    internal abstract fun bindPortfolioTaxLotFragment(fragment: PortfolioTaxLotFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ExternalAccountsWebViewFragment::class)
    internal abstract fun bindExternalAccountsWebViewFragment(fragment: ExternalAccountsWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OverviewFragment::class)
    internal abstract fun bindOverviewFragment(fragment: OverviewFragment): Fragment

    @ExperimentalCoroutinesApi
    @FlowPreview
    @Binds
    @IntoMap
    @FragmentKey(CompleteViewFragment::class)
    internal abstract fun bindCompleteViewFragment(fragment: CompleteViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BalancesFragment::class)
    internal abstract fun bindBalancesFragment(fragment: BalancesFragment): Fragment

    @ContributesAndroidInjector
    internal abstract fun contributeAccountsHomeFragment(): AccountsHomeFragment

    @Binds
    @IntoMap
    @FragmentKey(AccountsTabHostFragment::class)
    internal abstract fun bindAccountsTabHostFragment(fragment: AccountsTabHostFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BankDateTransactionPickerFragment::class)
    internal abstract fun bindBankDateTransactionPickerFragment(fragment: BankDateTransactionPickerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BankHomeFragment::class)
    internal abstract fun bindBankHomeFragment(fragment: BankHomeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(NewsContainerFragment::class)
    internal abstract fun bindNewsContainerFragment(fragment: NewsContainerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TextNewsDetailFragment::class)
    internal abstract fun bindTextNewsDetailFragment(fragment: TextNewsDetailFragment): Fragment

    @ContributesAndroidInjector
    internal abstract fun contributeMarketNewsFragment(): MarketNewsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCompanyNewsFragment(): CompanyNewsFragment

    @ContributesAndroidInjector
    internal abstract fun contributePortfolioNewsFragment(): PortfolioNewsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeWatchlistNewsFragment(): WatchlistNewsFragment

    @Binds
    @IntoMap
    @FragmentKey(MarketsFragment::class)
    internal abstract fun bindMarketsFragment(fragment: MarketsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MarketsOverviewFragment::class)
    internal abstract fun bindMarketsOverviewFragment(fragment: MarketsOverviewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MarketsFuturesFragment::class)
    internal abstract fun bindMarketsFuturesFragment(fragment: MarketsFuturesFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MarketsMoversFragment::class)
    internal abstract fun bindMarketsMoversFragment(fragment: MarketsMoversFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AlertsContainerFragment::class)
    internal abstract fun bindAlertContainerFragment(fragment: AlertsContainerFragment): Fragment

    @ContributesAndroidInjector
    internal abstract fun contributeMarketAlertsFragment(): MarketAlertsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeAccountAlertsFragment(): AccountAlertsFragment

    @Binds
    @IntoMap
    @FragmentKey(WebViewFragment::class)
    internal abstract fun bindWebViewFragment(fragment: WebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(NavigableWebViewFragment::class)
    internal abstract fun bindNavigableWebviewFragment(fragment: NavigableWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AlertDetailFragment::class)
    internal abstract fun bindAlertDetailFragment(fragment: AlertDetailFragment): Fragment

    @ContributesAndroidInjector
    internal abstract fun contributeStockQuoteLookupFragment(): StockQuoteLookupFragment

    @ContributesAndroidInjector
    internal abstract fun contributeStockQuoteSearchFragment(): StockQuoteSearchFragment

    @ContributesAndroidInjector
    internal abstract fun contributeStockPutOptionLookupFragment(): StockPutOptionLookupFragment

    @ContributesAndroidInjector
    internal abstract fun contributeStockCallOptionLookupFragment(): StockCallOptionLookupFragment

    @Binds
    @IntoMap
    @FragmentKey(QuoteFragment::class)
    internal abstract fun bindQuoteFragment(fragment: QuoteFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ProspectusFragment::class)
    internal abstract fun bindProspectusFragment(fragment: ProspectusFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DevTemplateFragment::class)
    internal abstract fun bindDevTemplateFragment(fragment: DevTemplateFragment): Fragment

    @ContributesAndroidInjector
    internal abstract fun contributeEducationFragmentInjector(): EducationFragment

    @Binds
    @IntoMap
    @FragmentKey(AllBrokerageAccountsFragment::class)
    internal abstract fun bindAllBrokerageAccountsFragment(fragment: AllBrokerageAccountsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SetAlertFragment::class)
    internal abstract fun bindSetAlertFragment(fragment: SetAlertFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ManageAlertFragment::class)
    internal abstract fun bindManageAlertFragment(fragment: ManageAlertFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MarketAlertsFragment::class)
    internal abstract fun bindMarketAlertsFragment(fragment: MarketAlertsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AccountAlertsFragment::class)
    internal abstract fun bindAccountAlertsFragment(fragment: AccountAlertsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MarketDataSubscriptionPromptDialogFragment::class)
    internal abstract fun bindMarketDataSubscriptionPromptDialogFragment(fragment: MarketDataSubscriptionPromptDialogFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(UserAlertDetailFragment::class)
    internal abstract fun bindUserAlertDetailFragment(fragment: UserAlertDetailFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PortfolioFragment::class)
    internal abstract fun bindPortfolioFragment(fragment: PortfolioFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EarningsDetailsFragment::class)
    internal abstract fun bindEarningsDetailsFragment(fragment: EarningsDetailsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EarningsDetailsOverlayFragment::class)
    internal abstract fun bindEarningsDetailsOverlayFragment(fragment: EarningsDetailsOverlayFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(QuoteBottomSheetFragment::class)
    internal abstract fun bindBottomSheetQuoteFragment(fragment: QuoteBottomSheetFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(BarcodeFragment::class)
    internal abstract fun bindBarcodeFragment(fragment: BarcodeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(CompanyOverviewFragment::class)
    internal abstract fun bindCompanyOverviewFragment(fragment: CompanyOverviewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SettingsFragment::class)
    internal abstract fun bindSettingsFragment(fragment: SettingsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RealTimeQuoteStreamingPromptDialogFragment::class)
    internal abstract fun bindRealTimeQuoteStreamingPromptDialogFragment(fragment: RealTimeQuoteStreamingPromptDialogFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ReportsFragment::class)
    internal abstract fun bindReportsFragment(fragment: ReportsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PdfWebviewFragment::class)
    internal abstract fun bindPDFWebviewFragment(fragment: PdfWebviewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(QuoteLevel2Fragment::class)
    internal abstract fun bindLevel2Fragment(fragment: QuoteLevel2Fragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TradingDefaultsFragment::class)
    abstract fun bindTradingDefaultsFragment(fragment: TradingDefaultsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ThemeSelectionFragment::class)
    abstract fun bindThemeSelectionFragment(fragment: ThemeSelectionFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(UserSubscriptionsWebViewFragment::class)
    internal abstract fun bindSubscriptionsWebViewFragment(fragment: UserSubscriptionsWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AccountServicesWebViewFragment::class)
    internal abstract fun bindAccountServicesWebViewFragment(fragment: AccountServicesWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OptionsChainFragment::class)
    internal abstract fun bindOptionsChainFragment(fragment: OptionsChainFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OptionsChainDisclosureDialogFragment::class)
    internal abstract fun bindOptionsChainDisclosureDialogFragment(fragment: OptionsChainDisclosureDialogFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(StockPlanWebViewFragment::class)
    internal abstract fun bindStockPlanWebViewFragment(fragment: StockPlanWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(HoldingsFragment::class)
    internal abstract fun bindHoldingsFragment(fragment: HoldingsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SellAndExerciseFragment::class)
    internal abstract fun bindSellAndExerciseFragment(fragment: SellAndExerciseFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(StockOrdersWebFragment::class)
    internal abstract fun bindStockOrdersWebFragment(fragment: StockOrdersWebFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(AccountsOrganizeOnboardingFragment::class)
    internal abstract fun bindAccountsOrganizeFragment(onboardingFragment: AccountsOrganizeOnboardingFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PdfFragment::class)
    internal abstract fun bindPdfFragment(fragment: PdfFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ChartIqFragment::class)
    internal abstract fun bindChartIqFragment(fragment: ChartIqFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OrderConfirmationFragment::class)
    abstract fun bindOrderConfirmationFragment(fragment: OrderConfirmationFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ApprovalLevelFragment::class)
    abstract fun bindApprovalLevelFragment(fragment: ApprovalLevelFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OptionLegSelectFragment::class)
    abstract fun bindOptionLegSelectFragment(fragment: OptionLegSelectFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TradeOrderDetailsFragment::class)
    abstract fun bindTradeOrderDetailsFragment(fragment: TradeOrderDetailsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VerifyAccountBeforeOpenAccountDialogFragment::class)
    internal abstract fun bindVerifyAccountBeforeOpenAccountDialogFragment(fragment: VerifyAccountBeforeOpenAccountDialogFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VerifyAccountBeforeWebViewDialogFragment::class)
    internal abstract fun bindVerifyAccountBeforeWebViewDialogFragment(fragment: VerifyAccountBeforeWebViewDialogFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(WatchlistSelectionFragment::class)
    internal abstract fun bindWatchlistSelectionFragment(fragment: WatchlistSelectionFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PerformanceFragment::class)
    internal abstract fun bindPerformanceFragment(fragment: PerformanceFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ExtendedHoursDisclosureWebViewFragment::class)
    internal abstract fun bindExtendedHoursDisclosureFragment(fragment: ExtendedHoursDisclosureWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(SessionTerminatedFragment::class)
    internal abstract fun bindSessionTerminatedFragment(fragment: SessionTerminatedFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ChartBottomSheetFragment::class)
    internal abstract fun bindChartBottomSheetFragment(fragment: ChartBottomSheetFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PersonalNotificationsFragment::class)
    internal abstract fun bindPersonalNotificationsFragment(fragment: PersonalNotificationsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(VerifyExternalAccountWebViewFragment::class)
    internal abstract fun bindVerifyExternalAccountWebViewFragment(fragment: VerifyExternalAccountWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TradeAgreementWebViewFragment::class)
    internal abstract fun bindTradeAgreementWebViewFragment(fragment: TradeAgreementWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LiEtfAgreementWebViewFragment::class)
    internal abstract fun bindLiEtfAgreementWebviewFragment(fragment: LiEtfAgreementWebViewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TradeSelectFragment::class)
    internal abstract fun bindOverlayFragment(fragment: TradeSelectFragment): Fragment

    companion object {

        @Provides
        @IntoMap
        @FragmentKey(ManagedOverviewFragment::class)
        fun provideManagedOverviewFragment(
            @DeviceType deviceType: String,
            @Web webBaseUrl: String,
            mainNavigation: MainNavigation,
            webViewHelper: WebViewHelper,
            viewModelFactory: ViewModelProvider.Factory
        ): Fragment = ManagedOverviewFragment(
            deviceType = deviceType,
            managedAccountUrl = "$webBaseUrl/etx/wm/managedashboard",
            mainNavigation = mainNavigation,
            webViewHelper = webViewHelper,
            viewModelFactory = viewModelFactory
        )

        @Provides
        @IntoMap
        @FragmentKey(IraContributionAmountsFragment::class)
        fun provideIraContributionAmountsFragment(
            viewModelFactory: ViewModelProvider.Factory,
            snackbarFactory: SnackbarUtilFactory,
            @AmericanDateOnly dateFormatter: DateTimeFormatter
        ): Fragment = IraContributionAmountsFragment(
            viewModelProvider = viewModelFactory,
            snackbarUtilFactory = snackbarFactory,
            deadlineDateFormatter = dateFormatter
        )
    }
}
