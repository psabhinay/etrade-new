package com.etrade.mobilepro.news.api.model

import android.content.Context
import android.content.res.Resources.NotFoundException
import androidx.annotation.Keep
import androidx.annotation.StringRes
import com.etrade.mobilepro.news.api.R

@Keep
enum class NewsType(@StringRes private val titleResId: Int) {

    PORTFOLIO(R.string.news_type_portfolio),
    WATCHLIST(R.string.news_type_watchlist),
    MARKET(R.string.news_type_market),
    COMPANY(R.string.news_type_company),
    UNDEFINED(0);

    fun title(context: Context): String? =
        try {
            context.getString(titleResId)
        } catch (e: NotFoundException) {
            null
        }
}
