package com.etrade.mobilepro.transactions.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.transactions.ErrorInfo
import com.etrade.mobilepro.transactions.TransactionDetailInfo
import com.etrade.mobilepro.transactions.TransactionsResponseInfo
import com.etrade.mobilepro.transactions.api.TransactionsRepo
import com.etrade.mobilepro.util.Resource
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class TransactionsViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var viewModel: TransactionsViewModel

    @Test
    fun `returns error view state when userId is null`() {
        viewModel = testViewModel(userId = null)
        viewModel.fetch()
        assertTrue(viewModel.viewState.value is TransactionsViewModel.ViewState.Error)
    }

    @Test
    fun `returns error view state when accountId is null`() {
        viewModel = testViewModel(accountId = null)
        viewModel.fetch()
        assertTrue(viewModel.viewState.value is TransactionsViewModel.ViewState.Error)
    }

    @Test
    fun `returns error view state when transaction call is not successful`() {
        viewModel = testViewModel(error())
        viewModel.fetch()
        assertTrue(viewModel.viewState.value is TransactionsViewModel.ViewState.Error)
    }

    @Test
    fun `returns error view state when transaction call has no data`() {
        viewModel = testViewModel(successWith(null))
        viewModel.fetch()
        assertTrue(viewModel.viewState.value is TransactionsViewModel.ViewState.Error)
    }

    @Test
    fun `returns error view state when transaction call has errors`() {
        viewModel = testViewModel(successWith(ERRORS))
        viewModel.fetch()
        assertTrue(viewModel.viewState.value is TransactionsViewModel.ViewState.Error)
    }

    @Test
    fun `returns success view state when transactions are empty`() {
        viewModel = testViewModel(successWith(EMPTY_TRANSACTIONS))
        viewModel.fetch()
        val result = viewModel.viewState.value as TransactionsViewModel.ViewState.Success
        assertEquals(0, result.transactions.size)
    }

    @Test
    fun `returns success view state when contains 20044 error`() {
        viewModel = testViewModel(successWith(ERRORS_WITH_CODE_20044))
        viewModel.fetch()
        val result = viewModel.viewState.value as TransactionsViewModel.ViewState.Success
        assertEquals(0, result.transactions.size)
    }

    @Test
    fun `returns success view state when transaction call successful`() {
        viewModel = testViewModel(successWith(TRANSACTIONS))
        viewModel.fetch()
        val result = viewModel.viewState.value as TransactionsViewModel.ViewState.Success
        assertEquals(2, result.transactions.size)
        assertTrue(result.transactions.all { it.type != "Sweep" })
        assertEquals("5.00", result.transactions[0].amount)
    }

    @Test
    fun `includes sweep activity when it is enabled`() {
        viewModel = testViewModel(successWith(TRANSACTIONS))
        viewModel.isSweepActivityEnabled = true
        viewModel.fetch()
        val result = viewModel.viewState.value as TransactionsViewModel.ViewState.Success
        assertEquals(3, result.transactions.size)
        assertTrue(result.transactions.any { it.type == "Sweep" })
        assertEquals("5.00", result.transactions[0].amount)
    }

    companion object {

        private val TRANSACTIONS: TransactionsResponseInfo = TransactionsResponseInfoImpl(
            transactions = listOf(
                TransactionDetailInfoImp(
                    "5.00",
                    "02/07/19",
                    "TRANSFER TO XXXXXX5077 REFID:73641503;",
                    "Transfer"
                ),
                TransactionDetailInfoImp(
                    "1.00",
                    "02/07/19",
                    "TRANSFER TO XXXX1658-1 REFID:73755503;",
                    "Transfer"
                ),
                TransactionDetailInfoImp(
                    "3.00",
                    "02/07/19",
                    "TRANSFER FROM MARGIN TO CASH;",
                    "Sweep"
                ),
            )
        )

        private val EMPTY_TRANSACTIONS = TransactionsResponseInfoImpl(transactions = emptyList())

        private val ERRORS: TransactionsResponseInfo = TransactionsResponseInfoImpl(
            errors = listOf(ErrorInfoImpl(1), ErrorInfoImpl(2))
        )

        private val ERRORS_WITH_CODE_20044: TransactionsResponseInfo = TransactionsResponseInfoImpl(
            errors = listOf(ErrorInfoImpl(1), ErrorInfoImpl(errorCode = 20044))
        )

        private fun successWith(response: TransactionsResponseInfo?) =
            Observable.just<Resource<TransactionsResponseInfo>>(Resource.Success(response))

        private fun error(message: String = "Something went wrong") =
            Observable.error<Resource<TransactionsResponseInfo>>(RuntimeException(message))

        private fun testViewModel(
            response: Observable<Resource<TransactionsResponseInfo>> = successWith(TRANSACTIONS),
            userId: Long? = 0,
            accountId: Long? = 0,
        ): TransactionsViewModel {
            val repo = mock<TransactionsRepo> {
                on { getTransactions(any(), any()) } doReturn response
            }
            val user = mock<User> {
                on { getUserId() } doReturn userId
            }
            val daoProvider = mock< CachedDaoProvider> {
                on { getAccountDao() } doReturn mock()
            }
            return TransactionsViewModel(daoProvider, user, repo, mock(), mock()).also {
                it.accountId = accountId
            }
        }

        private data class ErrorInfoImpl(
            override val errorCode: Int = 0,
            override val errorType: Int = 0,
            override val errorDescription: String = "Error",
        ) : ErrorInfo

        private data class TransactionDetailInfoImp(
            override val amount: String,
            override val date: String,
            override val description: String,
            override val type: String,
        ) : TransactionDetailInfo

        private data class TransactionsResponseInfoImpl(
            override val errors: List<ErrorInfo> = listOf(),
            override val transactions: List<TransactionDetailInfo> = listOf(),
        ) : TransactionsResponseInfo
    }
}
