package com.etrade.mobilepro.tracking.params

import com.etrade.mobilepro.tracking.EVENT_ATTRIBUTE_CUSTOMER_ID
import com.etrade.mobilepro.tracking.EVENT_ATTRIBUTE_SECURITY_TYPE
import com.etrade.mobilepro.tracking.EVENT_ATTRIBUTE_SYMBOL
import com.etrade.mobilepro.tracking.UNSPECIFIED

const val EVENT_TRADE_SAVED = "Trade Saved"
const val EVENT_TRADE_PLACED = "Trade Placed"
private const val EVENT_ATTRIBUTE_SECTOR = "Sector"
private const val EVENT_ATTRIBUTE_ORDER_TYPE = "Order Type"
private const val EVENT_ATTRIBUTE_PRICE_TYPE = "Price Type"
private const val EVENT_ATTRIBUTE_QUANTITY = "Quantity"

class TradeEventAnalyticsParams(
    private val symbol: String,
    private val securityType: String,
    private val sector: String?,
    private val orderType: String?,
    private val priceType: String?,
    private val quantity: String?,
    private val customerId: String,
    override val name: String
) : AnalyticsParams {
    override val params: Map<String, String>
        get() {
            return mutableMapOf(
                EVENT_ATTRIBUTE_SYMBOL to symbol,
                EVENT_ATTRIBUTE_SECURITY_TYPE to securityType,
                EVENT_ATTRIBUTE_SECTOR to (sector ?: UNSPECIFIED),
                EVENT_ATTRIBUTE_CUSTOMER_ID to customerId
            ).also { map ->
                orderType?.let { map[EVENT_ATTRIBUTE_ORDER_TYPE] = it }
                priceType?.let { map[EVENT_ATTRIBUTE_PRICE_TYPE] = it }
                quantity?.let { map[EVENT_ATTRIBUTE_QUANTITY] = it }
            }
        }
}
