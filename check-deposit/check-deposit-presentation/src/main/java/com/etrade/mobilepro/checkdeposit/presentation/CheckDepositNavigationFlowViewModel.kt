package com.etrade.mobilepro.checkdeposit.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.delegate.consumable

class CheckDepositNavigationFlowViewModel : ViewModel() {

    private val _switchToDepositActivity = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val switchToDepositActivity: LiveData<ConsumableLiveEvent<Unit>>
        get() = _switchToDepositActivity

    private val _refreshDepositActivity = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val refreshDepositActivity: LiveData<ConsumableLiveEvent<Unit>>
        get() = _refreshDepositActivity

    var checkDepositRequestId: String? by consumable(null)

    fun switchToDepositActivity() {
        _switchToDepositActivity.value = ConsumableLiveEvent(Unit)
    }

    fun refreshDepositActivity() {
        _refreshDepositActivity.value = ConsumableLiveEvent(Unit)
    }
}
