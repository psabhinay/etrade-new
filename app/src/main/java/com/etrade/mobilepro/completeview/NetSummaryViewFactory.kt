package com.etrade.mobilepro.completeview

import androidx.lifecycle.LiveData
import com.etrade.completeview.dto.NetSummaryViewDto
import com.etrade.mobilepro.accountvalues.COMPLETE_VIEW_SUMMARY_STREAM_ID
import com.etrade.mobilepro.backends.mgs.MOVEMENT_TYPE_NEUTRAL
import com.etrade.mobilepro.backends.mgs.StreamingValueDto
import com.etrade.mobilepro.completeview.model.NetSummaryViewModel
import com.etrade.mobilepro.dummy.DAYS_GAIN_STREAM_ID
import com.etrade.mobilepro.dummy.NET_DAYS_GAIN_STREAM_ID
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.streaming.api.StreamingText
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory

class NetSummaryViewFactory(
    private val completeViewTextManager: StreamingTextManager,
    private val defaultTextManager: StreamingTextManager
) : DynamicUiViewFactory {

    private lateinit var streamingTextManager: StreamingTextManager

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is NetSummaryViewDto) {
            streamingTextManager = getStreamingTextManager(dto.accountId)
            val streamingValues = createStreamingNetSummaryValues(dto)
            val viewModel = NetSummaryViewModel(streamingValues.dayGain, streamingValues.netAssets)
            return object : GenericLayout {
                override val viewModelFactory = viewModelFactory { viewModel }
                override val viewModelClass = NetSummaryViewModel::class.java

                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is NetSummaryViewModel) {
                        viewModel.updateSummaryData(createStreamingNetSummaryValues(dto))
                    }
                }
                override val uniqueIdentifier: String = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("NetSummaryViewFactory registered to unknown type")
        }
    }

    private fun createStreamingNetSummaryValues(dto: NetSummaryViewDto): StreamingNetSummaryValues {
        return StreamingNetSummaryValues(
            dayGain = dto.dayGainDto.value.getValueAsStreamingText(dto.accountId),
            netAssets = dto.netAssetsDto.value.getValueAsStreamingText(dto.accountId)
        )
    }

    private fun StreamingValueDto.getValueAsStreamingText(accountId: String?): LiveData<StreamingText> {
        val correctedStreamId = if (streamId == DAYS_GAIN_STREAM_ID) {
            NET_DAYS_GAIN_STREAM_ID
        } else {
            streamId
        }
        val streamId = "$correctedStreamId:${accountId ?: COMPLETE_VIEW_SUMMARY_STREAM_ID}"
        val streamingValueDto = StreamingValueDto(initial, streamId, movementType ?: MOVEMENT_TYPE_NEUTRAL)
        return getStreamingText(streamingTextManager, streamingValueDto, isAccountStreamingAllowed = true)
    }

    private fun getStreamingTextManager(accountId: String?): StreamingTextManager = if (accountId == null) {
        completeViewTextManager
    } else {
        // for Individual account summary
        defaultTextManager
    }
}

data class StreamingNetSummaryValues(
    val dayGain: LiveData<StreamingText>,
    val netAssets: LiveData<StreamingText>
)
