package com.etrade.mobilepro.util.android.extension

import android.text.Editable

fun Editable.updateValue(value: String) {
    this.clear()
    this.append(value)
}
