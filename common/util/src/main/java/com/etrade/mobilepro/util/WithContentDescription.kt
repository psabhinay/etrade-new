package com.etrade.mobilepro.util

interface WithContentDescription {
    val contentDescription: String
}
