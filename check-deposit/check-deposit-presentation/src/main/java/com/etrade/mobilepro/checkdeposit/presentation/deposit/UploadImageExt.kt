package com.etrade.mobilepro.checkdeposit.presentation.deposit

import com.etrade.mobilepro.checkdeposit.usecases.api.UploadCheckImage
import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount

internal fun FundsFlowAccount.toImageUploadRequest(
    amount: String,
    @UploadCheckImage.CheckSide checkSide: Int,
    checkImage: ByteArray
): UploadCheckImage {
    return UploadCheckImageImpl(
        checkSide = checkSide,
        checkImage = checkImage,
        instNo = this.instNo.toString(),
        accountNumber = this.accountId,
        depositAmount = amount
    )
}

private class UploadCheckImageImpl(
    override val checkSide: Int,
    override val checkImage: ByteArray,
    override val instNo: String,
    override val accountNumber: String,
    override val depositAmount: String
) : UploadCheckImage
