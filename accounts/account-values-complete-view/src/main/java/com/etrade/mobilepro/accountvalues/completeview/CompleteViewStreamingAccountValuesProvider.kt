package com.etrade.mobilepro.accountvalues.completeview

import com.etrade.eo.accountvalues.api.AccountValuesVO
import com.etrade.eo.accountvalues.streaming.createLsId
import com.etrade.eo.streaming.SubscriptionMode
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.accountvalues.COMPLETE_VIEW_SUMMARY_STREAM_ID
import com.etrade.mobilepro.accountvalues.api.AccountValuesProvider
import com.etrade.mobilepro.accountvalues.pnl.calculator.AccountPnlValuesCalculator
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult.CalculatedAccountValuesVO
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class CompleteViewStreamingAccountValuesProvider @Inject constructor(
    private val accountDaoProvider: AccountDaoProvider,
    private val helper: SubscriptionManagerHelper<CalculationResult>,
    private val pnlCalculator: AccountPnlValuesCalculator
) : AccountValuesProvider {

    private var allAccountsSummarySubject: Subject<AccountValuesVO> = PublishSubject.create()
    private val lastCalculatedAccountValues = mutableMapOf<String, CalculatedAccountValuesVO>()
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun accountValues(accountId: String, isRealTimeQuotes: Boolean): Observable<AccountValuesVO> {
        logger.debug("accountValues() called for accountId = $accountId")
        return if (accountId == COMPLETE_VIEW_SUMMARY_STREAM_ID) {
            allAccountsSummarySubject
                .doFinally {
                    if (allAccountsSummarySubject.hasObservers().not()) {
                        lastCalculatedAccountValues.clear()
                    }
                }
        } else {
            val lsId = createLsId(accountId, isRealTimeQuotes)
            accountDaoProvider.getAccountDao().getAllAccountsObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap { accounts ->
                    helper.subscribeTo(lsId, AccountValuesFields.FIELD_SET, SubscriptionMode.MERGE)
                        .observeOn(AndroidSchedulers.mainThread())
                        .filter { it is CalculatedAccountValuesVO }
                        .map { it as CalculatedAccountValuesVO }
                        .doOnNext {
                            updateAllAccountsSummary(accountId, it, accounts)
                        }
                }
        }
    }

    private fun updateAllAccountsSummary(accountId: String, values: CalculatedAccountValuesVO, accounts: List<Account>) {
        val lastValues = lastCalculatedAccountValues[accountId]

        if (values != lastValues) {
            lastCalculatedAccountValues[accountId] = values

            val result = pnlCalculator.calculateCompleteViewSummaryAccountValues(accounts, lastCalculatedAccountValues)
            result.checkIfNotEmptyResultAndPost(allAccountsSummarySubject)
        }
    }
}
