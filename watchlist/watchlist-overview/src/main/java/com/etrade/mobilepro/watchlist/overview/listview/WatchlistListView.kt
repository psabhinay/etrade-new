package com.etrade.mobilepro.watchlist.overview.listview

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.Styles
import com.etrade.mobilepro.common.compose.theme.etColors
import com.etrade.mobilepro.watchlist.overview.R
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.WatchlistColumn
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.getWatchlistCellDisplayText
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import java.math.BigDecimal

@Composable
fun WatchlistListView(
    entryList: List<WatchlistEntry>?,
    onScroll: (Int, Int) -> Unit,
    onItemClick: (Int) -> Unit,
    Footer: (LazyListScope.() -> Unit) = {}
) {
    val watchlistItemDataList = entryList?.map {
        val change = getWatchlistCellDisplayText(it.change, WatchlistColumn.ChangeDollar)
        val changePercent = getWatchlistCellDisplayText(it.changePercent, WatchlistColumn.ChangePercent)
        val lastPrice = getWatchlistCellDisplayText(it.lastPrice, WatchlistColumn.LastPrice)
        val week52High = getWatchlistCellDisplayText(it.week52High, WatchlistColumn.DayHigh)
        val week52Low = getWatchlistCellDisplayText(it.week52Low, WatchlistColumn.DayLow)
        val (header, headerDescription) = it.getHeaderAndHeaderDescription()
        WatchlistListItemData(
            header = header,
            headerDetails = headerDescription,
            lastPrice = lastPrice,
            change = "$change ($changePercent)",
            changeColor = resolveDataColor(it.change),
            highAndLow = "$week52High / $week52Low",
        )
    } ?: emptyList()

    val state = rememberLazyListState()
    LazyColumn(state = state) {
        watchlistItemDataList.forEachIndexed { index, watchlistListItemData ->
            item {
                WatchlistListItemView(modifier = Modifier.clickable { onItemClick.invoke(index) }, watchlistListItemData)
                Divider(color = MaterialTheme.etColors.lightGrey, thickness = 1.dp)
            }
        }
        Footer()
    }
    LaunchedEffect(state.firstVisibleItemIndex) {
        onScroll(state.firstVisibleItemIndex, state.firstVisibleItemIndex + state.layoutInfo.visibleItemsInfo.size)
    }
}

fun WatchlistEntry.getHeaderAndHeaderDescription() = if (typeCode?.isOption == true) {
    val header = ticker.substringBefore('-')
    val headerDetails = description?.substringAfter(' ') ?: ""
    header to headerDetails
} else {
    "$ticker - $description" to ""
}

@Composable
fun WatchlistListItemView(modifier: Modifier = Modifier, itemData: WatchlistListItemData) {
    Column(modifier = modifier.padding(horizontal = Dimens.Size16dp)) {
        Text(
            modifier = Modifier
                .padding(top = Dimens.Size16dp),
            text = itemData.header,
            color = MaterialTheme.etColors.purple,
            style = Styles.Medium16sp,
            textAlign = TextAlign.Start,
        )
        if (itemData.headerDetails.isNotEmpty()) {
            Text(
                text = itemData.headerDetails,
                color = MaterialTheme.etColors.purple,
                style = Styles.Medium14sp,
                textAlign = TextAlign.Start,
            )
        }
        LabelValueRow(stringResource(R.string.last), itemData.lastPrice)
        LabelValueRow(stringResource(R.string.change), itemData.change, itemData.changeColor)
        LabelValueRow(stringResource(R.string.watchlist_fifty_two_wk_high_and_low), itemData.highAndLow)
        Spacer(modifier = Modifier.height(Dimens.Size14dp))
    }
}

@Composable
fun LabelValueRow(label: String, value: String, color: Color = Color.Unspecified) {
    Row(modifier = Modifier.height(Dimens.Size24dp), verticalAlignment = Alignment.CenterVertically) {
        Text(
            text = label,
            style = Styles.Regular14sp,
            textAlign = TextAlign.Start,
            maxLines = 1,
        )
        Text(
            text = value,
            modifier = Modifier.weight(1F),
            style = Styles.Medium16sp,
            color = color,
            textAlign = TextAlign.End,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
    }
}

@Composable
private fun resolveDataColor(value: BigDecimal?) = when (value?.signum()) {
    -1 -> MaterialTheme.etColors.red
    1 -> MaterialTheme.etColors.green
    else -> MaterialTheme.colors.onPrimary
}
