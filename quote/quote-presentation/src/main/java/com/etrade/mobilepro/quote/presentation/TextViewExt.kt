package com.etrade.mobilepro.quote.presentation

import android.text.SpannableString
import android.text.Spanned
import android.widget.TextView
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.util.android.textutil.CustomAlignSuperscriptSpan
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.formatters.formatVolumeCustom
import com.etrade.mobilepro.util.isQuoteSizeValid
import com.etrade.mobilepro.util.safeParseBigDecimal

private const val PRICE_BID_ASK_FORMAT = "%s X"
private const val SIZE_FORMAT = " %s%s"
private const val SHIFT_PERCENTAGE = 0.99F
private const val FONT_SCALE = 1.3f
private const val EXCHANGECODE_SHIFT_PERCENTAGE = 0.01F
private const val EXCHANGECODE_FONT_SCALE = 1.5f
private const val ONE = 1

fun TextView.setDetailsViewFormattedPrice(price: String, size: String, format: String) {
    this.text = String.format(format, formatPriceVal(price), size)
}

fun TextView.setTradeViewFullPriceText(priceVal: String?, sizeVal: String, exchangeCode: String? = null) {
    this.text = getBidAskSpannableText(formatPriceVal(priceVal), sizeVal, exchangeCode)
}

fun TextView.setTradeViewShortPriceText(priceVal: String?) {
    this.text = formatPriceVal(priceVal)
}

private fun formatPriceVal(priceVal: String?): String = MarketDataFormatter.formatMarketDataCustom(priceVal?.safeParseBigDecimal())

fun TextView.formatQuotePrice(quotePrice: String?, quoteSize: String?, quoteExchangeCode: String?) {
    if (quoteSize?.isQuoteSizeValid() == true) {
        val code = quoteExchangeCode ?: ""
        val size = quoteSize.safeParseBigDecimal()?.stripTrailingZeros()
        setTradeViewFullPriceText(
            quotePrice,
            MarketDataFormatter.formatVolumeCustom(size),
            code
        )
    } else {
        setTradeViewShortPriceText(quotePrice)
    }
}

private fun getBidAskSpannableText(priceVal: String, sizeVal: String, exchangeCode: String?): SpannableString {
    val price = String.format(PRICE_BID_ASK_FORMAT, priceVal)
    val size = String.format(SIZE_FORMAT, sizeVal, exchangeCode.orEmpty())
    val spannablePrice = SpannableString(price + size)

    // bid ask separator - X font size reduced by factor 1.3f
    spannablePrice.setSpan(
        CustomAlignSuperscriptSpan(SHIFT_PERCENTAGE, FONT_SCALE),
        spannablePrice.length - size.length - ONE,
        spannablePrice.length - size.length,
        Spanned.SPAN_MARK_MARK
    )

    // ExchangeCode shift top left and font size reduced by factor 1.25f
    if (!exchangeCode.isNullOrBlank()) {
        spannablePrice.setSpan(
            CustomAlignSuperscriptSpan(EXCHANGECODE_SHIFT_PERCENTAGE, EXCHANGECODE_FONT_SCALE),
            spannablePrice.length - ONE,
            spannablePrice.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    return spannablePrice
}
