package com.etrade.mobilepro.tier

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.GET

interface CustomerTierService {

    @JsonMoshi
    @GET("/app/chatxml/getCustomerTier.json")
    suspend fun getTier(): ServerResponseDto<CustomerTierResponse>
}
