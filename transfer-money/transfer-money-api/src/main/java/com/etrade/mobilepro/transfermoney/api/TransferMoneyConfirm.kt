package com.etrade.mobilepro.transfermoney.api

data class TransferMoneyConfirm(
    val errorDescription: String? = null,
    val confirmationNumber: String? = null,
    val headerMessage: String? = null,
    val message: String? = null,
    val footerMessage: String? = null
)
