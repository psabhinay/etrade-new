package com.etrade.mobilepro.quote.screen.presentation.ui.performance

import com.etrade.mobilepro.quoteapi.FundQuoteItem

fun mapFundPerformanceNonAdjusted(performanceItem: FundQuoteItem.FundPerformanceItem, adjust: String): FundPerformance {
    return FundPerformance(
        symbol = performanceItem.symbol,
        symbolAdjust = adjust,
        fundCategory = performanceItem.fundCategoryName,
        monthlyYTD = performanceItem.monthlyTrailingReturnYtd,
        monthly1Year = performanceItem.monthlyTrailingReturn1y,
        monthly5year = performanceItem.monthlyTrailingReturn5y,
        monthly10year = performanceItem.monthlyTrailingReturn10y,
        categoryMonthlyYTD = performanceItem.nonAdjMonthlyTrailingReturnYtd,
        categoryMonthly1Year = performanceItem.nonAdjMonthlyTrailingReturn1y,
        categoryMonthly5year = performanceItem.nonAdjMonthlyTrailingReturn5y,
        categoryMonthly10year = performanceItem.nonAdjMonthlyTrailingReturn10y,
        quarterlyYTD = performanceItem.quarterlyTrailingReturnYtd,
        quarterly1Year = performanceItem.quarterlyTrailingReturn1y,
        quarterly5year = performanceItem.quarterlyTrailingReturn5y,
        quarterly10year = performanceItem.quarterlyTrailingReturn10y,
        categoryQuarterlyYTD = performanceItem.nonAdjQuarterlyTrailingReturnYtd,
        categoryQuarterly1Year = performanceItem.nonAdjQuarterlyTrailingReturn1y,
        categoryQuarterly5year = performanceItem.nonAdjQuarterlyTrailingReturn5y,
        categoryQuarterly10year = performanceItem.nonAdjQuarterlyTrailingReturn10y,
        inceptionDate = performanceItem.inceptionDate,
        performanceAsOfDate = performanceItem.performanceAsOfDate,
        quarterlyPerformanceAsOfDate = performanceItem.quarteryPerformanceAsOfDate,
        monthlyReturnsSinceInception = performanceItem.monthlyTrailingReturnSinceInception,
        quarterlyReturnsSinceInception = performanceItem.quarterlyTrailingReturnSinceInception
    )
}

fun mapFundPerformanceAdjusted(performanceItem: FundQuoteItem.FundPerformanceItem, adjust: String): FundPerformance {
    return FundPerformance(
        symbol = performanceItem.symbol,
        symbolAdjust = adjust,
        fundCategory = performanceItem.fundCategoryName,
        monthlyYTD = performanceItem.monthlyLoadAdjTrailingReturnYtd,
        monthly1Year = performanceItem.monthlyLoadAdjTrailingReturn1y,
        monthly5year = performanceItem.monthlyLoadAdjTrailingReturn5y,
        monthly10year = performanceItem.monthlyLoadAdjTrailingReturn10y,
        categoryMonthlyYTD = performanceItem.adjMonthlyTrailingReturnYtd,
        categoryMonthly1Year = performanceItem.adjMonthlyTrailingReturn1y,
        categoryMonthly5year = performanceItem.adjMonthlyTrailingReturn5y,
        categoryMonthly10year = performanceItem.adjMonthlyTrailingReturn10y,
        quarterlyYTD = performanceItem.quarterlyLoadAdjTrailingReturnYtd,
        quarterly1Year = performanceItem.quarterlyLoadAdjTrailingReturn1y,
        quarterly5year = performanceItem.quarterlyLoadAdjTrailingReturn5y,
        quarterly10year = performanceItem.quarterlyLoadAdjTrailingReturn10y,
        categoryQuarterlyYTD = performanceItem.adjQuarterlyTrailingReturnYtd,
        categoryQuarterly1Year = performanceItem.adjQuarterlyTrailingReturn1y,
        categoryQuarterly5year = performanceItem.adjQuarterlyTrailingReturn5y,
        categoryQuarterly10year = performanceItem.adjQuarterlyTrailingReturn10y,
        inceptionDate = performanceItem.inceptionDate,
        performanceAsOfDate = performanceItem.performanceAsOfDate,
        quarterlyPerformanceAsOfDate = performanceItem.quarteryPerformanceAsOfDate,
        monthlyReturnsSinceInception = performanceItem.monthlyLoadAdjTrailingReturnSinceInception,
        quarterlyReturnsSinceInception = performanceItem.quarterlyLoadAdjTrailingReturnSinceInception
    )
}
