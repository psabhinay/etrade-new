package com.etrade.mobilepro.util.android.clicklisteners

import android.view.View

class OnViewDebounceClickListener(
    private val clickListener: View.OnClickListener,
    private val timeoutMillis: Long = 1000
) : View.OnClickListener {

    private var canClick = true

    override fun onClick(view: View) {
        if (canClick) {
            canClick = false
            clickListener.onClick(view)
            view.postDelayed({ canClick = true }, timeoutMillis)
        }
    }
}
