package com.etrade.mobilepro.usersubscriptionpresentation

import android.content.res.Resources
import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2BookType
import com.etrade.mobilepro.usersubscriptionapi.BookFilter

@SuppressWarnings("ComplexMethod")
fun BookFilter.getDisplayString(resources: Resources): String {
    return when (this.bookType) {
        Level2BookType.ARCA -> R.string.arca
        Level2BookType.ARCA_NSDQ -> R.string.nsdq_and_arca
        Level2BookType.MONTAGE -> R.string.montage
        Level2BookType.NSDQ -> R.string.nsdq
        Level2BookType.NYSE_OPEN -> R.string.nyse_open
        else -> null
    }?.let {
        if (this.requiresSubscription) {
            resources.getString(R.string.requires_subscription).format(resources.getString(it))
        } else {
            resources.getString(it)
        }
    } ?: ""
}
