package com.etrade.mobilepro.learn.presentation.tutorial

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.etrade.mobilepro.learn.presentation.R
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.walkthrough.api.State
import com.etrade.mobilepro.walkthrough.api.SymbolLookupAction
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingAction
import com.etrade.mobilepro.walkthrough.api.TradePlacingAction
import com.etrade.mobilepro.walkthrough.api.WalkthroughScenario
import com.etrade.mobilepro.walkthrough.api.WalkthroughScenario.SYMBOL_LOOKUP
import com.etrade.mobilepro.walkthrough.api.WalkthroughScenario.SYMBOL_TRACKING
import com.etrade.mobilepro.walkthrough.api.WalkthroughScenario.TRADE_PLACING
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import javax.inject.Inject

class LearnTutorialViewModel @Inject constructor(
    val res: Resources,
    private val walkthroughStatesViewModel: WalkthroughStatesViewModel
) : ViewModel() {

    val tutorialItems: LiveData<List<LearnTutorialItem>>
        get() = _tutorialItems
    val walkthroughStates: LiveData<ConsumableLiveEvent<State>> =
        walkthroughStatesViewModel.state
            .map { it.consumable() }

    private var _tutorialItems: MutableLiveData<List<LearnTutorialItem>> = MutableLiveData()

    init {
        walkthroughStatesViewModel.dispose()
        _tutorialItems.value = setStaticData()
    }

    private fun setStaticData(): ArrayList<LearnTutorialItem> {
        val items: ArrayList<LearnTutorialItem> = ArrayList()

        items.add(
            LearnTutorialItem(
                tutorialIconId = R.drawable.learn_tutorial_quote_lookup_icon,
                tutorialText = res.getString(R.string.learn_tutorial_quote_lookup_text),
                walkthroughScenario = SYMBOL_LOOKUP
            )
        )
        items.add(
            LearnTutorialItem(
                tutorialIconId = R.drawable.learn_tutorial_watchlist_icon,
                tutorialText = res.getString(R.string.learn_tutorial_watchlist_add_text),
                walkthroughScenario = SYMBOL_TRACKING
            )
        )
        items.add(
            LearnTutorialItem(
                tutorialIconId = R.drawable.learn_tutorial_trade_icon,
                tutorialText = res.getString(R.string.learn_tutorial_first_trade),
                walkthroughScenario = TRADE_PLACING
            )
        )

        return items
    }

    fun onTutorialItemClicked(learnTutorialItem: LearnTutorialItem) {
        startWalkthrough(learnTutorialItem.walkthroughScenario)
    }

    fun startWalkthrough(walkthroughScenario: WalkthroughScenario) {
        walkthroughStatesViewModel.dispose()
        when (walkthroughScenario) {
            SYMBOL_LOOKUP -> { walkthroughStatesViewModel.onAction(SymbolLookupAction.StartWalkthrough) }
            SYMBOL_TRACKING -> { walkthroughStatesViewModel.onAction(SymbolTrackingAction.StartWalkthrough) }
            TRADE_PLACING -> { walkthroughStatesViewModel.onAction(TradePlacingAction.StartWalkthrough) }
        }
    }
}
