package com.etrade.mobilepro.completeview

import android.content.res.Resources
import com.etrade.completeview.dto.AccountSummaryCardViewDto
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.di.CompleteView
import com.etrade.mobilepro.common.di.Default
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import javax.inject.Inject

/**
 * Constructs Card View to display individual Account summary on Complete View Screen
 */
class AccountSummaryCardViewFactory @Inject constructor(
    daoProvider: CachedDaoProvider,
    @CompleteView
    private val completeViewTextManager: StreamingTextManager,
    @Default
    private val defaultTextManager: StreamingTextManager,
    resources: Resources
) : AbstractAccountSummaryViewFactory(daoProvider, resources) {

    private var isSingleAccount = false

    override val accountSummaryLayoutRes: Int = R.layout.view_account_summary_card

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        return if (dto is AccountSummaryCardViewDto) {
            isSingleAccount = dto.isSingleAccount
            AccountSummaryDelegate(dto.data, dto.ctaList, dto.clickActionDto)
        } else {
            throw DynamicUiViewFactoryException("AccountSummaryViewFactory registered to unknown type")
        }
    }

    override val streamType: StreamType = StreamType.COMPLETE_VIEW

    override val streamingTextManager: StreamingTextManager
        get() = if (!isSingleAccount) {
            completeViewTextManager
        } else {
            // for Individual account summary
            defaultTextManager
        }
}
