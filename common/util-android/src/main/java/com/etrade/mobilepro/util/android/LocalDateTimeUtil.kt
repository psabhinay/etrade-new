package com.etrade.mobilepro.util.android

import org.slf4j.LoggerFactory
import org.threeten.bp.DateTimeException
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter

private val logger = LoggerFactory.getLogger("LocalDateTimeUtil")

fun String.parseSafeLocalDate(formatter: DateTimeFormatter): LocalDate? = try {
    LocalDate.parse(this, formatter)
} catch (ex: DateTimeException) {
    logger.error("invalid date : ${ex.message}")
    null
}

fun String.parseSafeLocalTime(formatter: DateTimeFormatter): LocalTime? {
    return try {
        LocalTime.parse(this, formatter)
    } catch (ex: DateTimeException) {
        logger.error("invalid time : ${ex.message}")
        null
    }
}
