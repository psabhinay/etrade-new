package com.etrade.mobilepro.checkdeposit.presentation.details

import com.etrade.mobilepro.checkdeposit.presentation.details.delegate.DepositDividerAdapterDelegate
import com.etrade.mobilepro.checkdeposit.presentation.details.delegate.DepositFooterMessageAdapterDelegate
import com.etrade.mobilepro.checkdeposit.presentation.details.delegate.DepositImageAdapterDelegate
import com.etrade.mobilepro.checkdeposit.presentation.details.delegate.DepositLabelValueAdapterDelegate
import com.etrade.mobilepro.imageloader.ImageLoader
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class DepositDetailsAdapter(
    imageLoader: ImageLoader
) : ListDelegationAdapter<MutableList<DepositDetail>>() {

    init {
        delegatesManager.addDelegate(DepositLabelValueAdapterDelegate())
        delegatesManager.addDelegate(DepositDividerAdapterDelegate())
        delegatesManager.addDelegate(DepositImageAdapterDelegate(imageLoader))
        delegatesManager.addDelegate(DepositFooterMessageAdapterDelegate())

        items = mutableListOf()
    }

    fun addItems(detailItems: List<DepositDetail>) {
        items.clear()
        items.addAll(detailItems)

        notifyDataSetChanged()
    }
}
