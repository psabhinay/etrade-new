package com.etrade.mobilepro.transfermoney.presentation.transfer.source

import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.etrade.mobilepro.transfermoney.api.repo.TransferStockAccountsDataSource
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

interface TransferSourceFilterer {
    fun filterFromAccounts(holder: TransferSourceHolder): List<TransferMoneyAccount>
    fun filterToAccounts(holder: TransferSourceHolder): List<TransferMoneyAccount>
}

class DefaultTransferSourceFilterer @Inject constructor(
    private val stockAccountsDataSource: TransferStockAccountsDataSource
) : TransferSourceFilterer {

    private val stockAccountIds: Set<String> by lazy {
        runBlocking {
            stockAccountsDataSource.getStockAccountIds()
        }
    }

    override fun filterFromAccounts(holder: TransferSourceHolder): List<TransferMoneyAccount> =
        holder.accounts
            .filter { !it.isIraAccount }
            .filter { it != holder.selectedTo }
            .filter { it.accountId !in stockAccountIds }
            .filter { it.canBeFrom }
            .filterNot { holder.selectedTo?.isExternalAccount == true && it.isExternalAccount }

    override fun filterToAccounts(holder: TransferSourceHolder): List<TransferMoneyAccount> =
        holder.accounts
            .filter { it != holder.selectedFrom }
            .filter { it.accountId !in stockAccountIds }
            .filter { it.canBeTo }
            .filterNot { holder.selectedFrom?.isExternalAccount == true && it.isExternalAccount }
}
