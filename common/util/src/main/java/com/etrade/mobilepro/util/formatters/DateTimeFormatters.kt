package com.etrade.mobilepro.util.formatters

import org.threeten.bp.DateTimeException
import org.threeten.bp.DateTimeUtils
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.OffsetDateTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.DateTimeParseException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.TimeZone

fun createAmericanSimpleDateFormatter(): DateTimeFormatter =
    DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US)

fun createAmericanSimpleTimeFormatter(): DateTimeFormatter =
    DateTimeFormatter.ofPattern("HH:mm 'ET'")

val americanSimpleDateFormatter: DateTimeFormatter = createAmericanSimpleDateFormatter()

val SHORT_DATE_FORMAT: DateTimeFormatter =
    DateTimeFormatter.ofPattern("MM/dd/yy", Locale.US)

fun parseAmericanDateFromString(dateString: String?): LocalDate? =
    parseDate(dateString, americanSimpleDateFormatter)

fun parseShortDateFromString(dateString: String?): LocalDate? =
    parseDate(dateString, SHORT_DATE_FORMAT)

private fun parseDate(dateString: String?, dateFormatter: DateTimeFormatter): LocalDate? {
    return parseDateBlock {
        dateString?.let {
            if (it.isBlank()) {
                null
            } else {
                LocalDate.parse(it, dateFormatter)
            }
        }
    }
}

fun parseCustomDateFromMillis(dateFormat: String): String =
    SimpleDateFormat(dateFormat, Locale.US).format(Calendar.getInstance().time)

fun parseCustomDateFromString(
    dateString: String,
    inputFormat: String,
    outputFormat: String
): String? {
    return try {
        val inputFormatter = DateTimeFormatter.ofPattern(inputFormat)
        val outputFormatter = DateTimeFormatter.ofPattern(outputFormat)
        LocalDateTime.parse(dateString, inputFormatter).format(outputFormatter)
    } catch (e: IllegalArgumentException) {
        null
    } catch (e: DateTimeException) {
        null
    } catch (e: DateTimeParseException) {
        null
    }
}

fun safeParseStringToDate(dateTimeString: String?): Date? {
    var date: Date? = parseDateBlock {
        DateTimeUtils.toDate(
            OffsetDateTime.parse(
                dateTimeString.orEmpty(),
                DateTimeFormatter.ISO_DATE_TIME
            ).toInstant()
        )
    }
    if (date == null) {
        date = sdf.parseDate(dateTimeString) ?: sdf1.parseDate(dateTimeString)
    }
    return date
}

private fun ThreadLocal<SimpleDateFormat>.parseDate(dateTimeString: String?): Date? {
    return parseDateBlock {
        dateTimeString?.let { get()?.parse(it) }
    }
}

private fun <T> parseDateBlock(block: () -> T?): T? {
    var date: T? = null
    try {
        date = block()
    } catch (e: DateTimeParseException) {
        // nop
    } catch (e: ParseException) {
        // nop
    }
    return date
}

// TODO move to corelib this date handling, but later, when more new patterns are known
private val sdf = getSimpleDateFormat("MM-dd-yy hh:mm:ss")

private val sdf1 = getSimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")

val shortSimpleDateFormat = getSimpleDateFormat("MM/dd/yy")

private fun getSimpleDateFormat(formatter: String) = object : ThreadLocal<SimpleDateFormat>() {
    override fun initialValue(): SimpleDateFormat {
        val dateFormat = SimpleDateFormat(formatter, Locale.US)
        dateFormat.timeZone = TimeZone.getTimeZone("America/New_York")
        return dateFormat
    }
}
