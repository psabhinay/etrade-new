package com.etrade.mobilepro.messagecenter.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "ConfirmSecMsgResponse", strict = false)
class ReplyMessageDto(
    @field:Element(name = "ReferenceNumber", required = false)
    var refNumber: String? = null,
    @field:ElementList(name = "MessageList", required = false)
    var communicationList: List<CommunicationDto?>? = null
)
