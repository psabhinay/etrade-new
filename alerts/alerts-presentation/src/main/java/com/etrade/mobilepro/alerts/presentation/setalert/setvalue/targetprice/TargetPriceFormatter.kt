package com.etrade.mobilepro.alerts.presentation.setalert.setvalue.targetprice

import com.etrade.mobilepro.inputvalueview.InputFieldManager

internal class TargetPriceFormatter : InputFieldManager.Formatter {
    override fun formatRawContent(rawContent: String): String = rawContent
}
