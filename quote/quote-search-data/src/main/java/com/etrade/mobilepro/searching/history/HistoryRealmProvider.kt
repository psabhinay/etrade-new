package com.etrade.mobilepro.searching.history

import com.etrade.mobilepro.searching.SearchingRealmModule
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Inject
import javax.inject.Provider

private const val HISTORY_SCHEMA_VERSION = 2L

class HistoryRealmProvider @Inject constructor(
    private val migrationHandler: HistoryRealmMigrationHandler
) : Provider<Realm> {
    private val historyRealmConfig: RealmConfiguration by lazy {
        RealmConfiguration.Builder()
            .name("selection.history")
            .modules(SearchingRealmModule())
            .schemaVersion(HISTORY_SCHEMA_VERSION)
            .migration(migrationHandler)
            .build()
    }

    override fun get(): Realm = Realm.getInstance(historyRealmConfig)
}
