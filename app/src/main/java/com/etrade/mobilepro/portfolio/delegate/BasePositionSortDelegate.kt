package com.etrade.mobilepro.portfolio.delegate

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.portfolio.PortfolioPositionsDisplayMode
import com.etrade.mobilepro.portfolio.data.PortfolioColumnRepo
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.portfolio.toSortState
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.util.SortOrder
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import kotlinx.coroutines.runBlocking

abstract class BasePositionSortDelegate constructor(
    private val portfolioColumnRepo: PortfolioColumnRepo
) : PositionSortDelegate {

    private val _columns: MutableLiveData<List<PortfolioColumn>> = MutableLiveData()

    private val columnsDataChangedListener: PortfolioColumnRepo.OnColumnsDataChangedListener =
        object : PortfolioColumnRepo.OnColumnsDataChangedListener {
            override fun onColumnDataChanged() = updateColumns()
        }

    override val columns: LiveData<List<PortfolioColumn>> = _columns

    init {
        portfolioColumnRepo.addOnColumnsDataChangedListener(columnsDataChangedListener)
    }

    abstract val tableViewType: PortfolioTableViewType

    suspend fun getCurrentColumns(): List<PortfolioColumn> =
        portfolioColumnRepo.getPortfolioColumns(
            positionTableMode,
            tableViewType,
            PortfolioPositionsDisplayMode.TABLE
        )

    protected fun getSortStateInfo(info: SortInfo): SortStateInfo {
        val columnPosition = if (info.column == PortfolioRecordColumn.SYMBOL) {
            null
        } else {
            val columns = _columns.value
            columns?.indexOfFirst { column ->
                column.column == info.column
            }
        }
        return SortStateInfo(
            sortState = info.order.toSortState(),
            sortedColumnPosition = columnPosition,
            stateChanged = ConsumableLiveEvent(true)
        )
    }

    override fun sortColumn(column: Int) {
        _columns.value
            ?.getOrNull(column)
            ?.column
            ?.apply { sortColumn(this) }
    }

    override fun updateColumns() {
        runBlocking {
            _columns.postValue(getCurrentColumns())
        }
    }

    override fun onCleared() {
        portfolioColumnRepo.removeOnColumnsDataChangedListener(columnsDataChangedListener)
    }

    override fun sortColumn(column: PortfolioRecordColumn) {
        val oldState = getOldSortInfo()
        val newState = if (oldState?.column == column) {
            oldState.copy(order = oldState.order.negate())
        } else {
            SortInfo(column, SortOrder.ASCENDING)
        }
        sortColumn(newState)
    }

    abstract fun getOldSortInfo(): SortInfo?
}
