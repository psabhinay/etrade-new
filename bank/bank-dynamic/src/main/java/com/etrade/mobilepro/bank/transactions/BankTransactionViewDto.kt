package com.etrade.mobilepro.bank.transactions

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class BankTransactionViewDto(
    @Json(name = "data")
    override val data: BankTransactionDataDto?,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?
) : BaseScreenView()
