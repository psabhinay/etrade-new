package com.etrade.mobilepro.transfermoney.presentation.common

import android.os.Parcelable
import com.etrade.mobilepro.transfermoney.api.TransferMoneyActivity
import com.etrade.mobilepro.transfermoney.api.TransferType
import kotlinx.parcelize.Parcelize

@Parcelize
@SuppressWarnings("LongParameterList")
class ParcelableTransferMoneyActivity(
    override val amount: String,
    override val date: String,
    override val frequency: String,
    override val fromAccount: String,
    override val confirmationNumber: String,
    override val referenceNumber: String,
    override val status: String,
    override val toAccount: String,
    override val transferType: TransferType?,
    override val id: String,
    override val appName: String,
    override val memo: String
) : Parcelable, TransferMoneyActivity
