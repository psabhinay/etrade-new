package com.etrade.mobilepro.accessvalidation

import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.androidconfig.api.repo.AndroidConfigRepo
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.dialog.viewmodel.AppDialogViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.DeviceRootChecker
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.android.isEmulator
import com.etrade.mobilepro.util.versionCodeInt
import com.etrade.mobilepro.viewmodel.ActivitySignal
import com.etrade.mobilepro.viewmodel.ActivitySignalViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject

interface AppAccessProvider : AppDialogViewModel, ActivitySignalViewModel {
    fun requestAccess(lifecycleScope: CoroutineScope, onAccessGranted: () -> Unit)
}

class DefaultAppAccessProvider @Inject constructor(
    private val applicationInfo: ApplicationInfo,
    private val androidConfigRepo: AndroidConfigRepo,
    private val rootChecker: DeviceRootChecker,
    private val resources: Resources
) : AppAccessProvider {

    private val _dialog = MutableLiveData<ConsumableLiveEvent<AppDialog?>?>()
    override val dialog: LiveData<ConsumableLiveEvent<AppDialog?>?> = _dialog

    override val activitySignal: LiveData<ConsumableLiveEvent<ActivitySignal>>
        get() = _activitySignal

    private val _activitySignal = MutableLiveData<ConsumableLiveEvent<ActivitySignal>>()

    override fun requestAccess(lifecycleScope: CoroutineScope, onAccessGranted: () -> Unit) {
        if (rootChecker.isRooted() && !isEmulator() && !BuildConfig.DEBUG) {
            showRootedDeviceDialog()
        } else {
            lifecycleScope.launch {
                val showForceUpdate = runCatching {
                    val minVersion = androidConfigRepo.getAndroidConfiguration().minimumVersion.versionCodeInt
                    minVersion > applicationInfo.appVersionCode().toInt()
                }.getOrNull() ?: false

                if (showForceUpdate) {
                    showForceUpdateDialog()
                } else {
                    onAccessGranted()
                }
            }
        }
    }

    private fun showRootedDeviceDialog() {
        _dialog.value = resources.appDialog {
            message = resources.getString(R.string.access_validation_root_warning_message)
            resourcesPositiveAction {
                labelRes = R.string.ok
                action = {
                    sendActivitySignal { activity ->
                        activity.finishAndRemoveTask()
                    }
                }
            }
        }.consumable()
    }

    private fun showForceUpdateDialog() {
        _dialog.value = resources.appDialog {
            message = resources.getString(R.string.access_validation_force_update_message)
            resourcesPositiveAction {
                labelRes = R.string.access_validation_to_android_store
                action = {
                    sendActivitySignal { activity ->
                        val intent = Intent(Intent.ACTION_VIEW).apply {
                            data = Uri.parse(
                                "https://play.google.com/store/apps/details?id=com.etrade.mobilepro.activity"
                            )
                        }
                        activity.startActivity(intent)
                        activity.finishAndRemoveTask()
                    }
                }
            }
        }.consumable()
    }

    private fun sendActivitySignal(signal: ActivitySignal) {
        _activitySignal.value = signal.consumable()
    }
}
