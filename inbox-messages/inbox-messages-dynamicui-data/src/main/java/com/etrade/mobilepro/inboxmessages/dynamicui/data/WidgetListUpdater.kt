package com.etrade.mobilepro.inboxmessages.dynamicui.data

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.MobileScreen
import com.etrade.mobilepro.inboxmessages.dynamicui.api.InboxMessagesWidgetPlace

interface WidgetListUpdater {
    val belongsToWidgetPlace: InboxMessagesWidgetPlace
    fun updateWithInboxMessages(input: MobileScreen?): List<BaseScreenView>
}
