package com.etrade.mobilepro.researchreport

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.arr.api.AnalystResearchReportRepo
import com.etrade.mobilepro.mod.api.MarketOnDemand
import com.etrade.mobilepro.mod.api.Report
import com.etrade.mobilepro.tracking.SCREEN_TITLE_ANALYST_RESEARCH_REPORTS
import com.etrade.mobilepro.tracking.ScreenViewModel
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ReportsViewModel @Inject constructor(
    private val marketOnDemand: MarketOnDemand,
    private val reportRepo: AnalystResearchReportRepo
) : ViewModel(), ScreenViewModel {

    override val screenName: String? = SCREEN_TITLE_ANALYST_RESEARCH_REPORTS

    private val mutableViewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState>
        get() = mutableViewState

    private val compositeDisposable = CompositeDisposable()

    fun getReports(symbol: String, isFundReport: Boolean) {
        if (isFundReport) {
            getFundReports(symbol)
        } else {
            getAnalystReports(symbol)
        }
    }

    private fun getFundReports(symbol: String) {
        mutableViewState.value = ViewState.Loading
        compositeDisposable.add(
            Single.fromCallable {
                marketOnDemand.getFundReports(symbol)
            }.subscribeOn(Schedulers.io()).subscribeBy(
                onError = {
                    mutableViewState.postValue(ViewState.Error)
                },
                onSuccess = {
                    showReports(it)
                }
            )
        )
    }

    private fun getAnalystReports(symbol: String) {
        compositeDisposable.add(
            reportRepo.getAnalystReports(symbol)
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                    onError = {
                        mutableViewState.postValue(ViewState.Error)
                    },
                    onSuccess = {
                        showReports(it)
                    }
                )
        )
    }

    private fun showReports(reports: List<Report>) {
        val items: MutableList<ReportListItem> = reports.map { ReportListItem.ReportItem(it) }.toMutableList()
        if (items.isNotEmpty()) {
            items.add(ReportListItem.DisclosuresItem)
        }
        mutableViewState.postValue(ViewState.Success(items))
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    sealed class ViewState {
        object Loading : ViewState()
        object Error : ViewState()
        data class Success(val results: List<ReportListItem>) : ViewState()
    }
}
