package com.etrade.mobilepro.trade.api.advanced

import com.etrade.mobilepro.trade.api.OrderPreviewResponse

interface AdvancedOrderPreviewResponse : OrderPreviewResponse, AdvancedOrderResponse {
    val previewId: String?
}
