package com.etrade.mobilepro.portfolio.model

import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import java.math.BigDecimal

data class SimpleInstrument(
    override val symbol: String,
    override val symbolDescription: String,
    override val displaySymbol: String,
    override val instrumentType: InstrumentType,
    override val ask: BigDecimal? = null,
    override val bid: BigDecimal? = null,
    override val priceEarningRation: BigDecimal? = null,
    override val earningsPerShare: BigDecimal? = null,
    override val fiftyTwoWeekHigh: BigDecimal? = null,
    override val fiftyTwoWeekLow: BigDecimal? = null,
    override val lastPrice: BigDecimal? = null,
    override val tickIndicator: Short? = null,
    override val marketCap: BigDecimal? = null,
    override val volume: BigDecimal? = null,
    override val daysChange: BigDecimal? = null,
    override val daysChangePercentage: BigDecimal? = null,
    override val delta: BigDecimal? = null,
    override val gamma: BigDecimal? = null,
    override val theta: BigDecimal? = null,
    override val vega: BigDecimal? = null,
    override val impliedVolatilityPercentage: BigDecimal? = null,
    override val daysToExpire: Int? = null,
    override val underlyingSymbolInfo: WithSymbolInfo? = null,
    override val previousClose: BigDecimal? = null
) : Instrument {

    constructor(instrument: Instrument) : this(
        symbol = instrument.symbol,
        symbolDescription = instrument.symbolDescription,
        displaySymbol = instrument.displaySymbol,
        instrumentType = instrument.instrumentType,
        ask = instrument.ask,
        bid = instrument.bid,
        priceEarningRation = instrument.priceEarningRation,
        earningsPerShare = instrument.earningsPerShare,
        fiftyTwoWeekHigh = instrument.fiftyTwoWeekHigh,
        fiftyTwoWeekLow = instrument.fiftyTwoWeekLow,
        lastPrice = instrument.lastPrice,
        tickIndicator = instrument.tickIndicator,
        marketCap = instrument.marketCap,
        volume = instrument.volume,
        daysChange = instrument.daysChange,
        daysChangePercentage = instrument.daysChangePercentage,
        delta = instrument.delta,
        gamma = instrument.gamma,
        theta = instrument.theta,
        vega = instrument.vega,
        impliedVolatilityPercentage = instrument.impliedVolatilityPercentage,
        daysToExpire = instrument.daysToExpire,
        underlyingSymbolInfo = instrument.underlyingSymbolInfo,
        previousClose = instrument.previousClose
    )
}
