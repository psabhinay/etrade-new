package com.etrade.mobilepro.tradingdefaults

import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.extensions.android.getTerms
import com.etrade.mobilepro.tracking.SCREEN_TITLE_TRADING_DEFAULTS
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.trade.api.EMPTY_ACCOUNT_ID
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.api.TradeAccountRepo
import com.etrade.mobilepro.tradingdefaults.preferences.DEFAULT_OPTIONS_CONTRACTS
import com.etrade.mobilepro.tradingdefaults.preferences.DEFAULT_OPTIONS_TERM
import com.etrade.mobilepro.tradingdefaults.preferences.DEFAULT_STOCKS_QUANTITY
import com.etrade.mobilepro.tradingdefaults.preferences.DEFAULT_STOCKS_TERM
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import javax.inject.Inject

private const val VALUE_PICKER_DROP_DOWN = "VALUE_PICKER_DROP_DOWN"

class TradingDefaultsViewModel @Inject constructor(
    private val preferences: TradingDefaultsPreferences,
    private val resources: Resources,
    private val tradeAccountRepo: TradeAccountRepo
) : ViewModel(), ScreenViewModel {

    override val screenName: String = SCREEN_TITLE_TRADING_DEFAULTS

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    private val _accounts = MutableLiveData<List<TradeAccount>>()

    private val _showValuePicker = MutableLiveData<ConsumableLiveEvent<DropDownMultiSelector.Settings<*>>>()
    val showValuePicker: LiveData<ConsumableLiveEvent<DropDownMultiSelector.Settings<*>>>
        get() = _showValuePicker

    private val _stocksAccountId: MutableLiveData<String?> = MutableLiveData(preferences.stocksAccountId)
    private val _stocksQuantity: MutableLiveData<String> = MutableLiveData(preferences.stocksQuantity)
    private val _stocksPriceType: MutableLiveData<PriceType?> = MutableLiveData(preferences.stocksPriceType)
    private val _stocksTerm: MediatorLiveData<OrderTerm?> = MediatorLiveData()

    private val _optionsAccountId: MutableLiveData<String?> = MutableLiveData(preferences.optionsAccountId)
    private val _optionsContracts: MutableLiveData<String> = MutableLiveData(preferences.optionsContracts.toString())
    private val _optionsPriceType: MutableLiveData<PriceType?> = MutableLiveData(preferences.optionsPriceType)
    private val _optionsTerm: MediatorLiveData<OrderTerm?> = MediatorLiveData()

    private val _items: MediatorLiveData<List<TradingDefaultsItem>> = MediatorLiveData()
    private val _viewState: MediatorLiveData<TradingDefaultsViewState> = MediatorLiveData()
    val viewState: LiveData<TradingDefaultsViewState>
        get() = _viewState

    // doesn't include default empty accounts
    private var hasEligibleAccounts = false

    init {
        addSourcesToTerm()
        addSourcesToItems()

        viewModelScope.launch {
            tradeAccountRepo.getTradeAccounts()
                .onSuccess { _accounts.value = it }
                .onFailure { logger.error("Failed to load accounts", it) }
        }

        _viewState.addSource(_items) {
            _viewState.value = TradingDefaultsViewState.Idle(it)
        }
    }

    private fun addSourcesToTerm() {
        _stocksTerm.addSource(_stocksPriceType) {
            if (it == preferences.stocksPriceType) {
                _stocksTerm.value = preferences.stocksTerm
            } else {
                _stocksTerm.value = DEFAULT_STOCKS_TERM
            }
        }
        _optionsTerm.addSource(_optionsPriceType) {
            if (it == preferences.optionsPriceType) {
                _optionsTerm.value = preferences.optionsTerm
            } else {
                _optionsTerm.value = DEFAULT_OPTIONS_TERM
            }
        }
    }

    private fun addSourcesToItems() {
        _items.addSource(_accounts) { updateItems() }
        _items.addSource(_stocksAccountId) { updateItems() }
        _items.addSource(_stocksPriceType) { updateItems() }
        _items.addSource(_stocksTerm) { updateItems() }
        _items.addSource(_optionsAccountId) { updateItems() }
        _items.addSource(_optionsPriceType) { updateItems() }
        _items.addSource(_optionsTerm) { updateItems() }
    }

    private fun updateItems() {
        val result: MutableList<TradingDefaultsItem> = mutableListOf()

        result.addStocksItems()

        result.add(TradingDefaultsItem.Divider)

        result.addOptionsItems()

        _items.postValue(result)
    }

    private fun MutableList<TradingDefaultsItem>.addOptionsItems() {
        val optionsPriceType = _optionsPriceType.value
        val optionsTerm = _optionsTerm.value

        add(TradingDefaultsItem.DividerTitle(resources.getString(R.string.options)))
        addAccountsPicker(_optionsAccountId)
        add(TradingDefaultsItem.EditorValue(resources.getString(R.string.trading_defaults_contracts), _optionsContracts))
        add(
            createPickerValue(
                R.string.price_type,
                optionsPriceType?.getLabel(),
                _optionsPriceType,
                listOf(
                    PriceType.MARKET,
                    PriceType.LIMIT,
                    PriceType.STOP,
                    PriceType.STOP_LIMIT,
                    PriceType.TRAILING_STOP_DOLLAR
                ).toSelectable(optionsPriceType, PriceType::getLabel)
            )
        )
        add(
            createPickerValue(
                R.string.term,
                optionsTerm?.getLabel(),
                _optionsTerm,
                getTermPickerItems(optionsPriceType, optionsTerm, false)
            )
        )
    }

    private fun MutableList<TradingDefaultsItem>.addStocksItems() {
        val stocksPriceType = _stocksPriceType.value
        val stocksTerm = _stocksTerm.value

        add(TradingDefaultsItem.DividerTitle(resources.getString(R.string.stocks)))
        addAccountsPicker(_stocksAccountId)
        add(TradingDefaultsItem.EditorValue(resources.getString(R.string.quantity), _stocksQuantity))
        add(
            createPickerValue(
                R.string.price_type, stocksPriceType?.getLabel(), _stocksPriceType,
                listOf(
                    PriceType.MARKET,
                    PriceType.LIMIT,
                    PriceType.STOP,
                    PriceType.STOP_LIMIT,
                    PriceType.TRAILING_STOP_DOLLAR,
                    PriceType.TRAILING_STOP_PERCENT
                ).toSelectable(stocksPriceType, PriceType::getLabel)
            )
        )
        add(
            createPickerValue(
                R.string.term,
                stocksTerm?.getLabel(),
                _stocksTerm,
                getTermPickerItems(stocksPriceType, stocksTerm, true)
            )
        )
    }

    @Suppress("LongMethod")
    private fun MutableList<TradingDefaultsItem>.addAccountsPicker(accountValue: MutableLiveData<String?>) {
        val accounts = _accounts.value
        val accountId = accountValue.value
        val selectedAccount = accounts?.run {
            find { it.id == accountId }
                ?: find { it.id == EMPTY_ACCOUNT_ID }
        }
        val noAccountLabel = resources.getString(R.string.trading_defaults_no_account_label)

        hasEligibleAccounts = checkIfEligibleAccounts(accounts)

        val items = mutableListOf(
            DropDownMultiSelector.Selectable(
                noAccountLabel,
                EMPTY_ACCOUNT_ID,
                selectedAccount?.id == EMPTY_ACCOUNT_ID
            )
        )
        accounts?.mapTo(items) { account ->
            DropDownMultiSelector.Selectable(
                account.name,
                account.id,
                account.id == accountId
            )
        }
        add(
            createPickerValue(
                resources.getString(R.string.title_account),
                selectedAccount?.name ?: noAccountLabel,
                accountValue,
                items
            ) { hasEligibleAccounts }
        )
    }

    private fun checkIfEligibleAccounts(accounts: List<TradeAccount>?) =
        when (accounts?.count()) {
            0 -> false
            1 -> accounts.first().id != EMPTY_ACCOUNT_ID
            else -> true
        }

    private fun <T> createPickerValue(
        @StringRes hint: Int,
        @StringRes valueDisplayTitle: Int?,
        value: MutableLiveData<T?>,
        itemsData: List<DropDownMultiSelector.Selectable<T>>,
        createCondition: () -> Boolean = { true }
    ): TradingDefaultsItem.PickerValue = createPickerValue(
        resources.getString(hint),
        valueDisplayTitle?.let { resources.getString(it) } ?: "",
        value,
        itemsData,
        createCondition
    )

    private fun <T> createPickerValue(
        hint: String,
        valueDisplayTitle: String,
        value: MutableLiveData<T?>,
        itemsData: List<DropDownMultiSelector.Selectable<T>>,
        createCondition: () -> Boolean = { true }
    ): TradingDefaultsItem.PickerValue {
        return TradingDefaultsItem.PickerValue(
            hint,
            valueDisplayTitle,
            if (createCondition.invoke()) {
                {
                    _showValuePicker.value = ConsumableLiveEvent(
                        createDropDownSettings(
                            hint,
                            value,
                            itemsData
                        )
                    )
                }
            } else { {} }
        )
    }

    private fun <T> createDropDownSettings(
        title: String,
        value: MutableLiveData<T?>,
        itemsData: List<DropDownMultiSelector.Selectable<T>>
    ): DropDownMultiSelector.Settings<T> = DropDownMultiSelector.Settings(
        tag = VALUE_PICKER_DROP_DOWN,
        multiSelectEnabled = false,
        title = title,
        itemsData = MutableLiveData(itemsData),
        selectListener = { _, selectable -> value.value = selectable.value }
    )

    private fun getTermPickerItems(priceType: PriceType?, term: OrderTerm?, isStocks: Boolean): List<DropDownMultiSelector.Selectable<OrderTerm>> {
        return priceType.getTerms(isStocks).toSelectable(term, OrderTerm::getLabel)
    }

    private fun <T> List<T>.toSelectable(selectedItem: T?, label: T.() -> Int): List<DropDownMultiSelector.Selectable<T>> {
        return map { item ->
            DropDownMultiSelector.Selectable(
                resources.getString(label(item)),
                item,
                item == selectedItem
            )
        }
    }

    fun saveChanges() {
        _viewState.value = TradingDefaultsViewState.Saving

        preferences.apply {
            stocksAccountId = _stocksAccountId.value ?: EMPTY_ACCOUNT_ID
            stocksQuantity = _stocksQuantity.value ?: DEFAULT_STOCKS_QUANTITY
            stocksPriceType = _stocksPriceType.value
            stocksTerm = _stocksTerm.value

            optionsAccountId = _optionsAccountId.value ?: EMPTY_ACCOUNT_ID
            optionsContracts = _optionsContracts.value?.toIntOrNull() ?: DEFAULT_OPTIONS_CONTRACTS
            optionsPriceType = _optionsPriceType.value
            optionsTerm = _optionsTerm.value
        }

        _viewState.value = TradingDefaultsViewState.Saved
    }
}
