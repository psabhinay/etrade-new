package com.etrade.mobilepro.bank

import android.os.Bundle
import android.os.Parcelable
import androidx.navigation.NavArgs
import java.io.Serializable
import java.lang.IllegalArgumentException
import java.lang.UnsupportedOperationException
import kotlin.Boolean
import kotlin.Suppress
import kotlin.jvm.JvmStatic
import org.threeten.bp.LocalDate

public data class BankDateTransactionPickerFragmentArgs(
  public val startDate: LocalDate,
  public val endDate: LocalDate,
  public val isStart: Boolean
) : NavArgs {
  @Suppress("CAST_NEVER_SUCCEEDS")
  public fun toBundle(): Bundle {
    val result = Bundle()
    if (Parcelable::class.java.isAssignableFrom(LocalDate::class.java)) {
      result.putParcelable("startDate", this.startDate as Parcelable)
    } else if (Serializable::class.java.isAssignableFrom(LocalDate::class.java)) {
      result.putSerializable("startDate", this.startDate as Serializable)
    } else {
      throw UnsupportedOperationException(LocalDate::class.java.name +
          " must implement Parcelable or Serializable or must be an Enum.")
    }
    if (Parcelable::class.java.isAssignableFrom(LocalDate::class.java)) {
      result.putParcelable("endDate", this.endDate as Parcelable)
    } else if (Serializable::class.java.isAssignableFrom(LocalDate::class.java)) {
      result.putSerializable("endDate", this.endDate as Serializable)
    } else {
      throw UnsupportedOperationException(LocalDate::class.java.name +
          " must implement Parcelable or Serializable or must be an Enum.")
    }
    result.putBoolean("isStart", this.isStart)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): BankDateTransactionPickerFragmentArgs {
      bundle.setClassLoader(BankDateTransactionPickerFragmentArgs::class.java.classLoader)
      val __startDate : LocalDate?
      if (bundle.containsKey("startDate")) {
        if (Parcelable::class.java.isAssignableFrom(LocalDate::class.java) ||
            Serializable::class.java.isAssignableFrom(LocalDate::class.java)) {
          __startDate = bundle.get("startDate") as LocalDate?
        } else {
          throw UnsupportedOperationException(LocalDate::class.java.name +
              " must implement Parcelable or Serializable or must be an Enum.")
        }
        if (__startDate == null) {
          throw IllegalArgumentException("Argument \"startDate\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"startDate\" is missing and does not have an android:defaultValue")
      }
      val __endDate : LocalDate?
      if (bundle.containsKey("endDate")) {
        if (Parcelable::class.java.isAssignableFrom(LocalDate::class.java) ||
            Serializable::class.java.isAssignableFrom(LocalDate::class.java)) {
          __endDate = bundle.get("endDate") as LocalDate?
        } else {
          throw UnsupportedOperationException(LocalDate::class.java.name +
              " must implement Parcelable or Serializable or must be an Enum.")
        }
        if (__endDate == null) {
          throw IllegalArgumentException("Argument \"endDate\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"endDate\" is missing and does not have an android:defaultValue")
      }
      val __isStart : Boolean
      if (bundle.containsKey("isStart")) {
        __isStart = bundle.getBoolean("isStart")
      } else {
        throw IllegalArgumentException("Required argument \"isStart\" is missing and does not have an android:defaultValue")
      }
      return BankDateTransactionPickerFragmentArgs(__startDate, __endDate, __isStart)
    }
  }
}
