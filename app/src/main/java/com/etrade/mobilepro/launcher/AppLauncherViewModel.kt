package com.etrade.mobilepro.launcher

import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.accessvalidation.AppAccessProvider
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.viewmodel.AppDialogViewModel
import com.etrade.mobilepro.encryption.viewmodel.CipherAuthenticatorViewModel
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.onboarding.OnboardingActivity
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.pushnotification.api.PAYLOAD_KEY
import com.etrade.mobilepro.pushnotification.api.SmartAlert
import com.etrade.mobilepro.pushnotification.api.TYPE_KEY
import com.etrade.mobilepro.pushnotification.presentation.SmartAlertNotificationFactory
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.viewmodel.ActivitySignal
import com.etrade.mobilepro.viewmodel.ActivitySignalViewModel
import kotlinx.coroutines.launch
import javax.crypto.Cipher
import javax.inject.Inject

class AppLauncherViewModel @Inject constructor(
    private val appLauncherMigration: AppLauncherMigration,
    private val applicationPreferences: ApplicationPreferences,
    private val cipherAuthenticatorViewModelDelegate: CipherAuthenticatorViewModel,
    private val appAccessProvider: AppAccessProvider
) : ViewModel(), ActivitySignalViewModel, AppDialogViewModel, CipherAuthenticatorViewModel by cipherAuthenticatorViewModelDelegate {

    override val activitySignal: LiveData<ConsumableLiveEvent<ActivitySignal>>
        get() = _activitySignal

    private val _dialog = MediatorLiveData<ConsumableLiveEvent<AppDialog?>?>().apply {
        addSource(appAccessProvider.dialog) { value = it }
    }
    override val dialog: LiveData<ConsumableLiveEvent<AppDialog?>?> = _dialog

    val authenticateCipherSignal: LiveData<ConsumableLiveEvent<Cipher>>
        get() = _authenticateCipherSignal

    private val _activitySignal = MediatorLiveData<ConsumableLiveEvent<ActivitySignal>>().apply {
        addSource(appAccessProvider.activitySignal) { value = it }
    }
    private val _authenticateCipherSignal: MutableLiveData<ConsumableLiveEvent<Cipher>> = MutableLiveData()

    init {
        requestAccess()
    }

    private fun migrateFromOldVersion() {
        viewModelScope.launch {
            appLauncherMigration.migrate { cipher ->
                cipherAuthenticatorViewModelDelegate.authenticate(cipher) {
                    _authenticateCipherSignal.value = it.consumable()
                }
            }
            onEulaAccepted()
        }
    }

    private fun onEulaAccepted() {
        if (applicationPreferences.shouldDisplayOnboarding) {
            showOnboarding()
        } else {
            showMainActivity()
        }
    }

    private fun sendActivitySignal(signal: ActivitySignal) {
        _activitySignal.value = signal.consumable()
    }

    private fun requestAccess() {
        appAccessProvider.requestAccess(viewModelScope) {
            migrateFromOldVersion()
        }
    }

    private fun showMainActivity() {
        sendActivitySignal { activity ->
            activity.apply {
                val smartAlert = activity.intent.createSmartAlert()
                startActivity(
                    Intent(this, MainActivity::class.java).apply {
                        smartAlert?.let {
                            putExtra(SmartAlertNotificationFactory.EXTRA_SMART_ALERT_KEY, it)
                        }
                    }
                )
                finish()
            }
        }
    }

    private fun Intent.createSmartAlert(): SmartAlert? {
        return extras?.let {
            val type = it.getString(TYPE_KEY)
            val message = it.getString(PAYLOAD_KEY)
            SmartAlert.create(type = type, message = message)
        }
    }

    private fun showOnboarding() {
        sendActivitySignal { activity ->
            activity.apply {
                startActivity(Intent(activity, OnboardingActivity::class.java))
                finish()
            }
        }
    }
}
