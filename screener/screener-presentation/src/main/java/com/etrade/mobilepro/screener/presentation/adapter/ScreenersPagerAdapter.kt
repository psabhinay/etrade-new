package com.etrade.mobilepro.screener.presentation.adapter

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.etf.EtfScreenersFragment
import com.etrade.mobilepro.screener.presentation.mutualfund.MutualFundScreenersFragment
import com.etrade.mobilepro.screener.presentation.stock.StockScreenersFragment
import com.etrade.mobilepro.util.android.adapter.DefaultFragmentPagerAdapter

class ScreenersPagerAdapter(fragment: Fragment) : DefaultFragmentPagerAdapter(
    fragment,
    listOf(
        FragmentPagerItem(StockScreenersFragment::class.java, R.string.screeners_stock_title),
        FragmentPagerItem(MutualFundScreenersFragment::class.java, R.string.screeners_mutual_fund_title),
        FragmentPagerItem(EtfScreenersFragment::class.java, R.string.screeners_etf_title)
    )
)
