package com.etrade.mobilepro.quote.screen.presentation.adapter

import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemRangeBinding
import com.etrade.mobilepro.quoteapi.QuoteItem

class QuoteRangeViewHolder(
    private val binding: QuoteItemRangeBinding,
    lifecycleOwner: LifecycleOwner
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.quoteWeekRange.setLifecycleOwner(lifecycleOwner)
    }

    fun bindTo(item: QuoteItem.QuoteRangeBarItem?) {
        item?.let {
            binding.quoteWeekRange.setData(item.rangeBarData)
        }
    }
}
