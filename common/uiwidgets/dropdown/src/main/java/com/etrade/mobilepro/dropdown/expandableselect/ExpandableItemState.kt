package com.etrade.mobilepro.dropdown.expandableselect

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/**
 * This is used to save last expanded item state on configuration change
 */
@Parcelize
internal class ExpandableItemState(
    val isExpanded: Boolean,
    val hashCodeValue: Int
) : Parcelable
