package com.etrade.mobilepro.appwidget.common // Intentional wrong package by legacy commit

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
@Deprecated(
    """
This class shouldn't be used anymore. The sole purposed of restoring this previously deleted class 
is for addressing a crash reported in ETAND-17176.
"""
)
internal data class WidgetDimens(
    val width: Int,
    val textSize16: Float,
    val textSize14: Float,
    val textSize12: Float,
    val textSize10: Float
) : Parcelable
