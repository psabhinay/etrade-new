package org.chromium.customtabsclient.shared

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentFactory
import com.etrade.mobilepro.baseactivity.ActivityNetworkConnectionDelegate
import com.etrade.mobilepro.baseactivity.ActivitySessionDelegate
import com.etrade.mobilepro.common.LoginChecker
import com.etrade.mobilepro.common.setupToolbarWithUpButton
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

/**
 * This Activity is used for the following use cases:
 * - To open internal web sites of the company.
 * - As a fallback when there is no browser installed that supportsChrome Custom Tabs.
 */
open class WebviewActivity : AppCompatActivity(), HasAndroidInjector {
    @Inject
    lateinit var activitySessionDelegate: ActivitySessionDelegate

    @Inject
    lateinit var activityNetworkConenectionDelegate: ActivityNetworkConnectionDelegate

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var fragmentInjectionFactory: FragmentFactory

    @Inject
    lateinit var loginChecker: LoginChecker

    private val url: String? by lazy { intent.getUrl() }
    private val externalUrlView: Boolean by lazy { intent.getExternalUrlView() }
    private val screenTitle: String? by lazy { intent.getTitle() ?: url }
    private val upIconResId: Int by lazy { intent.getUpIconResId() }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        supportFragmentManager.fragmentFactory = fragmentInjectionFactory
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_webview)

        screenTitle?.let { setupToolbarWithUpButton(title = it, upButtonResId = upIconResId) }
        if (savedInstanceState == null) {
            instantiateFragment()
        }
        activityNetworkConenectionDelegate.initNetworkConnectivityBanner(this)

        activitySessionDelegate.observeTimeoutEvent(this)
    }

    override fun onStart() {
        super.onStart()
        loginChecker.check(this)
    }

    open fun instantiateFragment() {
        fragmentInjectionFactory.instantiateFragment(WebViewFragment::class.java, createFragmentArgs()).apply {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, this)
                .commit()
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        activitySessionDelegate.onUserInteraction(this)
    }

    protected fun createFragmentArgs(): Bundle {
        return WebViewFragmentArgs(screenTitle, url, externalUrlView = externalUrlView).toBundle()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        /**
         * Creates an intent to start [WebviewActivity].
         *
         * @param context application context
         * @param url an url to open
         * @param titleResId resource ID of a titleResId to display
         *
         * @return the intent to start the activity
         */
        fun intent(context: Context, url: String, @StringRes titleResId: Int = 0): Intent {
            return context.createWebViewActivityIntent(
                activityClass = WebviewActivity::class.java,
                url = url,
                titleResId = titleResId
            )
        }

        /**
         * Creates an intent to start [WebviewActivity].
         *
         * @param context application context
         * @param url an url to open
         * @param title string title to display
         * @param upIconResId up button icon res
         * @param externalUrlView open url on external app
         *
         * @return the intent to start the activity
         */
        @Suppress("LongParameterList")
        fun intent(context: Context, url: String, title: String, @DrawableRes upIconResId: Int = 0, externalUrlView: Boolean = false): Intent {
            return context.createWebViewActivityIntent(
                activityClass = WebviewActivity::class.java,
                url = url,
                title = title,
                upIconResId = upIconResId,
                externalUrlView = externalUrlView
            )
        }
    }
}
