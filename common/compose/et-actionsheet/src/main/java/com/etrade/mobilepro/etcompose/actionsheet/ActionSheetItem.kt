package com.etrade.mobilepro.etcompose.actionsheet

data class ActionSheetItem(
    val title: String,
    val onClick: () -> Unit = {}
)
