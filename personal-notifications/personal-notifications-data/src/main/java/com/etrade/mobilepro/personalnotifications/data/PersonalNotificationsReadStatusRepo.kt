package com.etrade.mobilepro.personalnotifications.data

import com.etrade.mobilepro.common.CommonRealmModule
import com.etrade.mobilepro.common.readstatus.BaseReadStatusRepo
import io.realm.RealmConfiguration
import javax.inject.Inject

class PersonalNotificationsReadStatusRepo @Inject constructor() : BaseReadStatusRepo() {
    override val realmConfig: RealmConfiguration
        get() =
            RealmConfiguration.Builder()
                .name("personal.notifications.history")
                .modules(CommonRealmModule())
                .build()
}
