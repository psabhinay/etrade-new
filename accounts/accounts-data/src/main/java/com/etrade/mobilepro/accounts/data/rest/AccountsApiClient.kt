package com.etrade.mobilepro.accounts.data.rest

import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.GET
import retrofit2.http.Header

interface AccountsApiClient {
    @GET("v1/accounts.json")
    @JsonMoshi
    suspend fun getAccountsDetails(
        @Header("DataToken") consumerKey: String?
    ): AccountListResponseHolderDto
}
