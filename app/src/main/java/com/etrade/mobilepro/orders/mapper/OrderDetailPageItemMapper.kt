package com.etrade.mobilepro.orders.mapper

import com.etrade.mobilepro.R
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderDetailDescription
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.api.OrderLegItemInterface
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem
import com.etrade.mobilepro.order.details.presentation.OrderOptionsRatio
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.presentation.section.OrderDetailSectionMapper

class OrderDetailPageItemMapper(
    private val orderDetailSectionMapper: OrderDetailSectionMapper
) {
    @Suppress("LongMethod", "ComplexMethod", "NestedBlockDepth") // Lots of conditional mapping
    fun map(
        orderDetail: BaseOrderDetailsItem,
        index: Int,
        orderType: OrderType,
        bidAskItem: OrderDetailDisplayItem.OrderBidAskItem,
        formattedRatio: OrderOptionsRatio?
    ): List<OrderDetailItemInterface> {
        val items: MutableList<OrderDetailItemInterface> = mutableListOf()

        when (orderDetail) {
            is BaseOrderDetailsItem.OrderDetailsItem -> {
                if (orderType.isConditional() && index > 0) {
                    when (orderType) {
                        OrderType.CONDITIONAL_ONE_CANCEL_OTHER ->
                            items.add(OrderDetailDisplayItem.OrderSectionHeader(R.string.place_order_or))
                        OrderType.CONDITIONAL_OTOCO -> {
                            if (index == 1) {
                                items.add(OrderDetailDisplayItem.OrderSectionHeader(R.string.then_place_oco_order))
                            } else {
                                items.add(OrderDetailDisplayItem.OrderSectionHeader(R.string.place_order_or))
                            }
                        }
                        OrderType.CONDITIONAL_ONE_TRIGGER_OTHER -> {
                            val labelId = if (index == 2) {
                                R.string.orders_divider_conditional_oto_2
                            } else {
                                R.string.orders_divider_conditional_oto_1
                            }
                            items.add(OrderDetailDisplayItem.OrderSectionHeader(labelId))
                        }

                        else -> throw IllegalArgumentException("Unsupported conditional order")
                    }
                }
                items.addAll(
                    orderDetail.orderDescriptions.map {
                        it.toOrderLegItem()
                    }
                )

                items.add(bidAskItem)

                formattedRatio?.let {
                    if (it.ratioLabel.value != null) {
                        items.add(it)
                    }
                }

                items.add(OrderDetailDisplayItem.OrderSectionItem(orderDetailSectionMapper.map(orderDetail, orderType.isConditional())))
                if (!orderType.isConditional() && orderDetail.hasPartialExecution) {
                    orderDetail.orderDescriptions
                        .forEach {
                            items.add(it.toOrderExtraLegItem())
                            items.add(OrderDetailDisplayItem.OrderSectionItem(orderDetailSectionMapper.map(orderDetail, it)))
                        }
                }
            }
            is BaseOrderDetailsItem.AdvancedOrderItem -> {
                items.add(OrderDetailDisplayItem.OrderSectionHeader(R.string.contingent_header, index + 1))
                items.add(
                    OrderDetailDisplayItem.OrderLegItem(
                        displaySymbol = orderDetail.displaySymbol,
                        isAdvancedOrder = true,
                        originalSymbol = orderDetail.symbol
                    )
                )

                items.add(bidAskItem)

                items.add(
                    OrderDetailDisplayItem.OrderSectionItem(
                        orderDetailSectionMapper.map(orderDetail, false)
                    )
                )
            }
            is BaseOrderDetailsItem.StockPlanOrderDetailsItem -> {
                items.add(
                    OrderDetailDisplayItem.OrderLegItem(
                        displaySymbol = "${orderDetail.orderType} ${orderDetail.symbol}",
                        isAdvancedOrder = false
                    )
                )
                items.add(bidAskItem)
                items.add(
                    OrderDetailDisplayItem.OrderSectionItem(
                        orderDetailSectionMapper.map(orderDetail, false)
                    )
                )
            }
        }
        return items
    }
}

private fun OrderDetailDescription.toOrderLegItem(): OrderLegItemInterface {
    return OrderDetailDisplayItem.OrderLegItem(
        displaySymbol = displaySymbol,
        displayQuantity = displayQuantity,
        transactionType = transactionType,
        underlyingProductId = underlyingProductId,
        isAdvancedOrder = isAdvancedOrder,
        originalSymbol = originalSymbol
    )
}

private fun OrderDetailDescription.toOrderExtraLegItem(): OrderLegItemInterface {
    return OrderDetailDisplayItem.OrderExtraLegItem(
        displaySymbol = displaySymbol,
        displayQuantity = displayQuantity,
        transactionType = transactionType
    )
}
