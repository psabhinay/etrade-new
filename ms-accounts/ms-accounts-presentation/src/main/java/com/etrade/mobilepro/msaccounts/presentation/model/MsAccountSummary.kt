package com.etrade.mobilepro.msaccounts.presentation.model

import com.etrade.mobilepro.completeview.model.AccountSummarySection

data class MsAccountSummary(
    val title: CharSequence,
    val titleDescription: String,
    val additionalLabels: List<AccountSummarySection>
)
