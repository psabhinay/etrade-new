package com.etrade.mobilepro.trade.data.form

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.getExpirationDateFromOsiKey
import com.etrade.mobilepro.instrument.getOptionType
import com.etrade.mobilepro.instrument.getStrikePrice
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.api.option.isOption
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.etrade.mobilepro.util.ValueCoder
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import javax.inject.Inject

class TradeLegsCoder @Inject constructor(moshi: Moshi) : ValueCoder<List<@JvmSuppressWildcards TradeLeg>> {

    private val adapter = moshi.adapter<List<TradeLeg>>(Types.newParameterizedType(List::class.java, LegDetailsCommonDto::class.java))

    override fun encode(value: List<TradeLeg>): Any = adapter.toJson(value.toLegDetailsDtoList())

    override fun decode(encoded: Any): List<TradeLeg>? = (encoded as? String)?.let { adapter.fromJson(it) }
}

internal fun Iterable<TradeLeg>.toLegDetailsDtoList(): List<LegDetailsCommonDto> = map { it.toLegDetailsDto() }

internal fun TradeLeg.toLegDetailsDto(withProductType: Boolean = false): LegDetailsCommonDto {
    val option = isOption
    // Expiration date and strike price are required to perform preview request.
    val expirationDate = symbol.takeIf { option }?.let { getExpirationDateFromOsiKey(it) }
    return LegDetailsCommonDto().also {
        it.symbol = symbol
        it.quantity = quantity
        it.transactionCode = transactionType.typeCode.toString()
        it.expiryDay = expirationDate?.day?.toString()
        it.expiryMonth = expirationDate?.month?.toString()
        it.expiryYear = expirationDate?.year?.toString()
        it.optionType = getOptionType(symbol)?.requestCode // Strategy call requires this field to be set for some strategies to be determined correctly.
        it.isAMOption = isAMOption
        it.strikePrice = getStrikePrice(symbol)?.toPlainString() // In some cases when legs need to be reordered this is required to determine a strategy.
        if (withProductType) {
            // For saving conditional orders we need to specify a product type explicitly.
            it.productType = if (isOption) {
                InstrumentType.OPTN
            } else {
                InstrumentType.EQ
            }.typeCode
        }
    }
}
