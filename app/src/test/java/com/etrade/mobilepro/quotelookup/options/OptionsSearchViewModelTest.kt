package com.etrade.mobilepro.quotelookup.options

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.searching.dto.SymbolLookupResponseDto
import com.etrade.mobilepro.searching.mapper.SymbolLookupMapper
import com.etrade.mobilepro.searching.rest.SymbolSearchApiClient
import com.etrade.mobilepro.searching.symbol.DefaultSymbolLookupRepo
import com.etrade.mobilepro.searchingapi.SymbolLookupRepo
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.testutil.XmlDeserializer
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

internal class OptionsSearchViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private fun mockSearchService(symbolLookupResponse: SymbolLookupResponseDto = SymbolLookupResponseDto()) = mock<SymbolSearchApiClient> {
        on { symbolLookup(searchQuery.value, "OPTNC", searchQuery.expiryDay, searchQuery.expiryMonth, searchQuery.expiryYear, 2, 1) } doReturn Single.just(
            symbolLookupResponse
        )
    }

    @Test
    fun `returns error view state when call is not successful`() {
        val mockObserver = mock<Observer<StockOptionsViewModel.ViewState>>()
        val mockMoverService = mock<SymbolSearchApiClient> {
            on { symbolLookup(searchQuery.value, "OPTNC", searchQuery.expiryDay, searchQuery.expiryMonth, searchQuery.expiryYear, 2, 1) } doReturn Single.error(
                RuntimeException("Something went wrong")
            )
        }
        val symbolLookUpRo: SymbolLookupRepo = DefaultSymbolLookupRepo(mockMoverService, SymbolLookupMapper()::map)
        val vm = StockOptionsViewModel(symbolLookUpRo, mock(), mock())
        vm.search(searchQuery)
        vm.results.observeForever(mockObserver)

        verify(mockObserver).onChanged(eq(StockOptionsViewModel.ViewState.Error("Something went wrong")))
    }

    @Test
    fun `returns view state with empty result on success`() {
        val mockObserver = mock<Observer<StockOptionsViewModel.ViewState>>()

        val symbolLookupResponse = XmlDeserializer.getObjectFromXml("empty_symbol_lookup.xml", SymbolLookupResponseDto::class.java)
        val symbolLookUpRo: SymbolLookupRepo = DefaultSymbolLookupRepo(mockSearchService(symbolLookupResponse), SymbolLookupMapper()::map)

        val vm = StockOptionsViewModel(symbolLookUpRo, mock(), mock())
        vm.search(searchQuery)
        vm.results.observeForever(mockObserver)

        val result = vm.results.value as StockOptionsViewModel.ViewState.Success
        Assert.assertEquals(0, result.results.searchResultList.size)
        Assert.assertEquals(0, result.results.optionMonths.size)
        Assert.assertEquals("No Options were found for that symbol.", result.results.errorInfo?.errorDescription)
    }

    @Test
    fun `returns view state with result on success`() {
        val mockObserver = mock<Observer<StockOptionsViewModel.ViewState>>()

        val symbolLookupResponse = XmlDeserializer.getObjectFromXml("symbol_lookup.xml", SymbolLookupResponseDto::class.java)
        val symbolLookUpRo: SymbolLookupRepo = DefaultSymbolLookupRepo(mockSearchService(symbolLookupResponse), SymbolLookupMapper()::map)

        val vm = StockOptionsViewModel(symbolLookUpRo, mock(), mock())
        vm.search(searchQuery)
        vm.results.observeForever(mockObserver)

        val result = vm.results.value as StockOptionsViewModel.ViewState.Success
        Assert.assertEquals(40, result.results.searchResultList.size)
        Assert.assertEquals("125.00", result.results.searchResultList[0].strike)
        Assert.assertEquals(13, result.results.optionMonths.size)
        Assert.assertEquals(26, result.results.optionMonths[0].originSelectedDate.day)
    }

    private val searchQuery = SearchQuery(
        type = SearchType.CALL_OPTION,
        value = "FB",
        description = "FACEBOOK DESC",
        expiryDay = "0",
        expiryMonth = "0",
        expiryYear = "0"
    )
}
