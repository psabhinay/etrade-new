package com.etrade.mobilepro.trade.presentation.form.delegate.dependencies

import android.content.res.Resources
import com.etrade.mobilepro.trade.form.api.input.ContingentOptionTradeFormInputId

internal class ContingentOptionFactoryDelegateDependenciesImpl(
    inputId: ContingentOptionTradeFormInputId,
    resources: Resources
) : StockOptionsFactoryDelegateDependenciesImpl(inputId, resources) {

    override val saveAndPreviewActionRequiredInputs: Set<String> = setOf(
        inputId.accountId,
        inputId.securityType,
        inputId.lookupSymbol,
        inputId.priceType,
        inputId.term,
        inputId.limitPrice,
        inputId.limitPriceForStopLimit,
        inputId.stopPrice,
        inputId.stopPriceForStopLimit,
        inputId.advancedOrderType,
        inputId.contingentOrderInputId.symbol,
        inputId.contingentOrderInputId.condition,
        inputId.contingentOrderInputId.qualifier,
        inputId.contingentOrderInputId.conditionValue
    )
}
