package com.etrade.mobilepro.personalnotifications.api

import com.etrade.mobilepro.inboxmessages.InboxMessage

sealed class PersonalNotification(val identifier: String) {
    class ExternalAccountVerification(identifier: String) : PersonalNotification(identifier)

    class PendingTransaction(
        identifier: String,
        val accountUuid: String,
        val amount: String,
        val fundAvailabilityDate: String
    ) : PersonalNotification(identifier)

    sealed class Localytics(val message: InboxMessage) : PersonalNotification(message.id.toString()) {
        class EmergencyBroadcast(message: InboxMessage) : Localytics(message)
        class Inline(message: InboxMessage) : Localytics(message)
        class Banner(message: InboxMessage) : Localytics(message)
    }
}
