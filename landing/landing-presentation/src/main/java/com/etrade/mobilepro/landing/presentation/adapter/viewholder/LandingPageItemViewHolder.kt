package com.etrade.mobilepro.landing.presentation.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.landing.presentation.adapter.LandingPageItem
import com.etrade.mobilepro.landing.presentation.databinding.LandingPageSelectionItemBinding

internal class LandingPageItemViewHolder(
    private val binding: LandingPageSelectionItemBinding,
    listener: (LandingPageItem) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    private var item: LandingPageItem? = null

    init {
        binding.button.setOnClickListener {
            item?.let(listener)
        }
    }

    fun bindTo(item: LandingPageItem) {
        this.item = item
        binding.button.text = item.value.title
    }
}
