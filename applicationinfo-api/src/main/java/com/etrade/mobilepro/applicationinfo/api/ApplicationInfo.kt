package com.etrade.mobilepro.applicationinfo.api

interface ApplicationInfo {

    /**
     * Checks if the app runs on tablet (`true`) or phone (`false`).
     *
     * NOTE. DO NOT USE IT FOR BUILDING A UI.
     */
    val isTablet: Boolean

    /**
     * The type of device is being used.
     *
     * Possible values: `AND-TAB` for tablet and `AND-PHN` for mobile phone.
     */
    val deviceType: String

    fun connectionType(): String?
    fun appVersionCode(): String
    fun appVersionName(): String
    val appMajorMinorVersion: String
    fun operationSystemVersion(): String
    fun deviceDetails(): String
    fun platform(): String
    fun deviceId(): String
}
