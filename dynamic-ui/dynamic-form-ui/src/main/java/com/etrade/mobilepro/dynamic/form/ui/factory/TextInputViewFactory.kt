package com.etrade.mobilepro.dynamic.form.ui.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.api.TextInput
import com.etrade.mobilepro.dynamic.form.ui.TextInputView

internal object TextInputViewFactory : ComponentViewFactory<TextInput, TextInputView> {
    override fun createView(context: Context): TextInputView = TextInputView(context)
}
