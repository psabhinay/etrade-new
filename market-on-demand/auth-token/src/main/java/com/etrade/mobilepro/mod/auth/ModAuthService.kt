package com.etrade.mobilepro.mod.auth

import com.etrade.eo.rest.retrofit.Xml
import org.simpleframework.xml.Root
import org.simpleframework.xml.Text
import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Query

interface ModAuthService {

    @Root(name = "WSODResearchReporturl", strict = false)
    data class WsodResearchReportUrlResponse(
        @field:Text
        var reportUrl: String? = null
    )

    @Xml
    @POST("/e/t/mobile/GetWSODResearchReportUrl")
    fun getWsodResearchReportUrl(@Query("Ticker") ticker: String): Call<WsodResearchReportUrlResponse>
}
