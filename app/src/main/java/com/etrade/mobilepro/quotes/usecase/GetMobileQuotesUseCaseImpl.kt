package com.etrade.mobilepro.quotes.usecase

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuotesParameter
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuotesUseCase
import kotlinx.coroutines.rx2.awaitLast
import javax.inject.Inject

class GetMobileQuotesUseCaseImpl @Inject constructor(
    private val mobileQuoteRepo: MobileQuoteRepo
) : GetMobileQuotesUseCase {

    override suspend fun execute(parameter: GetMobileQuotesParameter): ETResult<List<MobileQuote>> {
        return mobileQuoteRepo.loadMobileQuotes(
            symbols = parameter.symbols.joinToString(","),
            requireExchangeCode = parameter.requireExchangeCode
        ).runCatchingET { awaitLast() }
    }
}
