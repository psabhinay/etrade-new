package com.etrade.mobilepro.feature.presentation.viewmodel

import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.feature.DelegatingFeatureFlagRepo
import com.etrade.mobilepro.feature.Feature
import com.etrade.mobilepro.feature.FeatureFlagProvider
import com.etrade.mobilepro.feature.FeatureFlagRepo
import com.etrade.mobilepro.feature.FeatureFlagStore
import com.etrade.mobilepro.feature.RemoteFeatureFlagProvider
import com.etrade.mobilepro.feature.presentation.R
import com.etrade.mobilepro.feature.qualifiers.AvailableFeatures
import com.etrade.mobilepro.feature.qualifiers.RemoteEnabledFeatures
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

abstract class FeatureFlagsViewModel : ViewModel() {
    abstract val featureFlagItemModels: List<FeatureFlagItemViewModel>

    abstract val actionItemModels: List<AsyncActionViewModel>
}

@ExperimentalCoroutinesApi
class DefaultFeatureFlagsViewModel @Inject constructor(
    private val featureFlagProvider: FeatureFlagRepo,
    @AvailableFeatures
    private val features: List<@JvmSuppressWildcards Feature>,
    @RemoteEnabledFeatures
    private val remoteFeatures: Set<@JvmSuppressWildcards Feature>,
    private val resources: Resources
) : FeatureFlagsViewModel() {

    private val providers: List<FeatureFlagProvider> = when (featureFlagProvider) {
        is DelegatingFeatureFlagRepo -> featureFlagProvider.providers
        is FeatureFlagProvider -> listOf(featureFlagProvider)
        else -> emptyList()
    }

    private val featureStore: FeatureFlagStore? by lazy {
        providers.firstOrNull { it is FeatureFlagStore } as? FeatureFlagStore
    }

    override val featureFlagItemModels by lazy {
        features.map { feature ->
            FeatureFlagItemViewModel(
                enabledFactory = { featureFlagProvider.isFeatureEnabled(feature) },
                providerStatusFactory = { getProviderStatus(feature) },
                feature = feature,
                isSettable = featureStore != null,
                remoteConfig = remoteFeatures.contains(feature)
            ) { featureStore?.setFeatureEnabled(feature, it) }
        }
    }

    override val actionItemModels: List<AsyncActionViewModel> by lazy {
        providers.mapNotNull { provider ->
            when (provider) {
                is FeatureFlagStore -> {
                    DefaultAsyncActionViewModel(
                        title = provider.actionTitle(R.string.feature_flags_clear_store_title),
                        scope = viewModelScope
                    ) {
                        runCatchingET {
                            provider.clearFeatures()
                        }
                    }
                }
                is RemoteFeatureFlagProvider -> {
                    DefaultAsyncActionViewModel(
                        title = provider.actionTitle(R.string.feature_flags_refresh_remote_title),
                        scope = viewModelScope,
                        action = provider::refreshFeatureFlags
                    )
                }
                else -> null
            }
        }
    }

    private fun FeatureFlagProvider.actionTitle(@StringRes titleRes: Int): String =
        resources.getString(titleRes, title)

    private fun getProviderStatus(feature: Feature): Flow<String> = combine(
        providers.map { it.enabledOrNull(feature) }
    ) { providersEnabled ->
        providersEnabled
            .mapIndexedNotNull { index, enabled -> enabled?.let { providers[index].status(it) } }
            .plus(feature.defaultStatus)
            .joinToString()
    }

    private fun FeatureFlagProvider.enabledOrNull(feature: Feature): Flow<Boolean?> {
        return hasFeature(feature).flatMapLatest { hasFeature ->
            if (hasFeature) {
                isFeatureEnabled(feature)
            } else {
                flowOf(null)
            }
        }
    }

    private fun FeatureFlagProvider.status(flagEnabled: Boolean): String =
        resources.getString(R.string.feature_flags_provider_status, title, flagEnabled.yesOrNo)

    private val Feature.defaultStatus: String
        get() = resources.getString(
            R.string.feature_flags_provider_status_default,
            defaultValue.yesOrNo
        )

    private val Boolean.yesOrNo: String
        get() = resources.getString(yesOrNoRes())
}

@StringRes
internal fun Boolean.yesOrNoRes(): Int = if (this) R.string.yes else R.string.no
