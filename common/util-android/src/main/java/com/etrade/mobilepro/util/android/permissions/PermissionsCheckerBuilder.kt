package com.etrade.mobilepro.util.android.permissions

import androidx.activity.ComponentActivity
import androidx.fragment.app.Fragment

class SinglePermissionsChecker(
    private val permission: String,
    private val fragment: Fragment? = null,
    private val activity: ComponentActivity? = null
) {

    fun withListener(listener: SinglePermissionCheckerListener): PermissionsChecker {
        return PermissionsChecker(
            permissions = listOf(permission),
            fragment = this@SinglePermissionsChecker.fragment,
            activity = this@SinglePermissionsChecker.activity,
            singleListener = listener
        )
    }
}

class MultiplePermissionsChecker(
    private val permissions: List<String>,
    private val fragment: Fragment? = null,
    private val activity: ComponentActivity? = null
) {

    fun withListener(listener: MultiplePermissionsCheckerListener): PermissionsChecker {
        return PermissionsChecker(
            permissions = this@MultiplePermissionsChecker.permissions,
            fragment = this@MultiplePermissionsChecker.fragment,
            activity = this@MultiplePermissionsChecker.activity,
            multipleListener = listener
        )
    }
}

open class PermissionsCheckerBuilder(
    private val fragment: Fragment? = null,
    private val activity: ComponentActivity? = null
) {

    fun withPermission(permission: String): SinglePermissionsChecker {
        return SinglePermissionsChecker(permission, fragment, activity)
    }

    fun withPermissions(permissions: List<String>): MultiplePermissionsChecker {
        return MultiplePermissionsChecker(permissions, fragment, activity)
    }
}

class FragmentPermissionsChecker(fragment: Fragment) : PermissionsCheckerBuilder(fragment = fragment)

class ActivityPermissionsChecker(activity: ComponentActivity) :
    PermissionsCheckerBuilder(activity = activity)
