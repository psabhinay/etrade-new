package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.trade.form.api.value.AmountToSellValueId
import com.etrade.mobilepro.trade.form.api.value.InvestmentTypeValueId

interface MutualFundTradeFormInputId :
    WithActionTradeFormInputId,
    WithFundToBuyTradeFormInputId,
    WithSymbolTradeFormInputId {
    val amount: String
    val quantity: QuantityInputId
    val positionTypeToTrade: String
    val amountToSellValueId: AmountToSellValueId
    val entireCashPosition: String
    val entireMarginPosition: String
    val investmentType: String
    val investmentTypeValueId: InvestmentTypeValueId
}

interface QuantityInputId {
    val dollars: PositionTypeInputId
    val shares: PositionTypeInputId
}

interface PositionTypeInputId {
    val cash: String
    val margin: String
}
