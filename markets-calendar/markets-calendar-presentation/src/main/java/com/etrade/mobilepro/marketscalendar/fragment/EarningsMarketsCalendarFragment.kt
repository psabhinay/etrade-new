package com.etrade.mobilepro.marketscalendar.fragment

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.marketscalendar.MarketCalendarRouter
import com.etrade.mobilepro.marketscalendar.R
import com.etrade.mobilepro.marketscalendar.api.model.EventsEarnings
import com.etrade.mobilepro.marketscalendar.data.dto.EARNINGS_EVENT
import com.etrade.mobilepro.marketscalendar.viewmodel.EarningsEventsMarketsCalendarViewModel
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

@RequireLogin
class EarningsMarketsCalendarFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    router: MarketCalendarRouter
) : BaseResultMarketsCalendarFragment<EventsEarnings, EarningsEventsMarketsCalendarViewModel>(viewModelFactory, snackbarUtilFactory, router) {

    override val noResultMessage: Int = R.string.no_result_earnings

    override val title: Int = R.string.earnings_details

    override val createViewModel: (ViewModelProvider) -> EarningsEventsMarketsCalendarViewModel =
        { it.get(EARNINGS_EVENT, EarningsEventsMarketsCalendarViewModel::class.java) }
}
