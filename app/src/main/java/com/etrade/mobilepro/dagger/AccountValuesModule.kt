package com.etrade.mobilepro.dagger

import com.etrade.eo.accountvalues.api.AccountValuesVO
import com.etrade.eo.accountvalues.streaming.StreamingValues
import com.etrade.eo.streaming.SubscriptionManager
import com.etrade.mobilepro.accountvalues.DefaultStreamingAccountValuesProvider
import com.etrade.mobilepro.accountvalues.api.AccountValuesProvider
import com.etrade.mobilepro.accountvalues.completeview.CompleteViewStreamingAccountValuesProvider
import com.etrade.mobilepro.accountvalues.completeview.toCalculationResult
import com.etrade.mobilepro.accountvalues.pnl.calculator.AccountPnlValuesCalculator
import com.etrade.mobilepro.accountvalues.pnl.calculator.AccountPnlValuesCalculatorImpl
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult
import com.etrade.mobilepro.common.di.ClientSideCalculations
import com.etrade.mobilepro.common.di.CompleteView
import com.etrade.mobilepro.common.di.Default
import com.etrade.mobilepro.common.di.Level1Streamer
import com.etrade.mobilepro.streaming.base.DefaultSubscriptionManagerHelper
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import com.etrade.mobilepro.streaming.client.side.calculations.ClientCalculationsSubscriptionManagerHelper
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */
@Module(
    includes = [
        StreamingModule::class
    ]
)
abstract class AccountValuesModule {

    @Default
    @Binds
    abstract fun bindAccountValuesProvider(provider: DefaultStreamingAccountValuesProvider): AccountValuesProvider

    @CompleteView
    @Binds
    abstract fun bindCompleteViewAccountValuesProvider(provider: CompleteViewStreamingAccountValuesProvider): AccountValuesProvider

    @Binds
    abstract fun bindAccountPnlValuesCalculator(provider: AccountPnlValuesCalculatorImpl): AccountPnlValuesCalculator

    @Binds
    @Singleton
    @ClientSideCalculations
    abstract fun bindClientCalculationsSubscriptionManagerHelper(
        provider: ClientCalculationsSubscriptionManagerHelper
    ): SubscriptionManagerHelper<AccountValuesVO>

    companion object {

        @Default
        @Provides
        @Singleton
        fun provideLevel1SubscriptionManagerHelper(
            @Level1Streamer subscriptionManager: SubscriptionManager
        ): SubscriptionManagerHelper<AccountValuesVO> {
            return DefaultSubscriptionManagerHelper(subscriptionManager) {
                StreamingValues(it, "") // account number is not used
            }
        }

        @Provides
        @Singleton
        fun provideCompleteViewSubscriptionManagerHelper(
            @Level1Streamer subscriptionManager: SubscriptionManager
        ): SubscriptionManagerHelper<CalculationResult> {
            return DefaultSubscriptionManagerHelper(subscriptionManager) {
                it.toCalculationResult()
            }
        }
    }
}
