package com.etrade.mobilepro.transfermoney.usecase

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.transfermoney.api.TransferCalendarHolidays
import com.etrade.mobilepro.transfermoney.api.repo.TransferCalendarRepo
import com.etrade.mobilepro.transfermoney.usecase.calendar.GetTransferCalendarRequest
import com.etrade.mobilepro.transfermoney.usecase.calendar.GetTransferCalendarUseCaseImpl
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.threeten.bp.LocalDate

@ExperimentalCoroutinesApi
internal class GetTransferCalendarUseCaseTest {

    private fun testData(holidays: List<TransferCalendarHolidays>, expected: Set<LocalDate>) {
        val repo: TransferCalendarRepo = mock {
            onBlocking { getCalendar() } doReturn ETResult.success(holidays)
        }
        val case = GetTransferCalendarUseCaseImpl(repo)

        runBlockingTest {
            val result = case.execute(GetTransferCalendarRequest(2019))

            assertNotNull(result)
            assertTrue(result.isSuccess)

            assertEquals(expected, result.getOrThrow())
        }
    }

    @Test
    fun `should return valid result`() {
        testData(
            listOf(
                TransferCalendarHolidays("EXTERNAL", "TELEBANK", listOf("20190121", "20191101")),
                TransferCalendarHolidays("EXTERNAL", "TEST_BANK", listOf("20200121", "20191201")),
                TransferCalendarHolidays("EXTERNAL_!", "TELEBANK", listOf("20200121", "20191201")),
                TransferCalendarHolidays("external", "TELEBANK", listOf("20201121", "20191211"))
            ),
            setOf(
                LocalDate.of(2019, 1, 21),
                LocalDate.of(2019, 11, 1),
                LocalDate.of(2020, 11, 21),
                LocalDate.of(2019, 12, 11)
            )
        )

        testData(
            listOf(TransferCalendarHolidays("EXTERNAL", "TELEBANK", listOf("121", "211"))),
            setOf(
                LocalDate.of(2019, 1, 21),
                LocalDate.of(2020, 1, 21),
                LocalDate.of(2019, 2, 11),
                LocalDate.of(2020, 2, 11)
            )
        )

        testData(
            listOf(TransferCalendarHolidays("EXTERNAL", "TELEBANK", listOf("1121", "1011"))),
            setOf(
                LocalDate.of(2019, 11, 21),
                LocalDate.of(2020, 11, 21),
                LocalDate.of(2019, 10, 11),
                LocalDate.of(2020, 10, 11)
            )
        )

        testData(
            listOf(TransferCalendarHolidays("EXTERNAL", "TELEBANK", listOf("11", "10"))),
            setOf()
        )
    }
}
