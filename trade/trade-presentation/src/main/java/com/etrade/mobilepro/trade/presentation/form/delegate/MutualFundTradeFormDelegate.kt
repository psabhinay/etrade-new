package com.etrade.mobilepro.trade.presentation.form.delegate

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.map
import com.etrade.mobilepro.dynamic.form.api.ActionListBuilder
import com.etrade.mobilepro.dynamic.form.api.ActionType
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.ExternalInput
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.InputHolder
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.form
import com.etrade.mobilepro.dynamic.form.api.inputParameterValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.livedata.combine
import com.etrade.mobilepro.livedata.combineWith
import com.etrade.mobilepro.quote.presentation.AffiliatedProductAction
import com.etrade.mobilepro.quote.presentation.AffiliatedProductTitleAction
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quote.presentation.TradeQuoteType
import com.etrade.mobilepro.quoteapi.FundsTradeQuoteItem
import com.etrade.mobilepro.quoteapi.FundsTradeSummaryItem
import com.etrade.mobilepro.quoteapi.InvalidMutualFunds
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesResult
import com.etrade.mobilepro.quoteapi.NonSupportedFunds
import com.etrade.mobilepro.quoteapi.QuoteHeaderViewItem
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.api.TradeOptionInfoType
import com.etrade.mobilepro.trade.dropdown.form.tradeSelect
import com.etrade.mobilepro.trade.dropdown.help.TradeHelpParams
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.input.MutualFundTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractAction
import com.etrade.mobilepro.trade.mutualfund.presentation.MutualFundTradeViewModel
import com.etrade.mobilepro.trade.presentation.BR
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction
import com.etrade.mobilepro.trade.presentation.form.component.FooterLink
import com.etrade.mobilepro.trade.presentation.form.component.builder.footer
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.AccountSectionGroup
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.SymbolInputParams
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.accountSectionGroup
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.extendParamsWithListener
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.liveForm
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.securityTypeAndSymbol
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.showAffiliatedProductDialog
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.showDialog
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.showInvalidMutualFundDialog
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.walkthrough.api.State

internal class MutualFundTradeFormDelegate(
    private val context: Context,
    private val securityType: SecurityType,
    private val inputId: MutualFundTradeFormInputId,
    private val searchIntentFactory: SearchIntentFactory
) : TradeFormFactoryDelegate {

    @Suppress("LongMethod")
    override fun createForm(
        accounts: List<TradeAccount>,
        accountDelegate: SelectedAccountDelegate,
        parameter: CreateTradeFormParameter,
        updateForm: TradeFormFactoryDelegate.OnFormChangeListener
    ): Form {
        val previewDependIdsLiveDta: MutableLiveData<Collection<String>> = MutableLiveData(emptyList())
        val previewDependIds = mutableListOf<String>()
        val initDependIds = listOf(
            inputId.accountId,
            inputId.action,
            inputId.securityType,
            inputId.symbol
        )
        val isFundOwned: MutableLiveData<Boolean> = MutableLiveData(false)
        val resources = context.resources
        return form {
            hidden(inputId.entireCashPosition)
            hidden(inputId.entireMarginPosition)

            accountSectionGroup(
                resources,
                inputId,
                AccountSectionGroup(
                    accounts,
                    accountDelegate.getCurrentAccountId(securityType), parameter.balanceViewModel
                )
            )

            verticalGroup {
                securityTypeAndSymbol(resources, securityType, parameter.onActionClickListener, parameter.walkthroughViewModel.state)
                quoteWidget(
                    resources,
                    parameter.quoteWidgetViewModel,
                    accounts.first { it.id == accountDelegate.getCurrentAccountId(securityType) },
                    parameter.onActionClickListener,
                    parameter.mutualFundTradeViewModel,
                    isFundOwned, previewDependIdsLiveDta, previewDependIds
                )
                horizontalGroup {
                    fundOrderType(resources, inputId.extractAction(parameter.defaultValues))
                    investmentAmount(resources, inputId)
                    amountToSell(resources, parameter.quoteWidgetViewModel.primaryFundsQuote)
                }
                investmentType(resources, isFundOwned)
                amountForPosition(resources, inputId, parameter.quoteWidgetViewModel.primaryFundsQuote)
                sharesForPosition(resources, inputId, parameter.quoteWidgetViewModel.primaryFundsQuote)
                fundToBuy(resources, parameter.onActionClickListener)
                fundToBuyWidget(
                    resources,
                    parameter.quoteWidgetViewModel,
                    accounts.first { it.id == accountDelegate.getCurrentAccountId(securityType) },
                    parameter.onActionClickListener
                )
                mutualFundsDisclaimer(resources, parameter.onActionClickListener, parameter.quoteWidgetViewModel.primaryFundsQuote)
                staticText {
                    text = resources.getString(R.string.trade_funds_performance_text)
                    styleId = R.style.TextAppearance_Etrade_Caption_Medium
                }
            }

            actions(parameter.onActionClickListener) {
                previewFundOrder(resources, previewDependIdsLiveDta, parameter.disableIfNoChange, parameter.defaultValues)
            }

            rules {
                liveForm(
                    inputId,
                    securityType,
                    updateForm,
                    parameter.conditionalOrderDisclosureViewModel,
                    accountDelegate
                )
                resetAccount(inputId)
                resetOrderType(inputId, previewDependIdsLiveDta, previewDependIds, initDependIds, isFundOwned)
                resetAmountToSell(inputId, parameter.quoteWidgetViewModel, previewDependIdsLiveDta, previewDependIds)
                onInvalidMutualFundSymbol(parameter.onActionClickListener, parameter.quoteWidgetViewModel, resources)
                onQuantityFieldsInput(inputId.quantity.dollars, previewDependIds, previewDependIdsLiveDta)
                onQuantityFieldsInput(inputId.quantity.shares, previewDependIds, previewDependIdsLiveDta)
            }
        }
    }

    private fun ActionListBuilder.previewFundOrder(
        resources: Resources,
        previewDependIdsLiveData:
            LiveData<Collection<String>>,
        disableIfNoChange: Boolean,
        defaultValues: InputValuesSnapshot
    ) {
        dynamicDependentIdAction(TradeFormAction.SubmitOrder) {
            actionType = ActionType.PRIMARY
            title = resources.getString(R.string.trade_action_preview)
            dependsOnValuesOf(emptyList(), disableIfNoChange, defaultValues)
            updateDependentIds(previewDependIdsLiveData)
        }
    }

    private fun RuleListBuilder.onInvalidMutualFundSymbol(
        actionClickListener: OnActionClickListener,
        viewModel: QuotesWidgetViewModel,
        resources: Resources
    ) {
        rule {
            viewModel.primaryFundsQuote.observeForever {
                val errorMessage = when (val content = it.getNonConsumedContent()) {
                    is InvalidMutualFunds -> resources.getString(R.string.trade_error_invalid_mutual_fund_symbol)
                    is NonSupportedFunds -> content.message
                    else -> return@observeForever
                }
                requireAnyInput(inputId.symbol).resetValue()
                actionClickListener.showInvalidMutualFundDialog(resources, errorMessage)
            }
        }
    }

    @Suppress("LongMethod")
    private fun GroupBuilder.securityTypeAndSymbol(
        resources: Resources,
        securityType: SecurityType,
        actionClickListener: OnActionClickListener,
        walkthroughState: LiveData<State>
    ) {
        val inputParams = extendParamsWithListener(
            resources = resources,
            prototype = SymbolInputParams(InstrumentType.validMutualFundsTradingTypes, inputId.symbol, inputId.symbolCoder, actionClickListener),
            onActionClickListener = actionClickListener,
            notEligibleSymbolDialogMessage = R.string.trade_error_invalid_mutual_fund_symbol
        )

        securityTypeAndSymbol(
            context.resources, inputId, securityType, inputParams,
            {
                val selectedAccountId = requireInput<SelectInput>(inputId.accountId).value.value?.id

                when (requireInput<SelectInput>(inputId.action).value.value?.id) {
                    inputId.actionValueId.sell -> {
                        selectedAccountId?.let {
                            actionClickListener.onActionClick(TradeFormAction.MutualFundSellEntry(it))
                        }
                    }
                    inputId.actionValueId.exchange -> {
                        selectedAccountId?.let {
                            actionClickListener.onActionClick(TradeFormAction.MutualFundExchangeSellEntry(it))
                        }
                    }
                    else -> {
                        actionClickListener.onActionClick(TradeFormAction.SymbolLookup(createSearchIntent(), inputParams.symbolInputId))
                    }
                }
            },
            walkthroughState
        )
    }

    @Suppress("LongParameterList")
    private fun GroupBuilder.quoteWidget(
        resources: Resources,
        viewModel: QuotesWidgetViewModel,
        tradeAccount: TradeAccount,
        actionClickListener: OnActionClickListener,
        mutualFundTradeViewModel: MutualFundTradeViewModel,
        ownedFund: MutableLiveData<Boolean>,
        previewDependIdsLiveData: MutableLiveData<Collection<String>>,
        previewDependIds: MutableList<String>
    ) {
        loadQuoteWidget(resources, viewModel, actionClickListener, isSymbolSelected()) {
            loadQuotes(
                viewModel,
                tradeAccount,
                mutualFundTradeViewModel,
                ownedFund,
                previewDependIdsLiveData,
                previewDependIds
            )
        }
    }

    private fun LiveData<FundsTradeQuoteItem?>.toFundsQuoteTitle(): LiveData<QuoteHeaderViewItem?> = this.map {
        it?.headerItem
    }

    private fun LiveData<FundsTradeQuoteItem?>.toFundsQuoteSummary(): LiveData<FundsTradeSummaryItem?> = this.map {
        it?.summaryItem
    }

    private fun InputHolder.isSymbolSelected() = requireAnyInput(inputId.symbol).hasValue

    private fun InputHolder.selectedOptionHasValue(inputId: String, valueId: String): LiveData<Boolean> {
        return requireInput<SelectInput>(inputId).value.map {
            it?.id == valueId
        }
    }

    private fun InputHolder.selectedOptionNotHaveValue(inputId: String, valueId: String): LiveData<Boolean> {
        return requireInput<SelectInput>(inputId).value.map {
            it?.id != valueId
        }
    }

    private fun InputHolder.isValueSelectedForInput(inputId: String, valueId: String) =
        (requireInput(inputId) as SelectInput).dropDown.selectedOption.value?.id == valueId

    private fun InputHolder.investmentTypeEnable(ownedFund: MutableLiveData<Boolean>): LiveData<Boolean> =
        isSymbolSelected().combineWith(ownedFund) { symbolSelected, fundOwned ->
            if (fundOwned == true) {
                false
            } else {
                symbolSelected
            }
        }

    @Suppress("LongParameterList", "LongMethod")
    private fun InputHolder.loadQuotes(
        viewModel: QuotesWidgetViewModel,
        tradeAccount: TradeAccount,
        mutualFundTradeViewModel: MutualFundTradeViewModel,
        ownedFund: MutableLiveData<Boolean>,
        previewDependIdsLiveData: MutableLiveData<Collection<String>>,
        previewDependIds: MutableList<String>
    ) {
        requireInput<ExternalInput<WithSymbolInfo>>(inputId.symbol).value.observeForever {
            if (it != null) {
                resetFieldsOnSymbolReset(inputId)
                if (isValueSelectedForInput(inputId.action, inputId.actionValueId.buy)) {
                    viewModel.getTradeQuoteByType(
                        symbol = it.symbol,
                        accountId = tradeAccount.id,
                        tradeType = TradeQuoteType.BUY
                    )
                    mutualFundTradeViewModel.mutualFundSellEntry.observeForever { ownedFundList ->
                        val isFundOwned = ownedFundList.firstOrNull { entry ->
                            entry.symbol == it.symbol
                        } != null
                        ownedFund.value = isFundOwned
                        if (isFundOwned) {
                            previewDependIds.remove(inputId.investmentType)
                        } else {
                            previewDependIds.addDistinct(inputId.investmentType)
                        }
                        previewDependIdsLiveData.value = previewDependIds
                    }
                    mutualFundTradeViewModel.getMutualFundSellEntries(tradeAccount.id)
                } else {
                    viewModel.getTradeQuoteByType(
                        symbol = it.symbol,
                        accountId = tradeAccount.id,
                        tradeType = TradeQuoteType.SELL,
                        instrumentType = it.instrumentType
                    )
                }
            }
        }
    }

    private fun GroupBuilder.fundOrderType(resources: Resources, tradeAction: TradeAction?) {
        tradeSelect(inputId.action) {
            title = resources.getString(R.string.trade_hint_order_type)
            defaultOptionId = inputId.actionValueId.converter.toKey(tradeAction ?: TradeAction.BUY)
            options {
                val actionValueId = inputId.actionValueId
                option(actionValueId.buy) { title = resources.getString(R.string.trade_order_type_buy) }
                option(actionValueId.sell) { title = resources.getString(R.string.trade_order_type_sell) }
                option(actionValueId.exchange) { title = resources.getString(R.string.trade_order_type_exchange) }
            }
            helpParams = TradeHelpParams(TradeOptionInfoType.ORDER_TYPE_MF)
        }
    }

    private fun GroupBuilder.investmentType(resources: Resources, ownedFund: MutableLiveData<Boolean>) {
        select(inputId.investmentType) {
            title = resources.getString(R.string.trade_hint_reinvestment_type)
            enabledIf { investmentTypeEnable(ownedFund) }
            visibleIf { selectedOptionHasValue(inputId.action, inputId.actionValueId.buy) }
            options {
                option(inputId.investmentTypeValueId.reInvestInFund) { title = resources.getString(R.string.trade_investment_type_reinvestment) }
                option(inputId.investmentTypeValueId.depositInFund) { title = resources.getString(R.string.trade_investment_type_deposit) }
            }
        }
    }

    private fun GroupBuilder.amountToSell(resources: Resources, funds: LiveData<ConsumableLiveEvent<MutualFundsTradeQuotesResult>>) {
        select(inputId.positionTypeToTrade) {
            title = resources.getString(R.string.trade_hint_amount_to_sell)
            enabledIf { isSymbolSelected() }
            visibleIf { selectedOptionNotHaveValue(inputId.action, inputId.actionValueId.buy) }
            dynamic {
                rule {
                    getSellTypeOptions(resources, funds)
                }
            }
        }
    }

    private fun getSellTypeOptions(
        resources: Resources,
        funds: LiveData<ConsumableLiveEvent<MutualFundsTradeQuotesResult>>
    ): LiveData<List<DropDownOption>> {
        val optionList = ArrayList<DropDownOption>()
        optionList.add(DropDownOption(inputId.amountToSellValueId.entirePosition, resources.getString(R.string.trade_amount_to_sell_entire_position)))
        optionList.add(DropDownOption(inputId.amountToSellValueId.dollars, resources.getString(R.string.trade_amount_to_sell_dollars)))
        optionList.add(DropDownOption(inputId.amountToSellValueId.shares, resources.getString(R.string.trade_amount_to_sell_shares)))

        val optionsLiveData: MutableLiveData<ArrayList<DropDownOption>> = MutableLiveData(optionList)
        return optionsLiveData.combine(funds.distinctUntilChanged()) { list, positions ->
            if (positions?.peekContent().hasMarginPosition()) {
                list?.add(
                    1, // insert cash option followed by entire position
                    DropDownOption(inputId.amountToSellValueId.cashPosition, resources.getString(R.string.trade_amount_to_sell_entire_cash_position))
                )
                list?.add(
                    2, // insert margin option followed by cash position
                    DropDownOption(inputId.amountToSellValueId.marginPosition, resources.getString(R.string.trade_amount_to_sell_entire_margin_position))
                )
            }
            list?.distinct().orEmpty()
        }
    }

    private fun GroupBuilder.fundToBuy(resources: Resources, actionClickListener: OnActionClickListener) {
        externalSelect(inputId.fundToBuy, inputId.symbolCoder) {
            title = resources.getString(R.string.trade_hint_fund_to_buy)
            action = {
                val selectedAccountId = requireInput<SelectInput>(inputId.accountId).value.value?.id
                val selectedSymbol = requireInput<ExternalInput<WithSymbolInfo>>(inputId.symbol).value.value
                selectedAccountId?.also { accountId ->
                    selectedSymbol?.also { selected ->
                        actionClickListener.onActionClick(TradeFormAction.MutualFundExchangeBuyEntry(accountId, selected))
                    }
                }
            }
            valueFormatter = { it.symbol }
            visibleIf { selectedOptionHasValue(inputId.action, inputId.actionValueId.exchange) }
            enabledIf { isSymbolSelected() }
        }
    }

    private fun GroupBuilder.fundToBuyWidget(
        resources: Resources,
        viewModel: QuotesWidgetViewModel,
        tradeAccount: TradeAccount,
        actionClickListener: OnActionClickListener
    ) {
        loadQuoteWidget(
            resources = resources,
            viewModel = viewModel,
            actionClickListener = actionClickListener,
            isVisible = isFundToBuySelected(),
            isExchangeBuy = true
        ) {
            loadExchangeBuyQuote(viewModel, tradeAccount)
        }
    }

    @Suppress("LongParameterList")
    private fun GroupBuilder.loadQuoteWidget(
        resources: Resources,
        viewModel: QuotesWidgetViewModel,
        actionClickListener: OnActionClickListener,
        isVisible: LiveData<Boolean>,
        isExchangeBuy: Boolean = false,
        loadQuoteWidget: InputHolder.() -> Unit
    ) {
        layout(R.layout.trade_view_mutual_fund_widget) {
            bindings {
                binding(BR.viewModel) {
                    variable = viewModel
                }
                binding(BR.quoteHeaderViewItem) {
                    variable = viewModel.getQuote(isExchangeBuy).toFundsQuoteTitle()
                }
                binding(BR.fundsQuoteSummaryItem) {
                    variable = viewModel.getQuote(isExchangeBuy).toFundsQuoteSummary()
                }
                binding(BR.layoutAction) {
                    variable = onLayoutAction(isExchangeBuy, actionClickListener)
                }
                binding(BR.affiliatedProductAction) {
                    variable = AffiliatedProductAction {
                        actionClickListener.showAffiliatedProductDialog(resources)
                    }
                }
                binding(BR.affiliatedProductTitleAction) {
                    variable = AffiliatedProductTitleAction {
                        onLayoutAction(isExchangeBuy, actionClickListener).onAction(viewModel)
                    }
                }
            }

            visibleIf { isVisible }

            rule { loadQuoteWidget() }
        }
    }

    private fun QuotesWidgetViewModel.getQuote(isExchangeBuy: Boolean): LiveData<FundsTradeQuoteItem?> = if (isExchangeBuy) {
        secondaryFundsQuote.map {
            it.peekContent().toFundsQuote()
        }
    } else {
        primaryFundsQuote.map {
            it.peekContent().toFundsQuote()
        }
    }

    private fun onLayoutAction(
        isExchangeBuy: Boolean,
        actionClickListener: OnActionClickListener
    ): LayoutAction {
        return LayoutAction { quoteViewModel ->

            val quote = if (isExchangeBuy) {
                quoteViewModel.secondaryFundsQuote
            } else {
                quoteViewModel.primaryFundsQuote
            }.value?.peekContent()

            if (!quoteViewModel.isInErrorState && quote is FundsTradeQuoteItem) {
                val symbol = SearchResultItem.Symbol(title = quote.symbol, type = quote.instrumentType)
                actionClickListener.onActionClick(TradeFormAction.ShowFundsPerformance(symbol))
            }
        }
    }

    private fun InputHolder.loadExchangeBuyQuote(
        viewModel: QuotesWidgetViewModel,
        tradeAccount: TradeAccount
    ) {
        @Suppress("UNCHECKED_CAST")
        (requireInput(inputId.fundToBuy) as ExternalInput<WithSymbolInfo>).value.observeForever {
            if (it != null) {
                viewModel.getTradeQuoteByType(
                    symbol = it.symbol,
                    accountId = tradeAccount.id,
                    tradeType = TradeQuoteType.EXCHANGE_BUY
                )
            }
        }
    }

    private fun InputHolder.isFundToBuySelected() = requireAnyInput(inputId.fundToBuy).hasValue

    private fun GroupBuilder.mutualFundsDisclaimer(
        resources: Resources,
        onActionClickListener: OnActionClickListener,
        fundsQuote: LiveData<ConsumableLiveEvent<MutualFundsTradeQuotesResult>>
    ) {
        footer {
            links {
                link {
                    FooterLink(
                        title = resources.getString(R.string.trade_link_mutual_fund_prospectus),
                        action = onFundsProspectusClicked(onActionClickListener, resources)
                    )
                }
                link {
                    FooterLink(
                        title = resources.getString(R.string.trade_link_mutual_fund_legal_disclosures),
                        action = {
                            onActionClickListener.onActionClick(TradeFormAction.MutualFundDisclosure(fundsQuote.value?.peekContent()))
                        }
                    )
                }
            }
        }
    }

    private fun InputHolder.onFundsProspectusClicked(
        onActionClickListener: OnActionClickListener,
        resources: Resources
    ): () -> Unit {
        return {
            val ticker = getTicker()
            if (ticker == null) {
                onActionClickListener.showDialog {
                    title = resources.getString(R.string.title_missing_symbol)
                    message = resources.getString(R.string.trade_warning_no_mutual_fund_symbol)
                    positiveAction {
                        label = resources.getString(R.string.ok)
                    }
                }
            } else {
                onActionClickListener.onActionClick(TradeFormAction.MutualFundProspectus(ticker))
            }
        }
    }

    private fun InputHolder.getTicker(): String? = inputParameterValue(inputId.symbol).value?.let {
        inputId.symbolCoder.decode(it)?.symbol
    }

    private fun RuleListBuilder.resetAccount(inputId: MutualFundTradeFormInputId) {
        rule {
            inputParameterValue(inputId.accountId).observeForever {
                resetFieldsOnAccount(inputId)
            }
        }
    }

    private fun RuleListBuilder.resetOrderType(
        inputId: MutualFundTradeFormInputId,
        previewDependIdsLiveData: MutableLiveData<Collection<String>>,
        previewDependIds: MutableList<String>,
        initDependIds: List<String>,
        fundOwned: MutableLiveData<Boolean>
    ) {
        rule {
            inputParameterValue(inputId.action).observeForever {
                resetFieldsOnOrderType(inputId)
                previewDependIds.clear()
                previewDependIds.addAll(initDependIds)
                fundOwned.value = false
                when (it) {
                    inputId.actionValueId.buy -> {
                        previewDependIds.addAll(listOf(inputId.amount, inputId.investmentType))
                        requireAnyInput(inputId.entireCashPosition).resetValue()
                        requireAnyInput(inputId.entireMarginPosition).resetValue()
                    }
                    inputId.actionValueId.sell -> previewDependIds.add(inputId.positionTypeToTrade)
                    inputId.actionValueId.exchange -> previewDependIds.addAll(listOf(inputId.positionTypeToTrade, inputId.fundToBuy))
                }
                previewDependIdsLiveData.value = previewDependIds
            }
        }
    }

    private fun InputHolder.resetFieldsOnAccount(inputId: MutualFundTradeFormInputId) {
        requireAnyInput(inputId.action).resetValue()
        resetFieldsOnOrderType(inputId)
    }

    private fun InputHolder.resetFieldsOnOrderType(inputId: MutualFundTradeFormInputId) {
        requireAnyInput(inputId.symbol).resetValue()
        resetFieldsOnSymbolReset(inputId)
    }

    private fun InputHolder.resetFieldsOnSymbolReset(inputId: MutualFundTradeFormInputId) {
        requireAnyInput(inputId.positionTypeToTrade).resetValue()
        resetQuantityFields(inputId.quantity)
        requireAnyInput(inputId.investmentType).resetValue()
        requireAnyInput(inputId.amount).resetValue()
        requireAnyInput(inputId.fundToBuy).resetValue()
    }

    private fun createSearchIntent(): Intent {
        return searchIntentFactory.createIntent(
            context = context,
            params = StartSearchParams(
                query = SearchQuery(SearchType.SYMBOL),
                stockQuoteSearchOnly = true,
                finishOnSearch = true
            ),
            reorderToFront = false
        )
    }
}

internal data class LayoutAction(val onAction: (QuotesWidgetViewModel) -> Unit)
