package com.etrade.mobilepro.common

import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import java.lang.ref.WeakReference
import java.util.Collections
import javax.inject.Inject

private typealias OnRestoreTabPosition = (previousTab: Int) -> Unit

interface RestoreTabPositionDelegate {

    fun init(
        targetFragment: Fragment,
        viewPager: ViewPager,
        actionOnRestore: OnRestoreTabPosition? = { viewPager.currentItem = it },
        rootDestinations: Set<Int> = Collections.singleton(R.id.menuFragment)
    )

    fun init(
        targetFragment: Fragment,
        viewPager: ViewPager2,
        actionOnRestore: OnRestoreTabPosition? = { viewPager.setCurrentItem(it, false) },
        rootDestinations: Set<Int> = Collections.singleton(R.id.menuFragment)
    )
}

class RestoreTabPositionDelegateImpl @Inject constructor(
    private val restorableState: RestorableState
) : RestoreTabPositionDelegate, LifecycleObserver {

    private val tabsPositionMap
        get() = restorableState.tabPositionMap

    /**
     * Classes that are listening for navigation changes (this will only contain 1 class at a time)
     */
    private val attachedClasses: MutableMap<Class<*>, Set<Int>> = mutableMapOf()
    private var fragmentRef: WeakReference<Fragment>? = null
    private var clazz: Class<*>? = null

    private var rootDestinations: Set<Int>? = null

    private val navControllerListener by lazy {
        NavController.OnDestinationChangedListener { _, destination, _ ->
            invalidateTabPosition(destination)
        }
    }

    private val viewPagerListener by lazy {
        object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                pageSelected(position)
            }
        }
    }

    private val viewPager2Callback by lazy {
        object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                pageSelected(position)
            }
        }
    }

    /**
     * Initializes the RestoreTabPositionDelegate (NOT a singleton) for a fragment, given its ViewPager and callback
     */
    override fun init(
        targetFragment: Fragment,
        viewPager: ViewPager,
        actionOnRestore: OnRestoreTabPosition?,
        rootDestinations: Set<Int>
    ) {
        viewPager.removeOnPageChangeListener(viewPagerListener) // Prevent duplicate listeners
        viewPager.addOnPageChangeListener(viewPagerListener)
        init(
            targetFragment,
            actionOnRestore,
            rootDestinations
        ) { position -> viewPager.currentItem = position }
    }

    override fun init(
        targetFragment: Fragment,
        viewPager: ViewPager2,
        actionOnRestore: OnRestoreTabPosition?,
        rootDestinations: Set<Int>
    ) {
        viewPager.unregisterOnPageChangeCallback(viewPager2Callback) // Prevent duplicate listeners
        viewPager.registerOnPageChangeCallback(viewPager2Callback)
        init(
            targetFragment,
            actionOnRestore,
            rootDestinations
        ) { position -> viewPager.setCurrentItem(position, false) }
    }

    private fun init(
        targetFragment: Fragment,
        actionOnRestore: OnRestoreTabPosition?,
        rootDestinations: Set<Int>,
        setCurrentItem: (Int) -> Unit
    ) {
        this.clazz = targetFragment::class.java
        this.fragmentRef = WeakReference(targetFragment)
        this.rootDestinations = rootDestinations

        targetFragment.viewLifecycleOwner.lifecycle.removeObserver(this) // Prevent duplicate listeners
        targetFragment.viewLifecycleOwner.lifecycle.addObserver(this)

        // Should have no visible effect if this somehow gets called twice
        if (clazz != null) {
            tabsPositionMap[clazz!!]?.let { previousPosition ->
                if (actionOnRestore == null) {
                    setCurrentItem(previousPosition)
                } else {
                    actionOnRestore.invoke(previousPosition)
                }
            }
        }

        val navController = targetFragment.findNavController()
        navController.removeOnDestinationChangedListener(navControllerListener) // Prevent duplicate listeners
        navController.addOnDestinationChangedListener(navControllerListener)
    }

    /**
     * Listeners will attach and detach automatically using Lifecycle Observer, so no need to call these
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onResume() {
        // Map prevents duplication
        if (clazz != null && rootDestinations != null) {
            attachedClasses[clazz!!] = rootDestinations!!
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onPause() {
        // Safe if not present
        clazz?.let {
            attachedClasses.remove(it)
        }
    }

    /**
     * If user navigates to a root destination (AKA leaves page via back/up), forget the saved tab position
     */
    private fun invalidateTabPosition(destination: NavDestination) {
        for (clazz in attachedClasses) {
            rootDestinations?.let {
                if (it.contains(destination.id)) {
                    tabsPositionMap[clazz.key] = null
                }
            }
        }
    }

    private fun pageSelected(page: Int) {
        for (clazz in attachedClasses) {
            tabsPositionMap[clazz.key] = page
        }
    }
}
