package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.arr.api.AnalystResearchReportRepo
import com.etrade.mobilepro.arr.data.AnalystResearchReportService
import com.etrade.mobilepro.arr.data.DefaultAnalystResearchReportRepo
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.di.Mod
import com.etrade.mobilepro.mod.DefaultMarketOnDemand
import com.etrade.mobilepro.mod.MarketOnDemandUrlBuilder
import com.etrade.mobilepro.mod.ModServiceClient
import com.etrade.mobilepro.mod.api.MarketOnDemand
import com.etrade.mobilepro.mod.auth.DefaultAuthTokenProvider
import com.etrade.mobilepro.mod.auth.ModAuthService
import com.etrade.mobilepro.mod.auth.api.AuthTokenProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
abstract class MarketOnDemandModule {

    @Binds
    abstract fun bindMarketOnDemand(defaultMarketOnDemand: DefaultMarketOnDemand): MarketOnDemand

    @Binds
    abstract fun bindAuthTokenProvider(defaultAuthTokenProvider: DefaultAuthTokenProvider): AuthTokenProvider

    @Binds
    abstract fun bindAnalystResearchReportRepo(defaultAnalystResearchReportsRepo: DefaultAnalystResearchReportRepo): AnalystResearchReportRepo

    companion object {

        @Provides
        @Singleton
        internal fun providesMarketOnDemandService(@Mod retrofit: Retrofit): ModServiceClient = retrofit.create(ModServiceClient::class.java)

        @Provides
        @Singleton
        internal fun provideModAuthService(@MobileTrade retrofit: Retrofit): ModAuthService = retrofit.create(ModAuthService::class.java)

        @Provides
        @Singleton
        internal fun provideMarketOnDemandUrlBuilder(@Mod modBaseUrl: String) = MarketOnDemandUrlBuilder(modBaseUrl)

        @Provides
        @Singleton
        internal fun provideAnalystResearchReportService(@MobileTrade retrofit: Retrofit): AnalystResearchReportService =
            retrofit.create(AnalystResearchReportService::class.java)
    }
}
