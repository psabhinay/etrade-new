package com.etrade.mobilepro.tableviewutils.presentation.util

data class ViewMeasuredSize(
    val width: Int,
    val height: Int
)
