package com.etrade.mobilepro.menu.fragment.recyclerview.holder

import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.dynamicui.DynamicUiLayoutRecyclerAdapter
import com.etrade.mobilepro.dynamicui.api.DynamicLayout
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.menu.MessagesMenuItem
import com.etrade.mobilepro.menupage.presentation.databinding.ViewMenuInboxMessagesBinding

class MessagesMenuItemViewHolder(
    private val lifecycleOwner: LifecycleOwner,
    private val toDynamicViewModels: (List<GenericLayout>) -> List<DynamicLayout>,
    private val binding: ViewMenuInboxMessagesBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: MessagesMenuItem) {
        binding.messagesRv.layoutManager = LinearLayoutManager(itemView.context)

        val adapter = DynamicUiLayoutRecyclerAdapter(lifecycleOwner, emptyList())

        adapter.values = toDynamicViewModels(listOf(item.messages))
        binding.messagesRv.adapter = adapter
    }
}
