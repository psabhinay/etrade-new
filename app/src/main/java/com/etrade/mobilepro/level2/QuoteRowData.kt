package com.etrade.mobilepro.level2

import com.etrade.mobilepro.streaming.api.Level2DataEvent

data class QuoteRowData(val bidItem: Level2DataEvent?, val askItem: Level2DataEvent?)
