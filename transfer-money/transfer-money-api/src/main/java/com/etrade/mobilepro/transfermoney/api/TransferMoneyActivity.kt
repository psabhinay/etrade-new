package com.etrade.mobilepro.transfermoney.api

interface TransferMoneyActivity {
    val status: String
    val date: String
    val fromAccount: String
    val toAccount: String
    val amount: String
    val frequency: String
    val confirmationNumber: String
    val referenceNumber: String
    val transferType: TransferType?
    val id: String
    val appName: String
    val memo: String
}
