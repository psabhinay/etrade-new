package com.etrade.mobilepro.login.data

import com.etrade.mobilepro.login.api.repo.LoginResultCode

/**
 * Login Error Codes list
 */

const val ET_NETWORK_ERROR = 1001
const val CYOTA_CHALLENGE = 10000
const val ET_INVALID_CREDENTIALS = 9603
const val FORCE_UPGRADE = 999
const val LOGIN_WARNING = 10002
const val ACCESS_DENIED = 9503
const val USER_NOT_LOGGED_IN = 9504
const val SYSTEM_UNDER_MAINTENANCE = 9598
const val WRONG_PASSWORD = 9604
const val FIVE_CONSECUTIVE_TIMES_WRONG_PASSWORD = 9608
const val PASSWORD_EXPIRED = 9900
const val CONFLICTED_USER = 920
const val CONFLICTED_USER_AND_PASSWORD = 921

const val CHANGE_USER_WRONG_PASSWORD = 48

const val CHANGE_USER_CONFLICTED_NEW_USER = -1

@Suppress("MagicNumber")
private val CAP1_CONFLICTED_USER = setOf(923, 924, 925, 926, 927, 928, 929, 931, 932, 933, 934, 935, 936, 937, 938, 939)

private const val CAP1_PASSWORD_CHANGE = 203

private const val CAP1_PREVIEW_USER = 930

/**
 * maps  server error code to  client ErrorCode Enum
 */
@Suppress("ComplexMethod")
internal fun mapLoginErrorCode(errorCode: Int?): LoginResultCode {
    return when (errorCode) {
        ET_NETWORK_ERROR -> LoginResultCode.NETWORK_ERROR

        CYOTA_CHALLENGE -> LoginResultCode.CYOTA_CHALLENGE

        FORCE_UPGRADE -> LoginResultCode.FORCE_UPGRADE

        ET_INVALID_CREDENTIALS, USER_NOT_LOGGED_IN -> LoginResultCode.INVALID_CREDENTIALS

        ACCESS_DENIED, SYSTEM_UNDER_MAINTENANCE -> LoginResultCode.ACCESS_DENIED

        FIVE_CONSECUTIVE_TIMES_WRONG_PASSWORD -> LoginResultCode.LOCKED_OUT

        PASSWORD_EXPIRED -> LoginResultCode.PASSWORD_EXPIRED

        CONFLICTED_USER, CONFLICTED_USER_AND_PASSWORD -> LoginResultCode.CONFLICTED_USER

        CAP1_PASSWORD_CHANGE -> LoginResultCode.CAP1_PASSWORD_CHANGE

        CAP1_PREVIEW_USER -> LoginResultCode.CAP1_PREVIEW_USER

        WRONG_PASSWORD -> LoginResultCode.WRONG_PASSWORD

        else -> {
            if (CAP1_CONFLICTED_USER.contains(errorCode)) {
                LoginResultCode.CAP1_CONFLICTED_USER
            } else {
                LoginResultCode.UNKNOWN
            }
        }
    }
}
