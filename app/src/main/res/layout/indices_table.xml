<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <variable
            name="obj"
            type="com.etrade.mobilepro.indices.BaseIndices" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@color/white"
        android:paddingBottom="@dimen/spacing_small"
        android:layout_marginBottom="@dimen/spacing_xsmall"
        android:theme="@style/NoFontPaddingTheme">

        <include
            android:id="@+id/indices_widget"
            layout="@layout/indices_table_widget"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:onDynamicUiViewStateSuccess="@{obj.viewState}"
            app:indicesItemClickListener="@{obj}"
            app:indices="@{obj.indices}" />

        <TextView
            android:id="@+id/futuresLabel"
            style="@style/Title.Divider"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/spacing_small"
            android:gravity="center"
            android:paddingTop="@dimen/spacing_small"
            android:paddingBottom="@dimen/spacing_small"
            android:text="@string/futures"
            app:layout_constraintTop_toBottomOf="@id/indices_widget"
            tools:ignore="NotSibling" />

        <LinearLayout
            android:id="@+id/futuresTickers"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:baselineAligned="false"
            android:orientation="horizontal"
            app:layout_constraintTop_toBottomOf="@id/futuresLabel">

            <include
                android:id="@+id/futures_left_index"
                layout="@layout/indices_table_widget_futures_tile"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_weight="1"
                app:index="@{obj.futureIndices.size() &gt; 0 ? obj.futureIndices.get(0) : null}" />

            <include
                android:id="@+id/futures_center_index"
                layout="@layout/indices_table_widget_futures_tile"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_weight="1"
                app:index="@{obj.futureIndices.size() &gt; 1 ? obj.futureIndices.get(1) : null}" />

            <include
                android:id="@+id/futures_right_index"
                layout="@layout/indices_table_widget_futures_tile"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_weight="1"
                app:index="@{obj.futureIndices.size() &gt; 2 ? obj.futureIndices.get(2) : null}" />

        </LinearLayout>

        <TextView
            android:id="@+id/delayedFuturesTimeStamp"
            style="@style/TextAppearance.Etrade.Caption.LightBlack"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:gravity="center"
            android:paddingTop="@dimen/spacing_medium"
            android:text="@{String.format(@string/delayed_future_quotes, obj.futures.delayedFuturesTimeStamp)}"
            app:goneUnless="@{safeUnbox(obj.futures.delayedFuturesTimeStamp != null)}"
            app:layout_constraintTop_toBottomOf="@id/futuresTickers"
            tools:text="Delayed Futures Quotes 11:00 PM EDT 3/01/2018" />

        <TextView
            android:id="@+id/futuresExpiry"
            style="@style/TextAppearance.Etrade.Caption.LightBlack"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:gravity="center"
            android:paddingBottom="@dimen/spacing_medium"
            android:text="@{String.format(@string/futures_expiry, obj.futures.futuresExpiry)}"
            app:goneUnless="@{safeUnbox(obj.futures.futuresExpiry != null)}"
            app:layout_constraintTop_toBottomOf="@id/delayedFuturesTimeStamp"
            tools:text="Futures based on Sep 21, 2018 Contract" />

        <androidx.constraintlayout.widget.Group
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:constraint_referenced_ids="futuresTickers,futuresLabel"
            app:goneUnless="@{safeUnbox(obj.futureIndices != null)}" />

        <androidx.core.widget.ContentLoadingProgressBar
            android:id="@+id/loadingIndicator"
            style="?android:attr/progressBarStyle"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:padding="@dimen/spacing_medium"
            app:onDynamicUiViewStateChange="@{obj.viewState}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <TextView
            android:id="@+id/errorTxtHeader"
            style="@style/TextAppearance.Etrade.Text.Regular.Medium"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/spacing_medium"
            android:gravity="center"
            android:letterSpacing="0"
            android:text="@string/error_message_general2"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintBottom_toTopOf="@id/retryButton"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            />

        <com.google.android.material.button.MaterialButton
            android:id="@+id/retryButton"
            android:layout_width="wrap_content"
            android:layout_height="@dimen/button_height"
            android:layout_margin="@dimen/spacing_medium"
            android:onClick="@{()->obj.getMarketIndices()}"
            android:text="@string/retry"
            app:layout_constraintTop_toBottomOf="@+id/errorTxtHeader"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent" />

        <androidx.constraintlayout.widget.Group
            tools:visibility="gone"
            android:id="@+id/group_futures_failure_view"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:constraint_referenced_ids="errorTxtHeader,retryButton"
            app:onDynamicUiViewStateError="@{obj.viewState}" />

    </androidx.constraintlayout.widget.ConstraintLayout>

</layout>
