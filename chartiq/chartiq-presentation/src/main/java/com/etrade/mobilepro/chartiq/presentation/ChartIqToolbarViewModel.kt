package com.etrade.mobilepro.chartiq.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

data class ToolbarUpdateAction(
    val isLastPage: Boolean,
    val title: CharSequence,
    val action: () -> Unit
)

class ChartIqToolbarViewModel @Inject constructor() : ViewModel() {

    val toolbarBackAction: LiveData<ToolbarUpdateAction>
        get() = _toolbarBackAction

    private val _toolbarBackAction: MutableLiveData<ToolbarUpdateAction> = MutableLiveData()

    fun setToolbarUpdateAction(action: ToolbarUpdateAction) {
        _toolbarBackAction.value = action
    }
}
