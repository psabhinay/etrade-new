package com.etrade.mobilepro.dynamic.form.api

import androidx.annotation.LayoutRes
import androidx.lifecycle.LiveData

class Layout(
    @LayoutRes val layoutResId: Int,
    val bindings: Iterable<Binding>,
    override val isVisible: LiveData<Boolean>
) : Component {

    class Binding(
        val variableId: Int,
        val variable: Any?
    )
}
