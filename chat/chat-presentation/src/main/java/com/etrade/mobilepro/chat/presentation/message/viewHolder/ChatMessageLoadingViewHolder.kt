package com.etrade.mobilepro.chat.presentation.message.viewHolder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.chat.presentation.databinding.ChatItemMessageLoadingBinding

class ChatMessageLoadingViewHolder(binding: ChatItemMessageLoadingBinding) :
    RecyclerView.ViewHolder(binding.root)
