package com.etrade.mobilepro.home.model

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.FragmentManager
import javax.inject.Inject

const val MARKET_DATA_AGREEMENT_KEY = "MarketDataAgreement"
const val PUSH_NOTIFICATION_PROMPT_IN_KEY = "PushNotificationPromptIn"

interface PromptInDialogsHelper {
    fun hasPendingDialogs(): Boolean
    fun addPendingDialog(key: String, dialog: PendingPromptInDialog): PendingPromptInDialog?
    fun showPendingDialog(key: String, fragmentManager: FragmentManager)
}

class DefaultPromptInDialogsHelper @Inject constructor(
    private val fragmentInjectFactory: FragmentFactory
) : PromptInDialogsHelper {

    private val _pendingDialogs = mutableMapOf<String, PendingPromptInDialog>()

    override fun addPendingDialog(key: String, dialog: PendingPromptInDialog) = _pendingDialogs.put(key, dialog)

    override fun hasPendingDialogs(): Boolean = _pendingDialogs.isNotEmpty()

    private fun getPendingDialog(key: String): PendingPromptInDialog? = _pendingDialogs[key]

    override fun showPendingDialog(key: String, fragmentManager: FragmentManager) {
        val pendingDialog = getPendingDialog(key)
        pendingDialog?.let {
            if (it.showDialogCondition()) {
                it.buildDialog(fragmentInjectFactory)?.show(fragmentManager, key)
            }
            it.onDialogShown?.invoke()
        }
    }
}

data class PendingPromptInDialog(
    val showDialogCondition: () -> Boolean,
    val dialogClass: Class<out DialogFragment>,
    val dialogArgs: Bundle? = null,
    val onDialogShown: (() -> Unit)? = null
) {
    fun buildDialog(fragmentFactory: FragmentFactory): DialogFragment? =
        fragmentFactory
            .instantiate(dialogClass.classLoader!!, dialogClass.name)
            .apply { arguments = dialogArgs } as? DialogFragment
}
