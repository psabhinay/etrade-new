package com.etrade.mobilepro.walkthrough.presentation

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.livedata.doOnNext
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.NoviceAnalyticsParams
import com.etrade.mobilepro.tracking.params.QuoteCompletedNoviceAnalyticParams
import com.etrade.mobilepro.tracking.params.QuoteStartedNoviceAnalyticParams
import com.etrade.mobilepro.tracking.params.TradeCompletedNoviceAnalyticParams
import com.etrade.mobilepro.tracking.params.TradeStartedNoviceAnalyticParams
import com.etrade.mobilepro.tracking.params.WatchlistCompletedNoviceAnalyticParams
import com.etrade.mobilepro.tracking.params.WatchlistStartedNoviceAnalyticParams
import com.etrade.mobilepro.walkthrough.api.IdleState
import com.etrade.mobilepro.walkthrough.api.State
import com.etrade.mobilepro.walkthrough.api.SymbolLookupState
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingState
import com.etrade.mobilepro.walkthrough.api.TradePlacingState
import org.slf4j.LoggerFactory
import javax.inject.Inject

class TrackingWalkthroughStatesViewModel @Inject constructor(
    private val walkthroughStatesViewModel: WalkthroughStatesViewModel,
    private val tracker: Tracker
) : WalkthroughStatesViewModel by walkthroughStatesViewModel {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    private var lastState: State? = null

    override var state: LiveData<State> = walkthroughStatesViewModel.state
        .doOnNext { state ->
            if (lastState is IdleState && state !is IdleState) {
                state.getWalkthroughStartedParams()?.let { tracker.event(it) }
            } else if (lastState?.isLastWalkthroughStep() == true && state is IdleState) {
                state.getWalkthroughCompletedParams()?.let { tracker.event(it) }
            }

            lastState = state
        }

    private fun State.getWalkthroughStartedParams(): NoviceAnalyticsParams? {
        return when (this) {
            is SymbolLookupState.DisplayLookupScreen -> QuoteStartedNoviceAnalyticParams()
            is SymbolTrackingState.DisplayInfoMessage -> WatchlistStartedNoviceAnalyticParams()
            is TradePlacingState.DisplayTradePage -> TradeStartedNoviceAnalyticParams()
            else -> {
                logger.error("No such params exist for", this)
                return null
            }
        }
    }

    private fun State.getWalkthroughCompletedParams(): NoviceAnalyticsParams? {
        return when (this) {
            is SymbolLookupState.Idle -> QuoteCompletedNoviceAnalyticParams()
            is SymbolTrackingState.Idle -> WatchlistCompletedNoviceAnalyticParams()
            is TradePlacingState.Idle -> TradeCompletedNoviceAnalyticParams()
            else -> {
                logger.error("No such params exist for", this)
                return null
            }
        }
    }

    private fun State?.isLastWalkthroughStep(): Boolean {
        return when (this) {
            is SymbolLookupState.DisplaySearchTooltip -> true
            is SymbolTrackingState.HighlightWatchlistActionInWatchlistPage -> true
            is TradePlacingState.HighlightTrade -> true
            else -> false
        }
    }
}
