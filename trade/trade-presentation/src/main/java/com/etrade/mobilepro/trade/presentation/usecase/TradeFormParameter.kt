package com.etrade.mobilepro.trade.presentation.usecase

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.viewmodel.CreateFormUseCaseParameter
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.trade.presentation.TradeConditionalOrderDisclosureViewModel
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel

interface TradeFormParameter : CreateFormUseCaseParameter {
    val conditionalOrderDisclosureViewModel: TradeConditionalOrderDisclosureViewModel
    val quoteWidgetViewModel: QuotesWidgetViewModel
    val walkthroughViewModel: WalkthroughStatesViewModel
    val defaultValues: InputValuesSnapshot
    val tradeOrderType: TradeOrderType
}
