package com.etrade.mobilepro.trade.presentation.form.delegate.shared

import android.content.Context
import android.content.res.Resources
import com.etrade.mobilepro.dynamic.form.api.ActionListBuilder
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.input.CONDITIONAL_ORDER_SECURITY_TYPE_ID
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.toSecurityType
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.component.builder.footer
import com.etrade.mobilepro.trade.presentation.form.delegate.conditional.ConditionalFormFactoryDelegate
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.usecase.TradeFormParameter

@Suppress("LongParameterList")
internal fun <T> GroupBuilder.conditionalOrderSymbol(
    context: Context,
    searchIntentFactory: SearchIntentFactory,
    inputId: T,
    parameter: TradeFormParameter,
    symbolInputParams: SymbolInputParams,
    initialSymbol: WithSymbolInfo? = null
) where T : WithActionTradeFormInputId {
    val resources = context.resources
    val onActionClickListener = parameter.onActionClickListener
    val params = extendParamsWithListener(
        resources = resources,
        prototype = symbolInputParams,
        onActionClickListener = onActionClickListener,
        notEligibleSymbolDialogMessage = R.string.trade_error_invalid_conditional_order_symbol
    )
    val externalAction = createStockSymbolExternalAction(context, params, inputId, searchIntentFactory, onActionClickListener)
    symbol(resources, params, externalAction, initialSymbol)
}

internal fun RuleListBuilder.observeSymbolChange(
    symbolInputId: String,
    listener: ConditionalFormFactoryDelegate.SymbolChangedListener,
    currentSecurityType: SecurityType,
    initialSymbolInfo: WithSymbolInfo? = null
) {
    rule {
        var previousWithSymbolInfo: WithSymbolInfo? = initialSymbolInfo
        inputValue<WithSymbolInfo>(symbolInputId).observeForever { symbolInfo ->
            if (symbolInfo != null && symbolInfo != previousWithSymbolInfo) {
                if (currentSecurityType != symbolInfo.instrumentType.toSecurityType()) {
                    listener.onSymbolChange(symbolInfo)
                }
                previousWithSymbolInfo = symbolInfo
            }
        }
    }
}

internal fun GroupBuilder.conditionalOrderFooter(resources: Resources, onActionClickListener: OnActionClickListener) {
    footer {
        links {
            exchangeKeyLink(resources, onActionClickListener)
        }
    }
}

internal fun InputValuesSnapshot.conditionalOrderSecurityType(): SecurityType? {
    return (this[CONDITIONAL_ORDER_SECURITY_TYPE_ID] as? String)?.let { SecurityType.valueOf(it) }
}

internal fun ActionListBuilder.conditionalOrderActions(
    context: Context,
    dependencies: FormFactoryDelegateDependencies,
    orderNumber: Int
) {
    val actionTitle = context.getString(R.string.trade_action_finish_order, orderNumber)
    submitOrder(actionTitle, dependencies.saveAndPreviewActionRequiredInputs)
}
