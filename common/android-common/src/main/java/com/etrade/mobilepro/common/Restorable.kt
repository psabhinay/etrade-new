package com.etrade.mobilepro.common

import androidx.navigation.NavController
import java.util.Collections

typealias RestorableMap = MutableMap<Class<*>, Restorable?>
typealias TabPositionMap = MutableMap<Class<*>, Int?>

interface Restorable

data class RestorableState(
    val stateMap: RestorableMap = mutableMapOf(),
    val tabPositionMap: TabPositionMap = mutableMapOf(),
    val attachedClasses: MutableSet<Class<*>> = mutableSetOf(),
    var selectedBottomNavButton: Int = 0
)

fun disposeOnDestinationChanged(
    rootDestinations: Set<Int> = Collections.singleton(R.id.menuFragment),
    doDispose: () -> Unit
): NavController.OnDestinationChangedListener = NavController.OnDestinationChangedListener { _, destination, _ ->
    if (destination.id in rootDestinations) {
        doDispose()
    }
}
