package com.etrade.mobilepro.alerts.presentation.setalert.setvalue

import android.content.res.Resources
import com.etrade.mobilepro.alerts.api.model.AlertValueType
import com.etrade.mobilepro.alerts.presentation.R
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.dailypercent.DailyPercentCustomizer
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.dailypercent.DailyPercentFormatter
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.dailypercent.DailyPercentValidator
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.dailyvolume.DailyVolumeFormatter
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.dailyvolume.DailyVolumeValidator
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.petarget.PePriceFormatter
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.petarget.PePriceValidator
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.targetprice.TargetPriceFormatter
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.targetprice.TargetPriceValidator
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import javax.inject.Inject

interface AlertInputValueDependenciesProvider {
    fun getDependencies(valueType: AlertValueType): InputFieldManager.Dependencies
}

class DefaultAlertInputValueDependenciesProvider @Inject constructor(private val resources: Resources) :
    AlertInputValueDependenciesProvider {
    private val deps = mutableMapOf<AlertValueType, InputFieldManager.Dependencies>()

    @Suppress("LongMethod")
    override fun getDependencies(valueType: AlertValueType): InputFieldManager.Dependencies {
        return deps[valueType] ?: {
            val dep = when (valueType) {
                AlertValueType.HIGH_TARGET_PRICE, AlertValueType.LOW_TARGET_PRICE -> InputFieldManager.Dependencies(
                    validator = TargetPriceValidator(
                        resources.getString(
                            R.string.generic_validation_error_description
                        )
                    ),
                    formatter = TargetPriceFormatter()
                )
                AlertValueType.HIGH_DAILY_PERCENT, AlertValueType.LOW_DAILY_PERCENT -> InputFieldManager.Dependencies(
                    validator = DailyPercentValidator(
                        resources.getString(
                            R.string.generic_validation_error_description
                        )
                    ),
                    formatter = DailyPercentFormatter(
                        resources.getString(
                            R.string.daily_percent_format
                        )
                    ),
                    customizer = DailyPercentCustomizer()
                )
                AlertValueType.HIGH_PE_TARGET, AlertValueType.LOW_PE_TARGET -> InputFieldManager.Dependencies(
                    validator = PePriceValidator(resources.getString(R.string.generic_validation_error_description)),
                    formatter = PePriceFormatter()
                )
                AlertValueType.DAILY_VOLUME -> InputFieldManager.Dependencies(
                    validator = DailyVolumeValidator(
                        resources.getString(
                            R.string.generic_validation_error_description
                        )
                    ),
                    formatter = DailyVolumeFormatter()
                )
            }
            deps[valueType] = dep
            dep
        }.invoke()
    }
}
