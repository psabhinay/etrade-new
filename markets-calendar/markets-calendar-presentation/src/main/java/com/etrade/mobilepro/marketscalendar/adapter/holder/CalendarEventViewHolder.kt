package com.etrade.mobilepro.marketscalendar.adapter.holder

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.marketscalendar.R
import com.etrade.mobilepro.marketscalendar.databinding.AdapterEventBinding
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails

open class CalendarEventViewHolder(
    private val binding: AdapterEventBinding,
    private val noValue: String,
    private val clickListener: View.OnClickListener? = null,
    rootView: ViewGroup? = null
) : RecyclerView.ViewHolder(rootView ?: binding.root) {

    private val emptyDefault = itemView.resources.getString(R.string.empty_default)
    private val valueNotAvailable = itemView.resources.getString(R.string.value_not_available)

    private fun setText(view: TextView, value: String?, noValue: String) {
        value
            ?.also {
                setTextStyle(view, it)

                view.text = it
            }
            ?: run { view.text = noValue }
    }

    private fun setTextStyle(view: TextView, value: String) {
        if (value.contains("-")) {
            TextViewCompat.setTextAppearance(view, R.style.TextAppearance_Etrade_Regular_Loss)
        } else {
            TextViewCompat.setTextAppearance(view, R.style.TextAppearance_Etrade_Regular_Gain)
        }
    }

    protected fun CharSequence.format(): CharSequence {
        return if (this == emptyDefault) {
            valueNotAvailable
        } else {
            this
        }
    }

    open fun bind(item: EventDetails) {
        binding.apply {
            root.tag = item
            root.setOnClickListener(clickListener)

            title.text = item.title
            estimateValue.text = item.estimate ?: noValue

            TextViewCompat.setTextAppearance(actualValue, R.style.TextAppearance_Etrade_Text_Medium_Large)
            TextViewCompat.setTextAppearance(surpriseValue, R.style.TextAppearance_Etrade_Text_Medium_Large)

            setText(binding.actualValue, item.actual, noValue)
            setText(binding.surpriseValue, item.surprise, noValue)

            date.text = item.date

            root.contentDescription = "${title.text} ${date.text} " +
                "${estimateTitle.text} ${estimateValue.text.format()} " +
                "${actualTitle.text} ${actualValue.text.format()} " +
                "${surpriseTitle.text} ${surpriseValue.text.format()}"
        }
    }
}
