package com.etrade.mobilepro.util.android.textutil

import android.text.InputFilter
import android.text.Spanned

class PatternInputFilter(private val regex: Regex?) : InputFilter {

    override fun filter(source: CharSequence, start: Int, end: Int, dest: Spanned, dstart: Int, dend: Int): CharSequence? {
        val subSource = source.subSequence(start, end).toString()
        val candidate = regex?.replace(subSource, "")
        return if (candidate == subSource) {
            null
        } else {
            candidate
        }
    }
}
