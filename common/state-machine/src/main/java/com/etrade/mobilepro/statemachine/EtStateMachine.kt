package com.etrade.mobilepro.statemachine

/**
 * ETrade interface which represents a state machine for triggering actions that transitions to
 * valid states.
 */
interface EtStateMachine<S, A> {
    val currentState: S
    var onStateChange: ((S) -> Unit)?

    fun transition(action: A)
}
