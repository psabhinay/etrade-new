package com.etrade.mobilepro.dynamic.form.ui

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.api.Action
import com.etrade.mobilepro.dynamic.form.api.ActionType
import com.etrade.mobilepro.dynamic.form.api.invoke
import com.etrade.mobilepro.util.android.extension.inflate

class ActionListView : LinearLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val actionToObserverList: MutableList<Pair<Action, Observer<Boolean>>> = mutableListOf()

    init {
        dividerDrawable = ContextCompat.getDrawable(context, R.drawable.divider_spacing_small)
        orientation = HORIZONTAL
        setBackgroundResource(R.color.windowBackground)
        showDividers = SHOW_DIVIDER_MIDDLE
        context.resources.getDimensionPixelSize(R.dimen.spacing_medium).let { p ->
            setPadding(p, p, p, p)
        }
    }

    fun render(actions: List<Action>, owner: LifecycleOwner) {
        resetView()

        actions.forEach { action ->
            val view = (inflate(action.layoutResId) as TextView)
                .apply {
                    text = action.title
                    setOnClickListener { action() }
                }
            val params = LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT)
                .apply { weight = 1f }
            addView(view, params)

            val observer = Observer<Boolean> { view.isEnabled = it }
            action.isEnabled.observe(owner, observer)
            actionToObserverList += action to observer
        }
    }

    private fun resetView() {
        removeAllViews()
        actionToObserverList.forEach { (action, observer) ->
            action.isEnabled.removeObserver(observer)
        }
        actionToObserverList.clear()
    }

    @get:LayoutRes
    private val Action.layoutResId: Int
        get() = when (actionType) {
            ActionType.PRIMARY -> R.layout.dynamic_form_view_action_primary
            ActionType.SECONDARY -> R.layout.dynamic_form_view_action_secondary
        }
}
