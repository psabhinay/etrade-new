package com.etrade.mobilepro.feedback.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FeedbackRequest(
    @Json(name = "value")
    val value: Value
)

@JsonClass(generateAdapter = true)
data class Value(
    @Json(name = "fields")
    val fields: List<Field>,
    @Json(name = "objectType")
    val objectType: String,
    @Json(name = "returnField")
    val returnField: String?
)

@JsonClass(generateAdapter = true)
data class Field(
    @Json(name = "name")
    val name: String,
    @Json(name = "value")
    val value: String?
)

/*

example of feedback request body json

{
  "value": {
    "fields": [
      {
        "name": "IL_Record_Type__c",
        "value": "Mobile_Customer_Feedback"
      },
      {
        "name": "RecordTypeId",
        "value": "Mobile Feedback"
      },
      {
        "name": "Account_Number__c",
        "value": ""
      },
      {
        "name": "Customer_User_ID__c",
        "value": ""
      },
      {
        "name": "Stars__c",
        "value": "4"
      },
      {
        "name": "Feedback__c",
        "value": "test for tests"
      },
      {
        "name": "Page__c",
        "value": "Menu"
      },
      {
        "name": "App_Version__c",
        "value": "7.3 165"
      },
      {
        "name": "Operating_System_Version__c",
        "value": "9"
      },
      {
        "name": "Device__c",
        "value": "Google Android SDK built for x86"
      },
      {
        "name": "Streaming__c",
        "value": "N"
      },
      {
        "name": "Connection__c",
        "value": "Wifi"
      },
      {
        "name": "Platform__c",
        "value": "Android"
      },
      {
        "name": "Source__c",
        "value": "Menu"
      },
      {
        "name": "Device_Id__c",
        "value": "3e0cd23be0a93d9e"
      }
    ],
    "objectType": "Customer_Feedback__c",
    "returnField": "Name__c"
  }
}

*/
