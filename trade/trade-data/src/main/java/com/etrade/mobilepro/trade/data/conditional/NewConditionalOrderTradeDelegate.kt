package com.etrade.mobilepro.trade.data.conditional

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.orders.api.OrigSysCode.Companion.origSysCodeKey
import com.etrade.mobilepro.trade.api.OrderPlaceResponse
import com.etrade.mobilepro.trade.api.OrderPreviewResponse
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderPreviewResponse
import com.etrade.mobilepro.trade.api.advanced.TrailingStopOrderResponse
import com.etrade.mobilepro.trade.data.TradeDelegate
import com.etrade.mobilepro.trade.data.TradeOrderArguments
import com.etrade.mobilepro.trade.data.advanced.ADVANCED_ORDER_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.AdvancedTradingApiClient
import com.etrade.mobilepro.trade.data.advanced.FORM_TARGET_KEY
import com.etrade.mobilepro.trade.data.advanced.INITIAL_STOP_PRICE_KEY
import com.etrade.mobilepro.trade.data.advanced.KEY_SECURITY_TYPE
import com.etrade.mobilepro.trade.data.advanced.ORDER_COUNT_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_TRIGGER_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.POSITION_EFFECT_KEY
import com.etrade.mobilepro.trade.data.advanced.PREVIEW_ID_KEY
import com.etrade.mobilepro.trade.data.advanced.PREVIEW_X_KEY
import com.etrade.mobilepro.trade.data.advanced.SIDE_KEY
import com.etrade.mobilepro.trade.data.advanced.TRADE_X_KEY
import com.etrade.mobilepro.trade.data.advanced.VALUE_SECURITY_TYPE_OPTION
import com.etrade.mobilepro.trade.data.advanced.copyTrailingStopParamsFromResponse
import com.etrade.mobilepro.trade.data.advanced.copyTrailingStopPriceToOffsetValue
import com.etrade.mobilepro.trade.data.advanced.extractSecurityType
import com.etrade.mobilepro.trade.data.advanced.place
import com.etrade.mobilepro.trade.data.advanced.preview
import com.etrade.mobilepro.trade.data.advanced.replaceKey
import com.etrade.mobilepro.trade.data.extractAction
import com.etrade.mobilepro.trade.data.fixAdvancedOrderOptionAction
import com.etrade.mobilepro.trade.data.fixLimitStopPriceParam
import com.etrade.mobilepro.trade.form.api.getTransactionType
import com.etrade.mobilepro.trade.form.api.input.CONDITIONAL_ORDER_SECURITY_TYPE_ID
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ConditionalTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractPriceType
import com.etrade.mobilepro.util.filterNotNullValues

private const val PATH = AdvancedTradingApiClient.PATH_OTA

private typealias ParametersModifier = (orderSnapshot: MutableMap<String, Any?>, index: Int) -> Unit

internal class NewConditionalOrderTradeDelegate(
    private val apiClient: AdvancedTradingApiClient,
    private val conditionalOrderTradeFormInputId: ConditionalOrderTradeFormInputId,
    private val conditionalTradeFormInputId: ConditionalTradeFormInputId,
    private val isTablet: Boolean
) : TradeDelegate {

    override suspend fun previewOrder(map: InputValuesSnapshot, marketDestination: MarketDestination): ETResult<OrderPreviewResponse> {
        return apiClient.preview(PATH, preparePreviewParameters(map))
    }

    override suspend fun placeOrder(arguments: TradeOrderArguments, order: TradeOrder): ETResult<OrderPlaceResponse> {
        val response = order.response
        require(response is AdvancedOrderPreviewResponse)
        return apiClient.place(PATH, preparePlaceParameters(arguments.params, response))
    }

    private fun preparePreviewParameters(snapshot: InputValuesSnapshot): Map<String, String> {
        val result = prepareParameters(snapshot, "createentry")

        // This parameter is needed to make do a successful request. Without it the request will fail. The probable meaning of this parameter is X position of
        // a "Preview" button. Why it is required for the request I have no idea. Why the value is 65 I have no idea too. Please address all questions to a
        // service team that maintains the endpoint.
        result[PREVIEW_X_KEY] = "65"

        return result.toMap()
    }

    private fun preparePlaceParameters(snapshot: InputValuesSnapshot, response: AdvancedOrderPreviewResponse): Map<String, String> {
        val result = prepareParameters(snapshot, "preview") { orderSnapshot, index ->
            orderSnapshot[ORDER_TYPE_KEY] = response.orders[index].orderType.typeCode
            response.orders[index].advOrderType?.let { orderSnapshot[ADVANCED_ORDER_TYPE_KEY] = it.typeCode }
            orderSnapshot.copyTrailingStopParamsFromResponse(response as TrailingStopOrderResponse, index, conditionalOrderTradeFormInputId)
            val isOption = orderSnapshot[KEY_SECURITY_TYPE] == VALUE_SECURITY_TYPE_OPTION
            val transactionType = orderSnapshot.extractAction(isOption, conditionalOrderTradeFormInputId)?.also {
                orderSnapshot.remove(conditionalOrderTradeFormInputId.action)
                orderSnapshot.replaceKey(conditionalOrderTradeFormInputId.priceType, ORDER_TRIGGER_KEY)
                orderSnapshot[INITIAL_STOP_PRICE_KEY] = orderSnapshot[conditionalOrderTradeFormInputId.stopPrice]
            }?.getTransactionType(isOption)
            transactionType?.let {
                orderSnapshot[SIDE_KEY] = it.orderAction.value
                orderSnapshot[POSITION_EFFECT_KEY] = it.positionEffect
            }
        }

        response.previewId?.let { result[PREVIEW_ID_KEY] = it }

        // This parameter is needed to make do a successful request. Without it the request will fail. The probable meaning of this parameter is X position of
        // a "Place" button. Why it is required for the request I have no idea. Why the value is 60 I have no idea too. Please address all questions to a
        // service team that maintains the endpoint.
        result[TRADE_X_KEY] = "60"

        return result.toMap()
    }

    private fun prepareParameters(snapshot: InputValuesSnapshot, method: String, update: ParametersModifier = { _, _ -> }): MutableMap<String, String> {
        val result = mutableMapOf<String, String>()
        result[conditionalTradeFormInputId.accountId] = snapshot[conditionalTradeFormInputId.accountId] as String

        var orderSize = addOrder(result, snapshot, conditionalTradeFormInputId.order1, update)
        orderSize += addOrder(result, snapshot, conditionalTradeFormInputId.order2, update)
        orderSize += addOrder(result, snapshot, conditionalTradeFormInputId.order3, update)
        result[ORDER_COUNT_KEY] = orderSize.toString()

        // These values are required by the endpoint for Logging / Telemetry.
        result[FORM_TARGET_KEY] = "$PATH$method"
        result[origSysCodeKey] = if (isTablet) {
            "tablet"
        } else {
            "android"
        }

        return result
    }

    private fun addOrder(result: MutableMap<String, String>, snapshot: InputValuesSnapshot, orderId: String, update: ParametersModifier): Int {
        @Suppress("UNCHECKED_CAST") val orderSnapshot = (snapshot[orderId] as? InputValuesSnapshot)?.toMutableMap() ?: return 0

        result.putAll(
            with(conditionalOrderTradeFormInputId) {
                orderSnapshot.remove(accountId)
                orderSnapshot.remove(securityType)
                orderSnapshot.remove(underlierSymbol)
                orderSnapshot.remove(CONDITIONAL_ORDER_SECURITY_TYPE_ID)
                orderSnapshot.fixLimitStopPriceParam(this, extractPriceType(orderSnapshot))
                orderSnapshot.fixAdvancedOrderOptionAction(conditionalOrderTradeFormInputId)
                copyTrailingStopPriceToOffsetValue(orderSnapshot)
                extractSymbolInfo(orderSnapshot)?.let { symbolInfo ->
                    val actualSymbol = symbolInfo.symbol
                    orderSnapshot[symbol] = actualSymbol
                    orderSnapshot[KEY_SECURITY_TYPE] = orderSnapshot.extractSecurityType(symbolInfo, actualSymbol)
                }
                orderSnapshot
                    .also { update(it, orderId.toInt()) }
                    .mapKeys { it.key + orderId }
                    .mapValues { it.value as? String }
                    .filterNotNullValues()
            }
        )

        return 1
    }
}
