package com.etrade.mobilepro.trade.dropdown

import android.os.Bundle
import android.view.View
import android.view.accessibility.AccessibilityEvent
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.common.Navigation
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.common.getNavigationButtonDescription
import com.etrade.mobilepro.common.setupToolbar
import com.etrade.mobilepro.dropdown.SelectorViewAdapter
import com.etrade.mobilepro.trade.help.ARGUMENT_KEY_SKIP_TITLE_UPDATE
import com.etrade.mobilepro.trade.help.ARGUMENT_KEY_TRADE_OPTION_INFO_TYPE
import com.etrade.mobilepro.util.android.accessibility.OnFirstAccessibilityFocusRunner
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import javax.inject.Inject

class TradeSelectFragment @Inject constructor() : Fragment(R.layout.fragment_trade_selector) {

    val dialog: BottomSheetDialogFragment?
        get() = parentFragment?.parentFragment as? BottomSheetDialogFragment

    private var viewAdapter: SelectorViewAdapter? = null

    private val selectorHolder: TradeSelectorHolder?
        get() = (parentFragment?.parentFragment as? HasTradeSelectorHolder)?.holder

    private val navController: NavController
        get() = findNavController()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog?.setUpOverlayToolbar()
        selectorHolder?.let {
            val adapter = SelectorViewAdapter(it.dropDownItems) { position ->
                dialog?.dismiss()
                selectorHolder?.listener?.invoke(position)
            }
            val viewManager = LinearLayoutManager(context)
            view.findViewById<RecyclerView>(R.id.overlayRecyclerView).apply {
                layoutManager = viewManager
                this.adapter = adapter
            }
            viewAdapter = adapter
            it.selectedPosition.observe(viewLifecycleOwner) { position ->
                updateSelectedPosition(position)
            }
        }
    }

    private fun updateSelectedPosition(position: Int?) {
        viewAdapter?.let { adapter ->
            val currentPosition = adapter.selectedPosition
            if (position != currentPosition) {
                adapter.selectedPosition = position
                currentPosition?.let { adapter.notifyItemChanged(currentPosition) }
                position?.let { adapter.notifyItemChanged(position) }
            }
        }
    }

    private fun BottomSheetDialogFragment.setUpOverlayToolbar() {
        val nav = Navigation(
            icon = R.drawable.ic_close,
            onClick = { dialog?.dismiss() },
            contentDescription = null
        )
        val actionEnd = selectorHolder?.helpParams?.let { params ->
            ToolbarActionEnd(
                isIcon = true,
                icon = R.drawable.ic_help,
                contentDescription = resources.getString(R.string.trade_content_description_help),
                onClick = {
                    navController.navigate(
                        R.id.dropdownTradeHelpFragment,
                        Bundle().apply {
                            putSerializable(ARGUMENT_KEY_TRADE_OPTION_INFO_TYPE, params.infoType)
                            putBoolean(ARGUMENT_KEY_SKIP_TITLE_UPDATE, true)
                        }
                    )
                }
            )
        }
        setupToolbar(
            title = selectorHolder?.title,
            navigation = nav,
            runOnFirstFocus = { toolbar, _, title, _ ->
                OnFirstAccessibilityFocusRunner(toolbar) {
                    title?.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED)
                    toolbar.navigationContentDescription = context?.getNavigationButtonDescription(nav.icon)
                }
            },
            actionEnd = actionEnd
        )
    }
}
