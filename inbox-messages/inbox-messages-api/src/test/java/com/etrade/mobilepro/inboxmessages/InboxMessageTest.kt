package com.etrade.mobilepro.inboxmessages

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class InboxMessageTest {

    @Test
    fun `MessageType convert succeeds with valid type name`() {
        assertThat(MessageType.convert("dynamic_navigation"), `is`(MessageType.DYNAMIC_NAVIGATION))
        assertThat(MessageType.convert("contextual"), `is`(MessageType.CONTEXTUAL))
    }

    @Test
    fun `MessageType convert returns null with invalid type name`() {
        assertThat(MessageType.convert("foo"), nullValue())
    }

    @Test
    fun `MessagePlace convert succeeds with valid place name`() {
        assertThat(MessagePlace.convert("menu"), `is`(MessagePlace.MENU))
        assertThat(MessagePlace.convert("customer_service"), `is`(MessagePlace.CUSTOMER_SERVICE))
    }

    @Test
    fun `MessagePlace convert returns null with invalid place name`() {
        assertThat(MessageType.convert("foo"), nullValue())
    }
}
