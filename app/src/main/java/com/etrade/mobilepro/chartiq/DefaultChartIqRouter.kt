package com.etrade.mobilepro.chartiq

import android.content.Intent
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import com.etrade.mobilepro.ChartiqInternalNavGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.chartiq.presentation.ChartIqRouter
import com.etrade.mobilepro.dynamic.form.fragment.createFormValuesArgument
import com.etrade.mobilepro.login.LoginActivity
import com.etrade.mobilepro.order.details.api.OrderData
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.squareup.moshi.Moshi
import javax.inject.Inject

class DefaultChartIqRouter @Inject constructor(
    private val moshi: Moshi
) : ChartIqRouter {

    override fun showLogin(activity: FragmentActivity?, symbol: SearchResultItem.Symbol) {
        activity?.let {
            it.startActivity(Intent(it, LoginActivity::class.java))
        }
    }

    override fun showEditOrderScreen(activity: FragmentActivity?, data: OrderData) {
        val navController = activity?.findNavController(R.id.nav_host_fragment)
        val formValues = createFormValuesArgument(moshi, data.formValues)
        val navDirection = ChartiqInternalNavGraphDirections.actionLaunchTrade(formValues, data.inputsDisabled, data.tradeOrderType)
        navController?.navigate(navDirection)
    }
}
