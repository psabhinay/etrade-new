package com.etrade.mobilepro.screener.presentation.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.screener.presentation.databinding.ItemScreenerFooterBinding

class ScreenerFooterViewHolder(binding: ItemScreenerFooterBinding) : RecyclerView.ViewHolder(binding.root)
