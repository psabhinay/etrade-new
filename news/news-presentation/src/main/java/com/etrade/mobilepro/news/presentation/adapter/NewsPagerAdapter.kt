package com.etrade.mobilepro.news.presentation.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.api.model.NewsType.COMPANY
import com.etrade.mobilepro.news.api.model.NewsType.MARKET
import com.etrade.mobilepro.news.api.model.NewsType.PORTFOLIO
import com.etrade.mobilepro.news.api.model.NewsType.WATCHLIST
import com.etrade.mobilepro.news.presentation.company.CompanyNewsFragment
import com.etrade.mobilepro.news.presentation.market.MarketNewsFragment
import com.etrade.mobilepro.news.presentation.portfolio.PortfolioNewsFragment
import com.etrade.mobilepro.news.presentation.watchlist.WatchlistNewsFragment
import com.google.android.material.tabs.TabLayout

class NewsPagerAdapter(
    private val fragment: Fragment,
    private val isAuth: Boolean
) : FragmentStateAdapter(fragment) {

    companion object {
        val AUTH_STATE_PAGES = listOf(PORTFOLIO, WATCHLIST, MARKET, COMPANY)
        val NON_AUTH_STATE_PAGES = listOf(WATCHLIST, MARKET, COMPANY)
    }

    private fun pages() = if (isAuth) AUTH_STATE_PAGES else NON_AUTH_STATE_PAGES

    fun getPageTitle(position: Int) = pages()[position].title(fragment.requireContext())

    fun resolveTabIndex(type: NewsType) = pages().indexOf(type)

    override fun getItemCount() = pages().size

    override fun createFragment(position: Int) =
        when (pages()[position]) {
            PORTFOLIO -> PortfolioNewsFragment()
            WATCHLIST -> WatchlistNewsFragment()
            MARKET -> MarketNewsFragment()
            COMPANY -> CompanyNewsFragment()
            else -> throw IllegalArgumentException("Unsupported news type: $this")
        }

    inner class TabLayoutListener(
        private val onTabUnselected: (type: NewsType) -> Unit = {},
        private val onTabReselected: (type: NewsType) -> Unit = {},
        private val onTabSelected: (type: NewsType) -> Unit = {}
    ) : TabLayout.OnTabSelectedListener {

        override fun onTabUnselected(tab: TabLayout.Tab) {
            onTabUnselected(tab.type())
        }

        override fun onTabReselected(tab: TabLayout.Tab) {
            onTabReselected(tab.type())
        }

        override fun onTabSelected(tab: TabLayout.Tab) {
            onTabSelected(tab.type())
        }

        private fun TabLayout.Tab.type() = pages()[position]
    }
}
