package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.search

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.billpay.api.SearchPayeeResultItem
import com.etrade.mobilepro.util.android.extension.dispatchUpdates

internal class SearchPayeeAdapter(
    private val clickListener: (SearchPayeeResultItem) -> Unit
) : RecyclerView.Adapter<SearchPayeeViewHolder>() {

    var items: List<SearchPayeeResultItem> = emptyList()
        set(value) {
            val oldValue = field
            field = value
            dispatchUpdates(oldValue, value)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        SearchPayeeViewHolder(parent, clickListener)

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: SearchPayeeViewHolder, position: Int) = holder.bind(items[position])
}
