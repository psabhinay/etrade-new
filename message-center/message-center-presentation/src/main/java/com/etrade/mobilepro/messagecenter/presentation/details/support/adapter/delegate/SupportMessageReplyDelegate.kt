package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterItemSupportMessageReplyBinding
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.Reply
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.SupportMessageItem
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.viewholder.SupportMessageReplyViewHolder
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class SupportMessageReplyDelegate(
    private val replyContentListener: (String) -> Unit
) : AbsListItemAdapterDelegate<Reply, SupportMessageItem, SupportMessageReplyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): SupportMessageReplyViewHolder =
        SupportMessageReplyViewHolder(
            parent.viewBinding(MessageCenterItemSupportMessageReplyBinding::inflate),
            replyContentListener
        )

    override fun isForViewType(item: SupportMessageItem, items: MutableList<SupportMessageItem>, position: Int): Boolean =
        item is Reply

    override fun onBindViewHolder(item: Reply, holder: SupportMessageReplyViewHolder, payloads: MutableList<Any>) =
        holder.bindTo(item)
}
