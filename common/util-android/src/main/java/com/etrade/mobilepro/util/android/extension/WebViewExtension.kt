package com.etrade.mobilepro.util.android.extension

import android.content.res.Resources
import android.webkit.WebView
import androidx.annotation.RawRes

fun WebView.loadFormattedData(@RawRes rawId: Int, replacementData: Map<String, String>?) {
    val finalData = resources.formatReplacementData(rawId, replacementData)
    loadDataWithBaseURL(null, finalData, "text/html", "utf-8", null)
}

private fun Resources.formatReplacementData(@RawRes rawId: Int, replacementData: Map<String, String>?): String {
    val reader = openRawResource(rawId).bufferedReader()
    var ch: Int
    val localBuffer = StringBuffer()
    while (reader.read().also { ch = it } != -1) localBuffer.append(ch.toChar())
    reader.close()
    var finalData = localBuffer.toString()
    replacementData?.map {
        finalData = finalData.replace(it.key, it.value)
    }
    return finalData
}
