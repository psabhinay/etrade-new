package com.etrade.mobilepro.quoteapi

import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.livedata.requireValue
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.StockData

internal inline fun <T> MutableLiveData<T>.updateWith(perform: (T) -> T?) {
    val prototype = requireValue
    perform(prototype)?.let {
        postValue(it)
    }
}

internal fun Level1Data.toLastPrice(
    prototype: QuotePriceWidgetItem,
    resources: QuoteTemplateResources,
    hasNeutralColor: Boolean = true
): QuotePriceWidgetItem? {
    return lastPrice?.let { lastPrice ->
        prototype.copy(
            price = lastPrice,
            priceColor = resources.getColorWithTickIndicator(tickIndicator, hasNeutralColor) ?: prototype.priceColor,
            exchangeCode = lastPriceExchange ?: prototype.exchangeCode,
            size = if (lastPriceExchange != null) {
                lastVolume
            } else {
                prototype.size
            }
        )
    }
}

private fun QuoteTemplateResources.getColorWithTickIndicator(tickIndicator: Short?, hasNeutralColor: Boolean = true) = when (tickIndicator) {
    (-1).toShort() -> negativeChangeColorRes
    1.toShort() -> positiveChangeColorRes
    0.toShort() -> if (hasNeutralColor) neutralChangeColorRes else null
    else -> {
        null
    }
}

internal fun Level1Data.toChange(prototype: QuoteChangeWidgetItem, useDaysChange: Boolean = false): QuoteChangeWidgetItem? {
    var change = change
    var changePercent = changePercent
    if (this is StockData && useDaysChange) {
        change = dayChange
        changePercent = dayChangePercent
    }
    return if (change != null || changePercent != null) {
        prototype.let { oldValue ->
            oldValue.copy(
                change = change ?: oldValue.change,
                changePercent = changePercent ?: oldValue.changePercent
            )
        }
    } else {
        null
    }
}

internal fun Level1Data.toChangeColor(prototype: Int, resources: QuoteTemplateResources, useDaysChange: Boolean = false): Int {
    val updatedChange = if (this is StockData && useDaysChange) {
        dayChange
    } else {
        change
    }
    return when (updatedChange?.toBigDecimalOrNull()?.signum()) {
        null -> prototype
        -1 -> resources.negativeChangeColorRes
        else -> resources.positiveChangeColorRes
    }
}

internal fun Level1Data.toBidPrice(prototype: QuotePriceWidgetItem): QuotePriceWidgetItem? = toPriceItem(prototype, bid, bidSize, bidExchange)

internal fun Level1Data.toAskPrice(prototype: QuotePriceWidgetItem): QuotePriceWidgetItem? = toPriceItem(prototype, ask, askSize, askExchange)

private fun toPriceItem(prototype: QuotePriceWidgetItem, price: String?, size: String?, exchangeCode: String?): QuotePriceWidgetItem? {
    return if (price != null || size != null) {
        prototype.copy(
            price = price ?: prototype.price,
            size = size ?: prototype.size,
            exchangeCode = exchangeCode ?: prototype.exchangeCode
        )
    } else {
        null
    }
}
