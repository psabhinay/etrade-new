package com.etrade.mobilepro.viewdelegate

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.util.AppMessage
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.TextMessage
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.viewmodel.AppMessageViewModel
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

class AppMessageViewDelegate(private val viewModel: AppMessageViewModel, private val snackBarUtil: SnackBarUtil) : ViewDelegate {

    private val snackbarCallback: BaseTransientBottomBar.BaseCallback<Snackbar> = object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            viewModel.dismissAppMessage()
        }
    }

    private var snackbar: Snackbar? = null

    override fun observe(owner: LifecycleOwner) {
        viewModel.appMessageSignal.observe(owner, Observer { onAppMessageSignal(it) })
    }

    private fun onAppMessageSignal(message: AppMessage?) {
        snackbar?.dismiss()
        snackbar = null

        if (message != null) {
            snackbar = when (message) {
                is ErrorMessage -> snackbar(message)
                is TextMessage -> snackbar(message)
            }?.apply {
                addCallback(snackbarCallback)
            }
        }
    }

    private fun snackbar(message: ErrorMessage): Snackbar? {
        val text = message.message
        val retryAction = message.retryAction

        return if (retryAction == null) {
            snackBarUtil.errorSnackbar(text, Snackbar.LENGTH_LONG)
        } else {
            snackBarUtil.retrySnackbar(message = text, actionOnRetry = retryAction)
        }
    }

    private fun snackbar(message: TextMessage): Snackbar? = snackBarUtil.snackbar(message.message, message.duration.length)
}

private val TextMessage.Duration.length: Int
    get() = when (this) {
        TextMessage.Duration.INDEFINITE -> Snackbar.LENGTH_INDEFINITE
        TextMessage.Duration.LONG -> Snackbar.LENGTH_LONG
        TextMessage.Duration.SHORT -> Snackbar.LENGTH_SHORT
    }
