package com.etrade.mobilepro.watchlist.presentation

import androidx.fragment.app.Fragment
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.etcompose.tabhorizontalpager.PagerContent
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.movers.widget.MoversWidgetViewModel
import com.etrade.mobilepro.navdestinations.openDisclosures
import com.etrade.mobilepro.news.presentation.watchlist.WatchlistNewsViewModel
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.fragment.navScoped
import com.etrade.mobilepro.util.android.fragment.purgeNavScoped
import com.etrade.mobilepro.watchlist.news.WatchlistNews
import com.etrade.mobilepro.watchlist.news.filter.WatchlistFilterViewModel
import com.etrade.mobilepro.watchlist.news.filter.initFilterDropDown
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.slf4j.LoggerFactory

class WatchlistNewsTabDelegate(
    private val fragment: Fragment,
    private val isAuth: UserAuthenticationState,
    viewModelFactory: ViewModelProvider.Factory,
) {
    private val logger = LoggerFactory.getLogger(this::class.java)
    private val viewModelsNavScope = setOf(R.id.watchlistContainerFragment, R.id.textNewsDetailFragment)
    private val moversWidgetViewModel by fragment.navScoped(
        scopedTo = viewModelsNavScope,
        initializer = { viewModelFactory.create(MoversWidgetViewModel::class.java) }
    )
    private val watchlistNewsViewModel by fragment.navScoped(
        scopedTo = viewModelsNavScope,
        initializer = { viewModelFactory.create(WatchlistNewsViewModel::class.java) }
    )
    private val watchlistFilterViewModel by fragment.navScoped(
        scopedTo = viewModelsNavScope,
        initializer = { viewModelFactory.create(WatchlistFilterViewModel::class.java) }
    )
    private val filterTapListener = MediatorLiveData<() -> Unit>().apply { value = {} }

    fun watchlistNews(): PagerContent = {
        WatchlistNews(
            moversVM = moversWidgetViewModel,
            newsVM = watchlistNewsViewModel,
            filterVM = watchlistFilterViewModel,
            filterTapListener = filterTapListener,
            onNewsTap = {
                watchlistNewsViewModel.newsRouter.navigateToTextNewsDetails(fragment.findNavController(), it.docId)
            },
            onSymbolTap = {
                watchlistNewsViewModel.newsRouter.navigateToQuoteDetails(fragment.requireView(), it, InstrumentType.EQ)
            },
            onDisclosuresTap = {
                fragment.context?.openDisclosures()?.let { fragment.startActivity((it)) }
            },
            onRefresh = {
                watchlistFilterViewModel.filterDataSource.refresh(forceUpdate = true)
            }
        )
    }

    fun resetState() {
        listOf(moversWidgetViewModel, watchlistFilterViewModel, watchlistNewsViewModel).forEach {
            fragment.purgeNavScoped(it, viewModelsNavScope)
        }
    }

    fun forceUpdateTab() {
        watchlistFilterViewModel.filterDataSource.refresh(forceUpdate = true)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    fun onViewCreated() {
        logger.debug("got VMs: $moversWidgetViewModel $watchlistNewsViewModel $watchlistFilterViewModel")
        val onFilterTap = fragment.initFilterDropDown(
            filterDataSource = watchlistFilterViewModel.filterDataSource,
            onFilterClickedUnAuth = {
                resetState()
                watchlistNewsViewModel.onFilterClickedUnAuth(fragment.findNavController())
            },
            isAuth = isAuth
        )
        filterTapListener.addSource(onFilterTap) {
            filterTapListener.value = it
        }
        watchlistFilterViewModel.filterDataSource.selectedItem.observe(fragment.viewLifecycleOwner) {
            watchlistNewsViewModel.applyFilter(it)
            moversWidgetViewModel.refresh(it.id)
        }
        watchlistFilterViewModel.filterDataSource.forcedUpdateWith.observe(fragment.viewLifecycleOwner) {
            it.consume { wl ->
                watchlistNewsViewModel.applyFilter(wl, forced = true)
                moversWidgetViewModel.refresh(wl.id, forced = true)
            }
        }
        watchlistFilterViewModel.filterDataSource.refresh()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    fun onWatchlistContentChanged() {
        watchlistNewsViewModel.refreshWithCurrentParams()
        moversWidgetViewModel.getMovers()
    }
}
