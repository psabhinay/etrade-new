package com.etrade.mobilepro.bloombergtv.presentation.delegate

import android.content.Intent
import android.view.ViewGroup
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.lifecycleScope
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvActivity
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvAudioService
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvService
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvViewModel
import com.etrade.mobilepro.bloombergtv.presentation.R
import com.etrade.mobilepro.bloombergtv.presentation.VideoControllerView
import com.etrade.mobilepro.bloombergtv.presentation.common.ACTION_PLAY
import com.etrade.mobilepro.bloombergtv.presentation.common.AUDIO_URL
import com.etrade.mobilepro.bloombergtv.presentation.common.START_TIME
import com.etrade.mobilepro.bloombergtv.presentation.common.VIDEO_URL
import com.google.android.material.snackbar.Snackbar

internal class VideoControllerDelegate(
    private val drawOverlayRequestDelegate: DrawOverlayRequestDelegate,
    private val viewModel: BloombergTvViewModel
) : BloombergTvActivityDelegate {
    private lateinit var activity: BloombergTvActivity
    private var controllerView: VideoControllerView? = null
    private var mediaPlayerDelegate: MediaPlayerDelegate? = null

    internal var isPlaying
        get() = viewModel.shouldBePlaying
        set(value) {
            viewModel.shouldBePlaying = value
        }

    internal fun attachTo(view: ViewGroup, mediaPlayerDelegate: MediaPlayerDelegate) {
        this.mediaPlayerDelegate = mediaPlayerDelegate
        controllerView?.attachTo(view, mediaPlayerDelegate)
    }

    internal fun show() {
        controllerView?.show()
    }

    internal fun hide() {
        controllerView?.hide()
    }

    internal fun enablePausePlayButton(enabled: Boolean) =
        controllerView?.enablePausePlayButton(enabled)

    internal fun onPictureInPictureClick() {
        viewModel.videoUrl?.let {
            if (drawOverlayRequestDelegate.hasDrawOverlayPermission { onPictureInPictureClick() }) {
                val intent = Intent(activity, BloombergTvService::class.java)
                intent.putExtra(VIDEO_URL, it)
                activity.snackbarUtil
                    .snackbar(
                        activity.getString(R.string.bloomberg_pip_entering_message),
                        Snackbar.LENGTH_SHORT
                    )
                    ?.show()
                activity.startService(intent)
                mediaPlayerDelegate?.pause()
                mediaPlayerDelegate?.releasePlayer()
                activity.finish()
            }
        }
    }

    internal fun onAudioOnlyModeClick() {
        viewModel.videoUrl?.let {
            val videoStartTime = mediaPlayerDelegate?.currentPosition ?: 0
            val intent = Intent(activity, BloombergTvAudioService::class.java)
            intent.putExtra(AUDIO_URL, it)
            intent.putExtra(START_TIME, videoStartTime)
            intent.action = ACTION_PLAY
            activity.snackbarViewModel.postAppMessage(activity.getString(R.string.bloomberg_audio_entering_message))
            activity.startForegroundService(intent)
            mediaPlayerDelegate?.releasePlayer()
            activity.onBackPressed()
        }
    }

    private fun hideSystemUI() {
        WindowInsetsControllerCompat(
            activity.window,
            activity.binding.root
        ).hide(WindowInsetsCompat.Type.statusBars() or WindowInsetsCompat.Type.navigationBars())
    }

    private fun showSystemUI() {
        WindowInsetsControllerCompat(
            activity.window,
            activity.binding.root
        ).show(WindowInsetsCompat.Type.statusBars() or WindowInsetsCompat.Type.navigationBars())
    }

    override fun init(activity: BloombergTvActivity) {
        this.drawOverlayRequestDelegate.init(activity)
        this.activity = activity
        controllerView = VideoControllerView(activity, this, activity.lifecycleScope).apply {
            onShow = ::showSystemUI
            onHide = ::hideSystemUI
        }
    }
}
