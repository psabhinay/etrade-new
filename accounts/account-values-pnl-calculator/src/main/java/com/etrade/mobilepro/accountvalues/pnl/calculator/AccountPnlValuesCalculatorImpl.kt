package com.etrade.mobilepro.accountvalues.pnl.calculator

import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult.CalculatedAccountValuesVO
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult.EmptyCalculationResult
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.pnl.calculations.calculateTotalCost
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.util.domain.data.deriveAccountTotalGainPercentage
import com.etrade.mobilepro.util.domain.data.deriveDaysGainPercentage
import com.etrade.mobilepro.util.safeParseBigDecimal
import java.math.BigDecimal
import javax.inject.Inject

class AccountPnlValuesCalculatorImpl @Inject constructor() : AccountPnlValuesCalculator {

    override fun calculateAccountValues(
        instrumentTypes: Map<String, InstrumentType>,
        positions: List<Position>,
        cashValue: BigDecimal
    ): CalculatedAccountValuesVO {
        val sumValues = calculateIntermediateAccountValues(instrumentTypes, positions)
        val marketValue = (sumValues.marketValueSum ?: BigDecimal.ZERO).add(cashValue)
        val totalGainPercent = if (sumValues.totalGainSum != null && sumValues.totalCostSum != null) {
            deriveAccountTotalGainPercentage(sumValues.totalGainSum, sumValues.totalCostSum)
        } else {
            null
        }
        val daysGainPercent = if (sumValues.daysGainSum != null) {
            deriveDaysGainPercentage(sumValues.daysGainSum, marketValue)
        } else {
            null
        }
        return CalculatedAccountValuesVO(
            marketValue = sumValues.marketValueSum ?: BigDecimal.ZERO,
            netLiquidationValue = marketValue,
            plDay = sumValues.daysGainSum,
            plDayPercent = daysGainPercent,
            totalCost = sumValues.totalCostSum,
            totalGain = if (totalGainPercent != null) sumValues.totalGainSum else null,
            totalGainPercent = totalGainPercent
        )
    }

    override fun calculateAllBrokerageAccountValues(
        accountIds: List<String>,
        lastCalculatedAccountValues: Map<String, CalculatedAccountValues>
    ): CalculationResult {
        val sumAccountValues = calculateAllBrokerageIntermediateAccountValues(accountIds, lastCalculatedAccountValues)
            ?: return EmptyCalculationResult
        val daysGain = sumAccountValues.daysGainSum
        val marketValue = sumAccountValues.marketValueSum
        val daysGainPercent = if (daysGain != null && marketValue != null) {
            deriveDaysGainPercentage(daysGain, marketValue)
        } else {
            null
        }

        val totalGain = sumAccountValues.totalGainSum
        val totalCost = sumAccountValues.totalCostSum
        val totalGainPercent = if (totalGain != null && totalCost != null) {
            deriveAccountTotalGainPercentage(totalGain, totalCost)
        } else {
            null
        }
        return CalculatedAccountValuesVO(
            netLiquidationValue = sumAccountValues.accountValueSum,
            plDay = daysGain,
            plDayPercent = daysGainPercent,
            totalGain = if (totalGainPercent != null) totalGain else null,
            totalGainPercent = totalGainPercent
        )
    }

    override fun calculateCompleteViewSummaryAccountValues(
        accounts: List<Account>,
        lastCalculatedAccountValues: Map<String, CalculatedAccountValuesVO>
    ): CalculationResult {
        var netAssets: BigDecimal? = null
        var daysGainSum: BigDecimal? = null
        accounts.forEach { account ->
            val calculateAccountValues =
                calculateCompleteViewValuesForProvidedAccount(account, lastCalculatedAccountValues) ?: return EmptyCalculationResult
            if (calculateAccountValues.nav != null) {
                netAssets = calculateAccountValues.nav + (netAssets ?: BigDecimal.ZERO)
            }
            if (calculateAccountValues.daysGain != null) {
                daysGainSum = calculateAccountValues.daysGain + (daysGainSum ?: BigDecimal.ZERO)
            }
        }
        return CalculatedAccountValuesVO(
            netAssets = netAssets,
            plDay = daysGainSum
        )
    }

    internal fun calculateIntermediateAccountValues(instrumentTypes: Map<String, InstrumentType>, positions: List<Position>): IntermediateAccountValues {
        var daysGainSum: BigDecimal? = null
        var marketValueSum: BigDecimal? = null
        var totalGainSum: BigDecimal? = null
        var totalCostSum: BigDecimal? = null

        positions.forEach { position ->
            val totalGain = position.totalGain
            val totalCost = getTotalCost(instrumentTypes[position.symbol], position)
            val daysGain = position.daysGain

            if (daysGain != null) {
                daysGainSum = daysGain + (daysGainSum ?: BigDecimal.ZERO)
                marketValueSum = when (instrumentTypes[position.symbol]) {
                    InstrumentType.BOND -> position.marketValue?.add(marketValueSum ?: BigDecimal.ZERO) ?: (marketValueSum ?: BigDecimal.ZERO)
                    else -> (position.mark?.multiply(position.quantity ?: BigDecimal.ZERO) ?: BigDecimal.ZERO)
                        .add(marketValueSum ?: BigDecimal.ZERO)
                }
            }
            if (totalGain != null && totalCost != null) {
                totalGainSum = totalGain + (totalGainSum ?: BigDecimal.ZERO)
                totalCostSum = totalCost + (totalCostSum ?: BigDecimal.ZERO)
            }
        }

        return IntermediateAccountValues(
            daysGainSum = daysGainSum,
            marketValueSum = marketValueSum,
            totalGainSum = totalGainSum,
            totalCostSum = totalCostSum
        )
    }

    internal fun getTotalCost(
        instrumentType: InstrumentType?,
        position: Position
    ): BigDecimal? {
        val pricePaid = position.pricePaid ?: return null
        val quantity = position.quantity ?: return null
        val commission = position.commission ?: return null
        val fees = position.fees ?: return null

        return if (instrumentType != null) {
            calculateTotalCost(instrumentType, pricePaid, quantity, commission, fees)
        } else {
            null
        }
    }

    internal fun calculateAllBrokerageIntermediateAccountValues(
        accountIds: List<String>,
        lastCalculatedAccountValues: Map<String, CalculatedAccountValues>
    ): IntermediateAccountValues? {
        var accountValueSum: BigDecimal? = null
        var daysGainSum: BigDecimal? = null
        var marketValueSum: BigDecimal? = null
        var totalGainSum: BigDecimal? = null
        var totalCostSum: BigDecimal? = null

        accountIds.forEach { id ->
            val accountValues = lastCalculatedAccountValues[id]?.accountValues ?: return null

            accountValues.apply {
                netLiquidationValue?.let {
                    accountValueSum = it + (accountValueSum ?: BigDecimal.ZERO)
                }
                if (marketValue != null && plDay != null) {
                    daysGainSum = plDay + (daysGainSum ?: BigDecimal.ZERO)
                    marketValueSum = marketValue + (marketValueSum ?: BigDecimal.ZERO)
                }
                if (totalCost != null && totalGain != null) {
                    totalCostSum = totalCost + (totalCostSum ?: BigDecimal.ZERO)
                    totalGainSum = totalGain + (totalGainSum ?: BigDecimal.ZERO)
                }
            }
        }
        return IntermediateAccountValues(accountValueSum, daysGainSum, marketValueSum, totalGainSum, totalCostSum)
    }

    internal fun calculateCompleteViewValuesForProvidedAccount(
        account: Account,
        lastCalculatedAccountValues: Map<String, CalculatedAccountValuesVO>
    ): IntermediateCompleteViewAccountValues? {
        val accountType = account.accountType
        var nav: BigDecimal? = null
        var daysGain: BigDecimal? = null
        when (accountType) {
            AccountType.Brokerage -> {
                val accountId = account.accountId
                val accountValues = lastCalculatedAccountValues[accountId]
                nav = accountValues?.netLiquidationValue ?: account.accountValue?.safeParseBigDecimal() ?: return null
                daysGain = accountValues?.plDay ?: account.daysGain?.safeParseBigDecimal() ?: BigDecimal.ZERO
            }
            AccountType.Bank -> {
                nav = account.availableBalance?.safeParseBigDecimal()
            }
            AccountType.ESP,
            AccountType.Managed -> {
                nav = account.accountValue?.safeParseBigDecimal()
                daysGain = account.daysGain?.safeParseBigDecimal()
            }
            else -> Unit
        }
        return IntermediateCompleteViewAccountValues(nav, daysGain)
    }
}

internal data class IntermediateCompleteViewAccountValues(
    val nav: BigDecimal?,
    val daysGain: BigDecimal?
)

internal data class IntermediateAccountValues(
    val accountValueSum: BigDecimal? = null,
    val daysGainSum: BigDecimal?,
    val marketValueSum: BigDecimal?,
    val totalGainSum: BigDecimal?,
    val totalCostSum: BigDecimal?
)
