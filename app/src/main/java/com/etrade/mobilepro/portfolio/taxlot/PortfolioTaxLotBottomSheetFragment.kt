package com.etrade.mobilepro.portfolio.taxlot

import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer.BottomSheetNavContainer
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import javax.inject.Inject

@RequireLogin
internal class PortfolioTaxLotBottomSheetFragment @Inject constructor(
    connectionNotifier: ConnectionNotifier,
    userSessionHandler: UserSessionHandler
) : BottomSheetNavContainer(connectionNotifier, userSessionHandler) {

    private val args: PortfolioTaxLotBottomSheetFragmentArgs by navArgs()

    override fun createNavHost(): NavHostFragment {
        return NavHostFragment.create(
            R.navigation.portfolio_tax_lot_graph,
            arguments
        )
    }

    override fun createDestinationChangeListener(): NavController.OnDestinationChangedListener {
        return NavController.OnDestinationChangedListener { _, destination, _ ->
            titleText = destination.label.toString()

            subTitleText = ""
        }
    }

    override fun onTitleChange(newTitle: String) {
        titleText = newTitle
    }
}
