package com.etrade.mobilepro.messagecenter.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "SecureMsgGetDetailsResponse", strict = false)
class SupportMessageDetailsResponseDto(
    @field:Element(name = "CommonDetails", required = false)
    var commonDetails: CommonDetailsDto? = null,
    @field:ElementList(name = "CommunicationList", required = false)
    var communicationList: List<CommunicationDto?>? = null
)

@Root(name = "CommonDetails", strict = false)
class CommonDetailsDto(
    @field:Element(name = "Userid", required = false)
    var userId: String? = null,
    @field:Element(name = "TopicId", required = false)
    var topicId: String? = null,
    @field:Element(name = "Count", required = false)
    var count: String? = null,
    @field:Element(name = "StiCasetype", required = false)
    var stiCasetype: String? = null,
    @field:Element(name = "ReplyFlag", required = false)
    var replyFlag: String? = null,
    @field:Element(name = "isEmailNotificationEnabled", required = false)
    var isEmailNotificationEnabled: String? = null,
    @field:Element(name = "EtPhone", required = false)
    var etPhone: String? = null,
    @field:Element(name = "LogId", required = false)
    var logId: String? = null,
    @field:Element(name = "CaseNo", required = false)
    var caseNo: String? = null,
    @field:Element(name = "EtFirstname", required = false)
    var etFirstname: String? = null,
    @field:Element(name = "Subject", required = false)
    var subject: String? = null,
    @field:Element(name = "Tier", required = false)
    var tier: String? = null,
    @field:Element(name = "AcctName", required = false)
    var accountName: String? = null,
    @field:Element(name = "AltEmailAddr", required = false)
    var alternativeEmail: String? = null,
    @field:Element(name = "EtEmail", required = false)
    var etEmail: String? = null,
    @field:Element(name = "FromName", required = false)
    var fromName: String? = null,
    @field:Element(name = "CreationTs", required = false)
    var creationTs: String? = null,
    @field:Element(name = "EtLastname", required = false)
    var etLastname: String? = null
)

@Root(name = "Communication", strict = false)
class CommunicationDto(
    @field:Element(name = "AcctName", required = false)
    var accountName: String? = null,
    @field:Element(name = "CaseNo", required = false)
    var caseNo: String? = null,
    @field:Element(name = "Subject", required = false)
    var subject: String? = null,
    @field:Element(name = "AcctNum", required = false)
    var accountId: String? = null,
    @field:Element(name = "Message", required = false)
    var message: String? = null,
    @field:Element(name = "FromName", required = false)
    var fromName: String? = null,
    @field:Element(name = "Topic", required = false)
    var topic: String? = null,
    @field:Element(name = "CreationTs", required = false)
    var creationTs: String? = null,
    @field:Element(name = "Direction", required = false)
    var direction: String? = null
)
