package com.etrade.mobilepro.documents

import org.threeten.bp.LocalDate

data class TaxDocument(
    val reportDate: LocalDate,
    val amended: Boolean,
    val year: String,
    override val accountUuid: String,
    override val institutionType: String,
    override val documentId: String,
    override val title: String,
    override val postEdgeFolder: String,
    override val headerTitle: String? = null,
    override val filename: String? = null
) : Document {
    override fun updateFilename(filename: String?): Document = this.copy(filename = filename)
}
