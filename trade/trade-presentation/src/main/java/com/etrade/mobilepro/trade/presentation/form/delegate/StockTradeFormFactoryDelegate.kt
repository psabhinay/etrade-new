package com.etrade.mobilepro.trade.presentation.form.delegate

import android.content.Context
import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.map
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.FormBuilder
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.HiddenInput
import com.etrade.mobilepro.dynamic.form.api.InputHolder
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.SwitchInput
import com.etrade.mobilepro.dynamic.form.api.form
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SecurityType.STOCK
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.delegate.TradeFormFactoryDelegate.OnFormChangeListener
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.AccountSectionGroup
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.SymbolInputParams
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.accountSectionGroup
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.collectStopOrLimitOrStopLimit
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.createStockSymbolExternalAction
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.extendParamsWithListener
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.liveForm
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.observeAdvancedOrderForConditionalOrderDisclosure
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.requiredStockFormInputs
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.requiredStockFormRules
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.resetAdvancedOrderTypeOnAdvancedSwitchTurnedOff
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.resetAdvancedOrderTypeOnPriceTypeChanges
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.resetPriceTypeIfDisclosureRejected
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.saveAndPreview
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.securityTypeAndSymbol
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.stockAndOptionsFooter
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.walkthroughRules
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.etrade.mobilepro.walkthrough.api.IdleState
import com.etrade.mobilepro.walkthrough.api.State

private val securityType = STOCK

internal open class StockTradeFormFactoryDelegate(
    private val context: Context,
    private val dependencies: FormFactoryDelegateDependencies,
    private val inputId: StockTradeFormInputId,
    private val searchIntentFactory: SearchIntentFactory,
    private val tradeFormTypeUpdateDelegate: TradeFormTypeUpdateDelegate<StockTradeFormInputId>
) : TradeFormFactoryDelegate {

    open val formType = TradeFormType(securityType)

    @Suppress("LongMethod")
    override fun createForm(
        accounts: List<TradeAccount>,
        accountDelegate: SelectedAccountDelegate,
        parameter: CreateTradeFormParameter,
        updateForm: OnFormChangeListener
    ): Form {
        val resources = context.resources
        return form {
            hiddenInput(this)

            accountSectionGroup(
                resources,
                inputId,
                AccountSectionGroup(
                    accounts,
                    accountDelegate.getCurrentAccountId(securityType), parameter.balanceViewModel
                )
            )

            verticalGroup {
                securityTypeAndSymbol(
                    resources,
                    securityType,
                    parameter.onActionClickListener,
                    parameter.walkthroughViewModel.state,
                    inputId.extractSymbolInfo(parameter.defaultValues)
                )
                requiredStockFormInputs(context, inputId, dependencies, parameter)
            }

            verticalGroup {
                advancedOrderSwitch(inputId, resources, parameter)
                advanceOrderType(inputId, resources)
                advancedOrderFormInputs(this, parameter)
                stockAndOptionsFooter(resources, parameter.onActionClickListener)
            }

            rules {
                requiredRules(updateForm, parameter, accountDelegate)
            }

            saveAndPreview(resources, parameter, dependencies.saveAndPreviewActionRequiredInputs + inputId.quantity)
        }
    }

    private fun RuleListBuilder.requiredRules(
        updateForm: OnFormChangeListener,
        parameter: CreateTradeFormParameter,
        accountDelegate: SelectedAccountDelegate
    ) {
        liveForm(
            inputId,
            securityType,
            updateForm,
            parameter.conditionalOrderDisclosureViewModel,
            accountDelegate
        )
        requiredStockFormRules(context.resources, parameter.onActionClickListener, inputId, parameter.quoteWidgetViewModel)
        observeAdvancedOrderForConditionalOrderDisclosure(inputId, parameter.conditionalOrderDisclosureViewModel)
        resetPriceTypeIfDisclosureRejected(inputId, parameter.conditionalOrderDisclosureViewModel)
        advancedOrderRules(this, updateForm)
        walkthroughRules(parameter, inputId, context.resources)
    }

    open fun advancedOrderRules(ruleBuilder: RuleListBuilder, updateForm: OnFormChangeListener) {
        ruleBuilder.apply {
            tradeFormTypeUpdateDelegate.onFormTypeUpdated(collectParams, formType, inputId, updateForm, ruleBuilder)
            resetAdvancedOrderTypeOnActionTypeChanges(inputId, inputId)
            resetAdvancedOrderTypeOnAdvancedSwitchTurnedOff(inputId)
            resetAdvancedOrderTypeOnPriceTypeChanges(inputId, inputId)
        }
    }

    open fun hiddenInput(formBuilder: FormBuilder) {
        inputId.apply {
            formBuilder.hidden(orderId)
            formBuilder.hidden(originalQuantity)
            formBuilder.hidden(positionLotId)
            formBuilder.hidden(walkthroughFormCompleted)
        }
    }

    open fun advancedOrderFormInputs(groupBuilder: GroupBuilder, parameter: CreateTradeFormParameter) = Unit

    private fun GroupBuilder.advancedOrderSwitch(inputId: StockTradeFormInputId, resources: Resources, parameter: CreateTradeFormParameter) {
        switch(inputId.advancedOrder) {
            title = resources.getString(R.string.trade_hint_advanced_order)
            enabledIf {
                advancedOrderSwitchAvailability(parameter)
            }
        }
    }

    @Suppress("LongMethod")
    private fun InputHolder.advancedOrderSwitchAvailability(
        parameter: CreateTradeFormParameter
    ): LiveData<Boolean> {
        return MediatorLiveData<Boolean>().apply {
            val onChanged = Observer<AdvancedOrderSwitchDependencies> { dependencies ->
                val isEnabled = isAdvancedOrderSwitchEnabled(inputId, dependencies, parameter)
                val walkthroughState = parameter.walkthroughViewModel.state.value
                if (!isEnabled) {
                    requireInput<SwitchInput>(inputId.advancedOrder).setValue(false)
                }
                value = isEnabled && (walkthroughState == null || walkthroughState is IdleState)
            }
            addSource(
                inputValue<DropDownOption?>(inputId.action).map {
                    val priceType = inputValue<DropDownOption?>(inputId.priceType).value?.id
                    val term = inputValue<DropDownOption?>(inputId.term).value?.id
                    AdvancedOrderSwitchDependencies(orderType = it?.id, priceType = priceType, term = term)
                },
                onChanged
            )
            addSource(
                inputValue<DropDownOption?>(inputId.priceType).map {
                    val orderType = inputValue<DropDownOption?>(inputId.action).value?.id
                    val term = inputValue<DropDownOption?>(inputId.term).value?.id
                    AdvancedOrderSwitchDependencies(orderType = orderType, priceType = it?.id, term = term)
                },
                onChanged
            )
            addSource(
                inputValue<DropDownOption?>(inputId.term).map {
                    val orderType = inputValue<DropDownOption?>(inputId.action).value?.id
                    val priceType = inputValue<DropDownOption?>(inputId.priceType).value?.id
                    AdvancedOrderSwitchDependencies(orderType = orderType, priceType = priceType, term = it?.id)
                },
                onChanged
            )
        }
    }

    private fun isAdvancedOrderSwitchEnabled(
        inputId: StockTradeFormInputId,
        dependencies: AdvancedOrderSwitchDependencies,
        parameter: CreateTradeFormParameter
    ): Boolean {
        val isSell = dependencies.orderType == inputId.actionValueId.sell
        val isBuyToCover = dependencies.orderType == inputId.actionValueId.buyToCover
        val isTrailingStopDollar = dependencies.priceType == inputId.priceTypeValueId.trailingStopDollar
        val isTrailingStopPercent = dependencies.priceType == inputId.priceTypeValueId.trailingStopPercent
        val isSellOrBuyToCoverOrNone = isSell || isBuyToCover || dependencies.orderType == null
        val isTrailingStop = isTrailingStopDollar || isTrailingStopPercent
        val isExtendedHours = dependencies.term == inputId.termValueId.extendedHoursDay ||
            dependencies.term == inputId.termValueId.extendedHoursImmediateOrCancel
        val isEdit = parameter.tradeOrderType.isEdit()
        return !isEdit && !isExtendedHours && !(isSellOrBuyToCoverOrNone && isTrailingStop)
    }

    private fun GroupBuilder.advanceOrderType(inputId: StockTradeFormInputId, resources: Resources) {
        select(inputId.advancedOrderType) {
            title = resources.getString(R.string.trade_hint_order_type)
            dynamic {
                rule {
                    MediatorLiveData<List<DropDownOption>>().apply {
                        val onChanged = Observer<DropDownOption?> {
                            value = orderTypeOptions(inputId, resources)
                        }
                        addSource(inputValue<DropDownOption?>(inputId.action), onChanged)
                        addSource(inputValue<DropDownOption?>(inputId.priceType), onChanged)
                    }
                }
            }
            visibleIf {
                inputValue<Boolean?>(inputId.advancedOrder).map {
                    it == true
                }
            }
        }
    }

    @Suppress("LongMethod")
    private fun GroupBuilder.securityTypeAndSymbol(
        resources: Resources,
        securityType: SecurityType,
        onActionClickListener: OnActionClickListener,
        walkthroughState: LiveData<State>,
        initialSymbol: WithSymbolInfo? = null
    ) {
        val params = extendParamsWithListener(
            resources = resources,
            prototype = SymbolInputParams(
                InstrumentType.validStocksTradingTypes,
                inputId.symbol,
                inputId.symbolCoder,
                onActionClickListener,
                isStockQuoteSearchOnly = true
            ),
            onActionClickListener = onActionClickListener,
            notEligibleSymbolDialogMessage = R.string.trade_error_invalid_stock_etf_symbol
        )
        val externalAction = createStockSymbolExternalAction(context, params, inputId, searchIntentFactory, onActionClickListener)
        securityTypeAndSymbol(
            context.resources,
            inputId,
            securityType,
            params,
            externalAction,
            walkthroughState,
            initialSymbol
        )
    }

    private fun InputHolder.orderTypeOptions(inputId: StockTradeFormInputId, resources: Resources): List<DropDownOption> {
        return mutableListOf<DropDownOption>().apply {
            val priceType = inputValue<DropDownOption?>(inputId.priceType).value?.id
            if (priceType != inputId.priceTypeValueId.trailingStopPercent && priceType != inputId.priceTypeValueId.trailingStopDollar) {
                add(
                    DropDownOption(
                        id = inputId.advancedOrderTypeValueId.contingent,
                        name = resources.getString(R.string.trade_advanced_order_type_contingent_title),
                        description = resources.getString(R.string.trade_advanced_order_type_contingent_description)
                    )
                )
            }
            val orderType = inputValue<DropDownOption?>(inputId.action).value?.id
            if (orderType == inputId.actionValueId.buy || orderType == inputId.actionValueId.sellShort) {
                add(
                    DropDownOption(
                        id = inputId.advancedOrderTypeValueId.stopLoss,
                        name = resources.getString(R.string.trade_advanced_order_type_stop_loss_title),
                        description = resources.getString(R.string.trade_advanced_order_type_stop_loss_description)
                    )
                )
            }
        }
    }

    private fun RuleListBuilder.resetAdvancedOrderTypeOnActionTypeChanges(
        actionInputId: WithActionTradeFormInputId,
        advancedInputId: WithAdvancedTradeFormInputId
    ) {
        rule {
            inputValue<DropDownOption?>(actionInputId.action).observeForever {
                val valueId = actionInputId.actionValueId
                if (it?.id == valueId.sell || it?.id == valueId.buyToCover) {
                    val advancedOrderTypeInput = getInput<SelectInput>(advancedInputId.advancedOrderType)
                    val isStopLossSelected = advancedOrderTypeInput?.value?.value?.id == advancedInputId.advancedOrderTypeValueId.stopLoss
                    if (isStopLossSelected) {
                        advancedOrderTypeInput?.resetValue()
                    }
                }
            }
        }
    }

    private val collectParams: CollectParams<StockTradeFormInputId> = { inputHolder, id ->
        symbol = inputHolder.inputValue<WithSymbolInfo>(id.symbol).value
        inputHolder.inputValue<DropDownOption>(id.action).value?.id?.let { action = id.actionValueId.converter.toValue(it) }
        quantity = inputHolder.inputValue<String>(id.quantity).value?.safeParseBigDecimal()
        collectStopOrLimitOrStopLimit(inputHolder, id.limitPrice, id.limitPriceForStopLimit, isLimit = true)
        collectStopOrLimitOrStopLimit(inputHolder, id.stopPrice, id.stopPriceForStopLimit)

        accountId = inputHolder.requireInput<SelectInput>(id.accountId).value.value?.id
        inputHolder.inputValue<DropDownOption>(id.priceType).value?.id?.let { priceType = id.priceTypeValueId.converter.toValue(it) }
        term = inputHolder.inputValue<DropDownOption>(id.term).value?.id?.let { id.termValueId.converter.toValue(it) }
        allOrNone = inputHolder.inputValue<Boolean>(id.allOrNone).value
        advancedOrder = inputHolder.inputValue<Boolean>(id.advancedOrder).value
        inputHolder.inputValue<DropDownOption>(id.advancedOrderType).value?.id?.let {
            advancedTradeType = id.advancedOrderTypeValueId.converter.toValue(it)
        }
        walkthroughFormCompleted = inputHolder.requireInput<HiddenInput>(id.walkthroughFormCompleted).value.value
    }

    private data class AdvancedOrderSwitchDependencies(
        val orderType: String?,
        val priceType: String?,
        val term: String?
    )
}
