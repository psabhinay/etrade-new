package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.trade.form.api.value.AdvancedOrderTypeValueId
import com.etrade.mobilepro.trade.form.api.value.ConditionalPriceTypeValueId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.trade.form.api.value.SecurityTypeValueId
import com.etrade.mobilepro.trade.form.api.value.StrategyTypeValueId
import com.etrade.mobilepro.util.MapConverter
import com.etrade.mobilepro.util.MutualConverter

internal fun ActionValueId.createConverter(): MutualConverter<String, TradeAction> {
    return MapConverter(
        mapOf(
            buy to TradeAction.BUY,
            buyToCover to TradeAction.BUY_TO_COVER,
            exchange to TradeAction.EXCHANGE,
            sell to TradeAction.SELL,
            sellShort to TradeAction.SELL_SHORT
        )
    )
}

internal fun AdvancedOrderOptionsActionValueId.createConverter(): MutualConverter<String, TradeAction> {
    return (this as ActionValueId).createConverter()
}

internal fun OrderTermValueId.createConverter(): MutualConverter<String, OrderTerm> {
    return MapConverter(
        mapOf(
            extendedHoursDay to OrderTerm.EXTENDED_HOUR_DAY,
            extendedHoursImmediateOrCancel to OrderTerm.EXTENDED_HOUR_IOC,
            fillOrKill to OrderTerm.FILL_OR_KILL,
            goodForDay to OrderTerm.GOOD_FOR_DAY,
            goodUntilCancel to OrderTerm.GOOD_UNTIL_CANCEL,
            immediateOrCancel to OrderTerm.IMMEDIATE_OR_CANCEL
        )
    )
}

internal fun PriceTypeValueId.createConverter(): MutualConverter<String, PriceType> {
    return MapConverter(
        mapOf(
            even to PriceType.EVEN,
            limit to PriceType.LIMIT,
            market to PriceType.MARKET,
            netCredit to PriceType.NET_CREDIT,
            netDebit to PriceType.NET_DEBIT,
            stop to PriceType.STOP,
            stopLimit to PriceType.STOP_LIMIT,
            trailingStopDollar to PriceType.TRAILING_STOP_DOLLAR,
            trailingStopPercent to PriceType.TRAILING_STOP_PERCENT,
        ).let {
            if (this is ConditionalPriceTypeValueId) {
                val updated = it.toMutableMap()
                updated[hiddenStop] = PriceType.HIDDEN_STOP
                return@let updated.toMap()
            }
            it
        }
    )
}

internal fun SecurityTypeValueId.createConverter(): MutualConverter<String, SecurityType> {
    return MapConverter(
        mapOf(
            conditional to SecurityType.CONDITIONAL,
            mutualFund to SecurityType.MUTUAL_FUND,
            option to SecurityType.OPTION,
            stock to SecurityType.STOCK,
            stockAndOptions to SecurityType.STOCK_AND_OPTIONS
        )
    )
}

internal fun StrategyTypeValueId.createConverter(): MutualConverter<String, StrategyType> {
    return MapConverter(
        mapOf(
            butterfly to StrategyType.BUTTERFLY,
            buyWrite to StrategyType.BUY_WRITE,
            calendar to StrategyType.CALENDAR,
            call to StrategyType.CALL,
            callSpread to StrategyType.CALL_SPREAD,
            collars to StrategyType.COLLARS,
            combo to StrategyType.COMBO,
            condor to StrategyType.CONDOR,
            custom to StrategyType.CUSTOM,
            diagonal to StrategyType.DIAGONAL,
            ironButterfly to StrategyType.IRON_BUTTERFLY,
            ironCondor to StrategyType.IRON_CONDOR,
            marriedPut to StrategyType.MARRIED_PUT,
            put to StrategyType.PUT,
            putSpread to StrategyType.PUT_SPREAD,
            stock to StrategyType.STOCK,
            straddle to StrategyType.STRADDLE,
            strangle to StrategyType.STRANGLE
        )
    )
}

internal fun AdvancedOrderTypeValueId.createConverter(): MutualConverter<String, AdvancedTradeType> {
    return MapConverter(
        mapOf(
            contingent to AdvancedTradeType.Contingent,
            stopLoss to AdvancedTradeType.StopLoss
        )
    )
}
