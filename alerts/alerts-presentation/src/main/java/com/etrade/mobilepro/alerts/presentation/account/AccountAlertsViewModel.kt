package com.etrade.mobilepro.alerts.presentation.account

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.alerts.api.model.AlertGroup
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.alerts.presentation.base.AlertsDeleter
import com.etrade.mobilepro.alerts.presentation.base.AlertsHolder
import com.etrade.mobilepro.alerts.presentation.base.BaseAlertsViewModel
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.android.SingleLiveEvent
import javax.inject.Inject

class AccountAlertsViewModel @Inject constructor(
    alertsRepo: AlertsRepo,
    user: User,
    alertsHolder: AlertsHolder,
    alertsDeleter: AlertsDeleter
) : BaseAlertsViewModel(alertsRepo, user, alertsHolder, alertsDeleter) {
    private val _viewState = MutableLiveData<ViewState>()
    private val _viewEffects = SingleLiveEvent<ViewEffects>()
    val viewEffects: LiveData<ViewEffects> = _viewEffects
    val viewState: LiveData<ViewState> = _viewState
    override val alertGroup: AlertGroup = AlertGroup.ACCOUNT
    override val headerItems: List<AlertItem> = emptyList()

    override fun onLoadingStart() {
        _viewState.postValue(ViewState.Loading)
    }

    override fun onAlertsChanged(alerts: List<AlertItem>) {
        _viewState.postValue(ViewState.Success(alerts, alerts.isEmpty())) // we specifically need to enqueue
    }

    override fun onLoadingFailed(it: Throwable, clearLoadedItems: Boolean) {
        _viewEffects.value = ViewEffects.LoadingFailed
        _viewState.postValue(ViewState.Error.LoadingError(it.message, clearLoadedItems))
    }

    override fun onDeletionFailed(it: Throwable, alert: AlertItem.Alert, actualAlerts: List<AlertItem>) {
        _viewEffects.value = ViewEffects.AlertDeleteFailed
        _viewState.postValue(ViewState.Error.DeletionError(it.message, alert, actualAlerts))
    }

    override fun onDeletionSuccess(alert: AlertItem.Alert) {
        _viewEffects.value = ViewEffects.AlertsWereDeleted
    }

    override fun hasErrorState(): Boolean = _viewState.value is ViewState.Error

    sealed class ViewState {
        object Loading : ViewState()

        data class Success(
            val alerts: List<AlertItem>,
            val showNoAlertsWarning: Boolean
        ) : ViewState()

        sealed class Error : ViewState() {
            data class LoadingError(
                val msg: String?,
                val wasIntendedToClearItems: Boolean
            ) : Error()

            data class DeletionError(
                val msg: String?,
                val alert: AlertItem.Alert,
                val alerts: List<AlertItem>
            ) : Error()
        }
    }
}
