package com.etrade.mobilepro.dagger

import com.etrade.eo.streaming.SubscriptionManager
import com.etrade.mobilepro.appconfig.api.LightStreamerSubscriptionManagerHolder
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.appconfig.api.repo.datasource.BaseUrlsDataSource
import com.etrade.mobilepro.appconfig.data.repo.DefaultAppConfigRepo
import com.etrade.mobilepro.appconfig.data.repo.datasource.DefaultBaseUrlsDataSource
import com.etrade.mobilepro.appconfig.data.repo.datasource.combiner.BaseUrlSetsCombiner
import com.etrade.mobilepro.appconfig.data.repo.datasource.combiner.DefaultBaseUrlsSetsCombiner
import com.etrade.mobilepro.common.di.EnvironmentName
import com.etrade.mobilepro.common.di.Level1Streamer
import com.etrade.mobilepro.common.di.Level2Streamer
import com.etrade.mobilepro.preferences.ApplicationPreferences
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class AppConfigModule {
    @Binds
    abstract fun bindBaseUrlsDataSource(dataSource: DefaultBaseUrlsDataSource): BaseUrlsDataSource

    @Binds
    abstract fun bindBaseUrlSetsCombiner(combiner: DefaultBaseUrlsSetsCombiner): BaseUrlSetsCombiner

    companion object {
        @Provides
        @Singleton
        fun provideAppConfigRepo(
            baseUrlsDataSource: BaseUrlsDataSource,
            @EnvironmentName defaultEnvironment: String,
            applicationPreferences: ApplicationPreferences,
            lsSubscriptionManagerHolder: LightStreamerSubscriptionManagerHolder
        ): AppConfigRepo = DefaultAppConfigRepo(
            baseUrlsDataSource = baseUrlsDataSource,
            defaultEnvironment = defaultEnvironment,
            appPreferences = applicationPreferences,
            lsSubscriptionManagerHolder = lsSubscriptionManagerHolder
        )

        @Provides
        @Singleton
        fun provideLightStreamerSubscriptionManagerHolder(
            @Level1Streamer level1SubscriptionManager: SubscriptionManager,
            @Level2Streamer level2SubscriptionManager: SubscriptionManager
        ): LightStreamerSubscriptionManagerHolder = LightStreamerSubscriptionManagerHolder(
            level1SubscriptionManager = level1SubscriptionManager,
            level2SubscriptionManager = level2SubscriptionManager
        )
    }
}
