package com.etrade.mobilepro.streaming.base

import com.etrade.eo.streaming.SubscriptionManager
import com.etrade.eo.streaming.SubscriptionMode
import com.etrade.eo.streaming.UpdateInfo
import com.etrade.eo.streaming.rx.subscribe
import io.reactivex.Observable
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.Collections

/**
 * Default implementation of [SubscriptionManagerHelper] that accepts an arbitrary [subscriptionManager] to work with and [updateInfoParser] to convert
 * [UpdateInfo] into a client's object.
 *
 * @param subscriptionManager The subscription manager.
 * @param lsDataAdapter the data adapter that light streamer client subscribe to
 * @param updateInfoParser Converts [UpdateInfo] object to a client's object.
 */
class DefaultSubscriptionManagerHelper<T : Any>(
    private val subscriptionManager: SubscriptionManager,
    private val lsDataAdapter: String? = null,
    private val useSnapshot: Boolean = true,
    private val updateInfoParser: (UpdateInfo) -> T?
) : SubscriptionManagerHelper<T> {

    private val streams: MutableMap<String, Stream<T>> = Collections.synchronizedMap(mutableMapOf())

    private val logger: Logger = LoggerFactory.getLogger(DefaultSubscriptionManagerHelper::class.java)

    @Synchronized
    override fun subscribeTo(streamId: String, fields: Set<String>, mode: SubscriptionMode): Observable<T> {
        val key = createKey(streamId, mode)
        logger.debug("Subscribing to $key...")
        val existingStream = streams[key]
        return when {
            existingStream == null -> createStream(key, streamId, fields, mode).also { logger.debug("New subscription is created for stream: $key") }
            existingStream.fields.containsAll(fields) -> existingStream.subscription.observable.also { logger.debug("Using existing subscription: $key") }
            else -> updateStream(existingStream, streamId, fields, mode).also { logger.debug("Updating existing subscription: $key") }
        }
    }

    private fun createStream(
        key: String,
        streamId: String,
        fields: Set<String>,
        mode: SubscriptionMode
    ): Observable<T> {
        return Subscription<T> {
            streams.remove(key)
            logger.debug("Subscription is disposed: $key")
        }.apply {
            source = subscriptionManager.subscribe(
                id = streamId,
                parser = updateInfoParser,
                useSnapshot = useSnapshot,
                subscriptionFields = fields,
                subscriptionMode = mode,
                lsDataAdapter = lsDataAdapter
            )
        }.also {
            streams[key] = Stream(fields, it)
        }.observable
    }

    private fun updateStream(
        stream: Stream<T>,
        streamId: String,
        fields: Set<String>,
        mode: SubscriptionMode
    ): Observable<T> {
        return stream.subscription.apply {
            source = subscriptionManager.subscribe(
                id = streamId,
                parser = updateInfoParser,
                useSnapshot = useSnapshot,
                subscriptionFields = stream.fields + fields,
                subscriptionMode = mode,
                lsDataAdapter = lsDataAdapter
            )
        }.observable
    }

    private fun createKey(streamId: String, mode: SubscriptionMode): String = "$streamId/$mode"

    private class Stream<T : Any>(
        val fields: Set<String>,
        val subscription: Subscription<T>
    )
}
