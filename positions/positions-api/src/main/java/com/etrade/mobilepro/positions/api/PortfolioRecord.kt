package com.etrade.mobilepro.positions.api

import com.etrade.mobilepro.instrument.Instrument

interface PortfolioRecord {
    val recordId: String
    val displayName: String
    val isTaxLot: Boolean
    val position: Position
    val instrument: Instrument
}
