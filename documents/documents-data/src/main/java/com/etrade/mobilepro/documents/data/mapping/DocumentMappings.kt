package com.etrade.mobilepro.documents.data.mapping

import com.etrade.mobilepro.documents.Document
import com.etrade.mobilepro.documents.data.rest.DocumentRequestDto
import com.etrade.mobilepro.documents.data.rest.DocumentRequestHolderDto

internal fun Document.toRequest(appVersion: String): DocumentRequestDto = DocumentRequestDto(
    holderDto = DocumentRequestHolderDto(
        accountUuid = this.accountUuid,
        instType = this.institutionType,
        documentId = this.documentId,
        postEdgeFolder = this.postEdgeFolder,
        appVersion = appVersion
    )
)
