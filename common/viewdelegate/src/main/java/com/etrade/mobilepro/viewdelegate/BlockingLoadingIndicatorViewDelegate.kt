package com.etrade.mobilepro.viewdelegate

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.progressoverlay.ProgressDialogFragment
import com.etrade.mobilepro.viewmodel.BlockingLoadingIndicatorViewModel

class BlockingLoadingIndicatorViewDelegate(private val viewModel: BlockingLoadingIndicatorViewModel, private val manager: FragmentManager) : ViewDelegate {

    override fun observe(owner: LifecycleOwner) {
        viewModel.blockingLoadingIndicator.observe(owner, Observer { onLoadingIndicator(it) })
    }

    private fun onLoadingIndicator(loading: Boolean) {
        if (loading) {
            ProgressDialogFragment.show(manager, false)
        } else {
            ProgressDialogFragment.dismiss(manager)
        }
    }
}
