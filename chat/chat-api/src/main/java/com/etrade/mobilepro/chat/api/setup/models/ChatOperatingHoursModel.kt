package com.etrade.mobilepro.chat.api.setup.models

data class GetChatOperatingHoursModel(val data: ChatOperatingHoursDataModel? = null)

data class ChatOperatingHoursDataModel(val isHoliday: Boolean = false, val inOperatingHours: Boolean = false)
