package com.etrade.mobilepro.trade.presentation.form.component.builder

import android.content.res.Resources
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.trade.presentation.form.component.StrategyTypeInput

internal fun GroupBuilder.strategyType(id: String, resources: Resources) {
    custom { StrategyTypeInput(id, resources) }
}
