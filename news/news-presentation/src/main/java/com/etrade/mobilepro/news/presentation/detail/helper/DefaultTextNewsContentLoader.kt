package com.etrade.mobilepro.news.presentation.detail.helper

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.createEmptyTextNewsItem
import com.etrade.mobilepro.news.api.repo.NewsRepo
import com.etrade.mobilepro.util.NetworkState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import org.slf4j.LoggerFactory
import javax.inject.Inject

class DefaultTextNewsContentLoader @Inject constructor(private val newsRepo: NewsRepo) :
    TextNewsContentLoader {

    private val logger = LoggerFactory.getLogger(DefaultTextNewsContentLoader::class.java)
    private val _networkState = MutableLiveData<NetworkState>()
    private val _content = MutableLiveData<NewsItem.Text>()
    private val compositeDisposable = CompositeDisposable()
    private var contentId: String? = null

    override val networkState: LiveData<NetworkState> = _networkState
    override val content: LiveData<NewsItem.Text> = _content

    override fun load(contentId: String) {
        if (contentId.isNotBlank() && this.contentId != contentId) {
            this.contentId = contentId
            _networkState.value = NetworkState.Loading
            _content.value = createEmptyTextNewsItem()
            compositeDisposable.add(
                newsRepo
                    .textContent(contentId)
                    .subscribeBy(
                        onNext = {
                            _networkState.postValue(NetworkState.Success)
                            _content.postValue(it)
                        },
                        onError = {
                            _networkState.postValue(NetworkState.Failed(it))
                            logger.debug(it.message)
                        }
                    )
            )
        }
    }

    override fun load(item: NewsItem.Text) {
        if (_content.value == item) { return }
        contentId = item.docId
        _networkState.postValue(NetworkState.Success)
        _content.postValue(item)
    }

    override fun loadEmpty() {
        load(createEmptyTextNewsItem())
    }

    override fun retry() {
        contentId?.let {
            load(it)
        }
    }

    override fun unsubscribe() {
        compositeDisposable.clear()
    }
}
