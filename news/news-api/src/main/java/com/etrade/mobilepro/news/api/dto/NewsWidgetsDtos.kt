package com.etrade.mobilepro.news.api.dto

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class NewsWidgetPortfolioDto(
    @Json(name = "data")
    override val data: Any?,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?
) : BaseScreenView()

@JsonClass(generateAdapter = true)
class NewsWidgetBriefingDto(
    @Json(name = "data")
    override val data: Any?,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?
) : BaseScreenView()

@JsonClass(generateAdapter = true)
class NewsWidgetTodayDto(
    @Json(name = "data")
    override val data: Any?,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?
) : BaseScreenView()
