package com.etrade.mobilepro.tableviewutils.presentation.sorting

import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.evrencoskun.tableview.sort.SortState

class TableDataSortingHandler<T : Cell> {

    fun sortTableData(
        cellItems: List<List<T>>,
        rowHeaders: List<SymbolCell>,
        sortInfo: SortStateInfo?
    ): Pair<List<SymbolCell>, List<List<T>>> {
        if (sortInfo == null) {
            return rowHeaders to cellItems
        }
        val sortedColumnPosition = sortInfo.sortedColumnPosition
        val comparator =
            if (sortedColumnPosition == null) { // Sorting by rowHeader column is applied.
                createComparator(sortInfo.sortState) { row ->
                    row.first.content as? Comparable<*>
                }
            } else {
                createComparator(sortInfo.sortState) { row ->
                    row.second.getOrNull(sortedColumnPosition)?.content as? Comparable<*>
                }
            }

        return (rowHeaders zip cellItems)
            .sortedWith(comparator)
            .unzip()
    }

    fun resolveSortState(currentSortState: SortStateInfo?, column: Int?): SortStateInfo {
        val sortState = if (column == currentSortState?.sortedColumnPosition) {
            when (currentSortState?.sortState) {
                SortState.ASCENDING -> SortState.DESCENDING
                else -> SortState.ASCENDING
            }
        } else {
            SortState.ASCENDING
        }

        return SortStateInfo(sortState, column)
    }

    private fun createComparator(
        sortOrder: SortState,
        getCellComparable: (Pair<SymbolCell, List<Cell>>) -> Comparable<*>?
    ): Comparator<Pair<SymbolCell, List<Cell>>> {
        return when (sortOrder) {
            SortState.ASCENDING -> Comparator { row1, row2 ->
                compareValues(
                    getCellComparable(row1),
                    getCellComparable(row2)
                )
            }
            SortState.DESCENDING -> Comparator { row1, row2 ->
                compareValues(
                    getCellComparable(row2),
                    getCellComparable(row1)
                )
            }
            SortState.UNSORTED -> Comparator { _, _ -> 0 }
        }
    }
}
