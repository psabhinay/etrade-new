package com.etrade.mobilepro.chart.netassets

import androidx.annotation.ColorRes
import com.etrade.mobilepro.barchartview.DataEntry
import com.etrade.mobilepro.chart.api.ChartPoint
import com.etrade.mobilepro.chart.netassets.data.NetAssetsChartRepo
import com.etrade.mobilepro.common.di.PrimaryColor
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.UseCase
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject
import kotlin.math.floor

interface GetNetAssetsChartDataEntriesUseCase : UseCase<GetNetAssetsChartDataEntriesParameter, Resource<List<DataEntry>>>

internal const val NUMBER_OF_MONTHS = 12

private const val NUMBER_OF_POINTS = 6

class GetNetAssetsChartDataEntriesUseCaseImpl @Inject constructor(
    private val netAssetsChartRepo: NetAssetsChartRepo,
    @ColorRes @PrimaryColor private val barColor: Int
) : GetNetAssetsChartDataEntriesUseCase {

    override suspend fun execute(parameter: GetNetAssetsChartDataEntriesParameter): Resource<List<DataEntry>> {
        val resource = netAssetsChartRepo.getNetAssetsChart(listOfNotNull(parameter.accountId))
        if (resource is Resource.Failed) {
            return Resource.Failed(error = resource.error)
        }

        val resourceData = resource.data
        if (resourceData == null || resourceData.accounts.isEmpty()) {
            return Resource.Failed(error = RuntimeException("No chart line data"))
        }

        return resource.map { data ->
            data?.accounts
                ?.firstOrNull()
                ?.points
                ?.takeLast(NUMBER_OF_POINTS)
                ?.sortedBy { point -> point.rawX }
                ?.mapIndexed { index, point ->
                    point.toDataEntry(index, barColor)
                }
        }
    }

    private fun ChartPoint.toDataEntry(index: Int, @ColorRes color: Int): DataEntry {
        val date = LocalDate.parse(rawX, DateTimeFormatter.BASIC_ISO_DATE)
        val month = date.month.value
        val order = (floor((index - month).toFloat() / NUMBER_OF_MONTHS) + 1) * NUMBER_OF_MONTHS
        return DataEntryImpl((month + order), rawY.toFloat(), color)
    }
}

class GetNetAssetsChartDataEntriesParameter(
    val accountId: String?
)

private class DataEntryImpl(
    override val x: Float,
    override val y: Float,
    override val barColor: Int
) : DataEntry {
    override val valueColor: Int = barColor
    override val valueDescriptionColor: Int? = barColor
}
