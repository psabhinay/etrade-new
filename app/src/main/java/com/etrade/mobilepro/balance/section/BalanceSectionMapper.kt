package com.etrade.mobilepro.balance.section

import com.etrade.mobilepro.balance.data.dto.BalanceDto
import com.etrade.mobilepro.balance.section.viewfactory.BalancesSectionViewFactory
import com.etrade.mobilepro.balance.section.viewfactory.CashAccountBalanceSectionViewFactory
import com.etrade.mobilepro.balance.section.viewfactory.DayTradingAccountBalanceSectionViewFactory
import com.etrade.mobilepro.balance.section.viewfactory.MarginAccountBalanceSectionViewFactory
import com.etrade.mobilepro.balance.section.viewfactory.PortfolioMarginAccountBalanceSectionViewFactory
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

/**
 * Maps the balance details to list of section items
 *
 * supported Account types:
 * 1. Cash,
 * 2. Regular margin,
 * 3. Day trading,
 * 4. Portfolio Margin
 */

class BalanceSectionMapper(private val resources: BalancesLabelResources) {

    private val balancesViewMap = mutableMapOf<BALANCEACCOUNTTYPE, BalancesSectionViewFactory>()

    init {
        balancesViewMap[BALANCEACCOUNTTYPE.CASH] = CashAccountBalanceSectionViewFactory()
        balancesViewMap[BALANCEACCOUNTTYPE.MARGIN_REGULAR] = MarginAccountBalanceSectionViewFactory()
        balancesViewMap[BALANCEACCOUNTTYPE.MARGIN_DAY_TRADING] = DayTradingAccountBalanceSectionViewFactory()
        balancesViewMap[BALANCEACCOUNTTYPE.MARGIN_PORTFOLIO] = PortfolioMarginAccountBalanceSectionViewFactory()
    }

    fun map(balanceDto: BalanceDto, actionClickListener: (BalancesActionType) -> Unit): List<TableSectionItemLayout> {
        val viewFactory = balancesViewMap[getAccountType(balanceDto)]
        return balanceDto.balanceRealTime?.let {
            viewFactory?.createSectionView(it, resources, actionClickListener)
        } ?: throw InvalidSectionTypeException("Account type $balanceDto.balancesAccountType contains no data")
    }

    private fun getAccountType(balanceDto: BalanceDto): BALANCEACCOUNTTYPE {
        return when (balanceDto.balancesAccountType) {
            "CASH" -> {
                BALANCEACCOUNTTYPE.CASH
            }
            "MARGIN" -> {
                val isDayTradingAccount = balanceDto.balanceRealTime?.dtStatus?.isNotBlank()
                isDayTradingAccount?.let {
                    BALANCEACCOUNTTYPE.MARGIN_DAY_TRADING
                } ?: BALANCEACCOUNTTYPE.MARGIN_REGULAR
            }
            "PMMARGIN" -> BALANCEACCOUNTTYPE.MARGIN_PORTFOLIO
            else -> throw InvalidSectionTypeException("Account type $balanceDto.balancesAccountType not supported")
        }
    }
}

private class InvalidSectionTypeException(message: String) : Exception(message)

private enum class BALANCEACCOUNTTYPE {
    CASH,
    MARGIN_REGULAR,
    MARGIN_DAY_TRADING,
    MARGIN_PORTFOLIO,
}

data class BalancesLabelResources(
    val header: BalanceHeaderLabels,
    val cash: CashAccountLabels,
    val margin: MarginAccountLabels,
    val defaultResources: BalancesResources
)

data class BalanceHeaderLabels(
    val netAccountVal: String,
    val cashCall: String,
    val marginCall: String,
    val securities: String,
    val purchasingPower: String,
    val fundsWithHeld: String,
    val fundsWithdrawal: String,
    val cashBalance: String,
    val fundsHold: String,
    val cashMarginBalance: String,
    val dayTrading: String,
    val dayTradingPurchasingPower: String
)

data class CashAccountLabels(
    val cashTitle: String,
    val totalMarketValue: String,
    val availableInvestment: String,
    val availableWithdrawal: String,
    val netBalance: String,
    val call: String,
    val securities: String,
    val settledInvestment: String,
    val unsettledInvestment: String,
    val sweepDeposit: String,
    val unInvestedCash: String,
    val holdingNetBalance: String,
    val purchasingPowerFundsWithheld: String,
    val purchasingPowerFundsWithdrawal: String
)

data class MarginAccountLabels(
    val marginTitle: String,
    val marginableSecurities: String,
    val nonMarginableSecuritiesOrOptions: String,
    val maxAvailableWithdrawal: String,
    val netCashMarginbalance: String,
    val fedCall: String,
    val houseCall: String,
    val minEquityCall: String,
    val call: String,
    val securities: String,
    val shortSecurities: String,
    val equity: String,
    val equityPercentage: String,
    val balance: String,
    val netMarginBalance: String,
    val shortReserve: String,

    // dayTrade properties
    val dayTradingPurchasingPower: String,
    val dayTradeStatus: String,
    val dayTradeFundsWithheldCash: String,
    val dayTradeFundsWithheldMargin: String,
    val dayTradesForLast5Days: String,
    val dayTradesAllowUntilPatternDayTradingQualification: String
)

data class BalancesResources(
    val currencySign: String,
    val emptyPriceDefault: String,
    val percentageSign: String,
    val marginCallTextColor: Int,
    val infoIcon: Int,
    val infoIconDescription: String
)
