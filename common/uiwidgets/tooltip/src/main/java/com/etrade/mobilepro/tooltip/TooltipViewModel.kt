package com.etrade.mobilepro.tooltip

import android.view.MotionEvent
import androidx.lifecycle.ViewModel
import javax.inject.Inject

open class TooltipViewModel @Inject constructor() : ViewModel() {

    private val tooltips: MutableList<Tooltip> = mutableListOf()

    fun show(block: Tooltip.Builder.() -> Unit): Tooltip {
        return showTooltip(block).also { tooltips.add(it) }
    }

    fun dismiss(tooltip: Tooltip?) {
        tooltip?.dismiss()
        tooltips.remove(tooltip)
    }

    fun dispatchTouchEvent(ev: MotionEvent?): Boolean? {
        return ev?.let { event -> tooltips.any { it.containsTooltip(event.rawX, event.rawY) }.takeIf { it } }
    }
}
