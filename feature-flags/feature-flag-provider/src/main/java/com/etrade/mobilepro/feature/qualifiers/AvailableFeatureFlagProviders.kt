package com.etrade.mobilepro.feature.qualifiers

import com.etrade.mobilepro.feature.FeatureFlagProvider
import javax.inject.Qualifier

/**
 * Dependency qualifier for a [List] of [FeatureFlagProvider]s that will be used to resolve
 * feature flags.
 */
@Qualifier
annotation class AvailableFeatureFlagProviders
