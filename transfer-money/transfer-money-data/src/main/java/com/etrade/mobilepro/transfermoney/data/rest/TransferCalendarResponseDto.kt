package com.etrade.mobilepro.transfermoney.data.rest

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class TransferCalendarResponseDto(
    @Json(name = "TransferCalendar")
    val calendar: TransferCalendar
) : BaseDataDto() {

    @JsonClass(generateAdapter = true)
    class TransferCalendar(
        @Json(name = "Holidays")
        val holidays: List<Holiday>
    ) {

        @JsonClass(generateAdapter = true)
        data class Holiday(
            @Json(name = "fromInstType")
            val fromInstType: String,
            @Json(name = "toInstType")
            val toInstType: String,
            @Json(name = "holidays")
            val holidays: List<String>
        )
    }
}
