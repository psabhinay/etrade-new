package com.etrade.mobilepro.dynamic.form.ui

import android.content.Context
import android.util.AttributeSet
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.api.ExternalInput
import com.etrade.mobilepro.valuepicker.ValuePickerWidget

internal class ExternalInputView : ValuePickerWidget, ComponentView<ExternalInput<*>>, WithInputId {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val inputViewDelegate: InputViewDelegate = InputViewDelegate(this, ::setTitle)

    private val displayValueObserver: Observer<CharSequence?> = Observer { value = it }

    private var input: ExternalInput<*>? = null
    override val inputId: String?
        get() { return input?.id }

    init {
        setOnClickListener {
            input?.action?.invoke()
        }
    }

    override fun render(component: ExternalInput<*>, owner: LifecycleOwner) {
        inputViewDelegate.render(component, owner)

        input?.displayValue?.removeObserver(displayValueObserver)

        input = component
        setupView(component)

        component.displayValue.observe(owner, displayValueObserver)
    }

    private fun setTitle(title: CharSequence?) {
        hint = title
    }

    /**
     * ETAND-20061 was done to provide a workaround fix for bug introduced with PR:
     * https://github.com/material-components/material-components-android/pull/2025
     *
     * There is currently an OPEN ticket to fix the issue in the library:
     * https://github.com/material-components/material-components-android/issues/2266
     *
     **/
    private fun setupView(component: ExternalInput<*>) {
        if (component.showClearAction) {
            enableClearAction {
                input?.clearValue()
            }
        } else {
            disableClearAction()
        }
    }
}
