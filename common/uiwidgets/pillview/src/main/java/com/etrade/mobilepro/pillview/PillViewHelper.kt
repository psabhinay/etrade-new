package com.etrade.mobilepro.pillview

import android.os.CountDownTimer
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

interface PillViewHelper {
    fun show(text: CharSequence?, hasLoadingBar: Boolean = false, autoHide: Boolean = true)
    fun hide()
    fun pillState(): LiveData<PillView.State>
}

private const val PILL_AUTOHIDE_INTERVAL_MS = 3000L
private const val PILL_LOADING_DEBOUNCE_INTERVAL_MS = 1000L

@MainThread
open class DefaultPillViewHelper : PillViewHelper {
    private val _pillState = MutableLiveData<PillView.State>().apply { value = PillView.State.Hidden }
    private var currentTimeout: CountDownTimer? = null
    private var loadingDebounceTimer: CountDownTimer? = null

    override fun show(text: CharSequence?, hasLoadingBar: Boolean, autoHide: Boolean) {
        loadingDebounceTimer?.cancel()
        currentTimeout?.cancel()
        if (autoHide) {
            currentTimeout = object : CountDownTimer(
                PILL_AUTOHIDE_INTERVAL_MS,
                PILL_AUTOHIDE_INTERVAL_MS
            ) {
                override fun onFinish() {
                    _pillState.postValue(PillView.State.Hidden)
                }

                override fun onTick(millisUntilFinished: Long) {
                    // nothing to do
                }
            }
            currentTimeout?.start()
        }
        if (hasLoadingBar) {
            loadingDebounceTimer = object : CountDownTimer(
                PILL_LOADING_DEBOUNCE_INTERVAL_MS,
                PILL_LOADING_DEBOUNCE_INTERVAL_MS
            ) {
                override fun onFinish() {
                    _pillState.postValue(PillView.State.Loading(text = text))
                }

                override fun onTick(millisUntilFinished: Long) {
                    // nothing to do
                }
            }
            loadingDebounceTimer?.start()
        } else {
            _pillState.value = PillView.State.Shown(text = text)
        }
    }

    override fun hide() {
        loadingDebounceTimer?.cancel()
        currentTimeout?.cancel()
        _pillState.value = PillView.State.Hidden
    }

    override fun pillState(): LiveData<PillView.State> = _pillState
}
