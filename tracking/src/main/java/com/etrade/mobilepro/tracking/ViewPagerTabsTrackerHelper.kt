package com.etrade.mobilepro.tracking

interface ViewPagerTabsTrackerHelper {

    var lastTabPosition: Int

    fun trackTab(position: Int, tabTitle: String)
}
