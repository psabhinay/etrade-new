package com.etrade.mobilepro.dynamic.form.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable

interface ClickRequester {
    val clickRequestSignal: LiveData<ConsumableLiveEvent<Unit>>

    fun requestClick()
}

class ClickRequesterImpl : ClickRequester {
    private val _clickRequestSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()
    override val clickRequestSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _clickRequestSignal

    override fun requestClick() {
        _clickRequestSignal.postValue(Unit.consumable())
    }
}
