package com.etrade.mobilepro.compose.etdialogs.edit

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import com.etrade.mobilepro.compose.etdialogs.elements.DialogButton
import javax.inject.Inject

interface TextValueEditor {
    val state: ViewState
    fun update(state: ViewState)
    fun initWith(settings: Settings, makeVisible: Boolean = false)

    data class ViewState(
        val status: Status = Status.Gone,
        val value: TextFieldValue = TextFieldValue(""),
        val errorMsg: AnnotatedString? = null,
        val settings: Settings = Settings(),
    ) {
        enum class Status { Gone, Input } // Loading, Error(with retry action bundled) are potential candidates as well, also consider sealed class
    }

    data class Settings(
        val title: String = "",
        val hint: String = "",
        val rules: Rules = Rules(),
        val onUpdateValue: (TextFieldValue) -> Unit = { },
        val onDone: () -> Unit = { },
        val onDismiss: () -> Unit = { },
        val buttonRow: List<DialogButton<ViewState>> = emptyList()
    ) {
        data class Rules(
            val excludedChars: Set<Char> = emptySet(),
            val inputLength: Int = Int.MAX_VALUE
        )
    }

    companion object
}

fun TextValueEditor.Companion.createWith(settings: TextValueEditor.Settings): TextValueEditor = DefaultTextValueEditor(settings)

@Suppress("LongParameterList")
fun TextValueEditor.Companion.create(
    title: String,
    hint: String = "",
    rules: TextValueEditor.Settings.Rules = TextValueEditor.Settings.Rules(),
    onDone: (String) -> Unit,
    onDismiss: () -> Unit = { },
    validator: (input: String) -> AnnotatedString? = { null },
    buttonRowBuilder: MutableList<DialogButton<TextValueEditor.ViewState>>.(editor: TextValueEditor, onDone: () -> Unit, onDismiss: () -> Unit) -> Unit
): TextValueEditor {
    val impl = DefaultTextValueEditor()
    val onUpdateValue: (TextFieldValue) -> Unit = { value ->
        if (value.text.none { rules.excludedChars.contains(it) }) {
            val newValue = if (value.text.length > rules.inputLength) {
                value.text.substring(0..value.text.length - 2)
            } else {
                value.text
            }
            impl.update(
                state = impl.state.copy(
                    value = TextFieldValue(
                        text = newValue,
                        selection = TextRange(newValue.length, newValue.length)
                    ),
                    errorMsg = validator(value.text)
                )
            )
        }
    }
    val onDoneWrapped: () -> Unit = {
        impl.update(state = impl.state.copy(status = TextValueEditor.ViewState.Status.Gone))
        onDone(impl.state.value.text.take(rules.inputLength))
    }
    val onDismissWrapped: () -> Unit = {
        impl.update(state = impl.state.copy(status = TextValueEditor.ViewState.Status.Gone))
        onDismiss()
    }
    val buttonRow = mutableListOf<DialogButton<TextValueEditor.ViewState>>()
    buttonRowBuilder(buttonRow, impl, onDoneWrapped, onDismissWrapped)
    impl.initWith(
        settings = TextValueEditor.Settings(
            title = title,
            hint = hint,
            onUpdateValue = onUpdateValue,
            onDone = onDoneWrapped,
            onDismiss = onDismissWrapped,
            buttonRow = buttonRow
        )
    )
    return impl
}

fun TextValueEditor.show(value: String = "") {
    update(state = state.copy(status = TextValueEditor.ViewState.Status.Input, value = TextFieldValue(value)))
}

class DefaultTextValueEditor @Inject constructor() : TextValueEditor {
    internal constructor(settings: TextValueEditor.Settings) : this() {
        initWith(settings)
    }

    override var state: TextValueEditor.ViewState by mutableStateOf(TextValueEditor.ViewState())
        private set

    override fun update(state: TextValueEditor.ViewState) {
        this.state = state
    }

    override fun initWith(settings: TextValueEditor.Settings, makeVisible: Boolean) {
        this.state = TextValueEditor.ViewState(
            status = if (makeVisible) {
                TextValueEditor.ViewState.Status.Input
            } else {
                TextValueEditor.ViewState.Status.Gone
            },
            settings = settings
        )
    }
}
