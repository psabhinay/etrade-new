package com.etrade.mobilepro.ui_demo.compose.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.AnnotatedString
import androidx.fragment.app.Fragment
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.EtradeTheme
import com.etrade.mobilepro.compose.etdialogs.edit.TextValueEditDialog
import com.etrade.mobilepro.compose.etdialogs.edit.TextValueEditor
import com.etrade.mobilepro.compose.etdialogs.edit.create
import com.etrade.mobilepro.compose.etdialogs.edit.show
import com.etrade.mobilepro.compose.etdialogs.elements.DialogButton
import com.etrade.mobilepro.compose.etdialogs.info.InfoDialog
import com.etrade.mobilepro.compose.etdialogs.info.InfoDialogManager
import com.etrade.mobilepro.compose.etdialogs.info.create
import com.etrade.mobilepro.compose.etdialogs.info.hide
import com.etrade.mobilepro.compose.etdialogs.info.show
import com.etrade.mobilepro.ui_demo.R
import com.etrade.mobilepro.ui_demo.databinding.ActivityComposeDialogBinding
import com.etrade.mobilepro.util.android.binding.viewBinding

class ComposeDialogsActivity : AppCompatActivity() {
    private val binding by viewBinding(ActivityComposeDialogBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container, ComposeDialogContainerFragment())
            .commit()
    }
}

class ComposeDialogContainerFragment: Fragment() {
    private val textValueEditor = TextValueEditor.create(
        title = "Dialog title",
        onDone = { Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show() },
        onDismiss = { Toast.makeText(requireContext(), "Dismissed", Toast.LENGTH_SHORT).show() },
    ) { _, onDone, onDismiss ->
        add(
            DialogButton(
                title = AnnotatedString("Cancel"),
                onClick = onDismiss
            )
        )
        add(
            DialogButton(
                title = AnnotatedString("OK"),
                onClick = onDone,
                isEnabled = { state -> state.value.text.isNotEmpty() }
            )
        )
    }

    private val yesNoDialog = InfoDialogManager.create(
        title = "Delete random stuff",
        message = AnnotatedString("Are you sure you want to press one of these buttons?"),
        onDismiss = { Toast.makeText(requireContext(), "Dismissed", Toast.LENGTH_SHORT).show() },
    ) { dialogManager, onDismiss ->
        add(
            DialogButton(
                title = AnnotatedString("No"),
                onClick = onDismiss
            )
        )
        add(
            DialogButton(
                title = AnnotatedString("Yes"),
                onClick = {
                    dialogManager.hide()
                    Toast.makeText(requireContext(), "Deleted!", Toast.LENGTH_SHORT).show()
                }
            )
        )
    }

    private val nameWithRulesEditor = TextValueEditor.create(
        title = "Some chars are restricted",
        hint = "Max length = 5",
        rules = TextValueEditor.Settings.Rules(
            excludedChars = setOf('"', '=', '<', '>', '[', ']', '!', '@', '#', '^', '&', '\'', ':', ';', ',', '?'),
            inputLength = 5,
        ),
        onDone = { Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show() },
        onDismiss = { /* no op */ },
    ) { _, onDone, onDismiss ->
        add(
            DialogButton(
                title = AnnotatedString("Cancel"),
                onClick = onDismiss
            )
        )
        add(
            DialogButton(
                title = AnnotatedString("Done"),
                onClick = onDone,
                isEnabled = { state -> state.value.text.isNotEmpty() }
            )
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View = ComposeView(requireContext()).apply {
        setContent {
            EtradeTheme {
                Column(modifier = Modifier.padding(horizontal = Dimens.Size16dp)) {
                    Button(onClick = { textValueEditor.show() }) {
                        Text("Open Edit dialog")
                    }
                    Button(onClick = { nameWithRulesEditor.show() }) {
                        Text("Open Edit with rules dialog")
                    }
                    Spacer(modifier = Modifier.height(Dimens.Size24dp))
                    Button(onClick = { yesNoDialog.show() }) {
                        Text("Open Yes No dialog")
                    }
                }
                TextValueEditDialog(textValueEditor.state)
                TextValueEditDialog(nameWithRulesEditor.state)
                InfoDialog(yesNoDialog.state)

            }
        }
    }
}
