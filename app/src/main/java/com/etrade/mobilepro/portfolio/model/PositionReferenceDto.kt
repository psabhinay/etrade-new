package com.etrade.mobilepro.portfolio.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PositionReferenceDto(
    @Json(name = "accountUuid")
    val accountUuid: String,
    @Json(name = "accountId")
    val accountId: String,
    @Json(name = "positionId")
    val positionId: String,
    @Json(name = "pricePaid")
    val pricePaid: String,
    @Json(name = "basisPrice")
    val basisPrice: String,
    @Json(name = "symbol")
    val symbol: String,
    @Json(name = "commission")
    val commission: String,
    @Json(name = "quantity")
    val quantity: String,
    @Json(name = "displayQuantity")
    val displayQuantity: String,
    @Json(name = "fees")
    val fees: String,
    @Json(name = "todayQuantity")
    val todayQuantity: String,
    @Json(name = "todayPricePaid")
    val todayPricePaid: String,
    @Json(name = "todayCommissions")
    val todayCommissions: String,
    @Json(name = "daysGainValue")
    val daysGainValue: String,
    @Json(name = "daysGainPercentage")
    val daysGainPercentage: String,
    @Json(name = "totalGainValue")
    val totalGainValue: String,
    @Json(name = "totalGainPercentage")
    val totalGainPercentage: String,
    @Json(name = "todaysClose")
    val todaysClose: String = "",
    @Json(name = "daysQuantity")
    val daysQuantity: String?,
    @Json(name = "daysPurchase")
    val daysPurchase: Boolean,
    @Json(name = "hasLots")
    val hasLots: Boolean,
    @Json(name = "inTheMoneyFlag")
    val inTheMoneyFlag: String,
    @Json(name = "optionUnderlier")
    val optionUnderlier: String,
    @Json(name = "strikePrice")
    val strikePrice: String,
    @Json(name = "markToMarket")
    val markToMarket: String,
    @Json(name = "marketValue")
    val marketValue: String,
    @Json(name = "baseSymbolPrice")
    val baseSymbolPrice: String?
)
