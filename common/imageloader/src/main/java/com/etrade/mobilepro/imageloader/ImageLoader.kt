package com.etrade.mobilepro.imageloader

import android.widget.ImageView

interface ImageLoader {

    fun show(url: String, target: ImageView, onResourceReady: (() -> Unit)? = null)

    fun showAndSetBackground(url: String, target: ImageView, color: Int? = null)
}
