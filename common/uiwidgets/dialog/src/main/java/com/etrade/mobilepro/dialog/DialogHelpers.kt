package com.etrade.mobilepro.dialog

import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.etrade.mobilepro.common.OverlayFragmentManagerHolder

fun Resources.confirmationDialog(
    @StringRes title: Int,
    @StringRes message: Int,
    positiveAction: () -> Unit,
    negativeAction: () -> Unit
): AppDialog {
    return appDialog {
        titleRes = title
        messageRes = message
        cancelable = false
        resourcesPositiveAction {
            labelRes = R.string.yes
            action = positiveAction
        }
        resourcesNegativeAction {
            labelRes = R.string.no
            action = negativeAction
        }
    }
}

fun OverlayFragmentManagerHolder.displayWalkthroughMessage(
    fragmentProvider: () -> Fragment,
    tag: String,
    showAsDialogFragment: Boolean,
    fragmentManager: FragmentManager? = null,
    onDisplayError: () -> Unit
) {
    overlayManager?.manageFragment(
        visible = true,
        fragmentProvider = fragmentProvider,
        showDialogs = false,
        tag = tag,
        showAsDialogFragment = showAsDialogFragment,
        fragmentManager = fragmentManager
    ) ?: run { onDisplayError() }
}

fun OverlayFragmentManagerHolder.hideWalkthroughMessage(
    fragmentProvider: () -> Fragment,
    tag: String,
    showAsDialogFragment: Boolean,
    fragmentManager: FragmentManager? = null
) {
    overlayManager?.manageFragment(
        visible = false,
        fragmentProvider = fragmentProvider,
        showDialogs = true,
        tag = tag,
        showAsDialogFragment = showAsDialogFragment,
        fragmentManager = fragmentManager
    )
}

fun AppDialog.Action.decorate(action: (() -> Unit)?) =
    copy(
        perform = {
            perform?.invoke()
            action?.invoke()
        }
    )

fun AppDialog.decorateActions(additionalAction: (() -> Unit)?) =
    this.copy(
        positiveAction = positiveAction?.decorate(additionalAction),
        negativeAction = negativeAction?.decorate(additionalAction)
    )
