package com.etrade.mobilepro.portfolio.taxlot

import android.content.Context
import android.view.ViewGroup
import com.etrade.mobilepro.R
import com.etrade.mobilepro.instrument.underlier
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableRow
import com.etrade.mobilepro.portfolio.tableview.PortfolioTableViewAdapter
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.SymbolRowHeaderViewHolder
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.DateTimeParseException

private val CLOSEDATE_FORMAT: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yy")
private val CLOSEDATE_ACCESSIBILITY_FORMAT: DateTimeFormatter =
    DateTimeFormatter.ofPattern("MMM dd yyyy")

class TaxLotTableViewAdapter(private val context: Context) : PortfolioTableViewAdapter(context) {

    private val closeLotLabel by lazy { context.getString(R.string.portfolio_tax_lot_row_header) }

    override fun getAllRowHeaderViewTypes(): List<Int> {
        return listOf(R.layout.table_view_row_header_tax_lot, R.layout.table_view_row_header_layout)
    }

    override fun getAllCellViewTypes(): List<Int> {
        return listOf(R.layout.table_view_cell_layout_tax_lot, R.layout.table_view_cell_layout)
    }

    override fun getCellItemViewType(row: Int, column: Int): Int {
        return if (data.rows.getOrNull(row)?.isTaxLot == true) {
            R.layout.table_view_cell_layout_tax_lot
        } else {
            R.layout.table_view_cell_layout
        }
    }

    override fun getRowHeaderItemViewType(position: Int): Int {
        return if (data.rows.getOrNull(position)?.isTaxLot == true) {
            R.layout.table_view_row_header_tax_lot
        } else {
            R.layout.table_view_row_header_layout
        }
    }

    override fun getCellContentDescription(
        symbol: String,
        contentDescriptionWithoutSymbol: String,
        rowData: PortfolioRecordsTableRow?,
        row: SymbolCell?
    ) = if (rowData?.isTaxLot == true) {
        val date = row?.displayText?.let { formatLotDateForAccessibility(it) } ?: ""
        "$symbol, $date, $closeLotLabel, $contentDescriptionWithoutSymbol"
    } else {
        "$symbol, $contentDescriptionWithoutSymbol"
    }

    override fun onCreateRowHeaderViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val itemView = inflater.inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.table_view_row_header_tax_lot -> SymbolRowHeaderViewHolder(
                itemView
            )
            R.layout.table_view_row_header_layout -> SymbolRowHeaderViewHolder(
                itemView
            )
            else -> throw UnsupportedOperationException("Unknown view type: $viewType")
        }
    }

    override fun onBindRowHeaderViewHolderDelegate(
        rowHeaderItemModel: Any,
        holder: AbstractViewHolder,
        rowPosition: Int
    ) {
        val rowCell = rowHeaderItemModel as SymbolCell
        val symbol = rowCell.underlier.symbol
        val symbolDesc = rowCell.getSymbolContentDescription()
        val rowData = data.rows.getOrNull(rowPosition)
        if (rowData?.isTaxLot == true) {
            val date = formatLotDateForAccessibility(rowCell.displayText)
            holder.itemView.contentDescription = "$symbolDesc, $date, $closeLotLabel"
        } else {
            val description = rowCell.description ?: ""

            val resultDescription = if (description == symbol) {
                symbolDesc
            } else {
                // replacing first word (symbol) in content description: e.g. "AAPL March 24, 2021 $150 put" -> "Ay Ay P L March..."
                "$symbolDesc, ${description.substring(description.indexOf(" ") + 1)}"
            }
            holder.itemView.contentDescription = resultDescription
        }

        rowData?.let { row ->
            if (!row.isTaxLot) {
                holder.itemView.setBackgroundResource(row.backgroundResource)
            }
        }
    }

    private fun formatLotDateForAccessibility(closeDate: String): String {
        return try {
            LocalDate.parse(closeDate, CLOSEDATE_FORMAT).format(CLOSEDATE_ACCESSIBILITY_FORMAT)
        } catch (e: DateTimeParseException) {
            closeDate
        }
    }
}
