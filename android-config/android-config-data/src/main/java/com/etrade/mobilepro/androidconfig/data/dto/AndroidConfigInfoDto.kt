package com.etrade.mobilepro.androidconfig.data.dto

import com.etrade.mobilepro.androidconfig.api.AndroidConfigInfo
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "android", strict = false)
data class AndroidConfigInfoDto(
    @field:Element(name = "MinimumVersion")
    override var minimumVersion: String = "",

    @field:Element(name = "akamaiDomain")
    override var akamaiDomain: String = "",

    @field:Element(name = "akamaiChartVersion")
    override var chartVersion: String = ""
) : AndroidConfigInfo
