package com.etrade.mobilepro.trade.data.option

import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.backends.api.ServerMessageException
import com.etrade.mobilepro.backends.api.ServerResponseException
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.trade.api.OrderPlaceResponse
import com.etrade.mobilepro.trade.api.OrderPreviewResponse
import com.etrade.mobilepro.trade.api.OrderRequest
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.option.ConfirmedOptionOrder
import com.etrade.mobilepro.trade.api.option.OptionOrder
import com.etrade.mobilepro.trade.api.option.OptionOrderPlaceResponse
import com.etrade.mobilepro.trade.api.option.OptionOrderPreviewResponse
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.TradeDelegate
import com.etrade.mobilepro.trade.data.TradeOrderArguments
import com.etrade.mobilepro.trade.data.createOptionTradeBody
import com.etrade.mobilepro.trade.data.customorder.CustomOrderApiClient
import com.etrade.mobilepro.trade.data.customorder.CustomOrderDto
import com.etrade.mobilepro.trade.data.customorder.CustomOrderResponse
import com.etrade.mobilepro.trade.data.priceTypeWarnings
import com.etrade.mobilepro.trade.data.util.formatOrderPlacedTime
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.util.ValueCoder
import com.squareup.moshi.Moshi

internal abstract class BaseOptionTradeDelegate(
    private val apiClient: CustomOrderApiClient,
    private val moshi: Moshi,
    private val tradeFormInputId: OptionTradeFormInputId,
    private val tradeLegsCoder: ValueCoder<List<TradeLeg>>,
    private val isTablet: Boolean
) : TradeDelegate {

    protected abstract suspend fun CustomOrderApiClient.previewOrderCall(body: String): ServerResponseDto<CustomOrderResponse>
    protected abstract suspend fun CustomOrderApiClient.placeOrderCall(body: String): ServerResponseDto<CustomOrderResponse>

    override suspend fun previewOrder(map: InputValuesSnapshot, marketDestination: MarketDestination): ETResult<OrderPreviewResponse> =
        apiClient.execute { previewOrderCall(createBody(map)) }.map { it.toOrderPreviewResponse() }

    override suspend fun placeOrder(arguments: TradeOrderArguments, order: TradeOrder): ETResult<OrderPlaceResponse> =
        apiClient.execute { placeOrderCall(createBody(arguments.params)) }.map { it.toOrderPlaceResponse() }

    private suspend fun CustomOrderApiClient.execute(method: suspend CustomOrderApiClient.() -> ServerResponseDto<CustomOrderResponse>) =
        runCatchingET {
            val result = method()
            try {
                result.doIfSuccessfulOrThrow { it }
            } catch (e: ServerResponseException) {
                val error = e.error
                throw if (error is ServerError.Known) {
                    ServerMessageException(error.messages)
                } else {
                    e
                }
            }
        }

    private fun CustomOrderResponse.toOrderPreviewResponse(): OrderPreviewResponse {
        val order = requireNotNull(order)
        return object : OptionOrderPreviewResponse {
            override val orders: List<OptionOrder> = listOf(order)
            override val warnings: Collection<ServerMessage> = order.priceTypeWarnings(getAllMessages())
        }
    }

    private fun CustomOrderResponse.toOrderPlaceResponse(): OrderPlaceResponse {
        val order = requireNotNull(order)
        return object : OptionOrderPlaceResponse {
            override val orders: List<ConfirmedOptionOrder> = listOf(order.confirmedOptionOrder)
            override val warnings: Collection<ServerMessage> = getAllMessages()
        }
    }

    private val CustomOrderDto.confirmedOptionOrder: ConfirmedOptionOrder
        get() {
            return object : ConfirmedOptionOrder, OptionOrder by this {
                override val confirmationOrderId: String? = orderId
                override val placedDate: String? = formatOrderPlacedTime(placedMs)
                override val orderRequest: OrderRequest? = null
            }
        }

    private fun createBody(map: InputValuesSnapshot) = map.createOptionTradeBody(tradeLegsCoder, tradeFormInputId.securityTypeValueId, moshi, isTablet)
}
