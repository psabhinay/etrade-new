package com.etrade.mobilepro.backends.mgs

import com.etrade.mobilepro.util.formatters.toWebDeepLink
import com.etrade.mobilepro.util.formatters.toWebViewDeepLink
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MobileScreen(
    @Json(name = "views")
    val views: List<BaseScreenView?> = emptyList(), // for now api could return null item for view collection
    @Json(name = "references")
    val references: List<BaseReference<*>>? = null,
    @Json(name = "meta")
    val meta: List<BaseMeta>? = null
)

abstract class BaseScreenView {
    @Json(name = "type")
    open val type: String? = null

    @Json(name = "data")
    abstract val data: Any?

    @Json(name = "cta")
    abstract val ctaList: List<CallToActionDto>?

    @Json(name = "action")
    open val clickActionDto: ClickActionDto? = null
}

abstract class BaseReference<T> : Reference<T> {
    @Json(name = "type")
    val type: String? = null

    @Json(name = "data")
    abstract val data: Any
}

open class BaseMeta {
    @Json(name = "type")
    open val type: String? = null
}

@JsonClass(generateAdapter = true)
data class CallToActionDto(
    @Json(name = "cta_label")
    val label: String,
    @Json(name = "cta_action")
    val clickActionDto: ClickActionDto,
    @Json(name = "cta_title")
    val title: String? = null
)

@JsonClass(generateAdapter = true)
data class ClickActionDto(
    @Json(name = "app_url")
    val appUrl: String? = null,
    @Json(name = "web_url")
    val webUrl: String? = null,
    @Json(name = "webview_url")
    val webViewUrl: String? = null,
    @Json(name = "title")
    val webViewTitle: String? = null
) {

    fun toWebDeepLink(): String? = webUrl.toWebDeepLink()

    fun toWebViewDeepLink(baseUrl: String? = "", externalWebViewTitle: String? = null): String? = webViewUrl.toWebViewDeepLink(
        baseUrl = baseUrl,
        externalWebViewTitle = externalWebViewTitle,
        webViewTitle = webViewTitle
    )
}
