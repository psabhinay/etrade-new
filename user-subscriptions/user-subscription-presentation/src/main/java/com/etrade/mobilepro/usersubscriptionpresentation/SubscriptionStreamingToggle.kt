package com.etrade.mobilepro.usersubscriptionpresentation

interface SubscriptionStreamingToggle {
    fun toggle(toggle: Boolean)
}
