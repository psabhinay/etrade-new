package com.etrade.mobilepro.news.data.repo

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.api.model.NewsSettings
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.api.repo.NewsRepo
import com.etrade.mobilepro.news.data.mapper.TextNewsMapper
import com.etrade.mobilepro.news.data.repo.datasource.NewsDataSourceFactory
import com.etrade.mobilepro.news.data.repo.datasource.NewsPagingSource
import com.etrade.mobilepro.news.data.rest.NewsApiClient
import com.etrade.mobilepro.watchlist.repo.CombinedWatchlistRepo
import com.etrade.mobilepro.watchlist.repo.DeviceWatchlistRepo
import io.reactivex.Observable

class DefaultNewsRepo(
    private val apiClient: NewsApiClient,
    private val newsSettings: NewsSettings,
    deviceWatchlistRepo: DeviceWatchlistRepo,
    combinedWatchlistRepo: CombinedWatchlistRepo,
) : NewsRepo {
    private val dataSourceFactory = NewsDataSourceFactory(apiClient, deviceWatchlistRepo, combinedWatchlistRepo)
    private var dataSource: NewsPagingSource? = null
    private val textNewsMapper = TextNewsMapper()

    override fun news(
        type: NewsType,
        requestParams: NewsRequest?,
        headerItemsSource: Observable<List<NewsItem>>?
    ): Pager<Int, NewsItem> {
        dataSource = dataSourceFactory.getNewsDataSource(type, requestParams, headerItemsSource)
        return dataSource?.run {
            val pagingConfig = PagingConfig(
                enablePlaceholders = false,
                prefetchDistance = newsSettings.prefetchDistance,
                initialLoadSize = newsSettings.initialLoadCount,
                pageSize = newsSettings.pageSize
            )

            Pager(pagingConfig) {
                this
            }
        } ?: throw IllegalArgumentException("No data source for type ${type.name}")
    }

    override fun textContent(contentId: String): Observable<NewsItem.Text> {
        return apiClient
            .getNewsDetail(contentId)
            .toObservable()
            .map { newsDto -> newsDto.data?.newsPayload?.docsList?.docs ?: emptyList() }
            .map { list -> list.first() }
            .map { docDto -> textNewsMapper.map(docDto.textDoc) }
    }

    override fun symbolNews(
        requestParams: NewsRequest.Symbol,
        maxCount: Int
    ): Observable<List<NewsItem.Text>> {
        return apiClient.getSymbolNews(requestParams.joinSymbols(), 0, maxCount)
            .toObservable()
            .map { newsDto -> newsDto.data?.newsPayload?.docsList?.docs ?: emptyList() }
            .map { list ->
                list.map {
                    textNewsMapper.map(it.textDoc)
                }
            }
    }

    override fun marketNews(maxCount: Int): Observable<List<NewsItem.Text>> {
        return apiClient.getMarketNews(0, maxCount)
            .toObservable()
            .map { newsDto -> newsDto.data?.newsPayload?.docsList?.docs ?: emptyList() }
            .map { list ->
                list.map {
                    textNewsMapper.map(it.textDoc)
                }
            }
    }
}
