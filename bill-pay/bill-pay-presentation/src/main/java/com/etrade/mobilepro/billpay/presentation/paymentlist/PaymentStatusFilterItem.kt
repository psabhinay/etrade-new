package com.etrade.mobilepro.billpay.presentation.paymentlist

import com.etrade.mobilepro.billpay.api.PaymentStatus

data class PaymentStatusFilterItem(val label: String, val status: PaymentStatus) {
    override fun toString(): String {
        return label
    }
}
