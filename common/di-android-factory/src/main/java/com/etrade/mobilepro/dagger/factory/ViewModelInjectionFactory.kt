package com.etrade.mobilepro.dagger.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

interface ViewModelInjectionTracker {
    fun trackScreenIfApplicable(viewModel: Any)
}

@Suppress("UNCHECKED_CAST")
@Singleton
class ViewModelInjectionFactory @Inject constructor(
    private val creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>,
    private val injectionTracker: ViewModelInjectionTracker
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        val creator = creators[modelClass] ?: creators.entries.firstOrNull {
            modelClass.isAssignableFrom(it.key)
        }?.value ?: throw IllegalArgumentException("unknown model class $modelClass")
        return (creator.get() as T).also { injectionTracker.trackScreenIfApplicable(it) }
    }
}
