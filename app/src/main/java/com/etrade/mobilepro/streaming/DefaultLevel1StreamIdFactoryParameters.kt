package com.etrade.mobilepro.streaming

import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.lightstreamer.level1.Level1StreamIdFactory
import javax.inject.Inject

class DefaultLevel1StreamIdFactoryParameters @Inject constructor(
    private val user: User
) : Level1StreamIdFactory.Parameters {

    override val isRealtimeQuotes: Boolean
        get() = user.isRealtimeUser
}
