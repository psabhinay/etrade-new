package com.etrade.mobilepro.transactions.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.R
import com.etrade.mobilepro.transactions.Transaction
import com.etrade.mobilepro.transactions.TransactionDetailInfo
import com.etrade.mobilepro.transactions.databinding.AdapterTransactionItemBinding
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

private val LOCALDATE_DISPLAYED_DATE_FORMAT: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yy")

class TransactionsListAdapter(private val emptyValue: String) : RecyclerView.Adapter<TransactionsListAdapter.TransactionsViewHolder>() {

    var transactionsList: List<TransactionDetailInfo> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionsViewHolder {
        val binding = DataBindingUtil.inflate<AdapterTransactionItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.adapter_transaction_item,
            parent,
            false
        )

        return TransactionsViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return transactionsList.size
    }

    override fun onBindViewHolder(holder: TransactionsViewHolder, position: Int) {
        val transaction: TransactionDetailInfo = transactionsList[position]
        val amount = transaction.amount
        val formattedAmount = getFormattedAmount(amount)

        val transactionDate: LocalDate = LocalDate.parse(transaction.date, LOCALDATE_DISPLAYED_DATE_FORMAT)

        holder.bind(
            Transaction.create(
                transaction.type ?: emptyValue,
                transaction.description ?: emptyValue,
                amount,
                formattedAmount,
                holder.itemView.context.getString(R.string.accessibility_empty_amount_description),
                transactionDate.format(createAmericanSimpleDateFormatter())
            )
        )
    }

    private fun getFormattedAmount(amount: String?) = MarketDataFormatter.formatMoneyValueWithCurrency(amount?.toBigDecimalOrNull())

    class TransactionsViewHolder(private val binding: AdapterTransactionItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(transaction: Transaction) {
            binding.obj = transaction
        }
    }
}
