// Generated by view binder compiler. Do not edit!
package com.etrade.mobilepro.bank.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.etrade.mobilepro.bank.R;
import com.etrade.mobilepro.swiperefresh.MultiSwipeRefreshLayout;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class BankHomeFragmentBinding implements ViewBinding {
  @NonNull
  private final MultiSwipeRefreshLayout rootView;

  @NonNull
  public final ContentLoadingProgressBar bankHomeLoadingIndicator;

  @NonNull
  public final RecyclerView bankHomeRecyclerView;

  @NonNull
  public final MultiSwipeRefreshLayout pullToRefresh;

  private BankHomeFragmentBinding(@NonNull MultiSwipeRefreshLayout rootView,
      @NonNull ContentLoadingProgressBar bankHomeLoadingIndicator,
      @NonNull RecyclerView bankHomeRecyclerView, @NonNull MultiSwipeRefreshLayout pullToRefresh) {
    this.rootView = rootView;
    this.bankHomeLoadingIndicator = bankHomeLoadingIndicator;
    this.bankHomeRecyclerView = bankHomeRecyclerView;
    this.pullToRefresh = pullToRefresh;
  }

  @Override
  @NonNull
  public MultiSwipeRefreshLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static BankHomeFragmentBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static BankHomeFragmentBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.bank_home_fragment, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static BankHomeFragmentBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.bank_home_loading_indicator;
      ContentLoadingProgressBar bankHomeLoadingIndicator = ViewBindings.findChildViewById(rootView, id);
      if (bankHomeLoadingIndicator == null) {
        break missingId;
      }

      id = R.id.bank_home_recycler_view;
      RecyclerView bankHomeRecyclerView = ViewBindings.findChildViewById(rootView, id);
      if (bankHomeRecyclerView == null) {
        break missingId;
      }

      MultiSwipeRefreshLayout pullToRefresh = (MultiSwipeRefreshLayout) rootView;

      return new BankHomeFragmentBinding((MultiSwipeRefreshLayout) rootView,
          bankHomeLoadingIndicator, bankHomeRecyclerView, pullToRefresh);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
