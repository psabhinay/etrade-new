package com.etrade.mobilepro.backends.mgs

import com.etrade.mobilepro.testutil.getObjectFromJson
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class PaginationMetaSuite {

    @Test
    fun `check has next page`() {
        val paginationMeta = PaginationMeta::class.getObjectFromJson<PaginationMeta>(
            "pagination-meta-has-next-page.json",
            PaginationMeta::class.java
        )
        assertEquals("pagination", paginationMeta.type)
        assertTrue(paginationMeta.nextPage.continuationToken?.isNotBlank() == true)
        assertTrue(paginationMeta.nextPage.hasNext)
    }

    @Test
    fun `check no next page`() {
        val paginationMeta = PaginationMeta::class.getObjectFromJson<PaginationMeta>("pagination-meta-no-next-page.json", PaginationMeta::class.java)
        assertEquals("pagination", paginationMeta.type)
        assertNull(paginationMeta.nextPage.continuationToken)
        assertFalse(paginationMeta.nextPage.hasNext)
    }
}
