package com.etrade.mobilepro.dummy

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.util.randomString
import java.math.BigDecimal
import java.util.Random

private val random = Random()

fun getCurrencyValue(): BigDecimal {
    val bound = if (random.nextBoolean()) {
        100_000
    } else {
        100_000_000
    }
    val amount = "${random.nextInt(bound)}.${random.nextInt(99)}".toBigDecimal()
    return if (random.nextBoolean()) {
        amount.negate()
    } else {
        amount
    }
}

fun getFormattedCurrency() = MarketDataFormatter.formatMoneyValueWithCurrency(getCurrencyValue())

fun getPercentage() = "${random.nextInt(100)}%"

fun getRandomString(outputStrLength: Int = 10): String {
    return ('a'..'z').randomString(outputStrLength)
}
