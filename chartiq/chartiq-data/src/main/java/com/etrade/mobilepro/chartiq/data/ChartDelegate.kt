package com.etrade.mobilepro.chartiq.data

import com.etrade.mobilepro.chartiq.api.model.OrderDetails
import com.etrade.mobilepro.chartiq.data.dto.ChartRequestType
import com.etrade.mobilepro.instrument.Symbol

interface ChartDelegate {
    fun onWebViewReady()
    fun onChartSettingsClicked()
    fun showChart()
    fun showAlertDialog(content: AlertDialogContent)
    fun onSymbolLookup()
    fun onSymbolsToCompareUpdated(symbols: List<Symbol>)
    fun showMessage(message: String)
    fun showOrderDetails(details: OrderDetails)
    fun showLogin()
    fun makeServiceCall(requestId: String, query: String?, requestBody: String?, requestType: ChartRequestType)
}

data class AlertDialogContent(
    val alertId: String,
    val message: String,
    val title: String?,
    val positiveButton: AlertDialogAction?,
    val negativeButton: AlertDialogAction?,
    val neutralButton: AlertDialogAction?
)

data class AlertDialogAction(val title: String, val callback: String)
