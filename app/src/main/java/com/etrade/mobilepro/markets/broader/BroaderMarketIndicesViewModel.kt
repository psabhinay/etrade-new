package com.etrade.mobilepro.markets.broader

import android.content.res.Resources
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.viewModelScope
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.di.Default
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.livedata.postUpdateValue
import com.etrade.mobilepro.markets.MarketsStreamingDelegate
import com.etrade.mobilepro.markets.MarketsStreamingDelegateImpl
import com.etrade.mobilepro.markets.common.MarketsTableViewListener
import com.etrade.mobilepro.markets.common.buildSymbolRowItem
import com.etrade.mobilepro.markets.common.getSymbolRowInfo
import com.etrade.mobilepro.markets.common.getUpdatedCellItemsForSymbol
import com.etrade.mobilepro.markets.common.setSymbolsAndCells
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.data.response.MarketIndexResponse
import com.etrade.mobilepro.markets.tableHeaders
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.screener.presentation.common.TableViewInitializeParams
import com.etrade.mobilepro.screener.presentation.common.TableViewPayload
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.tableviewutils.presentation.adapter.HasTableViewAdapter
import com.etrade.mobilepro.tableviewutils.presentation.adapter.TableViewAdapter
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.sorting.TableDataSortable
import com.etrade.mobilepro.tableviewutils.presentation.sorting.TableDataSortingHandler
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.MarketsBroaderIndicesTapEventAnalyticsParams
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.rx.awaitResult
import com.etrade.mobilepro.util.safeParseBigDecimal
import io.reactivex.Observable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

private const val TOP_INDEX_NUM = 3

class BroaderMarketIndicesViewModel @Inject constructor(
    private val dataColorResolver: DataColorResolver,
    @Default streamingController: StreamingSubscriptionController,
    private val marketsService: MoverService,
    resources: Resources,
    private val streamingStatusController: StreamingStatusController,
    private val user: User,
    private val tracker: Tracker
) : DynamicViewModel(R.layout.broader_market_indices),
    HasTableViewAdapter,
    LifecycleObserver,
    TableDataSortable {

    override val variableId: Int = com.etrade.mobilepro.BR.obj

    private val columnHeaders by lazy { resources.tableHeaders() }

    private val sortingHandler = TableDataSortingHandler<ResultDataCell>()

    val sortState: LiveData<SortStateInfo?>
        get() = _sortState
    private val _sortState = MutableLiveData<SortStateInfo?>()

    private val _tableViewInitializeParams = MutableLiveData<TableViewInitializeParams>()
    val tableViewInitializeParams: LiveData<TableViewInitializeParams>
        get() = _tableViewInitializeParams

    private val _broaderIndicesPayload = MutableLiveData<TableViewPayload>()
    val broaderIndicesPayload: LiveData<TableViewPayload>
        get() = _broaderIndicesPayload

    private val _viewState = MutableLiveData<DynamicUiViewState>()
    val viewState: LiveData<DynamicUiViewState>
        get() = _viewState

    private val setOfFields = StockData.fields()
    private val streamingDelegate: MarketsStreamingDelegate by lazy {
        MarketsStreamingDelegateImpl(streamingController, streamingStatusController, setOfFields)
    }

    private var isSubscribedToStreaming: Boolean = false
    private var setColorToNeutralRequired: AtomicBoolean = AtomicBoolean(false)

    private val isUserLoggedInAndRealTime
        get() = user.isLoggedIn() && user.isRealtimeUser

    private val streamingStatusUpdateListener: (StreamingStatusUpdate) -> Unit = { status ->
        if (isUserLoggedInAndRealTime && viewState.value == DynamicUiViewState.Success && status is StreamingStatusUpdate.StreamingToggleUpdate) {
            if (status.enabled) {
                startStreaming()
            } else {
                cancelStreaming()
                if (!isSubscribedToStreaming && setColorToNeutralRequired.getAndSet(false)) {
                    setColorToNeutral()
                }
            }
        }
    }

    val scope: CoroutineScope? = viewModelScope

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onLifeCyclePause() {
        streamingStatusController.unsubscribeFromStreamingStatusUpdates(streamingStatusUpdateListener)
        cancelStreaming()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onLifeCycleResume() {
        streamingStatusController.subscribeForStreamingStatusUpdates(streamingStatusUpdateListener)
        startStreaming()
    }

    override fun setTableViewAdapter(tableViewAdapter: TableViewAdapter) {
        _tableViewInitializeParams.value = TableViewInitializeParams(
            R.dimen.markets_default_row_header_width,
            MarketsTableViewListener(
                cellClickListener = { title -> navigateToQuoteDetails(title, InstrumentType.EQ) },
                rowHeaderClickListener = { title, type -> navigateToQuoteDetails(title, type) },
                sortable = this
            )
        ) { tableViewAdapter }
    }

    fun refresh() {
        if (viewState.value != DynamicUiViewState.Success) {
            _viewState.value = DynamicUiViewState.Loading
        }
        viewModelScope.launch {
            marketsService.getMarketIndices().toObservable().awaitResult().fold(
                {
                    onSuccess(it)
                },
                {
                    _viewState.value = DynamicUiViewState.Error()
                    logger.error("Unable to fetch Market Indices", it)
                }
            )
        }
    }

    private fun onSuccess(response: MarketIndexResponse) {
        response.quotes?.run {
            if (size > TOP_INDEX_NUM) {
                val broaderIndices = subList(TOP_INDEX_NUM, size)
                val cellItems = mutableListOf<List<ResultDataCell>>()
                val rowHeaders = mutableListOf<SymbolCell>()
                broaderIndices.setSymbolsAndCells(rowHeaders, cellItems, dataColorResolver)
                _broaderIndicesPayload.value = TableViewPayload(
                    columnHeaders, rowHeaders, cellItems
                )
                startStreaming()
                _viewState.postValue(DynamicUiViewState.Success)
            } else {
                // when there is no data we want to show retry button with generic error message
                _viewState.value = DynamicUiViewState.Error()
                logger.debug("onSuccess size is smaller than $TOP_INDEX_NUM")
            }
        }
    }

    private fun cancelStreaming() {
        if (isSubscribedToStreaming) {
            streamingDelegate.cancelSubscription()
            isSubscribedToStreaming = false
        }
    }

    private fun startStreaming() {
        if (_broaderIndicesPayload.value == null || !isUserLoggedInAndRealTime) {
            return
        }
        if (streamingDelegate.isStreamingEnabled() && !isSubscribedToStreaming) {
            isSubscribedToStreaming = true
            val rowHeaders = _broaderIndicesPayload.value?.rowHeaders ?: return
            val symbols = rowHeaders.map { Symbol(it) }.toSet()
            logger.debug("subscribed symbols: $symbols")
            streamingDelegate.initSubscription(Observable.just(symbols), ::handleSymbolUpdate)
        }
    }

    private fun setColorToNeutral() {
        _broaderIndicesPayload.value?.run {
            val tableCellItems: MutableList<MutableList<ResultDataCell>> = mutableListOf()
            cellItems.forEach { rowCells ->
                val rowCellItems: MutableList<ResultDataCell> = mutableListOf()
                rowCells.forEachIndexed { index, cell ->
                    if (index == 0) {
                        rowCellItems.add(
                            ResultDataCell(
                                value = cell.value,
                                displayText = cell.displayText,
                                instrumentType = cell.instrumentType,
                                symbol = cell.symbol
                            )
                        )
                    } else {
                        rowCellItems.add(cell)
                    }
                }
                tableCellItems.add(rowCellItems)
            }
            _broaderIndicesPayload.value = this.copy(cellItems = tableCellItems)
        }
    }

    private fun handleSymbolUpdate(symbol: String, instrumentType: InstrumentType, data: Level1Data) {
        setColorToNeutralRequired.set(true)
        val lastPrice = data.lastPrice?.safeParseBigDecimal() ?: return

        val symbolCells = buildSymbolCells(symbol, instrumentType, data, MarketDataFormatter.formatMarketDataCustom(lastPrice))
        _broaderIndicesPayload.value?.run {
            _broaderIndicesPayload.postUpdateValue {
                TableViewPayload(
                    columnHeaderList,
                    rowHeaders,
                    getUpdatedCellItemsForSymbol(cellItems, symbolCells, symbol)
                )
            }
        }
    }

    private fun buildSymbolCells(symbol: String, instrumentType: InstrumentType, data: Level1Data, lastPrice: String): List<ResultDataCell> {
        return getSymbolRowInfo(data, symbol, lastPrice).run {
            buildSymbolRowItem(instrumentType = instrumentType, dataColorResolver = dataColorResolver)
        }
    }

    override fun onCleared() {
        cancelStreaming()
        super.onCleared()
    }

    private fun navigateToQuoteDetails(title: String, type: InstrumentType) {
        tracker.event(MarketsBroaderIndicesTapEventAnalyticsParams())
        clickEvents.postValue(CtaEvent.LaunchQuote(SearchResultItem.Symbol(title = title, type = type).toBundle()))
    }

    override fun sortTable(column: Int?) {
        val oldSortState = _sortState.value
        val newSortState = if (column == null) {
            _sortState.value
        } else {
            sortingHandler.resolveSortState(oldSortState, column)
        }

        val isApplyCurrentSortState = column == null && oldSortState != null

        if (isApplyCurrentSortState || newSortState != oldSortState) {
            applySortToTableViewData(newSortState)
        }
    }

    private fun applySortToTableViewData(newSortState: SortStateInfo?) {
        val tableViewPayload = _broaderIndicesPayload.value

        if (tableViewPayload != null && tableViewPayload.cellItems.isNotEmpty()) {
            val sortedData = sortingHandler.sortTableData(tableViewPayload.cellItems, tableViewPayload.rowHeaders, newSortState)
            val updatedModel = tableViewPayload.copy(
                rowHeaders = sortedData.first,
                cellItems = sortedData.second
            )
            _broaderIndicesPayload.value = updatedModel
        }
        _sortState.value = newSortState
    }
}
