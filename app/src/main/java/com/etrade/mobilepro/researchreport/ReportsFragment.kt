package com.etrade.mobilepro.researchreport

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.databinding.FragmentReportsBinding
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.addDividerItemDecoration
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@RequireLogin
class ReportsFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.fragment_reports) {

    private val binding by viewBinding(FragmentReportsBinding::bind)

    private val viewModel: ReportsViewModel by viewModels { viewModelFactory }

    private val navArgs: ReportsFragmentArgs by navArgs()

    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    private var snackBar: Snackbar? = null

    private val disclosuresClickListener = {
        startActivity(
            SpannableTextOverlayActivity.intent(
                requireActivity(),
                getString(R.string.disclosures),
                getString(R.string.disclosures_analyst_reports),
                com.etrade.mobilepro.education.R.drawable.ic_close
            )
        )
    }

    private val reportsAdapter = ReportsAdapter(disclosuresClickListener)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getReports(navArgs.symbol, navArgs.isFundReport)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding.reportsRv) {
            layoutManager = LinearLayoutManager(context)
            adapter = reportsAdapter
            addDividerItemDecoration(R.drawable.thin_divider)
        }
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    ReportsViewModel.ViewState.Loading -> onLoading()
                    ReportsViewModel.ViewState.Error -> onError()
                    is ReportsViewModel.ViewState.Success -> onSuccess(it.results)
                }
            }
        )
    }

    private fun onError() {
        binding.reportsLoadingBar.hide()
        snackBar = snackbarUtil.retrySnackbar {
            viewModel.getReports(navArgs.symbol, navArgs.isFundReport)
        }
    }

    private fun onLoading() {
        binding.reportsLoadingBar.show()
        binding.noReportsTv.visibility = View.GONE
        snackBar?.dismiss()
    }

    private fun onSuccess(items: List<ReportListItem>) {
        binding.reportsLoadingBar.hide()
        snackBar?.dismiss()
        reportsAdapter.items = items
        if (items.isEmpty()) {
            binding.noReportsTv.visibility = View.GONE
            binding.noReportsTv.visibility = View.VISIBLE
        } else {
            binding.noReportsTv.visibility = View.VISIBLE
            binding.noReportsTv.visibility = View.GONE
        }
    }
}
