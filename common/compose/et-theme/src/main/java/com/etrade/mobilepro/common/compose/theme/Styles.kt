package com.etrade.mobilepro.common.compose.theme

import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight

object Styles {
    val Regular10sp = etradeTypography.overline

    val Regular12sp = etradeTypography.caption
    val Medium12sp = etradeTypography.caption.copy(
        fontWeight = FontWeight.Medium
    )

    val Regular14sp = etradeTypography.body2
    val Medium14sp = etradeTypography.subtitle2

    val Regular16sp = etradeTypography.body1
    val Medium16sp = etradeTypography.body1.copy(
        fontWeight = FontWeight.Medium
    )

    val Medium18sp = TextStyle(
        fontWeight = FontWeight.Medium,
        fontSize = FontSizes.Size18sp,
        letterSpacing = FontSizes.Size0sp
    )

    val Medium24sp = TextStyle(
        fontWeight = FontWeight.Medium,
        fontSize = FontSizes.Size24sp,
        letterSpacing = FontSizes.Size0sp
    )
}
