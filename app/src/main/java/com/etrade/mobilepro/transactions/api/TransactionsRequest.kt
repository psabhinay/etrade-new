package com.etrade.mobilepro.transactions.api

import com.etrade.mobilepro.transactions.InstitutionType
import org.threeten.bp.LocalDate

data class TransactionsRequest(
    val userId: Long,
    val accountId: Long,
    val institutionType: InstitutionType,
    val fromDate: LocalDate,
    val toDate: LocalDate
)
