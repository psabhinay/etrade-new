package com.etrade.mobilepro.quoteapi

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.safeParseBigDecimal

data class QuoteChangeWidgetItem(
    val change: String,
    val changePercent: String
) {
    fun formatQuoteChange(formatter: String, change: String?, changePercent: String?): String {
        val percentChange: String = MarketDataFormatter.formatMarketDataCustom(changePercent?.toBigDecimalOrNull())
        val changeVal = MarketDataFormatter.formatMarketDataCustom(change?.safeParseBigDecimal())
        return String.format(formatter, changeVal, percentChange)
    }
}
