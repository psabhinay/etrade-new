package com.etrade.mobilepro.quote.presentation

data class AffiliatedProductAction(val onAction: () -> Unit)
data class AffiliatedProductTitleAction(val onAction: () -> Unit)
