package com.etrade.mobilepro.checkdeposit.presentation.activity

import android.view.ViewGroup
import com.etrade.mobilepro.checkdeposit.activity.service.DepositActivityItemDto
import com.etrade.mobilepro.checkdeposit.presentation.databinding.ItemDepositActivityBinding
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class CheckDepositActivityAdapterDelegate(
    private val depositActivityItemListener: ((DepositActivityItemDto, String) -> Unit)?
) : AbsListItemAdapterDelegate<DepositActivityResultItem, DepositActivityResultItem, DepositActivityItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): DepositActivityItemViewHolder {
        return DepositActivityItemViewHolder(parent.viewBinding(ItemDepositActivityBinding::inflate), depositActivityItemListener)
    }

    override fun isForViewType(item: DepositActivityResultItem, items: MutableList<DepositActivityResultItem>, position: Int): Boolean {
        return true // single type of items, always true
    }

    override fun onBindViewHolder(item: DepositActivityResultItem, holder: DepositActivityItemViewHolder, payloads: MutableList<Any>) {
        holder.bindTo(item)
    }
}
