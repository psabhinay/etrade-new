package com.etrade.mobilepro.news.presentation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.presentation.detail.ParcelableNewsItem
import com.etrade.mobilepro.searchingapi.items.SearchResultItem

@Suppress("TooManyFunctions") // subject to further refactoring / dividing
interface NewsRouter {
    fun navigateToQuoteDetails(view: View, symbol: String, instrumentType: InstrumentType)
    fun navigateToQuoteDetails(activity: FragmentActivity?, params: Bundle)
    fun navigateToQuotePage(navController: NavController, params: Bundle)
    /**
     * Navigate to News hub page
     *
     * @param view "View all" view
     * @param accountId News widget's selected account Id.
     */
    fun navigateToNewsHubPage(view: View, accountId: String, isStockPlan: Boolean)

    /**
     * Navigate to News page for account selected.
     *
     * @param view "View all" button view
     * @param accountId News widget's selected account Id.
     */
    fun navigateToAccountNewsPage(view: View, accountId: String)

    fun navigateToMarketNews(view: View)

    fun navigateToTextNewsDetails(navController: NavController?, newsDocId: String)
    fun navigateToTextNewsDetails(navController: NavController?, item: NewsItem.Text)
    fun navigateToTextNewsDetails(navController: NavController?)
    fun navigateWatchlistTabWithLogin(navController: NavController, tabIndex: Int)
    fun navigateToSetAlert(
        navController: NavController,
        symbol: SearchResultItem.Symbol,
        from: NewsType? = null
    )

    fun navigateToTrade(
        navController: NavController,
        params: Bundle,
        from: NewsType
    )

    fun forwardOnLoginToCurrentNav()

    fun openSearch(simpleSearch: Boolean = true, location: String? = null)

    fun companyNewsSymbolArg(bundle: Bundle): SearchResultItem.Symbol?
    fun portfolioNewsArgs(bundle: Bundle): Pair<String?, Boolean>
    fun newsDetailsArgs(bundle: Bundle?): Pair<String?, ParcelableNewsItem?>
    fun newsContainerArgs(bundle: Bundle?): NewsContainerFragment.Params?
}
