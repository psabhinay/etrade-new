package com.etrade.mobilepro.billpay.presentation

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input.UsPhoneLengthValidator
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input.UsPhoneValidator
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(application = android.app.Application::class)
class PhoneValidationTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()

    private val invalidLengthMessage = "Invalid length"
    private val lengthValidator = UsPhoneLengthValidator(
        invalidLengthMessage
    )
    private val invalidPhoneMessage = "Invalid Phone"
    private val phoneValidator = UsPhoneValidator(
        invalidPhoneMessage,
        context
    )

    @Test
    fun testEmptyValueLength() {
        val value = ""
        val lengthValue = lengthValidator.validate(value)
        assertTrue(
            "Length should be invalid",
            lengthValue is InputFieldManager.Value.Invalid
        )
        val invalidLength = lengthValue as InputFieldManager.Value.Invalid
        assertEquals(
            "Error message must match",
            invalidLength.reason,
            invalidLengthMessage
        )
    }

    @Test
    fun testFormattedNumberLength() {
        val value = "(213) 373-4253" // Valid formatted number
        val lengthValue = lengthValidator.validate(value)
        assertTrue(
            "Length should be valid",
            lengthValue is InputFieldManager.Value.Valid
        )
    }

    @Test
    fun testNumberLength() {
        val value = "2133734253" // Valid number without format
        val lengthValue = lengthValidator.validate(value)
        assertTrue(
            "Length should be valid",
            lengthValue is InputFieldManager.Value.Valid
        )
    }

    @Test
    fun testFormattedInvalidNumberLength() {
        val value = "(213) 373-425" // One digit less
        val lengthValue = lengthValidator.validate(value)
        assertTrue(
            "Length should be invalid",
            lengthValue is InputFieldManager.Value.Invalid
        )
    }

    @Test
    fun testEmptyValuePhone() {
        val value = ""
        val phoneValue = phoneValidator.validate(value)
        assertTrue(
            "Phone should be invalid",
            phoneValue is InputFieldManager.Value.Invalid
        )
        val phoneInvalid = phoneValue as InputFieldManager.Value.Invalid
        assertEquals(
            "Error message must match",
            phoneInvalid.reason,
            invalidPhoneMessage
        )
    }

    @Test
    fun testValidFormattedPhone() {
        val value = "(213) 373-4253"
        val phoneValue = phoneValidator.validate(value)
        assertTrue(
            "Phone should be valid",
            phoneValue is InputFieldManager.Value.Valid
        )
    }

    @Test
    fun testValidPhone() {
        val value = "2133734253"
        val phoneValue = phoneValidator.validate(value)
        assertTrue(
            "Phone should be valid",
            phoneValue is InputFieldManager.Value.Valid
        )
    }
}
