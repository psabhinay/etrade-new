package com.etrade.mobilepro.dynamicui.api

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider

interface GenericLayout {

    val viewModelFactory: ViewModelProvider.Factory

    val viewModelClass: Class<out DynamicViewModel>

    val uniqueIdentifier: String

    /**
     * Updates [viewModel] with new data.
     *
     * @param viewModel The view model to refresh.
     */
    fun refresh(viewModel: DynamicViewModel) = Unit
}

/**
 * Converts [GenericLayout] to [DynamicViewModel].
 *
 * @param activity The activity, in whose scope the ViewModel should be retained.
 * @param key The key to use to identify the ViewModel.
 *
 * @return instance of [DynamicViewModel]
 */
fun GenericLayout.toDynamicViewModel(activity: FragmentActivity, key: String): DynamicViewModel {
    return ViewModelProvider(activity, viewModelFactory).get(key, viewModelClass)
}

/**
 * Converts [GenericLayout] to [DynamicViewModel].
 *
 * @param fragment The fragment, in whose scope the ViewModel should be retained.
 * @param key The key to use to identify the ViewModel.
 *
 * @return instance of [DynamicViewModel]
 */
fun GenericLayout.toDynamicViewModel(fragment: Fragment, key: String): DynamicViewModel {
    return ViewModelProvider(fragment, viewModelFactory).get(key, viewModelClass)
}

/**
 * Converts [GenericLayout]s to list of [DynamicViewModel]s.
 *
 * @param fragment The fragment, in whose scope the ViewModel should be retained.
 * @param block The operation to perform on each [DynamicViewModel].
 *
 * @return list of [DynamicViewModel]s
 */
fun Iterable<GenericLayout>.toDynamicViewModels(
    fragment: Fragment,
    shouldRefresh: Boolean = true,
    block: (DynamicViewModel) -> Unit
): List<DynamicViewModel> {
    return map { layout ->
        layout.toDynamicViewModel(fragment, layout.uniqueIdentifier).also {
            if (shouldRefresh) {
                layout.refresh(it)
            }
        }.also(block)
    }
}
