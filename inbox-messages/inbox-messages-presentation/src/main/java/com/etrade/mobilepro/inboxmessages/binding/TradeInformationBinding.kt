package com.etrade.mobilepro.inboxmessages.binding

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.InboxMessagesPopupFragmentArgs
import com.etrade.mobilepro.inboxmessages.R

class TradeInformationBinding : InboxMessageBinding {

    override val layoutRes: Int = R.layout.inbox_message_dashboard_information_view

    private val onItemClickListener = { item: InboxMessage, view: View ->
        val cta = CtaEvent.NavDirectionsNavigation(
            object : NavDirections {
                override fun getArguments(): Bundle = InboxMessagesPopupFragmentArgs(item.message).toBundle()

                override fun getActionId(): Int = R.id.inbox_messages_nav_graph
            }
        )

        view.findNavController().navigate(cta.ctaDirections)
    }

    override fun bind(item: InboxMessage, view: View) {
        view.findViewById<ImageView>(R.id.logo).apply {
            setOnClickListener { onItemClickListener(item, view) }
        }
        view.findViewById<TextView>(R.id.title).apply {
            text = item.title
            setOnClickListener { onItemClickListener(item, view) }
            isSelected = true
        }
        view.findViewById<TextView>(R.id.subtitle).apply {
            text = item.subtitle
            setOnClickListener { onItemClickListener(item, view) }
        }
        view.findViewById<ImageView>(R.id.close).visibility = View.GONE
    }
}
