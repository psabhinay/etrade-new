package com.etrade.mobilepro.chart.netassets;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\r\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\tR\u0018\u0010\f\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u000f\u001a\u0004\b\r\u0010\u000eR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011\u00a8\u0006\u0013"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/DataEntryImpl;", "Lcom/etrade/mobilepro/barchartview/DataEntry;", "x", "", "y", "barColor", "", "(FFI)V", "getBarColor", "()I", "valueColor", "getValueColor", "valueDescriptionColor", "getValueDescriptionColor", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getX", "()F", "getY", "netassets_debug"})
final class DataEntryImpl implements com.etrade.mobilepro.barchartview.DataEntry {
    private final float x = 0.0F;
    private final float y = 0.0F;
    private final int barColor = 0;
    private final int valueColor = 0;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer valueDescriptionColor = null;
    
    public DataEntryImpl(float x, float y, int barColor) {
        super();
    }
    
    @java.lang.Override()
    public float getX() {
        return 0.0F;
    }
    
    @java.lang.Override()
    public float getY() {
        return 0.0F;
    }
    
    @java.lang.Override()
    public int getBarColor() {
        return 0;
    }
    
    @java.lang.Override()
    public int getValueColor() {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Integer getValueDescriptionColor() {
        return null;
    }
}