package com.etrade.mobilepro.trade.conditionalOrderDisclosure.data

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.api.ConditionalOrderDisclosureRepo
import retrofit2.Retrofit
import javax.inject.Inject

class ConditionalOrderDisclosureRepoImpl @Inject constructor(
    private val applicationPreferences: ApplicationPreferences,
    @MobileTrade retrofit: Retrofit
) : ConditionalOrderDisclosureRepo {

    private val apiClient: ConditionalOrderDisclosureApiClient by lazy {
        retrofit.create(ConditionalOrderDisclosureApiClient::class.java)
    }

    override suspend fun isTrailingOrderAllowed(): ETResult<Boolean> {
        return runCatchingET {
            applicationPreferences.isAdvancedOrderDisclosureSigned ||
                applicationPreferences.isConditionalOrderDisclosureSigned
        }
    }

    override suspend fun isDisclosureSigned(): ETResult<Boolean> {
        return runCatchingET {
            applicationPreferences.isConditionalOrderDisclosureSigned
        }
    }

    override suspend fun signDisclosure(): ETResult<List<ServerMessage>> {
        return apiClient.runCatchingET {
            sendAgreement().serverMessages.also { acknowledgeConfirmationResponse() }
        }.onSuccess {
            applicationPreferences.isAdvancedOrderDisclosureSigned = true
            applicationPreferences.isConditionalOrderDisclosureSigned = true
        }
    }

    // A workaround service call that acts like an acknowledgment of the sendAgreement response.
    // This is required in order to continue with "Save Order" without the need for the user to logout and re login.
    private suspend fun acknowledgeConfirmationResponse() {
        val body = AcknowledgeConfirmationDto(requestValueDto = UserDecisionDto("Y"))
        apiClient.sendAgreementConfirmationAcknowledgement(body)
    }
}
