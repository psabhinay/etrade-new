package com.etrade.mobilepro.chat.api.message.repo

import com.etrade.mobilepro.chat.api.ChatParamsRequestModel
import com.etrade.mobilepro.chat.api.message.models.ChatForMobileModel
import com.etrade.mobilepro.chat.api.message.models.ChatRequestModel
import com.etrade.mobilepro.chat.api.message.models.ChatRetrievedMessageModel
import com.etrade.mobilepro.common.result.ETResult

interface ChatMessagesRepo {
    suspend fun postMessage(chatRequestModel: ChatRequestModel.PostMessage): ETResult<ChatForMobileModel>

    suspend fun terminateChat(chatRequestModel: ChatRequestModel.TerminateChat): ETResult<ChatForMobileModel>

    suspend fun getMessages(chatRequestModel: ChatRequestModel.GetMessages): ETResult<ChatRetrievedMessageModel>

    suspend fun getMobileChat(chatRequestModel: ChatParamsRequestModel): ETResult<ChatForMobileModel>
}
