package com.etrade.mobilepro.trade.calculator

import java.math.BigDecimal
import java.math.RoundingMode
import java.text.NumberFormat

internal const val CALCULATOR_ZERO = "0"
internal const val DEFAULT_CALCULATOR_VALUE = "0.00"
internal const val INVALID_DATA = "--"
private const val MINIMUM_FRACTION_DIGITS = 2
private const val MAX_FRACTION_DIGITS = 5
private val REMOVE_ZEROES_IN_FRONT = "[^0-9?!.]".toRegex()

internal fun calculateResult(amount: String?, price: String?): String {
    val amountParsed = amount?.toValueDigital()?.toBigDecimalOrNull() ?: return CALCULATOR_ZERO
    if (amountParsed == CALCULATOR_ZERO.toBigDecimal()) return CALCULATOR_ZERO
    val priceParsed = price?.toValueDigital()?.toBigDecimalOrNull() ?: return CALCULATOR_ZERO
    if (priceParsed == DEFAULT_CALCULATOR_VALUE.toBigDecimal()) {
        return if (amountParsed > BigDecimal.ZERO) {
            INVALID_DATA
        } else {
            CALCULATOR_ZERO
        }
    }
    return amountParsed.divide(priceParsed, RoundingMode.FLOOR).toBigInteger().toString()
}

internal fun String.toValueDigital() =
    if (isEmpty()) {
        DEFAULT_CALCULATOR_VALUE
    } else {
        /**
         * Need to replace ',' for '.' for numerical systems that use a comma as decimal separator.
         */
        NumberFormat.getNumberInstance().apply {
            minimumFractionDigits = MINIMUM_FRACTION_DIGITS
            maximumFractionDigits = MAX_FRACTION_DIGITS
            isGroupingUsed = false
            roundingMode = RoundingMode.HALF_UP
        }.format(BigDecimal(replace(',', '.').replace(REMOVE_ZEROES_IN_FRONT, ""))).replace(',', '.')
    }

internal fun String.removeLastCharacter() = substring(
    0,
    if (length > 0) {
        length - 1
    } else 0
)
