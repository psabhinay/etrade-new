package com.etrade.mobilepro.home.overview.viewfactory

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.etrade.mobilepro.common.DefaultTimeStampReporter
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.TimestampEvent
import com.etrade.mobilepro.util.android.TimestampEventProvider
import com.etrade.mobilepro.util.android.consumable
import com.nhaarman.mockitokotlin2.mock
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

internal class OverviewFooterViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var tested: OverviewFooterViewModel

    private val mutableTsLiveData = MutableLiveData<ConsumableLiveEvent<TimestampEvent>>()

    private val observer = mock<Observer<String>>()

    @Before
    fun setUp() {
        val timestampFormatter: FullTimestampFormatterType = { timestamp ->
            timestamp.toString()
        }
        val timeStampReporter = DefaultTimeStampReporter(user = mock(), timestampFormatter = timestampFormatter)
        tested = OverviewFooterViewModel(
            webViewHelper = mock(),
            customizeViewVisible = true,
            timeStampReporter = timeStampReporter
        )
        tested.addTimestampEventProviders(
            listOf(object : TimestampEventProvider {
                override val timestampEvents = mutableTsLiveData
            })
        )
        tested.timestamp.observeForever(observer)
    }

    @After
    fun tearDown() {
        tested.timestamp.removeObserver(observer)
    }

    @Test
    fun `do not update formatted timestamp for decreased value`() {
        val originalTimestamp = System.currentTimeMillis()
        mutableTsLiveData.postValue(TimestampEvent(originalTimestamp).consumable())

        val decreasedTimestamp = originalTimestamp - 1000
        mutableTsLiveData.postValue(TimestampEvent(decreasedTimestamp).consumable())

        assertEquals(originalTimestamp.toString(), tested.timestamp.value)
    }

    @Test
    fun `update formatted timestamp for increased value`() {
        val originalTimestamp = System.currentTimeMillis()
        mutableTsLiveData.value = TimestampEvent(originalTimestamp).consumable()

        val increasedTimestamp = originalTimestamp + 1000
        mutableTsLiveData.value = TimestampEvent(increasedTimestamp).consumable()

        assertEquals(increasedTimestamp.toString(), tested.timestamp.value)
    }
}
