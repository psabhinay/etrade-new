package com.etrade.mobilepro.watchlist.presentation

import android.content.Context
import android.content.Intent
import com.etrade.mobilepro.editlist.EditListActivity
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.editlist.intent.BaseIntentBuilder
import com.etrade.mobilepro.editlist.intent.ListActionBuilder
import com.etrade.mobilepro.editlist.intent.ListEditDslMarker

/**
 * See [https://etrade.invisionapp.com/share/2CQIYEQUBGJ#/screens/321318236_Watch_Lists_-_Customize_-_Menu].
 */
class EditWatchlistColumnsActivity : EditListActivity<WatchlistColumnWrapper>() {

    override val viewModelClass: Class<out EditListViewModel<WatchlistColumnWrapper>> = EditWatchlistColumnsViewModel::class.java

    companion object {

        /**
         * Creates an intent to start [EditWatchlistColumnsActivity].
         *
         * @param context a context of the application package implementing this class
         * @param init initialization block
         *
         * @return prepared intent
         */
        fun intent(context: Context, init: IntentBuilder.() -> Unit): Intent = IntentBuilder(context).apply(init).intent
    }

    /**
     * Builds an [Intent] to start [EditWatchlistColumnsActivity].
     *
     * @param context a context of the application package implementing this class
     */
    @ListEditDslMarker
    class IntentBuilder(context: Context) : BaseIntentBuilder(Intent(context, EditWatchlistColumnsActivity::class.java)) {

        init {
            listAction {
                isVisible = false
            }
        }

        /**
         * Provide a list's action parameters.
         */
        private fun listAction(init: ListActionBuilder.() -> Unit) = ListActionBuilder(intent).init()
    }
}
