package com.etrade.mobilepro.portfolio.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.dao.realm.BaseRealmDao
import com.etrade.mobilepro.dao.realm.asObservable
import com.etrade.mobilepro.positions.dao.PortfolioPositionDao
import com.etrade.mobilepro.positions.dao.PositionRealmObject
import io.reactivex.Observable
import io.realm.Realm
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Realm implementation of [PortfolioPositionDao].
 *
 * @param realm instance of Realm
 */
class RealmPortfolioPositionDao(realm: Realm) : BaseRealmDao<PositionRealmObject>(realm, PositionRealmObject::class.java), PortfolioPositionDao {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun getPositions(positionIds: List<String>): LiveData<List<PositionRealmObject>> {
        return getAllIn("positionId", positionIds.toTypedArray(), true).map { it.sortedWith(PositionComparator(positionIds)) }
    }

    override fun getPositionsWithSymbol(symbol: String): Observable<List<PositionRealmObject>> {
        return getAllWhere("symbol", symbol).asObservable()
    }

    override fun getPositionsWithSymbolNonAsync(symbol: String): List<PositionRealmObject> {
        return Realm.getInstance(realm.configuration).use {
            it.copyFromRealm(it.where(classObj).equalTo("symbol", symbol).findAll())
        }
    }

    override fun getAccountPositionsAsObservable(accountId: String): Observable<List<PositionRealmObject>> {
        return getAllWhere("accountId", accountId).asObservable()
    }

    override fun getAccountPositions(accountId: String): List<PositionRealmObject> =
        Realm.getInstance(realm.configuration).use { realmInstance ->
            realmInstance.where(classObj).equalTo("accountId", accountId).findAll().let { results ->
                results.realm.copyFromRealm(results)
            }
        }

    override fun getAccountPositionsWithUuid(accountUuid: String): List<PositionRealmObject> =
        Realm.getInstance(realm.configuration).use { realmInstance ->
            realmInstance.where(classObj).equalTo("accountUuid", accountUuid).findAll().let { results ->
                results.realm.copyFromRealm(results)
            }
        }

    override fun updatePositions(positions: List<PositionRealmObject>) {
        runCatching {
            realm.executeTransactionAsync { bgRealm ->
                val existingPositions = bgRealm.where(classObj).findAll()
                val positionUpdateMap = positions.associateBy { it.positionId }
                for (existingPosition in existingPositions) {
                    existingPosition.updateWithStreamUpdatedObject(positionUpdateMap[existingPosition.positionId])
                }
            }
        }.onFailure {
            logger.error("Failed to save positions", it)
        }
    }

    override fun deleteInvalidPositions(accountUuid: String, validPositionIds: List<String>) {
        runCatching {
            realm.executeTransactionAsync {
                val positions = it.where(classObj).not().`in`("positionId", validPositionIds.toTypedArray())
                    .and()
                    .equalTo("accountUuid", accountUuid)
                    .findAll()
                positions.deleteAllFromRealm()
            }
        }.onFailure {
            logger.error("Failed to delete positions", it)
        }
    }
}

private class PositionComparator(private val ids: List<String>) : Comparator<PositionRealmObject> {
    override fun compare(o1: PositionRealmObject, o2: PositionRealmObject): Int = ids.indexOf(o1.positionId) - ids.indexOf(o2.positionId)
}
