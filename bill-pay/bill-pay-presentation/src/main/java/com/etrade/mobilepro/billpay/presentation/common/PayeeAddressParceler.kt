package com.etrade.mobilepro.billpay.presentation.common

import android.os.Parcel
import com.etrade.mobilepro.billpay.api.PayeeAddress
import kotlinx.android.parcel.Parceler

object PayeeAddressParceler : Parceler<PayeeAddress> {

    override fun create(parcel: Parcel): PayeeAddress = parcel.run {
        PayeeAddress(
            address1 = readString(),
            address2 = readString(),
            city = readString(),
            state = readString(),
            zipCode = readString()
        )
    }

    override fun PayeeAddress.write(parcel: Parcel, flags: Int) = parcel.run {
        writeString(address1)
        writeString(address2)
        writeString(city)
        writeString(state)
        writeString(zipCode)
    }
}
