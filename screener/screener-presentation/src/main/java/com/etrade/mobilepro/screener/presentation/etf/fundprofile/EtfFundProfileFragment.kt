package com.etrade.mobilepro.screener.presentation.etf.fundprofile

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.screener.api.EtfFundGroup
import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.FundFamily
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.etf.EtfFundProfileFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.FragmentEtfFundProfileBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.toggleCheckedState
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.button.MaterialButtonToggleGroup
import javax.inject.Inject

private const val FUND_FAMILY_TAG = "etf fund family tag"
private const val FUND_GROUP_TAG = "etf fund group tag"
private const val FUND_CATEGORY_TAG = "etf fund category tag"

private const val MORNING_STAR_1 = 1
private const val MORNING_STAR_2 = 2
private const val MORNING_STAR_3 = 3
private const val MORNING_STAR_4 = 4
private const val MORNING_STAR_5 = 5

class EtfFundProfileFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<EtfFundProfileFilter, EtfFundProfileViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {
    override val layoutRes: Int = R.layout.fragment_etf_fund_profile
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.ETF
    private val binding by viewBinding(FragmentEtfFundProfileBinding::bind) { onDestroyBinding() }

    override val viewModel by viewModels<EtfFundProfileViewModel> { viewModelFactory }

    private lateinit var fundFamilySelector: DropDownMultiSelector<FundFamily>
    private lateinit var etfFundGroupSelector: DropDownMultiSelector<EtfFundGroup>
    private lateinit var fundCategorySelector: DropDownMultiSelector<FundCategory>
    private val handler = Handler(Looper.getMainLooper())
    private val buttonGroupListener =
        MaterialButtonToggleGroup.OnButtonCheckedListener { _, checkedId, isChecked ->
            val ratingPair = when (checkedId) {
                binding.morningstarRating.button1.id -> Pair(MORNING_STAR_1, binding.morningstarRating.button1)
                binding.morningstarRating.button2.id -> Pair(MORNING_STAR_2, binding.morningstarRating.button2)
                binding.morningstarRating.button3.id -> Pair(MORNING_STAR_3, binding.morningstarRating.button3)
                binding.morningstarRating.button4.id -> Pair(MORNING_STAR_4, binding.morningstarRating.button4)
                binding.morningstarRating.button5.id -> Pair(MORNING_STAR_5, binding.morningstarRating.button5)
                else -> throw IllegalArgumentException("Unknown rating button")
            }
            ratingPair.second.toggleCheckedState(isChecked, handler)
            viewModel.morningStarRatingToggled(ratingPair.first, isChecked)
        }
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<EtfFundProfileFilter, EtfFundProfileViewModel.ViewState, Boolean>>
        get() = mapOf(
            binding.bottomTwentyPercent to ExpenseRatio.BOTTOM_20,
            binding.belowMidTwentyPercent to ExpenseRatio.BELOW_MID_20,
            binding.midTwentyPercent to ExpenseRatio.MID_20,
            binding.aboveMidTwentyPercent to ExpenseRatio.ABOVE_MID_20,
            binding.topTwentyPercent to ExpenseRatio.TOP_20,
            binding.leveragedEtfsExclude to LeveragedETfs.EXCLUDE,
            binding.leveragedEtfsShowOnly to LeveragedETfs.SHOW_ONLY,
            binding.exchangedTradedNotesExclude to ExchangeTradedNotes.EXCLUDE,
            binding.exchangedTradedNotesShowOnly to ExchangeTradedNotes.SHOW_ONLY
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
    }

    private fun FragmentEtfFundProfileBinding.onDestroyBinding() {
        morningstarRating.buttonGroup.removeOnButtonCheckedListener(buttonGroupListener)
    }

    override fun onDestroyView() {
        handler.removeCallbacksAndMessages(null)
        super.onDestroyView()
    }

    private fun setupFragment() {
        setupDropDownLists()
        setupDropDownListsClickListeners()
        setupInfoPopups()
        setupSwitchListeners()
        binding.morningstarRating.buttonGroup.addOnButtonCheckedListener(buttonGroupListener)
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                updateFunds(viewState = it)
                updateCheckableButtons(viewState = it)
                updateMorningStar(viewState = it)
                updateSwitches(viewState = it)
            }
        )
        viewModel.stageData.observe(
            viewLifecycleOwner,
            Observer {
                binding.fundCategory.selectionEnabled = it !is Resource.Loading
            }
        )
    }

    private fun updateFunds(viewState: EtfFundProfileViewModel.ViewState) {
        with(binding) {
            fundFamily.selectedValue = viewState.filter.fundFamily.title
            fundGroup.selectedValue = viewState.filter.etfFundGroup.title
            fundCategory.selectedValue = when (viewState.filter.fundCategories.size) {
                0 -> getString(R.string.fund_profile_fund_all_categories)
                1 -> viewState.filter.fundCategories.first().title
                else -> getString(R.string.multiple_items_selected)
            }
        }
    }

    private fun updateCheckableButtons(viewState: EtfFundProfileViewModel.ViewState) {
        binding.apply {
            bottomTwentyPercent.isChecked = viewState.filter.bottom20
            belowMidTwentyPercent.isChecked = viewState.filter.belowMid20
            midTwentyPercent.isChecked = viewState.filter.mid20
            aboveMidTwentyPercent.isChecked = viewState.filter.aboveMid20
            topTwentyPercent.isChecked = viewState.filter.top20
            leveragedEtfsExclude.isChecked = viewState.filter.leveragedEtfsExclude
            leveragedEtfsShowOnly.isChecked = viewState.filter.leveragedEtfsShowOnly
            exchangedTradedNotesExclude.isChecked = viewState.filter.exchangeTradedNotesExclude
            exchangedTradedNotesShowOnly.isChecked = viewState.filter.exchangeTradedNotesShowOnly
        }
    }

    private fun updateMorningStar(viewState: EtfFundProfileViewModel.ViewState) {
        binding.apply {
            morningstarRating.button1.toggleCheckedState(viewState.filter.morningStarRating.contains(MORNING_STAR_1), handler)
            morningstarRating.button2.toggleCheckedState(viewState.filter.morningStarRating.contains(MORNING_STAR_2), handler)
            morningstarRating.button3.toggleCheckedState(viewState.filter.morningStarRating.contains(MORNING_STAR_3), handler)
            morningstarRating.button4.toggleCheckedState(viewState.filter.morningStarRating.contains(MORNING_STAR_4), handler)
            morningstarRating.button5.toggleCheckedState(viewState.filter.morningStarRating.contains(MORNING_STAR_5), handler)
        }
    }

    private fun updateSwitches(viewState: EtfFundProfileViewModel.ViewState) {
        binding.switchAllStarsFund.isChecked = viewState.filter.allStarsFunds
        binding.switchOptionsAvailableEtf.isChecked = viewState.filter.optionsAvailable
    }

    @Suppress("LongMethod") // already grouped
    private fun setupDropDownLists() {
        fundFamilySelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = FUND_FAMILY_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.fund_profile_title_fund_family),
                itemsData = viewModel.fundFamilyData,
                selectListener = { _, selected ->
                    viewModel.fundFamilySelected(selected.value)
                }
            )
        )
        etfFundGroupSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = FUND_GROUP_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.fund_profile_title_fund_group),
                snackbarUtilFactory = snackbarUtilFactory,
                itemsData = viewModel.etfFundGroupData,
                selectListener = { _, selected ->
                    viewModel.fundGroupSelected(selected.value)
                },
                onCancelListener = {
                    viewModel.didSelectItem()
                },
                selectionFlow = DropDownMultiSelector.SelectionFlow(
                    willSelectListener = { pos, item ->
                        viewModel.willSelectItem(pos, item)
                    },
                    didSelectListener = { _, _ ->
                        viewModel.didSelectItem()
                    },
                    stageData = viewModel.stageData,
                    onRetry = { pos, item ->
                        viewModel.willSelectItem(pos, item)
                    }
                )
            )
        )
        fundCategorySelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = FUND_CATEGORY_TAG,
                multiSelectEnabled = true,
                title = getString(R.string.fund_profile_title_fund_category),
                itemsData = viewModel.fundCategoryData,
                selectListener = { _, selected ->
                    viewModel.fundCategorySelected(selected.value)
                },
                onCancelListener = { fallBackValues ->
                    viewModel.categorySelectionCancelled(fallBackValues)
                },
                eventListener = { event, dropDownDialog ->
                    when (event) {
                        DropDownMultiSelector.Settings.Event.CLEAR_ALL -> viewModel.fundCategoryCleared()
                        DropDownMultiSelector.Settings.Event.SAVE -> dropDownDialog.get()?.dismiss()
                    }
                }
            ),
            fallBackValuesProvider = {
                viewModel.prevCategorySelection
            }
        )
    }

    private fun setupDropDownListsClickListeners() {
        with(binding) {
            fundFamily.setOnClickListener {
                fundFamilySelector.open()
            }
            fundGroup.setOnClickListener {
                etfFundGroupSelector.open()
            }
            fundCategory.setOnClickListener {
                viewModel.backupFundCategorySelection()
                fundCategorySelector.open()
            }
        }
    }

    @Suppress("LongMethod") // nothing special here
    private fun setupInfoPopups() {
        binding.apply {
            infoAllStarsFund.setOnClickListener {
                screenersRouter.actionShowPopup(findNavController(), getString(R.string.fund_profile_info_all_stars_fund))
            }
            infoMorningstarRating.setOnClickListener {
                screenersRouter.actionShowPopup(findNavController(), getString(R.string.etf_fund_profile_info_morningstar_rating))
            }
            exchangedTradedNotesInfo.setOnClickListener {
                screenersRouter.actionShowPopup(findNavController(), getString(R.string.fund_profile_info_exchange_traded_notes))
            }
            leveragedEtfsInfo.setOnClickListener {
                screenersRouter.actionShowPopup(findNavController(), getString(R.string.fund_profile_info_leveraged_etfs_info))
            }
        }
    }

    private fun setupSwitchListeners() {
        binding.apply {
            switchAllStarsFund.setOnCheckedChangeListener { _, isChecked ->
                viewModel.consumeEvent(FundSwitch.ALL_STAR, isChecked)
            }
            switchOptionsAvailableEtf.setOnCheckedChangeListener { _, isChecked ->
                viewModel.consumeEvent(FundSwitch.OPTIONS_AVAILABLE, isChecked)
            }
        }
    }
}
