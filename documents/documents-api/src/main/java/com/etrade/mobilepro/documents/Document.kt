package com.etrade.mobilepro.documents

interface Document {
    val accountUuid: String
    val documentId: String
    val title: String
    val institutionType: String
    val postEdgeFolder: String
    val headerTitle: String?
    val filename: String?

    fun updateFilename(filename: String?): Document
}
