package com.etrade.mobilepro.barcode.data

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class BarcodeHistoryRealmItem(
    var upc: String = "",
    var companyName: String = "",
    var productName: String = "",
    var timeInMilliseconds: Long = 0L,
    var symbol: String = "",
    @PrimaryKey
    var id: String = upc + timeInMilliseconds
) : RealmObject()
