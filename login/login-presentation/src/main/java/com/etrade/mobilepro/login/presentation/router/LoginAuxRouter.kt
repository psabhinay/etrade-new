package com.etrade.mobilepro.login.presentation.router

import android.content.Context

interface LoginAuxRouter {
    fun navigateToEula(context: Context)
}
