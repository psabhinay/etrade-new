package com.etrade.mobilepro.markets.data.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ErrMsgBuffer(
    @Json(name = "errMsg")
    var errMsg: String? = null
)
