package com.etrade.mobilepro.alerts

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.etrade.mobilepro.R
import com.etrade.mobilepro.alerts.api.model.AlertAction
import com.etrade.mobilepro.alerts.presentation.AlertsSharedViewModel
import com.etrade.mobilepro.alerts.presentation.manage.ManageAlertViewModel
import com.etrade.mobilepro.alerts.presentation.manage.adapter.UserAlertsAdapter
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.databinding.FragmentManageAlertsBinding
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@RequireLogin
class ManageAlertFragment @Inject constructor(
    val viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.fragment_manage_alerts) {

    private val binding by viewBinding(FragmentManageAlertsBinding::bind)

    private val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private var snackBar: Snackbar? = null
    private val viewModel by viewModels<ManageAlertViewModel> { viewModelFactory }
    private val sharedViewModel by activityViewModels<AlertsSharedViewModel> { viewModelFactory }

    private val alertsAdapter = UserAlertsAdapter(
        alertListener = { alert, action ->
            viewModel.alertAction(alert, action)
        },
        swipeRevealListener = { alert, action ->
            viewModel.deleteButtonAction(alert, action)
        },
        dragLocker = {
            false
        }
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        setupFragment()
        bindViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupFragment() {
        with(binding.alertsRv) {
            layoutManager =
                LinearLayoutManager(activity).apply { orientation = RecyclerView.VERTICAL }
            setupRecyclerViewDivider(R.drawable.thin_divider)
            adapter = alertsAdapter
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
        }
        binding.swipeRefresh.apply {
            applyColorScheme()
            setOnRefreshListener {
                viewModel.refresh()
            }
        }
    }

    @Suppress("LongMethod", "ComplexMethod")
    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                binding.swipeRefresh.isRefreshing = it is ManageAlertViewModel.ViewState.Loading
                when (it) {
                    ManageAlertViewModel.ViewState.Loading -> {
                        snackBar?.dismiss()
                    }
                    is ManageAlertViewModel.ViewState.Success -> {
                        if (it.alerts.isEmpty()) {
                            binding.noItemsTv.visibility = View.VISIBLE
                        } else {
                            binding.noItemsTv.visibility = View.GONE
                        }
                        binding.alertsRv.post {
                            alertsAdapter.submitAlerts(it.alerts)
                        }
                    }
                    ManageAlertViewModel.ViewState.Error.Loading -> {
                        snackBar = snackbarUtil.retrySnackbar {
                            viewModel.refresh()
                        }
                    }
                    is ManageAlertViewModel.ViewState.Error.Deleting -> {
                        alertsAdapter.submitAlerts(it.actualAlerts)
                        snackBar = snackbarUtil.retrySnackbar {
                            viewModel.alertAction(it.failedAlert, AlertAction.DELETE)
                        }
                    }
                }
            }
        )
        viewModel.viewEffects.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    ManageAlertViewModel.ViewEffects.AlertWasDeleted -> {
                        snackBar = snackbarUtil.snackbar(getString(R.string.alert_was_removed), Snackbar.LENGTH_SHORT)?.apply { show() }
                    }
                    is ManageAlertViewModel.ViewEffects.AlertWasTappedOn -> {
                        sharedViewModel.selectedUserAlert = it.alert
                        findNavController().navigate(R.id.userAlertsDetailFragment)
                    }
                }
            }
        )
        sharedViewModel.deletedUserAlert.observe(
            viewLifecycleOwner,
            Observer {
                snackBar = snackbarUtil.snackbar(getString(R.string.alert_was_removed), Snackbar.LENGTH_SHORT)?.apply { show() }
                viewModel.removeAlertFromList(it)
            }
        )
        sharedViewModel.alertWasUpdated.observe(
            viewLifecycleOwner,
            Observer {
                snackbarUtil.snackbar(it, Snackbar.LENGTH_SHORT)?.apply { show() }
                viewModel.refresh()
            }
        )
    }
}
