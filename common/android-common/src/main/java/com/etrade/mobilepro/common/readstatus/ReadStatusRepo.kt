package com.etrade.mobilepro.common.readstatus

import io.realm.RealmConfiguration
import java.util.concurrent.TimeUnit

interface ReadStatusRepo {

    val realmConfig: RealmConfiguration

    fun markAsRead(entityId: String, type: ReadableType)
    fun wasRead(entityId: String): Boolean
    fun markAsRead(entityIds: List<String>, type: ReadableType)
    fun clearExpiredReadableEntities(elapsedTimeInMillis: Long = TimeUnit.DAYS.toMillis(1), type: ReadableType)
    fun clearAll()
}
