package com.etrade.mobilepro.positions

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.common.di.RemoteDynamicScreen
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.dynamic.form.fragment.createFormValuesArgument
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.home.overview.viewmodel.OVERVIEW_CACHE_EXPIRATION_TIME
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.safeUnderlyingSymbolInfo
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.orders.api.OrderAction
import com.etrade.mobilepro.portfolio.PortfolioRecordsViewModel
import com.etrade.mobilepro.portfolio.PortfolioSettingsRepository
import com.etrade.mobilepro.portfolio.PortfolioTableMode
import com.etrade.mobilepro.portfolio.PortfolioViewModel
import com.etrade.mobilepro.portfolio.addLegsToTradeParams
import com.etrade.mobilepro.portfolio.edit.EditPortfolioTableActivity
import com.etrade.mobilepro.portfolio.getTradeFormParams
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.SortOrder
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.evrencoskun.tableview.preference.Preferences
import com.squareup.moshi.Moshi
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import org.slf4j.LoggerFactory
import javax.inject.Provider

private const val MAX_NUM_OPTIONS_TO_ROLL = 2
private const val PORTFOLIO_CACHE_EXPIRATION_EXPIRATION = OVERVIEW_CACHE_EXPIRATION_TIME

abstract class PositionListViewModel(
    @RemoteDynamicScreen
    private val dynamicScreenRepo: DynamicScreenRepo,
    private val moshi: Moshi,
    private val tradingDefaultsPreferences: TradingDefaultsPreferences,
    private val darkModeChecker: DarkModeChecker,
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    private val portfolioSettingsRepository: PortfolioSettingsRepository,
    private val positionTableMode: PositionTableMode
) : ViewModel() {

    private val _viewState: MutableLiveData<ViewState> = MutableLiveData()
    val viewState: LiveData<ViewState>
        get() = _viewState

    private val _scrollState: MutableLiveData<ConsumableLiveEvent<Preferences>> = MutableLiveData()
    val scrollState: LiveData<ConsumableLiveEvent<Preferences>>
        get() = _scrollState

    val optionsToRoll: LiveData<List<DropDownMultiSelector.Selectable<PortfolioRecord>>>
        get() = _optionsToRoll
    private val _optionsToRoll: MutableLiveData<List<DropDownMultiSelector.Selectable<PortfolioRecord>>> =
        MutableLiveData()

    private var currentRollingAccountId: String? = null
    private val selectedOptionsToRoll: MutableList<PortfolioRecord> = mutableListOf()

    private val disposable = CompositeDisposable()
    private val logger = LoggerFactory.getLogger(PortfolioViewModel::class.java)

    private var sorting: SortInfo = SortInfo(PortfolioRecordColumn.defaultColumn, SortOrder.defaultOrder)

    private var previousScreenRequest: ScreenRequest? = null

    val dataUpdated: LiveData<ConsumableLiveEvent<Boolean>>
        get() = _dataUpdated
    protected val _dataUpdated = MutableLiveData<ConsumableLiveEvent<Boolean>>()

    val tableViewIndex: Int
        get() = when (positionTableMode) {
            PositionTableMode.PORTFOLIO_VIEW -> portfolioSettingsRepository.tableMode
            else -> PortfolioTableMode.STANDARD
        }.index

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    fun onCustomizeViewClicked(context: Context) {
        with(context) {
            val intent = EditPortfolioTableActivity.intent(this) {
                positionTableMode = this@PositionListViewModel.positionTableMode
                portfolioTableViewType = PortfolioTableViewType.values()[tableViewIndex]
            }
            startActivity(intent)
        }
    }

    /**
     * Refreshes underlying data.
     *
     * @param sortInfo sorting state to use
     */
    fun refresh(sortInfo: SortInfo = sorting) {
        val screenRequest = if (sortInfo != sorting) {
            getScreenRequest(sortInfo)
        } else {
            previousScreenRequest ?: getScreenRequest(sortInfo)
        }

        // reset previous screen request to allow fetching
        previousScreenRequest = null

        fetchData(screenRequest, false)
    }

    protected abstract fun getScreenRequest(sortInfo: SortInfo = sorting): ScreenRequest

    /**
     * Fetches portfolio data.
     */
    fun fetchData(screenRequest: ScreenRequest = previousScreenRequest ?: getScreenRequest(), useCache: Boolean = true) {
        if (previousScreenRequest == screenRequest) {
            return
        }

        _dataUpdated.value = false.consumable()

        _viewState.value = ViewState.Loading

        disposable.add(
            dynamicScreenRepo.getScreen(screenRequest, true, PORTFOLIO_CACHE_EXPIRATION_EXPIRATION.takeIf { useCache }).subscribeBy(
                onNext = { response ->
                    val state = when (response) {
                        is Resource.Success -> ViewState.Idle(response.data ?: emptyList(), darkModeChecker.uiMode)
                        is Resource.Loading -> ViewState.Loading
                        is Resource.Failed -> ViewState.Error
                    }.also {
                        previousScreenRequest = screenRequest
                    }
                    _viewState.postValue(state)
                },
                onError = {
                    _viewState.postValue(ViewState.Error)
                    logger.warn("Fetching data has failed", it)
                }
            )
        )
    }

    fun attach(portfolioRecordsViewModel: PortfolioRecordsViewModel) {
        portfolioRecordsViewModel.resumeStreaming()
    }

    fun detach(portfolioRecordsViewModel: PortfolioRecordsViewModel) {
        portfolioRecordsViewModel.pauseStreaming()
    }

    fun saveScrollState(scrollState: Preferences) {
        _scrollState.value = ConsumableLiveEvent(scrollState)
    }

    fun getTradeDirections(portfolioRecord: PortfolioRecord, accountId: String?, orderAction: OrderAction): NavDirections {
        val recInstrument = portfolioRecord.instrument
        val instrumentType = recInstrument.instrumentType

        val formParams = getTradeFormParams(
            portfolioRecord,
            accountId,
            orderAction,
            tradeFormParametersBuilderProvider,
            tradingDefaultsPreferences.getDefaultQuantityWithInstrumentType(instrumentType)
        )
        val formBundle = createFormValuesArgument(moshi, formParams)

        return MainGraphDirections.actionGlobalTradeBottomSheetFragment(formBundle)
    }

    fun getRollOptionsDirections(accountIdSelected: String?, recordList: List<PortfolioRecord>): NavDirections? {
        if (recordList.isNotEmpty()) {
            val recInstrument = recordList[0].instrument
            val symbolInfo = recInstrument.safeUnderlyingSymbolInfo
            val formParams = tradeFormParametersBuilderProvider.get()
                .apply {
                    symbol = symbolInfo
                    accountId = accountIdSelected
                    addLegsToTradeParams(
                        recordList,
                        tradingDefaultsPreferences.getDefaultQuantityWithInstrumentType(
                            InstrumentType.OPTN
                        )
                    )
                }
                .create(SecurityType.OPTION)
            val formBundle = createFormValuesArgument(moshi, formParams)

            return MainGraphDirections.actionGlobalTradeBottomSheetFragment(
                formBundle,
                true,
                SearchResultItem.Symbol(title = symbolInfo.symbol, type = symbolInfo.instrumentType)
            )
        } else {
            return null
        }
    }

    fun getRollTwoOptionsDirections(): NavDirections? {
        val selectedOptionsToRollOrdered = _optionsToRoll.value?.mapNotNull {
            if (it.selected) {
                it.value
            } else {
                null
            }
        } ?: emptyList()

        return getRollOptionsDirections(currentRollingAccountId, selectedOptionsToRollOrdered)
    }

    fun rollTwoOptions(
        portfolioRecordsViewModel: PortfolioRecordsViewModel,
        position: Int
    ): Boolean {
        selectedOptionsToRoll.clear()
        currentRollingAccountId = portfolioRecordsViewModel.accountId
        _optionsToRoll.value = portfolioRecordsViewModel.peekRecord(position)?.instrument?.let {
            portfolioRecordsViewModel.findOptionsPositions(it.underlyingSymbolInfo?.symbol).map { record ->
                DropDownMultiSelector.Selectable(
                    title = record.instrument.displaySymbol,
                    value = record,
                    selected = false
                )
            }
        } ?: emptyList()

        return _optionsToRoll.value?.isNotEmpty() == true
    }

    fun toggleSelectionOptionToRoll(selectedRecord: PortfolioRecord) {
        val existingRecord = selectedOptionsToRoll.find { it.recordId == selectedRecord.recordId }
        if (existingRecord != null) {
            selectedOptionsToRoll.remove(existingRecord)
        } else {
            selectedOptionsToRoll.add(selectedRecord)
        }
        updateOptionsToRollSelection()
    }

    fun clearSelectedOptionsToRoll() {
        selectedOptionsToRoll.clear()
        updateOptionsToRollSelection()
    }

    private fun updateOptionsToRollSelection() {
        _optionsToRoll.updateValue {
            it.map { selectableItem ->
                val isSelected =
                    selectedOptionsToRoll.any { selected -> selected.recordId == selectableItem.value.recordId }
                DropDownMultiSelector.Selectable(
                    title = selectableItem.title,
                    value = selectableItem.value,
                    selected = isSelected,
                    enabled = selectedOptionsToRoll.size < MAX_NUM_OPTIONS_TO_ROLL || isSelected
                )
            }
        }
    }

    /**
     * Current view state.
     */
    sealed class ViewState {

        /**
         * There is nothing going on.
         */
        data class Idle(val layouts: List<GenericLayout>, val uiMode: Int) : ViewState()

        /**
         * Some data is being loaded.
         */
        object Loading : ViewState()

        /**
         * An error happened.
         */
        object Error : ViewState()
    }
}
