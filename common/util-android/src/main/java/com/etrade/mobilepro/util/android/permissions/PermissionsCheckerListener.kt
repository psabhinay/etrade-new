package com.etrade.mobilepro.util.android.permissions

interface MultiplePermissionsCheckerListener {

    fun onMultiplePermissionsResult(response: MultiplePermissionsResponse)
}

interface SinglePermissionCheckerListener {

    fun onPermissionGranted(response: PermissionGrantedResponse) {}

    fun onPermissionDenied(response: PermissionDeniedResponse) {}

    fun onPermissionRationaleShouldBeShown(permission: String) {}
}

class PermissionDeniedResponse(val permission: String, val permanentlyDenied: Boolean, val shouldShowRequestPermissionRationale: Boolean = !permanentlyDenied)

data class PermissionGrantedResponse(val permission: String)

class MultiplePermissionsResponse(
    val permissionsGranted: List<PermissionGrantedResponse>,
    val permissionsDenied: List<PermissionDeniedResponse>
)
