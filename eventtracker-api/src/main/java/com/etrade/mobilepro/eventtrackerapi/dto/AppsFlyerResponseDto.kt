package com.etrade.mobilepro.eventtrackerapi.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AppsFlyerResponseDto(
    @Json(name = "controller_id")
    val controllerID: String? = null,
    @Json(name = "expected_completion_time")
    val expectedCompletionTime: String? = null,
    @Json(name = "status_callback_url")
    val statusCallBackUrl: String? = null,
    @Json(name = "Subject_request_id")
    val subjectRequestId: String? = null,
    @Json(name = "Request_status")
    val requestStatus: String? = null
)
