package com.etrade.mobilepro.screener.data.json.mutualfund

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.ExpensesFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val EXPENSES_FIELD = "ExpenseRatio"

class ExpensesFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val expensesFilter = screenerFilter as ExpensesFilter
        val arguments = mutableListOf<Argument>()

        if (expensesFilter.isNotEmpty()) {
            arguments.add(
                Argument(getExpensesClauses(expensesFilter), EXPENSES_FIELD)
            )
        }

        return PartialScreenerParams(arguments)
    }

    private fun getExpensesClauses(expensesFilter: ExpensesFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (expensesFilter.expenseRatioLessThan5) {
            clauses.addAll(expenseRatioLessThan5Clauses)
        }
        if (expensesFilter.expenseRatio5to1) {
            clauses.addAll(expenseRatio5to1Clauses)
        }
        if (expensesFilter.expenseRatio1to15) {
            clauses.addAll(expenseRatio1to15Clauses)
        }
        if (expensesFilter.expenseRatio15to2) {
            clauses.addAll(expenseRatio15to2Clauses)
        }
        if (expensesFilter.expenseRatioGreaterThan2) {
            clauses.addAll(expenseRatioGreaterThan2Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private val expenseRatioLessThan5Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf(".5"))
    )
    private val expenseRatio5to1Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf(".5")),
        Clause(OPERATOR_LESS_THAN, listOf("1"))
    )
    private val expenseRatio1to15Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("1")),
        Clause(OPERATOR_LESS_THAN, listOf("1.5"))
    )
    private val expenseRatio15to2Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("1.5")),
        Clause(OPERATOR_LESS_THAN, listOf("2"))
    )
    private val expenseRatioGreaterThan2Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("2")),
        Clause(OPERATOR_LESS_THAN, listOf("100"))
    )
}
