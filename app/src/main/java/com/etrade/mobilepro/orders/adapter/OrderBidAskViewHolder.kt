package com.etrade.mobilepro.orders.adapter

import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem
import com.etrade.mobilepro.quote.presentation.databinding.QuoteWidgetBidAskBinding
import com.etrade.mobilepro.util.android.binding.resources

class OrderBidAskViewHolder(
    private val binding: QuoteWidgetBidAskBinding,
    lifecycleOwner: LifecycleOwner
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.lifecycleOwner = lifecycleOwner
    }

    fun bindTo(item: OrderDetailDisplayItem.OrderBidAskItem?) {
        with(binding) {
            if (item != null) {
                quoteAskTitleView.text = resources.getString(item.askLabel)
                quoteBidTitleView.text = resources.getString(item.bidLabel)
                quoteBidAskItem = item
            } else {
                quoteAskPriceView.setText(R.string.empty_default)
                quoteBidPriceView.setText(R.string.empty_default)
            }
        }
    }
}
