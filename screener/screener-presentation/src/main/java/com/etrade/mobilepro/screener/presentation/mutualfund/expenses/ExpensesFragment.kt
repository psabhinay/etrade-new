package com.etrade.mobilepro.screener.presentation.mutualfund.expenses

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.mutualfund.ExpensesFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.FragmentExpensesBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class ExpensesFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<ExpensesFilter, ExpensesViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {

    private val binding by viewBinding(FragmentExpensesBinding::bind)
    override val viewModel by viewModels<ExpensesViewModel> { viewModelFactory }
    override val layoutRes: Int = R.layout.fragment_expenses
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<ExpensesFilter, ExpensesViewModel.ViewState, Boolean>>
        get() = mapOf(
            binding.lessThanHalfPercent to ExpenseRatio.LESS_THAN_DOT_5,
            binding.halfToOnePercent to ExpenseRatio.DOT_5_TO_1,
            binding.oneToOneDotFivePercent to ExpenseRatio.ONE_TO_1_DOT_5,
            binding.oneDotFiveToTwoPercent to ExpenseRatio.ONE_DOT_5_TO_2,
            binding.moreThanTwoPercent to ExpenseRatio.GREATER_THAN_2
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                it.filter.apply {
                    binding.apply {
                        lessThanHalfPercent.isChecked = expenseRatioLessThan5
                        halfToOnePercent.isChecked = expenseRatio5to1
                        oneToOneDotFivePercent.isChecked = expenseRatio1to15
                        oneDotFiveToTwoPercent.isChecked = expenseRatio15to2
                        moreThanTwoPercent.isChecked = expenseRatioGreaterThan2
                    }
                }
            }
        )
    }
}
