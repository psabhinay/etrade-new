package com.etrade.mobilepro.orders.presentation.section.viewfactory

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderDetailDescription
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.ExecInstruction
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.orders.api.MarketSession
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.presentation.R
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import java.math.BigDecimal

internal const val START_INDEX = 0
internal const val END_INDEX = 4
private val dnrOrderTerms = listOf(OrderTerm.GOOD_UNTIL_CANCEL_DNR, OrderTerm.GOOD_TILL_DATE_DNR)
private val dniOrderTerms = listOf(OrderTerm.GOOD_UNTIL_CANCEL_DNI, OrderTerm.GOOD_TILL_DATE_DNI)

fun getDatesSections(
    detailsDto: BaseOrderDetailsItem.OrderDetailsItem,
    resources: OrderDetailLabelResources,
    showStrategy: Boolean
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    val stockResources = resources.defaultResources.resource

    if (showStrategy && detailsDto.orderStatus != OrderStatus.SAVED && !detailsDto.isAdvancedOrder) {
        detailsDto.strategyType?.let {
            sections.add(createSummarySection(resources.stock.strategy, stockResources.getString(it.getLabel())))
        }
    }
    detailsDto.orderNumber.takeIf { it.toInt() != 0 }?.let {
        sections.add(createSummarySection(resources.stock.orderNumber, it))
    }

    detailsDto.orderPlacedDate?.let {
        sections.add(createSummarySection(resources.stock.orderPlaced, it))
    }
    if (!detailsDto.hasPartialExecution) {
        detailsDto.executedDate?.let {
            sections.add(createSummarySection(resources.stock.executed, it))
        }
    }
    detailsDto.cancelledRequestedDate?.let {
        sections.add(createSummarySection(resources.stock.cancelRequested, it))
    }
    detailsDto.cancelledDate?.let {
        sections.add(createSummarySection(resources.stock.cancelledDate, it))
    }
    detailsDto.expiredData?.let {
        sections.add(createSummarySection(resources.stock.expired, it))
    }
    detailsDto.systemRejectedData?.let {
        sections.add(createSummarySection(resources.stock.systemRejected, it))
    }
    return sections
}

fun getPriceSections(
    detailsDto: BaseOrderDetailsItem.OrderDetailsItem,
    resources: OrderDetailLabelResources,
    isConditionalOrder: Boolean
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    val stockResources = resources.defaultResources.resource

    if (detailsDto.goodTillDateFlag) {
        val expirationDate = detailsDto.gtdExpirationData ?: DateFormattingUtils.formatToShortDate(0)
        sections.add(createSummarySection(resources.stock.term, String.format(resources.stock.orderTermGTD, expirationDate)))
    } else if (detailsDto.marketSession == MarketSession.MARKET_OVERNIGHT) {
        sections.add(createSummarySection(resources.stock.term, resources.stock.orderTermNight))
    } else {
        if (detailsDto.origSysCode == OrigSysCode.ORIG_SYS_CODE_DETW && detailsDto.term == OrderTerm.EXTENDED_HOUR_DAY) {
            sections.add(createSummarySection(resources.stock.term, resources.stock.orderTermWeb))
        } else {
            sections.add(createSummarySection(resources.stock.term, stockResources.getString(detailsDto.term.getLabel())))
        }
    }

    if (detailsDto.marketDestination != MarketDestination.UNKNOWN && detailsDto.marketDestination != MarketDestination.AUTO) {
        sections.add(createSummarySection(resources.stock.marketDestination, detailsDto.marketDestination.codeString))
    }

    if (detailsDto.orderType.isMutualFund()) {
        sections.add(createSummarySection(resources.stock.priceType, resources.defaultResources.resource.getString(R.string.market)))
    } else {
        sections.add(createSummarySection(resources.stock.priceType, stockResources.getString(detailsDto.priceType.getLabel())))
    }

    var addAllOrNoneSection = true

    when (detailsDto.priceType) {
        PriceType.TRAILING_STOP_DOLLAR -> {
            detailsDto.advancedItem?.offsetValue?.let {
                sections.add(
                    createSummarySection(
                        resources.stock.stopValDollar,
                        stockResources.getQuantityString(R.plurals.stop_val_dollar_point, it.toInt(), it)
                    )
                )
            }
            addAllOrNoneSection = false
        }
        PriceType.TRAILING_STOP_PERCENT -> {
            detailsDto.advancedItem?.offsetValue?.let {
                sections.add(
                    createSummarySection(resources.stock.stopValPercent, stockResources.getString(R.string.stop_val_dollar_percent, it.toPlainString()))
                )
            }
            addAllOrNoneSection = false
        }
        PriceType.TRAILING_STOP_LIMIT -> {
            detailsDto.advancedItem?.offsetValue?.let {
                sections.add(
                    createSummarySection(
                        resources.stock.stopValDollar,
                        stockResources.getQuantityString(R.plurals.stop_val_dollar_point, it.toInt(), it)
                    )
                )
            }
            detailsDto.limitOffsetValue?.let {
                sections.add(createSummarySection(resources.stock.limitOffsetValue, it.toPlainString()))
            }
        }
        else ->
            detailsDto.stopPrice?.let {
                sections.add(createSummarySection(resources.stock.stopPrice, MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(it)))
            }
    }

    if (detailsDto.orderStatus != OrderStatus.SAVED) {
        detailsDto.advancedItem?.price?.let {
            sections.add(createSummarySection(resources.stock.price, MarketDataFormatter.formatMoneyValueWithCurrency(it)))
        }
    }

    if (detailsDto.orderStatus == OrderStatus.SAVED && !isConditionalOrder &&
        (PriceType.isMarketPrice(detailsDto.priceType.typeCode) || detailsDto.priceType == PriceType.EVEN)
    ) {
        if (PriceType.isMarketPrice(detailsDto.priceType.typeCode)) {
            sections.add(createSummarySection(resources.stock.price, stockResources.getString(R.string.market)))
        } else if (detailsDto.priceType == PriceType.EVEN) {
            sections.add(createSummarySection(resources.stock.price, stockResources.getString(R.string.even)))
        }
    } else {
        detailsDto.limitPrice?.let {
            sections.add(createSummarySection(resources.stock.limitPrice, MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(it)))
        }
    }

    if (detailsDto.orderStatus == OrderStatus.EXECUTED && !detailsDto.hasPartialExecution) {
        detailsDto.priceExecuted?.also {
            sections.add(createSummarySection(resources.stock.priceExecuted, MarketDataFormatter.formatMoneyValueWithCurrency(it)))
        }
    }

    if (addAllOrNoneSection) {
        sections.add(
            createSummarySection(
                resources.stock.allOrNone,
                stockResources.getString(
                    if (detailsDto.execInstruction.allOrNoneFlag) {
                        R.string.yes
                    } else {
                        R.string.no
                    }
                )
            )
        )
    }

    if ((detailsDto.orderStatus == OrderStatus.EXECUTED || detailsDto.orderStatus == OrderStatus.OPEN) && !detailsDto.hasPartialExecution) {
        if (detailsDto.orderType.isMutualFund()) {
            val estimatedCommission = detailsDto.estimatedCommission ?: BigDecimal.ZERO
            if (detailsDto.orderStatus == OrderStatus.OPEN) {
                sections.add(
                    createSummarySection(
                        resources.stock.estimatedCommissionFees,
                        MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(estimatedCommission)
                    )
                )
            } else if (detailsDto.orderStatus == OrderStatus.EXECUTED) {
                sections.add(
                    createSummarySection(
                        resources.stock.commissionFees,
                        MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(estimatedCommission)
                    )
                )
            }
        } else {
            detailsDto.commission?.let {
                sections.add(createSummarySection(resources.stock.commissionFees, MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(it)))
            }
        }
    }

    if (detailsDto.orderStatus == OrderStatus.SAVED) {
        val estimatedCommission = detailsDto.estimatedCommission ?: BigDecimal.ZERO
        sections.add(
            createSummarySection(
                resources.stock.estimatedCommission,
                MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(estimatedCommission)
            )
        )

        val estimatedTotalCost = detailsDto.estimatedTotalCost?.abs() ?: BigDecimal.ZERO
        var costAndProceedsLabel = resources.stock.estimatedTotalCost
        detailsDto.estimatedTotalCost?.let {
            costAndProceedsLabel = if (it.signum() < 0) {
                resources.stock.estimatedTotalProceeds
            } else {
                resources.stock.estimatedTotalCost
            }
        }

        sections.add(
            createSummarySection(
                costAndProceedsLabel,
                MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(estimatedTotalCost)
            )
        )
    }
    return sections
}

fun getSavedInfo(detailsDto: BaseOrderDetailsItem.OrderDetailsItem, resources: OrderDetailLabelResources, showStrategy: Boolean): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    val stockResources = resources.defaultResources.resource

    if (detailsDto.orderStatus == OrderStatus.SAVED) {
        detailsDto.orderSavedDate?.let {
            sections.add(createSummarySection(resources.stock.orderSavedDate, it))
        }
        sections.add(createSummarySection(resources.stock.account, detailsDto.account.replaceRange(START_INDEX, END_INDEX, "XXXXX")))
        if (detailsDto.orderType == OrderType.EQUITY) {
            sections.add(createSummarySection(resources.stock.orderStatus, stockResources.getString(detailsDto.orderStatus.getLabel())))
        }
        if (showStrategy) {
            detailsDto.strategyType?.let {
                sections.add(createSummarySection(resources.stock.strategy, stockResources.getString(it.getLabel())))
            }
        }
    } else {
        sections.add(createSummarySection(resources.stock.orderStatus, stockResources.getString(detailsDto.orderStatus.getLabel())))
    }
    return sections
}

fun getQuantity(
    quantity: String,
    execInstruction: ExecInstruction,
    isConditionalOrder: Boolean,
    resources: OrderDetailLabelResources,
    advancedOrderType: AdvancedOrderType?,
    orderTerm: OrderTerm
): TableSectionItemLayout {
    val quantityVal = when {
        isNotShowingAnyFlag(isConditionalOrder, advancedOrderType) -> quantity
        execInstruction.allOrNoneFlag -> quantity.plus(" (AON)")
        isDniVisible(orderTerm, execInstruction) -> quantity.plus(" (DNI)")
        isDnrVisible(orderTerm, execInstruction) -> quantity.plus(" (DNR)")
        else -> quantity
    }
    return if (quantity.contains("/")) {
        createSummarySection(resources.stock.quantityExecutedEntered, quantityVal)
    } else {
        createSummarySection(resources.stock.quantity, quantityVal)
    }
}

private fun isDnrVisible(
    orderTerm: OrderTerm,
    execInstruction: ExecInstruction
) = orderTerm in dnrOrderTerms && execInstruction.doNotReduceFlag

private fun isDniVisible(
    orderTerm: OrderTerm,
    execInstruction: ExecInstruction
) = orderTerm in dniOrderTerms && execInstruction.doNotIncreaseFlag

private fun isNotShowingAnyFlag(isConditionalOrder: Boolean, advancedOrderType: AdvancedOrderType?) =
    isConditionalOrder || advancedOrderType in AdvancedOrderType.contingentOrderTypes || advancedOrderType in AdvancedOrderType.bracketedOrderTypes

fun getPartialInfo(
    execInstruction: ExecInstruction,
    detailDescription: OrderDetailDescription,
    isConditionalOrder: Boolean,
    resources: OrderDetailLabelResources,
    advancedOrderType: AdvancedOrderType?,
    orderTerm: OrderTerm
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    sections.add(getQuantity(detailDescription.displayQuantity, execInstruction, isConditionalOrder, resources, advancedOrderType, orderTerm))

    detailDescription.executedDate?.also {
        sections.add(createSummarySection(resources.stock.orderExecuted, it))
    }
    sections.add(
        createSummarySection(
            resources.stock.priceExecuted,
            MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(detailDescription.priceExecuted)
        )
    )
    detailDescription.commission?.also {
        sections.add(createSummarySection(resources.stock.commissionFees, MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(it)))
    }
    return sections
}
