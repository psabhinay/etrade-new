package com.etrade.mobilepro.debugmenu.data.rest

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "inside", strict = false)
internal class EnvDto(
    @field:Element(name = "ServerSideConfiguration", required = false)
    var serverSideConfiguration: String? = null,
    @field:Element(name = "TradingUrl", required = false)
    var tradingUrl: String? = null,
    @field:Element(name = "MyETUrl", required = false)
    var myETUrl: String? = null,
    @field:Element(name = "ChartUrl", required = false)
    var chartUrl: String? = null,
    @field:Element(name = "APIUrl", required = false)
    var apiUrl: String? = null,
    @field:Element(name = "ConsumerKey", required = false)
    var consumerKey: String? = null,
    @field:Element(name = "MODUrl", required = false)
    var modUrl: String? = null
)
