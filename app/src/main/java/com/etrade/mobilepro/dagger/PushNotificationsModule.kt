package com.etrade.mobilepro.dagger

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.pushnotification.api.repo.AlertPushSettingsRepo
import com.etrade.mobilepro.pushnotification.data.repo.DefaultAlertPushSettingsRepo
import com.etrade.mobilepro.pushnotification.data.rest.PushSettingsApiClient
import com.etrade.mobilepro.pushnotification.presentation.LocalyticsPushNotificationsService
import com.etrade.mobilepro.pushnotification.presentation.MessagingService
import com.etrade.mobilepro.pushnotification.presentation.NotificationOptInDialog
import com.etrade.mobilepro.pushnotification.presentation.SmartAlertNotificationFactory
import com.etrade.mobilepro.pushnotification.presentation.SmartAlertNotificationFactoryImpl
import com.etrade.mobilepro.pushnotification.presentation.SmartAlertService
import com.etrade.mobilepro.pushnotification.presentation.viewmodel.NotificationOptInViewModel
import com.etrade.mobilepro.pushnotification.usecase.ManageSmartAlertSubscriptionUseCase
import com.etrade.mobilepro.pushnotification.usecase.ManageSmartAlertSubscriptionUseCaseImpl
import com.etrade.mobilepro.pushnotification.usecase.SmartAlertSubscriptionUseCase
import com.etrade.mobilepro.pushnotification.usecase.SmartAlertSubscriptionUseCaseImpl
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.KeyValueStore
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
interface PushNotificationsModule {

    @Binds
    fun bindManageSmartAlertSubscriptionUseCase(impl: ManageSmartAlertSubscriptionUseCaseImpl): ManageSmartAlertSubscriptionUseCase

    @Binds
    fun bindSmartAlertSubscriptionUseCase(impl: SmartAlertSubscriptionUseCaseImpl): SmartAlertSubscriptionUseCase

    @Binds
    @IntoMap
    @ViewModelKey(NotificationOptInViewModel::class)
    fun bindAccountAlertsViewModel(viewModel: NotificationOptInViewModel): ViewModel

    @Binds
    @IntoMap
    @FragmentKey(NotificationOptInDialog::class)
    fun bindNotificationOptInDialogFragment(fragment: NotificationOptInDialog): Fragment

    companion object {

        @Provides
        @Singleton
        fun provideAlertPushSettingsRepo(
            appInfo: ApplicationInfo,
            user: User,
            apiClient: PushSettingsApiClient,
            keyValueStore: KeyValueStore
        ): AlertPushSettingsRepo =
            DefaultAlertPushSettingsRepo(
                deviceIdProvider = appInfo::deviceId,
                alertNotificationDataProvider = { user.session?.notificationToken },
                apiClient = apiClient,
                store = keyValueStore
            )

        @Provides
        @Singleton
        fun providePushSettingsApiClient(@MobileTrade retrofit: Retrofit): PushSettingsApiClient =
            retrofit.create(PushSettingsApiClient::class.java)

        @Provides
        @Singleton
        fun provideSmartAlertNotificationFactory(
            context: Context,
            keyValueStore: KeyValueStore
        ): SmartAlertNotificationFactory =
            SmartAlertNotificationFactoryImpl(
                context = context,
                store = keyValueStore,
                activityClass = MainActivity::class.java
            )

        @Provides
        @Singleton
        fun provideMessagingServices(
            notificationFactory: SmartAlertNotificationFactory,
            smartAlertSubscription: SmartAlertSubscriptionUseCase,
            store: KeyValueStore,
            context: Context
        ): Array<MessagingService> = arrayOf(
            SmartAlertService(notificationFactory, smartAlertSubscription, store, context),
            LocalyticsPushNotificationsService()
        )
    }
}
