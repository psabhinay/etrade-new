package com.etrade.mobilepro.quotes.usecase

import com.etrade.mobilepro.quote.mapper.MobileQuotePageItemMapper
import com.etrade.mobilepro.quoteapi.QuoteItems
import com.etrade.mobilepro.util.UseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface MapDualPaneMobileQuoteUseCase : UseCase<MapMobileQuoteUseCaseParameter, Pair<QuoteItems, QuoteItems>>

class MapDualPaneMobileQuoteUseCaseImpl @Inject constructor(
    private val mapper: MobileQuotePageItemMapper
) : MapDualPaneMobileQuoteUseCase {
    override suspend fun execute(parameter: MapMobileQuoteUseCaseParameter): Pair<QuoteItems, QuoteItems> = withContext(Dispatchers.IO) {
        with(parameter) {
            when {
                quote.typeCode.isMutualFundOrMoneyMarketFund -> mapper.mapFundDualPane(quote)
                quote.typeCode.isOption -> mapper.mapOptionsDualPane(quote, isShowHoldings)
                quote.typeCode.isIndex -> mapper.mapIndexDualPane(quote)
                else -> mapper.mapDualPane(quote, streamingEnabled, isShowHoldings)
            }
        }
    }
}
