package com.etrade.mobilepro.util

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Executes a [block] and converts the response data. Ensures, that successful response always has non-null data.
 *
 * @param block the block to execute
 * @param convert a function that converts call's response to the actual resource data
 * @param T type of the call's response body
 * @param R type of the data
 *
 * @return resource with converted response data
 */
suspend fun <T, R> execute(block: suspend () -> T, convert: (T) -> R?): Resource<R> {
    return when (val response = execute(block)) {
        is Resource.Success -> convertNullSafe(response.data, convert)
        is Resource.Loading -> Resource.Loading()
        is Resource.Failed -> Resource.Failed()
    }
}

/**
 * Safely executes a [block], catching any exception that may occur.
 *
 * If the call executes successfully, [Resource.Success] will be returned. If the call execution is failed, [Resource.Failed] will be returned.
 *
 * @param block the block to execute
 * @param T type of the response's body
 *
 * @return resource
 */
@SuppressWarnings("TooGenericExceptionCaught")
suspend fun <T> execute(block: suspend () -> T): Resource<T> = withContext(Dispatchers.IO) {
    try {
        Resource.Success(block())
    } catch (e: Throwable) {
        Resource.Failed<T>(error = e)
    }
}

private inline fun <T, R> convertNullSafe(data: T?, convert: (T) -> R?): Resource<R> = data?.let(convert)?.let { Resource.Success(it) } ?: Resource.Failed()
