package com.etrade.mobilepro.ui_demo.dagger

import com.etrade.mobilepro.ui_demo.MainActivity
import com.etrade.mobilepro.ui_demo.featureflags.FeatureFlagsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    /* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */

    @ContributesAndroidInjector
    internal abstract fun contributeFeatureFlagsActivityInjector(): FeatureFlagsActivity

    @ContributesAndroidInjector
    internal abstract fun contributeMainActivityInjector(): MainActivity
}
