package com.etrade.mobilepro.usersubscriptionapi

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MarketDataAgreementResponse(
    @Json(name = "data")
    val data: QuoteAgreementData? = null
)

@JsonClass(generateAdapter = true)
data class QuoteAgreementData(
    @Json(name = "Status")
    val status: String? = null,
    @Json(name = "Type")
    val type: String? = null
) {
    val isRealtime: Boolean = status == "RT"
}
