package com.etrade.mobilepro.portfolio.model

import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.completeview.model.AccountSummary
import com.etrade.mobilepro.completeview.model.AccountSummaryViewModel
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.portfolio.model.dto.CallToActionParcelable
import com.etrade.mobilepro.util.android.CallToAction

class ExtendedAccountSummaryViewModel(
    val accountSummaryViewModel: AccountSummaryViewModel,
    portfolioSummaryModelBuilder: (Account?) -> ExtendedPortfolioSummary
) : DynamicViewModel(R.layout.view_account_summary_with_extra),
    AccountSummary by accountSummaryViewModel {

    val portfolioSummaryModel: ExtendedPortfolioSummary by lazy {
        portfolioSummaryModelBuilder(accountSummaryViewModel.account)
    }

    override val variableId: Int = BR.obj

    init {
        proxyEvents(accountSummaryViewModel)
    }

    override fun onCtaClicked(appUrl: String?) {
        accountSummaryViewModel.onCtaClicked(appUrl)
    }

    private fun proxyEvents(viewModel: DynamicViewModel) {
        // Event mapped to the actual clickEvents observed by the UI
        viewModel.clickEvents.observeForever {
            clickEvents.postValue(it)
        }
        viewModel.appMessageSignal.observeForever {
            mutableAppMessageSignal.postValue(it)
        }
    }

    fun viewMore() {
        clickEvents.value = CtaEvent.LaunchPortfolioBottomSheet
    }
}

data class PortfolioCtaList(
    val headerViewCtaList: List<CallToAction>,
    val extendedViewCtaList: List<CallToActionParcelable>
)
