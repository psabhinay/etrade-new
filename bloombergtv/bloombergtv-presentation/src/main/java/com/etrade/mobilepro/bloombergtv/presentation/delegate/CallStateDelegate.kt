@file:Suppress("deprecation")

package com.etrade.mobilepro.bloombergtv.presentation.delegate

import android.annotation.TargetApi
import android.content.Context
import android.content.Context.AUDIO_SERVICE
import android.content.Context.TELEPHONY_SERVICE
import android.media.AudioManager
import android.media.AudioManager.MODE_NORMAL
import android.media.AudioManager.OnModeChangedListener
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.S
import android.telephony.PhoneStateListener
import android.telephony.PhoneStateListener.LISTEN_CALL_STATE
import android.telephony.PhoneStateListener.LISTEN_NONE
import android.telephony.TelephonyManager
import android.telephony.TelephonyManager.CALL_STATE_IDLE
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvActivity

internal class CallStateDelegate(
    private val onCallStarted: () -> Unit = {},
    private val onCallEnded: () -> Unit = {},
) : BloombergTvActivityDelegate {

    var hasOngoingCall: Boolean = false
        private set

    private lateinit var context: Context

    private val audioManager: AudioManager?
        get() = context.getSystemService(AUDIO_SERVICE) as? AudioManager

    private val telephonyManager: TelephonyManager?
        get() = context.getSystemService(TELEPHONY_SERVICE) as? TelephonyManager

    @TargetApi(S)
    private lateinit var onModeChangedListener: Any

    @Suppress("deprecation")
    private val phoneStateListener =
        object : PhoneStateListener() {
            override fun onCallStateChanged(state: Int, phoneNumber: String?) {
                hasOngoingCall = state != CALL_STATE_IDLE
                triggerCallback()
            }
        }

    override fun init(activity: BloombergTvActivity) {
        init(activity.baseContext)
    }

    fun init(context: Context) {
        this.context = context
        if (SDK_INT >= S) {
            onModeChangedListener = OnModeChangedListener { mode ->
                hasOngoingCall = mode != MODE_NORMAL
                triggerCallback()
            }
            audioManager?.addOnModeChangedListener(
                context.mainExecutor,
                onModeChangedListener as OnModeChangedListener
            )
        } else {
            @Suppress("deprecation")
            telephonyManager?.listen(phoneStateListener, LISTEN_CALL_STATE)
        }
    }

    fun dismantle() {
        if (SDK_INT >= S) {
            audioManager?.removeOnModeChangedListener(
                onModeChangedListener as OnModeChangedListener
            )
        } else {
            @Suppress("deprecation")
            telephonyManager?.listen(phoneStateListener, LISTEN_NONE)
        }
    }

    private fun triggerCallback() = if (hasOngoingCall) { onCallStarted() } else { onCallEnded() }
}
