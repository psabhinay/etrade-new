package com.etrade.mobilepro.inboxmessages.dynamicui.data.bannerwidget

import com.etrade.mobilepro.accountslist.data.dto.AccountReferenceDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.MobileScreen
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.MessagePlace
import com.etrade.mobilepro.inboxmessages.MessageType
import com.etrade.mobilepro.inboxmessages.dynamicui.api.InboxMessagesWidgetPlace
import com.etrade.mobilepro.inboxmessages.dynamicui.bannerwidget.BannerWidgetViewDto
import com.etrade.mobilepro.inboxmessages.dynamicui.data.WidgetListUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.data.isForDarkMode
import com.etrade.mobilepro.inboxmessages.userType
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import javax.inject.Inject

class CompleteViewBannerWidgetListUpdater @Inject constructor(
    private val bannerWidgetDtoFactory: BannerWidgetDtoFactory,
    private val inboxMessagesRepo: InboxMessagesRepository,
    private val darkModeChecker: DarkModeChecker
) : WidgetListUpdater {
    override val belongsToWidgetPlace: InboxMessagesWidgetPlace = InboxMessagesWidgetPlace.COMPLETE_VIEW

    override fun updateWithInboxMessages(input: MobileScreen?): List<BaseScreenView> {
        val views = input?.views.orEmpty().filterNotNull()
        val accounts = input?.references?.filterIsInstance<AccountReferenceDto>()?.get(0)
            ?.convertToModel()
        val messages = inboxMessagesRepo.getMessagesBlocking(
            type = MessageType.DASHBOARD_INFORMATION,
            place = MessagePlace.COMPLETE_VIEW_BANNER,
            userType = accounts?.userType()
        )

        val resultingViews = views.toMutableList()
        messages.getOrNull()?.findMessageBasedOnTheme(darkModeChecker.isInDarkMode())?.let { resultingViews.add(it) }
        return resultingViews
    }

    private fun List<InboxMessage>.findMessageBasedOnTheme(inDarkMode: Boolean): BannerWidgetViewDto? {
        val messages = if (inDarkMode) {
            asSequence().filter { it.isForDarkMode() }.sortedBy { it.receivedDate }
        } else {
            asSequence().filterNot { it.isForDarkMode() }.sortedBy { it.receivedDate }
        }.toList()

        return if (messages.isEmpty()) {
            null
        } else {
            // display only one banner at a time.
            bannerWidgetDtoFactory.create(messages[0])
        }
    }
}
