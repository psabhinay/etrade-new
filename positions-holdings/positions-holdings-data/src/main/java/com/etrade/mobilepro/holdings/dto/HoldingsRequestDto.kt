package com.etrade.mobilepro.holdings.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HoldingsRequestDto(
    @Json(name = "value")
    val value: RequestValue
)

@JsonClass(generateAdapter = true)
data class RequestValue(
    @Json(name = "brokerageAccounts")
    val brokerageAccounts: String,
    @Json(name = "stockPlanAccounts")
    val stockPlanAccounts: String,
    @Json(name = "symbol")
    val symbol: String
)
