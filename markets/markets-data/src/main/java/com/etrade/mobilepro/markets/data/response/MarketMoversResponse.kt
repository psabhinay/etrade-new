package com.etrade.mobilepro.markets.data.response

interface MarketMoversResponse {
    var quotes: List<Quote>?
}
