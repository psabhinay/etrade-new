package com.etrade.mobilepro.themeselection

import android.content.Context
import androidx.annotation.IntDef
import androidx.appcompat.app.AppCompatDelegate
import com.etrade.eo.userprefrences.api.KeyValueStorage
import javax.inject.Inject

const val UI_CURRENT_MODE = "uiModeStatus"
const val FOLLOWING_SYSTEM_PREFERENCES = 0
const val DARK_MODE = 1
const val LIGHT_MODE = 2

@Retention(AnnotationRetention.SOURCE)
@IntDef(
    FOLLOWING_SYSTEM_PREFERENCES,
    DARK_MODE,
    LIGHT_MODE
)
annotation class UiMode

interface NightModePreferences {
    @UiMode
    var uiMode: Int

    val uiModeName: String

    fun switchTheme(@UiMode newMode: Int)

    fun initTheme()
}

class NightModePreferencesImpl @Inject constructor(
    private val context: Context,
    private val keyValueStorage: KeyValueStorage
) : NightModePreferences {

    @UiMode
    override var uiMode: Int
        get() {
            return keyValueStorage.getIntValue(UI_CURRENT_MODE)
        }
        set(value) {
            keyValueStorage.putIntValue(UI_CURRENT_MODE, value)
        }

    override val uiModeName: String
        get() = when (uiMode) {
            DARK_MODE -> context.getString(R.string.theme_selection_dark)
            LIGHT_MODE -> context.getString(R.string.theme_selection_light)
            else -> context.getString(R.string.theme_selection_system)
        }

    override fun switchTheme(@UiMode newMode: Int) {
        when (newMode) {
            FOLLOWING_SYSTEM_PREFERENCES -> goFollowSystem()
            DARK_MODE -> goDark()
            LIGHT_MODE -> goLight()
        }
    }

    private fun goLight() =
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO).also {
            uiMode = LIGHT_MODE
        }

    private fun goDark() =
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES).also {
            uiMode = DARK_MODE
        }

    private fun goFollowSystem() =
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM).also {
            uiMode = FOLLOWING_SYSTEM_PREFERENCES
        }

    override fun initTheme() {
        when (uiMode) {
            LIGHT_MODE -> goLight()
            DARK_MODE -> goDark()
        }
    }
}
