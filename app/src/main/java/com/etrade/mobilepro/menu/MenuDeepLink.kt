package com.etrade.mobilepro.menu

import com.etrade.mobilepro.menu.MenuDeepLink.Host.MENU
import com.etrade.mobilepro.menu.MenuDeepLink.Host.SHOW
import java.util.Locale

enum class MenuDeepLink(val host: Host, val path: String, val isLoginRequired: Boolean = false) {
    ALERTS(SHOW, "/alerts", isLoginRequired = true),
    BLOOMBERG_TV(SHOW, "/bloombergTV"),
    CALENDAR(SHOW, "/calendar", isLoginRequired = true),
    CHECK_DEPOSIT(SHOW, "/checkDeposit", isLoginRequired = true),
    CUSTOMER_SERVICE(MENU, "/customerService"),
    EDUCATION_VIDEOS(SHOW, "/educationVideos"),
    NEWS(SHOW, "/news"),
    OPEN_ACCOUNT(SHOW, "/openAccount"),
    QUOTES(SHOW, "/quotes"),
    SCREENERS(SHOW, "/screeners"),
    SETTINGS(SHOW, "/settings"),
    TAX_DOCUMENTS(MENU, "/taxDocuments", isLoginRequired = true),
    TRANSFER(MENU, "/transfer", isLoginRequired = true),
    USER_PROFILE(MENU, "/myProfile", isLoginRequired = true),
    VERIFY_ACCOUNT_WEB_VIEW(MENU, "/verifyAccountWebView");

    val appUrl: String
        get() = "etrade://${host.name.lowercase(Locale.ROOT)}$path"

    enum class Host {
        MENU,
        SHOW
    }

    companion object {
        val loginRequiredPaths: List<String>
            get() = values().asList().filter { it.isLoginRequired }.map { it.path }
    }
}
