package com.etrade.mobilepro.common.main.navigation

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode

interface MainNavigation {

    fun navigateToCompleteView(activity: FragmentActivity)

    fun navigateToQuoteDetails(activity: FragmentActivity?, bundle: Bundle)

    fun navigateToQuoteDetails(navController: NavController, bundle: Bundle)

    fun launchBottomSheetOrders(activity: FragmentActivity?, accountId: String, oderStatus: OrderStatus)

    fun launchExtendedPortfolioBottomSheet(activity: FragmentActivity?, info: Any)

    @Suppress("LongParameterList")
    fun launchBottomSheetPortfolioTaxLot(
        activity: FragmentActivity?,
        positionId: String,
        accountUuid: String,
        symbol: String,
        portfolioTableViewType: PortfolioTableViewType,
        positionTableMode: PositionTableMode
    )

    fun navigateToAccount(navController: NavController, accountId: String)

    fun launchQuoteDetailsBottomSheet(activity: FragmentActivity?, bundle: Bundle)

    fun openPdf(navController: NavController, url: String)
}
