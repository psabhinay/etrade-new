package com.etrade.mobilepro.checkdeposit.usecases.validation

sealed class ValidationResult {

    object Valid : ValidationResult()
    data class Invalid(
        val message: String? = "",
        val code: String? = ""
    ) : ValidationResult()
}
