package com.etrade.mobilepro.screener.presentation.stock.technicals.percentinput

import android.text.InputFilter
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class PercentCustomizerTest {
    private lateinit var sut: PercentCustomizer

    @BeforeEach
    fun setup() {
        sut = PercentCustomizer()
    }

    @Test
    fun updateInputFiltersOnInit() {
        val event = InputFieldManager.Event.ON_INIT

        val result = sut.updateInputFilters(event)

        assertTrue(result.isEmpty())
    }

    @Test
    fun updateInputFiltersOnFocusGain() {
        val event = InputFieldManager.Event.ON_FOCUS_GAINED

        val result = sut.updateInputFilters(event)

        assertTrue(result.size == 1)
        val filter = result[0]
        assertTrue(filter is InputFilter.LengthFilter)
    }

    @Test
    fun updateInputFiltersOnFocusLost() {
        val event = InputFieldManager.Event.ON_FOCUS_LOST

        val result = sut.updateInputFilters(event)

        assertTrue(result.size == 1)
        val filter = result[0]
        assertTrue(filter is InputFilter.LengthFilter)
    }
}
