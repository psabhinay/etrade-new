package com.etrade.mobilepro.markets.earnings

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.databinding.ItemTodayEarningBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.extension.viewBinding

class TodayEarningsWidgetRecyclerView : RecyclerView {
    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    init {
        layoutManager = LinearLayoutManager(context)
        adapter = TodayEarningsAdapter()
        setupRecyclerViewDivider(R.drawable.thin_divider)
    }

    fun setItems(items: List<MarketEventDetailsPayload>) {
        (adapter as TodayEarningsAdapter).setItems(items)
    }

    private class TodayEarningsAdapter : RecyclerView.Adapter<TodayEarningsViewHolder>() {

        private val items: MutableList<MarketEventDetailsPayload> = mutableListOf()

        fun setItems(items: List<MarketEventDetailsPayload>) {
            this.items.clear()
            this.items.addAll(items)

            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodayEarningsViewHolder {
            return TodayEarningsViewHolder(parent.viewBinding(ItemTodayEarningBinding::inflate))
        }

        override fun onBindViewHolder(holder: TodayEarningsViewHolder, position: Int) {
            holder.bind(items[position])
        }

        override fun getItemCount(): Int = items.size
    }

    private class TodayEarningsViewHolder(private val binding: ItemTodayEarningBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MarketEventDetailsPayload) {
            with(binding) {
                title.text = item.event.title

                root.contentDescription = "${item.event.title}, ${item.event.date}"

                root.setOnClickListener {
                    item.event.symbol?.let { symbol ->
                        item.clickListener(symbol, it.findNavController())
                    }
                }
            }
        }
    }
}
