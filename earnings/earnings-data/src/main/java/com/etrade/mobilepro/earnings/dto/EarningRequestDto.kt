package com.etrade.mobilepro.earnings.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EarningsDetailsRequest(
    @Json(name = "earnings")
    val earnings: EarningsDetailsRequestValue
)

@JsonClass(generateAdapter = true)
data class EarningsDetailsRequestValue(
    @Json(name = "symbol")
    val symbol: String,
    @Json(name = "fiscalPeriod")
    val fiscalPeriod: String,
    @Json(name = "fiscalPeriodCount")
    val fiscalPeriodCount: Int
)

internal fun createEarningsDetailRequest(symbol: String, fiscalPeriod: String, fiscalPeriodCount: Int): EarningsDetailsRequest = EarningsDetailsRequest(
    earnings = EarningsDetailsRequestValue(
        symbol = symbol,
        fiscalPeriod = fiscalPeriod,
        fiscalPeriodCount = fiscalPeriodCount
    )
)
