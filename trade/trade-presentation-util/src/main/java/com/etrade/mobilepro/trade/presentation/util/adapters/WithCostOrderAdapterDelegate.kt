package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.WithCostOrder
import com.etrade.mobilepro.trade.presentation.util.R
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import java.math.BigDecimal

internal class WithCostOrderAdapterDelegate(private val resources: Resources) : OrderAdapterDelegate {

    override fun createOrderSectionItems(order: Order): List<TableSectionItemLayout> {
        require(order is WithCostOrder)
        return listOfNotNull(
            order.fee?.let { fee -> sectionItem(resources, R.string.trade_label_estimated_commission, MarketDataFormatter.formatCurrencyValue(fee)) },
            order.cost?.let { cost ->
                val formattedCost = MarketDataFormatter.formatCurrencyValue(cost.abs())
                if (cost < BigDecimal.ZERO) {
                    R.string.trade_label_estimated_total_proceeds
                } else {
                    R.string.trade_label_estimated_total_cost
                }.let {
                    sectionItem(resources, it, formattedCost)
                }
            }
        )
    }
}
