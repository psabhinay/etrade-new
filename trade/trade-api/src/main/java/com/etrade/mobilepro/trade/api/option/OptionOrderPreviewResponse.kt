package com.etrade.mobilepro.trade.api.option

import com.etrade.mobilepro.trade.api.OrderPreviewResponse

interface OptionOrderPreviewResponse : OptionOrderResponse, OrderPreviewResponse
