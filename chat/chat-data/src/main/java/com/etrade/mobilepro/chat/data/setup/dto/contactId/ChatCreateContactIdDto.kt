package com.etrade.mobilepro.chat.data.setup.dto.contactId

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChatCreateContactIdDto(

    @Json(name = "data")
    val data: ChatContactIdDataDto? = null
)
