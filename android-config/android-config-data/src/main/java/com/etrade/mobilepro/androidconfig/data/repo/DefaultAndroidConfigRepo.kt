package com.etrade.mobilepro.androidconfig.data.repo

import com.etrade.mobilepro.androidconfig.api.AndroidConfigInfo
import com.etrade.mobilepro.androidconfig.api.repo.AndroidConfigRepo
import com.etrade.mobilepro.common.di.MobileEtrade
import retrofit2.Retrofit
import javax.inject.Inject

class DefaultAndroidConfigRepo @Inject constructor(
    @MobileEtrade private val retrofit: Retrofit
) : AndroidConfigRepo {

    private val apiClient = retrofit.create(AndroidConfigApiClient::class.java)

    override suspend fun getAndroidConfiguration(): AndroidConfigInfo {
        return apiClient.getAndroidConfigInfo()
    }
}
