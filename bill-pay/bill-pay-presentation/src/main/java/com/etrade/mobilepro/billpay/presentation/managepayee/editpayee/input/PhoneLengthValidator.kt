package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.inputvalueview.validators.FixedLengthValidator

open class PhoneLengthValidator(
    length: Int,
    errorMessage: String
) : FixedLengthValidator(length, errorMessage) {

    override fun validate(value: String): InputFieldManager.Value {
        val phoneNumberWithoutFormat = value.replace(Regex("\\D"), "")
        return super.validate(phoneNumberWithoutFormat)
    }
}
