package com.etrade.mobilepro.menu.helper

import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.home.model.MainActivityViewModel
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.tooltip.Tooltip
import com.etrade.mobilepro.tooltip.TooltipViewModel
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MENU

internal class MainMenuPageHelper(
    private val fragment: Fragment,
    private val applicationPreferences: ApplicationPreferences,
    private val viewModelFactory: ViewModelProvider.Factory
) {
    private val mainActivityViewModel: MainActivityViewModel by fragment.activityViewModels { viewModelFactory }
    private val tooltipViewModel: TooltipViewModel by fragment.activityViewModels()
    private var tooltip: Tooltip? = null

    fun openQuotes() {
        mainActivityViewModel.openSearchActivity(
            SearchQuery(SearchType.SYMBOL),
            simpleSymbolSearch = false,
            quoteLookupLocation = SCREEN_TITLE_MENU
        )
    }

    fun dismissTooltip() {
        tooltipViewModel.dismiss(tooltip)
        tooltip = null
    }
}
