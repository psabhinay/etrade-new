package com.etrade.mobilepro.barcode.adapter

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.etrade.mobilepro.barcode.R
import com.etrade.mobilepro.barcode.fragment.BarcodeResultsFragment
import com.etrade.mobilepro.barcode.fragment.BarcodeScanFragment

sealed class BarcodeTab(@StringRes val title: Int, val fragmentClass: Class<out Fragment>) {
    object Scan : BarcodeTab(R.string.barcode_tab_scan, BarcodeScanFragment::class.java)
    object Results : BarcodeTab(R.string.barcode_tab_results, BarcodeResultsFragment::class.java)
}
