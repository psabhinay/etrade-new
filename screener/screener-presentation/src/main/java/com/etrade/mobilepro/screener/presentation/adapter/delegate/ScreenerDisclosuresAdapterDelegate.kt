package com.etrade.mobilepro.screener.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.presentation.adapter.viewholder.ScreenerDisclosuresViewHolder
import com.etrade.mobilepro.screener.presentation.databinding.ItemScreenerDisclosuresBinding
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class ScreenerDisclosuresAdapterDelegate :
    AbsListItemAdapterDelegate<ScreenerItem.DisclosuresText, ScreenerItem, ScreenerDisclosuresViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): ScreenerDisclosuresViewHolder =
        ScreenerDisclosuresViewHolder(parent.viewBinding(ItemScreenerDisclosuresBinding::inflate))

    override fun isForViewType(item: ScreenerItem, items: MutableList<ScreenerItem>, position: Int): Boolean =
        item is ScreenerItem.DisclosuresText

    override fun onBindViewHolder(item: ScreenerItem.DisclosuresText, holder: ScreenerDisclosuresViewHolder, payloads: MutableList<Any>) {
        holder.bindTo(item)
    }
}
