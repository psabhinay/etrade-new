package com.etrade.mobilepro.streaming.lightstreamer.level2

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.Level2DataEvent
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.jupiter.api.Test
import java.util.concurrent.TimeUnit

internal class Level2StreamingQuoteProviderTest {
    private val streamingParams = object : Level2StreamIdFactory.Parameters {
        override val bookCount: Int
            get() = 5
    }
    private val streamIdFactory = Level2StreamIdFactory(streamingParams)

    @Test
    fun `once in a PULSE_RATE_MS accumulated data is flushed`() {
        val testScheduler = TestScheduler()
        val slowObservable = Observable
            .interval(PULSE_RATE_MS * 2, TimeUnit.MILLISECONDS, testScheduler)
            .map(::createL2DataEvent)
            .startWith(createL2DataEvent())
        val source = mock<SubscriptionManagerHelper<Level2DataEvent>>()
        whenever(source.subscribeTo(any(), any(), any())).thenReturn(slowObservable)

        val sut = Level2StreamingQuoteProvider(streamIdFactory, source, streamingParams, testScheduler)

        val testObserver = sut
            .getQuote("ANY", InstrumentType.EQ, emptySet())
            .subscribeOn(testScheduler)
            .observeOn(testScheduler).test()

        testObserver.assertNotTerminated()
            .assertNoErrors()
            .assertValueCount(0) // "time" hasn't started so no value expected

        testScheduler.advanceTimeBy(PULSE_RATE_MS / 2, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(0)

        testScheduler.advanceTimeBy(PULSE_RATE_MS / 2, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(1)

        testObserver.dispose()
    }

    @Test
    fun `once in a PULSE_RATE_MS accumulated data is not flushed if accumulated data is not about ASK or BID`() {
        val testScheduler = TestScheduler()
        val slowObservable = Observable
            .interval(PULSE_RATE_MS * 2, TimeUnit.MILLISECONDS, testScheduler)
            .map(::createL2DataEvent)
            .startWith(createL2DataEvent(side = "Errr"))
        val source = mock<SubscriptionManagerHelper<Level2DataEvent>>()
        whenever(source.subscribeTo(any(), any(), any())).thenReturn(slowObservable)

        val sut = Level2StreamingQuoteProvider(streamIdFactory, source, streamingParams, testScheduler)

        val testObserver = sut
            .getQuote("ANY", InstrumentType.EQ, emptySet())
            .subscribeOn(testScheduler)
            .observeOn(testScheduler).test()

        testObserver.assertNotTerminated()
            .assertNoErrors()
            .assertValueCount(0) // "time" hasn't started so no value expected

        testScheduler.advanceTimeBy(PULSE_RATE_MS / 2, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(0)

        testScheduler.advanceTimeBy(PULSE_RATE_MS / 2, TimeUnit.MILLISECONDS)
        testObserver.assertValueCount(0)

        testObserver.dispose()
    }

    private fun createL2DataEvent(i: Long? = null, side: String = ASK_SIDE): Level2DataEvent = object : Level2DataEvent {
        override val key: String
            get() = i?.toString() ?: ""
        override val side: String
            get() = side
        override val price: Double
            get() = 0.0
        override val size: Long
            get() = 0
        override val consolidatedSize: Long
            get() = 0
        override val marketMakerId: String?
            get() = ""
        override val time: Long
            get() = 0
        override val consolidateCount: Long
            get() = 0

        override fun copy(): Level2DataEvent = createL2DataEvent(i, side)
    }
}
