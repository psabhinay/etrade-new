package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.orders.api.ContingentQualifier
import com.etrade.mobilepro.trade.form.api.value.QualifierValueId

object DefaultQualifierValueId : QualifierValueId {
    override val bidPrice: String = ContingentQualifier.BID.followPrice
    override val askPrice: String = ContingentQualifier.ASK.followPrice
    override val lastPrice: String = ContingentQualifier.LAST_PRICE.followPrice
}
