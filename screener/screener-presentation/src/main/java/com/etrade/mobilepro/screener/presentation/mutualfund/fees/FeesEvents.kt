package com.etrade.mobilepro.screener.presentation.mutualfund.fees

import com.etrade.mobilepro.screener.api.filter.mutualfund.FeesFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class NoLoad : ScreenerFilterEvent<FeesFilter, FeesViewModel.ViewState, Boolean> {
    NO_LOAD_TRANSACTTION {
        override fun update(viewState: FeesViewModel.ViewState, eventValue: Boolean): FeesViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(noLoadTransaction = eventValue))
    },
    TRANSACTION_FEE {
        override fun update(viewState: FeesViewModel.ViewState, eventValue: Boolean): FeesViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(transactionFee = eventValue))
    }
}

internal enum class Load : ScreenerFilterEvent<FeesFilter, FeesViewModel.ViewState, Boolean> {
    FRONT_END {
        override fun update(viewState: FeesViewModel.ViewState, eventValue: Boolean): FeesViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(frontEndLoad = eventValue))
    },
    DEFERRED {
        override fun update(viewState: FeesViewModel.ViewState, eventValue: Boolean): FeesViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(deferredLoad = eventValue))
    }
}
