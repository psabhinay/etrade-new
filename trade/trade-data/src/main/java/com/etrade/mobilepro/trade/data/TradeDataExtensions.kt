package com.etrade.mobilepro.trade.data

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.MutableInputValuesSnapshot
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.etrade.mobilepro.trade.data.form.input.DefaultOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.toLegDetailsDtoList
import com.etrade.mobilepro.trade.data.form.value.AdvancedOrderOptionsActionValueId
import com.etrade.mobilepro.trade.data.stock.TRUE
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractAction
import com.etrade.mobilepro.trade.form.api.input.extractPriceType
import com.etrade.mobilepro.trade.form.api.value.SecurityTypeValueId
import com.etrade.mobilepro.util.ValueCoder
import com.etrade.mobilepro.util.json.toJsonString
import com.squareup.moshi.Moshi

internal const val DAY_TRADING_FLAG = "dayTradingFlag"
internal const val DAY_TRADING_VER = "dayTradingVer"

internal fun InputValuesSnapshot.createOptionTradeBody(
    tradeLegsCoder: ValueCoder<List<TradeLeg>>,
    securityTypeValueId: SecurityTypeValueId,
    moshi: Moshi,
    isTablet: Boolean
): String = moshi.toJsonString(createOptionTradeBody(tradeLegsCoder, securityTypeValueId, isTablet))

internal fun InputValuesSnapshot.createOptionTradeBody(
    tradeLegsCoder: ValueCoder<List<TradeLeg>>,
    securityTypeValueId: SecurityTypeValueId,
    isTablet: Boolean
): InputValuesSnapshot {
    val body = toMutableMap()
    body.fixLimitStopPriceParam(DefaultOptionTradeFormInputId, DefaultOptionTradeFormInputId.extractPriceType(this))
    body.setOptionOrderTypeAndTradeLegs(securityTypeValueId, tradeLegsCoder, this)
    body[DefaultOptionTradeFormInputId.symbol] = DefaultOptionTradeFormInputId.extractSymbol(this)
    // this is required for gateway to tell where the trade is from
    body["origSysCodeReq"] = OrigSysCode.ORIG_SYS_CODE_ANDR.numberCode
    body["orderSource"] = if (isTablet) "tablet" else "android"
    body[DAY_TRADING_FLAG] = TRUE
    body[DAY_TRADING_VER] = "1"
    return mapOf("value" to body)
}

internal fun MutableInputValuesSnapshot.setOptionOrderTypeAndTradeLegs(
    securityTypeValueId: SecurityTypeValueId,
    tradeLegsCoder: ValueCoder<List<TradeLeg>>,
    params: InputValuesSnapshot
) {
    val tradeLegs = params.getTradeLegs(tradeLegsCoder)?.toLegDetailsDtoList()
    this[DefaultOptionTradeFormInputId.tradeLegs] = tradeLegs
    setOptionOrderType(DefaultOptionTradeFormInputId.securityType, securityTypeValueId, tradeLegs, params)
}

internal fun MutableInputValuesSnapshot.setOptionOrderType(
    securityType: String,
    securityTypeValueId: SecurityTypeValueId,
    tradeLegs: List<LegDetailsCommonDto>?,
    params: InputValuesSnapshot
) {
    if (!containsKey(CommonOrderConstants.ORDER_TYPE)) {
        this[CommonOrderConstants.ORDER_TYPE] = when (params[securityType]) {
            securityTypeValueId.stockAndOptions -> params.getOptionOrderType(tradeLegs, OrderType.EQUITY, OrderType.BUY_WRITES)?.typeCode
            securityTypeValueId.option -> params.getOptionOrderType(tradeLegs, OrderType.SIMPLE_OPTION, OrderType.SPREADS)?.typeCode
            securityTypeValueId.conditional -> params.getOptionOrderType(tradeLegs, OrderType.SIMPLE_OPTION, OrderType.SPREADS)?.typeCode

            // Default to Simple / Complex options.
            else -> params.getOptionOrderType(tradeLegs, OrderType.SIMPLE_OPTION, OrderType.SPREADS)?.typeCode
        }
    }
}

private fun InputValuesSnapshot.getTradeLegs(
    tradeLegsCoder: ValueCoder<List<TradeLeg>>
): List<TradeLeg>? {
    return this[DefaultOptionTradeFormInputId.tradeLegs]?.let {
        tradeLegsCoder.decode(it)
    }
}

private fun InputValuesSnapshot.getOptionOrderType(
    tradeLegs: List<TradeLeg>?,
    singleLegOrderType: OrderType,
    multipleLegOrderType: OrderType
): OrderType? {
    fun List<TradeLeg>.orderType(): OrderType? = when {
        isEmpty() -> null
        size == 1 -> singleLegOrderType
        else -> multipleLegOrderType
    }
    return getOrderTypeByStrategy() ?: when {
        isTrailingStop(DefaultOptionTradeFormInputId) -> OrderType.ADVANCE_OPTION
        else -> tradeLegs?.orderType()
    }
}

internal fun InputValuesSnapshot.getStrategyType(): StrategyType = (this[DefaultOptionTradeFormInputId.strategy] as? String)
    ?.let { value -> DefaultOptionTradeFormInputId.strategyValueId.converter.toValue(value) }
    ?: StrategyType.UNKNOWN

private fun InputValuesSnapshot.getOrderTypeByStrategy(): OrderType? =
    when (getStrategyType()) {
        StrategyType.BUTTERFLY -> OrderType.BUTTERFLY
        StrategyType.COLLARS -> OrderType.COLLARS
        StrategyType.CONDOR -> OrderType.CONDOR
        StrategyType.CUSTOM -> OrderType.CUSTOM
        StrategyType.IRON_BUTTERFLY -> OrderType.IRON_BUTTERFLY
        StrategyType.IRON_CONDOR -> OrderType.IRON_CONDOR
        else -> null
    }

/**
 * Limit price for Limit order and for Stop Limit On Quote order is send to endpoint under the same parameter name.
 * Stop price for Stop order and for Stop Limit On Quote order is send to endpoint under the same parameter name.
 *
 * Since two dynamic form inputs cannot have the same ids, input id for limit price for Stop Limit On Quote order has different id but is
 * assigned correct id here. Same goes with the stop price for Stop Price for Stop Limit On Quote order.
 */
internal fun MutableInputValuesSnapshot.fixLimitStopPriceParam(withPriceTradeFormInputId: WithPriceTradeFormInputId, priceType: PriceType?) {
    if (priceType == PriceType.STOP_LIMIT) {
        this[withPriceTradeFormInputId.limitPrice] = this.remove(withPriceTradeFormInputId.limitPriceForStopLimit)
        this[withPriceTradeFormInputId.stopPrice] = this.remove(withPriceTradeFormInputId.stopPriceForStopLimit)
    }
}

/**
 * Correction required to the action (Sell Close / Sell Open) value in preview order requests for Advanced Option Order ( Conditional and Contingent )
 */
internal fun <T> MutableInputValuesSnapshot.fixAdvancedOrderOptionAction(inputId: T) where T : WithSymbolTradeFormInputId, T : WithActionTradeFormInputId {
    val symbolInfo = inputId.extractSymbolInfo(this)
    if (symbolInfo?.instrumentType?.isOption == true) {
        val actionInputIdValue = remove(inputId.action) as? String ?: ""
        val tradeAction = inputId.actionValueId.converter.toValue(actionInputIdValue)
        tradeAction?.let {
            put(inputId.action, AdvancedOrderOptionsActionValueId.converter.toKey(tradeAction))
        }
    }
}

/**
 * Correction required to the action (Sell Close / Sell Open) value in place order requests for Advanced Option Order ( Conditional and Contingent )
 */
internal fun <T> InputValuesSnapshot.extractAction(isOption: Boolean, inputId: T): TradeAction? where T : WithActionTradeFormInputId {
    return if (isOption) {
        val value = get(inputId.action).toString()
        AdvancedOrderOptionsActionValueId.converter.toValue(value)
    } else {
        inputId.extractAction(this)
    }
}

internal fun InputValuesSnapshot.isTrailingStop(withPriceTradeFormInputId: WithPriceTradeFormInputId): Boolean {
    return this[withPriceTradeFormInputId.priceType]?.let {
        it == withPriceTradeFormInputId.priceTypeValueId.trailingStopPercent ||
            it == withPriceTradeFormInputId.priceTypeValueId.trailingStopDollar
    } ?: false
}
