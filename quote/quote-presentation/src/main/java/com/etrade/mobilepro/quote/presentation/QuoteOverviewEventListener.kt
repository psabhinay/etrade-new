package com.etrade.mobilepro.quote.presentation

interface QuoteOverviewEventListener {
    fun onChartClicked()
}
