package com.etrade.mobilepro.messagecenter.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "CreateSecureMsgResponse", strict = false)
class ReferenceBookResponseDto(
    @field:ElementList(name = "AccountList", required = false)
    var accounts: List<AccountRefDto?>? = null,
    @field:ElementList(name = "SubjectTopicList", required = false)
    var subjects: List<SubjectDto?>? = null
)

@Root(name = "Subject", strict = false)
class SubjectDto(
    @field:Element(name = "SubjectId", required = false)
    var id: String? = null,
    @field:Element(name = "SubjectDescription", required = false)
    var desc: String? = null,
    @field:Element(name = "IsActiveFlag", required = false)
    var isActive: String? = null,
    @field:ElementList(required = false, inline = true)
    var topics: List<TopicDto?>? = null
)

@Root(name = "Topic", strict = false)
class TopicDto(
    @field:Element(name = "SubjectId", required = false)
    var subjectId: String? = null,
    @field:Element(name = "TopicDescription", required = false)
    var desc: String? = null,
    @field:Element(name = "IsActiveFlag", required = false)
    var isActive: String? = null,
    @field:Element(name = "TopicId", required = false)
    var topicId: String? = null
)

@Root(name = "Accounts", strict = false)
class AccountRefDto(
    @field:Element(name = "InstName", required = false)
    var instName: String? = null,
    @field:Element(name = "AccountId", required = false)
    var accountId: String? = null,
    @field:Element(name = "AccountInstNo", required = false)
    var accountInstNo: String? = null,
    @field:Element(name = "ProductCd", required = false)
    var productCd: String? = null,
    @field:Element(name = "AccountName", required = false)
    var name: String? = null
)
