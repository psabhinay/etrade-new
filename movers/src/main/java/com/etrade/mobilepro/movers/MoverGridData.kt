package com.etrade.mobilepro.movers

import java.util.Date

data class MoverGridData(
    val items: List<MoverItem> = emptyList(),
    val timestamp: Long = Date().time
)
