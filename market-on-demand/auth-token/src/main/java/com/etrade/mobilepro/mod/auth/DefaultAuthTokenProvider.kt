package com.etrade.mobilepro.mod.auth

import com.etrade.mobilepro.mod.auth.api.AuthTokenProvider
import java.io.IOException
import javax.inject.Inject

class DefaultAuthTokenProvider @Inject constructor(
    private val modAuthService: ModAuthService
) : AuthTokenProvider {

    private val authTokenMatcher by lazy {
        Regex(
            pattern =
            """ReportSearch/\?symbol=.+&(.*)"""
        )
    }

    @Throws(IOException::class, AuthTokenProvider.ModAuthTokenException::class)
    override fun getModAuthToken(ticker: String): String {
        require(ticker.isNotEmpty()) { "Ticker can not be empty" }

        val authTokenResponse = modAuthService.getWsodResearchReportUrl(ticker).execute()
        if (authTokenResponse.isSuccessful) {
            val responseBody = authTokenResponse.body()
            return extractAuthToken(responseBody?.reportUrl) ?: throw AuthTokenProvider.ModAuthTokenException("Could not extract mod auth token: $responseBody")
        } else {
            throw AuthTokenProvider.ModAuthTokenException("Could not generate mod auth token. Error response: $authTokenResponse")
        }
    }

    private fun extractAuthToken(wsodReportUrl: String?): String? = wsodReportUrl?.let { reportUrl ->
        authTokenMatcher.find(reportUrl)?.groupValues?.get(1)
    }
}
