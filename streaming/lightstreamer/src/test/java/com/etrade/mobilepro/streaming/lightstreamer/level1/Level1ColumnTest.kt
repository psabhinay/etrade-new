package com.etrade.mobilepro.streaming.lightstreamer.level1

import com.etrade.eo.streaming.UpdateInfo
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class Level1ColumnTest {

    @Test
    fun `check value changed result`() {
        val updateInfo = createUpdateInfo(true)
        Level1Column.values().forEach {
            assertNotNull(updateInfo.getValue(it))
        }
    }

    @Test
    fun `check value unchanged result`() {
        val updateInfo = createUpdateInfo(false, null)
        Level1Column.values().forEach {
            assertNull(updateInfo.getValue(it))
        }
    }

    @Test
    fun `check changed ticker indicator`() {
        assertEquals(-1, createUpdateInfo(true, "-").getTickIndicator(Level1Column.LAST))
        assertEquals(1, createUpdateInfo(true, "+").getTickIndicator(Level1Column.LAST))
    }

    @Test
    fun `check unchanged ticker indicator`() {
        assertNull(createUpdateInfo(false, "-").getTickIndicator(Level1Column.LAST))
        assertNull(createUpdateInfo(false, "+").getTickIndicator(Level1Column.LAST))
    }

    @Test
    fun `check neutral tick indicator`() {
        assertEquals(0, createLastUnChangedVolumeChanged().getTickIndicator(Level1Column.LAST))
    }

    private fun createUpdateInfo(changed: Boolean, value: String? = "value"): UpdateInfo {
        return mock {
            whenever(it.isValueChanged(any<String>())).thenReturn(changed)
            whenever(it.getNewValue(any<String>())).thenReturn(value)
        }
    }

    private fun createLastUnChangedVolumeChanged(): UpdateInfo {
        return mock {
            whenever(it.isValueChanged(Level1Column.LAST.column)).thenReturn(false)
            whenever(it.isValueChanged(Level1Column.ACCUM_VOLUME.column)).thenReturn(true)
            whenever(it.getNewValue(any<String>())).thenReturn("")
        }
    }
}
