package com.etrade.mobilepro.order.details.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

const val ORIG_SYS_CODE_REQ = "57"

@JsonClass(generateAdapter = true)
data class OrderDetailsRequestDto(
    @Json(name = "value")
    val orderRequest: OrderDetailsRequestValueDto
)

@JsonClass(generateAdapter = true)
data class OrderDetailsRequestValueDto(
    @Json(name = "accountId")
    val accountId: String,
    @Json(name = "orderNumber")
    val orderNumber: String = ""
)

@JsonClass(generateAdapter = true)
data class OrderDeleteRequestDto(
    @Json(name = "value")
    val orderRequest: OrderDeleteRequestValueDto
)

@JsonClass(generateAdapter = true)
data class OrderDeleteRequestValueDto(
    @Json(name = "accountId")
    val accountId: String,
    @Json(name = "preparedId")
    val preparedId: String,
    @Json(name = "origSysCodeReq")
    val origSysCodeReq: String = ORIG_SYS_CODE_REQ
)
