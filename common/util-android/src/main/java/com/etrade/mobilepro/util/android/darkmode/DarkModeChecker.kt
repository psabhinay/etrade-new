package com.etrade.mobilepro.util.android.darkmode

import android.content.Context
import com.etrade.mobilepro.util.android.extension.isInDarkMode
import java.lang.ref.WeakReference
import javax.inject.Inject

private const val DARK_MODE = 1
private const val LIGHT_MODE = 2

interface DarkModeChecker {
    val uiMode: Int
    fun isInDarkMode(): Boolean
    fun registerWith(context: Context)
}

class DefaultDarkModeChecker @Inject constructor() : DarkModeChecker {
    private var contextRef = WeakReference<Context>(null)
    override val uiMode: Int
        get() = if (isInDarkMode()) {
            DARK_MODE
        } else {
            LIGHT_MODE
        }

    override fun isInDarkMode(): Boolean {
        return contextRef.get()?.isInDarkMode() ?: false
    }

    override fun registerWith(context: Context) {
        this.contextRef = WeakReference(context)
    }
}
