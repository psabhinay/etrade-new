package com.etrade.mobilepro.feature

import app.cash.turbine.test
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.CoreMatchers.sameInstance
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class DefaultDelegatingFeatureFlagRepoTest {

    private lateinit var providers: List<TestFlagProvider>
    private lateinit var sut: DefaultDelegatingFeatureFlagRepo

    private val testDispatcher = TestCoroutineDispatcher()

    private val featureFalseDefault = BaseFeature("feature1", false)
    private val featureTrueDefault = BaseFeature("feature2", true)

    private val testFlagProvider1 = Mockito.spy(TestFlagProvider())

    @BeforeEach
    fun setup() {
        providers = listOf(testFlagProvider1, TestFlagProvider(), TestFlagProvider())
        sut = DefaultDelegatingFeatureFlagRepo(providers, testDispatcher, listOf(featureFalseDefault, featureTrueDefault))
    }

    @AfterEach
    fun teardown() {
        testDispatcher.cancelChildren()
    }

    @Test
    fun findProvider_providersEnabled_returnsHighestPriorityProvider() {
        testDispatcher.runBlockingTest {
            sut.findProvider(featureFalseDefault).test {
                assertThat(awaitItem(), nullValue())
                expectNoEvents()
                providers[2].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), sameInstance(providers[2]))
                expectNoEvents()
                providers[0].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), sameInstance(providers[0]))
                expectNoEvents()
                providers[1].setFeatureEnabled(featureFalseDefault, false)
                expectNoEvents()
                providers[0].setFeatureEnabled(featureFalseDefault, false)
                expectNoEvents()
            }
        }
    }

    @Test
    fun findProvider_lowPriorityProviderChanged_resultUnchanged() {
        testDispatcher.runBlockingTest {
            sut.findProvider(featureFalseDefault).test {
                assertThat(awaitItem(), nullValue())
                expectNoEvents()
                providers[0].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), sameInstance(providers[0]))
                expectNoEvents()
                providers[1].setFeatureEnabled(featureFalseDefault, false)
                expectNoEvents()
            }
        }
    }

    @Test
    fun findProvider_otherFeatureChanged_resultUnchanged() {
        testDispatcher.runBlockingTest {
            sut.findProvider(featureFalseDefault).test {
                assertThat(awaitItem(), nullValue())
                expectNoEvents()
                providers[1].setFeatureEnabled(featureTrueDefault, true)
                expectNoEvents()
            }
        }
    }

    @Test
    fun findProvider_lowPriorityProviderDisabled_resultUnchanged() {
        testDispatcher.runBlockingTest {
            sut.findProvider(featureFalseDefault).test {
                assertThat(awaitItem(), nullValue())
                providers[2].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), sameInstance(providers[2]))
                providers[0].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), sameInstance(providers[0]))
                providers[0].removeFeature(featureFalseDefault)
                assertThat(awaitItem(), sameInstance(providers[2]))
                expectNoEvents()
            }
        }
    }

    @Test
    fun findProvider_highPriorityStoreCleared_returnsNextProvider() {
        testDispatcher.runBlockingTest {
            sut.findProvider(featureFalseDefault).test {
                assertThat(awaitItem(), nullValue())
                providers[2].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), sameInstance(providers[2]))
                providers[0].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), sameInstance(providers[0]))
                providers[0].clearFeatures()
                assertThat(awaitItem(), sameInstance(providers[2]))
                expectNoEvents()
            }
        }
    }

    @Test
    fun findProvider_highPriorityProviderDisabled_returnsHighestPriorityProvider() {
        testDispatcher.runBlockingTest {
            sut.findProvider(featureFalseDefault).test {
                assertThat(awaitItem(), nullValue())
                expectNoEvents()
                providers[2].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), sameInstance(providers[2]))
                providers[0].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), sameInstance(providers[0]))
                providers[0].removeFeature(featureFalseDefault)
                assertThat(awaitItem(), sameInstance(providers[2]))
                expectNoEvents()
            }
        }
    }

    @Test
    fun isFeatureEnabled_providersEnabled_returnsHighestPriorityValue() {
        testDispatcher.runBlockingTest {
            sut.isFeatureEnabled(featureFalseDefault).test {
                assertThat(awaitItem(), equalTo(false))
                expectNoEvents()
                providers[2].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), equalTo(true))
                expectNoEvents()
                providers[0].setFeatureEnabled(featureFalseDefault, false)
                assertThat(awaitItem(), equalTo(false))
                expectNoEvents()
            }
        }
    }

    @Test
    fun isFeatureEnabled_lowPriorityProviderChanged_resultUnchanged() {
        testDispatcher.runBlockingTest {
            sut.isFeatureEnabled(featureFalseDefault).test {
                assertThat(awaitItem(), equalTo(false))
                expectNoEvents()
                providers[0].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), equalTo(true))
                expectNoEvents()
                providers[1].setFeatureEnabled(featureFalseDefault, false)
                expectNoEvents()
            }
        }
    }

    @Test
    fun isFeatureEnabled_otherFeatureChanged_resultUnchanged() {
        testDispatcher.runBlockingTest {
            sut.isFeatureEnabled(featureFalseDefault).test {
                assertThat(awaitItem(), equalTo(false))
                expectNoEvents()
                providers[1].setFeatureEnabled(featureTrueDefault, true)
                expectNoEvents()
            }
        }
    }

    @Test
    fun isFeatureEnabled_highPriorityProviderDisabled_returnsHighestPriorityValue() {
        testDispatcher.runBlockingTest {
            sut.isFeatureEnabled(featureFalseDefault).test {
                assertThat(awaitItem(), equalTo(false))
                expectNoEvents()
                providers[2].setFeatureEnabled(featureFalseDefault, true)
                assertThat(awaitItem(), equalTo(true))
                providers[0].setFeatureEnabled(featureFalseDefault, false)
                assertThat(awaitItem(), equalTo(false))
                providers[0].removeFeature(featureFalseDefault)
                assertThat(awaitItem(), equalTo(true))
                expectNoEvents()
            }
        }
    }

    @Test
    fun findProvider_removeUnavailableFeature_returnsNullProvider() {
        testDispatcher.runBlockingTest {
            sut = DefaultDelegatingFeatureFlagRepo(providers, testDispatcher, listOf(featureFalseDefault))
            testFlagProvider1.setFeatureEnabled(featureTrueDefault, true)
            sut.findProvider(featureTrueDefault).test {
                assertThat(awaitItem(), equalTo(null))
                expectNoEvents()
            }
            verify(testFlagProvider1).removeFeature(featureTrueDefault)
        }
    }

    private class TestFlagProvider : FeatureFlagStore {
        override val title = "Test"
        override suspend fun clearFeatures() {
            featureValues.value = emptyMap()
        }

        val featureValues = MutableStateFlow<Map<Feature, Boolean>>(emptyMap())

        override suspend fun setFeatureEnabled(feature: Feature, enabled: Boolean) {
            featureValues.value = featureValues.value + (feature to enabled)
        }

        override suspend fun removeFeature(feature: Feature) {
            featureValues.value = featureValues.value.toMutableMap().apply {
                remove(feature)
            }.toMap()
        }

        override fun isFeatureEnabled(feature: Feature): Flow<Boolean> = featureValues.mapNotNull {
            it[feature]
        }

        override fun hasFeature(feature: Feature): Flow<Boolean> =
            featureValues.map { it.containsKey(feature) }
    }
}
