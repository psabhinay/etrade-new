package com.etrade.mobilepro.arr.data

import com.etrade.eo.rest.retrofit.Xml
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface AnalystResearchReportService {

    @Xml
    @GET
    fun getAnalystResearchReports(@Url httpUrl: String): Single<AnalystReportDto>

    @Xml
    @GET("/e/t/mobile/GetWSODResearchReportUrl")
    fun getAnalystResearchReportListUrl(@Query("Ticker") ticker: String): Single<WSODResearchReportUrl>
}
