package com.etrade.mobilepro.transfermoney.widget

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.transfermoney.usecase.calendar.GetTransferCalendarUseCase
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.stub
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate

@ExperimentalCoroutinesApi
class TransferMoneyCalendarViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var useCaseMock: GetTransferCalendarUseCase

    @Mock
    private lateinit var observerMock: Observer<ViewState>

    private val dispatcher = TestCoroutineDispatcher()

    private val testCurrentDate = LocalDate.parse("2020-01-01")

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        dispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `current day is pre-selected when there are no holidays`() {
        useCaseMock.stub {
            onBlocking { execute(any()) } doReturn ETResult.success(emptySet())
        }

        val sut = generateSut()

        assertNotNull(sut.viewState.value)
        sut.viewState.value?.run {
            assertNotNull(selectedDate)
            assert(holidays?.isEmpty() == true)
            assert(selectedDate?.isEqual(testCurrentDate) == true)
        }
    }

    @Test
    fun `current day is not a holiday and it is pre-selected by default`() {
        useCaseMock.stub {
            onBlocking { execute(any()) } doReturn ETResult.success(
                setOf(
                    LocalDate.parse("2020-01-02"),
                    LocalDate.parse("2020-01-03"),
                    LocalDate.parse("2020-01-07")
                )
            )
        }

        val sut = generateSut()

        assertNotNull(sut.viewState.value)
        sut.viewState.value?.run {
            assertNotNull(selectedDate)
            assert(holidays?.size == 3)
            assert(selectedDate?.isEqual(testCurrentDate) == true)
        }
    }

    @Test
    fun `when origin account is external and current day is a holiday, pre-selected date is provided`() {
        useCaseMock.stub {
            onBlocking { execute(any()) } doReturn ETResult.success(
                setOf(
                    testCurrentDate,
                    LocalDate.parse("2020-01-03")
                )
            )
        }

        val sut = generateSut(isOriginAccountExternal = true)

        assertNotNull(sut.viewState.value)
        sut.viewState.value?.run {
            assert(holidays?.size == 2)
            assert(holidays?.contains(selectedDate) == false)
            assertNotNull(selectedDate)
            selectedDate?.let { date ->
                assert(date.dayOfWeek != DayOfWeek.SATURDAY && date.dayOfWeek != DayOfWeek.SUNDAY)
            }
        }
    }

    @Test
    fun `when destination account is external and current day is a holiday, pre-selected date is provided`() {
        useCaseMock.stub {
            onBlocking { execute(any()) } doReturn ETResult.success(
                setOf(
                    testCurrentDate,
                    LocalDate.parse("2020-01-03")
                )
            )
        }

        val sut = generateSut(isDestinationAccountExternal = true)

        assertNotNull(sut.viewState.value)
        sut.viewState.value?.run {
            assert(holidays?.size == 2)
            assert(holidays?.contains(selectedDate) == false)
            assertNotNull(selectedDate)
            selectedDate?.let { date ->
                assert(date.dayOfWeek != DayOfWeek.SATURDAY && date.dayOfWeek != DayOfWeek.SUNDAY)
            }
        }
    }

    @Test
    fun `pre-selected date can be provided even though there are many holidays and destination account is external`() {
        val holidayCount = 29L
        val holidaysMock = mutableSetOf<LocalDate>().apply {
            for (i in 0 until holidayCount) {
                add(testCurrentDate.plusDays(i))
            }
        }

        useCaseMock.stub {
            onBlocking { execute(any()) } doReturn ETResult.success(holidaysMock)
        }

        val sut = generateSut(isDestinationAccountExternal = true)

        assertNotNull(sut.viewState.value)
        sut.viewState.value?.run {
            assertEquals(holidays?.size, holidayCount.toInt())
            assertNotNull(selectedDate)
            selectedDate?.let { date ->
                assert(date.isAfter(testCurrentDate))
                assert(holidays?.contains(date) == false)
                assert(date.dayOfWeek != DayOfWeek.SATURDAY && date.dayOfWeek != DayOfWeek.SUNDAY)
            }
        }
    }

    private fun generateSut(
        isOriginAccountExternal: Boolean = false,
        isDestinationAccountExternal: Boolean = false
    ) = TransferMoneyCalendarViewModel(
        transferCalendarUseCase = useCaseMock,
        isOriginAccountExternal = isOriginAccountExternal,
        currentDate = testCurrentDate,
        isDestinationAccountExternal = isDestinationAccountExternal,
        initialDate = null,
        leadDays = null
    ).apply {
        viewState.observeForever(observerMock)
    }
}
