package com.etrade.mobilepro.news.presentation.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.news.presentation.databinding.ItemCompanyNewsHeaderBinding
import com.etrade.mobilepro.news.presentation.databinding.ItemTextNewsBinding
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.viewBinding

class NewsAdapter(
    private val selectNewsItemListener: ((NewsItem.Text, Int) -> Unit)? = null,
    private val symbolSelectListener: ((String) -> Unit)? = null,
    private val statusChecker: ((NewsItem.Text) -> Boolean)? = null,
    private val selectionChecker: ((Int) -> Boolean)? = null,
    private val showSearch: Boolean = true,
    private val lifecycleOwner: LifecycleOwner
) : PagingDataAdapter<NewsItem, RecyclerView.ViewHolder>(
    object : DiffUtil.ItemCallback<NewsItem>() {
        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: NewsItem, newItem: NewsItem): Boolean =
            oldItem == newItem

        override fun areItemsTheSame(oldItem: NewsItem, newItem: NewsItem): Boolean =
            oldItem == newItem

        override fun getChangePayload(oldItem: NewsItem, newItem: NewsItem): Any? {
            return null
        }
    }
) {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.item_text_news -> (holder as TextNewsItemViewHolder).bindTo(
                getItem(position),
                wasRead = statusChecker?.invoke(getItem(position) as NewsItem.Text),
                isSelected = selectionChecker?.invoke(position) ?: false
            )
            R.layout.item_company_news_header -> (holder as CompanyNewsHeaderViewHolder).bindTo(
                getItem(position),
                showSearch
            )
        }
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        onBindViewHolder(holder, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_text_news -> {
                TextNewsItemViewHolder(
                    parent.viewBinding(ItemTextNewsBinding::inflate),
                    selectNewsItemListener,
                    symbolSelectListener
                )
            }
            R.layout.item_empty_news -> {
                val view = parent.inflate(R.layout.item_empty_news)
                EmptyNewsItemViewHolder(view)
            }
            R.layout.item_company_news_header -> {
                val binding: ItemCompanyNewsHeaderBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_company_news_header, parent, false
                )
                binding.lifecycleOwner = lifecycleOwner

                CompanyNewsHeaderViewHolder(binding)
            }
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }

    override fun getItemViewType(position: Int) =
        when (getItem(position)) {
            is NewsItem.Text -> R.layout.item_text_news
            is NewsItem.CompanyHeader -> R.layout.item_company_news_header
            else -> R.layout.item_empty_news
        }
}
