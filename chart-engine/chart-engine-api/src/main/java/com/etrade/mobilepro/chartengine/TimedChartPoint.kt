package com.etrade.mobilepro.chartengine

import org.threeten.bp.LocalDateTime

interface TimedChartPoint {
    val x: LocalDateTime
    val y: Double
}
