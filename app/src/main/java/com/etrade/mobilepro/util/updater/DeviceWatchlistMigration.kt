package com.etrade.mobilepro.util.updater

import android.database.Cursor
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import java.io.File
import javax.inject.Inject

/**
 * A migration procedure that migrates device watchlist entries.
 *
 * @param watchlistRepo watchlist repository that will be used to store items
 */
class DeviceWatchlistMigration @Inject constructor(private val watchlistRepo: WatchlistRepo) : Migration {

    override suspend fun migrate(databaseFile: File) {
        getOldDeviceWatchlist(databaseFile)
            .map { WatchlistEntry(ticker = it, watchlistId = DEVICE_WATCHLIST_ID) }
            .takeIf { it.isNotEmpty() }
            ?.apply { watchlistRepo.saveWatchList(DEVICE_WATCHLIST_ID, this, emptyList()) }
    }

    private fun getOldDeviceWatchlist(databaseFile: File): List<String> {
        return readTableRows(databaseFile, "WATCHLISTENTRIES", listOf("Symbol")) {
            readWatchlistEntries(it)
        }
    }

    private fun readWatchlistEntries(cursor: Cursor): List<String> {
        return mutableListOf<String>().apply {
            while (cursor.moveToNext()) {
                add(cursor.getString(0))
            }
        }
    }
}
