package com.etrade.mobilepro.markets.data.response

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false, name = "GetDollarTopGainerResponse")
data class MarketDollarTopGainerResponse constructor(
    @field:ElementList(name = "QuoteList")
    override var quotes: List<Quote>? = null
) : MarketMoversResponse
