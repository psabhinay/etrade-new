package com.etrade.mobilepro.dummy

import com.etrade.mobilepro.accountslist.data.dto.AccountReferenceDto
import com.etrade.mobilepro.accountslist.data.dto.AccountsListDto
import com.etrade.mobilepro.accountslist.data.dto.AccountsViewDto

private const val COUNT = 5

object AccountsViewDummyData {

    private val accountIds = (1..COUNT).map { it.toString() }

    val views: List<AccountsViewDto>
    val references: List<AccountReferenceDto>

    init {
        views = listOf(createView(accountIds))
        references = createAccountReferences()
    }

    private fun createAccountReferences() = (1..COUNT).map { createDummyAccountReferenceDto() }

    private fun createView(accountIds: List<String>) = AccountsViewDto(AccountsListDto(accountIds), null)
}
