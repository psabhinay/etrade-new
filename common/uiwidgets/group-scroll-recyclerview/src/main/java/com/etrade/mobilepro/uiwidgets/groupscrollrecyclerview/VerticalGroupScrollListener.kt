package com.etrade.mobilepro.uiwidgets.groupscrollrecyclerview

import android.view.MotionEvent
import android.view.MotionEvent.ACTION_CANCEL
import android.view.MotionEvent.ACTION_DOWN
import android.view.MotionEvent.ACTION_MOVE
import android.view.MotionEvent.ACTION_UP
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE

/**
 * A RecyclerView.OnScrollListener attached to multiple [VerticalGroupScrollRecyclerView]s to synchronize their vertical scrolling.
 */
class VerticalGroupScrollListener(
    private val synchronizedGroup: Collection<VerticalGroupScrollRecyclerView>
) : RecyclerView.OnScrollListener(), RecyclerView.OnItemTouchListener {

    private var scrolledY: Int = 0
    private var currentTouchedRecyclerView: RecyclerView? = null
    private var lastTouchedRecyclerView: RecyclerView? = null
    private var moved: Boolean = false

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (newState == SCROLL_STATE_IDLE) {
            // on scrolling stops, we remove scroll listener.
            recyclerView.removeOnScrollListener(this)
            currentTouchedRecyclerView = null
            moved = false
        }
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        synchronizedGroup.forEach {
            if (it != recyclerView) {
                it.scrollBy(0, dy)
            }
        }
    }

    @Suppress("LongMethod", "NestedBlockDepth")
    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
        // prevent multi-touch since we're reusing this listener across multiple recycler views
        if ((currentTouchedRecyclerView != null && rv != currentTouchedRecyclerView)) {
            return true
        }

        when (e.action) {
            ACTION_DOWN -> {
                currentTouchedRecyclerView = rv
                if (rv.scrollState == SCROLL_STATE_IDLE) {
                    // This is to prevent more than one scroll listener added across all the synchronized views.
                    lastTouchedRecyclerView?.let {
                        if (rv != it) {
                            it.removeOnScrollListener(this)
                            it.stopScroll()
                        }
                    }

                    scrolledY = rv.computeVerticalScrollOffset()
                    rv.addOnScrollListener(this)
                }
            }
            ACTION_MOVE -> {
                currentTouchedRecyclerView = rv
                moved = true
            }
            ACTION_UP, ACTION_CANCEL -> {
                // if no scroll happened or is going to happen, we remove the scroll listener
                if (!moved && rv.computeVerticalScrollOffset() == scrolledY && rv.scrollState == SCROLL_STATE_IDLE) {
                    rv.removeOnScrollListener(this)
                }
                currentTouchedRecyclerView = null
                lastTouchedRecyclerView = rv
            }
        }

        return false
    }

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        // NO OP
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        // NO OP
    }
}
