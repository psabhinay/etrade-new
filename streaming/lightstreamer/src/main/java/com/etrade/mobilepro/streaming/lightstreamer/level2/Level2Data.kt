package com.etrade.mobilepro.streaming.lightstreamer.level2

import com.etrade.mobilepro.streaming.api.Level2DataEvent

/**
 * Level 2 quotes data.
 */
class Level2Data(
    val bidList: List<Level2DataEvent> = emptyList(),
    val askList: List<Level2DataEvent> = emptyList()
)
