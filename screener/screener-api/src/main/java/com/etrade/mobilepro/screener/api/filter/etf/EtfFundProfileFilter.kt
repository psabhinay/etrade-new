package com.etrade.mobilepro.screener.api.filter.etf

import com.etrade.mobilepro.screener.api.EtfFundGroup
import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.FundFamily
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class EtfFundProfileFilter(
    val fundFamily: FundFamily,
    val etfFundGroup: EtfFundGroup,
    val fundCategories: Set<FundCategory>,

    val bottom20: Boolean = false,
    val belowMid20: Boolean = false,
    val mid20: Boolean = false,
    val aboveMid20: Boolean = false,
    val top20: Boolean = false,

    val leveragedEtfsExclude: Boolean = false,
    val leveragedEtfsShowOnly: Boolean = false,

    val exchangeTradedNotesExclude: Boolean = false,
    val exchangeTradedNotesShowOnly: Boolean = false,

    val morningStarRating: Set<Int> = emptySet(),

    val allStarsFunds: Boolean = false,
    val optionsAvailable: Boolean = false
) : ScreenerFilter {
    private fun isFundCategoryEmpty() = fundCategories.isEmpty() || (fundCategories.size == 1 && fundCategories.first().isAllItemsElement)

    fun isExpenseRatioValueSelected() = bottom20 || belowMid20 || mid20 ||
        aboveMid20 || top20

    override fun isEmpty(): Boolean = fundFamily.isAllItemsElement && etfFundGroup.isAllItemsElement && isFundCategoryEmpty() &&
        !isExpenseRatioValueSelected() && !leveragedEtfsExclude && !leveragedEtfsShowOnly && !exchangeTradedNotesExclude &&
        !exchangeTradedNotesShowOnly && morningStarRating.isEmpty() && !allStarsFunds && !optionsAvailable

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.ETF
}
