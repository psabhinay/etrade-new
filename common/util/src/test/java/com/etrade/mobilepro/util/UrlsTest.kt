package com.etrade.mobilepro.util

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.Test

class UrlsTest {

    @Test
    fun `prependBaseUrl replaces path-only url`() {
        val url = "/z/foo"
        assertThat(
            url.prependBaseUrl("https://test.com"),
            `is`("https://test.com/z/foo")
        )
    }

    @Test
    fun `prependBaseUrl ignores full urls`() {
        val url = "https://test2.com/z/foo"
        assertThat(
            url.prependBaseUrl("https://test.com"),
            `is`("https://test2.com/z/foo")
        )
    }

    @Test
    fun `prependBaseUrl returns baseUrl for null`() {
        val url = null
        assertThat(
            url.prependBaseUrl("https://test.com"),
            `is`("https://test.com")
        )
    }
}
