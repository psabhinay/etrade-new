package com.etrade.mobilepro.checkdeposit.presentation.details.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.checkdeposit.presentation.databinding.AdapterDepositFooterMessageBinding
import com.etrade.mobilepro.checkdeposit.presentation.details.DepositDetail
import com.etrade.mobilepro.checkdeposit.presentation.details.holder.DepositDetailFooterMessageViewHolder
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class DepositFooterMessageAdapterDelegate : AbsListItemAdapterDelegate<DepositDetail.FooterMessage, DepositDetail, DepositDetailFooterMessageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): DepositDetailFooterMessageViewHolder {
        return DepositDetailFooterMessageViewHolder(parent.viewBinding(AdapterDepositFooterMessageBinding::inflate))
    }

    override fun isForViewType(item: DepositDetail, items: MutableList<DepositDetail>, position: Int): Boolean = item is DepositDetail.FooterMessage

    override fun onBindViewHolder(item: DepositDetail.FooterMessage, holder: DepositDetailFooterMessageViewHolder, payloads: MutableList<Any>) {
        holder.bind(item.text)
    }
}
