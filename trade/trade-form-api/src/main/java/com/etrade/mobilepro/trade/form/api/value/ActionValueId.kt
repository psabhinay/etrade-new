package com.etrade.mobilepro.trade.form.api.value

import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.util.MutualConverter

interface ActionValueId {
    val buy: String
    val buyToCover: String
    val exchange: String
    val sell: String
    val sellShort: String
    val converter: MutualConverter<String, TradeAction>
}
