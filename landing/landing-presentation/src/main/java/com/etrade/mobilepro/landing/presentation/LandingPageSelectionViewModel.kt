package com.etrade.mobilepro.landing.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.etrade.mobilepro.landing.api.LandingPage
import com.etrade.mobilepro.landing.api.LandingRouter
import com.etrade.mobilepro.landing.api.repo.LandingRepo
import com.etrade.mobilepro.landing.presentation.helper.LandingPageSelectionShowHideHelper
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.guard
import com.etrade.mobilepro.viewmodel.NavigationSignal
import com.etrade.mobilepro.viewmodel.NavigationSignalViewModel
import javax.inject.Inject

class LandingPageSelectionViewModel @Inject constructor(
    landingPageSelectionShowHideHelper: LandingPageSelectionShowHideHelper,
    private val landingRepo: LandingRepo,
    private val landingRouter: LandingRouter
) : ViewModel(), LandingPageSelectionShowHideHelper by landingPageSelectionShowHideHelper, NavigationSignalViewModel {

    override val navigationSignal: LiveData<ConsumableLiveEvent<NavigationSignal>>
        get() = _navigationSignal

    private val _navigationSignal: MutableLiveData<ConsumableLiveEvent<NavigationSignal>> = MutableLiveData()

    private val _hideSignal: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()
    internal val hideSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _hideSignal

    private val _viewState = MutableLiveData<ViewState>()
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    internal fun fetchData() {
        if (viewState.value == null) {
            val landingState = landingRepo.getLandingState()
            _viewState.value = ViewState(
                status = ViewState.Status.MAIN_ITEMS,
                landingRepo = landingRepo,
                landingPages = landingState.pages
            )
        }
    }

    internal fun itemSelected(item: LandingPage) {
        val newStatus = createNewStatus(item) guard {
            val actualItem = item.subItems?.firstOrNull() ?: item
            landingRepo.setSelectedPage(actualItem)
            landingRouter.createPendingNavigation()
            landingRouter.getPendingNavigation()?.let {
                _navigationSignal.value = { controller: NavController ->
                    it.navPreparation?.invoke()
                    controller.navigate(it.navDirections, it.navOptions)
                }.consumable()
            }
            _hideSignal.value = Unit.consumable()
            hideLandingPageSelection()
            return
        }
        _viewState.updateValue {
            it.copy(status = newStatus)
        }
    }

    internal fun onBackPressed() {
        _viewState.updateValue {
            it.copy(status = ViewState.Status.MAIN_ITEMS)
        }
    }

    private fun createNewStatus(item: LandingPage): ViewState.Status? =
        if (item.hasNoneOrOneSubItem() || item.id != LandingPage.PORTFOLIO_ID) {
            null
        } else {
            ViewState.Status.PORTFOLIO_ITEMS
        }

    internal data class ViewState(
        val status: Status = Status.LOADING,
        private val landingRepo: LandingRepo,
        private val landingPages: List<LandingPage> = emptyList()
    ) {
        enum class Status { LOADING, MAIN_ITEMS, PORTFOLIO_ITEMS }

        private val portfolioSubItems = landingPages.find { it is LandingPage.Portfolio }?.subItems ?: emptyList()
        private val hasMoreThanOneAccount = landingRepo.getLandingState().userAccounts.hasMoreThanOneAccount()
        private val shouldShowPortfolioPage = landingRepo.getLandingState().userAccounts.brokerageAccounts.isNotEmpty()

        private val allItems = landingPages.asSequence()
            .filterNot {
                it is LandingPage.AllBrokerage && (portfolioSubItems.size == 1 || !hasMoreThanOneAccount)
            }
            .filter { shouldShowPortfolioPage || it !is LandingPage.Portfolio }
            .toList()
        val items: List<LandingPage> = when (status) {
            Status.LOADING,
            Status.MAIN_ITEMS -> allItems
            Status.PORTFOLIO_ITEMS -> portfolioSubItems
        }

        val hasBackArrow: Boolean = !(status == Status.LOADING || status == Status.MAIN_ITEMS)
    }
}
