package com.etrade.mobilepro.orders.list.mapper

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.orders.data.dto.OrderDataDto
import com.etrade.mobilepro.orders.data.dto.StockPlanOrdersResponseDto
import com.etrade.mobilepro.testutil.getObjectFromJson
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.simpleframework.xml.convert.AnnotationStrategy
import org.simpleframework.xml.core.Persister

internal class OrdersMapperTest {
    @Test
    fun `test orders list`() {
        val response = getResponse()
        assertNotNull(response.data)
        val orderList = response.data?.ordersResponse?.orderList?.map {
            OrderItemMapper().map(it)
        }
        assertNotNull(orderList)
        assertEquals(20, orderList?.size, "20 items in response")

        val appleStockOrder = orderList?.get(0)
        appleStockOrder?.apply {
            assertEquals("2067", orderNumber)
            assertEquals(OrderStatus.CANCELLED, orderStatus)
            assertEquals(OrderType.EQUITY, orderType)
            assertEquals(AdvancedOrderType.UNKNOWN, advancedOrderType)
            assertEquals(1, orderDescriptions.size)
            val appleStockOrderDescription = orderDescriptions[0]
            assertEquals("99", appleStockOrderDescription.fillQuantityValue)
            assertEquals(TransactionType.BUY_OPEN, appleStockOrderDescription.transactionType)
            assertEquals("AAPL", appleStockOrderDescription.displaySymbol)
        }

        val epamContingentOrder = orderList?.get(3)
        epamContingentOrder?.apply {
            assertEquals("2064", orderNumber)
            assertEquals(OrderStatus.EXPIRED, orderStatus)
            assertEquals(OrderType.CONTINGENT, orderType)
            assertEquals(AdvancedOrderType.AO_CONTINGENT, advancedOrderType)
            assertEquals(1, orderDescriptions.size)
            val epamContingentOrderDescription = orderDescriptions[0]
            assertEquals("200/500", epamContingentOrderDescription.fillQuantityValue)
            assertEquals(TransactionType.BUY_OPEN, epamContingentOrderDescription.transactionType)
            assertEquals("EPAM", epamContingentOrderDescription.displaySymbol)
        }

        val multiLegOrder = orderList?.get(4)
        multiLegOrder?.apply {
            assertEquals("2063", orderNumber)
            assertEquals(OrderStatus.EXPIRED, orderStatus)
            assertEquals(OrderType.CUSTOM, orderType)
            assertEquals(AdvancedOrderType.UNKNOWN, advancedOrderType)
            assertEquals(3, orderDescriptions.size)
            assertEquals(listOf("HUN", "HUN---190517P00024000", "HUN---190517C00017000"), symbols)
            val multiLegOrderFirst = orderDescriptions.elementAtOrNull(0)
            multiLegOrderFirst?.let { firstLeg ->
                assertEquals("100", firstLeg.fillQuantityValue)
                assertEquals(TransactionType.BUY_OPEN, firstLeg.transactionType)
                assertEquals("HUN", firstLeg.displaySymbol)
            }

            val multiLegOrderSecond = orderDescriptions.elementAtOrNull(1)
            multiLegOrderSecond?.let { secondLeg ->
                assertEquals("1", secondLeg.fillQuantityValue)
                assertEquals(TransactionType.BUY_OPEN_OPT, secondLeg.transactionType)
                assertEquals("HUN May 17 '19 \$24 Put", secondLeg.displaySymbol)
            }
        }

        val conditionalOrder = orderList?.get(18)
        conditionalOrder?.apply {
            assertEquals("2019", orderNumber)
            assertEquals(OrderStatus.EXPIRED, orderStatus)
            assertEquals(OrderType.CONDITIONAL_ONE_TRIGGER_OTHER, orderType)
            assertEquals(listOf("TSLA", "SPX", "SPXW--190814C02690000", "GOOGL", "GOOGL-190726C01162500"), symbols)
            assertEquals(3, orderDescriptions.size)
            val firstOrderDescription = orderDescriptions[0]
            assertEquals("5", firstOrderDescription.fillQuantityValue)
            assertEquals(TransactionType.BUY_OPEN, firstOrderDescription.transactionType)
            assertEquals("TSLA", firstOrderDescription.displaySymbol)
            val secondOrderDescription = orderDescriptions[1]
            assertEquals("4", secondOrderDescription.fillQuantityValue)
            assertEquals(TransactionType.BUY_OPEN_OPT, secondOrderDescription.transactionType)
            assertEquals("SPXW Aug 14 '19 \$2690 Call", secondOrderDescription.displaySymbol)
            val thirdOrderDescription = orderDescriptions[2]
            assertEquals("5", thirdOrderDescription.fillQuantityValue)
            assertEquals(TransactionType.BUY_OPEN_OPT, thirdOrderDescription.transactionType)
            assertEquals("GOOGL Jul 26 '19 \$1162.50 Call", thirdOrderDescription.displaySymbol)
        }

        val conditionalOTOOrder = orderList?.get(19)
        conditionalOTOOrder?.apply {
            assertEquals("2048", orderNumber)
            assertEquals(OrderStatus.OPEN, orderStatus)
            assertEquals(OrderType.CONDITIONAL_ONE_TRIGGER_OTHER, orderType)
            assertEquals(listOf("FB", "F"), symbols)
            assertEquals(2, orderDescriptions.size)
            val firstOrderDescription = orderDescriptions[0]
            assertEquals("7", firstOrderDescription.fillQuantityValue)
            assertEquals(TransactionType.BUY_OPEN, firstOrderDescription.transactionType)
            assertEquals("FB", firstOrderDescription.displaySymbol)
            val secondOrderDescription = orderDescriptions[1]
            assertEquals("5", secondOrderDescription.fillQuantityValue)
            assertEquals(TransactionType.BUY_OPEN, secondOrderDescription.transactionType)
            assertEquals("F", secondOrderDescription.displaySymbol)
        }
    }

    @Test
    fun `test saved orders list and all statuses are saved`() {
        val response = getSavedOrderResponse()
        assertNotNull(response.data)
        val orderList = response.data?.savedOrderResponse?.orderList?.map {
            OrderItemMapper().map(it)
        }
        assertEquals(5, orderList?.size)
        orderList?.let {
            for (orderItem in orderList) {
                assertEquals(OrderStatus.SAVED, orderItem.orderStatus)
            }
        }
    }

    @Test
    fun `test mapStockPanOrder`() {
        val response = getStockPlanOrdersResponse()
        assertNotNull(response)
        val order = response?.let {
            OrderItemMapper().mapStockPlanOrders(it)
        }
        order?.get(0)?.apply {
            assertEquals(orderNumber, "6336516")
            assertEquals(date, "01/16/2020")
            assertEquals(planType, "Stock Options")
            assertEquals(orderType, "Cash Exercise")
            assertEquals(priceType, "n/a")
            assertEquals(executionPrice, "$0.00")
            assertEquals(sharesToExercise, "20")
            assertEquals(exercised, "2")
            assertEquals(sharesToSell, "10")
            assertEquals(sold, "0")
            assertEquals(accountIndex, "1")
            assertEquals(orderStatus, OrderStatus.EXECUTED)
        }
    }

    private fun getResponse(): ServerResponseDto<OrderDataDto> {
        return OrdersMapperTest::class.getObjectFromJson("order_list_response.json", ServerResponseDto::class.java, OrderDataDto::class.java)
    }

    private fun getSavedOrderResponse(): ServerResponseDto<OrderDataDto> {
        return OrdersMapperTest::class.getObjectFromJson("saved_order_list_response.json", ServerResponseDto::class.java, OrderDataDto::class.java)
    }

    private fun getStockPlanOrdersResponse(): StockPlanOrdersResponseDto? {
        val resource = this.javaClass.getResource("stock_plan_order_response.xml")?.readText()
        val persister = Persister(AnnotationStrategy())
        return try {
            persister.read(StockPlanOrdersResponseDto::class.java, resource, false)
        } catch (e: java.lang.Exception) {
            null
        }
    }
}
