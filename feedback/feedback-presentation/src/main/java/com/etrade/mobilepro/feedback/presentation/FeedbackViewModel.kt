package com.etrade.mobilepro.feedback.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.feedback.api.FeedbackConstants
import com.etrade.mobilepro.feedback.api.FeedbackModel
import com.etrade.mobilepro.feedback.api.FeedbackRepository
import com.etrade.mobilepro.feedback.api.FeedbackRequestFactory
import com.etrade.mobilepro.feedback.api.isSubmitSuccessful
import com.etrade.mobilepro.tracking.SCREEN_TITLE_LEAVE_FEEDBACK
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(FeedbackViewModel::class.java)

class FeedbackViewModel @Inject constructor(
    private val dispatchers: DispatcherProvider,
    private val feedbackRepository: FeedbackRepository,
    private val feedbackRequestFactory: FeedbackRequestFactory
) : ViewModel(), ScreenViewModel {
    private val _viewState = MutableLiveData<ViewState>().apply { value = ViewState.Default }
    val viewState: LiveData<ViewState> = _viewState

    override val screenName: String? = SCREEN_TITLE_LEAVE_FEEDBACK

    fun sendFeedback(rating: Int, text: String) {
        _viewState.value = ViewState.Loading

        val feedbackModel = FeedbackModel(rating, text, FeedbackConstants.menu)

        viewModelScope.launch {

            feedbackRepository.sendFeedback(
                feedbackRequestFactory.createFeedbackRequest(feedbackModel)
            )
                .onSuccess {
                    if (it.isSubmitSuccessful()) {
                        _viewState.value = ViewState.Success
                    } else {
                        logger.debug("Feedback response indicated failure: $it")
                        _viewState.value = ViewState.Failure
                    }
                }
                .onFailure {
                    logger.error("Feedback not successful", it)
                    _viewState.value = ViewState.Failure
                }
        }
    }

    sealed class ViewState {
        object Default : ViewState()
        object Loading : ViewState()
        object Success : ViewState()
        object Failure : ViewState()
    }
}
