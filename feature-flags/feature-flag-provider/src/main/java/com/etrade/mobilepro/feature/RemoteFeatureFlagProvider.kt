package com.etrade.mobilepro.feature

import com.etrade.mobilepro.common.result.ETResult

interface RemoteFeatureFlagProvider : FeatureFlagProvider {
    suspend fun refreshFeatureFlags(): ETResult<Unit>
}
