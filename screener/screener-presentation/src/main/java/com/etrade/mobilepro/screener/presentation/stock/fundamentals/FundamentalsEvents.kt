package com.etrade.mobilepro.screener.presentation.stock.fundamentals

import com.etrade.mobilepro.screener.api.filter.stock.FundamentalsFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class PeRatio : ScreenerFilterEvent<FundamentalsFilter, FundamentalsViewModel.ViewState, Boolean> {
    ZERO_15X {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(peRatio0to15x = eventValue))
    },
    FIFTEEN_25X {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(peRatio15to25x = eventValue))
    },
    TWENTY_FIVE_50X {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(peRatio25to50x = eventValue))
    },
    FIFTY_100X {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(peRatio50to100x = eventValue))
    },
    BELOW_AVG {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(peRatioBelowIndustryAvg = eventValue, peRatioAboveIndustryAvg = false))
    },
    ABOVE_AVG {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(peRatioAboveIndustryAvg = eventValue, peRatioBelowIndustryAvg = false))
    };

    override fun conditionalUpdate(
        viewState: FundamentalsViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<FundamentalsViewModel.ViewState> {
        var newState = update(viewState, eventValue)
        if (!newState.filter.peRatioRangeButtonsAreChecked()) {
            newState = uncheckPeriodButtons(newState)
        }
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = (newState.filter.peRatioButtonsAreChecked() && newState.filter.periodButtonsAreChecked()) ||
                (newState.filter.peRatioBlockIsEmpty() && !viewState.filter.peRatioBlockIsEmpty())
        )
    }

    private fun uncheckPeriodButtons(viewState: FundamentalsViewModel.ViewState): FundamentalsViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                peRatioOverTrailing12Months = false,
                peRatioOverCurrentFiscalYear = false,
                peRatioOverNextFiscalYear = false
            )
        )
}

internal enum class PeRatioPeriod : ScreenerFilterEvent<FundamentalsFilter, FundamentalsViewModel.ViewState, Boolean> {
    TRAILING_12_MONTHS {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(peRatioOverTrailing12Months = eventValue && viewState.filter.peRatioRangeButtonsAreChecked()))
    },
    CURRENT_FISCAL_YEAR {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(peRatioOverCurrentFiscalYear = eventValue && viewState.filter.peRatioRangeButtonsAreChecked()))
    },
    NEXT_FISCAL_YEAR {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(peRatioOverNextFiscalYear = eventValue && viewState.filter.peRatioRangeButtonsAreChecked()))
    };

    override fun conditionalUpdate(
        viewState: FundamentalsViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<FundamentalsViewModel.ViewState> {
        val newState = update(viewState, eventValue)
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = viewState.filter.peRatioButtonsAreChecked() && newState.filter.periodButtonsAreChecked()
        )
    }
}

internal enum class PriceBookRatio : ScreenerFilterEvent<FundamentalsFilter, FundamentalsViewModel.ViewState, Boolean> {
    ZERO_1X {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(priceBookRatio0to1x = eventValue))
    },
    ONE_2X {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(priceBookRatio1to2x = eventValue))
    },
    TWO_3X {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(priceBookRatio2to3x = eventValue))
    },
    GREATER_THAN_3X {
        override fun update(viewState: FundamentalsViewModel.ViewState, eventValue: Boolean): FundamentalsViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(priceBookRatioGreaterThan3x = eventValue))
    }
}
