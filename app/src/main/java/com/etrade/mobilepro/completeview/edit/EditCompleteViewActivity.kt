package com.etrade.mobilepro.completeview.edit

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import com.etrade.mobilepro.R
import com.etrade.mobilepro.accounts.AccountsOrganizeBoardingViewModel
import com.etrade.mobilepro.editlist.DragNDropTouchHelperCallback
import com.etrade.mobilepro.editlist.EditListActivity
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.editlist.ItemAdapter
import com.etrade.mobilepro.editlist.intent.BaseIntentBuilder
import com.etrade.mobilepro.editlist.intent.ListActionBuilder
import com.etrade.mobilepro.editlist.intent.ListEditDslMarker
import com.etrade.mobilepro.editlist.intent.isDeletableItems
import com.etrade.mobilepro.editlist.intent.isToggleableItems
import com.etrade.mobilepro.home.overview.edit.OverviewWidget
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsWebViewActivity
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsWebViewFragment.Companion.RESULT_CODE_MS_ACCOUNTS_WEB_VIEW_CLOSED
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.addDividerItemDecoration

class EditCompleteViewActivity : EditListActivity<OverviewWidget>() {

    override val viewModelClass: Class<out EditListViewModel<OverviewWidget>>
        get() = EditCompleteViewViewModel::class.java

    private val accountsOrganizeBoardingViewModel: AccountsOrganizeBoardingViewModel by viewModels { viewModelFactory }

    private val msAccountsWebViewRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_CODE_MS_ACCOUNTS_WEB_VIEW_CLOSED) {
                (viewModel as EditCompleteViewViewModel).refreshMsWidgetToggles()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val callback = DragNDropTouchHelperCallback(accountsOrganizeBoardingViewModel)
        val itemTouchHelper = ItemTouchHelper(callback)
        val itemAdapter = ItemAdapter(
            accountsOrganizeBoardingViewModel,
            itemTouchHelper,
            intent.isDeletableItems,
            intent.isToggleableItems
        ) {
            callback.isDragFinished
        }

        binding.topSeparator.visibility = View.VISIBLE

        initMsAccountVisibilitySettingView()

        binding.accountList.apply {
            visibility = View.VISIBLE
            adapter = itemAdapter
            itemTouchHelper.attachToRecyclerView(this)
            addDividerItemDecoration(R.drawable.thin_divider)
        }

        bindViewModel(itemAdapter)

        if (savedInstanceState == null) {
            accountsOrganizeBoardingViewModel.fetchItems()
        }
    }

    override fun onApplyClicked() {
        accountsOrganizeBoardingViewModel.applyChanges()
        super.onApplyClicked()
    }

    private fun bindViewModel(itemAdapter: ItemAdapter) {
        accountsOrganizeBoardingViewModel.listItems.observeBy(this) {
            if (it.size > 1) {
                itemAdapter.items = it
            } else {
                binding.topSeparator.visibility = View.GONE
            }
        }
    }

    private fun initMsAccountVisibilitySettingView() {
        val viewModel = this.viewModel as? EditCompleteViewViewModel ?: return

        if (viewModel.isMsAccountVisibilityCtaAvailable) {
            binding.bottomSeparator.visibility = View.VISIBLE
            binding.msAccountVisibilitySetting.root.apply {
                visibility = View.VISIBLE
                setOnClickListener {
                    launchMsAccountsWebViewActivity()
                }
            }
            viewModel.errorSignal.observe(
                this,
                Observer {
                    it.consume {
                        snackbarUtil.retrySnackbar(getString(com.etrade.mobilepro.editlist.R.string.error_message_general)) {
                            viewModel.refreshMsWidgetToggles()
                        }
                    }
                }
            )
        }
    }

    private fun launchMsAccountsWebViewActivity() {
        val viewModel = this.viewModel as? EditCompleteViewViewModel ?: return

        msAccountsWebViewRequest.launch(
            MsAccountsWebViewActivity.intent(
                this@EditCompleteViewActivity,
                viewModel.msSettingsCtaUrl,
                viewModel.msSettingsCtaTitle
            ).also {
                it.removeFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
        )
    }

    companion object {

        /**
         * Creates an intent to start [EditCompleteViewActivity].
         *
         * @param context a context of the application package implementing this class
         *
         * @return prepared intent
         */
        fun intent(context: Context, init: IntentBuilder.() -> Unit): Intent = IntentBuilder(context).apply(init).intent
    }

    /**
     * Builds an [Intent] to start [EditCompleteViewActivity].
     *
     * @param context a context of the application package implementing this class
     */
    @ListEditDslMarker
    class IntentBuilder(context: Context) : BaseIntentBuilder(Intent(context, EditCompleteViewActivity::class.java)) {

        init {
            isDeletableItems = false
            isToggleableItems = true
            listFooterMessage = R.string.edit_list_footer_message
            listAction {
                isVisible = false
            }
        }

        /**
         * Provide a list's action parameters.
         */
        private fun listAction(init: ListActionBuilder.() -> Unit) = ListActionBuilder(intent).init()
    }
}
