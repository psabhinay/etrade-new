package com.etrade.mobilepro.home

import android.content.Intent
import android.net.Uri
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_EXTRA_OPEN_MESSAGES
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.home.model.AppActionViewModel
import com.etrade.mobilepro.inboxmessages.getResolvedGenericDeepLink
import com.etrade.mobilepro.inboxmessages.isValidGenericLinkFormat
import com.etrade.mobilepro.navigation.EtNavController
import com.etrade.mobilepro.navigation.hasBeenToLoginScreen
import com.etrade.mobilepro.navigation.pendingNavUri
import com.etrade.mobilepro.onboarding.EXTRA_SHOW_OPEN_AN_ACCOUNT_PAGE
import com.etrade.mobilepro.pushnotification.api.SmartAlert
import com.etrade.mobilepro.pushnotification.presentation.SmartAlertNotificationFactory
import com.etrade.mobilepro.quotelookup.createSearchIntent
import com.etrade.mobilepro.quotes.QUOTE_BOTTOM_SHEET_TAG
import com.etrade.mobilepro.quotes.QuoteBottomSheetFragment
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.items.EXTRA_QUOTE_TICKER
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.util.android.ACTION_TOKEN_EXTRA
import com.etrade.mobilepro.util.android.logAppAction

internal const val ACTION_OPEN_WIDGET_SETTINGS = "MainActivity.action.open_widget_settings"
internal const val ACTION_OPEN_ACCOUNTS = "MainActivity.action.open_accounts"
internal const val ACTION_OPEN_WATCH_LIST = "MainActivity.action.open_widget_watch_list"
internal const val ACTION_ADD_SYMBOL_TO_WIDGET_WATCHLIST = "MainActivity.action.add_symbol_to_widget_watchlist"
internal const val ACTION_OPEN_SYMBOL_LOOKUP = "MainActivity.action.open_symbol_lookup"
internal const val ACTION_OPEN_TRADE_PAGE = "MainActivity.action.open_trade_page"

internal const val EXTRA_WATCHLIST_ID = "MainActivity.extra.watchlist_id"

internal const val FRAGMENT = "fragment"
internal const val QUOTE_SHEET = "quoteSheet"
internal const val COMPLETE_VIEW = "completeView"
internal const val INTENT_PROCESSED_FLAG = "this_intent_was_processed"

class MainActivityIntentHelper(
    private val activity: FragmentActivity,
    private val appActionViewModel: AppActionViewModel,
    private val etNavController: EtNavController,
    private val mainNavigation: MainNavigation,
    private val baseUrl: String
) {

    private var appActionToIgnore: Uri? = null

    fun onIntent(intent: Intent?) {
        intent ?: return
        if (intent.getBooleanExtra(EXTRA_SHOW_OPEN_AN_ACCOUNT_PAGE, false)) {
            intent.removeExtra(EXTRA_SHOW_OPEN_AN_ACCOUNT_PAGE)
            etNavController.clearPendingDirections()
            etNavController.navigate(MainGraphDirections.actionHomeToOpenAnAccount())
        } else {

            // Handles Intent with OPEN_ACCOUNT Only.
            handleIntentToOpenAccountsScreen(intent)
        }
        checkAppActionSuccessfulNavigation(intent)

        intent.getBundleExtra(EXTRA_QUOTE_TICKER)
            ?.let {
                intent.removeExtra(EXTRA_QUOTE_TICKER)
                mainNavigation.launchQuoteDetailsBottomSheet(activity, it)
            }
    }

    fun onNewIntent(intent: Intent) {
        if (intent.hasExtra(INTENT_PROCESSED_FLAG)) {
            return
        }

        (activity.supportFragmentManager.findFragmentByTag(QUOTE_BOTTOM_SHEET_TAG) as? QuoteBottomSheetFragment)?.dismissAllowingStateLoss()
        handleDestinationFragment(intent)

        handleChatIntent(intent)
        handleSmartAlertIntent(intent)
        handleAppActionIntent(intent)
        handleWatchlistIntent(intent)
        handleIntentToOpenWidgetSettings(intent)
        handleIntentToAddSymbolToWatchlist(intent)
        handleIntentToOpenSymbolLookup(intent)
        handleIntentToOpenTradePage(intent)
        handleIntentToOpenGenericDeepLink(intent)
        intent.putExtra(INTENT_PROCESSED_FLAG, true)
    }

    private fun handleDestinationFragment(intent: Intent) {
        val destination = intent.getStringExtra(FRAGMENT)
        if (destination != null) {
            when (destination) {
                QUOTE_SHEET -> {
                    val quoteTicker = intent.getParcelableExtra<SearchResultItem.Symbol>(EXTRA_QUOTE_TICKER)
                    mainNavigation.navigateToQuoteDetails(activity, requireNotNull(quoteTicker).toBundle())
                }
                COMPLETE_VIEW -> {
                    mainNavigation.navigateToCompleteView(activity)
                }
            }
        } else {
            intent.getBundleExtra(EXTRA_QUOTE_TICKER)
                ?.let { mainNavigation.navigateToQuoteDetails(activity, it) }
        }
    }

    private fun checkAppActionSuccessfulNavigation(intent: Intent) {
        val token = intent.getStringExtra(ACTION_TOKEN_EXTRA)
        if (pendingNavUri != null && token != null && hasBeenToLoginScreen) {
            logAppAction(token, false)
            intent.removeExtra(ACTION_TOKEN_EXTRA)
            etNavController.clearPendingAppActions()
        }
    }

    private fun handleChatIntent(intent: Intent) {
        if (intent.getBooleanExtra(CHAT_EXTRA_OPEN_MESSAGES, false)) {
            intent.removeExtra(CHAT_EXTRA_OPEN_MESSAGES)
            intent.extras?.let { etNavController.navigate(MainGraphDirections.actionHomeToChat(it)) }
        }
    }

    private fun handleWatchlistIntent(intent: Intent) {
        if (intent.action == ACTION_OPEN_WATCH_LIST) {
            intent.getStringExtra(EXTRA_WATCHLIST_ID)?.let {
                etNavController.navigate(MainGraphDirections.actionHomeToWatchlist(watchlistId = it))
            }
        }
    }

    private fun handleIntentToOpenWidgetSettings(intent: Intent) {
        if (intent.action == ACTION_OPEN_WIDGET_SETTINGS) {
            etNavController.navigate(MainGraphDirections.actionToWidgetSettings(widgetId = intent.getIntExtra(ACTION_OPEN_WIDGET_SETTINGS, 0)))
        }
    }

    private fun handleIntentToOpenAccountsScreen(intent: Intent) {
        if (intent.action == ACTION_OPEN_ACCOUNTS) {
            appActionViewModel.navigateToAccountsLandingPage { navigateToCompleteView ->
                if (navigateToCompleteView) {
                    etNavController.navigate(MainGraphDirections.actionHomeToCompleteview())
                } else {
                    etNavController.navigate(MainGraphDirections.actionHomeToOverview())
                }
            }

            // Clear the action once consumed
            intent.action = ""
        }
    }

    private fun handleIntentToAddSymbolToWatchlist(intent: Intent) {
        if (intent.action == ACTION_ADD_SYMBOL_TO_WIDGET_WATCHLIST) {
            etNavController.navigate(MainGraphDirections.actionToAddSymbolToWidgetWatchlist())
        }
    }

    private fun handleIntentToOpenSymbolLookup(intent: Intent) {
        if (intent.action == ACTION_OPEN_SYMBOL_LOOKUP) {
            activity.startActivity(
                activity.createSearchIntent(
                    StartSearchParams(
                        query = SearchQuery(SearchType.SYMBOL),
                        stockQuoteSearchOnly = false
                    )
                )
            )
        }
    }

    private fun handleIntentToOpenTradePage(intent: Intent) {
        if (intent.action == ACTION_OPEN_TRADE_PAGE) {
            etNavController.navigate(MainGraphDirections.actionLaunchTrade())
        }
    }

    private fun handleAppActionIntent(intent: Intent) {
        intent.data?.let {
            if (appActionToIgnore != it) {
                appActionViewModel.suppressNavigateToLandingPage = true
                etNavController.navigateFromAppAction(it)
            }
        }
        appActionToIgnore = null
    }

    fun handleCanceledAppAction() {
        appActionToIgnore = pendingNavUri
    }

    private fun handleSmartAlertIntent(intent: Intent) {
        val alert: SmartAlert = (intent.getSerializableExtra(SmartAlertNotificationFactory.EXTRA_SMART_ALERT_KEY) as? SmartAlert) ?: return

        when (alert) {
            is SmartAlert.Account -> {
                etNavController.navigate(MainGraphDirections.actionToAlerts(alertMessageFromNotification = alert.message))
            }
            is SmartAlert.Stock -> intent.getBundleExtra(EXTRA_QUOTE_TICKER)?.let { mainNavigation.navigateToQuoteDetails(activity, it) }
        }
    }

    private fun handleIntentToOpenGenericDeepLink(intent: Intent) {
        intent.data?.let { uri ->
            if (uri.isValidGenericLinkFormat()) {
                val deepLink = uri.getResolvedGenericDeepLink(baseUrl)
                activity.findNavController(R.id.nav_host_fragment).navigate(Uri.parse(deepLink))
            }
        }
    }
}
