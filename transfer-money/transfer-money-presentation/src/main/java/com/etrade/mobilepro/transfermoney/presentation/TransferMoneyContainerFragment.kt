package com.etrade.mobilepro.transfermoney.presentation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.RestoreTabPositionDelegate
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.tracking.initTabsTracking
import com.etrade.mobilepro.transfermoney.presentation.adapter.TransferMoneyPagerAdapter
import com.etrade.mobilepro.transfermoney.presentation.adapter.getActivityTabIndex
import com.etrade.mobilepro.transfermoney.presentation.databinding.TransferFragmentContainerBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.logAppAction
import javax.inject.Inject
import com.etrade.mobilepro.common.R as pool

const val TRANSFER_MONEY_PATH = "/transferMoney"
const val TRANSFER_MONEY_DISABLE_TABS_RESTORING_FLAG = "disable_restoring_tabs"

@RequireLogin
class TransferMoneyContainerFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val restoreTabPositionDelegate: RestoreTabPositionDelegate
) : Fragment(R.layout.transfer_fragment_container) {

    private val binding by viewBinding(TransferFragmentContainerBinding::bind)
    private val navigationFlowViewModel: TransferMoneyNavigationFlowViewModel by navGraphViewModels(R.id.transfer_money_nav_graph)

    private lateinit var viewModel: TransferMoneyContainerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logAppAction(activity?.intent, TRANSFER_MONEY_PATH)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = viewModels<TransferMoneyContainerViewModel> { viewModelFactory }.value
        TransferMoneyPagerAdapter(this).attach(binding.tabLayout, binding.viewPager)
        binding.apply {
            viewPager.initTabsTracking(viewModel)
            if (arguments?.containsKey(TRANSFER_MONEY_DISABLE_TABS_RESTORING_FLAG) == false) {
                restoreTabPositionDelegate.init(
                    this@TransferMoneyContainerFragment,
                    viewPager = viewPager,
                    rootDestinations = setOf(
                        pool.id.menuFragment,
                        pool.id.bankFragment,
                        pool.id.transferMenuFragment
                    )
                )
            }
            navigationFlowViewModel.switchToTransferActivity.observeBy(viewLifecycleOwner) {
                it.getNonConsumedContent()?.also {
                    viewPager.setCurrentItem(getActivityTabIndex(), false)
                }
            }
        }
    }
}
