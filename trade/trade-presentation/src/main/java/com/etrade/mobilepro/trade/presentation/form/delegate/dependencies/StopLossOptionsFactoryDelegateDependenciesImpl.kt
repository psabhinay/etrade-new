package com.etrade.mobilepro.trade.presentation.form.delegate.dependencies

import android.content.res.Resources
import com.etrade.mobilepro.trade.form.api.input.StopLossOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.trade.presentation.R

internal class StopLossOptionsFactoryDelegateDependenciesImpl(
    inputId: StopLossOptionTradeFormInputId,
    resources: Resources
) : StockOptionsFactoryDelegateDependenciesImpl(inputId, resources) {

    private val priceTypeValueId: PriceTypeValueId = inputId.priceTypeValueId
    private val stopLossDelegateDependencies = StopLossDelegateDependencies(inputId, resources)

    override val saveAndPreviewActionRequiredInputs = stopLossDelegateDependencies.saveAndPreviewActionRequiredInputs

    override val stopPriceValidPriceTypes = listOf(
        priceTypeValueId.stop,
        priceTypeValueId.trailingStopDollar
    )

    override val priceTypePriceLabelMap = mapOf(
        priceTypeValueId.limit to resources.getString(R.string.trade_hint_limit_price),
        priceTypeValueId.stop to resources.getString(R.string.trade_hint_stop_price),
        priceTypeValueId.stopLimit to resources.getString(R.string.trade_hint_stop_price),
        priceTypeValueId.trailingStopDollar to resources.getString(R.string.trade_hint_trailing_stop_dollar)
    )

    override val limitOrderTermLabelMap = stopLossDelegateDependencies.limitOrderTermLabelMap
}
