package com.etrade.mobilepro.dropdown.expandableselect

import java.util.Stack

/*
These are helper functions for ExpandalbeDropdown component
Names are pretty self-explanatory, also look at tests
 */

/**
 * Finds first item in the tree with given comparator
 * Fills Stack with path to this item
 */
fun <T> List<DropDownExpandableSelector.Selectable<T>>?.findInTree(
    pathToSelected: Stack<DropDownExpandableSelector.Selectable<T>>? = null,
    comparator: (item: DropDownExpandableSelector.Selectable<T>) -> Boolean
): DropDownExpandableSelector.Selectable<T>? {
    val onThisLevel = this?.find { comparator.invoke(it) }
    if (onThisLevel != null) {
        pathToSelected?.push(onThisLevel)
        return onThisLevel
    } else {
        this?.forEach {
            val found = it.subItems.findInTree(pathToSelected, comparator)
            if (found != null) {
                pathToSelected?.push(it)
                return found
            }
        }
        return null
    }
}

/**
 * Finds all items in tree with given comparator
 * Returns list of stacks containing paths to found items, the top item in each stack is one that satisfied comparator
 */
fun <T> List<DropDownExpandableSelector.Selectable<T>>?.findAllInTree(
    comparator: (item: DropDownExpandableSelector.Selectable<T>) -> Boolean
): List<Stack<DropDownExpandableSelector.Selectable<T>>> {

    fun findAllInSubTree(
        comparator: (item: DropDownExpandableSelector.Selectable<T>) -> Boolean,
        parentPath: Stack<DropDownExpandableSelector.Selectable<T>>,
        inputList: List<DropDownExpandableSelector.Selectable<T>>,
        resultingList: MutableList<Stack<DropDownExpandableSelector.Selectable<T>>>
    ) {
        inputList.forEach { item ->
            if (comparator.invoke(item)) {
                resultingList.add(parentPath.cloneAndPush(item))
            }
            if (item.subItems != null) {
                findAllInSubTree(
                    comparator = comparator,
                    parentPath = parentPath.cloneAndPush(item),
                    inputList = item.subItems,
                    resultingList = resultingList
                )
            }
        }
    }

    val result = mutableListOf<Stack<DropDownExpandableSelector.Selectable<T>>>()
    this?.let { findAllInSubTree(comparator, Stack(), it, result) }
    return result
}

internal fun <T> List<DropDownExpandableSelector.Selectable<T>>?.findPathToItem(
    item: DropDownExpandableSelector.Selectable<T>
): Stack<DropDownExpandableSelector.Selectable<T>> {

    fun populateStackRecursively(
        comparator: (DropDownExpandableSelector.Selectable<T>) -> Boolean,
        list: List<DropDownExpandableSelector.Selectable<T>>?,
        stack: Stack<DropDownExpandableSelector.Selectable<T>>
    ): Boolean {
        if (list?.find { comparator(it) } != null) {
            stack.push(item)
            return true
        } else {
            list?.forEach {
                if (populateStackRecursively(comparator, it.subItems, stack)) {
                    stack.push(it)
                    return true
                }
            }
        }
        return false
    }

    val stack = Stack<DropDownExpandableSelector.Selectable<T>>()
    val toggledExpanded = item.copy(isExpanded = !item.isExpanded)
    val comparator: (DropDownExpandableSelector.Selectable<T>) -> Boolean = { candidate -> candidate == item || candidate == toggledExpanded }
    populateStackRecursively(comparator, this, stack)
    return stack
}

internal fun <T> List<DropDownExpandableSelector.Selectable<T>>?.clearAllSelected(): List<DropDownExpandableSelector.Selectable<T>>? =
    this
        ?.map {
            if (it.selected) {
                it.copy(selected = false)
            } else {
                it
            }
        }
        ?.map {
            it.copy(subItems = it.subItems.clearAllSelected())
        }

internal fun <T> List<DropDownExpandableSelector.Selectable<T>>?.addNewSelected(
    comparator: (item: DropDownExpandableSelector.Selectable<T>) -> Boolean
): List<DropDownExpandableSelector.Selectable<T>>? =
    this
        ?.map {
            if (comparator(it)) {
                it.copy(selected = true)
            } else {
                it
            }
        }
        ?.map {
            it.copy(subItems = it.subItems.addNewSelected(comparator))
        }

@Suppress("UNCHECKED_CAST")
private fun <T> Stack<T>.cloneAndPush(item: T): Stack<T> = (this.clone() as Stack<T>).apply { push(item) }

internal fun <T> List<DropDownExpandableSelector.Selectable<T>>.removeSingleElementSubItems(): List<DropDownExpandableSelector.Selectable<T>> =
    this
        .map {
            when {
                it.subItems?.size in 0..1 -> it.copy(subItems = null)
                it.subItems == null -> it
                else -> it.copy(subItems = it.subItems.removeSingleElementSubItems())
            }
        }
