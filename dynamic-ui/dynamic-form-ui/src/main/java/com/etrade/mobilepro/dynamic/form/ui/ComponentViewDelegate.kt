package com.etrade.mobilepro.dynamic.form.ui

import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.api.Component
import com.etrade.mobilepro.util.android.goneUnless

class ComponentViewDelegate(private val view: View) : ComponentView<Component> {

    private val isVisibleObserver: Observer<Boolean> = Observer {
        view.goneUnless(it)
    }

    private var previousComponent: Component? = null

    override fun render(component: Component, owner: LifecycleOwner) {
        previousComponent?.isVisible?.removeObserver(isVisibleObserver)

        previousComponent = component

        component.isVisible.observe(owner, isVisibleObserver)
    }
}
