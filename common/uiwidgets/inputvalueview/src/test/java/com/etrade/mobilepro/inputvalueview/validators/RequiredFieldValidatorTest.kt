package com.etrade.mobilepro.inputvalueview.validators

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class RequiredFieldValidatorTest {

    private lateinit var subject: RequiredFieldValidator
    private val errorMessage = "Value is required"
    private val singleString = "1"
    private val longString by lazy { "1".repeat(1000) }
    private val emptyString = ""
    private val blankString = " "

    @BeforeEach
    fun setup() {
        subject = RequiredFieldValidator(errorMessage)
    }

    @Test
    fun `should be valid only if the input is not blank`() {
        var result = subject.validate(singleString)
        assertTrue(result is InputFieldManager.Value.Valid)

        result = subject.validate(longString)
        assertTrue(result is InputFieldManager.Value.Valid)

        result = subject.validate(emptyString)
        assertTrue(result is InputFieldManager.Value.Invalid)

        result = subject.validate(blankString)
        assertTrue(result is InputFieldManager.Value.Invalid)

        if (result is InputFieldManager.Value.Invalid) {
            assertTrue(result.reason == errorMessage)
        }
    }
}
