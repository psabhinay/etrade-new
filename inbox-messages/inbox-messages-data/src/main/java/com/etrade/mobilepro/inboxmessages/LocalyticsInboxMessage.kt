package com.etrade.mobilepro.inboxmessages

import com.etrade.mobilepro.inboxmessages.InboxAttributes.ARROW
import com.etrade.mobilepro.inboxmessages.InboxAttributes.ASSET_HEIGHT
import com.etrade.mobilepro.inboxmessages.InboxAttributes.BACKGROUND_COLOR
import com.etrade.mobilepro.inboxmessages.InboxAttributes.BODY_TEXT
import com.etrade.mobilepro.inboxmessages.InboxAttributes.DEEP_LINK
import com.etrade.mobilepro.inboxmessages.InboxAttributes.HEADLINE
import com.etrade.mobilepro.inboxmessages.InboxAttributes.ICON
import com.etrade.mobilepro.inboxmessages.InboxAttributes.MESSAGE_TYPE
import com.etrade.mobilepro.inboxmessages.InboxAttributes.PLACE
import com.etrade.mobilepro.inboxmessages.InboxAttributes.SUBTITLE
import com.etrade.mobilepro.inboxmessages.InboxAttributes.SYMBOL
import com.etrade.mobilepro.inboxmessages.InboxAttributes.THEME
import com.etrade.mobilepro.inboxmessages.InboxAttributes.TITLE
import com.etrade.mobilepro.inboxmessages.InboxAttributes.USER_TYPE
import com.localytics.androidx.InboxCampaign
import java.util.Date
import java.util.Locale

internal class LocalyticsInboxMessage(val campaign: InboxCampaign) : InboxMessage {

    override val id: Long = campaign.campaignId

    override val message: String = campaign.summary ?: ""

    override val rawAttributes: Map<String, String> = campaign.attributes.toCaseInsensitiveMap()

    override val type: MessageType = MessageType.convert(rawAttributes[MESSAGE_TYPE]) ?: MessageType.DASHBOARD_INFORMATION

    override val title: String = when (type) {
        MessageType.DASHBOARD_INFORMATION -> campaign.name ?: ""
        MessageType.TRADE_INFORMATION -> campaign.name ?: ""
        MessageType.DYNAMIC_NOTIFICATIONS -> campaign.name ?: ""
        else -> rawAttributes[TITLE] ?: ""
    }

    override val subtitle: String = when (type) {
        MessageType.DASHBOARD_INFORMATION -> campaign.title ?: ""
        MessageType.TRADE_INFORMATION -> campaign.title ?: ""
        MessageType.DYNAMIC_NOTIFICATIONS -> campaign.title ?: ""
        else -> rawAttributes[SUBTITLE] ?: ""
    }

    override val place: MessagePlace? = MessagePlace.convert(rawAttributes[PLACE])

    override val deepLink: String?
        get() = rawAttributes[DEEP_LINK]?.updateDeepLink()

    override val userType: List<UserType>? = UserType.convert(rawAttributes[USER_TYPE])

    override val assetHeight: Int?
        get() = rawAttributes[ASSET_HEIGHT]?.toInt()

    override val thumbnailUrl: String
        get() = campaign.thumbnailUri.toString()

    override val hasThumbnail: Boolean
        get() = campaign.hasThumbnail()

    override val isSimpleInboxMessage: Boolean
        get() = subtitle.isEmpty() && !hasThumbnail

    override val theme: CampaignAssetsTheme?
        get() = CampaignAssetsTheme.convert(rawAttributes[THEME])

    override val backgroundColor: String?
        get() = rawAttributes[BACKGROUND_COLOR]

    override val receivedDate: Date
        get() = campaign.receivedDate

    override val arrow: Boolean
        get() = rawAttributes[ARROW].shouldBeShown()

    override val icon: Boolean
        get() = rawAttributes[ICON].shouldBeShown()

    override val headLine: String?
        get() = rawAttributes[HEADLINE]

    override val bodyText: String?
        get() = rawAttributes[BODY_TEXT]

    override val symbol: String?
        get() = rawAttributes[SYMBOL]
}

private const val TRANSFER_MONEY_DEEP_LINK_IN = "etrade://show/transfermoney"
// query part should be in sync with constant in the TransferMenuFragment.kt
private const val TRANSFER_MONEY_DEEP_LINK_OUT = "etrade://menu/transfer?dest=transfermoney"

private fun String?.updateDeepLink(): String? = if (this?.lowercase(Locale.getDefault()) == TRANSFER_MONEY_DEEP_LINK_IN) {
    TRANSFER_MONEY_DEEP_LINK_OUT
} else {
    this
}
