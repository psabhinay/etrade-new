package com.etrade.mobilepro.trade.form.api.value

import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.util.MutualConverter

interface AdvancedOrderTypeValueId {
    val contingent: String
    val stopLoss: String
    val converter: MutualConverter<String, AdvancedTradeType>
}
