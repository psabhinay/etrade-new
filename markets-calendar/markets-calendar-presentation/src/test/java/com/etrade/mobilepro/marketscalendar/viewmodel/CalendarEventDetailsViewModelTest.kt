package com.etrade.mobilepro.marketscalendar.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.util.android.CalendarEvent
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.MockitoAnnotations
import org.threeten.bp.ZonedDateTime
import java.util.concurrent.TimeUnit

internal class CalendarEventDetailsViewModelTest {

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun `test add to calendar action visibility`() {
        val viewModel = CalendarEventDetailsViewModel()
        val observer: Observer<Boolean> = mock()
        val details: EventDetails = mock()

        viewModel.currentDateTime = ZonedDateTime.from(EVENTS_DATE_TIME_FORMATTER.parse("06/24/2019 09:00 AM EDT"))
        viewModel.addToCalendarActionVisibility.observeForever(observer)

        // past date
        whenever(details.date).thenReturn("06/24/2019 08:30 AM EDT")

        viewModel.item.value = details

        verify(observer).onChanged(false)

        // future date
        whenever(details.date).thenReturn("06/24/2019 09:30 AM EDT")

        viewModel.item.value = details

        verify(observer).onChanged(true)
    }

    @Test
    fun `test calendar event start and end time`() {
        val viewModel = CalendarEventDetailsViewModel()
        val observerCalendarEvent: Observer<CalendarEvent> = mock()
        val observerAddToCalendarActionVisibility: Observer<Boolean> = mock()
        val details: EventDetails = mock()

        viewModel.currentDateTime = ZonedDateTime.from(EVENTS_DATE_TIME_FORMATTER.parse("06/24/2019 09:00 AM EDT"))
        viewModel.addToCalendarActionVisibility.observeForever(observerAddToCalendarActionVisibility)
        viewModel.getCalendarEvent().observeForever(observerCalendarEvent)

        whenever(details.date).thenReturn("06/24/2019 09:30 AM EDT")
        whenever(details.title).thenReturn("Test title")

        viewModel.item.value = details

        viewModel
            .getCalendarEvent()
            .test()
            .awaitValue()
            .assertHasValue()

        assertEquals(TimeUnit.MINUTES.toMillis(30), (viewModel.getCalendarEvent().value!!.endTimeMillis - viewModel.getCalendarEvent().value!!.startTimeMillis))
    }
}
