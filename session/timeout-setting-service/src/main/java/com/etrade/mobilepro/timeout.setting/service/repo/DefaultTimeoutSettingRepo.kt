package com.etrade.mobilepro.timeout.setting.service.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.timeout.setting.service.SettingMessage
import com.etrade.mobilepro.timeout.setting.service.dto.TimeoutSettingRequestDto
import com.etrade.mobilepro.timeout.setting.service.restclient.TimeoutSettingService
import retrofit2.Retrofit

class DefaultTimeoutSettingRepo constructor(retrofit: Retrofit) : TimeoutSettingRepo {
    private val timeoutSettingService: TimeoutSettingService = retrofit.create(TimeoutSettingService::class.java)

    override suspend fun setTimeout(request: TimeoutSettingRequestDto): ETResult<SettingMessage> {
        return timeoutSettingService.runCatchingET { setUserSessionTimeout(request) }.map {
            SettingMessage(
                it.data?.text.orEmpty(),
                it.isSuccess ?: false
            )
        }
    }
}
