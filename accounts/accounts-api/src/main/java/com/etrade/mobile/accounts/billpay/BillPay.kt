package com.etrade.mobile.accounts.billpay

data class BillPay(
    val isRestricted: Boolean,
    val isEnrolled: Boolean,
    val isPrimary: Boolean,
    val isEligible: Boolean
)
