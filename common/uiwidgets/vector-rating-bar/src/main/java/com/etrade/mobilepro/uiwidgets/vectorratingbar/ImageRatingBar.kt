package com.etrade.mobilepro.uiwidgets.vectorratingbar

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import kotlinx.parcelize.Parcelize

private const val MAX_RATING = 5 // 1 to 5 stars

class ImageRatingBar : ConstraintLayout {

    @Parcelize
    private class State(
        val parentState: Parcelable?,
        val rating: Int
    ) : Parcelable

    constructor(context: Context) :
        super(context) {
            initView(null)
        }

    constructor(context: Context, attrs: AttributeSet?) :
        super(context, attrs) {
            initView(attrs)
        }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) :
        super(context, attrs, defStyleAttr) {
            initView(attrs)
        }

    var rating: Int = 0
        set(value) {
            field = value
            onRatingUpdated()
        }

    private lateinit var images: List<AppCompatImageView>

    private var ratingUnselectedDrawable: Int = 0
    private var ratingSelectedDrawable: Int = 0
    private val ratingViewIds = listOf(
        R.id.imageRating1,
        R.id.imageRating2,
        R.id.imageRating3,
        R.id.imageRating4,
        R.id.imageRating5
    )

    var onRatingBarChangeListener: OnRatingBarChangeListener = object : OnRatingBarChangeListener {
        override fun onRatingChanged(rating: Int) {
            // default rating change listener
        }
    }

    private fun getStarContentDescription(stars: Int) =
        "${resources.getQuantityString(R.plurals.rating_content_description, stars, stars)} ${context.getString(R.string.rating_hint)}"

    private fun findRatingView(ratingBarView: View, resId: Int, stars: Int) =
        ratingBarView.findViewById<AppCompatImageView>(resId).apply { contentDescription = getStarContentDescription(stars) }

    private fun initView(attrs: AttributeSet?) {
        val ratingBarView = LayoutInflater.from(context).inflate(R.layout.view_image_rating_bar, this, true)
        context.withStyledAttributes(attrs, R.styleable.ImageRatingBar) {
            ratingUnselectedDrawable = getResourceId(R.styleable.ImageRatingBar_rating_unselected, ratingUnselectedDrawable)
            ratingSelectedDrawable = getResourceId(R.styleable.ImageRatingBar_rating_selected, ratingSelectedDrawable)
        }

        val imagesList = mutableListOf<AppCompatImageView>()
        for (rating in 1..MAX_RATING) {
            imagesList.add(
                findRatingView(ratingBarView, ratingViewIds[rating - 1], rating).apply {
                    setImageResource(ratingUnselectedDrawable)
                    setOnClickListener {
                        this@ImageRatingBar.rating = rating
                    }
                }
            )
        }
        images = imagesList
    }

    private fun onRatingUpdated() {
        for (i in 1..MAX_RATING) {
            val star = getStar(i)
            if (i <= rating) {
                star.setImageResource(ratingSelectedDrawable)
            } else {
                star.setImageResource(ratingUnselectedDrawable)
            }
        }

        onRatingBarChangeListener.onRatingChanged(rating)
    }

    override fun onSaveInstanceState(): Parcelable = State(
        parentState = super.onSaveInstanceState(),
        rating = rating
    )

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is State) {
            rating = state.rating
            super.onRestoreInstanceState(state.parentState)
        } else {
            super.onRestoreInstanceState(state)
        }
    }

    private fun getStar(starRating: Int) = images[starRating - 1]

    interface OnRatingBarChangeListener {
        fun onRatingChanged(rating: Int)
    }
}
