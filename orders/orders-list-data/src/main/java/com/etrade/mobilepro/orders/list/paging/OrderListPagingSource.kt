package com.etrade.mobilepro.orders.list.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.etrade.mobilepro.orders.list.api.OrderItem
import com.etrade.mobilepro.orders.list.api.OrderParams
import com.etrade.mobilepro.orders.list.api.OrderParamsLoadSizeAndKey
import com.etrade.mobilepro.orders.list.api.OrdersRepo
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.slf4j.Logger
import org.slf4j.LoggerFactory

const val ORDERS_CACHE_EXPIRATION = 20000L

/**
 * A paging source that uses the end timeStamp as key from the response
 */

class OrderListPagingSource(
    private val cachedOrdersRepo: OrdersRepo,
    private val orderParams: OrderParams
) : PagingSource<String, OrderItem>() {
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    private fun getCacheExpiration(forceRefresh: Boolean) = if (!forceRefresh) ORDERS_CACHE_EXPIRATION else null

    override fun getRefreshKey(state: PagingState<String, OrderItem>): String? {
        return state.anchorPosition?.let {
            state.closestPageToPosition(it)?.nextKey
        }
    }

    @ExperimentalCoroutinesApi
    @Suppress("TooGenericExceptionCaught")
    override suspend fun load(params: LoadParams<String>): LoadResult<String, OrderItem> {
        try {
            val orderItemsAndKey = cachedOrdersRepo.getOrders(
                OrderParamsLoadSizeAndKey(orderParams, params.loadSize, params.key),
                getCacheExpiration(orderParams.forceRefresh)
            )

            val errorThrowable = orderItemsAndKey.errorThrowable
            if (errorThrowable != null) {
                return LoadResult.Error(errorThrowable)
            }
            if (!orderItemsAndKey.isLoading) {
                return LoadResult.Page(orderItemsAndKey.orderItems, null, orderItemsAndKey.key)
            }
        } catch (ex: Exception) {
            logger.error(ex.message)
            return LoadResult.Error(ex)
        }
        return LoadResult.Page(emptyList(), null, null)
    }
}
