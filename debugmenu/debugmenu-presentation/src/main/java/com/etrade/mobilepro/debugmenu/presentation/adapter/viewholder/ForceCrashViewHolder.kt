package com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemForcecrashBinding
import com.etrade.mobilepro.debugmenu.presentation.exception.DebugForceCrashException

internal class ForceCrashViewHolder(binding: DebugmenuItemForcecrashBinding) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.title.setOnClickListener { throw DebugForceCrashException() }
    }
}
