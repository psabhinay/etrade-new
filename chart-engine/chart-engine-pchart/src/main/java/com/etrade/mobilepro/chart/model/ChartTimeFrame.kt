package com.etrade.mobilepro.chart.model

import com.etrade.mobilepro.chartengine.TimeFrame

sealed class ChartTimeFrame(val formatString: String, val timeFrame: TimeFrame) {
    object Day : ChartTimeFrame("h a", TimeFrame.Day)
    object OneWeek : ChartTimeFrame("EEE", TimeFrame.OneWeek)
    object OneMonth : ChartTimeFrame("d", TimeFrame.OneMonth)
    object ThreeMonths : ChartTimeFrame("MMM", TimeFrame.ThreeMonths)
    object SixMonths : ChartTimeFrame("MMM", TimeFrame.SixMonths)
    object OneYear : ChartTimeFrame("MMM", TimeFrame.OneYear)
    object TwoYears : ChartTimeFrame("yyyy", TimeFrame.TwoYears)
    object ThreeYears : ChartTimeFrame("yyyy", TimeFrame.ThreeYears)
}
