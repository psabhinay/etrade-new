package com.etrade.mobilepro.transfermoney.presentation.detail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.transfermoney.api.TransferMoneyActivity
import com.etrade.mobilepro.transfermoney.presentation.R
import com.etrade.mobilepro.transfermoney.presentation.activity.TransferActivityViewModel
import com.etrade.mobilepro.transfermoney.presentation.common.isRecurring
import com.etrade.mobilepro.transfermoney.presentation.common.isScheduled
import com.etrade.mobilepro.transfermoney.presentation.databinding.TransferFragmentActivityDetailBinding
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

private const val CANCEL_TRANSFER_DLG_TAG = "cancel_transfer_dialog_tag"
private const val REAL_TIME_PAYMENT = "RTP"
private const val CANCEL_SCHEDULED_TRANSFER_REQUEST_CODE = 0
private const val CANCEL_SCHEDULED_RECURRING_TRANSFER_REQUEST_CODE = 1

@RequireLogin
class TransferActivityDetailFragment @Inject constructor(
    snackbarUtilFactory: SnackbarUtilFactory,
    viewModelFactory: ViewModelProvider.Factory
) : OverlayBaseFragment() {
    override val layoutRes: Int = R.layout.transfer_fragment_activity_detail
    private val binding by viewBinding(TransferFragmentActivityDetailBinding::bind)
    private val viewModel by viewModels<TransferActivityDetailViewModel> { viewModelFactory }
    private val parentViewModel by navGraphViewModels<TransferActivityViewModel>(R.id.transfer_money_nav_graph) { viewModelFactory }
    private val args by navArgs<TransferActivityDetailFragmentArgs>()
    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
        if (savedInstanceState == null) {
            viewModel.initialize(args.transfer)
        }
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                CANCEL_SCHEDULED_TRANSFER_REQUEST_CODE -> viewModel.cancelTransfer(onlyCurrent = true)
                CANCEL_SCHEDULED_RECURRING_TRANSFER_REQUEST_CODE -> viewModel.cancelTransfer(onlyCurrent = false)
            }
        }
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_NEUTRAL) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                CANCEL_SCHEDULED_RECURRING_TRANSFER_REQUEST_CODE -> viewModel.cancelTransfer(onlyCurrent = true)
            }
        }
    }

    private fun setupFragment() {
        binding.cancelTransfer.setOnClickListener {
            val isRecurring = viewModel.hasRecurringItem()
            val fragment = createCustomDialogFragment(isRecurring)
            fragment.show(parentFragmentManager, CANCEL_TRANSFER_DLG_TAG)
        }
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                binding.cancelTransfer.isEnabled = !it.isLoading
                when {
                    it.isLoading -> binding.loadingIndicator.show()
                    it.hasError -> {
                        binding.loadingIndicator.hide()
                        snackBarUtil.retrySnackbar { it.retry?.invoke() }
                    }
                    it.itemsDeleted != TransferActivityDetailViewModel.Amount.NONE -> {
                        parentViewModel.onTransferCancelled(onlyOneWasCancelled = it.itemsDeleted == TransferActivityDetailViewModel.Amount.ONE)
                        findNavController().navigateUp()
                    }
                    else -> {
                        binding.loadingIndicator.hide()
                        binding.transferDetailsTable.setContent(generateTableSections(it.item))
                        binding.cancelTransfer.visibility = if (it.item?.isScheduled() == true) {
                            View.VISIBLE
                        } else {
                            View.GONE
                        }
                    }
                }
            }
        )
    }

    @Suppress("LongMethod")
    private fun generateTableSections(data: TransferMoneyActivity?): List<TableSectionItemLayout> =
        data?.let { item ->
            val tableSectionList = mutableListOf(
                createSummarySection(getString(R.string.transfer_status), item.status),
                item.fromAccount.let {
                    createSummarySection(
                        label = getString(R.string.transfer_from_full),
                        value = it,
                        valueDescription = requireContext().accountNameContentDescription(it)
                    )
                },
                item.toAccount.let {
                    createSummarySection(
                        label = getString(R.string.transfer_to_full),
                        value = it,
                        valueDescription = requireContext().accountNameContentDescription(it)
                    )
                },
                createSummarySection(getString(R.string.amount), item.amount),
                createSummarySection(
                    getString(
                        if (item.isRecurring() && item.isScheduled()) {
                            R.string.transfer_next_schedule_date
                        } else {
                            R.string.schedule_date
                        }
                    ),
                    item.date
                ),
                createSummarySection(getString(R.string.transfer_repeat), item.frequency),
            )
            item.appName.let { appName ->
                if (appName == REAL_TIME_PAYMENT) {
                    tableSectionList += listOf(
                        createSummarySection(
                            label = getString(R.string.transfer_reference),
                            value = item.referenceNumber,
                            valueDescription = item.referenceNumber.characterByCharacter()
                        ),
                        createSummarySection(
                            label = getString(R.string.transfer_id),
                            value = item.id,
                            valueDescription = item.id.characterByCharacter()
                        ),
                        createSummarySection(
                            label = getString(R.string.transfer_memo),
                            value = item.memo
                        )
                    )
                } else {
                    tableSectionList += listOf(
                        createSummarySection(
                            label = getString(R.string.transfer_confirmation_number),
                            value = item.confirmationNumber,
                            valueDescription = item.confirmationNumber.characterByCharacter()
                        )
                    )
                }
            }

            tableSectionList
        } ?: emptyList()

    private fun createCustomDialogFragment(isRecurring: Boolean): CustomDialogFragment = if (isRecurring) {
        CustomDialogFragment.newInstance(
            title = getString(R.string.transfer_cancel),
            message = getString(R.string.transfer_cancel_recurring_desc),
            okTitle = getString(R.string.all),
            cancelTitle = getString(R.string.no),
            neutralTitle = getString(R.string.transfer_this_one),
            requestCode = getRequestCode(isRecurring)
        )
    } else {
        CustomDialogFragment.newInstance(
            title = getString(R.string.transfer_cancel),
            message = getString(R.string.transfer_cancel_desc),
            okTitle = getString(R.string.yes),
            cancelTitle = getString(R.string.no),
            requestCode = getRequestCode(isRecurring)
        )
    }

    private fun getRequestCode(isRecurring: Boolean): Int = if (isRecurring) {
        CANCEL_SCHEDULED_RECURRING_TRANSFER_REQUEST_CODE
    } else {
        CANCEL_SCHEDULED_TRANSFER_REQUEST_CODE
    }
}
