package com.etrade.mobilepro.eventtrackerapi

import android.content.Context

interface EventTrackerInterface {
    fun setupTracker(shouldStartTracking: Boolean, context: Context)
}
