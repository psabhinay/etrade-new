package com.etrade.mobilepro.checkdeposit.usecases

import com.etrade.mobilepro.checkdeposit.deposit.service.RemoteCheckDepositService
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.util.UseCase

interface AcceptServiceAgreementUseCase : UseCase<Unit, ETResult<Boolean>>

class AcceptServiceAgreementUseCaseImpl(
    private val checkDepositService: RemoteCheckDepositService
) : AcceptServiceAgreementUseCase {

    override suspend fun execute(parameter: Unit): ETResult<Boolean> = runCatchingET {
        val response = checkDepositService.captureserviceagreement()

        response.doIfSuccessfulOrThrow {
            it.captureStatus ?: false
        }
    }
}
