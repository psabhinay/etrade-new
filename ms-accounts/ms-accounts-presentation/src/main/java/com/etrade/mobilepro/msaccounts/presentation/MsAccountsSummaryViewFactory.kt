package com.etrade.mobilepro.msaccounts.presentation

import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import com.etrade.msaccounts.dto.MsAccountsSummaryViewDto
import javax.inject.Inject

class MsAccountsSummaryViewFactory @Inject constructor(
    private val streamingTextManager: StreamingTextManager,
    private val resources: Resources
) : DynamicUiViewFactory {
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is MsAccountsSummaryViewDto) {
            return MsAccountsSummaryViewLayout(dto, streamingTextManager, resources)
        } else {
            throw DynamicUiViewFactoryException("MsAccountsSummaryViewFactory registered to unknown type ${dto.javaClass.canonicalName}")
        }
    }
}

class MsAccountsSummaryViewLayout(
    val dto: MsAccountsSummaryViewDto,
    private val streamingTextManager: StreamingTextManager,
    private val resources: Resources
) : GenericLayout {
    override val viewModelFactory: ViewModelProvider.Factory
        get() = viewModelFactory {
            MsAccountsSummaryViewModel(dto.data, streamingTextManager, resources)
        }
    override val viewModelClass: Class<out DynamicViewModel>
        get() = MsAccountsSummaryViewModel::class.java

    override fun refresh(viewModel: DynamicViewModel) {
        if (viewModel is MsAccountsSummaryViewModel) {
            viewModel.refresh(dto.data)
        }
    }
    override val uniqueIdentifier: String
        get() = javaClass.name
}
