package com.etrade.mobilepro.feature.firebase

import android.content.res.Resources
import androidx.annotation.VisibleForTesting
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.feature.Feature
import com.etrade.mobilepro.feature.RemoteFeatureFlagProvider
import com.etrade.mobilepro.feature.qualifiers.RemoteEnabledFeatures
import com.google.android.gms.tasks.Task
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.firebase.remoteconfig.FirebaseRemoteConfigValue
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

private typealias RemoteConfigKeyMap = Map<String, FirebaseRemoteConfigValue>

@Singleton
class FirebaseFeatureFlagProvider(
    private val remoteFeatures: Set<Feature>,
    private val dispatcher: CoroutineDispatcher,
    private val remoteConfig: FirebaseRemoteConfig,
    private val resources: Resources,
    private val isDevModeEnabled: Boolean
) : RemoteFeatureFlagProvider {

    @Inject
    constructor (
        @RemoteEnabledFeatures
        remoteFeatures: Set<@JvmSuppressWildcards Feature>,
        resources: Resources
    ) : this(
        remoteFeatures,
        Dispatchers.IO,
        FirebaseRemoteConfig.getInstance(),
        resources,
        BuildConfig.DEBUG
    )

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    override val title: String
        get() = resources.getString(R.string.firebase_title)

    @VisibleForTesting
    internal val allValues = MutableStateFlow<RemoteConfigKeyMap>(emptyMap())

    suspend fun initializeRemoteConfig() = withContext(dispatcher) {
        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setMinimumFetchIntervalInSeconds(getCacheExpirationSeconds(isDevModeEnabled))
            .build()
        remoteConfig.setConfigSettingsAsync(configSettings)

        logger.debug("Activating current config")
        remoteConfig.activate().awaitTask(true)
        logger.debug("Fetching remote config")
        remoteConfig.fetch().awaitTask().onFailure {
            logger.debug("Re-Fetching remote config")
            remoteConfig.fetch().awaitTask()
        }
    }

    override fun isFeatureEnabled(feature: Feature): Flow<Boolean> =
        checkAllowed(feature, feature.defaultValue) {
            try {
                it[feature.key]?.asBoolean()
            } catch (e: IllegalArgumentException) {
                logger.error(
                    "Invalid value '${it[feature.key]?.asString()}' for key ${feature.key}",
                    e
                )
                feature.defaultValue
            } ?: feature.defaultValue
        }

    override fun hasFeature(feature: Feature): Flow<Boolean> =
        checkAllowed(feature, false) { it.containsKey(feature.key) }

    override suspend fun refreshFeatureFlags(): ETResult<Unit> = withContext(dispatcher) {
        logger.debug("Fetching and Activating Firebase Remote Config")
        remoteConfig.fetchAndActivate().awaitTask(true)
    }

    private suspend fun Task<*>.awaitTask(activate: Boolean = false): ETResult<Unit> {
        return suspendCoroutine { cont ->
            addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // After config data is successfully fetched, it must be activated before
                    // newly fetched values are returned.
                    logger.debug("Remote Config task successful")
                    if (activate) {
                        allValues.value = remoteConfig.all
                    }
                    cont.resume(ETResult.success(Unit))
                } else {
                    val msg = "Remote Config task failed"
                    logger.error(msg, task.exception)
                    cont.resume(ETResult.failure(task.exception ?: IllegalStateException(msg)))
                }
            }
        }
    }

    /**
     * If the [feature] is allowed (included in the [remoteFeatures]) then provide a flow using the
     * given mapper. Otherwise return a flow of the [disallowedValue].
     */
    private fun checkAllowed(
        feature: Feature,
        disallowedValue: Boolean,
        mapper: suspend (RemoteConfigKeyMap) -> Boolean
    ): Flow<Boolean> =
        if (!remoteFeatures.contains(feature)) {
            flowOf(disallowedValue)
        } else {
            allValues.map(mapper).distinctUntilChanged()
        }.flowOn(dispatcher)

    private fun getCacheExpirationSeconds(isDevModeEnabled: Boolean): Long = if (isDevModeEnabled) {
        CACHE_EXPIRATION_SECS_DEV
    } else {
        CACHE_EXPIRATION_SECS
    }

    companion object {
        const val CACHE_EXPIRATION_SECS = 1 * 60 * 60L
        const val CACHE_EXPIRATION_SECS_DEV = 1L
    }
}
