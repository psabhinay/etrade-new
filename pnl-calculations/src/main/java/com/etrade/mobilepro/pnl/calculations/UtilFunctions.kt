package com.etrade.mobilepro.pnl.calculations

import java.math.BigDecimal

fun Iterable<BigDecimal>.sum(): BigDecimal? = if (this.iterator().hasNext()) reduce { sum, item -> sum + item } else null
