package com.etrade.mobilepro.billpay.presentation.paymentpreview

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.billpay.api.PaymentResult
import com.etrade.mobilepro.billpay.api.PaymentStatus
import com.etrade.mobilepro.billpay.presentation.BillPayViewModel
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.billpay.presentation.common.NavigationViewModel
import com.etrade.mobilepro.billpay.presentation.common.formatAmount
import com.etrade.mobilepro.billpay.presentation.common.toPayee
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayFragmentPaymentPreviewBinding
import com.etrade.mobilepro.billpay.presentation.paymentdetail.ParcelablePayment
import com.etrade.mobilepro.billpay.presentation.viewmodel.PaymentPreviewAssistedFactory
import com.etrade.mobilepro.common.Navigation
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.common.setupToolbar
import com.etrade.mobilepro.common.toolbarView
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import org.threeten.bp.DateTimeUtils
import javax.inject.Inject

private val DUPLICATE_PAYMENT_TAG = "${PaymentPreviewFragment::class.java.canonicalName}.duplicate_payment_tag"

@RequireLogin
class PaymentPreviewFragment @Inject constructor(
    private val viewModelAssistedFactory: PaymentPreviewAssistedFactory,
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : OverlayBaseFragment() {

    private val binding by viewBinding(BillPayFragmentPaymentPreviewBinding::bind)

    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.bill_pay_nav_graph)
    private val sharedViewModel: BillPayViewModel by navGraphViewModels(R.id.bill_pay_nav_graph) { viewModelFactory }
    private val viewModel: PaymentPreviewViewModel by viewModels {
        viewModelAssistedFactory.create(
            sessionId = requireNotNull(sharedViewModel.sessionId),
            paymentPreview = paymentPreview
        )
    }
    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    private val args: PaymentPreviewFragmentArgs by navArgs()
    private val paymentPreview: PaymentPreview get() = args.payment

    override val layoutRes: Int = R.layout.bill_pay_fragment_payment_preview

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()
        setupViewModel()
    }

    override fun onResume() {
        super.onResume()
        activity?.toolbarView?.visibility = View.GONE
    }

    override fun onStop() {
        super.onStop()
        activity?.toolbarView?.visibility = View.VISIBLE
    }

    private fun setupViewModel() {
        viewModel.viewState.observe(viewLifecycleOwner, ::render)
    }

    private fun setupListeners() {
        binding.paymentAction.setOnClickListener {
            if (viewModel.isPaymentSuccessful()) {
                navigateToPaymentDetails()
            } else {
                viewModel.schedulePayment()
            }
        }
    }

    private fun navigateToPaymentDetails() = viewModel.getSuccessOrNull()
        ?.run {
            ParcelablePayment(
                id = paymentId,
                status = PaymentStatus.SCHEDULED,
                amount = paymentPreview.amount.formatAmount(),
                payeeName = paymentPreview.destinationAccount.name,
                payeeNickname = paymentPreview.destinationAccount.nickName,
                payeeAccountNumber = paymentPreview.destinationAccount.accountNumber,
                deliveryDate = deliveryDate,
                scheduleDate = paymentPreview.scheduleDate,
                memo = paymentPreview.memo,
                confirmationNumber = confirmationNumber,
                accountId = paymentPreview.originAccount.accountId,
                accountName = paymentPreview.originAccount.accountName
            )
        }
        ?.let(navigationViewModel::paymentScheduled)
        ?.also { findNavController().popBackStack() }

    private fun navigateBack(clearPaymentPreview: Boolean) {
        if (clearPaymentPreview) { navigationViewModel.clearPaymentPreview() }
        findNavController().navigateUp()
    }

    private fun showDuplicatePaymentDialog() {
        CustomDialogFragment.newInstance(
            title = getString(R.string.please_note),
            message = getString(R.string.bill_pay_duplicate_payment),
            okTitle = getString(android.R.string.ok)
        ).show(childFragmentManager, DUPLICATE_PAYMENT_TAG)
    }

    private fun render(state: PaymentPreviewViewState) {
        renderError(state.error)
        renderLoading(state.isLoading)
        renderPaymentResult(state)
        renderToolbar(state.isLoading, state.paymentSucceed)
        renderSections(viewModel.getSuccessOrNull())

        binding.paymentAction.isEnabled = !state.isLoading
    }

    private fun renderError(error: ConsumableLiveEvent<Unit>?) {
        error?.getNonConsumedContent()?.let {
            snackBarUtil.errorSnackbar(duration = Snackbar.LENGTH_LONG)
        }
    }

    private fun renderLoading(isLoading: Boolean) {
        if (isLoading) {
            binding.paymentAction.visibility = View.INVISIBLE
            binding.contentScrollView.visibility = View.INVISIBLE
            binding.progressBar.show()
        } else {
            binding.paymentAction.visibility = View.VISIBLE
            binding.contentScrollView.visibility = View.VISIBLE
            binding.progressBar.hide()
        }
    }

    private fun renderToolbar(isLoading: Boolean, paymentSucceed: Boolean) {
        if (paymentSucceed) {
            renderConfirmationToolbar()
        } else {
            renderPreviewToolbar(isLoading = isLoading)
        }
    }

    private fun renderPreviewToolbar(isLoading: Boolean) = setupToolbar(
        title = getString(R.string.preview),
        navigation = Navigation(
            icon = R.drawable.ic_arrow_back,
            onClick = View.OnClickListener { navigateBack(clearPaymentPreview = false) },
            contentDescription = getString(R.string.label_back)
        ),
        actionEnd = if (isLoading) {
            null
        } else {
            ToolbarActionEnd(
                text = R.string.cancel,
                onClick = View.OnClickListener { navigateBack(clearPaymentPreview = true) }
            )
        }
    )

    private fun renderConfirmationToolbar() = setupToolbar(
        title = getString(R.string.confirmation),
        navigation = Navigation(
            icon = R.drawable.ic_close,
            onClick = View.OnClickListener { navigateBack(clearPaymentPreview = true) },
            contentDescription = getString(R.string.close)
        )
    )

    private fun renderSections(successResult: PaymentResult.Success?) = listOfNotNull(
        successResult?.confirmationNumber?.let(::getConfirmationNumberSection),
        getScheduleDateSection(paymentPreview.scheduleDate),
        successResult?.deliveryDate
            ?.run(DateTimeUtils::toSqlDate)
            ?.run(DateFormattingUtils::formatToShortDate)
            ?.run(::getDeliveryDateSection),
        getDestinationAccountSection(paymentPreview.destinationAccount.toPayee()),
        getOriginAccountSection(paymentPreview.originAccount),
        getAvailableCashSection(paymentPreview.originAccount).takeIf { !viewModel.isPaymentSuccessful() },
        getAmountSection(paymentPreview.amount),
        getMemoSection(paymentPreview.memo)
    ).let(binding.sectionLayout::setContent)

    private fun renderPaymentResult(state: PaymentPreviewViewState) {
        if (state.paymentSucceed) {
            binding.paymentAction.setText(R.string.bill_pay_view_payment)
            binding.successView.setTitle(formatCurrency(paymentPreview.amount))
            binding.successView.visibility = View.VISIBLE
            binding.divider.visibility = View.GONE
            binding.footerMessage.text = getString(R.string.bill_pay_payment_confirmation_footer)
        } else {
            binding.paymentAction.setText(R.string.submit)
            binding.successView.visibility = View.GONE
            binding.divider.visibility = View.VISIBLE
            binding.footerMessage.text = resources.getQuantityString(
                R.plurals.bill_pay_payment_preview_footer,
                paymentPreview.destinationAccount.leadDays,
                paymentPreview.destinationAccount.leadDays
            )
        }

        state.consumablePaymentResult?.let {
            when (it) {
                is PaymentResult.Success -> {
                    binding.successView.playAnimation()
                    navigationViewModel.clearPaymentPreview()
                    navigationViewModel.refreshPayments()
                }
                PaymentResult.Error.DuplicateEntry -> showDuplicatePaymentDialog()
                PaymentResult.Error.Generic -> snackBarUtil.errorSnackbar(duration = Snackbar.LENGTH_LONG)
            }
        }
    }
}
