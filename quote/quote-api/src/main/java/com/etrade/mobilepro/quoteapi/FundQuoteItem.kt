package com.etrade.mobilepro.quoteapi

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes

sealed class FundQuoteItem : QuoteItemInterface {
    data class MutualFundDetailItem(
        val netAssetValue: String,
        val change: QuoteChangeWidgetItem,
        @ColorRes
        val changeColor: Int,
        val publicOfferPrice: String,
        val previousClose: String,
        val familyName: String,
        val timeStamp: String? = null
    ) : FundQuoteItem()

    data class MoneyMarketFundDetail(
        val sevenDayYield: String,
        val annualTotalReturn: String,
        val weightedMaturity: String,
        val familyName: String,
        val timeStamp: String? = null
    ) : FundQuoteItem()

    data class FundClassSection(
        val title: String,
        val selectedFundClass: IndexedValue<FundClassInfo>?,
        val fundClassInfoList: List<FundClassInfo>
    ) : FundQuoteItem()

    data class FundIconSection(
        val hasNoTransactionsFee: Boolean,
        val isAllStar: Boolean,
        val isAffiliatedProduct: Boolean
    ) : FundQuoteItem()

    data class MorningStarRatings(
        val asOfDate: String,
        val numFundsInOverallRating: Int,
        @DrawableRes
        val overAllDrawable: Int,
        val numFundsInMsRatingFor3y: Int,
        @DrawableRes
        val threeYearsDrawable: Int,
        val numFundsInMsRatingFor5y: Int,
        @DrawableRes
        val fiveYearsDrawable: Int,
        val numFundsInMsRatingFor10y: Int,
        @DrawableRes
        val tenYearsDrawable: Int
    ) : FundQuoteItem()

    data class MorningStarDetails(
        val morningStarCategory: String,
        val availability: String
    ) : FundQuoteItem()

    data class QuarterlyTrailingReturnsSection(
        val asOfDate: String,
        val quarterlyTrailingReturnYtd: String,
        val quarterlyTrailingReturn1y: String,
        val quarterlyTrailingReturn5y: String,
        val quarterlyTrailingReturn10y: String
    ) : FundQuoteItem()

    data class SummarySection(
        val netExpenseRatio: String,
        val grossExpenseRatio: String,
        val totalNetAssets: String,
        val inceptionDate: String,
        val thirtyDaySecYield: String,
        val oneYearSecYield: String,
        val initialInvestment: String,
        val subsequentInvestment: String,
        val salesCharge: String,
        val fundRedemptionFee: String,
        val etradeTransactionFee: String
    ) : FundQuoteItem()

    data class FundPerformanceItem(
        val symbol: String,
        val fundCategoryName: String,

        val nonAdjMonthlyTrailingReturnYtd: String,
        val nonAdjMonthlyTrailingReturn1y: String,
        val nonAdjMonthlyTrailingReturn5y: String,
        val nonAdjMonthlyTrailingReturn10y: String,

        val adjMonthlyTrailingReturnYtd: String,
        val adjMonthlyTrailingReturn1y: String,
        val adjMonthlyTrailingReturn5y: String,
        val adjMonthlyTrailingReturn10y: String,

        val nonAdjQuarterlyTrailingReturnYtd: String,
        val nonAdjQuarterlyTrailingReturn1y: String,
        val nonAdjQuarterlyTrailingReturn5y: String,
        val nonAdjQuarterlyTrailingReturn10y: String,

        val adjQuarterlyTrailingReturnYtd: String,
        val adjQuarterlyTrailingReturn1y: String,
        val adjQuarterlyTrailingReturn5y: String,
        val adjQuarterlyTrailingReturn10y: String,

        val monthlyTrailingReturnYtd: String,
        val monthlyTrailingReturn1y: String,
        val monthlyTrailingReturn5y: String,
        val monthlyTrailingReturn10y: String,

        val quarterlyTrailingReturnYtd: String,
        val quarterlyTrailingReturn1y: String,
        val quarterlyTrailingReturn5y: String,
        val quarterlyTrailingReturn10y: String,

        val monthlyLoadAdjTrailingReturnYtd: String,
        val monthlyLoadAdjTrailingReturn1y: String,
        val monthlyLoadAdjTrailingReturn5y: String,
        val monthlyLoadAdjTrailingReturn10y: String,

        val quarterlyLoadAdjTrailingReturnYtd: String,
        val quarterlyLoadAdjTrailingReturn1y: String,
        val quarterlyLoadAdjTrailingReturn5y: String,
        val quarterlyLoadAdjTrailingReturn10y: String,

        val inceptionDate: String,
        val performanceAsOfDate: String,
        val quarteryPerformanceAsOfDate: String,
        val monthlyLoadAdjTrailingReturnSinceInception: String,
        val monthlyTrailingReturnSinceInception: String,
        val quarterlyLoadAdjTrailingReturnSinceInception: String,
        val quarterlyTrailingReturnSinceInception: String

    ) : FundQuoteItem()

    data class FeesAndExpensesItem(
        val salesCharge: String,
        val fundRedemptionFee: String,
        val etradeTransactionFee: String,
        val etradeEarlyRedemptionFee: String,
        val availability: String,
        val initialInvestment: String,
        val subsequentInvestment: String,
        val initialIra: String,
        val subsequentIra: String,
        val netExpenseRatio: String,
        val grossExpenseRatio: String,
        val managementFee: String,
        val administrationFee: String,
        val twelveB1Fee: String,
        val additionalFeeDetails: Pair<Boolean?, List<FeeDetail>>
    ) : FundQuoteItem()

    data class QuoteFundsFooterItem(
        val descriptionText: String
    ) : FundQuoteItem()

    object LinksItem : FundQuoteItem()

    object MutualFundDisclosureTextItem : FundQuoteItem()
    object MoneyMarketFundDisclosureTextItem : FundQuoteItem()
}
