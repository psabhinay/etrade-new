package com.etrade.mobilepro.home.overview.viewmodel

import android.content.Context
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.common.di.IsTablet
import com.etrade.mobilepro.common.di.LocalDynamicScreen
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dynamicui.api.DynamicProperty
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.DynamicWidget
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.MessageType
import com.etrade.mobilepro.inboxmessages.addOrRemoveInboxMessagesView
import com.etrade.mobilepro.inboxmessages.dynamicui.data.extension.toInboxMessagesWidgetPlace
import com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt.rearrangeFundingPromptsToTheirAccountSummaries
import com.etrade.mobilepro.inboxmessages.inboxMessagesDynamicViewLiveData
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate
import com.etrade.mobilepro.tracking.SCREEN_TITLE_ACCOUNTS
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.SortOrder
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

const val OVERVIEW_CACHE_EXPIRATION_TIME = 15000L

class OverviewViewModel @Inject constructor(
    @Web
    private val baseUrl: String,
    @LocalDynamicScreen
    private val dynamicScreenRepo: DynamicScreenRepo,
    private val streamingStatusController: StreamingStatusController,
    @IsTablet private val isTablet: Boolean,
    private val darkModeChecker: DarkModeChecker,
    imageLoader: ImageLoader,
    userViewModel: UserViewModel,
    inboxMessagesRepository: InboxMessagesRepository,
    dispatchers: DispatcherProvider,
    context: Context
) : ViewModel(), ScreenViewModel {

    private val viewStates = MutableLiveData<ViewState>()
    private val disposable: CompositeDisposable = CompositeDisposable()
    private val inboxMessages = inboxMessagesDynamicViewLiveData(
        baseUrl = baseUrl,
        imageLoader = imageLoader,
        type = MessageType.DASHBOARD_INFORMATION,
        coroutineContext = dispatchers.IO,
        repository = inboxMessagesRepository,
        context = context
    )
    var freshPageLoad: Boolean = true
    val lifecycleObserverSet = mutableSetOf<LifecycleObserver>()
    val isUserAuthenticated = userViewModel.isAuthenticated

    override val screenName = SCREEN_TITLE_ACCOUNTS

    private val streamingUpdateListener: (StreamingStatusUpdate) -> Unit = {
        freshPageLoad = true
    }

    init {
        streamingStatusController.subscribeForStreamingStatusUpdates(streamingUpdateListener)
    }

    private fun fetchOverviewScreen(request: ScreenRequest, forceRefresh: Boolean, accountUuid: String? = null) {
        val state = viewStates.value
        if (forceRefresh.not() && state is ViewState.Loading) {
            return
        }

        disposable.clear()
        val cacheExpiration = if (forceRefresh) {
            null
        } else {
            OVERVIEW_CACHE_EXPIRATION_TIME
        }

        disposable.add(
            dynamicScreenRepo.getScreen(request, showCachedData = true, cacheExpiration = cacheExpiration).subscribeBy(
                onNext = { response ->
                    val screens = response.data ?: extractCachedList()
                    when (response) {
                        is Resource.Success -> manageScreens(request, screens, false)
                        is Resource.Loading -> manageScreens(request, screens, true)
                        is Resource.Failed -> viewStates.postValue(ViewState.Error())
                    }
                },
                onError = { throwable ->
                    viewStates.postValue(ViewState.Error(throwable.message))
                }
            )
        )
        prefetchPositions(accountUuid, cacheExpiration)
    }

    private fun manageScreens(request: ScreenRequest, views: List<GenericLayout>, isLoading: Boolean) {
        val screens = views.rearrangeFundingPromptsToTheirAccountSummaries(request.toInboxMessagesWidgetPlace())
        injectProperties(request, screens)
        val firstPaneList = mutableListOf<GenericLayout>()
        val secondPaneList = mutableListOf<GenericLayout>()
        if (isTablet && !isUserAuthenticated) {
            screens.forEach {
                when ((it as? DynamicWidget)?.type) {
                    DynamicWidget.Type.LOGIN,
                    DynamicWidget.Type.DEFAULT -> firstPaneList.add(it)
                    DynamicWidget.Type.MARKET_INDICES,
                    DynamicWidget.Type.NEWS_BRIEFING,
                    DynamicWidget.Type.NEWS,
                    DynamicWidget.Type.MOVERS,
                    DynamicWidget.Type.FUTURES -> secondPaneList.add(it)
                }
            }
        } else {
            firstPaneList.addAll(screens)
        }
        val viewState = if (isLoading) {
            ViewState.Loading(firstPaneList, secondPaneList, darkModeChecker.uiMode)
        } else {
            ViewState.Success(firstPaneList, secondPaneList, darkModeChecker.uiMode)
        }
        viewStates.postValue(viewState)
    }

    fun fetchOverview(forceRefresh: Boolean = false) {
        if (forceRefresh) {
            freshPageLoad = true
        }
        fetchOverviewScreen(ServicePath.OverView, forceRefresh)
    }

    fun getViewState(): LiveData<ViewState> {
        return viewStates.combineLatest(inboxMessages).map { (state, inboxMessagesPayload) ->
            when (state) {
                is ViewState.Success -> {
                    val newList = state.firstList.addOrRemoveInboxMessagesView(view = inboxMessagesPayload.view)
                    state.copy(firstList = newList)
                }
                else -> state
            }
        }
    }

    private fun injectProperties(request: ScreenRequest, screens: List<GenericLayout>) {
        screens.forEach {
            (it as? DynamicProperty)?.shouldHide = request is ServicePath.AccountOverView
        }
    }

    override fun onCleared() {
        disposable.clear()
        streamingStatusController.unsubscribeFromStreamingStatusUpdates(streamingUpdateListener)
        super.onCleared()
    }

    private fun extractCachedList(): List<GenericLayout> {
        return when (val viewState = viewStates.value) {
            is ViewState.Loading -> viewState.combinedList
            is ViewState.Success -> viewState.combinedList
            else -> emptyList()
        }
    }

    private fun prefetchPositions(accountUuid: String?, cacheExpiration: Long?) {
        if (accountUuid != null && streamingStatusController.isStreamingToggleEnabled) {
            disposable.add(
                dynamicScreenRepo.getScreen(
                    request = ServicePath.Portfolio(
                        accountUuid = accountUuid,
                        sortColumn = PortfolioRecordColumn.defaultColumn,
                        sortOrder = SortOrder.defaultOrder
                    ),
                    showCachedData = true,
                    cacheExpiration = cacheExpiration
                ).subscribe()
            )
        }
    }

    sealed class ViewState {
        data class Loading(
            val firstList: List<GenericLayout> = emptyList(),
            val secondList: List<GenericLayout>? = null,
            val uiMode: Int
        ) : ViewState() {
            val combinedList = firstList + secondList.orEmpty()
        }

        data class Error(val message: String? = "Something went wrong!") : ViewState()

        data class Success(
            val firstList: List<GenericLayout>,
            val secondList: List<GenericLayout>? = null,
            val uiMode: Int
        ) : ViewState() {
            val combinedList = firstList + (secondList ?: emptyList())
        }
    }
}
