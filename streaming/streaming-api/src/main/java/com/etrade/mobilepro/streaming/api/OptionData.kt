package com.etrade.mobilepro.streaming.api

interface OptionData : Level1Data {

    val openInterest: String?
    val intrinsicValue: String?
    val delta: String?
    val gamma: String?
    val theta: String?
    val vega: String?
    val rho: String?
    val impliedVolatility: String?

    companion object {

        /**
         * List of all [OptionData] fields.
         */
        @Suppress("LongMethod")
        fun fields(): Set<Level1Field> {
            return setOf(
                Level1Field.ASK,
                Level1Field.ASK_SIZE,
                Level1Field.BID,
                Level1Field.BID_SIZE,
                Level1Field.CHANGE,
                Level1Field.CHANGE_PERCENT,
                Level1Field.CLOSING_MARK,
                Level1Field.DAY_HIGH,
                Level1Field.DAY_LOW,
                Level1Field.DELTA,
                Level1Field.EPS,
                Level1Field.GAMMA,
                Level1Field.IMPLIED_VOLATILITY,
                Level1Field.INTRINSIC_VALUE,
                Level1Field.LAST_PRICE,
                Level1Field.LAST_VOLUME,
                Level1Field.MARK,
                Level1Field.OPEN_INTEREST,
                Level1Field.OPEN_PRICE,
                Level1Field.PREVIOUS_CLOSE_PRICE,
                Level1Field.PRICE_TO_EARNINGS,
                Level1Field.RHO,
                Level1Field.THETA,
                Level1Field.TICK_INDICATOR,
                Level1Field.TIMESTAMP_SINCE_EPOCH,
                Level1Field.VEGA,
                Level1Field.VOLUME,
                Level1Field.YEAR_HIGH,
                Level1Field.YEAR_HIGH_DATE,
                Level1Field.YEAR_LOW,
                Level1Field.YEAR_LOW_DATE
            )
        }
    }
}
