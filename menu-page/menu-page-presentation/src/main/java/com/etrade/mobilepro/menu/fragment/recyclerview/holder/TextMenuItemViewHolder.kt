package com.etrade.mobilepro.menu.fragment.recyclerview.holder

import android.view.View
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.menu.TextMenuItem
import com.etrade.mobilepro.menuitem.MenuItemTitleView
import com.etrade.mobilepro.menuitem.MenuItemViewWithArrow
import com.etrade.mobilepro.menuitem.MenuItemViewWithArrowAndSubtitle
import com.etrade.mobilepro.menuitem.MenuItemViewWithCount
import com.etrade.mobilepro.menupage.presentation.R
import com.etrade.mobilepro.util.android.goneUnless

class TextMenuItemViewHolder(
    itemView: View,
    private val onMenuItemClickListener: (TextMenuItem) -> Unit
) : RecyclerView.ViewHolder(itemView) {

    fun bind(item: TextMenuItem) {
        if (!item.clickable) {
            itemView.setOnClickListener(null)
        } else {
            itemView.setOnClickListener { onMenuItemClickListener(item) }
        }

        val title = item.text ?: ""
        when (itemView) {
            is MenuItemTitleView -> updateTextView(itemView, title, item.icon, item.showNewPill)
            is MenuItemViewWithArrow -> {
                itemView.menuTitleText = title
                itemView.showLabelPill = item.showNewPill
            }
            is MenuItemViewWithArrowAndSubtitle -> {
                itemView.menuTitleText = title
                itemView.menuSubTitleText = item.subtext.toString()
                itemView.showLabelPill = item.showNewPill
            }
            is MenuItemViewWithCount -> updateMenuItemViewWithCount(title, item.subtext, item.showRightIcon, item.showNewPill)
        }
        itemView.contentDescription = item.contentDescription
    }

    private fun updateTextView(
        titleView: MenuItemTitleView,
        title: CharSequence,
        @DrawableRes leftIcon: Int?,
        showLabelPill: Boolean = false
    ) {
        val textView = titleView.titleTextView
        textView.text = title
        val icon = leftIcon?.let { ContextCompat.getDrawable(itemView.context, it) }
        textView.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null)
        titleView.showLabelPill = showLabelPill
    }

    private fun updateMenuItemViewWithCount(
        text: CharSequence?,
        count: CharSequence?,
        showRightIcon: Boolean,
        showLabelPill: Boolean = false
    ) {
        val messageCountTextView: TextView = itemView.findViewById(R.id.menu_message_count)
        messageCountTextView.goneUnless(showRightIcon)
        messageCountTextView.text = count
        val menuTitleView = itemView.findViewById<MenuItemTitleView>(R.id.menuTitleView)
        menuTitleView.text = text.toString()
        menuTitleView.showLabelPill = showLabelPill
    }
}
