package com.etrade.mobilepro.trade.presentation.form.util

import com.etrade.mobilepro.dynamic.form.api.ExternalInput
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.Input
import com.etrade.mobilepro.dynamic.form.api.inputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.data.form.input.DefaultConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultStockTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultStopLossStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormInputIdMap
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.extractTradeOrderType
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId

internal fun Form.injectSymbol(symbolInputId: String, symbol: SearchResultItem.Symbol) {
    requireInput<ExternalInput<WithSymbolInfo>>(symbolInputId).setValue(symbol)
}

internal inline fun <reified T : TradeFormInputId> Form.displayValue(inputId: TradeFormInputId?, id: T.() -> String): String? {
    return if (inputId is T) {
        getInput<Input<Any>>(id(inputId))?.displayValue?.value?.toString()
    } else {
        null
    }
}

internal fun Form.getTradeFormInputId(tradeFormInputIdMap: TradeFormInputIdMap): TradeFormInputId? {
    val tradeFormType = tradeFormInputIdMap.extractTradeOrderType(inputValuesSnapshot)
    return tradeFormInputIdMap.getInputId(tradeFormType)
}

fun TradeFormInputIdMap.getInputId(securityType: SecurityType): TradeFormInputId? {
    return getInputId(TradeFormType(securityType))
}

/**
 * Collection of Symbol Input Id and Symbol Decoder for those security type that supports trade banner
 */
private val tradeHaltBannerSupportingSecuritySymbolId = mapOf(
    DefaultStockTradeFormInputId.symbol to DefaultStockTradeFormInputId.symbolCoder,
    DefaultOptionTradeFormInputId.lookupSymbol to DefaultOptionTradeFormInputId.symbolCoder,
    DefaultStopLossStockTradeFormInputId.symbol to DefaultStopLossStockTradeFormInputId.symbolCoder,
    DefaultConditionalOrderTradeFormInputId.symbol to DefaultConditionalOrderTradeFormInputId.symbolCoder,
    DefaultContingentStockTradeFormInputId.symbol to DefaultContingentStockTradeFormInputId.symbolCoder
)

/**
 * Parse symbol value from the collection of form values
 */
fun InputValuesSnapshot.parseSymbol(): String {

    val validSymbolInputId = tradeHaltBannerSupportingSecuritySymbolId.keys.firstOrNull {
        containsKey(it)
    }

    return this[validSymbolInputId]?.let {
        tradeHaltBannerSupportingSecuritySymbolId[validSymbolInputId]?.decode(it)?.symbol
    } ?: ""
}
