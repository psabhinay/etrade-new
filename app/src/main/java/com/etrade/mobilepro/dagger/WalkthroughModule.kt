package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.statemachine.EtStateMachine
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.walkthrough.api.SymbolLookupAction
import com.etrade.mobilepro.walkthrough.api.SymbolLookupState
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingAction
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingState
import com.etrade.mobilepro.walkthrough.api.TradePlacingAction
import com.etrade.mobilepro.walkthrough.api.TradePlacingState
import com.etrade.mobilepro.walkthrough.presentation.SharedWalkthroughStatesViewModel
import com.etrade.mobilepro.walkthrough.presentation.TrackingWalkthroughStatesViewModel
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.etrade.mobilepro.walkthrough.presentation.symbollookup.SymbolLookupStateMachine
import com.etrade.mobilepro.walkthrough.presentation.symboltracking.SymbolTrackingStateMachine
import com.etrade.mobilepro.walkthrough.presentation.tradeplacing.TradePlacingStateMachine
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class WalkthroughModule {

    @Binds
    abstract fun bindSymbolLookupStateMachine(impl: SymbolLookupStateMachine): EtStateMachine<SymbolLookupState, SymbolLookupAction>

    @Binds
    abstract fun bindSymbolTrackingStateMachine(impl: SymbolTrackingStateMachine): EtStateMachine<SymbolTrackingState, SymbolTrackingAction>

    @Binds
    abstract fun bindTradePlacingStateMachine(impl: TradePlacingStateMachine): EtStateMachine<TradePlacingState, TradePlacingAction>

    companion object {

        @Provides
        @Singleton
        fun provideTracking(
            walkthroughStatesViewModel: SharedWalkthroughStatesViewModel,
            tracker: Tracker
        ): WalkthroughStatesViewModel {
            return TrackingWalkthroughStatesViewModel(walkthroughStatesViewModel, tracker)
        }
    }
}
