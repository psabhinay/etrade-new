package com.etrade.mobilepro.home.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.etrade.mobilepro.home.AccountTab
import com.etrade.mobilepro.util.android.extension.instantiateFragment

internal class AccountsFragmentPagerAdapter(
    private val fragment: Fragment,
    private val accountTabList: List<AccountTab>,
    var bundle: Bundle? = null
) : FragmentStateAdapter(fragment) {

    override fun createFragment(position: Int): Fragment {
        val fragmentClass = accountTabList[position].fragmentClass
        return fragment.childFragmentManager.fragmentFactory.instantiateFragment(fragmentClass, bundle)
    }

    override fun getItemCount(): Int = accountTabList.size

    fun getIndexOfAccountTab(accountTab: AccountTab): Int =
        accountTabList.indexOf(accountTab)

    fun getIndexOf(title: String?): Int =
        title?.let {
            accountTabList.indexOfFirst { it.title.contains(title, true) }
        } ?: -1

    fun getPageTitle(position: Int): String = accountTabList[position].title
}
