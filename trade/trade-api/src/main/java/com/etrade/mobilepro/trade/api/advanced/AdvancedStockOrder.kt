package com.etrade.mobilepro.trade.api.advanced

import com.etrade.mobilepro.trade.api.stock.StockOrder

interface AdvancedStockOrder : AdvancedOrder, StockOrder
