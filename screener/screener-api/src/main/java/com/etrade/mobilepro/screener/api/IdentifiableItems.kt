package com.etrade.mobilepro.screener.api

interface Identifiable {
    val id: String
    val title: String
    val isAllItemsElement: Boolean
}

/**
 * Implements logic of multiselection in drop down lists on screener screens
 * By default those lists have first item "All items"
 * If user selects item other than default one, then default item is removed from the selection set
 * If user deselects the only left selected non-default item then default item is added to to selection set
 * if user selects default item, all other items become deselected
 * If user deselects selected default item nothing happens
 * also see unit test
 */
fun <T : Identifiable> Set<T>.toggle(item: T, defaultForEmpty: T): Set<T> {
    if (item == defaultForEmpty) {
        return setOf(defaultForEmpty)
    }
    var newSet = if (this.contains(item)) {
        this - item
    } else {
        this + item
    }
    if (!item.isAllItemsElement) {
        newSet = newSet - defaultForEmpty
    }
    return if (newSet.isEmpty()) {
        newSet + defaultForEmpty
    } else {
        newSet
    }
}

data class Industry(
    override val id: String,
    override val title: String,
    val value: String? = null,
    override val isAllItemsElement: Boolean = false
) : Identifiable {
    override fun toString(): String = title
}

data class Sector(
    override val id: String,
    override val title: String,
    val value: List<Industry>? = null,
    override val isAllItemsElement: Boolean = false
) : Identifiable {
    override fun toString(): String = title
}

data class Region(
    override val id: String,
    override val title: String,
    override val isAllItemsElement: Boolean = false
) : Identifiable {
    override fun toString(): String = title
}

data class Country(
    override val id: String,
    override val title: String,
    override val isAllItemsElement: Boolean = false
) : Identifiable {
    override fun toString(): String = title
}

data class Percent(
    override val id: String,
    override val title: String,
    override val isAllItemsElement: Boolean = false,
    val clauses: List<Clause>
) : Identifiable {
    override fun toString(): String = title
}

data class IndustryType(
    override val id: String,
    override val title: String,
    override val isAllItemsElement: Boolean = false
) : Identifiable {
    override fun toString(): String = title
}

data class FundFamily(
    override val id: String,
    override val title: String,
    override val isAllItemsElement: Boolean = false
) : Identifiable {
    override fun toString(): String = title
}

data class MutualFundGroup(
    override val id: String,
    override val title: String,
    override val isAllItemsElement: Boolean = false,
    val type: Type
) : Identifiable {
    enum class Type {
        ALL,
        ALTERNATIVE,
        BALANCED,
        COMMODITY,
        DOMESTIC_EQUITY,
        INTERNATIONAL_EQUITY,
        LIFE_CYCLE,
        MONEY_MARKET,
        MUNICIPAL_BOND,
        MUNICIPAL_STATE_BOND,
        SPECIALTY,
        TAXABLE_BOND;

        fun asRequestParam() = if (this == ALL) { "" } else { this.name }
    }
    override fun toString(): String = title
}

data class EtfFundGroup(
    override val id: String,
    override val title: String,
    override val isAllItemsElement: Boolean = false,
    val type: Type
) : Identifiable {
    enum class Type {
        ALL,
        ALTERNATIVE,
        BALANCED,
        COMMODITY,
        DOMESTIC_EQUITY,
        INTERNATIONAL_EQUITY,
        LIFE_CYCLE,
        MUNICIPAL_BOND,
        MUNICIPAL_STATE_BOND,
        SPECIALTY,
        TAXABLE_BOND;

        fun asRequestParam() = if (this == ALL) { "" } else { this.name }
    }
    override fun toString(): String = title
}

data class FundCategory(
    override val id: String,
    override val title: String,
    override val isAllItemsElement: Boolean = false
) : Identifiable {

    override fun toString(): String = title
}

data class MovingAverage(
    override val id: String,
    override val title: String,
    override val isAllItemsElement: Boolean = false
) : Identifiable {
    override fun toString(): String = title
}
