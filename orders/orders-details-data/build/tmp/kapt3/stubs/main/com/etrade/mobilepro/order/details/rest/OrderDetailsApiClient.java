package com.etrade.mobilepro.order.details.rest;

import com.etrade.eo.rest.retrofit.Xml;
import com.etrade.mobilepro.backends.neo.BaseDataDto;
import com.etrade.mobilepro.backends.neo.ServerResponseDto;
import com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequestDto;
import com.etrade.mobilepro.order.details.dto.OrderConfirmCancelResponseDto;
import com.etrade.mobilepro.order.details.dto.OrderDeleteRequestDto;
import com.etrade.mobilepro.order.details.dto.OrderDetailsRequestDto;
import com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDto;
import com.etrade.mobilepro.order.details.dto.OrderGroupCancelResponseDto;
import com.etrade.mobilepro.order.details.dto.StockPlanOrderCancelResponseDto;
import com.etrade.mobilepro.order.details.dto.StockPlanOrderDetailsResponseDto;
import com.etrade.mobilepro.util.json.JsonMoshi;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\bf\u0018\u00002\u00020\u0001J\u001e\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u0007H\'J\u001b\u0010\b\u001a\u00020\t2\b\b\u0001\u0010\n\u001a\u00020\u000bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ!\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00042\b\b\u0001\u0010\u0006\u001a\u00020\u000fH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0010J\u001b\u0010\u0011\u001a\u00020\u00122\b\b\u0001\u0010\n\u001a\u00020\u000bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ\u001e\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u0015H\'J\u001e\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u0015H\'JT\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u00032\b\b\u0001\u0010\u0019\u001a\u00020\u000b2\b\b\u0001\u0010\u001a\u001a\u00020\u000b2\b\b\u0001\u0010\u001b\u001a\u00020\u000b2\b\b\u0003\u0010\u001c\u001a\u00020\u000b2\b\b\u0003\u0010\u001d\u001a\u00020\u000b2\b\b\u0003\u0010\u001e\u001a\u00020\u000b2\b\b\u0003\u0010\u001f\u001a\u00020\u000bH\'\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006 "}, d2 = {"Lcom/etrade/mobilepro/order/details/rest/OrderDetailsApiClient;", "", "cancelConfirm", "Lio/reactivex/Single;", "Lcom/etrade/mobilepro/backends/neo/ServerResponseDto;", "Lcom/etrade/mobilepro/order/details/dto/OrderConfirmCancelResponseDto;", "requestBody", "Lcom/etrade/mobilepro/order/details/dto/OrderConfirmCancelRequestDto;", "cancelEmployeeStockPlanOrder", "Lcom/etrade/mobilepro/order/details/dto/StockPlanOrderCancelResponseDto;", "tradeId", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "deleteSavedOrder", "Lcom/etrade/mobilepro/backends/neo/BaseDataDto;", "Lcom/etrade/mobilepro/order/details/dto/OrderDeleteRequestDto;", "(Lcom/etrade/mobilepro/order/details/dto/OrderDeleteRequestDto;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getEmployeeStockPlanDetails", "Lcom/etrade/mobilepro/order/details/dto/StockPlanOrderDetailsResponseDto;", "getOrderDetails", "Lcom/etrade/mobilepro/order/details/dto/OrderDetailsResponseDto;", "Lcom/etrade/mobilepro/order/details/dto/OrderDetailsRequestDto;", "getSavedOrderDetails", "groupCancel", "Lcom/etrade/mobilepro/order/details/dto/OrderGroupCancelResponseDto;", "accountId", "orderId", "orderGroupType", "formTarget", "cancel", "cancelX", "cancelY", "orders-details-data"})
public abstract interface OrderDetailsApiClient {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "/app/vieworders/getorderdetails.json")
    @com.etrade.mobilepro.util.json.JsonMoshi()
    public abstract io.reactivex.Single<com.etrade.mobilepro.backends.neo.ServerResponseDto<com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDto>> getOrderDetails(@org.jetbrains.annotations.NotNull()
    @com.etrade.mobilepro.util.json.JsonMoshi()
    @retrofit2.http.Body()
    com.etrade.mobilepro.order.details.dto.OrderDetailsRequestDto requestBody);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "/app/preparedorder/getpreparedorderdetail.json")
    @com.etrade.mobilepro.util.json.JsonMoshi()
    public abstract io.reactivex.Single<com.etrade.mobilepro.backends.neo.ServerResponseDto<com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDto>> getSavedOrderDetails(@org.jetbrains.annotations.NotNull()
    @com.etrade.mobilepro.util.json.JsonMoshi()
    @retrofit2.http.Body()
    com.etrade.mobilepro.order.details.dto.OrderDetailsRequestDto requestBody);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.FormUrlEncoded()
    @retrofit2.http.POST(value = "/e/t/mobile/vieworderdetails")
    @com.etrade.eo.rest.retrofit.Xml()
    public abstract java.lang.Object getEmployeeStockPlanDetails(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "tradeId")
    java.lang.String tradeId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.etrade.mobilepro.order.details.dto.StockPlanOrderDetailsResponseDto> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.FormUrlEncoded()
    @retrofit2.http.POST(value = "/e/t/mobile/espcancelorder")
    @com.etrade.eo.rest.retrofit.Xml()
    public abstract java.lang.Object cancelEmployeeStockPlanOrder(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "inputTradeId")
    java.lang.String tradeId, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.etrade.mobilepro.order.details.dto.StockPlanOrderCancelResponseDto> continuation);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "/app/ordercancel/cancelconfirm.json")
    @com.etrade.mobilepro.util.json.JsonMoshi()
    public abstract io.reactivex.Single<com.etrade.mobilepro.backends.neo.ServerResponseDto<com.etrade.mobilepro.order.details.dto.OrderConfirmCancelResponseDto>> cancelConfirm(@org.jetbrains.annotations.NotNull()
    @com.etrade.mobilepro.util.json.JsonMoshi()
    @retrofit2.http.Body()
    com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequestDto requestBody);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "/e/t/mobile/groupcancelentry.json")
    @retrofit2.http.FormUrlEncoded()
    @com.etrade.mobilepro.util.json.JsonMoshi()
    public abstract io.reactivex.Single<com.etrade.mobilepro.order.details.dto.OrderGroupCancelResponseDto> groupCancel(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "accountid")
    java.lang.String accountId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "ordernum")
    java.lang.String orderId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "ordergrouptype")
    java.lang.String orderGroupType, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "_formtarget")
    java.lang.String formTarget, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "cancel")
    java.lang.String cancel, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "cancel.x")
    java.lang.String cancelX, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "cancel.y")
    java.lang.String cancelY);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "/app/preparedorder/deletesavedorder.json")
    @com.etrade.mobilepro.util.json.JsonMoshi()
    public abstract java.lang.Object deleteSavedOrder(@org.jetbrains.annotations.NotNull()
    @com.etrade.mobilepro.util.json.JsonMoshi()
    @retrofit2.http.Body()
    com.etrade.mobilepro.order.details.dto.OrderDeleteRequestDto requestBody, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.etrade.mobilepro.backends.neo.ServerResponseDto<? extends com.etrade.mobilepro.backends.neo.BaseDataDto>> continuation);
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 3)
    public final class DefaultImpls {
    }
}