package com.etrade.mobilepro.billpay.presentation.paymentlist

import com.etrade.mobilepro.billpay.api.Payment
import com.etrade.mobilepro.billpay.api.PaymentStatus
import org.threeten.bp.LocalDate
import java.math.BigDecimal

class PaymentListItem(
    val paymentId: String,
    val status: PaymentStatus,
    val amount: BigDecimal?,
    val accountName: String?,
    val accountNumber: String?,
    val payeeName: String?,
    val payeeNickname: String?,
    private val deliveryDate: LocalDate?,
    private val scheduleDate: LocalDate?
) {
    fun getPaymentDate() = when (status) {
        PaymentStatus.PAID -> deliveryDate
        else -> scheduleDate
    }
}

fun Payment.toPaymentListItem(accountName: String?): PaymentListItem {
    return PaymentListItem(
        paymentId = paymentId,
        status = status,
        amount = amount,
        payeeName = payeeName,
        payeeNickname = payeeNickname,
        deliveryDate = deliveryDate,
        scheduleDate = scheduleDate,
        accountNumber = accountId,
        accountName = accountName
    )
}
