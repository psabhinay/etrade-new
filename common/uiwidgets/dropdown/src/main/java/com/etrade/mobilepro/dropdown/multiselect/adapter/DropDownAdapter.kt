package com.etrade.mobilepro.dropdown.multiselect.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.dropdown.R
import com.etrade.mobilepro.dropdown.databinding.BottomSheetSelectorRowBinding
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.util.android.extension.viewBinding

private const val HALF_OPACITY = 0.5f
private const val FULL_OPACITY = 1f

internal class DropDownAdapter<T>(
    private val listener: (Int, DropDownMultiSelector.Selectable<T>) -> Unit
) : RecyclerView.Adapter<DropDownAdapter.SelectableViewHolder<T>>() {
    var ghostText: String? = null
    var actionItemSettings: DropDownMultiSelector.ActionItemSettings? = null

    private val items = mutableListOf<DropDownMultiSelector.Selectable<T>>()

    private val ghostTextPosition: Int
        get() = itemCount - 1

    private val actionItemPosition: Int
        get() = itemCount - 1

    fun submitItems(items: List<DropDownMultiSelector.Selectable<T>>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SelectableViewHolder<T> =
        SelectableViewHolder(parent.viewBinding(BottomSheetSelectorRowBinding::inflate), listener)

    override fun getItemCount(): Int = this.items.size

    override fun onBindViewHolder(holder: SelectableViewHolder<T>, position: Int) {
        holder.bindTo(items[position])
        handleGhostText(position, holder)
        handleActionItem(position, holder)
    }

    private fun handleActionItem(position: Int, holder: SelectableViewHolder<T>) {
        actionItemSettings?.let { settings ->
            if (position == actionItemPosition) {
                holder.actionItemTextView.apply {
                    text = settings.title
                    setCompoundDrawablesWithIntrinsicBounds(settings.startIcon, null, settings.endIcon, null)
                    setOnClickListener { settings.action() }
                }
                holder.actionItemGroup.visibility = View.VISIBLE
            } else {
                holder.actionItemGroup.visibility = View.GONE
            }
        }
    }

    private fun handleGhostText(position: Int, holder: SelectableViewHolder<T>) {
        ghostText?.also {
            if (position == ghostTextPosition) {
                holder.ghostTextView.text = it
                holder.ghostTextView.visibility = View.VISIBLE
            } else {
                holder.ghostTextView.visibility = View.GONE
            }
        }
    }

    class SelectableViewHolder<T>(
        private val binding: BottomSheetSelectorRowBinding,
        private val listener: (Int, DropDownMultiSelector.Selectable<T>) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        val ghostTextView: TextView = binding.selectorRowGhostText
        val actionItemGroup: View = binding.groupActionItem
        val actionItemTextView: TextView = binding.actionItem

        private var item: DropDownMultiSelector.Selectable<T>? = null

        init {
            val clickListener = View.OnClickListener {
                this.item?.let { item ->
                    val position = bindingAdapterPosition
                    if (position != RecyclerView.NO_POSITION && item.enabled) {
                        listener.invoke(position, item)
                    }
                }
            }

            binding.selectorRowText.setOnClickListener(clickListener)
            binding.selectorRowDescription.setOnClickListener(clickListener)
        }

        fun bindTo(item: DropDownMultiSelector.Selectable<T>) {
            this.item = item
            val description = item.descriptionFormatter?.invoke(item.value)

            binding.selectorRowText.text = if (item.titleFormatter != null) {
                item.titleFormatter.invoke(item.value)
            } else {
                item.title
            }

            binding.selectorRowText.contentDescription =
                if (item.contentDescriptionFormatter != null) {
                    item.contentDescriptionFormatter.invoke(item.value)
                } else {
                    item.title
                }

            if (item.titleColor != null) {
                binding.selectorRowText.setTextColor(item.titleColor)
            }

            with(binding.selectorRowDescription) {
                if (description != null) {
                    text = description
                    visibility = View.VISIBLE
                } else {
                    visibility = View.GONE
                }
            }

            handleEnabledState(item)
        }

        private fun handleEnabledState(item: DropDownMultiSelector.Selectable<T>) {
            with(binding.selectorRowText) {
                if (item.enabled) {
                    alpha = FULL_OPACITY

                    if (item.descriptionFormatter != null) {
                        handleTextDescriptionIcon(item.selected)
                    } else {
                        handleTextRowIcon(item.selected)
                    }
                    isEnabled = true
                } else {
                    setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0)
                    binding.selectorRowText.isSelected = false
                    binding.selectorRowText.alpha = HALF_OPACITY
                    binding.selectorRowText.isEnabled = false
                }
            }
        }

        private fun handleTextRowIcon(isSelected: Boolean) {
            with(binding.selectorRowText) {
                if (isSelected) {
                    setCompoundDrawablesRelativeWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_tableview_checkmark,
                        0
                    )
                    binding.selectorRowText.isSelected = true
                } else {
                    setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0)
                    binding.selectorRowText.isSelected = false
                }
            }
        }

        private fun handleTextDescriptionIcon(isSelected: Boolean) {
            with(binding.selectorRowImageCheck) {
                if (isSelected) {
                    setImageResource(R.drawable.ic_tableview_checkmark)
                } else {
                    setImageDrawable(null)
                }
            }
        }
    }
}
