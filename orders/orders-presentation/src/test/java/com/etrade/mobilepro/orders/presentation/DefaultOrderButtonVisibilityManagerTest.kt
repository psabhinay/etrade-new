package com.etrade.mobilepro.orders.presentation

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.ExecInstruction
import com.etrade.mobilepro.orders.api.MarketSession
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode
import com.etrade.mobilepro.orders.api.PriceType
import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal

internal class DefaultOrderButtonVisibilityManagerTest {

    @Test
    fun `test STOCK orderType=EQUITY status=OPEN priceType=MARKET origSysCode=ORIG_SYS_CODE_OH`() {
        val orderButtonVisibilityManager = DefaultOrderButtonVisibilityManager(
            orderType = OrderType.EQUITY,
            orderStatus = OrderStatus.OPEN,
            advancedOrderType = AdvancedOrderType.UNKNOWN,
            priceType = PriceType.MARKET,
            orderTerm = OrderTerm.GOOD_FOR_DAY,
            origSysCode = OrigSysCode.ORIG_SYS_CODE_OH,
            hasDependentFlag = false,
            dependentOrderNo = -2147483648,
            isMultiLeg = false,
            qtyReserveShow = BigDecimal.valueOf(200),
            orderSpecialAttribCd = "0",
            hiddenFlag = false,
            goodThroughDateFlag = false,
            instrumentType = InstrumentType.EQ,
            marketSession = MarketSession.MARKET_REGULAR,
            stopPrice = BigDecimal.ZERO,
            execInstruction = ExecInstruction(false, false, false, false)
        )

        assertEquals(true, orderButtonVisibilityManager.isCancellable)
        assertEquals(true, orderButtonVisibilityManager.isEditable)
        assertEquals(false, orderButtonVisibilityManager.isCompleted)
        assertEquals(false, orderButtonVisibilityManager.isPreviewable)
        assertEquals(false, orderButtonVisibilityManager.isDeletable)
        assertEquals(false, orderButtonVisibilityManager.isCancelOnly)
        assertEquals(true, orderButtonVisibilityManager.isOrderSupported)
    }

    @Test
    fun `test STOCK orderType=EQUITY status=SAVED priceType=MARKET origSysCode=ORIG_SYS_CODE_OH`() {
        val orderButtonVisibilityManager = DefaultOrderButtonVisibilityManager(
            orderType = OrderType.EQUITY,
            orderStatus = OrderStatus.SAVED,
            advancedOrderType = AdvancedOrderType.UNKNOWN,
            priceType = PriceType.MARKET,
            orderTerm = OrderTerm.GOOD_FOR_DAY,
            origSysCode = OrigSysCode.ORIG_SYS_CODE_OH,
            hasDependentFlag = false,
            dependentOrderNo = -2147483648,
            isMultiLeg = false,
            qtyReserveShow = BigDecimal.ZERO,
            orderSpecialAttribCd = "0",
            hiddenFlag = false,
            goodThroughDateFlag = false,
            instrumentType = InstrumentType.EQ,
            marketSession = MarketSession.MARKET_REGULAR,
            stopPrice = BigDecimal.ZERO,
            execInstruction = ExecInstruction(false, false, false, false)
        )

        assertEquals(false, orderButtonVisibilityManager.isCancellable)
        assertEquals(true, orderButtonVisibilityManager.isEditable)
        assertEquals(false, orderButtonVisibilityManager.isCompleted)
        assertEquals(true, orderButtonVisibilityManager.isPreviewable)
        assertEquals(true, orderButtonVisibilityManager.isDeletable)
        assertEquals(false, orderButtonVisibilityManager.isCancelOnly)
        assertEquals(true, orderButtonVisibilityManager.isOrderSupported)
    }

    @Test
    fun `test OPTION orderType=SPREADS status=OPEN priceType=NET_DEBIT`() {
        val orderButtonVisibilityManager = DefaultOrderButtonVisibilityManager(
            orderType = OrderType.SPREADS,
            orderStatus = OrderStatus.OPEN,
            advancedOrderType = AdvancedOrderType.UNKNOWN,
            priceType = PriceType.NET_DEBIT,
            orderTerm = OrderTerm.GOOD_FOR_DAY,
            origSysCode = OrigSysCode.ORIG_SYS_CODE_OH,
            hasDependentFlag = false,
            dependentOrderNo = -2147483648,
            isMultiLeg = true,
            qtyReserveShow = BigDecimal.ZERO,
            orderSpecialAttribCd = "0",
            hiddenFlag = false,
            goodThroughDateFlag = false,
            instrumentType = InstrumentType.OPTN,
            marketSession = MarketSession.MARKET_REGULAR,
            stopPrice = BigDecimal.ZERO,
            execInstruction = ExecInstruction(false, false, false, false)
        )

        assertEquals(true, orderButtonVisibilityManager.isCancellable)
        assertEquals(true, orderButtonVisibilityManager.isEditable)
        assertEquals(false, orderButtonVisibilityManager.isCompleted)
        assertEquals(false, orderButtonVisibilityManager.isPreviewable)
        assertEquals(false, orderButtonVisibilityManager.isDeletable)
        assertEquals(false, orderButtonVisibilityManager.isCancelOnly)
        assertEquals(true, orderButtonVisibilityManager.isOrderSupported)
    }

    @Test
    fun `test OPTION orderType=SPREADS status=CANCEL_REQUESTED priceType=NET_DEBIT`() {
        val orderButtonVisibilityManager = DefaultOrderButtonVisibilityManager(
            orderType = OrderType.SPREADS,
            orderStatus = OrderStatus.CANCEL_REQUESTED,
            advancedOrderType = AdvancedOrderType.UNKNOWN,
            priceType = PriceType.NET_DEBIT,
            orderTerm = OrderTerm.GOOD_FOR_DAY,
            origSysCode = OrigSysCode.ORIG_SYS_CODE_OH,
            hasDependentFlag = false,
            dependentOrderNo = -2147483648,
            isMultiLeg = true,
            qtyReserveShow = BigDecimal.ZERO,
            orderSpecialAttribCd = "0",
            hiddenFlag = false,
            goodThroughDateFlag = false,
            instrumentType = InstrumentType.OPTN,
            marketSession = MarketSession.MARKET_UNSPECIFIED,
            stopPrice = BigDecimal.ZERO,
            execInstruction = ExecInstruction(false, false, false, false)
        )

        assertEquals(true, orderButtonVisibilityManager.isCancellable)
        assertEquals(true, orderButtonVisibilityManager.isEditable)
        assertEquals(false, orderButtonVisibilityManager.isCompleted)
        assertEquals(false, orderButtonVisibilityManager.isPreviewable)
        assertEquals(false, orderButtonVisibilityManager.isDeletable)
        assertEquals(false, orderButtonVisibilityManager.isCancelOnly)
        assertEquals(true, orderButtonVisibilityManager.isOrderSupported)
    }

    @Test
    fun `test OPTION orderType=CUSTOM status=OPEN priceType=MARKET`() {
        val orderButtonVisibilityManager = DefaultOrderButtonVisibilityManager(
            orderType = OrderType.CUSTOM,
            orderStatus = OrderStatus.OPEN,
            advancedOrderType = AdvancedOrderType.UNKNOWN,
            priceType = PriceType.MARKET,
            orderTerm = OrderTerm.GOOD_UNTIL_CANCEL,
            origSysCode = OrigSysCode.ORIG_SYS_CODE_ANDR,
            hasDependentFlag = false,
            dependentOrderNo = -2147483648,
            isMultiLeg = true,
            qtyReserveShow = BigDecimal.ZERO,
            orderSpecialAttribCd = "0",
            hiddenFlag = false,
            goodThroughDateFlag = false,
            instrumentType = InstrumentType.OPTN,
            marketSession = MarketSession.MARKET_REGULAR,
            stopPrice = BigDecimal.ZERO,
            execInstruction = ExecInstruction(false, false, false, false)
        )

        assertEquals(true, orderButtonVisibilityManager.isCancellable)
        assertEquals(true, orderButtonVisibilityManager.isEditable)
        assertEquals(false, orderButtonVisibilityManager.isCompleted)
        assertEquals(false, orderButtonVisibilityManager.isPreviewable)
        assertEquals(false, orderButtonVisibilityManager.isDeletable)
        assertEquals(false, orderButtonVisibilityManager.isCancelOnly)
        assertEquals(true, orderButtonVisibilityManager.isOrderSupported)
    }

    @Test
    fun `test CONTINGENT orderType=CONTINGENT status=SAVED priceType=MARKET`() {
        val orderButtonVisibilityManager = DefaultOrderButtonVisibilityManager(
            orderType = OrderType.CONTINGENT,
            orderStatus = OrderStatus.SAVED,
            advancedOrderType = AdvancedOrderType.AO_CONTINGENT,
            priceType = PriceType.LIMIT,
            orderTerm = OrderTerm.UNKNOWN,
            origSysCode = OrigSysCode.ORIG_SYS_UNSPECIFIED,
            hasDependentFlag = false,
            dependentOrderNo = -2147483648,
            isMultiLeg = false,
            qtyReserveShow = BigDecimal.ZERO,
            orderSpecialAttribCd = "",
            hiddenFlag = false,
            goodThroughDateFlag = false,
            instrumentType = InstrumentType.EQ,
            marketSession = MarketSession.MARKET_UNSPECIFIED,
            stopPrice = BigDecimal.ZERO,
            execInstruction = ExecInstruction(false, false, false, false)
        )

        assertEquals(false, orderButtonVisibilityManager.isCancellable)
        assertEquals(false, orderButtonVisibilityManager.isEditable)
        assertEquals(false, orderButtonVisibilityManager.isCompleted)
        assertEquals(true, orderButtonVisibilityManager.isPreviewable)
        assertEquals(true, orderButtonVisibilityManager.isDeletable)
        assertEquals(false, orderButtonVisibilityManager.isCancelOnly)
        assertEquals(true, orderButtonVisibilityManager.isOrderSupported)
    }

    @Test
    fun `test CONDITIONAL orderType=CONDITIONAL_ONE_TRIGGER_OTHER status=OPEN priceType=STOP`() {
        val orderButtonVisibilityManager = DefaultOrderButtonVisibilityManager(
            orderType = OrderType.CONDITIONAL_ONE_TRIGGER_OTHER,
            orderStatus = OrderStatus.OPEN,
            advancedOrderType = AdvancedOrderType.UNKNOWN,
            priceType = PriceType.STOP,
            orderTerm = OrderTerm.UNKNOWN,
            origSysCode = OrigSysCode.ORIG_SYS_UNSPECIFIED,
            hasDependentFlag = false,
            dependentOrderNo = -2147483648,
            isMultiLeg = true,
            qtyReserveShow = BigDecimal.ZERO,
            orderSpecialAttribCd = "",
            hiddenFlag = false,
            goodThroughDateFlag = false,
            instrumentType = InstrumentType.EQ,
            marketSession = MarketSession.MARKET_UNSPECIFIED,
            stopPrice = BigDecimal.ZERO,
            execInstruction = ExecInstruction(false, false, false, false)
        )

        assertEquals(true, orderButtonVisibilityManager.isCancellable)
        assertEquals(false, orderButtonVisibilityManager.isEditable)
        assertEquals(false, orderButtonVisibilityManager.isCompleted)
        assertEquals(false, orderButtonVisibilityManager.isPreviewable)
        assertEquals(false, orderButtonVisibilityManager.isDeletable)
        assertEquals(true, orderButtonVisibilityManager.isCancelOnly)
        assertEquals(true, orderButtonVisibilityManager.isOrderSupported)
    }
}
