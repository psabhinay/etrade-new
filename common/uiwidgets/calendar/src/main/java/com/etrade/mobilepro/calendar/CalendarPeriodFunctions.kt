package com.etrade.mobilepro.calendar

import org.threeten.bp.LocalDate

interface CalendarPeriodFunctions {

    /**
     * Get first date in provided calendar range
     * @param date - current date used in calendar
     * @param position - current calendar page (value from [androidx.viewpager.widget.ViewPager.getCurrentItem])
     * @return start [LocalDate] in selected calendar page
     */
    fun getFirstDateOfRange(date: LocalDate = LocalDate.now(), position: Int = MIDDLE_OF_CALENDAR_RANGE): LocalDate

    /**
     * Get last date in provided calendar range
     * @param date - current date used in calendar
     * @param position - current calendar page (value from [androidx.viewpager.widget.ViewPager.getCurrentItem])
     * @return end [LocalDate] in selected calendar page
     */
    fun getLastDateOfRange(date: LocalDate = LocalDate.now(), position: Int = MIDDLE_OF_CALENDAR_RANGE): LocalDate

    /**
     * Returns offset based on provided bias and calendar page
     * Used to calculate calendar values
     * Depends on how far user switched to left or right in calendar the values could be -3, -2, -1 or 0, 1, 2, 3
     * For example for calendar with mode [CalendarMode.MONTH] we got:
     * 0 - current month
     * -1 - previous month
     * 1 - next month
     */
    fun getOffset(bias: Int, n: Int): Long {
        return when {
            n == bias -> 0
            n < bias -> ((n % bias) - bias).toLong()
            n > bias -> (n % bias).toLong()

            else -> 0
        }
    }
}
