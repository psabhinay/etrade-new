package com.etrade.mobilepro.alerts

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.alerts.presentation.account.AccountAlertsViewModel
import com.etrade.mobilepro.alerts.presentation.adapter.AlertsAdapter
import com.etrade.mobilepro.alerts.presentation.base.AlertTab
import com.etrade.mobilepro.alerts.presentation.base.BaseAlertsFragment
import com.etrade.mobilepro.alerts.presentation.base.BaseAlertsViewModel.ViewEffects
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.databinding.FragmentAlertsStandardBinding
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@RequireLogin
class AccountAlertsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    readStatusRepo: ReadStatusRepo
) : BaseAlertsFragment(viewModelFactory, R.layout.fragment_alerts_standard, readStatusRepo) {

    private val binding by viewBinding(FragmentAlertsStandardBinding::bind)

    override val alertTab: AlertTab = AlertTab.ACCOUNT
    private var snackBar: Snackbar? = null
    override val viewModel: AccountAlertsViewModel by navGraphViewModels(R.id.mainGraph) { viewModelFactory }
    override val snackbarUtil: SnackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { activity?.findViewById(R.id.nav_host_fragment) }
        )
    }

    override fun provideAlertsRecyclerView(): RecyclerView = binding.alertsRv

    override fun provideDeletionLiveEvent(): LiveData<String> = sharedViewModel.deletedAccountAlert

    override fun setupFragment() {
        binding.swipeRefreshAlerts.applyColorScheme()
    }

    override fun bindViewModel(adapter: AlertsAdapter) {
        binding.swipeRefreshAlerts.setOnRefreshListener {
            adapter.markDirty()
            viewModel.loadMore(clearAlreadyLoadedItems = true)
        }
        viewModel.viewState.observeBy(viewLifecycleOwner) {
            handleViewState(it)
        }
        viewModel.viewEffects.observeBy(viewLifecycleOwner) {
            handleViewEffects(it)
        }
        sharedViewModel.inEditMode.observeBy(viewLifecycleOwner) {
            updateForEditableMode(it)
        }
    }

    private fun handleViewState(it: AccountAlertsViewModel.ViewState) = when (it) {
        is AccountAlertsViewModel.ViewState.Loading -> {
            binding.swipeRefreshAlerts.isRefreshing = true
            snackBar?.dismiss()
        }
        is AccountAlertsViewModel.ViewState.Success -> {
            binding.swipeRefreshAlerts.isRefreshing = false
            sharedViewModel.inEditMode.value?.let { updateForEditableMode(it) }
            updateAlertsAdapter(it.alerts)
            binding.noAlerts.visibility = if (it.showNoAlertsWarning) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
        is AccountAlertsViewModel.ViewState.Error.LoadingError -> {
            binding.swipeRefreshAlerts.isRefreshing = false
            viewModel.loadMore(clearAlreadyLoadedItems = it.wasIntendedToClearItems)
        }
        is AccountAlertsViewModel.ViewState.Error.DeletionError -> {
            viewModel.deleteAlert(it.alert)
            updateAlertsAdapter(it.alerts)
        }
    }

    private fun handleViewEffects(effect: ViewEffects) {
        when (effect) {
            ViewEffects.AlertsWereDeleted -> {
                snackBar = snackbarUtil.snackbar(
                    getString(R.string.alerts_were_removed), Snackbar.LENGTH_SHORT
                )?.apply { show() }
            }
            ViewEffects.AlertDeleteFailed -> {
                snackBar = snackbarUtil.snackbar(
                    getString(R.string.alert_delete_failure), Snackbar.LENGTH_SHORT
                )?.apply { show() }
            }
            ViewEffects.LoadingFailed -> {
                snackBar = snackbarUtil.snackbar(
                    getString(R.string.loading_alerts_failed), Snackbar.LENGTH_SHORT
                )?.apply { show() }
            }
        }
    }
}
