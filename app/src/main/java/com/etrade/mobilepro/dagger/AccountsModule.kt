package com.etrade.mobilepro.dagger

import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobile.accounts.details.AccountsDetailsRepo
import com.etrade.mobilepro.accounts.AccountOrderRepo
import com.etrade.mobilepro.accounts.AccountOrderRepoImpl
import com.etrade.mobilepro.accounts.InMemoryDefaultAccountRepo
import com.etrade.mobilepro.accounts.data.repo.DefaultAccountsDetailsRepo
import com.etrade.mobilepro.accounts.data.rest.AccountsApiClient
import com.etrade.mobilepro.accountslist.api.AccountsListRepo
import com.etrade.mobilepro.accountslist.data.DefaultAccountsListRepo
import com.etrade.mobilepro.common.di.Api
import com.etrade.mobilepro.common.di.RemoteDynamicScreen
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.msaccounts.api.MsAccountsRepo
import com.etrade.mobilepro.msaccounts.api.MsPreferencesRepo
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsErrorViewLayout
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsSummaryViewLayout
import com.etrade.msaccounts.DefaultMsAccountsRepo
import com.etrade.msaccounts.DefaultMsPreferencesRepo
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(
    includes = [
        AccountValuesModule::class
    ]
)
interface AccountsModule {
    @Binds
    fun bindAccountsDetailedListRepo(repo: DefaultAccountsDetailsRepo): AccountsDetailsRepo

    @Binds
    fun bindAccountsListRepo(accountsListRepo: DefaultAccountsListRepo): AccountsListRepo

    @Binds
    fun bindAccountsOrderRepo(accountsOrderRepo: AccountOrderRepoImpl): AccountOrderRepo

    @Binds
    fun bindDefaultAccountRepo(repo: InMemoryDefaultAccountRepo): DefaultAccountRepo

    @Binds
    @Singleton
    fun bindMsPreferencesRepo(repo: DefaultMsPreferencesRepo): MsPreferencesRepo

    companion object {

        @Provides
        @Singleton
        fun provideApiClient(@Api retrofit: Retrofit): AccountsApiClient = retrofit.create(AccountsApiClient::class.java)

        @Provides
        fun providePreferencesRepo(
            @RemoteDynamicScreen
            dynamicScreenRepo: DynamicScreenRepo,
            msPreferencesRepo: MsPreferencesRepo
        ): MsAccountsRepo = DefaultMsAccountsRepo(
            dynamicScreenRepo,
            msPreferencesRepo,
            { MsAccountsErrorViewLayout() }
        ) { screen ->
            // It's a temporary solution, we expect to get `isEnrolledForDualAcctVisibility` flag
            // as part of `user_preferences` object to be able to check if user has passed enrollment.
            screen.any { it is MsAccountsSummaryViewLayout }
        }
    }
}
