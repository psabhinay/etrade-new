package com.etrade.mobilepro.marketscalendar.viewmodel

import com.etrade.mobilepro.marketscalendar.api.model.EventsEconomic
import com.etrade.mobilepro.marketscalendar.api.model.EventsRequest
import com.etrade.mobilepro.marketscalendar.api.model.EventsResponse
import com.etrade.mobilepro.marketscalendar.api.repo.MarketsCalendarRepo
import com.etrade.mobilepro.marketscalendar.data.dto.ECONOMIC_EVENT
import io.reactivex.Single
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.text.DecimalFormat
import java.util.regex.Pattern
import javax.inject.Inject

class EconomicEventsMarketsCalendarViewModel @Inject constructor(
    marketsCalendarRepo: MarketsCalendarRepo,
    sharedCalendarState: SharedCalendarState
) : MarketsCalendarResultViewModel<EventsEconomic>(marketsCalendarRepo, sharedCalendarState) {

    private val pattern: Pattern = Pattern.compile("-?\\d+.?\\d+|-?\\d+")

    override val eventType: String = ECONOMIC_EVENT

    override val eventFilterEnable: String = "1"

    override val marketsEventsRequest: MarketsCalendarRepo.(EventsRequest) -> Single<EventsResponse<EventsEconomic>> = MarketsCalendarRepo::getEventsEconomic

    override fun mapEventItem(item: EventsEconomic): EventDetails {
        return EventDetails(
            item.ecEcondetName,
            getEventDate(item, EVENTS_DATE_TIME_FORMATTER),
            item.ecEcondetConcensus,
            item.ecEcondetActual,
            getEventSurprise(item, bigDecimalValueTemplate),
            eventType,
            description = item.ecEconDesc ?: item.ecEconDefinition ?: item.ecEconName
        )
    }

    private fun getEventSurprise(item: EventsEconomic, doubleValueTemplate: DecimalFormat): String? {
        if (selectedDate.isAfter(currentDate)) {
            return null
        }

        return item.ecEcondetActual?.let { actualStr ->
            item.ecEcondetConcensus?.let { estimateStr ->
                val actual: String = pattern.matcher(actualStr).run {
                    find()
                    group()
                }
                val estimate: String = pattern.matcher(estimateStr).run {
                    find()
                    group()
                }

                val surprise: String = doubleValueTemplate.format(actual.toBigDecimal() - estimate.toBigDecimal())

                val surpriseExtension: String = actualStr.substringAfterLast(actual)

                "$surprise$surpriseExtension"
            }
        }
    }

    private fun getEventDate(item: EventsEconomic, formatter: DateTimeFormatter): String? = item.ecSeStartdate?.let {
        val time: LocalDateTime = LocalDateTime.parse(it, EVENTS_FULL_DATE_TIME_FORMATTER)

        time.atZone(nyTimeZone).format(formatter)
    }
}
