package com.etrade.mobilepro.quotes

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.FloatingWindow
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import com.etrade.mobilepro.R
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.quote.screen.presentation.BottomSheetBackPressDelegate
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.help.TradeHelpFragmentArgs
import com.etrade.mobilepro.trade.help.getTitleId
import com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer.BottomSheetNavContainer
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingState
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import javax.inject.Inject

private const val DESCRIPTION = "description"
private const val QUOTE = "quoteTicker"
private const val TITLE = "title"
private const val IS_FUND_REPORT = "isFundReport"
private const val KEY_ORDER_NUMBER = "orderNumber"

const val QUOTE_BOTTOM_SHEET_TAG = "QuoteBottomSheetFragment"

class QuoteBottomSheetFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val walkthroughStatesViewModel: WalkthroughStatesViewModel,
    connectionNotifier: ConnectionNotifier,
    userSessionHandler: UserSessionHandler
) : BottomSheetNavContainer(connectionNotifier, userSessionHandler) {

    private val viewModel: QuoteBottomSheetFragmentViewModel by activityViewModels { viewModelFactory }

    override val bottomSheetMargin = 0

    override fun createNavHost() = NavHostFragment.create(
        R.navigation.quote_navigation_graph,
        arguments
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.quoteDescription.observe(
            viewLifecycleOwner,
            Observer {
                titleText = it.first
                titleTextContentDescription = it.first.characterByCharacter()
                subTitleText = it.second
            }
        )
        walkthroughStatesViewModel.state.observeBy(viewLifecycleOwner) {
            when (it) {
                SymbolTrackingState.DisplayWatchlistPage -> dismiss()
            }
        }
    }

    override fun createDestinationChangeListener() = NavController.OnDestinationChangedListener { _, destination, args ->
        if ((destination is FloatingWindow || destination.id == R.id.quotePage) &&
            destination.id != R.id.optionsChainDisclosureDialog
        ) {
            return@OnDestinationChangedListener
        }
        onDestinationChanged(destination, args)
    }

    @Suppress("LongMethod") // just one big `when`
    private fun onDestinationChanged(destination: NavDestination, args: Bundle?) {
        when (destination.id) {
            R.id.companyNewsFragment -> {
                val symbol = args?.get(QUOTE) as? SearchResultItem.Symbol
                val title = symbol?.underlyerSymbol ?: ""
                val subtitle = args?.get(DESCRIPTION) as? String ?: ""
                viewModel.updateQuoteDesc(title, subtitle)
            }
            R.id.reportFragment -> {
                val title = getTitleForReportFragment(args)
                viewModel.updateQuoteDesc(title, "")
            }
            R.id.pdfFragment -> {
                val title = args?.get(TITLE) as? String ?: ""
                viewModel.updateQuoteDesc(title, "")
            }
            R.id.conditionalOrderFormFragment -> {
                val orderNumber = args?.getInt(KEY_ORDER_NUMBER)
                val title = getString(R.string.trade_title_conditional_order, orderNumber)
                viewModel.updateQuoteDesc(title, "")
            }
            R.id.tradeHelpFragment, R.id.tradeHelpFragmentInQuoteGraph -> {
                val title = getTitleForHelpFragment(args)
                viewModel.updateQuoteDesc(title, "")
            }
            else -> {
                updateTitleFromDestination(destination)
            }
        }
    }

    private fun updateTitleFromDestination(destination: NavDestination) {
        viewModel.updateQuoteDesc(destination.label?.toString().orEmpty(), "")
    }

    private fun getTitleForReportFragment(args: Bundle?) =
        if (args?.getBoolean(IS_FUND_REPORT) == true) {
            getString(R.string.fund_reports)
        } else {
            getString(R.string.quote_analyst_research_reports)
        }

    private fun getTitleForHelpFragment(args: Bundle?): String {
        return args?.let { getString(TradeHelpFragmentArgs.fromBundle(args).tradeOptionInfoType.getTitleId()) } ?: ""
    }

    override fun onTitleChange(newTitle: String) {
        viewModel.updateQuoteDesc(newTitle, "")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        // Setting the description to nothing in order to prevent old header text from flashing when opening a new quote.
        viewModel.updateQuoteDesc("", "")
    }

    override fun onBackPressed(isLastPageInStack: Boolean) {
        val backPressDelegate = hostNavFragment?.childFragmentManager?.fragments?.firstOrNull {
            it is BottomSheetBackPressDelegate
        } as? BottomSheetBackPressDelegate
        backPressDelegate?.onBackPressed {
            super.onBackPressed(isLastPageInStack)
        } ?: super.onBackPressed(isLastPageInStack)
    }
}
