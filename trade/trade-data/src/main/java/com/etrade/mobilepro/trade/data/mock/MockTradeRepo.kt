package com.etrade.mobilepro.trade.data.mock

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.OrderLeg
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.TradeRepo
import com.etrade.mobilepro.trade.api.stock.ConfirmedStockOrder
import com.etrade.mobilepro.trade.api.stock.StockOrderPlaceResponse
import com.etrade.mobilepro.trade.api.stock.StockOrderPreviewResponse
import java.math.BigDecimal
import javax.inject.Inject

class MockTradeRepo @Inject constructor() : TradeRepo {

    private val order = TradeOrder(
        orderId = "orderId",
        accountId = "accountId",
        response = MockOrderResponse()
    )

    override suspend fun previewOrder(
        map: InputValuesSnapshot,
        marketDestination: MarketDestination,
        changeOrder: Boolean
    ): ETResult<TradeOrder> = ETResult.success(order)

    override suspend fun placeOrder(orderId: String): ETResult<TradeOrder> = ETResult.success(order)

    override suspend fun loadOrder(orderId: String): ETResult<TradeOrder> = ETResult.success(order)

    override suspend fun clearOrder(orderId: String): ETResult<Unit> = ETResult.success(Unit)

    private class MockOrderResponse : StockOrderPreviewResponse, StockOrderPlaceResponse {
        override val orders: List<ConfirmedStockOrder> = listOf(MockStockOrder())
        override val warnings: List<ServerMessage> = listOf(
            ServerMessage.warning("An example of a warning.", 0)
        )
        override val isOvernightTrading: Boolean = false
    }

    private class MockStockOrder : ConfirmedStockOrder {
        override val account: String = "account"
        override val legs: List<OrderLeg> = listOf(
            object : OrderLeg {
                override val action: TransactionType = TransactionType.BUY_OPEN
                override val quantity: BigDecimal? = BigDecimal.TEN
                override val symbol: String = "AAPL"
            }
        )
        override val borrowRate: String? = null
        override val otcTier: String? = null
        override val estDailyBorrowInterest: String? = null
        override val priceType: PriceType = PriceType.MARKET
        override val term: OrderTerm = OrderTerm.GOOD_FOR_DAY
        override val allOrNone: Boolean = false
        override val limitPrice: BigDecimal? = null
        override val stopPrice: BigDecimal? = null
        override val yourPrice: String? = null
        override val fee: BigDecimal? = null
        override val cost: BigDecimal? = null
        override val confirmationOrderId: String? = null
        override val placedDate: String? = null
    }
}
