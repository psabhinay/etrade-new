package com.etrade.mobilepro.checkdeposit.presentation.contributiondetails

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.checkdeposit.presentation.R
import com.etrade.mobilepro.checkdeposit.presentation.databinding.FragmentIraContributionAmountsBinding
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.iraaccount.api.IraContributionDetails
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.HeaderSectionItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import org.threeten.bp.format.DateTimeFormatter

@RequireLogin
class IraContributionAmountsFragment(
    private val viewModelProvider: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val deadlineDateFormatter: DateTimeFormatter
) : OverlayBaseFragment() {

    private val binding by viewBinding(FragmentIraContributionAmountsBinding::bind)

    private val viewModel by navGraphViewModels<IraContributionAmountsViewModel>(R.id.check_deposit_graph) { viewModelProvider }
    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    override val layoutRes: Int = R.layout.fragment_ira_contribution_amounts

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeIraDetails()
    }

    private fun observeIraDetails() {
        viewModel.iraContributionDetails.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    IraContributionAmountsViewModel.ViewState.Loading -> {
                        binding.iraContributionLoading.show()
                    }
                    is IraContributionAmountsViewModel.ViewState.Loaded -> {
                        binding.iraContributionLoading.hide()
                        binding.iraContributionTable.setContent(
                            generateTableSections(it.contributionDetails)
                        )
                    }
                    is IraContributionAmountsViewModel.ViewState.Failed -> {
                        binding.iraContributionLoading.hide()
                        snackbarUtil.retrySnackbar { it.retry() }
                    }
                }
            }
        )
    }

    private fun generateTableSections(contributionDetails: IraContributionDetails): List<TableSectionItemLayout> {
        val currentYear = generateContributionYear(contributionDetails.currentYearContributionDetails)
        val previousYear = contributionDetails
            .takeIf { contributionDetails.shouldShowPreviousYear }
            ?.previousYearContributionDetails
            ?.let {
                generateContributionYear(it)
            }.orEmpty()
        return currentYear + previousYear
    }

    private fun generateContributionYear(contributionDetails: IraContributionDetails.Details): List<TableSectionItemLayout> {
        val contributionDeadline = contributionDetails.contributionDeadline?.format(deadlineDateFormatter) ?: getString(R.string.empty_default)
        return listOf(
            HeaderSectionItem(getString(R.string.ira_contribution_year, contributionDetails.taxYear)),
            createSummarySection(
                label = getString(R.string.ira_ytd_contribution_text),
                value = '$' + contributionDetails.yearToDateContribution,
                labelDescription = getString(R.string.ira_ytd_contribution_description)
            ),
            createSummarySection(getString(R.string.ira_contribution_limit_text), '$' + contributionDetails.contributionLimit),
            createSummarySection(getString(R.string.ira_still_contribute_text), '$' + contributionDetails.contributionAmountLeft),
            createSummarySection(getString(R.string.ira_contribution_deadline_text), contributionDeadline)
        )
    }
}
