package com.etrade.mobilepro.watchlist.dto

import com.etrade.mobile.power.backends.neo.BaseUsData
import com.etrade.mobile.power.backends.neo.BaseUsResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AddWatchListEntryResponseDto(
    @Json(name = "data") override val data: AddWatchListEntryResponseDataDto?
) : BaseUsResponse()

@JsonClass(generateAdapter = true)
data class AddWatchListEntryResponseDataDto(
    @Json(name = "watchlistEntry")
    val watchlistEntry: WatchlistEntriesDto
) : BaseUsData()

@JsonClass(generateAdapter = true)
data class AddWatchlistEntryRequestDto(
    @Json(name = "value")
    val editWatchListEntry: EditWatchListEntryDto
)
