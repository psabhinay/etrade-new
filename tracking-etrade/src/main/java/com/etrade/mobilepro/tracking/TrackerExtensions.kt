package com.etrade.mobilepro.tracking

import androidx.viewpager2.widget.ViewPager2
import com.etrade.mobilepro.tracking.params.ScreenViewedAnalyticsParams
import com.etrade.mobilepro.ui.multitoggle.MultiToggle

private const val VALUE_YES = "YES"
private const val VALUE_NO = "NO"

internal const val UNSPECIFIED = "unspecified"
internal const val EVENT_ATTRIBUTE_SYMBOL = "Symbol"
internal const val EVENT_ATTRIBUTE_CUSTOMER_ID = "CustomerID"
internal const val EVENT_ATTRIBUTE_SECURITY_TYPE = "Security Type"

fun Tracker.screen(screenName: String) = screen(screenName, ScreenViewedAnalyticsParams(screenName))

fun ViewPager2.initTabsTracking(viewModel: TabsHostScreenViewModel, multiToggle: MultiToggle? = null) {
    val onPageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            multiToggle?.setToggledByIndex(position, true)
            viewModel.trackTab(position)
        }
    }
    multiToggle?.apply {
        setToggledByIndex(currentItem, true)
        onToggledListener = { toggle, selected ->
            if (selected) {
                val index = toggleIds.indexOf(toggle.id)
                setCurrentItem(index, true)
            }
        }
    }
    registerOnPageChangeCallback(onPageChangeCallback)
}

internal val Boolean.trackerValue: String
    get() = if (this) VALUE_YES else VALUE_NO
