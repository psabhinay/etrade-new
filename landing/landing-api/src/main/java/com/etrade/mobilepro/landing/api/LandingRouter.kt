package com.etrade.mobilepro.landing.api

interface LandingRouter {
    fun createPendingNavigation()
    fun getPendingNavigation(): LandingPendingNavigation?
}
