package com.etrade.mobilepro.etcompose.toolbar

import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.runtime.Composable

private val DEFAULT_ICON = Icons.Rounded.Add
private const val EMPTY_STRING = ""

/**
 * An all-encompassing sealed class for action items used within ETToolbar.
 */
sealed class ToolbarAction(val onClick: () -> Unit) {

    class TextAction(
        onClick: () -> Unit,
        val text: String
    ) : ToolbarAction(onClick)

    class IconAction(
        onClick: () -> Unit,
        val icon: @Composable () -> Unit = { Icon(imageVector = DEFAULT_ICON, contentDescription = EMPTY_STRING) }
    ) : ToolbarAction(onClick)
}
