package com.etrade.mobilepro.dynamic.form.ui.factory

import androidx.annotation.MainThread
import com.etrade.mobilepro.dynamic.form.api.Component
import com.etrade.mobilepro.dynamic.form.api.DynamicText
import com.etrade.mobilepro.dynamic.form.api.ExternalInput
import com.etrade.mobilepro.dynamic.form.api.Layout
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.StaticText
import com.etrade.mobilepro.dynamic.form.api.SwitchInput
import com.etrade.mobilepro.dynamic.form.api.TextInput
import com.etrade.mobilepro.dynamic.form.ui.ComponentView
import kotlin.reflect.KClass

internal class ComponentViewFactoryProvider {

    val viewFactories: Map<KClass<out Component>, ComponentViewFactory<Component, ComponentView<Component>>>
        get() = _viewFactories

    private val _viewFactories: MutableMap<KClass<out Component>, ComponentViewFactory<Component, ComponentView<Component>>> = mutableMapOf()

    init {
        addViewFactory(DynamicText::class, DynamicTextViewFactory)
        addViewFactory(ExternalInput::class, ExternalInputViewFactory)
        addViewFactory(Layout::class, LayoutViewFactory)
        addViewFactory(SelectInput::class, SelectInputViewFactory)
        addViewFactory(StaticText::class, StaticViewFactory)
        addViewFactory(SwitchInput::class, SwitchInputViewFactory)
        addViewFactory(TextInput::class, TextInputViewFactory)
    }

    @MainThread
    @Suppress("UNCHECKED_CAST")
    fun <C : Component, V : ComponentView<C>> addViewFactory(cls: KClass<C>, viewFactory: ComponentViewFactory<C, V>) {
        _viewFactories[cls] = viewFactory as ComponentViewFactory<Component, ComponentView<Component>>
    }
}
