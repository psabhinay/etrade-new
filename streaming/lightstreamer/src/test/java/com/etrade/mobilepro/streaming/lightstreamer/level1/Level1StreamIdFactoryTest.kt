package com.etrade.mobilepro.streaming.lightstreamer.level1

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.StreamIdFactory
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Level1StreamIdFactoryTest {

    private val parameters: Parameters =
        Parameters(false)
    private val factory: StreamIdFactory<Any> = Level1StreamIdFactory(parameters)

    @Test
    fun `check realtime option id`() {
        parameters.isRealtimeQuotes = true
        Assertions.assertEquals("OPTION:ETFC:REALTIME", factory.createStreamId("ETFC", InstrumentType.OPTN))
    }

    @Test
    fun `check realtime non-option id`() {
        parameters.isRealtimeQuotes = true
        Assertions.assertEquals("STOCK:ETFC:REALTIME", factory.createStreamId("ETFC", InstrumentType.INDX))
    }

    @Test
    fun `check delayed option id`() {
        parameters.isRealtimeQuotes = false
        Assertions.assertEquals("OPTION:ETFC:DELAYED", factory.createStreamId("ETFC", InstrumentType.OPTN))
    }

    @Test
    fun `check delayed non-option id`() {
        parameters.isRealtimeQuotes = false
        Assertions.assertEquals("STOCK:ETFC:DELAYED", factory.createStreamId("ETFC", InstrumentType.INDX))
    }

    private class Parameters(override var isRealtimeQuotes: Boolean) : Level1StreamIdFactory.Parameters
}
