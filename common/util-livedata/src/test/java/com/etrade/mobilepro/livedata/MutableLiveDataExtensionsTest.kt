package com.etrade.mobilepro.livedata

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.TestObserver
import com.jraska.livedata.test
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class MutableLiveDataExtensionsTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun `check setDistinctValue does not set the same value multiple times`() {
        val liveData = MutableLiveData("value")
        val observer = liveData.test()
        repeat(5) {
            liveData.setDistinctValue("value")
        }
        observer.assertHasValue()
            .assertHistorySize(1)
    }

    @Test
    fun `check setDistinctValue set new values`() {
        val expectedValue = "initialValue"
        val liveData = MutableLiveData(expectedValue)
        val observer = liveData.test()
        repeat(2) {
            liveData.setDistinctValue("newValue")
            liveData.setDistinctValue("oldValue")
        }
        liveData.setDistinctValue(expectedValue)
        observer.assertHistorySize(6)
            .assertValue(expectedValue)
    }

    @Test
    fun `check update`() {
        updatedLiveData("oldValue", "newValue")
            .assertHistorySize(2)
            .assertValue("newValue")
    }

    @Test
    fun `check update when values are equal`() {
        updatedLiveData("value", "value")
            .assertHistorySize(1)
            .assertValue("value")
    }

    @Test
    fun `check update when there's no current value`() {
        updatedLiveData(null, "value")
            .assertHistorySize(1)
            .assertValue(null as String?)
    }

    @Test
    fun `check update to null value`() {
        updatedLiveData("value", null)
            .assertHistorySize(2)
            .assertValue(null as String?)
    }

    @Test
    fun `check when old and new values are null`() {
        updatedLiveData(null, null)
            .assertHistorySize(1)
            .assertValue(null as String?)
    }

    private fun updatedLiveData(initialValue: String?, newValue: String?): TestObserver<String?> {
        val liveData = MutableLiveData(initialValue)
        val test = liveData.test()
        liveData.updateValue {
            assertEquals(initialValue, it)
            newValue
        }
        return test
    }
}
