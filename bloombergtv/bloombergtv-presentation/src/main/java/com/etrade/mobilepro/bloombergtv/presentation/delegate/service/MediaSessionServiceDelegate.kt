package com.etrade.mobilepro.bloombergtv.presentation.delegate.service

import android.content.Context
import android.graphics.BitmapFactory
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.MediaMetadataCompat.METADATA_KEY_ALBUM_ART
import android.support.v4.media.MediaMetadataCompat.METADATA_KEY_DISPLAY_TITLE
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import com.etrade.mobilepro.bloombergtv.presentation.R
import com.etrade.mobilepro.common.result.runCatchingET

private const val AUDIO_PLAYER_TAG = "AudioPlayer"

internal class MediaSessionServiceDelegate(
    private val play: () -> Unit,
    private val pause: () -> Unit,
    private val stop: () -> Unit,
) {
    val token get() = mediaSession?.sessionToken

    private lateinit var context: Context
    private var mediaSession: MediaSessionCompat? = null

    fun init(context: Context) = runCatchingET {

        this.context = context

        mediaSession = MediaSessionCompat(context, AUDIO_PLAYER_TAG).apply {

            isActive = true
            setState(false)

            val iconImage = BitmapFactory.decodeResource(context.resources, R.drawable.ic_surround_sound_white_24px)
            setMetadata(
                MediaMetadataCompat.Builder()
                    .putBitmap(METADATA_KEY_ALBUM_ART, iconImage)
                    .putString(METADATA_KEY_DISPLAY_TITLE, context.getString(R.string.audio_mode_title))
                    .build()
            )

            setCallback(object : MediaSessionCompat.Callback() {
                override fun onPlay() = play()
                override fun onPause() = pause()
                override fun onStop() = stop()
            })
        }
    }.getOrNull() ?: stop()

    fun setState(isPlaying: Boolean) {
        mediaSession?.setPlaybackState(buildState(isPlaying))
    }

    fun release() {
        mediaSession?.isActive = false
        mediaSession?.release()
    }

    private fun buildState(isPlaying: Boolean) =
        with(PlaybackStateCompat.Builder()) {
            val actionPlayOrPause =
                if (isPlaying) {
                    PlaybackStateCompat.ACTION_PAUSE
                } else {
                    PlaybackStateCompat.ACTION_PLAY
                }
            setActions(
                actionPlayOrPause
                    or PlaybackStateCompat.ACTION_PLAY_PAUSE
                    or PlaybackStateCompat.ACTION_STOP
            )
            build()
        }
}
