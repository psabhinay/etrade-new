package com.etrade.mobilepro.menu.viewmodel

import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.builder.TextMenuItemBuilder

abstract class MenuPageViewModel(val resources: Resources) : ViewModel() {
    abstract val menuPageItems: LiveData<List<MenuItem>>

    fun TextMenuItemBuilder.text(@StringRes id: Int) {
        text = resources.getString(id)
    }

    fun TextMenuItemBuilder.subtext(@StringRes id: Int) {
        subtext = resources.getString(id)
    }
}
