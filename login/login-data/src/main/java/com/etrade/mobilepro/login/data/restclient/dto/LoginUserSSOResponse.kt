package com.etrade.mobilepro.login.data.restclient.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false, name = "LoginUserSSOResponse")
internal data class LoginUserSSOResponse constructor(
    @field:Element(name = "UserInfo", required = false)
    var userInfoDto: UserInfoDto? = null,

    @field:Element(name = "Result")
    var result: SessionResult? = null
)
