package com.etrade.mobilepro.billpay.api

data class SearchPayeeResultItem(
    val payeeId: String?,
    val name: String,
    val category: String?,
    val merchantId: Int,
    val merchantZipRequired: Boolean,
    val exactMatch: Boolean
)
