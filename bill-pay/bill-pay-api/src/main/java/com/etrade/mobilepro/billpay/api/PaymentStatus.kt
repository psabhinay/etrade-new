package com.etrade.mobilepro.billpay.api

enum class PaymentStatus(val status: String) {
    ALL("All"),
    SCHEDULED("Scheduled"),
    PAID("Paid"),
    FAILED("Failed"),
    CANCELLED("Cancelled"),
    INPROCESS("InProcess"),
    UNKNOWN("");

    companion object {
        fun fromStatusString(status: String?): PaymentStatus {
            return values().firstOrNull { status == it.status } ?: UNKNOWN
        }
    }
}
