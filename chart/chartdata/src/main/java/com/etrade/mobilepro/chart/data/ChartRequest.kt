package com.etrade.mobilepro.chart.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ChartRequestDto(
    @Json(name = "value")
    val value: ChartRequestValueDto
)

@JsonClass(generateAdapter = true)
class ChartRequestValueDto(
    @Json(name = "accountList")
    val accounts: List<String>,
    @Json(name = "indexList")
    val indexes: List<String>,
    @Json(name = "period")
    val period: String,
    @Json(name = "optimized")
    val optimized: String = "1"
)
