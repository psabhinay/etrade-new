package com.etrade.mobilepro.positionsholdingsapi.restapi

import com.etrade.mobilepro.holdings.dto.HoldingsRequestDto
import com.etrade.mobilepro.holdings.dto.HoldingsResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface HoldingsApiClient {
    /**
     * Api to get holdings for a ticker via HTTP Post method
     * @param brokerageAccounts Ids of brokerage accounts separated by ","
     * @param stockPlanAccounts Ids of stock plan accounts separated by ","
     * @param symbol Symbol name to look holdings for
     */
    @JsonMoshi
    @POST("/app/positions/getholdingsinfo.json")
    fun getHoldings(
        @Body @JsonMoshi body: HoldingsRequestDto
    ): Single<HoldingsResponseDto>
}
