package com.etrade.mobilepro.checkdeposit.presentation.deposit

import com.etrade.mobilepro.checkdeposit.usecases.api.CHECK_BACK
import com.etrade.mobilepro.checkdeposit.usecases.api.CHECK_FRONT
import com.etrade.mobilepro.checkdeposit.usecases.api.ImageUploadResult
import com.etrade.mobilepro.checkdeposit.usecases.api.UploadCheckImage
import com.etrade.mobilepro.checkdeposit.usecases.deposit.CheckDepositResult
import com.etrade.mobilepro.common.Restorable
import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount
import com.etrade.mobilepro.util.delegate.consumable

sealed class DepositDetailsViewState {
    open val selectedAccount: FundsFlowAccount? = null
    open val depositAmount: String? = null
    open val iraDepositType: String? = null

    // we have to have a way to force-trigger the logic where there are distinctUntilChange()-s without affecting actual payload
    abstract fun invalidate(): DepositDetailsViewState

    data class InProgress(
        override val depositAmount: String? = null,
        override val selectedAccount: FundsFlowAccount? = null,
        override val iraDepositType: String? = null,
        val invalidateFlag: Boolean = false
    ) : DepositDetailsViewState() {
        override fun invalidate(): DepositDetailsViewState = this.copy(invalidateFlag = !invalidateFlag)
    }

    data class ReadyForCapture(
        override val depositAmount: String,
        override val selectedAccount: FundsFlowAccount,
        override val iraDepositType: String? = null,
        val invalidateFlag: Boolean = false
    ) : DepositDetailsViewState() {
        override fun invalidate(): DepositDetailsViewState = this.copy(invalidateFlag = !invalidateFlag)
    }

    companion object {
        @JvmStatic
        @SuppressWarnings("LongMethod") // Trying to make this smaller runs into issues with smart casts and nullability
        fun validate(depositAmount: String?, selectedAccount: FundsFlowAccount?, selectedContributionType: String?): DepositDetailsViewState {
            return if (depositAmount != null && selectedAccount != null) {
                if (selectedAccount.isIraAccount) {
                    if (selectedContributionType != null) {
                        ReadyForCapture(
                            depositAmount = depositAmount,
                            selectedAccount = selectedAccount,
                            iraDepositType = selectedContributionType
                        )
                    } else {
                        InProgress(
                            depositAmount = depositAmount,
                            selectedAccount = selectedAccount,
                            iraDepositType = selectedContributionType
                        )
                    }
                } else {
                    ReadyForCapture(
                        depositAmount = depositAmount,
                        selectedAccount = selectedAccount,
                        iraDepositType = selectedContributionType
                    )
                }
            } else {
                InProgress(
                    depositAmount = depositAmount,
                    selectedAccount = selectedAccount,
                    iraDepositType = selectedContributionType
                )
            }
        }
    }
}

sealed class ViewState : Restorable {
    open val depositDetails: DepositDetailsViewState? = null
    open val checkCapture: CheckCaptureViewState? = null
    open val validationState: ValidateDepositBusinessRulesViewState? = null
    open val depositImageState: DepositImageViewState? = null

    object Loading : ViewState()

    object AccountFetchFailed : ViewState()

    data class DetailsNeeded(
        override val depositDetails: DepositDetailsViewState.InProgress = DepositDetailsViewState.InProgress(),
        override val validationState: ValidateDepositBusinessRulesViewState? = null
    ) : ViewState()
    data class CaptureInProgress(
        override val depositDetails: DepositDetailsViewState.ReadyForCapture,
        override val checkCapture: CheckCaptureViewState.InProgress,
        override val depositImageState: DepositImageViewState? = null
    ) : ViewState()
    data class ReadyForSubmission(
        override val depositDetails: DepositDetailsViewState.ReadyForCapture,
        override val checkCapture: CheckCaptureViewState.Completed,
        override val depositImageState: DepositImageViewState? = null
    ) : ViewState()
    data class DepositSubmitted(
        override val depositImageState: DepositImageViewState.Valid
    ) : ViewState()
}

sealed class CheckCaptureViewState {

    open val checkFront: CheckCapture? = null
    open val checkBack: CheckCapture? = null

    fun updateCheckSide(@UploadCheckImage.CheckSide checkSide: Int, capture: CheckCapture): CheckCaptureViewState {
        val front = if (checkSide == CHECK_FRONT) capture else checkFront
        val back = if (checkSide == CHECK_BACK) capture else checkBack
        return if (front is CheckCapture.Success && back is CheckCapture.Success) {
            Completed(checkFront = front, checkBack = back)
        } else {
            InProgress(checkFront = front, checkBack = back)
        }
    }

    data class InProgress(
        override val checkFront: CheckCapture? = null,
        override val checkBack: CheckCapture? = null
    ) : CheckCaptureViewState()

    data class Completed(
        override val checkFront: CheckCapture.Success,
        override val checkBack: CheckCapture.Success
    ) : CheckCaptureViewState()

    sealed class CheckCapture {
        data class Success(
            val result: ImageUploadResult
        ) : CheckCapture()

        object Uploading : CheckCapture()

        class Failure(errorMessage: String? = null) : CheckCapture() {
            val consumerErrorMessage by consumable(errorMessage)
        }
    }
}

sealed class ValidateDepositBusinessRulesViewState {

    object Valid : ValidateDepositBusinessRulesViewState()

    data class Invalid(val message: String) : ValidateDepositBusinessRulesViewState() {
        val consumerMessage by consumable(message)
    }

    data class Error(val message: String) : ValidateDepositBusinessRulesViewState() {
        val consumerMessage by consumable(message)
    }
}

sealed class DepositImageViewState {

    object Loading : DepositImageViewState()

    data class Valid(val depositResults: CheckDepositResult.Valid) : DepositImageViewState()

    data class Invalid(
        val message: String,
        val frontImageFieldToFlag: Boolean,
        val rearImageFieldToFlag: Boolean,
        val amountFieldToFlag: Boolean
    ) : DepositImageViewState() {
        val consumerMessage by consumable(message)
    }

    data class Error(val message: String) : DepositImageViewState() {
        val consumerMessage by consumable(message)
    }
}
