package com.etrade.mobilepro.movers

import android.view.View
import java.math.BigDecimal

data class MoverItem(
    val ticker: String,
    val displaySymbol: String,
    val price: String,
    val gain: String,
    val gainPercentage: String,
    val decimalPercentChange: BigDecimal?,
    val priceDescription: String? = null,
    val gainDescription: String? = null,
    val shouldDisplayOptionsData: Boolean,
    val optionsExpiryDate: String? = null,
    /* $100C where $100 - strike price and Call type is C and Put type is P */
    val optionsStrikePricePlusType: String? = null,
    val isPositiveGain: Boolean = true,
    val itemClickHandler: ((view: View) -> Unit)? = null
)
