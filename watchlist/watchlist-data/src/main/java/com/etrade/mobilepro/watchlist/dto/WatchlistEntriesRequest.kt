package com.etrade.mobilepro.watchlist.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class WatchlistEntriesRequest(
    @Json(name = "value")
    val watchlistEntriesValue: WatchlistEntriesValue,
    @Json(name = "type")
    val watchlistEntriesType: WatchlistEntriesRequestType = WatchlistEntriesRequestType()
)

@JsonClass(generateAdapter = true)
data class WatchlistEntriesValue(
    @Json(name = "extendedHourQuote")
    val extendedHourQuote: String = "0",
    @Json(name = "Pagination")
    val pagination: WatchlistPagination = WatchlistPagination(),
    @Json(name = "watchlistID")
    val watchlistId: String
)

@JsonClass(generateAdapter = true)
class WatchlistEntriesRequestType(
    @Json(name = "charset")
    val charset: String = "utf-8",
    @Json(name = "mediaType")
    val mediaType: String = "application/json; charset\u003dutf-8",
    @Json(name = "subtype")
    val subtype: String = "json",
    @Json(name = "type")
    val type: String = "application"
)
