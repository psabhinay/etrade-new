package com.etrade.mobilepro.trade.data.mutualfunds

import com.etrade.mobilepro.backends.api.ServerMessageException
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.backends.neo.errors
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.FundsOrderActionType
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.trade.api.OrderPlaceResponse
import com.etrade.mobilepro.trade.api.OrderPreviewResponse
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrderResponse
import com.etrade.mobilepro.trade.api.mutualfunds.orderIds
import com.etrade.mobilepro.trade.data.TradeDelegate
import com.etrade.mobilepro.trade.data.TradeOrderArguments
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId
import com.etrade.mobilepro.trade.data.mutualfunds.dto.MutualFundsTradeOrderResponseDto
import com.etrade.mobilepro.trade.data.mutualfunds.dto.MutualFundsTradeRequestValueDto
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.input.extractAction
import com.squareup.moshi.Moshi

internal class NewMutualFundsOrderTradeDelegate(
    private val apiClient: MutualFundsOrderTradingApiClient,
    private val isTablet: Boolean,
    private val moshi: Moshi
) : TradeDelegate {

    override suspend fun previewOrder(map: InputValuesSnapshot, marketDestination: MarketDestination): ETResult<OrderPreviewResponse> {
        return apiClient.runCatchingET {
            val requestBody = map.createRequestBody(moshi, isTablet)
            when (val actionType = DefaultMutualFundTradeFormInputId.extractAction(map) ?: TradeAction.BUY) {
                TradeAction.BUY -> makePreview(BUY_PREVIEW_REQUEST, requestBody)
                TradeAction.SELL -> makePreview(SELL_PREVIEW_REQUEST, requestBody)
                TradeAction.EXCHANGE -> makePreview(EXCHANGE_PREVIEW_REQUEST, requestBody)
                else -> throw UnsupportedOperationException("Action is not supported: $actionType")
            }
        }
    }

    override suspend fun placeOrder(arguments: TradeOrderArguments, order: TradeOrder): ETResult<OrderPlaceResponse> {
        val response = order.response
        if (response !is OrderPreviewResponse) {
            return ETResult.failure(IllegalStateException("Order ${order.orderId} is not in preview state."))
        }

        return placeNewFundsOrderAuto(response)
    }

    private suspend fun placeNewFundsOrderAuto(response: OrderPreviewResponse): ETResult<OrderPlaceResponse> {
        return apiClient.runCatchingET {
            val order = (response as MutualFundsOrderResponse).orders.first()

            val body = toMutualFundBuyPlaceRequest(
                previewIds = response.orderIds,
                overnightIndicatorFlag = "0"
            )

            when (order.orderActionType) {
                FundsOrderActionType.BUY -> makePlaceOrder(BUY_PLACE_REQUEST, body)
                FundsOrderActionType.SELL -> makePlaceOrder(SELL_PLACE_REQUEST, body)
                FundsOrderActionType.EXCHANGE -> makePlaceOrder(EXCHANGE_PLACE_REQUEST, body)
            }
        }
    }
}

private suspend fun MutualFundsOrderTradingApiClient.makePreview(request: String, body: String): OrderPreviewResponse {
    return makeCall(
        call = { makeFundsPreviewOrder(request, body) },
        createResponse = { responseDto -> MutualFundsOrderPreviewResponseWrapper(responseDto) }
    )
}

private suspend fun MutualFundsOrderTradingApiClient.makePlaceOrder(request: String, bodyDto: MutualFundsTradeRequestValueDto): OrderPlaceResponse {
    return makeCall(
        call = { makeFundsPlaceOrder(request, bodyDto) },
        createResponse = { responseDto -> MutualFundsOrderPlaceResponseWrapper(responseDto) }
    )
}

private suspend fun <T : MutualFundsOrderResponse> MutualFundsOrderTradingApiClient.makeCall(
    call: suspend MutualFundsOrderTradingApiClient.() -> ServerResponseDto<MutualFundsTradeOrderResponseDto>,
    createResponse: (MutualFundsTradeOrderResponseDto) -> T
): T {
    return call().data?.let { responseDto ->
        val errors = responseDto.messages?.errors.orEmpty()

        if (errors.isEmpty()) {
            createResponse(responseDto)
        } else {
            throw ServerMessageException(errors)
        }
    } ?: throw IllegalStateException()
}
