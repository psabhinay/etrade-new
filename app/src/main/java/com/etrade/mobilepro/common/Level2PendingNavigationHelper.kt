package com.etrade.mobilepro.common

import dagger.Reusable
import javax.inject.Inject

@Reusable
class Level2PendingNavigationHelper @Inject constructor() {
    var pendingNavigationToLevel2 = false
}
