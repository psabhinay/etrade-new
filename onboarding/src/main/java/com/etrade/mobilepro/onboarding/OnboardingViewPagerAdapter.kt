package com.etrade.mobilepro.onboarding

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.etrade.mobilepro.imageloader.ImageLoader

class OnboardingViewPagerAdapter(
    private val pages: List<OnboardingPage>,
    private val imageLoader: ImageLoader
) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        return OnboardingPageView(container.context)
            .apply { displayData(pages[position], imageLoader) }
            .also { container.addView(it) }
    }

    override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
        container.removeView(obj as View)
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean = view == obj

    override fun getCount(): Int = pages.size
}
