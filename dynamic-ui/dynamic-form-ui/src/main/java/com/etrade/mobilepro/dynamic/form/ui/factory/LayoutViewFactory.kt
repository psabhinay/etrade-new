package com.etrade.mobilepro.dynamic.form.ui.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.api.Layout
import com.etrade.mobilepro.dynamic.form.ui.LayoutView

internal object LayoutViewFactory : ComponentViewFactory<Layout, LayoutView> {
    override fun createView(context: Context): LayoutView = LayoutView(context)
}
