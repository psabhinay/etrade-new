package com.etrade.mobilepro.news.presentation.detail.helper

import android.widget.TextView
import androidx.core.widget.ContentLoadingProgressBar
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.news.presentation.customview.TextNewsCellView

interface TextNewsContentBindHelper {
    @Suppress("LongParameterList")
    fun bindTextNewsContent(
        contentLoader: TextNewsContentLoader,
        viewLifecycleOwner: LifecycleOwner,
        contentLoadingProgressBar: ContentLoadingProgressBar?,
        contentTextView: TextView?,
        newsCell: TextNewsCellView?,
        actionOnRetry: () -> Unit
    )

    fun bindTextNewsFontAdjusting(
        fontAdjustHandler: TextNewsFontAdjustHandler,
        viewLifecycleOwner: LifecycleOwner,
        contentTextView: TextView?,
        newsCell: TextNewsCellView?
    )
}
