package com.etrade.mobilepro.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.etrade.mobilepro.R

/**
 * Collection of all top level destination. Going back or up from any of these destination will close the app.
 */
val topLevelDestinations = setOf(
    R.id.accountsHomeFragment,
    R.id.completeviewFragment,
    R.id.marketFragment,
    R.id.menuFragment,
    R.id.overviewFragment,
    R.id.tradeFragment,
    R.id.watchlistContainerFragment
)

/**
 * Collection of all destinations, which should show bottom navigation bar.
 */
val destinationsWithBottomBar = setOf(
    R.id.accountTabHostFragment,
    R.id.alertsFragment,
    R.id.allBrokerageAccountsFragment,
    R.id.bankFragment,
    R.id.billPayContainerFragment,
    R.id.chatSetupFragment,
    R.id.checkDepositLanding,
    R.id.customerServiceMenuFragment,
    R.id.educationFragment,
    R.id.learnContainerFragment,
    R.id.marketsCalendarFragment,
    R.id.messageCenterListFragment,
    R.id.newsFragment,
    R.id.profileMenuFragment,
    R.id.transferMoneyContainerFragment,
    R.id.tradeAppActionRedirectFragment,
    R.id.tradeRouterFragment,
    R.id.transferMenuFragment
) + topLevelDestinations

/**
 * Collection of destinations that shows account name as a title.
 */
val destinationsWithAccountNameTitle = setOf(
    R.id.completeviewFragment,
    R.id.allBrokerageAccountsFragment,
    R.id.accountTabHostFragment,
    R.id.bankFragment
)

fun FragmentManager.getCurrentNavigationFragment(): Fragment? = primaryNavigationFragment?.childFragmentManager?.fragments?.first()
