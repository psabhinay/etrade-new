package com.etrade.mobilepro.inboxmessages

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.map
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dynamicui.DynamicUiLayoutRecyclerAdapter
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.toDynamicViewModels
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import javax.inject.Inject

class InboxMessagesViewModel @Inject constructor(
    @Web private val baseUrl: String,
    imageLoader: ImageLoader,
    inboxMessagesRepository: InboxMessagesRepository,
    dispatchers: DispatcherProvider,
    context: Context
) : ViewModel() {

    val inboxMessages: LiveData<List<GenericLayout>> = inboxMessagesDynamicViewLiveData(
        baseUrl = baseUrl,
        imageLoader = imageLoader,
        type = MessageType.DASHBOARD_INFORMATION,
        coroutineContext = dispatchers.IO,
        repository = inboxMessagesRepository,
        context = context
    ).map {
        if (it.view == null) {
            emptyList()
        } else {
            listOf(it.view)
        }
    }
}

fun addInboxMessagesView(fragment: Fragment, recyclerView: RecyclerView, viewModelFactory: ViewModelProvider.Factory) {
    recyclerView.layoutManager = LinearLayoutManager(fragment.context)

    val adapter = DynamicUiLayoutRecyclerAdapter(fragment.viewLifecycleOwner, emptyList())
    val dynamicViewModelBlock: (DynamicViewModel) -> Unit = {
        it.clickEvents.observe(
            fragment.viewLifecycleOwner,
            Observer { event ->
                when (event) {
                    is CtaEvent.NavDirectionsNavigation -> {
                        fragment.findNavController().navigate(event.ctaDirections)
                    }
                }
            }
        )
    }

    fragment.viewModels<InboxMessagesViewModel> { viewModelFactory }.value.inboxMessages.observe(
        fragment.viewLifecycleOwner,
        Observer {
            adapter.values = it.toDynamicViewModels(fragment, block = dynamicViewModelBlock)
        }
    )
    recyclerView.adapter = adapter
}
