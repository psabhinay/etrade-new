package com.etrade.mobilepro.dynamicui

import android.content.Context
import android.util.AttributeSet
import android.view.View

class DynamicDividerView : View {

    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)
}
