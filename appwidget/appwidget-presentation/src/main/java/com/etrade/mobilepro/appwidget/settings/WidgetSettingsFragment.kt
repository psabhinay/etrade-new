package com.etrade.mobilepro.appwidget.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.appwidget.R
import com.etrade.mobilepro.appwidget.api.settings.WidgetId
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.databinding.AppwidgetFragmentSettingsBinding
import com.etrade.mobilepro.appwidget.watchlist.selection.WatchlistSelectionViewModel
import com.etrade.mobilepro.appwidget.widgetcontainer.DefaultAppWidgetProvider
import com.etrade.mobilepro.common.OnNavigateUpListener
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.slider.Slider
import javax.inject.Inject

internal const val WIDGET_ID_KEY = "widgetId"
private const val WIDGET_SETTINGS_REFRESH_DROPDOWN_TAG = "widget_Settings_refresh_dropdown_tag"
private const val WIDGET_SETTINGS_LAYOUT_DROPDOWN_TAG = "widget_Settings_layout_dropdown_tag"

class WidgetSettingsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val router: WidgetSettingsRouter
) : OverlayBaseFragment(), OnNavigateUpListener {
    private val widgetId: WidgetId? by lazy { arguments?.getInt(WIDGET_ID_KEY)?.let { WidgetId(it) } }
    private val viewModel: WidgetSettingsViewModel by viewModels { viewModelFactory }
    private val watchListsViewModel: WatchlistSelectionViewModel by activityViewModels { viewModelFactory }
    private val snackbarUtil: SnackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private var binding: AppwidgetFragmentSettingsBinding? = null
    private lateinit var refreshDropDown: DropDownMultiSelector<WidgetSettings.RefreshRate>
    private lateinit var layoutDropDown: DropDownMultiSelector<WidgetSettings.Layout>

    override val layoutRes: Int = R.layout.appwidget_fragment_settings

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        binding = AppwidgetFragmentSettingsBinding.inflate(inflater)
        return binding?.root
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun onNavigateUp(): Boolean {
        activity?.finish()
        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.onBackPressedDispatcher?.addCallback(viewLifecycleOwner) { activity?.finish() }
        setupFragment()
        bindViewModel()
        viewModel.fetch(widgetId)
        watchListsViewModel.fetch(widgetId)
    }

    private fun setupFragment() {
        setupClickListeners()
        createDropDowns()
    }

    @Suppress("LongMethod") // only ui binding
    private fun bindViewModel() {
        viewModel.viewState.combineLatest(watchListsViewModel.viewState).observe(
            viewLifecycleOwner,
            { (viewState, watchlistViewState) ->
                binding?.run {
                    if (viewState.settings != null) {
                        refreshRate.setText(viewState.settings.refreshRate.asStringRes())
                        layout.setText(viewState.settings.layout.asStringRes())
                        opacity.value = viewState.settings.opacityAlpha.toFloat()
                    }
                    when {
                        !watchlistViewState.isLoading && watchlistViewState.retryAction != null -> {
                            content.visibility = View.GONE
                            progress.hide()
                            snackbarUtil.retrySnackbar { watchlistViewState.retryAction.invoke() }
                        }
                        watchlistViewState.isLoading -> {
                            content.visibility = View.GONE
                            progress.show()
                        }
                        else -> {
                            content.visibility = View.VISIBLE
                            progress.hide()
                            watchlist.setText(
                                viewState.settings?.getSelectedWatchlistName(
                                    watchlistViewState.watchlistRows
                                )
                            )
                        }
                    }
                }
            }
        )
        viewModel.applySettingsSignal.observe(
            viewLifecycleOwner,
            {
                it.consume {
                    context?.let { context -> DefaultAppWidgetProvider.applyWidgetSettings(context) }
                }
            }
        )
    }

    private fun setupClickListeners() {
        binding?.run {
            opacity.addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
                override fun onStartTrackingTouch(slider: Slider) = Unit
                override fun onStopTrackingTouch(slider: Slider) {
                    viewModel.updateOpacity(slider.value)
                }
            })
            refreshRate.setOnClickListener {
                refreshDropDown.open()
            }
            layout.setOnClickListener {
                layoutDropDown.open()
            }
            watchlist.setOnClickListener {
                widgetId?.let { widgetId -> router.openWatchlistSelection(findNavController(), widgetId) }
            }
        }
    }

    private fun createDropDowns() {
        refreshDropDown = DefaultDropDownMultiSelector(
            supportFragmentManager = childFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = WIDGET_SETTINGS_REFRESH_DROPDOWN_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.appwidget_refresh_rate),
                itemsData = viewModel.refreshRateSelectorItemsData,
                selectListener = { _, item ->
                    viewModel.updateRefreshRate(item.value)
                }
            )
        )
        layoutDropDown = DefaultDropDownMultiSelector(
            supportFragmentManager = childFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = WIDGET_SETTINGS_LAYOUT_DROPDOWN_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.appwidget_layout),
                itemsData = viewModel.layoutSelectorItemsData,
                selectListener = { _, item ->
                    viewModel.updateLayout(item.value)
                }
            )
        )
    }
}
