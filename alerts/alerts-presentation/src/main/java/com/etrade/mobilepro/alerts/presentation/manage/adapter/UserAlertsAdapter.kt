package com.etrade.mobilepro.alerts.presentation.manage.adapter

import com.etrade.mobilepro.alerts.api.model.AlertAction
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.alerts.presentation.manage.adapter.delegate.UserAlertAdapterDelegate
import com.etrade.mobilepro.alerts.presentation.manage.adapter.delegate.UserNewsAlertAdapterDelegate
import com.etrade.mobilepro.common.customview.SwipeRevealLayout
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class UserAlertsAdapter(
    alertListener: ((UserAlertItem, AlertAction) -> Unit)?,
    swipeRevealListener: ((UserAlertItem, SwipeRevealLayout.Action) -> Unit)?,
    dragLocker: (() -> Boolean)?
) : ListDelegationAdapter<MutableList<UserAlertItem>>() {
    init {
        delegatesManager.addDelegate(
            UserAlertAdapterDelegate(
                alertListener,
                swipeRevealListener,
                dragLocker
            )
        )
        delegatesManager.addDelegate(
            UserNewsAlertAdapterDelegate(
                alertListener,
                swipeRevealListener,
                dragLocker
            )
        )
        items = mutableListOf()
    }

    fun submitAlerts(alerts: List<UserAlertItem>) {
        dispatchUpdates(items, alerts, UserAlertsDiffCallback(items, alerts))
        items.clear()
        items.addAll(alerts)
    }
}
