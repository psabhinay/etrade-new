package com.etrade.mobilepro.orders.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AdvancedOrderDto(
    @Json(name = "aoCondDisplaySymbol")
    val aoCondDisplaySymbol: String,
    @Json(name = "aoCondSecurityType")
    val aoCondSecurityType: String,
    @Json(name = "aoCondSymbol")
    val aoCondSymbol: String,
    @Json(name = "aoTerm")
    val aoTerm: String,
    @Json(name = "aoType")
    val aoType: String,
    @Json(name = "followPrice")
    val followPrice: String,
    @Json(name = "initialStopPrice")
    val initialStopPrice: String,
    @Json(name = "offsetType")
    val offsetType: String,
    @Json(name = "offsetValue")
    val offsetValue: String,
    @Json(name = "priceBrkLimit")
    val priceBrkLimit: String,
    @Json(name = "priceTrail")
    val priceTrail: String,
    @Json(name = "qualifier")
    val qualifier: String
)
