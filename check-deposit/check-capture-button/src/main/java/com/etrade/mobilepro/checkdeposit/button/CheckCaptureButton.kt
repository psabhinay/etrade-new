package com.etrade.mobilepro.checkdeposit.button

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.core.content.res.getColorOrThrow
import androidx.core.content.res.getDrawableOrThrow
import androidx.core.content.withStyledAttributes

class CheckCaptureButton : FrameLayout {

    private lateinit var initialIcon: Drawable
    private lateinit var loadingIcon: Drawable
    private lateinit var successIcon: Drawable
    private lateinit var failureIcon: Drawable

    private lateinit var initialBackground: Drawable
    private lateinit var loadingBackground: Drawable
    private lateinit var successBackground: Drawable
    private lateinit var failureBackground: Drawable

    @ColorInt private var initialTextColor: Int = 0
    @ColorInt private var loadingTextColor: Int = 0
    @ColorInt private var successTextColor: Int = 0
    @ColorInt private var failureTextColor: Int = 0

    private lateinit var textView: TextView

    var text: String? = null
        set(value) {
            field = value
            textView.text = value
        }

    var state: State = State.INITIAL
        set(value) {
            field = value
            val drawableTop = when (value) {
                State.INITIAL -> {
                    background = initialBackground
                    textView.setTextColor(initialTextColor)
                    initialIcon.also {
                        (it as? Animatable)?.start()
                    }
                }
                State.LOADING -> {
                    background = loadingBackground
                    textView.setTextColor(loadingTextColor)
                    loadingIcon.also {
                        (it as? Animatable)?.start()
                    }
                }
                State.FAILURE -> {
                    background = failureBackground
                    textView.setTextColor(failureTextColor)
                    failureIcon.also {
                        (it as? Animatable)?.start()
                    }
                }
                State.SUCCESS -> {
                    background = successBackground
                    textView.setTextColor(successTextColor)
                    successIcon.also {
                        (it as? Animatable)?.start()
                    }
                }
            }
            setIcon(drawableTop)
        }

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs, defStyleAttr)
    }

    @SuppressLint("ResourceType")
    private fun init(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = R.style.CaptureButton) {
        val view = LayoutInflater.from(context).inflate(R.layout.check_capture_button, this, true)
        isClickable = true
        textView = view.findViewById(R.id.check_capture_button)

        context.withStyledAttributes(attrs, R.styleable.CaptureButton, defStyleAttr, defStyleRes) {
            initialIcon = getDrawableOrThrow(R.styleable.CaptureButton_state_initial_icon)
            loadingIcon = getDrawableOrThrow(R.styleable.CaptureButton_state_loading_icon)
            successIcon = getDrawableOrThrow(R.styleable.CaptureButton_state_success_icon)
            failureIcon = getDrawableOrThrow(R.styleable.CaptureButton_state_failure_icon)

            initialBackground = getDrawableOrThrow(R.styleable.CaptureButton_state_initial_background)
            loadingBackground = getDrawableOrThrow(R.styleable.CaptureButton_state_loading_background)
            successBackground = getDrawableOrThrow(R.styleable.CaptureButton_state_success_background)
            failureBackground = getDrawableOrThrow(R.styleable.CaptureButton_state_failure_background)

            initialTextColor = getColorOrThrow(R.styleable.CaptureButton_state_initial_text_color)
            loadingTextColor = getColorOrThrow(R.styleable.CaptureButton_state_loading_text_color)
            successTextColor = getColorOrThrow(R.styleable.CaptureButton_state_success_text_color)
            failureTextColor = getColorOrThrow(R.styleable.CaptureButton_state_failure_text_color)

            text = getString(R.styleable.CaptureButton_android_text)
        }
        state = State.INITIAL
    }

    private fun setIcon(drawable: Drawable) {
        textView.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null)
    }

    enum class State {
        INITIAL,
        LOADING,
        FAILURE,
        SUCCESS
    }
}
