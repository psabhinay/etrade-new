package com.etrade.mobilepro.alerts.presentation.base

import androidx.annotation.CallSuper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.alerts.api.model.AlertGroup
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.common.customview.SwipeRevealLayout
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy

private const val MAX_ALERTS_REQUEST_COUNT = 100

@Suppress("TooManyFunctions")
abstract class BaseAlertsViewModel(
    private val alertsRepo: AlertsRepo,
    private val user: User,
    private val alertsHolder: AlertsHolder,
    private val alertsDeleter: AlertsDeleter
) : ViewModel() {
    protected abstract val alertGroup: AlertGroup
    protected open val headerItems: List<AlertItem> = emptyList()

    private var disposable: Disposable? = null
    private var loadingInProgress = false
    private var endOfAlertsIsReached = false
    private var endOfFilteredAlertsIsReached = false
    private var triedLoadingOnce = false
    private var alertClearancePending = false
    private var markedAlertCounter = 0

    private val _refreshViews = MutableLiveData<ConsumableLiveEvent<Unit>>()
    private val _markedCountIsZero = MutableLiveData<Boolean>(true)

    val markedCountIsZero = _markedCountIsZero
    internal val refreshViews = _refreshViews

    protected abstract fun onLoadingStart()

    protected abstract fun onAlertsChanged(alerts: List<AlertItem>)

    protected abstract fun onLoadingFailed(it: Throwable, clearLoadedItems: Boolean)

    protected abstract fun onDeletionFailed(it: Throwable, alert: AlertItem.Alert, actualAlerts: List<AlertItem>)

    protected abstract fun onDeletionSuccess(alert: AlertItem.Alert)

    protected open fun onAlertChunkLoaded(hasMore: Boolean) {}

    protected abstract fun hasErrorState(): Boolean

    fun loadMore(clearAlreadyLoadedItems: Boolean) {
        if (loadingInProgress) {
            return
        }
        loadingInProgress = true
        val userId = user.getUserId() ?: return
        alertsHolder.resetIfUserIsChanged(userId, headerItems)
        if (clearAlreadyLoadedItems) {
            onLoadingStart() // show swipe refresh indicator
        } else {
            // show progress bar at the list's bottom
            onAlertsChanged(alertsHolder.alerts(addBottomLoadingIndicator = true))
        }
        if (clearAlreadyLoadedItems) {
            endOfAlertsIsReached = false
            alertClearancePending = true
        }
        val lastAlertInfo = alertsHolder.lastAlertInfo(goingToRefresh = clearAlreadyLoadedItems)
        loadChunk(userId, lastAlertInfo.id, lastAlertInfo.occurredAt, clearAlreadyLoadedItems)
    }

    @Suppress("LongMethod")
    private fun loadChunk(userId: Long, alertId: String, occurredAt: Long, clearAlreadyLoadedItems: Boolean) {
        disposable?.dispose()
        disposable = alertsRepo
            .loadNext(
                userId = userId.toString(),
                startingEventId = alertId,
                startingTimeStamp = occurredAt,
                maxCount = MAX_ALERTS_REQUEST_COUNT
            )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = { list ->
                    if (alertClearancePending) {
                        alertsHolder.deleteAll(headerItems)
                        alertClearancePending = false
                    }
                    endOfAlertsIsReached = list.size < MAX_ALERTS_REQUEST_COUNT
                    val filteredList = list.filter {
                        it.belongsTo() == alertGroup && !alertsDeleter.isDeleting(it.id)
                    }
                    endOfFilteredAlertsIsReached = filteredList.isEmpty()
                    alertsHolder.add(filteredList)
                    onAlertsChanged(alertsHolder.alerts())
                    onAlertChunkLoaded(hasMore = !endOfAlertsIsReached)
                    val last = list.lastOrNull()
                    if (!endOfAlertsIsReached && endOfFilteredAlertsIsReached && last != null) {
                        loadChunk(userId, last.id, last.occurredAt, clearAlreadyLoadedItems)
                    } else {
                        loadingInProgress = false
                    }
                },
                onError = {
                    onLoadingFailed(it, clearAlreadyLoadedItems)
                    loadingInProgress = false
                }
            )
    }

    fun thresholdReached() {
        if (!endOfAlertsIsReached && !endOfFilteredAlertsIsReached) {
            loadMore(clearAlreadyLoadedItems = false)
        }
    }

    @CallSuper
    open fun onBecomeActive() {
        if (hasErrorState()) { return }
        hideDeleteButtons()
        if (alertsHolder.isEmpty(headerItems) && !triedLoadingOnce) {
            triedLoadingOnce = true
            loadMore(clearAlreadyLoadedItems = true)
        }
    }

    @CallSuper
    open fun onBecomeInactive() {
        if (hasErrorState()) { return }
        hideDeleteButtons()
    }

    fun reset() {
        alertsHolder.deleteAll(headerItems)
        triedLoadingOnce = false
        onLoadingStart()
    }

    fun hideDeleteButtons() {
        alertsHolder.hideAllDeleteButtons()
        onAlertsChanged(alertsHolder.alerts())
    }

    fun updateHeaderItems(isVisible: Boolean) =
        alertsHolder.setHeadersVisibility(headerItems, isVisible)

    fun updateAlertsForEditing(isEditable: Boolean) {
        alertsHolder.setEditable(isEditable).also {
            if (it) {
                onAlertsChanged(alertsHolder.alerts())
                _refreshViews.value = Unit.consumable()
                if (!isEditable) {
                    markedAlertCounter = 0
                }
            }
        }
    }

    /**
     *  Toggles markedCountIsZero to true when the marked alert counter is zero and false when 1.
     *
     *  Since all functions that change the counter will(should) call this function, setting
     *  markedCountIsZero to false iff = 1 represents false if > 0.
     */
    private fun onMarkedCountChanges() {
        when (markedAlertCounter) {
            0 -> _markedCountIsZero.value = true
            1 -> _markedCountIsZero.value = false
        }
    }

    fun deleteAlert(alert: AlertItem.Alert) {
        val userId = user.getUserId()?.toString() ?: return

        alertsHolder.delete(alert.id)
        onAlertsChanged(alertsHolder.alerts())

        alertsDeleter.delete(
            userId, alert,
            onError = { notDeletedAlert, throwable ->
                alertsHolder.insertAccordingToOccurrenceDate(notDeletedAlert)
                alertsDeleter.unsubscribeAll()
                onDeletionFailed(throwable, notDeletedAlert, alertsHolder.alerts())
            },
            onSuccess = {
                markedAlertCounter--
                if (markedAlertCounter == 0) {
                    onMarkedCountChanges()
                    onDeletionSuccess(it)
                }
            }
        )
    }

    fun deleteMarkedAlerts() {
        alertsHolder.alerts()
            .filterIsInstance<AlertItem.Alert>()
            .forEach {
                if (it.status.isMarked) { deleteAlert(it) }
            }
    }

    fun deleteButtonAction(alert: AlertItem.Alert, action: SwipeRevealLayout.Action) {
        when (action) {
            SwipeRevealLayout.Action.OPEN -> alertsHolder.toggleDeleteButtonVisibility(alert, isVisible = true)
            SwipeRevealLayout.Action.CLOSE -> alertsHolder.toggleDeleteButtonVisibility(alert, isVisible = false)
            SwipeRevealLayout.Action.START_DRAG -> alertsHolder.hideAllDeleteButtons()
        }
        onAlertsChanged(alertsHolder.alerts())
    }

    fun alertWasDeleted(alertId: String) {
        alertsHolder.delete(alertId)
        onAlertsChanged(alertsHolder.alerts())
    }

    fun markAndCountAlert(alert: AlertItem.Alert) {
        alertsHolder.toggleMarkedStatus(alert)
        if (alert.status.isMarked) {
            markedAlertCounter++
        } else {
            markedAlertCounter--
        }
        onMarkedCountChanges()
    }

    fun markAllAlerts() {
        var alertCount: Int
        alertsHolder.alerts()
            .filterIsInstance<AlertItem.Alert>()
            .also { list ->
                alertCount = list.size

                if (markedAlertCounter < alertCount) {
                    list.onEach {
                        if (!it.status.isMarked) { markAndCountAlert(it) }
                    }
                } else {
                    // all alerts have been marked at this point
                    list.onEach { markAndCountAlert(it) }
                }
            }.let(::onAlertsChanged)

        _refreshViews.value = Unit.consumable()
    }

    fun isCellsDraggingLocked(): Boolean = loadingInProgress

    override fun onCleared() {
        disposable?.dispose()
        alertsDeleter.unsubscribeAll()
        super.onCleared()
    }

    sealed class ViewEffects {
        object AlertsWereDeleted : ViewEffects()
        object AlertDeleteFailed : ViewEffects()
        object LoadingFailed : ViewEffects()
    }
}
