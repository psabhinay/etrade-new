package com.etrade.mobilepro.trade.data.customorder

import com.etrade.eo.rest.retrofit.Scalar
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

internal interface OrderStrategyApiClient {

    @JsonMoshi
    @POST("/app/customorder/strategyidentifier.json")
    @Headers("Content-Type: application/json")
    suspend fun getCustomOrderStrategy(
        @Scalar @Body body: String
    ): ServerResponseDto<CustomOrderStrategyResponseBodyDto>
}
