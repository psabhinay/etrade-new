package com.etrade.mobilepro.chart.performance.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r\u00a8\u0006\u000e"}, d2 = {"Lcom/etrade/mobilepro/chart/performance/model/PerformanceChartPoint;", "Lcom/etrade/mobilepro/chartengine/TimedChartPoint;", "point", "Lcom/etrade/mobilepro/chart/api/ChartPoint;", "defaultValue", "", "(Lcom/etrade/mobilepro/chart/api/ChartPoint;D)V", "x", "Lorg/threeten/bp/LocalDateTime;", "getX", "()Lorg/threeten/bp/LocalDateTime;", "y", "getY", "()D", "performance_debug"})
public final class PerformanceChartPoint implements com.etrade.mobilepro.chartengine.TimedChartPoint {
    @org.jetbrains.annotations.NotNull()
    private final org.threeten.bp.LocalDateTime x = null;
    private final double y = 0.0;
    
    public PerformanceChartPoint(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.chart.api.ChartPoint point, double defaultValue) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public org.threeten.bp.LocalDateTime getX() {
        return null;
    }
    
    @java.lang.Override()
    public double getY() {
        return 0.0;
    }
}