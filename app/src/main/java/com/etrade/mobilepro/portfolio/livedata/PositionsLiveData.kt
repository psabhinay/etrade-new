package com.etrade.mobilepro.portfolio.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.positions.dao.PortfolioPositionDao

class PositionsLiveData(private val positionDao: PortfolioPositionDao) : SingleSourceLiveData<List<Position>>() {
    override fun getData(ids: List<String>): LiveData<List<Position>> = positionDao.getPositions(ids).map { it }
}
