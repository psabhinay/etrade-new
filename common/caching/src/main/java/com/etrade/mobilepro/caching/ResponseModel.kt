package com.etrade.mobilepro.caching

interface ResponseModel {
    var retrievedTimestamp: Long
    var response: String?
}
