package com.etrade.mobilepro.screener.data.datasource

import android.content.res.Resources
import com.etrade.mobilepro.screener.api.EtfFundGroup
import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.FundFamily
import com.etrade.mobilepro.screener.api.datasource.EtfFundDataSource
import com.etrade.mobilepro.screener.data.R
import javax.inject.Inject

@Suppress("LargeClass") // no logic inside, only hardcoded values
class DefaultEtfFundDataSource @Inject constructor(
    resources: Resources
) : EtfFundDataSource {
    private val allFundFamilies by lazy {
        FundFamily(
            id = "",
            title = resources.getString(R.string.fund_profile_fund_all_families),
            isAllItemsElement = true
        )
    }

    private val allFundCategories by lazy {
        FundCategory(
            id = "%",
            title = resources.getString(R.string.fund_profile_fund_all_categories),
            isAllItemsElement = true
        )
    }

    private val allFundGroups by lazy {
        EtfFundGroup(
            id = "%",
            title = resources.getString(R.string.fund_profile_fund_all_groups),
            isAllItemsElement = true,
            type = EtfFundGroup.Type.ALL
        )
    }

    private val fundFamiliesList by lazy {
        listOf(
            allFundFamilies,
            FundFamily(
                id = "AAM",
                title = resources.getString(R.string.aam)
            ),
            FundFamily(
                id = "Aberdeen Standard Investments",
                title = resources.getString(R.string.aberdeen_standard_investments)
            ),
            FundFamily(
                id = "AdvisorShares",
                title = resources.getString(R.string.advisor_shares)
            ),
            FundFamily(
                id = "AlphaClone",
                title = resources.getString(R.string.alphaclone)
            ),
            FundFamily(
                id = "ALPS",
                title = resources.getString(R.string.alps)
            ),
            FundFamily(
                id = "ALPS ETF",
                title = resources.getString(R.string.alps_etf)
            ),
            FundFamily(
                id = "ArrowShares",
                title = resources.getString(R.string.arrow_shares)
            ),
            FundFamily(
                id = "Barclays",
                title = resources.getString(R.string.barclays)
            ),
            FundFamily(
                id = "Barclays Funds",
                title = resources.getString(R.string.barclays_funds)
            ),
            FundFamily(
                id = "CAMBRIA ETF TRUST",
                title = resources.getString(R.string.cambria_etf_trust)
            ),
            FundFamily(
                id = "Citigroup",
                title = resources.getString(R.string.citigroup)
            ),
            FundFamily(
                id = "Columbia",
                title = resources.getString(R.string.columbia)
            ),
            FundFamily(
                id = "Credit Suisse AG",
                title = resources.getString(R.string.credit_suisse)
            ),
            FundFamily(
                id = "Deutsche Bank",
                title = resources.getString(R.string.deutsche_bank)
            ),
            FundFamily(
                id = "Direxion Funds",
                title = resources.getString(R.string.direxion_funds)
            ),
            FundFamily(
                id = "Egshares",
                title = resources.getString(R.string.egshares)
            ),
            FundFamily(
                id = "ELEMENTS",
                title = resources.getString(R.string.elements)
            ),
            FundFamily(
                id = "Emerging Global Advisors",
                title = resources.getString(R.string.emerging_global_advisors)
            ),
            FundFamily(
                id = "ETFS Asian Gold Trust",
                title = resources.getString(R.string.etfs_asian_gold_trust)
            ),
            FundFamily(
                id = "ETFS Palladium Trust",
                title = resources.getString(R.string.etfs_palladium_trust)
            ),
            FundFamily(
                id = "ETFS Platinum Trust",
                title = resources.getString(R.string.etfs_platinum_trust)
            ),
            FundFamily(
                id = "ETFS Precious Metals Basket Trust",
                title = resources.getString(R.string.etfs_precious_metals_basket_trust)
            ),
            FundFamily(
                id = "ETFS Silver Trust",
                title = resources.getString(R.string.etfs_silver_trust)
            ),
            FundFamily(
                id = "ETFS Swiss Gold Trust",
                title = resources.getString(R.string.etfs_swiss_gold_trust)
            ),
            FundFamily(
                id = "ETFS White Metals Basket Trust",
                title = resources.getString(R.string.etfs_white_metals_basket_trust)
            ),
            FundFamily(
                id = "Exchange Traded Concepts Trust",
                title = resources.getString(R.string.exchange_traded_concepts_trust)
            ),
            FundFamily(
                id = "Exchange Traded Concepts, LLC",
                title = resources.getString(R.string.exchange_traded_concepts_llc)
            ),
            FundFamily(
                id = "Fidelity Investments",
                title = resources.getString(R.string.fidelity_investments)
            ),
            FundFamily(
                id = "First Trust",
                title = resources.getString(R.string.first_trust)
            ),
            FundFamily(
                id = "Flexshares Trust",
                title = resources.getString(R.string.flexshares_trust)
            ),
            FundFamily(
                id = "Franklin Templeton Investments",
                title = resources.getString(R.string.franklin_templeton_investments)
            ),
            FundFamily(
                id = "Global X Funds",
                title = resources.getString(R.string.global_x_funds)
            ),
            FundFamily(
                id = "Goldman Sachs",
                title = resources.getString(R.string.goldman_sachs)
            ),
            FundFamily(
                id = "GreenHaven",
                title = resources.getString(R.string.greenhaven)
            ),
            FundFamily(
                id = "Guggenheim Investments",
                title = resources.getString(R.string.guggenheim_investments)
            ),
            FundFamily(
                id = "Highland Funds",
                title = resources.getString(R.string.highland_funds)
            ),
            FundFamily(
                id = "Horizons ETFs",
                title = resources.getString(R.string.horizons_etfs)
            ),
            FundFamily(
                id = "Huntington Strategy Shares",
                title = resources.getString(R.string.huntington_strategy_shares)
            ),
            FundFamily(
                id = "IndexIQ",
                title = resources.getString(R.string.indexiq)
            ),
            FundFamily(
                id = "International Securities Exchange",
                title = resources.getString(R.string.international_securities_exchange)
            ),
            FundFamily(
                id = "iShares",
                title = resources.getString(R.string.ishares)
            ),
            FundFamily(
                id = "JPMorgan",
                title = resources.getString(R.string.jpmorgan)
            ),
            FundFamily(
                id = "KraneShares",
                title = resources.getString(R.string.kraneshares)
            ),
            FundFamily(
                id = "LocalShares",
                title = resources.getString(R.string.localshares)
            ),
            FundFamily(
                id = "Market Vectors",
                title = resources.getString(R.string.market_vectors)
            ),
            FundFamily(
                id = "Morgan Stanley",
                title = resources.getString(R.string.morgan_stanley)
            ),
            FundFamily(
                id = "Pax World",
                title = resources.getString(R.string.pax_world)
            ),
            FundFamily(
                id = "PIMCO",
                title = resources.getString(R.string.pimco)
            ),
            FundFamily(
                id = "PowerShares",
                title = resources.getString(R.string.powershares)
            ),
            FundFamily(
                id = "PowerShares DB",
                title = resources.getString(R.string.powershares_db)
            ),
            FundFamily(
                id = "Precidian Funds LLC",
                title = resources.getString(R.string.precidian_funds_llc)
            ),
            FundFamily(
                id = "ProShares",
                title = resources.getString(R.string.proshares)
            ),
            FundFamily(
                id = "QuantShares",
                title = resources.getString(R.string.quantshares)
            ),
            FundFamily(
                id = "Renaissance Capital",
                title = resources.getString(R.string.renaissance_capital)
            ),
            FundFamily(
                id = "RevenueShares",
                title = resources.getString(R.string.revenueshares)
            ),
            FundFamily(
                id = "Royal Bank of Scotland NV",
                title = resources.getString(R.string.royal_bank_of_scotland_nv)
            ),
            FundFamily(
                id = "Russell",
                title = resources.getString(R.string.russell)
            ),
            FundFamily(
                id = "Sage Quant ETF Trust",
                title = resources.getString(R.string.sage_quant_etf_trust)
            ),
            FundFamily(
                id = "Schwab ETFs",
                title = resources.getString(R.string.schwab_etfs)
            ),
            FundFamily(
                id = "SPDR State Street Global Advisors",
                title = resources.getString(R.string.spdr_state_street_global_advisors)
            ),
            FundFamily(
                id = "Teucrium",
                title = resources.getString(R.string.teucrium)
            ),
            FundFamily(
                id = "UBS AG",
                title = resources.getString(R.string.ubs_ag)
            ),
            FundFamily(
                id = "United States Commodity Funds LLC",
                title = resources.getString(R.string.united_states_commodity_funds_llc)
            ),
            FundFamily(
                id = "Van Eck",
                title = resources.getString(R.string.van_eck)
            ),
            FundFamily(
                id = "Vanguard",
                title = resources.getString(R.string.vanguard)
            ),
            FundFamily(
                id = "Vident Financial",
                title = resources.getString(R.string.vident_financial)
            ),
            FundFamily(
                id = "WisdomTree",
                title = resources.getString(R.string.wisdomtree)
            )
        )
    }

    private val fundGroupList by lazy {
        listOf(
            allFundGroups,
            EtfFundGroup(
                id = "Alternative",
                title = resources.getString(R.string.alternative),
                type = EtfFundGroup.Type.ALTERNATIVE
            ),
            EtfFundGroup(
                id = "Balanced",
                title = resources.getString(R.string.balanced),
                type = EtfFundGroup.Type.BALANCED
            ),
            EtfFundGroup(
                id = "Commodity",
                title = resources.getString(R.string.commodity),
                type = EtfFundGroup.Type.COMMODITY
            ),
            EtfFundGroup(
                id = "Domestic Equity",
                title = resources.getString(R.string.domestic_equity),
                type = EtfFundGroup.Type.DOMESTIC_EQUITY
            ),
            EtfFundGroup(
                id = "International Equity",
                title = resources.getString(R.string.international_equity),
                type = EtfFundGroup.Type.INTERNATIONAL_EQUITY
            ),
            EtfFundGroup(
                id = "Life Cycle",
                title = resources.getString(R.string.life_cycle),
                type = EtfFundGroup.Type.LIFE_CYCLE
            ),
            EtfFundGroup(
                id = "Municipal Bond",
                title = resources.getString(R.string.municipal_bond),
                type = EtfFundGroup.Type.MUNICIPAL_BOND
            ),
            EtfFundGroup(
                id = "Municipal State Bond",
                title = resources.getString(R.string.municipal_state_bond),
                type = EtfFundGroup.Type.MUNICIPAL_STATE_BOND
            ),
            EtfFundGroup(
                id = "Specialty",
                title = resources.getString(R.string.specialty),
                type = EtfFundGroup.Type.SPECIALTY
            ),
            EtfFundGroup(
                id = "Taxable Bond",
                title = resources.getString(R.string.taxable_bond),
                type = EtfFundGroup.Type.TAXABLE_BOND
            )
        )
    }

    override fun getAllFundFamiliesItem(): FundFamily = allFundFamilies

    override fun getFundFamilies(): List<FundFamily> = fundFamiliesList

    override fun getAllFundCategoriesItem(): FundCategory = allFundCategories

    override fun getAllFundGroupsItem(): EtfFundGroup = allFundGroups

    override fun getFundGroups(): List<EtfFundGroup> = fundGroupList
}
