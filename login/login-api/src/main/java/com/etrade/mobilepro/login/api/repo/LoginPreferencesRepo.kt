package com.etrade.mobilepro.login.api.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.encryption.api.CipherAuthenticator

interface LoginPreferencesRepo {

    val isBiometricAuthenticationEnabled: Boolean
    val isFirstLogin: Boolean

    var isPasswordHidden: Boolean
    var isHidePasswordEnabled: Boolean
    var isRememberUserNameEnabled: Boolean
    var isSecurityCodeEnabled: Boolean
    var isRsaUser: Boolean
    var isLoginMdaPopupShown: Boolean
    var version: Int
    var isLoginForSameUser: Boolean
    var isPushNotificationPopupShown: Boolean

    fun disableBiometricAuthentication()

    /**
     * Registers successful login event. If at least one login event was registered [isFirstLogin] will start to return `false`.
     *
     * Each time a user logs in successfully this method should be called.
     */
    fun registerLoginEvent()

    /**
     * Returns whether the exception is no valid biometric or a generic error, also resets secure key and removes previous encrypted password and username
     * if the exception is related to secure key
     */
    fun checkException(exception: Throwable): LoginPreferencesError

    suspend fun setUserName(userName: String): ETResult<Unit>
    suspend fun getUserName(): ETResult<String>

    suspend fun setUserPassword(password: String, authenticator: CipherAuthenticator): ETResult<Unit>
    suspend fun getUserPassword(authenticator: CipherAuthenticator): ETResult<String>
    suspend fun updateUserPasswordIfChanged(password: String, authenticator: CipherAuthenticator): ETResult<Unit>
}
