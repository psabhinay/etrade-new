package com.etrade.mobilepro.portfolio.taxlot

import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.portfolio.model.TaxLotReferenceDto
import com.etrade.mobilepro.position.createUnderlyingSymbolInfo
import com.etrade.mobilepro.positions.api.TaxLotPosition
import org.junit.Assert
import org.junit.Test
import org.threeten.bp.Instant
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.RoundingMode

internal class TaxLotUtilKtTest {

    @Test
    fun `validate the tax lot days gain calculation for given set of data`() {
        // Given
        val quantity = "12"
        val lastPrice = BigDecimal("100.34")
        val previousClose = BigDecimal("100.00")
        val pricePaid = "95.00"

        val suite = createTaxLots(quantity = quantity, pricePaid = pricePaid)
        val testData =
            createInstrument(symbol = "AAPL", lastPrice = lastPrice, previousClose = previousClose)

        // When
        val result = suite.updateWithInstrument(testData)

        // Then
        val expectedDayGain = "4.08" // ( last - prev) * Quantity
        Assert.assertEquals(expectedDayGain, result.daysGain?.toPlainString())

        val expectedTotalGain = "64.08" // ( last - price paid) * Quantity
        Assert.assertEquals(expectedTotalGain, result.totalGain.truncatedString())

        val expectedDayGainPercentage = "0.34" // ( last - prev) / prev close in %
        Assert.assertEquals(expectedDayGainPercentage, result.daysGainPercentage.truncatedString())

        val expectedTotalGainPercentage = "5.62" // ( last - price paid) / price paid close in %
        Assert.assertEquals(
            expectedTotalGainPercentage,
            result.totalGainPercentage.truncatedString()
        )
    }

    @Test
    fun `Validate the result of createTodayPositionInfo for given position which is traded today`() {
        // Given
        val quantity = "10"
        val pricePaid = "95.00"

        val todayInstant = Instant.now().epochSecond
        val suite =
            createTaxLots(quantity = quantity, pricePaid = pricePaid, acquiredTimeMs = todayInstant)

        // When
        val result = suite.createTodayPositionInfo()

        // Then
        Assert.assertNotEquals(ZERO, result.quantity)
        Assert.assertNotEquals(ZERO, result.pricePaid)
    }

    @Test
    fun `If positions bought today, day gain dollar will be same as total gain dollar`() {
        // Given
        val quantity = "12"
        val lastPrice = BigDecimal("100.34")
        val previousClose = BigDecimal("100.00")
        val pricePaid = "95.00"
        val todayInstant = Instant.now().epochSecond

        val suite =
            createTaxLots(quantity = quantity, pricePaid = pricePaid, acquiredTimeMs = todayInstant)
        val testData =
            createInstrument(symbol = "AAPL", lastPrice = lastPrice, previousClose = previousClose)

        // When
        val result = suite.updateWithInstrument(testData)

        // Then
        Assert.assertEquals(result.daysGain, result.totalGain)
        Assert.assertEquals(result.daysGainPercentage, result.totalGainPercentage)
    }

    @Test
    fun `If positions bought earlier, day gain dollar will NOT be same as total gain dollar`() {
        // Given
        val quantity = "12"
        val lastPrice = BigDecimal("100.34")
        val previousClose = BigDecimal("100.00")
        val pricePaid = "95.00"

        val suite = createTaxLots(quantity = quantity, pricePaid = pricePaid)
        val testData =
            createInstrument(symbol = "AAPL", lastPrice = lastPrice, previousClose = previousClose)

        // When
        val result = suite.updateWithInstrument(testData)

        // Then
        Assert.assertNotEquals(result.daysGain, result.totalGain)
    }

    private fun createInstrument(
        symbol: String,
        symbolDescription: String = symbol,
        instrumentType: InstrumentType = InstrumentType.EQ,
        displaySymbol: String = symbol,
        underlyingSymbol: String = symbol,
        underlyingInstrumentType: InstrumentType = InstrumentType.EQ,
        lastPrice: BigDecimal,
        previousClose: BigDecimal,
    ): Instrument {
        return object : Instrument {
            override val displaySymbol: String = displaySymbol
            override val ask: BigDecimal? = null
            override val bid: BigDecimal? = null
            override val priceEarningRation: BigDecimal? = null
            override val earningsPerShare: BigDecimal? = null
            override val fiftyTwoWeekHigh: BigDecimal? = null
            override val fiftyTwoWeekLow: BigDecimal? = null
            override val lastPrice: BigDecimal = lastPrice
            override val tickIndicator: Short? = null
            override val marketCap: BigDecimal? = null
            override val volume: BigDecimal? = null
            override val daysChange: BigDecimal? = null
            override val daysChangePercentage: BigDecimal? = null
            override val delta: BigDecimal? = null
            override val gamma: BigDecimal? = null
            override val theta: BigDecimal? = null
            override val vega: BigDecimal? = null
            override val impliedVolatilityPercentage: BigDecimal? = null
            override val daysToExpire: Int? = null
            override val underlyingSymbolInfo: WithSymbolInfo? =
                createUnderlyingSymbolInfo(underlyingSymbol, underlyingInstrumentType)
            override val previousClose: BigDecimal = previousClose
            override val symbol: String = symbol
            override val symbolDescription = symbolDescription
            override val instrumentType: InstrumentType = instrumentType
        }
    }

    private fun createTaxLots(
        quantity: String,
        pricePaid: String,
        acquiredTimeMs: Long = 1617113683
    ): TaxLotPosition {
        return TaxLotReferenceDto(
            positionId = "12345",
            taxLotId = "12345",
            acquiredDateMs = acquiredTimeMs,
            price = pricePaid,
            termCode = "",
            daysGainValue = "",
            daysGainPct = "",
            marketValueRaw = "",
            totalCost = "",
            totalCostForGainPct = "",
            totalGainValue = "",
            lotSourceCode = "",
            originalQty = "",
            remainingQty = quantity,
            availableQty = "",
            orderNo = "",
            legNo = "",
            locationCode = "",
            exchangeRate = "",
            settlementCurrency = "",
            paymentCurrency = "",
            adjPrice = "",
            commPerShare = "0",
            feesPerShare = "0",
            shortType = "",
            inTheMoneyFlagRaw = ""
        )
    }
}

private fun BigDecimal?.truncatedString(): String? {
    return this?.setScale(2, RoundingMode.HALF_DOWN)?.toPlainString()
}
