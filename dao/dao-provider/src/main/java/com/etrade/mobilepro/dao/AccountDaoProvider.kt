package com.etrade.mobilepro.dao

import com.etrade.mobilepro.account.dao.AccountDao

interface AccountDaoProvider {
    fun getAccountDao(): AccountDao
}
