package com.etrade.mobilepro.searching.symbol.cache

import io.realm.RealmObject

open class SymbolDb(
    var title: String = "",
    var instrumentId: String = "",
    var description: String = "",
    var type: String = "",
    var cachedAt: Long = 0,
    var queryValue: String = "",
    var expire: String = "",
    var strike: String = "",
    var displaySymbol: String = "",
    var securityType: String = "",
    var settlementSessionCd: Int = 0
) : RealmObject()
