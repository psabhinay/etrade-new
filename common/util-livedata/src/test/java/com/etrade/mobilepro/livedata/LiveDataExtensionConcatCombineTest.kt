package com.etrade.mobilepro.livedata

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class LiveDataExtensionConcatCombineTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    private var mutableLiveData1 = MutableLiveData<ViewState>()
    private val mutableLiveData2 = MutableLiveData<ViewState>()
    private val liveData1 = mutableLiveData1
    private val liveData2 = mutableLiveData2
    private val mediatorLiveData = liveData1.concatCombine(liveData2) {
        a, b ->
        combine(a, b)
    }

    @Test
    fun `concatCombine both succeed test`() {
        setup()
        val mockObserver = mock<Observer<ViewState>>()
        mediatorLiveData.observeForever(mockObserver)
        mutableLiveData2.value = ViewState.Success("Ending execute 2")
        mutableLiveData1.value = ViewState.Success("Ending execute 1")

        verify(mockObserver).onChanged(eq(ViewState.Success("Ending execute 1Ending execute 2")))
    }

    @Test
    fun `concatCombine first succeed second failed test`() {
        setup()
        val mockObserver = mock<Observer<ViewState>>()
        mediatorLiveData.observeForever(mockObserver)

        mutableLiveData2.postValue(ViewState.Error("Execute 2 failed"))
        mutableLiveData1.postValue(ViewState.Success("Ending execute 1"))

        verify(mockObserver).onChanged(eq(ViewState.Success("Ending execute 1Execute 2 failed")))
    }

    @Test
    fun `concatCombine first failed second succeed test`() {
        setup()
        val mockObserver = mock<Observer<ViewState>>()
        mediatorLiveData.observeForever(mockObserver)
        mutableLiveData2.postValue(ViewState.Success("Ending execute 2"))
        mutableLiveData1.postValue(ViewState.Error("Execute 1 failed"))

        verify(mockObserver).onChanged(eq(ViewState.Error("Execute 1 failed")))
    }

    private fun setup() {
        mutableLiveData1.value = ViewState.Loading("execute 1 is about to start")
        mutableLiveData2.value = ViewState.Loading("execute 2 is about to start")
    }

    private fun combine(a: ViewState, b: ViewState): ViewState {
        val result: String

        if (a is ViewState.Success) {
            result = when (b) {
                is ViewState.Success -> {
                    a.results.plus(b.results)
                }
                is ViewState.Loading -> {
                    a.results.plus(b.loadingMessage)
                }
                is ViewState.Error -> {
                    a.results.plus(b.errorMessage)
                }
            }
            return ViewState.Success(result)
        }
        return a
    }

    sealed class ViewState {
        data class Loading(val loadingMessage: String) : ViewState()
        data class Error(val errorMessage: String) : ViewState()
        data class Success(val results: String) : ViewState()
    }
}
