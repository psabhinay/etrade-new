package com.etrade.mobilepro.login.session

import com.etrade.eo.webview.cookiejar.api.CookieStoreCleaner
import com.etrade.mobilepro.login.api.repo.LogoutRepo
import org.slf4j.LoggerFactory

class DefaultLogoutCase(private val logoutRepo: LogoutRepo, private val storeCleaner: CookieStoreCleaner) :
    LogoutUseCase {

    private val logger = LoggerFactory.getLogger(DefaultLogoutCase::class.java)

    override suspend fun execute(parameter: Unit) {
        val logoutResponse = logoutRepo.logout()
        val exceptionMsg = logoutResponse.exceptionOrNull()

        if (logoutResponse.isFailure) {
            logger.error("logout failed due to ${exceptionMsg?.message ?: " unknown IO error"}", exceptionMsg)
        } else {
            logger.debug(exceptionMsg?.message ?: "Logout successful")
        }

        // due to above service call dependence on Cookies,
        // cookies cleared only after logout call is completed
        storeCleaner.clean(true)
    }
}
