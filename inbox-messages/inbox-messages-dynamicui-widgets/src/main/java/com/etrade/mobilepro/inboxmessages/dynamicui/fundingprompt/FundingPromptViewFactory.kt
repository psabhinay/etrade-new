package com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt

import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobile.accounts.dto.AccountDto
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.inboxmessages.dynamicui.api.WithAccountDtoInfo
import com.etrade.mobilepro.inboxmessages.dynamicui.api.fundingprompt.FundingPrompt
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class FundingPromptViewFactory @Inject constructor(
    private val navHelper: FundingPromptNavHelper,
    private val resources: Resources
) : DynamicUiViewFactory {
    override fun createView(dto: Any, meta: List<Any>): GenericLayout =
        if (dto is FundingPromptViewDto) {
            FundingPromptViewLayout(dto, navHelper, resources)
        } else {
            throw DynamicUiViewFactoryException("FundingPromptViewFactory registered to unknown type")
        }
}

class FundingPromptViewLayout(
    private val dto: FundingPromptViewDto,
    private val navHelper: FundingPromptNavHelper,
    private val resources: Resources
) : GenericLayout, WithAccountDtoInfo {

    val logger: Logger = LoggerFactory.getLogger(javaClass)

    var accountSourceLabel: String? = null

    override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
        FundingPromptViewModel(resources, dto, navHelper, accountSourceLabel)
    }
    override val viewModelClass: Class<out DynamicViewModel> = FundingPromptViewModel::class.java

    override fun refresh(viewModel: DynamicViewModel) {
        if (viewModel is FundingPromptViewModel) {
            viewModel.refresh(dto as? FundingPromptViewDto)
        } else {
            logger.error("view model is not of type FundingPromptViewModel")
        }
    }

    override val uniqueIdentifier: String = run {
        val promptKey = (dto.data as? FundingPrompt)?.let {
            it.account.accountUuid + it.image
        }

        "${javaClass.name}_$promptKey"
    }

    override val account: AccountDto?
        get() = (dto.data as? FundingPrompt)?.account
}
