package com.etrade.mobilepro.bloombergtv.presentation

import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.os.IBinder
import android.view.ViewGroup
import android.view.WindowManager
import com.etrade.mobilepro.bloombergtv.common.BloombergTvPipStatusHelper
import com.etrade.mobilepro.bloombergtv.presentation.common.VIDEO_URL
import com.etrade.mobilepro.bloombergtv.presentation.delegate.CallStateDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.service.AudioFocusServiceDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.service.MediaPlayerServiceDelegate
import com.etrade.mobilepro.bloombergtv.presentation.pip.PipViewFactory
import dagger.android.AndroidInjection
import java.lang.ref.WeakReference
import javax.inject.Inject

class BloombergTvService : Service() {

    @Inject
    lateinit var bloombergTvPipStatusHelper: BloombergTvPipStatusHelper

    private val windowManager: WindowManager
        get() = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    private var videoContainerRef: WeakReference<ViewGroup>? = null

    private val mediaPlayerDelegate = MediaPlayerServiceDelegate()

    private val becomingNoisyReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            pause()
        }
    }

    private val callStateDelegate: CallStateDelegate = CallStateDelegate(
        onCallStarted = ::pause,
        onCallEnded = ::play,
    )

    private val audioFocusServiceDelegate = AudioFocusServiceDelegate(
        onFocusGain = ::play,
        onFocusLoss = ::pause,
        onFocusLossTransient = ::pause,
        onFocusLossCanDuck = { mediaPlayerDelegate.mute() },
    )

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
        registerReceiver(becomingNoisyReceiver, IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY))
        callStateDelegate.init(baseContext)
        audioFocusServiceDelegate.init(baseContext)
    }

    override fun onDestroy() {
        super.onDestroy()
        bloombergTvPipStatusHelper.isPipMode = false
        mediaPlayerDelegate.dismantlePlayer()
        unregisterReceiver(becomingNoisyReceiver)
        callStateDelegate.dismantle()
        audioFocusServiceDelegate.dismantle()
        videoContainerRef?.get()?.let { windowManager.removeView(it) }
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        stopSelf()
    }

    override fun onBind(intent: Intent): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        intent?.getStringExtra(VIDEO_URL)
            ?.takeIf { audioFocusServiceDelegate.requestFocus() }
            ?.let { url ->
                videoContainerRef = WeakReference(
                    PipViewFactory(baseContext).createPipView(
                        windowManager,
                        onReady = { holder ->
                            mediaPlayerDelegate.start(
                                url,
                                holder,
                                videoContainerProvider = { videoContainerRef?.get() },
                                hasOngoingCall = { callStateDelegate.hasOngoingCall },
                                onError = ::stopSelf
                            )
                        },
                        onDestroyed = {
                            mediaPlayerDelegate.dismantlePlayer()
                        }
                    )
                )
                bloombergTvPipStatusHelper.isPipMode = true
            } ?: stopSelf()

        return START_STICKY
    }

    private fun play() {
        if (audioFocusServiceDelegate.requestFocus()) {
            mediaPlayerDelegate.unmute()
            mediaPlayerDelegate.play()
        }
    }

    private fun pause() {
        mediaPlayerDelegate.pause()
    }
}
