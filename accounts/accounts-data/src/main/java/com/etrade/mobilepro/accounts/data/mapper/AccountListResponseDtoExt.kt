package com.etrade.mobilepro.accounts.data.mapper

import com.etrade.mobile.accounts.billpay.BillPay
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobile.accounts.details.AccountsDetailedList
import com.etrade.mobilepro.accounts.data.rest.AccountListResponseDto
import java.math.BigDecimal
import java.math.BigInteger

@Suppress("LongMethod")
fun AccountListResponseDto.mapToApiModel(): AccountsDetailedList {
    return object : AccountsDetailedList {
        override val liabilityValue: BigDecimal? = this@mapToApiModel.summary?.liabilityValue?.toBigDecimal()
        override val netAssetValue: BigDecimal? = this@mapToApiModel.summary?.netAssetValue?.toBigDecimal()
        override val highRiskFlag: Boolean = this@mapToApiModel.restricted?.highRiskFlag ?: false
        override val accounts: List<AccountDetails> = this@mapToApiModel.holder?.accounts?.map {
            object : AccountDetails {
                override val nickname: String? = ""
                override val instNo: BigInteger = it.instNo?.toBigIntegerOrNull() ?: BigInteger.ZERO
                override val accountCategory: String = ""
                override val accountId: String = it.accountId ?: ""
                override val accountIdKey: String = it.accountIdKey ?: ""
                override val maskedAccountId: String = it.maskedAccountId ?: ""
                override val accountMode: String = it.accountMode ?: ""
                override val accountName: String = it.accountName ?: "--"
                override val accountShortName: String = it.accountShortName ?: "--"
                override val accountLongName: String = it.accountLongName ?: "--"
                override val mobileDescription: String = it.mobileDescription ?: "--"
                override val accountValue: BigDecimal? = it.accountValue?.toBigDecimal()
                override val accountType: String = it.accountType ?: ""
                override val institutionType: String = it.institutionType ?: ""
                override val accountStatus: String = it.accountStatus ?: ""
                override val restrictionLevel: String = it.restrictionLevel ?: ""
                override val closedDate: Long = it.closedDate ?: 0L
                override val brokerage: AccountDetails.Brokerage = object : AccountDetails.Brokerage {
                    override val cashAvailableForWithdrawal: BigDecimal? = it.brokerage?.cashAvailableForWithdrawal?.toBigDecimal()
                    override val marginAvailableForWithdrawal: BigDecimal? = it.brokerage?.marginAvailableForWithdrawal?.toBigDecimal()
                    override val purchasingPower: BigDecimal? = it.brokerage?.purchasingPower?.toBigDecimal()
                    override val totalAvailableForWithdrawal: BigDecimal? = it.brokerage?.totalAvailableForWithdrawal?.toBigDecimal()
                    override val marginBalance: BigDecimal? = it.brokerage?.marginBalance?.toBigDecimal()
                    override val netCashBalance: BigDecimal? = it.brokerage?.netCashBalance?.toBigDecimal()
                    override val cmAccount: Boolean = it.brokerage?.cmAccount ?: false
                    override val contributionDetails: List<AccountDetails.ContributionDetails> = it.brokerage?.contributionDetails?.map { dto ->
                        object : AccountDetails.ContributionDetails {
                            override val contributionAmount: BigDecimal? = dto.contributionAmount?.toBigDecimal()
                            override val taxYear: Int = dto.taxYear ?: 0
                        }
                    } ?: emptyList()
                }
                override val billPay: BillPay? = null
            }
        } ?: emptyList()
    }
}
