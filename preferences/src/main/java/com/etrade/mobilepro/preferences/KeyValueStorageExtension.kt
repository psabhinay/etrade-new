package com.etrade.mobilepro.preferences

import com.etrade.eo.userprefrences.api.KeyValueStorage
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

inline fun <T> KeyValueStorage.delegate(
    key: String,
    default: T,
    crossinline getter: KeyValueStorage.(String, T) -> T,
    crossinline setter: KeyValueStorage.(String, T) -> Unit
): ReadWriteProperty<Any, T> {
    return object : ReadWriteProperty<Any, T> {

        override fun getValue(thisRef: Any, property: KProperty<*>): T = getter(key, default)

        override fun setValue(thisRef: Any, property: KProperty<*>, value: T) = setter(key, value)
    }
}

@Suppress("UNCHECKED_CAST")
inline fun <reified T : Any> KeyValueStorage.preference(key: String, default: T? = null): ReadWriteProperty<Any, T> {
    return when (T::class) {
        String::class -> delegate(key, default as? String ?: "", KeyValueStorage::getStringValue, KeyValueStorage::putStringValue)
        Int::class -> delegate(key, default as? Int ?: -1, KeyValueStorage::getIntValue, KeyValueStorage::putIntValue)
        Boolean::class -> delegate(key, default as? Boolean ?: false, KeyValueStorage::getBooleanValue, KeyValueStorage::putBooleanValue)

        else -> throw UnsupportedOperationException()
    } as ReadWriteProperty<Any, T>
}

inline fun <reified T : Enum<T>> KeyValueStorage.getEnumValue(key: String, default: T): T =
    enumValueOf(getStringValue(key, default.toString()) ?: default.toString())

inline fun <reified T : Enum<T>> KeyValueStorage.putEnumValue(key: String, value: T) =
    putStringValue(key, value.toString())
