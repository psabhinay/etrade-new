// Code generated by moshi-kotlin-codegen. Do not edit.
@file:Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION",
    "RedundantExplicitType", "LocalVariableName", "RedundantVisibilityModifier",
    "PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.trade.`data`.common.LegDetailsCommonDto
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.`internal`.Util
import java.lang.NullPointerException
import java.lang.reflect.Constructor
import kotlin.Int
import kotlin.String
import kotlin.Suppress
import kotlin.Unit
import kotlin.collections.List
import kotlin.collections.emptySet
import kotlin.jvm.Volatile
import kotlin.text.buildString

public class OrderConfirmCancelRequestJsonAdapter(
  moshi: Moshi
) : JsonAdapter<OrderConfirmCancelRequest>() {
  private val options: JsonReader.Options = JsonReader.Options.of("previewId", "underlier",
      "strategyType", "requestType", "originalOrderNo", "overrideRestrictedCd", "priceType",
      "allOrNone", "accountId", "orderType", "orderTerm", "acctDesc", "stopPrice", "optionLevelCd",
      "limitPrice", "orderNumber", "destMktCode", "origSysCodeReq", "orderSource", "dayTradingFlag",
      "dayTradingVer", "overnightIndicatorFlag", "clearancecode", "legRequestList")

  private val stringAdapter: JsonAdapter<String> = moshi.adapter(String::class.java, emptySet(),
      "previewId")

  private val listOfLegDetailsCommonDtoAdapter: JsonAdapter<List<LegDetailsCommonDto>> =
      moshi.adapter(Types.newParameterizedType(List::class.java, LegDetailsCommonDto::class.java),
      emptySet(), "legRequestList")

  @Volatile
  private var constructorRef: Constructor<OrderConfirmCancelRequest>? = null

  public override fun toString(): String = buildString(47) {
      append("GeneratedJsonAdapter(").append("OrderConfirmCancelRequest").append(')') }

  public override fun fromJson(reader: JsonReader): OrderConfirmCancelRequest {
    var previewId: String? = null
    var underlier: String? = null
    var strategyType: String? = null
    var requestType: String? = null
    var originalOrderNo: String? = null
    var overrideRestrictedCd: String? = null
    var priceType: String? = null
    var allOrNone: String? = null
    var accountId: String? = null
    var orderType: String? = null
    var orderTerm: String? = null
    var acctDesc: String? = null
    var stopPrice: String? = null
    var optionLevelCd: String? = null
    var limitPrice: String? = null
    var orderNumber: String? = null
    var destMktCode: String? = null
    var origSysCodeReq: String? = null
    var orderSource: String? = null
    var dayTradingFlag: String? = null
    var dayTradingVer: String? = null
    var overnightIndicatorFlag: String? = null
    var clearanceCode: String? = null
    var legRequestList: List<LegDetailsCommonDto>? = null
    var mask0 = -1
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> {
          previewId = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("previewId",
              "previewId", reader)
          // $mask = $mask and (1 shl 0).inv()
          mask0 = mask0 and 0xfffffffe.toInt()
        }
        1 -> {
          underlier = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("underlier",
              "underlier", reader)
          // $mask = $mask and (1 shl 1).inv()
          mask0 = mask0 and 0xfffffffd.toInt()
        }
        2 -> {
          strategyType = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("strategyType",
              "strategyType", reader)
          // $mask = $mask and (1 shl 2).inv()
          mask0 = mask0 and 0xfffffffb.toInt()
        }
        3 -> {
          requestType = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("requestType",
              "requestType", reader)
          // $mask = $mask and (1 shl 3).inv()
          mask0 = mask0 and 0xfffffff7.toInt()
        }
        4 -> {
          originalOrderNo = stringAdapter.fromJson(reader) ?:
              throw Util.unexpectedNull("originalOrderNo", "originalOrderNo", reader)
          // $mask = $mask and (1 shl 4).inv()
          mask0 = mask0 and 0xffffffef.toInt()
        }
        5 -> {
          overrideRestrictedCd = stringAdapter.fromJson(reader) ?:
              throw Util.unexpectedNull("overrideRestrictedCd", "overrideRestrictedCd", reader)
          // $mask = $mask and (1 shl 5).inv()
          mask0 = mask0 and 0xffffffdf.toInt()
        }
        6 -> {
          priceType = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("priceType",
              "priceType", reader)
          // $mask = $mask and (1 shl 6).inv()
          mask0 = mask0 and 0xffffffbf.toInt()
        }
        7 -> {
          allOrNone = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("allOrNone",
              "allOrNone", reader)
          // $mask = $mask and (1 shl 7).inv()
          mask0 = mask0 and 0xffffff7f.toInt()
        }
        8 -> {
          accountId = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("accountId",
              "accountId", reader)
          // $mask = $mask and (1 shl 8).inv()
          mask0 = mask0 and 0xfffffeff.toInt()
        }
        9 -> {
          orderType = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("orderType",
              "orderType", reader)
          // $mask = $mask and (1 shl 9).inv()
          mask0 = mask0 and 0xfffffdff.toInt()
        }
        10 -> {
          orderTerm = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("orderTerm",
              "orderTerm", reader)
          // $mask = $mask and (1 shl 10).inv()
          mask0 = mask0 and 0xfffffbff.toInt()
        }
        11 -> {
          acctDesc = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("acctDesc",
              "acctDesc", reader)
          // $mask = $mask and (1 shl 11).inv()
          mask0 = mask0 and 0xfffff7ff.toInt()
        }
        12 -> {
          stopPrice = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("stopPrice",
              "stopPrice", reader)
          // $mask = $mask and (1 shl 12).inv()
          mask0 = mask0 and 0xffffefff.toInt()
        }
        13 -> {
          optionLevelCd = stringAdapter.fromJson(reader) ?:
              throw Util.unexpectedNull("optionLevelCd", "optionLevelCd", reader)
          // $mask = $mask and (1 shl 13).inv()
          mask0 = mask0 and 0xffffdfff.toInt()
        }
        14 -> {
          limitPrice = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("limitPrice",
              "limitPrice", reader)
          // $mask = $mask and (1 shl 14).inv()
          mask0 = mask0 and 0xffffbfff.toInt()
        }
        15 -> {
          orderNumber = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("orderNumber",
              "orderNumber", reader)
          // $mask = $mask and (1 shl 15).inv()
          mask0 = mask0 and 0xffff7fff.toInt()
        }
        16 -> {
          destMktCode = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("destMktCode",
              "destMktCode", reader)
          // $mask = $mask and (1 shl 16).inv()
          mask0 = mask0 and 0xfffeffff.toInt()
        }
        17 -> {
          origSysCodeReq = stringAdapter.fromJson(reader) ?:
              throw Util.unexpectedNull("origSysCodeReq", "origSysCodeReq", reader)
          // $mask = $mask and (1 shl 17).inv()
          mask0 = mask0 and 0xfffdffff.toInt()
        }
        18 -> {
          orderSource = stringAdapter.fromJson(reader) ?: throw Util.unexpectedNull("orderSource",
              "orderSource", reader)
          // $mask = $mask and (1 shl 18).inv()
          mask0 = mask0 and 0xfffbffff.toInt()
        }
        19 -> {
          dayTradingFlag = stringAdapter.fromJson(reader) ?:
              throw Util.unexpectedNull("dayTradingFlag", "dayTradingFlag", reader)
          // $mask = $mask and (1 shl 19).inv()
          mask0 = mask0 and 0xfff7ffff.toInt()
        }
        20 -> {
          dayTradingVer = stringAdapter.fromJson(reader) ?:
              throw Util.unexpectedNull("dayTradingVer", "dayTradingVer", reader)
          // $mask = $mask and (1 shl 20).inv()
          mask0 = mask0 and 0xffefffff.toInt()
        }
        21 -> {
          overnightIndicatorFlag = stringAdapter.fromJson(reader) ?:
              throw Util.unexpectedNull("overnightIndicatorFlag", "overnightIndicatorFlag", reader)
          // $mask = $mask and (1 shl 21).inv()
          mask0 = mask0 and 0xffdfffff.toInt()
        }
        22 -> {
          clearanceCode = stringAdapter.fromJson(reader) ?:
              throw Util.unexpectedNull("clearanceCode", "clearancecode", reader)
          // $mask = $mask and (1 shl 22).inv()
          mask0 = mask0 and 0xffbfffff.toInt()
        }
        23 -> legRequestList = listOfLegDetailsCommonDtoAdapter.fromJson(reader) ?:
            throw Util.unexpectedNull("legRequestList", "legRequestList", reader)
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    if (mask0 == 0xff800000.toInt()) {
      // All parameters with defaults are set, invoke the constructor directly
      return  OrderConfirmCancelRequest(
          previewId = previewId as String,
          underlier = underlier as String,
          strategyType = strategyType as String,
          requestType = requestType as String,
          originalOrderNo = originalOrderNo as String,
          overrideRestrictedCd = overrideRestrictedCd as String,
          priceType = priceType as String,
          allOrNone = allOrNone as String,
          accountId = accountId as String,
          orderType = orderType as String,
          orderTerm = orderTerm as String,
          acctDesc = acctDesc as String,
          stopPrice = stopPrice as String,
          optionLevelCd = optionLevelCd as String,
          limitPrice = limitPrice as String,
          orderNumber = orderNumber as String,
          destMktCode = destMktCode as String,
          origSysCodeReq = origSysCodeReq as String,
          orderSource = orderSource as String,
          dayTradingFlag = dayTradingFlag as String,
          dayTradingVer = dayTradingVer as String,
          overnightIndicatorFlag = overnightIndicatorFlag as String,
          clearanceCode = clearanceCode as String,
          legRequestList = legRequestList ?: throw Util.missingProperty("legRequestList",
              "legRequestList", reader)
      )
    } else {
      // Reflectively invoke the synthetic defaults constructor
      @Suppress("UNCHECKED_CAST")
      val localConstructor: Constructor<OrderConfirmCancelRequest> = this.constructorRef ?:
          OrderConfirmCancelRequest::class.java.getDeclaredConstructor(String::class.java,
          String::class.java, String::class.java, String::class.java, String::class.java,
          String::class.java, String::class.java, String::class.java, String::class.java,
          String::class.java, String::class.java, String::class.java, String::class.java,
          String::class.java, String::class.java, String::class.java, String::class.java,
          String::class.java, String::class.java, String::class.java, String::class.java,
          String::class.java, String::class.java, List::class.java, Int::class.javaPrimitiveType,
          Util.DEFAULT_CONSTRUCTOR_MARKER).also { this.constructorRef = it }
      return localConstructor.newInstance(
          previewId,
          underlier,
          strategyType,
          requestType,
          originalOrderNo,
          overrideRestrictedCd,
          priceType,
          allOrNone,
          accountId,
          orderType,
          orderTerm,
          acctDesc,
          stopPrice,
          optionLevelCd,
          limitPrice,
          orderNumber,
          destMktCode,
          origSysCodeReq,
          orderSource,
          dayTradingFlag,
          dayTradingVer,
          overnightIndicatorFlag,
          clearanceCode,
          legRequestList ?: throw Util.missingProperty("legRequestList", "legRequestList", reader),
          mask0,
          /* DefaultConstructorMarker */ null
      )
    }
  }

  public override fun toJson(writer: JsonWriter, value_: OrderConfirmCancelRequest?): Unit {
    if (value_ == null) {
      throw NullPointerException("value_ was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("previewId")
    stringAdapter.toJson(writer, value_.previewId)
    writer.name("underlier")
    stringAdapter.toJson(writer, value_.underlier)
    writer.name("strategyType")
    stringAdapter.toJson(writer, value_.strategyType)
    writer.name("requestType")
    stringAdapter.toJson(writer, value_.requestType)
    writer.name("originalOrderNo")
    stringAdapter.toJson(writer, value_.originalOrderNo)
    writer.name("overrideRestrictedCd")
    stringAdapter.toJson(writer, value_.overrideRestrictedCd)
    writer.name("priceType")
    stringAdapter.toJson(writer, value_.priceType)
    writer.name("allOrNone")
    stringAdapter.toJson(writer, value_.allOrNone)
    writer.name("accountId")
    stringAdapter.toJson(writer, value_.accountId)
    writer.name("orderType")
    stringAdapter.toJson(writer, value_.orderType)
    writer.name("orderTerm")
    stringAdapter.toJson(writer, value_.orderTerm)
    writer.name("acctDesc")
    stringAdapter.toJson(writer, value_.acctDesc)
    writer.name("stopPrice")
    stringAdapter.toJson(writer, value_.stopPrice)
    writer.name("optionLevelCd")
    stringAdapter.toJson(writer, value_.optionLevelCd)
    writer.name("limitPrice")
    stringAdapter.toJson(writer, value_.limitPrice)
    writer.name("orderNumber")
    stringAdapter.toJson(writer, value_.orderNumber)
    writer.name("destMktCode")
    stringAdapter.toJson(writer, value_.destMktCode)
    writer.name("origSysCodeReq")
    stringAdapter.toJson(writer, value_.origSysCodeReq)
    writer.name("orderSource")
    stringAdapter.toJson(writer, value_.orderSource)
    writer.name("dayTradingFlag")
    stringAdapter.toJson(writer, value_.dayTradingFlag)
    writer.name("dayTradingVer")
    stringAdapter.toJson(writer, value_.dayTradingVer)
    writer.name("overnightIndicatorFlag")
    stringAdapter.toJson(writer, value_.overnightIndicatorFlag)
    writer.name("clearancecode")
    stringAdapter.toJson(writer, value_.clearanceCode)
    writer.name("legRequestList")
    listOfLegDetailsCommonDtoAdapter.toJson(writer, value_.legRequestList)
    writer.endObject()
  }
}
