package com.etrade.mobilepro.trade.data.contingent

import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.trade.data.DAY_TRADING_FLAG
import com.etrade.mobilepro.trade.data.DAY_TRADING_VER
import com.etrade.mobilepro.trade.data.TradeRequestParamsUpdater
import com.etrade.mobilepro.trade.data.advanced.FORM_TARGET_KEY
import com.etrade.mobilepro.trade.data.advanced.OPT_LEVEL
import com.etrade.mobilepro.trade.data.advanced.PLATFORM_KEY
import com.etrade.mobilepro.trade.data.advanced.PLATFORM_VALUE
import com.etrade.mobilepro.trade.data.advanced.PREVIEW_X_KEY
import com.etrade.mobilepro.trade.data.advanced.PRIMARY_SECURITY_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.VALUE_SECURITY_TYPE_STOCK
import com.etrade.mobilepro.trade.data.form.input.DefaultContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.data.stock.ContingentStockTradeRequestParamsUpdater
import com.etrade.mobilepro.trade.form.api.input.ContingentStockTradeFormInputId
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
internal class ContingentOrderTradeDelegateStockTest : AbstractContingentOrderTradeDelegateTest<ContingentStockTradeFormInputId>() {

    override val inputId: ContingentStockTradeFormInputId = DefaultContingentStockTradeFormInputId
    override val requestParamUpdater: TradeRequestParamsUpdater = ContingentStockTradeRequestParamsUpdater()
    override val orderType: OrderType = OrderType.EQUITY

    @Test
    fun `PREVIEW - MARKET BUY 3 AAPL - MSFT LAST_PRICE LESS_THAN_OR_EQUAL_TO 100`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.stock,
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.contingent,
            inputId.contingentOrderInputId.symbol to "MSFT:EQ",
            inputId.contingentOrderInputId.contingentSymbol to "MSFT:EQ",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.lastPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.lessThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "100"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.contingentOrderInputId.symbol to "MSFT",
            inputId.contingentOrderInputId.contingentSymbol to "MSFT",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.lastPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.lessThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "100",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_STOCK,
            inputId.contingentOrderInputId.securityType to VALUE_SECURITY_TYPE_STOCK,
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "contingentcreateentry",
            PREVIEW_X_KEY to "53",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }

    @Test
    fun `PREVIEW - LIMIT BUY 3 AAPL - MSFT ASK_PRICE GREATER_THAN_OR_EQUAL_TO 400`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.stock,
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "101",
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to "0",
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.contingent,
            inputId.contingentOrderInputId.symbol to "MSFT:EQ",
            inputId.contingentOrderInputId.contingentSymbol to "MSFT:EQ",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.askPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.greaterThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "400"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "101",
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to "0",
            inputId.contingentOrderInputId.symbol to "MSFT",
            inputId.contingentOrderInputId.contingentSymbol to "MSFT",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_STOCK,
            inputId.contingentOrderInputId.securityType to VALUE_SECURITY_TYPE_STOCK,
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.askPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.greaterThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "400",
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "contingentcreateentry",
            PREVIEW_X_KEY to "53",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )
        comparePreviewInputAndOutput(input, output)
    }

    @Test
    fun `PREVIEW - STOP_LIMIT_ON_QUOTE SELL_SHORT 3 AAPL - AAPL BID_PRICE GREATER_THAN_OR_EQUAL_TO 180`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.stock,
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.stopLimit,
            inputId.limitPriceForStopLimit to "100",
            inputId.stopPriceForStopLimit to "120",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to "0",
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.contingent,
            inputId.contingentOrderInputId.symbol to "AAPL:EQ",
            inputId.contingentOrderInputId.contingentSymbol to "AAPL:EQ",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.bidPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.greaterThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "180"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.stopLimit,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to "0",
            inputId.limitPrice to "100",
            inputId.stopPrice to "120",
            inputId.contingentOrderInputId.symbol to "AAPL",
            inputId.contingentOrderInputId.contingentSymbol to "AAPL",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.bidPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.greaterThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "180",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_STOCK,
            inputId.contingentOrderInputId.securityType to VALUE_SECURITY_TYPE_STOCK,
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "contingentcreateentry",
            PREVIEW_X_KEY to "53",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }

    @Test
    fun `PREVIEW - STOP BUY 3 AAPL - AAPL BID_PRICE GREATER_THAN_OR_EQUAL_TO 180`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.stock,
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.stop,
            inputId.stopPrice to "120",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to "0",
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.contingent,
            inputId.contingentOrderInputId.symbol to "AAPL:EQ",
            inputId.contingentOrderInputId.contingentSymbol to "AAPL:EQ",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.bidPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.greaterThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "180"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.stop,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to "0",
            inputId.stopPrice to "120",
            inputId.contingentOrderInputId.symbol to "AAPL",
            inputId.contingentOrderInputId.contingentSymbol to "AAPL",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.bidPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.greaterThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "180",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_STOCK,
            inputId.contingentOrderInputId.securityType to VALUE_SECURITY_TYPE_STOCK,
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "contingentcreateentry",
            PREVIEW_X_KEY to "53",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }
}
