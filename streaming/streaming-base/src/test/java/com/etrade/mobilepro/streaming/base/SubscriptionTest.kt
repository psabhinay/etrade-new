package com.etrade.mobilepro.streaming.base

import com.etrade.eo.streaming.rx.ConnectionEvent
import com.etrade.eo.streaming.rx.ConnectionState
import com.etrade.eo.streaming.rx.DataEvent
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.TestScheduler
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.Test
import java.util.concurrent.TimeUnit

class SubscriptionTest {

    @Test
    fun `check null source`() {
        Subscription<Unit> { fail("Observable was not disposed.") }.apply {
            var exception: Exception? = null
            observable.subscribeBy(
                onNext = { exception = RuntimeException("onNext") },
                onError = { exception = RuntimeException("onError", it) },
                onComplete = { exception = RuntimeException("onComplete") }
            )

            source = null
            assertNull(source)

            exception.failOnException()
        }
    }

    @Test
    fun `check new source`() {
        Subscription<Int> { fail("Observable was not disposed.") }.apply {
            var exception: Exception? = null
            val actual: MutableList<Int> = mutableListOf()
            observable.subscribeBy(
                onNext = { actual.add(it) },
                onError = { exception = RuntimeException("onError", it) },
                onComplete = { exception = RuntimeException("onComplete") }
            )

            val expected = listOf(DataEvent(1), DataEvent(2), DataEvent(3))
            source = Observable.fromIterable(expected)
            assertNotNull(source)

            exception.failOnException()

            assertEquals(expected.map { it.data }, actual)
        }
    }

    @Test
    fun `check setting a source disposes previous source`() {
        Subscription<Int> { fail("Observable was not disposed.") }.apply {
            var exception: Exception? = null
            val actual: MutableList<Int> = mutableListOf()
            observable.subscribeBy(
                onNext = { actual.add(it) },
                onError = { exception = RuntimeException("onError", it) },
                onComplete = { exception = RuntimeException("onComplete") }
            )

            val scheduler = TestScheduler()
            source = Observable.interval(INTERVAL, TimeUnit.MILLISECONDS, scheduler)
                .map { DataEvent(1) }
            assertNotNull(source)

            scheduler.advanceTimeBy(DELAY_TIME, TimeUnit.MILLISECONDS)
            exception.failOnException()

            source = Observable.interval(INTERVAL, TimeUnit.MILLISECONDS, scheduler)
                .map { DataEvent(2) }
            assertNotNull(source)

            scheduler.advanceTimeBy(DELAY_TIME, TimeUnit.MILLISECONDS)
            exception.failOnException()

            source = null

            scheduler.advanceTimeBy(INTERVAL, TimeUnit.MILLISECONDS)
            exception.failOnException()

            var previous = 0
            actual.forEach {
                if (it > previous) {
                    previous = it
                } else if (it < previous) {
                    fail<Unit>("Items from old source merged into items from new source: $actual")
                }
            }
        }
    }

    @Test
    fun `check arbitrary streaming events are filtered out`() {
        Subscription<Int> { fail("Observable was not disposed.") }.apply {
            var exception: Exception? = null
            val actual: MutableList<Int> = mutableListOf()
            observable.subscribeBy(
                onNext = { actual.add(it) },
                onError = { exception = RuntimeException("onError", it) },
                onComplete = { exception = RuntimeException("onComplete") }
            )

            val expected = listOf(
                ConnectionEvent(ConnectionState.CONNECTING),
                DataEvent(1),
                DataEvent(2),
                DataEvent(3),
                ConnectionEvent(ConnectionState.DISCONNECTED)
            )
            source = Observable.fromIterable(expected)
            assertNotNull(source)

            exception.failOnException()

            assertEquals(expected.filterIsInstance<DataEvent<Int>>().map { it.data }, actual)
        }
    }

    @Test
    fun `check subscription is disposed`() {
        var disposed = false
        Subscription<Int> { disposed = true }.apply {
            observable.subscribeBy().dispose()
        }
        assertTrue(disposed)
    }

    private fun Exception?.failOnException() {
        if (this != null) {
            fail<Unit>(message, this)
        }
    }
}
