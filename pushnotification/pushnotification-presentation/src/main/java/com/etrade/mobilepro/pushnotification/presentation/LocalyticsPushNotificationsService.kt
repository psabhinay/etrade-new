package com.etrade.mobilepro.pushnotification.presentation

import com.google.firebase.messaging.RemoteMessage
import com.localytics.androidx.Localytics

class LocalyticsPushNotificationsService : MessagingService {

    override fun onDestroy() {
        // np
    }

    override fun onNewToken(token: String) {
        Localytics.setPushRegistrationId(token)
    }

    override fun onMessageReceived(message: RemoteMessage) {
        Localytics.handleFirebaseMessage(message.data)
    }
}
