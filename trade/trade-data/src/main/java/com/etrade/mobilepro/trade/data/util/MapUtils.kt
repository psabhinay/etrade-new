package com.etrade.mobilepro.trade.data.util

fun MutableMap<String, Any?>.removeParams(params: Set<String>) {
    params.forEach { param -> remove(param) }
}
