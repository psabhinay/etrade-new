package com.etrade.mobilepro.trade.form.api.input

interface ContingentOptionTradeFormInputId :
    OptionTradeFormInputId,
    WithActionTradeFormInputId,
    WithContingentOrderInputId,
    WithQuantity
