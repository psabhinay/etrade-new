package com.etrade.mobilepro.streaming.lightstreamer.level2

enum class Level2BookType(val bookName: String) {
    ARCA("ARC"),
    ARCA_NSDQ("ARCNDQ"),
    MONTAGE("MONT"),
    NSDQ("ITC"),
    NYSE_OPEN("NYO"),
    OPTION("OPTALL"),
    NO_BOOK("NO_BOOK")
}
