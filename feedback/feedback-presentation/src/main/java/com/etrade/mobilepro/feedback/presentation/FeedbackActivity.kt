package com.etrade.mobilepro.feedback.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.eo.corelibandroid.addAccessibilityDelegate
import com.etrade.feedback.presentation.R
import com.etrade.feedback.presentation.databinding.ActivityFeedbackBinding
import com.etrade.mobilepro.baseactivity.ActivityNetworkConnectionDelegate
import com.etrade.mobilepro.baseactivity.ActivitySessionDelegate
import com.etrade.mobilepro.common.extension.clearFocusOnTouchOutside
import com.etrade.mobilepro.common.setupToolbarWithUpButton
import com.etrade.mobilepro.common.viewmodel.SnackbarViewModel
import com.etrade.mobilepro.uiwidgets.vectorratingbar.ImageRatingBar
import com.etrade.mobilepro.util.android.accessibility.bindButtonAccessibility
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

const val FEEDBACK_PATH = "/feedback"

class FeedbackActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var snackbarViewModel: SnackbarViewModel

    @Inject
    lateinit var snackbarUtilFactory: SnackbarUtilFactory

    @Inject
    lateinit var activitySessionDelegate: ActivitySessionDelegate

    @Inject
    lateinit var activityNetworkConenectionDelegate: ActivityNetworkConnectionDelegate

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    lateinit var viewModel: FeedbackViewModel

    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ this }, { binding.feedbackContainer }) }

    private var feedbackTextHint = ""

    private val binding by viewBinding(ActivityFeedbackBinding::inflate)

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory).get(FeedbackViewModel::class.java)
        setContentView(binding.root)
        setupViews()

        setupToolbarWithUpButton(R.string.feedback_activity_title)

        viewModel.viewState.observe(
            this,
            Observer {
                when (it) {
                    FeedbackViewModel.ViewState.Default -> {
                        binding.loadingIndicator.hide()
                    }
                    FeedbackViewModel.ViewState.Loading -> {
                        binding.loadingIndicator.show()
                    }
                    FeedbackViewModel.ViewState.Success -> {
                        binding.loadingIndicator.hide()
                        onFeedbackSubmitSuccess()
                    }
                    FeedbackViewModel.ViewState.Failure -> {
                        binding.loadingIndicator.hide()
                        onFeedbackSubmitError()
                    }
                }
            }
        )

        activityNetworkConenectionDelegate.initNetworkConnectivityBanner(this)

        activitySessionDelegate.observeTimeoutEvent(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        clearFocusOnTouchOutside(event)
        return super.dispatchTouchEvent(event)
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        activitySessionDelegate.onUserInteraction(this)
    }

    private fun setRatingBar() {
        binding.ratingBar.addAccessibilityDelegate(getString(R.string.feedback_activity_rating_hint, binding.ratingBar.rating.toString()))
        binding.ratingBar.onRatingBarChangeListener = object : ImageRatingBar.OnRatingBarChangeListener {
            override fun onRatingChanged(rating: Int) {
                if (rating == 0) {
                    setSubmitButtonState(isEnabled = false)
                } else {
                    setSubmitButtonState(isEnabled = true)
                }
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setFeedbackText() {
        binding.feedbackText.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
            when (hasFocus) {
                true -> {
                    feedbackTextHint = binding.feedbackText.hint.toString()
                    binding.feedbackText.hint = ""
                }
                false -> {
                    binding.feedbackText.hint = feedbackTextHint
                    feedbackTextHint = ""
                }
            }
        }

        binding.feedbackText.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            when (event.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_UP -> view.parent.requestDisallowInterceptTouchEvent(false)
            }

            false
        }
    }

    private fun setSubmitButton() {
        binding.submitButton.setOnClickListener {
            viewModel.sendFeedback(binding.ratingBar.rating, binding.feedbackText.text.toString())

            setSubmitButtonState(isEnabled = false)
        }
        setSubmitButtonState(isEnabled = false)
    }

    private fun setupViews() {
        setRatingBar()
        setFeedbackText()
        setSubmitButton()
    }

    private fun onFeedbackSubmitSuccess() {
        snackbarViewModel.postAppMessage(getString(R.string.feedback_submit_success_message))
        onBackPressed()
    }

    private fun onFeedbackSubmitError() {
        snackbarUtil
            .snackbar(getString(R.string.feedback_sumbit_error_message), Snackbar.LENGTH_SHORT)
            ?.show()

        setSubmitButtonState(isEnabled = true)
    }

    private fun setSubmitButtonState(isEnabled: Boolean) {
        binding.submitButton.isEnabled = isEnabled
        binding.submitButton.bindButtonAccessibility(
            if (isEnabled) {
                ""
            } else {
                getString(R.string.submit_feedback_disabled)
            }
        )
    }
}
