package com.etrade.mobilepro.biometric.api

class BiometricProviderResult(
    val isSuccessful: Boolean,
    val providerError: BiometricProviderError? = null,
    val isBiometricSupported: Boolean = providerError != BiometricProviderError.ERROR_HW_NOT_PRESENT &&
        providerError != BiometricProviderError.ERROR_BIOMETRIC_NO_HW_BACKED_KEYSTORE
)
