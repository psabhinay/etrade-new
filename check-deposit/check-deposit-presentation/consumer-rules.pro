-keep class com.miteksystems.misnap.params.*
-keepclassmembers class com.miteksystems.misnap.params.* { *; }

-keep class com.miteksystems.misnap.storage.CameraInfoCacher
-keepclassmembers class com.miteksystems.misnap.storage.CameraInfoCacher { *; }
-keep class com.miteksystems.misnap.utils.Utils
-keepclassmembers class com.miteksystems.misnap.utils.Utils { *; }

-keep class com.miteksystems.misnap.analyzer.*
-keepclassmembers class com.miteksystems.misnap.analyzer.* { *; }
-keep class com.miteksystems.misnap.analyzer.MiSnapAnalyzer$MiSnapAnalyzerExtraInfo { *; }

-keep class com.miteksystems.misnap.documents.*
-keepclassmembers class com.miteksystems.misnap.documents.* { *; }
-keep class com.miteksystems.misnap.events.*
-keepclassmembers class com.miteksystems.misnap.events.* { *; }

-keep class com.miteksystems.misnap.misnapworkflow.**
-keepclassmembers class com.miteksystems.misnap.misnapworkflow.** { *; }
-keep class com.miteksystems.misnap.misnapworkflow_UX2.**
-keepclassmembers class com.miteksystems.misnap.misnapworkflow_UX2.** { *; }

-keep class com.miteksystems.misnap.barcode.**
-keepclassmembers class com.miteksystems.misnap.barcode.** { *; }
-dontwarn com.miteksystems.misnap.barcode.R*

-keepclasseswithmembers class com.miteksystems.** {
    native <methods>;
}

-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}
