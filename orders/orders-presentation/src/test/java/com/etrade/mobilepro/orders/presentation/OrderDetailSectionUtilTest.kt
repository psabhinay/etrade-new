package com.etrade.mobilepro.orders.presentation

import android.content.res.Resources
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.ExecInstruction
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.orders.presentation.section.OrderLabels
import com.etrade.mobilepro.orders.presentation.section.OrderResources
import com.etrade.mobilepro.orders.presentation.section.viewfactory.getQuantity
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.SummarySectionItem
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class OrderDetailSectionUtilTest {

    @Mock
    private lateinit var resources: Resources

    private lateinit var orderDetailLabelResources: OrderDetailLabelResources

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        orderDetailLabelResources = generateMockOrderDetailLabelResources()
    }

    @Test
    fun `test getQuantity all flag false`() {
        val quantity = "3"
        val tableSectionItemLayout = getQuantity(
            quantity,
            ExecInstruction(allOrNoneFlag = false, neverHeldFlag = false, doNotReduceFlag = false, doNotIncreaseFlag = false),
            false,
            orderDetailLabelResources,
            AdvancedOrderType.UNKNOWN,
            OrderTerm.GOOD_FOR_DAY
        )
        assertTrue("check tableSectionItemLayout is SummarySectionItem", tableSectionItemLayout is SummarySectionItem)
        val summarySectionItem: SummarySectionItem = tableSectionItemLayout as SummarySectionItem
        assertEquals("check summarySectionItem.value", quantity, summarySectionItem.value)
    }

    @Test
    fun `test getQuantity isConditionalOrder true and allOrNoneFlag true`() {
        val quantity = "3"
        val tableSectionItemLayout = getQuantity(
            quantity = quantity,
            execInstruction = ExecInstruction(allOrNoneFlag = true, neverHeldFlag = false, doNotReduceFlag = false, doNotIncreaseFlag = false),
            isConditionalOrder = true,
            resources = orderDetailLabelResources,
            advancedOrderType = AdvancedOrderType.UNKNOWN,
            orderTerm = OrderTerm.GOOD_FOR_DAY
        )
        assertTrue("check tableSectionItemLayout is SummarySectionItem", tableSectionItemLayout is SummarySectionItem)
        val summarySectionItem: SummarySectionItem = tableSectionItemLayout as SummarySectionItem
        assertEquals("check summarySectionItem.value", quantity, summarySectionItem.value)
    }

    @Test
    fun `test getQuantity isConditionalOrder false and allOrNoneFlag true`() {
        val quantity = "3"
        val tableSectionItemLayout = getQuantity(
            quantity = quantity,
            execInstruction = ExecInstruction(allOrNoneFlag = true, neverHeldFlag = false, doNotReduceFlag = false, doNotIncreaseFlag = false),
            isConditionalOrder = false,
            resources = orderDetailLabelResources,
            advancedOrderType = AdvancedOrderType.UNKNOWN,
            orderTerm = OrderTerm.GOOD_FOR_DAY
        )
        assertTrue("check tableSectionItemLayout is SummarySectionItem", tableSectionItemLayout is SummarySectionItem)
        val summarySectionItem: SummarySectionItem = tableSectionItemLayout as SummarySectionItem
        assertEquals("check summarySectionItem.value", quantity.plus(" (AON)"), summarySectionItem.value)
    }

    @Test
    fun `test getQuantity isConditionalOrder false and doNotIncreaseFlag true`() {
        val quantity = "3"
        val tableSectionItemLayout = getQuantity(
            quantity = quantity,
            execInstruction = ExecInstruction(allOrNoneFlag = false, neverHeldFlag = false, doNotReduceFlag = false, doNotIncreaseFlag = true),
            isConditionalOrder = false,
            resources = orderDetailLabelResources,
            advancedOrderType = AdvancedOrderType.UNKNOWN,
            orderTerm = OrderTerm.GOOD_TILL_DATE_DNI
        )
        assertTrue("check tableSectionItemLayout is SummarySectionItem", tableSectionItemLayout is SummarySectionItem)
        val summarySectionItem: SummarySectionItem = tableSectionItemLayout as SummarySectionItem
        assertEquals("check summarySectionItem.value", quantity.plus(" (DNI)"), summarySectionItem.value)
    }

    @Test
    fun `test getQuantity isConditionalOrder false and doNotReduceFlag true`() {
        val quantity = "3"
        val tableSectionItemLayout = getQuantity(
            quantity = quantity,
            execInstruction = ExecInstruction(allOrNoneFlag = false, neverHeldFlag = false, doNotReduceFlag = true, doNotIncreaseFlag = false),
            isConditionalOrder = false,
            resources = orderDetailLabelResources,
            advancedOrderType = AdvancedOrderType.UNKNOWN,
            orderTerm = OrderTerm.GOOD_TILL_DATE_DNR
        )
        assertTrue("check tableSectionItemLayout is SummarySectionItem", tableSectionItemLayout is SummarySectionItem)
        val summarySectionItem: SummarySectionItem = tableSectionItemLayout as SummarySectionItem
        assertEquals("check summarySectionItem.value", quantity.plus(" (DNR)"), summarySectionItem.value)
    }

    private fun generateMockOrderDetailLabelResources(): OrderDetailLabelResources {
        return OrderDetailLabelResources(
            OrderLabels(
                "All or None",
                "Order Number",
                "Order Placed",
                "Order Status",
                "Executed",
                "Order Type",
                "Quantity",
                "Term",
                "Price Type",
                "Limit Price",
                "Stop Price",
                "Price Executed",
                "Commission/Fees",
                "Cancel Requested",
                "Expired",
                "System Rejected",
                "Cancelled",
                "Order Saved",
                "Account",
                "Strategy",
                "Estimated Commission",
                "Estimated Total Cost",
                "Estimated Total Process",
                "Quantity (Executed/Entered)",
                "GTD %1$",
                "Extended Hrs Day(7am-8pm ET)",
                "Extended Hrs Night(8pm-4am ET)",
                "Destination",
                "Order executed",
                "Qualifier",
                "Condition",
                "Value",
                "Stop Val $",
                "Stop Val %",
                "Price",
                "Limit Offset",
                "Estimated Commission/Fees",
                "Description",
                "Exercise Value",
                "Shares Exercised",
                "Shares to Exercise",
                "Shares to Issue",
                "Est. Gross Proceeds",
                "Total Price",
                "Sec. Fee",
                "Broker Assist Fee",
                "Disbursement Fee",
                "Est. Tax Withheld",
                "Est. Shares WithHeld/Value",
                "Est. Net Proceeds",
                "Shares to Exercise",
                "Shares to Issue",
                "Shares WithHeld/Value"
            ),
            OrderResources(
                "$",
                "--",
                "%",
                122323,
                resources
            )
        )
    }
}
