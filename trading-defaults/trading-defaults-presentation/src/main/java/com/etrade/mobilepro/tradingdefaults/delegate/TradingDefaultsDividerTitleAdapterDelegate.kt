package com.etrade.mobilepro.tradingdefaults.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.tradingdefaults.TradingDefaultsItem
import com.etrade.mobilepro.tradingdefaults.viewholder.TradingDefaultsDividerTitleViewHolder
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class TradingDefaultsDividerTitleAdapterDelegate :
    AbsListItemAdapterDelegate<TradingDefaultsItem.DividerTitle, TradingDefaultsItem, TradingDefaultsDividerTitleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): TradingDefaultsDividerTitleViewHolder {
        return TradingDefaultsDividerTitleViewHolder.from(parent)
    }

    override fun isForViewType(item: TradingDefaultsItem, items: MutableList<TradingDefaultsItem>, position: Int): Boolean =
        item is TradingDefaultsItem.DividerTitle

    override fun onBindViewHolder(item: TradingDefaultsItem.DividerTitle, holder: TradingDefaultsDividerTitleViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }
}
