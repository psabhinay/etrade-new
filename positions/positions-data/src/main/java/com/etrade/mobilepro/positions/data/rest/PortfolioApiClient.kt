package com.etrade.mobilepro.positions.data.rest

import com.etrade.eo.rest.retrofit.Xml
import com.etrade.mobilepro.positions.data.dto.PortfolioEntryDto
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface PortfolioApiClient {

    @Xml
    @POST("e/t/mobile/GetPortfolioEntries")
    fun getPortfolioEntries(@Body request: RequestBody): Single<PortfolioEntryDto>
}

internal fun createPortfolioEntriesRequestBody(
    userId: String,
    accountId: String,
    extendedHourFlag: Boolean
): RequestBody = "UserId=$userId&AccountId=$accountId&Ehflag=$extendedHourFlag"
    .toRequestBody("application/x-www-form-urlencoded".toMediaTypeOrNull())
