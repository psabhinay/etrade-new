package com.etrade.mobilepro.news.presentation.dynamic

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicWidget
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.news.api.dto.NewsWidgetBriefingDto
import com.etrade.mobilepro.news.briefing.api.BriefingNewsRepo
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

class NewsWidgetBriefingViewFactory @Inject constructor(
    private val widgetNewsRepo: BriefingNewsRepo,
    private val statusRepo: ReadStatusRepo,
    private val router: NewsRouter
) : DynamicUiViewFactory {
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is NewsWidgetBriefingDto) {
            return object : GenericLayout, DynamicWidget {
                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is NewsWidgetBriefingViewModel) {
                        viewModel.fetchNews()
                    }
                }

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    NewsWidgetBriefingViewModel(widgetNewsRepo, statusRepo, router)
                }

                override val viewModelClass = NewsWidgetBriefingViewModel::class.java

                override val type = DynamicWidget.Type.NEWS_BRIEFING

                override val uniqueIdentifier: String = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("NewsWidgetPortfolioViewFactory registered to unknown type")
        }
    }
}
