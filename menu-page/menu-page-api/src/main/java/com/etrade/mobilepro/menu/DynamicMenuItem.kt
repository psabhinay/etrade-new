package com.etrade.mobilepro.menu.repo

class DynamicMenuItem(
    val ctaDeepLink: String? = null,

    val icon: String? = null,

    val label: String = "",

    val menuItemType: MenuItemType = MenuItemType.MenuItemView,

    val subLabel: String? = null,

    var localyticsTag: String? = null
)

enum class MenuItemType {
    Divider,
    MenuItemView,
    MenuItemViewWithArrow,
    MenuItemViewWithArrowAndSubtitle,
    MenuItemViewWithArrowAndSubtitle_Bg_Purple,
    MenuItemViewWithCount,
    MenuItemViewAlertCount
}
