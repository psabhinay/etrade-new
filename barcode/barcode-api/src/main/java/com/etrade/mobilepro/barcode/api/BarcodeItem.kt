package com.etrade.mobilepro.barcode.api

data class BarcodeItem(
    val id: String = "",
    val company: String,
    val product: String,
    val symbol: String,
    val upc: String,
    val timeInMilliseconds: Long = System.currentTimeMillis(),
    var isDeleteButtonVisible: Boolean = false
)
