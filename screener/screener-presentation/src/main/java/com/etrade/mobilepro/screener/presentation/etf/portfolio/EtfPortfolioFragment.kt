package com.etrade.mobilepro.screener.presentation.etf.portfolio

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.screener.api.Country
import com.etrade.mobilepro.screener.api.Industry
import com.etrade.mobilepro.screener.api.IndustryType
import com.etrade.mobilepro.screener.api.Percent
import com.etrade.mobilepro.screener.api.Region
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.etf.EtfPortfolioFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.FragmentEtfPortfolioBinding
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentEtfPortfolioCountryExposureBinding
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentEtfPortfolioIndustryExposureBinding
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentEtfPortfolioRegionalExposureBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

private const val REGION_TAG = "Region tag"
private const val COUNTRY_TAG = "Country tag"
private const val REGION_PERCENT_TAG = "Region percent tag"
private const val COUNTRY_PERCENT_TAG = "Country percent tag"
private const val INDUSTRY_TYPE_TAG = "Industry type tag"
private const val INDUSTRY_TAG = "Industry tag"
private const val INDUSTRY_PERCENT_TYPE_TAG = "Industry type percent tag"

class EtfPortfolioFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<EtfPortfolioFilter, EtfPortfolioViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {
    override val layoutRes: Int = R.layout.fragment_etf_portfolio
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.ETF
    private val binding by viewBinding(FragmentEtfPortfolioBinding::bind)
    private val regionExposure by viewBinding(PartialFragmentEtfPortfolioRegionalExposureBinding::bind)
    private val countryExposure by viewBinding(PartialFragmentEtfPortfolioCountryExposureBinding::bind)
    private val industryExposure by viewBinding(PartialFragmentEtfPortfolioIndustryExposureBinding::bind)
    override val viewModel by viewModels<EtfPortfolioViewModel> { viewModelFactory }
    private lateinit var regionSelectorDropDown: DropDownMultiSelector<Region>
    private lateinit var countrySelectorDropDown: DropDownMultiSelector<Country>
    private lateinit var industryTypeSelectorDropDown: DropDownMultiSelector<IndustryType>
    private lateinit var regionPercentSelectorDropDown: DropDownMultiSelector<Percent>
    private lateinit var countryPercentSelectorDropDown: DropDownMultiSelector<Percent>
    private lateinit var industryTypePercentSelectorDropDown: DropDownMultiSelector<Percent>
    private lateinit var industrySelectorDropDown: DropDownMultiSelector<Industry>
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<EtfPortfolioFilter, EtfPortfolioViewModel.ViewState, Boolean>>
        get() = mapOf(
            binding.lessThanOneHundredM to EtfTotalNetAsset.LESS_THAN_100M,
            binding.oneHundredToFiveHundreds to EtfTotalNetAsset.ONE_HUNDRED_M_TO_500M,
            binding.fiveHundredsMToOneB to EtfTotalNetAsset.FIVE_HUNDRED_M_TO_1B,
            binding.oneBToTwoB to EtfTotalNetAsset.ONE_B_TO_2B,
            binding.greaterThanTwoB to EtfTotalNetAsset.GREATER_THAN_2B,
            binding.bottomTwentyPercent to EtfYield.BOTTOM_20,
            binding.belowTwentyPercent to EtfYield.BELOW_MID_20,
            binding.midTwentyPercent to EtfYield.MID_20,
            binding.aboveTwentyPercent to EtfYield.ABOVE_MID_20,
            binding.topTwentyPercent to EtfYield.TOP_20
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindFragment()
    }

    private fun setupFragment() {
        setupRegionSelectors()
        setupCountrySelectors()
        setupIndustryTypeSelectors()
        setupClickListeners()
    }

    private fun bindFragment() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                updateDropDownFieldsBlock(viewState = it)
                updateTotalNetAssetsBlock(viewState = it)
                updateYieldBlock(viewState = it)
            }
        )
    }

    private fun setupClickListeners() {
        with(regionExposure) {
            regionSelector.setOnClickListener {
                regionSelectorDropDown.open()
            }
            regionPercentValue.setOnClickListener {
                regionPercentSelectorDropDown.open()
            }
        }
        countryExposure.countrySelector.setOnClickListener {
            countrySelectorDropDown.open()
        }
        countryExposure.countryPercentValue.setOnClickListener {
            countryPercentSelectorDropDown.open()
        }
        with(industryExposure) {
            typeSelector.setOnClickListener {
                industryTypeSelectorDropDown.open()
            }
            industrySelector.setOnClickListener {
                industrySelectorDropDown.open()
            }
            industryPercentValue.setOnClickListener {
                industryTypePercentSelectorDropDown.open()
            }
        }
    }

    private fun setupRegionSelectors() {
        regionSelectorDropDown = DefaultDropDownMultiSelector(
            parentFragmentManager,
            DropDownMultiSelector.Settings(
                tag = REGION_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.region),
                itemsData = viewModel.regionsDropDownListData,
                selectListener = { _, region ->
                    viewModel.regionSelected(region.value)
                }
            )
        )
        regionPercentSelectorDropDown = DefaultDropDownMultiSelector(
            parentFragmentManager,
            DropDownMultiSelector.Settings(
                tag = REGION_PERCENT_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.percent_invested_hint),
                itemsData = viewModel.regionPercentDropDownListData,
                selectListener = { _, percent ->
                    viewModel.regionPercentSelected(percent.value)
                }
            )
        )
    }

    private fun setupCountrySelectors() {
        countrySelectorDropDown = DefaultDropDownMultiSelector(
            parentFragmentManager,
            DropDownMultiSelector.Settings(
                tag = COUNTRY_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.country),
                itemsData = viewModel.countriesDropDownListData,
                selectListener = { _, country ->
                    viewModel.countrySelected(country.value)
                }
            )
        )
        countryPercentSelectorDropDown = DefaultDropDownMultiSelector(
            parentFragmentManager,
            DropDownMultiSelector.Settings(
                tag = COUNTRY_PERCENT_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.percent_invested_hint),
                itemsData = viewModel.countryPercentDropDownListData,
                selectListener = { _, percent ->
                    viewModel.countryPercentSelected(percent.value)
                }
            )
        )
    }

    @Suppress("LongMethod") // nothing special
    private fun setupIndustryTypeSelectors() {
        industryTypeSelectorDropDown = DefaultDropDownMultiSelector(
            parentFragmentManager,
            DropDownMultiSelector.Settings(
                tag = INDUSTRY_TYPE_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.type),
                itemsData = viewModel.industryTypeDropDownListData,
                selectListener = { _, industryType ->
                    viewModel.industryTypeSelected(industryType.value)
                }
            )
        )
        industryTypePercentSelectorDropDown = DefaultDropDownMultiSelector(
            parentFragmentManager,
            DropDownMultiSelector.Settings(
                tag = INDUSTRY_PERCENT_TYPE_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.percent_invested_hint),
                itemsData = viewModel.industryTypePercentDropDownListData,
                selectListener = { _, percent ->
                    viewModel.industryTypePercentSelected(percent.value)
                }
            )
        )
        industrySelectorDropDown = DefaultDropDownMultiSelector(
            parentFragmentManager,
            DropDownMultiSelector.Settings(
                tag = INDUSTRY_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.type),
                itemsData = viewModel.industryDropDownListData,
                selectListener = { _, industry ->
                    viewModel.industrySelected(industry.value)
                }
            )
        )
    }

    private fun updateDropDownFieldsBlock(viewState: EtfPortfolioViewModel.ViewState) {
        with(regionExposure) {
            regionSelector.selectedValue = viewState.filter.region.title
            regionPercentValue.text = viewState.filter.regionPercentInvested.title
            regionPercentValue.isEnabled = viewState.regionPercentEnabled()
        }
        with(countryExposure) {
            countrySelector.selectedValue = viewState.filter.country.title
            countryPercentValue.text = viewState.filter.countryPercentInvested.title
            countryPercentValue.isEnabled = viewState.countryPercentEnabled()
        }
        with(industryExposure) {
            typeSelector.selectedValue = viewState.filter.industryType.title
            industrySelector.selectedValue = viewState.filter.industry.title
            industrySelector.selectionEnabled = viewState.industryEnabled()
            industryPercentValue.text = viewState.filter.industryTypePercentInvested.title
            industryPercentValue.isEnabled = viewState.industryTypePercentEnabled()
        }
    }

    private fun updateTotalNetAssetsBlock(viewState: EtfPortfolioViewModel.ViewState) = viewState.filter.apply {
        binding.apply {
            lessThanOneHundredM.isChecked = netAssetsLessThan100
            oneHundredToFiveHundreds.isChecked = netAssets100to500
            fiveHundredsMToOneB.isChecked = netAssets500to1
            oneBToTwoB.isChecked = netAssets1to2
            greaterThanTwoB.isChecked = netAssetsGreaterThan2
        }
    }

    private fun updateYieldBlock(viewState: EtfPortfolioViewModel.ViewState) = viewState.filter.apply {
        binding.apply {
            bottomTwentyPercent.isChecked = yieldBottom20
            belowTwentyPercent.isChecked = yieldBelowMid20
            midTwentyPercent.isChecked = yieldMid20
            aboveTwentyPercent.isChecked = yieldAboveMid20
            topTwentyPercent.isChecked = yieldTop20
        }
    }
}
