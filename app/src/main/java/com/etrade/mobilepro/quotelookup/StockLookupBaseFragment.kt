package com.etrade.mobilepro.quotelookup

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.R
import com.etrade.mobilepro.databinding.FragmentQuoteLookupBinding
import com.etrade.mobilepro.home.FRAGMENT
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.home.QUOTE_SHEET
import com.etrade.mobilepro.quote.search.history.QuoteSearchHistory
import com.etrade.mobilepro.quote.search.history.databinding.QuoteViewRecentSymbolsBinding
import com.etrade.mobilepro.quote.search.presentation.OnSearchResultItemClickListener
import com.etrade.mobilepro.quote.search.presentation.SearchResultsAdapter
import com.etrade.mobilepro.searchingapi.items.EXTRA_QUOTE_TICKER
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.NetworkState
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.android.textutil.TextUtil
import com.etrade.mobilepro.walkthrough.api.SymbolLookupAction
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingAction
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class StockLookupBaseFragment : Fragment(R.layout.fragment_quote_lookup) {

    protected val binding by viewBinding(FragmentQuoteLookupBinding::bind)

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var snackbarUtilFactory: SnackbarUtilFactory

    protected val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    @Inject
    lateinit var walkthroughStatesViewModel: WalkthroughStatesViewModel

    protected val viewModel: SearchViewModel by activityViewModels { viewModelFactory }

    protected var errorSnackbar: Snackbar? = null

    private var updateViews: Boolean = true

    private val quoteViewRecentSymbolsBinding: QuoteViewRecentSymbolsBinding
        get() = binding.searchResultContainer

    protected val genericListener: OnSearchResultItemClickListener =
        object : OnSearchResultItemClickListener {
            override fun onSearchResultItemClicked(item: SearchResultItem) {
                (item as? SearchResultItem.Symbol)
                    ?.also { navigateToNextPage(it) }
            }
        }

    abstract fun navigateToNextPage(symbol: SearchResultItem.Symbol)

    abstract val resultsDataSource: LiveData<List<SearchResultItem>?>

    @StringRes
    abstract fun getDisclosureResId(): Int

    private val searchResultsAdapter = SearchResultsAdapter(symbolSelectListener = genericListener)

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        quoteViewRecentSymbolsBinding.apply {
            quoteSearchHistory = object : QuoteSearchHistory {
                override val items: LiveData<List<SearchResultItem>?> = resultsDataSource
            }
            onSearchResultItemClickListener = genericListener
        }
        setupDisclosure()
        setupRecyclerView()
        bindViewModel()
    }

    private fun setupDisclosure() {
        val text = getString(getDisclosureResId())
        val formattedText = TextUtil.convertTextWithHmlTagsToSpannable(text)
        binding.initialDisclosure.root.text = formattedText
    }

    private fun setupRecyclerView() {
        quoteViewRecentSymbolsBinding.recentSymbolsItemsView.run {
            setupRecyclerViewDivider(R.drawable.thin_divider)
            // there's a crash that happens if results recycler view is updating/in animation and we finish the activity
            itemAnimator = null
            adapter = searchResultsAdapter
        }
    }

    private fun bindViewModel() {
        viewModel.headerTitleState.observe(
            viewLifecycleOwner,
            Observer {
                val recentSymbolsSectionTitleView =
                    quoteViewRecentSymbolsBinding.recentSymbolsSectionTitleView
                when (it) {
                    is HeaderTitleState.Hidden ->
                        recentSymbolsSectionTitleView.visibility =
                            View.GONE
                    is HeaderTitleState.Visible -> {
                        recentSymbolsSectionTitleView.apply {
                            visibility = View.VISIBLE
                            text = getString(it.resId)
                        }
                    }
                }
            }
        )
        viewModel.networkState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is NetworkState.Success -> {
                        binding.searchResultContainer.root.visibility = View.VISIBLE
                        binding.optionsContainer.visibility = View.GONE
                        binding.searchLoadingProgressBar.hide()
                        errorSnackbar?.dismiss()
                    }
                    is NetworkState.Loading -> {
                        binding.searchLoadingProgressBar.show()
                        errorSnackbar?.dismiss()
                    }
                    is NetworkState.Failed -> {
                        binding.searchLoadingProgressBar.hide()
                        errorSnackbar = snackbarUtil.snackbar(getString(R.string.error_message_general))?.apply { show() }
                    }
                }
            }
        )
        resultsDataSource.observe(
            viewLifecycleOwner,
            Observer { results ->
                results?.let {
                    if (it.size == 1 && viewModel.openQuote) {
                        val ticker = it[0] as SearchResultItem.Symbol
                        navigateToNextPage(ticker)
                        updateViews = false
                    } else {
                        if (updateViews) {
                            toggleEmptyState(it.isEmpty())
                            searchResultsAdapter.updateData(it)
                        } else {
                            updateViews = true
                        }
                    }
                }
            }
        )
    }

    private fun toggleEmptyState(empty: Boolean) {
        val recentSymbolsItemsView = quoteViewRecentSymbolsBinding.recentSymbolsItemsView
        val recentSymbolsSectionTitleView =
            quoteViewRecentSymbolsBinding.recentSymbolsSectionTitleView

        if (empty && viewModel.networkState.value != NetworkState.Loading) {
            binding.noResults.visibility = View.VISIBLE
            binding.initialDisclosure.root.visibility = View.VISIBLE
            recentSymbolsItemsView.visibility = View.GONE
            if (viewModel.queryStr.isNullOrBlank()) {
                binding.noResults.text = getString(R.string.no_symbol_or_company_result)
            } else {
                binding.noResults.text = getString(R.string.no_search_results, viewModel.queryStr)
            }
            recentSymbolsSectionTitleView.visibility = View.GONE
        } else {
            binding.noResults.visibility = View.GONE
            binding.initialDisclosure.root.visibility = View.GONE
            recentSymbolsItemsView.visibility = View.VISIBLE
            if (viewModel.queryStr.isNullOrBlank()) {
                recentSymbolsSectionTitleView.visibility = View.VISIBLE
            } else {
                recentSymbolsSectionTitleView.visibility = View.GONE
            }
        }
    }

    protected open fun finishOnSearch(): Boolean = activity?.intent?.getBooleanExtra(EXTRA_FLAG_FINISH_ON_SEARCH, false) == true

    protected fun dismissOrGoToQuote(symbol: SearchResultItem.Symbol) {
        if (finishOnSearch()) {
            (activity as? QuoteLookupActivity)?.dismiss(symbol)
        } else {
            walkthroughStatesViewModel.onAction(SymbolTrackingAction.SymbolSelected)
            walkthroughStatesViewModel.onAction(SymbolLookupAction.SymbolSelected)
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(FRAGMENT, QUOTE_SHEET)
            intent.putExtra(EXTRA_QUOTE_TICKER, symbol)
            intent.flags = (Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            activity?.finish()
        }
    }
}
