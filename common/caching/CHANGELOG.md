# 0.0.3

    shouldPutToCache method was added to SimpleCachedResource.Controller and CachedResource.Controller
    This method allows to check if network response is valid before putting it to cache

# 0.0.2

    Resource class moved outside
    SimpleCachedResource class was added
    
# 0.0.1
    
    Resource and CachedResource classes were added
