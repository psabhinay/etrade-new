package com.etrade.mobilepro.util.android.textutil

import android.text.Spannable
import android.text.style.RelativeSizeSpan
import com.etrade.mobilepro.instrument.InstrumentType
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

private const val TICKER = "SPX"
private const val DESCRIPTION = "SPX Dec 17 '21 $4000 Put"
private const val DESCRIPTION_WITH_TRAILING_CHARACTER = "SPX Dec 17 '21 $4000 Putw"
private const val OUTPUT_LONG = "SPX \nDec 17 '21 \n$4000 Put "
private const val OPTION_DATA = "12/17/21 $4000P"

@RunWith(RobolectricTestRunner::class)
class SymbolTextViewSpansKtTest {

    @Test
    fun prepareSymbolSpan_whenNotOption_returnsTicker() {
        val result = prepareSymbolSpan(
            TICKER,
            InstrumentType.INDX,
            DESCRIPTION,
        )
        assertEquals(TICKER, result)
    }

    @Test
    fun prepareSymbolSpan_whenHasUnwantedTrailingCharacters_trimsAppropriately() {
        val result = prepareSymbolSpan(
            TICKER,
            InstrumentType.OPTN,
            DESCRIPTION_WITH_TRAILING_CHARACTER,
        )
        assertEquals(OUTPUT_LONG, result.toString())
    }

    @Test
    fun prepareSymbolSpan_whenRegularFormat_returnsCorrectSpannedString() {
        val result = prepareSymbolSpan(
            TICKER,
            InstrumentType.OPTN,
            DESCRIPTION,
        )
        assertEquals(OUTPUT_LONG, result.toString())
        assertTrue(result is Spannable)
        val spannable = result as Spannable
        val spans = spannable.getSpans(0, result.length, Any::class.java)
        assertEquals(1, spans.size)
        val relativeSizeSpan = spans[0]
        assertTrue(relativeSizeSpan is RelativeSizeSpan)
        val expectedStart = OUTPUT_LONG.indexOf(' ')
        val expectedEnd = OUTPUT_LONG.length
        assertEquals(expectedStart, spannable.getSpanStart(relativeSizeSpan))
        assertEquals(expectedEnd, spannable.getSpanEnd(relativeSizeSpan))
    }

    @Test
    fun prepareWidgetTicker_whenNotOption_returnsTicker() {
        val result = prepareWidgetTicker(
            TICKER,
            InstrumentType.INDX,
            DESCRIPTION,
        )
        assertEquals(TICKER to null, result)
    }

    @Test
    fun prepareWidgetTicker_whenShortFormat_formatsDateCorrectly() {
        val result = prepareWidgetTicker(
            TICKER,
            InstrumentType.OPTN,
            DESCRIPTION,
        )
        assertEquals(TICKER to OPTION_DATA, result)
    }

    @Test
    fun extractUnderlyingOptionSymbolFromDescriptionTest() {
        val result = extractUnderlyingSymbol(DESCRIPTION_WITH_TRAILING_CHARACTER, InstrumentType.OPTN)
        assertEquals(TICKER, result)
    }
}
