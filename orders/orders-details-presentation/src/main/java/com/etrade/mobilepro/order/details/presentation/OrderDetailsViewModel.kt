package com.etrade.mobilepro.order.details.presentation

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.getOrDefault
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.companyoverview.CompanyOverviewProvider
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dynamic.form.api.Action
import com.etrade.mobilepro.eventtrackerapi.AppsFlyerRepo
import com.etrade.mobilepro.livedata.combineWith
import com.etrade.mobilepro.livedata.concatCombine
import com.etrade.mobilepro.marketschedule.MarketScheduleProviderHolder
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderData
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.order.details.router.PreviewOrderDetailsArgument
import com.etrade.mobilepro.order.details.router.WithSnackBarMessage
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.presentation.OrderButtonVisibilityManager
import com.etrade.mobilepro.orders.presentation.isOrderSupported
import com.etrade.mobilepro.quote.mapper.MobileQuoteWidgetItemMapper
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.EVENT_TRADE_PLACED
import com.etrade.mobilepro.tracking.params.TradeEventAnalyticsParams
import com.etrade.mobilepro.trade.accountupgrade.util.getApprovalLevelUpgradeUrl
import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.OrderResponse
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsTradeLeg
import com.etrade.mobilepro.trade.api.ticker
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.optionlegselect.TradeLegBidAskViewModel
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import com.etrade.mobilepro.viewdelegate.LifecycleObserverViewDelegate
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.rx2.awaitLast
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.BigDecimal

@Suppress("LongParameterList")
class OrderDetailsViewModel @AssistedInject constructor(
    private val daoProvider: AccountDaoProvider,
    private val cacheDaoProvider: CachedDaoProvider,
    private val quoteService: MobileQuoteRepo,
    private val quoteMapper: MobileQuoteWidgetItemMapper,
    private val marketScheduleProviderHolder: MarketScheduleProviderHolder,
    @Web private val baseUrl: String,
    private val dispatcherProvider: DispatcherProvider,
    private val user: User,
    private val companyOverviewProvider: CompanyOverviewProvider,
    private val resources: Resources,
    private val tracker: Tracker,
    private val appsFlyerRepo: AppsFlyerRepo,
    @Assisted private val delegate: OrderDetailsDelegate
) : ViewModel() {

    @AssistedFactory
    interface Factory {
        fun create(delegate: OrderDetailsDelegate): OrderDetailsViewModel
    }

    val dialog: LiveData<AppDialog?>
        get() = mutableDialog
    val items: LiveData<List<OrderDetailItemInterface>>
        get() = _items
    val errorMessage: LiveData<ConsumableLiveEvent<ErrorMessage>>
        get() = _errorMessage
    val optionsApprovalUpgradeUrl: LiveData<ConsumableLiveEvent<String>>
        get() = mutableOptionsApprovalUpgradeUrl
    val withSnackBarMessageSignal: LiveData<ConsumableLiveEvent<WithSnackBarMessage>>
        get() = _withSnackBarMessageSignal
    val orderDeletedSignal: LiveData<ConsumableLiveEvent<String>>
        get() = mutableOrderDeletedSignal
    val orderConfirmedSignal: LiveData<ConsumableLiveEvent<String>>
        get() = mutableOrderConfirmedSignal
    val orderCanceledSignal: LiveData<ConsumableLiveEvent<String>>
        get() = mutableOrderCanceledSignal
    val openTradeSignal: LiveData<ConsumableLiveEvent<OrderData>>
        get() = mutableOpenTradeSignal
    val openPreviewSignal: LiveData<ConsumableLiveEvent<PreviewOrderDetailsArgument>>
        get() = mutableOpenPreviewSignal
    val openShowAffiliatedDialog: LiveData<Boolean?>
        get() = mutableShowAffiliatedDialogSignal

    val loadingIndicator: LiveData<Boolean>
        get() = _loadingIndicator

    val lifecycleObserverViewDelegate = LifecycleObserverViewDelegate()
    lateinit var viewModelProvider: ViewModelProvider

    var orderDetailsItem: BaseOrderDetailsItem? = null

    private val mutableDialog: MutableLiveData<AppDialog?> = MutableLiveData()
    private val mutableOptionsApprovalUpgradeUrl: MutableLiveData<ConsumableLiveEvent<String>> = MutableLiveData()
    private val mutableOrderDeletedSignal: MutableLiveData<ConsumableLiveEvent<String>> = MutableLiveData()
    private val mutableOrderConfirmedSignal: MutableLiveData<ConsumableLiveEvent<String>> = MutableLiveData()
    private val mutableOrderCanceledSignal: MutableLiveData<ConsumableLiveEvent<String>> = MutableLiveData()
    private val mutableOpenTradeSignal: MutableLiveData<ConsumableLiveEvent<OrderData>> = MutableLiveData()
    private val mutableOpenPreviewSignal: MutableLiveData<ConsumableLiveEvent<PreviewOrderDetailsArgument>> = MutableLiveData()
    private val mutableShowAffiliatedDialogSignal: MutableLiveData<Boolean?> = MutableLiveData()

    private val logger: Logger = LoggerFactory.getLogger(javaClass)

    private val refreshLoadingIndicator: MutableLiveData<Boolean> = MutableLiveData()
    private val orderDetailsArgument: MutableLiveData<OrderDetailsArgument> = MutableLiveData()

    private val buttonVisibilityManager: LiveData<OrderButtonVisibilityManager> = orderDetailsArgument.map { argument ->
        delegate.createButtonVisibilityManager(argument)
    }
    private val orderDetailsItems: MutableLiveData<List<BaseOrderDetailsItem.OrderDetailsItem>> = MutableLiveData()
    private val actionsArgument: LiveData<ActionsArgument> = orderDetailsArgument.combineWith(orderDetailsItems) { argument, orderDetails ->
        ActionsArgument(argument, orderDetails)
    }

    private val _items: MutableLiveData<List<OrderDetailItemInterface>> = MutableLiveData()
    private val _errorMessage: MutableLiveData<ConsumableLiveEvent<ErrorMessage>> = MutableLiveData()
    private val _withSnackBarMessageSignal: MutableLiveData<ConsumableLiveEvent<WithSnackBarMessage>> = MutableLiveData()
    private var _loadingIndicator: LiveData<Boolean> = refreshLoadingIndicator

    val actions: LiveData<List<Action>> = actionsArgument.concatCombine(buttonVisibilityManager) { argument, manager ->
        if (manager.isOrderSupported) {
            delegate.createActions(argument, manager)
        } else {
            emptyList()
        }
    }

    val noActionsMessage: LiveData<CharSequence?> = orderDetailsArgument.concatCombine(actions) { argument, actions ->
        delegate.noActionsMessage.takeIf { argument.orderStatus != OrderStatus.CANCELLED && actions.isEmpty() }
    }

    init {
        cacheDaoProvider.open()
    }

    override fun onCleared() {
        cacheDaoProvider.close()
        delegate.detach()
        lifecycleObserverViewDelegate.dispose()
    }

    fun resetActionStates() = delegate.resetActionStates()

    fun fetch(argument: OrderDetailsArgument) {
        resetDelegate()
        if (!argument.isSame(orderDetailsArgument.value)) {
            orderDetailsArgument.value = argument
            refresh()
        }
    }

    fun refresh() {
        var argument = orderDetailsArgument.value ?: return

        viewModelScope.launch {
            refreshLoadingIndicator.value = true

            val currentDelegate = delegate
            val quotesDiffered = async(start = CoroutineStart.LAZY) { getQuotes(argument.symbols).getOrDefault(emptyMap()) }
            val itemsDeferred = async { currentDelegate.loadItems(argument, quotesDiffered) }

            itemsDeferred.await()
                .onSuccess { details ->
                    argument = argument.clone(details.actualOrderStatus)
                    orderDetailsItems.value = details.orderDetailsItems
                    orderDetailsArgument.value = argument

                    _items.value = details.items
                    orderDetailsItem = details.detailsItem
                    setArgumentWithSnackBarMessage(argument)
                }
                .onFailure {
                    handleError(it, ErrorMessage(retryAction = ::refresh))
                    quotesDiffered.cancel()
                }

            refreshLoadingIndicator.value = false
        }
    }

    fun showAffiliatedDialog() {
        mutableShowAffiliatedDialogSignal.value = true
    }

    fun closeAffiliatedDialog() {
        mutableShowAffiliatedDialogSignal.value = false
    }

    private fun resetDelegate() {
        delegate.detach()
        delegate.attachTo(this)

        resetLoadingIndicators()
    }

    private fun resetLoadingIndicators() {
        _loadingIndicator = refreshLoadingIndicator
        delegate.loadingIndicators.forEach(::contributeLoadingIndicator)
    }

    private fun contributeLoadingIndicator(indicator: LiveData<Boolean>) {
        _loadingIndicator = _loadingIndicator.concatCombine(indicator) { loading1, loading2 -> loading1 || loading2 }
    }

    private fun onOptionsApprovalUpgrade(accountId: String) {
        viewModelScope.launch {
            val account = daoProvider.getAccountDao().getAccountWithAccountId(accountId)
            val isIra = account?.isIra ?: false
            val url = "$baseUrl${getApprovalLevelUpgradeUrl(isIra)}"
            mutableOptionsApprovalUpgradeUrl.value = url.consumable()
        }
    }

    private fun <T> executeOperation(
        loadingIndicator: MutableLiveData<Boolean>,
        operation: suspend () -> ETResult<T>,
        onSuccess: (T) -> Unit,
        interceptFailure: (Throwable) -> Boolean
    ) {
        if (loadingIndicator.value == true) {
            return
        }

        viewModelScope.launch {
            loadingIndicator.value = true

            operation()
                .onSuccess(onSuccess)
                .onFailure {
                    if (!interceptFailure(it)) {
                        handleError(
                            it,
                            ErrorMessage {
                                executeOperation(loadingIndicator, operation, onSuccess, interceptFailure)
                            }
                        )
                    }
                }

            loadingIndicator.value = false
        }
    }

    private fun handleError(t: Throwable, message: ErrorMessage) {
        logger.error(message.message.toString(), t)
        postError(message)
    }

    private fun postError(message: ErrorMessage) {
        _errorMessage.value = ConsumableLiveEvent(message)
    }

    private fun SecurityType.getLabel(): String {
        return resources.getString(
            when (this) {
                SecurityType.CONDITIONAL -> R.string.trade_security_type_conditional
                SecurityType.STOCK -> R.string.trade_security_type_stock
                SecurityType.OPTION -> R.string.trade_security_type_options
                SecurityType.MUTUAL_FUND -> R.string.trade_security_type_mutual_fund
                SecurityType.STOCK_AND_OPTIONS -> R.string.trade_security_type_stock_and_options
            }
        )
    }

    private fun Order.getSecurityTypeLabel(securityType: SecurityType?): String {
        val responseLeg = legs.first()
        return if (responseLeg is MutualFundsTradeLeg &&
            responseLeg.instrumentType.isMoneyMarketFund
        ) {
            resources.getString(R.string.money_market_fund)
        } else {
            requireNotNull(securityType?.getLabel())
        }
    }

    private fun trackTradePlaced(response: Order, securityType: SecurityType?) {
        val aatId = user.session?.aatId
        if (aatId.isNullOrEmpty()) {
            return
        }

        viewModelScope.launch(dispatcherProvider.Main) {
            val symbol = response.ticker
            val securityTypeLabel = response.getSecurityTypeLabel(securityType)
            val eventParams = TradeEventAnalyticsParams(
                symbol = symbol,
                securityType = securityTypeLabel,
                sector = companyOverviewProvider.companySector(symbol),
                orderType = response.legs.joinToString(",") { resources.getString(it.action.getLabel()) },
                priceType = response.priceType?.getLabel()?.let(resources::getString),
                quantity = response.legs.fold(BigDecimal.ZERO) { acc, leg ->
                    acc + (leg.quantity ?: BigDecimal.ZERO)
                }.toPlainString(),
                customerId = aatId,
                name = EVENT_TRADE_PLACED
            )
            tracker.event(eventParams)
            trackAppsFlyerEvent(response, securityTypeLabel)
        }
    }

    private suspend fun trackAppsFlyerEvent(order: Order, securityTypeLabel: String) {
        val accountEnding = order.account.substringAfterLast("-")
        val account = daoProvider.getAccountDao().getAllAccountsNonAsync().firstOrNull {
            it.accountId.endsWith(accountEnding)
        }
        val accountType = if (account == null) {
            AccountType.Unknown
        } else {
            if (account.isIra == true) {
                AccountMode.IRA
            } else {
                AccountType.Brokerage
            }
        }

        appsFlyerRepo.trackEvent(
            "trade",
            mapOf("SecurityType" to securityTypeLabel, "AccountType" to accountType.name)
        )
    }

    private suspend fun getQuotes(symbols: List<String>): ETResult<Map<String, OrderDetailItemInterface>> {
        return quoteService.loadMobileQuotes(symbols = symbols.joinToString(","), extendedHours = marketScheduleProviderHolder.isExtendedHourEligiblePeriod)
            .runCatchingET { awaitLast() }
            .map { quotes ->
                quoteMapper.map(quotes, showDayChange = true)
            }
    }

    private fun setArgumentWithSnackBarMessage(argument: OrderDetailsArgument) {
        if (argument is WithSnackBarMessage &&
            !argument.snackBarMessage.isNullOrBlank()
        ) {
            _withSnackBarMessageSignal.value = ConsumableLiveEvent(argument)
        }
    }

    class OrderDetails(
        val items: List<OrderDetailItemInterface>,
        val actualOrderStatus: OrderStatus,
        val detailsItem: BaseOrderDetailsItem? = null,
        val orderDetailsItems: List<BaseOrderDetailsItem.OrderDetailsItem>? = null
    )

    data class ActionsArgument(
        val orderDetailsArgument: OrderDetailsArgument,
        val orderDetailsItems: List<BaseOrderDetailsItem.OrderDetailsItem>? = null
    )

    interface OrderDetailsDelegate {

        val noActionsMessage: CharSequence?
        val loadingIndicators: Set<LiveData<Boolean>>

        suspend fun loadItems(argument: OrderDetailsArgument, quotes: Deferred<Map<String, OrderDetailItemInterface>>): ETResult<OrderDetails>

        fun attachTo(viewModel: OrderDetailsViewModel)
        fun detach()

        fun createActions(argument: ActionsArgument, manager: OrderButtonVisibilityManager): List<Action>
        fun createButtonVisibilityManager(argument: OrderDetailsArgument): OrderButtonVisibilityManager
        fun resetActionStates()
    }

    abstract class BaseOrderDetailsDelegate : OrderDetailsDelegate {

        override val noActionsMessage: CharSequence? = null

        protected val mutableDialog: MutableLiveData<AppDialog?>
            get() = viewModel.mutableDialog

        protected val onOptionsApprovalUpgrade: (String) -> Unit = { accountId ->
            viewModel.onOptionsApprovalUpgrade(accountId)
        }

        private var orderDetailsViewModel: OrderDetailsViewModel? = null

        protected val viewModel: OrderDetailsViewModel
            get() = requireNotNull(orderDetailsViewModel) {
                "OrderDetailsViewModel is not attached to this delegate."
            }

        final override fun attachTo(viewModel: OrderDetailsViewModel) {
            orderDetailsViewModel = viewModel
        }

        final override fun detach() {
            orderDetailsViewModel = null
        }

        override fun resetActionStates() {
            orderDetailsViewModel?.mutableDialog?.value = null
        }

        protected fun getQuoteDetailsViewModels(orderPosition: Int = 0): Pair<QuotesWidgetViewModel, TradeLegBidAskViewModel> {
            val quoteWidgetViewModelClass = QuotesWidgetViewModel::class.java
            val tradeLegBidAskViewModelClass = TradeLegBidAskViewModel::class.java
            return with(viewModel.viewModelProvider) {
                if (orderPosition == 0) {
                    // To make it possible to reuse the same view models that may already exist in a ViewModelStore we use default key generation for the first
                    // order.
                    get(quoteWidgetViewModelClass) to get(tradeLegBidAskViewModelClass)
                } else {
                    fun <T> Class<T>.key(): String = "$canonicalName:$orderPosition"

                    get(quoteWidgetViewModelClass.key(), quoteWidgetViewModelClass) to get(tradeLegBidAskViewModelClass.key(), tradeLegBidAskViewModelClass)
                }
            }.also { (quoteWidgetViewModel, tradeLegBidAskViewModel) ->
                tradeLegBidAskViewModel.quoteWidgetViewModel = quoteWidgetViewModel
                viewModel.lifecycleObserverViewDelegate += quoteWidgetViewModel
            }
        }

        protected fun <T> executeOperation(
            loadingIndicator: MutableLiveData<Boolean>,
            operation: suspend () -> ETResult<T>,
            onSuccess: (T) -> Unit,
            interceptFailure: (Throwable) -> Boolean = { false }
        ) {
            viewModel.executeOperation(loadingIndicator, operation, onSuccess, interceptFailure)
        }

        protected fun postError(error: ErrorMessage) = viewModel.postError(error)

        protected fun refresh() = viewModel.refresh()

        protected fun sendOrderDeletedSignal(orderId: String) {
            viewModel.mutableOrderDeletedSignal.value = ConsumableLiveEvent(orderId)
        }

        protected fun sendOrderConfirmedSignal(orderId: String, response: OrderResponse, securityType: SecurityType?) {
            viewModel.trackTradePlaced(response.orders.first(), securityType)
            viewModel.mutableOrderConfirmedSignal.value = ConsumableLiveEvent(orderId)
        }

        protected fun sendOrderCanceledSignal(orderId: String) {
            viewModel.mutableOrderCanceledSignal.value = ConsumableLiveEvent(orderId)
        }

        protected fun openTradeSignal(orderData: OrderData) {
            viewModel.mutableOpenTradeSignal.value = ConsumableLiveEvent(orderData)
        }

        protected fun openPreviewSignal(argument: PreviewOrderDetailsArgument) {
            viewModel.mutableOpenPreviewSignal.value = ConsumableLiveEvent(argument)
        }
    }
}
