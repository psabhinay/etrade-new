package com.etrade.mobilepro.feedback.api

import com.etrade.mobilepro.common.result.ETResult

interface FeedbackRepository {
    suspend fun sendFeedback(feedbackRequest: FeedbackRequest): ETResult<FeedbackResponse>
}
