package com.etrade.mobilepro.completeview

import android.view.View
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.streaming.api.StreamingText
import com.etrade.mobilepro.util.formatCurrencyAmount

@BindingAdapter("formatAccountSummary")
fun View.setAccountSummaryContentDescription(streamingText: StreamingText?) {
    contentDescription =
        streamingText?.text?.formatCurrencyAmount(resources.getString(R.string.not_available_description))
}
