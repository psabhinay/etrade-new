package com.etrade.mobilepro.inboxmessages.dynamicui.api

enum class InboxMessagesWidgetPlace(val code: String) {
    COMPLETE_VIEW("C"), OVERVIEW("P")
}
