package com.etrade.mobilepro.trade.presentation.util

import android.os.Bundle
import com.etrade.mobilepro.dynamic.form.fragment.createFormValuesArgument
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.getTradeAction
import com.etrade.mobilepro.trade.form.api.toSecurityType
import com.squareup.moshi.Moshi

fun SearchResultItem.Symbol.toFormBundle(
    builder: TradeFormParametersBuilder,
    moshi: Moshi,
    selectedAccountId: String? = null,
    transactionType: TransactionType? = null
): Bundle {
    val formParams = builder
        .apply {
            symbol = this@toFormBundle
            accountId = selectedAccountId
            action = transactionType?.getTradeAction()
        }
        .create(instrumentType.toSecurityType())

    return createFormValuesArgument(moshi, formParams)
}
