package com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer

import android.app.Activity
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver

class KeyboardListener(
    private val activity: Activity,
    private val onKeyboardToggleAction: (shown: Boolean) -> Unit
) : ViewTreeObserver.OnGlobalLayoutListener {

    private val keyboardBottom by lazy {
        val bounds = Rect()
        activity.window.decorView.getWindowVisibleDisplayFrame(bounds)
        bounds.bottom
    }

    override fun onGlobalLayout() {
        onKeyboardToggleAction.invoke(activity.isKeyboardOpened())
    }

    fun scrollViewForKeyboardSize(view: View, isKeyboardShown: Boolean) {
        view.post {
            if (isKeyboardShown) {
                val bounds = Rect()
                view.findFocus()?.getGlobalVisibleRect(bounds)
                val bottom = bounds.bottom
                if (bottom > keyboardBottom) {
                    val scrollOffset = bottom - keyboardBottom
                    view.scrollBy(0, scrollOffset)
                }
            } else {
                view.scrollBy(0, -view.scrollY)
            }
        }
    }
}

fun Activity.isKeyboardOpened(): Boolean {
    val bounds = Rect()
    val activityRoot = (findViewById<ViewGroup>(android.R.id.content)).getChildAt(0)
    val visibleThreshold = resources.getDimension(R.dimen.keyboard_min_height).toInt()

    activityRoot.getWindowVisibleDisplayFrame(bounds)

    return activityRoot.rootView.height - bounds.bottom > visibleThreshold
}

fun Activity.getViewTreeObserver() = findViewById<View>(android.R.id.content)?.viewTreeObserver
