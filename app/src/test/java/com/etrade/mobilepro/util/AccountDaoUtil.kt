package com.etrade.mobilepro.util

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.account.dao.AccountDao
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.mockito.Mockito

private fun mockAccountDaoProvider(accountDao: AccountDao) = mock<AccountDaoProvider> {
    on { getAccountDao() } doReturn (accountDao)
}

private fun mockAccountDao(accounts: LiveData<List<Account>>) = mock<AccountDao> {
    on { getAllAccounts() } doReturn (accounts)
}

internal fun createMockDaoProvider(mockAccountsLiveData: LiveData<List<Account>>? = null): AccountDaoProvider {
    return mockAccountsLiveData?.let { mockAccountDaoProvider(mockAccountDao(it)) }
        ?: Mockito.mock(AccountDaoProvider::class.java)
}
