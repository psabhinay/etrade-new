package com.etrade.mobilepro.localytics

import android.app.Activity

interface LocalyticsHelper {
    fun isAppLauncherActivity(activity: Activity?): Boolean
}
