package com.etrade.mobilepro.trade.presentation.form.delegate.shared

import android.content.Context
import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.ExternalAction
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.InputBuilder
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.SelectInputBuilder
import com.etrade.mobilepro.dynamic.form.api.SwitchInput
import com.etrade.mobilepro.dynamic.form.api.TextInput
import com.etrade.mobilepro.dynamic.form.api.TextInputIcon
import com.etrade.mobilepro.dynamic.form.api.inputParameterValue
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.livedata.combine
import com.etrade.mobilepro.livedata.takeUntil
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.trade.api.TradeOptionInfoType
import com.etrade.mobilepro.trade.dropdown.form.tradeSelect
import com.etrade.mobilepro.trade.dropdown.help.TradeHelpParams
import com.etrade.mobilepro.trade.form.api.input.BaseTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithQuantity
import com.etrade.mobilepro.trade.form.api.input.extractPriceType
import com.etrade.mobilepro.trade.form.api.value.ConditionalPriceTypeValueId
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.form.util.getInputFractionalErrorDialog
import com.etrade.mobilepro.trade.presentation.usecase.TradeFormParameter
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.walkthrough.api.IdleState
import com.etrade.mobilepro.walkthrough.api.State
import com.etrade.mobilepro.walkthrough.api.TradePlacingState
import java.math.BigDecimal

private const val ALL_OR_NONE_QUANTITY = 300
private const val DEFAULT_STOCK_QUANTITY_FORM_VALUE = "1"

@Suppress("LongMethod")
internal fun <T> GroupBuilder.requiredStockFormInputs(
    context: Context,
    inputId: T,
    dependencies: FormFactoryDelegateDependencies,
    parameter: TradeFormParameter,
    tradingDefaultsPreferences: TradingDefaultsPreferences? = null
) where T : BaseTradeFormInputId, T : WithActionTradeFormInputId, T : WithQuantity {
    quoteWidget(inputId.symbol, parameter.quoteWidgetViewModel, R.layout.quote_view_with_retry)

    val resources = context.resources
    val textInputIcon = TextInputIcon(
        R.drawable.trade_ic_system_calculator,
        resources.getString(R.string.trade_content_description_calculator)
    ) {
        parameter.onActionClickListener.onActionClick(TradeFormAction.Calculator)
    }

    horizontalGroup {
        orderType(resources, inputId, parameter.walkthroughViewModel.state)
        val defaultQuantity = if (tradingDefaultsPreferences?.stocksQuantity.isNullOrEmpty()) {
            DEFAULT_STOCK_QUANTITY_FORM_VALUE
        } else {
            tradingDefaultsPreferences?.stocksQuantity
        }
        quantity(inputId, resources.getString(R.string.trade_hint_quantity), textInputIcon, defaultQuantity, parameter.walkthroughViewModel.state)
    }

    prices(
        resources,
        inputId,
        dependencies,
        parameter.walkthroughViewModel.state,
        parameter.editPriceType(inputId),
        tradingDefaultsPreferences?.stocksPriceType
    )
    term(
        resources,
        inputId,
        dependencies.limitOrderTermLabelMap,
        dependencies.allowShortSellUpdatedTerms,
        tradingDefaultsPreferences?.stocksTerm,
        parameter.walkthroughViewModel.state
    )
    allOrNoneSwitch(resources, inputId, dependencies.allOrNoneValidPriceTypes)
}

@Suppress("LongParameterList")
internal fun GroupBuilder.prices(
    resources: Resources,
    inputId: WithPriceTradeFormInputId,
    dependencies: FormFactoryDelegateDependencies,
    walkthroughState: LiveData<State>,
    editModePriceType: PriceType?,
    defaultPriceType: PriceType?
) {
    val valueId = inputId.priceTypeValueId
    horizontalGroup {
        priceType(resources, inputId, walkthroughState, editModePriceType, defaultPriceType = defaultPriceType)
        val priceTypePriceLabelMap = dependencies.priceTypePriceLabelMap
        price(inputId.limitPrice, inputId, priceTypePriceLabelMap, null, dependencies.isLimitPriceInputVisible)
        price(inputId.stopPrice, inputId, priceTypePriceLabelMap, null, dependencies.isStopPriceInputVisible)
    }
    stopLimitPrices(inputId, resources, valueId)
}

internal fun GroupBuilder.extendParamsWithListener(
    resources: Resources,
    prototype: SymbolInputParams,
    onActionClickListener: OnActionClickListener,
    @StringRes notEligibleSymbolDialogMessage: Int
): SymbolInputParams {

    return prototype.copy(
        onInvalidSymbol = {
            requireAnyInput(prototype.symbolInputId).resetValue()
            onActionClickListener.showNotEligibleSymbolDialog(resources, notEligibleSymbolDialogMessage)
        }
    )
}

internal fun GroupBuilder.createStockSymbolExternalAction(
    context: Context,
    params: SymbolInputParams,
    inputId: WithActionTradeFormInputId,
    searchIntentFactory: SearchIntentFactory,
    onActionClickListener: OnActionClickListener
): ExternalAction {

    return {
        val intent = searchIntentFactory.createIntent(
            context = context,
            params = StartSearchParams(
                query = SearchQuery(SearchType.SYMBOL),
                stockQuoteSearchOnly = params.isStockQuoteSearchOnly,
                finishOnSearch = true,
                showOwnedSymbols = params.showOwnedSymbols && inputParameterValue(inputId.action).value == inputId.actionValueId.sell,
                selectedAccountId = inputParameterValue(inputId.accountId).value as? String
            ),
            reorderToFront = false
        )
        onActionClickListener.onActionClick(TradeFormAction.SymbolLookup(intent, params.symbolInputId))
    }
}

internal fun <T> RuleListBuilder.requiredStockFormRules(
    resources: Resources,
    onActionClickListener: OnActionClickListener,
    inputId: T,
    quoteWidgetViewModel: QuotesWidgetViewModel
) where T : BaseTradeFormInputId, T : WithQuantity, T : WithActionTradeFormInputId {
    requiredCommonFormRules(inputId)
    resetQuantityOnOrderTypeChange(inputId)
    updateCalculatorVisibilityOnSymbolChange(inputId, quoteWidgetViewModel)
    observeQuantityChange(resources, onActionClickListener, inputId)
}

internal fun <T> RuleListBuilder.requiredCommonFormRules(
    inputId: T
) where T : BaseTradeFormInputId, T : WithQuantity, T : WithActionTradeFormInputId {
    resetAllOrNoneOnQuantityChange(inputId)
    resetPricesOnVisibilityChange(inputId)
    resetPricesOnSymbolChange(inputId.symbol, inputId)
    resetTermAndAllOrNoneOnPriceTypeChange(inputId)
}

internal fun <T> RuleListBuilder.observeQuantityChange(
    resources: Resources,
    onActionClickListener: OnActionClickListener,
    inputId: T
) where T : BaseTradeFormInputId, T : WithQuantity, T : WithActionTradeFormInputId {
    rule {
        val quantityInput = requireInput<TextInput>(inputId.quantity)
        quantityInput.value.observeForever {
            if (requireInput<SelectInput>(inputId.action).value.value?.id !=
                inputId.actionValueId.sell && it?.contains('.') == true
            ) {
                quantityInput.setValue(
                    quantityInput.value.value.toString().replace(".", "")
                )
                val dialog = resources.getInputFractionalErrorDialog()
                onActionClickListener.onActionClick(TradeFormAction.Dialog(dialog))
            }
        }
    }
}

internal fun TradeFormParameter.editPriceType(inputId: WithPriceTradeFormInputId): PriceType? {
    return inputId.extractPriceType(defaultValues)
        ?.takeIf { tradeOrderType == TradeOrderType.EDIT_OPEN }
}

@Suppress("LongParameterList")
internal fun <T> GroupBuilder.term(
    resources: Resources,
    inputId: T,
    limitOrderTermLabelMap: Map<String, String>,
    allowShortSellUpdatedTerms: Boolean,
    defaultTerm: OrderTerm?,
    walkthroughState: LiveData<State>? = null
) where T : WithPriceTradeFormInputId, T : WithActionTradeFormInputId {
    select(inputId.term) {
        title = resources.getString(R.string.trade_hint_term)
        defaultTerm?.let {
            defaultOptionId = inputId.termValueId.converter.toKey(it)
        }
        dynamicTermsRule(inputId, resources, allowShortSellUpdatedTerms, limitOrderTermLabelMap)
        enabledIfReachesState(walkthroughState, TradePlacingState.HighlightTerm)
    }
}

internal fun <T> InputBuilder<T>.enabledIfReachesState(
    walkthroughState: LiveData<State>?,
    targetState: State
) {
    val currentState = walkthroughState?.value
    if (currentState == null || currentState is IdleState) {
        return
    }
    enabledIf {
        walkthroughState
            .map { it is IdleState || it == targetState }
            .takeUntil { it }
    }
}

private fun <T> SelectInputBuilder.dynamicTermsRule(
    inputId: T,
    resources: Resources,
    allowShortSellUpdatedTerms: Boolean,
    limitOrderTermLabelMap: Map<String, String>
) where T : WithPriceTradeFormInputId, T : WithActionTradeFormInputId {
    dynamic {
        rule {
            inputValue<DropDownOption>(inputId.priceType).combine(inputValue<DropDownOption>(inputId.action)) { priceTypeOption, actionOption ->
                val isShortSellAction = actionOption?.id == inputId.actionValueId.sellShort
                if (allowShortSellUpdatedTerms && isShortSellAction && inputId !is ConditionalPriceTypeValueId) {
                    createSellShortTermOptions(resources, inputId, priceTypeOption)
                } else {
                    createTermOptions(resources, inputId, limitOrderTermLabelMap, priceTypeOption)
                }
            }
        }
    }
}

private fun GroupBuilder.orderType(
    resources: Resources,
    inputId: WithActionTradeFormInputId,
    walkthroughState: LiveData<State>? = null
) {
    tradeSelect(inputId.action) {
        title = resources.getString(R.string.trade_hint_order_type)
        options {
            val actionValueId = inputId.actionValueId
            option(actionValueId.buy) { title = resources.getString(R.string.trade_order_type_buy) }
            option(actionValueId.sell) { title = resources.getString(R.string.trade_order_type_sell) }
            option(actionValueId.sellShort) { title = resources.getString(R.string.trade_order_type_sell_short) }
            option(actionValueId.buyToCover) { title = resources.getString(R.string.trade_order_type_buy_to_cover) }
        }
        helpParams = TradeHelpParams(TradeOptionInfoType.ORDER_TYPE)
        enabledIfReachesState(walkthroughState, TradePlacingState.HighlightOrderType)
    }
}

internal fun <T> GroupBuilder.allOrNoneSwitch(resources: Resources, inputId: T, allOrNoneValidPriceTypes: List<String>)
        where T : WithPriceTradeFormInputId, T : WithQuantity {
    switch(inputId.allOrNone) {
        title = resources.getString(R.string.trade_hint_all_or_none_300)
        valueCoder = inputId.allOrNoneValueCoder

        visibleIf {
            inputParameterValue(inputId.priceType).map {
                it in allOrNoneValidPriceTypes
            }
        }

        enabledIf {
            inputValue<String>(inputId.quantity).map {
                it?.toBigDecimalOrNull()?.compareTo(BigDecimal(ALL_OR_NONE_QUANTITY)) ?: -1 > -1
            }
        }
    }
}

private fun <T> RuleListBuilder.resetQuantityOnOrderTypeChange(inputId: T) where T : WithQuantity, T : WithActionTradeFormInputId {
    var previousSelectedOrderTypeId: String? = null
    rule {
        requireInput<SelectInput>(inputId.action).value.observeForever {
            val orderTypeId = it?.id
            if (previousSelectedOrderTypeId != null && previousSelectedOrderTypeId != orderTypeId) {
                requireInput<TextInput>(inputId.quantity).resetValue()
            }
            previousSelectedOrderTypeId = orderTypeId
        }
    }
}

private fun <T> RuleListBuilder.resetAllOrNoneOnQuantityChange(inputId: T) where T : WithQuantity, T : WithPriceTradeFormInputId {
    var previousQuantityValue: Any? = null
    rule {
        inputParameterValue(inputId.quantity).observeForever {
            if (previousQuantityValue != null && previousQuantityValue != it) {
                requireInput<SwitchInput>(inputId.allOrNone).setValue(false)
            }
            previousQuantityValue = it
        }
    }
}

private fun RuleListBuilder.updateCalculatorVisibilityOnSymbolChange(inputId: WithQuantity, quoteWidgetViewModel: QuotesWidgetViewModel) {
    rule {
        quoteWidgetViewModel.isQuoteLoaded.observeForever {
            requireInput<TextInput>(inputId.quantity).setIconVisible(it)
        }
    }
}
