package com.etrade.mobilepro.trade.data

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.underlier
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.api.option.OptionTradeStrategy
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.TradeFormInputIdMap
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.getTradeAction
import com.etrade.mobilepro.trade.form.api.input.CONDITIONAL_ORDER_SECURITY_TYPE_ID
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ConditionalTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithFundToBuyTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithQuantity
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithTradeLegsTradeFormInputId
import com.etrade.mobilepro.util.ValueCoder
import java.math.BigDecimal
import javax.inject.Inject

private typealias ParamValue = Pair<String, Any>

class DefaultTradeFormParametersBuilder @Inject constructor(
    private val conditionalTradeFormInputId: ConditionalTradeFormInputId,
    private val formInputIdMap: TradeFormInputIdMap,
    private val tradeLegsCoder: ValueCoder<List<TradeLeg>>
) : TradeFormParametersBuilder {

    override var orderId: String? = null
    override var originalQuantity: String? = null
    override var accountId: String? = null
    override var symbol: WithSymbolInfo? = null
    override var action: TradeAction? = null
    override var term: OrderTerm? = null
    override var quantity: BigDecimal? = null
    override var priceType: PriceType? = null
    override var limitPrice: BigDecimal? = null
    override var stopPrice: BigDecimal? = null
    override var offsetValue: BigDecimal? = null
    override var optionTradeStrategy: OptionTradeStrategy? = null
    override var fundToBuy: WithSymbolInfo? = null
    override var positionLotId: String? = null
    override var allOrNone: Boolean? = null
    override var advancedOrder: Boolean? = null
    override var advancedTradeType: AdvancedTradeType? = null
    override var walkthroughFormCompleted: String? = null

    override fun create(securityType: SecurityType): InputValuesSnapshot {
        val id = requireNotNull(formInputIdMap.getInputId(TradeFormType(securityType, advancedTradeType ?: AdvancedTradeType.None)))

        val params = mutableListOf<ParamValue>()

        securityType.toValueId(id)?.let {
            params += pair(id.securityType, it)
        }

        accountId?.let {
            params += pair(id.accountId, it)
        }

        optionTradeFormParams(params, id)
        stockTradeFormParams(params, id)
        withActionTradeFormParams(params, id)
        withAdvancedTradeFormParams(params, id)
        withConditionalTradeFormParams(params, id)
        withTradeLegsFormParams(params, id)
        withPriceTradeFormParams(params, id)
        withSymbolTradeFormParams(params, id)
        withQuantityTradeFormParams(params, id)
        withFundToBuyTradeFormParams(params, id)

        return params.toMap()
    }

    private fun optionTradeFormParams(params: MutableList<ParamValue>, id: TradeFormInputId) {
        if (id is OptionTradeFormInputId) {
            params += pair(id.orderId, orderId)
            params += pair(id.lookupSymbol, symbol?.let { id.symbolCoder.encode(it.underlier) })
        }
    }

    private fun stockTradeFormParams(params: MutableList<ParamValue>, id: TradeFormInputId) {
        if (id is StockTradeFormInputId) {
            params += pair(id.positionLotId, positionLotId)
            params += pair(id.orderId, orderId)
            params += pair(id.originalQuantity, originalQuantity)
            params += pair(id.walkthroughFormCompleted, walkthroughFormCompleted)
        }
    }

    private fun withActionTradeFormParams(params: MutableList<ParamValue>, id: TradeFormInputId) {
        if (id is WithActionTradeFormInputId) {
            params += pair(id.action, action?.let { id.actionValueId.converter.toKey(it) })
        }
    }

    private fun withAdvancedTradeFormParams(params: MutableList<ParamValue>, id: TradeFormInputId) {
        if (id is WithAdvancedTradeFormInputId) {
            params += pair(id.advancedOrder, advancedOrder?.toString())
            advancedTradeType?.let {
                params += pair(id.advancedOrderType, id.advancedOrderTypeValueId.converter.toKey(it))
            }
        }
    }

    private fun withConditionalTradeFormParams(params: MutableList<ParamValue>, id: TradeFormInputId) {
        if (id is ConditionalOrderTradeFormInputId) {
            optionTradeStrategy?.let { strategy ->
                val optionLeg: TradeLeg? = strategy.optionLegs.firstOrNull()
                optionLeg?.let { leg ->
                    params += pair(
                        conditionalTradeFormInputId.order1,
                        LinkedHashMap<String, Any?>().apply {
                            put(id.accountId, accountId)
                            put(id.securityType, SecurityType.OPTION.toValueId(id))
                            put(id.symbol, id.symbolCoder.encode(Symbol(leg.symbol, InstrumentType.OPTN)))
                            put(id.action, id.actionValueId.converter.toKey(leg.transactionType.getTradeAction()))
                            put(id.quantity, leg.quantity.toString())
                            put(id.underlierSymbol, symbol?.underlier?.let { id.symbolCoder.encode(it) })
                            put(id.priceType, priceType?.let { id.priceTypeValueId.converter.toKey(it) } ?: id.priceTypeValueId.market)
                            put(id.term, term?.let { id.termValueId.converter.toKey(it) } ?: id.termValueId.goodForDay)
                            put(CONDITIONAL_ORDER_SECURITY_TYPE_ID, SecurityType.OPTION.name)
                        }
                    )
                }
            }
        }
    }

    private fun withTradeLegsFormParams(params: MutableList<ParamValue>, id: TradeFormInputId) {
        if (id is WithTradeLegsTradeFormInputId) {
            optionTradeStrategy?.let { strategy ->
                params += pair(id.strategy, id.strategyValueId.converter.toKey(strategy.strategyType))
                params += pair(id.tradeLegs, tradeLegsCoder.encode(strategy.optionLegs))
            }
        }
    }

    private fun withPriceTradeFormParams(params: MutableList<ParamValue>, id: TradeFormInputId) {
        if (id is WithPriceTradeFormInputId) {
            params += pair(id.priceType, priceType?.let { id.priceTypeValueId.converter.toKey(it) })
            params += pair(id.limitPrice, limitPrice?.toPlainString())
            params += pair(id.stopPrice, stopPrice?.toPlainString())
            params += pair(id.term, term?.let { id.termValueId.converter.toKey(it) })
            params += pair(id.allOrNone, allOrNone?.let { id.allOrNoneValueCoder.encode(it) })
            withPriceTradeFormParams(params, id, priceType)
        }
    }

    private fun withPriceTradeFormParams(params: MutableList<ParamValue>, id: WithPriceTradeFormInputId, priceType: PriceType?) {
        when (priceType) {
            PriceType.STOP_LIMIT -> {
                params += pair(id.limitPriceForStopLimit, limitPrice?.toPlainString())
                params += pair(id.stopPriceForStopLimit, stopPrice?.toPlainString())
            }
            PriceType.STOP -> params += pair(id.stopPriceForStopLimit, stopPrice?.toPlainString())
            PriceType.TRAILING_STOP_PERCENT,
            PriceType.TRAILING_STOP_DOLLAR -> params += pair(id.stopPrice, offsetValue?.toPlainString())
            else -> {
            }
        }
    }

    private fun withSymbolTradeFormParams(params: MutableList<ParamValue>, id: TradeFormInputId) {
        if (id is WithSymbolTradeFormInputId) {
            params += pair(id.symbol, symbol?.let { id.symbolCoder.encode(it) })
        }
    }

    private fun withQuantityTradeFormParams(params: MutableList<ParamValue>, id: TradeFormInputId) {
        if (id is WithQuantity) {
            params += pair(id.quantity, quantity?.toPlainString())
        }
    }

    private fun withFundToBuyTradeFormParams(params: MutableList<ParamValue>, id: TradeFormInputId) {
        if (id is WithFundToBuyTradeFormInputId) {
            params += pair(id.fundToBuy, fundToBuy?.let { id.symbolCoder.encode(it) })
        }
    }

    private fun SecurityType.toValueId(inputId: TradeFormInputId): String? {
        return inputId.securityTypeValueId.converter.toKey(this)
    }
}

private operator fun <T> MutableList<T>.plusAssign(element: T?) {
    if (element != null) {
        add(element)
    }
}

private fun pair(id: String, value: Any?): ParamValue? = value?.let { id to it }
