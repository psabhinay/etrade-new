package com.etrade.mobilepro.quote.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MutualFundsTradeQuoteRequestValueDto(
    @Json(name = "value")
    val mutualFundsTradeQuoteRequestDto: MutualFundsTradeQuoteRequestDto
)

@JsonClass(generateAdapter = true)
data class MutualFundsTradeQuoteRequestDto(
    @Json(name = "accountId")
    val accountId: String = "",
    @Json(name = "symbol")
    val symbol: String = "",
    @Json(name = "orderSource")
    val orderSource: String = "android"
)
