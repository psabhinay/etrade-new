package com.etrade.mobilepro.learn.presentation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import androidx.lifecycle.lifecycleScope
import com.etrade.mobilepro.common.RestoreTabPositionDelegate
import com.etrade.mobilepro.learn.presentation.databinding.LearnFragmentContainerBinding
import com.etrade.mobilepro.learn.presentation.library.LibraryFragment
import com.etrade.mobilepro.learn.presentation.tutorial.LearnTutorialFragment
import com.etrade.mobilepro.tooltip.Tooltip
import com.etrade.mobilepro.tooltip.TooltipGravity
import com.etrade.mobilepro.tooltip.TooltipViewModel
import com.etrade.mobilepro.util.android.adapter.DefaultFragmentPagerAdapter
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.android.extension.addOnPageChangeListener
import com.etrade.mobilepro.util.android.extension.reduceDragSensitivity
import com.etrade.mobilepro.util.android.extension.setContentDescriptionForTabs
import com.etrade.mobilepro.util.android.extension.setupInnerClickListenersForAccessibility
import com.etrade.mobilepro.util.android.extension.viewCoroutineScope
import com.etrade.mobilepro.walkthrough.api.SymbolLookupAction
import com.etrade.mobilepro.walkthrough.api.SymbolLookupState
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val ACCESSIBILITY_SETUP_DELAY = 250L

class LearnContainerFragment @Inject constructor(
    private val restoreTabPositionDelegate: RestoreTabPositionDelegate,
    private val walkthroughStatesViewModel: WalkthroughStatesViewModel
) : Fragment(R.layout.learn_fragment_container) {

    private val fragments = listOf(
        DefaultFragmentPagerAdapter.FragmentPagerItem(
            LearnTutorialFragment::class.java,
            R.string.learn_tutorials_title
        ),
        DefaultFragmentPagerAdapter.FragmentPagerItem(
            LibraryFragment::class.java,
            R.string.learn_library_title
        )
    )

    private val binding by viewBinding(LearnFragmentContainerBinding::bind)

    private val tooltipViewModel: TooltipViewModel by activityViewModels()
    private var tooltip: Tooltip? = null
    private val symbolLookupState = Transformations.map(walkthroughStatesViewModel.state) {
        it.consumable()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        DefaultFragmentPagerAdapter(this, fragments)
            .attach(binding.tabLayout, binding.viewPager)
        binding.viewPager.let { restoreTabPositionDelegate.init(this, it) }
        binding.viewPager.reduceDragSensitivity()
        viewLifecycleOwner.lifecycleScope.launch {
            delay(ACCESSIBILITY_SETUP_DELAY)
            setupTabAccessibility()
        }

        symbolLookupState.observe(
            viewLifecycleOwner,
            Observer {
                when (it.getNonConsumedContent()) {
                    SymbolLookupState.DisplaySearchTooltip -> showWatchlistTooltip()
                }
            }
        )
    }

    private fun setupTabAccessibility() {
        binding.apply {
            tabLayout.setContentDescriptionForTabs()
            tabLayout.setupInnerClickListenersForAccessibility()
            viewPager.addOnPageChangeListener(
                onPageSelected = {
                    tabLayout.setContentDescriptionForTabs()
                }
            )
        }
    }

    private fun showWatchlistTooltip() {
        val targetView = activity?.findViewById<View>(R.id.menu_quote_lookup) ?: return
        tooltip = tooltipViewModel.show {
            target = targetView
            text = getString(R.string.learn_quote_lookup_search_tooltip_text)
            gravity = TooltipGravity.BOTTOM
            yOffset = resources.getDimensionPixelOffset(R.dimen.spacing_small)
            durationParams = Tooltip.DurationParams(viewCoroutineScope)
        }
        walkthroughStatesViewModel.onAction(SymbolLookupAction.LookupActionHighlighted)
    }
}
