package com.etrade.mobilepro.trade.api.mutualfunds

import com.etrade.mobilepro.orders.api.DividendReInvestmentType
import com.etrade.mobilepro.orders.api.FundsOrderActionType
import com.etrade.mobilepro.orders.api.FundsQuantityType
import com.etrade.mobilepro.trade.api.Order
import java.math.BigDecimal

interface MutualFundsOrder : Order {
    override val legs: List<MutualFundsTradeLeg>
    val orderId: String
    val commission: BigDecimal?
    val commissionMessage: String?
    val dividendReInvestmentType: DividendReInvestmentType
    val orderActionType: FundsOrderActionType
    val priceTypeMessage: String
    val quantityType: FundsQuantityType
}
