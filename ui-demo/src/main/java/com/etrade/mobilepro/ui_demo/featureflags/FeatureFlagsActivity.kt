package com.etrade.mobilepro.ui_demo.featureflags

import android.os.Bundle
import com.etrade.mobilepro.ui_demo.BaseDemoActivity
import com.etrade.mobilepro.ui_demo.R

class FeatureFlagsActivity: BaseDemoActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feature_flags)
    }
}
