package com.etrade.completeview.dto

import com.etrade.mobilepro.backends.mgs.StreamingValueDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NetAssetsDto(
    @Json(name = "account_uuids")
    val accountUuids: List<String>,
    @Json(name = "account_summary_label")
    val label: String,
    @Json(name = "account_summary_streamable_value")
    val value: StreamingValueDto
)
