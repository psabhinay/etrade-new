package com.etrade.mobilepro.util.android.navigation

import android.net.Uri
import androidx.annotation.IdRes
import androidx.navigation.NavController

fun NavController.hasDestinationInBackStack(@IdRes destinationId: Int): Boolean {
    return try {
        getBackStackEntry(destinationId)
        true
    } catch (e: IllegalArgumentException) {
        false
    }
}

/**
 * Performs [deepLink] navigation safely. If an invalid [deepLink] is given then [onInvalidDeepLink] callback will be called.
 *
 * @param deepLink Deep link to the destination.
 * @param onInvalidDeepLink Callback to invoke on unreachable deep link.
 */
inline fun NavController.navigateSafely(deepLink: Uri, onInvalidDeepLink: (Throwable) -> Unit) {
    try {
        navigate(deepLink)
    } catch (e: IllegalArgumentException) {
        onInvalidDeepLink(e)
    }
}
