package com.etrade.mobilepro.chat.usecase

import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.api.ChatState
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType
import com.etrade.mobilepro.common.result.ETResult
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Test

@ExperimentalCoroutinesApi
class ChatRequestUseCaseTest {

    @Test
    fun testVerifyMessageShownState() {
        testData(ChatRetrieveMessageType.ChatEstablished, ChatState.MessagesShown(listOf(ChatMessage.Agent(TEST_TEXT_MESSAGE)), unread = 1))
        testData(ChatRetrieveMessageType.ChatMessage, ChatState.MessagesShown(listOf(ChatMessage.Agent(TEST_TEXT_MESSAGE)), unread = 1))
        testData(ChatRetrieveMessageType.AgentNotTyping, ChatState.MessagesShown(emptyList()))
        testData(ChatRetrieveMessageType.AgentTyping, ChatState.MessagesShown(listOf(ChatMessage.AgentTyping)))
    }

    @Test
    fun testVerifyQueueShownState() {
        val estimatedAwaitTime = 1234
        testData(ChatRetrieveMessageType.ChatRequestSuccess, ChatState.QueueShown(estimatedAwaitTime))
        testData(ChatRetrieveMessageType.QueueUpdate, ChatState.QueueShown(estimatedAwaitTime))
    }

    @Test
    fun testVerifyErrorState() {
        testData(ChatRetrieveMessageType.ChatRequestFail, ChatState.Error("reason"))
    }

    @Test
    fun testVerifyTerminatedByAgentState() {
        testData(ChatRetrieveMessageType.ChatEnded, ChatState.TerminatedByAgent)
    }

    private fun testData(retrieveMessageType: ChatRetrieveMessageType, expected: ChatState) {
        val case = ChatRequestUseCaseImpl(
            mock {
                onBlocking {
                    getMessages(chatTestMessagesData)
                } doReturn ETResult.success(getRetrievedMessageModel(retrieveMessageType))
                onBlocking {
                    postMessage(chatTestPostMessageData)
                } doReturn ETResult.success(chatTestForMobileModel)
                onBlocking {
                    terminateChat(chatTestTerminateData)
                } doReturn ETResult.success(chatTestForMobileModel)
            }
        )

        runBlockingTest {
            val result: ETResult<ChatState> = case.execute(chatTestMessagesData)

            assertNotNull(result)
            assertTrue(result.isSuccess)
            assertEquals(expected, result.getOrThrow())
        }
    }

    private fun getRetrievedMessageModel(retrieveMessageType: ChatRetrieveMessageType) =
        when (retrieveMessageType) {
            ChatRetrieveMessageType.ChatRequestSuccess,
            ChatRetrieveMessageType.QueueUpdate -> chatRetrievedMessageQueueUpdateModel
            ChatRetrieveMessageType.ChatEstablished,
            ChatRetrieveMessageType.ChatMessage -> chatRetrievedMessageTextModel
            ChatRetrieveMessageType.AgentTyping -> chatRetrievedMessageAgentTypingModel
            ChatRetrieveMessageType.ChatEnded -> chatRetrievedMessageChatEndedModel
            ChatRetrieveMessageType.AgentNotTyping -> chatRetrievedMessageAgentNotTypingModel
            ChatRetrieveMessageType.ChatRequestFail -> chatRetrievedMessageFailedModel
            else -> chatRetrievedMessageFailedModel
        }
}
