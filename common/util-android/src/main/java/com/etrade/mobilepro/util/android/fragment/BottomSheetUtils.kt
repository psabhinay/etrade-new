package com.etrade.mobilepro.util.android.fragment

import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * Indicates if the current fragment is contained within a BottomSheetDialogFragment
 *
 * @param frag: The fragment to check
 *
 * @return If the fragment is a child of a BottomSheetDialogFragment
 */
tailrec fun Fragment.isChildOfBottomSheet(frag: Fragment?): Boolean {
    if (frag == null) return false
    if (frag is BottomSheetDialogFragment) return true
    return isChildOfBottomSheet(frag.parentFragment)
}
