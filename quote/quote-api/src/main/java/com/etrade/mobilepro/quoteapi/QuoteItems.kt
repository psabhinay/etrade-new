package com.etrade.mobilepro.quoteapi

class QuoteItems(
    val snapshot: List<QuoteItemInterface>,
    val performance: List<QuoteItemInterface> = emptyList(),
    val feesAndExpenses: List<QuoteItemInterface> = emptyList()
)
