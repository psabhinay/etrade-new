package com.etrade.mobilepro.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SetExtensionsTest {

    @Test
    fun `check empty sets symmetric difference`() {
        assertEquals(emptySet<Any>(), emptySet<Any>().symmetricDifference(emptySet()))
    }

    @Test
    fun `check receiver set is empty`() {
        val expected = setOf(1, 2, 3)
        assertEquals(expected, emptySet<Any>().symmetricDifference(expected))
    }

    @Test
    fun `check argument set is empty`() {
        val expected = setOf(1, 2, 3)
        assertEquals(expected, expected.symmetricDifference(emptySet()))
    }

    @Test
    fun `check self symmetric difference`() {
        val self = setOf(1, 2, 3)
        assertEquals(emptySet<Int>(), self.symmetricDifference(self))
    }

    @Test
    fun `check symmetric difference`() {
        assertEquals(setOf(1, 4), setOf(1, 2, 3).symmetricDifference(setOf(2, 3, 4)))
    }

    @Test
    fun `check commutative characteristic`() {
        val setA = setOf(1, 2, 3)
        val setB = setOf(2, 3, 4)
        assertEquals(setA.symmetricDifference(setB), setB.symmetricDifference(setA))
    }

    @Test
    fun `check associative characteristic`() {
        val setA = setOf(1, 2, 3)
        val setB = setOf(2, 3, 4)
        val setC = setOf(3, 4, 5)
        assertEquals(setA.symmetricDifference(setB).symmetricDifference(setC), setB.symmetricDifference(setC).symmetricDifference(setA))
    }
}
