package com.etrade.mobilepro.chart.netassets

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.time.Month
import java.time.format.TextStyle
import java.util.Locale

class NetAssetsChartFormattersSuite {

    @Test
    fun `verify X axis formatter`() {
        var month = Month.JANUARY
        for (i in 1..NUMBER_OF_MONTHS * 2) {
            val expected = month.getDisplayName(TextStyle.SHORT, Locale.ENGLISH)
            val actual = formatXAxis(i.toFloat())
            assertEquals(expected, actual)

            month = if (month == Month.DECEMBER) {
                Month.JANUARY
            } else {
                Month.of(month.value + 1)
            }
        }
    }

    @Test
    fun `verify Y axis formatter`() {
        val expectedValues = mapOf(
            204f to "204",
            700000f to "700K",
            1400000f to "1.4M",
            1400001f to "1.4M",
            500000000f to "500M",
            500000001f to "500M"
        )

        expectedValues.forEach { entry ->
            val expected = entry.value
            val actual = formatYAxis(entry.key)
            assertEquals(expected, actual)
        }

        expectedValues.forEach { entry ->
            val expected = "-${entry.value}"
            val actual = formatYAxis(-entry.key)
            assertEquals(expected, actual)
        }
    }
}
