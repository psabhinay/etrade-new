package com.etrade.mobilepro.bank.actions

import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory

class BankActionsViewFactory : DynamicUiViewFactory {
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        (dto as? BankActionsViewDto)?.let {
            return object : GenericLayout {
                override val viewModelFactory = viewModelFactory {
                    toBankActionsViewModel(dto.data as String)
                }
                override val viewModelClass = BankActionsViewModel::class.java

                override val uniqueIdentifier: String = "${javaClass.name}_${dto.data}"
            }
        } ?: throw DynamicUiViewFactoryException("BankActionsViewFactory registered to unknown type")
    }

    private fun toBankActionsViewModel(bankAccountId: String?) = BankActionsViewModel(bankAccountId)
}
