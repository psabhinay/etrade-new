package com.etrade.mobilepro.barcode.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.etrade.mobilepro.barcode.api.BarcodeHistoryRepo
import com.etrade.mobilepro.barcode.api.BarcodeItem
import com.etrade.mobilepro.dao.realm.asLiveData
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.Sort

class DefaultBarcodeHistoryRepo : BarcodeHistoryRepo {

    private val realmConfig = RealmConfiguration.Builder()
        .name("barcode.history")
        .modules(BarcodeRealmModule())
        .build()

    override fun saveBarcodeToHistory(barcodeItem: BarcodeItem) {
        Realm.getInstance(realmConfig).use {
            it.executeTransaction { realm ->
                realm.insertOrUpdate(
                    BarcodeHistoryRealmItem(
                        barcodeItem.upc,
                        barcodeItem.company,
                        barcodeItem.product,
                        barcodeItem.timeInMilliseconds,
                        barcodeItem.symbol
                    )
                )
            }
        }
    }

    override fun deleteBarcodeFromHistory(barcodeItem: BarcodeItem) {
        Realm.getInstance(realmConfig).use {
            it.executeTransaction { realm ->
                realm
                    .where(BarcodeHistoryRealmItem::class.java)
                    .equalTo("id", barcodeItem.id)
                    .findAll()
                    .deleteAllFromRealm()
            }
        }
    }

    override fun getBarcodeHistory(): LiveData<List<BarcodeItem>> {
        val result: LiveData<List<BarcodeHistoryRealmItem>> = Realm.getInstance(realmConfig)
            .where(BarcodeHistoryRealmItem::class.java)
            .findAllAsync()
            .sort("timeInMilliseconds", Sort.DESCENDING)
            .asLiveData()

        return Transformations.map(result) {
            it.map { barcode ->
                BarcodeItem(
                    barcode.id,
                    barcode.companyName,
                    barcode.productName,
                    barcode.symbol,
                    barcode.upc,
                    barcode.timeInMilliseconds
                )
            }
        }
    }
}
