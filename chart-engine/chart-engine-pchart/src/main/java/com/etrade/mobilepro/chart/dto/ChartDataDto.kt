package com.etrade.mobilepro.chart.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChartDataDto(
    @Json(name = "Display")
    val displayData: List<ChartOHLCDataDto>,
    @Json(name = "YesterdaysClose")
    val yesterdaysClose: YesterdaysCloseDto
)

@JsonClass(generateAdapter = true)
data class ChartOHLCDataDto(
    @Json(name = "D")
    val date: String,
    @Json(name = "O")
    val open: Double,
    @Json(name = "H")
    val high: Double,
    @Json(name = "L")
    val low: Double,
    @Json(name = "C")
    val close: Double,
    @Json(name = "V")
    val volume: Long
)

@JsonClass(generateAdapter = true)
data class YesterdaysCloseDto(
    @Json(name = "C")
    val close: Double
)
