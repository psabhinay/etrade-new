package com.etrade.mobilepro.streaming.client.side.calculations

import android.os.SystemClock
import com.etrade.eo.accountvalues.api.AccountValuesVO
import com.etrade.eo.streaming.SubscriptionMode
import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.account.dao.AccountDao
import com.etrade.mobilepro.accountvalues.pnl.calculator.AccountPnlValuesCalculator
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculatedAccountValues
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult.CalculatedAccountValuesVO
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.streaming.api.OptionData
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import com.etrade.mobilepro.streaming.positions.cache.StreamingQuoteProviderWithCachedPositions
import com.etrade.mobilepro.util.safeParseBigDecimal
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference
import javax.inject.Inject

const val ALL_BROKERAGE_STREAM_ID: String = "ALL_BROKERAGE_ACCOUNTS"

private const val UPDATE_INTERVAL: Long = 2L

class ClientCalculationsSubscriptionManagerHelper @Inject constructor(
    private val realmDaoProvider: CachedDaoProvider,
    private val pnlCalculator: AccountPnlValuesCalculator,
    private val streamingQuoteProvider: StreamingQuoteProviderWithCachedPositions
) : SubscriptionManagerHelper<AccountValuesVO> {

    private val accountDao: AccountDao by lazy {
        realmDaoProvider.getAccountDao()
    }

    private val lastCalculatedAccountValuesCache = mutableMapOf<String, CalculatedAccountValues>()
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)
    private val optionFields = OptionData.fields()
    private val stockFields = StockData.fields()
    private val streams = mutableMapOf<String, Stream>()

    override fun subscribeTo(streamId: String, fields: Set<String>, mode: SubscriptionMode): Observable<AccountValuesVO> {
        logger.debug("subscribeTo() called with streamId = $streamId")

        val accountId = extractAccountIdFromStreamId(streamId)
        val stream = getOrCreateStream(accountId)

        return stream.source
            .asObservable(
                doOnSubscribe = {
                    startStreaming(accountId, stream)
                },
                doFinally = {
                    stopStreaming(accountId, stream)
                }
            )
    }

    private fun subscribeToSymbolsUpdates(instrumentTypes: Map<String, InstrumentType>, disposable: CompositeDisposable) {
        instrumentTypes.forEach { (symbol, type) ->
            streamingQuoteProvider.getQuote(symbol, type, if (type.isOption) optionFields else stockFields)
                .subscribeOn(Schedulers.io())
                .subscribe()
                .addTo(disposable)
        }
    }

    @Synchronized
    private fun getOrCreateStream(accountId: String): Stream {
        return streams[accountId] ?: Stream().also { stream ->
            streams[accountId] = stream
        }
    }

    private fun startStreaming(accountId: String, stream: Stream) {
        synchronized(stream) {
            val disposable = stream.disposable
            if (disposable == null) {
                stream.disposable = when (accountId) {
                    ALL_BROKERAGE_STREAM_ID -> initAllBrokerageAccountsStreaming(stream)
                    else -> initAccountStreaming(accountId, stream.source)
                }
            }
        }
    }

    private fun stopStreaming(accountId: String, stream: Stream) {
        synchronized(stream) {
            logger.debug("stopStreaming() called with accountId = $accountId")
            val disposable = stream.disposable
            if (disposable != null && stream.source.hasObservers().not()) {
                disposable.dispose()
                stream.disposable = null
                streams.remove(accountId)

                if (accountId == ALL_BROKERAGE_STREAM_ID) {
                    stream.allBrokerageAccountIds?.forEach { id ->
                        stopStreaming(id, getOrCreateStream(id))
                    }
                }
            }
        }
    }

    @SuppressWarnings("LongMethod")
    private fun initAccountStreaming(accountId: String, subject: SerializedSubjectWithValue<AccountValuesVO>): Disposable {
        logger.debug("initAccountStreaming() called for accountId = $accountId")
        val streamingCompositeDisposable = CompositeDisposable()
        var instrumentTypes: Map<String, InstrumentType> = emptyMap()
        var positionIds = emptySet<String>()
        val positionsDao = realmDaoProvider.getPositionsDao()
        val instrumentDao = realmDaoProvider.getInstrumentDao()
        val account = accountDao.getAccountWithAccountIdAndType(accountId)

        return Observable.interval(UPDATE_INTERVAL, TimeUnit.SECONDS).map {
            positionsDao.getAccountPositions(accountId)
        }.observeOn(Schedulers.computation()).map { positions ->
            if (!subject.hasValue() && positions.isEmpty()) {
                return@map CalculationResult.EmptyCalculationResult
            }
            val symbols = positions.associateBy { it.symbol }.keys
            val newPositionIds = positions.associateBy { it.positionId }.keys

            if (symbols != instrumentTypes.keys || newPositionIds != positionIds) {
                instrumentTypes = instrumentDao.getInstrumentsNonAsync(symbols).associate { it.symbol to it.instrumentType }
                positionIds = newPositionIds
                streamingCompositeDisposable.clear()
                subscribeToSymbolsUpdates(instrumentTypes, streamingCompositeDisposable)
            }

            handleUpdate(accountId, forceCalculate = positions.isEmpty()) {
                getCashValueAndCalculate(instrumentTypes, positions, account)
            }
        }.doFinally {
            streamingCompositeDisposable.clear()
        }.observeOn(AndroidSchedulers.mainThread()).subscribeBy {
            subject.checkedOnNext(it)
        }
    }

    private fun getCashValueAndCalculate(
        instrumentTypes: Map<String, InstrumentType>,
        positions: List<Position>,
        account: Account?
    ): CalculatedAccountValuesVO = pnlCalculator.calculateAccountValues(
        instrumentTypes,
        positions,
        account?.ledgerAccountValue?.safeParseBigDecimal()
            ?: BigDecimal.ZERO
    )

    private fun initAllBrokerageAccountsStreaming(stream: Stream): Disposable {
        logger.debug("initAllBrokerageAccountsStreaming() called")
        return accountDao.getAllAccountsObservable()
            .observeOn(AndroidSchedulers.mainThread())
            .switchMap { accounts ->
                val accountIds = accounts
                    .filter { it.accountType == AccountType.Brokerage }
                    .map { it.accountId }
                    .also { ids ->
                        stream.allBrokerageAccountIds = ids
                        ids.forEach { id ->
                            val accountStream = getOrCreateStream(id)
                            startStreaming(id, accountStream)
                        }
                    }

                Observable.interval(0L, UPDATE_INTERVAL, TimeUnit.SECONDS)
                    .map {
                        handleUpdate(ALL_BROKERAGE_STREAM_ID) {
                            pnlCalculator.calculateAllBrokerageAccountValues(accountIds, lastCalculatedAccountValuesCache)
                        }
                    }
            }
            .subscribeBy {
                stream.source.checkedOnNext(it)
            }
    }

    private fun handleUpdate(
        accountId: String,
        forceCalculate: Boolean = false,
        calculate: () -> CalculationResult
    ): CalculationResult {
        logger.debug("an update was handled for accountId = $accountId")
        val lastCalculatedAccountValues = lastCalculatedAccountValuesCache[accountId]

        return if (lastCalculatedAccountValues == null || isCalculationsExpired(lastCalculatedAccountValues.timeStamp, UPDATE_INTERVAL) || forceCalculate) {
            val result = calculate()
            if (result is CalculatedAccountValuesVO) {
                lastCalculatedAccountValuesCache[accountId] = CalculatedAccountValues(result, SystemClock.elapsedRealtime())
            }
            result
        } else {
            lastCalculatedAccountValues.accountValues
        }
    }
}

private data class Stream(
    val source: SerializedSubjectWithValue<AccountValuesVO> = SerializedSubjectWithValue(),
    var disposable: Disposable? = null,
    var allBrokerageAccountIds: List<String>? = null
)

private class SerializedSubjectWithValue<T : Any> {
    private val subject: Subject<T> = BehaviorSubject.create<T>().toSerialized()
    private val current = AtomicReference<T>()

    fun hasObservers() = subject.hasObservers()

    fun asObservable(doOnSubscribe: () -> Unit, doFinally: () -> Unit): Observable<T> = subject
        .doOnSubscribe { doOnSubscribe() }
        .doFinally(doFinally)

    fun hasValue() = current.get() != null

    fun onNext(value: T) {
        current.set(value)
        subject.onNext(value)
    }
}

private fun SerializedSubjectWithValue<AccountValuesVO>.checkedOnNext(value: CalculationResult) {
    if (value is CalculatedAccountValuesVO) {
        this.onNext(value)
    }
}
