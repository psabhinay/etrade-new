package com.etrade.mobilepro.quote.mapper

import android.content.res.Resources
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.quote.R
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.math.PERCENT_MULTIPLIER
import com.etrade.mobilepro.util.safeParseBigDecimal
import java.math.BigDecimal
import javax.inject.Inject

private const val invalidNumber = "-9999999.000000"

class QuoteOptionDetailMapper @Inject constructor(private val resources: Resources) {
    fun map(quote: MobileQuote): QuoteItem.QuoteOptionDetailItem {
        val impliedVolatility = if (quote.impliedVolatility == invalidNumber || quote.impliedVolatility.isEmpty()) {
            "--"
        } else {
            quote.impliedVolatility.formatImpliedVolatility()
        }

        return QuoteItem.QuoteOptionDetailItem(
            quote.dayHigh.formatPrice(),
            quote.dayLow.formatPrice(),
            quote.openPrice.formatPrice(),
            quote.previousClose.formatPrice(),
            quote.daysToExpire,
            quote.openInterest,
            quote.intrinsicValue.formatPrice(),
            quote.timePremium,
            quote.deliverableContract.formatDeliverableContract(),
            quote.delta.formatOptionValue(),
            quote.gamma.formatOptionValue(),
            quote.theta.formatOptionValue(),
            quote.vega.formatOptionValue(),
            quote.rho.formatOptionValue(),
            impliedVolatility
        )
    }

    private fun String.formatDeliverableContract(): String {
        val shareCount = toIntOrNull() ?: return ""
        return resources.getQuantityString(R.plurals.quote_number_of_deliverables, shareCount, shareCount)
    }
}

private fun String.formatImpliedVolatility() = MarketDataFormatter.formatMarketPercData(safeParseBigDecimal()?.times(BigDecimal(PERCENT_MULTIPLIER)))

private fun String.formatOptionValue(): String {
    val raw = toDoubleOrNull() ?: return ""
    return String.format("%.4f", raw)
}

private fun String?.formatPrice() = MarketDataFormatter.formatMarketDataCustom(this?.toBigDecimalOrNull())
