package com.etrade.mobilepro.orders.api

data class ExecInstruction(
    val allOrNoneFlag: Boolean,
    val neverHeldFlag: Boolean,
    val doNotReduceFlag: Boolean,
    val doNotIncreaseFlag: Boolean
)
