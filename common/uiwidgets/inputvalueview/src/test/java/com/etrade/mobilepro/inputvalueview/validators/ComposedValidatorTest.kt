package com.etrade.mobilepro.inputvalueview.validators

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class ComposedValidatorTest {

    private lateinit var subject: ComposedValidator
    private val requiredErrorMessage = "Value is required"
    private val validRange = 2..32
    private val lengthErrorMessage = "Value length is not in range"
    private val validString by lazy { "1".repeat(30) }
    private val blankString by lazy { " ".repeat(30) }
    private val longString by lazy { "1".repeat(1000) }
    private val emptyString = ""

    @BeforeEach
    fun setup() {
        subject = RequiredFieldValidator(requiredErrorMessage) +
            LengthInRangeValidator(validRange, lengthErrorMessage)
    }

    @Test
    fun `should be valid if all validators are valid`() {
        val result = subject.validate(validString)
        assertTrue(result is InputFieldManager.Value.Valid)
    }

    @Test
    fun `should be invalid if the length is meet but required field is not valid`() {
        val result = subject.validate(blankString)
        assertTrue(result is InputFieldManager.Value.Invalid)
        if (result is InputFieldManager.Value.Invalid) {
            assertTrue(result.reason == requiredErrorMessage)
        }
    }

    @Test
    fun `should be invalid if the value is not blank but the length is not valid`() {
        val result = subject.validate(longString)
        assertTrue(result is InputFieldManager.Value.Invalid)
        if (result is InputFieldManager.Value.Invalid) {
            assertTrue(result.reason == lengthErrorMessage)
        }
    }

    @Test
    fun `should display an reason based on the order of the validators`() {
        val result = subject.validate(emptyString)
        assertTrue(result is InputFieldManager.Value.Invalid)
        if (result is InputFieldManager.Value.Invalid) {
            assertTrue(result.reason == requiredErrorMessage)
        }
    }
}
