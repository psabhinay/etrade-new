package com.etrade.mobile.accounts.dto

import androidx.annotation.Keep
import com.etrade.mobilepro.util.json.FallbackEnum
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@FallbackEnum(name = "Unknown")
@Keep
enum class AccountType {
    @Json(name = "Brokerage")
    Brokerage,

    @Json(name = "Bank")
    Bank,

    @Json(name = "Managed")
    Managed,

    @Json(name = "ESP")
    ESP,

    @Json(name = "EAS")
    EAS,

    @Json(name = "Unknown")
    Unknown
}

@FallbackEnum(name = "Unknown")
@Keep
enum class ManagedAccountType {
    @Json(name = "Fixed Income Portfolios")
    FixedIncomePortfolios,

    @Json(name = "Blend Portfolios")
    BlendPortfolios,

    @Json(name = "Blend Portfolios - Flex")
    BlendPortfoliosFlex,
    @Json(name = "Unknown")
    Unknown
}

@FallbackEnum(name = "UNKNOWN")
@Keep
enum class InstitutionType {
    @Json(name = "TELEBANK")
    TELEBANK,
    @Json(name = "ADP")
    ADP,
    @Json(name = "MSSB")
    MSSB,
    @Json(name = "OLINK")
    OLINK,
    @Json(name = "UNKNOWN")
    UNKNOWN
}

@JsonClass(generateAdapter = true)
open class AccountDto(
    @Json(name = "accountUuid")
    val accountUuid: String,
    @Json(name = "accountId")
    val accountId: String,
    @Json(name = "accountMode")
    val accountMode: String,
    @Json(name = "acctDesc")
    val accountDescription: String,
    @Json(name = "accountShortName")
    val accountShortName: String,
    @Json(name = "accountLongName")
    val accountLongName: String,
    @Json(name = "acctType")
    val accountType: AccountType,
    @Json(name = "managedAccountType")
    val managedAccountType: ManagedAccountType?,
    @Json(name = "instType")
    val institutionType: InstitutionType,
    @Json(name = "isIRA")
    val isIra: Boolean?,
    @Json(name = "encAccountId")
    val encAccountId: String?,
    @Json(name = "restrictionLevel")
    val restrictionLevel: String?,
    @Json(name = "cashAvailableForWithdrawal")
    val cashAvailableForWithdrawal: String?,
    @Json(name = "maFlag")
    val marginAccountFlag: Boolean?,
    @Json(name = "marginAvailableForWithdrawal")
    val marginAvailableForWithdrawal: String?,
    @Json(name = "purchasingPower")
    val purchasingPower: String?,
    @Json(name = "totalAvailableForWithdrawal")
    val totalAvailableForWithdrawal: String?,
    @Json(name = "funded")
    val funded: Boolean?,
    @Json(name = "ledgerAccountValue")
    val ledgerAccountValue: String?,
    @Json(name = "accountValue")
    val accountValue: String?,
    @Json(name = "accountIndex")
    val accountIndex: String?,
    @Json(name = "daysGain")
    val daysGain: String?,
    @Json(name = "optionLevel")
    val optionLevel: Int?,
    @Json(name = "streamingRestrictions")
    val streamingRestrictions: StreamingRestrictions?,
    @Json(name = "promptsForFunding")
    val promptsForFunding: String?,
    /**
     * This value is required for client side pnl calculation logic for accounts with type [AccountType.Bank].
     * Originally this value is missing in network response so we have to retrieve it from [AccountSummaryViewDto] instance and set it here afterwards.
     */
    var availableBalance: String? = null
)

@JsonClass(generateAdapter = true)
class StreamingRestrictions(
    @Json(name = "accountStreaming")
    val isAccountStreamingAllowed: Boolean?
)
