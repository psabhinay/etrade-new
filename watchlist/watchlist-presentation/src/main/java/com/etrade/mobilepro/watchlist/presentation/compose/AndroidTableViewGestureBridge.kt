package com.etrade.mobilepro.watchlist.presentation.compose

import android.os.SystemClock
import android.view.MotionEvent
import androidx.compose.foundation.gestures.detectHorizontalDragGestures
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.pointer.PointerInputScope
import androidx.compose.ui.input.pointer.consumeAllChanges
import com.evrencoskun.tableview.TableView
import kotlin.math.sign

/**
 * Bridges compose world drag events to the Android View system
 * Intended to be used with TableView as it adds specific metaData to MotionEvents so TableView could recognize them.
 * It is expected that tableView.isInCompose is set to true when using this bridge
 * currOffset params are probably unnecessary and were added for better compatibility(theoretically)
 * curOffset is used for supplying X and Y for ACTION_UP and ACTION_CANCEL events, though it seems it can be skipped
 */
@Suppress("LongMethod")
suspend fun PointerInputScope.bridgeHorizontalGestures(
    onNewEvent: (MotionEvent, direction: Int) -> Unit,
    currentOffset: () -> Offset,
    onCurrentOffsetUpdate: (Offset) -> Unit
) {
    detectHorizontalDragGestures(
        onDragStart = {
            val ev = MotionEvent.obtain(
                SystemClock.uptimeMillis(),
                SystemClock.uptimeMillis(),
                MotionEvent.ACTION_DOWN,
                it.x,
                it.y,
                TableView.COMPOSE_MOTION_EVENT_META_STATE
            )
            onNewEvent(ev, 0)
        },
        onHorizontalDrag = { change, amount ->
            val ev = MotionEvent.obtain(
                change.previousUptimeMillis,
                change.uptimeMillis,
                MotionEvent.ACTION_MOVE,
                change.position.x,
                change.position.y,
                TableView.COMPOSE_MOTION_EVENT_META_STATE
            )
            onCurrentOffsetUpdate(change.position)
            change.consumeAllChanges()
            onNewEvent(ev, (amount.takeIf { !it.isNaN() }?.sign?.toInt() ?: 0).unaryMinus())
        },
        onDragEnd = {
            val ev = MotionEvent.obtain(
                SystemClock.uptimeMillis(),
                SystemClock.uptimeMillis(),
                MotionEvent.ACTION_UP,
                currentOffset().x,
                currentOffset().y,
                TableView.COMPOSE_MOTION_EVENT_META_STATE
            )
            onNewEvent(ev, 0)
        },
        onDragCancel = {
            val ev = MotionEvent.obtain(
                SystemClock.uptimeMillis(),
                SystemClock.uptimeMillis(),
                MotionEvent.ACTION_CANCEL,
                currentOffset().x,
                currentOffset().y,
                TableView.COMPOSE_MOTION_EVENT_META_STATE
            )
            onNewEvent(ev, 0)
        }
    )
}
