package com.etrade.mobilepro.chat.data.setup.dto.chatActiveStatus

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChatActiveStatusDto(
    @Json(name = "data") val data: ChatActiveStatusDataDto
)
