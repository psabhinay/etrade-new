package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.trade.form.api.value.ConditionValueId

object DefaultConditionValueId : ConditionValueId {
    override val lessThanOrEqualTo: String = "4"
    override val greaterThanOrEqualTo: String = "3"
}
