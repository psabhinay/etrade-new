package com.etrade.mobilepro.stockplan

import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ScrollOnTopListener
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.order.details.router.OrderDetailsRouter
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.searchingapi.items.EXTRA_QUOTE_TICKER
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.android.CalendarEvent
import com.etrade.mobilepro.util.android.addToCalendar
import org.chromium.customtabsclient.shared.NavigableWebViewFragment
import javax.inject.Inject

/**
 * Base fragment for handling common logic amongst all
 * stock plan webview related functionality
 */
@RequireLogin
open class StockPlanWebViewFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    @DeviceType private val deviceType: String,
    private val mainNavigation: MainNavigation,
    private val orderDetailsRouter: OrderDetailsRouter
) : NavigableWebViewFragment(viewModelFactory, deviceType), StockPlanHandler, ScrollOnTopListener {
    private val sharedViewModel by activityViewModels<StockPlanWebViewSharedViewModel> { viewModelFactory }

    interface ActionListener {
        fun onTradeButtonClicked()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navigableWebViewModel.headerLiveData.observe(
            viewLifecycleOwner,
            Observer { header ->
                header.getNonConsumedContent()?.let {
                    updateHeaderText(it)
                    updateVisibility(it)
                }
            }
        )
        sharedViewModel.addLanguageChangeListener(this) {
            if (this.view != null) {
                loading = true
                webViewReference.reload()
            }
        }
    }

    override fun onDestroyView() {
        sharedViewModel.removeLanguageChangeListener(this)
        super.onDestroyView()
    }

    @SuppressWarnings("ComplexCondition")
    fun updateVisibility(header: String) {
        if (header == SELL ||
            header == EXERCISE ||
            header == STOCK_PLAN ||
            header == PREVIEW
        ) {
            hideToolbar()
        } else {
            showToolbar()
        }
    }

    override fun setupWebView(webView: WebView) {
        super.setupWebView(webView)
        webView.addJavascriptInterface(StockPlanJSInterface(this), STOCK_PLAN_JS_INTERFACE_NAME)
        backButton.setOnClickListener {
            webView.loadUrl(JSBRIDGE_GO_BACK)
            if (CONFIRMATION.equals(navigableWebViewModel.headerLiveData.value?.peekContent(), true)) {
                hideToolbar()
                getUrl().takeIf { it.isNotBlank() }?.let {
                    val url = it.replace(accountIndexRegex, "")
                    webView.loadUrl(ABOUT_BLANK)
                    webView.loadUrl(url)
                }
            }
        }
    }

    override fun tradeButtonClicked(jsonString: String) {
        (parentFragment as? ActionListener)?.onTradeButtonClicked()
    }

    override fun updatePageTitle(pageTitle: String) {
        navigableWebViewModel.updateHeader(pageTitle)
    }

    override fun openNativeQuote(symbol: String) {
        this.activity?.runOnUiThread {
            mainNavigation.navigateToQuoteDetails(
                activity,
                bundleOf(
                    EXTRA_QUOTE_TICKER to SearchResultItem.Symbol(symbol, InstrumentType.INDX)
                )
            )
        }
    }

    override fun openPopup(pageTitle: String, textResId: Int) {
        this.activity?.runOnUiThread {
            when (pageTitle) {
                DISCLOSURES -> {
                    activity?.let {
                        startActivity(
                            SpannableTextOverlayActivity.intent(
                                it,
                                getString(R.string.disclosures),
                                getString(textResId),
                                R.drawable.ic_close
                            )
                        )
                    }
                }
            }
        }
    }

    override fun openURL(url: String, isPdf: Boolean) {
        requireActivity().runOnUiThread {
            if (isPdf) {
                mainNavigation.openPdf(findNavController(), url)
            } else {
                webViewReference.loadUrl(url)
            }
        }
    }

    override fun openNativeCalendar(date: Long, eventName: String) {
        context?.let {
            addToCalendar(it, CalendarEvent(eventName, date))
        }
    }

    override fun openOrderDetails(argument: OrderDetailsArgument) {
        requireActivity().runOnUiThread {
            orderDetailsRouter.openOrderDetails(findNavController(), argument)
        }
    }

    override fun navigateToAccount(accountId: String) {
        this.activity?.runOnUiThread {
            mainNavigation.navigateToAccount(findNavController(), accountId)
        }
    }

    override val isOnTop get() = webViewReference.scrollY == 0

    override fun scrollUp() {
        webViewReference.scrollTo(0, 0)
    }

    override fun onWebViewRequest(request: WebResourceRequest?) {
        request?.let { sharedViewModel.onWebViewRequest(this.javaClass, lifecycle.currentState, it) }
    }
}
