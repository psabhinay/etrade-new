package com.etrade.mobilepro.screener.data.filter

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.datasource.CountryDataSource
import com.etrade.mobilepro.screener.api.datasource.EtfFundDataSource
import com.etrade.mobilepro.screener.api.datasource.IndustryTypeDataSource
import com.etrade.mobilepro.screener.api.datasource.MovingAveragesDataSource
import com.etrade.mobilepro.screener.api.datasource.MutualFundDataSource
import com.etrade.mobilepro.screener.api.datasource.PercentDataSource
import com.etrade.mobilepro.screener.api.datasource.RegionDataSource
import com.etrade.mobilepro.screener.api.datasource.SectorDataSource
import com.etrade.mobilepro.screener.api.filter.ScreenerFilterFactory
import com.etrade.mobilepro.screener.api.filter.etf.EtfFundProfileFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfPerformanceFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfPortfolioFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfRiskFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfTechnicalsFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.ExpensesFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.FeesFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.FundProfileFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.PerformanceFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.PortfolioFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.RiskFilter
import com.etrade.mobilepro.screener.api.filter.stock.EarningsDividendsFilter
import com.etrade.mobilepro.screener.api.filter.stock.FundamentalsFilter
import com.etrade.mobilepro.screener.api.filter.stock.MarketSegmentFilter
import com.etrade.mobilepro.screener.api.filter.stock.OpinionsFilter
import com.etrade.mobilepro.screener.api.filter.stock.PriceVolumeFilter
import com.etrade.mobilepro.screener.api.filter.stock.TechnicalsFilter
import javax.inject.Inject

class DefaultScreenerFilterFactory @Inject constructor(
    private val sectorDataSource: SectorDataSource,
    private val mutualFundDataSource: MutualFundDataSource,
    private val etfFundDataSource: EtfFundDataSource,
    private val regionDataSource: RegionDataSource,
    private val percentDataSource: PercentDataSource,
    private val countryDataSource: CountryDataSource,
    private val industryTypeDataSource: IndustryTypeDataSource,
    private val movingAveragesDataSource: MovingAveragesDataSource
) : ScreenerFilterFactory {
    @Suppress("LongMethod", "ComplexMethod") // method is simple and straightforward
    override fun createDefault(type: ScreenerType): ScreenerFilter =
        when (type) {
            ScreenerType.Stock.MarketSegment -> MarketSegmentFilter(
                sector = sectorDataSource.getAllSectorsItem(),
                industry = sectorDataSource.getAllIndustriesItem()
            )
            ScreenerType.Stock.PriceAndVolume -> PriceVolumeFilter()
            ScreenerType.Stock.Opinions -> OpinionsFilter()
            ScreenerType.Stock.Fundamentals -> FundamentalsFilter()
            ScreenerType.Stock.EarningsAndDividends -> EarningsDividendsFilter()
            ScreenerType.Stock.Technicals -> TechnicalsFilter()
            ScreenerType.MutualFund.FundProfile -> FundProfileFilter(
                fundFamilies = setOf(mutualFundDataSource.getAllFundFamiliesItem()),
                fundGroup = mutualFundDataSource.getAllFundGroupsItem(),
                fundCategories = setOf(mutualFundDataSource.getAllFundCategoriesItem())
            )
            ScreenerType.MutualFund.Performance -> PerformanceFilter()
            ScreenerType.MutualFund.Expenses -> ExpensesFilter()
            ScreenerType.MutualFund.Fees -> FeesFilter()
            ScreenerType.MutualFund.Risks -> RiskFilter()
            ScreenerType.MutualFund.Portfolio -> PortfolioFilter()
            ScreenerType.Etf.FundProfile -> EtfFundProfileFilter(
                fundFamily = etfFundDataSource.getAllFundFamiliesItem(),
                etfFundGroup = etfFundDataSource.getAllFundGroupsItem(),
                fundCategories = setOf(etfFundDataSource.getAllFundCategoriesItem())
            )
            ScreenerType.Etf.Performance -> EtfPerformanceFilter()
            ScreenerType.Etf.Risk -> EtfRiskFilter()
            ScreenerType.Etf.Portfolio -> EtfPortfolioFilter(
                region = regionDataSource.getAllRegionsItem(),
                regionPercentInvested = percentDataSource.getEmptyPercent(),
                country = countryDataSource.getAllCountriesItem(),
                countryPercentInvested = percentDataSource.getEmptyPercent(),
                industry = industryTypeDataSource.getAllIndustriesItem(),
                industryType = industryTypeDataSource.getAllTypesItem(),
                industryTypePercentInvested = percentDataSource.getEmptyPercent()
            )
            ScreenerType.Etf.Technicals -> EtfTechnicalsFilter(average = movingAveragesDataSource.getEmptyItem())
        }
}
