package com.etrade.mobilepro.news.briefing.data.rest

import com.etrade.eo.rest.retrofit.Scalar
import io.reactivex.Single
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.http.Body
import retrofit2.http.POST

const val BRIEFING_NEWS_PATH = "e/t/mobile/GetMovingMarket"

interface BriefingNewsApiClient {

    @Scalar
    @POST(BRIEFING_NEWS_PATH)
    fun getBriefingNews(
        @Body request: RequestBody = "StoryId=BRIEFING.COM".toRequestBody("text/plain".toMediaTypeOrNull())
    ): Single<String>
}
