package com.etrade.mobilepro.inboxmessages.dynamicui.api.lowcash

class LowCashPrompt(
    val height: Height,
    val image: String,
    val deeplink: String
) {
    companion object {
        const val PROMPT_INBOX_CODE = "L"
    }
    enum class Height(val value: String) {
        SMALL("40") // for now the only one option
    }
}
