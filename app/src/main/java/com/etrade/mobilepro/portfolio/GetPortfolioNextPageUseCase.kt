package com.etrade.mobilepro.portfolio

import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.backends.mgs.PaginationMeta
import com.etrade.mobilepro.common.di.RemoteDynamicScreen
import com.etrade.mobilepro.dynamic.referencehelper.ReferenceHelper
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.positions.data.dto.PositionListViewDto
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.util.execute
import kotlinx.coroutines.rx2.awaitLast
import javax.inject.Inject

interface GetPortfolioNextPageUseCase : UseCase<GetPortfolioNextPageParameter, Resource<GetPortfolioNextPageResult>>

class GetPortfolioNextPageUseCaseImpl @Inject constructor(
    @RemoteDynamicScreen private val screenDataSource: ScreenDataSource,
    private val referenceHelper: ReferenceHelper
) : GetPortfolioNextPageUseCase {

    override suspend fun execute(parameter: GetPortfolioNextPageParameter): Resource<GetPortfolioNextPageResult> {
        return execute({
            with(parameter) {
                ServicePath.Portfolio(accountUuid, sortInfo.column, sortInfo.order, count, nextPageToken)
            }.let { screenDataSource.getScreen(it).awaitLast() }
        }) {
            mapResource(it)
        }
    }

    private fun mapResource(resource: Resource<BaseMobileResponse>): GetPortfolioNextPageResult? {
        return resource.data?.screen?.run {
            references?.forEach(referenceHelper::persist)
            GetPortfolioNextPageResult(
                views.filterIsInstance<PositionListViewDto>().firstOrNull()?.data?.positions?.map { it.id }.orEmpty(),
                meta?.filterIsInstance<PaginationMeta>()?.firstOrNull()?.nextPage?.continuationToken
            )
        }
    }
}

class GetPortfolioNextPageParameter(
    val accountUuid: String,
    val sortInfo: SortInfo,
    val count: Int,
    val nextPageToken: String?
)

class GetPortfolioNextPageResult(
    val positionIds: List<String> = emptyList(),
    val nextPageToken: String? = null
)
