package com.etrade.mobilepro.livedata

import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import androidx.lifecycle.map
import kotlinx.coroutines.runBlocking
import java.util.concurrent.CountDownLatch

val <T> LiveData<T?>.requireValue: T
    get() = value!!

/**
 * Sets the value to the result of a function that is called when the first `LiveData`s has data
 * or when they receive updates after that.
 */
fun <T, A, B> LiveData<A>.concatCombine(other: LiveData<B>, onChange: (A, B) -> T): MediatorLiveData<T> =
    generalConcatCombine(other) { source1Value, source2Value, assignmentCheck ->
        assignmentCheck(onChange(source1Value, source2Value))
    }

/**
 * Same exact functionality as [concatCombine] but launches a coroutine on the scope passed in
 * @param onChange suspend method to execute your merge functionality, methods inside can be taken to a different Dispatcher
 */
inline fun <T, A, B> LiveData<A>.concatCombineCoroutine(
    other: LiveData<B>,
    crossinline onChange: suspend (A, B) -> T,
    noinline includeSourceTwo: () -> Boolean = { true }
): MediatorLiveData<T> =
    generalConcatCombine(other, includeSourceTwo) { source1Value, source2Value, assignmentCheck ->
        runBlocking {
            assignmentCheck(onChange(source1Value, source2Value))
        }
    }

fun <T, A, B> LiveData<A>.generalConcatCombine(
    other: LiveData<B>,
    includeSourceTwo: () -> Boolean = { true },
    block: (A, B, assignmentCheck: (T) -> Unit) -> Unit
): MediatorLiveData<T> {
    var source1emitted = false
    val result = MediatorLiveData<T>()
    val assignmentCheck: (T) -> Unit = { test ->
        if (test != result.value) {
            result.value = test
        }
    }

    val mergeF = {
        val source1Value = this.value
        val source2Value = other.value

        if (source1emitted && source1Value != null && source2Value != null) {
            block(source1Value, source2Value, assignmentCheck)
        }
    }

    result.addSource(this) { source1emitted = true; mergeF.invoke() }
    result.addSource(other) {
        if (includeSourceTwo()) {
            mergeF.invoke()
        }
    }

    return result
}

/**
 * Combine the latest values emitted by this [LiveData] ([A]) and another [LiveData] ([B]) objects. If either of the [LiveData] objects don't have a value or
 * they are `null`, then the resulting [LiveData] object will not emit a new value.
 *
 * Combinations:
 *
 *    | A    | B    | Result |
 *    |------|------|--------|
 *    | null | null | null   |
 *    | null | B    | null   |
 *    | A    | null | null   |
 *    | A    | B    | A to B |
 *
 * @param b The other [LiveData] object.
 * @param A The type of a value of this [LiveData].
 * @param B The type of a value of the other [LiveData].
 *
 * @return [LiveData] object that contains emits pairs of values `A to B`.
 */
fun <A, B> LiveData<A>.combineLatest(b: LiveData<B>): LiveData<Pair<A, B>> {
    return MediatorLiveData<Pair<A, B>>().apply {
        var lastA: A? = null
        var lastB: B? = null

        addSource(this@combineLatest) {
            if (it == null && value != null) value = null
            lastA = it
            if (lastA != null && lastB != null) value = lastA!! to lastB!!
        }

        addSource(b) {
            if (it == null && value != null) value = null
            lastB = it
            if (lastA != null && lastB != null) value = lastA!! to lastB!!
        }
    }
}

/**
 * Combines `this` result with a result of [another] [LiveData] using [transform] operation. If `this` [LiveData] never emits any value, then result [LiveData]
 * will not emit a value.
 *
 * @param another The other [LiveData] to combine the result with.
 * @param transform The operation to use to combine values of two [LiveData] objects.
 * @param A The type of a value of this [LiveData].
 * @param B The type of a value of the other [LiveData].
 * @param T The type of a value of the result [LiveData].
 */
fun <A, B, T> LiveData<A>.combineWith(another: LiveData<B>, transform: (A, B?) -> T): LiveData<T> {
    return MediatorLiveData<T>().apply {
        var lastA: A? = this@combineWith.value
        var lastB: B? = another.value

        val merge = {
            lastA?.let { value = transform(it, lastB) }
        }

        addSource(this@combineWith) {
            lastA = it
            merge()
        }

        addSource(another) {
            lastB = it
            merge()
        }
    }
}

/**
 * Emits only the first n items of this [LiveData].
 *
 * @param count The number of items which this [LiveData] must emmit.
 * @param A The type of a value of this [LiveData].
 */
fun <A> LiveData<A>.take(count: Int): LiveData<A> {
    return MediatorLiveData<A>().apply {
        var emissions = 0
        addSource(this@take) {
            emissions++
            if (emissions <= count) {
                value = it
            } else {
                removeSource(this@take)
            }
        }
    }
}

/**
 * Discard any items emitted by this [LiveData] after the given predicate condition is met.
 */
fun <A> LiveData<A>.takeUntil(predicate: (A) -> Boolean): LiveData<A> {
    return MediatorLiveData<A>().apply {
        addSource(this@takeUntil) {
            value = it
            if (predicate.invoke(it)) {
                removeSource(this@takeUntil)
            }
        }
    }
}

/**
 * Emits only the first item of this [LiveData].
 *
 * @param A The type of a value of this [LiveData].
 */
fun <A> LiveData<A>.first(): LiveData<A> = take(1)

/**
 * Combines `this` result with a result of [other] [LiveData] using [transform] operation. If `this` [LiveData] never emits any value, then result [LiveData]
 * will not emit a value.
 *
 * @param other The other [LiveData] to combine the result with.
 * @param transform The operation to use to combine values of two [LiveData] objects.
 * @param A The type of a value of this [LiveData].
 * @param B The type of a value of the other [LiveData].
 * @param C The type of a value of the result [LiveData].
 */
fun <A, B, C> LiveData<A>.combine(other: LiveData<B>, transform: (A?, B?) -> C): LiveData<C> {
    return MediatorLiveData<C>().apply {
        var lastA: A? = this@combine.value
        var lastB: B? = other.value

        val merge = {
            value = transform(lastA, lastB)
        }

        addSource(this@combine) {
            lastA = it
            merge()
        }

        addSource(other) {
            lastB = it
            merge()
        }
    }
}

/**
 * Combines `this` result with a result of [second] [LiveData] and [third] [LiveData] using [transform] operation.
 * If `this` [LiveData] never emits any value, then result [LiveData]
 * will not emit a value.
 *
 * @param second The second [LiveData] to combine the result with.
 * @param third The third [LiveData] to combine the result with.
 * @param transform The operation to use to combine values of three [LiveData] objects.
 * @param A The type of a value of this [LiveData].
 * @param B The type of a value of the second [LiveData].
 * @param C The type of a value of the third [LiveData].
 * @param D The type of a value of the result [LiveData].
 */
fun <A, B, C, D> LiveData<A>.combineWithTwo(second: LiveData<B>, third: LiveData<C>, transform: (A?, B?, C?) -> D): LiveData<D> {
    return MediatorLiveData<D>().apply {
        var lastA: A? = this@combineWithTwo.value
        var lastB: B? = second.value
        var lastC: C? = third.value

        val merge = {
            value = transform(lastA, lastB, lastC)
        }

        addSource(this@combineWithTwo) {
            lastA = it
            merge()
        }

        addSource(second) {
            lastB = it
            merge()
        }

        addSource(third) {
            lastC = it
            merge()
        }
    }
}

/**
 * This resembles doOnNext operator from Rx
 * @param sideEffect The passed in closure is called every time the value of the livedata changes and is not null
 *                   The closure receives the current value
 *
 * @return the same livedata(object under the hood is changed, though) with intact value
 */
inline fun <T> LiveData<T>.doOnNext(crossinline sideEffect: (T) -> Unit): LiveData<T> =
    Transformations.map(this) {
        if (it != null) {
            sideEffect(it)
        }
        it
    }

/**
 * Combine multiple LiveData sources into one by merging their emissions.
 */
fun <T> LiveData<T>.merge(vararg sources: LiveData<T>): LiveData<T> {
    return MediatorLiveData<T>().apply {
        addSource(this@merge) { value = it }
        for (source in sources) {
            addSource(source) { value = it }
        }
    }
}

/**
 * merge the latest values emitted by this [LiveData] ([A]) and another [LiveData] ([B]) objects. If either of the [LiveData] objects has a value ,
 * then the resulting [LiveData] object will emit a new value.
 *
 * Combinations:
 *
 *    | A    | B    | Result    |
 *    |------|------|-----------|
 *    | null | null | null      |
 *    | null | B    | null to B |
 *    | A    | null | A to null |
 *    | A    | B    | A to B    |
 *
 * @param b The other [LiveData] object.
 * @param A The type of a value of this [LiveData].
 * @param B The type of a value of the other [LiveData].
 *
 * @return [LiveData] object that contains emits pairs of values `A to B`.
 */
fun <A, B> LiveData<A>.mergeLatest(b: LiveData<B>): LiveData<Pair<A?, B?>> {
    return MediatorLiveData<Pair<A?, B?>>().apply {
        var lastA: A? = null
        var lastB: B? = null

        addSource(this@mergeLatest) {
            lastA = it
            value = lastA to lastB
        }

        addSource(b) {
            lastB = it
            value = lastA to lastB
        }
    }
}

/**
 * This waits for the first value(if it still has no, i.e. if current value is null) in blocking way
 */
fun <T> LiveData<T>.waitForFirst(): T? {
    val latch = CountDownLatch(1)
    var value: T? = null
    val observer = Observer<T> {
        value = it
        latch.countDown()
    }
    this.observeForever(observer)
    latch.await()
    this.removeObserver(observer)
    return value
}

/**
 * Filters items emitted by a [LiveData] by only emitting those that satisfy the given predicate
 */
fun <T> LiveData<T>.filter(predicate: (T) -> Boolean): LiveData<T> = MediatorLiveData<T>().apply {
    addSource(this@filter) {
        if (predicate(it)) value = it
    }
}

/**
 * Filters items emitted by a [LiveData] by only emitting those that are not null
 */
fun <T> LiveData<T?>.filterNotNull(): LiveData<T> = filter { it != null }.map { it!! }

/**
 * Wrapper function to avoid observer declaration on client side
 */
inline fun <T> LiveData<T>.observeBy(
    owner: LifecycleOwner,
    crossinline onChanged: (T) -> Unit
): Observer<T> {
    val wrappedObserver = Observer<T> { t -> onChanged.invoke(t) }
    observe(owner, wrappedObserver)
    return wrappedObserver
}

/** Helper function to observe changes within a [Fragment]'s lifecycle  */
inline fun <T> LiveData<T>.observeIn(
    fragment: Fragment,
    crossinline onChanged: (T) -> Unit
): Observer<T> = observeBy(fragment.viewLifecycleOwner, onChanged)

/**
 * Wrapper function to observer forever while having a function reference to stop observing
 * its data source.
 * Short notation for anonymous functions has the limitation that they cannot referenced
 * its compiled class (e.x by using [this]), thus forcing the developer to use full notation for
 * anonymous classes instead of taking advantage of SAM conversion. This extension function address
 * that problem.
 *
 * https://stackoverflow.com/a/29985138
 */
inline fun <T> LiveData<T>.observeForever(
    crossinline block: Observer<T>.(item: T, stopObserving: () -> Unit) -> Unit
): Observer<T> {
    return object : Observer<T> {
        override fun onChanged(item: T) {
            block(item) { this@observeForever.removeObserver(this) }
        }
    }.also { observeForever(it) }
}

/**
 * Skips first @param count emits
 */
fun <T> LiveData<T>.skip(count: Int): LiveData<T> {
    var counter = 0
    return this.filter {
        counter++
        counter > count
    }
}
