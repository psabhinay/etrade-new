package com.etrade.mobilepro.quote.screen.presentation.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemChainBinding
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.android.accessibility.bindButtonAccessibility

class QuoteChainViewHolder(
    private val binding: QuoteItemChainBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(
        item: QuoteItem.QuoteChainItem?,
        level2ClickListener: (() -> Unit)? = null,
        optionsChainClickListener: ((SearchResultItem.Symbol) -> Unit)? = null
    ) {
        item ?: return
        with(binding) {
            level2Quote.bindButtonAccessibility()
            level2Quote.setOnClickListener {
                level2ClickListener?.invoke()
            }
            optionsChain.bindButtonAccessibility()
            optionsChain.setOnClickListener {
                optionsChainClickListener?.invoke(item.symbol)
            }
            when (item.symbol.type) {
                InstrumentType.INDX -> {
                    level2Quote.visibility = View.GONE
                    divider.visibility = View.GONE
                }
                else -> {
                    level2Quote.visibility = View.VISIBLE
                    divider.visibility = View.VISIBLE
                }
            }
        }
    }
}
