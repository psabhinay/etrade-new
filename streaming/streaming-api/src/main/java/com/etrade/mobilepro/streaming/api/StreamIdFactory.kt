package com.etrade.mobilepro.streaming.api

import com.etrade.mobilepro.instrument.InstrumentType

/**
 * A factory to create stream ids for different applications.
 *
 * @param P Custom payload that may be required to create a stream ID.
 */
interface StreamIdFactory<P> {

    /**
     * Creates stream id.
     *
     * @param symbol The symbol.
     * @param instrumentType The instrument type of the symbol.
     * @param payload Custom payload.
     *
     * @return Stream id.
     */
    fun createStreamId(symbol: String, instrumentType: InstrumentType, payload: P? = null): String
}
