package com.etrade.mobilepro.feedback

import com.etrade.mobilepro.feedback.api.FeedbackRepository
import com.etrade.mobilepro.feedback.api.FeedbackRequestFactory
import com.etrade.mobilepro.feedback.data.DefaultFeedbackRepository
import com.etrade.mobilepro.feedback.data.FeedbackService
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
interface FeedbackModule {

    @Binds
    @Singleton
    fun bindFeedbackRequestFactory(factory: DefaultFeedbackRequestFactory): FeedbackRequestFactory

    companion object {
        @Provides
        @Singleton
        fun provideFeedbackRepository(feedbackService: FeedbackService): FeedbackRepository = DefaultFeedbackRepository(feedbackService)
    }
}
