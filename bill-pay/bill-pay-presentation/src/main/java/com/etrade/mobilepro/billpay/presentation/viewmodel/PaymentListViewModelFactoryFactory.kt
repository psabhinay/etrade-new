package com.etrade.mobilepro.billpay.presentation.viewmodel

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.presentation.paymentlist.PaymentListViewModel
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

interface PaymentListViewModelFactoryFactory {
    fun create(
        sessionId: String?,
        allAccountList: List<AccountDetails>?
    ): ViewModelProvider.Factory
}

class PaymentListViewModelFactoryFactoryImp @Inject constructor(
    private val factory: PaymentListViewModel.Factory
) : PaymentListViewModelFactoryFactory {
    override fun create(
        sessionId: String?,
        allAccountList: List<AccountDetails>?
    ): ViewModelProvider.Factory =
        viewModelFactory { factory.create(sessionId, allAccountList) }
}
