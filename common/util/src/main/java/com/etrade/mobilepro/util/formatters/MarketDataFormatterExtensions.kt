package com.etrade.mobilepro.util.formatters

import com.etrade.eo.core.util.MarketDataFormatter
import java.math.BigDecimal

private val LARGE_TRADE_SIZE = BigDecimal("10000")

/**
 * Format we want where if value is less than one, up to 4 decimal places, otherwise 2 decimal places
 */
fun MarketDataFormatter.formatMarketDataCustom(decimal: BigDecimal?): String = decimal?.let {
    if (it.abs() < BigDecimal.ONE) {
        numberFormatter.format(decimal)
    } else {
        formatPriceToBeUpdated(decimal)
    }
} ?: EMPTY_FORMATTED_VALUE

/**
 * Format we want where if value is less than one, up to 4 decimal places, otherwise 2 decimal places
 * besides it add the percent (%) char at the end
 */
fun MarketDataFormatter.formatMarketPercentDataCustom(decimal: BigDecimal?): String = decimal?.let {
    formatMarketDataCustom(decimal).plus("%")
} ?: EMPTY_FORMATTED_VALUE

/**
 * Format similar case where if less than one, up to 4 decimal places, other wise two, but this format contains commas
 */
fun MarketDataFormatter.formatMarketDataCustomWithoutDollarSign(decimal: BigDecimal?): String = decimal?.let {
    if (it.abs() < BigDecimal.ONE) {
        numberFormatter.format(it)
    } else {
        formatMarketDataWithoutDollarSign(it)
    }
} ?: EMPTY_FORMATTED_VALUE

/**
 * If volume is less than 10K we use as is, if it is greater we format with a suffix using volumeIntegerFormat
 */
fun MarketDataFormatter.formatVolumeCustom(volume: BigDecimal?): String = volume?.let {
    if (it >= LARGE_TRADE_SIZE) {
        formatVolumeInteger(it)
    } else {
        volume.toPlainString()
    }
} ?: EMPTY_FORMATTED_VALUE
