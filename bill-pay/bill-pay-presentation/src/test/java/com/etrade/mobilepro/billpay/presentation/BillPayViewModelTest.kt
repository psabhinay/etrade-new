package com.etrade.mobilepro.billpay.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.BillPayStatus
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.usecase.BillPayInitalDataLoaderResult
import com.etrade.mobilepro.billpay.usecase.BillPayInitialSetupUseCase
import com.etrade.mobilepro.billpay.usecase.PayeeListUseCaseImpl
import com.etrade.mobilepro.common.RestorableViewModelDelegate
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelper
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentMatchers.anyString

@ExperimentalCoroutinesApi
internal class BillPayViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    private val mockRepo = mock<BillPayRepo>()
    private val mockBillPayInitialSetupUseCase = mock<BillPayInitialSetupUseCase>()
    private val mockViewPagerTabsTrackerHelper: ViewPagerTabsTrackerHelper = mock()
    private val mockRestorableDelegate: RestorableViewModelDelegate = mock()

    private lateinit var sut: BillPayViewModel

    @Before
    fun setUp() {
        Dispatchers.setMain(testCoroutineDispatcher)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `loadBillPayDetails should return successfully when InitialSetupUseCase is successful`() {
        runBlocking(testCoroutineDispatcher) {
            whenever(mockBillPayInitialSetupUseCase.execute(Unit)).thenReturn(ETResult.success(BillPayInitalDataLoaderResult(BillPayStatus.Supported)))
            sut = BillPayViewModel(PayeeListUseCaseImpl(mockRepo), mockBillPayInitialSetupUseCase, mockViewPagerTabsTrackerHelper, mockRestorableDelegate)
        }

        sut.loadBillPayDetails()

        sut.viewState.observeForever {
            assertEquals(false, it.isLoading)
            assertEquals(BillPayStatus.Supported, it.status?.peekContent())
        }
    }

    @Test
    fun `loadBillPayDetails should return failure when InitialSetupUseCase fails`() {
        runBlocking(testCoroutineDispatcher) {
            whenever(mockBillPayInitialSetupUseCase.execute(Unit)).thenReturn(ETResult.failure(Throwable("bad things")))
            sut = BillPayViewModel(PayeeListUseCaseImpl(mockRepo), mockBillPayInitialSetupUseCase, mockViewPagerTabsTrackerHelper, mockRestorableDelegate)
        }

        sut.loadBillPayDetails()

        sut.viewState.observeForever {
            assertEquals(false, it.isLoading)
            assertEquals("bad things", it.error?.error?.message)
        }
    }

    @Test
    fun `onPayeeAdded should return successfully when sessionID is valid`() {
        val mockPayee = Payee("1", "test1", "", "1234", "0", -1, false)
        runBlocking(testCoroutineDispatcher) {
            whenever(mockRepo.getPayeesList(anyString())).thenReturn(
                ETResult.success(
                    listOf(
                        Payee("1", "test1", "", "1234", "0", -1, false),
                        Payee("2", "test2", "", "1235", "0", -1, false)
                    )
                )
            )
            whenever(mockRepo.getAccounts()).thenReturn(ETResult.success(emptyList()))
            whenever(mockBillPayInitialSetupUseCase.execute(Unit)).thenReturn(
                ETResult.success(
                    BillPayInitalDataLoaderResult(
                        status = BillPayStatus.Supported,
                        sessionId = "123456"
                    )
                )
            )
            sut = BillPayViewModel(PayeeListUseCaseImpl(mockRepo), mockBillPayInitialSetupUseCase, mockViewPagerTabsTrackerHelper, mockRestorableDelegate)
        }

        sut.loadBillPayDetails()
        sut.onPayeeAdded(mockPayee)

        sut.addedPayee.observeForever {
            assertEquals(mockPayee, it.peekContent())
        }
    }

    @Test
    fun `onPayeeAdded should throw IllegalStateException when sessionID is invalid or null`() = runBlockingTest(testCoroutineDispatcher) {
        val mockPayee = Payee("1", "test1", "", "1234", "0", -1, false)
        sut = BillPayViewModel(PayeeListUseCaseImpl(mockRepo), mockBillPayInitialSetupUseCase, mockViewPagerTabsTrackerHelper, mockRestorableDelegate)
        val result = runCatching {
            sut.onPayeeAdded(mockPayee)
        }

        assertEquals("BillPaySessionId is null", result.exceptionOrNull()?.message)
    }
}
