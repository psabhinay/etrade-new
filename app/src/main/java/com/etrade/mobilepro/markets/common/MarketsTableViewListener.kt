package com.etrade.mobilepro.markets.common

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.DataCellViewHolder
import com.etrade.mobilepro.tableviewutils.presentation.holder.SymbolRowHeaderViewHolder
import com.etrade.mobilepro.tableviewutils.presentation.sorting.SortableTableViewListener
import com.etrade.mobilepro.tableviewutils.presentation.sorting.TableDataSortable

class MarketsTableViewListener(
    private val cellClickListener: ((String) -> Unit)? = null,
    private val rowHeaderClickListener: ((String, InstrumentType) -> Unit)? = null,
    sortable: TableDataSortable? = null
) : SortableTableViewListener(sortable) {

    /**
     * Called when user click any cell item.
     *
     * @param cellView : Clicked Cell ViewHolder.
     * @param column : X (Column) position of Clicked Cell item.
     * @param row : Y (Row) position of Clicked Cell item.
     */
    override fun onCellClicked(cellView: RecyclerView.ViewHolder, column: Int, row: Int) {
        cellClickListener?.let { viewHolderListener ->
            val cell: DataCell? = (cellView as DataCellViewHolder).cell
            cell?.id?.let {
                viewHolderListener.invoke(it)
            }
        }
    }

    /**
     * Called when user long press any cell item.
     *
     * @param cellView : Long Pressed Cell ViewHolder.
     * @param column : X (Column) position of Long Pressed Cell item.
     * @param row : Y (Row) position of Long Pressed Cell item.
     */
    override fun onCellLongPressed(
        cellView: RecyclerView.ViewHolder,
        column: Int,
        row: Int
    ) {
        // unused
    }

    /**
     * Called when user long press any column header item.
     *
     * @param columnHeaderView : Long Pressed Column Header ViewHolder.
     * @param column : X (Column) position of Long Pressed Column Header item.
     */
    override fun onColumnHeaderLongPressed(columnHeaderView: RecyclerView.ViewHolder, column: Int) {
        // unused
    }

    /**
     * Called when user click any Row Header item.
     *
     * @param rowHeaderView : Clicked Row Header ViewHolder.
     * @param row : Y (Row) position of Clicked Row Header item.
     */
    override fun onRowHeaderClicked(rowHeaderView: RecyclerView.ViewHolder, row: Int) {
        rowHeaderClickListener?.let {
            val cell: SymbolCell? = (rowHeaderView as SymbolRowHeaderViewHolder).cell
            cell?.run { it.invoke(symbol, instrumentType) }
        }
    }

    /**
     * Called when user long press any row header item.
     *
     * @param rowHeaderView : Long Pressed Row Header ViewHolder.
     * @param row : Y (Row) position of Long Pressed Row Header item.
     */
    override fun onRowHeaderLongPressed(rowHeaderView: RecyclerView.ViewHolder, row: Int) {
        // unused
    }
}
