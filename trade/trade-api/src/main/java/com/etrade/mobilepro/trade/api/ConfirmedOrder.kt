package com.etrade.mobilepro.trade.api

interface ConfirmedOrder : Order {
    val confirmationOrderId: String?
    val placedDate: String?
}
