package com.etrade.mobilepro.markets.futures.widget

import androidx.annotation.ColorRes
import androidx.lifecycle.MutableLiveData
import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.indices.Futures
import com.etrade.mobilepro.indices.IndexDataWithColor
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.data.response.FutureQuote
import com.etrade.mobilepro.util.android.ChangeColorPicker
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.formatters.formatMarketPercentDataCustom
import com.etrade.mobilepro.util.market.time.NEW_YORK_ZONE_ID
import com.etrade.mobilepro.util.safeParseBigDecimal
import kotlinx.coroutines.rx2.await
import org.slf4j.LoggerFactory
import org.threeten.bp.Instant
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.DateTimeParseException
import java.util.Locale
import javax.inject.Inject

interface FutureIndicesLoader {
    suspend fun fetchFutureIndices(
        futuresTemporalDescriptionConsumer: MutableLiveData<Futures>,
        futuresDataConsumer: MutableLiveData<List<IndexDataWithColor>>,
        alsoDoOnSuccess: () -> Unit,
        alsoDoOnError: (Throwable?) -> Unit
    )
}

class DefaultFutureIndicesLoader @Inject constructor(
    @ColorRes private val dowJonesColor: Int,
    @ColorRes private val nasdaqColor: Int,
    @ColorRes private val sP500Color: Int,
    private val service: MoverService,
    private val changeColorPicker: ChangeColorPicker
) : FutureIndicesLoader {
    private val formatter = DateTimeFormatter.ofPattern("MM/dd/yy h:mma", Locale.US).withZone(NEW_YORK_ZONE_ID)
    private val logger = LoggerFactory.getLogger(this::class.java)

    override suspend fun fetchFutureIndices(
        futuresTemporalDescriptionConsumer: MutableLiveData<Futures>,
        futuresDataConsumer: MutableLiveData<List<IndexDataWithColor>>,
        alsoDoOnSuccess: () -> Unit,
        alsoDoOnError: (Throwable?) -> Unit
    ) {
        service.getMarketFutures()
            .runCatching { await() }
            .fold(
                onSuccess = { response ->
                    response.quotes
                        .takeUnless { it.isNullOrEmpty() }
                        ?.also {
                            getFuturesTemporalDescription(it[0])?.let { description ->
                                futuresTemporalDescriptionConsumer.postValue(description)
                            }
                        }
                        ?.mapIndexedNotNull(::mapFutureQuoteToIndexData)
                        .takeUnless { it.isNullOrEmpty() }
                        ?.let {
                            futuresDataConsumer.postValue(it)
                            alsoDoOnSuccess()
                        } ?: alsoDoOnError(null)
                },
                onFailure = alsoDoOnError
            )
    }

    private fun getFuturesTemporalDescription(quote: FutureQuote): Futures? {
        return try {
            val millis = Instant.from(formatter.parse("${quote.lastTradeDate} ${quote.lastTradeTime}")).toEpochMilli()
            val formattedDateTime = DateFormattingUtils.formatFullShortTimeDateWithSlashes(millis)
            Futures(formattedDateTime, quote.expireDate)
        } catch (ex: DateTimeParseException) {
            logger.error("failed to parse futures time stamp", ex)

            null
        }
    }

    private fun mapFutureQuoteToIndexData(index: Int, quote: FutureQuote): IndexDataWithColor? {
        return quote.symbol?.let { symbol ->
            quote.price?.let { price ->
                val change = quote.change?.safeParseBigDecimal()
                val textColor = changeColorPicker.getChangeColor(change)
                val percentChangeWithoutSign = quote.percentchange?.trim()?.dropLast(1)
                val percentChange: String =
                    MarketDataFormatter.formatMarketPercentDataCustom(percentChangeWithoutSign?.toBigDecimalOrNull())
                val formattedChange: String = MarketDataFormatter.formatMarketDataCustom(change)
                IndexDataWithColor(
                    symbol,
                    getFuturesDisplaySymbol(symbol),
                    price,
                    formattedChange,
                    percentChange,
                    textColorRes = textColor,
                    indicatorColor = getIndicatorColor(index),
                    isPositiveChange = change?.signum() != -1
                )
            }
        }
    }

    private fun getFuturesDisplaySymbol(symbol: String): String {
        return when (symbol) {
            "YM" -> "DOW"
            "NQ" -> "NASDAQ"
            "ES" -> "S&P 500"
            else -> symbol
        }
    }

    @ColorRes
    private fun getIndicatorColor(index: Int): Int {
        return when (index) {
            0 -> dowJonesColor
            1 -> nasdaqColor
            else -> sP500Color
        }
    }
}
