package com.etrade.mobilepro.messagecenter.presentation.details.support

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.api.model.ReferenceBook
import com.etrade.mobilepro.messagecenter.api.model.SupportMessage
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterReferenceBookDataSource
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterRepo
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.SupportMessageHolder
import kotlinx.coroutines.launch
import org.threeten.bp.Instant
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

private const val STATUS_MESSAGE_SENT = "Message Sent"

class SupportMessageDetailsViewModel @Inject constructor(
    private val repo: MessageCenterRepo,
    private val referenceBookDataSource: MessageCenterReferenceBookDataSource
) : ViewModel() {
    private val supportMsgDateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm a").withZone(ZoneId.systemDefault())
    private val _viewState = MutableLiveData<SupportMessageViewState>()
    internal val viewState: LiveData<SupportMessageViewState>
        get() = _viewState

    internal fun refresh(message: Message, forced: Boolean) {
        val current = _viewState.value
        if (current == null || forced) {
            _viewState.value = current?.copy(status = SupportMessageViewState.Status.Loading) ?: SupportMessageViewState(
                status = SupportMessageViewState.Status.Loading
            )
            viewModelScope.launch {
                val referenceBook = referenceBookDataSource.getCachedReferenceBook() ?: referenceBookDataSource.fetchReferenceBook().getOrNull()
                if (referenceBook == null) {
                    _viewState.updateValue { it.copy(status = SupportMessageViewState.Status.Error { refresh(message, forced = true) }) }
                    return@launch
                }
                repo
                    .getSupportMessageDetails(message)
                    .fold(
                        onSuccess = { supportMessage ->
                            handleSuccessRefresh(supportMessage, message, referenceBook)
                        },
                        onFailure = {
                            _viewState.updateValue { it.copy(status = SupportMessageViewState.Status.Error { refresh(message, forced = true) }) }
                        }
                    )
            }
        }
    }

    internal fun replyContentChanged(content: String) {
        _viewState.value?.setReplyContent(content)
    }

    internal fun replyTapped() =
        _viewState.updateValue {
            it.copyWithoutTouchingRefreshFlag(replyBlockIsVisible = true, bottomButtonsStatus = SupportMessageViewState.BottomButtonsStatus.SendAndCancel)
        }

    internal fun cancelTapped() {
        _viewState.updateValue {
            it.copyWithoutTouchingRefreshFlag(replyBlockIsVisible = false, bottomButtonsStatus = SupportMessageViewState.BottomButtonsStatus.ReplyInvitation)
        }
        _viewState.value?.setReplyContent("")
    }

    internal fun sendReplyTapped() {
        _viewState.value = _viewState.value?.copy(
            status = SupportMessageViewState.Status.Loading,
            replyBlockIsVisible = false
        ) ?: SupportMessageViewState(status = SupportMessageViewState.Status.Loading)
        viewModelScope.launch {
            val content = _viewState.value?.getReplyContent() ?: ""
            _viewState.value?.itemsHolder?.initialMessage?.let { message ->
                repo.sendReply(
                    caseNo = message.caseNo,
                    msgId = message.id,
                    content = content
                ).fold(
                    onSuccess = { newHistory ->
                        handleSuccessReply(newHistory)
                    },
                    onFailure = {
                        _viewState.updateValue { it.copy(status = SupportMessageViewState.Status.Error { sendReplyTapped() }) }
                    }
                )
            }
        }
    }

    private fun initialBottomButtonsStatus(itemsHolder: SupportMessageHolder): SupportMessageViewState.BottomButtonsStatus =
        if (itemsHolder.header.status != STATUS_MESSAGE_SENT && itemsHolder.header.replyEnabled) {
            SupportMessageViewState.BottomButtonsStatus.ReplyInvitation
        } else {
            SupportMessageViewState.BottomButtonsStatus.Hidden
        }

    private fun handleSuccessRefresh(
        supportMessage: SupportMessage,
        message: Message,
        referenceBook: ReferenceBook
    ): Boolean {
        val itemsHolder = SupportMessageHolder.createFrom(supportMessage, message, referenceBook)
        val bottomButtonsStatus = initialBottomButtonsStatus(itemsHolder)
        return _viewState.updateValue {
            it.copy(
                status = SupportMessageViewState.Status.Idle,
                itemsHolder = itemsHolder,
                _needToRefresh = true,
                bottomButtonsStatus = bottomButtonsStatus
            )
        }
    }

    private fun handleSuccessReply(newHistory: List<SupportMessage.Communication>): Boolean {
        val replySentAt = Instant.now()
        return _viewState.updateValue {
            it.copy(
                status = SupportMessageViewState.Status.Idle,
                itemsHolder = it.itemsHolder
                    .withNewHistory(
                        history = newHistory,
                        latestMessageCreatedAt = replySentAt,
                        latestMessageCreatedAtFormatted = supportMsgDateFormatter.format(replySentAt)
                    )
                    .withNewHeaderStatus(STATUS_MESSAGE_SENT),
                _messageSent = true,
                _needToRefresh = true,
                bottomButtonsStatus = SupportMessageViewState.BottomButtonsStatus.Hidden,
                replyBlockIsVisible = false
            )
        }
    }
}
