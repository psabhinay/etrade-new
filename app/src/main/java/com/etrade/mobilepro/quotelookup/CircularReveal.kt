package com.etrade.mobilepro.quotelookup

import android.animation.Animator
import android.content.Context
import android.graphics.Point
import android.transition.TransitionValues
import android.transition.Visibility
import android.util.AttributeSet
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import androidx.annotation.IdRes
import com.etrade.mobilepro.R
import com.etrade.mobilepro.util.android.animutil.AnimUtils

/**
 * A transition which shows/hides a view with a circular clipping mask. Callers should provide the
 * center point of the reveal either [directly][.setCenter] or by
 * [specifying][.centerOn] another view to center on; otherwise the target `view`'s
 * pivot point will be used.
 */
internal class CircularReveal : Visibility {

    private var center: Point? = null
    private var startRadius: Float = 0.0f
    private var endRadius: Float = 0.0f
    @IdRes
    private var centerOnId = View.NO_ID
    private val centerOn: View? = null

    constructor() : super() {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.CircularReveal)
        startRadius = a.getDimension(R.styleable.CircularReveal_startRadius, 0f)
        endRadius = a.getDimension(R.styleable.CircularReveal_endRadius, 0f)
        centerOnId = a.getResourceId(R.styleable.CircularReveal_centerOn, View.NO_ID)
        a.recycle()
    }

    /**
     * The center point of the reveal or conceal, relative to the target `view`.
     */
    fun setCenter(center: Point) {
        this.center = center
    }

    override fun onAppear(
        sceneRoot: ViewGroup,
        view: View?,
        startValues: TransitionValues,
        endValues: TransitionValues
    ): Animator? {
        if (view == null || view.height == 0 || view.width == 0) return null
        ensureCenterPoint(sceneRoot, view)
        return center?.y?.let { y ->
            center?.x?.let { x ->
                getFullyRevealedRadius(view)?.let {
                    ViewAnimationUtils.createCircularReveal(
                        view,
                        x,
                        y,
                        startRadius,
                        it
                    )
                }
            }
        }?.let { AnimUtils.NoPauseAnimator(it) }
    }

    override fun onDisappear(
        sceneRoot: ViewGroup,
        view: View?,
        startValues: TransitionValues,
        endValues: TransitionValues
    ): Animator? {
        if (view == null || view.height == 0 || view.width == 0) return null
        ensureCenterPoint(sceneRoot, view)
        return center?.x?.let { x ->
            center?.y?.let { y ->
                getFullyRevealedRadius(view)?.let {
                    ViewAnimationUtils.createCircularReveal(
                        view,
                        x,
                        y,
                        it,
                        endRadius
                    )
                }
            }
        }?.let { AnimUtils.NoPauseAnimator(it) }
    }

    private fun ensureCenterPoint(sceneRoot: ViewGroup, view: View) {
        if (center != null) return
        if (centerOn != null || centerOnId != View.NO_ID) {
            val source: View?
            if (centerOn != null) {
                source = centerOn
            } else {
                source = sceneRoot.findViewById(centerOnId)
            }
            if (source != null) {
                // use window location to allow views in diff hierarchies
                val loc = IntArray(2)
                source.getLocationInWindow(loc)
                val srcX = loc[0] + source.width / 2
                val srcY = loc[1] + source.height / 2
                view.getLocationInWindow(loc)
                center = Point(srcX - loc[0], srcY - loc[1])
            }
        }
        // else use the pivot point
        if (center == null) {
            center = Point(Math.round(view.pivotX), Math.round(view.pivotY))
        }
    }

    private fun getFullyRevealedRadius(view: View): Float ? {
        return center?.x?.let { x -> Math.max(x, view.width - x).toDouble() }?.let {
            center?.y?.let { y -> Math.max(y, view.height - y).toDouble() }?.let { it2 ->
                Math.hypot(
                    it,
                    it2
                ).toFloat()
            }
        }
    }
}
