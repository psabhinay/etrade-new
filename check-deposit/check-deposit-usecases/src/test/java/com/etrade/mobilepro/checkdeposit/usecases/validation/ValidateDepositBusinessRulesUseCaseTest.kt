package com.etrade.mobilepro.checkdeposit.usecases.validation

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.checkdeposit.deposit.service.RemoteCheckDepositService
import com.etrade.mobilepro.checkdeposit.validate.ValidateDepositBusinessRulesResponseDto
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.util.invoke
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

internal class ValidateDepositBusinessRulesUseCaseTest {

    private fun mockServiceCall(path: String): RemoteCheckDepositService {
        return mock {
            onBlocking { validateDepositBusinessRules(any()) } doReturn
                ValidateDepositBusinessRulesUseCaseTest::class.getObjectFromJson(
                    path,
                    ServerResponseDto::class.java,
                    ValidateDepositBusinessRulesResponseDto::class.java
                )
        }
    }

    private fun testUseCase(path: String, verify: ((value: ValidationResult) -> ValidationResult, (exception: Throwable) -> Unit) -> Unit) {
        val service = mockServiceCall(path)
        val case = ValidateDepositBusinessRulesUseCaseImpl(service)

        val result = runBlocking { case(ValidateDepositBusinessRulesRequest("", "", "", "")) }

        assertNotNull(result)

        val onSuccess: (value: ValidationResult) -> ValidationResult = mock()
        val onFailure: (exception: Throwable) -> Unit = mock()

        result.fold(
            onSuccess = onSuccess,
            onFailure = onFailure
        )

        verify(onSuccess, onFailure)
    }

    @Test
    fun `result should be Invalid if errors present`() {
        testUseCase("validatedepositbusinessrules_invalid.json") { onSuccess, onFailure ->
            verify(onFailure, never()).invoke(any())
            verify(onSuccess).invoke(
                ValidationResult.Invalid(
                    message = "You have entered a check amount that exceeds the daily limit of \$100,000. <BR/>Please consider mailing the check to E*TRADE.",
                    code = "CHECK_DEPOSIT_LIMIT_EXCEEDED"
                )
            )
        }
    }

    @Test
    fun `result should be Valid if errors present`() {
        testUseCase("validatedepositbusinessrules_valid.json") { onSuccess, onFailure ->
            verify(onFailure, never()).invoke(any())
            verify(onSuccess).invoke(ValidationResult.Valid)
        }
    }
}
