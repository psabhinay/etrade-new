package com.etrade.mobilepro.trade.dropdown

import android.content.res.Resources
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commitNow
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.dropdown.CommonDropDownItemConverter
import com.etrade.mobilepro.dropdown.DropDownItem
import com.etrade.mobilepro.dropdown.DropDownItemConverter
import com.etrade.mobilepro.dropdown.DropDownManager
import com.etrade.mobilepro.trade.dropdown.help.TradeHelpParams
import org.slf4j.LoggerFactory

class TradeSelector<T>(
    private val supportFragmentManager: FragmentManager,
    private val resources: Resources,
    private val itemConverter: DropDownItemConverter<T> = CommonDropDownItemConverter(resources)::convert
) : DropDownManager<T> {

    override val view: View?
        get() = dialogFragment?.view

    private val logger = LoggerFactory.getLogger(TradeSelector::class.java)

    private var dialogFragment: TradeSelectBottomSheetFragment? = null
    private var holder: TradeSelectorHolder? = null
    private var tag = "TRADE_SELECTOR_DIALOG_FRAGMENT"
    private var dropDownItems: List<T> = emptyList()

    override fun init(
        tag: String,
        title: String,
        items: List<T>,
        initialSelectedPosition: Int?,
        dividerPosition: Int?,
        toolbarActionEnd: ToolbarActionEnd?
    ) {
        init(tag, title, items, initialSelectedPosition)
    }

    fun init(
        tag: String,
        title: String,
        items: List<T>,
        initialSelectedPosition: Int?,
        helpParams: TradeHelpParams? = null
    ) {
        this.tag = tag
        if (dialogFragment == null) {
            dialogFragment = supportFragmentManager.findFragmentByTag(tag) as TradeSelectBottomSheetFragment?
                ?: TradeSelectBottomSheetFragment()
        }

        val dropDownItems: List<DropDownItem> = items.map(itemConverter)
        holder = TradeSelectorHolder(
            title,
            dropDownItems,
            helpParams = helpParams
        ).also {
            it.updateSelectedPosition(initialSelectedPosition)
            dialogFragment?.holder = it
        }
        this.dropDownItems = items
    }

    override fun updateInitialSelectedPosition(initialSelectedPosition: Int?) {
        holder?.updateSelectedPosition(initialSelectedPosition)
    }

    override fun openDropDown() {
        if (supportFragmentManager.isDestroyed) {
            return
        }
        if (dialogFragment?.isAdded == false) {
            try {
                supportFragmentManager.findFragmentByTag(tag)?.let {
                    supportFragmentManager.commitNow {
                        remove(it)
                    }
                }
                dialogFragment?.showNow(supportFragmentManager, tag)
            } catch (e: IllegalStateException) {
                // See https://stackoverflow.com/questions/7575921/illegalstateexception-can-not-perform-this-action-after-onsaveinstancestate-wit
                logger.error("Could not show Watchlist dropdown: Commit error \n$e")
            }
        }
    }

    override fun closeDropDown() {
        dialogFragment
            ?.takeIf { it.isAdded }
            ?.also { it.dismiss() }
    }

    override fun setListener(listener: (position: Int, item: T) -> Unit) {
        holder?.listener = { position ->
            dropDownItems.getOrNull(position)?.let { item ->
                listener.invoke(position, item)
                holder?.updateSelectedPosition(position)
            }
        }
    }

    override fun setToolBarActionEnd(toolbarActionEnd: ToolbarActionEnd) {
        // empty impl
    }

    override fun isOpen(tag: String): Boolean = supportFragmentManager.findFragmentByTag(tag)?.isAdded == true
}
