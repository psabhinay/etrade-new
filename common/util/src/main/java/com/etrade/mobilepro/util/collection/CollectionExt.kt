package com.etrade.mobilepro.util.collection

fun <E> MutableCollection<E>.removeAndReturnFirstIf(predicate: (E) -> Boolean): E? {
    var item: E? = null
    val each: MutableIterator<E> = iterator()
    while (each.hasNext()) {
        val next = each.next()
        if (predicate(next)) {
            each.remove()
            item = next
            break
        }
    }
    return item
}

inline fun <reified D : E, E> List<E>.indicesOfElementsWithType(): List<Int> {
    return this.mapIndexed { index: Int, e: E ->
        if (e is D) {
            return@mapIndexed index
        }
        return@mapIndexed null
    }.filterNotNull()
}
