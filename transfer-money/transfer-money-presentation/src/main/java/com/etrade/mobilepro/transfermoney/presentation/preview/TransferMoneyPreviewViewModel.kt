package com.etrade.mobilepro.transfermoney.presentation.preview

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.tracking.SCREEN_TITLE_TRANSFER_MONEY_PREVIEW
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.transfermoney.presentation.R
import com.etrade.mobilepro.transfermoney.presentation.common.getTitleId
import com.etrade.mobilepro.transfermoney.presentation.confirmation.ParcelableTransferConfirmation
import com.etrade.mobilepro.transfermoney.presentation.confirmation.PreviewConfirmation
import com.etrade.mobilepro.transfermoney.usecase.confirm.GetQuickTransferConfirmRequest
import com.etrade.mobilepro.transfermoney.usecase.confirm.GetQuickTransferConfirmResult
import com.etrade.mobilepro.transfermoney.usecase.confirm.QuickTransferConfirmUseCase
import com.etrade.mobilepro.transfermoney.usecase.preview.GetQuickTransferPreviewRequest
import com.etrade.mobilepro.transfermoney.usecase.preview.GetQuickTransferPreviewResult
import com.etrade.mobilepro.transfermoney.usecase.preview.GetQuickTransferPreviewUseCase
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class TransferMoneyPreviewViewModel @Inject constructor(
    private val getQuickTransferPreviewUseCase: GetQuickTransferPreviewUseCase,
    private val getQuickTransferConfirmUseCase: QuickTransferConfirmUseCase,
    private val resources: Resources
) : ViewModel(), ScreenViewModel {

    override val screenName: String? = SCREEN_TITLE_TRANSFER_MONEY_PREVIEW

    private val emptyValue = resources.getString(R.string.empty_default)
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    private val _snackbarMessage: MutableLiveData<Unit> = MutableLiveData()
    val snackbarMessage: LiveData<Unit>
        get() = _snackbarMessage

    private val _popupErrorMessage: MutableLiveData<TransferError> = MutableLiveData()
    val popupErrorMessage: LiveData<TransferError>
        get() = _popupErrorMessage

    private val _transferMoneyPreviewItems: MutableLiveData<List<TableSectionItemLayout>> = MutableLiveData()
    val transferMoneyPreviewItems: LiveData<List<TableSectionItemLayout>>
        get() = _transferMoneyPreviewItems

    private val _loadingIndicator: MutableLiveData<Boolean> = MutableLiveData()
    val loadingIndicator: LiveData<Boolean>
        get() = _loadingIndicator

    private val _iraAccountDisclaimerVisibility: MutableLiveData<Boolean> = MutableLiveData()
    val iraAccountDisclaimer: LiveData<Boolean>
        get() = _iraAccountDisclaimerVisibility

    private val _footerMessage: MutableLiveData<String> = MutableLiveData()
    val footerMessage: LiveData<String>
        get() = _footerMessage

    private val _contentVisibility: MutableLiveData<Boolean> = MutableLiveData()
    val contentVisibility: LiveData<Boolean>
        get() = _contentVisibility

    private val _selectedIraYear: MutableLiveData<String> = MutableLiveData<String>().apply { value = "" }
    internal val selectedIraYear: LiveData<String>
        get() = _selectedIraYear

    val iraYear: String?
        get() = if (_selectedIraYear.value?.isBlank() == true) {
            null
        } else {
            _selectedIraYear.value
        }

    internal val taxYearVisibility: LiveData<Boolean>
        get() = _iraAccountDisclaimerVisibility.combineLatest(loadingIndicator).map { pair -> pair.first && !pair.second }

    private val _noTaxYear = MutableLiveData<ConsumableLiveEvent<Unit>>()
    internal val noTaxYear: LiveData<ConsumableLiveEvent<Unit>>
        get() = _noTaxYear

    private val _confirmationViewState = MutableLiveData<ConfirmationViewState>()
    val confirmationViewState: LiveData<ConfirmationViewState>
        get() = _confirmationViewState

    private lateinit var previewConfirmation: PreviewConfirmation

    fun loadPreview(parameter: GetQuickTransferPreviewRequest) {
        _loadingIndicator.value = true
        _contentVisibility.value = false

        viewModelScope.launch {
            getQuickTransferPreviewUseCase.execute(parameter).fold(
                onSuccess = {
                    _loadingIndicator.value = false
                    handleOnLoadPreview(it)
                },
                onFailure = {
                    logger.error("Transfer Money Preview call failed", it)

                    _contentVisibility.value = false
                    _snackbarMessage.value = Unit
                }
            )
        }
    }

    fun submitTransferConfirmation(parameter: GetQuickTransferConfirmRequest) {
        if (iraYear.isNullOrEmpty() && iraAccountDisclaimer.value == true) {
            _noTaxYear.value = ConsumableLiveEvent(Unit)
            return
        }
        _confirmationViewState.value = ConfirmationViewState.Loading

        viewModelScope.launch {
            getQuickTransferConfirmUseCase.execute(parameter).fold(
                onSuccess = {
                    when (it) {
                        is GetQuickTransferConfirmResult.Success ->
                            _confirmationViewState.value = ConfirmationViewState.Success(mapOnLoadConfirmSuccess(it))
                        is GetQuickTransferConfirmResult.Failed ->
                            _confirmationViewState.value = ConfirmationViewState.Failed(it.errorDescription)
                    }
                },
                onFailure = {
                    logger.error("Transfer Money Confirm call failed", it)

                    _confirmationViewState.value = ConfirmationViewState.Failed()
                }
            )
        }
    }

    internal fun taxYearSelected(year: String) {
        _selectedIraYear.value = year
    }

    private fun handleOnLoadPreview(result: GetQuickTransferPreviewResult) {
        when (result) {
            is GetQuickTransferPreviewResult.Success -> handleOnLoadPreviewSuccess(result)
            is GetQuickTransferPreviewResult.Failed -> handleOnLoadPreviewFailed(result)
        }
    }

    private fun handleOnLoadPreviewSuccess(result: GetQuickTransferPreviewResult.Success) {
        val emptyDate = resources.getString(R.string.empty_date_default)

        val amount = result.amount ?: emptyValue
        val scheduledDate = result.scheduleDate ?: emptyDate

        val items = listOf(
            createSummarySection(
                resources.getString(R.string.transfer_from_label),
                result.transferFrom,
                valueDescription = resources.accountNameContentDescription(result.transferFrom)
            ),
            createSummarySection(
                resources.getString(R.string.transfer_to_label),
                result.transferTo,
                valueDescription = resources.accountNameContentDescription(result.transferTo)
            ),
            createSummarySection(resources.getString(R.string.amount), amount),
            createSummarySection(resources.getString(R.string.schedule_date), scheduledDate),
            createSummarySection(resources.getString(R.string.transfer_repeat), result.repeat.getTitleId().let { resources.getString(it) })
        )

        _contentVisibility.value = true
        _transferMoneyPreviewItems.value = items
        _footerMessage.value = concatDetailedMessage(result.headerMessage, result.message, result.footerMessage)
        _iraAccountDisclaimerVisibility.value = result.isIraAccount
        previewConfirmation = PreviewConfirmation(amount, result.transferFrom, result.transferTo, scheduledDate)
    }

    private fun handleOnLoadPreviewFailed(result: GetQuickTransferPreviewResult.Failed) {
        val errorDescription = result.errorDescription
        val errorCode = result.errorCode

        if (errorDescription == null) {
            _snackbarMessage.value = Unit
        } else {
            val transferError = TransferError(errorCode, errorDescription)
            _popupErrorMessage.value = transferError
        }

        _contentVisibility.value = false
    }

    private fun mapOnLoadConfirmSuccess(result: GetQuickTransferConfirmResult.Success) = ParcelableTransferConfirmation(
        previewConfirmation.amount,
        previewConfirmation.fromAccount,
        previewConfirmation.toAccount,
        previewConfirmation.scheduleDate,
        if (result.confirmationNumber.isNullOrBlank()) { "" } else { result.confirmationNumber!! },
        concatDetailedMessage(result.headerMessage, result.message, result.footerMessage)
    )

    private fun concatDetailedMessage(headerMessage: String?, message: String?, footerMessage: String?) = buildString {
        headerMessage?.let { append(it) }
        if (isNotEmpty()) append('\n').append('\n')
        message?.let { append(it) }
        if (isNotEmpty()) append(' ')
        footerMessage?.let { append(it) }
        if (isEmpty()) append(emptyValue)
    }
}
