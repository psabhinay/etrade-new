package com.etrade.mobilepro.trade.api

enum class AccountMode {
    MARGIN,
    PORTFOLIO_MARGIN,
    CASH,
    IRA,
    UNKNOWN
}
