package com.etrade.mobilepro.checkdeposit.presentation.details.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.checkdeposit.presentation.databinding.AdapterDepositDetailImageBinding
import com.etrade.mobilepro.checkdeposit.presentation.details.DepositDetail
import com.etrade.mobilepro.checkdeposit.presentation.details.holder.DepositDetailImageViewHolder
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class DepositImageAdapterDelegate(
    private val imageLoader: ImageLoader
) : AbsListItemAdapterDelegate<DepositDetail.Image, DepositDetail, DepositDetailImageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): DepositDetailImageViewHolder {
        return DepositDetailImageViewHolder(
            imageLoader,
            parent.viewBinding(
                AdapterDepositDetailImageBinding::inflate
            )
        )
    }

    override fun isForViewType(item: DepositDetail, items: MutableList<DepositDetail>, position: Int): Boolean = item is DepositDetail.Image

    override fun onBindViewHolder(item: DepositDetail.Image, holder: DepositDetailImageViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }
}
