package com.etrade.mobilepro.inputvalidator.amount

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import java.util.regex.Pattern

// based on https://regexr.com/2rios
private val pattern: Pattern by lazy { Pattern.compile("""-?[\d]{1,10}([.|,]\d{1,2})?""") }
private val noLeadingDigitPattern: Pattern by lazy { Pattern.compile("""-?[.|,][\d]{1,2}""") }

class AmountValidator(private val genericValidationErrorDesc: String) : InputFieldManager.Validator {
    override fun validate(value: String): InputFieldManager.Value {
        if (value.isEmpty()) {
            return InputFieldManager.Value.Valid(value)
        }
        if (noLeadingDigitPattern.matcher(value).matches()) {
            return InputFieldManager.Value.Valid(value)
        }
        return if (pattern.matcher(value).matches() && (value.toDoubleOrNull() ?: 0.0) > 0.0) {
            InputFieldManager.Value.Valid(value)
        } else {
            InputFieldManager.Value.Invalid(value, genericValidationErrorDesc)
        }
    }
}
