package com.etrade.mobilepro.trade.dropdown

interface HasTradeSelectorHolder {
    var holder: TradeSelectorHolder?
}
