package com.etrade.mobilepro.home

import android.app.Activity
import android.os.Bundle
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.chart.linechart.viewfactory.MarketOverviewChartViewModel
import com.etrade.mobilepro.common.OnAccountsClickListener
import com.etrade.mobilepro.common.di.IsTablet
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.databinding.FragmentOverviewBinding
import com.etrade.mobilepro.databinding.FragmentOverviewDualBinding
import com.etrade.mobilepro.dynamicui.GenericRecyclerAdapter
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.toDynamicViewModels
import com.etrade.mobilepro.home.overview.edit.EditOverviewActivity
import com.etrade.mobilepro.home.overview.viewfactory.OverviewFooterViewModel
import com.etrade.mobilepro.home.overview.viewmodel.OverviewViewModel
import com.etrade.mobilepro.inboxmessages.InboxMessagesDynamicViewModel
import com.etrade.mobilepro.indices.MarketIndices
import com.etrade.mobilepro.login.presentation.MESSAGE_LOGIN_ACTION
import com.etrade.mobilepro.login.presentation.cta.LoginCtaItem
import com.etrade.mobilepro.login.presentation.cta.LoginViewCtaListener
import com.etrade.mobilepro.navdestinations.openDisclosures
import com.etrade.mobilepro.navigation.EtNavController
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.swiperefresh.MultiSwipeRefreshLayout
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.AuthenticationStatusProvider
import com.etrade.mobilepro.util.android.TimestampEventProvider
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.etrade.mobilepro.util.android.extension.clearItemDecorations
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

class OverviewFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val mainNavigation: MainNavigation,
    private val loginCtaListener: LoginViewCtaListener,
    @IsTablet private val isTablet: Boolean,
    private val authStatusProvider: AuthenticationStatusProvider,
    private val darkModeChecker: DarkModeChecker,
    private val userViewModel: UserViewModel
) : Fragment(), OnAccountsClickListener, OnAccountsTabSelectionListener {

    private val etNavController: EtNavController by lazy {
        EtNavController(
            findNavController(),
            userViewModel
        )
    }

    private val overviewViewModel: OverviewViewModel by viewModels { viewModelFactory }
    private var snackBar: Snackbar? = null
    private lateinit var viewAdapter: GenericRecyclerAdapter
    private val viewAdapterSecond: GenericRecyclerAdapter by lazy { GenericRecyclerAdapter(viewLifecycleOwner, emptyList()) }
    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    private var shouldFixScrollToTop = false

    private val isDualPane get() = isTablet && !authStatusProvider.isAuthenticated()

    private val dataDelegate = AccountsTabDataDelegate {
        if (overviewViewModel.freshPageLoad) {
            fetchValues()
        }
    }

    private var pullDownToRefresh: MultiSwipeRefreshLayout? = null
    private var loadingIndicator: ContentLoadingProgressBar? = null

    private var fragmentOverviewDualBinding: FragmentOverviewDualBinding? = null
    private var fragmentOverviewBinding: FragmentOverviewBinding? = null

    private var overviewRecyclerView: RecyclerView? = null

    private var overviewRecyclerViewSecond: RecyclerView? = null

    private val customizeWidgetsRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                overviewRecyclerView?.smoothScrollToPosition(0)
                overviewRecyclerViewSecond?.smoothScrollToPosition(0)
                fetchValues(true)
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return if (isDualPane) {
            fragmentOverviewDualBinding = FragmentOverviewDualBinding.inflate(layoutInflater)
            pullDownToRefresh = fragmentOverviewDualBinding?.swipeRefresh
            loadingIndicator = fragmentOverviewDualBinding?.loadingIndicator
            overviewRecyclerView = fragmentOverviewDualBinding?.overviewRecyclerView
            overviewRecyclerViewSecond = fragmentOverviewDualBinding?.overviewRecyclerViewSecond
            fragmentOverviewDualBinding?.root
        } else {
            fragmentOverviewBinding = FragmentOverviewBinding.inflate(layoutInflater)
            pullDownToRefresh = fragmentOverviewBinding?.swipeRefresh
            loadingIndicator = fragmentOverviewBinding?.loadingIndicator
            overviewRecyclerView = fragmentOverviewBinding?.overviewRecyclerView
            fragmentOverviewBinding?.root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()

        pullDownToRefresh?.setOnRefreshListener {
            fetchValues(true)
        }

        if (overviewViewModel.freshPageLoad) {
            fetchValues()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (isDualPane) {
            fragmentOverviewDualBinding?.onDestroyBinding()
            fragmentOverviewDualBinding = null
            overviewRecyclerView = null
        } else {
            fragmentOverviewBinding?.onDestroyBinding()
            fragmentOverviewBinding = null
            overviewRecyclerViewSecond = null
        }
        pullDownToRefresh = null
        loadingIndicator = null
    }

    private fun FragmentOverviewBinding.onDestroyBinding() {
        viewAdapter.disposeAll()
        overviewRecyclerView.adapter = null
    }

    private fun FragmentOverviewDualBinding.onDestroyBinding() {
        viewAdapter.disposeAll()
        overviewRecyclerView.adapter = null
        viewAdapterSecond.disposeAll()
        overviewRecyclerViewSecond.adapter = null
    }

    private fun fetchValues(forceRefresh: Boolean = false) {
        overviewViewModel.fetchOverview(forceRefresh)
    }

    private fun setupFragment() {
        viewAdapter = GenericRecyclerAdapter(viewLifecycleOwner, emptyList())

        if (isDualPane) {
            fragmentOverviewDualBinding?.let {
                with(it.overviewRecyclerView) {
                    layoutManager = LinearLayoutManager(context)
                    adapter = viewAdapter
                }
            }
            fragmentOverviewDualBinding?.let {
                with(it.overviewRecyclerViewSecond) {
                    visibility = View.VISIBLE
                    layoutManager = LinearLayoutManager(context)
                    adapter = viewAdapterSecond
                }
            }
        } else {
            fragmentOverviewBinding?.let {
                with(it.overviewRecyclerView) {
                    layoutManager = LinearLayoutManager(context)
                    adapter = viewAdapter
                }
            }
        }
    }

    @SuppressWarnings("LongMethod")
    private fun bindViewModel() {
        overviewViewModel.getViewState().observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is OverviewViewModel.ViewState.Loading -> {
                        if (darkModeChecker.uiMode != it.uiMode) {
                            populateItems(emptyList(), emptyList(), false)
                            return@Observer
                        }
                        if (pullDownToRefresh?.isRefreshing == false) {
                            loadingIndicator?.show()
                        }

                        snackBar?.dismiss()
                        populateItems(it.firstList, it.secondList, false)
                    }
                    is OverviewViewModel.ViewState.Error -> {
                        loadingIndicator?.hide()
                        pullDownToRefresh?.isRefreshing = false
                        snackBar = snackbarUtil.retrySnackbar { fetchValues() }
                    }

                    is OverviewViewModel.ViewState.Success -> {
                        if (darkModeChecker.uiMode != it.uiMode) {
                            populateItems(emptyList(), emptyList(), false)
                            fetchValues(true)
                        } else {
                            populateItems(it.firstList, it.secondList, overviewViewModel.freshPageLoad)
                            loadingIndicator?.hide()
                            snackBar?.dismiss()
                            pullDownToRefresh?.isRefreshing = false
                            overviewViewModel.freshPageLoad = false
                        }
                    }
                }
            }
        )
    }

    private fun populateItems(firstList: List<GenericLayout>, secondList: List<GenericLayout>?, shouldRefresh: Boolean) {
        setupItemDecoration(firstList, secondList)
        viewAdapter.values = constructViewModels(firstList, shouldRefresh)
        if (isTablet) {
            secondList?.let { viewAdapterSecond.values = constructViewModels(it, shouldRefresh) }
        }
    }

    private fun RecyclerView.applyDecorations(items: List<GenericLayout>, countOfLastItemsWithoutDivider: Int) {
        clearItemDecorations()
        setupRecyclerViewDivider(
            R.drawable.thick_divider,
            countOfLastItemsWithoutDivider = countOfLastItemsWithoutDivider,
            excludeIndexList = getIndexListToExcludeFromDivider(items)
        )
    }

    private fun setupItemDecoration(first: List<GenericLayout>, second: List<GenericLayout>?) {
        overviewRecyclerView?.applyDecorations(first, 2)
        second?.let { overviewRecyclerViewSecond?.applyDecorations(it, 1) }
    }

    private fun getIndexListToExcludeFromDivider(views: List<GenericLayout>?) =
        if (overviewViewModel.isUserAuthenticated) {
            views?.let {
                listOfNotNull(
                    includeMarketOverviewSection(it),
                    it.indexOf(it.find { view -> view.viewModelClass == InboxMessagesDynamicViewModel::class.java })
                )
            }
        } else {
            views?.let {
                listOf(
                    it.indexOf(it.find { view -> view.viewModelClass == InboxMessagesDynamicViewModel::class.java })
                )
            }
        }

    // Returns market index if Market Overview is immediately followed by Charts.
    private fun includeMarketOverviewSection(it: List<GenericLayout>): Int? {
        val marketIndex = it.indexOf(it.find { view -> view.viewModelClass == MarketIndices::class.java })
        val chartIndex = it.indexOf(it.find { view -> view.viewModelClass == MarketOverviewChartViewModel::class.java })
        return if ((chartIndex - marketIndex) == 1) {
            marketIndex
        } else {
            null
        }
    }

    @Suppress("LongMethod") // many different event types
    private fun getObserver(): Observer<in CtaEvent> {
        return Observer { ctaEvent ->
            when (ctaEvent) {
                is CtaEvent.AccountsTabNavigation -> (parentFragment as? NavigateTabs)?.navigateToTab(ctaEvent)
                is CtaEvent.NavDirectionsNavigation -> findNavController().navigate(ctaEvent.ctaDirections)
                is CtaEvent.CustomizeEvent -> {
                    val intent = EditOverviewActivity.intent(requireContext()) {
                        isDeletableItems = false
                        isToggleableItems = true
                        listFooterMessage = R.string.edit_list_footer_message
                    }

                    customizeWidgetsRequest.launch(intent)
                }
                is CtaEvent.LaunchQuote -> mainNavigation.navigateToQuoteDetails(activity, ctaEvent.bundle)
                is CtaEvent.MessageEvent -> onMessageEvent(event = ctaEvent.message)
                is CtaEvent.LaunchOrdersBottomSheet -> launchOrdersBottomSheet(ctaEvent)
                is CtaEvent.Disclosures -> startActivity(activity?.openDisclosures())
                is CtaEvent.UriEvent -> etNavController.navigateFromAppAction(ctaEvent.uri)
            }
        }
    }

    private fun launchOrdersBottomSheet(ctaEvent: CtaEvent.LaunchOrdersBottomSheet) {
        mainNavigation.launchBottomSheetOrders(
            activity,
            ctaEvent.accountId,
            OrderStatus.valueOf(ctaEvent.orderStatus)
        )
    }

    private fun onMessageEvent(event: Message) {
        when (event.what) {
            MESSAGE_LOGIN_ACTION -> loginCtaListener.onCtaEvent(event.obj as LoginCtaItem) { snackbarUtil }
        }
    }

    private fun removeAllRegisteredObservers() {
        overviewViewModel.lifecycleObserverSet.forEach {
            lifecycle.removeObserver(it)
        }
        overviewViewModel.lifecycleObserverSet.clear()
    }

    private fun constructViewModels(list: List<GenericLayout>, shouldRefresh: Boolean): List<DynamicViewModel> {
        removeAllRegisteredObservers()
        val timestampEventProviders = mutableListOf<TimestampEventProvider>()
        var footerViewModel: OverviewFooterViewModel? = null
        val viewModels = list.toDynamicViewModels(this, shouldRefresh) {
            if (it is LifecycleObserver) {
                lifecycle.addObserver(it)
                overviewViewModel.lifecycleObserverSet.add(it)
            }
            if (it is OverviewFooterViewModel) {
                footerViewModel = it
            }
            if (it is TimestampEventProvider) {
                timestampEventProviders.add(it)
            }
            it.clickEvents.observe(viewLifecycleOwner, getObserver())
        }
        footerViewModel?.addTimestampEventProviders(timestampEventProviders)
        return viewModels
    }

    override fun onAccountsClick() {
        overviewRecyclerView?.apply {
            stopScroll()
            smoothScrollToPosition(0)
        }
    }

    override fun onTabSelected() {
        shouldFixScrollToTop = true
        dataDelegate.fetchData(view != null)
    }
}
