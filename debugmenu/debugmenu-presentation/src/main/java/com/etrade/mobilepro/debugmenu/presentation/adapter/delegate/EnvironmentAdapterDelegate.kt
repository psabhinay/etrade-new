package com.etrade.mobilepro.debugmenu.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder.EnvironmentViewHolder
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemEnvironmentBinding
import com.etrade.mobilepro.debugmenu.presentation.item.DebugMenuItem
import com.etrade.mobilepro.debugmenu.presentation.item.EnvironmentItem
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class EnvironmentAdapterDelegate(
    private val listener: (EnvironmentItem) -> Unit
) : AbsListItemAdapterDelegate<EnvironmentItem, DebugMenuItem, EnvironmentViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): EnvironmentViewHolder {
        return EnvironmentViewHolder(parent.viewBinding(DebugmenuItemEnvironmentBinding::inflate), listener)
    }

    override fun isForViewType(item: DebugMenuItem, items: MutableList<DebugMenuItem>, position: Int): Boolean =
        item is EnvironmentItem

    override fun onBindViewHolder(item: EnvironmentItem, holder: EnvironmentViewHolder, payloads: MutableList<Any>) =
        holder.bindTo(item)
}
