package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.common.di.DeviceInfo
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.messagecenter.DefaultMessageCenterAccountsDataSource
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterAccountsDataSource
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterReferenceBookDataSource
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterRepo
import com.etrade.mobilepro.messagecenter.data.repo.DefaultMessageCenterReferenceBookDataSource
import com.etrade.mobilepro.messagecenter.data.repo.DefaultMessageCenterRepo
import com.etrade.mobilepro.messagecenter.data.repo.DefaultMessageListReducer
import com.etrade.mobilepro.messagecenter.data.repo.MessageListReducer
import com.etrade.mobilepro.messagecenter.data.rest.MessageCenterApiClient
import com.etrade.mobilepro.messagecenter.presentation.messagelist.router.DefaultMessageDetailsRouter
import com.etrade.mobilepro.messagecenter.presentation.messagelist.router.MessageDetailsRouter
import com.etrade.mobilepro.session.api.UserConsumerKeyInterface
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
interface MessageCenterModule {

    @Binds
    fun bindMessageCenterAccountsDataSource(dataSource: DefaultMessageCenterAccountsDataSource): MessageCenterAccountsDataSource

    @Binds
    fun bindMessageDetailsRouter(router: DefaultMessageDetailsRouter): MessageDetailsRouter

    @Binds
    @Singleton
    fun bindMessageCenterReferenceBookDataSource(dataSource: DefaultMessageCenterReferenceBookDataSource): MessageCenterReferenceBookDataSource

    @Binds
    fun bindMessageListReducer(reducer: DefaultMessageListReducer): MessageListReducer

    companion object {

        @Provides
        @Singleton
        fun provideMessageCenterApiClient(@MobileTrade retrofit: Retrofit): MessageCenterApiClient = retrofit.create(MessageCenterApiClient::class.java)

        @Suppress("LongParameterList")
        @Provides
        fun provideMessageCenterRepo(
            apiClient: MessageCenterApiClient,
            accountsDataSource: MessageCenterAccountsDataSource,
            user: UserConsumerKeyInterface,
            @DeviceInfo deviceInfo: String,
            reducer: MessageListReducer
        ): MessageCenterRepo =
            DefaultMessageCenterRepo(apiClient, accountsDataSource, user::getConsumerKey, deviceInfo, reducer)
    }
}
