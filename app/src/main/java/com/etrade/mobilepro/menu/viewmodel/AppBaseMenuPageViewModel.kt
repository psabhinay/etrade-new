package com.etrade.mobilepro.menu.viewmodel

import android.content.res.Resources
import androidx.annotation.StringRes
import com.etrade.mobilepro.MenuGraphDirections
import com.etrade.mobilepro.menu.builder.MenuPageBuilder
import com.etrade.mobilepro.menu.builder.TextMenuItemBuilder
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen

abstract class AppBaseMenuPageViewModel(
    resources: Resources,
    protected val tracker: Tracker
) : MenuPageViewModel(resources) {

    fun MenuPageBuilder.requireLogin(isLoggedIn: UserAuthenticationState, setup: MenuPageBuilder.() -> Unit) {
        if (isLoggedIn()) {
            setup()
        }
    }

    fun TextMenuItemBuilder.accountWebNav(url: String, @StringRes titleRes: Int? = null) {
        navDirections(
            MenuGraphDirections.actionMenuToAccountServices(
                url = url,
                title = titleRes?.let { resources.getString(titleRes) } ?: text
            )
        )
    }

    fun TextMenuItemBuilder.extendedHoursDisclosureWebNav(url: String) {
        navDirections(MenuGraphDirections.actionMenuToExtendedHoursDisclosure(text, url))
    }

    fun TextMenuItemBuilder.trackScreen(pageTitle: String) {
        action {
            tracker.screen(pageTitle)
        }
    }
}
