package com.etrade.mobilepro.trade.optionlegselect

import com.etrade.eo.netbidaskcalculator.api.SpreadLeg
import com.etrade.mobilepro.orders.api.signedQuantity
import java.math.BigDecimal

private val ONE_HUNDRED = BigDecimal("100")

internal interface InternalSpreadLeg : SpreadLeg {
    val askSize: String?
    val bidSize: String?
}

internal class StockSpreadLeg(
    override val ask: BigDecimal,
    override val bid: BigDecimal,
    override val multiplier: BigDecimal = BigDecimal.ONE,
    override val quantity: Int,
    override val askSize: String?,
    override val bidSize: String?
) : InternalSpreadLeg

internal class OptionSpreadLeg(leg: OptionTradeLegWithOption) : InternalSpreadLeg {
    override val ask: BigDecimal = leg.option.streamAsk.value ?: BigDecimal.ZERO
    override val bid: BigDecimal = leg.option.streamBid.value ?: BigDecimal.ZERO
    override val multiplier: BigDecimal = ONE_HUNDRED
    override val quantity: Int = leg.transactionType.signedQuantity(leg.quantity)
    override val askSize: String? = leg.option.streamAskSize.value
    override val bidSize: String? = leg.option.streamBidSize.value
}
