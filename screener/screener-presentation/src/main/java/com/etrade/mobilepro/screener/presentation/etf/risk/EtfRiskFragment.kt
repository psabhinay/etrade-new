package com.etrade.mobilepro.screener.presentation.etf.risk

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.etf.EtfRiskFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.FragmentEtfRiskBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class EtfRiskFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<EtfRiskFilter, EtfRiskViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {
    override val layoutRes: Int = R.layout.fragment_etf_risk
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.ETF
    private val binding by viewBinding(FragmentEtfRiskBinding::bind)
    override val viewModel by viewModels<EtfRiskViewModel> { viewModelFactory }
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<EtfRiskFilter, EtfRiskViewModel.ViewState, Boolean>>
        get() = mapOf(
            binding.bottomTwentyPercent to EtfStandardDeviation.BOTTOM_20,
            binding.belowMidTwentyPercent to EtfStandardDeviation.BELOW_MID_20,
            binding.midTwentyPercent to EtfStandardDeviation.MID_20,
            binding.aboveMidTwentyPercent to EtfStandardDeviation.ABOVE_MID_20,
            binding.topTwentyPercent to EtfStandardDeviation.TOP_20
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                it.filter.apply {
                    binding.apply {
                        bottomTwentyPercent.isChecked = bottom20
                        belowMidTwentyPercent.isChecked = belowMid20
                        midTwentyPercent.isChecked = mid20
                        aboveMidTwentyPercent.isChecked = aboveMid20
                        topTwentyPercent.isChecked = top20
                    }
                }
            }
        )
    }
}
