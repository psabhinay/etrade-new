package com.etrade.mobilepro.tableviewutils.api

/**
 * Repository to store columns order.
 */
interface ColumnRepo<T> {

    /**
     * Gets columns from the repository.
     *
     * @return list of columns
     */
    suspend fun getColumns(): List<T>

    /**
     * Saves columns to thr repository.
     *
     * @param columns columns to save
     */
    suspend fun saveColumns(columns: List<T>)

    /**
     * Gets columns in String form
     *
     * @returns list of column Strings
     */
    fun getColumnsAsString(): List<String>
}
