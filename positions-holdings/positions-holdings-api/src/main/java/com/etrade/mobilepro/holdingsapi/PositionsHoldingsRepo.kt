package com.etrade.mobilepro.holdingsapi

import io.reactivex.Observable

interface PositionsHoldingsRepo {
    /**
     * Performs a request for a holding for a symbol, with default retry policy.
     * @param brokerageAccounts Ids of brokerage accounts separated by ","
     * @param stockPlanAccounts Ids of stock plan accounts separated by ","
     * @param symbol Symbol name to look holdings for
     */
    fun loadHoldings(
        brokerageAccounts: String,
        stockPlanAccounts: String,
        symbol: String
    ): Observable<PositionHoldingsItem>
}
