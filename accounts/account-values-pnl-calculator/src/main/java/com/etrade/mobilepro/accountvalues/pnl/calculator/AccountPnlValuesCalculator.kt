package com.etrade.mobilepro.accountvalues.pnl.calculator

import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult.CalculatedAccountValuesVO
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.positions.api.Position
import java.math.BigDecimal

/**
 * Calculates Account level pnl values such as Day Gain and Percent, Total Gain and Percent.
 * Used in PNL calculations for Individual Brokerage, All brokerage and CompleteView
 */
interface AccountPnlValuesCalculator {

    fun calculateAccountValues(
        instrumentTypes: Map<String, InstrumentType>,
        positions: List<Position>,
        cashValue: BigDecimal
    ): CalculatedAccountValuesVO

    fun calculateAllBrokerageAccountValues(
        accountIds: List<String>,
        lastCalculatedAccountValues: Map<String, CalculatedAccountValues>
    ): CalculationResult

    fun calculateCompleteViewSummaryAccountValues(
        accounts: List<Account>,
        lastCalculatedAccountValues: Map<String, CalculatedAccountValuesVO>
    ): CalculationResult
}
