package com.etrade.mobilepro.ui.multitoggle

import androidx.annotation.DrawableRes

/**
 * An item within a [MultiToggle]
 */
sealed class Toggle(val id: Int, var contentDescription: String?) {

    /**
     * The selection state of the toggle
     */
    var isSelected: Boolean = false
}

class ImageToggle(id: Int, @DrawableRes val imageRes: Int, contentDescription: String?) :
    Toggle(id, contentDescription)

class TextToggle(id: Int, val title: CharSequence?, contentDescription: String?) :
    Toggle(id, contentDescription)
