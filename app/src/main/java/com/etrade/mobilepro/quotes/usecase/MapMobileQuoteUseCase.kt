package com.etrade.mobilepro.quotes.usecase

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.mapper.MobileQuotePageItemMapper
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.QuoteItems
import com.etrade.mobilepro.util.UseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface MapMobileQuoteUseCase : UseCase<MapMobileQuoteUseCaseParameter, QuoteItems>

class MapMobileQuoteUseCaseImpl @Inject constructor(
    private val mapper: MobileQuotePageItemMapper
) : MapMobileQuoteUseCase {

    override suspend fun execute(parameter: MapMobileQuoteUseCaseParameter): QuoteItems = withContext(Dispatchers.IO) {
        with(parameter) {
            if (quote.typeCode.isMutualFundOrMoneyMarketFund) {
                if (quote.typeCode == InstrumentType.MF) {
                    mapper.getMutualFundItems(quote)
                } else {
                    mapper.getMoneyMarketFundItems(quote)
                }
            } else {
                QuoteItems(mapper.map(quote, streamingEnabled, isShowHoldings))
            }
        }
    }
}

class MapMobileQuoteUseCaseParameter(
    val quote: MobileQuote,
    val streamingEnabled: Boolean,
    val isShowHoldings: Boolean
)
