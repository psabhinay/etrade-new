package com.etrade.mobilepro.trade.data.stock

import com.etrade.mobilepro.backends.api.ServerMessageException
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dynamic.form.value.MutableInputValuesSnapshot
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.trade.data.DAY_TRADING_FLAG
import com.etrade.mobilepro.trade.data.DAY_TRADING_VER
import com.etrade.mobilepro.trade.data.form.input.DefaultStockTradeFormInputId
import com.etrade.mobilepro.util.filterNotNullValues

internal const val TRUE = "true"
internal const val FALSE = "false"

internal fun addOrderParams(
    map: MutableInputValuesSnapshot,
    isTablet: Boolean,
    userId: String,
    marketDestination: MarketDestination = MarketDestination.AUTO
) {
    map[DefaultStockTradeFormInputId.symbol] = DefaultStockTradeFormInputId.extractSymbol(map)

    map[StockOrderConstants.PLATFORM] = if (isTablet) "tablet" else "android"
    map[StockOrderConstants.USER_ID] = userId
    map[StockOrderConstants.LOT_SUPPORT] = FALSE

    if (marketDestination != MarketDestination.AUTO) {
        map[StockOrderConstants.DESTINATION_MARKET_CODE] = marketDestination.symbol
    }

    resolvePrices(map)
    resolveMarketSessionAndAllOrNone(map)

    if (map[DefaultStockTradeFormInputId.positionLotId] != null) {
        map[StockOrderConstants.LOT_SUPPORT] = TRUE
        map[StockOrderConstants.LOT_QUANTITY] = map[DefaultStockTradeFormInputId.quantity]
    }
}

internal fun addPreviewOrderParams(map: MutableInputValuesSnapshot) {
    map[DAY_TRADING_FLAG] = TRUE
    map[DAY_TRADING_VER] = "1"
}

internal fun addPlaceOrderParams(
    map: MutableInputValuesSnapshot,
    yourPrice: String?,
    overnightIndicatorFlag: Boolean = false
) {
    if (yourPrice != null) {
        map[StockOrderConstants.PRICE] = yourPrice
    }
    if (overnightIndicatorFlag) {
        map[StockOrderConstants.OVERNIGHT_INDICATOR_FLAG] = "1"
    }
}

internal fun resolvePrices(map: MutableInputValuesSnapshot) {
    val priceTypeValueId = DefaultStockTradeFormInputId.priceTypeValueId
    when (map[DefaultStockTradeFormInputId.priceType]) {
        priceTypeValueId.trailingStopDollar -> {
            map[StockOrderConstants.OFFSET_VALUE] = map[DefaultStockTradeFormInputId.stopPrice]
            map.remove(DefaultStockTradeFormInputId.stopPrice)
            map[StockOrderConstants.OFFSET_TYPE] = priceTypeValueId.trailingStopDollar
        }
        priceTypeValueId.trailingStopPercent -> {
            map[StockOrderConstants.OFFSET_VALUE] = map[DefaultStockTradeFormInputId.stopPrice]
            map.remove(DefaultStockTradeFormInputId.stopPrice)
            map[StockOrderConstants.OFFSET_TYPE] = priceTypeValueId.trailingStopPercent
        }
        priceTypeValueId.stopLimit -> {
            map[DefaultStockTradeFormInputId.limitPrice] = map[DefaultStockTradeFormInputId.limitPriceForStopLimit]
            map.remove(DefaultStockTradeFormInputId.limitPriceForStopLimit)
            map[DefaultStockTradeFormInputId.stopPrice] = map[DefaultStockTradeFormInputId.stopPriceForStopLimit]
            map.remove(DefaultStockTradeFormInputId.stopPriceForStopLimit)
        }
    }
}

private fun resolveMarketSessionAndAllOrNone(map: MutableInputValuesSnapshot) {
    val termId = DefaultStockTradeFormInputId.term
    val termValueId = DefaultStockTradeFormInputId.termValueId
    when (map[termId]) {
        termValueId.extendedHoursDay -> {
            map[termId] = termValueId.goodForDay
            map[StockOrderConstants.MARKET_SESSION] = "2"
        }
        termValueId.extendedHoursImmediateOrCancel -> {
            map[termId] = termValueId.immediateOrCancel
            map[StockOrderConstants.MARKET_SESSION] = "2"
        }
        else -> {
            map[StockOrderConstants.MARKET_SESSION] = "1"
        }
    }
}

internal suspend fun MutableInputValuesSnapshot.makeStockOrder(apiClient: StockTradingApiClient, requestType: String): ETResult<StockOrderResponseDto> {
    return apiClient.runCatchingET {
        val order = makeStockOrder(requestType, mapValues { it.value as? String? }.filterNotNullValues())
        val errors = order.getErrors(requestType == PLACE_NEW_STOCK_ORDER || requestType == PLACE_CHANGE_STOCK_ORDER)
        if (errors.isEmpty()) {
            order
        } else {
            throw ServerMessageException(errors)
        }
    }
}
