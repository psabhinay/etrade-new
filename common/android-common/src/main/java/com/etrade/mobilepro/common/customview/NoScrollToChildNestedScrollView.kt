package com.etrade.mobilepro.common.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.core.widget.NestedScrollView

class NoScrollToChildNestedScrollView : NestedScrollView {
    var enableScrollToView: (child: View?) -> Boolean = { false }
    private var focusedView: View? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun requestChildFocus(child: View?, focused: View?) {
        focusedView = focused
        super.requestChildFocus(child, focused)
    }

    override fun scrollBy(x: Int, y: Int) {
        if (enableScrollToView.invoke(focusedView)) {
            super.scrollBy(x, y)
        }
    }
}
