package com.etrade.mobilepro.toolbar

data class ToolbarDataModel(
    val titleText: String? = null,
    val showTitle: Boolean = false
)
