package com.etrade.mobilepro.billpay.api

enum class BillPayStatus {
    Supported,
    NotEnrolled,
    NotEligible
}
