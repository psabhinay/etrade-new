package com.etrade.mobilepro.tableviewutils.presentation.sorting

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.tableviewutils.presentation.TableViewListener
import com.evrencoskun.tableview.TableView

open class SortableTableViewListener(
    private val sortable: TableDataSortable?
) : TableViewListener() {

    override fun onCornerViewClicked(tableView: TableView, cornerView: View) {
        sortable?.sortTable(null)
    }

    override fun onColumnHeaderClicked(columnHeaderView: RecyclerView.ViewHolder, column: Int) {
        sortable?.sortTable(column)
    }
}
