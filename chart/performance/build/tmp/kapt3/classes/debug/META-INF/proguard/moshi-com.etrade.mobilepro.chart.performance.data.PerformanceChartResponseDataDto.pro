-if class com.etrade.mobilepro.chart.performance.data.PerformanceChartResponseDataDto
-keepnames class com.etrade.mobilepro.chart.performance.data.PerformanceChartResponseDataDto
-if class com.etrade.mobilepro.chart.performance.data.PerformanceChartResponseDataDto
-keep class com.etrade.mobilepro.chart.performance.data.PerformanceChartResponseDataDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
