package com.etrade.mobilepro.screener.presentation.base

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.screener.api.ScreenerDisclosure
import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.adapter.ScreenerSetAdaper
import com.etrade.mobilepro.screener.presentation.databinding.FragmentScreenersBinding
import com.etrade.mobilepro.screener.presentation.databinding.ResultsButtonViewBinding
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar

internal const val MAX_RESULTS_TO_ENABLE_BUTTON = 100
private const val WITH_DISCLOSURES_LAST_ITEM_WITHOUT_DIVIDER = 2
private const val NO_DISCLOSURES_LAST_ITEM_WITHOUT_DIVIDER = 1

abstract class BaseScreenersFragment(
    protected val snackbarUtilFactory: SnackbarUtilFactory,
    private val screenersRouter: ScreenersRouter
) : Fragment(R.layout.fragment_screeners) {

    private val binding by viewBinding(FragmentScreenersBinding::bind)
    private val viewResultsBinding by viewBinding(ResultsButtonViewBinding::bind)

    protected abstract val viewModel: BaseScreenersViewModel

    protected abstract val disclosureType: ScreenerDisclosure

    protected abstract val screenerSetSelectedListener: (ScreenerItem.ScreenerSet) -> Unit

    protected abstract val resultNavListener: () -> Unit

    protected open val disclosuresText: String? = null

    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private var snackbar: Snackbar? = null

    protected open val screenerSetClearListener: (ScreenerItem.ScreenerSet) -> Unit = {
        viewModel.clearScreenerSet(it)
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = ScreenerSetAdaper(screenerSetSelectedListener, screenerSetClearListener)
        baseSetupFragment(adapter)
        baseBindViewModel(adapter)
    }

    private fun baseSetupFragment(adapter: ScreenerSetAdaper) {
        binding.apply {
            screenerSetsRv?.layoutManager = LinearLayoutManager(activity).apply { orientation = RecyclerView.VERTICAL }
            screenerSetsRv?.setupRecyclerViewDivider(
                R.drawable.thin_divider,
                countOfLastItemsWithoutDivider = if (disclosuresText != null) {
                    WITH_DISCLOSURES_LAST_ITEM_WITHOUT_DIVIDER
                } else {
                    NO_DISCLOSURES_LAST_ITEM_WITHOUT_DIVIDER
                }
            )
            screenerSetsRv?.itemAnimator = null
            screenerSetsRv?.adapter = adapter
            disclosuresBtn?.setOnClickListener {
                screenersRouter.actionResultsToDisclosure(findNavController(), disclosureType)
            }
            viewResultsBinding.viewResultsBtn.setOnClickListener {
                resultNavListener.invoke()
            }
        }
    }

    private fun baseBindViewModel(adapter: ScreenerSetAdaper) {
        viewModel.update(disclosuresText)
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer { viewState ->
                viewState?.run {
                    adapter.submitItems(screenerItems)
                    applyPrescreenResults(preScreenResults)
                }
            }
        )
    }

    private fun applyPrescreenResults(results: Resource<Int>) {
        applyResultsCount(results.data, forceDisableButton = results is Resource.Loading)
        snackbar?.dismiss()
        when (results) {
            is Resource.Loading -> viewResultsBinding.resultsLoadingPb.show()
            is Resource.Success -> viewResultsBinding.resultsLoadingPb.hide()
            is Resource.Failed -> {
                snackbar = snackbarUtil.retrySnackbar {
                    viewModel.retryPrescreen()
                }
            }
        }
    }

    private fun applyResultsCount(count: Int?, forceDisableButton: Boolean) {
        viewResultsBinding.viewResultsBtn.apply {
            if (count == null) {
                isEnabled = false
                setText(R.string.view_results_btn_title)
            } else {
                text = getString(R.string.view_results_btn_title_with_count, count)
                isEnabled = !forceDisableButton && count in 1..MAX_RESULTS_TO_ENABLE_BUTTON
            }
        }
    }
}
