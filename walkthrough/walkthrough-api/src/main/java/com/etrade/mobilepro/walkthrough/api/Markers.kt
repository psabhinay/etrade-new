package com.etrade.mobilepro.walkthrough.api

interface Action
interface State

interface IdleState
interface CancelAction
