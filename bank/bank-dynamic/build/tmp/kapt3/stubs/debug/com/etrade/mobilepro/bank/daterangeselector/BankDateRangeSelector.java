package com.etrade.mobilepro.bank.daterangeselector;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\bR\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\fX\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u001c\u0010\u000f\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00110\u0010X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u00020\u0015X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017\u00a8\u0006\u0018"}, d2 = {"Lcom/etrade/mobilepro/bank/daterangeselector/BankDateRangeSelector;", "Lcom/etrade/mobilepro/dynamicui/api/GenericLayout;", "resources", "Landroid/content/res/Resources;", "startDate", "Landroidx/lifecycle/LiveData;", "Lorg/threeten/bp/LocalDate;", "endDate", "(Landroid/content/res/Resources;Landroidx/lifecycle/LiveData;Landroidx/lifecycle/LiveData;)V", "getResources", "()Landroid/content/res/Resources;", "uniqueIdentifier", "", "getUniqueIdentifier", "()Ljava/lang/String;", "viewModelClass", "Ljava/lang/Class;", "Lcom/etrade/mobilepro/dynamicui/api/DynamicViewModel;", "getViewModelClass", "()Ljava/lang/Class;", "viewModelFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "getViewModelFactory", "()Landroidx/lifecycle/ViewModelProvider$Factory;", "bank-dynamic_debug"})
public final class BankDateRangeSelector implements com.etrade.mobilepro.dynamicui.api.GenericLayout {
    @org.jetbrains.annotations.NotNull()
    private final android.content.res.Resources resources = null;
    private final androidx.lifecycle.LiveData<org.threeten.bp.LocalDate> startDate = null;
    private final androidx.lifecycle.LiveData<org.threeten.bp.LocalDate> endDate = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.ViewModelProvider.Factory viewModelFactory = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Class<? extends com.etrade.mobilepro.dynamicui.api.DynamicViewModel> viewModelClass = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String uniqueIdentifier = null;
    
    public BankDateRangeSelector(@org.jetbrains.annotations.NotNull()
    android.content.res.Resources resources, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<org.threeten.bp.LocalDate> startDate, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<org.threeten.bp.LocalDate> endDate) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.res.Resources getResources() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.lifecycle.ViewModelProvider.Factory getViewModelFactory() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.Class<? extends com.etrade.mobilepro.dynamicui.api.DynamicViewModel> getViewModelClass() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getUniqueIdentifier() {
        return null;
    }
    
    public void refresh(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.dynamicui.api.DynamicViewModel viewModel) {
    }
}