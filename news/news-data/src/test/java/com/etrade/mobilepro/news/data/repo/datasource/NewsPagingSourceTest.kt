package com.etrade.mobilepro.news.data.repo.datasource

import androidx.paging.PagingSource
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.data.dto.NewsDto
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import io.reactivex.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import org.mockito.Mockito

@ExperimentalCoroutinesApi
class NewsPagingSourceTest {

    private lateinit var sut: NewsPagingSourceTest

    private val emptyNewsDto = NewsDto(null)

    open class NewsPagingSourceTest(
        requestParams: NewsRequest?,
        private val newsDtoSingle: Single<NewsDto>
    ) : NewsPagingSource(
        requestParams = requestParams,
        Observable.just(
            listOf(
                NewsItem.CompanyHeader(
                    quoteWidgetData = null,
                    onSearchFieldTapListener = null,
                    onSetAlertListener = null,
                    onAddToWatchlistListener = null,
                    onTradeListener = null,
                    subsequentFirstItem = null
                )
            )
        )
    ) {
        public override fun getNewsRange(
            startIndex: Int,
            size: Int,
            requestParams: NewsRequest?
        ): Single<NewsDto> {
            return newsDtoSingle
        }
    }

    @Test
    fun `test null newsDto source`() {
        sut = Mockito.spy(NewsPagingSourceTest(null, Single.just(emptyNewsDto)))
        runBlockingTest {
            sut.load(PagingSource.LoadParams.Refresh(null, 20, false))
            verify(sut).getNewsRange(0, 20, null)
        }
    }

    @Test
    fun `test portfolio news request load after`() {
        sut = Mockito.spy(NewsPagingSourceTest(NewsRequest.Portfolio(), Single.just(emptyNewsDto)))
        runBlockingTest {
            sut.load(PagingSource.LoadParams.Append(100, 20, false))
            verify(sut).getNewsRange(100, 20, NewsRequest.Portfolio())
        }
    }

    @Test
    fun `test market news request load initial and after`() {
        sut = Mockito.spy(NewsPagingSourceTest(NewsRequest.Market, Single.just(emptyNewsDto)))
        runBlockingTest {
            sut.load(PagingSource.LoadParams.Refresh(null, 20, false))
            verify(sut).getNewsRange(0, 20, NewsRequest.Market)
            sut.load(PagingSource.LoadParams.Append(20, 20, false))
            verify(sut).getNewsRange(20, 20, NewsRequest.Market)
        }
    }

    @Test
    fun `test symbol news request load initial and after`() {
        sut = Mockito.spy(NewsPagingSourceTest(NewsRequest.Symbol(resource = listOf("MS", "FB", "AAPL")), Single.just(emptyNewsDto)))
        runBlockingTest {
            sut.load(PagingSource.LoadParams.Refresh(null, 20, false))
            verify(sut).getNewsRange(0, 20, NewsRequest.Symbol(resource = listOf("MS", "FB", "AAPL")))
            sut.load(PagingSource.LoadParams.Append(20, 20, false))
            verify(sut).getNewsRange(20, 20, NewsRequest.Symbol(resource = listOf("MS", "FB", "AAPL")))
        }
    }

    @Test
    fun `test watchlist news request load initial and after`() {
        sut = Mockito.spy(NewsPagingSourceTest(NewsRequest.Watchlist("1234"), Single.just(emptyNewsDto)))
        runBlockingTest {
            sut.load(PagingSource.LoadParams.Refresh(null, 20, false))
            verify(sut).getNewsRange(0, 20, NewsRequest.Watchlist("1234"))
            sut.load(PagingSource.LoadParams.Append(20, 20, false))
            verify(sut).getNewsRange(20, 20, NewsRequest.Watchlist("1234"))
        }
    }
}
