package com.etrade.mobilepro.barcode.viewmodel

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.tracking.SCREEN_TITLE_BARCODE_RESULTS
import com.etrade.mobilepro.tracking.SCREEN_TITLE_BARCODE_SCAN
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.tracking.TabsHostScreenViewModel
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelper
import javax.inject.Inject

class BarcodeViewModel @Inject constructor(
    override val viewPagerTabsTrackerHelper: ViewPagerTabsTrackerHelper
) : ViewModel(), ScreenViewModel, TabsHostScreenViewModel {

    override val screenName: String? = SCREEN_TITLE_BARCODE_SCAN

    override val tabNames: Array<String> = arrayOf(SCREEN_TITLE_BARCODE_SCAN, SCREEN_TITLE_BARCODE_RESULTS)
}
