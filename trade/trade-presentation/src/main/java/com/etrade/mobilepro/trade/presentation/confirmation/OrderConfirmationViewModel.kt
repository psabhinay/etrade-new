package com.etrade.mobilepro.trade.presentation.confirmation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.trade.api.OrderRequest
import com.etrade.mobilepro.trade.api.OrderResponse
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.TradeRepo
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class OrderConfirmationViewModel @Inject constructor(
    private val orderResponseAdapter: OrderResponseAdapter,
    private val tradeRepo: TradeRepo
) : ViewModel() {

    val contentAvailableIndicator: LiveData<Boolean>
        get() = _contentAvailableIndicator
    val openPortfolioSignal: LiveData<ConsumableLiveEvent<String>>
        get() = _openPortfolioSignal
    val exitConfirmationSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _exitConfirmationSignal

    private val order: MutableLiveData<TradeOrder> = MutableLiveData()
    private val orderResponse: LiveData<Pair<OrderResponse, OrderRequest?>> = order.map { it.response to it.orderRequest }

    val confirmationItems: LiveData<List<OrderConfirmationItem>> = orderResponse.map(orderResponseAdapter::createConfirmationItems)

    private val logger: Logger = LoggerFactory.getLogger(OrderConfirmationViewModel::class.java)

    private val _contentAvailableIndicator: MutableLiveData<Boolean> = MutableLiveData(false)
    private val _openPortfolioSignal: MutableLiveData<ConsumableLiveEvent<String>> = MutableLiveData()
    private val _exitConfirmationSignal: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()

    private var currentOrderId: String? = null

    fun fetch(orderId: String) {
        if (orderId != currentOrderId || orderResponse.value == null) {
            currentOrderId = orderId
            refresh()
        }
    }

    fun openPortfolio() {
        order.value?.accountId?.let { accountId ->
            _openPortfolioSignal.value = ConsumableLiveEvent(accountId)
        }
    }

    fun exitConfirmation() {
        _exitConfirmationSignal.value = Unit.consumable()
    }

    private fun refresh() {
        val orderId = currentOrderId ?: return

        viewModelScope.launch {
            tradeRepo.loadOrder(orderId)
                .onSuccess {
                    order.value = it
                    _contentAvailableIndicator.value = true
                }
                .onFailure {
                    logger.error("Unable to load preview data", it)
                }
        }
    }
}
