package com.etrade.mobilepro.accountslist.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AccountsListDto(
    @Json(name = "account_uuids")
    val accountUuids: List<String>
)
