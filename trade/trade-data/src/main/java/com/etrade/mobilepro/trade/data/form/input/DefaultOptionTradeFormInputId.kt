package com.etrade.mobilepro.trade.data.form.input

import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.data.form.CommonAllOrNoneValueCoder
import com.etrade.mobilepro.trade.data.form.DefaultSymbolInputValueCoder
import com.etrade.mobilepro.trade.data.form.value.CommonOrderTermValueId
import com.etrade.mobilepro.trade.data.form.value.CommonPriceTypeValueId
import com.etrade.mobilepro.trade.data.form.value.OptionStrategyTypeValueId
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.trade.form.api.value.StrategyTypeValueId
import com.etrade.mobilepro.util.ValueCoder

object DefaultOptionTradeFormInputId :
    OptionTradeFormInputId,
    TradeFormInputId by DefaultTradeFormInputId,
    WithAdvancedTradeFormInputId by DefaultAdvancedTradeFormInputId {
    override val allOrNone: String = "allOrNone"
    override val allOrNoneValueCoder: ValueCoder<Boolean> = CommonAllOrNoneValueCoder
    override val limitPrice: String = "limitPrice"
    override val limitPriceForStopLimit: String = "limitPriceForStopLimit"
    override val orderId: String = "originalOrderNo"
    override val priceType: String = "priceType"
    override val priceTypeValueId: PriceTypeValueId = CommonPriceTypeValueId
    override val stopPrice: String = "stopPrice"
    override val stopPriceForStopLimit: String = "stopPriceForStopLimit"
    override val strategy: String = "strategyType"
    override val strategyValueId: StrategyTypeValueId = OptionStrategyTypeValueId
    override val symbol: String = "symbol"
    override val symbolCoder: ValueCoder<WithSymbolInfo> = DefaultSymbolInputValueCoder
    override val term: String = "orderTerm"
    override val termValueId: OrderTermValueId = CommonOrderTermValueId
    override val tradeLegs: String = "legRequestList"
    override val lookupSymbol: String = "underlier"
}
