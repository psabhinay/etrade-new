package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.accessvalidation.AppAccessProvider
import com.etrade.mobilepro.accessvalidation.DefaultAppAccessProvider
import dagger.Binds
import dagger.Module

/* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */

@Module
abstract class SecurityModule {

    @Binds
    abstract fun bindAppAccessProvider(implementation: DefaultAppAccessProvider): AppAccessProvider
}
