package com.etrade.mobilepro.portfolio.model

import com.etrade.mobilepro.positions.api.TaxLotPosition
import com.etrade.mobilepro.util.safeParseBigDecimal
import java.math.BigDecimal

class TaxLotDto(
    override var taxLotId: String = "",
    override var positionId: String = "",
    override var symbol: String = "",
    override var acquiredDateMs: Long = 0L,
    var price: String = "",
    var daysGainRaw: String = "",
    var daysGainPct: String = "",
    var marketValueRaw: String = "",
    var markToMarket: String = "",
    var totalCostForGainPct: String = "",
    var totalGainRaw: String = "",
    var remainingQty: String = "",
    var commPerShare: String = "",
    var feesPerShare: String = "",
    override var inTheMoneyFlag: Boolean? = null
) : TaxLotPosition {

    override val daysGain: BigDecimal?
        get() = daysGainRaw.safeParseBigDecimal()

    override val totalGain: BigDecimal?
        get() = totalGainRaw.safeParseBigDecimal()

    override val pricePaid: BigDecimal?
        get() = price.safeParseBigDecimal()

    override val commission: BigDecimal?
        get() = commPerShare.safeParseBigDecimal()

    override val fees: BigDecimal?
        get() = feesPerShare.safeParseBigDecimal()

    override val quantity: BigDecimal?
        get() = remainingQty.safeParseBigDecimal()

    override val displayQuantity: BigDecimal?
        get() = quantity

    override val daysGainPercentage: BigDecimal?
        get() = daysGainPct.safeParseBigDecimal()

    override val totalGainPercentage: BigDecimal?
        get() = totalCostForGainPct.safeParseBigDecimal()

    override val todayClose: BigDecimal?
        get() = null

    override val baseSymbolAndPrice: String?
        get() = null

    override val mark: BigDecimal?
        get() = markToMarket.safeParseBigDecimal()

    override val marketValue: BigDecimal?
        get() = marketValueRaw.safeParseBigDecimal()

    @Suppress("LongParameterList", "LongMethod")
    fun copy(
        taxLotId: String = this.taxLotId,
        positionId: String = this.positionId,
        symbol: String = this.symbol,
        acquiredDateMs: Long = this.acquiredDateMs,
        price: String = this.price,
        daysGainRaw: String = this.daysGainRaw,
        daysGainPct: String = this.daysGainPct,
        marketValueRaw: String = this.marketValueRaw,
        markToMarket: String = this.markToMarket,
        totalCostForGainPct: String = this.totalCostForGainPct,
        totalGainRaw: String = this.totalGainRaw,
        remainingQty: String = this.remainingQty,
        commPerShare: String = this.commPerShare,
        feesPerShare: String = this.feesPerShare,
        inTheMoneyFlag: Boolean? = this.inTheMoneyFlag
    ): TaxLotDto {
        return TaxLotDto(
            taxLotId = taxLotId,
            positionId = positionId,
            symbol = symbol,
            acquiredDateMs = acquiredDateMs,
            price = price,
            daysGainRaw = daysGainRaw,
            daysGainPct = daysGainPct,
            marketValueRaw = marketValueRaw,
            markToMarket = markToMarket,
            totalCostForGainPct = totalCostForGainPct,
            totalGainRaw = totalGainRaw,
            remainingQty = remainingQty,
            commPerShare = commPerShare,
            feesPerShare = feesPerShare,
            inTheMoneyFlag = inTheMoneyFlag
        )
    }
}
