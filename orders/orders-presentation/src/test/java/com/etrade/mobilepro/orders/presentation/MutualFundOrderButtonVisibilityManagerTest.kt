package com.etrade.mobilepro.orders.presentation

import com.etrade.mobilepro.orders.api.OrderStatus
import org.junit.Assert.assertEquals
import org.junit.Test

internal class MutualFundOrderButtonVisibilityManagerTest {

    @Test
    fun `test MutualFund Order orderType=MUTUAL_FUND status=OPEN`() {
        val orderButtonVisibilityManager = mutualFundOrderButtonVisibilityManager(OrderStatus.OPEN)

        assertEquals(true, orderButtonVisibilityManager.isCancellable)
        assertEquals(false, orderButtonVisibilityManager.isEditable)
        assertEquals(false, orderButtonVisibilityManager.isCompleted)
        assertEquals(false, orderButtonVisibilityManager.isPreviewable)
        assertEquals(false, orderButtonVisibilityManager.isDeletable)
        assertEquals(true, orderButtonVisibilityManager.isCancelOnly)
        assertEquals(true, orderButtonVisibilityManager.isOrderSupported)
    }

    @Test
    fun `test MutualFund Order orderType=MUTUAL_FUND status=CANCEL_REJECTED`() {
        val orderButtonVisibilityManager = mutualFundOrderButtonVisibilityManager(OrderStatus.CANCEL_REJECTED)

        assertEquals(false, orderButtonVisibilityManager.isCancellable)
        assertEquals(false, orderButtonVisibilityManager.isEditable)
        assertEquals(true, orderButtonVisibilityManager.isCompleted)
        assertEquals(false, orderButtonVisibilityManager.isPreviewable)
        assertEquals(false, orderButtonVisibilityManager.isDeletable)
        assertEquals(false, orderButtonVisibilityManager.isCancelOnly)
        assertEquals(true, orderButtonVisibilityManager.isOrderSupported)
    }

    @Test
    fun `test MutualFund Order orderType=MUTUAL_FUND status=CANCELLED`() {
        val orderButtonVisibilityManager = mutualFundOrderButtonVisibilityManager(OrderStatus.CANCELLED)

        assertEquals(false, orderButtonVisibilityManager.isCancellable)
        assertEquals(false, orderButtonVisibilityManager.isEditable)
        assertEquals(true, orderButtonVisibilityManager.isCompleted)
        assertEquals(false, orderButtonVisibilityManager.isPreviewable)
        assertEquals(false, orderButtonVisibilityManager.isDeletable)
        assertEquals(false, orderButtonVisibilityManager.isCancelOnly)
        assertEquals(true, orderButtonVisibilityManager.isOrderSupported)
    }
}
