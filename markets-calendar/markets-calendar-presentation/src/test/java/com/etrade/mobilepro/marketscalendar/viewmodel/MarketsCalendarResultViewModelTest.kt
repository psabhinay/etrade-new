package com.etrade.mobilepro.marketscalendar.viewmodel

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.marketscalendar.api.model.EventsEarnings
import com.etrade.mobilepro.marketscalendar.api.model.EventsEconomic
import com.etrade.mobilepro.marketscalendar.api.model.EventsResponse
import com.etrade.mobilepro.marketscalendar.api.model.EventsResponseData
import com.etrade.mobilepro.marketscalendar.api.model.EventsResponseDm
import com.etrade.mobilepro.marketscalendar.api.repo.MarketsCalendarRepo
import com.etrade.mobilepro.marketscalendar.data.dto.EventsEarningsData
import com.etrade.mobilepro.marketscalendar.data.dto.EventsEconomicData
import com.etrade.mobilepro.marketscalendar.data.dto.EventsResponseDto
import com.etrade.mobilepro.marketscalendar.data.mapper.mapToEventsEarningsResponse
import com.etrade.mobilepro.marketscalendar.data.mapper.mapToEventsEconomicResponse
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.threeten.bp.LocalDate
import org.threeten.bp.Month
import java.util.concurrent.TimeUnit

class MarketsCalendarResultViewModelTest {

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var marketsCalendarRepo: MarketsCalendarRepo

    @Mock
    private lateinit var resources: Resources

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        whenever(resources.getString(any(), any())).thenReturn("test %s")
    }

    private inline fun <reified T> testEventsTimeSavings(
        path: String,
        selectedDate: LocalDate,
        expectedTimeSavings: String,
        viewModel: MarketsCalendarResultViewModel<T>,
        mockApiCall: (response: EventsResponse<T>) -> Unit
    ) {
        val cls2: Class<*> = when (T::class) {
            EventsEarnings::class -> EventsEarningsData::class.java
            else -> EventsEconomicData::class.java
        }
        var responseDto: EventsResponseDto<*> =
            this::class.getObjectFromJson(
                path,
                EventsResponseDto::class.java,
                cls2
            )
        assertNotNull(responseDto.dataDto?.eventsResponseDm?.eventsDataList)

        when (T::class) {
            EventsEarnings::class -> {
                @Suppress("UNCHECKED_CAST")
                val response: EventsResponse<EventsEarnings> = (responseDto as EventsResponseDto<EventsEarningsData>).mapToEventsEarningsResponse()
                @Suppress("UNCHECKED_CAST")
                mockApiCall(response as EventsResponse<T>)
            }
            else -> {
                @Suppress("UNCHECKED_CAST")
                val response: EventsResponse<EventsEconomic> = (responseDto as EventsResponseDto<EventsEconomicData>).mapToEventsEconomicResponse()
                @Suppress("UNCHECKED_CAST")
                mockApiCall(response as EventsResponse<T>)
            }
        }

        viewModel.selectedDate = selectedDate
        viewModel.loadEvents()

        viewModel
            .events
            .test()
            .awaitNextValue(1, TimeUnit.SECONDS)
            .assertHasValue()

        val result: List<EventDetails> = viewModel.events.value!!.data!!

        result.forEach {
            val timeSavings: String = it.date!!.substringAfterLast(" ")

            assertEquals(expectedTimeSavings, timeSavings)
        }
    }

    @Test
    fun `test economic events response for EST time`() {
        testEventsTimeSavings(
            "economic_events_est.json",
            LocalDate.of(2019, Month.FEBRUARY, 20),
            "EST",
            EconomicEventsMarketsCalendarViewModel(marketsCalendarRepo, DefaultSharedCalendarState())
        ) { whenever(marketsCalendarRepo.getEventsEconomic(any())).thenReturn(Single.just(it)) }
    }

    @Test
    fun `test economic events response for EDT time`() {
        testEventsTimeSavings(
            "economic_events_edt.json",
            LocalDate.of(2019, Month.JUNE, 19),
            "EDT",
            EconomicEventsMarketsCalendarViewModel(marketsCalendarRepo, DefaultSharedCalendarState())
        ) { whenever(marketsCalendarRepo.getEventsEconomic(any())).thenReturn(Single.just(it)) }
    }

    @Test
    fun `test earnings events response for EST time`() {
        testEventsTimeSavings(
            "earnings_events_est.json",
            LocalDate.of(2019, Month.FEBRUARY, 19),
            "EST",
            EarningsEventsMarketsCalendarViewModel(marketsCalendarRepo, DefaultSharedCalendarState(), resources)
        ) { whenever(marketsCalendarRepo.getEventsEarningsRx(any())).thenReturn(Single.just(it)) }
    }

    @Test
    fun `test earnings events response for EDT time`() {
        testEventsTimeSavings(
            "earnings_events_edt.json",
            LocalDate.of(2019, Month.JUNE, 19),
            "EDT",
            EarningsEventsMarketsCalendarViewModel(marketsCalendarRepo, DefaultSharedCalendarState(), resources)
        ) { whenever(marketsCalendarRepo.getEventsEarningsRx(any())).thenReturn(Single.just(it)) }
    }

    private fun testEconomicSurpriseValueCalculation(expectedSurprise: String?, estimate: String?, actual: String?) {
        whenever(marketsCalendarRepo.getEventsEconomic(any())).thenReturn(
            Single.just(
                EventsResponse(
                    EventsResponseData(
                        EventsResponseDm(
                            listOf(
                                EventsEconomic(
                                    ecEcondetConcensus = estimate,
                                    ecEcondetActual = actual,
                                    ecEcondetName = null,
                                    ecEconDefinition = null,
                                    ecEconDesc = null,
                                    ecEconName = null,
                                    ecSeStartdate = null
                                )
                            )
                        )
                    )
                )
            )
        )

        val viewModel = EconomicEventsMarketsCalendarViewModel(marketsCalendarRepo, DefaultSharedCalendarState())
        viewModel.selectedDate = LocalDate.of(2019, Month.JUNE, 1)

        viewModel
            .events
            .test()
            .awaitNextValue(1, TimeUnit.SECONDS)
            .assertHasValue()

        val result: EventDetails = viewModel.events.value!!.data!!.first()

        assertEquals(expectedSurprise, result.surprise)
    }

    @Test
    fun `test economic events surprise value calculation`() {
        testEconomicSurpriseValueCalculation("-0.16M", "5.35M", "5.19M")
        testEconomicSurpriseValueCalculation("-3.1%", "2.7%", "-0.4%")
        testEconomicSurpriseValueCalculation(null, null, "1.2%")
        testEconomicSurpriseValueCalculation("4.1", "130", "134.1")
        testEconomicSurpriseValueCalculation("0.6M", "+1M", "+1.6M")
        testEconomicSurpriseValueCalculation("0.6M", "+1M", "1.6M")
        testEconomicSurpriseValueCalculation("0.6M", "1M", "+1.6M")
    }
}
