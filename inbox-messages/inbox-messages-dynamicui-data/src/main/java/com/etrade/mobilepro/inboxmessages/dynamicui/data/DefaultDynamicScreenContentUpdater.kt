package com.etrade.mobilepro.inboxmessages.dynamicui.data

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.MobileScreen
import com.etrade.mobilepro.inboxmessages.dynamicui.api.DynamicScreenContentUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.api.InboxMessagesWidgetPlace
import javax.inject.Inject

class DefaultDynamicScreenContentUpdater @Inject constructor(
    private val widgetListUpdaters: Set<@JvmSuppressWildcards WidgetListUpdater>
) : DynamicScreenContentUpdater {

    override fun updateWithInboxMessages(place: InboxMessagesWidgetPlace?, input: MobileScreen?): List<BaseScreenView> {
        if (place == null || input == null) {
            return input?.views.orEmpty().filterNotNull()
        }
        return widgetListUpdaters
            .filter { it.belongsToWidgetPlace == place }
            .fold(input) { screen, updater ->
                val updatedViews = updater.updateWithInboxMessages(screen)
                screen.copy(views = updatedViews)
            }
            .views
            .filterNotNull()
    }
}
