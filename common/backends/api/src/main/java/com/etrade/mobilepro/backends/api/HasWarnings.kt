package com.etrade.mobilepro.backends.api

/**
 * Marks an interface that may contain warning messages. These messages must be shown to a user to indicate an operation's result.
 */
interface HasWarnings {

    /**
     * A collection of warning messages.
     */
    val warnings: Collection<ServerMessage>
}
