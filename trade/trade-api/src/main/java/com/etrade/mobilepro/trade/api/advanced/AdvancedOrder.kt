package com.etrade.mobilepro.trade.api.advanced

import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.trade.api.Order

interface AdvancedOrder : Order {
    val orderType: OrderType
    val orderTrigger: String
    val advOrderType: AdvancedOrderType?
}
