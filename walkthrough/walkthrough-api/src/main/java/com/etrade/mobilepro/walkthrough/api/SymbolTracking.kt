package com.etrade.mobilepro.walkthrough.api

sealed class SymbolTrackingState : State {
    object Idle : SymbolTrackingState(), IdleState
    object DisplayInfoMessage : SymbolTrackingState()
    object LookupSymbol : SymbolTrackingState()
    object HighlightWatchlistActionInQuotePage : SymbolTrackingState()
    object DisplayWatchlistPage : SymbolTrackingState()
    object HighlightWatchlistActionInWatchlistPage : SymbolTrackingState()
}

sealed class SymbolTrackingAction : Action {
    object StartWalkthrough : SymbolTrackingAction()
    object InfoMessageDisplayed : SymbolTrackingAction()
    object SymbolSelected : SymbolTrackingAction()
    object SymbolAddedToWatchlist : SymbolTrackingAction()
    object WatchlistPageDisplayed : SymbolTrackingAction()
    object WatchlistActionInWatchlistPageHighlighted : SymbolTrackingAction()
    object Canceled : SymbolTrackingAction(), CancelAction
}
