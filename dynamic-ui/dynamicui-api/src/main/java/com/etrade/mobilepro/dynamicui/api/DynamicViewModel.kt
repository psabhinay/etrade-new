package com.etrade.mobilepro.dynamicui.api

import android.net.Uri
import android.os.Bundle
import android.os.Message
import androidx.annotation.LayoutRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.util.AppMessage
import com.etrade.mobilepro.util.android.SingleLiveEvent
import com.etrade.mobilepro.viewmodel.AppMessageViewModel
import org.slf4j.Logger
import org.slf4j.LoggerFactory

abstract class DynamicViewModel(
    @LayoutRes
    override val layoutId: Int
) : ViewModel(), DynamicLayout, AppMessageViewModel {

    override val appMessageSignal: LiveData<AppMessage?>
        get() = mutableAppMessageSignal

    protected val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    open val clickEvents: MutableLiveData<CtaEvent> = SingleLiveEvent()

    protected val mutableAppMessageSignal: MutableLiveData<AppMessage?> = MutableLiveData()

    init {
        logger.debug("ViewModel instantiated")
    }

    override val viewModel = this

    open fun onCtaClicked(appUrl: String?) {
        logger.debug("CTA clicked with action url '$appUrl")
        appUrl?.let {
            // TODO - default CTA
            // clickEvents.value = appUrl
        }
    }

    override fun onCleared() {
        super.onCleared()
        logger.warn("${this.javaClass.simpleName} cleared")
    }

    final override fun dismissAppMessage() {
        mutableAppMessageSignal.value = null
    }
}

sealed class CtaEvent {
    data class NavDirectionsNavigation(val ctaDirections: NavDirections) : CtaEvent()
    data class AccountsTabNavigation(val accountId: String, val tabTitle: String?, val tabArgs: String?) : CtaEvent()
    data class ActionEvent(val url: String, val title: String) : CtaEvent()
    data class MessageEvent(val message: Message) : CtaEvent()
    data class LaunchQuote(val bundle: Bundle) : CtaEvent()
    data class LaunchOrdersBottomSheet(val accountId: String, val orderStatus: String) : CtaEvent()
    data class PositionModeTypeUpdated(val index: Int) : CtaEvent()
    data class InfoDialogEvent(val title: String, val message: String) : CtaEvent()
    data class DismissInboxMessage(val message: InboxMessage) : CtaEvent()
    object LaunchPortfolioBottomSheet : CtaEvent()
    object RetryEvent : CtaEvent()
    object CustomizeEvent : CtaEvent()
    object BloombergTvEvent : CtaEvent()
    object Disclosures : CtaEvent()
    object ViewAllBrokerageAccounts : CtaEvent()
    object LaunchCustomizeColumns : CtaEvent()
    object ShowDropdownSelector : CtaEvent()

    data class UriEvent(val uri: Uri) : CtaEvent() {
        constructor(uri: String) : this(Uri.parse(uri))
    }
}
