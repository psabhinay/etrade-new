package com.etrade.mobilepro.common.di

import javax.inject.Qualifier

@Qualifier
annotation class PositiveColor

@Qualifier
annotation class NegativeColor

@Qualifier
annotation class NeutralColor

@Qualifier
annotation class DowJonesColor

@Qualifier
annotation class NasdaqColor

@Qualifier
annotation class SP500Color

@Qualifier
annotation class PrimaryColor
