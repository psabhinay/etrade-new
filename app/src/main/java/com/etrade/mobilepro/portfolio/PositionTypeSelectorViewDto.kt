package com.etrade.mobilepro.portfolio

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PositionTypeSelectorViewDto(
    @Json(name = "isPortfolio")
    val isPortfolio: Boolean = true,
    @Json(name = "data")
    override val data: String? = "",
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>? = null
) : BaseScreenView()
