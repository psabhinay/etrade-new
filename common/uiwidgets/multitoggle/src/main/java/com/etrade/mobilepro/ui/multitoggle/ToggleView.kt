package com.etrade.mobilepro.ui.multitoggle

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.LayoutRes
import androidx.core.content.res.getDrawableOrThrow
import androidx.core.content.withStyledAttributes
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.etrade.mobilepro.util.formatAccessibilityDescription
import com.google.android.material.card.MaterialCardView

@Suppress("LeakingThis")
@SuppressLint("ViewConstructor")
internal sealed class ToggleView(
    context: Context,
    toggle: Toggle,
    @LayoutRes private val layoutRes: Int
) : FrameLayout(context) {

    abstract val toggle: Toggle

    init {
        inflate(context, layoutRes, this)
        id = toggle.id
        setTag(R.id.tb_toggle_id, toggle)
    }

    internal class Text(
        context: Context,
        textToggle: TextToggle,
        paddingPx: Int,
        @LayoutRes layoutRes: Int
    ) : ToggleView(context, textToggle, layoutRes) {

        override val toggle = textToggle

        val textView: TextView = findViewById(android.R.id.text1)

        init {
            textView.setup(paddingPx)
            textView.text = toggle.title
            textView.contentDescription =
                toggle.contentDescription.formatAccessibilityDescription()
        }
    }

    internal class Image(
        context: Context,
        imageToggle: ImageToggle,
        paddingPx: Int,
        @LayoutRes layoutRes: Int
    ) : ToggleView(context, imageToggle, layoutRes) {

        override val toggle = imageToggle

        val imageView: ImageView = findViewById(android.R.id.icon)
        private val containerView: FrameLayout = findViewById(R.id.image_button_container)

        init {
            containerView.apply {
                setup(paddingPx)
                contentDescription = toggle.contentDescription
            }
            imageView.setImageResource(toggle.imageRes)
        }
    }

    internal fun View.setup(paddingPx: Int) {
        context.withStyledAttributes(attrs = intArrayOf(R.attr.selectableItemBackground)) {
            foreground = getDrawableOrThrow(0)
        }
        layoutParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT,
            Gravity.CENTER
        )
        isFocusable = false
        isClickable = false
        importantForAccessibility = MaterialCardView.IMPORTANT_FOR_ACCESSIBILITY_YES
        setPadding(paddingPx, paddingPx, paddingPx, paddingPx)
    }
}

internal fun ToggleView.applyContentColor(@ColorInt color: Int) {
    when (this) {
        is ToggleView.Text -> textView.setTextColor(color)
        is ToggleView.Image -> imageView.applyColor(color)
    }
}

internal fun ToggleView.applyColor(@ColorInt color: Int, selectBackground: Boolean) {
    if (selectBackground) {
        background = ColorDrawable(color)
    } else {
        applyContentColor(color)
    }
}

private fun ImageView.applyColor(color: Int) {
    drawable.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
        color,
        BlendModeCompat.SRC_IN
    )
}

internal fun Toggle.createToggleView(context: Context, paddingPx: Int, @LayoutRes layoutRes: Int) =
    when (this) {
        is TextToggle -> ToggleView.Text(context, this, paddingPx, layoutRes)
        is ImageToggle -> ToggleView.Image(context, this, paddingPx, layoutRes)
    }
