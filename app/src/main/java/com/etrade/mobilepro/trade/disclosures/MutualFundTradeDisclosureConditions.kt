package com.etrade.mobilepro.trade.disclosures

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MutualFundTradeDisclosureConditions(
    val hasEarlyRedemptionFee: Boolean,
    val isFrontEndOrSalesFeeFund: Boolean,
    val isMoneyMarketFund: Boolean,
    val isNoLoadNoTransactionFeeFund: Boolean
) : Parcelable
