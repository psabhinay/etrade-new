package com.etrade.mobilepro.portfoliosnapshot

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AccountId(
    @Json(name = "accountId")
    val accountId: String
)

@JsonClass(generateAdapter = true)
class PortfolioSnapshotRequestBody(
    @Json(name = "value")
    val params: AccountId
)
