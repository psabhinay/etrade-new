package com.etrade.mobilepro.debugmenu.api

import com.etrade.mobilepro.appconfig.api.BaseUrls

sealed class EnvChange {
    data class Success(val urls: BaseUrls) : EnvChange()
    object AuthFailed : EnvChange()
    data class ServerError(val code: Int? = null) : EnvChange()
}
