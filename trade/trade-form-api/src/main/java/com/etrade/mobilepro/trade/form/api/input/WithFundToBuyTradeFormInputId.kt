package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.util.ValueCoder

interface WithFundToBuyTradeFormInputId : TradeFormInputId {
    val fundToBuy: String
    val symbolCoder: ValueCoder<WithSymbolInfo>
}

fun WithFundToBuyTradeFormInputId.extractSymbol(map: Map<String, String?>): String? = map[fundToBuy]?.let { symbolCoder.decode(it)?.symbol }
