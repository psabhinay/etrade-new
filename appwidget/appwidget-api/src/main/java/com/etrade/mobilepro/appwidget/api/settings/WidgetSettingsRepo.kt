package com.etrade.mobilepro.appwidget.api.settings

interface WidgetSettingsRepo {
    suspend fun loadSettings(id: WidgetId): WidgetSettings
    fun loadSettingsBlocking(id: WidgetId): WidgetSettings

    /**
     * Returns true if operation was successful
     */
    suspend fun saveSettings(id: WidgetId, settings: WidgetSettings): Boolean
    fun saveSettingsBlocking(id: WidgetId, settings: WidgetSettings): Boolean

    /**
     * Returns true if operation was successful
     */
    suspend fun deleteSettings(id: WidgetId): Boolean
    fun deleteSettingsBlocking(id: WidgetId): Boolean
}
