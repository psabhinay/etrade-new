package com.etrade.mobilepro.messagecenter.data.dto

import com.etrade.mobilepro.backends.af.ResultDto
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "CreateSecureMsgResponse", strict = false)
data class SendNewMessageDto(
    @field:Element(name = "Result", required = false)
    var resultDto: ResultDto? = null
)
