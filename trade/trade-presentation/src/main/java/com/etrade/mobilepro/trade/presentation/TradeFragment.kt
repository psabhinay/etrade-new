package com.etrade.mobilepro.trade.presentation

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.EditText
import android.widget.TextView
import androidx.core.view.children
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.balance.viewmodel.AccountBalanceViewModel
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.dialog.viewmodel.DialogViewModel
import com.etrade.mobilepro.dynamic.form.ui.FormView
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.order.details.router.OrderConfirmationAction
import com.etrade.mobilepro.orders.sharedviewmodel.OrderSharedViewModel
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quote.search.history.QuoteSearchHistoryViewModel
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.trade.dropdown.form.TradeSelectInput
import com.etrade.mobilepro.trade.dropdown.form.TradeSelectInputViewFactory
import com.etrade.mobilepro.trade.mutualfund.presentation.MutualFundTradeViewModel
import com.etrade.mobilepro.trade.optionlegselect.TradeLegBidAskViewModel
import com.etrade.mobilepro.trade.presentation.confirmation.KEY_CONFIRMATION_ACTION
import com.etrade.mobilepro.trade.presentation.form.approval.ApprovalLinkComponent
import com.etrade.mobilepro.trade.presentation.form.approval.ApprovalLinkComponentViewFactory
import com.etrade.mobilepro.trade.presentation.form.component.AccountDataPointsComponent
import com.etrade.mobilepro.trade.presentation.form.component.StrategyTypeInput
import com.etrade.mobilepro.trade.presentation.form.component.TradeLegsInput
import com.etrade.mobilepro.trade.presentation.form.component.view.factory.AccountDataPointsComponentViewFactory
import com.etrade.mobilepro.trade.presentation.form.component.view.factory.StrategyTypeInputViewFactory
import com.etrade.mobilepro.trade.presentation.form.component.view.factory.TradeLegsInputViewFactory
import com.etrade.mobilepro.trade.presentation.util.OptionsApprovalUpgradeViewDelegate
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetClicksViewModel
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetViewDelegate
import com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer.BottomSheetNavContainer
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.fragment.OrientationLock
import com.etrade.mobilepro.util.android.goneUnless
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.BlockingLoadingIndicatorViewDelegate
import com.etrade.mobilepro.viewdelegate.LifecycleObserverViewDelegate
import com.etrade.mobilepro.viewdelegate.ViewDelegate
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.google.android.material.textfield.TextInputLayout
import com.squareup.moshi.Moshi
import javax.inject.Inject

private const val QUOTE_WIDGET_BASE = "advancedBase"
private const val QUOTE_WIDGET_BID_ASK = "advancedBidAsk"

@RequireLogin
class TradeFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    tradeRouter: TradeRouter,
    dialogViewModel: DialogViewModel,
    moshi: Moshi,
    orientationLock: OrientationLock,
    private val marketTradeStatusDelegate: MarketTradeStatusDelegate,
    private val walkthroughViewModel: WalkthroughStatesViewModel
) : BaseTradeFormFragment(snackbarUtilFactory, viewModelFactory, tradeRouter, dialogViewModel, orientationLock, moshi, walkthroughViewModel) {

    override val formViewModel: TradeViewModel by viewModels { viewModelFactory }

    private val viewModelProvider: ViewModelProvider by lazy { ViewModelProvider(this, viewModelFactory) }

    private val advancedOrderQuoteWidgetViewModel: QuotesWidgetViewModel by lazy {
        viewModelProvider.get(QUOTE_WIDGET_BASE, QuotesWidgetViewModel::class.java)
    }
    private val advancedBidAskOrderQuoteWidgetViewModel: QuotesWidgetViewModel by lazy {
        viewModelProvider.get(QUOTE_WIDGET_BID_ASK, QuotesWidgetViewModel::class.java)
    }
    private val balanceViewModel: AccountBalanceViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }
    private val mutualFundTradeViewModel: MutualFundTradeViewModel by viewModels { viewModelFactory }
    private val orderSharedViewModel: OrderSharedViewModel by activityViewModels { viewModelFactory }
    private val quoteSearchHistoryViewModel: QuoteSearchHistoryViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }
    private val tradeLegBidAskViewModel: TradeLegBidAskViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }

    private val actionSheetClicksViewModel: ActionSheetClicksViewModel by viewModels { viewModelFactory }

    private val actionSheetViewDelegate: ActionSheetViewDelegate by lazy {
        ActionSheetViewDelegate(
            formViewModel,
            actionSheetClicksViewModel,
            childFragmentManager
        )
    }

    private val viewDelegates: Iterable<ViewDelegate> by lazy {
        listOf(
            OptionsApprovalUpgradeViewDelegate(requireContext(), formViewModel.optionsApprovalUpgradeUrl),
            actionSheetViewDelegate,
            BlockingLoadingIndicatorViewDelegate(formViewModel, childFragmentManager),
            LifecycleObserverViewDelegate(listOf(advancedOrderQuoteWidgetViewModel, advancedBidAskOrderQuoteWidgetViewModel))
        )
    }

    private var layoutListener: ViewTreeObserver.OnGlobalLayoutListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionSheetViewDelegate.onCreate(savedInstanceState)
    }

    override fun onDestroyView() {
        view?.viewTreeObserver?.removeOnGlobalLayoutListener(layoutListener)
        super.onDestroyView()
    }

    override fun initFormView(formView: FormView) {
        super.initFormView(formView)
        formView.apply {
            addViewFactory(AccountDataPointsComponent::class, AccountDataPointsComponentViewFactory())
            addViewFactory(ApprovalLinkComponent::class, ApprovalLinkComponentViewFactory())
            addViewFactory(StrategyTypeInput::class, StrategyTypeInputViewFactory())
            addViewFactory(TradeLegsInput::class, TradeLegsInputViewFactory())
            addViewFactory(TradeSelectInput::class, TradeSelectInputViewFactory())

            // To support QA team automation test, all trade fields should have content description = label
            layoutListener?.let {
                viewTreeObserver.removeOnGlobalLayoutListener(it)
            }
            layoutListener = ViewTreeObserver.OnGlobalLayoutListener { updateContentDescription(formView, isInitialCall = true) }
            viewTreeObserver.addOnGlobalLayoutListener(layoutListener)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        formViewModel.apply {
            editOrderSignal.observe(viewLifecycleOwner, Observer { onEditOrderSignal(it) })
            tradeOrderFlowEndSignal.observe(viewLifecycleOwner, Observer { onTradeOrderFlowEnd() })
        }

        viewDelegates.forEach {
            it.observe(viewLifecycleOwner)
        }

        marketTradeStatusDelegate.marketStatus.observeBy(
            viewLifecycleOwner,
            {
                it.consume { status ->
                    view.findViewById<TextView>(R.id.market_halt_banner).goneUnless(status.isMarketHalted)
                }
            }
        )
    }

    private fun onEditOrderSignal(editOrderEvent: ConsumableLiveEvent<EditOrderEvent>) {
        editOrderEvent.consume { event ->
            activity?.let { event(findNavController(), it) }
        }
    }

    override fun beforeFormFetch() {
        super.beforeFormFetch()
        val action = arguments?.getSerializable(KEY_CONFIRMATION_ACTION) as? OrderConfirmationAction
        action?.let {
            formViewModel.orderConfirmationAction = it
        }

        formViewModel.advancedOrderBidAskViewModel = advancedBidAskOrderQuoteWidgetViewModel
        formViewModel.advancedQuoteWidgetViewModel = advancedOrderQuoteWidgetViewModel
        formViewModel.balanceViewModel = balanceViewModel
        formViewModel.mutualFundTradeViewModel = mutualFundTradeViewModel
        formViewModel.orderSharedViewModel = orderSharedViewModel
        formViewModel.quoteSearchHistoryViewModel = quoteSearchHistoryViewModel
        formViewModel.tradeLegBidAskViewModel = tradeLegBidAskViewModel

        tradeLegBidAskViewModel.quoteWidgetViewModel = quoteWidgetViewModel
    }

    private fun updateContentDescription(formView: FormView, viewGroup: ViewGroup = formView, isInitialCall: Boolean) {
        viewGroup.children.filterIsInstance<ViewGroup>().forEach { child ->
            when (child) {
                is TextInputLayout -> child.editText?.let {
                    it.contentDescription = child.hint
                    refreshContentDescription(formView, it, isInitialCall)
                }
                else -> updateContentDescription(formView, child, isInitialCall)
            }
        }
    }

    private fun refreshContentDescription(formView: FormView, editText: EditText, isInitialCall: Boolean) {
        if (isInitialCall) {
            editText.doAfterTextChanged {
                updateContentDescription(formView, isInitialCall = false)
            }
        }
    }

    private fun onTradeOrderFlowEnd() {
        val container = parentFragment?.parentFragment
        if (container is BottomSheetNavContainer) {
            container.dismiss()
        }
    }
}
