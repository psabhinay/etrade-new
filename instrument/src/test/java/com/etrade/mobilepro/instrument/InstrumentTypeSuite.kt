package com.etrade.mobilepro.instrument

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class InstrumentTypeSuite {

    @Test
    fun testFactoryMethod() {
        InstrumentType.values().forEach { expected ->
            val actual = InstrumentType.from(expected.typeCode)
            assertEquals(expected, actual)
        }
    }

    @Test
    fun testFactoryMethodNullArgument() {
        assertEquals(InstrumentType.UNKNOWN, InstrumentType.from(null))
    }

    @Test
    fun testFactoryMethodUnknownArgument() {
        assertEquals(InstrumentType.UNKNOWN, InstrumentType.from("Kotlin"))
    }

    @Test
    fun testIsIndex() {
        checkBooleanProperty(setOf(InstrumentType.INDX)) { isIndex }
    }

    @Test
    fun testIsMutualFundOrMoneyMarketFund() {
        checkBooleanProperty(setOf(InstrumentType.MF, InstrumentType.MMF)) { isMutualFundOrMoneyMarketFund }
    }

    @Test
    fun testIsOption() {
        checkBooleanProperty(setOf(InstrumentType.OPTN, InstrumentType.OPTNC, InstrumentType.OPTNP)) { isOption }
    }

    @Test
    fun testOptionType() {
        val optionTypes = mapOf(
            InstrumentType.OPTNP to "Put",
            InstrumentType.OPTNC to "Call"
        )
        InstrumentType.values().forEach { item ->
            val expected = optionTypes[item]
            val actual = item.optionType
            assertEquals(expected, actual)
        }
    }

    private fun checkBooleanProperty(expectedTypes: Set<InstrumentType>, property: InstrumentType.() -> Boolean) {
        InstrumentType.values().forEach { item ->
            val expected = expectedTypes.contains(item)
            val actual = item.property()
            assertEquals(expected, actual)
        }
    }
}
