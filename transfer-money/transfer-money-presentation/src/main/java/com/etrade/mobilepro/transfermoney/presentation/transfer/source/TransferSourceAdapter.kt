package com.etrade.mobilepro.transfermoney.presentation.transfer.source

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.livedata.doOnNext
import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import javax.inject.Inject

interface TransferSourceAdapter {
    class SelectableTransferSource(
        val from: LiveData<List<DropDownMultiSelector.Selectable<TransferMoneyAccount>>>,
        val to: LiveData<List<DropDownMultiSelector.Selectable<TransferMoneyAccount>>>
    )

    fun adaptTransferSourceHolderToSelectable(
        holderData: LiveData<TransferSourceHolder>,
        holderUpdateListener: (TransferSourceHolder) -> Unit
    ): SelectableTransferSource
}

class DefaultTransferSourceAdapter @Inject constructor(
    private val resources: Resources,
    private val filterer: TransferSourceFilterer
) : TransferSourceAdapter {

    private var prevHolder: TransferSourceHolder? = null

    private fun shouldClearFromField(newItem: TransferSourceHolder, oldItem: TransferSourceHolder?): Boolean {
        return newItem.selectedTo != null && oldItem?.selectedFrom != null && oldItem.selectedFrom?.isExternalAccount == true
    }

    override fun adaptTransferSourceHolderToSelectable(
        holderData: LiveData<TransferSourceHolder>,
        holderUpdateListener: (TransferSourceHolder) -> Unit
    ): TransferSourceAdapter.SelectableTransferSource {
        val commonSource = holderData
            .doOnNext {
                if (it.selectedFrom?.isExternalAccount == true && it.selectedTo?.isExternalAccount == true) {
                    if (shouldClearFromField(it, prevHolder)) {
                        holderUpdateListener.invoke(it.copyAsHolder(selectedFrom = null))
                    } else {
                        holderUpdateListener.invoke(it.copyAsHolder(selectedTo = null))
                    }
                }
                prevHolder = it
            }
        val fromData = adaptFromData(commonSource)
        val toData = adaptToData(commonSource)
        return TransferSourceAdapter.SelectableTransferSource(fromData, toData)
    }

    private fun adaptToData(commonSource: LiveData<TransferSourceHolder>): LiveData<List<DropDownMultiSelector.Selectable<TransferMoneyAccount>>> =
        commonSource
            .map { holder ->
                filterer
                    .filterToAccounts(holder)
                    .map { transferMoneyAccount ->
                        DropDownMultiSelector.Selectable(
                            title = transferMoneyAccount.title,
                            value = transferMoneyAccount,
                            selected = transferMoneyAccount == holder.selectedTo,
                            contentDescriptionFormatter = mapContentDescription
                        )
                    }
            }

    private fun adaptFromData(commonSource: LiveData<TransferSourceHolder>): LiveData<List<DropDownMultiSelector.Selectable<TransferMoneyAccount>>> =
        commonSource
            .map { holder ->
                filterer
                    .filterFromAccounts(holder)
                    .map { transferMoneyAccount ->
                        DropDownMultiSelector.Selectable(
                            title = transferMoneyAccount.title,
                            value = transferMoneyAccount,
                            selected = transferMoneyAccount == holder.selectedFrom,
                            contentDescriptionFormatter = mapContentDescription
                        )
                    }
            }

    private val mapContentDescription = { account: TransferMoneyAccount ->
        resources.accountNameContentDescription(account.title) ?: account.title
    }
}
