package com.etrade.mobilepro.login.presentation.widget

import android.content.Context
import android.util.AttributeSet
import android.view.ContextThemeWrapper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.login.widget.LoginResultDialogFragment
import com.etrade.mobilepro.util.android.extension.showSoftKeyboard

class FullScreenLoginWidget : LoginWidget {
    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    private val activity: AppCompatActivity = ((context as ContextThemeWrapper).baseContext as AppCompatActivity)
    override var shouldFinishActivity: Boolean = true // Finish Activity containing Full screen Widget

    override fun onFinishInflate() {
        super.onFinishInflate()
        binding.loginEtradeLogo.visibility = View.VISIBLE
        binding.loginRememberUserCheckbox.visibility = View.VISIBLE
        binding.signUpButton.visibility = View.VISIBLE
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val lifecycleOwner = (context as ContextThemeWrapper).baseContext as? LifecycleOwner
            ?: throw IllegalArgumentException("view component is not a lifecycleowner implementation")
        attachTo(lifecycleOwner)
        LoginResultDialogFragment.restore(activity.supportFragmentManager)?.onAttached()
    }

    override fun initLoginScreen(savedUserName: String?, isRememberUserChecked: Boolean) {
        binding.loginRememberUserCheckbox.isChecked = isRememberUserChecked

        if (isRememberUserChecked && savedUserName?.isNotEmpty() == true) {
            binding.loginUserName.setText(savedUserName)
        }

        if (binding.loginUserName.text.isNullOrBlank()) {
            binding.loginUserName.requestFocus()
        } else {
            binding.loginPassword.requestFocus()
        }

        showSoftKeyboard()
    }
}
