package com.etrade.mobilepro.ui_demo.actionsheet

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.ui_demo.actionsheet.default.DefaultStyleViewModel
import com.etrade.mobilepro.ui_demo.actionsheet.default.ExternalAccountStyleViewModel
import com.etrade.mobilepro.ui_demo.databinding.ActivityActionSheetBinding
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetClicksViewModel
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetStyle
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetViewDelegate
import com.etrade.mobilepro.util.android.binding.viewBinding

class ActionSheetActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityActionSheetBinding::inflate)

    private val clicksViewModel :ActionSheetClicksViewModel by viewModels()

    private val defaultStyleViewModel = DefaultStyleViewModel()
    private val externalAccountStyleViewModel = ExternalAccountStyleViewModel()
    private val defaultActionSheetViewDelegate: ActionSheetViewDelegate by lazy {
        ActionSheetViewDelegate(
            defaultStyleViewModel,
            clicksViewModel,
            supportFragmentManager
        )
    }
    private val externalAccountActionSheetViewDelegate: ActionSheetViewDelegate by lazy {
        ActionSheetViewDelegate(
            externalAccountStyleViewModel,
            clicksViewModel,
            supportFragmentManager,
            ActionSheetStyle.ExternalAccountVerification
        )
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        defaultActionSheetViewDelegate.onCreate(savedInstanceState)
        defaultActionSheetViewDelegate.observe(this)
        externalAccountActionSheetViewDelegate.onCreate(savedInstanceState)
        externalAccountActionSheetViewDelegate.observe(this)

        binding.defaultBubbles.setOnClickListener {
            defaultStyleViewModel.showActionSheet()
        }
        binding.externalAccount.setOnClickListener {
            externalAccountStyleViewModel.showActionSheet()
        }

        defaultStyleViewModel.result.observe(this) {
            with(binding.result) {
                append(it)
                append("\n")
            }
        }
        externalAccountStyleViewModel.result.observe(this) {
            with(binding.result) {
                append(it)
                append("\n")
            }
        }
    }
}
