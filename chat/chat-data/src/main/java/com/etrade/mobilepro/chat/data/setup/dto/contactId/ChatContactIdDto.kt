package com.etrade.mobilepro.chat.data.setup.dto.contactId

import com.etrade.mobilepro.chat.data.common.dto.ChatMetaDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChatContactIdDto(

    @Json(name = "data")
    val data: ChatContactIdDataDto? = null
)

@JsonClass(generateAdapter = true)
data class ChatContactIdDataDto(

    @Json(name = "Messages")
    val messages: ChatContactIdMessagesDto,

    @Json(name = "metadata")
    val metadata: ChatMetaDataDto? = null
)
