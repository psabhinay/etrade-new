package com.etrade.mobilepro.common

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.etrade.mobilepro.util.android.network.ConnectionNotifier

private const val DEFAULT_UP_BUTTON_CONTENT_DESCRIPTION = "Navigate up"

data class ToolbarActionEnd(
    @StringRes val text: Int = 0,
    val contentDescription: String? = null,
    val onClick: View.OnClickListener,
    @ColorRes val textColor: Int? = null,
    val isIcon: Boolean = false,
    @DrawableRes val icon: Int = 0
)

data class Navigation(
    @DrawableRes val icon: Int,
    val onClick: View.OnClickListener,
    val contentDescription: String?
)

/**
 * Sets up Toolbar for an activity.
 * When calling from fragment, call no sooner than from onStart() or use 'post' method to avoid crash.
 *
 * @param titleResId title resource ID
 * @param upButtonResId drawable resource ID for 'up' button icon (0 is for default icon)
 */
fun AppCompatActivity.setupToolbarWithUpButton(
    @StringRes titleResId: Int,
    @StringRes titleContentDescription: Int? = null,
    @DrawableRes upButtonResId: Int = 0,
    actionEnd: ToolbarActionEnd? = null
) {
    setupToolbarWithUpButton(
        title = getString(titleResId),
        titleContentDescription = titleContentDescription?.let(::getString),
        upButtonResId = upButtonResId,
        actionEnd = actionEnd
    )
}

/**
 * Sets up Toolbar for an activity.
 *
 * @param title title for the toolbar.
 * @param upButtonResId drawable resource ID for 'up' button icon (0 is for default icon)
 */
@Suppress("LongMethod")
fun AppCompatActivity.setupToolbarWithUpButton(
    title: String,
    titleContentDescription: String? = null,
    @DrawableRes upButtonResId: Int = 0,
    actionEnd: ToolbarActionEnd? = null
) {
    val toolBar = findViewById<Toolbar>(R.id.toolBar)
    val networkUnavailable = findViewById<TextView>(R.id.network_unavailable)
    val toolbarActionEnd = findViewById<TextView>(R.id.toolbarActionEnd)
    val toolbarTitle = findViewById<TextView>(R.id.toolbarTitle)

    setSupportActionBar(toolBar)

    val connectionNotifier = ConnectionNotifier(baseContext)
    connectionNotifier.createNotifier(object : ConnectionNotifier.ConnectionNotification {
        override fun onNetworkAvailable() = runOnUiThread {
            networkUnavailable?.visibility = View.GONE
        }

        override fun onNetworkLost() = runOnUiThread {
            networkUnavailable?.visibility = View.VISIBLE
        }
    })?.let { connectivityObserver ->
        lifecycle.addObserver(connectivityObserver)
    }

    supportActionBar?.apply {
        setDisplayShowTitleEnabled(false)
        setDisplayHomeAsUpEnabled(true)

        if (upButtonResId != 0) {
            setHomeAsUpIndicator(upButtonResId)
            setHomeActionContentDescription(getNavigationButtonDescription(upButtonResId))
        } else {
            if (toolBar?.isDefaultNavigationContentDescription() == true) {
                setHomeActionContentDescription(R.string.label_back)
            }
        }
    }

    actionEnd?.let { actionEndConfig ->
        actionEndConfig.textColor?.let {
            toolbarActionEnd.setTextColor(getColor(it))
        }
        toolbarActionEnd.setText(actionEndConfig.text)
        toolbarActionEnd.setOnClickListener(actionEndConfig.onClick)
        toolbarActionEnd.visibility = View.VISIBLE
        toolbarActionEnd.contentDescription = actionEndConfig.contentDescription
    }

    toolbarTitle.text = title
    toolbarTitle.contentDescription = titleContentDescription ?: getToolbarTitleContentDescription(title, this)
}

private fun Toolbar.isDefaultNavigationContentDescription() = navigationContentDescription == DEFAULT_UP_BUTTON_CONTENT_DESCRIPTION

@Suppress("LongMethod")
fun Fragment.setupToolbar(
    title: String? = null,
    navigation: Navigation? = null,
    actionEnd: ToolbarActionEnd? = null,
    runOnFirstFocus: ((Toolbar, start: TextView?, title: TextView?, end: TextView?) -> Unit)? = null
) {
    val view = requireView()
    val toolBar = view.findViewById<Toolbar>(R.id.toolBar)
    val toolbarActionEnd = view.findViewById<TextView>(R.id.toolbarActionEnd)
    val toolbarActionEndIcon = view.findViewById<ImageView>(R.id.toolbarActionEndIcon)
    val toolbarActionStart = view.findViewById<TextView>(R.id.toolbarActionStart)
    val toolbarTitle = view.findViewById<TextView>(R.id.toolbarTitle)

    title?.let {
        toolbarTitle.text = it
        toolbarTitle.contentDescription = getToolbarTitleContentDescription(it, requireContext())
        runOnFirstFocus?.invoke(toolBar, toolbarActionStart, toolbarTitle, toolbarActionEnd)
    }
    navigation?.let { nav ->
        toolBar.setNavigationIcon(nav.icon)
        toolBar.navigationContentDescription = nav.contentDescription
        toolBar.setNavigationOnClickListener(nav.onClick)
    }
    if (actionEnd == null) {
        toolbarActionEnd.visibility = View.INVISIBLE
    } else {
        if (actionEnd.isIcon) {
            toolbarActionEndIcon.apply {
                setToolbarActionEndIconAndContentDescription(actionEnd.icon, actionEnd.contentDescription)
                setOnClickListener(actionEnd.onClick)
                visibility = View.VISIBLE
            }
        } else {
            toolbarActionEnd.apply {
                setToolbarActionTextAndContentDescription(actionEnd.text, actionEnd.contentDescription)
                setOnClickListener(actionEnd.onClick)
                visibility = View.VISIBLE
            }
        }
    }
    initConnectionNotifier()
}

private fun Fragment.initConnectionNotifier() {
    val connectionNotifier = ConnectionNotifier(requireContext())
    connectionNotifier.createNotifier(object : ConnectionNotifier.ConnectionNotification {
        override fun onNetworkAvailable() {
            activity?.runOnUiThread {
                networkUnavailableView?.visibility = View.GONE
            }
        }

        override fun onNetworkLost() {
            activity?.runOnUiThread {
                networkUnavailableView?.visibility = View.VISIBLE
            }
        }
    })?.let { connectivityObserver ->
        lifecycle.addObserver(connectivityObserver)
    }
}

fun Context.getNavigationButtonDescription(@DrawableRes icon: Int): String? {
    return when (icon) {
        R.drawable.ic_close_black, R.drawable.ic_close -> getString(R.string.label_close)
        // In case if we setup toolbar from fragment, we could get "Unlabeled button" for back button, instead of "Navigate Up button"
        // so description should be set explicitly
        R.drawable.ic_arrow_back, R.drawable.ic_arrow_back_black, R.drawable.ic_arrow_back_purple -> getString(R.string.label_back)
        else -> null
    }
}

fun getToolbarTitleContentDescription(title: CharSequence, context: Context): String {
    return context.getString(R.string.label_header_description, title)
}

fun TextView.setToolbarActionTextAndContentDescription(@StringRes textResId: Int, contentDescription: CharSequence? = null) {
    setToolbarActionTextAndContentDescription(context.getString(textResId), contentDescription)
}

fun TextView.setToolbarActionTextAndContentDescription(text: CharSequence?, contentDescription: CharSequence? = null) {
    this.text = text
    this.contentDescription = contentDescription ?: context.getString(R.string.button_content_description_template, text ?: "")
}

fun ImageView.setToolbarActionEndIconAndContentDescription(icon: Int, contentDescription: CharSequence? = null) {
    this.setImageResource(icon)
    this.contentDescription = contentDescription
}

fun Toolbar.setToolbarIcon(isLastPageInStack: Boolean, isWhite: Boolean = false) {
    if (isLastPageInStack) {
        setNavigationIcon(
            if (isWhite) {
                R.drawable.ic_close
            } else {
                R.drawable.ic_close_black
            }
        )
        setNavigationContentDescription(R.string.label_close)
    } else {
        setNavigationIcon(
            if (isWhite) {
                R.drawable.ic_arrow_back
            } else {
                R.drawable.ic_arrow_back_black
            }
        )
        setNavigationContentDescription(R.string.label_back)
    }
}

val Activity.toolBarContainer: ViewGroup
    get() = findViewById(R.id.view_toolbar)

val Activity.toolbarView: Toolbar
    get() = findViewById(R.id.toolBar)

val Activity.toolbarTitleView: TextView?
    get() = findViewById(R.id.toolbarTitle)

val Fragment.toolbarTitleView: TextView?
    get() = activity?.toolbarTitleView

val Fragment.overlayToolbarTitleView: TextView?
    get() = view?.findViewById(R.id.toolbarTitle) // Drop down components depend on this

val Activity.toolbarActionStartView: TextView?
    get() = findViewById(R.id.toolbarActionStart)

val Activity.toolbarActionEndView: TextView?
    get() = findViewById(R.id.toolbarActionEnd)

val Fragment.toolbarActionEndView: TextView?
    get() = view?.findViewById(R.id.toolbarActionEnd)

val Activity.networkUnavailableView: TextView?
    get() = findViewById(R.id.network_unavailable)

val Fragment.networkUnavailableView: TextView?
    get() = view?.findViewById(R.id.network_unavailable)

val Activity.marketTradeHaltBannerView: TextView?
    get() = findViewById(R.id.market_halt_banner)
