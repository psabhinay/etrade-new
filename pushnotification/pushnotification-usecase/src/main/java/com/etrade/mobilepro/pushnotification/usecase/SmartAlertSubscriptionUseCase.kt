package com.etrade.mobilepro.pushnotification.usecase

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.pushnotification.api.repo.AlertPushSettingsRepo
import com.etrade.mobilepro.util.flatMap
import com.google.firebase.messaging.FirebaseMessaging
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

private const val SMART_ALERTS_TOPIC = "global"

/**
 * Use case which offers common operations related to a smart alert subscription
 */
interface SmartAlertSubscriptionUseCase {
    suspend fun isSubscribed(): Boolean
    suspend fun subscribe(): ETResult<Unit>
    suspend fun subscribeWith(token: String): ETResult<Unit>
    suspend fun unsubscribe(): ETResult<Unit>
}

class SmartAlertSubscriptionUseCaseImpl @Inject constructor(
    private val repo: AlertPushSettingsRepo,
    private val isAuthenticated: () -> Boolean
) : SmartAlertSubscriptionUseCase {

    override suspend fun isSubscribed(): Boolean {
        return repo.getRegisteredToken().isSuccess
    }

    override suspend fun subscribe(): ETResult<Unit> =
        runCatchingET { getFirebaseToken() }
            .flatMap { subscribeWith(it) }

    override suspend fun subscribeWith(token: String): ETResult<Unit> {
        return repo.unregisterCurrentToken()
            .flatMap {
                if (isAuthenticated()) {
                    repo.registerToken(token)
                        .mapCatching { subscribeFirebaseTopics() }
                } else {
                    ETResult.failure(IllegalStateException("User must be authenticated in order to subscribe to smart alerts"))
                }
            }
    }

    override suspend fun unsubscribe(): ETResult<Unit> {
        return repo.unregisterCurrentToken()
    }

    private suspend fun getFirebaseToken(): String {
        return suspendCoroutine { cont ->
            FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
                task.takeIf { it.isSuccessful }
                    ?.result
                    ?.takeIf { it.isNotEmpty() }
                    ?.let(cont::resume)
                    ?: cont.resumeWithException(
                        IllegalStateException(
                            "Could not get Firebase token",
                            task.exception
                        )
                    )
            }
        }
    }

    private suspend fun subscribeFirebaseTopics() {
        return suspendCoroutine { cont ->
            FirebaseMessaging.getInstance()
                .subscribeToTopic(SMART_ALERTS_TOPIC)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        cont.resume(Unit)
                    } else {
                        cont.resumeWithException(IllegalStateException("Could not subscribe to  Firebase topics: $SMART_ALERTS_TOPIC", task.exception))
                    }
                }
        }
    }
}
