@file:Suppress("LongMethod", "LongParameterList")
package com.etrade.mobilepro.feature.presentation.compose

import com.etrade.mobilepro.feature.BaseFeature
import com.etrade.mobilepro.feature.BaseFeatureCategory
import com.etrade.mobilepro.feature.FeatureCategory
import com.etrade.mobilepro.feature.presentation.viewmodel.AsyncActionViewModel
import com.etrade.mobilepro.feature.presentation.viewmodel.FeatureFlagItemViewModel
import com.etrade.mobilepro.feature.presentation.viewmodel.FeatureFlagsViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flowOf

internal fun itemViewModel(
    id: String = "",
    flagEnabled: Boolean = true,
    isSettable: Boolean = true,
    remoteConfig: Boolean = false,
    category: FeatureCategory? = null,
    explanation: String? = "This is the feature $id explanation. ".repeat(2)
) = FeatureFlagItemViewModel(
    enabledFactory = { flowOf(true) },
    providerStatusFactory = { MutableStateFlow("Store: ") },
    feature = BaseFeature(
        category = category,
        title = "Feature $id Title",
        key = "feature_key_$id",
        defaultValue = flagEnabled,
        explanation = explanation
    ),
    remoteConfig = remoteConfig,
    isSettable = isSettable,
    setEnabled = {}
)

internal fun defaultItemStatus(flagEnabled: Boolean = true): String =
    "LocalStore: No, Firebase: No, Default: ${if (flagEnabled) "Yes" else "No"}"

internal fun asyncActionViewModel(title: String, refreshing: Boolean) =
    object : AsyncActionViewModel {
        override val isExecuting: StateFlow<Boolean> = MutableStateFlow(refreshing)
        override val title = title
        override fun execute() { /* no-op */ }
    }

internal fun listViewModel() = object : FeatureFlagsViewModel() {
    private val category1 =
        BaseFeatureCategory("First Category", "This is the first category explanation")
    private val category2 =
        BaseFeatureCategory("Second Category", "This is the second category explanation")

    override val featureFlagItemModels: List<FeatureFlagItemViewModel>
        get() = listOf(
            itemViewModel(
                id = "one",
                flagEnabled = true,
                isSettable = true,
                category = category1,
            ),
            itemViewModel(
                id = "two",
                flagEnabled = false,
                isSettable = true,
                category = category1
            ),
            itemViewModel(
                id = "three",
                flagEnabled = true,
                isSettable = false,
                category = category2,
                remoteConfig = true
            ),
            itemViewModel(
                id = "four",
                flagEnabled = false,
                isSettable = true,
                category = category2
            ),
        )
    override val actionItemModels: List<AsyncActionViewModel>
        get() = listOf(
            asyncActionViewModel("First Action Item", false),
            asyncActionViewModel("Second Action Item (Refreshing)", true),
        )
}
