package com.etrade.mobilepro.appwidget.api

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.watchlistapi.WatchlistEntry

interface WidgetViewStateRepo {
    suspend fun getIndicesViewState(): IndicesViewState
    suspend fun setIndicesViewState(viewState: IndicesViewState?)
    suspend fun isWidgetPlaced(): Boolean
    suspend fun setWidgetPlaced(isPlaced: Boolean)
    suspend fun getLatestResolvedWatchlistEntries(): ETResult<List<WatchlistEntry>>
    suspend fun setLatestResolvedWatchlistEntries(entries: List<WatchlistEntry>?)
}
