package com.etrade.mobilepro.msaccounts.dao

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class MsAccount(
    @PrimaryKey
    var accountUuid: String = "",
    var accountDisplayName: String = "",
    var accountDisplayNumber: String = "",
    var balance: String = "",
    var balanceType: String = "",
    var instType: String = "",
    var keyAccountId: String = "",
    var platform: String = ""
) : RealmObject()
