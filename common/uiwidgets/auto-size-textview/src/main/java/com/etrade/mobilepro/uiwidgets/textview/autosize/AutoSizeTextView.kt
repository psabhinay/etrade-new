package com.etrade.mobilepro.uiwidgets.textview.autosize

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.TextViewCompat
import com.etrade.mobilepro.uiwidgets.autosize.textview.R

class AutoSizeTextView : AppCompatTextView {
    private var minTextSize = 0
    private var maxTextSize = 0
    private var granularity = 0

    constructor(context: Context) : super(context) {
        init(context, null, 0)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init(context, attrs, defStyle)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int) {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.AutoSizeTextView,
            defStyle, 0
        ).apply {
            try {
                minTextSize = getDimensionPixelSize(
                    R.styleable.AutoSizeTextView_auto_size_minTextSize,
                    context.resources.getDimensionPixelSize(R.dimen.font_size_xsmall)
                )
                maxTextSize = getDimensionPixelSize(
                    R.styleable.AutoSizeTextView_auto_size_maxTextSize,
                    context.resources.getDimensionPixelSize(R.dimen.font_size_large)
                )
                granularity = getDimensionPixelSize(
                    R.styleable.AutoSizeTextView_auto_size_granularity,
                    context.resources.getDimensionPixelSize(R.dimen.auto_size_granularity)
                )
            } finally {
                recycle()
            }
        }
    }

    override fun setText(text: CharSequence?, type: BufferType?) {
        disableAutoSizing()
        val textLength = text?.length ?: 0
        if (textLength != 0 && textLength < getText().length) {
            setTextSize(TypedValue.COMPLEX_UNIT_PX, maxTextSize.toFloat())
        }
        super.setText(text, type)
        post { enableAutoSizing() } // enable after the view is laid out and measured at max text size
    }

    private fun disableAutoSizing() {
        TextViewCompat.setAutoSizeTextTypeWithDefaults(this, TextViewCompat.AUTO_SIZE_TEXT_TYPE_NONE)
    }

    private fun enableAutoSizing() {
        TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(
            this,
            minTextSize, maxTextSize, granularity, TypedValue.COMPLEX_UNIT_PX
        )
    }
}
