package com.etrade.mobilepro.trade.presentation.conditional

import android.content.res.Resources
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.inputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.presentation.BaseTradeFormViewModel
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction
import com.etrade.mobilepro.trade.presentation.form.delegate.InboxMessagesDelegate
import com.etrade.mobilepro.trade.presentation.usecase.CreateConditionalTradeFormParameter
import com.etrade.mobilepro.trade.presentation.usecase.CreateConditionalTradeFormUseCase
import com.etrade.mobilepro.trade.presentation.usecase.TradeFormParameter
import com.etrade.mobilepro.trade.router.TradeRouter
import javax.inject.Inject

class ConditionalOrderFormViewModel @Inject constructor(
    createConditionalTradeFormUseCase: CreateConditionalTradeFormUseCase,
    inboxMessagesDelegate: InboxMessagesDelegate,
    resources: Resources,
    tradeRouter: TradeRouter,
    @Web baseUrl: String
) : BaseTradeFormViewModel(createConditionalTradeFormUseCase, resources, inboxMessagesDelegate, tradeRouter, baseUrl) {

    var accountId: String = ""
    var orderInputId: String = ""
    var orderNumber: Int = 0

    override fun handleAction(action: TradeFormAction) {
        when (action) {
            TradeFormAction.SubmitOrder -> onSubmitOrder()
        }
    }

    override fun createParameter(
        initialValues: InputValuesSnapshot,
        defaultValues: InputValuesSnapshot,
        disableIfNoChange: Boolean,
        onActionClickListener: OnActionClickListener
    ): TradeFormParameter {
        return CreateConditionalTradeFormParameter(
            accountId = accountId,
            conditionalOrderDisclosureViewModel = disclosureViewModel,
            defaultValues = defaultValues,
            onActionClickListener = onActionClickListener,
            optionsQuoteBidAskViewModel = advancedOrderBidAskViewModel,
            orderNumber = orderNumber,
            quoteWidgetViewModel = quoteWidgetViewModel,
            tradeOrderType = tradeOrderType,
            walkthroughViewModel = walkthroughViewModel
        )
    }

    fun cancel() = popBackStack()

    private fun onSubmitOrder() {
        allowConsumeFormInputValues = false
        sharedTradeViewModel.injectFormInputValues(mapOf(orderInputId to form.value?.inputValuesSnapshot))
        popBackStack()
    }

    private fun popBackStack() {
        navigateTo { it.popBackStack() }
    }
}
