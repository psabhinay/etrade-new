package com.etrade.mobilepro.tableviewutils.presentation.holder

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.etrade.mobilepro.tableviewutils.presentation.R
import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell
import com.etrade.mobilepro.util.android.accessibility.processAccessibilityStreamingEvent
import com.etrade.mobilepro.util.color.DataColor
import com.etrade.mobilepro.util.formatLetterRepresentationToNumber
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

open class DataCellViewHolder(
    itemView: View,
    private val onTextViewUpdated: ((TextView, DataCell) -> Unit)? = null
) : AbstractViewHolder(itemView), HasDataCell {

    var contentDescription: String? = null
        set(value) {
            field = value
            cell?.processAccessibility()
        }

    private val cellTextView: TextView = itemView.findViewById<View>(R.id.txtData) as TextView
    private val accessibilityFocusedView: View = ((cellTextView.parent as? View) ?: cellTextView).apply {
        // for accessibility purpose
        id = View.generateViewId()
    }

    override var cell: DataCell? = null
        set(value) {
            field = value
            value?.let {
                cellTextView.apply {
                    text = it.displayText
                    it.processAccessibility()
                    setTextColor(resolveDataColor(value.dataColor))
                    setBackgroundResource(resolveDataBackground(value.dataColor))
                    onTextViewUpdated?.invoke(this, it)
                }
            }
        }

    private fun DataCell.processAccessibility() {
        accessibilityFocusedView.processAccessibilityStreamingEvent(
            accessibilityText = displayText.formatLetterRepresentationToNumber(),
            accessibilityUserTouchText = this@DataCellViewHolder.contentDescription ?: accessibilityFocusedView.contentDescription?.toString()
        )
    }

    private fun resolveDataColor(dataColor: DataColor) = when (dataColor) {
        DataColor.POSITIVE -> ContextCompat.getColor(cellTextView.context, R.color.green)
        DataColor.NEGATIVE -> ContextCompat.getColor(cellTextView.context, R.color.red)
        DataColor.NEUTRAL -> ContextCompat.getColor(cellTextView.context, R.color.textColorPrimary)
        DataColor.FILLED_POSITIVE, DataColor.FILLED_NEGATIVE -> ContextCompat.getColor(cellTextView.context, R.color.white)
    }

    private fun resolveDataBackground(dataColor: DataColor) = when (dataColor) {
        DataColor.POSITIVE, DataColor.NEGATIVE, DataColor.NEUTRAL -> 0
        DataColor.FILLED_POSITIVE -> R.drawable.bg_data_cell_positive
        DataColor.FILLED_NEGATIVE -> R.drawable.bg_data_cell_negative
    }
}
