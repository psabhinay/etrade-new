package com.etrade.mobilepro.messagecenter.data.mapper

import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.data.dto.SupportMessageDetailsResponseDto
import com.etrade.mobilepro.testutil.XmlDeserializer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter

class SupportMessageDtoMapperTest {
    private val supportMsgDateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm a").withZone(ZoneId.of("America/New_York"))
    private val message = Message(
        status = "Message Sent",
        id = "",
        accountId = "",
        accountShortName = "",
        descId = "",
        subjectId = "",
        caseNo = "",
        caseTypeId = "",
        caseTypeDesc = "",
        isUnread = false,
        createdAt = null,
        createdAtFormatted = "",
        readAt = null
    )

    @Test
    fun testMapper() {
        val input = XmlDeserializer.getObjectFromXml("support_msg_response.xml", SupportMessageDetailsResponseDto::class.java)
        assertNotNull(input)

        val output = input.toSupportMessage(formatter = supportMsgDateFormatter, message = message)

        assertEquals("1088098", output.commonDetails.userId)
        assertEquals(false, output.commonDetails.replyEnabled)
        assertEquals("Bill Payment", output.commonDetails.subject)
        assertEquals("Message Sent", output.commonDetails.status)
        assertEquals(1546990740000L, output.commonDetails.createdAt?.toEpochMilli())
        assertEquals(1, output.history.size)
        assertEquals("test message", output.history[0].message)
        assertEquals(1546990740000L, output.history[0].createdAt?.toEpochMilli())
    }
}
