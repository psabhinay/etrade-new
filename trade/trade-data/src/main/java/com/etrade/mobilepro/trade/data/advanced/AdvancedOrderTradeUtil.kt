package com.etrade.mobilepro.trade.data.advanced

import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.getOptionType
import com.etrade.mobilepro.instrument.getStrikePrice
import com.etrade.mobilepro.trade.api.advanced.TrailingStopOrderDetails
import com.etrade.mobilepro.trade.api.advanced.TrailingStopOrderResponse
import com.etrade.mobilepro.trade.data.DAY_TRADING_FLAG
import com.etrade.mobilepro.trade.data.DAY_TRADING_VER
import com.etrade.mobilepro.trade.data.form.value.CommonAdvancedPriceTypeValueId
import com.etrade.mobilepro.trade.data.stock.TRUE
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.shared.WithTrailingStopInputId

internal const val VALUE_SECURITY_TYPE_STOCK = "1"
internal const val VALUE_SECURITY_TYPE_OPTION = "2"

internal const val KEY_SECURITY_TYPE = "securitytype"
internal const val PRIMARY_SECURITY_TYPE_KEY = "securitytype0"

internal const val OPTION_EXPIRATION_KEY = "expiration"
internal const val OPTION_STRIKE_PRICE_KEY = "strike"
internal const val OPTION_TYPE_KEY = "type"

internal const val ORDER_COUNT_KEY = "ordercount"
internal const val PREVIEW_ID_KEY = "previewid"
internal const val ORDER_TYPE_KEY = "ordertype"
internal const val ADVANCED_ORDER_TYPE_KEY = "aoordertype"
internal const val ORDER_TRIGGER_KEY = "ordertrigger"
internal const val INITIAL_STOP_PRICE_KEY = "initialstopprice"
internal const val POSITION_EFFECT_KEY = "positioneffect"
internal const val SIDE_KEY = "side"

internal const val FORM_TARGET_KEY = "_formtarget"
internal const val PLATFORM_KEY = "origSysCode"
internal const val PLATFORM_VALUE = "Android"
internal const val OPT_LEVEL = "optLevel"

internal const val TRADE_X_KEY = "trade.x"
internal const val PREVIEW_X_KEY = "preview.x"

internal typealias ParametersModifierWithInputId = (orderSnapshot: MutableMap<String, Any?>, index: Int, inputId: WithSymbolTradeFormInputId) -> Unit

/**
 * Parameters below are added to align with the legacy Android app and is needed to make do a successful request. Without it the request will fail.
 * previewX indicates position of a Web Form "Preview" button, a leftover of earliest design api to cater Web.
 * Value of 65 is arbitrarily chosen between 1 to 99.
 */
internal fun getAdvancedOrderPreviewTradeParams(formTarget: String, previewX: String): Map<String, String> {
    return mapOf(
        PLATFORM_KEY to PLATFORM_VALUE,
        FORM_TARGET_KEY to formTarget,
        PREVIEW_X_KEY to previewX,
        OPT_LEVEL to "4",
        DAY_TRADING_FLAG to TRUE,
        DAY_TRADING_VER to "1"
    )
}

/**
 * Parameters below are added to align with the legacy Android app and is needed to make do a successful request. Without it the request will fail.
 * trade.X indicates position of a Web Form "Place" button, a leftover of earliest design api to cater Web.
 * Value of 26 is arbitrarily chosen between 1 to 99.
 */
internal fun getAdvancedOrderPlaceTradeParams(formTarget: String, tradeX: String): Map<String, String> {
    return mapOf(
        PLATFORM_KEY to PLATFORM_VALUE,
        FORM_TARGET_KEY to formTarget,
        TRADE_X_KEY to tradeX,
        OPT_LEVEL to "4"
    )
}

internal fun MutableMap<String, Any?>.addSymbol(
    inputId: WithSymbolTradeFormInputId,
    securityTypeKey: String,
    orderCount: Int? = null,
    update: ParametersModifierWithInputId = { _, _, _ -> }
): Int {
    inputId.extractSymbolInfo(this)?.let { symbolInfo ->
        val actualSymbol = symbolInfo.symbol
        this[inputId.symbol] = actualSymbol
        this[securityTypeKey] = this.extractSecurityType(symbolInfo, actualSymbol, orderCount?.toString().orEmpty())
    }
    update(this, orderCount ?: 0, inputId)
    return 1
}

internal fun MutableMap<String, Any?>.extractSecurityType(
    symbolInfo: WithSymbolInfo,
    actualSymbol: String,
    legCount: String = ""
): String {
    return if (symbolInfo.instrumentType.isOption) {
        this["$OPTION_EXPIRATION_KEY$legCount"] = "1"
        this["$OPTION_STRIKE_PRICE_KEY$legCount"] = getStrikePrice(actualSymbol)?.toPlainString()
        this["$OPTION_TYPE_KEY$legCount"] = getOptionType(actualSymbol)?.requestCode
        VALUE_SECURITY_TYPE_OPTION
    } else {
        VALUE_SECURITY_TYPE_STOCK
    }
}

internal fun MutableMap<String, Any?>.removeRedundantAdvancedOrderParams(inputId: WithAdvancedTradeFormInputId) {
    remove(inputId.advancedOrder)
    remove(inputId.advancedOrderType)
}

internal fun MutableMap<String, Any?>.copyTrailingStopParamsFromResponse(
    orderResponse: TrailingStopOrderResponse,
    index: Int,
    inputId: WithPriceTradeFormInputId,
    suffix: String = ""
) {
    val orderDetails: TrailingStopOrderDetails = orderResponse.trailingStopOrderDetails[index]
    if (isTrailingStop(this[inputId.priceType])) {
        this["offsettype$suffix"] = orderDetails.offsetType
        this["offsetvalue$suffix"] = orderDetails.offsetValue
        this["$INITIAL_STOP_PRICE_KEY$suffix"] = orderDetails.initialStopPrice
        this["aofollowprice$suffix"] = orderDetails.followPrice
        this["$ADVANCED_ORDER_TYPE_KEY$suffix"] = orderDetails.orderType
    }
}

internal fun <T> T.copyTrailingStopPriceToOffsetValue(snapshot: MutableMap<String, Any?>) where T : WithPriceTradeFormInputId, T : WithTrailingStopInputId {
    when (snapshot[priceType]) {
        priceTypeValueId.trailingStopPercent, priceTypeValueId.trailingStopDollar -> snapshot.replaceKey(stopPrice, offsetValue)
        else -> { /* Do Nothing */
        }
    }
}

private fun isTrailingStop(inputId: Any?): Boolean {
    return inputId == CommonAdvancedPriceTypeValueId.trailingStopDollar || inputId == CommonAdvancedPriceTypeValueId.trailingStopPercent
}

internal fun <K, V> MutableMap<K, V>.replaceKey(oldKey: K, newKey: K) {
    remove(oldKey)?.let { put(newKey, it) }
}
