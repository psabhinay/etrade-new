package com.etrade.mobilepro.themeselection

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.getStringOrThrow
import androidx.core.content.withStyledAttributes
import com.etrade.mobilepro.themeselection.databinding.RowSelectionBinding

class TextViewSelection : ConstraintLayout {

    private lateinit var binding: RowSelectionBinding

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    /**
     * Property to get/set selection.
     */
    var isModeSelected: Boolean = false
        set(value) {
            binding.themeSelected.visibility = if (value) {
                View.VISIBLE
            } else {
                View.GONE
            }
            field = value
            isSelected = value
        }

    private fun init(attrs: AttributeSet?) {
        binding = RowSelectionBinding.inflate(LayoutInflater.from(context), this, true)

        context.withStyledAttributes(set = attrs, attrs = R.styleable.TextViewSelection, defStyleRes = R.style.TextViewSelection) {
            val modeTitle = getStringOrThrow(R.styleable.TextViewSelection_modeTitle)
            binding.themeLabel.text = modeTitle
        }
    }
}
