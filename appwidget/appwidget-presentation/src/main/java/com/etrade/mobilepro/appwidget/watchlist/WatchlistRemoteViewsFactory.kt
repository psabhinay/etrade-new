package com.etrade.mobilepro.appwidget.watchlist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RemoteViews
import com.etrade.eo.core.util.MarketDataFormatter.formatMarketData
import com.etrade.mobilepro.appwidget.R
import com.etrade.mobilepro.appwidget.api.WatchlistEntryTuple
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.common.BaseRemoteViewsFactory
import com.etrade.mobilepro.appwidget.common.formatIndexChange
import com.etrade.mobilepro.appwidget.widgetcontainer.EXTRA_INSTRUMENT_TYPE_CODE
import com.etrade.mobilepro.appwidget.widgetcontainer.EXTRA_SYMBOL
import com.etrade.mobilepro.util.android.textutil.prepareWidgetTicker
import com.etrade.mobilepro.util.color.DataColor
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import java.math.BigDecimal

internal class WatchlistRemoteViewsFactory(
    private val context: Context,
    private val dataColorResolver: DataColorResolver,
    private val layout: WidgetSettings.Layout
) : BaseRemoteViewsFactory<WatchlistEntryTuple> {

    @Suppress("LongMethod")
    override fun create(item: WatchlistEntryTuple): RemoteViews {

        val displayTiles = when (layout) {
            WidgetSettings.Layout.TILES -> true
            WidgetSettings.Layout.LIST -> false
        }

        return RemoteViews(context.packageName, getLayoutResId(displayTiles)).apply {
            val (watchlistEntry, data) = item

            val lastPrice: BigDecimal? = data?.lastPrice?.toBigDecimal() ?: watchlistEntry.lastPrice
            val change: BigDecimal? = data?.change?.toBigDecimal() ?: watchlistEntry.change
            val changePercent: BigDecimal? = data?.changePercent?.toBigDecimal() ?: watchlistEntry.changePercent

            // symbol + options data
            prepareWidgetTicker(watchlistEntry.ticker, watchlistEntry.typeCode, watchlistEntry.description)
                .let { (symbol, optionData) ->
                    setTextViewText(R.id.tvDisplayName, symbol)
                    optionData?.let {
                        setTextViewText(R.id.tvOptionData, it)
                        setViewVisibility(R.id.tvOptionData, View.VISIBLE)
                    } ?: setViewVisibility(R.id.tvOptionData, View.INVISIBLE)
                }

            // last price
            setTextViewText(R.id.tvPrice, formatMarketData(lastPrice))

            // change + change percent
            val indexChange = formatIndexChange(context, change, changePercent, displayTiles)
            setTextViewText(R.id.tvChanges, indexChange)

            // background/text color
            val isPositiveChange = isPositive(change)
            if (displayTiles) {
                setupBackground(isPositiveChange)
            } else {
                setUpChangesTextColor(isPositiveChange)
            }

            // click listener
            setupClickListener(watchlistEntry)
        }
    }

    private fun RemoteViews.setupBackground(isPositiveChange: Boolean) {

        val background = if (isPositiveChange) {
            R.drawable.appwidget_bg_watchlist_entry_profit
        } else {
            R.drawable.appwidget_bg_watchlist_entry_loss
        }
        setInt(R.id.watchlistEntryContainer, "setBackgroundResource", background)

        // Padding must be set again after a background change
        setViewPadding(
            R.id.watchlistEntryContainer,
            R.dimen.appwidget_watchlist_entry_padding_start.inPixels(),
            R.dimen.appwidget_watchlist_entry_padding_top.inPixels(),
            R.dimen.appwidget_watchlist_entry_padding_end.inPixels(),
            R.dimen.appwidget_watchlist_entry_padding_bottom.inPixels(),
        )
    }

    private fun RemoteViews.setUpChangesTextColor(isPositiveChange: Boolean) {
        val color = context.getColor(
            if (isPositiveChange) {
                R.color.appwidget_index_change_profit
            } else {
                R.color.appwidget_index_change_loss
            }
        )
        setTextColor(R.id.tvChanges, color)
    }

    private fun RemoteViews.setupClickListener(watchlistEntry: WatchlistEntry) {
        val bundle = Bundle().apply {
            putString(EXTRA_SYMBOL, watchlistEntry.ticker)
            putString(EXTRA_INSTRUMENT_TYPE_CODE, watchlistEntry.typeCode?.typeCode)
        }
        val intent = Intent().apply { putExtras(bundle) }
        setOnClickFillInIntent(R.id.watchlistEntryContainer, intent)
    }

    private fun isPositive(change: BigDecimal?) =
        when (dataColorResolver.resolveFilledDataColor(change)) {
            DataColor.NEUTRAL,
            DataColor.POSITIVE,
            DataColor.FILLED_POSITIVE -> true
            DataColor.NEGATIVE,
            DataColor.FILLED_NEGATIVE -> false
        }

    private fun getLayoutResId(displayTiles: Boolean) =
        if (displayTiles) {
            R.layout.appwidget_grid_item_watchlist_entry
        } else {
            R.layout.appwidget_list_item_watchlist_entry
        }

    private fun Int.inPixels() = context.resources.getDimensionPixelOffset(this)
}
