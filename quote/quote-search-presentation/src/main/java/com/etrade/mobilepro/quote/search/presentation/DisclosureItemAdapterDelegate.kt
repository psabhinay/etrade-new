package com.etrade.mobilepro.quote.search.presentation

import android.text.method.LinkMovementMethod
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.search.presentation.databinding.QuoteItemSearchResultDisclosureBinding
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchResultItem.DisclosureText
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.etrade.mobilepro.util.android.textutil.TextUtil
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class DisclosureItemAdapterDelegate :
    AbsListItemAdapterDelegate<DisclosureText, SearchResultItem, DisclosureItemAdapterDelegate.DisclosureTextViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): DisclosureTextViewHolder {
        return DisclosureTextViewHolder(parent.viewBinding(QuoteItemSearchResultDisclosureBinding::inflate))
    }

    override fun isForViewType(item: SearchResultItem, items: MutableList<SearchResultItem>, position: Int): Boolean {
        return item is DisclosureText
    }

    override fun onBindViewHolder(item: DisclosureText, holder: DisclosureTextViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }

    class DisclosureTextViewHolder(private val binding: QuoteItemSearchResultDisclosureBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: DisclosureText) {
            binding.disclosureView.apply {
                // disable view clickable and longClickable event to suppress accessibility "Double tap to activate"
                setAccessibilityDelegate(object : View.AccessibilityDelegate() {
                    override fun onPopulateAccessibilityEvent(
                        host: View?,
                        event: AccessibilityEvent?
                    ) {
                        host?.isClickable = false
                        host?.isLongClickable = false

                        super.onPopulateAccessibilityEvent(host, event)
                    }
                })

                val disclosure = resources.getString(item.textResource)
                text = TextUtil.convertTextWithHmlTagsToSpannable(disclosure)
                movementMethod = LinkMovementMethod.getInstance()
            }
        }
    }
}
