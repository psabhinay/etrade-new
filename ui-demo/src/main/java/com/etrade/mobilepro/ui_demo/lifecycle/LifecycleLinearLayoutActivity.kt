package com.etrade.mobilepro.ui_demo.lifecycle

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.ui_demo.databinding.ActivityLifecycleLinearLayoutBinding
import com.etrade.mobilepro.util.android.binding.dataBinding

class LifecycleLinearLayoutActivity: AppCompatActivity() {

    private val viewModel: LifecycleActivityViewModel by viewModels()

    private val binding by dataBinding(ActivityLifecycleLinearLayoutBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.viewModel = viewModel
    }
}
