package com.etrade.mobilepro.appwidget.watchlist.selection

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.etrade.mobilepro.watchlistapi.Watchlist

class WatchlistSelectionAdapter(
    private val clickListener: (Watchlist) -> Unit
) : RecyclerView.Adapter<WatchlistViewHolder>() {
    var items: List<WatchlistRow> = emptyList()
        set(value) {
            val oldValue = field
            field = value
            dispatchUpdates(oldValue, value)
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WatchlistViewHolder {
        return WatchlistViewHolder(parent, clickListener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: WatchlistViewHolder, position: Int) = holder.bind(items[position])
}
