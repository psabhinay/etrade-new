package com.etrade.mobilepro.markets.viewmodel

import android.content.res.Resources
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.markets.common.MarketsBaseViewModel
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.data.response.MarketFutureResponse
import com.etrade.mobilepro.markets.futures.MarketsFuturesViewModel
import com.etrade.mobilepro.testutil.XmlDeserializer
import com.etrade.mobilepro.util.color.DataColor
import com.etrade.mobilepro.util.color.DataColorResolver
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Test

@ExperimentalCoroutinesApi
internal class MarketsFutureViewModelTest : MarketsBaseViewModelTest() {

    @Test
    fun `fetches market indices upon creation`() {
        val mockDynamicService = mockMoverService()

        setupMarketsFuturesViewModel(mockDynamicService, mockResources, mockDataColorResolver)

        verify(mockDynamicService).getMarketFutures()
    }

    @Test
    fun `returns error view state when indices call is failed`() {
        val mockMoverService = mock<MoverService> {
            on { getMarketFutures() } doReturn Single.error(RuntimeException("Something went wrong"))
        }

        runBlockingTest {
            val vm = setupMarketsFuturesViewModel(mockMoverService, mockResources, mockDataColorResolver)
            vm.viewState.observeForever(mockObserver)
            verify(mockObserver).onChanged(eq(MarketsBaseViewModel.ViewState.Error()))
        }
    }

    @Test
    fun `returns view state with market future on success`() {
        val marketIndexResponse = XmlDeserializer.getObjectFromXml("markets_future.xml", MarketFutureResponse::class.java)
        val mockMoverService = mockMoverService(marketIndexResponse)

        runBlockingTest {
            val vm = setupMarketsFuturesViewModel(mockMoverService, mockResources, mockDataColorResolver)
            vm.viewState.observeForever(mockObserver)
            val success = vm.viewState.value as MarketsBaseViewModel.ViewState.Success
            val cellItems = success.cellItems
            val rowHeaders = success.rowHeaders

            Assert.assertEquals(3, cellItems.size)
            Assert.assertEquals(3, cellItems[2].size)
            Assert.assertEquals(3, rowHeaders.size)
            Assert.assertEquals(DataColor.FILLED_POSITIVE, cellItems[0][0].dataColor)
            Assert.assertEquals(DataColor.NEGATIVE, cellItems[0][1].dataColor)
            Assert.assertEquals(DataColor.NEGATIVE, cellItems[0][2].dataColor)
            Assert.assertEquals("24,048.00", cellItems[0][0].displayText)
            Assert.assertEquals("-52.00", cellItems[0][1].displayText)
            Assert.assertEquals("-0.22%", cellItems[0][2].displayText)
            Assert.assertEquals("6,689.00", cellItems[1][0].displayText)
            Assert.assertEquals("-53.75", cellItems[1][1].displayText)
            Assert.assertEquals("-0.80%", cellItems[1][2].displayText)
            Assert.assertEquals("2,671.50", cellItems[2][0].displayText)
            Assert.assertEquals("-0.10", cellItems[2][1].displayText)
            Assert.assertEquals("0.00%", cellItems[2][2].displayText)
            Assert.assertEquals(InstrumentType.EQ, rowHeaders[0].instrumentType)
            Assert.assertEquals("DJIA Index", rowHeaders[0].displayText)
            Assert.assertEquals("DJIA Index", rowHeaders[0].filterableKeyword)
            Assert.assertEquals("YM", rowHeaders[0].symbol)
            Assert.assertEquals("YM", rowHeaders[0].id)
            Assert.assertEquals(InstrumentType.EQ, rowHeaders[1].instrumentType)
            Assert.assertEquals("NASDAQ 100", rowHeaders[1].displayText)
            Assert.assertEquals("NASDAQ 100", rowHeaders[1].filterableKeyword)
            Assert.assertEquals("NQ", rowHeaders[1].symbol)
            Assert.assertEquals("NQ", rowHeaders[1].id)
            Assert.assertEquals(InstrumentType.EQ, rowHeaders[2].instrumentType)
            Assert.assertEquals("S&P 500", rowHeaders[2].displayText)
            Assert.assertEquals("S&P 500", rowHeaders[2].filterableKeyword)
            Assert.assertEquals("ES", rowHeaders[2].symbol)
            Assert.assertEquals("ES", rowHeaders[2].id)

            Assert.assertEquals("MAR 15 2019", success.timeStamp)
        }
    }

    @Test
    fun `returns view state with empty market indices on success`() {
        val mockMoverService = mockMoverService(MarketFutureResponse(ArrayList()))

        runBlockingTest {
            val vm = setupMarketsFuturesViewModel(mockMoverService, mockResources, mockDataColorResolver)
            vm.viewState.observeForever(mockObserver)

            Assert.assertEquals(MarketsBaseViewModel.ViewState.Empty, vm.viewState.value)
        }
    }

    private fun mockMoverService(marketFutureResponse: MarketFutureResponse = MarketFutureResponse()) = mock<MoverService> {
        on { getMarketFutures() } doReturn Single.just(marketFutureResponse)
    }

    private fun setupMarketsFuturesViewModel(
        mockMoverService: MoverService,
        mockResources: Resources,
        mockDataColorResolver: DataColorResolver
    ) = MarketsFuturesViewModel(
        mockMoverService,
        mockResources,
        mockDataColorResolver
    )
}
