package com.etrade.mobilepro.tableview.controlbar

import androidx.databinding.BindingAdapter
import com.google.android.material.button.MaterialButton

@BindingAdapter("dropdownText")
fun MaterialButton.bindDropdownText(selectedText: String?) {
    selectedText?.let {
        text = it
        contentDescription = resources.getString(R.string.label_dropdown_description, it)
    }
}
