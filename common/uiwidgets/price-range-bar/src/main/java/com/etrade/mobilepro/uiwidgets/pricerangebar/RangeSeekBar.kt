package com.etrade.mobilepro.uiwidgets.pricerangebar

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.Style
import android.graphics.RectF
import android.graphics.Typeface
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.ColorUtils
import kotlin.math.max

/**
 * Widget that lets users select a minimum and maximum value on a given numerical range. The range
 * value types can be one of Long, Double, Integer, Float, Short, Byte or BigDecimal.<br></br>
 * <br></br>
 * Improved [MotionEvent] handling for smoother use, anti-aliased painting for improved
 * aesthetics.
 *
 * @param <T> The Number type of the range values. One of Long, Double, Integer, Float, Short, Byte
 * or BigDecimal.
 */

class RangeSeekBar<T : Number> : AppCompatImageView {
    private val textSize16 = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 16f, resources.displayMetrics)
    private val textSize10 = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10f, resources.displayMetrics)
    private val ET_HEIGHT = ApplyDimension(15)
    private val ET_WIDTH = ApplyDimension(3)
    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val customViewHeight = ApplyDimension(120)
    private val lineHeight = ApplyDimension(5)
    private val padding = ApplyDimension(0)
    private val DAY_LOW_LABEL = context.getString(R.string.day_low)
    private val DAY_HIGH_LABEL = context.getString(R.string.day_high)
    private val typefaceMedium: Typeface = ResourcesCompat.getFont(context, R.font.roboto_medium)!!

    private var absoluteMinValuePrim: Double = 0.toDouble()
    private var absoluteMaxValuePrim: Double = 0.toDouble()
    private var normalizedMinValue = 0.0
    private var normalizedMaxValue = 1.0
    private var minText: String = ""
    private var maxText: String = ""
    private var typeface: Typeface
    private var rect: RectF
    private var dayLowRect: RectF
    private var dayHighRect: RectF
    private val primaryTextColor = ContextCompat.getColor(context, R.color.textColorPrimary)
    private val primaryColor = ContextCompat.getColor(context, R.color.primaryColor)
    private val lightGrayColor = ContextCompat.getColor(context, R.color.light_grey)
    private val rangeBarColor = ColorUtils.setAlphaComponent(primaryColor, 102)
    private val textPaint = Paint()

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defStyle: Int) : super(context, attributeSet, defStyle) {
        isFocusable = true
        isFocusableInTouchMode = true
        typeface = ResourcesCompat.getFont(context, R.font.roboto)!!
        rect = RectF(padding, 0.5f * (height - lineHeight), width - padding, 0.5f * (height + lineHeight))
        dayLowRect = RectF(rect.left, rect.top, rect.left + ET_WIDTH, rect.bottom + ET_HEIGHT)
        dayHighRect = dayLowRect
    }

    private fun ApplyDimension(dip: Int): Float {
        val displayMetrics = context.resources.displayMetrics
        val px = Math.round(dip * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT))
        return px.toFloat()
    }

    fun setRange(absoluteMinValue: Double, absoluteMaxValue: Double) {
        absoluteMinValuePrim = absoluteMinValue
        absoluteMaxValuePrim = absoluteMaxValue
    }

    /**
     * Sets the currently selected minimum value. The widget will be invalidated and redrawn.
     *
     * @param value The Number value to set the minimum value to. Will be clamped to given absolute
     * minimum/maximum range.
     */
    fun setSelectedMinValue(value: T) {
        // in case absoluteMinValue == absoluteMaxValue, avoid division by zero when normalizing.
        if (0.0 == absoluteMaxValuePrim - absoluteMinValuePrim) {
            setNormalizedMinValue(0.0)
        } else {
            setNormalizedMinValue(valueToNormalized(value))
        }
    }

    /**
     * Sets the currently selected maximum value. The widget will be invalidated and redrawn.
     *
     * @param value The Number value to set the maximum value to. Will be clamped to given absolute
     * minimum/maximum range.
     */
    fun setSelectedMaxValue(value: T) {
        // in case absoluteMinValue == absoluteMaxValue, avoid division by zero when normalizing.
        if (0.0 == absoluteMaxValuePrim - absoluteMinValuePrim) {
            setNormalizedMaxValue(1.0)
        } else {
            setNormalizedMaxValue(valueToNormalized(value))
        }
    }

    /**
     * Ensures correct size of the widget.
     */
    @Synchronized
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = View.MeasureSpec.getSize(widthMeasureSpec)
        val height = customViewHeight.toInt()
        setMeasuredDimension(width, height)
    }

    /**
     * Draws the widget on the given canvas.
     */
    @Synchronized
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        // draw seek bar background line
        with(rect) {
            left = padding
            top = 0.5f * (height - lineHeight)
            right = width - padding
            bottom = 0.5f * (height + lineHeight)
        }

        with(paint) {
            style = Style.FILL
            color = lightGrayColor
            isAntiAlias = true
        }

        canvas.drawRect(rect, paint)

        // draw seek bar active range line
        rect.left = normalizedToScreen(normalizedMinValue)
        rect.right = normalizedToScreen(normalizedMaxValue)

        if (rect.left > rect.right)
            rect.right = rect.left

        paint.color = rangeBarColor
        canvas.drawRect(rect, paint)

        // draw seek bar active range line
        with(dayLowRect) {
            left = rect.left
            top = rect.top
            right = rect.left + ET_WIDTH
            bottom = rect.bottom + ET_HEIGHT
        }

        paint.color = primaryColor
        canvas.drawRect(dayLowRect, paint)

        val dayLow_X = rect.left + ET_WIDTH
        var dayHigh_X = rect.right - ET_WIDTH
        var dayHigh_Y = rect.right
        if (dayLow_X > dayHigh_X) {
            dayHigh_X = rect.left + ET_WIDTH
            dayHigh_Y = dayHigh_X + ET_WIDTH
        }

        with(dayHighRect) {
            left = dayHigh_X
            top = rect.top - ET_HEIGHT
            right = dayHigh_Y
            bottom = rect.bottom
        }

        canvas.drawRect(dayHighRect, paint)

        // display text
        with(textPaint) {
            color = primaryTextColor
            textSize = textSize10
            typeface = typeface
            textAlign = Paint.Align.CENTER
        }

        var xCord = normalizedTextToScreen(normalizedMaxValue)
        var ycord = 0.5f * height - 2.8f * ET_HEIGHT

        var txtWidth = textPaint.maxTextWidth(DAY_HIGH_LABEL, maxText)

        if (xCord + txtWidth * 0.5f > width - padding) {
            xCord = width - padding
            textPaint.textAlign = Paint.Align.RIGHT
        } else if (xCord - txtWidth * 0.5f < padding) {
            xCord = padding
            textPaint.textAlign = Paint.Align.LEFT
        }

        canvas.drawText(DAY_HIGH_LABEL, xCord, ycord, textPaint)

        textPaint.textSize = textSize16
        textPaint.typeface = typefaceMedium
        canvas.drawText(maxText, xCord, 0.5f * height - ET_HEIGHT * 1.5f, textPaint)

        xCord = normalizedTextToScreen(normalizedMinValue)
        ycord = 0.5f * height + ET_HEIGHT * 2.2f

        txtWidth = textPaint.maxTextWidth(DAY_LOW_LABEL, minText)

        if (xCord + txtWidth * 0.5f > width - padding) {
            xCord = width - padding
            textPaint.textAlign = Paint.Align.RIGHT
        } else if (xCord - txtWidth * 0.5f < padding) {
            xCord = padding
            textPaint.textAlign = Paint.Align.LEFT
        }
        canvas.drawText(minText, xCord, ycord, textPaint)

        textPaint.textSize = textSize10
        textPaint.typeface = typeface
        canvas.drawText(DAY_LOW_LABEL, xCord, 0.5f * height + ET_HEIGHT * 3.0f, textPaint)
    }

    private fun Paint.maxTextWidth(labelText: String, valueText: String): Float {
        val valueTextWidth = measureTextWidth(valueText, textSize16)
        val labelTextWidth = measureTextWidth(labelText, textSize10)
        return max(valueTextWidth, labelTextWidth)
    }

    private fun Paint.measureTextWidth(text: String, textSize: Float): Float {
        val currentTextSize = this.textSize
        this.textSize = textSize
        val textWidth = this.measureText(text)
        this.textSize = currentTextSize
        return textWidth
    }

    /**
     * Overridden to save instance state when device orientation changes. This method is called
     * automatically if you assign an id to the RangeSeekBar widget using the [.setId]
     * method. Other members of this class than the normalized min and max values don't need to be
     * saved.
     */
    override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putParcelable("SUPER", super.onSaveInstanceState())
        bundle.putDouble("MIN", normalizedMinValue)
        bundle.putDouble("MAX", normalizedMaxValue)
        return bundle
    }

    /**
     * Overridden to restore instance state when device orientation changes. This method is called
     * automatically if you assign an id to the RangeSeekBar widget using the [.setId]
     * method.
     */
    override fun onRestoreInstanceState(parcel: Parcelable) {
        val bundle = parcel as Bundle
        super.onRestoreInstanceState(bundle.getParcelable("SUPER"))
        normalizedMinValue = bundle.getDouble("MIN")
        normalizedMaxValue = bundle.getDouble("MAX")
    }

    /**
     * Sets normalized min value to value so that 0 <= value <= normalized max value <= 1. The View
     * will get invalidated when calling this method.
     *
     * @param value The new normalized min value to set.
     */
    fun setNormalizedMinValue(value: Double) {
        normalizedMinValue = Math.max(0.0, Math.min(1.0, Math.min(value, normalizedMaxValue)))
        invalidate()
    }

    /**
     * Sets normalized max value to value so that 0 <= normalized min value <= value <= 1. The View
     * will get invalidated when calling this method.
     *
     * @param value The new normalized max value to set.
     */
    fun setNormalizedMaxValue(value: Double) {
        normalizedMaxValue = Math.max(0.0, Math.min(1.0, Math.max(value, normalizedMinValue)))
        invalidate()
    }

    /**
     * Converts the given Number value to a normalized double.
     *
     * @param value The Number value to normalize.
     * @return The normalized double.
     */
    private fun valueToNormalized(value: T): Double {
        return if (0.0 == absoluteMaxValuePrim - absoluteMinValuePrim) {
            // prevent division by zero, simply return 0.
            0.0
        } else (value.toDouble() - absoluteMinValuePrim) / (absoluteMaxValuePrim - absoluteMinValuePrim)
    }

    /**
     * Converts a normalized value into screen space.
     *
     * @param normalizedCoord The normalized value to convert.
     * @return The converted value in screen space.
     */
    private fun normalizedToScreen(normalizedCoord: Double): Float {

        return (normalizedCoord * width).toFloat()
    }

    private fun normalizedTextToScreen(normalizedCoord: Double): Float {
        return (normalizedCoord * width).toFloat()
    }

    fun setMinText(minText: String) {
        this.minText = minText
    }

    fun setMaxText(maxText: String) {
        this.maxText = maxText
    }
}
