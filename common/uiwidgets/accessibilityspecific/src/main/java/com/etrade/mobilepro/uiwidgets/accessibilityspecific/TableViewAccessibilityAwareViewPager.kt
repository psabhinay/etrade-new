package com.etrade.mobilepro.uiwidgets.accessibilityspecific

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager
import com.etrade.mobilepro.util.android.accessibility.accessibilityEnabled

class TableViewAccessibilityAwareViewPager : ViewPager {
    constructor(context: Context) : super(context) {
        touchPagingEnabled = !context.accessibilityEnabled
    }
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        touchPagingEnabled = !context.accessibilityEnabled
    }

    private var touchPagingEnabled: Boolean

    @Suppress("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return !touchPagingEnabled || super.onTouchEvent(event)
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return touchPagingEnabled && super.onInterceptTouchEvent(event)
    }
}
