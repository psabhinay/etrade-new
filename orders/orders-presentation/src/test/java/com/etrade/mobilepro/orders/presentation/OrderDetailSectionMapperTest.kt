package com.etrade.mobilepro.orders.presentation

import android.content.res.Resources
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderDetailDescription
import com.etrade.mobilepro.order.details.api.OrderHistoryItem
import com.etrade.mobilepro.orders.api.ExecInstruction
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.orders.api.MarketSession
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.orders.presentation.section.OrderDetailSectionMapper
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.SummarySectionItem
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.math.BigDecimal

class OrderDetailSectionMapperTest {

    @Mock
    private lateinit var resources: Resources

    private lateinit var orderDetailLabelResources: OrderDetailLabelResources

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        orderDetailLabelResources = generateMockOrderDetailLabelResources(resources)
        Mockito.`when`(resources.getString(anyInt())).thenReturn(" ")
    }

    @Test
    fun `test single leg stock order`() {
        val orderDetailSectionMapper = OrderDetailSectionMapper(orderDetailLabelResources)
        val mapResult = orderDetailSectionMapper.map(getStockOrderDetail(), false)
        val quantityItem = mapResult.filter {
            (it as SummarySectionItem).label == "Quantity"
        }
        val orderTypeItem = mapResult.filter {
            (it as SummarySectionItem).label == "Order Type"
        }
        assertTrue(quantityItem.isNotEmpty())
        assertTrue(orderTypeItem.isNotEmpty())
    }

    @Test
    fun `test stock order with multilegs`() {
        val orderDetailSectionMapper = OrderDetailSectionMapper(orderDetailLabelResources)
        val mapResult = orderDetailSectionMapper.map(getStockOrderDetail().copy(hasMultipleLegs = true), false)
        val quantityItem = mapResult.filter {
            (it as SummarySectionItem).label == "Quantity"
        }
        val orderTypeItem = mapResult.filter {
            (it as SummarySectionItem).label == "Order Type"
        }
        assertTrue(quantityItem.isEmpty())
        assertTrue(orderTypeItem.isEmpty())
    }

    @Test
    fun `test stock order with partial execution`() {
        val orderDetailSectionMapper = OrderDetailSectionMapper(orderDetailLabelResources)
        val mapResult = orderDetailSectionMapper.map(getStockOrderDetail().copy(hasPartialExecution = true), false)
        val quantityItem = mapResult.filter {
            (it as SummarySectionItem).label == "Quantity"
        }
        val orderTypeItem = mapResult.filter {
            (it as SummarySectionItem).label == "Order Type"
        }
        assertTrue(quantityItem.isEmpty())
        assertTrue(orderTypeItem.isNotEmpty())
    }

    @Test
    fun `test stock plan order`() {
        val orderDetailSectionMapper = OrderDetailSectionMapper(orderDetailLabelResources)
        val mapResult = orderDetailSectionMapper.map(getStockPlanOrder(), false)
        val estSharesWithheldItem = mapResult.first {
            (it as SummarySectionItem).label == "Est. Shares WithHeld/Value"
        } as SummarySectionItem
        val placedDateItem = mapResult.first {
            (it as SummarySectionItem).label == "Order Placed"
        } as SummarySectionItem
        val totalPriceItem = mapResult.first {
            (it as SummarySectionItem).label == "Total Price"
        } as SummarySectionItem
        assertEquals(estSharesWithheldItem.value, "-4/\$0.00")
        assertEquals(placedDateItem.value, "01/02/2019 05:43:49")
        assertEquals(totalPriceItem.value, "\$0.00")
    }

    private fun getStockOrderDetail(): BaseOrderDetailsItem.OrderDetailsItem = BaseOrderDetailsItem.OrderDetailsItem(
        OrderStatus.OPEN, OrderType.EQUITY, PriceType.MARKET, "200", "12345",
        "09/01/2019", "09/01/2019", OrderTerm.GOOD_UNTIL_CANCEL, null, null, null, null, null,
        null, "12345", listOf(getOrderDetailDescription()), null, null, TransactionType.BUY_OPEN, BigDecimal.ZERO, BigDecimal.ZERO, null,
        null, null, null, false, null, false, false, null, MarketSession.MARKET_REGULAR,
        MarketDestination.AUTO, OrigSysCode.ORIG_SYS_CODE_ANDR, ExecInstruction(false, false, false, false), null, null,
        false, 0, false, "1", tradeLegs = emptyList()
    )

    private fun getStockPlanOrder(): BaseOrderDetailsItem.StockPlanOrderDetailsItem = BaseOrderDetailsItem.StockPlanOrderDetailsItem(
        orderHistoryList = listOf(
            OrderHistoryItem(
                price = "\$190.00",
                transaction = "Order Placed",
                sellQuantity = "4",
                date = "01/02/2019 05:43:49"
            )
        ),
        orderNumber = "4221810",
        orderType = "Net Exercise and Hold",
        orderStatus = OrderStatus.OPEN,
        description = "APPLE CORPORATION(AAPL)",
        term = "GTC",
        gtdExpiration = "",
        sharesExercised = "0",
        sharesSold = "4",
        sharesToExercise = "0",
        sharesToSell = "4",
        estSharesToSell = "4",
        priceType = "Limit",
        limitPrice = "$190.00",
        grossProceeds = "\$760.00(USD)",
        estGrossProceeds = "\$760.00(USD)",
        totalPrice = "\$0.00",
        commission = "\$10.95",
        secFee = "\$0.00",
        brokerAssistFee = "\$0.00",
        disbursementFee = "\$0.00",
        taxesWithheld = "\$0.00",
        estNetProceeds = "(\$10.95)(USD)",
        netProceeds = "(\$10.95)(USD)",
        exerciseValue = "\$0.00",
        sharesToKeep = "-4",
        estSharesToKeep = "-4",
        sharesIssued = "-4",
        sharesToIssue = "-4",
        appreciationSharesValue = "0/\$0.00",
        estAppreciationSharesValue = "0/\$0.00",
        taxSharesValue = "0/\$0.00",
        estTaxSharesValue = "0/\$0.00",
        planPrice = "\$400.00",
        grantType = "Nonqual",
        planNumber = "OP07302015",
        planDate = "05/21/2012",
        vestPeriod = "0",
        costBasis = "105.005",
        planType = "D",
        symbol = "AAPL"
    )

    private fun getOrderDetailDescription(): OrderDetailDescription = OrderDetailDescription("200", "0", "200", TransactionType.BUY_OPEN, "QAA", "QAA")
}
