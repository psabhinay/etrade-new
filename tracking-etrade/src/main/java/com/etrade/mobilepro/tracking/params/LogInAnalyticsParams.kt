package com.etrade.mobilepro.tracking.params

import com.etrade.mobilepro.tracking.EVENT_ATTRIBUTE_CUSTOMER_ID
import com.etrade.mobilepro.tracking.UNSPECIFIED
import com.etrade.mobilepro.tracking.trackerValue

private const val ACCOUNT_FUNDED = "Account Funded"
private const val ACCOUNT_TYPES = "Account Types"
private const val IS_STREAMING_ENABLED = "Is Streaming Enabled"
private const val INTERNATIONAL = "International"
private const val SERVICE_TIER = "Service Tier"
private const val VALUE_NOT_FOUND = "Not Found"
private const val ROOTED_DEVICE = "Rooted device"

data class TrackLogInParams(
    private val international: Boolean,
    private val accountFunded: Boolean,
    private val accountTypes: String?,
    private val isStreamingEnabled: Boolean,
    private val serviceTier: String?,
    private val isRootedDevice: Boolean,
    private val customerId: String
) : AnalyticsParams {

    override val name: String = ""

    override val params: Map<String, String>
        get() = mapOf(
            ACCOUNT_FUNDED to accountFunded.toString(),
            ACCOUNT_TYPES to (accountTypes ?: UNSPECIFIED),
            IS_STREAMING_ENABLED to isStreamingEnabled.toString(),
            INTERNATIONAL to international.toString(),
            SERVICE_TIER to getTierValue(serviceTier),
            ROOTED_DEVICE to isRootedDevice.trackerValue,
            EVENT_ATTRIBUTE_CUSTOMER_ID to customerId
        )

    // According to discussion with PO, treat "Not Found" as "unspecified"
    private fun getTierValue(tier: String?): String {
        return tier?.takeUnless { it.equals(VALUE_NOT_FOUND, true) } ?: UNSPECIFIED
    }
}
