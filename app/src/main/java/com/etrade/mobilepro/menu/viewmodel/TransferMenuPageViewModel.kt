package com.etrade.mobilepro.menu.viewmodel

import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.ActionOnlyNavDirections
import com.etrade.mobile.accounts.dto.InstitutionType
import com.etrade.mobilepro.R
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.menu.AccountDisplayPolicy
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.NewMenuItemValidator
import com.etrade.mobilepro.menu.NewMenuItems
import com.etrade.mobilepro.menu.builder.menuPage
import com.etrade.mobilepro.tracking.SCREEN_TITLE_ACCOUNT_TRANSFER
import com.etrade.mobilepro.tracking.SCREEN_TITLE_DIRECT_DEPOSIT_SETUP
import com.etrade.mobilepro.tracking.SCREEN_TITLE_RETIREMENT_PLAN_ROLLOVER
import com.etrade.mobilepro.tracking.SCREEN_TITLE_TRANSFER_MONEY
import com.etrade.mobilepro.tracking.SCREEN_TITLE_WIRE_TRANSFERS_DRAWER
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import javax.inject.Inject

class TransferMenuPageViewModel @Inject constructor(
    resources: Resources,
    tracker: Tracker,
    @Web val baseUrl: String,
    private val daoProvider: AccountDaoProvider,
    private val newMenuItemValidator: NewMenuItemValidator
) : AppBaseMenuPageViewModel(resources, tracker) {

    override val menuPageItems: LiveData<List<MenuItem>>
        get() = MutableLiveData(getMenuPageItems())

    @SuppressWarnings("LongMethod")
    private fun getMenuPageItems(): List<MenuItem> {
        return menuPage(newMenuItemValidator) {

            textWithArrowAndSubtitle {
                navDirections(ActionOnlyNavDirections(R.id.transfer_money_nav_graph))
                text(R.string.menu_transfer_money_title)
                subtext(R.string.menu_transfer_money_subtitle)
                trackScreen(SCREEN_TITLE_TRANSFER_MONEY)
            }

            textWithArrowAndSubtitle(R.id.transferMoneyWireTransferMenuItem) {
                text(R.string.menu_wire_transfer_title)
                subtext(R.string.menu_wire_transfer_subtitle)
                clickable = true
                trackScreen(SCREEN_TITLE_WIRE_TRANSFERS_DRAWER)
            }

            textWithArrowAndSubtitle {
                text(R.string.menu_account_transfer_title)
                subtext(R.string.menu_account_transfer_subtitle)
                accountWebNav(url(R.string.menu_url_account_transfer))
                trackScreen(SCREEN_TITLE_ACCOUNT_TRANSFER)
                newMenuItemKey(NewMenuItems.ACCOUNT_TRANSFER_KEY)
            }

            textWithArrowAndSubtitle(R.id.transferMoneyRetirementPlanRolloversMenuItem) {
                text(R.string.menu_retirement_plan_rollover_title)
                subtext(R.string.menu_retirement_plan_rollover_subtitle)
                clickable = true
                trackScreen(SCREEN_TITLE_RETIREMENT_PLAN_ROLLOVER)
            }

            textWithArrowAndSubtitle(R.id.transferMoneyDirectDepositMenuItem) {
                text(R.string.menu_setup_direct_deposit_title)
                subtext(R.string.menu_setup_direct_deposit_subtitle)
                clickable = true
                trackScreen(SCREEN_TITLE_DIRECT_DEPOSIT_SETUP)
            }

            textWithArrowAndSubtitle {
                navDirections(ActionOnlyNavDirections(R.id.bill_pay_nav_graph))
                text(R.string.menu_bill_pay_title)
                subtext(R.string.menu_bill_pay_subtitle)
            }
        }
    }

    fun getAccountFormattedStringByInstitutionType(
        institutionTypes: Array<InstitutionType>,
        accountDisplayPolicy: AccountDisplayPolicy = AccountDisplayPolicy.Default
    ): String {
        fun Account.isValidInstitutionType() = institutionTypes.contains(this.institutionType)

        val accounts = daoProvider.getAccountDao().getAllAccounts().value?.filter { it ->
            when (accountDisplayPolicy) {
                AccountDisplayPolicy.IncludeRetirementAccountsOnly -> it.isRetirementAccount()
                AccountDisplayPolicy.ExcludeRetirementAccounts -> (!it.isRetirementAccount()) && it.isValidInstitutionType()
                else -> it.isValidInstitutionType()
            }
        }
        val accountFormattedStr: StringBuilder = StringBuilder()
        if (accounts?.isNotEmpty() == true) {
            accounts.forEach { account ->
                accountFormattedStr.append("<li> ${account.accountDescription}: ${account.accountId}")
            }
        }
        return if (accountFormattedStr.isNotEmpty()) {
            accountFormattedStr.toString()
        } else {
            ""
        }
    }

    fun createWireTransferAction(): List<ActionItem> {
        return listOfNotNull(
            createSendAction(),
            createReceiveAction()
        )
    }

    private fun createReceiveAction(): ActionItem =
        ActionItem(R.id.transferMoneyWireTransferActionReceive, R.string.menu_wire_transfer_action_receive, R.drawable.ic_wire_transfer_receive)

    private fun createSendAction(): ActionItem =
        ActionItem(R.id.transferMoneyWireTransferActionSend, R.string.menu_wire_transfer_action_send, R.drawable.ic_wire_transfer_send)

    private fun url(@StringRes id: Int) = resources.getString(id, baseUrl)
}
