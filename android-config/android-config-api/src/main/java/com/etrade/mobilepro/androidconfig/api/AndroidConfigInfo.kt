package com.etrade.mobilepro.androidconfig.api

interface AndroidConfigInfo {
    val minimumVersion: String
    val akamaiDomain: String
    val chartVersion: String
}
