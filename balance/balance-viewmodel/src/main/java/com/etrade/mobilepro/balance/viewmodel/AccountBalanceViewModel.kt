package com.etrade.mobilepro.balance.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobilepro.balance.api.AccountBalance
import com.etrade.mobilepro.balance.api.GetAccountBalanceParameter
import com.etrade.mobilepro.balance.api.GetAccountBalanceUseCase
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.util.invoke
import com.etrade.mobilepro.viewmodel.BlockingLoadingIndicatorViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import java.math.BigDecimal
import javax.inject.Inject

class AccountBalanceViewModel @Inject constructor(
    private val defaultAccountRepo: DefaultAccountRepo,
    private val getAccountBalanceUseCase: GetAccountBalanceUseCase
) : ViewModel(), BlockingLoadingIndicatorViewModel {

    override val blockingLoadingIndicator: LiveData<Boolean>
        get() = _blockingLoadingIndicator

    val accountBalance: LiveData<AccountBalance?>
        get() = _accountBalance
    val accountBalanceFailed: LiveData<Boolean>
        get() = _accountBalanceFailed

    private val _accountBalance: MutableLiveData<AccountBalance?> = MutableLiveData()
    private val _accountBalanceFailed: MutableLiveData<Boolean> = MutableLiveData(false)
    private val _blockingLoadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)

    private var currentAccountId: String? = null
    private var balanceRefreshJob: Job? = null

    fun fetch(accountId: String) {
        if (accountId != currentAccountId) {
            cancelBalanceRefreshJob()
            currentAccountId = accountId
            _accountBalance.value = null
            refresh(forceRefresh = false)

            viewModelScope.launch { defaultAccountRepo.setAccountId(accountId) }
        }
    }

    fun refresh(forceRefresh: Boolean = true) {
        val accountId = currentAccountId.takeIf { balanceRefreshJob == null } ?: return

        if (accountId.isEmpty()) {
            _accountBalance.value = createEmptyAccountBalance()
        } else {
            refreshAccountBalance(accountId, forceRefresh)
        }
    }

    private fun cancelBalanceRefreshJob() {
        balanceRefreshJob?.run {
            cancel()
            balanceRefreshJob = null
        }
    }

    private fun refreshAccountBalance(accountId: String, forceRefresh: Boolean = true) {
        balanceRefreshJob = viewModelScope.launch {
            _accountBalanceFailed.value = false
            _blockingLoadingIndicator.value = true

            val result = getAccountBalanceUseCase(GetAccountBalanceParameter(accountId, forceRefresh))

            if (isActive) {
                result
                    .onSuccess { _accountBalance.value = it }
                    .onFailure { _accountBalanceFailed.value = true }

                _blockingLoadingIndicator.value = false
                balanceRefreshJob = null
            }
        }
    }

    private fun createEmptyAccountBalance(): AccountBalance {
        return object : AccountBalance {
            override val isPatternDayTrader: Boolean = false
            override val accountValue: BigDecimal? = null
            override val buyingPower: BigDecimal? = null
            override val cashAvailableForInvestment: BigDecimal? = null
            override val totalAvailableForWithdrawal: BigDecimal? = null
            override val availableExcessEquity: BigDecimal? = null
            override val intradayMargin: BigDecimal? = null
            override val intradayNonMargin: BigDecimal? = null
        }
    }
}
