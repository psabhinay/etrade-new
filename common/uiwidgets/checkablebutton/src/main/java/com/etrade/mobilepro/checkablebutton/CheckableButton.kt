package com.etrade.mobilepro.checkablebutton

import android.content.Context
import android.content.res.TypedArray
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.core.content.withStyledAttributes
import com.etrade.mobilepro.checkablebutton.databinding.ViewCheckableButtonBinding
import com.etrade.mobilepro.util.android.extension.layoutInflater
import com.etrade.mobilepro.util.formatCheckableButtonSymbols
import kotlinx.parcelize.Parcelize

class CheckableButton : FrameLayout {
    constructor(ctx: Context) : super(ctx) {
        init(null)
    }

    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet) {
        init(attributeSet)
    }

    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle) {
        init(attributeSet)
    }

    private val binding = ViewCheckableButtonBinding.inflate(layoutInflater, this, true)

    private fun init(attributeSet: AttributeSet?) {
        context.withStyledAttributes(attributeSet, R.styleable.CheckableButton) {
            setTextAndDescription()
            isChecked = getBoolean(R.styleable.CheckableButton_isChecked, false)
            selfUpdating = getBoolean(R.styleable.CheckableButton_selfUpdating, true)
        }
        with(binding) {
            if (contentDescription != null) {
                setupAccessibility()
            }
            toggleUnderlyingButtonsVisibility(isChecked)
            checked.setOnClickListener {
                if (selfUpdating) {
                    toggleUnderlyingButtonsVisibility(false)
                }
                checkedStateListener?.invoke(false, true)
                _isChecked = false
            }
            unchecked.setOnClickListener {
                if (selfUpdating) {
                    toggleUnderlyingButtonsVisibility(true)
                }
                checkedStateListener?.invoke(true, true)
                _isChecked = true
            }
        }
    }

    private var selfUpdating = true
    private var _isChecked = false

    var isChecked: Boolean
        get() = _isChecked
        set(value) {
            checkedStateListener?.invoke(value, false)
            binding.toggleUnderlyingButtonsVisibility(value)
            _isChecked = value
        }

    var checkedStateListener: ((isChecked: Boolean, byUser: Boolean) -> Unit)? = null

    override fun onSaveInstanceState(): Parcelable = State(
        parentState = super.onSaveInstanceState(),
        isChecked = isChecked
    )

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is State) {
            isChecked = state.isChecked
            super.onRestoreInstanceState(state.parentState)
        } else {
            super.onRestoreInstanceState(state)
        }
    }

    private fun TypedArray.setTextAndDescription() {
        val titleText = getString(R.styleable.CheckableButton_title)
        val contentDescription = titleText?.formatCheckableButtonSymbols()
        with(binding) {
            checked.text = titleText
            unchecked.text = titleText
            checked.contentDescription = contentDescription
            unchecked.contentDescription = contentDescription
        }
    }

    private fun ViewCheckableButtonBinding.toggleUnderlyingButtonsVisibility(isChecked: Boolean) {
        if (isChecked) {
            checked.visibility = View.VISIBLE
            unchecked.visibility = View.GONE
        } else {
            unchecked.visibility = View.VISIBLE
            checked.visibility = View.GONE
        }
    }

    private fun ViewCheckableButtonBinding.setupAccessibility() {
        checked.contentDescription = "$contentDescription ${context.getString(R.string.checked)}"
        unchecked.contentDescription = "$contentDescription ${context.getString(R.string.unchecked)}"
    }
}

@Parcelize
private class State(
    val parentState: Parcelable?,
    val isChecked: Boolean
) : Parcelable
