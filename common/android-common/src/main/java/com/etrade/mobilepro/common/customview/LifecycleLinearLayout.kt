package com.etrade.mobilepro.common.customview

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry

/**
 * Base class for custom LinearLayouts that provides lifecycle to enable livedata updates with
 * data-binding. To enable, call `binding.lifecycleOwner = this` on your data-binding object.
 */
open class LifecycleLinearLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : LinearLayout(context, attrs), LifecycleOwner {

    private val registry: LifecycleRegistry by lazy { LifecycleRegistry(this) }

    override fun getLifecycle(): Lifecycle {
        return registry
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        registry.handleLifecycleEvent(Lifecycle.Event.ON_START)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        registry.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
    }
}
