package com.etrade.mobilepro.editlist

import android.app.Activity.RESULT_OK
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * Abstract implementation of [ViewModel] for [EditListActivity].
 *
 * It does not modify items provided with a [provideItems] method call. When [onApplyChanges] method is called its corresponding arguments contain the same items.
 */
abstract class EditListViewModel<T : EditListItem> : ViewModel() {

    /**
     * List of items to be displayed.
     */
    val listItems: LiveData<List<T>>
        get() = _listItems

    /**
     * State of a list editing.
     */
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    private val _listItems: MutableLiveData<List<T>> = MutableLiveData()
    private val _viewState: MutableLiveData<ViewState> = MutableLiveData()

    private val initialItems: MutableList<T> = mutableListOf()
    protected val items: MutableList<T> = mutableListOf()
    private val removedItems: MutableList<T> = mutableListOf()

    private val isSaving: Boolean
        get() = _viewState.value == ViewState.InProgress

    /**
     * Fetches [listItems].
     */
    fun fetchItems() {
        if (isSaving) return
        onFetchItems(_viewState)
    }

    /**
     * Moves item from [oldPosition] position to [newPosition]
     *
     * @param oldPosition index of the item
     * @param newPosition index of the item
     */
    open fun moveItem(oldPosition: Int, newPosition: Int): Boolean {
        if (isSaving || oldPosition < 0 || newPosition >= items.size) {
            return false
        }

        val item = items.removeAt(oldPosition)
        items.add(newPosition, item)

        _listItems.value = items.toList()

        return true
    }

    /**
     * Removes an item at specific [position] from [listItems].
     *
     * @param position an index of the item in the list
     */
    internal fun removeItemAt(position: Int) {
        if (isSaving) return
        removedItems.add(items.removeAt(position))
        _listItems.value = items.toList()
    }

    /**
     * Toggles item in [position]
     * @return Boolean if item toggled
     */
    open fun toggleItemAt(position: Int): Boolean {
        val value = !items[position].isChecked

        if (isSaving || items.count { it.isChecked } == 1 && !value) {
            return true
        }

        items[position] = createFrom(items[position], value)

        _listItems.value = items.toList()

        return value
    }

    /**
     * Applies changes to a list.
     */
    fun applyChanges() {
        if (isSaving) return
        if (items != initialItems) {
            onApplyChanges(items, removedItems, _viewState)
        } else {
            _viewState.value = ViewState.Success()
        }
    }

    /**
     * Cancels all changes.
     */
    internal fun cancelChanges() {
        if (isSaving) return
        onCancelChanges(_viewState)
    }

    protected open fun onCancelChanges(viewState: MutableLiveData<ViewState>) {
        viewState.value = ViewState.Canceled
    }

    /**
     * Revers any changes made to items.
     *
     * @return `true` if changes are reverted, `false` if there were no changes to revert
     */
    internal fun revertChanges(): Boolean {
        if (items == initialItems) return false

        removedItems.clear()

        _listItems.value = items.apply {
            clear()
            addAll(initialItems)
        }.toList()

        return true
    }

    /**
     * Called when an action should be performed on a list.
     */
    internal fun performListAction() {
        onPerformListAction(_viewState)
    }

    /**
     * Instructs a descendant to fetch items. When items are ready they should be provided with a call to [provideItems] method.
     *
     * @param viewState live data of edit state
     */
    protected abstract fun onFetchItems(viewState: MutableLiveData<ViewState>)

    /**
     * Stores changes of edited list. Descendants must update [viewState] value accordingly to a current state.
     *
     * @param items ordered (not sorted) list of items to save
     * @param removedItems list of removed items in order of they were removed
     * @param viewState live data of edit state
     */
    protected abstract fun onApplyChanges(items: List<T>, removedItems: List<T>, viewState: MutableLiveData<ViewState>)

    /**
     * Called when view model wants to perform the list action.
     *
     * @param viewState live data of action state
     */
    protected open fun onPerformListAction(viewState: MutableLiveData<ViewState>) {
        // does nothing by default
    }

    /**
     * Provide [editListItems] after fetch.
     */
    protected fun provideItems(editListItems: List<T>) {
        initialItems.apply {
            clear()
            addAll(editListItems)
        }

        items.apply {
            clear()
            addAll(editListItems)
        }

        _listItems.postValue(editListItems)
    }

    protected open fun createFrom(item: T, value: Boolean): T {
        return item
    }

    /**
     * Edit state.
     */
    sealed class ViewState {

        /**
         * Nothing is happening.
         */
        object Idle : ViewState()

        /**
         * Called when a list editing is canceled.
         */
        object Canceled : ViewState()

        /**
         * Some operation is in progress.
         */
        object InProgress : ViewState()

        /**
         * List editing is finished and successful.
         * @param resultCode passed as code into setResult(code, intent)
         * @param intent used by setResult(code, intent) to pass some data back to onActivityResult
         */
        data class Success(
            val resultCode: Int = RESULT_OK,
            val intent: Intent? = null
        ) : ViewState()

        /**
         * There were a error when performing the last operation.
         */
        object Error : ViewState()
    }
}
