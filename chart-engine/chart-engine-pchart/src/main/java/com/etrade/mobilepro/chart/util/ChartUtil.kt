@file:SuppressWarnings("MagicNumber")

package com.etrade.mobilepro.chart.util

import android.graphics.Typeface
import android.util.TypedValue
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.etrade.mobilepro.chart.R
import com.etrade.mobilepro.chart.formatters.XAxisValueIndexToDataFormatter
import com.etrade.mobilepro.chart.formatters.XAxisValueMinuteToDateFormatter
import com.etrade.mobilepro.chart.formatters.YAxisValuePercentageFormatter
import com.etrade.mobilepro.chart.model.ChartConfiguration
import com.etrade.mobilepro.chart.model.ChartLineSet
import com.etrade.mobilepro.chart.model.ChartTimeFrame
import com.etrade.mobilepro.chart.rest.PchartEndpointType
import com.etrade.mobilepro.chartengine.ChartRequest
import com.etrade.mobilepro.chartengine.SymbolChartData
import com.etrade.mobilepro.chartengine.TimeFrame
import com.etrade.mobilepro.util.market.time.NEW_YORK_ZONE_ID
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import org.threeten.bp.ZonedDateTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.ChronoUnit
import java.util.Locale
import java.util.concurrent.TimeUnit

@Suppress("LongMethod")
fun configureChart(
    lineChart: LineChart,
    chartConfiguration: ChartConfiguration,
    chartData: LineData? = null
) {
    val xAxis = lineChart.xAxis
    val leftAxis = lineChart.axisLeft
    val rightAxis = lineChart.axisRight

    val formattedXValues = configureXAxisAndGetFormattedValues(lineChart, xAxis, chartConfiguration)

    rightAxis.axisMinimum = chartConfiguration.yAxisMin.toFloat()
    rightAxis.axisMaximum = chartConfiguration.yAxisMax.toFloat()
    val yFormatter = if (chartConfiguration.showPercentage) {
        YAxisValuePercentageFormatter()
    } else {
        null
    }
    rightAxis.valueFormatter = yFormatter
    formattedXValues?.run {
        lineChart.createContentDescription(
            xValues = this,
            chartConfiguration = chartConfiguration,
            dataSets = chartData?.dataSets?.map {
                it as LineDataSet
            }
        )
    }

    val textDimension = TypedValue().run {
        lineChart.context.resources.getValue(R.dimen.font_size_small_float, this, true)
        float
    }

    leftAxis.isEnabled = false
    rightAxis.textSize = textDimension
    xAxis.textSize = textDimension

    rightAxis.xOffset = 16f
    rightAxis.typeface = Typeface.DEFAULT
    rightAxis.textColor = ContextCompat.getColor(lineChart.context, R.color.black)
    rightAxis.textSize = textDimension
    rightAxis.setDrawGridLines(false)
    rightAxis.setDrawZeroLine(false)
    rightAxis.setDrawAxisLine(false)
    rightAxis.isGranularityEnabled = false
}

@Suppress("LongMethod", "ComplexMethod")
private fun configureXAxisAndGetFormattedValues(lineChart: LineChart, xAxis: XAxis, chartConfiguration: ChartConfiguration): List<Pair<Float, String>>? {
    val specificLabels: Set<Float>?
    val formattedXValues: List<Pair<Float, String>>?
    val dateFormat = DateTimeFormatter.ofPattern(chartConfiguration.timeFrame.formatString, Locale.US).withZone(NEW_YORK_ZONE_ID)
    if (isOneWeekOrNonOptionOneMonth(chartConfiguration.timeFrame, chartConfiguration.isOption)) {
        specificLabels = chartConfiguration.listOfLabels?.map { it.index.toFloat() }?.toSet()
        val xAxisValueIndexToDataFormatter = XAxisValueIndexToDataFormatter(dateFormat, chartConfiguration.listOfLabels)
        formattedXValues = specificLabels?.map { Pair(it, xAxisValueIndexToDataFormatter.getFormattedValue(it)) }
        xAxis.axisMinimum = 0f
        xAxis.axisMaximum = chartConfiguration.maxTimeInMillis.toFloat()
        xAxis.valueFormatter = xAxisValueIndexToDataFormatter
    } else {
        val maxZonedDateTimeFromData = Instant.ofEpochMilli(chartConfiguration.maxTimeInMillis).atZone(NEW_YORK_ZONE_ID)
        val maxRangeZonedDateTime = maxZonedDateTimeFromData.withHour(16).withMinute(5)
        var minRangeZonedDateTime = maxZonedDateTimeFromData.withHour(9).withMinute(30)
        val xAxisValueMinuteToDateFormatter = XAxisValueMinuteToDateFormatter(dateFormat)

        when (chartConfiguration.timeFrame) {
            is ChartTimeFrame.Day -> {
                specificLabels = setOf(
                    Duration.of(maxZonedDateTimeFromData.withHour(10).withMinute(0).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat(),
                    Duration.of(maxZonedDateTimeFromData.withHour(13).withMinute(0).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat(),
                    Duration.of(maxZonedDateTimeFromData.withHour(16).withMinute(0).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat()
                    // 10:00am, 1:00pm, 4:00pm sharp
                )
                formattedXValues = specificLabels.map { Pair(it, xAxisValueMinuteToDateFormatter.getFormattedValue(it)) }
            }
            ChartTimeFrame.OneMonth -> {
                minRangeZonedDateTime = maxZonedDateTimeFromData.withHour(0).withMinute(0).minusMonths(1)
                specificLabels = getLabelSetForTimeFrame(minRangeZonedDateTime, maxZonedDateTimeFromData, ChronoUnit.DAYS, 2)
                formattedXValues = specificLabels.map { Pair(it, xAxisValueMinuteToDateFormatter.getFormattedValue(it)) }
                // from today
                // 2 days tick size
            }
            ChartTimeFrame.ThreeMonths -> {
                minRangeZonedDateTime = maxZonedDateTimeFromData.withHour(0).withMinute(0).minusMonths(3)
                specificLabels = getLabelSetForTimeFrame(minRangeZonedDateTime, maxZonedDateTimeFromData.withDayOfMonth(1), ChronoUnit.MONTHS, 1)
                formattedXValues = specificLabels.reversed().map { Pair(it, xAxisValueMinuteToDateFormatter.getFormattedValue(it)) }
                // first of this month
                // 1 month tick size
            }
            ChartTimeFrame.SixMonths -> {
                minRangeZonedDateTime = maxZonedDateTimeFromData.withHour(0).withMinute(0).minusMonths(6)
                specificLabels = getLabelSetForTimeFrame(minRangeZonedDateTime, maxZonedDateTimeFromData.withDayOfMonth(1), ChronoUnit.MONTHS, 1)
                formattedXValues = specificLabels.reversed().map { Pair(it, xAxisValueMinuteToDateFormatter.getFormattedValue(it)) }
                // first of this month
                // 1 month tick size
            }
            ChartTimeFrame.OneYear -> {
                minRangeZonedDateTime = maxZonedDateTimeFromData.withHour(0).withMinute(0).minusYears(1)
                specificLabels = getLabelSetForTimeFrame(minRangeZonedDateTime, maxZonedDateTimeFromData.withDayOfMonth(1), ChronoUnit.MONTHS, 3)
                formattedXValues = specificLabels.reversed().map { Pair(it, xAxisValueMinuteToDateFormatter.getFormattedValue(it)) }
                // first of current month
                // 3 months tick size
            }
            ChartTimeFrame.TwoYears -> {
                minRangeZonedDateTime = maxZonedDateTimeFromData.withHour(0).withMinute(0).minusYears(2)
                specificLabels = getLabelSetForTimeFrame(minRangeZonedDateTime, maxZonedDateTimeFromData.withDayOfYear(1), ChronoUnit.YEARS, 1)
                formattedXValues = specificLabels.reversed().map { Pair(it, xAxisValueMinuteToDateFormatter.getFormattedValue(it)) }
                // first day of this year
                // tick size one year.
            }
            ChartTimeFrame.ThreeYears -> {
                minRangeZonedDateTime = maxZonedDateTimeFromData.withHour(0).withMinute(0).minusYears(3)
                specificLabels = getLabelSetForTimeFrame(minRangeZonedDateTime, maxZonedDateTimeFromData.withDayOfYear(1), ChronoUnit.YEARS, 1)
                formattedXValues = specificLabels.reversed().map { Pair(it, xAxisValueMinuteToDateFormatter.getFormattedValue(it)) }
                // first day of this year
                // tick size one year.
            }
            else -> throw UnsupportedOperationException("Chart time frame is not supported: ${chartConfiguration.timeFrame}")
        }

        xAxis.axisMinimum = Duration.of(minRangeZonedDateTime.toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat()
        xAxis.axisMaximum = Duration.of(maxRangeZonedDateTime.toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat()
        xAxis.valueFormatter = xAxisValueMinuteToDateFormatter
    }

    specificLabels?.let { lineChart.setSpecificPositionLabels(it) }

    xAxis.textColor = ContextCompat.getColor(lineChart.context, R.color.black)
    xAxis.axisLineColor = ContextCompat.getColor(lineChart.context, R.color.light_grey)
    xAxis.axisLineWidth = 1f
    xAxis.typeface = Typeface.DEFAULT
    xAxis.setDrawGridLines(false)
    xAxis.setDrawAxisLine(true)
    xAxis.position = XAxis.XAxisPosition.BOTTOM
    return formattedXValues
}

fun getLabelSetForTimeFrame(minZonedDateTime: ZonedDateTime, maxZonedDateTime: ZonedDateTime, unit: ChronoUnit, tickSize: Long): Set<Float> {
    val floatSet = mutableSetOf<Float>()
    var min = maxZonedDateTime
    while (min > minZonedDateTime) {
        floatSet.add(Duration.of(min.toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat())
        min = min.minus(tickSize, unit)
    }
    return floatSet
}

fun setColorForLineDataSet(set: LineDataSet, @ColorInt lineColor: Int) {
    set.axisDependency = YAxis.AxisDependency.RIGHT
    set.color = lineColor
    set.lineWidth = 2f
    set.isHighlightEnabled = false
    set.setDrawCircles(false)
    set.setDrawCircleHole(false)
    set.setDrawValues(false)
    set.disableDashedLine()
}

fun getChartLineDataWithDisplayDto(
    label: String,
    @ColorRes color: Int,
    data: SymbolChartData,
    chartTimeFrame: ChartTimeFrame,
    showAsPercentage: Boolean = true,
    isOption: Boolean = false
): ChartLineSet {
    val closingPrice = if (data.yesterdaysClose.compareTo(0.0) == 0) {
        data.displayData[0].close
    } else {
        data.yesterdaysClose
    }
    val result = ChartLineSet(label, color, closingPrice, showAsPercentage, isOption)
    for (chartOHLCItem in data.displayData) {
        result.addPoint(chartOHLCItem, chartTimeFrame)
    }
    if (isOneWeekOrNonOptionOneMonth(chartTimeFrame, isOption)) {
        result.highestTimeInMillis = data.displayData.size.toLong()
    }
    return result
}

private fun isOneWeekOrNonOptionOneMonth(chartTimeFrame: ChartTimeFrame, isOption: Boolean) =
    chartTimeFrame.timeFrame == TimeFrame.OneWeek || (chartTimeFrame.timeFrame == TimeFrame.OneMonth && !isOption)

fun getShouldCacheTime(chartRequest: ChartRequest): Long {
    return when (chartRequest.timeFrame) {
        TimeFrame.Day -> TimeUnit.MINUTES.toSeconds(3)
        TimeFrame.ExtendedHour -> TimeUnit.MINUTES.toSeconds(3)
        TimeFrame.OneWeek -> TimeUnit.MINUTES.toSeconds(15)
        TimeFrame.OneMonth -> TimeUnit.HOURS.toSeconds(10)
        else -> TimeUnit.DAYS.toSeconds(1)
    }
}

fun resolveEndpointType(isAuthenticated: Boolean) = if (isAuthenticated) {
    PchartEndpointType.SECURED
} else {
    PchartEndpointType.PUBLIC
}
