package com.etrade.mobilepro.trade.api

import com.etrade.mobilepro.instrument.WithSymbol
import com.etrade.mobilepro.orders.api.TransactionType
import java.math.BigDecimal

interface OrderLeg : WithSymbol {
    val action: TransactionType
    val quantity: BigDecimal?
}
