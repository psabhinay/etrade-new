package com.etrade.mobilepro.markets.data

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.markets.data.response.Quote
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class QuoteDtoMapperTest {

    @Test
    fun `valid quote dto containing positive values can be mapped to a market index`() {
        val index = Quote(
            symbol = "DJIND",
            symbolDesc = "DOW",
            price = "23,948.95",
            volume = "216,420,908",
            change = "411.27",
            percentChange = "1.75%",
            productType = "INDX",
            timestamp = "April 17, 2020 10:41 AM EST"
        ).toIndex()

        assertNotNull(index)
        with(index!!) {
            assertEquals(ticker, "DJIND")
            assertEquals(displayName, "DOW")
            assertEquals(price, BigDecimal("23948.95"))
            assertEquals(volume, BigDecimal("216420908"))
            assertEquals(change, BigDecimal("411.27"))
            assertEquals(percentChange, BigDecimal("1.75"))
            assertEquals(instrumentType, InstrumentType.INDX)
            assertEquals(timeStamp, "April 17, 2020 10:41 AM EST")
        }
    }

    @Test
    fun `valid quote dto containing negative can be mapped to a market index`() {
        val index = Quote(
            symbol = "DJIND",
            symbolDesc = "DOW",
            price = "-23,948.95",
            volume = "-216,420,908",
            change = "-411.27",
            percentChange = "-1.75%",
            productType = "INDX",
            timestamp = "April 17, 2020 10:41 AM EST"
        ).toIndex()

        assertNotNull(index)
        with(index!!) {
            assertEquals(ticker, "DJIND")
            assertEquals(displayName, "DOW")
            assertEquals(price, BigDecimal("-23948.95"))
            assertEquals(volume, BigDecimal("-216420908"))
            assertEquals(change, BigDecimal("-411.27"))
            assertEquals(percentChange, BigDecimal("-1.75"))
            assertEquals(instrumentType, InstrumentType.INDX)
            assertEquals(timeStamp, "April 17, 2020 10:41 AM EST")
        }
    }

    @Test
    fun `quote dto missing product type is mapped to an unknown instrument type market index`() {
        val index = Quote(
            symbol = "DJIND",
            symbolDesc = "DOW",
            price = "23,948.95",
            volume = "216,420,908",
            change = "411.27",
            percentChange = "1.75%",
            timestamp = "April 17, 2020 10:41 AM EST"
        ).toIndex()

        with(index!!) {
            assertEquals(ticker, "DJIND")
            assertEquals(displayName, "DOW")
            assertEquals(price, BigDecimal("23948.95"))
            assertEquals(volume, BigDecimal("216420908"))
            assertEquals(change, BigDecimal("411.27"))
            assertEquals(percentChange, BigDecimal("1.75"))
            assertEquals(instrumentType, InstrumentType.UNKNOWN)
            assertEquals(timeStamp, "April 17, 2020 10:41 AM EST")
        }
    }

    @Test
    fun `quote dto missing symbol cannot be mapped`() {
        val index = Quote(
            symbolDesc = "DOW",
            price = "23,948.95",
            volume = "216,420,908",
            change = "411.27",
            percentChange = "1.75%",
            productType = "INDX",
            timestamp = "April 17, 2020 10:41 AM EST"
        ).toIndex()

        assertNull(index)
    }

    @Test
    fun `quote dto missing symbol description cannot be mapped`() {
        val index = Quote(
            symbol = "DJIND",
            price = "23,948.95",
            volume = "216,420,908",
            change = "411.27",
            percentChange = "1.75%",
            productType = "INDX",
            timestamp = "April 17, 2020 10:41 AM EST"
        ).toIndex()

        assertNull(index)
    }

    @Test
    fun `quote dto missing price cannot be mapped`() {
        val index = Quote(
            symbol = "DJIND",
            symbolDesc = "DOW",
            volume = "216,420,908",
            change = "411.27",
            percentChange = "1.75%",
            productType = "INDX",
            timestamp = "April 17, 2020 10:41 AM EST"
        ).toIndex()

        assertNull(index)
    }

    @Test
    fun `quote dto missing volume cannot be mapped`() {
        val index = Quote(
            symbol = "DJIND",
            symbolDesc = "DOW",
            price = "23,948.95",
            change = "411.27",
            percentChange = "1.75%",
            productType = "INDX",
            timestamp = "April 17, 2020 10:41 AM EST"
        ).toIndex()

        assertNull(index)
    }

    @Test
    fun `quote dto missing change cannot be mapped`() {
        val index = Quote(
            symbol = "DJIND",
            symbolDesc = "DOW",
            price = "23,948.95",
            volume = "216,420,908",
            percentChange = "1.75%",
            productType = "INDX",
            timestamp = "April 17, 2020 10:41 AM EST"
        ).toIndex()

        assertNull(index)
    }

    @Test
    fun `quote dto missing percent change cannot be mapped`() {
        val index = Quote(
            symbol = "DJIND",
            symbolDesc = "DOW",
            price = "23,948.95",
            volume = "216,420,908",
            change = "411.27",
            productType = "INDX",
            timestamp = "April 17, 2020 10:41 AM EST"
        ).toIndex()

        assertNull(index)
    }

    @Test
    fun `quote dto missing timestamp cannot be mapped`() {
        val index = Quote(
            symbol = "DJIND",
            symbolDesc = "DOW",
            price = "23,948.95",
            volume = "216,420,908",
            change = "411.27",
            percentChange = "1.75%",
            productType = "INDX"
        ).toIndex()

        assertNull(index)
    }
}
