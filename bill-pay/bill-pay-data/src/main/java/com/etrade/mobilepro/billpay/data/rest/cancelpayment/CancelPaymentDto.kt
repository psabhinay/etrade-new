package com.etrade.mobilepro.billpay.data.rest.cancelpayment

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CancelPaymentRequestBodyDto(
    @Json(name = "value")
    val content: CancelPaymentRequestDto
)

@JsonClass(generateAdapter = true)
data class CancelPaymentRequestDto(
    @Json(name = "sessionId")
    val sessionId: String,
    @Json(name = "paymentId")
    val paymentId: String
)

@JsonClass(generateAdapter = true)
data class CancelPaymentResponseBodyDto(
    @Json(name = "successResults")
    val content: CancelPaymentResponseDto
) : BaseDataDto()

@JsonClass(generateAdapter = true)
data class CancelPaymentResponseDto(
    @Json(name = "status")
    val status: String
)

internal fun createCancelPaymentRequest(sessionId: String, paymentId: String): CancelPaymentRequestBodyDto = CancelPaymentRequestBodyDto(
    content = CancelPaymentRequestDto(
        sessionId = sessionId,
        paymentId = paymentId
    )
)
