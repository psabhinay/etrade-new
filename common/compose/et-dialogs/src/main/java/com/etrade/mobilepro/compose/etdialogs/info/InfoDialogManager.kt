package com.etrade.mobilepro.compose.etdialogs.info

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.AnnotatedString
import com.etrade.mobilepro.compose.etdialogs.elements.DialogButton
import javax.inject.Inject

interface InfoDialogManager {
    val state: ViewState
    fun update(state: ViewState)
    fun initWith(settings: Settings, makeVisible: Boolean = false)

    data class ViewState(
        val status: Status = Status.Gone,
        val settings: Settings = Settings()
    ) {
        enum class Status { Gone, Visible } // Loading, Error(with retry action bundled) are potential candidates as well, also consider sealed class
    }

    data class Settings(
        val title: String = "",
        val message: AnnotatedString = AnnotatedString(""),
        val onDismiss: () -> Unit = { },
        val buttonRow: List<DialogButton<ViewState>> = emptyList()
    )

    companion object
}

fun InfoDialogManager.Companion.createWith(settings: InfoDialogManager.Settings): InfoDialogManager = DefaultDialogManager(settings)

fun InfoDialogManager.Companion.create(
    title: String,
    message: AnnotatedString,
    onDismiss: () -> Unit = { },
    buttonRowBuilder: MutableList<DialogButton<InfoDialogManager.ViewState>>.(manager: InfoDialogManager, onDismiss: () -> Unit) -> Unit
): InfoDialogManager {
    val impl = DefaultDialogManager()
    val buttonRow = mutableListOf<DialogButton<InfoDialogManager.ViewState>>()
    val onDismissWrapped: () -> Unit = {
        onDismiss()
        impl.update(state = impl.state.copy(status = InfoDialogManager.ViewState.Status.Gone))
    }
    buttonRowBuilder(buttonRow, impl, onDismissWrapped)
    val settings = InfoDialogManager.Settings(title, message, onDismissWrapped, buttonRow)
    impl.initWith(settings)
    return impl
}

fun InfoDialogManager.show() {
    update(state = state.copy(status = InfoDialogManager.ViewState.Status.Visible))
}

fun InfoDialogManager.hide() {
    update(state = state.copy(status = InfoDialogManager.ViewState.Status.Gone))
}

class DefaultDialogManager @Inject constructor() : InfoDialogManager {
    internal constructor(settings: InfoDialogManager.Settings) : this() {
        initWith(settings)
    }

    override var state: InfoDialogManager.ViewState by mutableStateOf(InfoDialogManager.ViewState())
        private set

    override fun update(state: InfoDialogManager.ViewState) {
        this.state = state
    }

    override fun initWith(settings: InfoDialogManager.Settings, makeVisible: Boolean) {
        this.state = InfoDialogManager.ViewState(
            status = if (makeVisible) { InfoDialogManager.ViewState.Status.Visible } else { InfoDialogManager.ViewState.Status.Gone },
            settings = settings
        )
    }
}
