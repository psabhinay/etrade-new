package com.etrade.mobilepro.screener.api.datasource

import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.FundFamily
import com.etrade.mobilepro.screener.api.MutualFundGroup

interface MutualFundDataSource {
    fun getAllFundFamiliesItem(): FundFamily
    fun getFundFamilies(): List<FundFamily>

    fun getAllFundGroupsItem(): MutualFundGroup
    fun getFundGroups(): List<MutualFundGroup>

    fun getAllFundCategoriesItem(): FundCategory
}
