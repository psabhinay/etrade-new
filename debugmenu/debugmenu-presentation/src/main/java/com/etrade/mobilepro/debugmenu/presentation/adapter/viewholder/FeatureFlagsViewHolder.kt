package com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemFeatureFlagsBinding

internal class FeatureFlagsViewHolder(
    binding: DebugmenuItemFeatureFlagsBinding,
    private val listener: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.root.setOnClickListener { listener() }
    }
}
