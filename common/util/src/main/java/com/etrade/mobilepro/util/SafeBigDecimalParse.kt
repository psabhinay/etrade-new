package com.etrade.mobilepro.util

import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.Locale

private val logger = LoggerFactory.getLogger("SafeBigDecimalParser")

private val parser: ThreadLocal<DecimalFormat> = object : ThreadLocal<DecimalFormat>() {
    override fun initialValue(): DecimalFormat {
        return (DecimalFormat.getInstance(Locale.US) as DecimalFormat).apply {
            isParseBigDecimal = true
        }
    }
}

private val allowedChars: Set<Char> by lazy(LazyThreadSafetyMode.PUBLICATION) {
    setOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', ',', '.', 'E')
}

private val filteredChars: Set<Char> by lazy {
    setOf('+', '$', '%')
}

/**
 * Parses numeric values returned from some services that may contain sign indicators (+/-) in
 * addition to commas and decimal points.
 *
 * For example, some of the values that will be properly parsed are "+1.5" and "1,500"
 *
 * @return When the value can not be parsed, `null` is returned. Otherwise, the parsed [BigDecimal].
 */
@SuppressWarnings("TooGenericExceptionCaught")
fun String.safeParseBigDecimal(addToException: String? = null): BigDecimal? = try {
    val multiplier = getMultiplier()
    multiplier * parser.get().parse(filterOutIllegalChars(multiplier)) as BigDecimal
} catch (e: Exception) {
    logger.debug("Could not parse big decimal value: $this\nAdditional info:$addToException ${e.message}")
    null
}

fun String.getMultiplier(): BigDecimal {
    return when (lastOrNull()) {
        'k', 'K' -> BigDecimal("1000")
        'm', 'M' -> BigDecimal("1000000")
        'b', 'B' -> BigDecimal("1000000000")
        't', 'T' -> BigDecimal("1000000000000")
        else -> BigDecimal.ONE
    }
}

fun String.isBigDecimalZero(): Boolean {
    return safeParseBigDecimal()?.compareTo(BigDecimal.ZERO) == 0
}

private fun String.filterOutIllegalChars(multiplier: BigDecimal): String {
    return filter { !filteredChars.contains(it) } // remove chars that we expect to be in the string
        .let { if (multiplier == BigDecimal.ONE) it else it.substring(0, length - 1) } // remove multiplication suffix if it's there
        .takeIf { it == filter { c -> allowedChars.contains(c) } } // check if filtered value doesn't have any wrong chars
        ?: throw NumberFormatException("String contains illegal characters: $this")
}
