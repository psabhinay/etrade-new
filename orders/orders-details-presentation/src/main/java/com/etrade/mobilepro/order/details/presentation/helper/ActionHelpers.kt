package com.etrade.mobilepro.order.details.presentation.helper

import android.content.res.Resources
import com.etrade.mobilepro.dynamic.form.api.Action
import com.etrade.mobilepro.dynamic.form.api.ActionBuilder
import com.etrade.mobilepro.dynamic.form.api.ActionType
import com.etrade.mobilepro.dynamic.form.api.action
import com.etrade.mobilepro.order.details.presentation.R
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.presentation.OrderButtonVisibilityManager

internal fun placeOrder(resources: Resources, action: () -> Unit): Action {
    return action("placeOrder") {
        title = resources.getString(R.string.orders_action_place_order)
        onActionClick = { action() }
    }
}

internal fun OrderButtonVisibilityManager.newOrderAction(resources: Resources, action: () -> Unit): Action? {
    return actionIf(isCompleted, "newOrder") {
        title = resources.getString(R.string.orders_action_new_order)
        actionType = ActionType.PRIMARY
        onActionClick = { action() }
    }
}

internal fun OrderButtonVisibilityManager.cancelOrderAction(resources: Resources, action: () -> Unit): Action? {
    return actionIf(isCancellable, "cancelOrder") {
        title = resources.getString(R.string.orders_action_cancel_order)
        actionType = if (isCancelOnly) {
            ActionType.PRIMARY
        } else {
            ActionType.SECONDARY
        }
        onActionClick = { action() }
    }
}

internal fun OrderButtonVisibilityManager.deleteOrderAction(resources: Resources, action: () -> Unit): Action? {
    return actionIf(isDeletable, "deleteOrder") {
        title = resources.getString(R.string.orders_action_delete_order)
        actionType = ActionType.SECONDARY
        onActionClick = { action() }
    }
}

internal fun OrderButtonVisibilityManager.editOrderAction(resources: Resources, argument: OrderDetailsArgument, action: () -> Unit): Action? {
    return actionIf(isEditable, "editOrder") {
        title = resources.getString(R.string.orders_action_edit_order)
        actionType = if (argument.orderStatus == OrderStatus.SAVED) {
            ActionType.SECONDARY
        } else {
            ActionType.PRIMARY
        }
        onActionClick = { action() }
    }
}

internal fun OrderButtonVisibilityManager.previewOrderAction(resources: Resources, action: () -> Unit): Action? {
    return actionIf(isPreviewable, "previewOrder") {
        title = resources.getString(R.string.orders_action_preview_order)
        onActionClick = { action() }
    }
}

internal fun actionIf(isApplicable: Boolean, id: String, init: ActionBuilder.() -> Unit): Action? {
    return if (isApplicable) {
        action(id, init)
    } else {
        null
    }
}
