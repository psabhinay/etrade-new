package com.etrade.mobilepro.portfolio.tableview

data class PortfolioRecordsTableData(
    val columns: List<PortfolioColumn> = emptyList(),
    val rows: List<PortfolioRecordsTableRow> = emptyList()
)
