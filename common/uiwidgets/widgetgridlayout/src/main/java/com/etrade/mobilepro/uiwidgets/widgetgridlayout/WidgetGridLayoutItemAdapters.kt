package com.etrade.mobilepro.uiwidgets.widgetgridlayout

import com.etrade.mobilepro.movers.MoverItem

fun WidgetGridLayoutItem.toMoversItem(): MoverItem =
    MoverItem(
        ticker = ticker,
        displaySymbol = displaySymbol,
        price = price,
        gain = gain,
        gainPercentage = gainPercentage,
        decimalPercentChange = decimalPercentChange,
        shouldDisplayOptionsData = shouldDisplayOptionsData,
        optionsExpiryDate = optionsExpiryDate,
        optionsStrikePricePlusType = optionsStrikePricePlusType,
        isPositiveGain = isPositiveGain,
        itemClickHandler = itemClickHandler
    )
