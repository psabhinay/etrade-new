package com.etrade.mobilepro.appwidget.watchlist.selection

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.appwidget.api.settings.WidgetId
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettingsRepo
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import kotlinx.coroutines.launch
import kotlinx.coroutines.rx2.await
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val logger: Logger = LoggerFactory.getLogger(WatchlistSelectionViewModel::class.java)

class WatchlistSelectionViewModel @Inject constructor(
    private val watchlistRepo: WatchlistRepo,
    private val widgetSettingsRepo: WidgetSettingsRepo
) : ViewModel() {

    internal val viewState: LiveData<ViewState> get() = _viewState
    private val _viewState = MutableLiveData<ViewState>()

    internal fun onWatchlistSelected(watchlist: Watchlist) = viewModelScope.launch {
        val widgetId = _viewState.value?.widgetId ?: return@launch
        val settings = widgetSettingsRepo.loadSettings(widgetId)
        widgetSettingsRepo.saveSettings(widgetId, settings.copy(watchListId = watchlist.id))
        _viewState.updateValue {
            it.copy(
                watchlistRows = it.watchlistRows?.map { row ->
                    WatchlistRow(
                        watchlist = row.watchlist,
                        isSelected = row.watchlist.id == watchlist.id
                    )
                }
            )
        }
    }

    internal fun fetch(widgetId: WidgetId?, forced: Boolean = false) {
        if (widgetId == null) { return }
        if (_viewState.value == null || forced) {
            viewModelScope.launch {
                if (!_viewState.updateValue { it.copy(isLoading = true, retryAction = null) }) {
                    _viewState.value = ViewState(isLoading = true, widgetId = widgetId)
                }
                runCatching {
                    val selectedWatchlistId = widgetSettingsRepo.loadSettings(widgetId).watchListId
                    val watchlistCollection = watchlistRepo.getUserWatchlists().await()

                    watchlistCollection.map {
                        WatchlistRow(
                            watchlist = it,
                            isSelected = it.id == selectedWatchlistId
                        )
                    }
                }.onSuccess {
                    _viewState.value = ViewState(watchlistRows = it, widgetId = widgetId)
                }.onFailure {
                    logger.error(it.localizedMessage)
                    _viewState.value = ViewState(retryAction = { fetch(widgetId, forced = true) }, widgetId = widgetId)
                }
            }
        }
    }
}

internal data class ViewState(
    val isLoading: Boolean = false,
    val watchlistRows: List<WatchlistRow>? = null,
    val retryAction: (() -> Unit)? = null,
    val widgetId: WidgetId
)
