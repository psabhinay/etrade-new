package com.etrade.mobilepro.chartiq.usecase

import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.chartiq.api.model.OrderDetails
import com.etrade.mobilepro.chartiq.usecase.util.getOrderIdentifier
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem.OrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderCancelRequest
import com.etrade.mobilepro.order.details.api.OrderDetailsRepo
import com.etrade.mobilepro.order.details.api.OrderDetailsRequestParams
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.extensions.cancelOrder
import com.etrade.mobilepro.session.api.User
import kotlinx.coroutines.rx2.await
import kotlinx.coroutines.rx2.awaitLast
import org.slf4j.LoggerFactory
import javax.inject.Inject

class CancelOrderUseCaseImpl @Inject constructor(
    private val orderDetailsRepo: OrderDetailsRepo,
    private val applicationInfo: ApplicationInfo,
    private val user: User
) : CancelOrderUserCase {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    override suspend fun execute(parameter: OrderDetails): ETResult<CancelOrderResult> {
        val requestParams = OrderDetailsRequestParams(parameter.accountId, parameter.orderNumber, OrderStatus.ALL)
        return orderDetailsRepo.getOrderDetails(requestParams)
            .runCatchingET { awaitLast() }
            .map { it[0] as OrderDetailsItem }
            .mapCatching { item ->
                if (item.orderStatus == OrderStatus.CANCELLED || item.orderStatus == OrderStatus.CANCEL_REQUESTED) {
                    CancelOrderResult.CancelNotNeeded
                } else {
                    cancelOrder(parameter, item)
                }
            }
            .onFailure {
                logger.error("Unable to cancel order ", it)
                return ETResult.failure(it)
            }
    }

    private suspend fun cancelOrder(details: OrderDetails, item: OrderDetailsItem): CancelOrderResult {
        val request = OrderCancelRequest(
            accountId = details.accountId,
            userId = user.getUserId().toString(),
            platform = applicationInfo.platform(),
            instrumentType = details.instrumentType,
            orderTerm = item.term.typeCode,
            origSysCode = item.origSysCode,
            orderDetailsItem = item
        )
        return orderDetailsRepo.cancelOrder(item.getOrderIdentifier(), request, item.orderType, item.advancedItem?.orderType)
            .runCatching { await() }
            .map { CancelOrderResult.CancelSubmitted(it) }
            .getOrThrow()
    }
}
