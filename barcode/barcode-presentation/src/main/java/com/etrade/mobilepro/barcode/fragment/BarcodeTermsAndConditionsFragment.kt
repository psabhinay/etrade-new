package com.etrade.mobilepro.barcode.fragment

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.etrade.mobilepro.barcode.R
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory

abstract class BarcodeTermsAndConditionsFragment(
    @LayoutRes val layoutRes: Int,
    private var snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(layoutRes) {

    protected val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { activity?.window?.decorView?.findViewById(android.R.id.content) }
        )
    }

    abstract fun getBarcodeTermsAndConditions(): View

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getBarcodeTermsAndConditions().setOnClickListener {
            activity?.let { ctx ->
                startActivity(
                    SpannableTextOverlayActivity.intent(
                        ctx,
                        getString(R.string.barcode_terms_and_conditions_title),
                        getString(R.string.barcode_disclaimer),
                        R.drawable.ic_close
                    )
                )
            }
        }
    }
}
