package com.etrade.mobilepro.news.presentation.market

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.news.presentation.base.BaseNewsFragment
import com.etrade.mobilepro.news.presentation.databinding.FragmentNewsMarketBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import kotlinx.coroutines.ExperimentalCoroutinesApi

class MarketNewsFragment : BaseNewsFragment<MarketNewsViewModel>() {

    private val binding by viewBinding(FragmentNewsMarketBinding::bind)

    override val viewModelClass: Class<MarketNewsViewModel> = MarketNewsViewModel::class.java

    override val layoutRes: Int = R.layout.fragment_news_market

    override fun getNewsDetailContainer() = binding.newsDetailContainer?.root

    override fun getSwipeRefreshLayout(): SwipeRefreshLayout = binding.swipeRefresh

    override fun getNewsRecyclerView(): RecyclerView = binding.newsRv

    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onBecomeActive()
    }
}
