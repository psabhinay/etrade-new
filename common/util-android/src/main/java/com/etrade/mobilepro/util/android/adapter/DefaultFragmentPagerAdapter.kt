package com.etrade.mobilepro.util.android.adapter

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

open class DefaultFragmentPagerAdapter(
    private val fragment: Fragment,
    private val items: List<FragmentPagerItem>,
) : FragmentStateAdapter(fragment) {

    private val fragmentManager = fragment.childFragmentManager

    override fun createFragment(position: Int): Fragment {
        val cls = items[position].fragmentClass
        val classLoader = cls.classLoader
            ?: throw IllegalStateException("Bootstrap class loader is not supported.")
        return fragmentManager.fragmentFactory.instantiate(classLoader, cls.name).apply {
            arguments = fragment.arguments
        }
    }

    override fun getItemCount(): Int = items.size

    fun getPageTitle(position: Int): String =
        fragment.getString(items[position].titleRes)

    /**
     * Attaches this adapter to the viewpager and to a TabLayoutMediator that provides tab names.
     */
    fun attach(tabLayout: TabLayout, viewPager: ViewPager2) {
        viewPager.adapter = this
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = getPageTitle(position)
        }.attach()
    }

    class FragmentPagerItem(
        val fragmentClass: Class<out Fragment>,
        @StringRes
        val titleRes: Int
    )
}
