package com.etrade.mobilepro.accounts

import com.etrade.mobile.accounts.defaultaccount.WithAccountOptions
import com.etrade.mobile.accounts.defaultaccount.getIndexOf
import com.etrade.mobile.accounts.defaultaccount.getStateFor
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.preferences.UserPreferences
import javax.inject.Inject

interface AccountOrderRepo {

    suspend fun getAccounts(): List<AccountOrderItem>

    suspend fun saveAccounts(accounts: List<AccountOrderItem>)
}

class AccountOrderRepoImpl @Inject constructor(
    private val accountListRepo: AccountListRepo,
    private val userPreferences: UserPreferences
) : AccountOrderRepo {

    private var accountOptions: List<WithAccountOptions>
        get() = userPreferences.getAccountsOptions()
        set(value) = userPreferences.setAccountsOptions(value)

    override suspend fun getAccounts(): List<AccountOrderItem> {
        return accountListRepo.getAccountList()
            .map { account ->
                AccountOrderItem(
                    account.accountShortName,
                    account.accountUuid,
                    accountOptions.getStateFor(account.accountUuid)
                )
            }
            .sortedBy { accountOptions.getIndexOf(it.uuid) }
    }

    override suspend fun saveAccounts(accounts: List<AccountOrderItem>) {
        accountOptions = accounts.map { account ->
            object : WithAccountOptions {
                override val accountUuid: String
                    get() = account.uuid
                override val isEnabled: Boolean
                    get() = account.isChecked
            }
        }
    }
}
