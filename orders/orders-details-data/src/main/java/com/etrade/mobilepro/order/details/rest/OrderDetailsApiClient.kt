package com.etrade.mobilepro.order.details.rest

import com.etrade.eo.rest.retrofit.Xml
import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequestDto
import com.etrade.mobilepro.order.details.dto.OrderConfirmCancelResponseDto
import com.etrade.mobilepro.order.details.dto.OrderDeleteRequestDto
import com.etrade.mobilepro.order.details.dto.OrderDetailsRequestDto
import com.etrade.mobilepro.order.details.dto.OrderDetailsResponseDto
import com.etrade.mobilepro.order.details.dto.OrderGroupCancelResponseDto
import com.etrade.mobilepro.order.details.dto.StockPlanOrderCancelResponseDto
import com.etrade.mobilepro.order.details.dto.StockPlanOrderDetailsResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface OrderDetailsApiClient {

    @JsonMoshi
    @POST("/app/vieworders/getorderdetails.json")
    fun getOrderDetails(@Body @JsonMoshi requestBody: OrderDetailsRequestDto): Single<ServerResponseDto<OrderDetailsResponseDto>>

    @JsonMoshi
    @POST("/app/preparedorder/getpreparedorderdetail.json")
    fun getSavedOrderDetails(@Body @JsonMoshi requestBody: OrderDetailsRequestDto): Single<ServerResponseDto<OrderDetailsResponseDto>>

    @Xml
    @POST("/e/t/mobile/vieworderdetails")
    @FormUrlEncoded
    suspend fun getEmployeeStockPlanDetails(
        @Field("tradeId") tradeId: String
    ): StockPlanOrderDetailsResponseDto

    @Xml
    @POST("/e/t/mobile/espcancelorder")
    @FormUrlEncoded
    suspend fun cancelEmployeeStockPlanOrder(
        @Field("inputTradeId") tradeId: String
    ): StockPlanOrderCancelResponseDto

    @JsonMoshi
    @POST("/app/ordercancel/cancelconfirm.json")
    fun cancelConfirm(
        @Body @JsonMoshi requestBody: OrderConfirmCancelRequestDto
    ): Single<ServerResponseDto<OrderConfirmCancelResponseDto>>

    @JsonMoshi
    @FormUrlEncoded
    @POST("/e/t/mobile/groupcancelentry.json")
    fun groupCancel(
        @Field("accountid") accountId: String,
        @Field("ordernum") orderId: String,
        @Field("ordergrouptype") orderGroupType: String,
        @Field("_formtarget") formTarget: String = "groupcancelpreview",
        @Field("cancel") cancel: String = "cancel",
        @Field("cancel.x") cancelX: String = "24",
        @Field("cancel.y") cancelY: String = "5"
    ): Single<OrderGroupCancelResponseDto>

    @JsonMoshi
    @POST("/app/preparedorder/deletesavedorder.json")
    suspend fun deleteSavedOrder(@Body @JsonMoshi requestBody: OrderDeleteRequestDto): ServerResponseDto<BaseDataDto>
}
