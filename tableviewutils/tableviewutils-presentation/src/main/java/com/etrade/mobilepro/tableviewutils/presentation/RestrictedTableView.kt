package com.etrade.mobilepro.tableviewutils.presentation

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import com.etrade.mobilepro.tableviewutils.presentation.decorator.CustomTableViewDividerDecoration
import com.etrade.mobilepro.ui.nestedrecyclerview.RestrictedHeightView
import com.evrencoskun.tableview.TableView
import com.evrencoskun.tableview.handler.PreferencesHandler
import com.evrencoskun.tableview.preference.Preferences

/**
 * TableView with restricted height.
 */
open class RestrictedTableView : TableView, RestrictedHeightView {

    constructor(context: Context) : super(context) {
        initializedPreferencesHandler()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initializedPreferencesHandler()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initializedPreferencesHandler()
    }

    override val currentTop: Int
        get() = top

    override val currentBottom: Int
        get() = bottom

    /**
     * Needed to expose savePreferences() and loadPreferences() for configuration changes
     */
    private var rotationPreferencesHandler: PreferencesHandler? = null

    /**
     * Set [viewMaxHeight] as device height instead of 0 by default
     * With 0 [viewMaxHeight] [TableView] not able to restore scroll position after device rotation
     */
    override var viewMaxHeight: Int =
        maxOf(resources.displayMetrics.heightPixels, resources.displayMetrics.widthPixels)
        set(value) {
                if (field == value || value == 0) {
                    return
                }
                field = value
                requestLayout()
            }

    override fun setShowVerticalSeparators(showSeparators: Boolean) {
        super.setShowVerticalSeparators(showSeparators)
        if (showSeparators) {
            if (cellRecyclerView.itemDecorationCount == 0) {
                cellRecyclerView.addItemDecoration(createItemDecoration(LinearLayout.VERTICAL))
            }
        } else {
            for (i in 0 until cellRecyclerView.itemDecorationCount) {
                cellRecyclerView.removeItemDecorationAt(i)
            }
        }
        bringToFront()
    }

    override fun createItemDecoration(orientation: Int): DividerItemDecoration {
        val drawable = ContextCompat.getDrawable(context, R.drawable.thin_divider)
        return CustomTableViewDividerDecoration(context, orientation, drawable)
    }

    private fun initializedPreferencesHandler() {
        rotationPreferencesHandler = PreferencesHandler(this)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val newHeightMeasureSpec = MeasureSpec.makeMeasureSpec(viewMaxHeight, MeasureSpec.AT_MOST)
        super.onMeasure(widthMeasureSpec, newHeightMeasureSpec)
    }

    override fun checkScrollableVertically(direction: Int): Boolean =
        cellRecyclerView.canScrollVertically(direction)

    fun getScrollState(): Preferences? {
        return rotationPreferencesHandler?.savePreferences()
    }

    fun restoreScrollState(scrollState: Preferences) {
        rotationPreferencesHandler?.loadPreferences(scrollState)
    }
}
