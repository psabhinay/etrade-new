package com.etrade.mobilepro.accounts.util.android

import android.os.Parcel
import androidx.core.os.ParcelCompat.readBoolean
import androidx.core.os.ParcelCompat.writeBoolean
import com.etrade.mobile.accounts.billpay.BillPay
import com.etrade.mobile.accounts.details.AccountDetails
import kotlinx.parcelize.Parceler
import java.math.BigDecimal
import java.math.BigInteger

object AccountDetailsParceler : Parceler<AccountDetails> {

    override fun create(parcel: Parcel): AccountDetails = parcel.run {
        object : AccountDetails {
            override val accountIdKey: String = requireNotNull(readString())
            override val maskedAccountId: String = requireNotNull(readString())
            override val accountMode: String = requireNotNull(readString())
            override val accountName: String = requireNotNull(readString())
            override val accountShortName: String = requireNotNull(readString())
            override val accountLongName: String = requireNotNull(readString())
            override val mobileDescription: String = requireNotNull(readString())
            override val accountValue: BigDecimal? = readSerializable() as? BigDecimal
            override val institutionType: String = requireNotNull(readString())
            override val accountStatus: String = requireNotNull(readString())
            override val restrictionLevel: String = requireNotNull(readString())
            override val closedDate: Long = requireNotNull(readLong())
            override val brokerage: AccountDetails.Brokerage? = readBoolean(parcel).let { isPresent ->
                if (isPresent) { BrokerageParceler.create(parcel) } else { null }
            }
            override val billPay: BillPay? = readBoolean(parcel).let { isPresent ->
                if (isPresent) { BillPayParceler.create(parcel) } else { null }
            }
            override val accountId: String = requireNotNull(readString())
            override val nickname: String? = readString()
            override val instNo: BigInteger = readSerializable() as BigInteger
            override val accountType: String = requireNotNull(readString())
            override val accountCategory: String = requireNotNull(readString())
        }
    }

    override fun AccountDetails.write(parcel: Parcel, flags: Int) {
        parcel.apply {
            writeString(accountIdKey)
            writeString(maskedAccountId)
            writeString(accountMode)
            writeString(accountName)
            writeString(accountShortName)
            writeString(accountLongName)
            writeString(mobileDescription)
            writeSerializable(accountValue)
            writeString(institutionType)
            writeString(accountStatus)
            writeString(restrictionLevel)
            writeLong(closedDate)
            writeBoolean(parcel, brokerage != null)
            brokerage?.let {
                BrokerageParceler.run { it.write(parcel, flags) }
            }
            writeBoolean(parcel, billPay != null)
            billPay?.let {
                BillPayParceler.run { it.write(parcel, flags) }
            }
            writeString(accountId)
            writeString(nickname)
            writeSerializable(instNo)
            writeString(accountType)
            writeString(accountCategory)
        }
    }
}
