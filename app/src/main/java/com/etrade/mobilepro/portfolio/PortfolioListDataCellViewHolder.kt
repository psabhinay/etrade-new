package com.etrade.mobilepro.portfolio

import android.view.View
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.annotation.PluralsRes
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.etrade.mobilepro.R
import com.etrade.mobilepro.instrument.isSymbolLongOption
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableRow
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.android.accessibility.processAccessibilityStreamingEvent
import com.etrade.mobilepro.util.color.DataColor
import com.etrade.mobilepro.util.formatAccessibilityDescription
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder
import kotlin.math.roundToInt

private const val OPTION_DESC_DELIMITER = "\\b[\\s]+\\$\\b"

class PortfolioListDataCellViewHolder(itemView: View) : AbstractViewHolder(itemView) {

    private val context get() = itemView.context

    private val descriptionReplacements = mapOf(
        "\\bINC\\b" to "Incorporated",
        "\\bCO\\b" to "Corp"
    )
    private val heading = textView(R.id.heading)
    private val headingDetails = textView(R.id.heading_details)
    private val priceAndQuantityValue = textView(R.id.price_quantity_value)
    private val daysGainValue = textView(R.id.days_gain_value)
    private val totalGainValue = textView(R.id.total_gain_value)

    private var fullDescription = ""
    private var baseDescription = ""

    private fun String.resolveQuantityText(@PluralsRes resId: Int): String {
        val value = replace(",", "").toDouble().roundToInt()
        return context.resources.getQuantityString(resId, value, value)
    }

    @Suppress("LongMethod")
    fun bind(
        values: List<String>,
        row: PortfolioRecordsTableRow,
        columnMap: Map<PortfolioColumn, Int>
    ) {
        val isOption = isSymbolLongOption(row.rowHeader.symbol)
        val priceText = values[columnMap[PortfolioColumn.PRICE_PAID]!!]
        val quantityText = values[columnMap[PortfolioColumn.QUANTITY]!!]

        val symbol = row.rowHeader.symbol.substringBefore('-')
        var header = symbol
        var details = row.rowHeader.description
        val suffix: String

        if (isOption) {
            details = details?.substringAfter(' ')
            headingDetails.isVisible = true
            suffix = quantityText.resolveQuantityText(R.plurals.portfolio_contracts)
        } else {
            header = "$symbol - $details"
            details = ""
            headingDetails.isVisible = false
            suffix = quantityText.resolveQuantityText(R.plurals.portfolio_shares)
        }
        heading.text = header
        headingDetails.text = details

        val priceAndQuantityValueText = "$$priceText / $suffix"
        priceAndQuantityValue.text = priceAndQuantityValueText

        val daysGainValueDollar = values[columnMap[PortfolioColumn.DAYS_GAIN_DOLLAR]!!]
        val daysGainValuePercent = values[columnMap[PortfolioColumn.DAYS_GAIN_PERCENT]!!]
        val daysGainValueText = "$$daysGainValueDollar ($daysGainValuePercent)"
        daysGainValue.text = daysGainValueText
        daysGainValue.applyTextColor(daysGainValueDollar)
        val totalGainValueDollar = values[columnMap[PortfolioColumn.TOTAL_GAIN_DOLLAR]!!]
        val totalGainValuePercent = values[columnMap[PortfolioColumn.TOTAL_GAIN_PERCENT]!!]
        val totalGainValueText = "$$totalGainValueDollar ($totalGainValuePercent)"
        totalGainValue.text = totalGainValueText
        totalGainValue.applyTextColor(totalGainValueDollar)

        baseDescription =
            """
            $symbol.
            ${textView(R.id.days_gain_label).text},
            ${'$'}$daysGainValueDollar.
            $daysGainValuePercent.
            ${textView(R.id.total_gain_label).text},
            ${'$'}$totalGainValueDollar.
            $totalGainValuePercent.
            """.trimIndent()

        fullDescription =
            """
            ${String.format(
                "%s",
                header
                    .formatAccessibilityDescription(descriptionReplacements, false)
                    .replaceFirst(Regex(symbol), symbol.characterByCharacter())
            )},
            ${String.format(
                "%s",
                details
                    ?.split(Regex(OPTION_DESC_DELIMITER))
                    ?.joinToString(", $")
            )}.
            ${textView(R.id.price_quantity_label).text.replace(Regex("/"), "and")}.
            $$priceText.
            $suffix.
            ${baseDescription.removePrefix("$symbol.")}
            Double-tap to activate.
            """.trimIndent()

        itemView.processAccessibilityStreamingEvent(
            accessibilityText = baseDescription,
            accessibilityUserTouchText = fullDescription
        )
    }

    private fun TextView.applyTextColor(value: String) {
        val bigDecimal = value.safeParseBigDecimal() ?: return
        val color = when (bigDecimal.signum()) {
            -1 -> DataColor.NEGATIVE
            1 -> DataColor.POSITIVE
            else -> DataColor.NEUTRAL
        }
        setTextColor(resolveDataColor(color))
    }

    private fun textView(@IdRes resId: Int) = itemView.findViewById<TextView>(resId)

    private fun resolveDataColor(dataColor: DataColor) = when (dataColor) {
        DataColor.POSITIVE -> ContextCompat.getColor(context, R.color.green)
        DataColor.NEGATIVE -> ContextCompat.getColor(context, R.color.red)
        DataColor.NEUTRAL -> ContextCompat.getColor(context, R.color.textColorPrimary)
        DataColor.FILLED_POSITIVE, DataColor.FILLED_NEGATIVE -> ContextCompat.getColor(
            context,
            R.color.white
        )
    }
}
