package com.etrade.mobilepro.screener.api.filter.etf

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class EtfPerformanceFilter(
    val withinCategoryBottom20: Boolean = false,
    val withinCategoryBelowMid20: Boolean = false,
    val withinCategoryMid20: Boolean = false,
    val withinCategoryAboveMid20: Boolean = false,
    val withinCategoryTop20: Boolean = false,

    val withinCategoryOverLast1Month: Boolean = false,
    val withinCategoryOverLast3Months: Boolean = false,
    val withinCategoryOverLast6Months: Boolean = false,
    val withinCategoryOverLastYTD: Boolean = false,
    val withinCategoryOverLast1Year: Boolean = false,
    val withinCategoryOverLast3Years: Boolean = false,
    val withinCategoryOverLast5Years: Boolean = false,
    val withinCategoryOverLast10Years: Boolean = false,

    val navReturnBottom20: Boolean = false,
    val navReturnBelowMid20: Boolean = false,
    val navReturnMid20: Boolean = false,
    val navReturnAboveMid20: Boolean = false,
    val navReturnTop20: Boolean = false,

    val navReturnOverLast1Month: Boolean = false,
    val navReturnOverLast3Months: Boolean = false,
    val navReturnOverLast6Months: Boolean = false,
    val navReturnOverLastYTD: Boolean = false,
    val navReturnOverLast1Year: Boolean = false,
    val navReturnOverLast3Years: Boolean = false,
    val navReturnOverLast5Years: Boolean = false,
    val navReturnOverLast10Years: Boolean = false
) : ScreenerFilter {

    override fun isEmpty(): Boolean = isWithinCategoryEmpty() && isNavReturnEmpty()

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.ETF

    fun isWithinCategoryPerformanceValuesSelected(): Boolean = withinCategoryBottom20 || withinCategoryBelowMid20 ||
        withinCategoryMid20 || withinCategoryAboveMid20 || withinCategoryTop20

    fun isWithinCategoryPerformancePeriodSelected(): Boolean = withinCategoryOverLast1Month || withinCategoryOverLast3Months ||
        withinCategoryOverLast6Months || withinCategoryOverLastYTD || withinCategoryOverLast1Year || withinCategoryOverLast3Years ||
        withinCategoryOverLast5Years || withinCategoryOverLast10Years

    fun isWithinCategoryEmpty(): Boolean = !isWithinCategoryPerformanceValuesSelected() && !isWithinCategoryPerformancePeriodSelected()

    fun isNavReturnPerformanceValuesSelected(): Boolean = navReturnBottom20 || navReturnBelowMid20 ||
        navReturnMid20 || navReturnAboveMid20 || navReturnTop20

    fun isNavReturnPerformancePeriodSelected(): Boolean = navReturnOverLast1Month || navReturnOverLast3Months ||
        navReturnOverLast6Months || navReturnOverLastYTD || navReturnOverLast1Year || navReturnOverLast3Years ||
        navReturnOverLast5Years || navReturnOverLast10Years

    fun isNavReturnEmpty(): Boolean = !isNavReturnPerformanceValuesSelected() && !isNavReturnPerformancePeriodSelected()
}
