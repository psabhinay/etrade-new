package com.etrade.mobilepro.home.model

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.accounts.util.android.view.AccountViewType
import com.etrade.mobilepro.accounts.util.android.view.AccountsSpinnerModel
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.common.navigation.NavRouter
import com.etrade.mobilepro.common.navigation.NavRouterSignal
import com.etrade.mobilepro.common.navigation.NavRouterSignalViewModel
import com.etrade.mobilepro.common.navigation.navigate
import com.etrade.mobilepro.common.navigation.navigateToLandingPage
import com.etrade.mobilepro.home.AccountTab
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.landing.api.repo.LandingRepo
import com.etrade.mobilepro.navigation.BALANCES_PATH
import com.etrade.mobilepro.navigation.TAX_DOC_PATH
import com.etrade.mobilepro.navigation.TRANSACTIONS_PATH
import com.etrade.mobilepro.portfolio.PORTFOLIO_PATH
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.viewmodel.NavigationSignal
import com.etrade.mobilepro.viewmodel.NavigationSignalViewModel
import javax.inject.Inject

private const val DEEP_LINK_TAX_DOCS = "etrade://show/TaxDocs"
private val useDefaultIfAccountUuidIsEmptyDeepLinks = setOf(
    DEEP_LINK_TAX_DOCS
)

class AppActionViewModel @Inject constructor(
    private val landingRepo: LandingRepo,
    private val mainNavigation: MainNavigation,
    private val userViewModel: UserViewModel
) : ViewModel(), NavRouterSignalViewModel, NavigationSignalViewModel {

    override val navRouterSignal: LiveData<ConsumableLiveEvent<NavRouterSignal>>
        get() = _navRouterSignal

    override val navigationSignal: LiveData<ConsumableLiveEvent<NavigationSignal>>
        get() = _navigationSignal

    var suppressNavigateToLandingPage: Boolean = false

    private val _navRouterSignal: MutableLiveData<ConsumableLiveEvent<NavRouterSignal>> = MutableLiveData()
    private val _navigationSignal: MutableLiveData<ConsumableLiveEvent<NavigationSignal>> = MutableLiveData()

    fun handleQuoteAction(symbol: String) {
        _navigationSignal.value = { controller: NavController ->
            mainNavigation.navigateToQuoteDetails(controller, SearchResultItem.Symbol(symbol, InstrumentType.UNKNOWN).toBundle())
        }.consumable()
    }

    fun handleAccountTabAction(uri: Uri) {
        val landingPage = landingRepo.getLandingState().selected
        landingPage?.let {
            val accountUuid = if (it.uuid.isEmpty() && useDefaultIfAccountUuidIsEmptyDeepLinks.contains(uri.toString())) {
                userViewModel.defaultAccountUuid.orEmpty()
            } else {
                it.uuid
            }
            when (userViewModel.getAccountType(accountUuid)) {
                AccountViewType.ESP_ACCOUNT_VIEW,
                AccountViewType.SINGLE_BROKERAGE_ACCOUNT_VIEW -> {
                    navigateToSingleBrokerage(uri, accountUuid)
                }
                AccountViewType.MANAGED_ACCOUNT_VIEW -> {
                    navigateToManagedAccount(uri, accountUuid)
                }
                else -> {
                    navigateToFirstEligibleAccount(uri)
                }
            }
        }
    }

    fun updateDestination(destination: NavDestination) {
        if (destination.id == R.id.overviewFragment && userViewModel.isAuthenticated && !suppressNavigateToLandingPage) {
            suppressNavigateToLandingPage = true
            if (userViewModel.lastSelectedAccountViewType != AccountViewType.UNAUTHENTICATED_VIEW) {
                _navRouterSignal.navigateToLandingPage()
            }
        } else {
            suppressNavigateToLandingPage = false
        }
    }

    private fun navigateToFirstEligibleAccount(uri: Uri) {
        var brokerageAccount: AccountsSpinnerModel? = null
        var managedAccount: AccountsSpinnerModel? = null
        userViewModel.accountList.reversed().forEach { account ->
            if (account.accountViewType == AccountViewType.SINGLE_BROKERAGE_ACCOUNT_VIEW) {
                brokerageAccount = account
                return@forEach
            } else if (account.accountViewType == AccountViewType.MANAGED_ACCOUNT_VIEW && managedAccount == null) {
                managedAccount = account
            }
        }
        brokerageAccount?.let { mAccount ->
            mAccount.account?.accountUuid?.let { id ->
                navigateToSingleBrokerage(uri, id)
                return
            }
        }
        managedAccount?.let { mAccount ->
            mAccount.account?.accountUuid?.let { id ->
                navigateToManagedAccount(uri, id)
                return
            }
        }
        _navRouterSignal.navigate(MainGraphDirections.actionHomeToOverview())
    }

    private fun navigateToSingleBrokerage(uri: Uri, uuid: String) {
        val accountTab = when (uri.path) {
            PORTFOLIO_PATH -> AccountTab.Portfolio
            BALANCES_PATH -> AccountTab.Balances
            TRANSACTIONS_PATH -> AccountTab.Transactions
            TAX_DOC_PATH -> AccountTab.Documents
            else -> return
        }
        navigateToAccountTab(uuid, accountTab, getAdditionalArgumentsFromUri(uri))
    }

    private fun navigateToManagedAccount(uri: Uri, uuid: String) {
        val accountTab = when (uri.path) {
            BALANCES_PATH -> AccountTab.Balances
            TRANSACTIONS_PATH -> AccountTab.Transactions
            TAX_DOC_PATH -> AccountTab.Documents
            else -> return
        }
        navigateToAccountTab(uuid, accountTab, getAdditionalArgumentsFromUri(uri))
    }

    private fun navigateToAccountTab(uuid: String, accountTab: AccountTab, additionalArgs: String?) {
        _navRouterSignal.value = { controller: NavRouter ->
            userViewModel.updateSelectedAccount(uuid)
            val direction = MainGraphDirections.actionToSelectedAccountTabIndexOrTitle(
                tabTitle = accountTab.title,
                accountId = uuid,
                additionalArguments = additionalArgs
            )
            controller.navigate(direction)
            suppressNavigateToLandingPage = false
        }.consumable()
    }

    private fun getAdditionalArgumentsFromUri(uri: Uri): String? = if (uri.path == TAX_DOC_PATH) {
        TAX_DOC_PATH
    } else {
        null
    }

    fun navigateToAccountsLandingPage(invoke: (Boolean) -> Unit) {
        invoke(userViewModel.isAuthenticated && userViewModel.hasMoreThanOneAccount)
    }
}
