package com.etrade.mobilepro.livedata

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.test
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class LiveDataExtensionCombineWithTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun `check empty A and empty B`() {
        val liveDataA = MutableLiveData<Int>()
        val liveDataB = MutableLiveData<Int>()

        val result = liveDataA.combineWith(liveDataB) { _, _ ->
            throw RuntimeException("Should never be called")
        }

        result.test().assertNoValue()
    }

    @Test
    fun `check empty A and has B`() {
        val liveDataA = MutableLiveData<Int>()
        val liveDataB = MutableLiveData(20)

        val result = liveDataA.combineWith(liveDataB) { _, _ ->
            throw RuntimeException("Should never be called")
        }

        result.test().assertNoValue()
    }

    @Test
    fun `check has A and empty B`() {
        val expectedA = 10
        val liveDataA = MutableLiveData(expectedA)
        val liveDataB = MutableLiveData<Int>()

        val result = liveDataA.combineWith(liveDataB) { a, b ->
            assertEquals(expectedA, a)
            assertNull(b)
            a.toString()
        }

        result.test()
            .assertHasValue()
            .assertValue(expectedA.toString())
    }

    @Test
    fun `check has A and has B`() {
        val expectedA = 10
        val expectedB = 20
        val liveDataA = MutableLiveData(expectedA)
        val liveDataB = MutableLiveData(expectedB)

        val result = liveDataA.combineWith(liveDataB) { a, b ->
            assertEquals(expectedA, a)
            assertEquals(expectedB, b)
            (a + b!!).toString()
        }

        result.test()
            .assertHasValue()
            .assertValue((expectedA + expectedB).toString())
    }

    @Test
    fun `check sequence of operations`() {
        val liveDataA = MutableLiveData(10)
        val liveDataB = MutableLiveData(20)
        val result = liveDataA.combineWith(liveDataB) { a, b -> (a + b!!).toString() }

        liveDataA.value = 20
        result.test().assertHasValue().assertValue("40")

        liveDataB.value = 40
        result.test().assertHasValue().assertValue("60")

        liveDataA.value = null
        result.test().assertHasValue().assertValue("60")

        liveDataA.value = 40
        result.test().assertHasValue().assertValue("80")
    }
}
