package com.etrade.mobilepro.fundsflow.api

import com.etrade.mobilepro.util.formatters.getLastFourDigitOfAccountId
import java.math.BigInteger

private const val EXTERNAL_ACCOUNT_TYPE = "EXTERNAL"
private const val IRA_ACCOUNT_CATEGORY = "IRA"

interface FundsFlowAccount {
    val accountId: String
    val nickname: String?
    val instNo: BigInteger
    val accountType: String
    val accountCategory: String

    val isIraAccount
        get() = accountCategory.equals(IRA_ACCOUNT_CATEGORY, ignoreCase = true)

    val isExternalAccount
        get() = accountType.equals(EXTERNAL_ACCOUNT_TYPE, ignoreCase = true)

    fun displayString(): String {
        val accountLastFour = getLastFourDigitOfAccountId(accountId)
        return if (nickname.isNullOrBlank() || nickname == "null") {
            "$accountLastFour $accountType"
        } else {
            "$accountLastFour $nickname"
        }
    }
}
