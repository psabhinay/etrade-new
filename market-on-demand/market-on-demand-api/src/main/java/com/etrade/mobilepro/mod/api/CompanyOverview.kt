package com.etrade.mobilepro.mod.api

interface CompanyOverview {

    /**
     * The sector this company operates in, e.g. "Consumer Cyclicals"
     */
    val sector: String?

    /**
     * The industry, e.g. "Textiles & Apparel"
     */
    val industry: String?

    /**
     * A description of the business, e.g.
     * "Foo Corporation is a technology company. The Company develops, licenses, and supports a range of..."
     */
    val overview: String?

    /**
     * A HTTP URL of the company's website, e.g. https://polymediadivision.com
     */
    val webAddress: String?
}
