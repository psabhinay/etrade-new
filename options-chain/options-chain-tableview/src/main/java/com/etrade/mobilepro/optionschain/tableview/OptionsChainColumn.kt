package com.etrade.mobilepro.optionschain.tableview

import androidx.annotation.StringRes

enum class OptionsChainColumn(@StringRes val textRes: Int) {
    BID(R.string.bid),
    ASK(R.string.ask),
    LAST(R.string.last),
    CHANGE(R.string.change),
    VOLUME(R.string.volume),
    OPEN_INTEREST(R.string.open_interest);

    companion object {
        fun getDefaultColumns(isCall: Boolean): List<OptionsChainColumn> {
            val columnHeaders = mutableListOf(BID, ASK)

            if (isCall) {
                columnHeaders.reverse()
            }

            columnHeaders.add(LAST)
            columnHeaders.add(CHANGE)
            columnHeaders.add(VOLUME)
            columnHeaders.add(OPEN_INTEREST)

            return columnHeaders
        }
    }
}
