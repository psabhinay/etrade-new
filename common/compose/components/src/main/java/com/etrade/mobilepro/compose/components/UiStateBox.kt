package com.etrade.mobilepro.compose.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.Styles
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState

@Composable
fun UiStateBox(
    state: DynamicUiViewState,
    modifier: Modifier = Modifier,
    onRetry: (() -> Unit)? = null,
    successContent: @Composable () -> Unit,
) {
    UiStateBox(
        isSuccess = state is DynamicUiViewState.Success,
        isLoading = state is DynamicUiViewState.Loading,
        errorMsg = (state as? DynamicUiViewState.Error)?.message,
        onRetry = onRetry,
        warningMsg = (state as? DynamicUiViewState.Warning)?.message,
        modifier = modifier,
        successContent = successContent
    )
}

@Suppress("LongParameterList")
@Composable
fun UiStateBox(
    modifier: Modifier = Modifier,
    isSuccess: Boolean = true,
    isLoading: Boolean = false,
    errorMsg: String? = null,
    onRetry: (() -> Unit)? = null,
    warningMsg: String? = null,
    successContent: @Composable () -> Unit,
) {
    Box(modifier, contentAlignment = Alignment.Center) {
        if (isSuccess) {
            successContent()
        }
        if (isLoading) {
            LoadingIndicator(modifier = Modifier.padding(vertical = Dimens.Size16dp))
        }
        errorMsg?.let {
            Column(modifier = Modifier.padding(vertical = Dimens.Size16dp), horizontalAlignment = Alignment.CenterHorizontally) {
                Text(stringResource(R.string.error_message_general), style = Styles.Regular14sp.copy(color = colorResource(R.color.light_black)))
                Spacer(modifier = Modifier.height(Dimens.Size16dp))
                onRetry?.let {
                    RetryButton(onClick = it)
                }
            }
        }
        warningMsg?.let {
            Text(it)
        }
    }
}
