package com.etrade.mobilepro.caching

import com.etrade.eo.core.rx.RetryWithBackoff
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy

/**
 * Represents network-originated resource, that can be cached
 * see  {@link https://developer.android.com/jetpack/docs/guide#addendum}
 *
 * Type parameters:
 *  ResultType - business layer resource type
 *  NetworkType - data layer resource type, which comes from the network(could be json serializable)
 *  CacheType - data layer resource type, which comes from the db(could be RealmObject or other orm's dto)
 */
class CachedResource<ResultType : Any, NetworkType : Any, CacheType : Any>(
    private val id: Any,
    private val dataSourceDelegate: DataSourceDelegate<ResultType, NetworkType, CacheType>,
    private val controller: Controller<ResultType>,
    private val customRetryWithBackoff: RetryWithBackoff = RetryWithBackoff(failOnExhaustion = true)
) {
    init {
        require(customRetryWithBackoff.failOnExhaustion) { "RetryWithBackoff should fail on exhaustion " }
    }

    /**
     * Data manipulation(fetching, mapping, storing) delegate interface
     *
     * Type parameters:
     *  ResultType - business layer resource type
     *  NetworkType - data layer resource type, which comes from the network(could be json serializable)
     *  CacheType - data layer resource type, which comes from the db(could be RealmObject or other orm's dto)
     */
    interface DataSourceDelegate<ResultType, NetworkType, CacheType> {
        /**
         * Maps item, fetched from the network into one of the domain type
         */
        fun mapNetworkResponse(itemFromNetwork: NetworkType): ResultType

        /**
         * Maps item, fetched from the db into the one of domain type
         */
        fun mapCacheResponse(itemFromCache: CacheType): ResultType

        /**
         * Returns Single representing network call, that fetched the resource with provided resource Id
         * i.e. retrofit rx call
         * @param id Identity of resource, i.e. could be String or any data object
         */
        fun fetchFromNetwork(id: Any): Single<NetworkType>

        /**
         * Returns Single representing result of db query with provided resource Id
         * If this Single ends with *onError*, it is assumed as empty result.
         * So, to represent empty db query result this should end with *onError*, i.e. Single.error(NoSuchElementException())
         * The [java.util.NoSuchElementException] should be used to indicate the entry is not in the cache, as to not report the exception
         * as an actual error to telemetry. Other exceptions will be logged.
         * @param id Identity of resource, i.e. could be String or any data object
         */
        fun fetchFromDb(id: Any): Single<CacheType>

        /**
         * Returns Single representing result of db insertion with provided resource Id
         * Output of this Single could be exactly the inputted value, but it is not mandatory,
         * for example id field of the inputted value could be changed as a result of insertion.
         * If this Single ends with *onError*, this error won't be propagated further and inputted value will be returned
         * @param fetchedResult - item to be put into db
         */
        fun putToCache(fetchedResult: ResultType): Single<ResultType>
    }

    /**
     * Cache controlling interface
     */
    interface Controller<ResultType> {
        /**
         * Determines, if the itemFromCache should be refreshed - fetched again from the network
         * Should return true, if the item should be refreshed, false otherwise
         */
        fun shouldRefresh(itemFromCache: ResultType): Boolean

        /**
         * Determines, should the fetched from network item be put into the cache, or not.
         */
        fun shouldPutToCache(itemToCache: ResultType): Boolean = true
    }

    /**
     * Returns Observable, subscription to which triggers fetching and caching logic
     * Not intended to end with *onError*
     */
    fun asObservable(): Observable<Resource<ResultType>> {
        return Observable.create { sink ->
            sink.onNext(Resource.Loading())
            val subscription = dataSourceDelegate
                .fetchFromDb(id)
                .map { itemFromCache -> Resource.Success(dataSourceDelegate.mapCacheResponse(itemFromCache)) }
                .onErrorResumeNext(Single.just(Resource.Success()))
                .flatMapObservable { dbResult ->
                    val data = dbResult.data
                    if (data == null || controller.shouldRefresh(data)) {
                        sink.onNext(Resource.Loading(data))
                        dataSourceDelegate
                            .fetchFromNetwork(id)
                            .toObservable()
                            .retryWhen(customRetryWithBackoff)
                            .map { dataSourceDelegate.mapNetworkResponse(it) }
                            .flatMapSingle {
                                if (controller.shouldPutToCache(it)) {
                                    dataSourceDelegate
                                        .putToCache(it)
                                        .onErrorResumeNext(Single.just(it))
                                } else {
                                    Single.just(it)
                                }
                            }
                    } else {
                        Observable.just(data)
                    }
                }
                .subscribeBy(
                    onNext = { sink.onNext(Resource.Success(it)) },
                    onError = {
                        sink.onNext(Resource.Failed(error = it))
                        sink.onComplete()
                    },
                    onComplete = { sink.onComplete() }
                )
            sink.setCancellable {
                if (!subscription.isDisposed) {
                    subscription.dispose()
                }
            }
        }
    }
}
