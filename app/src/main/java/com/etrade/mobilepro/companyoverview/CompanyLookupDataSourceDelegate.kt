package com.etrade.mobilepro.companyoverview

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.mod.api.CompanyOverview
import com.etrade.mobilepro.mod.api.MarketOnDemand
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject
import javax.inject.Singleton

typealias CompanyLookupDataDelegate = SimpleCachedResource.DataSourceDelegate<CachedCompanyOverView, String>

@Singleton
class InMemoryCompanyLookupDataSourceDelegate @Inject constructor(
    private val marketOnDemand: MarketOnDemand
) : CompanyLookupDataDelegate {

    private val companyOverviews = ConcurrentHashMap<String, CachedCompanyOverView>()

    override fun fetchFromNetwork(id: String): Single<CachedCompanyOverView> = Single.fromCallable {
        CachedCompanyOverView(
            id = id,
            companyOverview = marketOnDemand.companyOverview(id),
            cachedAtMillis = System.currentTimeMillis()
        )
    }.subscribeOn(Schedulers.io())

    override fun fetchFromDb(id: String): Single<CachedCompanyOverView> = Single.fromCallable {
        companyOverviews[id] ?: throw NoSuchElementException("Company overview for $id not found in cache")
    }

    override fun putToCache(fetchedResult: CachedCompanyOverView): Single<CachedCompanyOverView> = Single.fromCallable {
        fetchedResult.also {
            companyOverviews[it.id] = it
        }
    }
}

data class CachedCompanyOverView(
    val id: String,
    val cachedAtMillis: Long,
    private val companyOverview: CompanyOverview
) : CompanyOverview by companyOverview
