package com.etrade.mobilepro.chart.cache

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.chartengine.SymbolChartData
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class CachedAggregationChartControllerTest {

    lateinit var sut: CachedAggregationChartController

    @Before
    fun setup() {
        sut = CachedAggregationChartController(object :
                SimpleCachedResource.Controller<SymbolChartData> {
                override fun shouldRefresh(itemFromCache: SymbolChartData): Boolean {
                    // For testing purposes, this will be our only `shouldRefresh` condition
                    return itemFromCache.queryValue.isNotBlank()
                }
            }
        )
    }

    @Test
    fun `should not refresh if no item should refresh`() {
        val shouldRefresh = sut.shouldRefresh(
            listOf(
                SymbolChartData(
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                )
            )
        )

        assertFalse(
            "Should return false if no item should refresh",
            shouldRefresh
        )
    }

    @Test
    fun `should refresh if any item should refresh`() {
        val shouldRefresh = sut.shouldRefresh(
            listOf(
                SymbolChartData(
                    queryValue = "This test should pass",
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                )
            )
        )

        assertTrue(
            "Should return true if any item should refresh",
            shouldRefresh
        )
    }

    @Test
    fun `should refresh if all items should refresh`() {
        val shouldRefresh = sut.shouldRefresh(
            listOf(
                SymbolChartData(
                    queryValue = "This test should pass",
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    queryValue = "This test should pass",
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    queryValue = "This test should pass",
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    queryValue = "This test should pass",
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                ),
                SymbolChartData(
                    queryValue = "This test should pass",
                    displayData = emptyList(),
                    yesterdaysClose = 0.0,
                    cachedAt = 0,
                    shouldCachedTimeInSeconds = 0
                )
            )
        )

        assertTrue(
            "Should return true if all items should refresh",
            shouldRefresh
        )
    }
}
