package com.etrade.mobilepro.portfolio.tableview

import androidx.annotation.StringRes
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.data.baseValueExtractors
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.util.color.DataColor
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.formatters.formatMarketDataCustomWithoutDollarSign
import com.etrade.mobilepro.util.formatters.formatMarketPercentDataCustom
import com.etrade.mobilepro.util.formatters.safeFormatVolume
import java.math.BigDecimal

enum class PortfolioColumn(
    val column: PortfolioRecordColumn,
    @StringRes val textRes: Int,
    @StringRes val reorderTextRes: Int = textRes,
    @StringRes val contentDescriptionRes: Int = reorderTextRes
) {

    ASK(PortfolioRecordColumn.ASK, R.string.ask) {

        override fun getDisplayText(baseValue: Any?): String = MarketDataFormatter.formatMarketDataCustom(baseValue as? BigDecimal)
    },

    BASE_SYMBOL_AND_PRICE(
        PortfolioRecordColumn.BASE_SYMBOL_PRICE,
        R.string.column_header_base_symbol_and_price,
        R.string.base_symbol_and_price
    ) {

        override fun getDisplayText(baseValue: Any?) = (baseValue as? String)?.takeIf { it.isNotBlank() } ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
    },

    BID(PortfolioRecordColumn.BID, R.string.bid) {

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustom(baseValue as? BigDecimal)
    },

    CHANGE_DOLLAR(
        PortfolioRecordColumn.PRICE_CHANGE,
        R.string.column_header_days_change_dollar,
        R.string.days_change_dollar
    ) {

        override val isCellDataColorful = true

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustomWithoutDollarSign(baseValue as? BigDecimal)
    },

    CHANGE_PERCENT(
        PortfolioRecordColumn.PRICE_CHANGE_PCT,
        R.string.column_header_days_change_percent,
        R.string.days_change_percent
    ) {

        override val isCellDataColorful = true

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketPercentDataCustom(baseValue as? BigDecimal)
    },

    DAYS_GAIN_DOLLAR(
        PortfolioRecordColumn.DAYS_GAIN,
        R.string.column_header_days_gain_dollar,
        R.string.days_gain_dollar
    ) {

        override val isCellDataColorful = true

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustomWithoutDollarSign(baseValue as? BigDecimal)
    },

    DAYS_GAIN_PERCENT(
        PortfolioRecordColumn.DAYS_GAIN_PCT,
        R.string.column_header_days_gain_percent,
        R.string.days_gain_percent
    ) {

        override val isCellDataColorful = true

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketPercData(baseValue as? Number)
    },

    DAYS_TO_EXPIRE(
        PortfolioRecordColumn.DAYS_EXPIRATION,
        R.string.column_header_days_to_expire, contentDescriptionRes = R.string.days_to_expiration
    ) {

        override fun getDisplayText(baseValue: Any?) = (baseValue as? Number)?.toString() ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
    },

    DELTA(PortfolioRecordColumn.DELTA, R.string.column_header_delta) {

        override fun getDisplayText(baseValue: Any?) = formatNullable(baseValue as? BigDecimal, MarketDataFormatter::formatMarketDataCustom)
    },

    EARNINGS_PER_SHARE(
        PortfolioRecordColumn.EPS,
        R.string.eps, contentDescriptionRes = R.string.eps_description
    ) {

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustom(baseValue as? BigDecimal)
    },

    FIFTY_TWO_WEEK_HIGH(
        PortfolioRecordColumn.WEEK_52_HIGH,
        R.string.column_header_fifty_two_wk_high,
        R.string.fifty_two_week_high
    ) {

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustom(baseValue as? BigDecimal)
    },

    FIFTY_TWO_WEEK_LOW(
        PortfolioRecordColumn.WEEK_52_LOW,
        R.string.column_header_fifty_two_wk_low,
        R.string.fifty_two_week_low
    ) {

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustom(baseValue as? BigDecimal)
    },

    GAMMA(PortfolioRecordColumn.GAMMA, R.string.column_header_gamma) {

        override fun getDisplayText(baseValue: Any?) = formatNullable(baseValue as? BigDecimal, MarketDataFormatter::formatMarketDataCustom)
    },

    IMPLIED_VOLATILITY_PERCENT(
        PortfolioRecordColumn.IV_PCT,
        R.string.column_header_implied_volatility_percent,
        R.string.implied_volatility_percent
    ) {

        override fun getDisplayText(baseValue: Any?) = formatNullable(baseValue as? BigDecimal, MarketDataFormatter::formatMarketDataCustom)
    },

    LAST_PRICE(
        PortfolioRecordColumn.LAST_TRADE,
        R.string.column_header_last_price,
        R.string.last_price
    ) {

        override val isCellDataColorful = true

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustom(baseValue as? BigDecimal)

        override fun resolveDataColor(baseValue: Any?, record: PortfolioRecord, dataColorResolver: DataColorResolver): DataColor {
            return dataColorResolver.resolveFilledDataColor(record.instrument.tickIndicator)
        }
    },

    MARK(PortfolioRecordColumn.MARK_TO_MARKET, R.string.column_header_mark) {

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustom(baseValue as? BigDecimal)
    },

    MARKET_CAP(
        PortfolioRecordColumn.MARKET_CAP,
        R.string.column_header_market_cap,
        R.string.market_cap
    ) {

        override fun getDisplayText(baseValue: Any?) = safeFormatVolume(baseValue as? BigDecimal)
    },

    MARKET_VALUE(
        PortfolioRecordColumn.MARKET_VALUE,
        R.string.column_header_market_value,
        R.string.market_value
    ) {

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustomWithoutDollarSign(baseValue as? BigDecimal)
    },

    PRICE_EARNING_RATIO(
        PortfolioRecordColumn.PE_RATIO,
        R.string.column_header_p_e, contentDescriptionRes = R.string.p_e_description
    ) {

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustom(baseValue as? BigDecimal)
    },

    PRICE_PAID(
        PortfolioRecordColumn.PRICE_PAID,
        R.string.column_header_price_paid,
        R.string.price_paid
    ) {

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustom(baseValue as? BigDecimal)
    },

    QUANTITY(
        PortfolioRecordColumn.QUANTITY,
        R.string.column_header_qty, contentDescriptionRes = R.string.quantity
    ) {

        override fun getDisplayText(baseValue: Any?) = baseValue?.toString() ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
    },

    THETA(PortfolioRecordColumn.THETA, R.string.column_header_theta) {

        override fun getDisplayText(baseValue: Any?) = formatNullable(baseValue as? BigDecimal, MarketDataFormatter::formatMarketDataCustom)
    },

    TODAYS_CLOSE(
        PortfolioRecordColumn.PREV_CLOSE,
        R.string.column_header_todays_close,
        R.string.todays_close
    ) {

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustom(baseValue as? BigDecimal)
    },

    TOTAL_GAIN_DOLLAR(
        PortfolioRecordColumn.TOTAL_GAIN,
        R.string.column_header_total_gain_dollar,
        R.string.total_gain_dollar
    ) {

        override val isCellDataColorful = true

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketDataCustomWithoutDollarSign(baseValue as? BigDecimal)
    },

    TOTAL_GAIN_PERCENT(
        PortfolioRecordColumn.TOTAL_GAIN_PCT,
        R.string.column_header_total_gain_percent,
        R.string.total_gain_percent
    ) {

        override val isCellDataColorful = true

        override fun getDisplayText(baseValue: Any?) = MarketDataFormatter.formatMarketPercData(baseValue as? Number)
    },

    VEGA(PortfolioRecordColumn.VEGA, R.string.column_header_vega) {

        override fun getDisplayText(baseValue: Any?) = formatNullable(baseValue as? BigDecimal, MarketDataFormatter::formatMarketDataCustom)
    },

    VOLUME(PortfolioRecordColumn.VOLUME, R.string.volume) {

        override fun getDisplayText(baseValue: Any?) = safeFormatVolume(baseValue as? BigDecimal)
    };

    /**
     * Creates a cell from a [record] in portfolio.
     *
     * @param record the record
     *
     * @return data cell to display in a table view
     */
    fun createCell(record: PortfolioRecord, dataColorResolver: DataColorResolver) = with(record) {
        val baseValueExtractor = baseValueExtractors.getValue(column)
        val baseValue = baseValueExtractor(record)
        val displayText = getDisplayText(baseValue)

        PortfolioRecordDataCell(
            rowId = recordId,
            displayText = displayText,
            value = baseValue ?: displayText,
            dataColor = resolveDataColor(baseValue, record, dataColorResolver),
            instrumentType = instrument.instrumentType,
            baseValue = baseValue
        )
    }

    protected open fun resolveDataColor(baseValue: Any?, record: PortfolioRecord, dataColorResolver: DataColorResolver) = if (isCellDataColorful) {
        dataColorResolver.resolveDataColor(baseValue)
    } else {
        DataColor.NEUTRAL
    }

    protected open val isCellDataColorful = false

    protected inline fun formatNullable(value: BigDecimal?, formatter: (BigDecimal) -> String): String =
        value?.let(formatter) ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE

    protected abstract fun getDisplayText(baseValue: Any?): String
}
