package org.chromium.customtabsclient.shared

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

class ObserverForCustomTabWarmUp(context: Context) : LifecycleObserver {
    private val webViewHelper = WebViewHelper(context)

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        webViewHelper.bindCustomTabsService()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        webViewHelper.unbindCustomTabsService()
    }
}
