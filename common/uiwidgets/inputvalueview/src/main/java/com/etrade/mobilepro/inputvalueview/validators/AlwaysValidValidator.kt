package com.etrade.mobilepro.inputvalueview.validators

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.inputvalueview.InputFieldManager.Value

/**
 * Default validator that is used when you don't need any validation at all
 */
class AlwaysValidValidator : InputFieldManager.Validator {
    override fun validate(value: String) = Value.Valid(value)
}
