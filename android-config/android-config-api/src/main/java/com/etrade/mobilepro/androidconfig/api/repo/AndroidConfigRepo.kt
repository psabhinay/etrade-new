package com.etrade.mobilepro.androidconfig.api.repo

import com.etrade.mobilepro.androidconfig.api.AndroidConfigInfo

interface AndroidConfigRepo {

    suspend fun getAndroidConfiguration(): AndroidConfigInfo
}
