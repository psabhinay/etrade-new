package com.etrade.mobilepro.calendar.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.calendar.CalendarMode
import com.etrade.mobilepro.calendar.databinding.AdapterWeekDayBinding
import org.threeten.bp.Duration
import org.threeten.bp.LocalDate

class DaysAdapter(
    private val mode: CalendarMode,
    position: Int,
    private val currentDate: LocalDate,
    var selectedDate: LocalDate?,
    private val disabledDaysPredicate: (LocalDate) -> Boolean,
    private val clickableDaysPredicate: (LocalDate) -> Boolean,
    private val events: Set<LocalDate>,
    private val onDateSelectListener: OnDateSelectListener,
    private val dayFormatter: ((LocalDate) -> String)?,
    private val weekDayHeight: Int
) : RecyclerView.Adapter<DayViewHolder>() {

    private val firstDateOfWeek: LocalDate = mode.getFirstDateOfRange(currentDate, position)
    private val size: Int = Duration.between(firstDateOfWeek.atStartOfDay(), mode.getLastDateOfRange(currentDate, position).atStartOfDay()).toDays().toInt() + 1
    private val currentMonth: LocalDate = getItem(firstDateOfWeek, size - 1L)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayViewHolder {
        val binding = AdapterWeekDayBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return DayViewHolder(
            binding,
            onDateSelectListener,
            disabledDaysPredicate,
            clickableDaysPredicate,
            dayFormatter,
            weekDayHeight
        )
    }

    override fun getItemCount(): Int = size

    override fun onBindViewHolder(holder: DayViewHolder, position: Int) {
        val item = getItem(firstDateOfWeek, position.toLong())

        if (mode == CalendarMode.MONTH) {
            if (item.monthValue == currentMonth.monthValue) {
                holder.itemView.visibility = View.VISIBLE
                holder.bind(item, events, currentDate, selectedDate)
            } else {
                holder.itemView.visibility = View.INVISIBLE
            }
        } else {
            holder.itemView.visibility = View.VISIBLE
            holder.bind(item, events, currentDate, selectedDate)
        }
    }

    fun processDaySelection(view: View, item: LocalDate) {
        AdapterWeekDayBinding.bind(view).markDayAndEvents(
            item = item,
            events = events,
            isSelectedDate = true,
            isCurrentDate = item == currentDate
        )
    }

    private fun getItem(firstDateOfWeek: LocalDate, position: Long): LocalDate = firstDateOfWeek.plusDays(position)
}

interface OnDateSelectListener {
    fun onDateSelected(view: View, position: Int, item: LocalDate)
}
