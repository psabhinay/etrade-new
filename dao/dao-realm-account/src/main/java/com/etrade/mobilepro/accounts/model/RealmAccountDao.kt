package com.etrade.mobilepro.accounts.model

import androidx.lifecycle.LiveData
import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.account.dao.AccountDao
import com.etrade.mobilepro.dao.realm.BaseRealmDao
import io.reactivex.Observable
import io.realm.Realm

class RealmAccountDao(realm: Realm) : AccountDao, BaseRealmDao<Account>(realm, Account::class.java) {
    override fun getAccountWithAccountId(accountId: String): Account? {
        return super.getWhere("accountId", accountId)
    }

    override fun getAccountWithAccountIdAndType(accountId: String, accountType: AccountType, async: Boolean): Account? {
        return Realm.getInstance(realm.configuration).use { realmInstance ->
            realmInstance.where(classObj)
                .equalTo("accountId", accountId)
                .and()
                .equalTo("_accountType", accountType.name).findFirst()?.let {
                    realmInstance.copyFromRealm(it)
                }
        }
    }

    override fun getAccountWithUuid(uuid: String): Account? {
        return super.get(uuid)
    }

    override fun getAllAccounts(async: Boolean): LiveData<List<Account>> {
        return super.getAll(async)
    }

    override fun getAllAccountsNonAsync(): List<Account> {
        return Realm.getInstance(realm.configuration).use {
            it.copyFromRealm(it.where(classObj).findAll())
        }
    }

    override fun getAllAccountsObservable(): Observable<List<Account>> {
        return super.getAllObservable()
    }
}
