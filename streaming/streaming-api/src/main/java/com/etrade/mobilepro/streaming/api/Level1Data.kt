package com.etrade.mobilepro.streaming.api

import org.threeten.bp.LocalDate

interface Level1Data {
    val ask: String?
    val askExchange: String?
    val askSize: String?
    val bid: String?
    val bidExchange: String?
    val bidSize: String?
    val change: String?
    val changePercent: String?
    val closingMark: String?
    val dayHigh: String?
    val dayLow: String?
    val earningsPerShare: String?
    val lastPrice: String?
    val lastPriceExchange: String?
    val lastVolume: String?
    val mark: String?
    val openPrice: String?
    val previousClosePrice: String?
    val priceToEarnings: String? // "P/E"
    val timestampSinceEpoch: Long?
    val volume: String?
    val tickIndicator: Short?
    val yearHigh: String?
    val yearHighDate: LocalDate?
    val yearLow: String?
    val yearLowDate: LocalDate?
}
