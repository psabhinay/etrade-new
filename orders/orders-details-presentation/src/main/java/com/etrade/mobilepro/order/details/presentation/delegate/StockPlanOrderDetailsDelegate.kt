package com.etrade.mobilepro.order.details.presentation.delegate

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.api.Action
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.api.OrderDetailsRepo
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem
import com.etrade.mobilepro.order.details.presentation.OrderDetailItemInterfaceMapperType
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel.ActionsArgument
import com.etrade.mobilepro.order.details.presentation.R
import com.etrade.mobilepro.order.details.presentation.helper.cancelOrderAction
import com.etrade.mobilepro.order.details.presentation.helper.mergeResult
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.order.details.router.StockPlanOrderDetailsArgument
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.extensions.android.cancelOrderDialog
import com.etrade.mobilepro.orders.extensions.android.cancelSuccessDialog
import com.etrade.mobilepro.orders.presentation.OrderButtonVisibilityManager
import com.etrade.mobilepro.orders.presentation.orderButtonVisibilityManager
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class StockPlanOrderDetailsDelegate @Inject constructor(
    private val orderDetailsRepo: OrderDetailsRepo,
    private val orderMapper: OrderDetailItemInterfaceMapperType,
    private val resource: Resources
) : OrderDetailsViewModel.BaseOrderDetailsDelegate() {

    override val loadingIndicators: Set<LiveData<Boolean>>
        get() = setOf(cancelLoadingIndicator)

    private val cancelLoadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)

    override suspend fun loadItems(
        argument: OrderDetailsArgument,
        quotes: Deferred<Map<String, OrderDetailItemInterface>>
    ): ETResult<OrderDetailsViewModel.OrderDetails> {
        require(argument is StockPlanOrderDetailsArgument)

        val symbol = argument.symbols.first()
        return orderDetailsRepo.getStockPlanOrderDetails(argument.orderNumber, symbol)
            .map {
                val items = combine(
                    symbol,
                    orderMapper(
                        it, 0, OrderType.UNKNOWN,
                        OrderDetailDisplayItem.OrderBidAskItem.DynamicOrderBidAskItemWithSymbol(symbol), null
                    ),
                    quotes.await()
                )
                OrderDetailsViewModel.OrderDetails(items, it.orderStatus)
            }
    }

    private fun combine(
        symbol: String,
        orderItems: List<OrderDetailItemInterface>,
        quotes: Map<String, OrderDetailItemInterface>
    ): List<OrderDetailItemInterface> {

        val results = mutableListOf<OrderDetailItemInterface>()

        val quote = quotes[symbol]
        quote?.also { quoteWidget ->
            results.add(quoteWidget)
            val (_, tradeLegBidAskViewModel) = getQuoteDetailsViewModels()
            mergeResult(orderItems, tradeLegBidAskViewModel.quoteBidAskItem)
        }
        results.addAll(orderItems)

        return results
    }

    override fun createActions(argument: ActionsArgument, manager: OrderButtonVisibilityManager): List<Action> {
        val orderDetailsArgument = argument.orderDetailsArgument
        require(orderDetailsArgument is StockPlanOrderDetailsArgument)

        return listOfNotNull(
            manager.cancelOrderAction(resource) {
                confirmCancel(orderDetailsArgument)
            }
        )
    }

    override fun createButtonVisibilityManager(argument: OrderDetailsArgument): OrderButtonVisibilityManager {
        return orderButtonVisibilityManager {
            isCancellable = argument.orderStatus == OrderStatus.OPEN
        }
    }

    private fun confirmCancel(argument: StockPlanOrderDetailsArgument) {
        mutableDialog.value = resource.cancelOrderDialog(
            positiveAction = { cancelOrder(argument) },
            negativeAction = ::resetActionStates
        )
    }

    private fun cancelOrder(argument: StockPlanOrderDetailsArgument) {
        executeOperation(
            loadingIndicator = cancelLoadingIndicator,
            operation = {
                orderDetailsRepo.cancelStockPlanOrder(argument.orderNumber)
            },
            onSuccess = {
                mutableDialog.value = resource.cancelSuccessDialog(resource.getString(R.string.orders_dialog_cancel_order_success_message), ::resetActionStates)
                refresh()
            }
        )
    }
}
