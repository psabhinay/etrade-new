package com.etrade.mobilepro.orders.list.api

import com.etrade.mobilepro.orders.api.OrderStatus

data class OrderParamsLoadSizeAndKey(
    val orderParams: OrderParams,
    val loadSize: Int,
    val key: String?
) {
    val isSaved: Boolean
        get() = orderParams.orderStatus == OrderStatus.SAVED

    fun getQueryStringForCache(url: String) = StringBuilder()
        .append(url)
        .append(orderParams.accountId)
        .append(loadSize)
        .append(orderParams.fromDate)
        .append(orderParams.toDate)
        .append(orderParams.orderStatus.requestCode)
        .append(key)
        .append(orderParams.isStockPlan)
        .toString()
}
