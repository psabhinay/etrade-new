package com.etrade.mobilepro.settings.viewmodel.feature

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.dropdown.DropDownSelector
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

interface SettingDropDownViewState {
    val title: String?
    val contentDescription: String?
    val openDropDownSelectorWithTag: ConsumableLiveEvent<String>?
}

interface HasDropDown {
    val viewState: LiveData<out SettingDropDownViewState>
    val dropDownTag: String
    fun createDropDownSelector(fragmentManager: FragmentManager): DropDownSelector
}
