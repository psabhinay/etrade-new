package com.etrade.mobilepro.trade.api.option

import com.etrade.mobilepro.trade.api.ConfirmedOrder
import com.etrade.mobilepro.trade.api.WithOrderRequest

interface ConfirmedOptionOrder : ConfirmedOrder, OptionOrder, WithOrderRequest
