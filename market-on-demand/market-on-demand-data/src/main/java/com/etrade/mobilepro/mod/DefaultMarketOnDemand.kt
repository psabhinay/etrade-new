package com.etrade.mobilepro.mod

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.mod.api.CompanyOverview
import com.etrade.mobilepro.mod.api.MarketOnDemand
import com.etrade.mobilepro.mod.api.Report
import com.etrade.mobilepro.mod.auth.api.AuthTokenProvider
import retrofit2.Call
import javax.inject.Inject

private const val ETRADE_PROVIDER = "Etrade"
private const val E_ASTERISK_TRADE = "E*TRADE"
private const val DATE_STRING_LENGTH = 10

class DefaultMarketOnDemand @Inject constructor(
    private val authTokenProvider: AuthTokenProvider,
    private val serviceClient: ModServiceClient,
    private val marketOnDemandUrlBuilder: MarketOnDemandUrlBuilder
) : MarketOnDemand {
    override fun getFundReports(symbol: String): List<Report> {
        val authToken = authTokenProvider.getModAuthToken()
        val response = getFundReports(
            symbol = symbol,
            authToken = authToken
        ).execute()

        return if (response.isSuccessful) {
            response.body()?.results?.filter {
                it.provider == ETRADE_PROVIDER
            }?.map {
                Report(
                    E_ASTERISK_TRADE,
                    it.reportUrl,
                    DateFormattingUtils.formatUsDateToDisplayedShortDate(it.reportDate?.take(DATE_STRING_LENGTH))
                )
            } ?: throw ModException("Report search failed.")
        } else {
            throw ModException("Report search failed.")
        }
    }

    @Throws(ModException::class)
    override fun companyOverview(symbol: String): CompanyOverview {
        val authToken = authTokenProvider.getModAuthToken()
        val response = companyLookup(
            symbol = symbol,
            authToken = authToken
        ).execute()

        if (response.isSuccessful) {
            return response.body() ?: throw ModException("Company overview response missing body")
        } else {
            throw ModException("Company overview response not successful")
        }
    }

    private fun companyLookup(symbol: String, authToken: String): Call<CompanyOverviewDto> {
        val httpUrl = marketOnDemandUrlBuilder.constructUrl("Research/API/Mobile/CompanyLookup/json", symbol, authToken)
        return serviceClient.companyLookupPost(httpUrl)
    }

    private fun getFundReports(symbol: String, authToken: String): Call<FundReportsResponseDto> {
        val httpUrl = marketOnDemandUrlBuilder.constructUrl("Research/API/Mobile/ReportSearch/json", symbol, authToken)
        return serviceClient.getFundReports(httpUrl)
    }
}
