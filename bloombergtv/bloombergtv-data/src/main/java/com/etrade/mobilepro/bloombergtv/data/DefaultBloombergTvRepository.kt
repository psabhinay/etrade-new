package com.etrade.mobilepro.bloombergtv.data

import com.etrade.mobilepro.bloombergtv.api.BloombergTvRepository
import com.etrade.mobilepro.bloombergtv.api.BloombergTvResponse
import io.reactivex.Single

class DefaultBloombergTvRepository(private val bloombergTvService: BloombergTvService) : BloombergTvRepository {
    override fun getBloombergtvResponse(): Single<BloombergTvResponse> {
        return bloombergTvService.getBloombergTvResponse()
    }
}
