package com.etrade.mobilepro.dynamicui.dto.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class BankAccountOverviewRequestDto(
    @Json(name = "accountOverview")
    val accountOverview: BankAccountOverviewPayLoad
)

@JsonClass(generateAdapter = true)
class BankAccountOverviewPayLoad(
    @Json(name = "accountUuid")
    val accountUuid: String?,
    @Json(name = "viewType")
    val viewType: String = "Transactions",
    @Json(name = "startDate")
    val startDate: String,
    @Json(name = "endDate")
    val endDate: String,
    @Json(name = "widgets")
    val widgetList: List<String> = emptyList()
)
