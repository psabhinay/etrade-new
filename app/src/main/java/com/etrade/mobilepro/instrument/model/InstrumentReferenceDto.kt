package com.etrade.mobilepro.instrument.model

import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
@SuppressWarnings("LongParameterList", "LongMethod")
class InstrumentReferenceDto(
    @Json(name = "instrumentId")
    val instrumentId: String,
    @Json(name = "symbol")
    override val symbol: String,
    @Json(name = "symbolDescription")
    override val symbolDescription: String = "",
    @Json(name = "displaySymbol")
    override val displaySymbol: String,
    @Json(name = "previousClose")
    val prevClose: String,
    @Json(name = "typeCode")
    val typeCodeRaw: String,
    @Json(name = "exchangeCode")
    val exchangeCode: String,
    @Json(name = "bid")
    val bidRaw: String,
    @Json(name = "ask")
    val askRaw: String,
    @Json(name = "dayChangeValue")
    val daysChangeVal: String?,
    @Json(name = "dayChangePerc")
    val daysChangePer: String?,
    @Json(name = "volume")
    val volumeRaw: String,
    @Json(name = "marketCap")
    val marketCapRaw: String,
    @Json(name = "pe")
    val pe: String,
    @Json(name = "eps")
    val eps: String,
    @Json(name = "lastPrice")
    val lastPriceRaw: String,
    @Json(name = "week52High")
    val week52High: String,
    @Json(name = "week52Low")
    val week52Low: String,
    @Json(name = "impliedVolatilityPct")
    val impliedVolatilityPct: String,
    @Json(name = "openInterest")
    val openInterest: String,
    @Json(name = "delta")
    val deltaRaw: String,
    @Json(name = "premium")
    val premium: String,
    @Json(name = "gamma")
    val gammaRaw: String,
    @Json(name = "vega")
    val vegaRaw: String,
    @Json(name = "theta")
    val thetaRaw: String,
    @Json(name = "expirationDate")
    val expirationDate: String,
    @Json(name = "underlyingTypeCode")
    val underlyingTypeCode: String,
    @Json(name = "underlyingExchangeCode")
    val underlyingExchangeCode: String,
    @Json(name = "underlyingSymbol")
    val underlyingSymbol: String,
    @Json(name = "daysExpiration")
    val daysExpiration: String
) : Instrument {

    override val instrumentType: InstrumentType
        get() = InstrumentType.from(typeCodeRaw)

    override val ask: BigDecimal?
        get() = askRaw.safeParseBigDecimal()

    override val bid: BigDecimal?
        get() = bidRaw.safeParseBigDecimal()

    override val priceEarningRation: BigDecimal?
        get() = pe.safeParseBigDecimal()

    override val earningsPerShare: BigDecimal?
        get() = eps.safeParseBigDecimal()

    override val fiftyTwoWeekHigh: BigDecimal?
        get() = week52High.safeParseBigDecimal()

    override val fiftyTwoWeekLow: BigDecimal?
        get() = week52Low.safeParseBigDecimal()

    override val lastPrice: BigDecimal?
        get() = lastPriceRaw.safeParseBigDecimal()

    override val tickIndicator: Short? = null

    override val marketCap: BigDecimal?
        get() = marketCapRaw.safeParseBigDecimal()

    override val volume: BigDecimal
        get() = volumeRaw.toBigDecimal()

    override val daysChange: BigDecimal?
        get() = daysChangeVal?.safeParseBigDecimal()

    override val daysChangePercentage: BigDecimal?
        get() = daysChangePer?.safeParseBigDecimal()

    override val delta: BigDecimal?
        get() = deltaRaw.safeParseBigDecimal()

    override val gamma: BigDecimal?
        get() = gammaRaw.safeParseBigDecimal()

    override val theta: BigDecimal?
        get() = thetaRaw.safeParseBigDecimal()

    override val vega: BigDecimal?
        get() = vegaRaw.safeParseBigDecimal()

    override val impliedVolatilityPercentage: BigDecimal?
        get() = impliedVolatilityPct.safeParseBigDecimal()

    override val daysToExpire: Int?
        get() = daysExpiration.toIntOrNull()

    override val underlyingSymbolInfo: WithSymbolInfo
        get() = Symbol(underlyingSymbol, instrumentType)

    override val previousClose: BigDecimal?
        get() = prevClose.safeParseBigDecimal()
}
