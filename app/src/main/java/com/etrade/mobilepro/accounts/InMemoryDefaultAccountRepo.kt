package com.etrade.mobilepro.accounts

import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.userviewmodel.UserViewModel
import javax.inject.Inject

class InMemoryDefaultAccountRepo @Inject constructor(
    private val userViewModel: UserViewModel
) : DefaultAccountRepo {
    override suspend fun getAccountId(): ETResult<String?> {
        return runCatchingET { userViewModel.defaultAccountId }
    }

    override suspend fun setAccountId(accountId: String?) {
        val selectedAccountModel = userViewModel.tradeEligibleAccounts.firstOrNull {
            it.account?.accountId == accountId
        }

        selectedAccountModel?.let {
            userViewModel.defaultAccount = it
        }
    }

    override suspend fun getDisplayName(): ETResult<String?> {
        return runCatchingET {
            userViewModel.defaultAccount?.accountDisplayName
        }
    }
}
