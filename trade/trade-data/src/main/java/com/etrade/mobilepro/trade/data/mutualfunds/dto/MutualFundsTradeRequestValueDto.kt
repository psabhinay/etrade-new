package com.etrade.mobilepro.trade.data.mutualfunds.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MutualFundsTradeRequestValueDto(
    @Json(name = "value")
    val tradePlaceOrderRequestDto: MutualFundsTradePlaceRequestDto
)

@JsonClass(generateAdapter = true)
class MutualFundsTradePlaceRequestDto(
    @Json(name = "previewIds")
    val previewIds: List<String>,
    @Json(name = "accountId")
    val overnightIndicatorFlag: String = "0"
)
