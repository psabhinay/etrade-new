package com.etrade.mobilepro.quoteapi.usecase

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.util.UseCase

interface GetMobileQuotesUseCase : UseCase<GetMobileQuotesParameter, ETResult<List<MobileQuote>>>

class GetMobileQuotesParameter(
    val symbols: List<String>,
    val requireExchangeCode: Boolean
)
