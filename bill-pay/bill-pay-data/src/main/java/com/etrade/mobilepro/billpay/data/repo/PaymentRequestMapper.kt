package com.etrade.mobilepro.billpay.data.repo

import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.billpay.api.PaymentRequest
import com.etrade.mobilepro.billpay.api.PaymentResult
import com.etrade.mobilepro.billpay.data.rest.schedulepayment.PayeeInfoDto
import com.etrade.mobilepro.billpay.data.rest.schedulepayment.PaymentInfoDto
import com.etrade.mobilepro.billpay.data.rest.schedulepayment.PaymentRequestBodyDto
import com.etrade.mobilepro.billpay.data.rest.schedulepayment.PaymentRequestDto
import com.etrade.mobilepro.billpay.data.rest.schedulepayment.PaymentResponseBodyDto
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.Locale

private const val DUPLICATE_PAYMENT_ERROR_CODE = "1302"
private const val DATE_PATTERN = "MM/dd/yyyy"

fun PaymentRequest.toDto(sessionId: String): PaymentRequestBodyDto {
    return PaymentRequestDto(
        sessionId = sessionId,
        payment = PaymentInfoDto(
            paymentId = paymentId,
            amount = amount,
            scheduleDate = getFormatter().format(scheduleDate),
            memo = memo,
            originAccountId = originAccount.accountId,
            originAccountInstNo = originAccount.instNo
        ),
        payee = PayeeInfoDto(
            payeeId = destinationAccount.payeeId,
            leadDays = destinationAccount.leadDays
        )
    ).let {
        PaymentRequestBodyDto(it)
    }
}

fun ServerResponseDto<PaymentResponseBodyDto>.toPaymentResult(): PaymentResult {
    val paymentDetails = data?.content?.firstOrNull()
    val isScheduled = paymentDetails?.paymentId != null
    val isDuplicate = (extractError() as? ServerError.Known)?.messages?.firstOrNull()?.code == DUPLICATE_PAYMENT_ERROR_CODE

    return when {
        isScheduled -> PaymentResult.Success(
            paymentId = requireNotNull(paymentDetails?.paymentId),
            confirmationNumber = requireNotNull(paymentDetails?.confirmationNumber),
            deliveryDate = LocalDate.parse(requireNotNull(paymentDetails?.deliveryDate), getFormatter())
        )
        isDuplicate -> PaymentResult.Error.DuplicateEntry
        else -> PaymentResult.Error.Generic
    }
}

private fun getFormatter() = DateTimeFormatter.ofPattern(DATE_PATTERN, Locale.US)
