package com.etrade.mobilepro.uiwidgets.widgetgridlayout

import android.content.Context
import android.util.AttributeSet
import com.etrade.mobilepro.movers.MoverGridLayout

class WidgetGridLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : MoverGridLayout(context, attrs, defStyleAttr) {

    fun submitItems(items: List<WidgetGridLayoutItem>) {
        setData(items.map { it.toMoversItem() })
    }
}
