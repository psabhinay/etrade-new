package com.etrade.mobilepro.util.delegate

import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty
import kotlin.reflect.KProperty0

/**
 * Delegates setting of a value to another object's property:
 *
 * ```
 * var x: Int by obj::variableIntProperty
 * ```
 */
operator fun <R> KMutableProperty0<R>.setValue(obj: Any, property: KProperty<*>, value: R) = set(value)

/**
 * Delegates getting of a value to another object's property:
 *
 * ```
 * val x: Int by obj::varOrValIntProperty
 * ```
 */
operator fun <R> KMutableProperty0<R>.getValue(obj: Any, property: KProperty<*>): R = get()

/**
 * Delegates getting of a value to another object's property:
 *
 * ```
 * val x: Int by obj::varOrValIntProperty
 * ```
 */
operator fun <R> KProperty0<R>.getValue(obj: Any, property: KProperty<*>): R = get()
