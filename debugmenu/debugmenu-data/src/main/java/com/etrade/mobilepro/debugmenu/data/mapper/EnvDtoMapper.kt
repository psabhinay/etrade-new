package com.etrade.mobilepro.debugmenu.data.mapper

import com.etrade.mobilepro.appconfig.api.BaseUrls
import com.etrade.mobilepro.debugmenu.data.rest.EnvDto

internal fun EnvDto?.mapToBaseUrls(): BaseUrls = BaseUrls(
    web = this?.myETUrl ?: "",
    express = "",
    marketOnDemand = this?.modUrl ?: "",
    mobileETrade = "",
    mobileTrade = this?.tradingUrl ?: "",
    api = this?.apiUrl ?: "",
    level1StreamerUrl = "",
    level2StreamerUrl = "",
    environmentChange = ""
)
