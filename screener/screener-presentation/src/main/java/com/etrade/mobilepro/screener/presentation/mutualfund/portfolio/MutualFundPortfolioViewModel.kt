package com.etrade.mobilepro.screener.presentation.mutualfund.portfolio

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.mutualfund.PortfolioFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.MutualFund
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class MutualFundPortfolioViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @MutualFund screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<PortfolioFilter, MutualFundPortfolioViewModel.ViewState>(
    aggregator,
    ScreenerType.MutualFund.Portfolio,
    screenerInteractor
) {
    override fun provideInitialState(initialFilter: PortfolioFilter): ViewState = ViewState(filter = initialFilter)

    data class ViewState(
        override val filter: PortfolioFilter
    ) : BaseFilterScreenViewModel.ViewState<PortfolioFilter>
}
