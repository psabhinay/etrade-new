package com.etrade.mobilepro.news.api.filter

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.util.NetworkState
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

interface FilterDataSource<T> {
    val networkState: LiveData<NetworkState>
    val items: LiveData<List<T>>
    val selectedItem: LiveData<T>
    fun refresh()
    fun getSelectedItemIndex(): Int
    fun setSelectedItem(item: T)
    fun findAndSelectItemByAccountId(accountId: String, isStockPlan: Boolean? = null)
    fun clear()
}

interface FilterDataSourceWithForcedUpdate<T> : FilterDataSource<T> {
    val forcedUpdateWith: LiveData<ConsumableLiveEvent<T>>
    fun refresh(forceUpdate: Boolean = false)
    override fun refresh() = refresh(forceUpdate = false)
}
