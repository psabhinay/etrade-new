package com.etrade.mobilepro.news.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.news.presentation.databinding.ItemNetworkStateLoadingBinding
import com.etrade.mobilepro.util.android.extension.viewBinding

class LoadingAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        NetworkStateItemViewHolder(parent.viewBinding(ItemNetworkStateLoadingBinding::inflate))
    override fun getItemCount() = 1
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = Unit
}
