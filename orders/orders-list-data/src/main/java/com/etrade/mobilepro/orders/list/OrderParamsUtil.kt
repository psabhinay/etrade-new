package com.etrade.mobilepro.orders.list

import com.etrade.mobilepro.orders.list.api.OrderParams
import com.etrade.mobilepro.orders.list.dto.OrderListRequestDto
import com.etrade.mobilepro.orders.list.dto.RequestValueDto
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.threeten.bp.LocalDate

private const val DEFAULT_START_DATE = ""

private val dateFormatter = createAmericanSimpleDateFormatter()
fun getOrderRequest(orderParams: OrderParams, loadSize: Int, key: String): OrderListRequestDto {
    val fromDateString = orderParams.fromDate?.let { dateFormatter.format(it) } ?: DEFAULT_START_DATE
    val toDateString = dateFormatter.format(orderParams.toDate)
    return OrderListRequestDto(
        RequestValueDto(
            orderParams.accountId,
            loadSize,
            fromDateString,
            toDateString,
            orderParams.orderStatus.requestCode,
            key
        )
    )
}

fun generateStockPlanOrdersRequestBody(requestParams: OrderParams): RequestBody {
    val orderStatus = requestParams.orderStatus.espStatusCode
    val fromDate = requestParams.fromDate?.let { formatLocalDateString(it) } ?: DEFAULT_START_DATE
    val toDate = formatLocalDateString(requestParams.toDate)
    val index = requestParams.accountIndex ?: "0"
    val requestString = "orderStatus=$orderStatus&fromDate=$fromDate&toDate=$toDate&SelectedAcctIndex=$index"
    return requestString.toRequestBody("application/x-www-form-urlencoded".toMediaTypeOrNull())
}

private fun formatLocalDateString(date: LocalDate): String {
    return "${date.month.value}%2f${date.dayOfMonth}%2f${date.year}"
}
