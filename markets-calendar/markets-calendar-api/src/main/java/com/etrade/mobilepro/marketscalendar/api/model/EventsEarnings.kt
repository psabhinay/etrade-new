package com.etrade.mobilepro.marketscalendar.api.model

class EventsEarnings(
    val symbol: String?,
    val epsestMean: String?,
    val epsactualValue: String?,
    val dateTime: String?,
    val epsannounceDate: String?,
    val epsannounceTime: String?
)
