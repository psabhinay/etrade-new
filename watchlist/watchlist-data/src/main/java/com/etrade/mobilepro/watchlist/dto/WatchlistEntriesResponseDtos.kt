package com.etrade.mobilepro.watchlist.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class WatchlistEntriesResponseDto(
    @Json(name = "data")
    val data: WatchlistEntiresDataDto
)

@JsonClass(generateAdapter = true)
class WatchlistEntiresDataDto(
    @Json(name = "watchListEntries")
    val watchListEntries: WatchlistEntriesDto? = null,
    @Json(name = "Messages")
    val messages: WatchlistMessage? = null
)

@JsonClass(generateAdapter = true)
class WatchlistEntriesDto(
    // // This watchlistID is returned by AddWatchListEntryResponse
    @Json(name = "watchlistID")
    val addedWatchlistId: String?,
    // this is the watchListID returned by WatchlistEntriesResponse
    @Json(name = "watchListID")
    val watchlistId: String?,
    @Json(name = "delayedorrtq")
    val delayedOrRtq: Int?,
    @Json(name = "entries")
    val entries: List<WatchlistEntryDto>? = null,
    @Json(name = "newIndexIDs")
    val newIndexIds: List<Int>? = null,
    @Json(name = "pagination")
    val pagination: WatchlistPagination? = null,
    @Json(name = "totalPostitionsCount")
    val totalPositionsCount: Long? = null
)
