package com.etrade.mobilepro.chat.data.setup.dto.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 *
 *  Request params used in Chat setup api
 *
 */

@JsonClass(generateAdapter = true)
data class ChatSetupRequestBodyDto(
    @Json(name = "value")
    val value: ChatInParams? = null
)

@JsonClass(generateAdapter = true)
data class ChatInParams(
    @Json(name = "ipAddress")
    val ipAddress: String? = null
)
