package com.etrade.mobilepro.dagger

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.transfermoney.presentation.TransferMoneyContainerViewModel
import com.etrade.mobilepro.transfermoney.presentation.activity.TransferActivityViewModel
import com.etrade.mobilepro.transfermoney.presentation.confirmation.TransferMoneyConfirmationViewModel
import com.etrade.mobilepro.transfermoney.presentation.contributiondetail.TransferIraContributionDetailViewModel
import com.etrade.mobilepro.transfermoney.presentation.detail.TransferActivityDetailViewModel
import com.etrade.mobilepro.transfermoney.presentation.external.ExternalAccountVerificationMethodActionSheetViewModel
import com.etrade.mobilepro.transfermoney.presentation.preview.TransferMoneyPreviewViewModel
import com.etrade.mobilepro.transfermoney.presentation.transfer.TransferViewModelFactoryFactory
import com.etrade.mobilepro.transfermoney.presentation.transfer.TransferViewModelFactoryFactoryImpl
import com.etrade.mobilepro.transfermoney.widget.assisted.TransferMoneyCalendarViewModelFactoryFactory
import com.etrade.mobilepro.transfermoney.widget.assisted.TransferMoneyCalendarViewModelFactoryFactoryImpl
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface TransferMoneyViewModelModule {

    @Binds
    fun bindTransferMoneyCalendarViewModelFactoryFactory(
        factory: TransferMoneyCalendarViewModelFactoryFactoryImpl
    ): TransferMoneyCalendarViewModelFactoryFactory

    @Binds
    fun bindTransferViewModelFactoryFactory(factory: TransferViewModelFactoryFactoryImpl): TransferViewModelFactoryFactory

    @Binds
    @IntoMap
    @ViewModelKey(TransferActivityViewModel::class)
    fun bindTransferActivityViewModel(viewModel: TransferActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransferIraContributionDetailViewModel::class)
    fun bindTransferIraContributionDetailViewModel(viewModel: TransferIraContributionDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransferActivityDetailViewModel::class)
    fun bindTransferActivityDetailViewModel(viewModel: TransferActivityDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransferMoneyConfirmationViewModel::class)
    fun bindTransferMoneyConfirmationViewModel(viewModel: TransferMoneyConfirmationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransferMoneyContainerViewModel::class)
    fun bindTransferMoneyContainerViewModel(viewModel: TransferMoneyContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransferMoneyPreviewViewModel::class)
    fun bindTransferMoneyPreviewViewModel(viewModel: TransferMoneyPreviewViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExternalAccountVerificationMethodActionSheetViewModel::class)
    fun bindExternalAccountVerificationMethodActionSheetViewModel(viewModel: ExternalAccountVerificationMethodActionSheetViewModel): ViewModel
}
