package com.etrade.mobilepro.dynamic.form.ui

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.FrameLayout
import androidx.appcompat.content.res.AppCompatResources
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.api.TextInput
import com.etrade.mobilepro.dynamic.form.api.TextInputType
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.view.KeyboardVisibilityMonitor
import com.etrade.mobilepro.util.android.view.buildKeyboardVisibilityMonitor
import com.etrade.mobilepro.util.android.view.toggleStyle
import com.google.android.material.textfield.TextInputLayout

private const val ZERO_FRACTION_DIGITS = 0

internal class TextInputView : FrameLayout, ComponentView<TextInput>, WithInputId {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val inputViewDelegate: InputViewDelegate = InputViewDelegate(this, ::setTitle)

    private val isIconVisibleObserver: Observer<Boolean>
        get() = Observer { layout.isEndIconVisible = it }

    private val textInputTypeObserver: Observer<TextInputType> = Observer {
        editText.apply {
            when (it) {
                is TextInputType.Decimal -> {
                    inputType = EditorInfo.TYPE_CLASS_NUMBER or EditorInfo.TYPE_NUMBER_FLAG_DECIMAL
                    filters = arrayOf(PatternValueFilter(createDecimalPattern(it.maxFractionDigits)))
                }
                is TextInputType.CurrencyDecimal -> {
                    inputType = EditorInfo.TYPE_CLASS_NUMBER or EditorInfo.TYPE_NUMBER_FLAG_DECIMAL
                    filters = arrayOf(PatternValueFilter(createCurrencyDecimalPattern(it.maxFractionDigits)))
                }
                is TextInputType.Integer -> {
                    inputType = EditorInfo.TYPE_CLASS_NUMBER
                    filters = arrayOf(PatternValueFilter(createDecimalPattern(ZERO_FRACTION_DIGITS)))
                }
            }
        }
    }

    private val monitorKeyboardVisibilityRequestObserver = Observer<ConsumableLiveEvent<Boolean>>
    { signal ->
        if (signal.isConsumed) { return@Observer }
        val shouldMonitorKeyboardVisibility = signal.peekContent()

        if (shouldMonitorKeyboardVisibility) {
            keyboardVisibilityMonitor?.startMonitoring()
        } else {
            keyboardVisibilityMonitor?.stopMonitoring()
            // In order to get keyboard visibility updates even when the device is rotated, consume
            // this signal only if a consumer requested it.
            signal.consume()
        }
    }

    @Suppress("EmptyFunctionBlock")
    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(text: Editable?) {
            input?.setValue(text?.takeIf { it.isNotBlank() }?.toString())
        }

        override fun beforeTextChanged(text: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {}
    }

    private val valueObserver: Observer<CharSequence?> = Observer {
        editText.apply {
            if (text.toString() != it) {
                setText(it)
                setSelection(text.length)
            }
        }
    }

    private val layout: TextInputLayout
    private val editText: EditText

    private var input: TextInput? = null
    override val inputId: String?
        get() { return input?.id }
    private var lifecycleOwner: LifecycleOwner? = null
    private var keyboardVisibilityMonitor: KeyboardVisibilityMonitor? = null

    init {
        inflate(R.layout.dynamic_form_view_text_input, true)
        layout = getChildAt(0) as TextInputLayout
        editText = layout.editText!!

        editText.setOnFocusChangeListener { _, hasFocus ->
            val owner = lifecycleOwner ?: return@setOnFocusChangeListener
            input?.apply {
                if (hasFocus) {
                    displayValue.removeObserver(valueObserver)
                    value.observe(owner, valueObserver)
                    editText.addTextChangedListener(textWatcher)
                } else {
                    editText.removeTextChangedListener(textWatcher)
                    value.removeObserver(valueObserver)
                    displayValue.observe(owner, valueObserver)
                }
            }
        }
    }

    override fun render(component: TextInput, owner: LifecycleOwner) {
        inputViewDelegate.render(component, owner)

        input?.apply {
            displayValue.removeObserver(valueObserver)
            textInputType.removeObserver(textInputTypeObserver)
            isIconVisible.removeObserver(isIconVisibleObserver)
            monitorKeyboardVisibilitySignal.removeObserver(monitorKeyboardVisibilityRequestObserver)
            keyboardVisibilityMonitor?.stopMonitoring()
        }

        input = component
        lifecycleOwner = owner
        initKeyboardVisibilityMonitor(owner)

        component.textInputIcon?.let { textInputIcon ->
            layout.apply {
                setEndIconDrawable(textInputIcon.iconResId)
                endIconMode = TextInputLayout.END_ICON_CUSTOM
                setEndIconTintList(AppCompatResources.getColorStateList(context, R.color.purple))
                setEndIconOnClickListener { textInputIcon.action.invoke() }
                endIconContentDescription = textInputIcon.contentDescription
            }
        }

        component.apply {
            displayValue.observe(owner, valueObserver)
            textInputType.observe(owner, textInputTypeObserver)
            isIconVisible.observe(owner, isIconVisibleObserver)
            monitorKeyboardVisibilitySignal.observe(owner, monitorKeyboardVisibilityRequestObserver)
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        layout.toggleStyle(enabled)
    }

    private fun setTitle(title: CharSequence?) {
        layout.hint = title
    }

    private fun initKeyboardVisibilityMonitor(owner: LifecycleOwner) {
        keyboardVisibilityMonitor = buildKeyboardVisibilityMonitor {
            targetView = this@TextInputView
            lifecycleOwner = owner
            notifyInitialVisibility = false
            listener = {
                input?.notifyKeyboardVisibility(it)
            }
        }
    }
}
