package com.etrade.mobilepro.bloombergtv.widget

import android.graphics.SurfaceTexture
import android.media.MediaPlayer
import android.view.TextureView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class PlayerFactory(private val tvUrl: String, private val textureView: TextureView, private val scope: CoroutineScope) {

    fun createMediaPlayer(thumbnailFactoryCreator: (MediaPlayer) -> ThumbnailFactory, onError: () -> Unit): MediaPlayer {
        val player = MediaPlayer()
        player.setOnPreparedListener { mp ->
            val thumbnailFactory = thumbnailFactoryCreator(mp)
            if (textureView.isAvailable) {
                scope.launch { thumbnailFactory.createThumbnail(textureView.surfaceTexture, textureView.width, textureView.height) }
                return@setOnPreparedListener
            }
            textureView.surfaceTextureListener = object : TextureView.SurfaceTextureListener {
                override fun onSurfaceTextureAvailable(
                    surfaceTexture: SurfaceTexture,
                    width: Int,
                    height: Int
                ) {
                    scope.launch { thumbnailFactory.createThumbnail(surfaceTexture, width, height) }
                    surfaceTexture.release()
                    textureView.surfaceTextureListener = null
                }

                override fun onSurfaceTextureDestroyed(surfaceTexture: SurfaceTexture): Boolean {
                    surfaceTexture.release()
                    textureView.surfaceTextureListener = null
                    return true
                }

                override fun onSurfaceTextureUpdated(surface: SurfaceTexture) = Unit
                override fun onSurfaceTextureSizeChanged(
                    surface: SurfaceTexture,
                    width: Int,
                    height: Int
                ) = Unit
            }
        }
        player.setOnErrorListener { mp, _, _ ->
            onError()
            mp.reset()
            true
        }
        player.setDataSource(tvUrl)
        return player
    }
}
