package com.etrade.mobilepro.dropdown.expandableselect.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.dropdown.R
import com.etrade.mobilepro.dropdown.databinding.BottomSheetSelectorRowBinding
import com.etrade.mobilepro.dropdown.expandableselect.DropDownExpandableSelector
import com.etrade.mobilepro.util.android.extension.dpToPx

private const val HALF_OPACITY = 0.5f
private const val FULL_OPACITY = 1f
private const val DEFAULT_LEVEL_PADDING_DP = 16

internal class ExpandableViewHolder<T>(
    private val binding: BottomSheetSelectorRowBinding,
    private val listener: (Int, DropDownExpandableSelector.Selectable<T>) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    private var item: DropDownExpandableSelector.Selectable<T>? = null

    init {
        val clickListener = View.OnClickListener {
            this.item?.let { item ->
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION && item.enabled) {
                    listener.invoke(position, item)
                }
            }
        }

        binding.selectorRowText.setOnClickListener(clickListener)
        binding.selectorRowDescription.setOnClickListener(clickListener)
    }

    fun bindTo(item: DropDownExpandableSelector.Selectable<T>) {
        this.item = item

        binding.selectorRowText.bindSelectorRowText(item)
        binding.selectorRowDescription.bindSelectorRowDescription(item)

        handleEnabledState(item)
        handleLevel(item.level)
        handleGhostText(item.ghostText)
    }

    private fun TextView.bindSelectorRowText(item: DropDownExpandableSelector.Selectable<T>) {
        text = if (item.titleFormatter != null) {
            item.titleFormatter.invoke(item.value)
        } else {
            item.title
        }

        contentDescription =
            if (item.contentDescriptionFormatter != null) {
                item.contentDescriptionFormatter.invoke(item.value)
            } else {
                item.title
            }

        if (item.titleColor != null) {
            setTextColor(item.titleColor)
        }
    }

    private fun TextView.bindSelectorRowDescription(item: DropDownExpandableSelector.Selectable<T>) {
        val description = item.descriptionFormatter?.invoke(item.value)
        if (description != null) {
            text = description
            visibility = View.VISIBLE
        } else {
            visibility = View.GONE
        }
    }

    private fun handleEnabledState(item: DropDownExpandableSelector.Selectable<T>) {
        if (item.enabled) {
            binding.selectorRowText.alpha = FULL_OPACITY

            if (item.descriptionFormatter != null) {
                handleTextDescriptionIcon(item)
            } else {
                handleTextRowIcon(item)
            }
        } else {
            binding.selectorRowText.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0)
            binding.selectorRowText.isSelected = false
            binding.selectorRowText.alpha = HALF_OPACITY
        }
        binding.selectorRowText.isEnabled = item.enabled
    }

    @Suppress("LongMethod") // nothing but setting ui icons
    private fun handleTextRowIcon(item: DropDownExpandableSelector.Selectable<T>) {
        if (item.selected) {
            binding.selectorRowText.isSelected = true
            when (item.state) {
                DropDownExpandableSelector.Selectable.State.NONE -> binding.selectorRowText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_tableview_checkmark_autocolor,
                    0
                )
                DropDownExpandableSelector.Selectable.State.COLLAPSED -> binding.selectorRowText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    R.drawable.ic_dropdown_arrow_auto_color,
                    0,
                    R.drawable.ic_tableview_checkmark_autocolor,
                    0
                )
                DropDownExpandableSelector.Selectable.State.EXPANDED -> binding.selectorRowText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    R.drawable.ic_drop_up_arrow_auto_color,
                    0,
                    R.drawable.ic_tableview_checkmark_autocolor,
                    0
                )
            }
        } else {
            binding.selectorRowText.isSelected = false
            when (item.state) {
                DropDownExpandableSelector.Selectable.State.NONE -> binding.selectorRowText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    0,
                    0,
                    0,
                    0
                )
                DropDownExpandableSelector.Selectable.State.COLLAPSED -> binding.selectorRowText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    R.drawable.ic_dropdown_arrow_auto_color,
                    0,
                    0,
                    0
                )
                DropDownExpandableSelector.Selectable.State.EXPANDED -> binding.selectorRowText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    R.drawable.ic_drop_up_arrow_auto_color,
                    0,
                    0,
                    0
                )
            }
        }
    }

    private fun handleTextDescriptionIcon(item: DropDownExpandableSelector.Selectable<T>) {
        with(binding.selectorRowImageCheck) {
            if (item.selected) {
                setImageResource(R.drawable.ic_tableview_checkmark_autocolor)
            } else {
                setImageDrawable(null)
            }
        }
    }

    private fun handleLevel(level: Int) {
        val left = binding.root.context.dpToPx(DEFAULT_LEVEL_PADDING_DP * (level * 2 + 1))
        with(binding.selectorRowText) {
            setPadding(left, paddingTop, paddingRight, paddingBottom)
        }
        with(binding.selectorRowDescription) {
            setPadding(
                left, paddingTop, paddingRight, paddingBottom
            )
        }
    }

    private fun handleGhostText(ghostText: String?) {
        with(binding) {
            if (ghostText == null) {
                selectorRowGhostText.visibility = View.GONE
                selectorRowText.visibility = View.VISIBLE
                selectorRowThinDivider.visibility = View.VISIBLE
            } else {
                selectorRowGhostText.visibility = View.VISIBLE
                selectorRowText.visibility = View.GONE
                selectorRowDescription.visibility = View.GONE
                selectorRowThinDivider.visibility = View.GONE
            }
            selectorRowGhostText.text = ghostText
        }
    }
}
