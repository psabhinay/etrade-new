package com.etrade.mobilepro.timeout.state

/**
 * This data class represents a user session and contains the data that is received from the login service.
 *
 */

interface TimeoutState {

    /**
     * @property shouldShowSarLogoutDialog Indicates if SAR dialog should show
     */
    var shouldShowSarLogoutDialog: Boolean

    /**
     * @property isShowingSessionDialog Indicates if session related dialog is showing
     */
    var isShowingSessionDialog: Boolean

    var shouldShowBiometricAuthentication: Boolean

    /**
     * @fun isDialogPresent Indicates if timeout dialog is showing or should show
     */
    fun isDialogPresent(): Boolean
}
