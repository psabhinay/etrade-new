package com.etrade.mobilepro.optionschain.tableview

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.etrade.eo.core.util.forEachNotNull
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.instrument.ExpirationDate
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.formatExpirationDate
import com.etrade.mobilepro.instrument.getExpirationDateFromOsiKey
import com.etrade.mobilepro.instrument.isSymbolLongOption
import com.etrade.mobilepro.instrument.toLocalDate
import com.etrade.mobilepro.livedata.concatCombine
import com.etrade.mobilepro.optionschains.api.Option
import com.etrade.mobilepro.optionschains.api.OptionsChain
import com.etrade.mobilepro.optionschains.api.OptionsChainException
import com.etrade.mobilepro.optionschains.api.OptionsChainRepo
import com.etrade.mobilepro.optionschains.api.OptionsPair
import com.etrade.mobilepro.optionschains.api.id
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.DateTimeFormatterBuilder
import javax.inject.Inject

class OptionChainTableViewModel @Inject constructor(
    private val optionsChainRepo: OptionsChainRepo,
    private val resources: Resources,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController
) : ViewModel() {

    private var optionsChainJob: Job? = null
    val errorMessage: LiveData<ConsumableLiveEvent<String>>
        get() = _errorMessage
    val loadingIndicator: LiveData<Boolean>
        get() = _loadingIndicator
    val selectedExpirationDatePosition: LiveData<Int>
        get() = _selectedExpirationDatePosition

    private val _optionsChain: MutableLiveData<OptionsChain> = MutableLiveData()
    private val _optionPairs: LiveData<List<OptionsPair>> = _optionsChain.map { chain -> chain.optionPairs }
    private val _selectedOptions: MutableLiveData<List<String>> = MutableLiveData(emptyList())

    val expirationDates: LiveData<List<String>> = _optionsChain.map { chain -> chain.expirationDates.map { it.printDate() } }

    var initialSelectedExpirationDate: ExpirationDate? = null
        set(value) {
            val currentExpirationDate = field
            if (currentExpirationDate == null) {
                field = value
            } else if (currentExpirationDate < value) {
                field = value
            }
        }

    val optionPairs: LiveData<List<SelectedOptionsPair>> = _optionPairs.concatCombine(_selectedOptions) { pairs, selectedOptions ->
        pairs.map { pair ->
            SelectedOptionsPair(
                put = pair.put.toSelectedOption(selectedOptions),
                call = pair.call.toSelectedOption(selectedOptions)
            )
        }
    }

    private val optionMap: LiveData<Map<String, Option>> = _optionPairs.map { it.optionMap }

    val selectedOptions: LiveData<List<Option?>> = optionMap.concatCombine(_selectedOptions) { options, selectedOptions ->
        invalidateCache(selectedOptions)
        selectedOptions.map { id ->
            options[id] // get latest option for id
                ?.also { selectedOptionsCache[id] = it } // cache latest option instance
                ?: selectedOptionsCache[id] // fall back to cached option if now latest version
        }
    }

    fun getSelectedOptionIdWithIndex(index: Int): String? = _selectedOptions.value?.getOrNull(index)

    private val selectedOptionsCache: MutableMap<String, Option> = mutableMapOf()

    private val _errorMessage: MutableLiveData<ConsumableLiveEvent<String>> = MutableLiveData()
    private val _loadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)
    private val _selectedExpirationDatePosition: MutableLiveData<Int> = MutableLiveData()

    private val formatter: DateTimeFormatter = DateTimeFormatterBuilder().appendPattern("MMM dd yyyy").toFormatter()
    private val logger: Logger = LoggerFactory.getLogger(OptionChainTableViewModel::class.java)

    private var previousSymbol: String? = null

    private val columns: Set<Level1Field> = setOf(
        Level1Field.ASK,
        Level1Field.ASK_SIZE,
        Level1Field.BID,
        Level1Field.BID_SIZE,
        Level1Field.CHANGE,
        Level1Field.LAST_PRICE,
        Level1Field.OPEN_INTEREST,
        Level1Field.TICK_INDICATOR,
        Level1Field.VOLUME
    )

    private val compositeDisposable = CompositeDisposable()

    private var cachedIntRange: IntRange = IntRange.EMPTY

    fun selectOptionWithIdAndTerm(id: String, isAMOption: Boolean) {
        require(isSymbolLongOption(id)) {
            "Only options are allowed."
        }

        updateSelectedOption {
            add(id)
        }
        initialSelectedExpirationDate = getExpirationDateFromOsiKey(id, isAMOption)
    }

    fun deselectOption(index: Int) {
        updateSelectedOption {
            removeAt(index)
        }
    }

    fun loadOptionsChain(symbol: String) {
        if (symbol != previousSymbol) {
            previousSymbol = symbol
            refresh()
        }
    }

    fun refresh() {
        val symbol = previousSymbol ?: return
        optionsChainJob?.cancel()
        optionsChainJob = viewModelScope.launch {
            _loadingIndicator.value = true
            val selectedExpirationDate = _optionsChain.value?.expirationDates?.getOrNull(
                selectedExpirationDatePosition.value ?: 0
            ) ?: initialSelectedExpirationDate
            optionsChainRepo.getOptionsChain(symbol, selectedExpirationDate)
                .onSuccess {
                    handleOptionChainsGetSuccess(it, selectedExpirationDate)
                }
                .onFailure {
                    handleOptionChainsGetError(symbol, it)
                }
            _loadingIndicator.value = false
        }
    }

    private fun CoroutineScope.handleOptionChainsGetError(symbol: String, it: Throwable) {
        if (isActive) {
            logger.error("Unable to load options chain for symbol: $symbol", it)
            val generalErrorMessage = resources.getString(R.string.error_message_general)
            if (it is OptionsChainException) {
                _errorMessage.value = ConsumableLiveEvent(it.errorMessage ?: generalErrorMessage)
                _optionsChain.value = it.optionsChain ?: OptionsChain(emptyList(), emptyList())
            } else {
                _errorMessage.value = ConsumableLiveEvent(generalErrorMessage)
                _optionsChain.value?.let { previousChain ->
                    _optionsChain.value = OptionsChain(previousChain.expirationDates, emptyList())
                }
            }
        }
    }

    private fun handleOptionChainsGetSuccess(
        it: OptionsChain,
        selectedExpirationDate: ExpirationDate?
    ) {
        _optionsChain.value = it
        var currentExpDate = it.expirationDates.indexOf(selectedExpirationDate)
        if (currentExpDate == -1) {
            currentExpDate = 0
        }
        if (_selectedExpirationDatePosition.value != currentExpDate) {
            _selectedExpirationDatePosition.value = currentExpDate
        }
    }

    fun stopStreaming() {
        compositeDisposable.clear()
    }

    fun resumeStreaming() {
        updateVisiblePositions(cachedIntRange, true)
    }

    fun updateVisiblePositions(intRange: IntRange, forceUpdate: Boolean = false) {
        if (streamingStatusController.isStreamingToggleEnabled && (forceUpdate || intRange != cachedIntRange)) {
            cachedIntRange = intRange

            compositeDisposable.clear()

            val optionsToStream: MutableMap<String, Option> = mutableMapOf()
            selectedOptions.value?.forEachNotNull {
                optionsToStream += it
            }

            addOptionsOnRange(cachedIntRange, optionsToStream)

            optionsToStream.values.forEach {
                stream(it)
            }
        }
    }

    private fun addOptionsOnRange(intRange: IntRange, optionsToStream: MutableMap<String, Option>) {
        if (_optionPairs.value?.isNullOrEmpty() == true) {
            logger.warn("There are not options to compare")
            return
        }
        if (intRange.none { it < 0 }) {
            _optionPairs.value?.slice(intRange)?.forEach { pair ->
                optionsToStream += pair.call
                optionsToStream += pair.put
            }
        }
    }

    fun updateSelectedExpirationDatePosition(position: Int) {
        _selectedExpirationDatePosition.value = position
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    private fun updateSelectedOption(operator: MutableList<String>.() -> Unit) {
        _selectedOptions.value = _selectedOptions.value
            .orEmpty()
            .toMutableList()
            .apply { operator() }
            .toList()
    }

    private fun stream(option: Option) {
        compositeDisposable.add(
            streamingQuoteProvider.getQuote(option.symbol, InstrumentType.OPTN, columns).subscribe {
                val optionId = option.id
                (_optionPairs.value?.optionMap?.get(optionId) ?: selectedOptionsCache[optionId])
                    ?.updateItem(it)
            }
        )
    }

    private fun invalidateCache(selectedOptions: Collection<String>) {
        (selectedOptionsCache.keys - selectedOptions.toHashSet()).forEach { selectedOptionsCache.remove(it) }
    }

    private fun ExpirationDate.printDate(): String = settlementSessionCode.formatExpirationDate(formatter.format(toLocalDate()))

    private fun Option.toSelectedOption(selectedOptions: Collection<String>): SelectedOption {
        return SelectedOption(this, selectedOptions.contains(id))
    }

    private val Iterable<OptionsPair>.optionMap: Map<String, Option>
        get() {
            val result = mutableMapOf<String, Option>()
            forEach { pair ->
                result += pair.call
                result += pair.put
            }
            return result.toMap()
        }

    private operator fun MutableMap<String, Option>.plusAssign(option: Option) {
        this[option.id] = option
    }
}
