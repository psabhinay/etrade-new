package com.etrade.mobilepro.dynamic.form.api

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.MutableInputValuesSnapshot

interface InputValueCollector {

    /**
     * Collects values from this holder into [snapshot] map.
     *
     * @param snapshot The destination to collect values to.
     */
    fun collectValuesTo(snapshot: MutableInputValuesSnapshot)
}

val InputValueCollector.inputValuesSnapshot: InputValuesSnapshot
    get() {
        return mutableMapOf<String, Any?>()
            .apply(::collectValuesTo)
            .toMap()
    }
