package com.etrade.mobilepro.markets.overview

import android.app.Activity
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.getOverlayIntentForUrlAction
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.databinding.FragmentMarketsOverviewBinding
import com.etrade.mobilepro.dynamicui.GenericRecyclerAdapter
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.toDynamicViewModels
import com.etrade.mobilepro.indices.WIDGET_TAG_INDICES
import com.etrade.mobilepro.markets.edit.EditMarketOverviewActivity
import com.etrade.mobilepro.markets.footer.MarketsOverviewFooterViewModel
import com.etrade.mobilepro.navigation.EtNavController
import com.etrade.mobilepro.searchingapi.items.EXTRA_WIDGET_TAG
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.settings.OVERLAY
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.tableviewutils.presentation.adapter.HasTableViewAdapter
import com.etrade.mobilepro.tableviewutils.presentation.adapter.SymbolTableViewAdapter
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.TimestampEventProvider
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.extension.withDrawable
import com.etrade.mobilepro.util.android.goneUnless
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import com.etrade.mobilepro.util.android.recyclerviewutil.RecyclerViewItemDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

internal class MarketsOverviewFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val userViewModel: UserViewModel,
    private val mainNavigation: MainNavigation,
    private val marketTradeStatusDelegate: MarketTradeStatusDelegate,
    private val connectionNotifier: ConnectionNotifier
) : Fragment(R.layout.fragment_markets_overview) {

    private val binding by viewBinding(FragmentMarketsOverviewBinding::bind)

    protected val logger: Logger = LoggerFactory.getLogger(this.javaClass)
    private val etNavController: EtNavController by lazy {
        EtNavController(
            findNavController(),
            userViewModel
        )
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private val customizeWidgetsRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                binding.marketOverviewRecyclerView.smoothScrollToPosition(0)
                fetchValues()
            }
        }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private val marketOverviewViewModel: MarketsOverviewViewModel by viewModels { viewModelFactory }
    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private val viewAdapter: GenericRecyclerAdapter
        get() = binding.marketOverviewRecyclerView.adapter as GenericRecyclerAdapter
    private var snackBar: Snackbar? = null

    @FlowPreview
    @ExperimentalCoroutinesApi
    private val clickObserver: Observer<CtaEvent> by lazy {
        Observer<CtaEvent> { ctaEvent ->
            when (ctaEvent) {
                is CtaEvent.ActionEvent -> {
                    val url = ctaEvent.url

                    if (url.contains(OVERLAY)) {
                        activity?.let {
                            startActivity(getOverlayIntentForUrlAction(url, ctaEvent.title, it))
                        }
                    } else {
                        findNavController().navigate(Uri.parse(url))
                    }
                }
                is CtaEvent.UriEvent -> {
                    findNavController().navigate(ctaEvent.uri)
                }
                is CtaEvent.NavDirectionsNavigation -> {
                    findNavController().navigate(ctaEvent.ctaDirections)
                }
                is CtaEvent.LaunchQuote -> {
                    mainNavigation.navigateToQuoteDetails(activity, ctaEvent.bundle)

                    if (ctaEvent.bundle.getString(EXTRA_WIDGET_TAG) == WIDGET_TAG_INDICES) {
                        marketOverviewViewModel.trackIndices()
                    }
                }
                is CtaEvent.CustomizeEvent -> {
                    marketOverviewViewModel.trackCustomize()
                    val intent = EditMarketOverviewActivity.intent(requireContext()) {
                        isDeletableItems = false
                        isToggleableItems = true
                        listFooterMessage = R.string.edit_list_footer_message
                    }

                    customizeWidgetsRequest.launch(intent)
                }
                is CtaEvent.BloombergTvEvent -> {
                    marketOverviewViewModel.trackBloombergTv()
                    etNavController.navigate(MainGraphDirections.actionLaunchBloombergtv())
                }
            }
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
        binding.swipeRefreshView.setOnRefreshListener {
            if (connectionNotifier.connected) {
                fetchValues()
            } else {
                binding.swipeRefreshView.isRefreshing = false
            }
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onPause() {
        super.onPause()
        removeLifecycleObservers()
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun removeLifecycleObservers() {
        marketOverviewViewModel.lifecycleObserverSet.forEach {
            lifecycle.removeObserver(it)
        }
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    override fun onResume() {
        super.onResume()
        marketOverviewViewModel.lifecycleObserverSet.forEach {
            lifecycle.addObserver(it)
        }
    }

    private fun setupFragment() {
        binding.marketOverviewRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = GenericRecyclerAdapter(this@MarketsOverviewFragment, emptyList())
            setupItemDecoration()
        }
    }

    private fun RecyclerView.setupItemDecoration() {
        val excludeIndexList = if (userViewModel.isAuthenticated) {
            withDrawable(context, R.drawable.thin_divider) {
                addItemDecoration(RecyclerViewItemDivider(it), 0)
            }
            listOf(0)
        } else {
            null
        }
        setupRecyclerViewDivider(R.drawable.thick_divider, excludeIndexList = excludeIndexList)
    }

    @Suppress("LongMethod")
    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun bindViewModel() {
        marketOverviewViewModel.getViewState().observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is MarketsOverviewViewModel.ViewState.Loading -> {
                        if (!binding.swipeRefreshView.isRefreshing) {
                            binding.loadingIndicator.show()
                        }
                        snackBar?.dismiss()
                    }
                    is MarketsOverviewViewModel.ViewState.Error -> {
                        binding.loadingIndicator.hide()
                        binding.swipeRefreshView.isRefreshing = false
                        snackBar = snackbarUtil.retrySnackbar { fetchValues() }
                    }
                    is MarketsOverviewViewModel.ViewState.Success -> {
                        onSuccessViewState(it)
                    }
                }
            }
        )
        userViewModel.userSessionState.observe(
            viewLifecycleOwner,
            Observer {
                marketOverviewViewModel.loadMarketOverview(it.peekContent())
            }
        )
        marketTradeStatusDelegate.marketStatus.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { status ->
                    binding.marketHaltBanner.goneUnless(status.isMarketHalted)
                }
            }
        )
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun fetchValues() {
        marketOverviewViewModel.shouldRefresh = true
        marketOverviewViewModel.loadMarketOverview(userViewModel.peekUserSessionState ?: SessionStateChange.UNAUTHENTICATED)
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun onSuccessViewState(it: MarketsOverviewViewModel.ViewState.Success) {
        var footerViewModel: MarketsOverviewFooterViewModel? = null
        val timestampEventProviders = mutableListOf<TimestampEventProvider>()
        viewAdapter.values = it.result.toDynamicViewModels(fragment = this, shouldRefresh = marketOverviewViewModel.shouldRefresh) { model ->
            if (model is HasTableViewAdapter) {
                activity?.let {
                    model.setTableViewAdapter(
                        SymbolTableViewAdapter(
                            it,
                            true
                        )
                    )
                }
            }
            if (model is LifecycleObserver && !marketOverviewViewModel.lifecycleObserverSet.contains(model)) {
                lifecycle.addObserver(model)
                marketOverviewViewModel.lifecycleObserverSet.add(model)
            }
            if (model is MarketsOverviewFooterViewModel) {
                footerViewModel = model
            }
            if (model is TimestampEventProvider) {
                timestampEventProviders.add(model)
            }
            model.clickEvents.observe(viewLifecycleOwner, clickObserver)
        }
        footerViewModel?.addTimestampEventProviders(timestampEventProviders)
        binding.loadingIndicator.hide()
        snackBar?.dismiss()
        binding.swipeRefreshView.isRefreshing = false
        marketOverviewViewModel.shouldRefresh = false
    }
}
