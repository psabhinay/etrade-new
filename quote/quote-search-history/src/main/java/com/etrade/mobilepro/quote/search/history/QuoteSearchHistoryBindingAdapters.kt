package com.etrade.mobilepro.quote.search.history

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.search.presentation.OnSearchResultItemClickListener
import com.etrade.mobilepro.quote.search.presentation.SearchResultsAdapter
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider

@BindingAdapter("quoteSearchHistoryItems")
internal fun RecyclerView.historyItems(items: List<SearchResultItem>?) = ensureAdapter().updateData(items.orEmpty())

@BindingAdapter("onSearchResultItemClickListener")
internal fun RecyclerView.onSearchResultItemClickListener(onSearchResultItemClickListener: OnSearchResultItemClickListener?) {
    ensureAdapter().onSearchResultItemClickListener = onSearchResultItemClickListener
}

private fun RecyclerView.ensureAdapter(): SearchResultsAdapter {
    return adapter as? SearchResultsAdapter ?: SearchResultsAdapter().also {
        setupRecyclerViewDivider(R.drawable.thin_divider)
        adapter = it
    }
}
