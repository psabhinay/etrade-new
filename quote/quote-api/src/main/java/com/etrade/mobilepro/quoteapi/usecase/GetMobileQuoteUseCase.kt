package com.etrade.mobilepro.quoteapi.usecase

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.quoteapi.GetQuoteParameter
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.util.UseCase

interface GetMobileQuoteUseCase : UseCase<GetQuoteParameter, ETResult<MobileQuote>>
