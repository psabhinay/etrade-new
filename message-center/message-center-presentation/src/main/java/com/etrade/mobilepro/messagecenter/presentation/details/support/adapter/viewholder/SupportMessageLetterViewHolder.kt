package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.messagecenter.presentation.R
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterItemSupportMessageLetterBinding
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.Letter
import com.etrade.mobilepro.util.android.binding.resources
import com.etrade.mobilepro.util.android.textutil.setTextAndLinkify

internal class SupportMessageLetterViewHolder(private val binding: MessageCenterItemSupportMessageLetterBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bindTo(item: Letter) {
        val resources = binding.resources
        val sentValueText = item.content.createdAtFormatted
        with(binding) {
            sentValue.text = sentValueText
            val refNumValueText = item.content.caseNo
            refNumValue.text = refNumValueText
            sentLayout.contentDescription =
                "${resources.getString(R.string.message_center_sent)} $sentValueText"
            refNumLayout.contentDescription =
                "${resources.getString(R.string.message_center_ref_number_colon)} $refNumValueText"
            content.setTextAndLinkify(item.content.message)
            divider.visibility = if (item.hasDivider) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
    }
}
