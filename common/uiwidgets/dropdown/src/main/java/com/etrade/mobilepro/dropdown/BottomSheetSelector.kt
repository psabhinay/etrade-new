package com.etrade.mobilepro.dropdown

import android.content.res.Resources
import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commitNow
import com.etrade.mobilepro.common.ToolbarActionEnd
import org.slf4j.LoggerFactory

class BottomSheetSelector<T>(
    private val supportFragmentManager: FragmentManager,
    private val resources: Resources,
    private val itemConverter: DropDownItemConverter<T> = CommonDropDownItemConverter(resources)::convert
) : DropDownManager<T> {

    override val view: View?
        get() = dialogFragment?.view

    private val logger = LoggerFactory.getLogger(BottomSheetSelector::class.java)

    private var dialogFragment: OverlayBottomSheetDialogFragment? = null
    private var tag = "BOTTOM_SHEET_SELECTOR_DIALOG_FRAGMENT"
    private var dropDownItems: List<T> = emptyList()

    override fun init(
        tag: String,
        title: String,
        items: List<T>,
        initialSelectedPosition: Int?,
        dividerPosition: Int?,
        toolbarActionEnd: ToolbarActionEnd?
    ) {
        init(tag, title, items, initialSelectedPosition, dividerPosition, null, toolbarActionEnd)
    }

    @Suppress("LongParameterList")
    fun init(
        tag: String,
        title: String,
        items: List<T>,
        initialSelectedPosition: Int?,
        dividerPosition: Int?,
        ghostText: String? = null,
        toolbarActionEnd: ToolbarActionEnd? = null
    ) {
        this.tag = tag
        if (dialogFragment == null) {
            dialogFragment = supportFragmentManager.findFragmentByTag(tag) as OverlayBottomSheetDialogFragment? ?: OverlayBottomSheetDialogFragment()
        }

        val dropDownItems: List<DropDownItem> = items.map(itemConverter)
        this.dropDownItems = items
        dialogFragment?.title = title
        dialogFragment?.items = dropDownItems
        dialogFragment?.selectedPosition = initialSelectedPosition
        dialogFragment?.dividerPosition = dividerPosition
        dialogFragment?.ghostText = ghostText
        dialogFragment?.toolbarActionEnd = toolbarActionEnd
    }

    override fun updateInitialSelectedPosition(initialSelectedPosition: Int?) {
        dialogFragment?.selectedPosition = initialSelectedPosition
    }

    override fun openDropDown() {
        if (supportFragmentManager.isDestroyed) {
            return
        }
        if (dialogFragment?.isAdded == false) {
            try {
                supportFragmentManager.findFragmentByTag(tag)?.let {
                    supportFragmentManager.commitNow {
                        remove(it)
                    }
                }
                dialogFragment?.showNow(supportFragmentManager, tag)
            } catch (e: IllegalStateException) {
                // See https://stackoverflow.com/questions/7575921/illegalstateexception-can-not-perform-this-action-after-onsaveinstancestate-wit
                logger.error("Could not show Watchlist dropdown: Commit error \n$e")
            }
        }
    }

    override fun closeDropDown() {
        dialogFragment
            ?.takeIf { it.isAdded }
            ?.also { it.dismiss() }
    }

    override fun setListener(listener: (position: Int, item: T) -> Unit) {
        dialogFragment?.listener = { position ->
            dropDownItems.getOrNull(position)?.let { item ->
                listener.invoke(position, item)
                dialogFragment?.selectedPosition = position
            }
        }
    }

    override fun setToolBarActionEnd(toolbarActionEnd: ToolbarActionEnd) {
        dialogFragment?.setToolBarActionEndText(toolbarActionEnd)
    }

    override fun isOpen(tag: String): Boolean = supportFragmentManager.findFragmentByTag(tag)?.isAdded == true

    fun setDismissAction(action: () -> Unit) {
        dialogFragment?.dismissAction = action
    }
}
