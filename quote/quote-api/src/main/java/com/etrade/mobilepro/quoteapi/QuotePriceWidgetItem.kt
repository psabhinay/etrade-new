package com.etrade.mobilepro.quoteapi

import com.etrade.mobilepro.util.safeParseBigDecimal

data class QuotePriceWidgetItem(
    val price: String,
    val priceColor: Int? = null,
    val exchangeCode: String? = null,
    val size: String? = null,
    val isNet: Boolean = false
) {
    fun getFormattedSize(): String = size?.safeParseBigDecimal()?.stripTrailingZeros()?.toPlainString() ?: ""

    fun formatQuotePriceContentDescription(formatter: String, price: String?): String {
        return String.format(formatter, price)
    }
}
