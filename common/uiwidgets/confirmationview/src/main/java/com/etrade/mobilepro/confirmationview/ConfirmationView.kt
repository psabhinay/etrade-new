package com.etrade.mobilepro.confirmationview

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.etrade.mobilepro.confirmationview.databinding.ConfirmationviewViewConfirmationBinding
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.util.android.extension.inflater

class ConfirmationView : ConstraintLayout {
    private var _binding: ConfirmationviewViewConfirmationBinding? = ConfirmationviewViewConfirmationBinding.inflate(inflater, this, true)
    val binding: ConfirmationviewViewConfirmationBinding
        get() = _binding!!

    constructor(context: Context) : super(context)

    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet)

    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    fun setAmountText(text: String) {
        binding.successView.setTitle(text)
    }

    fun setConfirmationText(text: String) {
        binding.confirmationText.text = text
    }

    fun setDetailsTable(sections: List<TableSectionItemLayout>) = binding.detailsTable.setContent(sections)

    fun setActionButton(text: String, onClickListener: OnClickListener) {
        binding.confirmationButton.text = text
        binding.confirmationButton.setOnClickListener(onClickListener)
    }

    fun playAnimation() {
        binding.successView.playAnimation()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        _binding = null
    }
}
