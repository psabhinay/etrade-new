package com.etrade.mobilepro.feature

import kotlinx.coroutines.flow.Flow

interface FeatureFlagRepo {
    fun isFeatureEnabled(feature: Feature): Flow<Boolean>
}
