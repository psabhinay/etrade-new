package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.markets.DefaultMarketCalendarRouter
import com.etrade.mobilepro.marketscalendar.MarketCalendarRouter
import com.etrade.mobilepro.marketscalendar.api.repo.MarketsCalendarRepo
import com.etrade.mobilepro.marketscalendar.data.repo.DefaultMarketsCalendarRepo
import com.etrade.mobilepro.marketscalendar.data.rest.MarketsCalendarApiService
import com.etrade.mobilepro.marketscalendar.viewmodel.DefaultSharedCalendarState
import com.etrade.mobilepro.marketscalendar.viewmodel.SharedCalendarState
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class MarketsCalendarModule {

    @Provides
    @Singleton
    internal fun provideMarketsCalendarApiService(@MobileTrade retrofit: Retrofit): MarketsCalendarApiService =
        retrofit.create(MarketsCalendarApiService::class.java)

    @Provides
    internal fun bindMarketCalendarRouter(router: DefaultMarketCalendarRouter): MarketCalendarRouter = router

    @Provides
    @Singleton // Per https://jira.corp.etradegrp.com/browse/ETAND-16112 market calendar tabs should share calendar state. Remove @Singleton to disable it.
    internal fun bindMarketCalendarSharedState(state: DefaultSharedCalendarState): SharedCalendarState = state

    @Provides
    @Singleton
    internal fun bindMarketsCalendarRepo(defaultMarketsCalendarRepo: DefaultMarketsCalendarRepo): MarketsCalendarRepo = defaultMarketsCalendarRepo
}
