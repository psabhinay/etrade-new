package com.etrade.mobilepro.trade.presentation.util

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.backends.api.FRONT_LOAD_MF_BREAKPOINT_WARNING
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.order.details.router.OrderConfirmationAction
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.order.details.router.PreviewMutualFundOrderDetailsArgument
import com.etrade.mobilepro.order.details.router.PreviewOrderDetailsArgument
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.presentation.handleServerMessages
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrderResponse
import com.etrade.mobilepro.trade.api.symbols
import com.etrade.mobilepro.util.android.textutil.TextUtil

class TradePreviewViewModelDelegate(
    private val resources: Resources,
    private val baseUrl: String,
    private val dialog: MutableLiveData<AppDialog?>,
    private val reSubmitMfOrder: (() -> Unit)? = null,
    private val openPreview: (OrderDetailsArgument) -> Unit
) {

    fun handlePreview(order: TradeOrder, confirmationAction: OrderConfirmationAction = OrderConfirmationAction.VIEW_PORTFOLIO) {
        if (order.hasFrontLoadMfBreakpointWarning()) {
            dialog.value = getFrontLoadMfBreakpointWarningDialog()
        } else {
            handleServerMessages(resources, baseUrl, order.response.warnings, dialog, emptyMap()) {
                openPreview(order, confirmationAction)
            }
        }
    }

    private fun openPreview(order: TradeOrder, confirmationAction: OrderConfirmationAction) {
        val argument: OrderDetailsArgument = when (order.response) {
            is MutualFundsOrderResponse -> PreviewMutualFundOrderDetailsArgument(
                orderId = order.orderId,
                orderStatus = OrderStatus.UNKNOWN,
                symbols = order.response.symbols,
                orderConfirmationAction = confirmationAction
            )
            else -> PreviewOrderDetailsArgument(
                orderId = order.orderId,
                orderStatus = OrderStatus.UNKNOWN,
                symbols = order.response.symbols,
                orderConfirmationAction = confirmationAction
            )
        }
        openPreview(argument)
    }

    private fun TradeOrder.hasFrontLoadMfBreakpointWarning() =
        this.response is MutualFundsOrderResponse &&
            this.response.warnings.firstOrNull()?.code == FRONT_LOAD_MF_BREAKPOINT_WARNING

    private fun getFrontLoadMfBreakpointWarningDialog(): AppDialog {
        return appDialog {
            val onDialogDismissed = {
                dialog.value = null
            }
            title = resources.getString(R.string.trade_front_load_breakpoint_warning_title)
            message = TextUtil.convertTextWithHmlTagsToSpannable(resources.getString(R.string.trade_front_load_breakpoint_warning_content))
            positiveAction {
                label = resources.getString(R.string.trade_front_load_breakpoint_warning_button_label)
                action = {
                    onDialogDismissed()
                    reSubmitMfOrder?.invoke()
                }
            }
            negativeAction {
                label = resources.getString(R.string.cancel)
                action = onDialogDismissed
            }
        }
    }
}
