package com.etrade.mobilepro.screener.api.filter.stock

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class EarningsDividendsFilter(
    val epsGrowthAnnual0to5: Boolean = false,
    val epsGrowthAnnual5to10: Boolean = false,
    val epsGrowthAnnual10to15: Boolean = false,
    val epsGrowthAnnual15to25: Boolean = false,
    val epsGrowthAnnual25to50: Boolean = false,
    val epsGrowthAnnualGreaterThan50: Boolean = false,
    val epsGrowthAnnualPositiveChange: Boolean = false,
    val epsGrowthAnnualNegativeChange: Boolean = false,

    val epsGrowthAnnualForCurrentFiscalYear: Boolean = false,
    val epsGrowthAnnualForNextFiscalYear: Boolean = false,

    val revenueGrowthAnnual0to5: Boolean = false,
    val revenueGrowthAnnual5to10: Boolean = false,
    val revenueGrowthAnnual10to15: Boolean = false,
    val revenueGrowthAnnual15to25: Boolean = false,
    val revenueGrowthAnnual25to50: Boolean = false,
    val revenueGrowthAnnualGreaterThan50: Boolean = false,

    val revenueGrowthAnnualPositiveChange: Boolean = false,
    val revenueGrowthAnnualNegativeChange: Boolean = false,

    val revenueGrowthAnnualForCurrentFiscalYear: Boolean = false,
    val revenueGrowthAnnualForNextFiscalYear: Boolean = false,

    val epsGrowthQuarterly0to5: Boolean = false,
    val epsGrowthQuarterly5to10: Boolean = false,
    val epsGrowthQuarterly10to15: Boolean = false,
    val epsGrowthQuarterly15to25: Boolean = false,
    val epsGrowthQuarterly25to50: Boolean = false,
    val epsGrowthQuarterlyGreaterThan50: Boolean = false,

    val epsGrowthQuarterlyPositiveChange: Boolean = false,
    val epsGrowthQuarterlyNegativeChange: Boolean = false,

    val revenueGrowthQuarterly0to5: Boolean = false,
    val revenueGrowthQuarterly5to10: Boolean = false,
    val revenueGrowthQuarterly10to15: Boolean = false,
    val revenueGrowthQuarterly15to25: Boolean = false,
    val revenueGrowthQuarterly25to50: Boolean = false,
    val revenueGrowthQuarterlyGreaterThan50: Boolean = false,

    val revenueGrowthQuarterlyPositiveChange: Boolean = false,
    val revenueGrowthQuarterlyNegativeChange: Boolean = false,

    val dividendYieldPositive: Boolean = false,
    val dividendYield0to2: Boolean = false,
    val dividendYield2to4: Boolean = false,
    val dividendYield4to6: Boolean = false,
    val dividendYield6to8: Boolean = false,
    val dividendYieldGreaterThan8: Boolean = false,

    val dividendYieldBelowIndustryAvg: Boolean = false,
    val dividendYieldAboveIndustryAvg: Boolean = false
) : ScreenerFilter {
    override fun isEmpty(): Boolean = isEpsGrowthAnnualEmpty() && isRevenueGrowthAnnualEmpty() && isEpsGrowthQuarterlyEmpty() &&
        isRevenueGrowthQuarterlyEmpty() && isDividendYieldEmpty()

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.STOCK

    private fun isEpsGrowthAnnualEmpty(): Boolean = !isEpsGrowthAnnualValuesSelected() &&
        !epsGrowthAnnualForCurrentFiscalYear && !epsGrowthAnnualForNextFiscalYear

    private fun isRevenueGrowthAnnualEmpty(): Boolean = !isRevenueGrowthAnnualValuesSelected() &&
        !revenueGrowthAnnualForCurrentFiscalYear && !revenueGrowthAnnualForNextFiscalYear

    private fun isEpsGrowthQuarterlyEmpty(): Boolean = !isEpsGrowthQuarterlyValuesSelected()

    private fun isRevenueGrowthQuarterlyEmpty(): Boolean = !isRevenueGrowthQuarterlyValuesSelected()

    private fun isDividendYieldEmpty(): Boolean = !isDividendYieldValuesSelected() &&
        !dividendYieldBelowIndustryAvg && !dividendYieldAboveIndustryAvg

    fun isEpsGrowthAnnualValuesSelected(): Boolean = epsGrowthAnnual0to5 ||
        epsGrowthAnnual5to10 ||
        epsGrowthAnnual10to15 ||
        epsGrowthAnnual15to25 ||
        epsGrowthAnnual25to50 ||
        epsGrowthAnnualGreaterThan50 ||
        epsGrowthAnnualPositiveChange ||
        epsGrowthAnnualNegativeChange

    fun isEpsGrowthAnnualPeriodSelected(): Boolean =
        epsGrowthAnnualForCurrentFiscalYear || epsGrowthAnnualForNextFiscalYear

    fun isEpsGrowthAnnualBlockEmpty(): Boolean = !isEpsGrowthAnnualValuesSelected() && !isEpsGrowthAnnualPeriodSelected()

    fun isRevenueGrowthAnnualValuesSelected(): Boolean = revenueGrowthAnnual0to5 ||
        revenueGrowthAnnual5to10 ||
        revenueGrowthAnnual10to15 ||
        revenueGrowthAnnual15to25 ||
        revenueGrowthAnnual25to50 ||
        revenueGrowthAnnualGreaterThan50 ||
        revenueGrowthAnnualPositiveChange ||
        revenueGrowthAnnualNegativeChange

    fun isRevenueGrowthAnnualPeriodSelected(): Boolean =
        revenueGrowthAnnualForCurrentFiscalYear || revenueGrowthAnnualForNextFiscalYear

    fun isRevenueGrowthAnnualBlockEmpty(): Boolean = !isRevenueGrowthAnnualValuesSelected() && !isRevenueGrowthAnnualPeriodSelected()

    fun isEpsGrowthQuarterlyValuesSelected(): Boolean = epsGrowthQuarterly0to5 ||
        epsGrowthQuarterly5to10 ||
        epsGrowthQuarterly10to15 ||
        epsGrowthQuarterly15to25 ||
        epsGrowthQuarterly25to50 ||
        epsGrowthQuarterlyGreaterThan50 ||
        epsGrowthQuarterlyPositiveChange ||
        epsGrowthQuarterlyNegativeChange

    fun isRevenueGrowthQuarterlyValuesSelected(): Boolean = revenueGrowthQuarterly0to5 ||
        revenueGrowthQuarterly5to10 ||
        revenueGrowthQuarterly10to15 ||
        revenueGrowthQuarterly15to25 ||
        revenueGrowthQuarterly25to50 ||
        revenueGrowthQuarterlyGreaterThan50 ||
        revenueGrowthQuarterlyPositiveChange ||
        revenueGrowthQuarterlyNegativeChange

    fun isDividendYieldValuesSelected(): Boolean = dividendYieldPositive ||
        dividendYield0to2 ||
        dividendYield2to4 ||
        dividendYield4to6 ||
        dividendYield6to8 ||
        dividendYieldGreaterThan8
}
