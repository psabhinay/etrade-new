package com.etrade.mobilepro.common.extension

import android.app.Activity
import android.content.res.Configuration
import android.graphics.Point
import android.graphics.Rect
import android.os.Build
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.View
import android.view.WindowInsets
import android.widget.EditText
import com.etrade.mobilepro.common.customview.SelfFocusHandlingView
import com.etrade.mobilepro.util.android.extension.clearFocusSafely
import com.etrade.mobilepro.util.android.extension.hideSoftKeyboard

val Activity.screenWidthPixels get() = getScreenSize().width()
val Activity.screenHeightPixels get() = getScreenSize().height()

/**
 * Clears focus from a view when a user touches a screen outside of the view's boundaries, hiding a keyboard if it is showing at the same time.
 *
 * @param event The touch screen event.
 */
fun Activity.clearFocusOnTouchOutside(event: MotionEvent) {
    if (event.action == MotionEvent.ACTION_DOWN) {
        val focusedView = currentFocus
        if (focusedView is EditText && focusedView !is SelfFocusHandlingView) {
            focusedView.clearFocusIfTouchedOutside(event)
        }
    }
}

private fun Activity.getScreenSize(): Rect =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        windowManager.currentWindowMetrics.bounds
    } else {
        with(DisplayMetrics()) {
            @Suppress("DEPRECATION")
            windowManager.defaultDisplay.getMetrics(this)
            Rect(0, 0, widthPixels, heightPixels)
        }
    }

@Suppress("DEPRECATION")
val Activity.getUsableScreenWidth: Int
    get() {
        val isLandscape = resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE
        return if (isLandscape) {
            // Samsung devices return incorrect insets
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && !Build.MANUFACTURER.equals(
                    "Samsung",
                    ignoreCase = true
                )
            ) {
                val navBarInset = windowManager.currentWindowMetrics.windowInsets
                    .getInsets(WindowInsets.Type.systemBars())
                screenWidthPixels - navBarInset.right - navBarInset.left
            } else {
                val appUsableSize = Point()
                windowManager.defaultDisplay?.apply { getSize(appUsableSize) }

                if (isLandscape) {
                    appUsableSize.x
                } else {
                    appUsableSize.y
                }
            }
        } else {
            screenWidthPixels
        }
    }

private fun View.clearFocusIfTouchedOutside(event: MotionEvent) {
    val outRect = Rect()
    getGlobalVisibleRect(outRect)
    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
        clearFocusSafely()
        hideSoftKeyboard()
    }
}
