package com.etrade.mobilepro.portfolio.taxlot

import com.etrade.mobilepro.portfolio.delegate.PositionSortDelegateFactory
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import dagger.assisted.AssistedFactory

@AssistedFactory
interface TaxLotSortDelegateFactory : PositionSortDelegateFactory {
    override fun create(
        portfolioTableViewType: PortfolioTableViewType,
        positionTableMode: PositionTableMode
    ): TaxLotSortDelegate
}
