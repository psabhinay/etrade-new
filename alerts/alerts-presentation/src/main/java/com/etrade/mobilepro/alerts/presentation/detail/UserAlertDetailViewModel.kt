package com.etrade.mobilepro.alerts.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.alerts.api.model.AlertResponse
import com.etrade.mobilepro.alerts.api.model.AlertValueType
import com.etrade.mobilepro.alerts.api.model.NewsAlertValueType
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.AlertInputValueDependenciesProvider
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.AlertSettingsHolder
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.UserInputValidator
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.quote.mapper.MobileQuoteWidgetItemMapper
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem
import com.etrade.mobilepro.session.api.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class UserAlertDetailViewModel @Inject constructor(
    private val settingsHolder: AlertSettingsHolder,
    private val alertsRepo: AlertsRepo,
    private val quoteRepo: MobileQuoteRepo,
    private val quoteMapper: MobileQuoteWidgetItemMapper,
    val inputValueDepsProvider: AlertInputValueDependenciesProvider,
    private val commonValidator: UserInputValidator,
    private val user: User
) : ViewModel() {
    private val _viewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState> = _viewState
    private val _viewEffects = MutableLiveData<ViewEffects>()
    val viewEffects: LiveData<ViewEffects> = _viewEffects
    private val compositeDisposable = CompositeDisposable()
    private var alert: UserAlertItem? = null

    fun setAlertButtonState(): LiveData<Pair<Boolean, ViewState>> =
        commonValidator.userInputIsValid(settingsHolder.inputValues, settingsHolder.switchedValues, inputValueDepsProvider).combineLatest(viewState)

    fun inputValueHolder(type: AlertValueType): MutableLiveData<String> = settingsHolder.inputValueHolder(type)

    fun switchValueHolder(type: NewsAlertValueType): MutableLiveData<Boolean> = settingsHolder.switchValueHolder(type)

    fun deleteAlert() {
        if (_viewState.value == ViewState.Loading.AlertUpdate) {
            return
        }
        this.alert?.let {
            _viewState.value = ViewState.Loading.AlertUpdate
            compositeDisposable.add(
                alertsRepo
                    .deleteUserAlert(it)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(
                        onComplete = {
                            _viewEffects.value = ViewEffects.AlertWasDeleted(it)
                        },
                        onError = {
                            _viewState.value = ViewState.Error.Deleting
                        }
                    )
            )
        }
    }

    fun loadAlert(alert: UserAlertItem) {
        if (this.alert != alert) {
            this.alert = alert
            _viewEffects.value = ViewEffects.LoadAlertValues(alert)
            when (alert) {
                is UserAlertItem.Alert -> settingsHolder.setupInputValues(alert)
                is UserAlertItem.NewsAlert -> settingsHolder.setupSwitchValues(alert)
            }
        }
        _viewState.value = ViewState.Loading.QuoteWidget(alert)
        compositeDisposable.add(
            quoteRepo
                .loadMobileQuote(alert.symbol)
                .map {
                    quoteMapper.map(it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = {
                        _viewState.value = ViewState.Success(alert, it)
                    },
                    onError = {
                        _viewState.value = ViewState.Error.Loading
                    }
                )
        )
    }

    fun updateAlert() {
        val token = user.token ?: return
        _viewState.value = ViewState.Loading.AlertUpdate
        this.alert?.let {
            when (it) {
                is UserAlertItem.Alert -> updateUserAlert(token, it)
                is UserAlertItem.NewsAlert -> updateUserNewsAlert(token, it)
            }
        }
    }

    private fun updateUserAlert(token: String, alert: UserAlertItem.Alert) {
        val setAlertValues = settingsHolder.inputValues
            .mapValues { it.value.value ?: "" }
        compositeDisposable.add(
            alertsRepo.updateAlert(
                sessionToken = token,
                symbol = alert.symbol,
                deleteInfo = alert.deleteInfo,
                edit = alert.edit,
                lastEdited = alert.lastEdited,
                values = setAlertValues
            )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        _viewState.value = ViewState.Success(alert)
                        _viewEffects.value = ViewEffects.AlertWasUpdated(it)
                    },
                    onError = {
                        _viewState.value = ViewState.Error.Updating
                    }
                )
        )
    }

    private fun updateUserNewsAlert(token: String, alert: UserAlertItem.NewsAlert) {
        val setNewsAlertValues = settingsHolder.switchedValues
            .mapValues { it.value.value ?: false }
            .filterValues { it } // only turned On values are needed
        compositeDisposable.add(
            alertsRepo.updateNewsAlert(
                sessionToken = token,
                symbol = alert.symbol,
                edit = alert.edit,
                deleteInfo = alert.deleteInfo,
                lastEdited = alert.lastEdited,
                values = setNewsAlertValues
            )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        _viewEffects.value = ViewEffects.AlertWasUpdated(it)
                    },
                    onError = {
                        _viewState.value = ViewState.Error.Updating
                    }
                )
        )
    }

    sealed class ViewState {
        sealed class Loading : ViewState() {
            data class QuoteWidget(
                val alert: UserAlertItem
            ) : Loading()

            object AlertUpdate : Loading()
        }
        data class Success(
            val alert: UserAlertItem,
            val quoteWidgetItem: QuoteWidgetItem? = null
        ) : ViewState()

        sealed class Error : ViewState() {
            object Loading : Error()
            object Deleting : Error()
            object Updating : Error()
        }
    }

    sealed class ViewEffects {
        data class AlertWasDeleted(
            val alert: UserAlertItem
        ) : ViewEffects()

        data class LoadAlertValues(
            val alert: UserAlertItem
        ) : ViewEffects()

        data class AlertWasUpdated(
            val response: AlertResponse?
        ) : ViewEffects()
    }
}
