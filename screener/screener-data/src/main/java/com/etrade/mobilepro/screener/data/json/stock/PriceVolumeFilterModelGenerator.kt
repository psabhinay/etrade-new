package com.etrade.mobilepro.screener.data.json.stock

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.stock.PriceVolumeFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val SHARE_PRICE_FIELD = "SharePrice"
private const val PRICE_PERFORMANCE_SP_OVER_4WEEKS_FIELD = "PricePerf_4Weeks_vsSP500"
private const val PRICE_PERFORMANCE_SP_OVER_13WEEKS_FIELD = "PricePerf_13Weeks_vsSP500"
private const val PRICE_PERFORMANCE_SP_OVER_52WEEKS_FIELD = "PricePerf_52Weeks_vsSP500"
private const val PRICE_PERFORMANCE_INDUSTRY_OVER_4WEEKS_FIELD = "PricePerf_4Weeks_Industry"
private const val PRICE_PERFORMANCE_INDUSTRY_OVER_13WEEKS_FIELD = "PricePerf_13Weeks_Industry"
private const val PRICE_PERFORMANCE_INDUSTRY_OVER_52WEEKS_FIELD = "PricePerf_52Weeks_Industry"

class PriceVolumeFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val priceVolumeFilter = screenerFilter as PriceVolumeFilter
        val arguments = mutableListOf<Argument>()

        if (!(priceVolumeFilter.sharePriceGreaterThan.isEmpty() && priceVolumeFilter.sharePriceLessThan.isEmpty())) {
            arguments.add(Argument(getSharePriceClauses(priceVolumeFilter), SHARE_PRICE_FIELD))
        }

        createPricePerformanceSpArguments(arguments, priceVolumeFilter)
        createPricePerformanceIndustryArguments(arguments, priceVolumeFilter)

        return PartialScreenerParams(arguments)
    }

    private fun createPricePerformanceSpArguments(arguments: MutableList<Argument>, priceVolumeFilter: PriceVolumeFilter) {
        if (priceVolumeFilter.isPricePerformanceSpOverSelected() && priceVolumeFilter.isPricePerformanceSpValuesSelected()) {
            val clauses = getPricePerformanceSpClauses(priceVolumeFilter)

            if (priceVolumeFilter.pricePerformanceSpOver4Weeks) {
                arguments.add(
                    Argument(
                        clauses,
                        PRICE_PERFORMANCE_SP_OVER_4WEEKS_FIELD
                    )
                )
            }
            if (priceVolumeFilter.pricePerformanceSpOver13Weeks) {
                arguments.add(
                    Argument(
                        clauses,
                        PRICE_PERFORMANCE_SP_OVER_13WEEKS_FIELD
                    )
                )
            }
            if (priceVolumeFilter.pricePerformanceSpOver52Weeks) {
                arguments.add(
                    Argument(
                        clauses,
                        PRICE_PERFORMANCE_SP_OVER_52WEEKS_FIELD
                    )
                )
            }
        }
    }

    private fun createPricePerformanceIndustryArguments(arguments: MutableList<Argument>, priceVolumeFilter: PriceVolumeFilter) {
        if (priceVolumeFilter.isPricePerformanceIndustryOverSelected() && priceVolumeFilter.isPricePerformanceIndustryValuesSelected()) {
            val clauses = getPricePerformanceIndustryClauses(priceVolumeFilter)

            if (priceVolumeFilter.pricePerformanceIndustryOver4Weeks) {
                arguments.add(
                    Argument(
                        clauses,
                        PRICE_PERFORMANCE_INDUSTRY_OVER_4WEEKS_FIELD
                    )
                )
            }
            if (priceVolumeFilter.pricePerformanceIndustryOver13Weeks) {
                arguments.add(
                    Argument(
                        clauses,
                        PRICE_PERFORMANCE_INDUSTRY_OVER_13WEEKS_FIELD
                    )
                )
            }
            if (priceVolumeFilter.pricePerformanceIndustryOver52Weeks) {
                arguments.add(
                    Argument(
                        clauses,
                        PRICE_PERFORMANCE_INDUSTRY_OVER_52WEEKS_FIELD
                    )
                )
            }
        }
    }

    private fun getSharePriceClauses(priceVolumeFilter: PriceVolumeFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (priceVolumeFilter.sharePriceGreaterThan.isNotEmpty()) {
            clauses.add(getSharePriceGreaterThanClause(priceVolumeFilter))
        }

        if (priceVolumeFilter.sharePriceLessThan.isNotEmpty()) {
            clauses.add(getSharePriceLessThanClause(priceVolumeFilter))
        }

        return clauses
    }

    private fun getPricePerformanceSpClauses(priceVolumeFilter: PriceVolumeFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (priceVolumeFilter.pricePerformanceSpLessThanMinus40) {
            clauses.addAll(pricePerformanceSpLessThanMinus40Clauses)
        }
        if (priceVolumeFilter.pricePerformanceSpMinus40toMinus20) {
            clauses.addAll(pricePerformanceSpMinus40toMinus20Clauses)
        }
        if (priceVolumeFilter.pricePerformanceSpMinus20to0) {
            clauses.addAll(pricePerformanceSpMinus20to0Clauses)
        }
        if (priceVolumeFilter.pricePerformanceSpZeroTo20) {
            clauses.addAll(pricePerformanceSpZeroTo20Clauses)
        }
        if (priceVolumeFilter.pricePerformanceSp20to40) {
            clauses.addAll(pricePerformanceSp20to40Clauses)
        }
        if (priceVolumeFilter.pricePerformanceSpGreaterThan40) {
            clauses.addAll(pricePerformanceSpGreaterThan40Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private fun getPricePerformanceIndustryClauses(priceVolumeFilter: PriceVolumeFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (priceVolumeFilter.pricePerformanceIndustryLessThanMinus40) {
            clauses.addAll(pricePerformanceIndustryLessThanMinus40Clauses)
        }
        if (priceVolumeFilter.pricePerformanceIndustryMinus40toMinus20) {
            clauses.addAll(pricePerformanceIndustryMinus40toMinus20Clauses)
        }
        if (priceVolumeFilter.pricePerformanceIndustryMinus20to0) {
            clauses.addAll(pricePerformanceIndustryMinus20to0Clauses)
        }
        if (priceVolumeFilter.pricePerformanceIndustryZeroTo20) {
            clauses.addAll(pricePerformanceIndustryZeroTo20Clauses)
        }
        if (priceVolumeFilter.pricePerformanceIndustry20to40) {
            clauses.addAll(pricePerformanceIndustry20to40Clauses)
        }
        if (priceVolumeFilter.pricePerformanceIndustryGreaterThan40) {
            clauses.addAll(pricePerformanceIndustryGreaterThan40Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private fun getSharePriceGreaterThanClause(priceVolumeFilter: PriceVolumeFilter) =
        Clause(OPERATOR_GREATER_THAN, listOf(priceVolumeFilter.sharePriceGreaterThan))
    private fun getSharePriceLessThanClause(priceVolumeFilter: PriceVolumeFilter) =
        Clause(OPERATOR_LESS_THAN, listOf(priceVolumeFilter.sharePriceLessThan))

    private val pricePerformanceSpLessThanMinus40Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("-100001")),
        Clause(OPERATOR_LESS_THAN, listOf("-40"))
    )
    private val pricePerformanceSpMinus40toMinus20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("-40")),
        Clause(OPERATOR_LESS_THAN, listOf("-20"))
    )
    private val pricePerformanceSpMinus20to0Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("-20")),
        Clause(OPERATOR_LESS_THAN, listOf("0"))
    )
    private val pricePerformanceSpZeroTo20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("20"))
    )
    private val pricePerformanceSp20to40Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("20")),
        Clause(OPERATOR_LESS_THAN, listOf("40"))
    )
    private val pricePerformanceSpGreaterThan40Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("40")),
        Clause(OPERATOR_LESS_THAN, listOf("100001"))
    )

    private val pricePerformanceIndustryLessThanMinus40Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("-100001")),
        Clause(OPERATOR_LESS_THAN, listOf("-40"))
    )
    private val pricePerformanceIndustryMinus40toMinus20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("-40")),
        Clause(OPERATOR_LESS_THAN, listOf("-20"))
    )
    private val pricePerformanceIndustryMinus20to0Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("-20")),
        Clause(OPERATOR_LESS_THAN, listOf("0"))
    )
    private val pricePerformanceIndustryZeroTo20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("20"))
    )
    private val pricePerformanceIndustry20to40Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("20")),
        Clause(OPERATOR_LESS_THAN, listOf("40"))
    )
    private val pricePerformanceIndustryGreaterThan40Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("40")),
        Clause(OPERATOR_LESS_THAN, listOf("100001"))
    )
}
