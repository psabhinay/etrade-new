package com.etrade.mobilepro.pushnotification.api

import java.io.Serializable

const val PAYLOAD_KEY = "payload"
const val TYPE_KEY = "type"
private const val ACCOUNT_TYPE = "Account"
private const val STOCK_TYPE = "Stock"

sealed class SmartAlert(val message: String) : Serializable {
    class Account(message: String) : SmartAlert(message)
    class Stock(message: String, val symbolTitle: String) : SmartAlert(message)

    companion object {
        fun create(type: String?, message: String?): SmartAlert? {
            if (type.isNullOrEmpty() || message.isNullOrEmpty()) {
                return null
            }

            return when (type) {
                ACCOUNT_TYPE -> Account(message)
                STOCK_TYPE ->
                    message.extractSymbolTitle()
                        ?.let { Stock(message = message, symbolTitle = it) }
                        ?: return null
                else -> null
            }
        }

        private fun String.extractSymbolTitle(): String? {
            return if (contains(' ')) {
                split(' ').firstOrNull()
            } else {
                this
            }
        }
    }
}
