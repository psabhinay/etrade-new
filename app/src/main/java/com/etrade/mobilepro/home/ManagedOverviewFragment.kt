package com.etrade.mobilepro.home

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.databinding.FragmentManagedOverviewBinding
import com.etrade.mobilepro.inboxmessages.addInboxMessagesView
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.webview.bridge.DEFAULT_INTERFACE_NAME
import com.etrade.mobilepro.webview.bridge.MobileNativeInterfaceImpl
import com.etrade.mobilepro.webview.bridge.OpenQuoteHandler
import com.etrade.mobilepro.webview.bridge.OpenUrlHandler
import org.chromium.customtabsclient.shared.WebViewFragment
import org.chromium.customtabsclient.shared.WebViewFragmentArgs
import org.chromium.customtabsclient.shared.WebViewHelper
import javax.inject.Inject

@RequireLogin
class ManagedOverviewFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    @DeviceType deviceType: String,
    managedAccountUrl: String,
    private val mainNavigation: MainNavigation,
    private val webViewHelper: WebViewHelper
) : WebViewFragment(deviceType), OpenQuoteHandler, OpenUrlHandler {

    private val binding by viewBinding(FragmentManagedOverviewBinding::bind)
    override val webviewBinding
        get() = binding.webviewContainer

    override val layoutRes = R.layout.fragment_managed_overview

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addInboxMessagesView(this, binding.inboxMessagesContainer, viewModelFactory)
    }

    override val headers: Map<String, String>
        get() = mapOf("AccountNumber" to accountsTabArgs.accountId.orEmpty())

    private val accountsTabArgs: AccountsTabHostFragmentArgs by navArgs()
    override val restoreState = false

    override val args: WebViewFragmentArgs = WebViewFragmentArgs(
        url = managedAccountUrl
    )

    override fun openQuote(symbol: String) {
        requireActivity().runOnUiThread {
            val bundle = SearchResultItem.Symbol(title = symbol, type = InstrumentType.EQ).toBundle()
            mainNavigation.navigateToQuoteDetails(activity, bundle)
        }
    }

    override fun openUrl(url: String, isPdf: Boolean) {
        requireActivity().runOnUiThread {
            if (isPdf) {
                findNavController().navigate(
                    MainGraphDirections.actionOpenPdf(
                        url = url
                    )
                )
            } else {
                webViewHelper.openCustomTab(uri = url)
            }
        }
    }

    override fun setupWebView(webView: WebView) {
        super.setupWebView(webView)
        webView.addJavascriptInterface(MobileNativeInterfaceImpl(openQuoteHandler = this, openUrlHandler = this), DEFAULT_INTERFACE_NAME)
    }
}
