package com.etrade.mobilepro.chartiq.presentation.util

import com.etrade.mobilepro.chartiq.data.AlertDialogContent

fun AlertDialogContent.hasNoActions() = positiveButton == null && negativeButton == null && neutralButton == null
