package com.etrade.mobilepro.usersubscriptionpresentation

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import javax.inject.Inject

class RealTimeQuoteStreamingPromptDialogFragment @Inject constructor(
    private val toggleStreaming: SubscriptionStreamingToggle
) : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return context?.let { ctx ->
            isCancelable = false
            val builder = MaterialAlertDialogBuilder(ctx)
                .setTitle(R.string.please_note)
                .setMessage(R.string.requires_real_time_streaming)
                .setPositiveButton(R.string.turn_streaming_on) { _, _ ->
                    // Have to dismiss first to make sure nav graph is at correct destination.
                    // Also moved destination to parent graph just to be safe.
                    dismiss()
                    toggleStreaming.toggle(true)
                }
                .setNegativeButton(R.string.cancel) { _, _ ->
                    dismiss()
                }

            builder.create()
        } ?: super.onCreateDialog(savedInstanceState)
    }
}
