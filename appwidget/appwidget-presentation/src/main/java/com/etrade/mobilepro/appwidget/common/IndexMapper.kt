package com.etrade.mobilepro.appwidget.common

import com.etrade.mobilepro.marketsapi.Index
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.util.safeParseBigDecimal

internal fun Index.copyWith(data: Level1Data): Index? {
    val price = data.lastPrice?.safeParseBigDecimal()
    val change = if (data is StockData) {
        data.dayChange?.safeParseBigDecimal()
    } else {
        data.change?.safeParseBigDecimal()
    }
    val percentChange = if (data is StockData) {
        data.dayChangePercent
    } else { data.changePercent }
        ?.replace("%", "")
        ?.safeParseBigDecimal()

    if (price == null || change == null || percentChange == null) { return null }

    return this.copy(
        price = price,
        change = change,
        percentChange = percentChange
    )
}
