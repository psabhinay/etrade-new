package com.etrade.mobilepro.screener.data.datasource

import android.content.res.Resources
import com.etrade.mobilepro.screener.api.Country
import com.etrade.mobilepro.screener.api.datasource.CountryDataSource
import com.etrade.mobilepro.screener.data.R
import javax.inject.Inject

class DefaultCountryDataSource @Inject constructor(private val resources: Resources) : CountryDataSource {
    private val allCountries by lazy {
        Country(
            id = "",
            title = resources.getString(R.string.all_countries),
            isAllItemsElement = true
        )
    }

    private val countriesData by lazy {
        listOf(
            allCountries,
            Country(
                id = resources.getString(R.string.country_australia_id),
                title = resources.getString(R.string.country_australia)
            ),
            Country(
                id = resources.getString(R.string.country_belgium_id),
                title = resources.getString(R.string.country_belgium)
            ),
            Country(
                id = resources.getString(R.string.country_brazil_id),
                title = resources.getString(R.string.country_brazil)
            ),
            Country(
                id = resources.getString(R.string.country_canada_id),
                title = resources.getString(R.string.country_canada)
            ),
            Country(
                id = resources.getString(R.string.country_china_id),
                title = resources.getString(R.string.country_china)
            ),
            Country(
                id = resources.getString(R.string.country_finland_id),
                title = resources.getString(R.string.country_finland)
            ),
            Country(
                id = resources.getString(R.string.country_france_id),
                title = resources.getString(R.string.country_france)
            ),
            Country(
                id = resources.getString(R.string.country_germany_id),
                title = resources.getString(R.string.country_germany)
            ),
            Country(
                id = resources.getString(R.string.country_hong_kong_id),
                title = resources.getString(R.string.country_hong_kong)
            ),
            Country(
                id = resources.getString(R.string.country_india_id),
                title = resources.getString(R.string.country_india)
            ),
            Country(
                id = resources.getString(R.string.country_japan_id),
                title = resources.getString(R.string.country_japan)
            ),
            Country(
                id = resources.getString(R.string.country_mexico_id),
                title = resources.getString(R.string.country_mexico)
            ),
            Country(
                id = resources.getString(R.string.country_netherland_id),
                title = resources.getString(R.string.country_netherland)
            ),
            Country(
                id = resources.getString(R.string.country_norway_id),
                title = resources.getString(R.string.country_norway)
            ),
            Country(
                id = resources.getString(R.string.country_russia_id),
                title = resources.getString(R.string.country_russia)
            ),
            Country(
                id = resources.getString(R.string.country_singapore_id),
                title = resources.getString(R.string.country_singapore)
            ),
            Country(
                id = resources.getString(R.string.country_south_africa_id),
                title = resources.getString(R.string.country_south_africa)
            ),
            Country(
                id = resources.getString(R.string.country_south_korea_id),
                title = resources.getString(R.string.country_south_korea)
            ),
            Country(
                id = resources.getString(R.string.country_spain_id),
                title = resources.getString(R.string.country_spain)
            ),
            Country(
                id = resources.getString(R.string.country_sweden_id),
                title = resources.getString(R.string.country_sweden)
            ),
            Country(
                id = resources.getString(R.string.country_switzerland_id),
                title = resources.getString(R.string.country_switzerland)
            ),
            Country(
                id = resources.getString(R.string.country_taiwan_id),
                title = resources.getString(R.string.country_taiwan)
            ),
            Country(
                id = resources.getString(R.string.country_uk_id),
                title = resources.getString(R.string.country_uk)
            ),
            Country(
                id = resources.getString(R.string.country_us_id),
                title = resources.getString(R.string.country_us)
            )
        )
    }

    override fun getAllCountriesItem(): Country = allCountries

    override fun getCountries(): List<Country> = countriesData
}
