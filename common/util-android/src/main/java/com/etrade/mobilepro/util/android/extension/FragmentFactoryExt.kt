package com.etrade.mobilepro.util.android.extension

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import kotlin.reflect.KClass

fun <T : Fragment> FragmentFactory.instantiate(cls: KClass<T>): Fragment = instantiateFragment(cls.java)

inline fun <reified T : Class<out Fragment>> FragmentFactory.instantiateFragment(fragmentClass: T, args: Bundle? = null): Fragment {
    return this.instantiate(fragmentClass.classLoader!!, fragmentClass.name).apply { arguments = args }
}
