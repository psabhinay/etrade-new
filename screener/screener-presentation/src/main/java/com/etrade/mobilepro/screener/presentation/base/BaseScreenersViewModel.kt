package com.etrade.mobilepro.screener.presentation.base

import android.text.Spannable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.livedata.postUpdateValue
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.datasource.ScreenerSetsDataSource
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.MarketsScreenerResultsAnalyticsParams
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.textutil.TextUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

abstract class BaseScreenersViewModel(
    private val screenerGroup: ScreenerType.Group,
    private val screenerSetsDataSource: ScreenerSetsDataSource,
    private val screenerInteractor: ScreenerInteractor,
    private val tracker: Tracker
) : ViewModel() {
    private var disclosureText: Spannable? = null
    private val compositeDisposable = CompositeDisposable()
    private val _viewState = MutableLiveData<ViewState>()
    internal val viewState: LiveData<ViewState?>
        get() = _viewState

    internal fun update(disclosureText: String?) {
        if (_viewState.value == null) {
            _viewState.value = ViewState()
            disclosureText?.let { this.disclosureText = TextUtil.convertTextWithHmlTagsToSpannable(it) }
            subscribeToPreScreenResults()
            subscribeToScreenersUpdates()
        }
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    fun trackScreenerResultPage(params: MarketsScreenerResultsAnalyticsParams) {
        tracker.event(params)
    }

    internal fun clearScreenerSet(screenerSet: ScreenerItem.ScreenerSet) = screenerSetsDataSource.clearScreenerSet(screenerSet)

    internal fun retryPrescreen() {
        screenerInteractor.retryPrescreen()
    }

    internal data class ViewState(
        val screenerItems: List<ScreenerItem> = emptyList(),
        val preScreenResults: Resource<Int> = Resource.Success()
    )

    private fun subscribeToScreenersUpdates() {
        compositeDisposable.add(
            screenerSetsDataSource.screeners(screenerGroup)
                .subscribeBy(
                    onNext = {
                        _viewState.postUpdateValue { state ->
                            val screenerItems = (it + ScreenerItem.Footer).toMutableList().apply {
                                disclosureText?.let { text -> add(ScreenerItem.DisclosuresText(text)) }
                            }
                            state.copy(screenerItems = screenerItems)
                        }
                    },
                    onComplete = {
                        throw IllegalStateException("Screener filter aggregator has emitted onComplete")
                    },
                    onError = {
                        throw IllegalStateException("Screener filter aggregator has emitted onError with ${it.message}", it)
                    }
                )
        )
    }

    private fun subscribeToPreScreenResults() =
        compositeDisposable.add(
            screenerInteractor
                .preScreenResults()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = {
                        _viewState.updateValue { state ->
                            state.copy(preScreenResults = it)
                        }
                    },
                    onComplete = {
                        throw IllegalStateException("Screener interactor at ($screenerGroup) has emitted onComplete")
                    },
                    onError = {
                        throw IllegalStateException("Screener interactor at ($screenerGroup) has emitted onError with ${it.message}", it)
                    }
                )
        )
}
