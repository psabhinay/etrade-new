package com.etrade.mobilepro.quoteapi

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.util.formatLetterRepresentationToNumber
import com.etrade.mobilepro.util.isQuoteSizeValid

interface QuoteHeaderViewItem : OrderDetailItemInterface {
    val title: String
    val titleContentDescription: String
    val lastPrice: LiveData<QuotePriceWidgetItem>
    val change: LiveData<QuoteChangeWidgetItem>
    val changeColor: LiveData<Int>
    val volume: LiveData<String>?
    val asOfDateTime: LiveData<String>
    val contentDescription: LiveData<String?>
    val isAffiliatedProduct: Boolean
}

@Suppress("LongMethod")
fun QuoteHeaderViewItem.updateContentDescription(
    priceFormat: String,
    sizeFormat: String,
    changeFormat: String,
    contentDescriptionFormat: String,
    contentDescription: MutableLiveData<String?>
) {
    val quotePriceAndSize = lastPrice.value
    val formattedQuotePrice = quotePriceAndSize?.let {
        val quotePrice = quotePriceAndSize.formatQuotePriceContentDescription(priceFormat, quotePriceAndSize.price)
        if (quotePriceAndSize.size.isQuoteSizeValid()) {
            val quoteSize = quotePriceAndSize.formatQuotePriceContentDescription(sizeFormat, quotePriceAndSize.size)
            "$quotePrice $quoteSize"
        } else {
            quotePrice
        }
    }

    val quoteChange = change.value
    val formattedChange = quoteChange?.let {
        quoteChange.formatQuoteChange(changeFormat, quoteChange.change, quoteChange.changePercent)
    }

    val formattedQuote = String.format(
        contentDescriptionFormat,
        title,
        formattedQuotePrice ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE,
        formattedChange ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE,
        MarketDataFormatter.formatVolumeTwoDecimals(volume?.value?.toBigDecimalOrNull()).formatLetterRepresentationToNumber(),
        asOfDateTime.value
    )
    contentDescription.postValue(formattedQuote)
}
