package com.etrade.mobilepro.quote.screen.presentation.adapter

import android.widget.TextView
import androidx.annotation.StringRes
import androidx.databinding.BindingAdapter
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.quote.screen.presentation.R
import com.etrade.mobilepro.quoteapi.FundQuoteItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.SectionLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.SummarySectionItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.SummarySectionMultiLineItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.safeParseBigDecimal

@BindingAdapter("moneyMarketFundDetail")
fun SectionLayout.bindMoneyMarketFundDetail(detail: FundQuoteItem.MoneyMarketFundDetail?) {
    bindContent(detail) {
        listOf(
            createSummarySectionItem(R.string.seven_day_current_yield, formatMarketDataCustom(sevenDayYield)),
            createSummarySectionItem(R.string.annual_total_return, formatMarketDataCustom(annualTotalReturn)),
            createSummarySectionItem(R.string.weighted_average_maturity, formatNumberWithZeroToTwoDecimals(weightedMaturity)),
            createMultilineSummarySectionItem(R.string.quote_family, familyName)
        )
    }
}

@BindingAdapter("morningStarDetails")
fun SectionLayout.bindMorningStarDetails(detail: FundQuoteItem.MorningStarDetails?) {
    bindContent(detail) {
        listOf(
            createMultilineSummarySectionItem(R.string.quote_morningstar_category, morningStarCategory),
            createMultilineSummarySectionItem(R.string.quote_availability, availability)
        )
    }
}

@BindingAdapter("summarySectionPart1")
fun SectionLayout.bindSummarySectionPart1(section: FundQuoteItem.SummarySection?) {
    bindContent(section) {
        listOf(
            createSummarySectionItem(R.string.quote_net_expense_ratio, formatMarketPercentData(netExpenseRatio)),
            createSummarySectionItem(R.string.quote_gross_expense_ratio, formatMarketPercentData(grossExpenseRatio)),
            createSummarySectionItem(R.string.quote_total_net_assets_title, formatMarketDataWithCurrency(totalNetAssets)),
            createSummarySectionItem(R.string.quote_fund_inception, inceptionDate),
            createSummarySectionItem(R.string.quote_thirty_day_sec_yield, thirtyDaySecYield),
            createSummarySectionItem(R.string.quote_yield, oneYearSecYield)
        )
    }
}

@BindingAdapter("summarySectionPart2")
fun SectionLayout.bindSummarySectionPart2(section: FundQuoteItem.SummarySection?) {
    bindContent(section) {
        listOf(
            createSummarySectionItem(R.string.quote_initial_investment, formatMarketDataWithCurrency(initialInvestment)),
            createSummarySectionItem(R.string.quote_subsequent_investment, formatMarketDataWithCurrency(subsequentInvestment)),
            createSummarySectionItem(R.string.sales_charge, salesCharge),
            createSummarySectionItem(R.string.fund_redemption_fee, fundRedemptionFee),
            createSummarySectionItem(R.string.quote_etrade_transaction_fee, etradeTransactionFee)
        )
    }
}

@BindingAdapter("formatPercentExact")
fun TextView.setPercentExact(percent: String?) {
    percent?.let {
        text = MarketDataFormatter.formatMarketPercData(it.safeParseBigDecimal())
        contentDescription = text
    }
}

private fun formatMarketDataCustom(value: String): String {
    return MarketDataFormatter.formatMarketDataCustom(value.safeParseBigDecimal())
}

private fun formatMarketDataWithCurrency(value: String): String {
    return value.safeParseBigDecimal()?.let { MarketDataFormatter.formatMarketDataWithCurrency(it) } ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
}

private fun formatMarketPercentData(value: String): String {
    return MarketDataFormatter.formatMarketPercData(value.safeParseBigDecimal())
}

private fun formatNumberWithZeroToTwoDecimals(value: String): String {
    return MarketDataFormatter.formatNumberWithZeroToTwoDecimals(value.safeParseBigDecimal())
}

private fun <T> SectionLayout.bindContent(content: T?, createItems: T.() -> List<TableSectionItemLayout>) = setContent(content?.createItems().orEmpty())

private fun SectionLayout.createSummarySectionItem(@StringRes labelRes: Int, value: String): TableSectionItemLayout {
    return SummarySectionItem(
        label = resources.getString(labelRes),
        value = value,
        textColor = null
    )
}

private fun SectionLayout.createMultilineSummarySectionItem(@StringRes labelRes: Int, value: String): TableSectionItemLayout {
    return SummarySectionMultiLineItem(
        label = resources.getString(labelRes),
        value = value,
        textColor = null
    )
}
