package com.etrade.mobilepro.swiperefresh

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

fun SwipeRefreshLayout.applyColorScheme() {
    setColorSchemeResources(R.color.primaryColor, R.color.primaryDarkColor, R.color.secondaryColor)
}
