package com.etrade.mobilepro.alerts.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AlertsCountRequest(
    @Json(name = "value")
    val searchType: AlertCountRequestValue = AlertCountRequestValue()
)

@JsonClass(generateAdapter = true)
data class AlertCountRequestValue(
    @Json(name = "searchType")
    val searchType: String = "UNREAD"
)
