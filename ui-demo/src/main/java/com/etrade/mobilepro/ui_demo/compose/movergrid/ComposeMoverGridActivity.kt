package com.etrade.mobilepro.ui_demo.compose.movergrid

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.etrade.mobilepro.common.compose.theme.EtradeTheme
import com.etrade.mobilepro.movers.MoverGridData
import com.etrade.mobilepro.movers.MoverItem
import com.etrade.mobilepro.movers.compose.MoverGrid
import java.math.BigDecimal
import java.util.UUID
import kotlin.random.Random

class ComposeMoverGridActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val items = generateMoverItems(7)
        setContent {
            EtradeTheme {
                MoverGrid(
                    MoverGridData(
                        items
                    )
                )
            }
        }
    }
}

private fun generateMoverItems(amount: Int): List<MoverItem> = (0..amount).map {
    fun createRandomValueWithFraction() =
        "${Random.nextInt(from = -100, until = 100)}.${Random.nextInt(from = -100, until = 100)}"

    val ticker = UUID.randomUUID().toString().takeLast(4).uppercase()
    val hasOptionsInfo = Random.nextBoolean()
    MoverItem(
        ticker = ticker,
        displaySymbol = ticker,
        price = createRandomValueWithFraction(),
        isPositiveGain = Random.nextBoolean(),
        gain = createRandomValueWithFraction(),
        gainPercentage = createRandomValueWithFraction(),
        decimalPercentChange = BigDecimal.valueOf(Random.nextDouble(from = -100.0, until = 100.0)),
        optionsExpiryDate = if (hasOptionsInfo) {
            "07/09/21"
        } else {
            null
        },
        optionsStrikePricePlusType = if (hasOptionsInfo) {
            "$999 C"
        } else {
            null
        },
        itemClickHandler = { println("$it tapped") },
        shouldDisplayOptionsData = true
    )
}
