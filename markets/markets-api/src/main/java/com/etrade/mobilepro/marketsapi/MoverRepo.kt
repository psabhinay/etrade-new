package com.etrade.mobilepro.marketsapi

import com.etrade.mobilepro.common.result.ETResult

interface MoverRepo {
    suspend fun getMarketIndices(): ETResult<List<Index>>
}
