-if class com.etrade.mobilepro.order.details.dto.OrderCancelRequestDto
-keepnames class com.etrade.mobilepro.order.details.dto.OrderCancelRequestDto
-if class com.etrade.mobilepro.order.details.dto.OrderCancelRequestDto
-keep class com.etrade.mobilepro.order.details.dto.OrderCancelRequestDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.etrade.mobilepro.order.details.dto.OrderCancelRequestDto
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.etrade.mobilepro.order.details.dto.OrderCancelRequestDto
-keepclassmembers class com.etrade.mobilepro.order.details.dto.OrderCancelRequestDto {
    public synthetic <init>(java.lang.String,java.lang.String,java.lang.String,java.lang.String,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
