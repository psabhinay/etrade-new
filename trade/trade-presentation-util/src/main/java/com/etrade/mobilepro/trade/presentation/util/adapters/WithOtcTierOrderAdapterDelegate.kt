package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.WithOtcTierOrder
import com.etrade.mobilepro.trade.presentation.util.R
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

class WithOtcTierOrderAdapterDelegate(private val resources: Resources) : OrderAdapterDelegate {

    override fun createOrderSectionItems(order: Order): List<TableSectionItemLayout> {
        require(order is WithOtcTierOrder)
        return createOtcTierSections(order)
    }

    private fun createOtcTierSections(response: WithOtcTierOrder): List<TableSectionItemLayout> {
        return response.otcTier
            .takeUnless { it.isNullOrEmpty() }
            ?.let { otcTier ->
                sectionItem(resources, R.string.trade_label_otc_tier_level, otcTier)
            }
            ?.let(::listOf)
            ?: emptyList()
    }
}
