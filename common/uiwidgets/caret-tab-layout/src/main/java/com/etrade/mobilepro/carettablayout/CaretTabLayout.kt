package com.etrade.mobilepro.carettablayout

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.view.children
import com.etrade.mobilepro.carettablayout.databinding.CaretTabLayoutViewBinding
import com.etrade.mobilepro.util.android.extension.inflater
import com.google.android.material.tabs.TabLayout

/**
 * The view that make the tabs visible using a caret so that users know that the page is scrollable.
 * A caret at the right end of the tabs pointing right (>).
 * When I click the caret on the right, I see the next (previously hidden) tab appear.
 * When there are no more tabs to the right, I see a caret that is pointed in the opposite direction to scroll me back to the previous tabs (<).
 * Clicking the caret does not change the tab selection that I am on.
 */
class CaretTabLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0,
) : RelativeLayout(context, attrs, defStyleAttr, defStyleRes) {

    private var scrollingDirection: Scrolling = Scrolling.LEFT

    private val binding = CaretTabLayoutViewBinding.inflate(inflater, this, true)

    val tabLayout: TabLayout
        get() = binding.tabLayout
    val tabArrow: ImageButton
        get() = binding.tabArrowButton

    init {
        applyStyle(attrs, defStyleAttr, defStyleRes)
        setListeners()
        tabLayout.post {
            toggleArrowDirectionIfNecessary()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        toggleArrowButton(!(tabLayout as FullWidthTabLayout).fitsScreenWidth)
    }

    private fun applyStyle(attrs: AttributeSet?, defStyleAttr: Int = 0, defStyleRes: Int = 0) {
        getStyle(attrs, defStyleAttr, defStyleRes).apply()
    }

    @Suppress("TooGenericExceptionCaught")
    private fun getStyle(attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) =
        with(styledAttributes(attrs, defStyleAttr, defStyleRes)) {
            try {
                val isLight = getBoolean(R.styleable.CaretTabLayout_isLight, false)
                if (isLight) { Style.LIGHT } else { Style.DARK }
            } catch (e: RuntimeException) {
                Style.DARK
            } finally {
                recycle()
            }
        }

    private fun styledAttributes(attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) =
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CaretTabLayout,
            defStyleAttr,
            defStyleRes
        )

    private fun Style.apply() {
        setBackgroundColor(get(backgroundColor))
        tabArrow.setColorFilter(get(caretColor))
        with(tabLayout) {
            setTabTextColors(get(tabTextColor), get(tabSelectedTextColor))
            setSelectedTabIndicatorColor(get(tabIndicatorColor))
        }
    }

    private fun get(@ColorRes color: Int) = resources.getColor(color, context.theme)

    private fun setListeners() {
        tabArrow.setOnClickListener {
            smoothScrollTabs()
        }
        tabLayout.setOnScrollChangeListener { _, _, _, _, _ ->
            toggleArrowDirectionIfNecessary()
        }
    }

    private fun smoothScrollTabs() {
        tabLayout.smoothScrollBy(tabLayout.getPixelsForTabScroll(), 0)
    }

    @Suppress("NestedBlockDepth")
    private fun TabLayout.getPixelsForTabScroll(): Int {
        when (scrollingDirection) {
            Scrolling.LEFT -> for (i in (tabCount - 1) downTo 0) {
                val tab = getTabAt(i)?.view ?: return 0
                val offset = tab.getTabTitleOffset()

                if (right >= tab.right - offset - scrollX) {
                    return getTabAt(i + 1)?.view?.right?.let {
                        it - scrollX - right
                    } ?: (tab.right - scrollX - right)
                }
            }
            Scrolling.RIGHT -> for (i in 0 until tabCount) {
                val tab = getTabAt(i)?.view ?: return 0
                val offset = tab.getTabTitleOffset()

                if (left <= tab.left + offset - scrollX) {
                    return getTabAt(i - 1)?.view?.left?.let {
                        it - scrollX - left
                    } ?: (tab.left - scrollX - left)
                }
            }
        }
        return 0
    }

    private fun ViewGroup.getTabTitleOffset() =
        children.firstOrNull { it is TextView }?.let { width / 2 } ?: 0

    private fun toggleArrowDirectionIfNecessary() {
        checkDirectionAndToggle(Scrolling.RIGHT) || checkDirectionAndToggle(Scrolling.LEFT)
    }

    private fun checkDirectionAndToggle(scrolling: Scrolling) =
        if (!tabLayout.canScrollHorizontally(scrolling.direction) &&
            scrollingDirection != scrolling
        ) {
            scrollingDirection = scrolling
            with(binding.tabArrowButton) {
                setImageResource(scrolling.image)
                contentDescription = context.getString(scrolling.description)
            }
            true
        } else {
            false
        }

    private fun toggleArrowButton(enabled: Boolean) {
        tabArrow.visibility = if (enabled) { VISIBLE } else { GONE }
    }

    private companion object {

        enum class Scrolling(
            val direction: Int,
            @DrawableRes val image: Int,
            @StringRes val description: Int,
        ) {
            LEFT(-1, R.drawable.ic_arrow_right, R.string.caret_tab_layout_scroll_right),
            RIGHT(1, R.drawable.ic_arrow_left, R.string.caret_tab_layout_scroll_left),
        }

        enum class Style(
            @ColorRes val backgroundColor: Int,
            @ColorRes val tabIndicatorColor: Int,
            @ColorRes val tabTextColor: Int,
            @ColorRes val tabSelectedTextColor: Int,
            @ColorRes val caretColor: Int,
        ) {
            DARK(
                backgroundColor = R.color.plum,
                tabIndicatorColor = R.color.light_purple,
                tabTextColor = R.color.tab_text_color,
                tabSelectedTextColor = R.color.tab_selected_text_color,
                caretColor = R.color.grey_nobel,
            ),
            LIGHT(
                backgroundColor = R.color.white,
                tabIndicatorColor = R.color.primaryColor,
                tabTextColor = R.color.grey,
                tabSelectedTextColor = R.color.light_black,
                caretColor = R.color.purple,
            ),
        }
    }
}
