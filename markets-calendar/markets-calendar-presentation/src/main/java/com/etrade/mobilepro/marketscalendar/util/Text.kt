package com.etrade.mobilepro.marketscalendar.util

import android.content.res.Resources
import com.etrade.mobilepro.marketscalendar.R

fun Resources.getCalendarEventTitle(symbol: String): String = getString(R.string.earnings_report_template, symbol)
