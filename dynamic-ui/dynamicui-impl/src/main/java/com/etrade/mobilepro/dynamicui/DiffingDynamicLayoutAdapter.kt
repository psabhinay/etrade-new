package com.etrade.mobilepro.dynamicui

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DiffUtil
import com.etrade.mobilepro.dynamicui.api.Diffable
import com.etrade.mobilepro.dynamicui.api.DynamicLayout
import com.etrade.mobilepro.util.android.recyclerviewutil.debugDiffResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.plus
import kotlinx.coroutines.withContext

class DiffingDynamicLayoutAdapter(
    lifecycleOwner: LifecycleOwner,
    initialItems: List<DynamicLayout> = emptyList(),
    private val awaitForMainThread: suspend () -> Unit,
    private val onValuesApplied: () -> Unit = {}
) : DynamicUiLayoutRecyclerAdapter(lifecycleOwner, initialItems) {

    private val diffScope = lifecycleOwner.lifecycleScope + Dispatchers.IO
    private var diffJob: Job? = null

    override var values: List<DynamicLayout>
        get() = _values
        set(value) {
            launchItemDiff(value)
        }

    private fun launchItemDiff(newItems: List<DynamicLayout>) {
        val oldItems = _values
        diffJob?.apply {
            if (!isCancelled) {
                val message = "Item diff cancelled due to new items received"
                logger.debug(message)
                cancel(message)
            }
        }
        logger.debug("Launching item diff with ${oldItems.size} old items and ${newItems.size} new items")
        diffJob = diffScope.launch {
            val diffResult = DiffUtil.calculateDiff(DiffCallback(oldItems, newItems))
            logger.debugDiffResult(diffResult)
            withContext(Dispatchers.Main) {
                diffJob = null
                _values = newItems
                diffResult.dispatchUpdatesTo(this@DiffingDynamicLayoutAdapter)
                // Wait for the list rendering
                awaitForMainThread()
                onValuesApplied()
            }
        }
    }

    inner class DiffCallback(
        private val oldItems: List<DynamicLayout>,
        private val newItems: List<DynamicLayout>
    ) :
        DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldItems.size

        override fun getNewListSize(): Int = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return checkItem(oldItemPosition, newItemPosition) { oldItem, newItem ->
                oldItem.isSameItem(newItem)
            }
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return checkItem(oldItemPosition, newItemPosition) { oldItem, newItem ->
                oldItem.hasSameContents(newItem)
            }
        }

        private fun checkItem(
            oldItemPosition: Int,
            newItemPosition: Int,
            check: (Diffable, Any) -> Boolean
        ): Boolean {
            val oldItem = oldItems[oldItemPosition]
            val newItem = newItems[newItemPosition]
            if (oldItem == newItem) {
                return true
            }
            if (oldItem is Diffable) {
                return check(oldItem, newItem)
            }
            logger.warn("${oldItem.javaClass.simpleName} is not Diffable")
            return false
        }
    }
}
