package com.etrade.mobilepro.lenslayoutmanager

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.abs
import kotlin.math.min

private const val DEFAULT_SCALE = 0.5f

/**
 * Layout manager that behaves as [LinearLayoutManager] with an exception that it's changes the magnifies an item in the center of [RecyclerView] of which it is
 * attached to.
 *
 * There are two properties that may change the  behavior of layout manager: [scale] and [numberOfItems]. See docs for each of them for explanation.
 */
class LensLayoutManager(context: Context) : LinearLayoutManager(context) {

    /**
     * Defines the scale of out-of-focus items.
     *
     * An item in the center of a [RecyclerView] has original size and its scale is always 1. Sizes of other items can vary from `original size` to `original
     * size * scale`.
     */
    var scale: Float = DEFAULT_SCALE
        set(value) {
            field = value
            shrinkAmount = 1 - value
        }

    /**
     * The maximum number of items that can be fully visible on [RecyclerView].
     */
    var numberOfItems: Int = 1
        set(value) {
            field = value
            shrinkDistance = calcShrinkDistance()
        }

    private var shrinkAmount: Float = calcShrinkAmount()
    private var shrinkDistance: Float = calcShrinkDistance()

    override fun onLayoutCompleted(state: RecyclerView.State) {
        super.onLayoutCompleted(state)
        if (orientation == RecyclerView.HORIZONTAL) {
            updateScaleForHorizontalLayout()
        } else {
            updateScaleForVerticalLayout()
        }
    }

    override fun scrollHorizontallyBy(dx: Int, recycler: RecyclerView.Recycler, state: RecyclerView.State): Int {
        return super.scrollHorizontallyBy(dx, recycler, state).also {
            updateScaleForHorizontalLayout()
        }
    }

    override fun scrollVerticallyBy(dy: Int, recycler: RecyclerView.Recycler, state: RecyclerView.State): Int {
        return super.scrollVerticallyBy(dy, recycler, state).also {
            updateScaleForVerticalLayout()
        }
    }

    private fun calcShrinkAmount(): Float = 1 - scale

    private fun calcShrinkDistance(): Float = 2f / numberOfItems

    private fun updateScaleForHorizontalLayout() = updateScale({ width }, { getDecoratedLeft(this) }, { getDecoratedRight(this) })

    private fun updateScaleForVerticalLayout() = updateScale({ height }, { getDecoratedTop(this) }, { getDecoratedBottom(this) })

    private inline fun updateScale(axisSize: () -> Int, start: View.() -> Int, end: View.() -> Int) {
        val center = axisSize() / 2f
        val d1 = shrinkDistance * center

        for (i in 0 until childCount) {
            val child = getChildAt(i) ?: continue
            val childMidpoint = (child.start() + child.end()) / 2f
            val d = min(d1, abs(center - childMidpoint))
            val scale = 1f - shrinkAmount * d / d1

            child.apply {
                scaleX = scale
                scaleY = scale
            }
        }
    }
}
