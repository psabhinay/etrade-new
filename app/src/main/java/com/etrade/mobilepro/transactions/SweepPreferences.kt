package com.etrade.mobilepro.transactions

interface SweepPreferences {
    var isSweepActivityEnabled: Boolean
}
