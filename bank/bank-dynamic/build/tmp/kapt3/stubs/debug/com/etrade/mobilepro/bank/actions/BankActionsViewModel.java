package com.etrade.mobilepro.bank.actions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\nJ\u0006\u0010\f\u001a\u00020\nR\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/etrade/mobilepro/bank/actions/BankActionsViewModel;", "Lcom/etrade/mobilepro/dynamicui/api/DynamicViewModel;", "bankAccountId", "", "(Ljava/lang/String;)V", "variableId", "", "getVariableId", "()I", "onCheckDepositClicked", "", "onPayBillClicked", "onTransferMoneyClicked", "bank-dynamic_debug"})
public final class BankActionsViewModel extends com.etrade.mobilepro.dynamicui.api.DynamicViewModel {
    private final java.lang.String bankAccountId = null;
    private final int variableId = 0;
    
    public BankActionsViewModel(@org.jetbrains.annotations.Nullable()
    java.lang.String bankAccountId) {
        super(0);
    }
    
    @java.lang.Override()
    public int getVariableId() {
        return 0;
    }
    
    public final void onCheckDepositClicked() {
    }
    
    public final void onTransferMoneyClicked() {
    }
    
    public final void onPayBillClicked() {
    }
}