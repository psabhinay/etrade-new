package com.etrade.mobilepro.checkdeposit.usecases.accounts

import com.etrade.mobilepro.checkdeposit.usecases.api.EligibleAccounts
import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount

class EligibleAccounts(
    override val termsSigned: Boolean,
    override val eligibleAccounts: List<FundsFlowAccount>
) : EligibleAccounts
