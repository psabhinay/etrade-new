package com.etrade.mobilepro.trade.conditionalOrderDisclosure.presentation

import android.content.res.Resources
import android.text.SpannableString
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.api.ConditionalOrderDisclosureRepo
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.android.textutil.setClickableSpan
import com.etrade.mobilepro.viewmodel.BlockingLoadingIndicatorViewModel
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

abstract class BaseConditionalOrderDisclosureViewModel(
    private val conditionalOrderDisclosureRepo: ConditionalOrderDisclosureRepo,
    private val resources: Resources
) : ViewModel(), BlockingLoadingIndicatorViewModel {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val _blockingLoadingIndicator = MutableLiveData<Boolean>()
    private val _errorMessage = MutableLiveData<ConsumableLiveEvent<ErrorMessage>>()
    private val _showDisclosureDialog = MutableLiveData<AppDialog?>()
    private val _showFullDisclosure = MutableLiveData<ConsumableLiveEvent<Unit>>()

    override val blockingLoadingIndicator: LiveData<Boolean>
        get() = _blockingLoadingIndicator
    val errorMessage: LiveData<ConsumableLiveEvent<ErrorMessage>>
        get() = _errorMessage
    val showDisclosureDialog: LiveData<AppDialog?>
        get() = _showDisclosureDialog
    val showFullDisclosure: LiveData<ConsumableLiveEvent<Unit>>
        get() = _showFullDisclosure

    private var signingResultAction: ((isSigned: Boolean) -> Unit)? = null

    protected fun checkIfTrailingOrderAllowed(resultAction: (Boolean) -> Unit) {
        checkDisclosureState(
            conditionalOrderDisclosureRepo::isTrailingOrderAllowed,
            resultAction
        )
    }

    protected fun checkDisclosureState(resultAction: (Boolean) -> Unit) {
        checkDisclosureState(
            conditionalOrderDisclosureRepo::isDisclosureSigned,
            resultAction
        )
    }

    private fun checkDisclosureState(
        disclosureCheck: suspend () -> ETResult<Boolean>,
        resultAction: (Boolean) -> Unit
    ) {
        viewModelScope.launch {
            _blockingLoadingIndicator.value = true
            disclosureCheck()
                .onSuccess { isSigned ->
                    if (isSigned) {
                        resultAction(true)
                    } else {
                        signingResultAction = resultAction
                        showDisclosureDialog()
                    }
                }
                .onFailure {
                    logger.error("Unable to check disclosure status", it)
                    resultAction(false)
                    _errorMessage.value = ErrorMessage(resources.getString(R.string.error_message_general)).consumable()
                }
            _blockingLoadingIndicator.value = false
        }
    }

    private fun showDisclosureDialog() {
        _showDisclosureDialog.value = resources.appDialog {
            titleRes = R.string.conditional_order_disclosure_title
            message = createDisclosureMessage()
            cancelable = false
            resourcesNegativeAction {
                labelRes = R.string.i_disagree
                action = ::userDisagreedWithDisclosure
            }
            resourcesPositiveAction {
                labelRes = R.string.i_agree
                action = ::onUserAgreedWithDisclosure
            }
        }
    }

    private fun createDisclosureMessage(): CharSequence {
        val body = resources.getString(R.string.conditional_order_disclosure_message)
        val agreementName = resources.getString(R.string.conditional_order_disclosure_title)
        val spannableMessage = body.plus("\n\n").plus(agreementName)
        return SpannableString(spannableMessage).apply {
            setClickableSpan(spannableMessage.length - agreementName.length, spannableMessage.length) {
                _showFullDisclosure.value = Unit.consumable()
            }
        }
    }

    private fun onUserAgreedWithDisclosure() {
        _showDisclosureDialog.value = null
        viewModelScope.launch {
            val onErrorAction = {
                signingResultAction?.invoke(false)
                _errorMessage.value = ErrorMessage(
                    message = resources.getString(R.string.error_message_general)
                ).consumable()
            }
            _blockingLoadingIndicator.value = true
            conditionalOrderDisclosureRepo.signDisclosure()
                .onSuccess { messages ->
                    if (messages.isEmpty()) {
                        signingResultAction?.invoke(true)
                    } else {
                        onErrorAction()
                    }
                }
                .onFailure {
                    logger.error("Unable to sign disclosure", it)
                    onErrorAction()
                }
            _blockingLoadingIndicator.value = false
        }
    }

    private fun userDisagreedWithDisclosure() {
        _showDisclosureDialog.value = null
        signingResultAction?.invoke(false)
    }
}
