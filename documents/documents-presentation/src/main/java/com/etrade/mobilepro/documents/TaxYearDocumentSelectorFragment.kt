package com.etrade.mobilepro.documents

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.documents.databinding.DocumentsTaxYearSelectorViewBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder

private const val CURRENT_VALUE = "TaxYearDocumentSelectorFragment:current_value"

@RequireLogin
class TaxYearDocumentSelectorFragment : DialogFragment() {

    private val args: TaxYearDocumentSelectorFragmentArgs by navArgs()
    private var currentValue: Int = 0

    @SuppressLint("InflateParams")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        currentValue = savedInstanceState?.getInt(CURRENT_VALUE) ?: args.value

        val context = requireContext()
        val binding = DocumentsTaxYearSelectorViewBinding.inflate(LayoutInflater.from(context))
        val picker = binding.numberPicker
        val builder = MaterialAlertDialogBuilder(context)
            .setPositiveButton(R.string.ok) { _, _ ->
                findNavController().previousBackStackEntry?.savedStateHandle?.set<Int>(DOCUMENTS_TAX_YEAR, picker.value)

                dismiss()
            }
            .setNegativeButton(R.string.cancel) { _, _ -> dismiss() }

        picker.minValue = args.minYear
        picker.maxValue = args.maxYear
        picker.value = currentValue
        picker.setOnValueChangedListener { _, _, newVal -> currentValue = newVal }

        builder.setView(binding.root)

        return builder.create().also { it.setCanceledOnTouchOutside(false) }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(CURRENT_VALUE, currentValue)

        super.onSaveInstanceState(outState)
    }
}
