package com.etrade.mobilepro.transfermoney.presentation.common

import androidx.annotation.StringRes
import com.etrade.mobilepro.transfermoney.api.TransferRepeat
import com.etrade.mobilepro.transfermoney.presentation.R

@Suppress("ComplexMethod") // nothing special
@StringRes
fun TransferRepeat.getTitleId(): Int = when (this) {
    TransferRepeat.None -> R.string.transfer_repeat_none
    TransferRepeat.Week -> R.string.transfer_repeat_week
    TransferRepeat.TwoWeeks -> R.string.transfer_repeat_two_weeks
    TransferRepeat.FourWeeks -> R.string.transfer_repeat_four_weeks
    TransferRepeat.Month -> R.string.transfer_repeat_month
    TransferRepeat.EndOfMonth -> R.string.transfer_repeat_end_of_month
    TransferRepeat.TwoMonths -> R.string.transfer_repeat_two_months
    TransferRepeat.ThreeMonths -> R.string.transfer_repeat_three_months
    TransferRepeat.FourMonths -> R.string.transfer_repeat_four_months
    TransferRepeat.SixMonths -> R.string.transfer_repeat_six_months
    TransferRepeat.Year -> R.string.transfer_repeat_year
}
