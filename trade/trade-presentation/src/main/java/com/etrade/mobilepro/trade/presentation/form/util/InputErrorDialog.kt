package com.etrade.mobilepro.trade.presentation.form.util

import android.content.res.Resources
import com.etrade.mobilepro.dialog.appDialog

fun Resources.getInputFractionalErrorDialog() = appDialog {
    titleRes = com.etrade.mobilepro.dynamic.form.ui.R.string.title_trade_error
    message =
        getString(com.etrade.mobilepro.dynamic.form.ui.R.string.error_buy_fractional_not_allowed_message)
    cancelable = false
    resourcesPositiveAction { labelRes = com.etrade.mobilepro.dynamic.form.ui.R.string.ok }
}
