package com.etrade.mobilepro.session

import com.squareup.moshi.Moshi
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okhttp3.ResponseBody
import okhttp3.ResponseBody.Companion.toResponseBody
import java.net.HttpURLConnection.HTTP_FORBIDDEN
import java.net.HttpURLConnection.HTTP_UNAUTHORIZED

private const val REQUEST_HEADER_CSRF_TOKEN = "stk1"
private const val REQUEST_HEADER_DATA_TOKEN = "DataToken"
private const val RESPONSE_ERROR_MESSAGE_INVALID_DATA_TOKEN = "Invalid Data Token."
private const val HTTP_INVALID_DATA_TOKEN = 432
private const val PHOENIX_PATH = "/phx/etm/services"
private const val APPS_FLYER_HOST = "hq1.appsflyer.com"

class SessionValidationInterceptor(
    private val terminationNotifier: SessionTerminationStateProvider,
    private val moshi: Moshi
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)

        return validateResponseAndNotifySessionExpiration(request, response)
    }

    private fun validateResponseAndNotifySessionExpiration(
        request: Request,
        response: Response
    ): Response {
        var updatedResponse = response
        val responseCode = response.code
        val requestHeaders = request.headers.names()
        val getResponseContent: () -> String? = {
            response.body?.let { body ->
                body.string().also { content ->
                    updatedResponse = body.buildNewResponse(response, content)
                }
            }
        }

        if (isUnauthorizedErrorResponse(responseCode, request.url, requestHeaders) ||
            isInvalidDataTokenErrorResponse(responseCode, requestHeaders, getResponseContent)
        ) {
            terminationNotifier.notifySessionTermination()
        }

        return updatedResponse
    }

    private fun isUnauthorizedErrorResponse(
        responseCode: Int,
        url: HttpUrl,
        requestHeaders: Set<String>
    ) = (
        responseCode.isGeneralUnauthorizedResponseCode() ||
            responseCode.isPhoenixUnauthorizedResponseCode(url)
        ) && requestHeaders.isAuthorizedRequest() && !url.isAppsFlyerCall()

    private fun Set<String>.isAuthorizedRequest() = contains(REQUEST_HEADER_CSRF_TOKEN)

    private fun HttpUrl.isAppsFlyerCall() = this.host == APPS_FLYER_HOST

    private fun isInvalidDataTokenErrorResponse(
        responseCode: Int,
        requestHeaders: Set<String>,
        getResponseContent: () -> String?
    ) =
        responseCode.isInvalidDataTokenResponse(requestHeaders) && isInvalidDataTokenErrorMessage(
            getResponseContent()
        )

    private fun Int.isGeneralUnauthorizedResponseCode() = this == HTTP_UNAUTHORIZED

    private fun Int.isPhoenixUnauthorizedResponseCode(url: HttpUrl) =
        this == HTTP_FORBIDDEN && url.encodedPath.startsWith(PHOENIX_PATH)

    private fun Int.isInvalidDataTokenResponse(requestHeaders: Set<String>) =
        this == HTTP_INVALID_DATA_TOKEN &&
            requestHeaders.contains(REQUEST_HEADER_DATA_TOKEN)

    private fun ResponseBody.buildNewResponse(response: Response, content: String): Response {
        val newBody = content.toResponseBody(contentType())
        return response.newBuilder().body(newBody).build()
    }

    private fun isInvalidDataTokenErrorMessage(content: String?): Boolean {
        val adapter = moshi.adapter(DepositActivityErrorResponse::class.java)
        val errorMessage = content?.let {
            adapter.fromJson(it)?.error?.message
        }

        return errorMessage == RESPONSE_ERROR_MESSAGE_INVALID_DATA_TOKEN
    }
}
