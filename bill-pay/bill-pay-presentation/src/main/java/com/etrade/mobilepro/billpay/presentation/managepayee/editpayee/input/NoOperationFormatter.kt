package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input

import com.etrade.mobilepro.inputvalueview.InputFieldManager

class NoOperationFormatter : InputFieldManager.Formatter {
    override fun formatRawContent(rawContent: String) = rawContent
}
