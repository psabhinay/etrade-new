package com.etrade.mobilepro.alerts.presentation.base

import com.etrade.mobilepro.alerts.presentation.R

enum class AlertTab(val titleId: Int) {
    MARKET(R.string.alerts_market_title),
    ACCOUNT(R.string.alerts_account_title)
}
