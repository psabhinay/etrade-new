package com.etrade.mobilepro.streaming.positions.cache

import android.os.Handler
import android.os.Looper
import com.etrade.mobilepro.dao.PositionsDaoProvider
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.positions.dao.PortfolioPositionDao
import com.etrade.mobilepro.positions.dao.PositionRealmObject
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.OptionData
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.util.throttleLast
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.MainScope
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val THROTTLE_INTERVAL: Long = 500L

/**
 * Subscribes to streaming and updates [PortfolioPositionDao] on update from [StreamingQuoteProvider].
 */
class StreamingQuoteProviderWithCachedPositions @Inject constructor(
    daoProvider: PositionsDaoProvider,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>
) : StreamingQuoteProvider<Level1Field, Level1Data, Any> {

    private val positionsDao: PortfolioPositionDao by lazy {
        daoProvider.getPositionsDao()
    }
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)
    private val streams: ConcurrentHashMap<String, Stream> = ConcurrentHashMap()
    private val optionFields = OptionData.fields()
    private val stockFields = StockData.fields()

    private val positionsToUpdate: MutableList<PositionRealmObject> = mutableListOf()

    private val cachePositionThrottle = throttleLast<Unit>(
        coroutineScope = MainScope(),
        destinationFunction = {
            positionsDao.updatePositions(positionsToUpdate.toList())
            positionsToUpdate.clear()
        }
    )

    override fun getQuote(symbol: String, instrumentType: InstrumentType, fields: Set<Level1Field>, payload: Any?): Observable<Level1Data> {
        val streamKey = symbol + instrumentType.typeCode
        val stream = getStream(streamKey, symbol, instrumentType)
        return stream.source
            .doOnDispose {
                logger.debug("doOnDispose() called with symbol = $symbol")
                clearSource(streamKey)
            }
            .doOnTerminate {
                logger.debug("doOnTerminate() called with symbol = $symbol")
                clearSource(streamKey)
            }
    }

    private fun clearSource(streamKey: String) {
        val stream = streams.remove(streamKey)
        stream?.disposable?.dispose()
    }

    private fun getStream(streamKey: String, symbol: String, instrumentType: InstrumentType): Stream {
        return streams[streamKey] ?: createStream(symbol, instrumentType).also { stream ->
            val existingStream = streams.putIfAbsent(streamKey, stream)
            if (existingStream != null) {
                stream.disposable.dispose()
            }
        }
    }

    private fun createStream(symbol: String, instrumentType: InstrumentType): Stream {
        val fields = if (instrumentType.isOption) optionFields else stockFields
        val source = PublishSubject.create<Level1Data>().toSerialized()
        var positions: List<PositionRealmObject> = emptyList()
        val disposable = streamingQuoteProvider.getQuote(symbol, instrumentType, fields)
            .throttleLast(THROTTLE_INTERVAL, TimeUnit.MILLISECONDS)
            .doOnNext { source.onNext(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally {
                source.onComplete()
            }
            .doOnError {
                source.onError(it)
            }
            .subscribeBy { data ->
                positionsToUpdate.addAll(positions.updateWithLevel1Data(instrumentType, data))
                cachePositionThrottle(Unit)
            }

        return Stream(
            source.doOnSubscribe {
                // Post on the android main thread, Realm instance originally created on main thread.
                Handler(Looper.getMainLooper()).post {
                    positions = positionsDao.getPositionsWithSymbolNonAsync(symbol)
                }
            },
            disposable
        )
    }
}

private data class Stream(
    val source: Observable<Level1Data>,
    val disposable: Disposable
)
