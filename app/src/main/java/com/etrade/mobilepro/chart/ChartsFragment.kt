package com.etrade.mobilepro.chart

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.fragment.app.Fragment
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.databinding.FragmentChartsBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

@RequireLogin
class ChartsFragment @Inject constructor() : Fragment(R.layout.fragment_charts) {

    private val binding by viewBinding(FragmentChartsBinding::bind)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ChartPagerAdapter(this).attach(binding.tabLayout, binding.chartsViewPager)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
    }
}
