package com.etrade.mobilepro.watchlist.usecase

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import kotlinx.coroutines.rx2.await
import javax.inject.Inject

typealias IsExtendedHoursOn = suspend () -> Boolean

interface GetWatchlistEntriesUseCase : UseCase<String, ETResult<List<WatchlistEntry>>>

class GetWatchlistEntriesUseCaseImpl @Inject constructor(
    private val watchlistRepo: WatchlistRepo,
    private val isExtendedHoursOn: IsExtendedHoursOn
) : GetWatchlistEntriesUseCase {
    override suspend fun execute(parameter: String) = runCatchingET {
        watchlistRepo.getWatchlistEntries(watchlistId = parameter, isExtendedHours = isExtendedHoursOn())
            .await()
    }
}
