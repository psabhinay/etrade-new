package com.etrade.mobilepro.news.presentation.company

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.api.repo.NewsRepo
import com.etrade.mobilepro.news.presentation.base.BaseNewsViewModel
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentLoader
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsFontAdjustHandler
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.searchingapi.SearchInteractor
import com.etrade.mobilepro.searchingapi.SelectionHistoryRepo
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.usersession.api.LoginSignalHandler
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.SingleLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.android.recyclerviewutil.SelectionTracker
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val DEFAULT_INSTRUMENT_TYPE = InstrumentType.EQ
private const val QUOTE_TRADE_NAME = "search"

private val logger = LoggerFactory.getLogger(CompanyNewsViewModel::class.java)

class CompanyNewsViewModel @Inject constructor(
    newsRepo: NewsRepo,
    statusRepo: ReadStatusRepo,
    contentLoader: TextNewsContentLoader,
    fontAdjustHandler: TextNewsFontAdjustHandler,
    searchInteractor: SearchInteractor,
    selectionTracker: SelectionTracker,
    private val preferences: ApplicationPreferences,
    private val selectionHistoryRepo: SelectionHistoryRepo
) : BaseNewsViewModel(newsRepo, statusRepo, contentLoader, fontAdjustHandler, selectionTracker) {

    private val logger = LoggerFactory.getLogger(CompanyNewsViewModel::class.java)

    private val compositeDisposable = CompositeDisposable()
    private val _currentSymbol = MutableLiveData<SearchResultItem.Symbol?>()
    private val _needOpenQuoteLookup = SingleLiveEvent<ConsumableLiveEvent<Unit>>()
    private var prevSymbol: SearchResultItem.Symbol? = null

    val loginSignalHandler = LoginSignalHandler()
    val currentSymbol: LiveData<SearchResultItem.Symbol?> = _currentSymbol
    val needOpenQuoteLookup: LiveData<ConsumableLiveEvent<Unit>> = _needOpenQuoteLookup

    private val _setAlertSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val setAlertSignal: LiveData<ConsumableLiveEvent<Unit>> get() = _setAlertSignal

    private val _addToWatchlistSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val addToWatchlistSignal: LiveData<ConsumableLiveEvent<Unit>> get() = _addToWatchlistSignal

    private val _tradeClickedSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val tradeClickedSignal: LiveData<ConsumableLiveEvent<Unit>> get() = _tradeClickedSignal

    private var onSetAlertListener: () -> Unit = {
        _setAlertSignal.postValue(ConsumableLiveEvent(Unit))
    }
    private var onAddToWatchlistListener: () -> Unit = {
        _addToWatchlistSignal.postValue(ConsumableLiveEvent(Unit))
    }

    private var onTradeClickedListener: () -> Unit = {
        _tradeClickedSignal.postValue(ConsumableLiveEvent(Unit))
    }

    private var onSearchBarTapListener: () -> Unit = {
        _needOpenQuoteLookup.postValue(Unit.consumable())
    }

    private var passedInSymbol: SearchResultItem.Symbol? = null

    var quoteWidgetViewModel: QuotesWidgetViewModel? = null

    init {
        compositeDisposable.add(
            searchInteractor.searchResultItemUntilChangedAsObservable(searchType = SearchType.SYMBOL)
                .subscribeBy(
                    onNext = {
                        if (it is SearchResultItem.Symbol) {
                            _currentSymbol.postValue(it)
                        }
                    },
                    onError = {
                        logger.debug(it.localizedMessage)
                    }
                )
        )
    }

    override fun onBecomeActive() {
        val latestSymbol = passedInSymbol ?: selectionHistoryRepo.symbolSelectionHistory().firstOrNull()

        if (latestSymbol != null) {
            if (_currentSymbol.value == null) {
                _currentSymbol.value = latestSymbol
            }
        } else {
            _currentSymbol.value = provideDefaultSymbol()
            if (preferences.shouldDisplayQuoteLookupOnCompanyNewsTab) {
                preferences.shouldDisplayQuoteLookupOnCompanyNewsTab = false
                _needOpenQuoteLookup.value = Unit.consumable()
            }
        }
    }

    override fun getNewsType(): NewsType = NewsType.COMPANY

    override fun provideHeaderItemsSource(): Observable<List<NewsItem>>? {
        return Observable.fromCallable {
            quoteWidgetViewModel?.getQuotes(prevSymbol?.underlyerSymbol ?: provideDefaultSymbol().underlyerSymbol)
            listOf(
                NewsItem.CompanyHeader(
                    quoteWidgetData = quoteWidgetViewModel?.quote,
                    onSearchFieldTapListener = onSearchBarTapListener,
                    onSetAlertListener = onSetAlertListener,
                    onAddToWatchlistListener = onAddToWatchlistListener,
                    onTradeListener = onTradeClickedListener,
                    subsequentFirstItem = null
                )
            )
        }
    }

    override fun provideHeaderItemsCount(): Int = 1

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    @ExperimentalCoroutinesApi
    fun applySymbol(symbol: SearchResultItem.Symbol, forced: Boolean = false) {
        if (prevSymbol == symbol && !forced) {
            return
        }
        prevSymbol = symbol
        val newsRequest = NewsRequest.Symbol(listOf(symbol.underlyerSymbol))
        refreshWithParams(newsRequest)
    }

    fun passedInSymbol(symbol: SearchResultItem.Symbol) {
        passedInSymbol(symbol.underlyerSymbol)
    }

    fun passedInSymbol(symbol: String?) {
        passedInSymbol = when {
            symbol.isNullOrEmpty() -> null
            else -> SearchResultItem.Symbol(title = symbol, type = DEFAULT_INSTRUMENT_TYPE)
        }
    }

    private fun provideDefaultSymbol(): SearchResultItem.Symbol =
        SearchResultItem.Symbol(title = "MS", type = DEFAULT_INSTRUMENT_TYPE)
}
