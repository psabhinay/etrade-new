package com.etrade.mobilepro.messagecenter.presentation.details.service

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.messagecenter.api.model.ServiceMessage
import com.etrade.mobilepro.messagecenter.presentation.R
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterFragmentMessageServiceDetailsBinding
import com.etrade.mobilepro.messagecenter.presentation.messagelist.MessageCenterListViewModel
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createMultiLineSummarySection
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.android.textutil.setTextAndLinkify
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@RequireLogin
class ServiceMessageDetailsFragment @Inject constructor(
    viewModelProvider: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : OverlayBaseFragment() {

    private val binding by viewBinding(MessageCenterFragmentMessageServiceDetailsBinding::bind)

    override val layoutRes: Int = R.layout.message_center_fragment_message_service_details
    private val viewModel by viewModels<ServiceMessageDetailsViewModel> { viewModelProvider }
    private val messageListViewModel by navGraphViewModels<MessageCenterListViewModel>(R.id.message_center_nav_graph) { viewModelProvider }
    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private var snackbar: Snackbar? = null
    private val args by navArgs<ServiceMessageDetailsFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
        viewModel.refresh(args.message.toDomain(), forced = false)
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                handleLoadingState(it.loading)
                handleErrorState(it.error)
                handleSuccessState(it.item)
                if (it.needToRefreshList == true) {
                    messageListViewModel.refresh(forced = true)
                }
            }
        )
    }

    private fun handleLoadingState(state: ServiceMessageDetailsViewModel.ViewState.Loading) =
        when (state) {
            ServiceMessageDetailsViewModel.ViewState.Loading.NONE -> {
                binding.scrollView.visibility = View.VISIBLE
                binding.loadingPb.hide()
            }
            ServiceMessageDetailsViewModel.ViewState.Loading.INITIAL -> {
                binding.scrollView.visibility = View.GONE
                binding.loadingPb.show()
            }
        }

    private fun handleErrorState(state: ServiceMessageDetailsViewModel.ViewState.Error) =
        when (state) {
            ServiceMessageDetailsViewModel.ViewState.Error.None -> {
                snackbar?.dismiss()
                binding.divider.visibility = View.VISIBLE
                binding.serviceMessageTable.visibility = View.VISIBLE
            }
            is ServiceMessageDetailsViewModel.ViewState.Error.Loading -> {
                snackbar = snackBarUtil.retrySnackbar { state.retry.invoke() }
                binding.divider.visibility = View.GONE
                binding.serviceMessageTable.visibility = View.GONE
            }
        }

    private fun handleSuccessState(state: ServiceMessage) {
        binding.serviceMessageTable.setContent(generateTableSections(state))
        binding.messages.setTextAndLinkify(TextUtils.join("\n", (state.messages + state.errorDescription).filterNotNull()))
    }

    private fun generateTableSections(serviceMessage: ServiceMessage): List<TableSectionItemLayout> =
        serviceMessage.payload.map { entry ->
            val key = entry.key
            val value = entry.value
            when {
                key == ServiceMessage.ACCOUNT_KEY -> createSummarySection(
                    label = key,
                    value = value,
                    valueDescription = "$key ${context?.accountNameContentDescription(value)}"
                )
                value.containsOnlyDigits() -> createMultiLineSummarySection(
                    label = key,
                    value = value,
                    description = "$key ${value.characterByCharacter()}"
                )
                else -> createMultiLineSummarySection(label = key, value = value)
            }
        } + createSummarySection(label = getString(R.string.message_center_request), value = serviceMessage.subject)
}

private fun String.containsOnlyDigits() = toCharArray().all { it.isDigit() }
