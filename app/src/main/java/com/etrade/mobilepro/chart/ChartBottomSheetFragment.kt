package com.etrade.mobilepro.chart

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.R
import com.etrade.mobilepro.chart.linechart.dto.MarketChartViewDto
import com.etrade.mobilepro.common.di.ViewFactory
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.databinding.FragmentChartBottomSheetBinding
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.GenericRecyclerAdapter
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.toDynamicViewModels
import com.etrade.mobilepro.indices.dto.MarketIndicesViewDto
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import javax.inject.Inject

class ChartBottomSheetFragment @Inject constructor(
    private val mainNavigation: MainNavigation,
    @ViewFactory private val objectFactories: Map<Class<*>, @JvmSuppressWildcards DynamicUiViewFactory>
) : BottomSheetDialogFragment() {

    private val binding by viewBinding(FragmentChartBottomSheetBinding::bind)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.ActionSheetDialogFragment)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener {
                findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
                    ?.let { BottomSheetBehavior.from(it) }
                    ?.apply {
                        state = BottomSheetBehavior.STATE_EXPANDED
                    }
                setOnShowListener(null)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chart_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.indexList.apply {
            val viewModels = listOfNotNull(
                objectFactories[MarketIndicesViewDto::class.java]?.createView(MarketIndicesViewDto("", null)),
                objectFactories[MarketChartViewDto::class.java]?.createView(MarketChartViewDto("", null))
            ).toDynamicViewModels(this@ChartBottomSheetFragment) {
                if (it is LifecycleObserver) {
                    lifecycle.addObserver(it)
                }
                it.clickEvents.observe(viewLifecycleOwner, ctaObserver())
            }
            layoutManager = LinearLayoutManager(context)
            adapter = GenericRecyclerAdapter(viewLifecycleOwner, viewModels)
        }
    }

    private fun ctaObserver() = Observer<CtaEvent> { ctaEvent ->
        when (ctaEvent) {
            is CtaEvent.NavDirectionsNavigation -> {
                findNavController().navigate(ctaEvent.ctaDirections)
            }
            is CtaEvent.LaunchQuote -> {
                mainNavigation.navigateToQuoteDetails(activity, ctaEvent.bundle)
            }
            else -> {
                // NO OP
            }
        }
    }
}
