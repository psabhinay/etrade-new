package com.etrade.mobilepro.session.api

sealed class SessionStateChange {
    object AUTHENTICATED : SessionStateChange()
    object UNAUTHENTICATED : SessionStateChange()
}
