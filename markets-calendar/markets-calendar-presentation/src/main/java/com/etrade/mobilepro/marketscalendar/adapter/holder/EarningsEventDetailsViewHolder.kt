package com.etrade.mobilepro.marketscalendar.adapter.holder

import android.view.View
import com.etrade.mobilepro.marketscalendar.databinding.AdapterEarningsDetailsBinding
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails

class EarningsEventDetailsViewHolder(
    private val binding: AdapterEarningsDetailsBinding,
    noValue: String,
    private val navigation: () -> Unit
) : CalendarEventViewHolder(binding.adapterEventItem, noValue, rootView = binding.root) {

    override fun bind(item: EventDetails) {
        super.bind(item)
        binding.adapterEventItem.apply {
            icArrow.visibility = View.GONE
            divider.visibility = View.GONE
        }
        binding.earningsAction.setOnClickListener { navigation() }
    }
}
