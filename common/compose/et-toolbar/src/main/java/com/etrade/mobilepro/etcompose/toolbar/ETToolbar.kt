package com.etrade.mobilepro.etcompose.toolbar

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Info
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.FontSizes
import com.etrade.mobilepro.common.compose.theme.etColors

@Suppress("LongMethod")
@Composable
fun ETToolbar(
    title: String,
    actionStart: ToolbarAction? = null,
    actionEnd: ToolbarAction? = null
) {
    TopAppBar(
        modifier = Modifier.fillMaxWidth(),
        backgroundColor = MaterialTheme.etColors.plum,
        contentColor = Color.White
    ) {
        Box(Modifier.height(Dimens.Size32dp)) {
            actionStart?.let {
                Row(
                    modifier = Modifier.fillMaxSize(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    when (it) {
                        is ToolbarAction.IconAction -> CreateIconButton(it)
                        is ToolbarAction.TextAction -> CreateTextButton(it)
                    }
                }
            }
            Row(
                modifier = Modifier.fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Center,
                    fontWeight = FontWeight.Bold,
                    fontSize = FontSizes.Size18sp,
                    maxLines = 2,
                    text = title
                )
            }

            actionEnd?.let {
                Row(
                    modifier = Modifier.fillMaxSize(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.End
                ) {
                    when (it) {
                        is ToolbarAction.IconAction -> CreateIconButton(it)
                        is ToolbarAction.TextAction -> CreateTextButton(it)
                    }
                }
            }
        }
    }
}

@Composable
fun CreateIconButton(iconData: ToolbarAction.IconAction) {
    IconButton(
        onClick = iconData.onClick,
    ) { iconData.icon.invoke() }
}

@Composable
fun CreateTextButton(textData: ToolbarAction.TextAction) {
    TextButton(
        onClick = textData.onClick,
        colors = ButtonDefaults.textButtonColors(contentColor = Color.White)
    ) {
        Text(text = textData.text)
    }
}

@Preview
@Composable
fun ETToolbarPreview() {

    val actionEnd = ToolbarAction.IconAction(
        onClick = {},
        icon = { Icon(imageVector = Icons.Rounded.Info, contentDescription = "action end") }
    )

    val text = ToolbarAction.TextAction(
        onClick = {},
        text = "TEST"
    )

    ETToolbar(
        title = "Test",
        actionStart = text,
        actionEnd = actionEnd
    )
}
