package com.etrade.mobilepro.transfermoney.data.repo

import com.etrade.mobilepro.caching.coroutine.Cache
import com.etrade.mobilepro.caching.coroutine.CacheController
import com.etrade.mobilepro.caching.coroutine.DataSource
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.transfermoney.api.Holidays
import com.etrade.mobilepro.transfermoney.api.TransferCalendarHolidays
import com.etrade.mobilepro.transfermoney.api.repo.TransferCalendarRepo
import javax.inject.Inject

private val HOLIDAYS_ID = "${TransferCalendarRepoImpl::class.java.canonicalName}.holidays"

class TransferCalendarRepoImpl @Inject constructor(
    private val holidaysDataSource: DataSource<Holidays, String>,
    private val holidaysCache: Cache<Holidays, String>
) : TransferCalendarRepo {

    override suspend fun getCalendar(): ETResult<List<TransferCalendarHolidays>> {
        return CacheController(HOLIDAYS_ID, holidaysDataSource, holidaysCache).fetch()
    }
}
