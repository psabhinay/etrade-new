package com.etrade.mobilepro.ui.nestedrecyclerview

import android.content.Context
import android.util.AttributeSet
import com.evrencoskun.tableview.TableScrollView

class RestrictedTableScrollView : TableScrollView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val restrictedViewHelper: RestrictedViewHelper = RestrictedViewHelper()

    init {
        restrictedViewHelper.attachTo(this)
    }
}
