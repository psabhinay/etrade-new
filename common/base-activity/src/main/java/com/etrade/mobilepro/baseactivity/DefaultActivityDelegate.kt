package com.etrade.mobilepro.baseactivity

import android.app.Activity
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import com.etrade.mobilepro.application.ApplicationLifecycle
import com.etrade.mobilepro.common.marketTradeHaltBannerView
import com.etrade.mobilepro.common.networkUnavailableView
import com.etrade.mobilepro.session.api.SessionEvents
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(DefaultActivityDelegate::class.java)

class DefaultActivityDelegate @Inject constructor(
    private val userSessionHandler: UserSessionHandler,
    private val applicationLifecycleObserver: ApplicationLifecycle,
    private val connectionNotifier: ConnectionNotifier
) : ActivitySessionDelegate, ActivityNetworkConnectionDelegate {

    override fun observeTimeoutEvent(activity: AppCompatActivity) {
        userSessionHandler.sessionTerminateEvent.observe(
            activity,
            Observer {
                when (it) {
                    SessionEvents.MSG_SESSION_SAR_TERMINATED,
                    SessionEvents.MSG_SESSION_TERMINATED -> {
                        logger.debug("terminate user")
                        activity.finish()
                    }
                    else -> {
                    }
                }
            }
        )
    }

    override fun onUserInteraction(activity: Activity) {
        applicationLifecycleObserver.onUserInteraction(activity)
    }

    override fun initNetworkConnectivityBanner(activity: FragmentActivity) {
        val connectionCallback = object : ConnectionNotifier.ConnectionNotification {
            override fun onNetworkAvailable() {
                activity.runOnUiThread {
                    activity.networkUnavailableView?.visibility = View.GONE
                }
            }

            override fun onNetworkLost() {
                activity.runOnUiThread {
                    activity.networkUnavailableView?.visibility = View.VISIBLE
                    activity.marketTradeHaltBannerView?.visibility = View.GONE
                }
            }
        }

        connectionNotifier.createNotifier(connectionCallback)?.let {
            activity.lifecycle.addObserver(it)
        }
    }
}
