package com.etrade.mobilepro.tradingdefaults.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.tradingdefaults.TradingDefaultsItem
import com.etrade.mobilepro.tradingdefaults.viewholder.TradingDefaultsDividerViewHolder
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class TradingDefaultsDividerAdapterDelegate :
    AbsListItemAdapterDelegate<TradingDefaultsItem.Divider, TradingDefaultsItem, TradingDefaultsDividerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): TradingDefaultsDividerViewHolder {
        return TradingDefaultsDividerViewHolder.from(parent)
    }

    override fun isForViewType(item: TradingDefaultsItem, items: MutableList<TradingDefaultsItem>, position: Int): Boolean = item is TradingDefaultsItem.Divider

    override fun onBindViewHolder(item: TradingDefaultsItem.Divider, holder: TradingDefaultsDividerViewHolder, payloads: MutableList<Any>) {
        // nothing to bind
    }
}
