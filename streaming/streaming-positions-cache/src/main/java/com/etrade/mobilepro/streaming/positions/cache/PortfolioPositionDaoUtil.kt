package com.etrade.mobilepro.streaming.positions.cache

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.pnl.calculations.calculateDaysGain
import com.etrade.mobilepro.pnl.calculations.calculateTotalGain
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.positions.dao.PositionRealmObject
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.util.domain.data.deriveMarketValue
import com.etrade.mobilepro.util.safeParseBigDecimal

@Suppress("UNCHECKED_CAST")
fun List<Position>.updateWithLevel1Data(instrumentType: InstrumentType, data: Level1Data): List<PositionRealmObject> {
    return (this as List<PositionRealmObject>).map { it.updateWithLevel1Data(instrumentType, data) }
}

@Suppress("LongMethod")
private fun PositionRealmObject.updateWithLevel1Data(instrumentType: InstrumentType, data: Level1Data): PositionRealmObject {
    if (!instrumentType.updateWithStreamingData) {
        return this
    }
    val updatedPrice: String
    val previousClosePrice: String

    if (instrumentType.isOption) {
        updatedPrice = data.mark.orEmpty()
        previousClosePrice = data.closingMark.orEmpty()
    } else {
        updatedPrice = data.lastPrice.orEmpty()
        previousClosePrice = data.previousClosePrice.orEmpty()
    }

    val lastPriceDecimal = updatedPrice.safeParseBigDecimal(addToException = "Last price for $instrumentType:${this.symbol}")
    val previousCloseDecimal = previousClosePrice.safeParseBigDecimal(addToException = "Prev close price for $instrumentType:${this.symbol}")
    val quantityDecimal = quantityRaw.safeParseBigDecimal()
    val todayQuantityDecimal = todayQuantity.safeParseBigDecimal()
    val todayPricePaidDecimal = todayPricePaid.safeParseBigDecimal()
    val todayCommissionsDecimal = todayCommissions.safeParseBigDecimal()
    val feesDecimal = feesRaw.safeParseBigDecimal()
    val pricePaidDecimal = pricePaidRaw.safeParseBigDecimal()

    val daysGain = calculateDaysGain(
        instrumentType,
        lastPriceDecimal,
        previousCloseDecimal,
        quantityDecimal,
        todayQuantityDecimal,
        todayPricePaidDecimal,
        todayCommissionsDecimal,
        feesDecimal,
        basisPrice.safeParseBigDecimal()
    )

    val totalGain = calculateTotalGain(
        instrumentType,
        lastPriceDecimal,
        pricePaidDecimal,
        quantityDecimal,
        commission,
        feesDecimal
    )

    val quantity = this.quantity
    val marketValue = if (lastPriceDecimal != null && quantity != null) {
        deriveMarketValue(lastPriceDecimal, quantity)
    } else {
        null
    }

    return copy(
        daysGainValue = daysGain?.dollar?.toPlainString() ?: this.daysGainValue,
        daysGainPercentageRaw = daysGain?.percent?.toPlainString() ?: this.daysGainPercentageRaw,
        totalGainValue = totalGain?.dollar?.toPlainString() ?: this.totalGainValue,
        totalGainPercentageRaw = totalGain?.percent?.toPlainString() ?: this.totalGainPercentageRaw,
        markToMarket = updatedPrice.takeIf { it.isNotEmpty() } ?: this.markToMarket,
        marketValueRaw = marketValue?.toPlainString() ?: ""
    )
}
