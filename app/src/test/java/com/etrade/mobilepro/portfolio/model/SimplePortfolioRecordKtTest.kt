package com.etrade.mobilepro.portfolio.model

import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.positions.data.baseValueExtractors
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.positions.data.epochDay
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

class SimplePortfolioRecordKtTest {

    @Test
    fun `validates the comparable for symbol name is the symbol itself for portfolio position`() {
        // When
        val comp = baseValueExtractors[PortfolioRecordColumn.SYMBOL]

        // Given
        val sut = getPortfolioRecord("AAPL")

        // Then
        assertEquals(comp?.invoke(sut), sut.displayName)
    }

    @Test
    fun `validate the comparable is adjusted display name for taxLot position`() {
        // When
        val comp = baseValueExtractors[PortfolioRecordColumn.SYMBOL]

        // Given
        val sut = getPortfolioRecord("07/04/18", true)

        // Then
        assertEquals(comp?.invoke(sut), sut.displayName.epochDay)
    }

    private fun getPortfolioRecord(
        displayName: String,
        isTaxLot: Boolean = false
    ): PortfolioRecord {
        return object : PortfolioRecord {
            override val recordId: String = "12345"
            override val displayName: String = displayName
            override val isTaxLot: Boolean = isTaxLot
            override val position: Position = mock()
            override val instrument: Instrument = mock()
        }
    }
}
