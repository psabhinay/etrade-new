package com.etrade.mobilepro.watchlist

import com.etrade.mobilepro.watchlistapi.WatchListDeleted
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.WatchlistEntryAdded
import com.etrade.mobilepro.watchlistapi.WatchlistEventListener
import com.etrade.mobilepro.watchlistapi.WatchlistEventNotifier
import com.etrade.mobilepro.watchlistapi.WatchlistUpdated
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import io.reactivex.Single
import java.util.Collections
import javax.inject.Inject

class WatchlistEventNotifierImpl @Inject constructor(
    private val watchlistRepo: WatchlistRepo
) : WatchlistRepo by watchlistRepo, WatchlistEventNotifier {

    private val listeners = Collections.synchronizedSet(mutableSetOf<WatchlistEventListener>())

    override fun addListener(listener: WatchlistEventListener) {
        listeners.add(listener)
    }

    override fun removeListener(listener: WatchlistEventListener) {
        listeners.remove(listener)
    }

    override fun addToWatchList(watchlistId: String, ticker: String): Single<Boolean> {
        return watchlistRepo
            .addToWatchList(watchlistId, ticker)
            .doOnSuccess { isSuccess ->
                if (isSuccess) {
                    listeners.forEach { it.onWatchlistEvent(WatchlistEntryAdded) }
                }
            }
    }

    override fun saveWatchList(watchlistId: String, entries: List<WatchlistEntry>, removedEntries: List<WatchlistEntry>): Single<Boolean> {
        return watchlistRepo
            .saveWatchList(watchlistId, entries, removedEntries)
            .doOnSuccess { isSuccess ->
                if (isSuccess) {
                    listeners.forEach { it.onWatchlistEvent(WatchlistUpdated) }
                }
            }
    }

    override fun deleteWatchList(watchlistId: String): Single<Boolean> {
        return watchlistRepo
            .deleteWatchList(watchlistId)
            .doOnSuccess { isSuccess ->
                if (isSuccess) {
                    listeners.forEach { it.onWatchlistEvent(WatchListDeleted(watchlistId)) }
                }
            }
    }
}
