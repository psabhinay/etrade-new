package com.etrade.mobilepro.chat.api

import com.etrade.mobilepro.chat.api.setup.models.ChatProductItemsModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatCustomerDetailsModel

data class ChatParamsModel(
    var chatCustomerDetailsModel: ChatCustomerDetailsModel? = null,
    var chatProductItems: ChatProductItemsModel? = null,
    var chatContactId: String = "-1",
    var metadata: ChatMetadataModel? = null
)
