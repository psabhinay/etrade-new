package com.etrade.mobilepro.login.api.repo

import com.etrade.mobilepro.common.result.ETResult

/**
 * This interface can be used to session related backend communication.
 */
interface LogoutRepo {

    /**
     * This function can be used to log out from E-TRADE systems.
     *
     *  that's methods will be called
     * to notify your application with the result of the logout call.
     *
     */
    suspend fun logout(): ETResult<Unit>
}
