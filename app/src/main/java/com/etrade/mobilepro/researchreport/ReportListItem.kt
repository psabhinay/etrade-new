package com.etrade.mobilepro.researchreport

import com.etrade.mobilepro.mod.api.Report

sealed class ReportListItem {
    data class ReportItem(val report: Report) : ReportListItem()
    object DisclosuresItem : ReportListItem()
}
