package com.etrade.mobilepro.dynamicui.dto.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class TaxDocumentsRequestDto(
    @Json(name = "taxDocuments")
    val taxDocuments: TaxDocumentsDto
)

@JsonClass(generateAdapter = true)
class TaxDocumentsDto(
    @Json(name = "accountUuid")
    val accountUuid: String,
    @Json(name = "year")
    val year: String,
    @Json(name = "appVersion")
    val appVersion: String
)
