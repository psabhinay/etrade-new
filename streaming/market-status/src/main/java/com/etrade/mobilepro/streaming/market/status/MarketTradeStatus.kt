package com.etrade.mobilepro.streaming.market.status

class MarketTradeStatus(val isMarketHalted: Boolean = false, val reason: String? = null)
