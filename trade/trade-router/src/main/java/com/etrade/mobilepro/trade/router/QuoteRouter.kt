package com.etrade.mobilepro.trade.router

import androidx.navigation.NavController

interface QuoteRouter {
    fun popQuotePage(navController: NavController, inclusive: Boolean = false)

    fun popQuotePageWithMessage(
        navController: NavController,
        inclusive: Boolean = false,
        message: String
    )
}
