package com.etrade.mobilepro.onboarding

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.onboarding.databinding.OnboardingPageViewBinding
import com.etrade.mobilepro.util.android.extension.layoutInflater

class OnboardingPageView : FrameLayout {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val binding = OnboardingPageViewBinding.inflate(layoutInflater, this, true)

    fun displayData(onboardingPage: OnboardingPage, imageLoader: ImageLoader) {
        setFields(onboardingPage.title, onboardingPage.subtitle, onboardingPage.subtitleColor)
        imageLoader.show(onboardingPage.image, binding.image)
    }

    private fun setFields(@StringRes titleRes: Int?, @StringRes subtitleRes: Int, @ColorRes subtitleColorRes: Int) {
        with(binding.title) {
            if (titleRes == null) {
                visibility = View.GONE
            } else {
                visibility = View.VISIBLE
                setText(titleRes)
            }
        }

        with(binding.subtitle) {
            setText(subtitleRes)
            setTextColor(ContextCompat.getColor(context, subtitleColorRes))
        }
    }
}
