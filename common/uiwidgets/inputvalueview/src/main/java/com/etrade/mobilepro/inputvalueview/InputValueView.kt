package com.etrade.mobilepro.inputvalueview

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.material.textfield.TextInputLayout

class InputValueView : TextInputLayout {

    constructor(ctx: Context) : super(ctx)
    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)
    private val _lastCompleteValue = MutableLiveData<String>()
    private val inputFieldManager = InputFieldManager(_lastCompleteValue)
    val lastCompleteValue: LiveData<String>
        get() = _lastCompleteValue

    fun setupView(
        dependencies: InputFieldManager.Dependencies,
        rawContentData: MutableLiveData<String>,
        validatedData: MutableLiveData<InputFieldManager.Value> = MutableLiveData()
    ) {
        inputFieldManager.setupView(editText, this, dependencies, rawContentData, validatedData)
    }

    fun getValue(): InputFieldManager.Value = inputFieldManager.getValue()

    fun formatContents() {
        inputFieldManager.formatContents()
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        @ColorRes val color = if (enabled) {
            R.color.hint_color
        } else {
            R.color.hint_color_disabled
        }
        defaultHintTextColor = ColorStateList.valueOf(ContextCompat.getColor(context, color))
    }
}
