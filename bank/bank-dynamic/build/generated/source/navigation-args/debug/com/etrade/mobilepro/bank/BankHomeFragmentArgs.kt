package com.etrade.mobilepro.bank

import android.os.Bundle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.String
import kotlin.jvm.JvmStatic

public data class BankHomeFragmentArgs(
  public val accountUuid: String,
  public val bankAccountId: String
) : NavArgs {
  public fun toBundle(): Bundle {
    val result = Bundle()
    result.putString("accountUuid", this.accountUuid)
    result.putString("bankAccountId", this.bankAccountId)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): BankHomeFragmentArgs {
      bundle.setClassLoader(BankHomeFragmentArgs::class.java.classLoader)
      val __accountUuid : String?
      if (bundle.containsKey("accountUuid")) {
        __accountUuid = bundle.getString("accountUuid")
        if (__accountUuid == null) {
          throw IllegalArgumentException("Argument \"accountUuid\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"accountUuid\" is missing and does not have an android:defaultValue")
      }
      val __bankAccountId : String?
      if (bundle.containsKey("bankAccountId")) {
        __bankAccountId = bundle.getString("bankAccountId")
        if (__bankAccountId == null) {
          throw IllegalArgumentException("Argument \"bankAccountId\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"bankAccountId\" is missing and does not have an android:defaultValue")
      }
      return BankHomeFragmentArgs(__accountUuid, __bankAccountId)
    }
  }
}
