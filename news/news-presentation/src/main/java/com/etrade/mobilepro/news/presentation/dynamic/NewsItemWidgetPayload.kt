package com.etrade.mobilepro.news.presentation.dynamic

import android.view.View
import androidx.navigation.NavController
import com.etrade.mobilepro.news.api.model.NewsItem

class NewsItemWidgetPayload(
    val item: NewsItem,
    var wasRead: Boolean,
    val itemSelectListener: ((NavController?, NewsItem.Text, Int) -> Unit)?,
    val symbolSelectListener: ((View, String) -> Unit)?
)

fun List<NewsItemWidgetPayload>.markAsRead(item: NewsItem): List<NewsItemWidgetPayload> = this.apply {
    find { it.item == item }?.let {
        it.wasRead = true
    }
}
