package com.etrade.mobilepro.quoteapi

import com.etrade.mobilepro.streaming.api.Level1Data

interface QuoteItemInterface {
    fun updateItem(data: Level1Data, isExtendedHours: Boolean = false) = Unit
}
