package com.etrade.mobilepro.trade.presentation.form.approval

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.dynamic.form.api.Component
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.trade.approvallevel.ApprovalLevelDataPoint

class ApprovalLinkComponent(
    val approvalLevelDataPoint: LiveData<ApprovalLevelDataPoint>,
    val onActionClickListener: OnActionClickListener,
    override val isVisible: LiveData<Boolean> = MutableLiveData(true)
) : Component
