package com.etrade.mobilepro.tracking.params

import com.etrade.mobilepro.alerts.api.model.NewsAlertValueType
import com.etrade.mobilepro.tracking.EVENT_ATTRIBUTE_CUSTOMER_ID
import com.etrade.mobilepro.tracking.EVENT_ATTRIBUTE_SYMBOL
import com.etrade.mobilepro.tracking.trackerValue

private const val EVENT_NAME = "Set News Alert"

private const val EVENT_ATTRIBUTE_UPGRADED_DOWNGRADED = "Upgraded/Downgraded"
private const val EVENT_ATTRIBUTE_IN_PLAY = "In Play"
private const val EVENT_ATTRIBUTE_EARNINGS = "Earnings"
private const val EVENT_ATTRIBUTE_SPLIT_NOTICE = "Split Notice"

class SetNewsAlertAnalyticsParams(
    private val symbol: String,
    private val customerId: String,
    private val attributes: Map<NewsAlertValueType, Boolean>
) : AnalyticsParams {

    override val name: String = EVENT_NAME

    override val params: Map<String, String>
        get() = mapOf(
            EVENT_ATTRIBUTE_SYMBOL to symbol,
            EVENT_ATTRIBUTE_CUSTOMER_ID to customerId,
            EVENT_ATTRIBUTE_UPGRADED_DOWNGRADED to getAttributeValue(NewsAlertValueType.UP_DOWN),
            EVENT_ATTRIBUTE_IN_PLAY to getAttributeValue(NewsAlertValueType.IN_PLAY),
            EVENT_ATTRIBUTE_EARNINGS to getAttributeValue(NewsAlertValueType.EARNINGS),
            EVENT_ATTRIBUTE_SPLIT_NOTICE to getAttributeValue(NewsAlertValueType.SPLIT_NOTICE)
        )

    private fun getAttributeValue(key: NewsAlertValueType): String = (attributes[key] ?: false).trackerValue
}
