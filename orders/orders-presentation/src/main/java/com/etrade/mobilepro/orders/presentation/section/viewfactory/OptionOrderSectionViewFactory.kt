package com.etrade.mobilepro.orders.presentation.section.viewfactory

import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection

class OptionOrderSectionViewFactory : OrderDetailSectionViewFactory {
    override fun createSectionView(
        detailsDto: BaseOrderDetailsItem.OrderDetailsItem,
        resources: OrderDetailLabelResources,
        isConditionalOrder: Boolean
    ): List<TableSectionItemLayout> {
        val sections = mutableListOf<TableSectionItemLayout>()
        val stockResources = resources.defaultResources.resource
        sections.addAll(getDatesSections(detailsDto, resources, !isConditionalOrder))
        sections.addAll(getSavedInfo(detailsDto, resources, !isConditionalOrder))
        sections.add(createSummarySection(resources.stock.orderType, stockResources.getString(detailsDto.orderAction.getLabel())))
        if (isConditionalOrder || !detailsDto.hasPartialExecution) {
            sections.add(
                getQuantity(
                    detailsDto.quantity, detailsDto.execInstruction, isConditionalOrder,
                    resources, detailsDto.advancedItem?.orderType, detailsDto.term
                )
            )
        }
        sections.addAll(getPriceSections(detailsDto, resources, isConditionalOrder))
        return sections
    }
}
