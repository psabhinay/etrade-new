package com.etrade.mobilepro.trade.api.advanced

interface ConfirmedAdvancedStockOrder : AdvancedStockOrder, ConfirmedAdvancedOrder
