package com.etrade.mobilepro.screener.api.datasource

import com.etrade.mobilepro.screener.api.Industry
import com.etrade.mobilepro.screener.api.Sector

interface SectorDataSource {
    fun getAllSectorsItem(): Sector
    fun getAllIndustriesItem(): Industry
    fun getSectors(): List<Sector>
    fun getIndustriesBySectorId(sectorId: String): List<Industry>
}
