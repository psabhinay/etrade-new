package com.etrade.mobilepro.indices.usecase

import androidx.annotation.ColorRes
import com.etrade.mobilepro.common.di.DowJonesColor
import com.etrade.mobilepro.common.di.NasdaqColor
import com.etrade.mobilepro.common.di.SP500Color
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.indices.IndexDataParameter
import com.etrade.mobilepro.indices.IndexDataWithColor
import com.etrade.mobilepro.indices.createIndexDataWithColor
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.data.response.Quote
import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.util.android.ChangeColorPicker
import com.etrade.mobilepro.util.safeLet
import com.etrade.mobilepro.util.safeParseBigDecimal
import kotlinx.coroutines.rx2.await
import javax.inject.Inject

interface GetTopIndicesUseCase : UseCase<Int, ETResult<List<IndexDataWithColor>>>

class GetTopIndicesUseCaseImpl @Inject constructor(
    private val moverService: MoverService,
    private val changeColorPicker: ChangeColorPicker,
    @ColorRes @DowJonesColor private val dowJonesColor: Int,
    @ColorRes @NasdaqColor private val nasdaqColor: Int,
    @ColorRes @SP500Color private val sP500Color: Int
) : GetTopIndicesUseCase {

    private val indexColors: Array<Int> = arrayOf(dowJonesColor, nasdaqColor, sP500Color)

    override suspend fun execute(parameter: Int): ETResult<List<IndexDataWithColor>> {
        return moverService.getMarketIndices()
            .runCatchingET { await() }
            .mapCatching { response ->
                val quotes = response.quotes
                if (quotes == null || quotes.size < parameter) {
                    throw IllegalStateException("Not enough indices returned ${quotes?.size} < $parameter")
                }
                quotes.take(parameter).mapIndexedNotNull { index, quote ->
                    safeLet(quote.symbol, quote.price, quote.symbolDesc) { symbol, price, symbolDesc ->
                        createIndexDataWithColor(
                            IndexDataParameter(
                                symbol = symbol,
                                symbolDesc = symbolDesc,
                                price = price,
                                change = quote.change,
                                percentChange = quote.percentChange,
                                indicatorColor = getColorForQuoteAtIndex(quote, index),
                                changeColorPicker = changeColorPicker
                            )
                        )
                    }
                }
            }
    }

    @ColorRes
    private fun getColorForQuoteAtIndex(quote: Quote, index: Int): Int {
        return if (index < indexColors.size) {
            indexColors[index]
        } else {
            val change = quote.change?.safeParseBigDecimal()
            changeColorPicker.getChangeColor(change)
        }
    }
}
