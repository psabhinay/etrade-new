package com.etrade.mobilepro.dynamicui

import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory

interface DynamicEnum<T> {
    val dtoName: String
    val dtoClass: Class<out T>
}

tailrec fun <T> addSubtypes(
    factory: PolymorphicJsonAdapterFactory<T>,
    enumTypes: List<DynamicEnum<out T>>,
    index: Int = 0
): PolymorphicJsonAdapterFactory<T> {
    if (index >= enumTypes.size) return factory
    val type = enumTypes[index]
    return addSubtypes(factory.withSubtype(type.dtoClass, type.dtoName), enumTypes, index + 1)
}
