package com.etrade.mobilepro.portfolio.edit

import android.content.Intent
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode

private const val EXTRA_POSITION_LIST_MODE = "positionListMode"
private const val EXTRA_PORTFOLIO_TABLE_VIEW_TYPE = "portfolioTableViewType"

internal var Intent.positionTableMode: PositionTableMode
    get() = getSerializableExtra(EXTRA_POSITION_LIST_MODE) as PositionTableMode
    set(value) {
        putExtra(EXTRA_POSITION_LIST_MODE, value)
    }

internal var Intent.portfolioTableViewType: PortfolioTableViewType
    get() = getSerializableExtra(EXTRA_PORTFOLIO_TABLE_VIEW_TYPE) as PortfolioTableViewType
    set(value) {
        putExtra(EXTRA_PORTFOLIO_TABLE_VIEW_TYPE, value)
    }
