package com.etrade.mobilepro.chart.data

import com.etrade.mobilepro.chart.model.ChartTimeFrame

/**
 * Parameters for [GetChartLineDataUseCase]. An implementation of the use case may or may not use all of the provided parameters.
 *
 * @param indexes List of indexes (for example: SPX for S&P 500 or MS for Morgan Stanley) with corresponding chart params. Can be empty.
 * @param accounts List of account IDs (for example: 12345678) with corresponding chart params. Can be empty.
 * @param chartTimeFrame Time frame of a chart.
 */
class GetChartLineDataParameter(
    val indexes: List<ChartParams>,
    val accounts: List<ChartParams>,
    val chartTimeFrame: ChartTimeFrame
)
