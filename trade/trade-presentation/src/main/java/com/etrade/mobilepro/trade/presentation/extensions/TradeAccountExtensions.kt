package com.etrade.mobilepro.trade.presentation.extensions

import android.content.res.Resources
import androidx.annotation.StringRes
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.balance.api.AccountBalance
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.presentation.AccountDataPoint
import com.etrade.mobilepro.trade.presentation.R
import java.math.BigDecimal
import java.util.LinkedList

internal fun AccountBalance.toDataPoints(resources: Resources, accountMode: AccountMode): List<AccountDataPoint> {
    val result = LinkedList<AccountDataPoint>()
    result += createAccountDataPoint(
        resources,
        R.string.trade_label_account_value,
        accountValue
    )

    if (isPatternDayTrader && accountMode == AccountMode.MARGIN) {
        result += createAccountDataPoint(resources, R.string.trade_label_intraday_marginable, intradayMargin)
        result += createAccountDataPoint(resources, R.string.trade_label_intraday_non_marginable, intradayNonMargin)
        result += createAccountDataPoint(resources, R.string.trade_label_overnight_marginable, buyingPower)
        result += createAccountDataPoint(resources, R.string.trade_label_overnight_non_marginable, totalAvailableForWithdrawal)
    } else {
        when (accountMode) {
            AccountMode.MARGIN -> {
                result += createAccountDataPoint(resources, R.string.trade_label_margin_purchasing_power, buyingPower)
                result += createAccountDataPoint(resources, R.string.trade_label_non_margin_purchasing_power, totalAvailableForWithdrawal)
            }
            AccountMode.PORTFOLIO_MARGIN -> {
                result += createAccountDataPoint(resources, R.string.trade_label_available_excess_equity, availableExcessEquity)
            }
            else -> {
                result += createAccountDataPoint(resources, R.string.trade_label_cash_available_for_investment, cashAvailableForInvestment)
            }
        }
    }

    return result
}

internal fun TradeAccount.toDropDownOption(): DropDownOption = DropDownOption(id, name)

private fun createAccountDataPoint(resources: Resources, @StringRes labelResId: Int, value: BigDecimal?): AccountDataPoint {
    return AccountDataPoint(
        resources.getString(labelResId),
        MarketDataFormatter.formatMarketDataWithCurrency(value)
    )
}
