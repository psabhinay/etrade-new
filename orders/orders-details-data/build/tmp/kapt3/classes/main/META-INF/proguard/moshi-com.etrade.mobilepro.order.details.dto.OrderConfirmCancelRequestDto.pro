-if class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequestDto
-keepnames class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequestDto
-if class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequestDto
-keep class com.etrade.mobilepro.order.details.dto.OrderConfirmCancelRequestDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
