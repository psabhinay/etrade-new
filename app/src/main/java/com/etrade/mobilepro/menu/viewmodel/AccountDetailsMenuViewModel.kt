package com.etrade.mobilepro.menu.viewmodel

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.builder.menuPage
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

class AccountDetailsMenuViewModel @Inject constructor(
    private val accountDaoProvider: AccountDaoProvider,
    tracker: Tracker,
    resources: Resources
) : AppBaseMenuPageViewModel(resources, tracker) {

    private val uuidMap = ConcurrentHashMap<String, Int>()
    private val nextId = AtomicInteger(0)

    private fun getIdForUuid(uuid: String): Int {
        return uuidMap.computeIfAbsent(uuid) { nextId.getAndIncrement() }
    }

    override val menuPageItems: LiveData<List<MenuItem>> by lazy {
        accountDaoProvider.getAccountDao().getAllAccounts().map(this::getMenuPageItems)
    }

    private fun getMenuPageItems(accounts: List<Account>): List<MenuItem> = menuPage {
        accounts.forEach { account ->
            textItem(getIdForUuid(account.accountUuid)) {
                text = account.accountLongName
                contentDescription = resources.accountNameContentDescription(text)
            }
        }
    }
}
