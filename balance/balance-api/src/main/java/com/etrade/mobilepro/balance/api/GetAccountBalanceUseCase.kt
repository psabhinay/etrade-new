package com.etrade.mobilepro.balance.api

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.util.UseCase

interface GetAccountBalanceUseCase : UseCase<GetAccountBalanceParameter, ETResult<AccountBalance>>

class GetAccountBalanceParameter(
    val accountId: String,
    val forceRefresh: Boolean
)
