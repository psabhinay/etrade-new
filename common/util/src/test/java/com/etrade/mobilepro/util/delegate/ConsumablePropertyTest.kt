package com.etrade.mobilepro.util.delegate

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Test

private const val TEST_STRING = "TEST"

internal class ConsumablePropertyTest {

    @Test
    fun `does not return the consumable property more than once`() {
        val test by consumable(TEST_STRING)
        assertThat(test, equalTo(TEST_STRING))
        assertThat(test, `is`(nullValue()))
        assertThat(test, `is`(nullValue()))
    }

    @Test
    fun `new value resets the consumption state`() {
        var test by consumable(TEST_STRING)
        assertThat(test, equalTo(TEST_STRING))
        assertThat(test, `is`(nullValue()))

        val newValue = "$TEST_STRING!"
        test = newValue
        assertThat(test, equalTo(newValue))
        assertThat(test, `is`(nullValue()))
    }

    @Test
    fun `handles null`() {
        val test by consumable(null)
        assertThat(test, `is`(nullValue()))
        assertThat(test, `is`(nullValue()))
    }
}
