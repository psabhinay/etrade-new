package com.etrade.mobilepro.alerts.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AlertsCountDto(
    @Json(name = "data")
    val data: Data
)

@JsonClass(generateAdapter = true)
data class Data(
    @Json(name = "alertInbox")
    val alertInbox: AlertInbox
)

@JsonClass(generateAdapter = true)
data class AlertInbox(
    @Json(name = "numberOfAlerts")
    val numberOfAlerts: Int
)
