package com.etrade.mobilepro.billpay.presentation

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import androidx.viewpager2.widget.ViewPager2
import com.etrade.mobilepro.billpay.api.BillPayStatus
import com.etrade.mobilepro.billpay.presentation.addpayment.AddPaymentFragment
import com.etrade.mobilepro.billpay.presentation.common.NavigationViewModel
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayFragmentContainerBinding
import com.etrade.mobilepro.billpay.presentation.managepayee.ManagePayeeFragment
import com.etrade.mobilepro.billpay.presentation.paymentlist.PaymentListFragment
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.RestoreTabPositionDelegate
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.tracking.initTabsTracking
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.adapter.DefaultFragmentPagerAdapter
import com.etrade.mobilepro.util.android.adapter.DefaultFragmentPagerAdapter.FragmentPagerItem
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.setContentDescriptionForTabs
import com.etrade.mobilepro.util.android.extension.setupInnerClickListenersForAccessibility
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject
import com.etrade.mobilepro.common.R as pool

private const val NOT_ENROLLED_TAG = "not_enrolled_dialog"
private const val NOT_ELIGIBLE_TAG = "not_eligible_dialog"
private const val OK_DIALOG_REQUEST_CODE = 133

const val ADD_PAYMENT_TAB_INDEX = 0
const val PAYMENT_LIST_TAB_INDEX = 1
const val MANAGE_PAYEE_TAB_INDEX = 2

private val billPayFragments = listOf(
    FragmentPagerItem(AddPaymentFragment::class.java, R.string.bill_pay_add_payment_title),
    FragmentPagerItem(PaymentListFragment::class.java, R.string.bill_pay_payment_list_title),
    FragmentPagerItem(ManagePayeeFragment::class.java, R.string.bill_pay_manage_payees_title)
)

@RequireLogin
class BillPayContainerFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val restoreTabPositionDelegate: RestoreTabPositionDelegate
) : Fragment(R.layout.bill_pay_fragment_container) {

    private val binding by viewBinding(BillPayFragmentContainerBinding::bind) { onDestroyBinding() }

    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.bill_pay_nav_graph)
    private val viewModel: BillPayViewModel by navGraphViewModels(R.id.bill_pay_nav_graph) { viewModelFactory }

    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    private val args: BillPayContainerFragmentArgs by navArgs()

    private val changePageListener = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            binding.billPayTabLayout.setContentDescriptionForTabs()
        }
    }
    private var snackbar: Snackbar? = null
    private var isRecreated: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isRecreated = savedInstanceState != null
        setupViewModels()
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                OK_DIALOG_REQUEST_CODE -> activity?.onBackPressed()
            }
        }
    }

    private fun setupViewModels() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer { render(it) })
        navigationViewModel.paymentScheduled.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.let { payment ->
                    binding.viewPager.setCurrentItem(PAYMENT_LIST_TAB_INDEX, false)
                    findNavController().navigate(
                        BillPayContainerFragmentDirections.billPayContainerToPaymentDetails(payment)
                    )
                }
            }
        )
        navigationViewModel.refreshPayees.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { viewModel.refreshPayees() }
            }
        )
        navigationViewModel.addedPayee.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { newPayee -> viewModel.onPayeeAdded(newPayee) }
            }
        )
    }

    private fun render(viewState: ViewState) {
        handleLoading(viewState.isLoading)
        handleStatus(viewState.status)
        handleError(viewState.error)
        navigateToArgTab()
    }

    private fun handleLoading(isLoading: Boolean) {
        if (isLoading) {
            binding.loadingIndicator.show()
        } else {
            binding.loadingIndicator.hide()
        }
    }

    private fun handleStatus(status: ConsumableLiveEvent<BillPayStatus>?) {
        if (status?.peekContent() == BillPayStatus.Supported && binding.viewPager.adapter == null) {
            DefaultFragmentPagerAdapter(this, billPayFragments)
                .attach(binding.billPayTabLayout, binding.viewPager)
            binding.viewPager.initTabsTracking(viewModel)

            restoreTabPositionDelegate.init(
                this,
                viewPager = binding.viewPager,
                rootDestinations = setOf(pool.id.menuFragment, pool.id.bankFragment)
            )

            binding.viewPager.visibility = View.VISIBLE
        } else if (binding.viewPager.adapter != null) {
            binding.viewPager.visibility = View.VISIBLE
        } else {
            binding.viewPager.visibility = View.INVISIBLE
        }
        setContentDescriptionForTabs()

        status?.getNonConsumedContent()?.let { billPayStatus ->
            when (billPayStatus) {
                BillPayStatus.NotEligible -> showDialog(R.string.bill_pay_not_eligible, NOT_ELIGIBLE_TAG)
                BillPayStatus.NotEnrolled -> showDialog(R.string.bill_pay_not_enrolled, NOT_ENROLLED_TAG)
                else -> {
                    // Branch not implemented, lintKotlin requires exhaustive when branches for enums.
                }
            }
        }
    }

    private fun setContentDescriptionForTabs() {
        binding.billPayTabLayout.setContentDescriptionForTabs()
        binding.billPayTabLayout.setupInnerClickListenersForAccessibility()
        binding.viewPager.registerOnPageChangeCallback(changePageListener)
    }

    private fun BillPayFragmentContainerBinding.onDestroyBinding() {
        viewPager.unregisterOnPageChangeCallback(changePageListener)
    }

    private fun handleError(error: Error?) {
        when (error) {
            null -> snackbar?.dismiss()
            else -> {
                snackbar = snackBarUtil.retrySnackbar { error.retry?.invoke() }
            }
        }
    }

    private fun showDialog(@StringRes message: Int, tag: String) {
        val dialog = CustomDialogFragment.newInstance(
            title = getString(R.string.please_note),
            message = getString(message),
            okTitle = getString(R.string.ok),
            requestCode = OK_DIALOG_REQUEST_CODE
        ).apply {
            isCancelable = false
        }

        parentFragmentManager.beginTransaction()
            .add(dialog, tag)
            .commit()
    }

    private fun navigateToArgTab() {
        if (args.tabIndex >= 0 && !isRecreated) {
            binding.viewPager.setCurrentItem(args.tabIndex, false)
        }
    }
}
