package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.trade.form.api.input.shared.WithStopLossInputId
import com.etrade.mobilepro.trade.form.api.input.shared.WithTrailingStopInputId

interface StopLossStockTradeFormInputId : StockTradeFormInputId, WithStopLossInputId, WithTrailingStopInputId
