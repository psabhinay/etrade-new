package com.etrade.mobilepro.util.android.extension

import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import org.slf4j.LoggerFactory

private const val TOUCH_SLOP_VALUE = 8

fun ViewPager.addOnPageChangeListener(
    onPageSelected: ((position: Int) -> Unit)? = null,
    onPageScrollStateChanged: ((state: Int) -> Unit)? = null,
    onPageScrolled: ((position: Int, positionOffset: Float, positionOffsetPixels: Int) -> Unit)? = null
): ViewPager.OnPageChangeListener {
    return object : ViewPager.OnPageChangeListener {

        override fun onPageScrollStateChanged(state: Int) { onPageScrollStateChanged?.invoke(state) }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            onPageScrolled?.invoke(position, positionOffset, positionOffsetPixels)
        }

        override fun onPageSelected(position: Int) { onPageSelected?.invoke(position) }
    }.also(::addOnPageChangeListener)
}

fun ViewPager2.addOnPageChangeListener(
    onPageSelected: ((position: Int) -> Unit)? = null,
    onPageScrollStateChanged: ((state: Int) -> Unit)? = null,
    onPageScrolled: ((position: Int, positionOffset: Float, positionOffsetPixels: Int) -> Unit)? = null
): ViewPager2.OnPageChangeCallback {
    return object : ViewPager2.OnPageChangeCallback() {

        override fun onPageScrollStateChanged(state: Int) { onPageScrollStateChanged?.invoke(state) }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            onPageScrolled?.invoke(position, positionOffset, positionOffsetPixels)
        }

        override fun onPageSelected(position: Int) { onPageSelected?.invoke(position) }
    }.also(::registerOnPageChangeCallback)
}

/**
 * Reduces drag sensitivity of [ViewPager2] widget
 * The nested scrolling defect described in the Jira ticket is a known issue since Jan 17, 2019.
 * Issue Tracker: https://issuetracker.google.com/issues/123006042
 * ViewPager2 horizontal scrolling conflict with the WebView child
 * https://github.com/android/views-widgets-samples/issues/154
 */
fun ViewPager2.reduceDragSensitivity() {
    val logger = LoggerFactory.getLogger(this::class.java)
    try {
        val recyclerViewField = ViewPager2::class.java.getDeclaredField("mRecyclerView")
        recyclerViewField.isAccessible = true
        if (recyclerViewField.get(this) != null) {
            val recyclerView = recyclerViewField.get(this) as RecyclerView
            val touchSlopField = RecyclerView::class.java.getDeclaredField("mTouchSlop")
            touchSlopField.isAccessible = true
            val touchSlop = touchSlopField.get(recyclerView) as Int
            // "8" was obtained experimentally
            touchSlopField.set(recyclerView, touchSlop * TOUCH_SLOP_VALUE)
            logger.debug("ViewPagerExtensions: reduceDragSensitivity completed.")
        } else {
            logger.error("ViewPagerExtensions: reduceDragSensitivity() recyclerViewField.get is null")
        }
    } catch (e: IllegalAccessException) {
        logger.error("ViewPagerExtensions failed while reduceDragSensitivity exception", e)
    }
}
