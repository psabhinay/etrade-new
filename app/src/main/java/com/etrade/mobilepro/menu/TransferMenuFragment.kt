package com.etrade.mobilepro.menu

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.etrade.mobile.accounts.dto.InstitutionType
import com.etrade.mobilepro.MenuGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.menu.viewmodel.TransferMenuPageViewModel
import com.etrade.mobilepro.tracking.SCREEN_TITLE_WIRE_TRANSFERS_RECEIVE
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen
import com.etrade.mobilepro.transfermoney.presentation.TRANSFER_MONEY_DISABLE_TABS_RESTORING_FLAG
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetClicksViewModel
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetDialogFragment
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.InvocationCooldown
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.mutableArgs
import javax.inject.Inject

// should be in sync with LocalyticsInboxMessage.kt
private const val TRANSFER_MONEY_DEEPLINK_DEST_PARAM_VALUE = "transfermoney"
private const val TRANSFER_MONEY_DEEPLINK_DEST_FORWARDED_FLAG = "forwarded"

// should be in sync with menu_nav_graph.xml, argument name of this fragment
private const val TRANSFER_MONEY_DEEPLINK_DEST_PARAM_KEY = "dest"

class TransferMenuFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    userViewModel: UserViewModel,
    private val tracker: Tracker
) : AppBaseMenuFragment(viewModelFactory, userViewModel) {

    override val menuPageViewModel by viewModels<TransferMenuPageViewModel> { viewModelFactory }
    private val actionSheetClicksViewModel: ActionSheetClicksViewModel by viewModels { viewModelFactory }

    private val actionSheetCooldown = InvocationCooldown()

    private val args by mutableArgs(
        initializer = {
            navArgs<TransferMenuFragmentArgs>().value
        },
        onArgsAccessed = { args ->
            args?.getString(TRANSFER_MONEY_DEEPLINK_DEST_PARAM_KEY)
                ?.takeIf { it.isNotEmpty() }
                ?.let { args.putString(TRANSFER_MONEY_DEEPLINK_DEST_PARAM_KEY, TRANSFER_MONEY_DEEPLINK_DEST_FORWARDED_FLAG) }
            args
        }
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        actionSheetClicksViewModel.actionSheetClicks.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { item ->
                    actionSheetCooldown.startCooldown()
                    when (item.id) {
                        R.id.transferMoneyWireTransferActionSend -> onSendMoney()
                        R.id.transferMoneyWireTransferActionReceive -> onReceiveMoney()
                    }
                }
            }
        )
    }

    override fun onMenuItemClick(menuItem: TextMenuItem) {
        when (menuItem.id) {
            R.id.transferMoneyWireTransferMenuItem -> showAndSaveDirections {
                showActionSheet()
            }
            R.id.transferMoneyRetirementPlanRolloversMenuItem -> showAndSaveDirections {
                showRetirementPlanRolloverPage()
            }
            R.id.transferMoneyDirectDepositMenuItem -> showAndSaveDirections {
                showDirectDepositPage()
            }
        }

        super.onMenuItemClick(menuItem)
    }

    override fun onItemsUpdated(items: List<MenuItem>) {
        if (items.isNotEmpty()) {
            args.dest.takeIf { it.isNullOrEmpty().not() }?.let {
                when (it) {
                    TRANSFER_MONEY_DEEPLINK_DEST_PARAM_VALUE -> findNavController().navigate(
                        R.id.transfer_money_nav_graph,
                        bundleOf(
                            TRANSFER_MONEY_DISABLE_TABS_RESTORING_FLAG to true
                        )
                    )
                    TRANSFER_MONEY_DEEPLINK_DEST_FORWARDED_FLAG -> findNavController().popBackStack()
                    else -> Unit
                }
            }
        }
    }

    private fun showAndSaveDirections(show: () -> Unit) {
        bottomNavigationViewModel.savedMenuDirections.add(
            TextMenuItem.MenuAction.MenuGenericAction(
                show
            )
        )
        show()
    }

    private fun showActionSheet() {
        actionSheetCooldown.invokeIfReady {
            if (isAdded) {
                ActionSheetDialogFragment.show(
                    childFragmentManager,
                    menuPageViewModel.createWireTransferAction()
                )
            }
        }
    }

    private fun onSendMoney() {
        val url =
            resources.getString(R.string.menu_url_wire_transfer_send, menuPageViewModel.baseUrl)
        val actionSendNavDirection = MenuGraphDirections.actionMenuToAccountServices(
            url = url,
            title = resources.getString(R.string.menu_wire_transfer_send_page_title)
        )
        navigate(actionSendNavDirection)
    }

    private fun onReceiveMoney() {
        val actionReceiveNavDirection = MenuGraphDirections.actionMenuToDataReplacement(
            htmlResourceId = R.raw.wire_transfer_receive_template,
            title = resources.getString(R.string.menu_wire_transfer_receive_page_title),
            replacementData = bankAndBrokerageReplacementBundle()
        )
        tracker.screen(SCREEN_TITLE_WIRE_TRANSFERS_RECEIVE)
        navigate(actionReceiveNavDirection)
    }

    private fun showDirectDepositPage() {
        val actionDirectDepositNavDirection = MenuGraphDirections.actionMenuToDataReplacement(
            htmlResourceId = R.raw.direct_deposit_instructions_template,
            title = resources.getString(R.string.menu_setup_direct_deposit_title),
            replacementData = bankAndBrokerageReplacementBundle(AccountDisplayPolicy.ExcludeRetirementAccounts)
        )
        navigate(actionDirectDepositNavDirection)
    }

    private fun bankAndBrokerageReplacementBundle(accountDisplayPolicy: AccountDisplayPolicy = AccountDisplayPolicy.Default): Bundle {
        return getReplacementDataBundle(
            hashMapOf<String, String>().also { replacementData ->
                val bankAccountsFormattedStr =
                    menuPageViewModel.getAccountFormattedStringByInstitutionType(
                        arrayOf(InstitutionType.TELEBANK)
                    )
                if (bankAccountsFormattedStr.isNotBlank()) {
                    val bankAccountsPlaceHolder =
                        getString(R.string.menu_wire_transfer_bank_account_placeholder)
                    replacementData[bankAccountsPlaceHolder] = bankAccountsFormattedStr
                }
                setupBrokerageAccountsFormattedStr(replacementData, accountDisplayPolicy)
            }
        )
    }

    private fun showRetirementPlanRolloverPage() {
        val replacementData: HashMap<String, String> = hashMapOf()
        setupBrokerageAccountsFormattedStr(
            replacementData,
            AccountDisplayPolicy.IncludeRetirementAccountsOnly
        )
        val retirementPlanRolloverNavDirection = MenuGraphDirections.actionMenuToDataReplacement(
            htmlResourceId = R.raw.retirement_plan_rollover_template,
            title = resources.getString(R.string.menu_wire_transfer_retirement_plan_page_title),
            replacementData = getReplacementDataBundle(replacementData)
        )
        navigate(retirementPlanRolloverNavDirection)
    }

    private fun setupBrokerageAccountsFormattedStr(
        replacementData: HashMap<String, String>,
        accountDisplayPolicy: AccountDisplayPolicy = AccountDisplayPolicy.Default
    ) {
        val brokerageAccountsFormattedStr =
            menuPageViewModel.getAccountFormattedStringByInstitutionType(
                arrayOf(InstitutionType.ADP, InstitutionType.MSSB),
                accountDisplayPolicy
            )
        if (brokerageAccountsFormattedStr.isNotBlank()) {
            val brokerageAccountsPlaceHolder =
                getString(R.string.menu_wire_transfer_brokerage_account_placeholder)
            replacementData[brokerageAccountsPlaceHolder] = brokerageAccountsFormattedStr
        }
    }

    private fun getReplacementDataBundle(replacementData: HashMap<String, String>) =
        Bundle().apply {
            putSerializable(REPLACEMENT_DATA, replacementData)
        }

    override fun onResume() {
        super.onResume()
        popNav()
    }
}
