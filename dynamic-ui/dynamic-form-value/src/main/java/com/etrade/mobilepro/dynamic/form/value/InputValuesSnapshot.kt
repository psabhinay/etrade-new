package com.etrade.mobilepro.dynamic.form.value

typealias InputValuesSnapshot = Map<String, Any?>

typealias MutableInputValuesSnapshot = MutableMap<String, Any?>
