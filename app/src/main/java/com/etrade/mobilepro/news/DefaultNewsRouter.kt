package com.etrade.mobilepro.news

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.QuoteGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.home.model.MainActivityViewModel
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.navigation.EtNavController
import com.etrade.mobilepro.navigation.NEWS_ARTICLE_ID_KEY
import com.etrade.mobilepro.navigation.NEWS_ITEM_KEY
import com.etrade.mobilepro.navigation.getParentActivity
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.presentation.NewsContainerFragment
import com.etrade.mobilepro.news.presentation.NewsContainerFragmentArgs
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.news.presentation.company.CompanyNewsFragmentArgs
import com.etrade.mobilepro.news.presentation.detail.ParcelableNewsItem
import com.etrade.mobilepro.news.presentation.detail.toParcelable
import com.etrade.mobilepro.news.presentation.portfolio.PortfolioNewsFragmentArgs
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.userviewmodel.UserViewModel
import javax.inject.Inject

/**
 * Implements NewsRouter
 * This implementation has state thus should not be provided as a singleton
 */
class DefaultNewsRouter @Inject constructor(
    private val mainNavigation: MainNavigation,
    private val userViewModel: UserViewModel,
    private val mainActivityViewModel: MainActivityViewModel,
) : NewsRouter {
    private var etNavController: EtNavController? = null
    private fun getOrCreateETNavController(navController: NavController): EtNavController {
        return etNavController ?: EtNavController(
            navController,
            userViewModel
        ).also { this.etNavController = it }
    }

    override fun navigateToQuoteDetails(
        view: View,
        symbol: String,
        instrumentType: InstrumentType
    ) {
        mainNavigation.navigateToQuoteDetails(
            view.getParentActivity(),
            SearchResultItem.Symbol(title = symbol, type = InstrumentType.EQ).toBundle()
        )
    }

    override fun navigateToQuoteDetails(activity: FragmentActivity?, params: Bundle) {
        mainNavigation.navigateToQuoteDetails(activity, params)
    }

    override fun navigateToQuotePage(navController: NavController, params: Bundle) {
        navController.navigate(R.id.quotePage, params)
    }

    override fun navigateToNewsHubPage(view: View, accountId: String, isStockPlan: Boolean) {
        view.findNavController().navigate(
            MainGraphDirections.actionNewsFragment(
                tabType = NewsType.PORTFOLIO,
                accountId = accountId,
                isStockPlan = isStockPlan
            )
        )
    }

    override fun navigateToAccountNewsPage(view: View, accountId: String) {
        view.findNavController().navigate(
            MainGraphDirections.actionToPortfolioNews(accountId = accountId)
        )
    }

    override fun navigateToMarketNews(view: View) {
        view.findNavController()
            .navigate(MainGraphDirections.actionMarketOverviewToMarketNewsFragment())
    }

    override fun navigateToTextNewsDetails(navController: NavController?, newsDocId: String) {
        navController?.navigate(
            R.id.textNewsDetailFragment,
            bundleOf(NEWS_ARTICLE_ID_KEY to newsDocId)
        )
    }

    override fun navigateToTextNewsDetails(navController: NavController?, item: NewsItem.Text) {
        navController?.navigate(
            R.id.textNewsDetailFragment,
            bundleOf(NEWS_ITEM_KEY to item.toParcelable())
        )
    }

    override fun navigateToTextNewsDetails(navController: NavController?) {
        navController?.navigate(R.id.textNewsDetailFragment)
    }

    override fun navigateWatchlistTabWithLogin(navController: NavController, tabIndex: Int) {
        getOrCreateETNavController(navController).navigate(
            MainGraphDirections.actionWlNewsWithLogin(tabIndex = tabIndex)
        )
    }

    override fun navigateToSetAlert(
        navController: NavController,
        symbol: SearchResultItem.Symbol,
        from: NewsType?
    ) {
        with(getOrCreateETNavController(navController)) {
            if (graphId == R.id.mainGraph) {
                from?.let {
                    navigate(
                        MainGraphDirections.actionToSetAlertFragment(symbol),
                        MainGraphDirections.actionNewsFragment(from)
                    )
                } ?: navigate(MainGraphDirections.actionToSetAlertFragment(symbol))
            } else {
                navigate(QuoteGraphDirections.actionToSetAlertFragment(symbol))
            }
        }
    }

    override fun navigateToTrade(navController: NavController, params: Bundle, from: NewsType) {
        with(getOrCreateETNavController(navController)) {
            if (graphId == R.id.mainGraph) {
                navigate(
                    MainGraphDirections.actionLaunchTrade(params),
                    MainGraphDirections.actionNewsFragment(from)
                )
            } else {
                navigate(QuoteGraphDirections.actionQuoteLaunchTrade(params))
            }
        }
    }

    override fun forwardOnLoginToCurrentNav() {
        etNavController?.onLoggedIn()
    }

    override fun openSearch(simpleSearch: Boolean, location: String?) {
        mainActivityViewModel.openSearchActivity(
            SearchQuery(SearchType.SYMBOL),
            sharedViewId = R.id.menu_company_lookup,
            simpleSymbolSearch = simpleSearch,
            quoteLookupLocation = location
        )
    }

    override fun companyNewsSymbolArg(bundle: Bundle): SearchResultItem.Symbol? {
        return CompanyNewsFragmentArgs.fromBundle(bundle).quoteTicker
    }

    override fun portfolioNewsArgs(bundle: Bundle): Pair<String?, Boolean> = with(
        PortfolioNewsFragmentArgs.fromBundle(bundle)
    ) {
        Pair(accountId, hideFilter)
    }

    override fun newsDetailsArgs(bundle: Bundle?): Pair<String?, ParcelableNewsItem?> {
        val passedDocId = bundle?.getString(NEWS_ARTICLE_ID_KEY)
        val passedNewsItem: ParcelableNewsItem? = bundle?.getParcelable(NEWS_ITEM_KEY)
        return Pair(passedDocId, passedNewsItem)
    }

    override fun newsContainerArgs(bundle: Bundle?): NewsContainerFragment.Params? = bundle?.let {
        with(NewsContainerFragmentArgs.fromBundle(bundle)) {
            NewsContainerFragment.Params(
                accountId, quoteTicker, isStockPlan, tabType
            )
        }
    }
}
