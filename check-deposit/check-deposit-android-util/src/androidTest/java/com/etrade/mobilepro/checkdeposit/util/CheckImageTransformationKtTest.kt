package com.etrade.mobilepro.checkdeposit.util

import android.app.Application
import android.graphics.BitmapFactory
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.etrade.mobile.math.calculateGCD
import com.etrade.mobilepro.checkdeposit.util.test.R
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.lessThanOrEqualTo
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
internal class CheckImageTransformationKtTest {

    @Test
    fun transformedImageIsNoWiderThan1600Pixels() {
        assertThat(transformedBitmapOptions.outWidth, lessThanOrEqualTo(MAX_WIDTH))
    }

    @Test
    fun transformedImageIsCompressedToJpeg() {
        assertThat(transformedBitmapOptions.outMimeType, equalTo("image/jpeg"))
    }

    @Test
    fun transformedImageRetainsAspectRatio() {
        with(transformedBitmapOptions) {
            val gcd = calculateGCD(outHeight, outWidth)
            val normalizedHeight = outHeight / gcd
            val normalizedWidth = outWidth / gcd
            assertThat(originalAspectRatio, equalTo(Pair(normalizedWidth, normalizedHeight)))
        }
    }

    companion object {
        private const val MAX_WIDTH = 1600

        private val context = ApplicationProvider.getApplicationContext<Application>()

        private val originalAspectRatio = Pair(16, 9)
        private val originalCheckBytes = context.resources.openRawResource(R.raw.check_front).readBytes()
        private val transformedCheckBytes = prepareCheckImageForUpload(originalCheckBytes)
        private val transformedBitmapOptions: BitmapFactory.Options = decodeBitmapBounds(transformedCheckBytes)

        @BeforeClass
        @JvmStatic
        fun initializeCheckImage() {
            // Assumptions for further testing of check image
            val checkImage = decodeBitmapBounds(originalCheckBytes)
            assertThat(checkImage.outWidth, equalTo(1920))
            assertThat(checkImage.outHeight, equalTo(1080))
        }

        private fun decodeBitmapBounds(bytes: ByteArray) = BitmapFactory.Options()
            .apply {
                inJustDecodeBounds = true
            }.also { opts ->
                BitmapFactory.decodeByteArray(
                    bytes,
                    0,
                    bytes.size,
                    opts
                )
            }
    }
}
