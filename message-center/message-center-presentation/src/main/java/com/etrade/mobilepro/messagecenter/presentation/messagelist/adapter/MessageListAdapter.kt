package com.etrade.mobilepro.messagecenter.presentation.messagelist.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterItemMessageBinding
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.viewBinding

internal class MessageListAdapter(private val clickListener: (Message) -> Unit) : RecyclerView.Adapter<MessageViewHolder>() {
    private var items: List<Message> = emptyList()

    fun submitItems(newItems: List<Message>) {
        dispatchUpdates(items, newItems, MessageListDiffUtilCallback(items, newItems))
        items = newItems
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder =
        MessageViewHolder(parent.viewBinding(MessageCenterItemMessageBinding::inflate), clickListener)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) = holder.bindTo(items[position])

    fun tryRegisterObserver(observer: RecyclerView.AdapterDataObserver) {
        try {
            this.registerAdapterDataObserver(observer)
        } catch (e: IllegalArgumentException) {
            // nop
        } catch (e: IllegalStateException) {
            // nop
        }
    }

    fun tryUnregisterObserver(observer: RecyclerView.AdapterDataObserver) {
        try {
            this.unregisterAdapterDataObserver(observer)
        } catch (e: IllegalArgumentException) {
            // nop
        } catch (e: IllegalStateException) {
            // nop
        }
    }
}
