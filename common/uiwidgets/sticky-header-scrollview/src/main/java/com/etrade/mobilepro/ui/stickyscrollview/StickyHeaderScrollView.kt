package com.etrade.mobilepro.ui.stickyscrollview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.core.widget.NestedScrollView

class StickyHeaderScrollView : NestedScrollView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    // Supports multiple headers but they all have to be at same Y-position
    private val stickyHeaders: MutableList<View> = mutableListOf()
    private var initialHeaderTop: Int = 0

    fun setStickyHeaders(headers: List<View?>) {
        stickyHeaders.clear()
        for (header in headers) {
            header?.let {
                stickyHeaders.add(header)
                initialHeaderTop = header.top
            }
        }
    }

    private fun handleHeaderStickiness(scrollY: Int) {
        for (view in stickyHeaders) {
            if (scrollY > initialHeaderTop) {
                view.translationY = (scrollY - initialHeaderTop).toFloat()
                view.bringToFront()
            } else {
                view.translationY = 0f
            }
        }
    }

    override fun onNestedScroll(target: View, dxConsumed: Int, dyConsumed: Int, dxUnconsumed: Int, dyUnconsumed: Int, type: Int, consumed: IntArray) {
        super.onNestedScroll(target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, type, consumed)
        /**
         * scroll up the parent the same amount a nested scrollable child scrolls up
         */
        if (dyConsumed < 0) {
            scrollBy(0, dyConsumed)
        }
    }

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)
        handleHeaderStickiness(t)
    }
}
