package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.WithSymbolInfo

interface OptionTradeFormInputId : BaseTradeFormInputId, WithAdvancedTradeFormInputId, WithTradeLegsTradeFormInputId {
    val lookupSymbol: String
}

fun OptionTradeFormInputId.extractSymbolLookupInfo(snapshot: InputValuesSnapshot): WithSymbolInfo? = snapshot[lookupSymbol]?.let { symbolCoder.decode(it) }
