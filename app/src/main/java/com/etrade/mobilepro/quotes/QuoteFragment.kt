package com.etrade.mobilepro.quotes

import android.content.DialogInterface
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.TextView
import androidx.core.view.doOnLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.QuoteGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.alerts.presentation.AlertsSharedViewModel
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.common.Level2PendingNavigationHelper
import com.etrade.mobilepro.common.OverlayFragmentManagerHolder
import com.etrade.mobilepro.common.consumeWithTextView
import com.etrade.mobilepro.common.networkUnavailableView
import com.etrade.mobilepro.databinding.FragmentQuoteBinding
import com.etrade.mobilepro.dialog.displayWalkthroughMessage
import com.etrade.mobilepro.dialog.hideWalkthroughMessage
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.dropdown.DropDownManager
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.learn.presentation.tutorials.ACTION
import com.etrade.mobilepro.learn.presentation.tutorials.ActionsMap.Companion.SYMBOL_LOOKUP_DISMISS_DIALOG
import com.etrade.mobilepro.learn.presentation.tutorials.POSITIVE_ACTION_TEXT
import com.etrade.mobilepro.learn.presentation.tutorials.SUBTITLE_TEXT
import com.etrade.mobilepro.learn.presentation.tutorials.WalkthroughMessageFragment
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.navigation.EtNavController
import com.etrade.mobilepro.news.presentation.company.CompanyNewsViewModel
import com.etrade.mobilepro.orders.showAffiliatedProductDialog
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.quote.presentation.QuoteOverviewEventListener
import com.etrade.mobilepro.quote.screen.presentation.QuoteChartViewModel
import com.etrade.mobilepro.quote.screen.presentation.QuoteTabsFragmentArgs
import com.etrade.mobilepro.quote.screen.presentation.QuoteTabsFragmentDirections
import com.etrade.mobilepro.quote.screen.presentation.adapter.QuoteAdapter
import com.etrade.mobilepro.quote.screen.presentation.ui.performance.FundPerformanceViewModel
import com.etrade.mobilepro.quotes.util.PendingQuoteMessageHelper
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.isEtfSymbol
import com.etrade.mobilepro.streaming.StreamingStatusNotifierDelegate
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.tooltip.Tooltip
import com.etrade.mobilepro.tooltip.TooltipGravity
import com.etrade.mobilepro.tooltip.TooltipViewModel
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.presentation.ConditionalOrderDisclosureViewDelegate
import com.etrade.mobilepro.usersubscriptionpresentation.UserSubscriptionViewModel
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import com.etrade.mobilepro.util.android.extension.post
import com.etrade.mobilepro.util.android.extension.setContentDescriptionForTabs
import com.etrade.mobilepro.util.android.extension.setupInnerClickListenersForAccessibility
import com.etrade.mobilepro.util.android.extension.viewCoroutineScope
import com.etrade.mobilepro.util.android.logAppAction
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.android.snackbar.showIfNotConsumed
import com.etrade.mobilepro.util.toSentence
import com.etrade.mobilepro.viewdelegate.BlockingLoadingIndicatorViewDelegate
import com.etrade.mobilepro.viewdelegate.LifecycleObserverViewDelegate
import com.etrade.mobilepro.walkthrough.api.SymbolLookupAction
import com.etrade.mobilepro.walkthrough.api.SymbolLookupState
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingAction
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingState
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.etrade.mobilepro.watchlist.delegates.AddToWatchListDelegate
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import org.chromium.customtabsclient.shared.WebviewActivity
import org.slf4j.LoggerFactory
import javax.inject.Inject

private const val option_disclosure_url = "/l/f/disclosure-library/options-price-reporting-authority"
private const val LIST_STATE = "ListState"
private const val MESSAGE_FRAGMENT_TAG = "QuoteFragment.message_fragment"
private const val PROSPECTUS_URL = "https://doc.morningstar.com/DocDetail.aspx?clientid=etrade&key=2fc038529bc433a6&investmenttype=1&doctype=prospectus&ticker="
private const val PERFORMANCE_URL = "https://www.etrade.wallst.com/v1/stocks/fund_performance/performance.asp?symbol="
const val QUOTE_PATH = "/Quote"

@Suppress("TooManyFunctions", "LongParameterList")
class QuoteFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val userViewModel: UserViewModel,
    private val userSubscriptionViewModel: UserSubscriptionViewModel,
    private val streamingStatusController: StreamingStatusController,
    private val streamingStatusNotifierDelegate: StreamingStatusNotifierDelegate,
    private val addToWatchListDelegate: AddToWatchListDelegate,
    private val appConfigRepo: AppConfigRepo,
    private val pendingQuoteMessageHelper: PendingQuoteMessageHelper,
    private val level2PendingNavigationHelper: Level2PendingNavigationHelper,
    private val marketTradeStatusDelegate: MarketTradeStatusDelegate,
    private val walkthroughStatesViewModel: WalkthroughStatesViewModel,
    private val overlayHolder: OverlayFragmentManagerHolder,
    private val fragmentFactory: FragmentFactory
) : Fragment(R.layout.fragment_quote) {

    private val logger = LoggerFactory.getLogger(QuoteFragment::class.java)
    private val binding by viewBinding(FragmentQuoteBinding::bind) { onDestroyBinding() }

    private lateinit var viewAdapter: QuoteAdapter
    private lateinit var secondPaneViewAdapter: QuoteAdapter
    private lateinit var newsViewModel: CompanyNewsViewModel
    private val alertsSharedViewModel: AlertsSharedViewModel by activityViewModels { viewModelFactory }
    private val quoteViewModel: QuoteViewModel by viewModels(
        ownerProducer = { parentFragment ?: this },
        factoryProducer = { viewModelFactory }
    )
    private val mfPerformanceViewModel: FundPerformanceViewModel by viewModels { viewModelFactory }
    private val chartViewModel: QuoteChartViewModel by viewModels { viewModelFactory }
    private val quoteBottomSheetFragmentViewModel: QuoteBottomSheetFragmentViewModel by activityViewModels { viewModelFactory }
    private var symbol: SearchResultItem.Symbol? = null
    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private val args: QuoteTabsFragmentArgs by lazy {
        QuoteTabsFragmentArgs.fromBundle(
            arguments
                ?: parentFragment?.arguments
                ?: throw IllegalArgumentException("No quote arguments were found")
        )
    }
    private var snackBar: Snackbar? = null
    private var listState: Parcelable? = null
    private var description = ""
    private val etNavController: EtNavController by lazy {
        EtNavController(
            findNavController(),
            userViewModel
        )
    }
    private var dropDownManager: DropDownManager<Watchlist>? = null
    private lateinit var snackbarUtil: SnackBarUtil
    private lateinit var tradeSelectionDelegate: TradeSelectionDelegate

    private val disclosureViewDelegate by lazy {
        ConditionalOrderDisclosureViewDelegate(
            requireContext(),
            snackBarUtil,
            quoteViewModel.securityAndOrderTypeHelper.disclosureViewModel
        )
    }
    private val blockingLoadingIndicatorViewDelegate by lazy {
        BlockingLoadingIndicatorViewDelegate(quoteViewModel.securityAndOrderTypeHelper.disclosureViewModel, childFragmentManager)
    }
    private val symbolLookupArgs by lazy {
        Bundle().apply {
            putString(SUBTITLE_TEXT, getString(R.string.learn_quote_lookup_dialog_description))
            putString(POSITIVE_ACTION_TEXT, getString(R.string.learn_quote_lookup_dialog_positive_action))
            putString(ACTION, SYMBOL_LOOKUP_DISMISS_DIALOG)
        }
    }
    private val symbolLookupFragmentProvider by lazy {
        { fragmentFactory.instantiateFragment(WalkthroughMessageFragment::class.java, symbolLookupArgs) }
    }
    private val lifecycleObserverViewDelegate by lazy { LifecycleObserverViewDelegate(listOf(quoteViewModel)) }
    private val tooltipViewModel: TooltipViewModel by activityViewModels()
    private var tooltip: Tooltip? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logAppAction(activity?.intent, QUOTE_PATH)
    }

    @Suppress("LongMethod")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tabContainer.visibility = View.GONE
        listState = savedInstanceState?.getParcelable(LIST_STATE)
        snackbarUtil = snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            // if a drop down is showing we want a snackbar to be shown over it
            { dropDownManager?.view ?: this.view }
        )

        quoteViewModel.isInDualPane = binding.quoteDetailsSeparator != null

        symbol = args.quoteTicker

        setupAddToWatchlistDelegate()
        setUpViewModels()

        symbol?.let {
            setupTabs(it)
            quoteViewModel.fetch(it)
        }

        setupSwipeRefresh()

        bindUserViewModel()
        setAdapters()
        setupMda()
        marketTradeStatusDelegate.consumeWithTextView(textView = binding.quoteMarketHaltBanner as TextView, lifecycleOwner = viewLifecycleOwner)
        disclosureViewDelegate.observe(viewLifecycleOwner)
        blockingLoadingIndicatorViewDelegate.observe(viewLifecycleOwner)

        pendingQuoteMessageHelper.message?.consume {
            snackBarUtil.snackbar(it, Snackbar.LENGTH_SHORT)?.show()
        }

        activity?.networkUnavailableView?.hideQuoteBanner()
    }

    private fun TextView.hideQuoteBanner() {
        doOnLayout {
            if (view != null) {
                if ((binding.quoteMarketHaltBanner.visibility == View.VISIBLE) && (visibility == View.VISIBLE)) {
                    binding.quoteMarketHaltBanner.visibility = View.GONE
                }
            }
        }
    }

    private fun setupSwipeRefresh() {
        binding.swipeRefresh.apply {
            applyColorScheme()
            setOnRefreshListener {
                snackBar?.dismiss()
                quoteViewModel.refresh()
            }
        }
    }
    private fun bindUserViewModel() {
        userViewModel.streamingToggleState.observe(
            viewLifecycleOwner,
            {
                if (it.getNonConsumedContent() == true) {
                    onStreamingEnabled()
                }
            }
        )
        userViewModel.observeLoginState(viewLifecycleOwner, quoteViewModel.loginSignalHandler) {
            if (level2PendingNavigationHelper.pendingNavigationToLevel2) {
                level2PendingNavigationHelper.pendingNavigationToLevel2 = false

                if (streamingStatusController.isStreamingToggleEnabled) {
                    symbol?.let { symbol ->
                        etNavController.navigate(QuoteTabsFragmentDirections.actionQuoteToLevel2(symbol))
                        etNavController.clearPendingDirections()
                    }
                } else {
                    quoteViewModel.refresh()
                    etNavController.navigate(QuoteTabsFragmentDirections.actionQuoteToStreamingTogglePromptDialog())
                    etNavController.clearPendingDirections()
                }
            } else {
                etNavController.onLoggedIn()
            }
        }
    }

    private fun setupAddToWatchlistDelegate() {
        addToWatchListDelegate.init(this, binding.loadingIndicator)
        symbol?.also {
            addToWatchListDelegate.setSymbol(it)
        }
        addToWatchListDelegate.newSymbolAddedToWatchList.observe(
            viewLifecycleOwner,
            {
                quoteBottomSheetFragmentViewModel.refreshWatchlist()
            }
        )
    }

    private fun setupMda() {
        userSubscriptionViewModel.userMdaState.observe(
            viewLifecycleOwner,
            { event ->
                event.getNonConsumedContent()?.let { isRealTime ->
                    if (isRealTime) {
                        symbol?.let {
                            etNavController.navigate(QuoteGraphDirections.actionQuoteToLevel2(it))
                        }
                    } else {
                        userViewModel.showMarketDataAgreement(resources.getString(R.string.please_note), resources.getString(R.string.mda_level2_message))
                    }
                }
            }
        )
    }

    private fun setupTabs(it: SearchResultItem.Symbol) {
        with(binding.tabContainer.tabLayout) {
            if (tabCount == 0) {
                addTab(newTab().apply { text = resources.getString(R.string.snapshot) }, 0)
                addTab(newTab().apply { text = resources.getString(R.string.quote_performance) }, 1)
                if (it.type == InstrumentType.MF) {
                    addTab(newTab().apply { text = resources.getString(R.string.fees_and_expenses) }, 2)
                }
            }

            setDefaultTab(it.type)
        }
    }

    private fun setDefaultTab(type: InstrumentType) {
        when (
            val tabViewType =
                quoteViewModel.lastSelectedTabIndex.value?.toTabViewType ?: args.fundsViewType
        ) {
            MutualFundViewType.SNAPSHOT, MutualFundViewType.PERFORMANCE -> quoteViewModel.selectTab(
                tabViewType.index
            )
            MutualFundViewType.FEES_EXPENSE ->
                if (type == InstrumentType.MF) {
                    quoteViewModel.selectTab(tabViewType.index)
                } else {
                    quoteViewModel.selectTab(MutualFundViewType.SNAPSHOT.index)
                }
        }
    }

    private fun setUpViewModels() {
        newsViewModel = ViewModelProvider(this, viewModelFactory).get(CompanyNewsViewModel::class.java)
        secondPaneViewAdapter = setupViewAdapter()
        viewAdapter = setupViewAdapter()

        setupQuoteViewModel()
        bindChartViewModel()

        alertsSharedViewModel.alertWasSet.observe(
            viewLifecycleOwner,
            { signal ->
                snackbarUtil.showIfNotConsumed(signal)
            }
        )
        quoteViewModel.openShowAffiliatedDialog.observe(viewLifecycleOwner, { showAffiliatedDialog(it) })
        setupWalkthroughListeners()
        setupSelectionDelegate()
    }

    @Suppress("LongMethod")
    private fun setupWalkthroughListeners() {
        quoteViewModel.symbolTrackingHighlightWatchlistActionSignal
            .observeBy(viewLifecycleOwner) { signal ->
                if (signal.isConsumed) { return@observeBy }
                if (signal.peekContent() == SymbolTrackingState.HighlightWatchlistActionInQuotePage) {
                    showWatchlistTooltip(
                        onDismiss = { signal.consume() }
                    )
                }
            }
        quoteViewModel.walkthroughStatesViewModel.state.observe(
            viewLifecycleOwner,
            {
                when (it) {
                    SymbolLookupState.DisplayDialogOverQuote -> overlayHolder.displayWalkthroughMessage(
                        symbolLookupFragmentProvider,
                        MESSAGE_FRAGMENT_TAG,
                        true,
                        activity?.supportFragmentManager
                    ) {
                        walkthroughStatesViewModel.onAction(SymbolLookupAction.Canceled)
                    }

                    SymbolLookupState.DisplayQuoteOnly -> {
                        overlayHolder.hideWalkthroughMessage(symbolLookupFragmentProvider, MESSAGE_FRAGMENT_TAG, true)
                    }
                }
            }
        )
    }

    private fun showWatchlistTooltip(onDismiss: () -> Unit) {
        post {
            val targetView = view?.findViewById<View>(R.id.outlined_add_watch_list_btn) ?: return@post
            tooltip = tooltipViewModel.show {
                target = targetView
                text = getString(R.string.watchlist_action_in_quote)
                gravity = TooltipGravity.BOTTOM
                yOffset = resources.getDimensionPixelOffset(R.dimen.spacing_small)
                this.onDismiss = onDismiss
                durationParams = Tooltip.DurationParams(viewCoroutineScope)
            }
            walkthroughStatesViewModel.onAction(SymbolTrackingAction.WatchlistActionInWatchlistPageHighlighted)
        }
    }

    private fun setupSelectionDelegate() {
        tradeSelectionDelegate = TradeSelectionDelegate(
            helper = quoteViewModel.securityAndOrderTypeHelper,
            manager = childFragmentManager,
            etNavController = etNavController,
            resources = resources
        )
        tradeSelectionDelegate.setup()
        tradeSelectionDelegate.observe(viewLifecycleOwner)

        lifecycleObserverViewDelegate.observe(viewLifecycleOwner)
    }

    @Suppress("LongMethod")
    private fun setupQuoteViewModel() {
        quoteViewModel.quoteDescription.observe(
            this@QuoteFragment.viewLifecycleOwner,
            { mobileQuote ->
                quoteBottomSheetFragmentViewModel.updateQuoteDesc(mobileQuote.searchSymbol, mobileQuote.searchSymbolDescription.toSentence())
                description = mobileQuote.quoteDescription.toSentence()
            }
        )
        quoteViewModel.lastSelectedTabIndex.observe(
            this@QuoteFragment.viewLifecycleOwner,
            {
                with(binding.tabContainer.tabLayout) {
                    getTabAt(it ?: 0)?.select()
                    setContentDescriptionForTabs()
                    setupInnerClickListenersForAccessibility()
                }
            }
        )
        quoteViewModel.viewState.observe(
            this@QuoteFragment.viewLifecycleOwner,
            {
                handleStateChange(it)
            }
        )
        quoteViewModel.symbol.observe(
            viewLifecycleOwner,
            {
                addToWatchListDelegate.setSymbol(it)
            }
        )
        quoteViewModel.showDisclosures.observeBy(viewLifecycleOwner) {
            if (it) setupDisclosures()
        }
    }

    private fun bindChartViewModel() = chartViewModel.showChartIq.observe(
        this@QuoteFragment.viewLifecycleOwner,
        { event ->
            event.getNonConsumedContent()?.let { show ->
                if (show) {
                    resolveEventListener()?.onChartClicked()
                }
            }
        }
    )

    private fun resolveEventListener(): QuoteOverviewEventListener? {
        return (activity as? QuoteOverviewEventListener)
            ?: (parentFragment as? QuoteOverviewEventListener)
            ?: run {
                logger.error("Couldn't resolve QuoteOverviewEventListener")
                null
            }
    }

    @Suppress("LongMethod", "ComplexMethod") // Lots of when conditions
    private fun handleStateChange(it: QuoteViewModel.ViewState?) {
        when (it) {
            is QuoteViewModel.ViewState.Loading -> {
                if (!binding.swipeRefresh.isRefreshing) {
                    binding.loadingIndicator.show()
                }
            }
            is QuoteViewModel.ViewState.Error -> {
                binding.loadingIndicator.hide()
                binding.swipeRefresh.isRefreshing = false

                snackBar = snackBarUtil.retrySnackbar(message = getString(R.string.error_message_general)) {
                    quoteViewModel.refresh()
                }
            }
            is QuoteViewModel.ViewState.Success -> {
                symbol?.type?.let {
                    if (it.isMutualFundOrMoneyMarketFund) {
                        showMutualFundTabs()
                    }
                }
                binding.loadingIndicator.hide()
                binding.swipeRefresh.isRefreshing = false

                viewAdapter.quoteItems = it.results.first
                if (quoteViewModel.isInDualPane) {
                    secondPaneViewAdapter.quoteItems = it.results.second
                }
            }
        }
    }

    private fun setAdapters() {
        with(binding.quotesRecyclerView) {
            layoutManager = LinearLayoutManager(context)
            listState?.let {
                (layoutManager as LinearLayoutManager).onRestoreInstanceState(listState)
            }
            adapter = viewAdapter
        }
        binding.quotesRecyclerView2.run {
            layoutManager = LinearLayoutManager(context)
            listState?.let {
                (layoutManager as LinearLayoutManager).onRestoreInstanceState(listState)
            }
            adapter = secondPaneViewAdapter
        }
    }

    private fun showMutualFundTabs() {
        binding.tabContainer.visibility = View.VISIBLE
        with(binding.tabContainer.tabLayout) {
            clearOnTabSelectedListeners()
            addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabReselected(tab: TabLayout.Tab?) {
                }

                override fun onTabUnselected(tab: TabLayout.Tab?) {
                }

                override fun onTabSelected(tab: TabLayout.Tab?) {
                    tab?.let { selectedTab ->
                        binding.quotesRecyclerView.scrollToPosition(0)
                        quoteViewModel.selectTab(selectedTab.position)
                    }
                }
            })
        }
    }

    @SuppressWarnings("ComplexMethod", "LongMethod")
    private fun setupViewAdapter(): QuoteAdapter = QuoteAdapter(
        viewLifecycleOwner, chartViewModel,
        selectNewsItemListener = { selectedItem, position ->
            newsViewModel.newsItemSelected(selectedItem, position)
            if (!newsViewModel.isInDualPane) {
                val direction = QuoteTabsFragmentDirections.actionQuoteToDetailsNews(selectedItem.docId)
                findNavController().navigate(direction)
            }
        },
        statusChecker = { item ->
            newsViewModel.wasRead(item.docId)
        },
        symbolSelectListener = { ticker ->
            val direction = QuoteTabsFragmentDirections.actionLaunchQuote(SearchResultItem.Symbol(title = ticker, type = InstrumentType.EQ))
            findNavController().navigate(direction)
        },
        selectionChecker = {
            newsViewModel.selectionTracker.current == it && newsViewModel.isInDualPane
        },
        disclosureClickListener = {
            quoteViewModel.setDisclosureState(true)
        },
        companyOverviewClickListener = {
            symbol?.let {
                findNavController().navigate(QuoteTabsFragmentDirections.actionQuoteToCompanyOverview(it.underlyerSymbol))
            }
        },
        viewAllNewsClickListener = {
            symbol?.let {
                val direction = if (it.type.isIndex) {
                    QuoteTabsFragmentDirections.actionQuoteToMarketNews(showInQuotePage = true)
                } else {
                    QuoteTabsFragmentDirections.actionQuoteToCompanyNews(quoteTicker = it, description = description)
                }
                findNavController().navigate(direction)
            }
        },
        newsRetryClickListener = {
            quoteViewModel.refresh()
        },
        tradeClickListener = {
            symbol?.let {
                quoteViewModel.securityAndOrderTypeHelper.openTrade()
            }
        },
        setAlertClickListener = {
            symbol?.let { navArg ->
                etNavController.navigate(QuoteTabsFragmentDirections.actionToSetAlertFragment(navArg))
            }
        },
        fundClassDropDown = BottomSheetSelector(childFragmentManager, resources),
        fetchQuoteAction = { ticker: String?, instrumentType: InstrumentType ->
            ticker?.let {
                val localSymbol = SearchResultItem.Symbol(it, instrumentType)
                symbol = localSymbol
                binding.swipeRefresh.isRefreshing = true
                quoteViewModel.fetch(localSymbol)
            }
        },
        fundInfoPopUpListener = {
            when (it) {
                R.id.all_star_icon -> {
                    findNavController().navigate(QuoteTabsFragmentDirections.actionQuoteShowPopup(getString(R.string.fund_profile_info_all_stars_fund)))
                }
                R.id.no_transaction_fee_icon -> {
                    findNavController().navigate(QuoteTabsFragmentDirections.actionQuoteShowPopup(getString(R.string.commission_free_fund_info)))
                }
                R.id.affiliated_product_fund_icon -> {
                    quoteViewModel.showAffiliatedDialog()
                }
            }
        },
        definitionsClickListener = {
            activity?.let {
                startActivity(
                    SpannableTextOverlayActivity.intent(
                        it,
                        getString(R.string.definitions),
                        getString(R.string.mutual_fund_definitions),
                        com.etrade.mobilepro.education.R.drawable.ic_close
                    )
                )
            }
        },
        fundDescriptionClickListener = { description ->
            findNavController().navigate(QuoteTabsFragmentDirections.actionQuoteToMfDescription(description))
        },
        prospectusClickListener = {
            symbol?.let {
                val url = PROSPECTUS_URL + quoteViewModel.cusipId
                findNavController().navigate(QuoteTabsFragmentDirections.actionQuoteToProspectus(url))
            }
        },
        reportClickListener = {
            symbol?.let {
                etNavController.navigate(
                    QuoteTabsFragmentDirections.actionQuoteToReports(
                        it.underlyerSymbol,
                        it.type.isMutualFundOrMoneyMarketFund
                    )
                )
            }
        },
        addToWatchlistClickListener = {
            symbol?.let {
                addToWatchListDelegate.showWatchList()
            }
        },
        level2ClickListener = {
            if (streamingStatusController.isStreamingToggleEnabled) {
                onStreamingEnabled()
            } else {
                if (streamingStatusNotifierDelegate.isUnableToStream.get()) {
                    streamingStatusNotifierDelegate.showNotificationDialog(true)
                } else {
                    if (!userViewModel.isAuthenticated) {
                        level2PendingNavigationHelper.pendingNavigationToLevel2 = true
                    }
                    etNavController.navigate(QuoteTabsFragmentDirections.actionQuoteToStreamingTogglePromptDialog())
                }
            }
        },
        performanceClickListener = {
            symbol?.let {
                val url = PERFORMANCE_URL + it.underlyerSymbol
                findNavController().navigate(QuoteTabsFragmentDirections.actionQuoteToPerformance(url))
            }
        },
        optionsChainClickListener = {
            findNavController().navigate(QuoteTabsFragmentDirections.actionQuoteToOptionsChain(it))
        }
    )

    private fun openEarningsDetails() {
        symbol?.also {
            etNavController.navigate(QuoteTabsFragmentDirections.actionQuoteToEarningsDetails(it.underlyerSymbol))
        }
    }

    private fun setupDisclosures() {
        if (symbol?.type?.isOption == true) {
            activity?.let {
                val viewListener = dialogYesListener
                val cancelListener = DialogInterface.OnClickListener { dialog, _ ->
                    quoteViewModel.setDisclosureState(false)
                    dialog.dismiss()
                }
                val characteristicsRisksListener = DialogInterface.OnClickListener { dialog, _ ->
                    quoteViewModel.setDisclosureState(false)
                    dialog.dismiss()
                    etNavController.navigate(QuoteTabsFragmentDirections.actionToDisclosureCharacteristicsRisks())
                }
                MaterialAlertDialogBuilder(it)
                    .setCancelable(true)
                    .setOnCancelListener { quoteViewModel.setDisclosureState(false) }
                    .setTitle(getString(R.string.disclosures))
                    .setMessage(getString(R.string.options_chain_disclosure_description))
                    .setPositiveButton(getString(R.string.view_disclosures), viewListener)
                    .setNeutralButton(R.string.options_chain_characteristics_risks, characteristicsRisksListener)
                    .setNegativeButton(getString(R.string.cancel), cancelListener)
                    .show()
            }
        } else {
            activity?.let {
                val disclosureText = when {
                    symbol?.type?.isMutualFundOrMoneyMarketFund == true ->
                        getString(R.string.mutual_fund_disclosure)
                    symbol?.isEtfSymbol() == true -> getString(R.string.etf_disclosure)
                    else -> getString(R.string.disclosure_text)
                }
                startActivity(
                    SpannableTextOverlayActivity.intent(
                        it,
                        getString(R.string.disclosures),
                        disclosureText,
                        com.etrade.mobilepro.education.R.drawable.ic_close
                    )
                )
            }
        }
    }

    override fun onPause() {
        super.onPause()
        listState = binding.quotesRecyclerView.layoutManager?.onSaveInstanceState()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(LIST_STATE, listState)
    }

    private val dialogYesListener = DialogInterface.OnClickListener { dialog, _ ->
        dialog.dismiss()
        quoteViewModel.setDisclosureState(false)
        context?.apply {
            startActivity(
                WebviewActivity.intent(
                    this,
                    appConfigRepo.getAppConfig().baseUrls.web.plus(option_disclosure_url),
                    R.string.disclosures
                )
            )
        }
    }

    private fun onStreamingEnabled() {
        userSubscriptionViewModel.checkUserIsRealTimeStreaming()
    }

    private fun FragmentQuoteBinding.onDestroyBinding() {
        walkthroughStatesViewModel.onAction(SymbolLookupAction.QuoteClosed)
        quotesRecyclerView.adapter = null
        quotesRecyclerView2.adapter = null
        addToWatchListDelegate.clearUp()
    }

    private fun showAffiliatedDialog(value: Boolean?) {
        value?.run {
            if (value) {
                showAffiliatedProductDialog { quoteViewModel.closeAffiliatedDialog() }
            }
        }
    }
}
