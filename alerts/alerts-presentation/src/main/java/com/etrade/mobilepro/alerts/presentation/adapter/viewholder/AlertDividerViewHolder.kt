package com.etrade.mobilepro.alerts.presentation.adapter.viewholder

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.alerts.api.model.AlertItem

class AlertDividerViewHolder(private val containerView: View) : RecyclerView.ViewHolder(containerView) {
    fun bindTo(item: AlertItem.Divider) {
        with(containerView.rootView) {
            when {
                item.show && visibility == View.GONE -> {
                    visibility = View.VISIBLE
                    layoutParams =
                        getModifiedLayoutParams(layoutParams, ViewGroup.LayoutParams.WRAP_CONTENT)
                }
                !item.show && visibility == View.VISIBLE -> {
                    visibility = View.GONE
                    layoutParams =
                        getModifiedLayoutParams(layoutParams, 0)
                }
            }
        }
    }

    private fun getModifiedLayoutParams(
        targetParams: ViewGroup.LayoutParams,
        newHeight: Int
    ): ViewGroup.LayoutParams {
        return ViewGroup.LayoutParams(targetParams).apply {
            height = newHeight
        }
    }
}
