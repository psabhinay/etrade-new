package com.etrade.mobilepro.appwidget.common

import android.widget.RemoteViews

interface BaseRemoteViewsFactory<T> {
    fun create(item: T): RemoteViews
}
