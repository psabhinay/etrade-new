package com.etrade.mobilepro.checkdeposit.presentation

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.checkdeposit.presentation.activity.CheckDepositActivityFragment
import com.etrade.mobilepro.checkdeposit.presentation.deposit.CheckDepositFragment
import com.etrade.mobilepro.util.android.adapter.DefaultFragmentPagerAdapter

const val ACTIVITY_TAB = 1

private val fragmentList = listOf(
    DefaultFragmentPagerAdapter.FragmentPagerItem(
        CheckDepositFragment::class.java,
        R.string.deposit
    ),
    DefaultFragmentPagerAdapter.FragmentPagerItem(
        CheckDepositActivityFragment::class.java,
        R.string.activity
    )
)

class CheckDepositFragmentPagerAdapter(
    fragment: Fragment
) : DefaultFragmentPagerAdapter(fragment, fragmentList)
