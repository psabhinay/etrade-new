package com.etrade.mobilepro.dynamic.form.ui

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.TextViewCompat
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.dynamic.form.api.StaticText

class StaticTextView : AppCompatTextView, ComponentView<StaticText> {

    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    private val defaultMargin: Int = context.resources.getDimensionPixelSize(R.dimen.spacing_medium)

    init {
        isFocusable = true
    }

    override fun setLayoutParams(params: ViewGroup.LayoutParams) {
        if (params is ViewGroup.MarginLayoutParams) {
            params.topMargin = defaultMargin
            params.bottomMargin = defaultMargin
            params.marginStart = defaultMargin
            params.marginEnd = defaultMargin
        }
        super.setLayoutParams(params)
    }

    override fun render(component: StaticText, owner: LifecycleOwner) {
        TextViewCompat.setTextAppearance(this, component.textStyleId ?: 0)
        text = component.title
        gravity = Gravity.CENTER
    }
}
