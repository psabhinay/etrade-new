package com.etrade.mobilepro.marketscalendar.viewmodel

import android.os.Parcelable
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import org.threeten.bp.LocalDateTime

@Parcelize
open class EventDetails(
    val title: String?,
    val date: String?,
    val estimate: String? = null,
    val actual: String? = null,
    val surprise: String? = null,
    val eventType: String,
    val symbol: String? = null,
    val description: String? = null
) : Parcelable, Comparable<EventDetails> {

    @IgnoredOnParcel
    private val sortDateTime: LocalDateTime? by lazy { getDateTime(date) }

    open fun getDateTime(date: String?): LocalDateTime? = date?.let { LocalDateTime.parse(it, EVENTS_DATE_TIME_FORMATTER) }

    override fun compareTo(other: EventDetails): Int {
        if (sortDateTime != null && other.sortDateTime != null) {
            val rhs = sortDateTime as LocalDateTime
            val lhs = other.sortDateTime as LocalDateTime

            return when {
                lhs.isBefore(rhs) -> 1
                lhs.isAfter(rhs) -> -1

                else -> other.title?.let { title?.compareTo(it) } ?: 0
            }
        }

        return 0
    }
}
