package com.etrade.mobilepro.screener.data.json.etf

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.OPERATOR_LIKE
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfTechnicalsFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val ETF_TECHNICALS_VALUES_FIELD = "Volume30DayAVG"

class EtfTechnicalsFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val priceVolumeFilter = screenerFilter as EtfTechnicalsFilter
        val arguments = mutableListOf<Argument>()

        if (priceVolumeFilter.priceMovingBelow && !priceVolumeFilter.average.isAllItemsElement) {
            arguments.add(Argument(listOf(priceMovingBelowClause), priceVolumeFilter.average.id))
        }

        if (priceVolumeFilter.priceMovingAbove && !priceVolumeFilter.average.isAllItemsElement) {
            arguments.add(Argument(listOf(priceMovingAboveClause), priceVolumeFilter.average.id))
        }

        if (priceVolumeFilter.isEtfTechnicalsValuesSelected()) {
            arguments.add(Argument(getEtfTechnicalsValuesClauses(priceVolumeFilter), ETF_TECHNICALS_VALUES_FIELD))
        }

        return PartialScreenerParams(arguments)
    }

    private fun getEtfTechnicalsValuesClauses(etfTechnicalsFilter: EtfTechnicalsFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (etfTechnicalsFilter.bottom20) {
            clauses.addAll(bottom20Clauses)
        }
        if (etfTechnicalsFilter.belowMid20) {
            clauses.addAll(belowMid20Clauses)
        }
        if (etfTechnicalsFilter.mid20) {
            clauses.addAll(mid20Clauses)
        }
        if (etfTechnicalsFilter.aboveMid20) {
            clauses.addAll(aboveMid20Clauses)
        }
        if (etfTechnicalsFilter.top20) {
            clauses.addAll(top20Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private val priceMovingBelowClause = Clause(OPERATOR_LIKE, listOf("Price Moving Below"))
    private val priceMovingAboveClause = Clause(OPERATOR_LIKE, listOf("Price Moving Above"))

    private val bottom20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("21"))
    )
    private val belowMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("21")),
        Clause(OPERATOR_LESS_THAN, listOf("41"))
    )
    private val mid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("41")),
        Clause(OPERATOR_LESS_THAN, listOf("61"))
    )
    private val aboveMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("61")),
        Clause(OPERATOR_LESS_THAN, listOf("81"))
    )
    private val top20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("81")),
        Clause(OPERATOR_LESS_THAN, listOf("101"))
    )
}
