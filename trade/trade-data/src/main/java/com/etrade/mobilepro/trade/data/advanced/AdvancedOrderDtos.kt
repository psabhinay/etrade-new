package com.etrade.mobilepro.trade.data.advanced

import com.etrade.mobilepro.instrument.InstrumentType
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class AdvancedOrderServerMessageDto(
    @Json(name = "code")
    val code: String,
    @Json(name = "message")
    val message: String
)

@JsonClass(generateAdapter = true)
class AdvancedExecInstructionDto(
    @Json(name = "AonFlag")
    val aonFlag: String
)

@JsonClass(generateAdapter = true)
class AdvancedLegInfoDto(
    @Json(name = "OrderAction")
    val orderAction: String,
    @Json(name = "PositionEffect")
    val positionEffect: String,
    @Json(name = "QuantityValue")
    val quantity: String
)

@JsonClass(generateAdapter = true)
class AdvancedOrderDetailDto(
    @Json(name = "AccountId")
    val accountId: String,
    @Json(name = "LimitPrice")
    val limitPrice: String,
    @Json(name = "OrderTerm")
    val orderTerm: String,
    @Json(name = "OrderTrigger")
    val orderTrigger: String,
    @Json(name = "OrderType")
    val orderType: String,
    @Json(name = "StopPrice")
    val stopPrice: String
)

@JsonClass(generateAdapter = true)
class AdvancedOrderDto(
    @Json(name = "OffsetType")
    val offsetType: String,
    @Json(name = "OffsetValue")
    val offsetValue: String,
    @Json(name = "InitialStopPrice")
    val initialStopPrice: String,
    @Json(name = "AoOrderType")
    val aoOrderType: String,
    @Json(name = "AoTerm")
    val aoTerm: String,
    @Json(name = "FollowPrice")
    val followPrice: String,
    @Json(name = "AoCondSecurityType")
    val securityType: InstrumentType?,
    @Json(name = "AoCondSymbol")
    val symbol: String?,
    @Json(name = "AoCondDisplaySymbol")
    val displaySymbol: String?
)

@JsonClass(generateAdapter = true)
class AdvancedOrderSvcResponseDto(
    @Json(name = "OrderValue")
    val orderValue: String,
    @Json(name = "PreviewId")
    val previewId: String,
    @Json(name = "TimePlaced")
    val timePlaced: String,
    @Json(name = "TotalFee")
    val totalFee: String
)

@JsonClass(generateAdapter = true)
class AdvancedProductIdDto(
    @Json(name = "Symbol")
    val symbol: String
)
