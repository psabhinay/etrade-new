package com.etrade.mobilepro.screener.presentation.etf.technicals

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.screener.api.MovingAverage
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.datasource.MovingAveragesDataSource
import com.etrade.mobilepro.screener.api.filter.etf.EtfTechnicalsFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Etf
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class EtfTechnicalsViewModel @Inject constructor(
    private val movingAveragesDataSource: MovingAveragesDataSource,
    aggregator: ScreenerFilterAggregator,
    @Etf screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<EtfTechnicalsFilter, EtfTechnicalsViewModel.ViewState>(
    aggregator,
    ScreenerType.Etf.Technicals,
    screenerInteractor
) {
    internal val averagePercentData: LiveData<List<DropDownMultiSelector.Selectable<MovingAverage>>> = Transformations.map(viewState) {
        movingAveragesDataSource.getMovingAverages().map { movingAverage ->
            DropDownMultiSelector.Selectable(
                title = movingAverage.title,
                value = movingAverage,
                selected = movingAverage.id == it.filter.average.id
            )
        }
    }

    override fun provideInitialState(initialFilter: EtfTechnicalsFilter): ViewState = ViewState(
        filter = initialFilter
    )

    internal fun averageSelected(average: MovingAverage) {
        updateState {
            it.copy(filter = it.filter.copy(average = average))
        }
    }

    internal fun priceMovingToggled(priceMoving: PriceMoving, isChecked: Boolean) {
        updateState {
            var filter = it.filter.copy(
                priceMovingAbove = if (priceMoving == PriceMoving.ABOVE) { isChecked } else { false },
                priceMovingBelow = if (priceMoving == PriceMoving.BELOW) { isChecked } else { false }
            )
            if (!filter.priceMovingBelow && !filter.priceMovingAbove) {
                filter = filter.copy(average = movingAveragesDataSource.getEmptyItem())
            }
            it.copy(filter = filter)
        }
    }

    data class ViewState(
        override val filter: EtfTechnicalsFilter
    ) : BaseFilterScreenViewModel.ViewState<EtfTechnicalsFilter> {
        fun isAverageEnabled() = filter.priceMovingBelow || filter.priceMovingAbove
    }
}
