package com.etrade.mobilepro.orders.adapter

import com.etrade.mobilepro.orders.api.OrderStatus

data class OrderFilterItem(val label: String, val status: OrderStatus) {
    override fun toString(): String {
        return label
    }
}
