package com.etrade.mobilepro.screener.presentation.common

import android.app.Dialog
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.util.android.textutil.TextUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class ScreenerPopupFragment : DialogFragment() {
    val popupText by lazy {
        arguments?.let {
            ScreenerPopupFragmentArgs.fromBundle(it).popupText
        } ?: ""
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val popup = createPopup(TextUtil.convertTextWithHmlTagsToSpannable(popupText))
        popup.setOnShowListener {
            popup.findViewById<TextView>(android.R.id.message)?.movementMethod = LinkMovementMethod.getInstance()
        }
        return popup
    }

    private fun createPopup(text: CharSequence): AlertDialog =
        MaterialAlertDialogBuilder(requireContext())
            .setCancelable(false)
            .setTitle(R.string.please_note)
            .setMessage(text)
            .setPositiveButton(R.string.ok) { _, _ ->
                dismiss()
            }
            .create()
}
