package org.chromium.customtabsclient.shared

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toolbar
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.util.android.binding.viewBinding
import org.chromium.customtabsclient.shared.databinding.NavigableWebviewBinding
import javax.inject.Inject

/**
 * A navigable webview fragment that has a top label and back button
 * to be able to navigate inside of a webview
 */
open class NavigableWebViewFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    @DeviceType private val deviceType: String
) : WebViewFragment(deviceType) {

    private val _binding by viewBinding(NavigableWebviewBinding::bind)
    open val navigableBinding: NavigableWebviewBinding
        get() = _binding

    override val webviewBinding
        get() = navigableBinding.webviewContainer

    override val layoutRes = R.layout.navigable_webview

    /**
     * Navigable web view model
     */
    protected val navigableWebViewModel: NavigableWebViewModel by viewModels { viewModelFactory }

    /**
     * Custom back button in the top of the webview
     */
    protected val backButton: ImageView
        get() = navigableBinding.back

    /**
     * Custom header in the top of the webview
     */
    protected val toolbarTitle: TextView
        get() = navigableBinding.title

    /**
     * Container for the header and back button
     */
    protected val toolbarContainer: Toolbar
        get() = navigableBinding.toolbar

    /**
     * Hide toolbar
     */
    protected fun hideToolbar() {
        toolbarContainer.visibility = View.GONE
    }

    /**
     * Show toolbar
     */
    protected fun showToolbar() {
        toolbarContainer.visibility = View.VISIBLE
    }

    /**
     * Update toolbar title
     */
    protected fun updateHeaderText(header: String) {
        toolbarTitle.text = header
    }
}
