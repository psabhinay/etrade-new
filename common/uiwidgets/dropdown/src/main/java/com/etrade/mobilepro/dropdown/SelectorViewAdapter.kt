package com.etrade.mobilepro.dropdown

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.Group
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView

class SelectorViewAdapter(private val dataSet: List<DropDownItem>, private val onItemClicked: (Int) -> Unit) :
    RecyclerView.Adapter<SelectorViewAdapter.ViewHolder>() {
    var selectedPosition: Int? = null
    var dividerPosition: Int? = null
    var ghostText: String? = null

    private val ghostTextPosition: Int = dataSet.size - 1

    inner class ViewHolder(viewGroup: ViewGroup) : RecyclerView.ViewHolder(viewGroup) {
        val title: TextView = viewGroup.findViewById(R.id.selector_row_text)
        val subTitle: TextView = viewGroup.findViewById(R.id.selector_row_description)
        val container: LinearLayout = viewGroup.findViewById(R.id.description_container)
        val thickDividerGroup: Group = viewGroup.findViewById(R.id.selector_row_divider_thick_group)
        val ghostTextView: TextView = viewGroup.findViewById(R.id.selector_row_ghost_text)
        val selectedCheckMark: ImageView = viewGroup.findViewById(R.id.selector_row_image_check)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val viewGroup = LayoutInflater.from(parent.context)
            .inflate(R.layout.bottom_sheet_selector_row, parent, false) as ViewGroup
        return ViewHolder(viewGroup)
    }

    override fun getItemCount() = dataSet.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position >= dataSet.size) {
            return
        }
        handleSelectedState(position, holder)
        handleDivider(position, holder)
        handleGhostText(position, holder)

        val dropDownItem: DropDownItem = dataSet[position]
        val textColorRes = if (dropDownItem.isEnabled) {
            R.color.textColorPrimary
        } else {
            R.color.textColorSecondary
        }
        val textColor = ContextCompat.getColor(holder.container.context, textColorRes)
        holder.title.setUpTitleAndDescription(textColor, dropDownItem)
        holder.subTitle.setUpSubtitle(textColor, dropDownItem)

        arrayListOf(holder.container, holder.title, holder.subTitle).forEach {
            it.isEnabled = dropDownItem.isEnabled
            it.setOnClickListener {
                selectedPosition = position
                onItemClicked(position)
            }
        }
    }

    private fun TextView.setUpTitleAndDescription(textColor: Int, dropDownItem: DropDownItem) {
        text = dropDownItem.title
        contentDescription = dropDownItem.contentDescription
        setTextColor(textColor)
    }

    private fun TextView.setUpSubtitle(textColor: Int, dropDownItem: DropDownItem) {
        if (dropDownItem.description.isNullOrEmpty()) {
            visibility = View.GONE
        } else {
            text = dropDownItem.description
            setTextColor(textColor)
            setSingleLine(dropDownItem.isSingleLineDescription)
            ellipsize = if (dropDownItem.isSingleLineDescription) {
                TextUtils.TruncateAt.END
            } else {
                null
            }
            visibility = View.VISIBLE
        }
    }

    private fun handleGhostText(position: Int, holder: ViewHolder) {
        ghostText?.also {
            if (position == ghostTextPosition) {
                holder.ghostTextView.text = it
                holder.ghostTextView.visibility = View.VISIBLE
            } else {
                holder.ghostTextView.visibility = View.GONE
            }
        }
    }

    private fun handleDivider(position: Int, holder: ViewHolder) {
        if (position == dividerPosition) {
            holder.thickDividerGroup.visibility = View.VISIBLE
        } else {
            holder.thickDividerGroup.visibility = View.GONE
        }
    }

    private fun handleSelectedState(position: Int, holder: ViewHolder) {
        if (position == selectedPosition) {
            holder.selectedCheckMark.setImageResource(R.drawable.ic_tableview_checkmark_autocolor)
            holder.selectedCheckMark.isSelected = true
        } else {
            holder.selectedCheckMark.setImageDrawable(null)
            holder.selectedCheckMark.isSelected = false
        }
    }
}
