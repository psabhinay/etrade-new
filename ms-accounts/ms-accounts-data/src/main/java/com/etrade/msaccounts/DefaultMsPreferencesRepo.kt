package com.etrade.msaccounts

import com.etrade.mobilepro.msaccounts.api.MsPreferencesRepo
import com.etrade.mobilepro.msaccounts.api.MsUserPreferences
import com.etrade.mobilepro.util.KeyValueStore
import com.squareup.moshi.Moshi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import org.slf4j.LoggerFactory
import javax.inject.Inject

private const val MS_USER_PREFERENCES_KEY = "msUserPreferences"

class DefaultMsPreferencesRepo @Inject constructor(
    moshi: Moshi,
    private val keyValueStore: KeyValueStore
) : MsPreferencesRepo {

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    private val moshiAdapter = moshi.adapter(MsUserPreferences::class.java).lenient().indent("")

    private val _msUserPreferences by lazy { MutableStateFlow(getInitialMSUserPreferences()) }

    override val msUserPreferences: StateFlow<MsUserPreferences> by lazy { _msUserPreferences.asStateFlow() }

    override fun updateMSUserPreferences(nextPrefs: MsUserPreferences) {
        logger.debug("updateMSUserPreferences $nextPrefs")

        keyValueStore.putString(MS_USER_PREFERENCES_KEY, moshiAdapter.toJson(nextPrefs))
        _msUserPreferences.value = nextPrefs
    }

    private fun getInitialMSUserPreferences(): MsUserPreferences {
        val prefs = keyValueStore.getString(MS_USER_PREFERENCES_KEY)?.let {
            moshiAdapter.fromJson(it)
        }
        logger.debug("getInitialMSUserPreferences $prefs")
        return prefs ?: MsUserPreferences()
    }
}
