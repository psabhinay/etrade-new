package com.etrade.mobilepro.order.details.dto;

import com.etrade.mobilepro.backends.neo.MessageDto;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0087\b\u0018\u00002\u00020\u0001B)\u0012\u0010\b\u0001\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\u0010\b\u0001\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0006J\u0011\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0011\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0003J-\u0010\f\u001a\u00020\u00002\u0010\b\u0003\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\u0010\b\u0003\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001R\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0019\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\b\u00a8\u0006\u0014"}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/OrderGroupCancelResponseDto;", "", "errors", "", "Lcom/etrade/mobilepro/backends/neo/MessageDto;", "warnings", "(Ljava/util/List;Ljava/util/List;)V", "getErrors", "()Ljava/util/List;", "getWarnings", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "orders-details-data"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class OrderGroupCancelResponseDto {
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<com.etrade.mobilepro.backends.neo.MessageDto> errors = null;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<com.etrade.mobilepro.backends.neo.MessageDto> warnings = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.OrderGroupCancelResponseDto copy(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "invest.trading.errors")
    java.util.List<com.etrade.mobilepro.backends.neo.MessageDto> errors, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "invest.trading.warnings")
    java.util.List<com.etrade.mobilepro.backends.neo.MessageDto> warnings) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public OrderGroupCancelResponseDto(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "invest.trading.errors")
    java.util.List<com.etrade.mobilepro.backends.neo.MessageDto> errors, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "invest.trading.warnings")
    java.util.List<com.etrade.mobilepro.backends.neo.MessageDto> warnings) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.etrade.mobilepro.backends.neo.MessageDto> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.etrade.mobilepro.backends.neo.MessageDto> getErrors() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.etrade.mobilepro.backends.neo.MessageDto> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.etrade.mobilepro.backends.neo.MessageDto> getWarnings() {
        return null;
    }
}