package com.etrade.mobilepro.trade.presentation.form

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.trade.presentation.usecase.CreateConditionalTradeFormParameter

interface ConditionalOrderFormFactory {
    suspend fun createConditionalOrderForm(parameter: CreateConditionalTradeFormParameter): ETResult<LiveData<Form>>
}
