package com.etrade.mobilepro.compose.components

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.Styles
import com.etrade.mobilepro.common.compose.theme.etColors
import com.etrade.mobilepro.compose.modifiers.etBorder
import com.etrade.mobilepro.util.SortOrder
import com.etrade.mobilepro.util.color.DataColor
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged

private const val SORT_INDICATOR_ANGLE_180 = -180F
private const val SORT_INDICATOR_ANGLE_0 = 0F
private const val ROW_HEADER_SIZE_OFFSET = 30

data class TableViewColumn(
    val text: String
)

data class TableViewRowHeader(
    val text: String,
    val subText: String?
)

data class TableViewCell(
    val text: String,
    val color: DataColor
)

data class TableViewSortState(
    val sortOrder: SortOrder = SortOrder.UNDEFINED,
    val sortedColumnPosition: Int? = null,
)

@Suppress("LongMethod", "ComplexMethod", "LongParameterList")
@ExperimentalFoundationApi
@Composable
fun ComposeTableView(
    columns: List<TableViewColumn>,
    rowHeaders: List<TableViewRowHeader>,
    cells: List<List<TableViewCell>>,
    sortState: TableViewSortState,
    onScroll: ((Int, Int) -> Unit)? = null,
    onSort: ((Int?, Int?) -> Unit)? = null,
    onRowHeaderClick: ((Int) -> Unit)? = null,
    contentPadding: PaddingValues = PaddingValues(0.dp),
    Footer: (LazyListScope.() -> Unit) = {}
) {
    val borderStroke = BorderStroke(Dimens.Size1dp, MaterialTheme.etColors.lightGrey)
    val horizontalScrollState = rememberScrollState()
    val lazyColumnState = rememberLazyListState()
    val columnHeaderRowBgColor = MaterialTheme.etColors.bottomSheetSurface

    val onSortRemember: ((Int?) -> Unit) by remember {
        mutableStateOf({ columnIndex ->
            onSort?.invoke(columnIndex, lazyColumnState.layoutInfo.visibleItemsInfo.size - 1)
        })
    }

    val tableViewState = remember {
        TableViewState(rowHeaders.size + 1, columns.size + 1)
    }

    tableViewState.isStateChange(rowHeaders.size + 1, columns.size + 1)

    LazyColumn(
        state = lazyColumnState,
        contentPadding = contentPadding
    ) {
        ColumnHeaderRow(tableViewState, borderStroke, columnHeaderRowBgColor) {
            ColumnHeaderCell(
                Modifier.tableCornerModifier(borderStroke),
                tableViewState,
                columnIndex = null,
                text = stringResource(id = R.string.symbol),
                horizontalArrangement = Arrangement.Start,
                textAlign = TextAlign.Start,
                sortState = sortState,
                onSort = onSortRemember
            )
            Row(modifier = Modifier.horizontalScroll(horizontalScrollState), verticalAlignment = Alignment.CenterVertically) {
                columns.forEachIndexed { columnIndex, columnHeader ->
                    if (columnIndex == 0) {
                        Spacer(modifier = Modifier.width(Dimens.Size16dp))
                    }
                    ColumnHeaderCell(
                        Modifier.tableHeaderModifier(),
                        tableViewState,
                        columnIndex = columnIndex + 1,
                        text = columnHeader.text,
                        sortState = sortState,
                        onSort = onSortRemember
                    )
                    Spacer(modifier = Modifier.width(Dimens.Size16dp))
                }
            }
        }

        TableBody(rowHeaders, cells) { rowIndex, rowHeader, cells ->
            TableRow(tableViewState, borderStroke) {
                RowHeaderCell(
                    Modifier.rowHeaderModifier(borderStroke, rowIndex, onRowHeaderClick),
                    rowHeader,
                    tableViewState,
                    rowIndex + 1
                )
                Row(
                    modifier = Modifier
                        .horizontalScroll(horizontalScrollState)
                        .padding(vertical = Dimens.Size12dp),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    cells.forEachIndexed { columnIndex, cell ->
                        if (columnIndex == 0) {
                            Spacer(modifier = Modifier.width(Dimens.Size16dp))
                        }
                        TableViewCell(cell, tableViewState, rowIndex + 1, columnIndex + 1)
                        Spacer(modifier = Modifier.width(Dimens.Size16dp))
                    }
                }
            }
        }

        Footer()

        if (!tableViewState.isReadyToDraw) {
            tableViewState.updateReadyToDraw(lazyColumnState.layoutInfo.visibleItemsInfo.size)
        }
    }
    onScroll?.let {
        LaunchedEffect(lazyColumnState) {
            snapshotFlow {
                Pair(lazyColumnState.firstVisibleItemIndex, lazyColumnState.layoutInfo.visibleItemsInfo.size)
            }.distinctUntilChanged().collect {
                it(it.first, it.first + it.second)
            }
        }
    }
}

@Composable
private fun RowHeaderCell(
    modifier: Modifier = Modifier,
    rowHeader: TableViewRowHeader,
    tableViewState: TableViewState,
    rowIndex: Int,
) {
    val isReadyToDraw = tableViewState.isReadyToDraw
    val width = tableViewState.getWidth(0, isReadyToDraw)
    Column(
        modifier
            .padding(Dimens.Size16dp)
            .drawModifier(isReadyToDraw, width)
            .onSizeChanged { result -> tableViewState.setCellWidth(rowIndex, 0, result.width) }
    ) {
        Text(
            text = rowHeader.text,
            textAlign = TextAlign.Start,
            softWrap = false,
            style = Styles.Regular16sp,
            color = MaterialTheme.etColors.purple,
        )
        val subText = rowHeader.subText
        if (!subText.isNullOrEmpty()) {
            Text(
                text = subText,
                textAlign = TextAlign.Start,
                style = Styles.Regular10sp,
                color = MaterialTheme.etColors.purple,
            )
        }
    }
}

@Composable
private fun TableViewCell(
    cellData: TableViewCell,
    tableViewState: TableViewState,
    rowIndex: Int,
    columnIndex: Int
) {
    val isReadyToDraw = tableViewState.isReadyToDraw
    val width = tableViewState.getWidth(columnIndex, isReadyToDraw)
    val textColor = resolveTextColor(dataColor = cellData.color)
    val modifier = resolveBackgroundColor(dataColor = cellData.color)?.let {
        Modifier.background(it, shape = RoundedCornerShape(Dimens.Size4dp))
    } ?: Modifier
    Text(
        text = cellData.text,
        textAlign = TextAlign.End,
        color = textColor,
        softWrap = false,
        style = tableViewState.getTextStyle(rowIndex, columnIndex),
        modifier = modifier
            .padding(Dimens.Size4dp)
            .drawModifier(isReadyToDraw, width),
        onTextLayout = { result -> tableViewState.setTextWidth(rowIndex, columnIndex, result) }
    )
}

@Suppress("LongParameterList")
@Composable
fun ColumnHeaderCell(
    modifier: Modifier = Modifier,
    tableViewState: TableViewState,
    columnIndex: Int?,
    horizontalArrangement: Arrangement.Horizontal = Arrangement.End,
    text: String,
    textStyle: TextStyle = Styles.Regular12sp,
    textAlign: TextAlign = TextAlign.End,
    sortState: TableViewSortState? = null,
    onSort: ((Int?) -> Unit)? = null
) {
    val updatedColumnIndex = columnIndex ?: 0
    val isReadyToDraw = tableViewState.isReadyToDraw
    val width = tableViewState.getWidth(updatedColumnIndex, isReadyToDraw)
    Row(
        modifier = modifier
            .drawModifier(isReadyToDraw, width)
            .clickable {
                onSort?.invoke(sortColumnIndex(columnIndex))
            }
            .onSizeChanged { result -> tableViewState.setCellWidth(0, updatedColumnIndex, result.width + ROW_HEADER_SIZE_OFFSET) },
        horizontalArrangement = horizontalArrangement,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = text,
            softWrap = false,
            textAlign = textAlign,
            style = textStyle
        )
        if (isSortStateChange(sortState, columnIndex)) {
            Triangle(
                modifier = Modifier
                    .padding(start = Dimens.Size4dp)
                    .graphicsLayer(
                        rotationX = animateSortState(sortState),
                    ),
                backgroundColor = MaterialTheme.etColors.grey
            )
        }
    }
}

@Composable
private fun TableRow(
    tableViewState: TableViewState,
    borderStroke: BorderStroke,
    content: @Composable RowScope.() -> Unit
) {
    Row(
        Modifier.etBorder(
            bottom = borderStroke
        ).drawWithContent {
            if (tableViewState.isReadyToDraw) { drawContent() }
        },
        verticalAlignment = Alignment.CenterVertically,
        content = content
    )
}

@Composable
private fun TableViewState.getWidth(columnIndex: Int, isReadyToDraw: Boolean): Dp {
    return if (isReadyToDraw) {
        getFinalColumnWidth(columnIndex).toDp()
    } else {
        0.dp
    }
}

@Suppress("FunctionNaming")
private fun LazyListScope.TableBody(
    rowHeaders: List<TableViewRowHeader>,
    cells: List<List<TableViewCell>>,
    content: @Composable (Int, TableViewRowHeader, List<TableViewCell>) -> Unit
) {
    cells.forEachIndexed { rowIndex, row ->
        item {
            content(rowIndex, rowHeaders[rowIndex], row)
        }
    }
}

@Suppress("FunctionNaming")
@ExperimentalFoundationApi
private fun LazyListScope.ColumnHeaderRow(
    tableViewState: TableViewState,
    borderStroke: BorderStroke,
    bgColor: Color,
    content: @Composable RowScope.() -> Unit
) {
    stickyHeader {
        Row(
            Modifier.columnRowModifier(borderStroke, bgColor).drawWithContent {
                if (tableViewState.isReadyToDraw) { drawContent() }
            },
            verticalAlignment = Alignment.CenterVertically,
            content = content
        )
    }
}

@Composable
private fun animateSortState(sortState: TableViewSortState?): Float {
    val rotationX: Float by animateFloatAsState(
        when (sortState?.sortOrder) {
            SortOrder.DESCENDING -> SORT_INDICATOR_ANGLE_180
            SortOrder.ASCENDING -> SORT_INDICATOR_ANGLE_0
            else -> {
                SORT_INDICATOR_ANGLE_0
            }
        }
    )
    return rotationX
}

@Composable
private fun Int.toDp(): Dp = with(LocalDensity.current) { toDp() }

private fun Modifier.drawModifier(ready: Boolean, width: Dp): Modifier {
    val modifier = this
        .drawWithContent {
            if (ready) { drawContent() }
        }
    return if (ready) {
        modifier.requiredWidth(width)
    } else {
        modifier
    }
}

@Composable
private fun resolveTextColor(dataColor: DataColor) = when (dataColor) {
    DataColor.POSITIVE -> MaterialTheme.etColors.green
    DataColor.NEGATIVE -> MaterialTheme.etColors.red
    DataColor.NEUTRAL -> MaterialTheme.colors.onPrimary
    DataColor.FILLED_POSITIVE, DataColor.FILLED_NEGATIVE -> MaterialTheme.etColors.bottomSheetSurface
}

@Composable
private fun resolveBackgroundColor(dataColor: DataColor) = when (dataColor) {
    DataColor.FILLED_POSITIVE -> MaterialTheme.etColors.green
    DataColor.FILLED_NEGATIVE -> MaterialTheme.etColors.red
    else -> null
}

private fun Modifier.rowHeaderModifier(
    borderStroke: BorderStroke,
    rowIndex: Int,
    onRowHeaderClick: ((Int) -> Unit)? = null
) = etBorder(end = borderStroke)
    .clickable { onRowHeaderClick?.invoke(rowIndex) }

private fun Modifier.columnRowModifier(borderStroke: BorderStroke, bgColor: Color) = requiredHeight(Dimens.Size40dp)
    .background(bgColor)
    .etBorder(bottom = borderStroke)

private fun Modifier.tableCornerModifier(borderStroke: BorderStroke) = etBorder(end = borderStroke)
    .padding(horizontal = Dimens.Size16dp)
    .requiredHeight(Dimens.Size40dp)

private fun Modifier.tableHeaderModifier() = padding(horizontal = Dimens.Size4dp).requiredHeight(Dimens.Size40dp)

private fun sortColumnIndex(columnIndex: Int?) = if (columnIndex == null) {
    columnIndex
} else {
    columnIndex - 1
}

private fun isSortStateChange(sortState: TableViewSortState? = null, columnIndex: Int?): Boolean {
    return sortColumnIndex(columnIndex) == sortState?.sortedColumnPosition && sortState?.sortOrder != SortOrder.UNDEFINED
}
