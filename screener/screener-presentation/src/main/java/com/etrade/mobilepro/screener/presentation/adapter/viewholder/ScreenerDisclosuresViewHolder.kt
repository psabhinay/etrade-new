package com.etrade.mobilepro.screener.presentation.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.presentation.databinding.ItemScreenerDisclosuresBinding

class ScreenerDisclosuresViewHolder(private val binding: ItemScreenerDisclosuresBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(item: ScreenerItem.DisclosuresText) {
        binding.disclosureContent.text = item.text
    }
}
