package com.etrade.mobilepro.streaming.lightstreamer.level2

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.StreamIdFactory
import javax.inject.Inject

/**
 * LightStreamer implementation of [StreamIdFactory] for Level 2 quotes.
 */
class Level2StreamIdFactory @Inject constructor(private val parameters: Parameters) : StreamIdFactory<Level2BookType> {

    override fun createStreamId(symbol: String, instrumentType: InstrumentType, payload: Level2BookType?): String {
        return if (instrumentType.isOption) {
            "L2OPTION:$symbol:${Level2BookType.OPTION.bookName}:${parameters.bookCount}"
        } else {
            "L2STOCK:$symbol:${payload?.bookName}:${parameters.bookCount}"
        }
    }

    /**
     * Parameters for creation of stream ids of LightStreamer Level 2 quotes.
     */
    interface Parameters {
        val bookCount: Int
    }
}
