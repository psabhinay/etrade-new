package com.etrade.mobilepro.billpay.api

sealed class SearchPayeeResult {
    data class Success(val payeeSearchResults: List<SearchPayeeResultItem>) : SearchPayeeResult()
    object NoResults : SearchPayeeResult()
    object Error : SearchPayeeResult()
}
