package com.etrade.mobilepro.alerts.presentation.setalert.setvalue

import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.alerts.api.model.AlertValueType
import com.etrade.mobilepro.alerts.api.model.NewsAlertValueType
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import javax.annotation.concurrent.NotThreadSafe

interface AlertSettingsHolder {
    val inputValues: Map<AlertValueType, MutableLiveData<String>>
    val switchedValues: Map<NewsAlertValueType, MutableLiveData<Boolean>>
    fun inputValueHolder(type: AlertValueType): MutableLiveData<String>
    fun switchValueHolder(type: NewsAlertValueType): MutableLiveData<Boolean>
    fun setupInputValues(alert: UserAlertItem.Alert)
    fun setupSwitchValues(alert: UserAlertItem.NewsAlert)
    fun resetContents()
}

@NotThreadSafe
class DefaultAlertSettingsHolder : AlertSettingsHolder {
    override val inputValues = mapOf(
        AlertValueType.HIGH_TARGET_PRICE to MutableLiveData<String>().apply { value = "" },
        AlertValueType.LOW_TARGET_PRICE to MutableLiveData<String>().apply { value = "" },
        AlertValueType.HIGH_DAILY_PERCENT to MutableLiveData<String>().apply { value = "" },
        AlertValueType.LOW_DAILY_PERCENT to MutableLiveData<String>().apply { value = "" },
        AlertValueType.HIGH_PE_TARGET to MutableLiveData<String>().apply { value = "" },
        AlertValueType.LOW_PE_TARGET to MutableLiveData<String>().apply { value = "" },
        AlertValueType.DAILY_VOLUME to MutableLiveData<String>().apply { value = "" }
    )
    override val switchedValues = mapOf(
        NewsAlertValueType.UP_DOWN to MutableLiveData<Boolean>(),
        NewsAlertValueType.IN_PLAY to MutableLiveData(),
        NewsAlertValueType.EARNINGS to MutableLiveData(),
        NewsAlertValueType.SPLIT_NOTICE to MutableLiveData()
    )

    override fun inputValueHolder(type: AlertValueType): MutableLiveData<String> = inputValues[type] ?: throw IllegalArgumentException("Unknown type $type")

    override fun switchValueHolder(type: NewsAlertValueType): MutableLiveData<Boolean> =
        switchedValues[type] ?: throw IllegalArgumentException("Unknown type $type")

    override fun setupInputValues(alert: UserAlertItem.Alert) {
        inputValues[AlertValueType.HIGH_TARGET_PRICE]?.value = alert.targetPriceHigh.activeValue()
        inputValues[AlertValueType.LOW_TARGET_PRICE]?.value = alert.targetPriceLow.activeValue()
        inputValues[AlertValueType.HIGH_DAILY_PERCENT]?.value = alert.dailyPercentHigh.activeValue()
        inputValues[AlertValueType.LOW_DAILY_PERCENT]?.value = alert.dailyPercentLow.activeValue()
        inputValues[AlertValueType.HIGH_PE_TARGET]?.value = alert.peTargetHigh.activeValue()
        inputValues[AlertValueType.LOW_PE_TARGET]?.value = alert.peTargetLow.activeValue()
        inputValues[AlertValueType.DAILY_VOLUME]?.value = alert.dailyVolume.activeValue()
    }

    override fun setupSwitchValues(alert: UserAlertItem.NewsAlert) {
        switchedValues[NewsAlertValueType.UP_DOWN]?.value = alert.upDownStatus
        switchedValues[NewsAlertValueType.IN_PLAY]?.value = alert.inPlayStatus
        switchedValues[NewsAlertValueType.EARNINGS]?.value = alert.earningsStatus
        switchedValues[NewsAlertValueType.SPLIT_NOTICE]?.value = alert.splitNoticeStatus
    }

    override fun resetContents() {
        inputValues.values.forEach { it.value = "" }
        switchedValues.values.forEach { it.value = false }
    }
}
