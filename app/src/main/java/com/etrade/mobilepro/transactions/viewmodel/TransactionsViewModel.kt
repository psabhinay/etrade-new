package com.etrade.mobilepro.transactions.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.preferences.preference
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.transactions.InstitutionType
import com.etrade.mobilepro.transactions.SweepPreferences
import com.etrade.mobilepro.transactions.TransactionDetailInfo
import com.etrade.mobilepro.transactions.TransactionsResponseInfo
import com.etrade.mobilepro.transactions.api.TransactionsRepo
import com.etrade.mobilepro.transactions.api.TransactionsRequest
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.delegate.suppressInitial
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import org.slf4j.LoggerFactory
import org.threeten.bp.LocalDate
import javax.inject.Inject

private const val SWEEP_TAG = "Sweep"
private const val TRANSACTIONS_CACHE_EXPIRATION = 20000L
private const val NUMBER_OF_MONTH_DIFF = 3L
private const val ERROR_CODE_EMPTY = 20044

private val LOGGER = LoggerFactory.getLogger(TransactionsViewModel::class.java)

class TransactionsViewModel @Inject constructor(
    private val daoProvider: CachedDaoProvider,
    private val user: User,
    private val transactionsRepo: TransactionsRepo,
    private val sweepPreferences: SweepPreferences,
    keyValueStorage: KeyValueStorage
) : ViewModel() {

    private val _viewStates = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState> = _viewStates

    private val _showStartDate = MutableLiveData<Boolean>()
    val showStartDate: LiveData<Boolean> = _showStartDate
    private val _showEndDate = MutableLiveData<Boolean>()
    val showEndDate: LiveData<Boolean> = _showEndDate

    private var transactions = listOf<TransactionDetailInfo>()
    private val compositeDisposable = CompositeDisposable()
    internal var accountId: Long? = null

    private var persistedTransactionsEndDateEpochDay: Int by keyValueStorage.preference(
        key = "persistedTransactionsEndDateEpochDay",
        default = LocalDate.now().toEpochDay().toInt()
    ) // safe for EpochDay

    internal var endDate: LocalDate
        set(value) {
            persistedTransactionsEndDateEpochDay = value.toEpochDay().toInt()
        }
        get() = LocalDate.ofEpochDay(persistedTransactionsEndDateEpochDay.toLong())

    private var persistedTransactionsStartDateEpochDay: Int by keyValueStorage.preference(
        key = "persistedTransactionsStartDateEpochDay",
        default = endDate.minusMonths(NUMBER_OF_MONTH_DIFF).toEpochDay().toInt()
    )

    internal var startDate: LocalDate
        set(value) {
            persistedTransactionsStartDateEpochDay = value.toEpochDay().toInt()
        }
        get() = LocalDate.ofEpochDay(persistedTransactionsStartDateEpochDay.toLong())

    var isSweepActivityEnabled: Boolean by suppressInitial(sweepPreferences.isSweepActivityEnabled) {
        sweepPreferences.isSweepActivityEnabled = isSweepActivityEnabled
        postTransactions(transactions)
    }

    init {
        daoProvider.open()
    }

    fun fetch() {
        getTransactions(true)
    }

    fun refresh() {
        getTransactions(false)
    }

    fun setShowStartDate(shouldShow: Boolean = true) {
        _showStartDate.value = shouldShow
    }

    fun setShowEndDate(shouldShow: Boolean = true) {
        _showEndDate.value = shouldShow
    }

    private fun getTransactions(useCache: Boolean = true) {

        fun getTransactionsError(parameter: String) {
            onError("Unable to get transactions. $parameter is null.")
        }

        accountId?.let { accountId ->
            user.getUserId()?.let { userId ->
                _viewStates.value = ViewState.Loading
                compositeDisposable += transactionsRepo.getTransactions(
                    TransactionsRequest(
                        userId,
                        accountId,
                        getAccountType(accountId),
                        startDate,
                        endDate
                    ),
                    TRANSACTIONS_CACHE_EXPIRATION.takeIf { useCache }
                ).subscribeBy(
                    onNext = ::onNext,
                    onError = ::onError
                )
            } ?: getTransactionsError("userId")
        } ?: getTransactionsError("accountId")
    }

    private fun onNext(resource: Resource<TransactionsResponseInfo>) {
        when (resource) {
            is Resource.Failed -> onError(resource.error)
            is Resource.Success -> onSuccess(resource.data)
            is Resource.Loading -> onLoading()
        }
    }

    private fun getAccountType(accountId: Long): InstitutionType {
        val type = daoProvider.getAccountDao()
            .getAccountWithAccountId(accountId.toString())?._institutionType
        return if (type == InstitutionType.MSSB.name) {
            InstitutionType.MSSB
        } else {
            InstitutionType.ADP
        }
    }

    private fun onLoading() {
        // no-op
    }

    private fun onSuccess(response: TransactionsResponseInfo?) {
        response?.let { data ->
            with(data.errors) {
                firstOrNull { it.errorCode == ERROR_CODE_EMPTY }?.let {
                    postTransactions(emptyList())
                } ?: firstOrNull()?.let {
                    onError("Service call failed: ${it.errorDescription}")
                }
            } ?: postTransactions(data.transactions)
        } ?: onError("Service call returned no data")
    }

    private fun onError(throwable: Throwable? = null) {
        onError("TransactionsViewModel error", throwable)
    }

    private fun onError(message: String, throwable: Throwable? = null) {
        throwable?.let {
            LOGGER.error(message, it)
        } ?: LOGGER.error(message)
        _viewStates.postValue(ViewState.Error)
    }

    private fun postTransactions(transactions: List<TransactionDetailInfo>) {
        this.transactions = transactions
        _viewStates.postValue(
            ViewState.Success(
                transactions.filter {
                    if (isSweepActivityEnabled) { true } else { it.type != SWEEP_TAG }
                }
            )
        )
    }

    override fun onCleared() {
        compositeDisposable.clear()
        daoProvider.close()
        super.onCleared()
    }

    sealed class ViewState {
        object Loading : ViewState()
        object Error : ViewState()
        data class Success(val transactions: List<TransactionDetailInfo>) : ViewState()
    }
}
