package com.etrade.mobilepro.feature

interface Feature {
    val key: String
    val defaultValue: Boolean
    val category: FeatureCategory?
    val title: String?
    val explanation: String?
}

interface FeatureCategory {
    val title: String
    val explanation: String
}

class BaseFeature(
    override val key: String,
    override val defaultValue: Boolean = false,
    override val category: FeatureCategory? = null,
    override val title: String? = null,
    override val explanation: String? = null,
) : Feature

class BaseFeatureCategory(
    override val title: String,
    override val explanation: String
) : FeatureCategory
