package com.etrade.mobilepro.msaccounts.presentation.view

import android.content.Context
import android.util.AttributeSet
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.common.customview.LifecycleLinearLayout
import com.etrade.mobilepro.completeview.model.AccountSummarySection
import com.etrade.mobilepro.msaccounts.presentation.databinding.MsAccountAdditionalLabelBinding
import com.etrade.mobilepro.util.android.extension.layoutInflater

class MsAccountAdditionalLabelsLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : LifecycleLinearLayout(context, attrs) {

    fun bindAccountSummarySections(sections: List<AccountSummarySection>) {
        removeAllViews()
        sections.forEach {
            val binding = MsAccountAdditionalLabelBinding.inflate(layoutInflater, this, true)
            binding.lifecycleOwner = this
            binding.section = it
        }
    }
}

@BindingAdapter("additionalLabels")
fun bindAccountSummarySections(
    sectionsLayout: MsAccountAdditionalLabelsLayout,
    sections: List<AccountSummarySection>?
) {
    sectionsLayout.bindAccountSummarySections(sections.orEmpty())
}
