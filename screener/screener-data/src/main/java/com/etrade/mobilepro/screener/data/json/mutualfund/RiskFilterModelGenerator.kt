package com.etrade.mobilepro.screener.data.json.mutualfund

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.RiskFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val VOLATILE_FIELD = "Beta"
private const val RISK_VALUES_FIELD = "StandardDev"

class RiskFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val riskFilter = screenerFilter as RiskFilter
        val arguments = mutableListOf<Argument>()

        if (riskFilter.isVolatileSelected()) {
            arguments.add(
                Argument(getVolatileClauses(riskFilter), VOLATILE_FIELD)
            )
        }
        if (riskFilter.isRiskValuesSelected()) {
            arguments.add(
                Argument(getRiskValuesClauses(riskFilter), RISK_VALUES_FIELD)
            )
        }

        return PartialScreenerParams(arguments)
    }

    private fun getVolatileClauses(riskFilter: RiskFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (riskFilter.lessVolatile) {
            clauses.addAll(lessVolatileClauses)
        }
        if (riskFilter.asVolatile) {
            clauses.addAll(asVolatileClauses)
        }
        if (riskFilter.moreVolatile) {
            clauses.addAll(moreVolatileClauses)
        }

        return clauses
    }

    private fun getRiskValuesClauses(riskFilter: RiskFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (riskFilter.bottom20) {
            clauses.addAll(bottom20Clauses)
        }
        if (riskFilter.belowMid20) {
            clauses.addAll(belowMid20Clauses)
        }
        if (riskFilter.mid20) {
            clauses.addAll(mid20Clauses)
        }
        if (riskFilter.aboveMid20) {
            clauses.addAll(aboveMid20Clauses)
        }
        if (riskFilter.top20) {
            clauses.addAll(top20Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private val lessVolatileClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("-.75")),
        Clause(OPERATOR_LESS_THAN, listOf(".75"))
    )
    private val asVolatileClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf(".75")),
        Clause(OPERATOR_LESS_THAN, listOf("1.25"))
    )
    private val moreVolatileClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("1.25")),
        Clause(OPERATOR_LESS_THAN, listOf("-.75"))
    )

    private val bottom20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("21"))
    )
    private val belowMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("21")),
        Clause(OPERATOR_LESS_THAN, listOf("41"))
    )
    private val mid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("41")),
        Clause(OPERATOR_LESS_THAN, listOf("61"))
    )
    private val aboveMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("61")),
        Clause(OPERATOR_LESS_THAN, listOf("81"))
    )
    private val top20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("81")),
        Clause(OPERATOR_LESS_THAN, listOf("101"))
    )
}
