package com.etrade.mobilepro.portfolio.tableview

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell

data class PortfolioRecordsTableRow(
    val rowHeader: SymbolCell,
    val cells: List<PortfolioRecordDataCell>,
    val isTaxLot: Boolean,
    val recordId: String,
    @ColorRes val backgroundColor: Int,
    @DrawableRes val backgroundResource: Int
)
