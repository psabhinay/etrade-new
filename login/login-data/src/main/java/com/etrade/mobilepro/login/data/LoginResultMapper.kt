package com.etrade.mobilepro.login.data

import com.etrade.mobilepro.login.api.repo.LoginFailure
import com.etrade.mobilepro.login.api.repo.LoginResult
import com.etrade.mobilepro.login.api.repo.LoginSuccess
import com.etrade.mobilepro.login.data.restclient.dto.LoginUserSSOResponse
import com.etrade.mobilepro.session.api.User

private const val RESULT_TYPE_ERROR = 1
private const val RESULT_TYPE_WARNING = 2

/**
 * converts  Login Response into LoginResult type based on below login scenarios
 *  1. Login Success
 *  2. Login Failure
 *  3. Successful Login with a Warning
 *
 *  for all others cases defaults to Login Failure
 */
internal fun parseLoginResult(response: LoginUserSSOResponse?, user: User): LoginResult {

    response?.result ?: return LoginFailure()

    val sessionError = response.result?.fault?.error

    return when (sessionError?.type) {

        RESULT_TYPE_ERROR -> LoginFailure(mapLoginErrorCode(sessionError.code), sessionError.description)

        RESULT_TYPE_WARNING -> response.userInfoDto?.let { userInfo ->
            user.session = userInfo
            LoginSuccess(userInfo.principalId, userInfo.isInternationalUser, userInfo.isRSATokenUser, userInfo.isRealTimeQuotes, sessionError.description)
        }

        else -> response.userInfoDto?.let {
            user.session = it
            LoginSuccess(it.principalId, it.isInternationalUser, it.isRSATokenUser, it.isRealTimeQuotes)
        }
    } ?: LoginFailure()
}
