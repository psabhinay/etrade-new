package com.etrade.mobilepro.searchingapi

import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SymbolLookUpResult
import io.reactivex.Observable

interface SymbolLookupRepo {
    fun loadSymbolLookup(id: SearchQuery): Observable<SymbolLookUpResult>
}
