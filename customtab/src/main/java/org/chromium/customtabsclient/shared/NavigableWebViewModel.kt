package org.chromium.customtabsclient.shared

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

class NavigableWebViewModel @Inject constructor() :
    ViewModel() {

    private val _headerLiveData: MutableLiveData<ConsumableLiveEvent<String>> = MutableLiveData()
    val headerLiveData: LiveData<ConsumableLiveEvent<String>>
        get() = _headerLiveData

    /**
     * Update header on navigable web view
     * @param header the string to update toolbar header with
     */
    fun updateHeader(header: String) {
        _headerLiveData.postValue(ConsumableLiveEvent(header))
    }
}
