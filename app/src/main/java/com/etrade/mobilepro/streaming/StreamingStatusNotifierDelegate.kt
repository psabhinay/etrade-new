package com.etrade.mobilepro.streaming

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.etrade.eo.streaming.StreamingClientStatus
import com.etrade.eo.streaming.rx.ConnectionStatusEvent
import com.etrade.eo.streaming.rx.RxStreamingStatusProvider
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

internal const val THROTTLE_INTERVAL: Long = 3
private val POPUP_DIALOG_TAG = "${StreamingStatusNotifierDelegate::class.java.canonicalName}_popup"

interface StreamingStatusNotifierDelegate {
    val isUnableToStream: AtomicBoolean
    fun init(lifecycleOwner: LifecycleOwner, parent: FragmentActivity)
    fun showNotificationDialog(forceShow: Boolean = false)
}

class StreamingStatusNotifierDelegateImpl @Inject constructor(
    private val rxStreamingStatusProvider: RxStreamingStatusProvider,
    private val streamingStatusController: StreamingStatusController,
    private val user: User
) : StreamingStatusNotifierDelegate, LifecycleObserver {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    private lateinit var activityRef: WeakReference<FragmentActivity>
    private val compositeDisposable = CompositeDisposable()
    private val isSubscribed: AtomicBoolean = AtomicBoolean(false)
    private var isNotified = AtomicBoolean(false)

    private val activity: FragmentActivity?
        get() = activityRef.get()

    private val isNotificationVisible: Boolean
        get() = activity?.supportFragmentManager?.findFragmentByTag(POPUP_DIALOG_TAG) != null

    private val streamingStatusUpdateListener: (StreamingStatusUpdate) -> Unit = { status ->
        if (user.isRealtimeUser) {
            logger.debug("Got stream enabled event from streamingStatusUpdateListener")
            subscribeToStreamingConnectionStatus()
        } else {
            logger.debug("Got stream disabled event from streamingStatusUpdateListener")
            unsubscribeFromStreamingConnectionStatus()
        }
        if (status is StreamingStatusUpdate.StreamingToggleUpdate && status.enabled && isUnableToStream.get()) {
            streamingStatusController.isStreamingToggleEnabled = false
        }
    }

    override val isUnableToStream: AtomicBoolean = AtomicBoolean(false)

    override fun init(lifecycleOwner: LifecycleOwner, parent: FragmentActivity) {
        activityRef = WeakReference(parent)
        lifecycleOwner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        logger.debug("Got start lifecycle event so will try to subscribeToStreamingConnectionStatus")
        streamingStatusController.subscribeForStreamingStatusUpdates(streamingStatusUpdateListener)
        subscribeToStreamingConnectionStatus()
        logger.debug("Is subscribed to connection status update: ${isSubscribed.get()}")
        logger.debug("Is realtime user: ${user.isRealtimeUser}")
        logger.debug("Is streaming enabled: ${streamingStatusController.isStreamingToggleEnabled}")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        logger.debug("Got stop lifecycle event so will try to unsubscribeFromStreamingConnectionStatus")
        streamingStatusController.unsubscribeFromStreamingStatusUpdates(streamingStatusUpdateListener)
        unsubscribeFromStreamingConnectionStatus()
        logger.debug("Is subscribed to connection status update: ${isSubscribed.get()}")
        logger.debug("Is realtime user: ${user.isRealtimeUser}")
        logger.debug("Is streaming enabled: ${streamingStatusController.isStreamingToggleEnabled}")
    }

    private fun unsubscribeFromStreamingConnectionStatus() {
        if (isSubscribed.getAndSet(false)) {
            logger.debug("Unsubscribe from streaming connection status provider")
            compositeDisposable.clear()
        }
    }

    private fun subscribeToStreamingConnectionStatus() {
        if (user.isRealtimeUser && !isSubscribed.get()) {
            isSubscribed.set(true)
            logger.debug("Subscribed to streaming connection status provider")
            compositeDisposable.clear()
            compositeDisposable.add(
                rxStreamingStatusProvider.getConnectionStatus()
                    .doOnNext {
                        logger.debug("RX LS connection event: $it")
                    }
                    .distinctUntilChanged()
                    .throttleLast(THROTTLE_INTERVAL, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(
                        onNext = { event ->
                            event?.let {
                                if (it is ConnectionStatusEvent) {
                                    handleConnectionStatusChange(it.status)
                                }
                            }
                        },
                        onError = { logger.warn(it.message) }
                    )
            )
        }
    }

    private fun handleConnectionStatusChange(status: StreamingClientStatus) {
        when (status) {
            StreamingClientStatus.CONNECTED -> {
                isUnableToStream.set(false)
                isNotified.set(false)
            }
            StreamingClientStatus.CONNECTING -> {
                val isStreamEnabled = streamingStatusController.isStreamingToggleEnabled
                streamingStatusController.isStreamingToggleEnabled = false
                if (!isUnableToStream.getAndSet(true) && isStreamEnabled) {
                    showNotificationDialog()
                }
            }
            else -> { /* Do Nothing */ }
        }
    }

    override fun showNotificationDialog(forceShow: Boolean) {
        activity?.let {
            if (!isNotificationVisible && (forceShow || !isNotified.get())) {
                isNotified.set(true)
                if (!forceShow) {
                    logger.debug("Ls client disconnected showing the notification")
                }
                val dialog = CustomDialogFragment.newInstance(
                    title = it.getString(R.string.error),
                    message = it.getString(R.string.mda_unable_to_stream),
                    okTitle = it.getString(R.string.ok),
                    cancelable = false
                )
                dialog.show(it.supportFragmentManager, POPUP_DIALOG_TAG)
            }
        }
    }
}
