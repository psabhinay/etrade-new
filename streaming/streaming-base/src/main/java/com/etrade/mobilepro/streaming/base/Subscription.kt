package com.etrade.mobilepro.streaming.base

import com.etrade.eo.streaming.rx.StreamingEvent
import com.etrade.eo.streaming.rx.onDataEvent
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject

/**
 * Subscription to a stream of values that allows switching of [source]s, but exposes to a client the same reference to [observable] object.
 *
 * When new [source] is set, previously provided source is disposed. Values that are emitted by the new source are exposed through [observable] property.
 * Clients must subscribe to it, rather to the source.
 *
 * To listen for a subscription changes use [observable].
 *
 * @param onObservableDisposed Called when [observable] signals onError or onCompleted, or it gets disposed by a the downstream.
 */
internal class Subscription<T : Any>(private val onObservableDisposed: () -> Unit) {

    /**
     * Underlying source of values.
     */
    var source: Observable<StreamingEvent>? = null
        set(value) {
            field = value
            disposable?.dispose()
            if (value != null) {
                subscribeTo(value)
            }
        }

    private var disposable: Disposable? = null

    private val subject: PublishSubject<T> = PublishSubject.create()

    /**
     * Stable reference to observable object, that can be used to observe the values emitted by different [source]s. Allows multiple observers.
     */
    val observable: Observable<T> = subject.doFinally {
        disposable?.dispose()
        onObservableDisposed()
    }.share()

    private fun subscribeTo(value: Observable<StreamingEvent>) {
        disposable = value.onDataEvent<T>()
            .subscribeBy(
                onNext = { subject.onNext(it) },
                onError = { subject.onError(it) }
            )
    }
}
