package com.etrade.mobilepro.viewmodel

import androidx.lifecycle.LiveData
import androidx.navigation.NavController
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

typealias NavigationSignal = (NavController) -> Unit

/**
 * Marks a view model that emits navigation signals.
 */
interface NavigationSignalViewModel {

    /**
     * Navigation signal is one-time event that can only be consumed once.
     */
    val navigationSignal: LiveData<ConsumableLiveEvent<NavigationSignal>>
}
