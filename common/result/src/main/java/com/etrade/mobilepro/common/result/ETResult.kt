@file:Suppress("UNCHECKED_CAST", "TooGenericExceptionCaught")

package com.etrade.mobilepro.common.result

/**
 * A discriminated union that encapsulates a successful outcome with a value of type [T]
 * or a failure with an arbitrary [Throwable] exception. Based on [kotlin.Result] but
 * not inline, so that it can be returned as a function result.
 */
class ETResult<out T> @PublishedApi internal constructor(
    @PublishedApi
    internal val value: Any?
) {

    /**
     * Returns `true` if this instance represents a successful outcome.
     * In this case [isFailure] returns `false`.
     */
    val isSuccess: Boolean get() = value !is Failure

    /**
     * Returns `true` if this instance represents a failed outcome.
     * In this case [isSuccess] returns `false`.
     */
    val isFailure: Boolean get() = value is Failure

    // value & exception retrieval

    /**
     * Returns the encapsulated value if this instance represents [success][ETResult.isSuccess] or `null`
     * if it is [failure][ETResult.isFailure].
     *
     * This function is a shorthand for `getOrElse { null }` (see [getOrElse]) or
     * `fold(onSuccess = { it }, onFailure = { null })` (see [fold]).
     */
    fun getOrNull(): T? =
        when {
            isFailure -> null
            else -> value as T
        }

    /**
     * Returns the encapsulated [Throwable] exception if this instance represents [failure][isFailure] or `null`
     * if it is [success][isSuccess].
     *
     * This function is a shorthand for `fold(onSuccess = { null }, onFailure = { it })` (see [fold]).
     */
    fun exceptionOrNull(): Throwable? =
        when (value) {
            is Failure -> value.exception
            else -> null
        }

    /**
     * Returns the encapsulated result of the given [transform] function applied to the encapsulated value
     * if this instance represents [success][ETResult.isSuccess] or the
     * original encapsulated [Throwable] exception if it is [failure][ETResult.isFailure].
     *
     * Note, that this function rethrows any [Throwable] exception thrown by [transform] function.
     * See [mapCatching] for an alternative that encapsulates exceptions.
     */
    inline fun <R> map(transform: (value: T) -> R): ETResult<R> {
        return when {
            isSuccess -> success(transform(value as T))
            else -> ETResult(value)
        }
    }

    inline fun <R> mapCatching(transform: (value: T) -> R): ETResult<R> {
        return when {
            isSuccess -> runCatchingET { transform(value as T) }
            else -> ETResult(value)
        }
    }

    /**
     * Returns the result of [onSuccess] for the encapsulated value if this instance represents [success][ETResult.isSuccess]
     * or the result of [onFailure] function for the encapsulated [Throwable] exception if it is [failure][ETResult.isFailure].
     *
     * Note, that this function rethrows any [Throwable] exception thrown by [onSuccess] or by [onFailure] function.
     */
    inline fun <R> fold(
        onSuccess: (value: T) -> R,
        onFailure: (exception: Throwable) -> R
    ): R {
        return when (val exception = exceptionOrNull()) {
            null -> onSuccess(value as T)
            else -> onFailure(exception)
        }
    }

    /**
     * Returns the encapsulated value if this instance represents [success][ETResult.isSuccess] or throws the encapsulated [Throwable] exception
     * if it is [failure][ETResult.isFailure].
     *
     * This function is a shorthand for `getOrElse { throw it }` (see [getOrElse]).
     */
    fun getOrThrow(): T {
        throwOnFailure()
        return value as T
    }

    /**
     * Returns a string `Success(v)` if this instance represents [success][ETResult.isSuccess]
     * where `v` is a string representation of the value or a string `Failure(x)` if
     * it is [failure][isFailure] where `x` is a string representation of the exception.
     */
    override fun toString(): String =
        when (value) {
            is Failure -> value.toString() // "Failure($exception)"
            else -> "Success($value)"
        }

    // companion with constructors

    /**
     * Companion object for [ETResult] class that contains its constructor functions
     * [success] and [failure].
     */
    companion object {
        /**
         * Returns an instance that encapsulates the given [value] as successful value.
         */
        @Suppress("INAPPLICABLE_JVM_NAME")
        @JvmName("success")
        fun <T> success(value: T): ETResult<T> =
            ETResult(value)

        /**
         * Returns an instance that encapsulates the given [Throwable] [exception] as failure.
         */
        @Suppress("INAPPLICABLE_JVM_NAME")
        @JvmName("failure")
        fun <T> failure(exception: Throwable): ETResult<T> =
            ETResult(createFailure(exception))
    }

    internal class Failure(
        @JvmField
        val exception: Throwable
    ) {
        override fun equals(other: Any?): Boolean = other is Failure && exception == other.exception
        override fun hashCode(): Int = exception.hashCode()
        override fun toString(): String = "Failure($exception)"
    }
}

/**
 * Creates an instance of internal marker [ETResult.Failure] class to
 * make sure that this class is not exposed in ABI.
 */
internal fun createFailure(exception: Throwable): Any =
    ETResult.Failure(exception)

/**
 * Throws exception if the result is failure. This internal function minimizes
 * inlined bytecode for [getOrThrow] and makes sure that in the future we can
 * add some exception-augmenting logic here (if needed).
 */
internal fun ETResult<*>.throwOnFailure() {
    if (value is ETResult.Failure) throw value.exception
}

/**
 * Calls the specified function [block] and returns its encapsulated result if invocation was successful,
 * catching any [Throwable] exception that was thrown from the [block] function execution and encapsulating it as a failure.
 */
inline fun <R> runCatchingET(block: () -> R): ETResult<R> {
    return try {
        ETResult.success(block())
    } catch (e: Throwable) {
        ETResult.failure(e)
    }
}

/**
 * Calls the specified function [block] with `this` value as its receiver and returns its encapsulated result if invocation was successful,
 * catching any [Throwable] exception that was thrown from the [block] function execution and encapsulating it as a failure.
 */
inline fun <T, R> T.runCatchingET(block: T.() -> R): ETResult<R> {
    return try {
        ETResult.success(block())
    } catch (e: Throwable) {
        ETResult.failure(e)
    }
}

// -- extensions ---

/**
 * Returns the encapsulated value if this instance represents [success][ETResult.isSuccess] or the
 * result of [onFailure] function for the encapsulated [Throwable] exception if it is [failure][ETResult.isFailure].
 *
 * Note, that this function rethrows any [Throwable] exception thrown by [onFailure] function.
 *
 * This function is a shorthand for `fold(onSuccess = { it }, onFailure = onFailure)` (see [fold]).
 */
inline fun <R, T : R> ETResult<T>.getOrElse(onFailure: (exception: Throwable) -> R): R {
    return when (val exception = exceptionOrNull()) {
        null -> value as T
        else -> onFailure(exception)
    }
}

/**
 * Returns the encapsulated value if this instance represents [success][ETResult.isSuccess] or the
 * [defaultValue] if it is [failure][ETResult.isFailure].
 *
 * This function is a shorthand for `getOrElse { defaultValue }` (see [getOrElse]).
 */
fun <R, T : R> ETResult<T>.getOrDefault(defaultValue: R): R {
    if (isFailure) return defaultValue
    return value as T
}

// transformation

/**
 * Returns the encapsulated result of the given [transform] function applied to the encapsulated [Throwable] exception
 * if this instance represents [failure][ETResult.isFailure] or the
 * original encapsulated value if it is [success][ETResult.isSuccess].
 *
 * Note, that this function rethrows any [Throwable] exception thrown by [transform] function.
 * See [recoverCatching] for an alternative that encapsulates exceptions.
 */
inline fun <R, T : R> ETResult<T>.recover(transform: (exception: Throwable) -> R): ETResult<R> {
    return when (val exception = exceptionOrNull()) {
        null -> this
        else -> ETResult.success(transform(exception))
    }
}

/**
 * Returns the encapsulated result of the given [transform] function applied to the encapsulated [Throwable] exception
 * if this instance represents [failure][ETResult.isFailure] or the
 * original encapsulated value if it is [success][ETResult.isSuccess].
 *
 * This function catches any [Throwable] exception thrown by [transform] function and encapsulates it as a failure.
 * See [recover] for an alternative that rethrows exceptions.
 */
inline fun <R, T : R> ETResult<T>.recoverCatching(transform: (exception: Throwable) -> R): ETResult<R> {
    return when (val exception = exceptionOrNull()) {
        null -> this
        else -> runCatchingET { transform(exception) }
    }
}

// "peek" onto value/exception and pipe

/**
 * Performs the given [action] on the encapsulated [Throwable] exception if this instance represents [failure][ETResult.isFailure].
 * Returns the original `ETResult` unchanged.
 */
inline fun <T> ETResult<T>.onFailure(action: (exception: Throwable) -> Unit): ETResult<T> {
    exceptionOrNull()?.let { action(it) }
    return this
}

/**
 * Performs the given [action] on the encapsulated value if this instance represents [success][ETResult.isSuccess].
 * Returns the original `ETResult` unchanged.
 */
inline fun <T> ETResult<T>.onSuccess(action: (value: T) -> Unit): ETResult<T> {
    if (isSuccess) action(value as T)
    return this
}
