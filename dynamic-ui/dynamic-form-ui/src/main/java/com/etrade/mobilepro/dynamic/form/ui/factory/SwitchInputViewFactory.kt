package com.etrade.mobilepro.dynamic.form.ui.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.api.SwitchInput
import com.etrade.mobilepro.dynamic.form.ui.SwitchInputView

internal object SwitchInputViewFactory : ComponentViewFactory<SwitchInput, SwitchInputView> {
    override fun createView(context: Context): SwitchInputView = SwitchInputView(context)
}
