package com.etrade.mobilepro.trade.form.api.value

import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.util.MutualConverter

interface PriceTypeValueId {
    val even: String
    val limit: String
    val market: String
    val netCredit: String
    val netDebit: String
    val stop: String
    val stopLimit: String
    val trailingStopDollar: String
    val trailingStopPercent: String
    val converter: MutualConverter<String, PriceType>
}
