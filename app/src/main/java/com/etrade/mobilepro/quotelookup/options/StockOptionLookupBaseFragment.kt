package com.etrade.mobilepro.quotelookup.options

import android.os.Bundle
import android.view.View
import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.quotelookup.StockLookupBaseFragment
import com.etrade.mobilepro.quotelookup.adapter.OptionsSearchResultsAdapter
import com.etrade.mobilepro.searchingapi.items.OptionDate
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider

private const val FILTER_DATE_TAG = "symbol_lookup_filter_tag"

abstract class StockOptionLookupBaseFragment : StockLookupBaseFragment() {

    private lateinit var optionSearchViewModel: StockOptionsViewModel
    abstract val optionSearchType: SearchType
    private var searchQuery: SearchQuery? = null
    private var dropDownManager: BottomSheetSelector<OptionDate>? = null
    private val optionAdapter = OptionsSearchResultsAdapter(symbolSelectListener = genericListener)

    override val resultsDataSource: LiveData<List<SearchResultItem>?>
        get() = viewModel.optionResults

    private val searchView: View?
        get() = activity?.findViewById(R.id.searchView)

    abstract fun observeCurrentSymbol()

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupOptionsRecyclerView()
        bindOptionsViewModel()
    }

    override fun navigateToNextPage(symbol: SearchResultItem.Symbol) {
        if (symbol.type.isOption) {
            optionSearchViewModel.addItemSelected(symbol)
            dismissOrGoToQuote(symbol)
        } else {
            searchView?.clearFocus()
            optionSearchViewModel.itemSelected(optionSearchType, symbol)
        }
    }

    override fun getDisclosureResId() = viewModel.searchOptionsDisclosureResId

    private fun setupOptionsRecyclerView() {
        with(binding.optionsList) {
            layoutManager = LinearLayoutManager(activity)
            setupRecyclerViewDivider(R.drawable.thin_divider)
            adapter = optionAdapter
        }
    }

    private fun bindOptionsViewModel() {
        optionSearchViewModel =
            ViewModelProvider(this, viewModelFactory).get(StockOptionsViewModel::class.java)

        optionSearchViewModel.results.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is StockOptionsViewModel.ViewState.Success -> {
                        binding.searchLoadingProgressBar.hide()
                        binding.searchResultContainer.root.visibility = View.GONE
                        binding.optionsContainer.visibility = View.VISIBLE
                        errorSnackbar?.dismiss()

                        val result = it.results
                        if (result.searchResultList.isEmpty()) {
                            toggleOptionEmptyState(true)
                        } else {
                            optionAdapter.updateData(result.searchResultList)

                            if (dropDownManager == null && result.optionMonths.isNotEmpty()) {
                                initDateFilterDropDown(result.optionMonths)
                                binding.filterDate.text =
                                    result.optionMonths[optionSearchViewModel.selectedPosition]
                                        .toString()
                                        .also(::updateDateFilterContentDescription)
                            }
                            toggleOptionEmptyState(false)
                        }
                    }
                    is StockOptionsViewModel.ViewState.Loading -> {
                        binding.searchLoadingProgressBar.show()
                        errorSnackbar?.dismiss()
                    }
                    is StockOptionsViewModel.ViewState.Error -> {
                        binding.searchLoadingProgressBar.hide()
                        errorSnackbar =
                            snackbarUtil.snackbar(getString(R.string.error_message_general))
                                ?.apply { show() }
                    }
                }
            }
        )

        observeCurrentSymbol()
    }

    private fun updateDateFilterContentDescription(date: String) {
        binding.filterDateContainer.contentDescription =
            "$date, ${getString(R.string.label_dropdown_description, getString(R.string.date))}"
    }

    @Suppress("LongMethod")
    private fun initDateFilterDropDown(it: List<OptionDate>) {
        parentFragmentManager.let { fragmentManager ->
            dropDownManager = BottomSheetSelector(fragmentManager, resources)

            dropDownManager?.init(
                tag = FILTER_DATE_TAG,
                title = getString(R.string.date),
                items = it,
                initialSelectedPosition = optionSearchViewModel.selectedPosition
            )

            dropDownManager?.setListener { position, selected ->
                optionSearchViewModel.searchQuery?.let {
                    optionSearchViewModel.search(
                        SearchQuery(
                            it.type, it.value, it.description,
                            selected.originSelectedDate.year.toString(),
                            selected.originSelectedDate.month.toString(),
                            selected.originSelectedDate.day.toString()
                        )
                    )
                }
                binding.filterDate.text =
                    selected.toString().also(::updateDateFilterContentDescription)
                optionSearchViewModel.selectedPosition = position
            }

            binding.filterDateContainer.setOnClickListener {
                searchView?.clearFocus()
                dropDownManager?.openDropDown()
            }
        }
    }

    protected fun searchByQuery(it: SearchResultItem.Symbol) {
        searchQuery = SearchQuery(
            type = optionSearchType,
            value = it.title,
            description = it.description,
            expiryDay = "0",
            expiryMonth = "0",
            expiryYear = "0"
        )

        searchQuery?.let {
            optionSearchViewModel.search(it)
        }
        dropDownManager = null
        optionSearchViewModel.selectedPosition = 0
    }

    private fun toggleOptionEmptyState(empty: Boolean) = if (empty) {
        binding.noResults.visibility = View.VISIBLE
        binding.noResults.text =
            getString(R.string.no_search_results, optionSearchViewModel.searchQuery?.value)
        binding.optionsContainer.visibility = View.GONE
    } else {
        binding.noResults.visibility = View.GONE
        binding.optionsContainer.visibility = View.VISIBLE
    }
}
