package com.etrade.mobilepro.appwidget.indices

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.appwidget.common.copy
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.marketsapi.Index
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import io.reactivex.Observable

private val SYMBOL_SUBSCRIPTION_FIELDS: Set<Level1Field> = StockData.fields()

@JvmName("indexToStream")
internal fun List<Index>.toQuoteStreams(
    quoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    recentQuoteMap: MutableMap<String, Level1Data>
): List<Observable<TickerData>> {
    return map { index ->
        val mapTickerId = index.getQuoteMapTickerId(index.ticker)
        quoteProvider.getQuote(
            symbol = index.ticker,
            instrumentType = InstrumentType.INDX,
            fields = SYMBOL_SUBSCRIPTION_FIELDS
        ).filter {
            recentQuoteMap[mapTickerId]?.lastPrice != it.lastPrice
        }.map { data ->
            recentQuoteMap[mapTickerId] = data
            TickerData(index.ticker, data, TickerOrigin.Index)
        }
    }
}

@JvmName("entryToStream")
internal fun List<WatchlistEntry>.toQuoteStreams(
    quoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    recentQuoteMap: MutableMap<String, Level1Data>
): List<Observable<TickerData>> {
    return map { index ->
        val mapTickerId = index.getQuoteMapTickerId(index.ticker)
        quoteProvider.getQuote(
            symbol = index.ticker,
            instrumentType = InstrumentType.INDX,
            fields = SYMBOL_SUBSCRIPTION_FIELDS
        ).filter {
            recentQuoteMap[mapTickerId]?.lastPrice != it.lastPrice
        }.map { data ->
            val previousData = recentQuoteMap[mapTickerId]
            val updatedData = if (previousData != null && (data.tickIndicator == null || data.tickIndicator == 0.toShort())) {
                data.copy(tickIndicator = previousData.tickIndicator)
            } else {
                data
            }
            recentQuoteMap[mapTickerId] = updatedData
            TickerData(index.ticker, updatedData, TickerOrigin.WatchlistEntry)
        }
    }
}

/**
 * Creates a unique id based on [T] class's simple name and [ticker] parameter to be used as a recent quote map id.
 * It is required as the same map is used for both Indices and Watchlist entries and there is a case
 * when the same ticker is present in both Indices and Watchlist collections (e.g. SPX), so to support
 * steaming of the same ticker in both collections we need to create a unique id.
 */
private fun <T : Any> T.getQuoteMapTickerId(ticker: String) = this::class.java.simpleName + ticker

internal fun Level1Data.getFormattedTimestamp() = timestampSinceEpoch?.let(DateFormattingUtils::formatFullDateTime)

internal enum class TickerOrigin { Index, WatchlistEntry }
internal data class TickerData(val ticker: String, val data: Level1Data, val origin: TickerOrigin)
internal object QuoteTimeOut
