package com.etrade.mobilepro.trade.calculator.view

import android.content.Context
import android.content.res.ColorStateList
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.trade.calculator.CALCULATOR_ZERO
import com.etrade.mobilepro.trade.calculator.DEFAULT_CALCULATOR_VALUE
import com.etrade.mobilepro.trade.calculator.R
import com.etrade.mobilepro.trade.calculator.removeLastCharacter
import com.etrade.mobilepro.trade.calculator.toValueDigital
import com.etrade.mobilepro.util.android.extension.hideSoftKeyboard
import com.etrade.mobilepro.util.android.extension.setTextSafely
import com.google.android.material.textfield.TextInputLayout

private val ZERO_FIRST_REGEX = "0\\d".toRegex()

abstract class CalculatorInputLayout : TextInputLayout {

    constructor(ctx: Context) : super(ctx)

    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet)

    private val defaultValue get() = wrap(DEFAULT_CALCULATOR_VALUE)

    var onValueChangeListener: ((value: String) -> Unit)? = null

    abstract val formatFieldStringRes: Int
        @StringRes get

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            handleInputChanged(requireNotNull(editText))
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // unused
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            // unused
        }
    }

    private fun wrap(text: String): String = resources.getString(formatFieldStringRes, text)

    fun setEditTextListeners() {
        with(requireNotNull(editText)) {
            addTextChangedListener(textWatcher)
            setOnFocusChangeListener { _, hasFocus -> onFocusChanged(this, hasFocus) }
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    hideSoftKeyboard()
                }
                false
            }
            setEndIconOnClickListener {
                setText(
                    if (hasFocus()) {
                        ""
                    } else {
                        defaultValue
                    }
                )
            }
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        defaultHintTextColor = ColorStateList.valueOf(ContextCompat.getColor(context, if (enabled) R.color.hint_color else R.color.hint_color_disabled))
        editText?.isFocusable = enabled
        editText?.isFocusableInTouchMode = enabled
    }

    private fun formatFocusedText(text: String?) = when {
        text.isNullOrEmpty() -> ""
        text.matches(ZERO_FIRST_REGEX) -> text.removeLastCharacter()
        text == "." -> "0."
        else -> text
    }

    private fun onFocusChanged(editText: EditText, hasFocus: Boolean) {
        if (hasFocus) {
            val amount = editText.text?.toString()?.toValueDigital()?.removeIfZero()
            editText.setTextSafely(amount, textWatcher)
            isEndIconVisible = editText.text.isNotEmpty()
        } else {
            editText.text = editText.text
            isEndIconVisible = !isTextDefault(editText.text?.toString())
        }
    }

    private fun handleInputChanged(editText: EditText) {
        val text = editText.text?.toString()
        val resultText = if (editText.isFocused) {
            formatFocusedText(text)
        } else {
            formatNotFocusedText(text)
        }
        if (resultText != text) {
            editText.setTextSafely(resultText, textWatcher)
        }
        isEndIconVisible = isEnabled && !isTextDefault(resultText)
        onValueChangeListener?.invoke(resultText)
    }

    private fun formatNotFocusedText(text: String?): String = when {
        text.isNullOrEmpty() -> wrap(DEFAULT_CALCULATOR_VALUE)
        text.startsWith("$") -> text
        else -> wrap(text.toValueDigital())
    }

    private fun isTextDefault(text: String?) = text == defaultValue

    interface OnValueChangedListener {
        fun onValueChanged(value: String)
    }
}

private fun String.removeIfZero() = if (toDouble() == 0.0) {
    ""
} else this

@BindingAdapter("setText")
fun setText(valueInput: CalculatorInputLayout, value: String?) {
    val text = if (value == CALCULATOR_ZERO) {
        ""
    } else {
        value
    }
    valueInput.editText?.setText(text)
}

@BindingAdapter("onValueChanged")
fun onValueChanged(valueInput: CalculatorInputLayout, listener: CalculatorInputLayout.OnValueChangedListener) {
    valueInput.onValueChangeListener = { listener.onValueChanged(it) }
}
