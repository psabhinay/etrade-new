package com.etrade.mobilepro.streaming.api

interface Level2DataEvent {
    val key: String
    val side: String
    val price: Double
    val size: Long
    val consolidatedSize: Long
    val marketMakerId: String?
    val time: Long
    val consolidateCount: Long

    fun copy(): Level2DataEvent
}
