package com.etrade.mobilepro.user.session.manager

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.SystemClock
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.session.api.SessionEvents
import com.etrade.mobilepro.session.api.User
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicReference
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(UserSessionTimeoutManager::class.java)
private const val TIMEOUT_ALARM_REQUEST_CODE = 1000

class UserSessionTimeoutManager @Inject constructor(
    private val context: Context,
    private val user: User,
    private val alarmManager: AlarmManager
) : SessionTimeoutManager {
    private val checkSessionTimeoutIntent = Intent(ACTION_CHECK_TIMEOUT)
    private val checkSessionTimeoutPendingIntent = AtomicReference<PendingIntent>()
    private var userMaxIdleTimeMillis: Long = 0

    init {
        context.registerReceiver(
            checkTimeoutAlarmReceiver(),
            IntentFilter(ACTION_CHECK_TIMEOUT)
        )
    }

    private fun checkTimeoutAlarmReceiver(): BroadcastReceiver {
        return object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                logger.debug("Received session timeout check alarm")
                checkAndTerminateSession()
            }
        }
    }

    override fun checkAndTerminateSession(): Boolean {
        cancelCheckTimeoutAlarm()
        return if (shouldTimeoutUser()) {
            handleTimeoutAction()
            true
        } else {
            if (checkSessionTimeoutPendingIntent.get() == null) {
                createCheckTimeoutAlarm()
            }
            false
        }
    }

    private val _sessionEvent: MutableLiveData<SessionEvents> = MutableLiveData()

    override val sessionEvent: LiveData<SessionEvents>
        get() = _sessionEvent

    private fun handleTimeoutAction() {
        if (isLoggedIn()) {
            logger.debug("handle session timeout")
            cancelCheckTimeoutAlarm()
            sendMessage(SessionEvents.MSG_SESSION_TERMINATED)
        }
    }

    override fun touchSession() {
        if (isLoggedIn()) {
            user.session?.touchSession(SystemClock.elapsedRealtime())
            logger.debug("Session timeout touched: ${user.session?.realTimeOfLastTouchMillis}")
        }
    }

    override fun shouldTimeoutUser(): Boolean = SystemClock.elapsedRealtime() >= timeToTimeout

    private val timeToTimeout: Long
        get() = user.session?.realTimeOfLastTouchMillis?.plus(userMaxIdleTimeMillis) ?: SystemClock.elapsedRealtime()

    private fun cancelCheckTimeoutAlarm() {
        logger.debug("timeout alarm cancelled")
        checkSessionTimeoutPendingIntent.getAndSet(null)?.also {
            alarmManager.cancel(it)
            sendMessage(SessionEvents.MSG_SESSION_TIMEOUT_ALARM_CANCELLED)
        }
    }

    private fun setMaxIdleTimeMillis(millis: Long) {
        require(millis >= 0) { "Timeout millis cannot be negative: $millis" }
        userMaxIdleTimeMillis = millis
    }

    private fun sendMessage(message: SessionEvents) {
        logger.debug("timeout sendMessage ${message.code}")
        _sessionEvent.postValue(message)
    }

    private fun isLoggedIn(): Boolean = user.isLoggedIn()

    private fun createCheckTimeoutAlarm() {
        val pendingIntent = PendingIntent.getBroadcast(
            context,
            TIMEOUT_ALARM_REQUEST_CODE,
            checkSessionTimeoutIntent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
        logger.debug("start check alarm timeout :$timeToTimeout")
        alarmManager.set(AlarmManager.ELAPSED_REALTIME, timeToTimeout, pendingIntent)
        checkSessionTimeoutPendingIntent.set(pendingIntent)
    }

    override fun initSessionTimeout(timeOutDuration: Long) {
        logger.debug("init session timeout :$timeOutDuration")
        touchSession()
        setMaxIdleTimeMillis(timeOutDuration)
        cancelCheckTimeoutAlarm()
        createCheckTimeoutAlarm()
    }

    override fun cancelSessionTimeout() {
        cancelCheckTimeoutAlarm()
    }

    companion object {
        private val ACTION_CHECK_TIMEOUT = "${UserSessionTimeoutManager::class.java.name}.alarm.CHECK_TIMEOUT"
    }
}
