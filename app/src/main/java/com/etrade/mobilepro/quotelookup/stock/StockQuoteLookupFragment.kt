package com.etrade.mobilepro.quotelookup.stock

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.quotelookup.StockLookupBaseFragment
import com.etrade.mobilepro.searchingapi.items.SearchResultItem

class StockQuoteLookupFragment : StockLookupBaseFragment() {

    override val resultsDataSource: LiveData<List<SearchResultItem>?>
        get() = viewModel.stockFundResults

    override fun navigateToNextPage(symbol: SearchResultItem.Symbol) {
        viewModel.addItemSelected(symbol)
        dismissOrGoToQuote(symbol)
    }

    override fun getDisclosureResId() = viewModel.searchStockFundDisclosureResId
}
