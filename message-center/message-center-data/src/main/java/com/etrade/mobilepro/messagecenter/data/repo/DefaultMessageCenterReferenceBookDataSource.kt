package com.etrade.mobilepro.messagecenter.data.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.messagecenter.api.model.ReferenceBook
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterAccountsDataSource
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterReferenceBookDataSource
import com.etrade.mobilepro.messagecenter.data.mapper.toReferenceBook
import com.etrade.mobilepro.messagecenter.data.rest.MessageCenterApiClient
import org.slf4j.LoggerFactory
import java.util.concurrent.atomic.AtomicReference
import javax.inject.Inject

class DefaultMessageCenterReferenceBookDataSource @Inject constructor(
    private val apiClient: MessageCenterApiClient,
    private val accountsDataSource: MessageCenterAccountsDataSource
) : MessageCenterReferenceBookDataSource {

    private val cached = AtomicReference<ReferenceBook>()
    private val logger = LoggerFactory.getLogger(DefaultMessageCenterReferenceBookDataSource::class.java)

    override suspend fun fetchReferenceBook(): ETResult<ReferenceBook> =
        runCatchingET {
            val accountIdDescMap = accountsDataSource.getAccountIdToShortDescriptionMap().fold(
                onSuccess = {
                    it
                },
                onFailure = {
                    logger.error("Get account id to short description failed", it)
                    emptyMap()
                }
            )
            apiClient
                .getReferenceBook()
                .toReferenceBook(accountIdDescMap)
                .also {
                    cached.set(it)
                }
        }

    override fun getCachedReferenceBook(): ReferenceBook? = cached.get()

    override fun clearCache() {
        cached.set(null)
    }
}
