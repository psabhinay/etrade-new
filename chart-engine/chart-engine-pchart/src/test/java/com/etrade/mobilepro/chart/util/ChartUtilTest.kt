package com.etrade.mobilepro.chart.util

import android.app.Application
import android.content.Context
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.chart.formatters.YAxisValuePercentageFormatter
import com.etrade.mobilepro.chart.model.ChartConfiguration
import com.etrade.mobilepro.chart.model.ChartTimeFrame
import com.etrade.mobilepro.chart.model.DisplayLabel
import com.etrade.mobilepro.util.market.time.NEW_YORK_ZONE_ID
import com.github.mikephil.charting.charts.LineChart
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import org.threeten.bp.temporal.ChronoUnit

private const val epochMillis = 1550699818414

@RunWith(AndroidJUnit4::class)
class ChartUtilTest {
    private lateinit var application: Application
    private lateinit var mockContext: Context

    @Before
    fun setUp() {
        application = getApplicationContext()
        mockContext = application.baseContext
    }

    @After
    fun tearDown() {
        application.onTerminate()
    }

    @Test
    fun `test set configurations for day chart`() {
        val lineChart = Mockito.spy(LineChart(mockContext))
        configureChart(lineChart, getChartConfiguration(ChartTimeFrame.Day))
        assertEquals(10.0f, lineChart.axisRight.mAxisMaximum)
        assertEquals(0.0f, lineChart.axisRight.mAxisMinimum)
        assertEquals(10.0f, lineChart.axisRight.mAxisRange)
        assertTrue("line chart y axis formatter is percentage formatter", lineChart.axisRight.valueFormatter is YAxisValuePercentageFormatter)

        val maxZonedDateTimeFromData = Instant.ofEpochMilli(epochMillis).atZone(NEW_YORK_ZONE_ID)
        val maxRangeZonedDateTime = maxZonedDateTimeFromData.withHour(16).withMinute(5)
        val minRangeZonedDateTime = maxZonedDateTimeFromData.withHour(9).withMinute(30)

        assertEquals(Duration.of(minRangeZonedDateTime.toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat(), lineChart.xAxis.mAxisMinimum)
        assertEquals(Duration.of(maxRangeZonedDateTime.toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat(), lineChart.xAxis.mAxisMaximum)

        val specificLabels = setOf(
            Duration.of(maxZonedDateTimeFromData.withHour(10).withMinute(0).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat(),
            Duration.of(maxZonedDateTimeFromData.withHour(13).withMinute(0).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat(),
            Duration.of(maxZonedDateTimeFromData.withHour(16).withMinute(0).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat()
        )

        assertTrue("line chart has specificLabelRenderer", lineChart.rendererXAxis is SpecificPositionLabelsXAxisRenderer)
        val labels = (lineChart.rendererXAxis as SpecificPositionLabelsXAxisRenderer).specificLabelPositions
        assertEquals(specificLabels.size, labels.size)
        for (index in specificLabels.indices) {
            assertEquals(specificLabels.elementAt(index), labels[index])
        }
    }

    @Test
    fun `test set configurations for week chart`() {
        val lineChart = Mockito.spy(LineChart(mockContext))
        val configuration = getChartConfiguration(ChartTimeFrame.OneWeek)
        configureChart(lineChart, configuration)
        assertEquals(10.0f, lineChart.axisRight.mAxisMaximum)
        assertEquals(0.0f, lineChart.axisRight.mAxisMinimum)
        assertEquals(10.0f, lineChart.axisRight.mAxisRange)
        assertTrue("line chart y axis formatter is percentage formatter", lineChart.axisRight.valueFormatter is YAxisValuePercentageFormatter)

        assertEquals(0.0f, lineChart.xAxis.mAxisMinimum)
        assertEquals(6.0f, lineChart.xAxis.mAxisMaximum)
        assertEquals(6.0f, lineChart.xAxis.mAxisRange)

        assertTrue("line chart has specificLabelRenderer", lineChart.rendererXAxis is SpecificPositionLabelsXAxisRenderer)
        val labels = (lineChart.rendererXAxis as SpecificPositionLabelsXAxisRenderer).specificLabelPositions
        assertEquals(configuration.listOfLabels?.size, labels.size)
        for (index in labels.indices) {
            assertEquals(configuration.listOfLabels?.elementAt(index)?.index?.toFloat(), labels[index])
        }
    }

    @Test
    fun `test set configurations for month chart`() {
        val lineChart = Mockito.spy(LineChart(mockContext))
        val configuration = getChartConfiguration(ChartTimeFrame.OneMonth)
        configureChart(lineChart, configuration)
        assertEquals(10.0f, lineChart.axisRight.mAxisMaximum)
        assertEquals(0.0f, lineChart.axisRight.mAxisMinimum)
        assertEquals(10.0f, lineChart.axisRight.mAxisRange)
        assertTrue("line chart y axis formatter is percentage formatter", lineChart.axisRight.valueFormatter is YAxisValuePercentageFormatter)

        assertEquals(0.0f, lineChart.xAxis.mAxisMinimum)
        assertEquals(6.0f, lineChart.xAxis.mAxisMaximum)
        assertEquals(6.0f, lineChart.xAxis.mAxisRange)

        assertTrue("line chart has specificLabelRenderer", lineChart.rendererXAxis is SpecificPositionLabelsXAxisRenderer)
        val labels = (lineChart.rendererXAxis as SpecificPositionLabelsXAxisRenderer).specificLabelPositions
        assertEquals(configuration.listOfLabels?.size, labels.size)
        for (index in labels.indices) {
            assertEquals(configuration.listOfLabels?.elementAt(index)?.index?.toFloat(), labels[index])
        }
    }

    @Test
    fun `test set configurations for six months chart`() {
        val lineChart = Mockito.spy(LineChart(mockContext))
        val configuration = getChartConfiguration(ChartTimeFrame.SixMonths)
        configureChart(lineChart, configuration)
        assertEquals(10.0f, lineChart.axisRight.mAxisMaximum)
        assertEquals(0.0f, lineChart.axisRight.mAxisMinimum)
        assertTrue("line chart y axis formatter is percentage formatter", lineChart.axisRight.valueFormatter is YAxisValuePercentageFormatter)

        val maxZonedDateTimeFromData = Instant.ofEpochMilli(epochMillis).atZone(NEW_YORK_ZONE_ID)
        val maxRangeZonedDateTime = maxZonedDateTimeFromData.withHour(16).withMinute(5)
        val minRangeZonedDateTime = maxZonedDateTimeFromData.withHour(0).withMinute(0).minusMonths(6)

        assertEquals(Duration.of(minRangeZonedDateTime.toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat(), lineChart.xAxis.mAxisMinimum)
        assertEquals(Duration.of(maxRangeZonedDateTime.toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat(), lineChart.xAxis.mAxisMaximum)

        val specificLabels = mutableSetOf<Float>()
        val firstLabelZonedDateTime = maxZonedDateTimeFromData.withDayOfMonth(1)
        specificLabels.add(Duration.of(firstLabelZonedDateTime.toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat())
        specificLabels.add(Duration.of(firstLabelZonedDateTime.minusMonths(1).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat())
        specificLabels.add(Duration.of(firstLabelZonedDateTime.minusMonths(2).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat())
        specificLabels.add(Duration.of(firstLabelZonedDateTime.minusMonths(3).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat())
        specificLabels.add(Duration.of(firstLabelZonedDateTime.minusMonths(4).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat())
        specificLabels.add(Duration.of(firstLabelZonedDateTime.minusMonths(5).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat())

        assertTrue("line chart has specificLabelRenderer", lineChart.rendererXAxis is SpecificPositionLabelsXAxisRenderer)
        val labels = (lineChart.rendererXAxis as SpecificPositionLabelsXAxisRenderer).specificLabelPositions
        assertEquals(specificLabels.size, labels.size)
        for (index in 0 until specificLabels.size) {
            assertEquals(specificLabels.elementAt(index), labels[index])
        }
    }

    @Test
    fun `test set configurations for three years chart`() {
        val lineChart = Mockito.spy(LineChart(mockContext))
        val configuration = getChartConfiguration(ChartTimeFrame.ThreeYears)
        configureChart(lineChart, configuration)
        assertEquals(10.0f, lineChart.axisRight.mAxisMaximum)
        assertEquals(0.0f, lineChart.axisRight.mAxisMinimum)
        assertTrue("line chart y axis formatter is percentage formatter", lineChart.axisRight.valueFormatter is YAxisValuePercentageFormatter)

        val maxZonedDateTimeFromData = Instant.ofEpochMilli(epochMillis).atZone(NEW_YORK_ZONE_ID)
        val maxRangeZonedDateTime = maxZonedDateTimeFromData.withHour(16).withMinute(5)
        val minRangeZonedDateTime = maxZonedDateTimeFromData.withHour(0).withMinute(0).minusYears(3)

        assertEquals(Duration.of(minRangeZonedDateTime.toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat(), lineChart.xAxis.mAxisMinimum)
        assertEquals(Duration.of(maxRangeZonedDateTime.toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat(), lineChart.xAxis.mAxisMaximum)

        val specificLabels = mutableSetOf<Float>()
        val firstLabelZonedDateTime = maxZonedDateTimeFromData.withDayOfYear(1)
        specificLabels.add(Duration.of(firstLabelZonedDateTime.toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat())
        specificLabels.add(Duration.of(firstLabelZonedDateTime.minusYears(1).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat())
        specificLabels.add(Duration.of(firstLabelZonedDateTime.minusYears(2).toInstant().toEpochMilli(), ChronoUnit.MILLIS).toMinutes().toFloat())

        assertTrue("line chart has specificLabelRenderer", lineChart.rendererXAxis is SpecificPositionLabelsXAxisRenderer)
        val labels = (lineChart.rendererXAxis as SpecificPositionLabelsXAxisRenderer).specificLabelPositions
        assertEquals(specificLabels.size, labels.size)
        for (index in 0 until specificLabels.size) {
            assertEquals(specificLabels.elementAt(index), labels[index])
        }
    }

    private fun getChartConfiguration(chartTimeFrame: ChartTimeFrame): ChartConfiguration {
        return when (chartTimeFrame) {
            ChartTimeFrame.OneWeek -> {
                val listOfLabels = mutableListOf<DisplayLabel>()
                listOfLabels.add(DisplayLabel(0, epochMillis))
                listOfLabels.add(DisplayLabel(2, epochMillis))
                listOfLabels.add(DisplayLabel(4, epochMillis))
                ChartConfiguration(chartTimeFrame, 0.0, 10.0, 6, true, listOfLabels)
            }
            ChartTimeFrame.OneMonth -> {
                val listOfLabels = mutableListOf<DisplayLabel>()
                listOfLabels.add(DisplayLabel(4, epochMillis))
                listOfLabels.add(DisplayLabel(6, epochMillis))
                listOfLabels.add(DisplayLabel(8, epochMillis))
                ChartConfiguration(chartTimeFrame, 0.0, 10.0, 6, true, listOfLabels)
            }
            else -> ChartConfiguration(chartTimeFrame, 0.0, 10.0, epochMillis, true)
        }
    }
}
