package com.etrade.mobilepro.editlist

/**
 * Defines an item that is used within [EditListActivity].
 */
interface EditListItem {

    /**
     * Human readable text to show.
     */
    val displayText: CharSequence

    val isChecked: Boolean

    val contentDescription: String
        get() = displayText.toString()

    val dragHandleContentDescription: String
        get() = "$contentDescription. Double Tap and hold to change order"
}
