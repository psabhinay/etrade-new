package com.etrade.mobilepro.marketscalendar

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.marketscalendar.viewmodel.SharedCalendarState
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.tracking.SCREEN_TITLE_EARNINGS_CALENDAR
import com.etrade.mobilepro.tracking.SCREEN_TITLE_EVENTS_CALENDAR
import com.etrade.mobilepro.tracking.TabsHostScreenViewModel
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelper
import com.etrade.mobilepro.user.session.manager.SessionState
import com.etrade.mobilepro.user.session.manager.SessionStateChangeListener
import com.etrade.mobilepro.util.AuthenticationStatusProvider
import javax.inject.Inject

class MarketsCalendarViewModel @Inject constructor(
    override val viewPagerTabsTrackerHelper: ViewPagerTabsTrackerHelper,
    private val sessionState: SessionState,
    private val sharedCalendarState: SharedCalendarState,
    authenticationStatusProvider: AuthenticationStatusProvider
) : ViewModel(), TabsHostScreenViewModel, SessionStateChangeListener {

    override val tabNames: Array<String> = arrayOf(SCREEN_TITLE_EARNINGS_CALENDAR, SCREEN_TITLE_EVENTS_CALENDAR)

    private val _viewState = MutableLiveData<ViewState>()
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    init {
        _viewState.value = ViewState(isAuthenticated = authenticationStatusProvider.isAuthenticated())
        sessionState.addListener(this)
    }

    override fun onCleared() {
        sessionState.removeListener(this)
        sharedCalendarState.reset()
        super.onCleared()
    }

    override fun onSessionStateChange(state: SessionStateChange) {
        _viewState.value = ViewState(isAuthenticated = state == SessionStateChange.AUTHENTICATED)
    }

    internal data class ViewState(
        val isAuthenticated: Boolean
    )
}
