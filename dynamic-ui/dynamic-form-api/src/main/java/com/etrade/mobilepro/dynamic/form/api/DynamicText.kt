package com.etrade.mobilepro.dynamic.form.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class DynamicText(
    val title: String,
    val displayValue: LiveData<CharSequence?>,
    override val isVisible: LiveData<Boolean> = MutableLiveData(true)
) : Component
