package com.etrade.mobilepro.streaming.base

import com.etrade.eo.streaming.SubscriptionMode
import io.reactivex.Observable

/**
 * Implementations helps a client to work with subscription manager.
 */
interface SubscriptionManagerHelper<T : Any> {

    /**
     * Subscribes to a [streamId] requiring a set of [fields] to be returned in a specific [mode].
     *
     * Contract of this method:
     *
     * * if there's active subscription with the same [streamId] and [fields], then the method must return the same subscription
     * * if there's active subscription with the same [streamId], but [fields] are different, then the method must return the same subscription but update the
     * `fields` in a way that will allow previous observers and the new observer to get required `fields`
     * * in all other cases it must create new subscription
     *
     * @param streamId The identifier of a stream. See StreamIdFactory in streaming-api module.
     * @param fields The set of fields to get.
     * @param mode The subscription mode.
     *
     * @return The observable object that can be used by *multiple* observers to listen for the stream of values.
     */
    fun subscribeTo(streamId: String, fields: Set<String>, mode: SubscriptionMode): Observable<T>
}
