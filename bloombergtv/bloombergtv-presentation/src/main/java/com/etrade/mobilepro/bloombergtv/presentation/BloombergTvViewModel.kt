package com.etrade.mobilepro.bloombergtv.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.bloombergtv.api.BloombergTvRepository
import com.etrade.mobilepro.bloombergtv.common.ViewState
import kotlinx.coroutines.launch
import kotlinx.coroutines.rx2.await
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class BloombergTvViewModel @Inject constructor(
    private val bloombergTvRepository: BloombergTvRepository
) : ViewModel() {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    private val _viewState = MutableLiveData<ViewState>(ViewState.Loading)
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    internal val videoUrl: String?
        get() = (viewState.value as? ViewState.Success)?.url

    internal var shouldBePlaying: Boolean = true

    internal fun onError(internalErrorMessage: String) {
        logger.error(internalErrorMessage)
        _viewState.postValue(ViewState.Failure(""))
    }

    internal fun init() {
        viewModelScope.launch {
            runCatching {
                bloombergTvRepository.getBloombergtvResponse().await()
            }.onSuccess {
                if (it.bloombergLiveData.url.isEmpty()) {
                    _viewState.postValue(ViewState.Failure(it.bloombergLiveData.errorDescription))
                } else {
                    _viewState.postValue(ViewState.Success(it.bloombergLiveData.url))
                }
            }.onFailure {
                _viewState.postValue(ViewState.Failure(it.message ?: ""))
            }
        }
    }
}
