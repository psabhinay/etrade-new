package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.MerchantInfo
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.PayeeAddress
import com.etrade.mobilepro.billpay.api.SavePayeeResult
import com.etrade.mobilepro.billpay.api.SearchPayeeResultItem
import com.etrade.mobilepro.billpay.api.StatesOfUS
import com.etrade.mobilepro.billpay.api.UsState
import com.etrade.mobilepro.billpay.presentation.common.ParcelablePayee
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.address.UsStateDelegate
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.search.SearchPayeeDelegate
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.livedata.postUpdateValue
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.R
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import com.etrade.mobilepro.billpay.presentation.R as BillPayR

private const val DEFAULT_LEAD_DAYS = -1
private const val MIN_SEARCH_LENGTH = 1

@FlowPreview
@ExperimentalCoroutinesApi
private val logger = LoggerFactory.getLogger(PayeeFormViewModel::class.java)

@FlowPreview
@ExperimentalCoroutinesApi
@Suppress("LargeClass")
class PayeeFormViewModel @AssistedInject constructor(
    private val repo: BillPayRepo,
    private val resources: Resources,
    @Assisted private val billPaySessionId: String,
    @Assisted private val payeeItem: ParcelablePayee?,
    @Assisted private val isEditMode: Boolean
) : ViewModel() {

    private val _viewState: MediatorLiveData<ViewState> = MediatorLiveData()
    internal val viewState: LiveData<ViewState> get() = _viewState

    // Live data that would be used by InputValueView
    internal val nameInputState = MutableLiveData<InputFieldManager.Value>()
    internal val nickNameInputState = MutableLiveData<InputFieldManager.Value>()
    internal val accountNumberInputState = MutableLiveData<InputFieldManager.Value>()
    internal val address1InputState = MutableLiveData<InputFieldManager.Value>()
    internal val address2InputState = MutableLiveData<InputFieldManager.Value>()
    internal val cityInputState = MutableLiveData<InputFieldManager.Value>()
    internal val zipCodeInputState = MutableLiveData<InputFieldManager.Value>()
    internal val phoneNumberInputState = MutableLiveData<InputFieldManager.Value>()

    internal val payee: Payee?
        get() = if (_viewState.value?.isPayeeValid(
                nameInputState,
                accountNumberInputState,
                address1InputState,
                cityInputState,
                zipCodeInputState,
                phoneNumberInputState
            ) == true
        ) {
            collectData()
        } else {
            null
        }

    private val requireFields by lazy {
        listOf(
            nameInputState,
            nickNameInputState,
            accountNumberInputState,
            address1InputState,
            address2InputState,
            cityInputState,
            zipCodeInputState,
            phoneNumberInputState
        )
    }

    internal val rawName = MutableLiveData<String>()
    internal val rawNickname = MutableLiveData<String>()
    internal val rawAccountNumber = MutableLiveData<String>()
    internal val rawAddress1 = MutableLiveData<String>()
    internal val rawAddress2 = MutableLiveData<String>()
    internal val rawCity = MutableLiveData<String>()
    internal val rawState = MutableLiveData<String>()
    internal val rawZipCode = MutableLiveData<String>()
    internal val rawPhoneNumber = MutableLiveData<String>()
    private val rawPayeeId = MutableLiveData<String?>()

    private val searchDelegate = SearchPayeeDelegate(repo, billPaySessionId)
    private val usStateDelegate = UsStateDelegate(repo)

    @FlowPreview
    @ExperimentalCoroutinesApi
    @AssistedFactory
    interface Factory {
        fun create(billPaySessionId: String, payeeItem: ParcelablePayee?, isEditMode: Boolean): PayeeFormViewModel
    }

    init {
        initViewState()
        loadStateList()
        setupValuesFromArgs()
        updatePayeeValidStateState()
        setUpSearch()
    }

    @FlowPreview
    @ExperimentalCoroutinesApi
    private fun initViewState() {
        _viewState.value = ViewState()
        requireFields.forEach { field ->
            _viewState.addSource(field) { updatePayeeValidStateState() }
        }
        _viewState.addSource(searchDelegate.searchActionState) { searchActionState ->
            _viewState.updateValue { it.copy(search = searchActionState) }
        }
        _viewState.addSource(usStateDelegate.loadStatesActionState) { loadUsStateActionState ->
            if (loadUsStateActionState.result != null) {
                if (loadUsStateActionState.result.isFailure) {
                    _viewState.updateValue { it.copy(loadUsStates = loadUsStateActionState.copy { loadStateList() }) }
                    return@addSource
                }
            }
            _viewState.updateValue { it.copy(loadUsStates = loadUsStateActionState) }
        }
    }

    private fun setupValuesFromArgs() {
        payeeItem?.also {
            val companyData: SearchPayeeResultItem? = it.merchantInfo?.id?.let { merchantId ->
                if (isEditMode && merchantId != 0) {
                    SearchPayeeResultItem(it.payeeId, it.name, it.category, merchantId, it.merchantZipRequired == true, false)
                } else {
                    null
                }
            }
            rawPayeeId.value = it.payeeId
            _viewState.updateValue { prevState ->
                prevState.copy(
                    isEditMode = isEditMode,
                    displaySearchMode = false,
                    selectedCompanyData = companyData,
                    selectedState = UsState(it.address?.state.orEmpty())
                )
            }
        }
    }

    private fun updatePayeeValidStateState() {
        _viewState.updateValue {
            it.copy(
                payeeValid = it.isPayeeValid(
                    nameInputState,
                    accountNumberInputState,
                    address1InputState,
                    cityInputState,
                    zipCodeInputState,
                    phoneNumberInputState
                )
            )
        }
    }

    private fun setUpSearch() {
        viewModelScope.launch {
            searchDelegate.bindSearch()
        }
    }

    private fun loadStateList() {
        viewModelScope.launch {
            usStateDelegate.loadStateList()
        }
    }

    internal fun onStateSelected(addressState: UsState) {
        rawState.value = addressState.code
        _viewState.updateValue { it.copy(selectedState = addressState) }
    }

    private fun collectData(): Payee? {
        return try {
            getPayeeFromForm()
        } catch (ex: IllegalArgumentException) {
            logger.error(ex.localizedMessage)
            null
        }
    }

    @Suppress("LongMethod")
    private fun getPayeeFromForm(): Payee {
        val companyData = _viewState.value?.selectedCompanyData
        val merchantInfo = getMerchantInfo(companyData)

        val name = companyData?.name ?: getValidValueOrThrow(nameInputState.value)
        val nickName = if (isEditMode || companyData == null) {
            rawNickname.value
        } else {
            null
        }
        val accountNumber = getValidValueOrThrow(accountNumberInputState.value)
        val isAddressAvailable: Boolean
        val address = if (companyData != null) {
            isAddressAvailable = false
            null
        } else {
            isAddressAvailable = true
            getPayeeAddress()
        }
        val phoneNumber = if (isEditMode || companyData == null) {
            getPhoneNumber(phoneNumberInputState.value)
        } else {
            null
        }
        val leadDays = if (companyData != null) { null } else { DEFAULT_LEAD_DAYS }

        return Payee(
            payeeId = rawPayeeId.value,
            name = name,
            nickName = nickName,
            accountNumber = accountNumber,
            isAddressAvailable = isAddressAvailable,
            address = address,
            merchantInfo = merchantInfo,
            phoneNumber = phoneNumber,
            leadDays = leadDays
        )
    }

    private fun getPayeeAddress(): PayeeAddress {
        return PayeeAddress(
            address1 = getValidValueOrThrow(address1InputState.value),
            address2 = rawAddress2.value,
            city = getValidValueOrThrow(cityInputState.value),
            state = viewState.value?.selectedState?.code.orEmpty(),
            zipCode = getValidValueOrThrow(zipCodeInputState.value)
        )
    }

    private fun getMerchantInfo(companyData: SearchPayeeResultItem?): MerchantInfo? =
        companyData?.let {
            val merchantZip = if (it.merchantZipRequired) {
                getValidValueOrThrow(zipCodeInputState.value)
            } else {
                null
            }

            MerchantInfo(
                id = it.merchantId,
                merchantZip = merchantZip,
                merchantZip4 = null
            )
        }

    private fun getValidValueOrThrow(source: InputFieldManager.Value?): String {
        require(source is InputFieldManager.Value.Valid) { "Payee is not valid" }
        return source.content
    }

    private fun getPhoneNumber(source: InputFieldManager.Value?): String {
        val formattedPhoneNumber = getValidValueOrThrow(source)
        val regex = Regex("\\D")
        return formattedPhoneNumber.replace(regex, "")
    }

    internal fun savePayee() {
        val currentData = collectData() ?: return
        _viewState.updateValue {
            it.copy(
                isLoading = true,
                isSaveSuccess = null,
                savePayeeError = null
            )
        }
        viewState.value?.let {
            viewModelScope.launch {
                repo.savePayee(billPaySessionId, currentData).fold(
                    onSuccess = this@PayeeFormViewModel::onSavePayeeSuccess,
                    onFailure = this@PayeeFormViewModel::onSavePayeeError
                )
            }
        }
    }

    internal fun deletePayee() {
        _viewState.updateValue {
            it.copy(
                isLoading = true,
                isDeleteSuccess = null,
                savePayeeError = null
            )
        }
        viewModelScope.launch {
            payeeItem?.payeeId?.let { payeeId ->
                repo.deletePayee(billPaySessionId, payeeId).fold(
                    onSuccess = this@PayeeFormViewModel::onDeletePayeeSuccess,
                    onFailure = this@PayeeFormViewModel::onSavePayeeError
                )
            }
        }
    }

    private fun onSavePayeeError(error: Throwable) {
        logger.error(error.localizedMessage)
        _viewState.postUpdateValue { prevState ->
            prevState.copy(
                isLoading = false,
                isSaveSuccess = null,
                isDeleteSuccess = null,
                savePayeeError = ConsumableLiveEvent(
                    IllegalArgumentException(resources.getString(R.string.error_message_general))
                )
            )
        }
    }

    private fun onSavePayeeSuccess(result: SavePayeeResult) {
        when (result) {
            SavePayeeResult.Successful -> {
                successViewState()
            }
            SavePayeeResult.GenericError -> {
                genericErrorViewState()
            }
            SavePayeeResult.PayeeAlreadyAdded -> {
                alreadyAddPayeeViewState()
            }
        }
    }

    private fun alreadyAddPayeeViewState() {
        _viewState.postUpdateValue { prevState ->
            prevState.copy(
                isLoading = false,
                isSaveSuccess = null,
                savePayeeError = ConsumableLiveEvent(
                    IllegalArgumentException(resources.getString(BillPayR.string.bill_pay_error_payee_already_added))
                )
            )
        }
    }

    private fun genericErrorViewState() {
        _viewState.postUpdateValue { prevState ->
            prevState.copy(
                isLoading = false,
                isSaveSuccess = null,
                savePayeeError = ConsumableLiveEvent(
                    IllegalArgumentException(resources.getString(R.string.error_message_general))
                )
            )
        }
    }

    private fun successViewState() {
        _viewState.postUpdateValue { prevState ->
            prevState.copy(
                isLoading = false,
                savePayeeError = null,
                isSaveSuccess = ConsumableLiveEvent(Unit)
            )
        }
    }

    private fun onDeletePayeeSuccess(isSuccess: Boolean): Boolean {
        return if (isSuccess) {
            _viewState.postUpdateValue { prevState ->
                prevState.copy(
                    isLoading = false,
                    savePayeeError = null,
                    isDeleteSuccess = ConsumableLiveEvent(Unit)
                )
            }
        } else {
            _viewState.postUpdateValue { prevState ->
                prevState.copy(
                    isLoading = false,
                    savePayeeError = ConsumableLiveEvent(Exception())
                )
            }
        }
    }

    internal fun searchPayee(searchTerm: String?) {
        val query = searchTerm ?: return
        if (query.length > MIN_SEARCH_LENGTH) {
            searchDelegate.searchPayee(searchTerm)
        } else {
            searchDelegate.deactivateSearch()
        }
    }

    internal fun selectSearchResult(item: SearchPayeeResultItem) {
        _viewState.updateValue {
            it.copy(
                selectedCompanyData = item,
                payeeValid = it.isPayeeValid(
                    nameInputState,
                    accountNumberInputState,
                    address1InputState,
                    cityInputState,
                    zipCodeInputState,
                    phoneNumberInputState
                ),
                search = it.search?.copy(isActive = false),
                displaySearchMode = false
            )
        }
    }

    fun isValidPayee(contentValidation: () -> Boolean): Boolean {
        if (isEditMode) {
            viewState.value?.let {
                return it.isPayeeValid(
                    nameInputState,
                    accountNumberInputState,
                    address1InputState,
                    cityInputState,
                    zipCodeInputState,
                    phoneNumberInputState
                )
            }
        }
        return contentValidation()
    }
}

internal data class ViewState(
    val isLoading: Boolean = false,
    val isEditMode: Boolean = false,
    val displaySearchMode: Boolean = true,
    val payeeValid: Boolean = false,
    val isSaveSuccess: ConsumableLiveEvent<Unit>? = null,
    val isDeleteSuccess: ConsumableLiveEvent<Unit>? = null,
    val savePayeeError: ConsumableLiveEvent<Throwable>? = null,
    val search: ActionState<List<SearchPayeeResultItem>>? = null,
    val selectedCompanyData: SearchPayeeResultItem? = null,
    val loadUsStates: ActionState<StatesOfUS>? = null,
    val selectedState: UsState? = null
) {
    @Suppress("LongParameterList")
    fun isPayeeValid(
        name: MutableLiveData<InputFieldManager.Value>,
        accountNumber: MutableLiveData<InputFieldManager.Value>,
        address1: MutableLiveData<InputFieldManager.Value>,
        city: MutableLiveData<InputFieldManager.Value>,
        zipCode: MutableLiveData<InputFieldManager.Value>,
        phoneNumber: MutableLiveData<InputFieldManager.Value>
    ): Boolean {
        if (selectedCompanyData != null) {
            var isValid = accountNumber.value is InputFieldManager.Value.Valid
            if (selectedCompanyData.merchantZipRequired) {
                isValid = isValid && zipCode.value is InputFieldManager.Value.Valid
            }
            return isValid
        } else {
            return listOf(
                name,
                accountNumber,
                address1,
                city,
                zipCode,
                phoneNumber
            ).all { it.value is InputFieldManager.Value.Valid }
        }
    }
}
