package com.etrade.mobilepro.orders.extensions.android

import android.content.res.Resources
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.dialog.confirmationDialog

fun Resources.cancelSuccessDialog(dialogMessage: String, onDismiss: () -> Unit): AppDialog {
    return appDialog {
        titleRes = R.string.orders_dialog_cancel_order_title
        message = dialogMessage
        resourcesPositiveAction {
            labelRes = R.string.ok
        }
        dismissAction = onDismiss
    }
}

fun Resources.cancelOrderDialog(positiveAction: () -> Unit, negativeAction: () -> Unit): AppDialog {
    return confirmationDialog(
        title = R.string.orders_dialog_cancel_order_title,
        message = R.string.orders_dialog_cancel_order_confirm_message,
        positiveAction = positiveAction,
        negativeAction = negativeAction
    )
}
