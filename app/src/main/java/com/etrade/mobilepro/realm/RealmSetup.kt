package com.etrade.mobilepro.realm

import android.content.Context
import com.etrade.mobilepro.account.dao.AccountRealmModule
import com.etrade.mobilepro.dao.realm.BuildConfig
import com.etrade.mobilepro.dao.realm.RealmSlf4jLogger
import com.etrade.mobilepro.instrument.dao.InstrumentRealmModule
import com.etrade.mobilepro.msaccounts.dao.MsAccountRealmModule
import com.etrade.mobilepro.positions.dao.PortfolioPositionsRealmModule
import com.etrade.mobilepro.util.secure.nextByteArray
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.log.LogLevel
import io.realm.log.RealmLog
import org.slf4j.LoggerFactory
import java.security.SecureRandom

private val logger = LoggerFactory.getLogger(RealmSetup::class.java)

class RealmSetup(private val applicationContext: Context) {

    fun configureRealm() {
        Realm.init(applicationContext)

        if (BuildConfig.DEBUG) {
            RealmLog.add(RealmSlf4jLogger())
            RealmLog.setLevel(LogLevel.DEBUG)
        }

        val config = RealmConfiguration.Builder()
            .name("et.realm")
            .addModule(AccountRealmModule())
            .addModule(InstrumentRealmModule())
            .addModule(MsAccountRealmModule())
            .addModule(PortfolioPositionsRealmModule())
            .encryptionKey(SecureRandom().nextByteArray(RealmConfiguration.KEY_LENGTH))
            .build()

        if (Realm.deleteRealm(config)) { // ensure that the previous DB is removed before we use new configuration that has different encryption key
            logger.debug("Default realm DB was successfully removed.")
        }
        Realm.setDefaultConfiguration(config)
    }
}
