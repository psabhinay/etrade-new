package com.etrade.mobilepro.common

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.etrade.mobilepro.session.api.User
import javax.inject.Inject

private const val ACTIVITY_TO_START = "com.etrade.mobilepro.home.MainActivity"

interface RequiredLoginProvider {
    fun isLoginRequired(): Boolean
}

fun Any.isRequireLoginAnnotationPresent(): Boolean {
    return javaClass.isAnnotationPresent(RequireLogin::class.java)
}

/**
 * if app was killed by system, user may end up being unauthenticated on screen that require login
 */
class LoginChecker @Inject constructor(private val user: User) {
    private var skipNextTime = false

    // to avoid main activity force death and recreation (and navigating to the default destination) on login cancel
    fun onLoginAborted() {
        skipNextTime = true
    }

    fun check(activity: AppCompatActivity) {
        if (skipNextTime) {
            skipNextTime = false
            return
        }
        if (!user.isLoggedIn() &&
            (activity.isRequireLoginAnnotationPresent() || hasRequireLoginFragment(activity.supportFragmentManager))
        ) {
            val intent = Intent(activity, Class.forName(ACTIVITY_TO_START))
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            activity.startActivity(intent)
        }
    }

    private fun hasRequireLoginFragment(fragmentManager: FragmentManager): Boolean {
        return fragmentManager.fragments.any {
            it.requireLogin() || hasRequireLoginFragment(it.childFragmentManager)
        }
    }

    private fun Fragment.requireLogin(): Boolean {
        return isRequireLoginAnnotationPresent() && ((parentFragment as? RequiredLoginProvider)?.isLoginRequired() ?: true)
    }
}
