package com.etrade.mobilepro.alerts.presentation.base

import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import javax.annotation.concurrent.NotThreadSafe

interface AlertsDeleter {
    fun isDeleting(alertId: String): Boolean
    fun delete(userId: String, alert: AlertItem.Alert, onSuccess: ((AlertItem.Alert) -> Unit)? = null, onError: ((AlertItem.Alert, Throwable) -> Unit)? = null)
    fun delete(userId: String, alertId: String, onSuccess: ((String) -> Unit)? = null, onError: ((String, Throwable) -> Unit)? = null)
    fun unsubscribeAll()
}

@NotThreadSafe
class DefaultAlertsDeleter(private val alertsRepo: AlertsRepo) : AlertsDeleter {
    private val compositeDisposable = CompositeDisposable()
    private val alertsUnderDeletion = mutableSetOf<String>()

    override fun isDeleting(alertId: String): Boolean = alertsUnderDeletion.contains(alertId)

    override fun delete(userId: String, alert: AlertItem.Alert, onSuccess: ((AlertItem.Alert) -> Unit)?, onError: ((AlertItem.Alert, Throwable) -> Unit)?) {
        if (isDeleting(alert.id)) {
            onError?.invoke(alert, IllegalArgumentException("This alert deletion is already in progress (alert id = ${alert.id})"))
        }
        alertsUnderDeletion.add(alert.id)
        compositeDisposable.add(
            alertsRepo
                .deleteAlert(userId, alert.id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onComplete = {
                        alertsUnderDeletion.remove(alert.id)
                        onSuccess?.invoke(alert)
                    },
                    onError = {
                        alertsUnderDeletion.remove(alert.id)
                        onError?.invoke(alert, it)
                    }
                )
        )
    }

    override fun delete(userId: String, alertId: String, onSuccess: ((String) -> Unit)?, onError: ((String, Throwable) -> Unit)?) {
        if (isDeleting(alertId)) {
            onError?.invoke(alertId, IllegalArgumentException("This alert deletion is already in progress (alert id = $alertId)"))
        }
        alertsUnderDeletion.add(alertId)
        compositeDisposable.add(
            alertsRepo
                .deleteAlert(userId, alertId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onComplete = {
                        alertsUnderDeletion.remove(alertId)
                        onSuccess?.invoke(alertId)
                    },
                    onError = {
                        alertsUnderDeletion.remove(alertId)
                        onError?.invoke(alertId, it)
                    }
                )
        )
    }

    override fun unsubscribeAll() {
        alertsUnderDeletion.clear()
        compositeDisposable.clear()
    }
}
