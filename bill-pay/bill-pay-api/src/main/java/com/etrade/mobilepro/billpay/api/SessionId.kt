package com.etrade.mobilepro.billpay.api

@JvmInline
value class SessionId(val value: String)
