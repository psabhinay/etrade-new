package com.etrade.mobilepro.orders.sharedviewmodel

import android.app.Activity
import androidx.navigation.NavController
import com.etrade.mobilepro.orders.api.TradeOrderType

interface OrderRefreshDelegate {
    @SuppressWarnings("LongParameterList")
    fun refreshOrderPages(
        orderSharedViewModel: OrderSharedViewModel,
        navController: NavController,
        activity: Activity,
        accountId: String,
        orderId: String,
        snackBarMessage: String?,
        tradeOrderType: TradeOrderType
    )
}
