package com.etrade.mobilepro.markets

import androidx.navigation.NavController
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.screener.api.ScreenerDisclosure
import com.etrade.mobilepro.screener.presentation.ScreenersRouter

class DefaultMarketScreenersRouter : ScreenersRouter {

    override fun actionScreenersContainerToStockMarketSegment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToStockMarketSegment())
    }

    override fun actionScreenersContainerToStockPriceVolume(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToStockPriceVolume())
    }

    override fun actionScreenersContainerToMutualFundPortfolioFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToMutualFundPortfolioFragment())
    }

    override fun actionScreenersContainerToOpinionsFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToOpinionsFragment())
    }

    override fun actionScreenersContainerToFundamentalsFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToFundamentalsFragment())
    }

    override fun actionScreenersContainerToTechnicalsFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToTechnicalsFragment())
    }

    override fun actionScreenersContainerToEtfRiskFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToEtfRiskFragment())
    }

    override fun actionScreenersContainerToRiskFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToRiskFragment())
    }

    override fun actionScreenersContainerToEarningsAndDividendsFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToEarningsAndDividendsFragment())
    }

    override fun actionScreenersContainerToFeesFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToFeesFragment())
    }

    override fun actionScreenersContainerToEtfPerformanceFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToEtfPerformanceFragment())
    }

    override fun actionScreenersContainerToPerformanceFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToPerformanceFragment())
    }

    override fun actionScreenersContainerToExpensesFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToExpensesFragment())
    }

    override fun actionScreenersContainerToEtfPortfolioFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToEtfPortfolioFragment())
    }

    override fun actionScreenersContainerToEtfFundProfileFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToEtfFundProfileFragment())
    }

    override fun actionScreenersContainerToFundProfileFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToFundProfileFragment())
    }

    override fun actionScreenersContainerToEtfTechnicalsFragment(navController: NavController) {
        navController.navigate(MarketsFragmentDirections.actionScreenersContainerToEtfTechnicalsFragment())
    }

    override fun actionToStockResultsFragment(navController: NavController) {
        navController.navigate(MainGraphDirections.actionMarketScreenersToStockResultsFragment())
    }

    override fun actionToMfResultsFragment(navController: NavController) {
        navController.navigate(MainGraphDirections.actionMarketScreenersToMutualFundResultsFragment())
    }

    override fun actionToEtfResultsFragment(navController: NavController) {
        navController.navigate(MainGraphDirections.actionMarketScreenersToEtfResultsFragment())
    }

    override fun actionShowPopup(navController: NavController, popupText: String) {
        navController.navigate(MainGraphDirections.actionShowScreenerPopup(popupText))
    }

    override fun actionResultsToDisclosure(navController: NavController, disclosureType: ScreenerDisclosure) {
        navController.navigate(MainGraphDirections.actionScreenersResultsToDisclosure(disclosureType))
    }
}
