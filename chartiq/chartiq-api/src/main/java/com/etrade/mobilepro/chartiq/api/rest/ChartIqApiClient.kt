package com.etrade.mobilepro.chartiq.api.rest

import com.etrade.eo.rest.retrofit.Scalar
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

interface ChartIqApiClient {

    @Scalar
    @GET("/phx/pchart/{endpointType}/graphs/{chartQuery}")
    suspend fun getChartData(
        @Path("endpointType") endpointType: String,
        @Path("chartQuery") chartQuery: String
    ): String

    @Scalar
    @GET("/app/user/getallviews.json")
    suspend fun getAllViews(): String

    @Scalar
    @GET("/app/user/getstudies.json")
    suspend fun getStudies(): String

    @Scalar
    @GET("/app/user/getdrawing.json")
    suspend fun getDrawing(
        @Query("symbol") symbol: String
    ): String

    @Scalar
    @POST
    @Headers("Content-Type: application/json")
    suspend fun postRequest(
        @Url url: String,
        @Scalar @Body requestBody: String
    ): String
}
