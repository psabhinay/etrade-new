package com.example.lightstreamer.mergemode

import com.etrade.eo.streaming.SubscriptionMode
import com.etrade.eo.streaming.UpdateInfo
import com.etrade.mobilepro.streaming.api.Level2DataEvent
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import io.reactivex.Observable

class L2DecoratorSubscriptionManagerHelper(
    private val mergeMapper: L2BookResponseMapper,
    private val underlyingHelper: SubscriptionManagerHelper<UpdateInfo>
) : SubscriptionManagerHelper<Level2DataEvent> {

    override fun subscribeTo(
        streamId: String,
        fields: Set<String>,
        mode: SubscriptionMode
    ): Observable<Level2DataEvent> =
        underlyingHelper
            .subscribeTo(streamId, fields, mode)
            .concatMapIterable(mergeMapper::mapMerge)
}
