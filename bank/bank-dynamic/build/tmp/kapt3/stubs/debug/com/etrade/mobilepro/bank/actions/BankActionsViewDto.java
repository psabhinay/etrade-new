package com.etrade.mobilepro.bank.actions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0007\u0018\u00002\u00020\u0001B#\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0010\b\u0001\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0007R\u001c\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/etrade/mobilepro/bank/actions/BankActionsViewDto;", "Lcom/etrade/mobilepro/backends/mgs/BaseScreenView;", "data", "", "ctaList", "", "Lcom/etrade/mobilepro/backends/mgs/CallToActionDto;", "(Ljava/lang/Object;Ljava/util/List;)V", "getCtaList", "()Ljava/util/List;", "getData", "()Ljava/lang/Object;", "bank-dynamic_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class BankActionsViewDto extends com.etrade.mobilepro.backends.mgs.BaseScreenView {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Object data = null;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<com.etrade.mobilepro.backends.mgs.CallToActionDto> ctaList = null;
    
    public BankActionsViewDto(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "data")
    java.lang.Object data, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "cta")
    java.util.List<com.etrade.mobilepro.backends.mgs.CallToActionDto> ctaList) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getData() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.util.List<com.etrade.mobilepro.backends.mgs.CallToActionDto> getCtaList() {
        return null;
    }
}