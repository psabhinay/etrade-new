package com.etrade.mobilepro.billpay.data.rest

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class SavePayeeResponseDto(
    @Json(name = "successResults")
    val successResult: SuccessResultDto? = null,
    @Json(name = "sessionId")
    val sessionId: String? = null
) : BaseDataDto()

@JsonClass(generateAdapter = true)
class SuccessResultDto(
    @Json(name = "status")
    val status: String? = null,
    @Json(name = "accountNumber")
    val accountNumber: String? = null
)
