package com.etrade.mobilepro.alerts.api.model

sealed class AlertItem {
    object LoadingIndicator : AlertItem()

    data class Divider(var show: Boolean = false) : AlertItem()

    data class Alert(
        val id: String,
        val subject: String,
        val occurredAt: Long,
        val occurredAtFormatted: String,
        val code: Int,
        val isDeleteButtonVisible: Boolean = false,
        val status: Status,
    ) : AlertItem() {

        fun belongsTo(): AlertGroup {
            return when {
                isInAccountCodeRange(code) -> AlertGroup.ACCOUNT
                isInMarketCodeRange(code) -> AlertGroup.MARKET
                else -> throw IllegalArgumentException("Failed to check alert group for $this")
            }
        }

        private fun isInAccountCodeRange(code: Int): Boolean =
            !isInMarketCodeRange(code)

        private fun isInMarketCodeRange(code: Int): Boolean =
            code in MARKET_CODE_RANGE_START_INCLUSIVE..MARKET_CODE_RANGE_END_INCLUSIVE
    }

    data class Status(
        val wasRead: Boolean = false,
        var isEditable: Boolean = false,
        var isMarked: Boolean = false,
    )
}

enum class AlertType {
    ACCOUNT,
    ADHOC,
    BANK,
    DIVIDEND,
    MARKET,
    UNKNOWN
}

enum class AlertGroup {
    MARKET,
    ACCOUNT,
}

/**
 * @see AlertAction.SELECT
 * @see AlertAction.MARK
 */
enum class AlertAction {
    /**
     * SELECT occurs when the user chooses to read the alert's details.
     */
    SELECT,
    DELETE,
    /**
     * MARK means that the user has checked off the alert for future action.
     */
    MARK,
}

private const val MARKET_CODE_RANGE_START_INCLUSIVE = 600
private const val MARKET_CODE_RANGE_END_INCLUSIVE = 899
