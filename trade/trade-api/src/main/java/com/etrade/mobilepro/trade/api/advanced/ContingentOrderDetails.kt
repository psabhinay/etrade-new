package com.etrade.mobilepro.trade.api.advanced

import com.etrade.mobilepro.orders.api.ContingentCondition
import com.etrade.mobilepro.orders.api.ContingentQualifier

interface ContingentOrderDetails {
    val condition: ContingentCondition
    val conditionValue: String
    val qualifier: ContingentQualifier
    val symbol: String
    val displaySymbol: String
}
