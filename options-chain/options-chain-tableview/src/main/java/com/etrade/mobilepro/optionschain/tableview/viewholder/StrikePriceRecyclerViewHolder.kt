package com.etrade.mobilepro.optionschain.tableview.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.optionschain.tableview.R
import com.etrade.mobilepro.optionschain.tableview.databinding.OptionsChainStrikePriceItemBinding
import java.math.BigDecimal

/**
 * ViewHolder class for Strike Price Recycler
 */
class StrikePriceRecyclerViewHolder(private val binding: OptionsChainStrikePriceItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(strikePrice: BigDecimal) {
        MarketDataFormatter.formatPriceToBeUpdated(strikePrice).let {
            binding.strikePriceItemTextView.text = it
            binding.strikePriceItemTextView.contentDescription =
                "${itemView.resources.getString(R.string.options_chain_strike)} $it"
        }
    }
}
