package com.etrade.mobilepro.chat.api.setup.repo

import com.etrade.mobilepro.chat.api.ChatParamsModel
import com.etrade.mobilepro.chat.api.setup.models.ChatActiveStatusModel
import com.etrade.mobilepro.common.result.ETResult

interface ChatSetupRepo {
    suspend fun getChatActiveStatus(): ETResult<ChatActiveStatusModel>
    suspend fun setupChat(params: ChatParamsModel): ChatSetupResult
}
