package com.etrade.mobilepro.quote.screen.presentation.extensions

import androidx.annotation.DrawableRes
import com.etrade.mobilepro.quote.screen.presentation.R
import com.etrade.mobilepro.quoteapi.FundQuoteItem

fun FundQuoteItem.MorningStarRatings?.overallStarRating(): String = getFundRatings(this?.overAllDrawable)

fun FundQuoteItem.MorningStarRatings?.threeYearsStarRating(): String = getFundRatings(this?.threeYearsDrawable)

fun FundQuoteItem.MorningStarRatings?.fiveYearsStarRating(): String = getFundRatings(this?.fiveYearsDrawable)

fun FundQuoteItem.MorningStarRatings?.tenYearsStarRating(): String = getFundRatings(this?.tenYearsDrawable)

fun getFundRatings(@DrawableRes res: Int? = null) = when (res) {
    R.drawable.fund_ratings_1 -> "1"
    R.drawable.fund_ratings_2 -> "2"
    R.drawable.fund_ratings_3 -> "3"
    R.drawable.fund_ratings_4 -> "4"
    R.drawable.fund_ratings_5 -> "5"
    else -> "0"
}
