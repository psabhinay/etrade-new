package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import com.etrade.mobilepro.orders.api.FundsOrderActionType
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.trade.api.ConfirmedOrder
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.action
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrder
import com.etrade.mobilepro.trade.api.quantity
import com.etrade.mobilepro.trade.api.ticker
import com.etrade.mobilepro.trade.presentation.util.OrderDisplayItem
import com.etrade.mobilepro.trade.presentation.util.R
import com.etrade.mobilepro.trade.presentation.util.formatLegDetails
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

internal class MutualFundOrderAdapter(
    private val resources: Resources,
    private val confirmedOrderAdapterDelegate: OrderAdapterDelegate
) : OrderAdapter {

    override fun createOrderDisplayItem(order: Order): OrderDisplayItem {
        require(order is MutualFundsOrder)

        val orderPlacedItems = confirmedOrderAdapterDelegate.createOrderSectionItems(order)
        return OrderDisplayItem(
            resources.formatLegDetails(
                order.action,
                order.ticker,
                getMutualFundAmountDisplayText(order.orderActionType, order.quantity, order.quantityType, resources)
            ),
            orderPlacedItems + order.createMutualFundsTradeOrderItems(resources, order is ConfirmedOrder)
        )
    }

    private fun MutualFundsOrder.createMutualFundsTradeOrderItems(
        resources: Resources,
        isConfirmedOrder: Boolean
    ): List<TableSectionItemLayout> {
        return listOfNotNull(accountItem(resources, this)) +
            createMutualFundsOrderTypeItems(resources, orderActionType) +
            toAmountItems(resources) +
            if (isConfirmedOrder) {
                toMutualFundsCommissionItems(resources)
            } else {
                emptyList()
            }
    }

    private fun createMutualFundsOrderTypeItems(resources: Resources, orderActionType: FundsOrderActionType): List<TableSectionItemLayout> {
        return listOfNotNull(
            resources.sectionItem(R.string.trade_label_order_type, orderActionType.getLabel())
        )
    }
}
