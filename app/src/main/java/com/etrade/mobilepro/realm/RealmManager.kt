package com.etrade.mobilepro.realm

import androidx.annotation.MainThread
import io.realm.Realm

/**
 *  Realm Manager holds a global soft reference to the default realm instance.
 *
 *  Any UI Component such as Fragment / Activity or a View Model that depends on realm instance is responsible to open or
 *  close the realm instance as per its own life cycle scope.
 *
 *  For eg: ViewModel - use init block to open and onCleared method to close the realm instance.
 */
object RealmManager {

    private var refCounter: Int = 0

    private var realm: Realm? = null

    @MainThread
    fun open() {
        if (refCounter == 0 || realm == null) {
            realm = Realm.getDefaultInstance()
            refCounter = 0
        }

        refCounter = refCounter.inc()
    }

    @MainThread
    fun close() {
        refCounter = refCounter.dec()
        if (refCounter == 0) {
            realm?.close()
        }
    }

    // This method should be called only after realm instance initialized with open
    @MainThread
    fun getDefaultInstance(): Realm {
        checkForOpenRealm()
        return requireNotNull(realm)
    }

    private fun checkForOpenRealm() {
        check((realm != null)) {
            "RealmManager: Realm is not initialized, call open() method first"
        }
        check(realm?.isClosed == false) {
            "RealmManager: Realm is closed, call open() method first"
        }
    }
}
