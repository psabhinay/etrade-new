package com.etrade.mobilepro.news.data.repo

import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.data.dto.DocDto
import com.etrade.mobilepro.news.data.dto.DocsListDto
import com.etrade.mobilepro.news.data.dto.NewsDataDto
import com.etrade.mobilepro.news.data.dto.NewsDto
import com.etrade.mobilepro.news.data.dto.NewsPayloadDto
import com.etrade.mobilepro.news.data.dto.TextDocDto
import com.etrade.mobilepro.news.data.rest.NewsApiClient
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner

private val noNewsResponse = NewsDto(NewsDataDto(NewsPayloadDto(DocsListDto(emptyList()))))

private val testDocDto1 = DocDto(TextDocDto(docId = "1", title = "t1", timestamp = null, url = "u1", symbols = "S1", content = "c1", source = "source1"))
private val testDocDto2 = DocDto(TextDocDto(docId = "2", title = "t2", timestamp = null, url = "u2", symbols = "S2", content = "c2", source = "source2"))
private val testDocDto3 = DocDto(TextDocDto(docId = "3", title = "t3", timestamp = null, url = "u3", symbols = "S3", content = "c3", source = "source3"))

private const val AMOUNT = 3

private val normalResponsePortfolio = NewsDto(NewsDataDto(NewsPayloadDto(DocsListDto(listOf(testDocDto1, testDocDto2, testDocDto3)))))

@RunWith(RobolectricTestRunner::class)
class DefaultWidgetNewsRepoTest {

    private lateinit var sut: DefaultWidgetNewsRepo

    @Mock
    private lateinit var apiClient: NewsApiClient

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        sut = DefaultWidgetNewsRepo(apiClient)
    }

    @Test
    fun `no news in response should result in NewsItem Empty`() {
        whenever(apiClient.getPortfolioNews(any(), any(), any(), any())).thenReturn(Single.just(noNewsResponse))

        val result = sut.getWidgetNews(NewsRequest.Portfolio(), 1).blockingGet()

        assertTrue("result size = 1", result.size == 1)
        assertTrue("the only element it NewsItem.Empty", result[0] is NewsItem.Empty)
    }

    @Test
    fun `success case portfolio`() {
        whenever(apiClient.getPortfolioNews(any(), any(), any(), any())).thenReturn(Single.just(normalResponsePortfolio))

        val result = sut.getWidgetNews(NewsRequest.Portfolio(), AMOUNT).blockingGet()

        assertTrue("result size is the same", result.size == AMOUNT)
        assertTrue("first id", (result[0] is NewsItem.Text && (result[0] as NewsItem.Text).docId == "1"))
        assertTrue("second id", (result[1] is NewsItem.Text && (result[1] as NewsItem.Text).docId == "2"))
        assertTrue("third id", (result[2] is NewsItem.Text && (result[2] as NewsItem.Text).docId == "3"))
    }
}
