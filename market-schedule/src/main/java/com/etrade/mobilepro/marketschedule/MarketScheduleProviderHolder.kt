package com.etrade.mobilepro.marketschedule

import com.etrade.eo.marketschedule.api.MarketScheduleProvider

/**
 * Provides interface for retrieving [MarketScheduleProvider] instance.
 * We need this holder class to be able to provide a band new instance of [MarketScheduleProvider] on each successful authorization.
 */
interface MarketScheduleProviderHolder {

    /**
     * Not null instance is returned if User is authorized and null otherwise.
     */
    var marketScheduleProvider: MarketScheduleProvider?

    /**
     * boolean indicating if current schedule is extended hours
     */
    val isExtendedHourEligiblePeriod: Boolean
}
