package com.etrade.mobilepro.alerts.data.dto

import org.simpleframework.xml.Root

@Root(strict = false, name = "DeleteAlertResponse")
class DeleteAlertResponseXmlDto
