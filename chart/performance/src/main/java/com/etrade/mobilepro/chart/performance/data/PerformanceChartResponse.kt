package com.etrade.mobilepro.chart.performance.data

import com.etrade.mobilepro.chart.data.ChartDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class PerformanceChartResponseDto(
    @Json(name = "data")
    val data: PerformanceChartResponseDataDto?
)

@JsonClass(generateAdapter = true)
internal class PerformanceChartResponseDataDto(
    @Json(name = "performance")
    val performance: ChartDto
)
