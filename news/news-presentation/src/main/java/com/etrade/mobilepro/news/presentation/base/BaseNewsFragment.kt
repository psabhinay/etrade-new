package com.etrade.mobilepro.news.presentation.base

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.annotation.CallSuper
import androidx.core.content.ContextCompat
import androidx.core.widget.ContentLoadingProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.etrade.mobilepro.common.BaseDaggerFragment
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.navdestinations.openDisclosures
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.news.presentation.NewsSetupResolver
import com.etrade.mobilepro.news.presentation.NewsSharedViewModel
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.news.presentation.adapter.AddItemsAnimator
import com.etrade.mobilepro.news.presentation.adapter.DisclosuresAdapter
import com.etrade.mobilepro.news.presentation.adapter.LoadingAdapter
import com.etrade.mobilepro.news.presentation.adapter.NewsAdapter
import com.etrade.mobilepro.news.presentation.customview.TextNewsCellView
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentBindHelper
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.util.android.recyclerviewutil.CustomRecyclerViewItemDivider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

@Suppress("TooManyFunctions")
abstract class BaseNewsFragment<VM : BaseNewsViewModel> : BaseDaggerFragment<VM>() {
    @Inject
    lateinit var bindHelper: TextNewsContentBindHelper

    @Inject
    lateinit var router: NewsRouter

    @Inject
    lateinit var setupResolver: NewsSetupResolver

    @Inject
    lateinit var addItemsAnimator: AddItemsAnimator

    protected lateinit var sharedViewModel: NewsSharedViewModel

    protected open val recyclerDividerPositions: List<Int> = emptyList()

    private var concatAdapter: ConcatAdapter? = null
    private var newsAdapter: NewsAdapter? = null
    private var newsContentTextView: TextView? = null
    private var newsContentLoadingProgressBar: ContentLoadingProgressBar? = null
    private var newsCell: TextNewsCellView? = null

    private val separator: Drawable? by lazy {
        ContextCompat.getDrawable(requireContext(), R.drawable.list_item_divider_light_grey)
    }

    private val loadStateListener: (CombinedLoadStates) -> Unit = {
        if (it.refresh is LoadState.NotLoading && (newsAdapter?.itemCount ?: 0) > 0) {
            viewModel.handleFirstLoadedItem(newsAdapter?.snapshot()?.items?.firstOrNull())
        }
        if (it.refresh is LoadState.Error) {
            viewModel.handleFirstLoadedItem(NewsItem.Empty)
        }
    }

    protected abstract fun getNewsDetailContainer(): View?

    protected abstract fun getSwipeRefreshLayout(): SwipeRefreshLayout

    protected abstract fun getNewsRecyclerView(): RecyclerView

    protected open fun getNewsAdapter(): NewsAdapter {
        return NewsAdapter(
            selectNewsItemListener = { selectedItem, position ->
                sharedViewModel.saveSelectedNewsItem(selectedItem)
                viewModel.newsItemSelected(selectedItem, position)
                if (!viewModel.isInDualPane) {
                    router.navigateToTextNewsDetails(findNavController())
                }
            },
            statusChecker = { item ->
                viewModel.wasRead(item.docId)
            },
            symbolSelectListener = {
                navigateToQuotePage(it)
            },
            selectionChecker = {
                viewModel.selectionTracker.current == it && viewModel.isInDualPane
            },
            showSearch = setupResolver.showSearch(activity, this),
            lifecycleOwner = viewLifecycleOwner
        ).apply {
            addLoadStateListener(loadStateListener)
        }
    }

    protected open fun setupNewsRecyclerView(recyclerView: RecyclerView) {
        recyclerView.adapter = concatAdapter
        val linearLayoutManager = LinearLayoutManager(activity)
        linearLayoutManager.orientation = RecyclerView.VERTICAL
        recyclerView.layoutManager = linearLayoutManager
        separator?.let { separator ->
            CustomRecyclerViewItemDivider(
                drawable = separator,
                positions = recyclerDividerPositions,
                removeTopDivider = false,
                countOfLastItemsWithoutDivider = 0
            ).also {
                recyclerView.addItemDecoration(it)
            }
        }
    }

    @CallSuper
    protected open fun setupFragment() {
        getSwipeRefreshLayout().applyColorScheme()
    }

    protected open fun bindViewModels(
        viewModel: VM,
        sharedViewModel: NewsSharedViewModel,
        adapter: NewsAdapter
    ) {
        // nop
    }

    @ExperimentalCoroutinesApi
    protected open fun onUserRefresh(viewModel: VM) {
        viewModel.refresh(true)
    }

    @ExperimentalCoroutinesApi
    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(setupResolver.hasOptionsMenu(getNewsDetailContainer(), parentFragment))
        newsContentLoadingProgressBar =
            getNewsDetailContainer()?.findViewById(R.id.news_content_loading_pb)
        newsContentTextView = getNewsDetailContainer()?.findViewById(R.id.news_content)
        newsCell = getNewsDetailContainer()?.findViewById(R.id.text_news_cell)

        super.onViewCreated(view, savedInstanceState)

        baseFragmentSetup()
        if (viewModel.isInDualPane) {
            baseFragmentDetailPartSetup()
        }
        setupFragment()
        val localNewsAdapter = newsAdapter ?: getNewsAdapter()
        newsAdapter = localNewsAdapter
        val loadingAdapter = LoadingAdapter()
        val disclosuresAdapter = DisclosuresAdapter { startActivity(activity?.openDisclosures()) }
        val recyclerView = getNewsRecyclerView()
        var isRestoring = viewModel.pagingDataFlow.value != PagingData.empty<NewsItem>()
        recyclerView.itemAnimator = addItemsAnimator
        concatAdapter = ConcatAdapter(localNewsAdapter, disclosuresAdapter, loadingAdapter).apply {
            registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    if (positionStart == 0 && !isRestoring) {
                        recyclerView.layoutManager?.scrollToPosition(0)
                    }
                    isRestoring = false
                }
            })
        }
        setupNewsRecyclerView(recyclerView)
        baseBindSharedViewModel()
        bindBaseViewModel(viewModel, localNewsAdapter, loadingAdapter, disclosuresAdapter)
        bindViewModels(viewModel, sharedViewModel, localNewsAdapter)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (setupResolver.shouldHaveOptionsMenuWithDetails(
                viewModel.isInDualPane,
                parentFragment
            )
        ) {
            val searchItem = menu.findItem(R.id.menu_company_lookup)
            searchItem?.let {
                menu.clear()
            }
            inflater.inflate(R.menu.options_menu_news_detail, menu)
            searchItem?.let {
                menu.add(it.groupId, it.itemId, it.order, it.title)
                val updatedSearchItem = menu.findItem(R.id.menu_company_lookup)
                updatedSearchItem?.setIcon(R.drawable.ic_search_24dp)
                updatedSearchItem?.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM)
            }
        } else {
            menu.clear()
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_font_adjust -> {
                viewModel.fontAdjustHandler.toggleFontSize()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    @ExperimentalCoroutinesApi
    private fun baseFragmentSetup() {
        viewModel.isInDualPane = getNewsDetailContainer() != null
        getSwipeRefreshLayout().setOnRefreshListener {
            onUserRefresh(viewModel)
        }
        sharedViewModel = activity?.run {
            ViewModelProvider(this, viewModelFactory).get(NewsSharedViewModel::class.java)
        } ?: throw IllegalStateException("Invalid Activity")
    }

    private fun baseFragmentDetailPartSetup() {
        newsCell?.toggleDisclosureArrowVisibility(false)
        newsCell?.symbolSelectListener = {
            navigateToQuotePage(it)
        }
        newsContentLoadingProgressBar?.hide()
    }

    private fun baseBindSharedViewModel() {
        sharedViewModel.activeTab.observe(
            viewLifecycleOwner,
            Observer {
                if (it == viewModel.getNewsType()) {
                    viewModel.onBecomeActive()
                }
            }
        )
    }

    @ExperimentalCoroutinesApi
    @Suppress("LongMethod")
    private fun bindBaseViewModel(
        baseViewModel: BaseNewsViewModel,
        newsAdapter: NewsAdapter,
        loadingAdapter: LoadingAdapter,
        disclosuresAdapter: DisclosuresAdapter
    ) {
        bindHelper.bindTextNewsContent(
            viewModel.contentLoader,
            viewLifecycleOwner,
            newsContentLoadingProgressBar,
            newsContentTextView,
            newsCell,
            actionOnRetry = {
                snackbarUtil.retrySnackbar {
                    viewModel.contentLoader.retry()
                }
            }
        )
        bindHelper.bindTextNewsFontAdjusting(
            viewModel.fontAdjustHandler,
            viewLifecycleOwner,
            newsContentTextView,
            newsCell
        )
        baseViewModel.selectedNewsItem.observe(
            viewLifecycleOwner,
            Observer { item ->
                newsCell?.bindTo(item)
            }
        )

        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            baseViewModel.pagingDataFlow.collectLatest {
                newsAdapter.submitData(it)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            newsAdapter.loadStateFlow.collectLatest {
                concatAdapter?.apply {
                    if (it.refresh is LoadState.NotLoading) {
                        addAdapter(disclosuresAdapter)
                    } else {
                        removeAdapter(disclosuresAdapter)
                    }
                }
                getSwipeRefreshLayout().isRefreshing = it.refresh == LoadState.Loading

                handleNetworkState(
                    it.refresh,
                    actionOnRetry = {
                        newsAdapter.retry()
                    }
                )

                concatAdapter?.apply {
                    if (it.append != LoadState.Loading) {
                        removeAdapter(loadingAdapter)
                        addAdapter(disclosuresAdapter)
                    } else {
                        removeAdapter(disclosuresAdapter)
                        addAdapter(loadingAdapter)
                    }
                }

                handleNetworkState(
                    it.append,
                    actionOnRetry = {
                        newsAdapter.retry()
                    }
                )
            }
        }

        baseViewModel.navigateLocationData.observe(
            viewLifecycleOwner,
            Observer {
                findNavController().navigate(it)
            }
        )
        baseViewModel.refreshItemAtIndexesEvent.observe(
            viewLifecycleOwner,
            Observer {
                it.forEach { changedItemIndex ->
                    newsAdapter.notifyItemChanged(changedItemIndex)
                }
            }
        )
    }

    private fun handleNetworkState(state: LoadState, actionOnRetry: () -> Unit) {
        if (state is LoadState.Error) {
            snackbarUtil.retrySnackbar(actionOnRetry = actionOnRetry)
        }
    }

    private fun navigateToQuotePage(ticker: String) {
        router.navigateToQuoteDetails(requireView(), ticker, InstrumentType.EQ)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        newsAdapter = null
        getNewsRecyclerView().adapter = null
    }
}
