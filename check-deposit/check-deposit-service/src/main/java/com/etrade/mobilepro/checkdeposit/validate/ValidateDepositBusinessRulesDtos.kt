package com.etrade.mobilepro.checkdeposit.validate

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ValidateDepositBusinessRulesDto(
    @Json(name = "value")
    val value: ValidateDepositBusinessRulesValueDto
)

@JsonClass(generateAdapter = true)
data class ValidateDepositBusinessRulesValueDto(
    @Json(name = "instNo")
    val instNo: String,
    @Json(name = "accountNo")
    val accountNo: String,
    @Json(name = "depositAmount")
    val depositAmount: String,
    @Json(name = "iraDepositType")
    val iraDepositType: String?
)

@JsonClass(generateAdapter = true)
class ValidateDepositBusinessRulesResponseDto : BaseDataDto()
