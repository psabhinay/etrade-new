package com.etrade.mobilepro.masked.input.value.view

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class MaskedInputUtilsTest {

    @Test
    fun `test handleOnTextAdded`() {
        data class Param(
            val input: CharSequence,
            val mask: String,
            val expectedResult: CharSequence
        )

        val rawValueBuilder = StringBuilder()
        listOf(
            Param("", "", ""),
            Param("", "$", ""),
            Param("123", "####", "123"),
            Param("$123", "$####", "123"),
            Param("12$3", "##$##", "123")
        ).forEach {
            rawValueBuilder.clear()
            rawValueBuilder.handleOnTextAdded(it.input, it.mask)
            assertEquals(it.expectedResult, rawValueBuilder.toString())
        }
    }

    @Test
    fun `test handleOnTextRemoved`() {
        data class Param(
            val rawValue: String,
            val startPosition: Int,
            val removedLength: Int,
            val mask: String,
            val expectedResult: CharSequence
        )

        val maskPlaceholderChar = '#'
        listOf(
            Param("", 0, 0, "###", ""),
            Param("123", 2, 1, "###", "12"),
            Param("123", 2, 1, "$####", "13"),
            Param("123", 2, 2, "$####", "1"),
            Param("123", 8, 1, "+1 (###) #", "12")
        ).forEach {
            val rawValueBuilder = StringBuilder(it.rawValue)
            rawValueBuilder.handleOnTextRemoved(
                it.startPosition,
                it.removedLength,
                it.mask,
                maskPlaceholderChar
            )
            assertEquals(it.expectedResult, rawValueBuilder.toString())
        }
    }
}
