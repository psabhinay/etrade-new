package com.etrade.mobilepro.documents.data

import org.threeten.bp.format.DateTimeFormatter
import java.util.Locale

internal val DOCUMENTS_STATEMENTS_DATE_FORMATTER_FOR_SERVER: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US)
internal val DOCUMENTS_TAX_DATE_FORMATTER_FOR_SERVER: DateTimeFormatter = DateTimeFormatter.ofPattern("M/d/yyyy", Locale.US)
internal val DOCUMENTS_DATE_FORMATTER_FOR_UI: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US)
