package com.etrade.mobilepro.trade.data.form.input

import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.data.form.DefaultSymbolInputValueCoder
import com.etrade.mobilepro.trade.data.form.value.MutualFundActionValueId
import com.etrade.mobilepro.trade.data.form.value.MutualFundsInvestmentTypeValueId
import com.etrade.mobilepro.trade.data.mutualfunds.QUANTITY_CASH_POSITION
import com.etrade.mobilepro.trade.data.mutualfunds.QUANTITY_DOLLARS
import com.etrade.mobilepro.trade.data.mutualfunds.QUANTITY_ENTIRE_POSITION
import com.etrade.mobilepro.trade.data.mutualfunds.QUANTITY_MARGIN_POSITION
import com.etrade.mobilepro.trade.data.mutualfunds.QUANTITY_SHARES
import com.etrade.mobilepro.trade.form.api.input.MutualFundTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.PositionTypeInputId
import com.etrade.mobilepro.trade.form.api.input.QuantityInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.trade.form.api.value.AmountToSellValueId
import com.etrade.mobilepro.trade.form.api.value.InvestmentTypeValueId
import com.etrade.mobilepro.util.ValueCoder

object DefaultMutualFundTradeFormInputId : MutualFundTradeFormInputId, TradeFormInputId by DefaultTradeFormInputId {
    override val action: String = "orderAction"
    override val actionValueId: ActionValueId = MutualFundActionValueId
    override val amount: String = "investmentAmount"
    override val quantity: QuantityInputId = object : QuantityInputId {
        override val dollars: PositionTypeInputId = object : PositionTypeInputId {
            override val cash: String = "dollarsCash"
            override val margin: String = "dollarsMargin"
        }
        override val shares: PositionTypeInputId = object : PositionTypeInputId {
            override val cash: String = "sharesCash"
            override val margin: String = "sharesMargin"
        }
    }
    override val investmentType: String = "reinvestoption"
    override val investmentTypeValueId: InvestmentTypeValueId = MutualFundsInvestmentTypeValueId
    override val positionTypeToTrade: String = "quantityType"
    override val amountToSellValueId: AmountToSellValueId = object : AmountToSellValueId {
        override val cashPosition: String = QUANTITY_CASH_POSITION
        override val entirePosition: String = QUANTITY_ENTIRE_POSITION
        override val marginPosition: String = QUANTITY_MARGIN_POSITION
        override val dollars: String = QUANTITY_DOLLARS
        override val shares: String = QUANTITY_SHARES
    }

    override val entireCashPosition: String = "Entire cash position"
    override val entireMarginPosition: String = "Entire margin position"
    override val fundToBuy: String = "Fund to Buy"
    override val symbol: String = "legDetails"
    override val symbolCoder: ValueCoder<WithSymbolInfo> = DefaultSymbolInputValueCoder
}
