package com.etrade.mobilepro.pushnotification.presentation

import com.google.firebase.messaging.RemoteMessage

interface MessagingService {

    fun onDestroy()

    fun onNewToken(token: String)

    fun onMessageReceived(message: RemoteMessage)
}
