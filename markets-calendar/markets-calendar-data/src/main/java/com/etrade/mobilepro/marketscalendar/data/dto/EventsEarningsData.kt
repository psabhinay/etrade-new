package com.etrade.mobilepro.marketscalendar.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class EventsEarningsData(
    @Json(name = "symbol")
    val symbol: String?,
    @Json(name = "epsestMean")
    val epsestMean: String?,
    @Json(name = "epsactualValue")
    val epsactualValue: String?,
    @Json(name = "dateTime")
    val dateTime: String?, // 20190610160300
    @Json(name = "epsannounceDate")
    val epsannounceDate: String?, // 20190610
    @Json(name = "epsannounceTime")
    val epsannounceTime: String? // 16:03:00
)
