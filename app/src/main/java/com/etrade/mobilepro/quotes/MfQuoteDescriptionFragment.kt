package com.etrade.mobilepro.quotes

import com.etrade.mobilepro.R
import com.etrade.mobilepro.overlay.BottomSheetSimpleTextFragment

class MfQuoteDescriptionFragment : BottomSheetSimpleTextFragment() {
    private val body: CharSequence? by lazy { arguments?.let { MfQuoteDescriptionFragmentArgs.fromBundle(it).bodyText } }

    override val text: CharSequence
        get() = body ?: getString(R.string.mf_quote_description_not_found)
}
