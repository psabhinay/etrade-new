package com.etrade.mobilepro.trade.data.form.input

import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.data.form.CommonAllOrNoneValueCoder
import com.etrade.mobilepro.trade.data.form.DefaultSymbolInputValueCoder
import com.etrade.mobilepro.trade.data.form.value.AdvancedActionValueId
import com.etrade.mobilepro.trade.data.form.value.CommonAdvancedPriceTypeValueId
import com.etrade.mobilepro.trade.data.form.value.CommonOrderTermValueId
import com.etrade.mobilepro.trade.form.api.input.StopLossStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.util.ValueCoder

object DefaultStopLossStockTradeFormInputId :
    StopLossStockTradeFormInputId,
    TradeFormInputId by DefaultTradeFormInputId,
    WithAdvancedTradeFormInputId by DefaultAdvancedTradeFormInputId {
    override val walkthroughFormCompleted: String = "WalkthroughFormCompleted"
    override val accountId: String = "accountid"
    override val action: String = "transaction0"
    override val actionValueId: ActionValueId = AdvancedActionValueId
    override val allOrNone: String = "aon0"
    override val allOrNoneValueCoder: ValueCoder<Boolean> = CommonAllOrNoneValueCoder
    override val limitPrice: String = "price0"
    override val limitPriceForStopLimit: String = "limitPriceForStopLimit"
    override val offsetValue: String = "offsetvalue0"
    override val orderId: String = "OrderId"
    override val originalQuantity = "originalquantity"
    override val priceType: String = "pricetype0"
    override val priceTypeValueId: PriceTypeValueId = CommonAdvancedPriceTypeValueId
    override val positionLotId: String = "lotid"
    override val quantity: String = "quantity0"
    override val stopPrice: String = "stopprice0"
    override val stopPriceForStopLimit: String = "stopPriceForStopLimit"
    override val stopLossInputId = DefaultStopLossOrderInputId
    override val symbol: String = "symbol0"
    override val symbolCoder: ValueCoder<WithSymbolInfo> = DefaultSymbolInputValueCoder
    override val term: String = "orderterm0"
    override val termValueId: OrderTermValueId = CommonOrderTermValueId
}
