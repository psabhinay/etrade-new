package com.etrade.mobilepro.landing.presentation.helper

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.session.api.SessionStateChange
import javax.inject.Inject

interface LandingPageSelectionShowHideHelper {
    val showLandingPageSelection: LiveData<Boolean>
    fun userSessionStateChange(event: SessionStateChange)
    fun hideLandingPageSelection()
}

class DefaultLandingPageSelectionShowHideHelper @Inject constructor(
    private val applicationPreferences: ApplicationPreferences
) : LandingPageSelectionShowHideHelper {
    private val _showLandingPageSelection = MutableLiveData<Boolean>()
    override val showLandingPageSelection: LiveData<Boolean>
        get() = _showLandingPageSelection

    override fun userSessionStateChange(event: SessionStateChange) {
        _showLandingPageSelection.value =
            applicationPreferences.shouldShowLandingSelection &&
            applicationPreferences.homeLandingCount == 1 &&
            event is SessionStateChange.AUTHENTICATED
    }

    override fun hideLandingPageSelection() {
        applicationPreferences.shouldShowLandingSelection = false
        _showLandingPageSelection.value = false
    }
}
