package com.etrade.mobilepro.chartiq.data.dto

import androidx.annotation.Keep
import com.etrade.mobilepro.util.json.FallbackEnum
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChartServiceRequestDto(
    @Json(name = "requestId")
    val requestId: String,
    @Json(name = "requestType")
    val requestType: ChartRequestType,
    @Json(name = "requestBody")
    val requestBody: String? = null,
    @Json(name = "query")
    val query: String? = null
)

@FallbackEnum(name = "Unknown")
@Keep
enum class ChartRequestType {
    @Json(name = "ChartData")
    ChartData,
    @Json(name = "Drawings")
    Drawings,
    @Json(name = "saveView")
    SaveView,
    @Json(name = "deleteView")
    DeleteView,
    @Json(name = "saveDrawings")
    SaveDrawings,
    @Json(name = "deleteDrawings")
    DeleteDrawings,
    @Json(name = "Studies")
    Studies,
    @Json(name = "Views")
    Views,
    @Json(name = "OpenOrders")
    OpenOrders,
    @Json(name = "Unknown")
    Unknown
}
