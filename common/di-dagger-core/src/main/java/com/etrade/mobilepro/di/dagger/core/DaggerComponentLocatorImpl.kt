package com.etrade.mobilepro.di.dagger.core

class DaggerComponentLocatorImpl : DaggerComponentLocator {

    private val factories = mutableMapOf<Class<*>, () -> Any>()
    private val components = mutableMapOf<Class<*>, Any>()

    override fun <T : Any> put(componentClass: Class<T>, component: T) {
        require(components.containsValue(componentClass).not())
        components[componentClass] = componentClass.cast(component)!!
    }

    /**
     * A separate implementation to put a factory into the map, thereby allowing a component to be initialized
     * later, when necessary. The factory will have global scoping, but the map will control singularity. As a result,
     * throughout the lifecycle of the locator, the factory may be called multiple times if the component is removed.
     */
    override fun <T : Any> putFactory(componentClass: Class<T>, componentFactory: () -> T) {
        factories[componentClass] = componentFactory
    }

    override fun <T : Any> getComponent(componentClass: Class<T>): T? =
        componentClass.cast(components[componentClass]) ?: factories[componentClass]?.let { factory ->
            componentClass.cast(factory())?.also { component ->
                components[componentClass] = component
            }
        }

    override fun <T : Any> remove(componentClass: Class<T>) {
        components.remove(componentClass)
    }
}
