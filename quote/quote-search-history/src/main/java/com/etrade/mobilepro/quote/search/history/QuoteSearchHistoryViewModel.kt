package com.etrade.mobilepro.quote.search.history

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.invoke
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class QuoteSearchHistoryViewModel @Inject constructor(
    private val getRecentQuoteSearchHistoryUseCase: GetRecentQuoteSearchHistoryUseCase
) : ViewModel(), QuoteSearchHistory {

    override val items: LiveData<List<SearchResultItem>?>
        get() = _items

    private val _items: MutableLiveData<List<SearchResultItem>> = MutableLiveData()

    val hasItems: LiveData<Boolean> = _items.map { it.isNotEmpty() }

    private val logger: Logger = LoggerFactory.getLogger(QuoteSearchHistoryViewModel::class.java)

    fun fetch(size: Int?) {
        if (_items.value == null) {
            refresh(size)
        }
    }

    private fun refresh(size: Int?) {
        viewModelScope.launch {
            getRecentQuoteSearchHistoryUseCase(size)
                .onSuccess { _items.value = it }
                .onFailure { logger.error("Unable to get quotes search history", it) }
        }
    }
}
