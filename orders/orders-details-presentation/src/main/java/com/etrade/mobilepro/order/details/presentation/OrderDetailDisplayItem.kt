package com.etrade.mobilepro.order.details.presentation

import android.view.View
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.api.OrderLegItemInterface
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quoteapi.QuoteBidAskItem
import com.etrade.mobilepro.quoteapi.QuotePriceWidgetItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

sealed class OrderDetailDisplayItem {
    data class OrderSectionHeader(
        @StringRes
        val text: Int,
        val order: Int? = null
    ) : OrderDetailDisplayItem(), OrderDetailItemInterface

    data class OrderLegItem(
        override var displaySymbol: String,
        override val displayQuantity: String = "",
        override val transactionType: TransactionType = TransactionType.UNKNOWN,
        val underlyingProductId: String = "",
        val isAdvancedOrder: Boolean = false,
        val isAMOption: Boolean = false,
        var originalSymbol: String = ""
    ) : OrderDetailDisplayItem(), OrderLegItemInterface

    data class OrderExtraLegItem(
        override val displaySymbol: String,
        override val displayQuantity: String,
        override val transactionType: TransactionType
    ) : OrderDetailDisplayItem(), OrderLegItemInterface

    object OrderSectionDividerItem : OrderDetailDisplayItem(), OrderDetailItemInterface

    data class OrderSectionItem(
        val data: List<TableSectionItemLayout>,
        val dividerVisibility: Int = View.VISIBLE
    ) : OrderDetailDisplayItem(), OrderDetailItemInterface

    sealed class OrderBidAskItem(
        @StringRes val bidLabel: Int = R.string.bid_title_text,
        @StringRes val askLabel: Int = R.string.ask_title_text
    ) : OrderDetailDisplayItem(), OrderDetailItemInterface, QuoteBidAskItem {

        class StaticOrderBidAskItem(
            bidLabel: Int = R.string.bid_title_text,
            askLabel: Int = R.string.ask_title_text,
            var bid: String = "",
            var bidSize: String = "",
            var ask: String = "",
            var askSize: String = ""
        ) : OrderBidAskItem(bidLabel, askLabel) {

            override val bidPrice: LiveData<QuotePriceWidgetItem> by lazy { createLiveData(bid, bidSize) }
            override val askPrice: LiveData<QuotePriceWidgetItem> by lazy { createLiveData(ask, askSize) }

            private fun createLiveData(price: String, size: String): LiveData<QuotePriceWidgetItem> {
                return MutableLiveData(QuotePriceWidgetItem(price = price, size = size))
            }
        }

        class DynamicOrderBidAskItem(quotesWidgetViewModel: QuotesWidgetViewModel) : OrderBidAskItem() {

            override val bidPrice: LiveData<QuotePriceWidgetItem>
            override val askPrice: LiveData<QuotePriceWidgetItem>

            init {
                quotesWidgetViewModel.quote.apply {
                    bidPrice = switchMap { it?.bidPrice ?: liveData { } }
                    askPrice = switchMap { it?.askPrice ?: liveData { } }
                }
            }
        }

        class DynamicOrderBidAskItemWithSymbol(val symbol: String) : OrderBidAskItem() {

            override var bidPrice: LiveData<QuotePriceWidgetItem> = MutableLiveData()
            override var askPrice: LiveData<QuotePriceWidgetItem> = MutableLiveData()

            fun updateWithViewModel(quotesWidgetViewModel: QuotesWidgetViewModel) {
                quotesWidgetViewModel.quote.apply {
                    bidPrice = switchMap { it?.bidPrice ?: liveData { } }
                    askPrice = switchMap { it?.askPrice ?: liveData { } }
                }
            }
        }

        class DelegatedOrderBidAskItem(item: QuoteBidAskItem) : OrderBidAskItem(), QuoteBidAskItem by item
    }
}

data class OrderOptionsRatio(
    val ratioLabel: LiveData<String>
) : OrderDetailDisplayItem(), OrderDetailItemInterface
