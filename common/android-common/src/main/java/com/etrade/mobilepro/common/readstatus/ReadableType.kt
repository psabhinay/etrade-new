package com.etrade.mobilepro.common.readstatus

import androidx.annotation.Keep

@Keep
enum class ReadableType {
    NEWS_ARTICLE,
    ALERT,
    PERSONAL_NOTIFICATION
}
