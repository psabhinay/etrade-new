package com.etrade.mobilepro.trade.api.mutualfunds

import com.etrade.mobilepro.trade.api.OrderPlaceResponse

interface MutualFundsOrderPlaceResponse : OrderPlaceResponse, MutualFundsOrderResponse {
    override val orders: List<ConfirmedMutualFundsOrder>
}
