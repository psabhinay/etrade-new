package com.etrade.mobilepro.bloombergtv.presentation.delegate.service

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v4.media.session.MediaSessionCompat
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.app.NotificationCompat
import androidx.media.app.NotificationCompat.MediaStyle
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvAudioService
import com.etrade.mobilepro.bloombergtv.presentation.R
import com.etrade.mobilepro.bloombergtv.presentation.common.ACTION_EXIT
import com.etrade.mobilepro.bloombergtv.presentation.common.ACTION_OPEN_IN_APP
import com.etrade.mobilepro.bloombergtv.presentation.common.ACTION_PLAY
import com.etrade.mobilepro.common.AppNotificationChannel
import com.etrade.mobilepro.common.createNotification

private const val ACTION_PLAY_VALUE = 0
private const val ACTION_OPEN_IN_APP_VALUE = 2
private const val ACTION_EXIT_VALUE = 3

internal class NotificationServiceDelegate {

    private lateinit var context: Context

    fun init(context: Context) {
        this.context = context
    }

    @Suppress("LongMethod")
    fun buildNotification(token: MediaSessionCompat.Token?, isPlaybackActive: Boolean) =
        with(createNotification(context, AppNotificationChannel.Silent)) {
            setContentTitle(context.getString(R.string.audio_mode_title))
            setSmallIcon(R.drawable.ic_hearing_white_24px)
            setContentIntent(playbackAction(ACTION_OPEN_IN_APP_VALUE, context))
            setDeleteIntent(playbackAction(ACTION_EXIT_VALUE, context))
            val playPauseIconRes =
                if (isPlaybackActive) {
                    android.R.drawable.ic_media_play
                } else {
                    android.R.drawable.ic_media_pause
                }
            addPlaybackAction(
                playPauseIconRes,
                R.string.audio_mode_play_or_pause_description,
                ACTION_PLAY_VALUE,
            )
            addPlaybackAction(
                R.drawable.ic_exit_to_app_white_24px,
                R.string.audio_mode_exit_to_app_description,
                ACTION_OPEN_IN_APP_VALUE,
            )
            addPlaybackAction(
                R.drawable.ic_clear_white_24px,
                R.string.audio_mode_close_description,
                ACTION_EXIT_VALUE,
            )
            setStyle(
                MediaStyle()
                    .setShowActionsInCompactView(0, 1, 2)
                    .setMediaSession(token)
            )
            build()
        }

    private fun NotificationCompat.Builder.addPlaybackAction(
        @DrawableRes iconRes: Int,
        @StringRes stringRes: Int,
        action: Int,
    ) = addAction(iconRes, context.getString(stringRes), playbackAction(action, context))

    private fun playbackAction(actionNumber: Int, context: Context): PendingIntent =
        with(Intent(context, BloombergTvAudioService::class.java)) {
            action = when (actionNumber) {
                ACTION_PLAY_VALUE -> ACTION_PLAY
                ACTION_OPEN_IN_APP_VALUE -> ACTION_OPEN_IN_APP
                ACTION_EXIT_VALUE -> ACTION_EXIT
                else -> ACTION_EXIT
            }
            PendingIntent.getService(context, actionNumber, this, PendingIntent.FLAG_IMMUTABLE)
        }
}
