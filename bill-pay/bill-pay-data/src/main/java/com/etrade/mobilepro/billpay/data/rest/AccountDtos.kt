package com.etrade.mobilepro.billpay.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
class AccountDto(
    @Json(name = "accountId")
    val accountId: String?,
    @Json(name = "instNo")
    val instNo: String?,
    @Json(name = "accountIdKey")
    val accountIdKey: String?,
    @Json(name = "maskedAccountId")
    val maskedAccountId: String?,
    @Json(name = "accountMode")
    val accountMode: String?,
    @Json(name = "accountDescription")
    val accountDescription: String?,
    @Json(name = "accountName")
    val accountName: String?,
    @Json(name = "accountShortName")
    val accountShortName: String?,
    @Json(name = "accountLongName")
    val accountLongName: String?,
    @Json(name = "mobileDescription")
    val mobileDescription: String?,
    @Json(name = "accountValue")
    val accountValue: String?,
    @Json(name = "accountType")
    val accountType: String?,
    @Json(name = "institutionType")
    val institutionType: String?,
    @Json(name = "accountStatus")
    val accountStatus: String?,
    @Json(name = "restrictionLevel")
    val restrictionLevel: String?,
    @Json(name = "closedDate")
    val closedDate: Long?,
    @Json(name = "cashTransferRestricitonCode")
    val cashTransferRestricitonCode: String?,
    @Json(name = "stockPlanOnly")
    val stockPlanOnly: Boolean,
    @Json(name = "brokerage")
    val brokerage: BrokerageDto?,
    @Json(name = "billPay")
    val billPay: BillPayDto?
)

@JsonClass(generateAdapter = true)
class BillPayDto(
    @Json(name = "isRestricted")
    val isRestricted: Boolean,
    @Json(name = "isEnrolled")
    val isEnrolled: Boolean,
    @Json(name = "isPrimary")
    val isPrimary: Boolean,
    @Json(name = "isEligible")
    val isEligible: Boolean
)

@JsonClass(generateAdapter = true)
class BrokerageDto(
    @Json(name = "cashAvailableForWithdrawal")
    val cashAvailableForWithdrawal: BigDecimal?,
    @Json(name = "marginAvailableForWithdrawal")
    val marginAvailableForWithdrawal: BigDecimal?,
    @Json(name = "purchasingPower")
    val purchasingPower: BigDecimal?,
    @Json(name = "totalAvailableForWithdrawal")
    val totalAvailableForWithdrawal: BigDecimal?,
    @Json(name = "marginBalance")
    val marginBalance: BigDecimal?,
    @Json(name = "netCashBalance")
    val netCashBalance: BigDecimal?,
    @Json(name = "cmAccount")
    val isCmAccount: Boolean?
)
