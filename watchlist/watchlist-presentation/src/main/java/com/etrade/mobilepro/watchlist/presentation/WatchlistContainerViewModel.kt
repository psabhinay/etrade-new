package com.etrade.mobilepro.watchlist.presentation

import android.content.res.Resources
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.ui.text.AnnotatedString
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.compose.etdialogs.edit.TextValueEditor
import com.etrade.mobilepro.compose.etdialogs.edit.create
import com.etrade.mobilepro.compose.etdialogs.elements.DialogButton
import com.etrade.mobilepro.compose.etdialogs.info.InfoDialogManager
import com.etrade.mobilepro.compose.etdialogs.info.create
import com.etrade.mobilepro.compose.etdialogs.info.hide
import com.etrade.mobilepro.etcompose.actionsheet.ActionSheetItem
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.WatchlistCRUDCallbacks
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import java.lang.ref.WeakReference
import javax.inject.Inject

private const val WL_NAME_MAX_LENGTH = 32
class WatchlistContainerViewModel @Inject constructor(
    private val resources: Resources,
    private val watchlistRepo: WatchlistRepo,
    private val userPreferences: UserPreferences,
    private val isAuth: UserAuthenticationState,
) : ViewModel() {

    private val _isBottomSheetVisible = MutableLiveData(false)
    val isBottomSheetVisible: LiveData<Boolean>
        get() = _isBottomSheetVisible

    internal val newWatchlistNameEditor = TextValueEditor.create(
        title = resources.getString(R.string.watchlist_create),
        hint = resources.getString(R.string.watchlist_name_hint),
        rules = TextValueEditor.Settings.Rules(
            excludedChars = setOf('"', '=', '<', '>', '[', ']', '!', '@', '#', '^', '&', '\'', ':', ';', ',', '?'),
            inputLength = WL_NAME_MAX_LENGTH,
        ),
        onDone = this::createWatchlist,
        onDismiss = { /* no op */ },
        validator = this::validateWatchlistName
    ) { _, onDone, onDismiss ->
        add(
            DialogButton(
                title = AnnotatedString(resources.getString(R.string.cancel)),
                onClick = onDismiss
            )
        )
        add(
            DialogButton(
                title = AnnotatedString(resources.getString(R.string.create)),
                onClick = onDone,
                isEnabled = { state -> state.value.text.isNotEmpty() && validateWatchlistName(state.value.text) == null }
            )
        )
    }

    internal val deleteWatchlistConfirmationDialog = InfoDialogManager.create(
        title = resources.getString(R.string.watchlist_remove),
        message = AnnotatedString(resources.getString(R.string.watchlist_remove_confirmation))
    ) { manager, onDismiss ->
        add(
            DialogButton(
                title = AnnotatedString(resources.getString(R.string.cancel)),
                onClick = onDismiss
            )
        )
        add(
            DialogButton(
                title = AnnotatedString(resources.getString(R.string.watchlist_remove)),
                onClick = {
                    val (id, _) = userPreferences.getRecentWatchlist()
                    id?.let {
                        deleteWatchlist(it)
                    }
                    manager.hide()
                }
            )
        )
    }

    private val compositeDisposable = CompositeDisposable()
    private var isWatchlistJustCreated: Boolean = false
    /**
     * Consumes a flag, therefore all subsequent calls to this method will return `false`
     *
     * @return `true` if a watchlist has been just created
     */
    fun consumeIsWatchlistJustCreated(): Boolean = isWatchlistJustCreated.also { isWatchlistJustCreated = false }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private var _crudCallbacks = WeakReference<WatchlistCRUDCallbacks?>(null)
    internal var crudCallbacks: WatchlistCRUDCallbacks?
        get() = _crudCallbacks.get()
        set(value) { _crudCallbacks = WeakReference(value) }

    /**
     * Creates a watchlist with this [name].
     *
     * @param name the name of new watchlist
     */
    @Suppress("LongMethod")
    fun createWatchlist(name: String) {
        crudCallbacks?.onStartCreating?.invoke()
        compositeDisposable.add(
            watchlistRepo.createWatchlist(name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = { newWatchList ->
                        isWatchlistJustCreated = true
                        val currentWatchlists = crudCallbacks?.currentWatchlists?.invoke() // mutableWatchLists.value
                        if (currentWatchlists != null) {
                            val newList: MutableList<Watchlist> = currentWatchlists.toMutableList()
                            // `(-insertion point - 1)`
                            val index = -newList.binarySearchBy(newWatchList.name, 2) { it.name } - 1
                            if (index >= 0) {
                                newList.add(index, newWatchList)
                            }
                            crudCallbacks?.onWatchlistCreated?.invoke(newWatchList, newList)
                            updateRecentWatchlist(newWatchList)
                        } else {
                            crudCallbacks?.onError?.invoke()
                        }
                    },
                    onError = {
                        crudCallbacks?.onError?.invoke()
                    }
                )
        )
    }

    fun deleteWatchlist(watchlistId: String) {
        compositeDisposable.add(
            watchlistRepo.deleteWatchList(watchlistId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = { isSuccessful ->
                        if (isSuccessful) {
                            crudCallbacks?.currentWatchlists?.invoke()?.toMutableList()?.let {
                                it.removeAll { watchlist -> watchlist.id == watchlistId }
                                val selectedWatchlist = it.first()
                                updateRecentWatchlist(selectedWatchlist)
                                crudCallbacks?.onWatchlistDeleted?.invoke(it)
                            }
                        } else {
                            crudCallbacks?.onError?.invoke()
                        }
                    },
                    onError = {
                        crudCallbacks?.onError?.invoke()
                    }
                )
        )
    }

    @ExperimentalMaterialApi
    internal fun actionSheetItems(
        onManage: () -> Unit,
        onCreateAuth: () -> Unit,
        onCreateUnAuth: () -> Unit,
        onDelete: (() -> Unit)?,
    ): List<ActionSheetItem> = mutableListOf(
        ActionSheetItem(resources.getString(R.string.watchlist_manage), onManage),
        ActionSheetItem(resources.getString(R.string.watchlist_create)) {
            if (isAuth()) {
                onCreateAuth()
            } else {
                onCreateUnAuth()
            }
        },
    ).apply {
        if (isAuth() && onDelete != null) {
            add(ActionSheetItem(resources.getString(R.string.watchlist_remove), onDelete))
        }
    }

    private fun updateRecentWatchlist(selectedWatchlist: Watchlist?) {
        userPreferences.setRecentWatchlist(selectedWatchlist?.id ?: DEVICE_WATCHLIST_ID, selectedWatchlist?.name)
    }

    fun setBottomSheetVisibility(it: Boolean) {
        _isBottomSheetVisible.value = it
    }

    private fun validateWatchlistName(name: String): AnnotatedString? {
        return if (crudCallbacks?.currentWatchlists?.invoke()?.firstOrNull { it.name?.equals(name, ignoreCase = true) == true } != null) {
            AnnotatedString(resources.getString(R.string.watchlist_error_duplicate_watchlist_name))
        } else {
            null
        }
    }
}
