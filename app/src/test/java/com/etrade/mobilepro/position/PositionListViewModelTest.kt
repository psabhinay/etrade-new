package com.etrade.mobilepro.position

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.orders.api.OrderAction
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.portfolio.PortfolioPositionsDisplayMode
import com.etrade.mobilepro.portfolio.PortfolioRecordsViewModel
import com.etrade.mobilepro.portfolio.PortfolioSettingsRepository
import com.etrade.mobilepro.portfolio.PortfolioTableMode
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.positions.PositionListViewModel
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.api.option.OptionTradeStrategy
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.etrade.mobilepro.util.json.BigDecimalAdapter
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.squareup.moshi.Moshi
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.robolectric.annotation.Config
import java.math.BigDecimal
import javax.inject.Provider

@RunWith(AndroidJUnit4::class)
@Config(application = Application::class)
class PositionListViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private fun mockDynamicScreenService(views: List<GenericLayout> = listOf()): DynamicScreenRepo {
        val viewMocks: Observable<Resource<List<GenericLayout>>> =
            Observable.just(Resource.Success(views))
        return mock {
            on { getScreen(any(), any(), anyOrNull()) } doReturn viewMocks
        }
    }

    private val darkModeChecker: DarkModeChecker = mock {
        on { uiMode } doReturn 1
    }

    private val moshi: Moshi = Moshi.Builder()
        .add(BigDecimalAdapter)
        .build()

    private val mockTradeFormParametersBuilder: TradeFormParametersBuilder = object : TradeFormParametersBuilder {
        override var orderId: String? = null
        override var originalQuantity: String? = null
        override var accountId: String? = null
        override var symbol: WithSymbolInfo? = null
        override var action: TradeAction? = null
        override var quantity: BigDecimal? = null
        override var priceType: PriceType? = null
        override var limitPrice: BigDecimal? = null
        override var offsetValue: BigDecimal? = null
        override var optionTradeStrategy: OptionTradeStrategy? = null
        override var fundToBuy: WithSymbolInfo? = null
        override var positionLotId: String? = null
        override var allOrNone: Boolean? = null
        override var advancedOrder: Boolean? = null
        override var advancedTradeType: AdvancedTradeType? = null
        override var walkthroughFormCompleted: String? = null
        override var stopPrice: BigDecimal? = null
        override var term: OrderTerm? = null

        override fun create(securityType: SecurityType): Map<String, String?> = emptyMap()
    }

    private val defaultTradePreferences = TradingDefaultsPreferences(
        mock(),
        mock {
            on { getStringValue(anyString(), anyOrNull()) } doReturn ("1")
        }
    )

    private val settingsRepository: PortfolioSettingsRepository = mock {
        on { tableMode } doReturn (PortfolioTableMode.STANDARD)
        on { positionsDisplayMode } doReturn (PortfolioPositionsDisplayMode.TABLE)
    }

    private val mockProvider: Provider<TradeFormParametersBuilder> =
        Provider { mockTradeFormParametersBuilder }

    private val msOptionRecord1 = getMockPortfolioRecord(
        "1",
        "MS----200717C00050000",
        "Jul 17 '20 \$50 Call",
        "MS Jul 17 '20 \$50 Call",
        BigDecimal.TEN,
        InstrumentType.OPTN,
        "MS",
        InstrumentType.EQ
    )

    private val msOptionRecord2 = getMockPortfolioRecord(
        "2",
        "MS----200717C00055000",
        "Jul 17 '20 \$55 Call",
        "MS Jul 17 '20 \$55 Call",
        BigDecimal.TEN,
        InstrumentType.OPTN,
        "MS",
        InstrumentType.EQ
    )

    private val msOptionRecord3 = getMockPortfolioRecord(
        "3",
        "MS----200612C00045500",
        "Jun 12 '20 \$45.50 Call",
        "MS Jun 12 '20 \$45.50 Call",
        BigDecimal.TEN.negate(),
        InstrumentType.OPTN,
        "MS",
        InstrumentType.EQ
    )

    private val aaplOptionRecord = getMockPortfolioRecord(
        "4",
        "AAPL--200918C00200000",
        "Sep 18 '20 $200 Call",
        "AAPL Sep 18 '20 \$200 Call",
        BigDecimal.ONE,
        InstrumentType.OPTN,
        "AAPL",
        InstrumentType.EQ
    )

    private class TestPositionListViewModel(
        dynamicScreenRepo: DynamicScreenRepo,
        moshi: Moshi,
        tradingDefaultsPreferences: TradingDefaultsPreferences,
        tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
        darkModeChecker: DarkModeChecker,
        portfolioSettingsRepository: PortfolioSettingsRepository
    ) : PositionListViewModel(
        dynamicScreenRepo,
        moshi,
        tradingDefaultsPreferences,
        darkModeChecker,
        tradeFormParametersBuilderProvider,
        portfolioSettingsRepository,
        PositionTableMode.PORTFOLIO_VIEW
    ) {
        override fun getScreenRequest(sortInfo: SortInfo): ScreenRequest {
            return ServicePath.Portfolio("12345", sortInfo.column, sortInfo.order)
        }
    }

    @Test
    fun `test open trade direction stock`() {
        val mockDynamicService = mockDynamicScreenService()
        val vm = TestPositionListViewModel(
            mockDynamicService,
            moshi,
            defaultTradePreferences,
            mockProvider,
            darkModeChecker,
            settingsRepository
        )
        val mockStockRecord =
            getMockPortfolioRecord("", "MS", "MS", "MS", BigDecimal.TEN, InstrumentType.EQ)
        vm.getTradeDirections(mockStockRecord, "12345", OrderAction.BUY_ACTION)
        mockTradeFormParametersBuilder.verifyParameterBuilderValues(
            "12345",
            "MS",
            InstrumentType.EQ,
            TradeAction.BUY,
            BigDecimal.ONE
        )
        vm.getTradeDirections(mockStockRecord, "123456", OrderAction.SELL_ACTION)
        mockTradeFormParametersBuilder.verifyParameterBuilderValues(
            "123456",
            "MS",
            InstrumentType.EQ,
            TradeAction.SELL,
            BigDecimal.TEN
        )
    }

    @Test
    fun `test open trade direction stock short position`() {
        val mockDynamicService = mockDynamicScreenService()
        val mockStockRecord = getMockPortfolioRecord("", "MS", "MS", "MS", BigDecimal.TEN.negate(), InstrumentType.EQ)
        val vm = TestPositionListViewModel(
            mockDynamicService,
            moshi,
            defaultTradePreferences,
            mockProvider,
            darkModeChecker,
            settingsRepository
        )
        vm.getTradeDirections(mockStockRecord, "12345", OrderAction.BUY_ACTION)
        mockTradeFormParametersBuilder.verifyParameterBuilderValues(
            "12345",
            "MS",
            InstrumentType.EQ,
            TradeAction.BUY_TO_COVER,
            BigDecimal.TEN
        )
        vm.getTradeDirections(mockStockRecord, "123456", OrderAction.SELL_ACTION)
        mockTradeFormParametersBuilder.verifyParameterBuilderValues(
            "123456",
            "MS",
            InstrumentType.EQ,
            TradeAction.SELL_SHORT,
            BigDecimal.ONE
        )
    }

    @Test
    fun `test open trade direction stock lots`() {
        val mockDynamicService = mockDynamicScreenService()
        val vm = TestPositionListViewModel(
            mockDynamicService,
            moshi,
            defaultTradePreferences,
            mockProvider,
            darkModeChecker,
            settingsRepository
        )
        val taxLotId = "1122334"
        val mockStockRecord = getMockPortfolioRecord(
            "1122334",
            "MS",
            "MS",
            "MS",
            BigDecimal.TEN,
            InstrumentType.EQ,
            isTaxLot = true
        )
        vm.getTradeDirections(mockStockRecord, "123456", OrderAction.SELL_ACTION)
        mockTradeFormParametersBuilder.verifyParameterBuilderValues(
            "123456",
            "MS",
            InstrumentType.EQ,
            TradeAction.SELL,
            BigDecimal.TEN,
            expectedPositionLotId = taxLotId
        )
    }

    @Test
    fun `test roll options direction`() {
        val mockDynamicService = mockDynamicScreenService()
        val vm = TestPositionListViewModel(
            mockDynamicService,
            moshi,
            defaultTradePreferences,
            mockProvider,
            darkModeChecker,
            settingsRepository
        )

        vm.getRollOptionsDirections("12345", listOf(msOptionRecord1))
        mockTradeFormParametersBuilder.verifyParameterBuilderValues(
            "12345",
            "MS",
            InstrumentType.EQ,
            null,
            null,
            null,
            object : OptionTradeStrategy {
                override val strategyType: StrategyType = StrategyType.CALL
                override val optionLegs: List<TradeLeg>
                    get() = listOf(
                        TestTradeLeg(
                            msOptionRecord1.instrument.symbol,
                            TransactionType.SELL_CLOSE_OPT,
                            10,
                            msOptionRecord1.instrument.displaySymbol,
                            false
                        )
                    )
            }
        )

        vm.getRollOptionsDirections("12345", listOf(msOptionRecord3))
        mockTradeFormParametersBuilder.verifyParameterBuilderValues(
            "12345",
            "MS",
            InstrumentType.EQ,
            null,
            null,
            null,
            object : OptionTradeStrategy {
                override val strategyType: StrategyType = StrategyType.CALL
                override val optionLegs: List<TradeLeg>
                    get() = listOf(
                        TestTradeLeg(
                            msOptionRecord3.instrument.symbol,
                            TransactionType.BUY_CLOSE_OPT,
                            10,
                            msOptionRecord3.instrument.displaySymbol,
                            false
                        )
                    )
            }
        )
    }

    @Test
    fun `test roll two options`() {
        val mockDynamicService = mockDynamicScreenService()
        val mockStockRecord = getMockPortfolioRecord("", "MS", "MS", "MS", BigDecimal.TEN, InstrumentType.EQ)
        val vm = TestPositionListViewModel(
            mockDynamicService,
            moshi,
            defaultTradePreferences,
            mockProvider,
            darkModeChecker,
            settingsRepository
        )

        val mockedRecordViewModel: PortfolioRecordsViewModel = mock {
            on { records } doReturn MutableLiveData(listOf(msOptionRecord1, msOptionRecord2, msOptionRecord3, aaplOptionRecord, mockStockRecord))
            on { peekRecord(any()) } doReturn msOptionRecord1
            on { accountId } doReturn "12345"
            on { findOptionsPositions(anyOrNull()) } doReturn listOf(msOptionRecord1, msOptionRecord2, msOptionRecord3)
        }

        vm.rollTwoOptions(mockedRecordViewModel, 0)

        assertEquals(3, vm.optionsToRoll.value?.size)
        assertTrue(vm.optionsToRoll.value?.all { it.value.instrument.underlyingSymbolInfo?.symbol == "MS" } == true)

        // Test no selections made, direction is null
        assertNull(vm.getRollTwoOptionsDirections())

        vm.toggleSelectionOptionToRoll(msOptionRecord2)
        vm.toggleSelectionOptionToRoll(msOptionRecord3)

        vm.getRollTwoOptionsDirections()
        mockTradeFormParametersBuilder.verifyParameterBuilderValues(
            "12345",
            "MS",
            InstrumentType.EQ,
            null,
            null,
            null,
            object : OptionTradeStrategy {
                override val strategyType: StrategyType = StrategyType.UNKNOWN
                override val optionLegs: List<TradeLeg>
                    get() = listOf(
                        TestTradeLeg(
                            msOptionRecord2.instrument.symbol,
                            TransactionType.SELL_CLOSE_OPT,
                            10,
                            msOptionRecord2.instrument.displaySymbol,
                            false
                        ),
                        TestTradeLeg(
                            msOptionRecord3.instrument.symbol,
                            TransactionType.BUY_CLOSE_OPT,
                            10,
                            msOptionRecord3.instrument.displaySymbol,
                            false
                        )
                    )
            }
        )
    }

    private fun TradeFormParametersBuilder.verifyParameterBuilderValues(
        expectedAccountId: String? = null,
        expectedSymbol: String? = null,
        expectedInstrumentType: InstrumentType? = null,
        expectedAction: TradeAction? = null,
        expectedQuantity: BigDecimal? = null,
        expectedPriceType: PriceType? = null,
        expectedOptionTradeStrategy: OptionTradeStrategy? = null,
        expectedPositionLotId: String? = null
    ) {
        assertEquals(expectedAccountId, accountId)
        assertEquals(expectedSymbol, symbol?.symbol)
        assertEquals(expectedInstrumentType, symbol?.instrumentType)
        assertEquals(expectedAction, action)
        assertEquals(expectedQuantity, quantity)
        assertEquals(expectedPriceType, priceType)
        assertEquals(expectedPositionLotId, positionLotId)

        val localOptionTradeStrategy = optionTradeStrategy

        if (expectedOptionTradeStrategy != null && localOptionTradeStrategy != null) {
            assertEquals(expectedOptionTradeStrategy.strategyType, localOptionTradeStrategy.strategyType)
            expectedOptionTradeStrategy.optionLegs.zip(localOptionTradeStrategy.optionLegs).forEach {
                assertEquals(it.first.symbol, it.second.symbol)
                assertEquals(it.first.transactionType, it.second.transactionType)
                assertEquals(it.first.quantity, it.second.quantity)
                assertEquals(it.first.displaySymbol, it.second.displaySymbol)
            }
        }
    }

    private data class TestTradeLeg(
        override val symbol: String,
        override val transactionType: TransactionType,
        override val quantity: Int,
        override val displaySymbol: String,
        override val isAMOption: Boolean
    ) : TradeLeg

    private fun getMockPortfolioRecord(
        recordId: String,
        symbol: String,
        symbolDescription: String,
        displaySymbol: String,
        quantity: BigDecimal,
        instrumentType: InstrumentType,
        underlyingSymbol: String? = null,
        underlyingInstrumentType: InstrumentType? = null,
        isTaxLot: Boolean = false
    ): PortfolioRecord = object : PortfolioRecord {
        override val recordId: String = recordId
        override val displayName: String = symbol
        override val isTaxLot: Boolean = isTaxLot
        override val position: Position = object : Position {
            override val positionId: String = ""
            override val symbol: String = symbol
            override val daysGain: BigDecimal? = null
            override val totalGain: BigDecimal? = null
            override val pricePaid: BigDecimal? = null
            override val commission: BigDecimal? = null
            override val fees: BigDecimal? = null
            override val quantity: BigDecimal? = quantity
            override val displayQuantity: BigDecimal? = quantity
            override val daysGainPercentage: BigDecimal? = null
            override val totalGainPercentage: BigDecimal? = null
            override val todayClose: BigDecimal? = null
            override val baseSymbolAndPrice: String? = null
            override val mark: BigDecimal? = null
            override val marketValue: BigDecimal? = null
            override val inTheMoneyFlag: Boolean? = null
        }
        override val instrument: Instrument = object : Instrument {
            override val displaySymbol: String = displaySymbol
            override val ask: BigDecimal? = null
            override val bid: BigDecimal? = null
            override val priceEarningRation: BigDecimal? = null
            override val earningsPerShare: BigDecimal? = null
            override val fiftyTwoWeekHigh: BigDecimal? = null
            override val fiftyTwoWeekLow: BigDecimal? = null
            override val lastPrice: BigDecimal? = null
            override val tickIndicator: Short? = null
            override val marketCap: BigDecimal? = null
            override val volume: BigDecimal? = null
            override val daysChange: BigDecimal? = null
            override val daysChangePercentage: BigDecimal? = null
            override val delta: BigDecimal? = null
            override val gamma: BigDecimal? = null
            override val theta: BigDecimal? = null
            override val vega: BigDecimal? = null
            override val impliedVolatilityPercentage: BigDecimal? = null
            override val daysToExpire: Int? = null
            override val underlyingSymbolInfo: WithSymbolInfo? = createUnderlyingSymbolInfo(underlyingSymbol, underlyingInstrumentType)
            override val previousClose: BigDecimal = BigDecimal.TEN
            override val symbol: String = symbol
            override val symbolDescription = symbolDescription
            override val instrumentType: InstrumentType = instrumentType
        }
    }
}

internal fun createUnderlyingSymbolInfo(underlyingSymbol: String?, underlyingInstrumentType: InstrumentType?): WithSymbolInfo? {
    val actualSymbol = underlyingSymbol ?: return null
    return object : WithSymbolInfo {
        override val symbol: String = actualSymbol
        override val instrumentType: InstrumentType = underlyingInstrumentType ?: InstrumentType.EQ
    }
}
