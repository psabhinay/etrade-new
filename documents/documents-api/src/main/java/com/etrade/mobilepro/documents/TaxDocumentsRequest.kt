package com.etrade.mobilepro.documents

data class TaxDocumentsRequest(
    val accountUuid: String,
    val year: Int
)
