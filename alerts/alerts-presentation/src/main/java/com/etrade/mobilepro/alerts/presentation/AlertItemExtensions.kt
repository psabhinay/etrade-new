package com.etrade.mobilepro.alerts.presentation

import android.os.Parcelable
import com.etrade.mobilepro.alerts.api.model.AlertGroup
import com.etrade.mobilepro.alerts.api.model.AlertItem
import kotlinx.parcelize.Parcelize

@Parcelize
data class AlertParcelableContent(
    val id: String,
    val subject: String,
    val occurredAtFormatted: String,
    val content: String,
    val group: AlertGroup? = null
) : Parcelable

fun AlertItem.Alert.withoutContentParcelable(): AlertParcelableContent =
    AlertParcelableContent(
        id = this.id,
        subject = this.subject,
        occurredAtFormatted = this.occurredAtFormatted,
        content = "",
        group = this.belongsTo()
    )
