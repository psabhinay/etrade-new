package com.etrade.mobilepro.dynamic.form.api

import com.etrade.mobilepro.util.WithContentDescription

data class DropDownOption(
    val id: String,
    val name: String,
    val description: String = "",
    override val contentDescription: String = name
) : WithContentDescription {
    override fun toString(): String = name
}
