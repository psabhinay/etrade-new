package com.etrade.mobilepro.login.presentation

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.biometric.api.BiometricsProvider
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticationDelegate
import com.etrade.mobilepro.biometric.presentation.BiometricPromptType
import com.etrade.mobilepro.biometric.presentation.R
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.dialog.viewmodel.AppDialogViewModel
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.encryption.viewmodel.CipherAuthenticatorViewModel
import com.etrade.mobilepro.login.api.repo.LoginFailure
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.login.api.repo.LoginResultCode
import com.etrade.mobilepro.login.api.repo.LoginSuccess
import com.etrade.mobilepro.login.presentation.cta.LoginCtaAction
import com.etrade.mobilepro.login.presentation.cta.LoginCtaItem
import com.etrade.mobilepro.login.presentation.cta.LoginViewProvider
import com.etrade.mobilepro.login.usecase.LogInUseCase
import com.etrade.mobilepro.login.usecase.LoginRequest
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.timeout.state.TimeoutState
import com.etrade.mobilepro.tracking.SCREEN_TITLE_FORGOT_PASSWORD
import com.etrade.mobilepro.tracking.SCREEN_TITLE_FORGOT_USER_ID
import com.etrade.mobilepro.tracking.SCREEN_TITLE_PRIVACY_POLICY
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.crypto.Cipher
import kotlin.coroutines.resume

const val MESSAGE_LOGIN_ACTION = 1
const val NO_SELECTION = -1

internal typealias BiometricCipherAuthenticator = (cipher: Cipher, promptType: BiometricPromptType, isUserInitiated: Boolean) -> Unit
internal typealias SecurityIdProvider = () -> Unit

class LoginViewModel(
    layoutId: Int,
    val loginViewProvider: LoginViewProvider,
    val biometricAuthenticationDelegate: BiometricAuthenticationDelegate,
    private val biometricsProvider: BiometricsProvider,
    private val cipherAuthenticatorViewModelDelegate: CipherAuthenticatorViewModel,
    private val loginPreferences: LoginPreferencesRepo,
    private val logInUseCase: LogInUseCase,
    private val resources: Resources,
    private val timeoutState: TimeoutState,
    private val preferences: ApplicationPreferences,
    private val tracker: Tracker
) : DynamicViewModel(layoutId), AppDialogViewModel, CipherAuthenticatorViewModel by cipherAuthenticatorViewModelDelegate {

    override val variableId: Int by lazy { BR.obj }

    private val _dialog: MutableLiveData<ConsumableLiveEvent<AppDialog?>?> = MutableLiveData()
    override val dialog: LiveData<ConsumableLiveEvent<AppDialog?>?> = _dialog

    private val _emptyUserName = MutableLiveData(true)
    private val _emptyPassword = MutableLiveData(true)
    private val _emptySecurityCode = MutableLiveData(true)

    val emptyUserName: LiveData<Boolean>
        get() = _emptyUserName

    val emptyPassword: LiveData<Boolean>
        get() = _emptyPassword

    val emptySecurityCode: LiveData<Boolean>
        get() = _emptySecurityCode

    private val _loginViewState = MutableLiveData<ConsumableLiveEvent<ViewState>>()
    val loginViewState: LiveData<ConsumableLiveEvent<ViewState>>
        get() = _loginViewState

    val isBiometricLoginEnabled: Boolean
        get() = loginPreferences.isBiometricAuthenticationEnabled && biometricsProvider.canAuthenticate().isSuccessful

    var shouldShowBiometricAuthentication: Boolean
        get() = timeoutState.shouldShowBiometricAuthentication
        set(value) {
            timeoutState.shouldShowBiometricAuthentication = value
        }

    var retainedUsername = ""
    var retainedPassword = ""
    var retainedSecurityCode = ""

    internal var usernameSelectionStart = NO_SELECTION
    internal var usernameSelectionEnd = NO_SELECTION

    internal var hidePasswordState = initHidePasswordState()
        set(value) {
            loginPreferences.isPasswordHidden = value
            field = value
        }

    internal var passwordSelectionStart = NO_SELECTION
    internal var passwordSelectionEnd = NO_SELECTION

    private var secureIdContinuation: CancellableContinuation<String>? = null

    private var loginJob: Job? = null

    private fun initHidePasswordState(): Boolean {
        return if (loginPreferences.isHidePasswordEnabled) {
            loginPreferences.isPasswordHidden
        } else {
            true.also { loginPreferences.isHidePasswordEnabled = it }
        }
    }

    internal fun onCtaAction(item: LoginCtaItem) {
        clickEvents.postValue(CtaEvent.MessageEvent(item.getEventMessage()))
        when (item.action) {
            LoginCtaAction.FORGOT_USERNAME -> trackForgotUserId()
            LoginCtaAction.FORGOT_PASSWORD -> trackForgotPassword()
            LoginCtaAction.PRIVACY_POLICY -> trackPrivacyPolicy()
            else -> Unit
        }
    }

    @Suppress("LongParameterList", "LongMethod")
    internal fun login(
        username: String,
        password: String,
        loginParameter: LoginParameter,
        securityCode: String? = null,
        isSecurityCodeEnabled: Boolean = false,
        isBiometricLogin: Boolean = false
    ) {
        if (isInputValidAndHandleEmpty(username, password).not()) {
            return
        }
        val securePassword = if (isBiometricLogin) {
            password
        } else {
            createSecurityPassword(password, securityCode, isSecurityCodeEnabled)
        }
        securePassword?.let {
            _loginViewState.value = ConsumableLiveEvent(ViewState.Loading)
            viewModelScope.launch {
                val shouldOfferBiometric = loginPreferences.isFirstLogin && biometricsProvider.canAuthenticate().isSuccessful
                val request = LoginRequest(username, securePassword, loginParameter.shouldHandleLanding, shouldOfferBiometric) { cipher ->
                    authenticate(cipher, loginParameter.isUserInitiated, shouldOfferBiometric, loginParameter.authenticator)
                }
                when (val result = logInUseCase.execute(request)) {
                    is LoginSuccess -> onLoginSuccess(result)
                    is LoginFailure -> onLoginFailure(result, isBiometricLogin)
                    else -> {
                        _loginViewState.value = ConsumableLiveEvent(ViewState.Error())
                    }
                }
            }
        }
    }

    private fun createSecurityPassword(password: String, securityCode: String?, isSecurityCodeEnabled: Boolean): String? {
        if (isSecurityCodeEnabled.not()) {
            return password
        }
        return if (securityCode.isNullOrEmpty()) {
            _emptySecurityCode.value = true
            null
        } else {
            password.plus(securityCode)
        }
    }

    private fun onLoginSuccess(result: LoginSuccess) {
        loginPreferences.apply {
            isRsaUser = result.isRsaUser
            registerLoginEvent()
        }
        _loginViewState.value = ConsumableLiveEvent(ViewState.Success(result.sessionResultMessage))
        preferences.shouldSyncChartIqData = true
    }

    private fun onLoginFailure(result: LoginFailure, isBiometricLogin: Boolean) {
        _loginViewState.value = handleLoginFailure(isBiometricLogin, result)
    }

    private fun handleLoginFailure(isBiometricLogin: Boolean, result: LoginFailure):
        ConsumableLiveEvent<ViewState> {
        // If we are in biometric login and fail we should
        // request the password again
        return if (isBiometricLogin && result.loginResultCode == LoginResultCode.INVALID_CREDENTIALS) {
            ConsumableLiveEvent(ViewState.Error(LoginResultCode.INVALID_STORED_PASSWORD))
        } else {
            ConsumableLiveEvent(
                ViewState.Error(
                    result.loginResultCode,
                    result.sessionResultMessage
                )
            )
        }
    }

    internal fun login(
        password: String? = null,
        loginParameter: LoginParameter,
        securityIdProvider: SecurityIdProvider
    ) {
        cancelSecurityId()
        loginJob?.cancel()

        loginJob = viewModelScope.launch {
            runCatching {
                try {
                    loginWithStoredCredentials(password, loginParameter, securityIdProvider)
                } catch (e: CancellationException) {
                    logger.debug("Login flow got cancelled by user.")
                } finally {
                    secureIdContinuation = null
                }
            }.onFailure {
                // when it is AuthenticationFailecException a proper message is already displayed we don't need an additional message
                if (it !is AuthenticationFailedException) {
                    _loginViewState.value = ConsumableLiveEvent(ViewState.Error())
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        loginJob?.cancel()
        loginJob = null
    }

    private suspend fun loginWithStoredCredentials(password: String? = null, loginParameter: LoginParameter, securityIdProvider: SecurityIdProvider) {
        val storedUsername = loginPreferences.getUserName().getOrThrow()
        val passwordAndSecurityId = password ?: loginPreferences.getUserPassword { cipher ->
            authenticate(cipher, loginParameter.isUserInitiated, false, loginParameter.authenticator)
        }.getOrThrow() + getSecurityIdIfNeeded(securityIdProvider)
        login(
            username = storedUsername,
            password = passwordAndSecurityId,
            loginParameter = loginParameter,
            isBiometricLogin = password == null
        )
    }

    fun resumeLoginWith(securityId: String) {
        withSecurityId {
            resume(securityId)
        }
    }

    fun cancelSecurityId() {
        withSecurityId {
            cancel()
        }
    }

    private fun withSecurityId(perform: CancellableContinuation<String>.() -> Unit) {
        secureIdContinuation?.apply {
            secureIdContinuation = null
            perform()
        }
    }

    private suspend fun authenticate(
        cipher: Cipher,
        isUserInitiated: Boolean,
        shouldOfferBiometric: Boolean,
        authenticator: BiometricCipherAuthenticator
    ): Cipher {
        return cipherAuthenticatorViewModelDelegate.authenticate(cipher) {
            if (shouldOfferBiometric) {
                offerBiometricAuthentication(cipher, isUserInitiated, authenticator)
            } else {
                authenticator(cipher, BiometricPromptType.AUTHENTICATE, isUserInitiated)
            }
        }
    }

    private fun offerBiometricAuthentication(cipher: Cipher, isUserInitiated: Boolean, authenticator: BiometricCipherAuthenticator) {
        _dialog.value = resources.appDialog {
            titleRes = R.string.biometric_authentication
            messageRes = R.string.enable_biometrics_dialog_message
            resourcesPositiveAction {
                labelRes = R.string.enable
                action = {
                    _dialog.value = null
                    authenticator(cipher, BiometricPromptType.REGISTER, isUserInitiated)
                }
            }
            resourcesNegativeAction {
                labelRes = R.string.enable_biometrics_dialog_negative
                action = {
                    _dialog.value = null
                    cipherAuthenticatorViewModelDelegate.onCipherAuthenticationCancelled()
                }
            }
        }.consumable()
    }

    private suspend fun getSecurityIdIfNeeded(requestSecurityId: SecurityIdProvider): String {
        return if (loginPreferences.isRsaUser) {
            suspendCancellableCoroutine { continuation ->
                secureIdContinuation = continuation
                requestSecurityId()
            }
        } else {
            ""
        }
    }

    private fun isInputValidAndHandleEmpty(
        username: String,
        password: String
    ): Boolean {
        val isValidUserName = username.isBlank().not()
        _emptyUserName.value = isValidUserName

        val isValidPassword = password.isBlank().not()
        _emptyPassword.value = isValidPassword

        return isValidUserName.and(isValidPassword)
    }

    fun initViewState() {
        _emptyUserName.value = true
        _emptyPassword.value = true
        _emptySecurityCode.value = false
    }

    internal fun setHidePasswordEnabled(isEnabled: Boolean) {
        loginPreferences.isHidePasswordEnabled = isEnabled
    }

    internal fun clearUsernameSelection() {
        usernameSelectionStart = NO_SELECTION
        usernameSelectionEnd = NO_SELECTION
    }

    internal fun clearPasswordSelection() {
        passwordSelectionStart = NO_SELECTION
        passwordSelectionEnd = NO_SELECTION
    }

    private fun trackForgotUserId() {
        tracker.screen(SCREEN_TITLE_FORGOT_USER_ID)
    }

    private fun trackForgotPassword() {
        tracker.screen(SCREEN_TITLE_FORGOT_PASSWORD)
    }

    private fun trackPrivacyPolicy() {
        tracker.screen(SCREEN_TITLE_PRIVACY_POLICY)
    }

    sealed class ViewState {
        object Loading : ViewState()

        data class Error(val loginErrorCode: LoginResultCode = LoginResultCode.UNKNOWN, val message: String? = null) : ViewState()

        data class Success(val message: String? = null) : ViewState()
    }
}
