package com.etrade.mobilepro.bank

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.bank.databinding.BankHomeFragmentBinding
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.dynamicui.DynamicUiLayoutRecyclerAdapter
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicProperty
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.toDynamicViewModels
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.etrade.mobilepro.util.android.recyclerviewutil.CustomRecyclerViewItemDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import org.threeten.bp.LocalDate
import java.util.Collections
import javax.inject.Inject

@RequireLogin
class BankHomeFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val darkModeChecker: DarkModeChecker
) : Fragment(R.layout.bank_home_fragment) {

    private val binding by viewBinding(BankHomeFragmentBinding::bind)

    private val args: BankHomeFragmentArgs by navArgs()

    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    private var snackBar: Snackbar? = null

    private val bankHomeViewModel: BankHomeViewModel by navGraphViewModels(R.id.bank_nav_graph) { viewModelFactory }

    private val viewAdapter: DynamicUiLayoutRecyclerAdapter
        get() = binding.bankHomeRecyclerView.adapter as DynamicUiLayoutRecyclerAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.pullToRefresh.setOnRefreshListener {
            viewModelStore.clear()
            fetchScreen(forceRefresh = true)
        }
        binding.bankHomeRecyclerView.apply {
            adapter = DynamicUiLayoutRecyclerAdapter(viewLifecycleOwner, emptyList())
            ContextCompat.getDrawable(requireContext(), R.drawable.list_item_divider_light_grey)
                ?.let { separator ->
                    val decorator = CustomRecyclerViewItemDivider(
                        drawable = separator,
                        positions = Collections.singletonList(1)
                    )
                    addItemDecoration(decorator)
                }
        }
        bindViewModel()
        fetchScreen()
    }

    @Suppress("LongMethod")
    private fun bindViewModel() {
        bankHomeViewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                binding.apply {
                    when (it) {
                        is BankHomeViewModel.ViewState.Loading -> {
                            bankHomeLoadingIndicator.show()
                        }
                        is BankHomeViewModel.ViewState.Error -> {
                            bankHomeLoadingIndicator.hide()
                            pullToRefresh.isRefreshing = false
                            snackBar =
                                snackBarUtil.retrySnackbar { fetchScreen(forceRefresh = true) }
                        }
                        is BankHomeViewModel.ViewState.Success -> {
                            if (darkModeChecker.uiMode != it.uiMode) {
                                fetchScreen(forceRefresh = true)
                            } else {
                                bankHomeLoadingIndicator.hide()
                                snackBar?.dismiss()
                                pullToRefresh.isRefreshing = false
                            }
                        }
                    }
                }
            }
        )
        bankHomeViewModel.widgets.observe(
            viewLifecycleOwner,
            Observer {
                it.forEach { layout ->
                    // we want to hide account name to avoid duplication with the dropdown in the toolbar
                    (layout as? DynamicProperty)?.shouldHide = true
                }
                viewAdapter.values = it.toDynamicViewModels(
                    this,
                    shouldRefresh = true
                ) { viewModel ->
                    observeWidgetEvents(viewModel)
                }
            }
        )

        bankHomeViewModel.dateRangeSelection.observe(
            viewLifecycleOwner,
            Observer {
                it?.consume { dateRange ->
                    loadBankTransactions(
                        startDate = dateRange.first,
                        endDate = dateRange.second
                    )
                }
            }
        )
    }

    private fun loadBankTransactions(startDate: LocalDate, endDate: LocalDate) {
        val safeAccountId =
            requireNotNull(args.bankAccountId.toLongOrNull()) { "AccountId can not be null, was null" }
        bankHomeViewModel.getTransactions(
            accountId = safeAccountId,
            startDate = startDate,
            endDate = endDate
        )
    }

    private fun observeWidgetEvents(viewModel: DynamicViewModel) {
        viewModel.clickEvents.observe(
            viewLifecycleOwner,
            Observer { ctaEvent ->
                when (ctaEvent) {
                    is CtaEvent.RetryEvent -> {
                        snackBar = snackBarUtil.retrySnackbar {
                            // retry balances service call
                            viewModel.onCtaClicked(null)
                        }
                    }
                    is CtaEvent.NavDirectionsNavigation -> {
                        findNavController().navigate(ctaEvent.ctaDirections)
                    }
                    is CtaEvent.UriEvent -> {
                        findNavController().navigate(ctaEvent.uri)
                    }
                }
            }
        )
    }

    private fun fetchScreen(forceRefresh: Boolean = false) {
        args.accountUuid.let { accountUuId ->
            bankHomeViewModel.fetchScreen(
                args.bankAccountId,
                accountUuId,
                forceRefresh = forceRefresh
            )

            if (forceRefresh) {
                bankHomeViewModel.dateRangeSelection.value?.peekContent()?.let {
                    loadBankTransactions(it.first, it.second)
                }
            }
        }
    }
}
