package com.etrade.mobilepro.trade.dropdown.help

import com.etrade.mobilepro.trade.api.TradeOptionInfoType

data class TradeHelpParams(
    val infoType: TradeOptionInfoType
)
