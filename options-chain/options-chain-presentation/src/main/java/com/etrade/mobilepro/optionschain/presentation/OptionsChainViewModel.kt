package com.etrade.mobilepro.optionschain.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.optionschains.api.Option
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.trade.optionlegselect.OptionTradeActionItemProvider
import com.etrade.mobilepro.trade.optionlegselect.TradeActionItemProvider
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetViewModel
import com.etrade.mobilepro.user.session.manager.SessionState
import com.etrade.mobilepro.user.session.manager.SessionStateChangeListener
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

class OptionsChainViewModel @Inject constructor(
    private val optionChainActionProvider: OptionChainActionProvider,
    @OptionTradeActionItemProvider private val optionTradeActionItemProvider: TradeActionItemProvider,
    private val sessionState: SessionState
) : ViewModel(), ActionSheetViewModel, SessionStateChangeListener {

    private val actionState: MutableLiveData<ActionState> = MutableLiveData()
    private val _navEvent: MutableLiveData<ConsumableLiveEvent<NavInfo>> = MutableLiveData()
    private val _navToQuoteDetailsSignal: MutableLiveData<ConsumableLiveEvent<SearchResultItem.Symbol>> = MutableLiveData()
    private var _pendingNavToQuote: NavInfo? = null
    private var instrumentType: InstrumentType = InstrumentType.UNKNOWN
    private var optionType: OptionChainType? = null
    private var selectedOption: Option? = null

    init {
        sessionState.addListener(this)
    }

    public override fun onCleared() {
        sessionState.removeListener(this)
    }

    override val actions: LiveData<List<ActionItem>> = actionState.map { state: ActionState ->
        when (state) {
            ActionState.IDLE -> emptyList()
            ActionState.OPTION_CHAIN -> optionChainActionProvider.getActions(instrumentType)
            ActionState.OPTION_TRADE -> optionTradeActionItemProvider.getActions()
        }
    }

    internal lateinit var disclosureViewModel: OptionsChainConditionalOrderDisclosureViewModel

    val navEvent: LiveData<ConsumableLiveEvent<NavInfo>> get() = _navEvent
    val navToQuoteDetailsSignal: LiveData<ConsumableLiveEvent<SearchResultItem.Symbol>> get() = _navToQuoteDetailsSignal

    override fun selectAction(item: ActionItem) {
        when (actionState.value) {
            ActionState.OPTION_CHAIN -> onActionOptionChain(item)
            ActionState.OPTION_TRADE -> onActionOptionTrade(item)
            ActionState.IDLE, null -> {
                throw IllegalStateException("Invalid action state:${actionState.value}")
            }
        }
    }

    private fun onActionOptionTrade(item: ActionItem) {
        selectedOption?.let { option ->
            if (optionType == OptionChainType.TRADE_CONDITIONAL) {
                actionState.value = ActionState.IDLE
                disclosureViewModel.onConditionalOrderSelected { isSigned ->
                    if (isSigned) {
                        navigateToTradeScreen(option, item)
                    } else {
                        cancelActions()
                    }
                }
            } else {
                navigateToTradeScreen(option, item)
            }
        } ?: cancelActions()
    }

    private fun navigateToTradeScreen(selectedOption: Option, item: ActionItem) {
        _navEvent.value = ConsumableLiveEvent(
            NavInfo(
                selectedOption,
                instrumentType,
                optionType,
                optionTradeActionItemProvider.getTransactionType(item.id)
            )
        )
        cancelActions()
    }

    private fun onActionOptionChain(item: ActionItem) {
        optionType = optionChainActionProvider.getOptionChainType(item.id)
        if (optionType == OptionChainType.QUOTE_DETAILS) {
            selectedOption?.let {
                _navToQuoteDetailsSignal.value = ConsumableLiveEvent(
                    SearchResultItem.Symbol(
                        title = it.symbol,
                        type = InstrumentType.OPTN,
                        displaySymbol = it.displaySymbol
                    )
                )
            }
            cancelActions()
        } else {
            actionState.value = ActionState.OPTION_TRADE
        }
    }

    override fun cancelActions() {
        selectedOption = null
        optionType = null
        instrumentType = InstrumentType.UNKNOWN
        actionState.value = ActionState.IDLE
    }

    fun selectOption(option: Option, type: InstrumentType) {
        instrumentType = type
        selectedOption = option
        actionState.value = ActionState.OPTION_CHAIN
    }

    fun savePendingNavInfo(navInfo: NavInfo) {
        _pendingNavToQuote = navInfo
    }

    private enum class ActionState {
        IDLE,
        OPTION_CHAIN,
        OPTION_TRADE
    }

    data class NavInfo(
        val option: Option,
        val instrumentType: InstrumentType,
        val optionChainType: OptionChainType?,
        val transactionType: TransactionType?
    )

    override fun onSessionStateChange(state: SessionStateChange) {
        when (state) {
            SessionStateChange.AUTHENTICATED -> {
                _pendingNavToQuote?.let { _navEvent.value = ConsumableLiveEvent(it) }
            }
        }
    }
}
