package com.etrade.mobilepro.marketscalendar.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.marketscalendar.adapter.holder.EarningsEventDetailsViewHolder
import com.etrade.mobilepro.marketscalendar.data.dto.EARNINGS_EVENT
import com.etrade.mobilepro.marketscalendar.databinding.AdapterEarningsDetailsBinding
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class EarningsEventDetailsAdapterDelegate(
    private val noValue: String,
    private val navigation: () -> Unit
) : AbsListItemAdapterDelegate<EventDetails, EventDetails, EarningsEventDetailsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): EarningsEventDetailsViewHolder {
        return EarningsEventDetailsViewHolder(
            parent.viewBinding(AdapterEarningsDetailsBinding::inflate),
            noValue,
            navigation
        )
    }

    override fun isForViewType(item: EventDetails, items: MutableList<EventDetails>, position: Int): Boolean = item.eventType == EARNINGS_EVENT

    override fun onBindViewHolder(item: EventDetails, holder: EarningsEventDetailsViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }
}
