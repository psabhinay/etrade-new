package com.etrade.mobilepro.chartiq.api.model

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import java.math.BigDecimal

data class OrderDetails(
    val accountId: String,
    val orderNumber: String,
    val symbol: String,
    val instrumentType: InstrumentType,
    val sharesAmount: BigDecimal?,
    val transactionType: TransactionType,
    val priceType: PriceType,
    val limitPrice: BigDecimal?,
    val stopPrice: BigDecimal?
)
