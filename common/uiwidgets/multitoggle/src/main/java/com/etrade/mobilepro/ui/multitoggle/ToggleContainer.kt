package com.etrade.mobilepro.ui.multitoggle

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import androidx.core.view.children

internal class ToggleContainer constructor(
    context: Context,
    attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {

    init {
        orientation = HORIZONTAL
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val viewMaxHeight = getVisibleToggleViews()
            .map { it.measuredHeight }
            .maxOrNull()
            .let { it ?: 0 }

        getVisibleToggleViews()
            .filter { it.measuredHeight < viewMaxHeight }
            .forEach { child ->
                val heightAdjustedSpec = MeasureSpec.makeMeasureSpec(viewMaxHeight, MeasureSpec.EXACTLY)
                val widthAdjustedSpec = MeasureSpec.makeMeasureSpec(child.measuredWidth, MeasureSpec.EXACTLY)
                child.measure(widthAdjustedSpec, heightAdjustedSpec)
            }
    }

    private fun getVisibleToggleViews() = children
        .filterIsInstance<ToggleView>()
        .filter { it.visibility != GONE }
}
