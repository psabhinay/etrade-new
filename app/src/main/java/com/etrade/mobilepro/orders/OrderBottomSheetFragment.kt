package com.etrade.mobilepro.orders

import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer.BottomSheetNavContainer
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import javax.inject.Inject

@RequireLogin
internal class OrderBottomSheetFragment @Inject constructor(
    connectionNotifier: ConnectionNotifier,
    userSessionHandler: UserSessionHandler
) : BottomSheetNavContainer(connectionNotifier, userSessionHandler) {

    private val args: OrderBottomSheetFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolBarDividerVisible = true
    }

    override fun createNavHost(): NavHostFragment {
        return NavHostFragment.create(
            R.navigation.orders_navigation_graph,
            arguments
        )
    }

    override fun createDestinationChangeListener(): NavController.OnDestinationChangedListener {
        return NavController.OnDestinationChangedListener { _, destination, _ ->
            titleText = getTitleFor(destination)
            subTitleText = ""
        }
    }

    private fun getTitleFor(destination: NavDestination): String {
        return if (destination.id == R.id.order_list) {
            when (args.orderType) {
                OrderStatus.OPEN -> getString(R.string.open_orders)
                OrderStatus.SAVED -> getString(R.string.saved_orders)
                else -> null
            }.orEmpty()
        } else {
            destination.label?.toString().orEmpty()
        }
    }

    override fun onTitleChange(newTitle: String) {
        titleText = newTitle
    }
}
