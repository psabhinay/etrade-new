package com.etrade.mobilepro.messagecenter.presentation.messagelist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterReferenceBookDataSource
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterRepo
import com.etrade.mobilepro.tier.CustomerTier
import com.etrade.mobilepro.tier.CustomerTierRepo
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MESSAGE_CENTER
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import kotlinx.coroutines.launch
import javax.inject.Inject

class MessageCenterListViewModel @Inject constructor(
    private val messageCenterRepo: MessageCenterRepo,
    messageCenterReferenceBookDataSource: MessageCenterReferenceBookDataSource,
    customerTierRepo: CustomerTierRepo
) : ViewModel(), ScreenViewModel {

    override val screenName: String = SCREEN_TITLE_MESSAGE_CENTER

    private val _viewState = MutableLiveData<ViewState>()
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    private val _snackBarMessage = MutableLiveData<ConsumableLiveEvent<String>>()
    internal val snackBarMessage: LiveData<ConsumableLiveEvent<String>>
        get() = _snackBarMessage

    private val _scrollMessagesToTop = MutableLiveData<ConsumableLiveEvent<Unit>>()
    internal val scrollMessagesToTop: LiveData<ConsumableLiveEvent<Unit>>
        get() = _scrollMessagesToTop

    private val _newMessageButtonVisible = MutableLiveData<Boolean>()
    internal val newMessageButtonVisible: LiveData<Boolean>
        get() = _newMessageButtonVisible

    init {
        messageCenterReferenceBookDataSource.clearCache()
        viewModelScope.launch {
            _newMessageButtonVisible.value =
                customerTierRepo.getTier().getOrNull()?.canSendNewMessages() ?: true
        }
    }

    internal fun refresh(forced: Boolean) {
        val isInitial = _viewState.value == null
        if (!isInitial && !forced) {
            return
        }
        if (isInitial) {
            _viewState.value = ViewState()
        }
        _viewState.updateValue {
            it.toLoading(
                if (isInitial) {
                    ViewState.Loading.INITIAL
                } else {
                    ViewState.Loading.REFRESH
                }
            )
        }
        viewModelScope.launch {
            messageCenterRepo.getMessageList().fold(
                onSuccess = { messages ->
                    _viewState.updateValue { it.toSuccess(items = messages) }
                },
                onFailure = {
                    _viewState.updateValue { it.toFailed { refresh(forced) } }
                }
            )
        }
    }

    internal fun showMessage(message: String) {
        _snackBarMessage.value = ConsumableLiveEvent(message)
    }

    internal fun scrollMessagesToTop() {
        _scrollMessagesToTop.value = ConsumableLiveEvent(Unit)
    }

    internal data class ViewState(
        val loading: Loading = Loading.NONE,
        val error: Error = Error.None,
        val items: List<Message> = emptyList()
    ) {
        enum class Loading {
            NONE, INITIAL, REFRESH
        }

        sealed class Error {
            object None : Error()
            class Loading(
                val retry: () -> Unit
            ) : Error()
        }

        fun toLoading(type: Loading): ViewState = this.copy(loading = type, error = Error.None)
        fun toFailed(retry: () -> Unit): ViewState = this.copy(loading = Loading.NONE, error = Error.Loading(retry))
        fun toSuccess(items: List<Message>): ViewState = this.copy(loading = Loading.NONE, error = Error.None, items = items)
    }

    // Exclude Tier List.
    private fun CustomerTier.canSendNewMessages(): Boolean {
        val result = this in setOf(
            CustomerTier.Gold,
            CustomerTier.Silver,
            CustomerTier.Bronze,
            CustomerTier.Tin
        )
        return result.not()
    }
}
