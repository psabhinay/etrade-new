package com.etrade.mobilepro.bank

import android.content.Context
import android.content.res.Resources
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.transactions.TransactionsServiceProvider
import com.etrade.mobilepro.util.android.coroutine.TestContextProvider
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

private const val PREVIOUS_DAYS = 60L

private val LOCALDATE_DISPLAYED_DATE_FORMAT: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")

@ExperimentalCoroutinesApi
private val testDispatcherProvider = TestContextProvider()
private val baseUrl = "baseUrl"
private val user = mockUser()
private val imageLoader = mock<ImageLoader>()
private val mockTransactionsServiceProvider = mockTransactionsServiceProvider()
private val resources = mock<Resources>()
private val darkModeChecker = mock<DarkModeChecker>()
private val inboxMessagesRepository = mock<InboxMessagesRepository>()
private val context = mock<Context>()

fun mockTransactionsServiceProvider() = mock<TransactionsServiceProvider> {}

fun mockUser() = mock<User> {
    on { getUserId() } doReturn(12345L)
}

fun getBankAccountStartDate() = LocalDate.now().minusDays(PREVIOUS_DAYS).format(LOCALDATE_DISPLAYED_DATE_FORMAT)

fun getBankAccountEndDate() = LocalDate.now().format(LOCALDATE_DISPLAYED_DATE_FORMAT)

@ExperimentalCoroutinesApi
fun createViewModel(cachedDynamicScreenRepo: DynamicScreenRepo): BankHomeViewModel {
    return BankHomeViewModel(
        baseUrl,
        cachedDynamicScreenRepo,
        imageLoader,
        mockTransactionsServiceProvider,
        resources,
        user,
        darkModeChecker,
        inboxMessagesRepository,
        testDispatcherProvider,
        context
    )
}

enum class ScreenResponseState {
    ERROR,
    SUCCESS,
    LOADING
}
