package com.etrade.mobilepro.dagger

import android.content.Context
import android.os.Build
import android.webkit.CookieManager
import com.etrade.eo.webviewcookiejar.SharedCookieManager
import com.etrade.mobilepro.BuildConfig
import com.etrade.mobilepro.appconfig.api.AppConfig
import com.etrade.mobilepro.appconfig.api.AppEnvironment
import com.etrade.mobilepro.appconfig.api.BaseUrls
import com.etrade.mobilepro.appconfig.api.LIGHT_STREAMER_LEVEL1_IDENTIFIER
import com.etrade.mobilepro.appconfig.api.LIGHT_STREAMER_LEVEL2_IDENTIFIER
import com.etrade.mobilepro.appconfig.api.Streaming
import com.etrade.mobilepro.appconfig.api.Timeouts
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.util.formatters.majorMinorVersion
import com.nhaarman.mockitokotlin2.mock
import dagger.Module
import dagger.Provides
import okhttp3.mockwebserver.MockWebServer
import org.mockito.Mockito.mock
import org.threeten.bp.Duration
import java.net.CookieHandler
import javax.inject.Singleton

@Module
class TestApplicationModule {

    @Singleton
    @Provides
    internal fun provideCookieManager(): SharedCookieManager {
        val sharedCookieManager = SharedCookieManager(mock(CookieManager::class.java))
        CookieHandler.setDefault(sharedCookieManager)
        return sharedCookieManager
    }

    @Provides
    @Singleton
    internal fun providesMockWebServer(): MockWebServer {
        return MockWebServer()
    }

    @Provides
    @Singleton
    @MobileTrade
    fun providesMobileBaseUrl(mockWebServer: MockWebServer): String = mockWebServer.url("/").toString()

    @Provides
    internal fun providesApplicationInfo(): ApplicationInfo {
        return DummyApplicationInfo()
    }

    @Singleton
    @Provides
    fun provideAppConfigRepo(@MobileTrade mobileTradeBaseUrl: String): AppConfigRepo = object : AppConfigRepo {
        override fun getAppConfig(): AppConfig = AppConfig(
            isCircuitBreakEnabled = false,
            environment = AppEnvironment.PROD,
            baseUrls = BaseUrls(
                web = "",
                express = "",
                mobileTrade = mobileTradeBaseUrl,
                mobileETrade = "",
                marketOnDemand = "",
                api = "",
                level1StreamerUrl = "",
                level2StreamerUrl = ""
            ),
            timeouts = Timeouts(
                connection = Duration.ofSeconds(1),
                read = Duration.ofSeconds(1),
                write = Duration.ofSeconds(1),
                limitSessionTo30Sec = false
            ),
            streamings = mapOf(
                LIGHT_STREAMER_LEVEL1_IDENTIFIER to Streaming(null, null),
                LIGHT_STREAMER_LEVEL2_IDENTIFIER to Streaming(null, null)
            )
        )

        override fun setEnvironment(environment: AppEnvironment, baseUrls: BaseUrls) {
            // nop
        }

        override fun setMarketCircuitBreaker(enabled: Boolean) {
            // nop
        }

        override fun setTimeouts(timeouts: Timeouts) {
            // nop
        }
    }

    @Provides
    fun provideMockContext(): Context = mock()
}

private class DummyApplicationInfo : ApplicationInfo {
    override fun deviceId(): String = ""

    override val isTablet: Boolean = false

    override val deviceType: String = "AND-PHN"

    override fun connectionType(): String = "Mobile"

    override fun appVersionCode(): String {
        return "${BuildConfig.VERSION_CODE}"
    }

    override fun appVersionName(): String {
        return BuildConfig.VERSION_NAME
    }

    override val appMajorMinorVersion: String
        get() = appVersionName().majorMinorVersion

    override fun operationSystemVersion(): String {
        return Build.VERSION.RELEASE
    }

    override fun deviceDetails(): String {
        return "${Build.MANUFACTURER} ${Build.MODEL}"
    }

    override fun platform(): String = "Android"
}
