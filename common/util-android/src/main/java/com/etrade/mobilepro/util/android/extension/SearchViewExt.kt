package com.etrade.mobilepro.util.android.extension

import android.text.InputFilter
import android.widget.EditText
import android.widget.SearchView

fun SearchView.setInputFilters(filters: Array<InputFilter>) {
    val fieldId = resources.getIdentifier("android:id/search_src_text", null, null)
    val underlyingField = findViewById<EditText>(fieldId)
    underlyingField?.filters = filters
}
