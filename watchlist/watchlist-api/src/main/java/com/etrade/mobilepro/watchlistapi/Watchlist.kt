package com.etrade.mobilepro.watchlistapi

import com.etrade.mobilepro.util.WithContentDescription
import com.etrade.mobilepro.util.formatAccessibilityDescription

data class Watchlist(
    val id: String,
    val name: String?,
    val uuid: String? = null
) : WithContentDescription {
    override fun toString(): String {
        return name ?: ""
    }

    override val contentDescription: String
        get() = toString().formatAccessibilityDescription()

    val isAllWatchlistsItem: Boolean = this.id == ALL_WATCHLISTS_ID
    val isDeviceWatchlist: Boolean = this.id == DEVICE_WATCHLIST_ID
    val isDeletable: Boolean = (this.name != INVESTMENTS_TO_WATCH_NAME && !isDeviceWatchlist)

    companion object {
        fun createAllWatchlistItem(title: String) = Watchlist(
            id = ALL_WATCHLISTS_ID,
            name = title
        )

        fun isAllWatchlistsItemId(id: String?): Boolean = id == ALL_WATCHLISTS_ID
        fun isDeviceWatchlist(watchlistId: String?) = watchlistId == DEVICE_WATCHLIST_ID
    }
}

const val ALL_WATCHLISTS_ID = "all_watchlists"
const val DEVICE_WATCHLIST_ID = "-1"

fun List<Watchlist>.insertAllWatchlistsItem(title: String, index: Int = 0) = this.toMutableList().apply {
    val addAllItem = Watchlist.createAllWatchlistItem(title)
    if (index in 0..size) {
        add(index, addAllItem)
    } else {
        add(addAllItem)
    }
}.toList()
