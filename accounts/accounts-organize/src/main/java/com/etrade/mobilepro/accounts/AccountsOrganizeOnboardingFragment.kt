package com.etrade.mobilepro.accounts

import android.content.pm.ActivityInfo
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.accounts.databinding.AccountsFragmentBinding
import com.etrade.mobilepro.common.setupBlurView
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.fragment.OrientationLock
import javax.inject.Inject

class AccountsOrganizeOnboardingFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val orientationLock: OrientationLock
) : Fragment(R.layout.accounts_fragment) {

    object Const {
        const val TAG = "AccountsOrganizeOnboardingFragment"
    }

    private val viewModel: AccountsOrganizeBoardingViewModel by activityViewModels { viewModelFactory }

    private val binding by viewBinding(AccountsFragmentBinding::bind)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        orientationLock.init(
            this,
            ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT,
            applyIfTablet = true
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBlurView(binding.blurView)
        setupItemsContainer()

        binding.buttonContinue.setOnClickListener {
            viewModel.applyChanges()
            viewModel.hideAccountsOrganizeBoarding(showDialogs = true)
        }

        if (savedInstanceState == null) {
            viewModel.fetchItems()
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            viewModel.hideAccountsOrganizeBoarding()
        }
    }

    private fun setupItemsContainer() {
        val callback = AccountsOrganizeDragNDropTouchHelperCallback(viewModel)
        val itemTouchHelper = ItemTouchHelper(callback)
        val itemAdapter = AccountsOrganizeAdapter(
            viewModel = viewModel,
            itemTouchHelper = itemTouchHelper,
            isDragFinished = { callback.isDragFinished }
        )

        val offsetTopBottom = resources.getDimensionPixelOffset(R.dimen.spacing_small)
        val offsetLeftRight = resources.getDimensionPixelOffset(R.dimen.spacing_xlarge)

        binding.itemsContainer.apply {
            adapter = itemAdapter
            itemTouchHelper.attachToRecyclerView(this)
            addItemDecoration(object : RecyclerView.ItemDecoration() {
                override fun getItemOffsets(
                    outRect: Rect,
                    view: View,
                    parent: RecyclerView,
                    state: RecyclerView.State
                ) {
                    with(outRect) {
                        top = offsetTopBottom
                        left = offsetLeftRight
                        right = offsetLeftRight
                        bottom = offsetTopBottom
                    }
                }
            })
            addOnScrollListener(AccountOrganizeOnScrollListener(binding.transparentGradient))
        }

        viewModel.listItems.observe(viewLifecycleOwner) { itemAdapter.items = it }
    }

    companion object {

        const val TAG = "AccountsOrganizeOnboardingFragment"
    }
}
