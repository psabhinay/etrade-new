package com.etrade.mobilepro.trade.form.api.input.shared

import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithQuantity
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId

interface StopLossInputId : WithSymbolTradeFormInputId, WithActionTradeFormInputId, WithPriceTradeFormInputId, WithTrailingStopInputId, WithQuantity
