package com.etrade.mobilepro.news.api.model

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.quoteapi.QuoteItemInterface
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem

sealed class NewsItem {
    object Empty : NewsItem(), QuoteItemInterface

    data class Text(
        val newsDateTimeSource: String,
        val title: String,
        val url: String,
        val symbols: List<String>,
        val content: String,
        val docId: String
    ) : NewsItem(), QuoteItemInterface

    data class CompanyHeader(
        val quoteWidgetData: LiveData<QuoteWidgetItem?>?,
        val onSearchFieldTapListener: (() -> Unit)?,
        val onSetAlertListener: (() -> Unit)?,
        val onAddToWatchlistListener: (() -> Unit)?,
        val onTradeListener: (() -> Unit)?,
        val subsequentFirstItem: NewsItem?
    ) : NewsItem()
}

fun createEmptyTextNewsItem(): NewsItem.Text = NewsItem.Text("", "", "", emptyList(), "", "")
