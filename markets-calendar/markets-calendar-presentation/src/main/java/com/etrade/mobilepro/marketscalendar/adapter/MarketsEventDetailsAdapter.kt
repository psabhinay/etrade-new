package com.etrade.mobilepro.marketscalendar.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class MarketsEventDetailsAdapter(
    item: EventDetails,
    delegate: AdapterDelegate<MutableList<EventDetails>>
) : ListDelegationAdapter<MutableList<EventDetails>>() {

    init {
        delegatesManager.addDelegate(delegate)

        items = mutableListOf(item)
    }

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        return delegatesManager.getItemViewType(items, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegatesManager.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegatesManager.onBindViewHolder(items, position, holder)
    }
}
