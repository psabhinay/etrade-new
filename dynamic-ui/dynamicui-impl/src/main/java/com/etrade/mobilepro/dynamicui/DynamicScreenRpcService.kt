package com.etrade.mobilepro.dynamicui

import com.etrade.eo.rest.retrofit.Scalar
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Url

interface DynamicScreenRpcService {

    @Scalar
    @POST
    fun getScreen(@Url url: String, @Body @JsonMoshi request: Any?): Single<String>
}
