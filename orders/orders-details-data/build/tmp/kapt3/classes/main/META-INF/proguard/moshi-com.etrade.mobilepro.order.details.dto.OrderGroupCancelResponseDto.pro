-if class com.etrade.mobilepro.order.details.dto.OrderGroupCancelResponseDto
-keepnames class com.etrade.mobilepro.order.details.dto.OrderGroupCancelResponseDto
-if class com.etrade.mobilepro.order.details.dto.OrderGroupCancelResponseDto
-keep class com.etrade.mobilepro.order.details.dto.OrderGroupCancelResponseDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
