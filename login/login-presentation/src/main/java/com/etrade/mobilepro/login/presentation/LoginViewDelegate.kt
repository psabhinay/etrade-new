package com.etrade.mobilepro.login.presentation

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.biometric.api.BiometricsProvider
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticationDelegate
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicWidget
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.encryption.viewmodel.CipherAuthenticatorViewModel
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.login.presentation.cta.LoginViewProvider
import com.etrade.mobilepro.login.usecase.LogInUseCase
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.timeout.state.TimeoutState
import com.etrade.mobilepro.tracking.Tracker
import javax.inject.Inject

class LoginViewDelegate @Inject constructor(
    private val biometricsProvider: BiometricsProvider,
    private val cipherAuthenticatorViewModel: CipherAuthenticatorViewModel,
    private val loginViewProvider: LoginViewProvider,
    private val loginPreferences: LoginPreferencesRepo,
    private val biometricAuthenticationDelegate: BiometricAuthenticationDelegate,
    private val logInUseCase: LogInUseCase,
    private val resources: Resources,
    private val timeoutState: TimeoutState,
    private val preferences: ApplicationPreferences,
    private val tracker: Tracker
) : GenericLayout, DynamicWidget {

    override val viewModelFactory: ViewModelProvider.Factory = object : ViewModelProvider.Factory {
        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return createViewModel() as T
        }
    }

    private fun createViewModel(): LoginViewModel {
        return LoginViewModel(
            R.layout.item_overview_login,
            loginViewProvider,
            biometricAuthenticationDelegate,
            biometricsProvider,
            cipherAuthenticatorViewModel,
            loginPreferences,
            logInUseCase,
            resources,
            timeoutState,
            preferences,
            tracker
        )
    }

    override val viewModelClass: Class<out DynamicViewModel> = LoginViewModel::class.java

    override val type = DynamicWidget.Type.LOGIN

    override val uniqueIdentifier: String = javaClass.name
}
