package com.etrade.mobilepro.companyoverview

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.caching.TimeElapsedCacheController
import com.etrade.mobilepro.caching.androidx.asLiveData
import com.etrade.mobilepro.mod.api.CompanyOverview
import com.etrade.mobilepro.util.Resource
import kotlinx.coroutines.rx2.awaitLast
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import javax.inject.Inject

class CachingCompanyOverviewProvider @Inject constructor(
    private val companySectorDataDelegate: CompanyLookupDataDelegate
) : CompanyOverviewProvider {
    override fun createCompanyOverviewLiveData(symbol: String): LiveData<Resource<CompanyOverview>> {
        return Transformations.map(createCompanyOverviewCachedResource(symbol).asLiveData()) { resource ->
            resource.map { it } // Because resource type is invariant and CachedCompanyOverview IS a CompanyOverview
        }
    }

    override fun createCompanySectorLiveData(symbol: String): LiveData<Resource<String?>> {
        val companyOverviewResource = createCompanyOverviewCachedResource(symbol).asLiveData()
        return Transformations.map(companyOverviewResource) { resource ->
            resource.map { it?.sector }
        }
    }

    override suspend fun companySector(symbol: String): String? {
        return createCompanyOverviewCachedResource(symbol).asObservable().awaitLast().data?.sector
    }

    private fun createCompanyOverviewCachedResource(symbol: String) = SimpleCachedResource(
        id = symbol,
        dataSourceDelegate = companySectorDataDelegate,
        controller = TimeElapsedCacheController(Duration.ofHours(3)) {
            Instant.ofEpochMilli(it.cachedAtMillis)
        }
    )
}
