package com.etrade.mobilepro.accountvalues

import com.etrade.eo.accountvalues.api.AccountValuesVO
import com.etrade.eo.accountvalues.streaming.LsFieldsStreamingPnL
import com.etrade.eo.accountvalues.streaming.createLsId
import com.etrade.eo.streaming.SubscriptionMode
import com.etrade.mobilepro.accountvalues.api.AccountValuesProvider
import com.etrade.mobilepro.common.di.ClientSideCalculations
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import io.reactivex.Observable
import javax.inject.Inject

class DefaultStreamingAccountValuesProvider @Inject constructor(
    @ClientSideCalculations private val helper: SubscriptionManagerHelper<AccountValuesVO>
) : AccountValuesProvider {

    override fun accountValues(accountId: String, isRealTimeQuotes: Boolean): Observable<AccountValuesVO> {
        val lsId = createLsId(accountId, isRealTimeQuotes)
        return helper.subscribeTo(lsId, LsFieldsStreamingPnL.FIELD_SET, SubscriptionMode.MERGE)
    }
}
