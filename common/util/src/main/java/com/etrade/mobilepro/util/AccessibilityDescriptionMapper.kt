package com.etrade.mobilepro.util

import com.etrade.mobile.libs.futures.EMPTY_FORMATTED_VALUE
import java.util.regex.Pattern

private const val SYMBOL_PERCENT = "%"
private const val SYMBOL_MINUS_DOLLAR = "-$"
private val currencyPattern = Pattern.compile("^[-]?[\$]\\d{1,3}(?:,?\\d{3})*\\.\\d{1,2}")
private val currencyPatternWithPercentage =
    Pattern.compile("^[-]?[\$]?[-]?\\d{1,3}(?:,?\\d{3})*\\.\\d{1,2}[ ]?[(][-]?\\d{1,3}(?:,?\\d{3})*\\.\\d{1,2}[%][)]")

private val wordsToReplace = mapOf(
    "Us" to "us",
    "." to " dot ",
    "/" to " slash ",
    "\\" to " backslash ",
    "ETF" to "E T F",
    "Acct" to "Account",
    "YTD" to "Year To Date"
)

fun String?.formatAccessibilityDescription(skipWords: List<String> = listOf()): String {
    if (isNullOrEmpty()) {
        return ""
    }
    var resultString = this
    wordsToReplace
        .filterKeys { key -> key !in skipWords }
        .forEach {
            resultString = resultString?.replace(it.key, it.value)
        }
    return resultString ?: ""
}

fun String?.formatAccessibilityDescription(
    replacementMap: Map<String, String>,
    includeDefaultMap: Boolean = true
): String {
    var result =
        when {
            includeDefaultMap -> this.formatAccessibilityDescription()
            isNullOrEmpty() -> ""
            else -> this
        }

    replacementMap.forEach {
        result = result.replace(Regex(it.key), it.value)
    }
    return result
}

@Suppress("ComplexMethod")
fun String.mapDateAccessibility(): String = when (this) {
    "Mon" -> "Monday"
    "Tue" -> "Tuesday"
    "Wed" -> "Wednesday"
    "Thu" -> "Thursday"
    "Fri" -> "Friday"
    "Jan" -> "January"
    "Feb" -> "February"
    "Mar" -> "March"
    "Apr" -> "April"
    "Jun" -> "June"
    "Jul" -> "July"
    "Aug" -> "August"
    "Sep" -> "September"
    "Oct" -> "October"
    "Nov" -> "November"
    "Dec" -> "December"
    else -> this
}

@Suppress("ComplexMethod", "MagicNumber")
fun Int.toMonthAccessibility(): String = when (this) {
    1 -> "January"
    2 -> "February"
    3 -> "March"
    4 -> "April"
    5 -> "May"
    6 -> "June"
    7 -> "July"
    8 -> "August"
    9 -> "September"
    10 -> "October"
    11 -> "November"
    12 -> "December"
    else -> this.toString()
}

fun String?.formatSymbolDollar() = this?.replace("$", "dollar")

fun String?.formatCurrencyAmount(emptyValue: String = "dash dash") =
    this?.replace(SYMBOL_MINUS_DOLLAR, SYMBOL_MINUS_DOLLAR.reversed())
        ?.replace(EMPTY_FORMATTED_VALUE, emptyValue)

fun String?.removePercent() =
    this?.replace(SYMBOL_PERCENT, "")

fun String?.hasDollarPattern() =
    this?.let {
        currencyPattern.matcher(this.trim()).matches() ||
            currencyPatternWithPercentage.matcher(this.trim()).matches()
    } ?: false
