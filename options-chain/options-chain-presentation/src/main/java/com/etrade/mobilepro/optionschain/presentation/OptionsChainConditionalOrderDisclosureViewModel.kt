package com.etrade.mobilepro.optionschain.presentation

import android.content.res.Resources
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.api.ConditionalOrderDisclosureRepo
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.presentation.BaseConditionalOrderDisclosureViewModel
import javax.inject.Inject

class OptionsChainConditionalOrderDisclosureViewModel @Inject constructor(
    repository: ConditionalOrderDisclosureRepo,
    resources: Resources
) : BaseConditionalOrderDisclosureViewModel(repository, resources) {

    fun onConditionalOrderSelected(onDisclosureClosed: (Boolean) -> Unit) {
        checkDisclosureState(onDisclosureClosed)
    }
}
