package com.etrade.mobilepro.data.mapper

import com.etrade.mobilepro.alerts.data.dto.AlertMessageDto
import com.etrade.mobilepro.alerts.data.mapper.AlertContentDtoMapper
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

private const val RESPONSE_TEXT = "&lt;!-- HTML MSG --&gt;&lt;link xmlns:fo=\"http://www.w3.org/1999/XSL/Format\" rel=\"stylesheet\" name=\"https://cdn.etrade.net/1/20120105.0/stylesheet/global.css\" type=\"text/css\"&gt;\n" +
    "&lt;style xmlns:fo=\"http://www.w3.org/1999/XSL/Format\" type=\"text/css\"&gt;\n" +
    ".disclaimer {margin: 0 0 5px 0; padding: 7px 0; color: #999999; text-align: center; border-right:1px #999999 solid;border-bottom:1px #999999 solid;border-left:1px #999999 solid; width:570px;}\n" +
    "&lt;/style&gt;\n" +
    "&lt;div xmlns:fo=\"http://www.w3.org/1999/XSL/Format\" align=\"left\"&gt;\n" +
    "&lt;table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"578\" align=\"left\"&gt;\n" +
    "&lt;tr&gt;\n" +
    "&lt;td rowspan=\"5\" width=\"8\"&gt;&amp;nbsp;&lt;/td&gt;\n" +
    "&lt;/tr&gt;\n" +
    "&lt;tr&gt;\n" +
    "&lt;td colspan=\"2\" height=\"30\"&gt;FACEBOOK INC CL A (&lt;a name=\"javascript:ParentReplace('/e/t/invest/quotesandresearch?traxui=SA&amp;amp;sym=FB')\"&gt;FB&lt;/a&gt;) stock has met your target volume of 12345678.00. 12,346,116 total shares have traded.\n" +
    "\t  &lt;/td&gt;\n" +
    "&lt;/tr&gt;\n" +
    "&lt;tr&gt;\n" +
    "&lt;td colspan=\"2\" height=\"30\"&gt;\n" +
    "&lt;div"
private const val RESPONSE_TEXT_WITH_FOOTER_AFTER_FIRST_FIXING = "<!DOCTYPE html><html><head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"></head><body><!-- HTML MSG --><style xmlns:fo=\"http://www.w3.org/1999/XSL/Format\" type=\"text/css\">body {margin: 0; padding: 0; color: #000000; background-color: #ffffff; text-align: left; }</style>\n" +
    "<table xmlns:fo=\"http://www.w3.org/1999/XSL/Format\" width=\"0\" border=\"0\" align=\"left\">\n" +
    "<tr>\n" +
    "<td width=\"8\"> </td><td>\n" +
    "\tYour mobile check deposit has been received and is currently being processed.\n" +
    "\t<br>\n" +
    "<br>\n" +
    "<strong>Account number: </strong>XXXX-2477<br>\n" +
    "<strong>Request date: </strong>03/25/2019<br>\n" +
    "<strong>Reference number: </strong>229150131518<br>\n" +
    "<strong>Deposit amount:</strong> \$ 88.00<br>\n" +
    "<strong>Status:</strong> In process\n" +
    "    <br>\n" +
    "<br>\n" +
    "    You'll receive another Smart Alert when your deposit posts to your account, which typically takes 1-2 business days. Also, once the deposit posts, it will be subject to <a name=\"https://us.etrade.com/e/t/estation/help?id=1903000000#Know2\" target=\"_blank\">standard hold times</a> before being available. To check the status of your deposit, visit our <a name=\"https://us.etrade.com/e/t/accounts/servicecenterhome\" target=\"_blank\">Customer Service</a> center anytime.\n" +
    "\t<br>\n" +
    "<br>\n" +
    "<strong>Important:</strong> We'll provide further instructions on what to do with your paper check in a future alert. In the meantime, please keep the paper check for your records.\n" +
    "\t<br>\n" +
    "<br>\n" +
    "\tIf you have questions, call us anytime at 1-800-ETRADE-1 (1-800-387-2331). From outside the U.S., call +1 678 624 6210. You can also log on to your account and send us a <a name=\"javascript:ParentReplace('/e/t/accounts/sccreatemsg')\">secure message</a>.\n" +
    "\t<br>\n" +
    "<br>\n" +
    "<span class=\"f1\" style=\"visibility:hidden\">(c) 2019 E*TRADE Financial Holdings, LLC. All rights reserved. Securities products and services are offered by E*TRADE Securities LLC, Member <a name=\"http://www.finra.org\" target=\"_blank\">FINRA</a>/<a name=\"http://www.sipc.org\" target=\"_blank\">SIPC</a>, and are not FDIC-insured, are not guaranteed deposits or obligations of E*TRADE Bank, and may lose value, including possible loss of the principal invested. Banking products and services are offered by E*TRADE Bank, a Federal Savings Bank, <a name=\"https://us.etrade.com/e/t/estation/help?id=1600000001\" target=\"_blank\">Member FDIC</a>. E*TRADE Securities LLC and E*TRADE Bank are separate but affiliated companies.\n" +
    "<br>\n" +
    "<br>\n" +
    "The information contained in this Smart Alert does not constitute a recommendation by E*TRADE Financial  or any of its subsidiaries and is subject to the <a name=\"https://us.etrade.com/e/t/estation/help?id=1209038000\" target=\"_blank\">Smart Alerts Terms and Conditions</a> and, as applicable, the <a name=\"https://us.etrade.com/e/t/estation/help?id=1209031000\" target=\"_blank\">E*TRADE Securities Customer Agreement</a> and the <a name=\"https://us.etrade.com/e/t/estation/help?id=1209021000\" target=\"_blank\">E*TRADE Bank Account Agreement</a>. We cannot respond to e-mails sent to this mailbox. If you have questions, please contact us through <a name=\"https://us.etrade.com/e/t/accounts/servicecenterhome\" target=\"_blank\">Customer Service</a>.</span></td>\n" +
    "</tr>\n" +
    "</table>\n" +
    "</body></html>"
private const val FOOTER = "<span class=\"f1\" style=\"visibility:hidden\">(c) 2019 E*TRADE Financial Holdings, LLC. All rights reserved. Securities products and services are offered by E*TRADE Securities LLC, Member <a name=\"http://www.finra.org\" target=\"_blank\">FINRA</a>/<a name=\"http://www.sipc.org\" target=\"_blank\">SIPC</a>, and are not FDIC-insured, are not guaranteed deposits or obligations of E*TRADE Bank, and may lose value, including possible loss of the principal invested. Banking products and services are offered by E*TRADE Bank, a Federal Savings Bank, <a name=\"https://us.etrade.com/e/t/estation/help?id=1600000001\" target=\"_blank\">Member FDIC</a>. E*TRADE Securities LLC and E*TRADE Bank are separate but affiliated companies.\n" +
    "<br>\n" +
    "<br>\n" +
    "The information contained in this Smart Alert does not constitute a recommendation by E*TRADE Financial  or any of its subsidiaries and is subject to the <a name=\"https://us.etrade.com/e/t/estation/help?id=1209038000\" target=\"_blank\">Smart Alerts Terms and Conditions</a> and, as applicable, the <a name=\"https://us.etrade.com/e/t/estation/help?id=1209031000\" target=\"_blank\">E*TRADE Securities Customer Agreement</a> and the <a name=\"https://us.etrade.com/e/t/estation/help?id=1209021000\" target=\"_blank\">E*TRADE Bank Account Agreement</a>. We cannot respond to e-mails sent to this mailbox. If you have questions, please contact us through <a name=\"https://us.etrade.com/e/t/accounts/servicecenterhome\" target=\"_blank\">Customer Service</a>.</span>"
private const val STYLE_START =
    "<!DOCTYPE html><html><head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"></head><body>"
private const val STYLE_END = "</body></html>"

internal class AlertContentDtoMapperTest {
    private val sut = AlertContentDtoMapper()

    @Test
    fun headTagsShouldBeAdded() {
        val output = sut.map(AlertMessageDto(content = RESPONSE_TEXT))

        assertTrue(output.content.startsWith(STYLE_START))
        assertTrue(output.content.endsWith(STYLE_END))
    }

    @Test
    fun width578isSubstitutedBy0() {
        val output = sut.map(AlertMessageDto(content = RESPONSE_TEXT))

        assertTrue(RESPONSE_TEXT.contains("width=\"578\""))
        assertFalse(output.content.contains("width=\"578\""))
        assertTrue(output.content.contains("width=\"0\""))
    }

    @Test
    fun footerRemoval() {
        assertTrue(RESPONSE_TEXT_WITH_FOOTER_AFTER_FIRST_FIXING.contains(FOOTER))

        val output = sut.map(AlertMessageDto(content = RESPONSE_TEXT_WITH_FOOTER_AFTER_FIRST_FIXING))

        assertFalse(output.content.contains(FOOTER))
    }
}
