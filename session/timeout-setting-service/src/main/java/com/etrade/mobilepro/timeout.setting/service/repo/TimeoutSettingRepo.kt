package com.etrade.mobilepro.timeout.setting.service.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.timeout.setting.service.SettingMessage
import com.etrade.mobilepro.timeout.setting.service.dto.TimeoutSettingRequestDto

interface TimeoutSettingRepo {
    suspend fun setTimeout(request: TimeoutSettingRequestDto): ETResult<SettingMessage>
}
