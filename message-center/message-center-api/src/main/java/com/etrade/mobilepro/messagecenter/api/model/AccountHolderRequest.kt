package com.etrade.mobilepro.messagecenter.api.model

data class AccountHolderRequest(
    val acctNo: String,
    val instNo: String,
    val instName: String,
    val name: String
)
