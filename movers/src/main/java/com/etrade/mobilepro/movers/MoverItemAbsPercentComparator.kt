package com.etrade.mobilepro.movers

import java.math.BigDecimal

/**
 * The idea behind the comparison logic is that move of -80% on one ticker
 * is more significant than move of +50% on another ticker and thus it deserves more attention.
 *
 * Examples:
 * TSLA -20% > AAPL +10%
 * TSLA -15% > AAPL -10%
 * TSLA +5%  = AAPL -5%
 * TSLA +15% > AAPL +10%
 */
class MoverItemAbsPercentComparator : Comparator<MoverItem> {

    override fun compare(mover1: MoverItem?, mover2: MoverItem?): Int {
        val mover1Change = mover1?.decimalPercentChange?.abs() ?: BigDecimal.ZERO
        val mover2Change = mover2?.decimalPercentChange?.abs() ?: BigDecimal.ZERO
        return mover1Change.compareTo(mover2Change)
    }
}
