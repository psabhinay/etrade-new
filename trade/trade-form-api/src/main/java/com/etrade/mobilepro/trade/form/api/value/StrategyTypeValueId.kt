package com.etrade.mobilepro.trade.form.api.value

import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.util.MutualConverter

interface StrategyTypeValueId {

    val butterfly: String
    val buyWrite: String
    val calendar: String
    val call: String
    val callSpread: String
    val collars: String
    val combo: String
    val condor: String
    val custom: String
    val diagonal: String
    val ironButterfly: String
    val ironCondor: String
    val marriedPut: String
    val put: String
    val putSpread: String
    val stock: String
    val straddle: String
    val strangle: String

    val converter: MutualConverter<String, StrategyType>
}
