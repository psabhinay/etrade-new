package com.etrade.mobilepro.quote.repo

import com.etrade.mobilepro.quote.dto.QuoteRequestDto
import com.etrade.mobilepro.quote.dto.QuoteResponse
import com.etrade.mobilepro.quote.mapper.MobileQuoteMapper
import com.etrade.mobilepro.quote.restapi.QuoteApiClient
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.QuoteEmptyResponse
import io.reactivex.Observable
import javax.inject.Inject

private const val TRADE_SCREEN = "TRD"

class DefaultMobileQuoteRepo @Inject constructor(
    private val apiClient: QuoteApiClient,
    private val mapper: MobileQuoteMapper
) : MobileQuoteRepo {

    override fun loadMobileQuote(
        symbolTitle: String,
        requireEarningsDate: Boolean,
        extendedHours: Boolean,
        requireExchangeCode: Boolean
    ): Observable<MobileQuote> {
        return loadRawMobileQuotes(symbolTitle, requireEarningsDate, extendedHours, requireExchangeCode)
            .map {
                it.firstOrNull() ?: throw QuoteEmptyResponse("Empty list of quotes")
            }
            .map(mapper::map)
    }

    override fun loadMobileQuotes(
        symbols: String,
        requireEarningsDate: Boolean,
        extendedHours: Boolean,
        requireExchangeCode: Boolean
    ): Observable<List<MobileQuote>> {
        return loadRawMobileQuotes(symbols, requireEarningsDate, extendedHours, requireExchangeCode).map { quoteList ->
            quoteList.map(mapper::map)
        }
    }

    @Suppress("LongParameterList") // every parameter is required
    private fun loadRawMobileQuotes(
        symbols: String,
        requireEarningsDate: Boolean,
        extendedHours: Boolean,
        requireExchangeCode: Boolean
    ): Observable<List<QuoteResponse>> {
        val flow = apiClient
            .getQuotes(getScreenName(requireExchangeCode), symbols, requireEarningsDate, extendedHours, QuoteRequestDto(""))
            .toObservable()

        return flow
            .map { it.data?.quotes ?: emptyList() }
    }

    private fun getScreenName(requireExchangeCode: Boolean = false): String {
        return if (requireExchangeCode) {
            TRADE_SCREEN
        } else {
            ""
        }
    }
}
