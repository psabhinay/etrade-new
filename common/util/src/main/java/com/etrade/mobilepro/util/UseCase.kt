package com.etrade.mobilepro.util

import kotlinx.coroutines.runBlocking

/**
 * Use case that gets parameter type of [P] and produces a result type [R]
 *
 * @param P parameter type
 * @param R result type
 */
interface UseCase<P, R> {

    /**
     * Executes a use case with a given [parameter].
     *
     * @param parameter use case parameter
     *
     * @return result of the operation
     */
    suspend fun execute(parameter: P): R
}

/**
 * Executes a use case blocking.
 *
 * @return result of the operation
 */
fun <R> UseCase<Unit, R>.executeBlocking(): R = executeBlocking(Unit)

/**
 * Executes a use case blocking.
 *
 * @param parameter use case parameter
 *
 * @return result of the operation
 */
fun <P, R> UseCase<P, R>.executeBlocking(parameter: P): R = runBlocking { execute(parameter) }

/**
 * Executes a use case.
 *
 * @return result of the operation
 */
suspend fun <R> UseCase<Unit, R>.execute(): R = execute(Unit)

/**
 * Invokes a use case.
 *
 * @return result of the operation
 */
suspend operator fun <R> UseCase<Unit, R>.invoke(): R = invoke(Unit)

/**
 * Invokes a use case with a given [parameter].
 *
 * @param parameter use case parameter
 *
 * @return result of the operation
 */
suspend operator fun <P, R> UseCase<P, R>.invoke(parameter: P): R = execute(parameter)
