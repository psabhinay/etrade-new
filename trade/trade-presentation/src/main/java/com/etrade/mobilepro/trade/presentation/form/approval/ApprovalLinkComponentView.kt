package com.etrade.mobilepro.trade.presentation.form.approval

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.TextViewCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.ui.ComponentView
import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.trade.approvallevel.ApprovalLevelDataPoint
import com.etrade.mobilepro.trade.approvallevel.R
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction
import com.etrade.mobilepro.util.formatDash

class ApprovalLinkComponentView : AppCompatTextView, ComponentView<ApprovalLinkComponent> {

    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    private var refToApprovalLinkComponent: ApprovalLinkComponent? = null
    private var accountMode: AccountMode? = null

    private var approvalLevelDataPointObserver: Observer<ApprovalLevelDataPoint> = Observer { approvalLevelDataPoint ->
        text = approvalLevelDataPoint.approvalLinkLabel
        contentDescription = approvalLevelDataPoint.approvalLinkLabel.formatDash()
        accountMode = approvalLevelDataPoint.accountMode
    }

    init {
        TextViewCompat.setTextAppearance(this, R.style.TextAppearance_Etrade_Text_Regular_Medium_Link)
        gravity = Gravity.END or Gravity.BOTTOM
        val dimensionPixelSize = context.resources.getDimensionPixelSize(R.dimen.spacing_medium)
        setPadding(0, dimensionPixelSize, dimensionPixelSize, 0)
        setOnClickListener {
            refToApprovalLinkComponent?.run {
                accountMode?.let {
                    onActionClickListener.onActionClick(TradeFormAction.ApprovalLevel(it))
                }
            }
        }
    }

    override fun render(component: ApprovalLinkComponent, owner: LifecycleOwner) {
        refToApprovalLinkComponent?.apply {
            component.approvalLevelDataPoint.removeObserver(approvalLevelDataPointObserver)
        }

        refToApprovalLinkComponent = component

        component.apply {
            component.approvalLevelDataPoint.observe(owner, approvalLevelDataPointObserver)
        }
    }
}
