package com.etrade.msaccounts.dto

import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.msaccounts.api.MsUserPreferences
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class UserPreferencesDto(
    @Json(name = "data")
    override val data: List<MsUserPreferencesContainerDto>,
) : BaseReference<MsUserPreferences>() {
    override fun convertToModel(): MsUserPreferences {
        return data.firstOrNull()?.msUserPreferences?.let {
            MsUserPreferences(
                loginUuid = it.loginUuid,
                isMsEligible = it.isUserPilot && it.isMsFeatureEnabled,
                ctaList = it.ctaList.orEmpty()
            )
        } ?: MsUserPreferences()
    }
}

@JsonClass(generateAdapter = true)
data class MsUserPreferencesContainerDto(
    @Json(name = "ms_user_preferences")
    val msUserPreferences: MsUserPreferencesDto,
    @Json(name = "customerLevel")
    val customerLevel: String? = null,
)

@JsonClass(generateAdapter = true)
data class MsUserPreferencesDto(
    @Json(name = "login_uuid")
    val loginUuid: String? = null,
    @Json(name = "isMSfeatureEnabled")
    val isMsFeatureEnabled: Boolean = false,
    @Json(name = "isUserPilot")
    val isUserPilot: Boolean = false,
    @Json(name = "cta")
    val ctaList: List<CallToActionDto>? = null
)
