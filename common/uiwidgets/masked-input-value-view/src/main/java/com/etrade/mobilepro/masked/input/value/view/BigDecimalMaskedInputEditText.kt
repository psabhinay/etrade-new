package com.etrade.mobilepro.masked.input.value.view

import android.content.Context
import android.util.AttributeSet
import com.etrade.mobilepro.util.safeParseBigDecimal
import java.math.BigDecimal

class BigDecimalMaskedInputEditText : MaskedInputEditText<BigDecimal?> {

    constructor(ctx: Context) : super(ctx)

    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet)

    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    override fun convertTextToValue(text: CharSequence): BigDecimal? {
        return text.toString().safeParseBigDecimal()
    }
}
