package com.etrade.mobilepro.util

sealed class NetworkState {

    object Loading : NetworkState()
    object Success : NetworkState()
    data class Failed(val throwable: Throwable) : NetworkState()
}
