package com.etrade.mobilepro.util.formatters

fun formatPayeeName(payeeName: String?, accountNumber: String?, payeeNickname: String? = null): String? {
    return when {
        payeeNickname != null && payeeNickname.isNotBlank() -> payeeNickname
        payeeName != null && accountNumber != null -> {
            formatAccountName(payeeName, accountNumber)
        }
        else -> null
    }
}
