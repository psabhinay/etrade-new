package com.etrade.mobilepro.screener.presentation.mutualfund

import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.screener.api.ScreenerDisclosure
import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseScreenersFragment
import com.etrade.mobilepro.tracking.params.MutualFundMarketsScreenerResultsAnalyticsParams
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class MutualFundScreenersFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter
) : BaseScreenersFragment(snackbarUtilFactory, screenersRouter) {

    override val viewModel by viewModels<MutualFundScreenersViewModel> ({ requireParentFragment() }) { viewModelFactory }

    override val disclosureType: ScreenerDisclosure = ScreenerDisclosure.MUTUAL_FUND_PRIMARY

    override val disclosuresText get() = getString(R.string.disclosure_mutual_fund_primary_inline)

    override val resultNavListener: () -> Unit = {
        screenersRouter.actionToMfResultsFragment(findNavController())
        viewModel.trackScreenerResultPage(MutualFundMarketsScreenerResultsAnalyticsParams())
    }

    override val screenerSetSelectedListener: (ScreenerItem.ScreenerSet) -> Unit = {
        when (it.type) {
            ScreenerType.MutualFund.FundProfile -> screenersRouter.actionScreenersContainerToFundProfileFragment(findNavController())
            ScreenerType.MutualFund.Performance -> screenersRouter.actionScreenersContainerToPerformanceFragment(findNavController())
            ScreenerType.MutualFund.Expenses -> screenersRouter.actionScreenersContainerToExpensesFragment(findNavController())
            ScreenerType.MutualFund.Fees -> screenersRouter.actionScreenersContainerToFeesFragment(findNavController())
            ScreenerType.MutualFund.Risks -> screenersRouter.actionScreenersContainerToRiskFragment(findNavController())
            ScreenerType.MutualFund.Portfolio -> screenersRouter.actionScreenersContainerToMutualFundPortfolioFragment(findNavController())
            else -> throw IllegalArgumentException("Wrong tab for ${it.type}")
        }
    }
}
