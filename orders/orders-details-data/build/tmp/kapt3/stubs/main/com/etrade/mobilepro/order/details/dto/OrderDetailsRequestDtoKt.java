package com.etrade.mobilepro.order.details.dto;

import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u000e\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"}, d2 = {"ORIG_SYS_CODE_REQ", "", "orders-details-data"})
public final class OrderDetailsRequestDtoKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORIG_SYS_CODE_REQ = "57";
}