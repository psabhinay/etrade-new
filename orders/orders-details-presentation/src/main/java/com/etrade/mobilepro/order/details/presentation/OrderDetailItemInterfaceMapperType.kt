package com.etrade.mobilepro.order.details.presentation

import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.orders.api.OrderType

typealias OrderDetailItemInterfaceMapperType = (
    @JvmSuppressWildcards BaseOrderDetailsItem,
    @JvmSuppressWildcards Int,
    @JvmSuppressWildcards OrderType,
    @JvmSuppressWildcards OrderDetailDisplayItem.OrderBidAskItem,
    @JvmSuppressWildcards OrderOptionsRatio?
) ->
(@JvmSuppressWildcards List<OrderDetailItemInterface>)
