package com.etrade.mobilepro.util.android.binding

import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.util.android.R
import com.etrade.mobilepro.util.android.databinding.FragmentDataBindingTestBinding
import org.hamcrest.CoreMatchers.isA
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsSame.sameInstance
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.IllegalStateException

@RunWith(AndroidJUnit4::class)
class FragmentDataBindingDelegateTest {

    @Test
    fun `when fragment resumed binding is valid`() {
        val fragment = DataBindingTestFragment()
        launchFragmentInContainer { fragment }

        assertThat(fragment.binding, notNullValue())
        assertThat(fragment.binding.testImage, isA(ImageView::class.java))
        assertThat(fragment.binding.testText, isA(TextView::class.java))
    }

    @Test(expected = IllegalStateException::class)
    fun `when fragment destroyed binding is invalid`() {
        val fragment = DataBindingTestFragment()
        val scenario = launchFragmentInContainer { fragment }

        assertThat(fragment.binding, notNullValue())

        scenario.moveToState(Lifecycle.State.DESTROYED)

        fragment.binding
    }

    @Test
    fun `when binding is accessed multiple times it is the same`() {
        val fragment = DataBindingTestFragment()
        launchFragmentInContainer { fragment }

        val firstBinding = fragment.binding
        val secondBinding = fragment.binding
        assertThat(firstBinding, sameInstance(secondBinding))
    }

    @Test
    fun `when fragment is recreated binding is different`() {
        val fragment = DataBindingTestFragment()
        val scenario = launchFragmentInContainer { fragment }

        val firstBinding = fragment.binding
        assertThat(firstBinding, notNullValue())

        scenario.recreate()

        val secondBinding = fragment.binding
        assertThat(firstBinding, not(sameInstance(secondBinding)))
        assertThat(firstBinding.root, not(sameInstance(secondBinding.root)))
    }
}

internal class DataBindingTestFragment : Fragment(R.layout.fragment_data_binding_test) {
    val binding by dataBinding(FragmentDataBindingTestBinding::bind)
}
