package com.etrade.mobilepro.completeview.repo

import com.etrade.completeview.dto.AccountSummaryCardViewDto
import com.etrade.completeview.dto.AccountSummaryViewDto
import com.etrade.completeview.dto.DayGainViewDto
import com.etrade.completeview.dto.NetAssetsViewDto
import com.etrade.completeview.dto.NetSummaryViewDto
import com.etrade.mobile.accounts.defaultaccount.getIndexOf
import com.etrade.mobile.accounts.defaultaccount.getStateFor
import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobilepro.accountslist.data.dto.AccountReferenceDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.MobileScreen
import com.etrade.mobilepro.dynamic.referencehelper.ReferenceHelper
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.dynamicui.DefaultDynamicScreenRepo
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.ViewAllAccountsViewDto
import com.etrade.mobilepro.dynamicui.datasource.mapWidgetDtoNameToDto
import com.etrade.mobilepro.home.overview.edit.OverviewWidgetRepo
import com.etrade.mobilepro.inboxmessages.dynamicui.api.DynamicScreenContentUpdater
import com.etrade.mobilepro.indices.dto.Action
import com.etrade.mobilepro.indices.dto.BehaviourOptions
import com.etrade.mobilepro.indices.dto.MarketIndicesViewDto
import com.etrade.mobilepro.portfolio.model.PositionsReferenceDto
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.collection.removeAndReturnFirstIf
import com.etrade.mobilepro.util.safeLet
import com.etrade.msaccounts.dto.AggregateValuesDto
import com.etrade.msaccounts.dto.AggregateValuesViewDto
import io.reactivex.Observable
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.rx2.asObservable
import javax.inject.Inject

class CompleteViewDynamicScreenRepo @Inject constructor(
    screenDataSource: ScreenDataSource,
    objectFactories: Map<Class<*>, @JvmSuppressWildcards DynamicUiViewFactory>,
    referenceHelper: ReferenceHelper,
    dynamicContentUpdater: DynamicScreenContentUpdater,
    private val widgetsRepo: OverviewWidgetRepo,
    private val user: User,
    private val userPreferences: UserPreferences
) : DefaultDynamicScreenRepo(
    screenDataSource,
    objectFactories,
    referenceHelper,
    dynamicContentUpdater
) {

    private val widgetList: Observable<List<BaseScreenView>>
        get() = widgetsRepo.getWidgetsFlow().map { widgets ->
            val enabledWidgets = widgets
                .filter { it.isChecked }
                .map { mapWidgetDtoNameToDto(it.widgetName) }
                .toMutableList()
            if (user.isMultiAccountUser) {
                enabledWidgets.add(0, ViewAllAccountsViewDto(null, null))
            }
            enabledWidgets
        }.asObservable()

    /**
     * Transforms the complete view response where the original views are adjusted to suit Mobile view.
     * Original views : Net Assets, Days Gain, AccountSummary1, AccountSummary2
     * Transformed views : NetAssetSummary, MarketIndices, AccountSummary1, AccountSummary2
     */
    override fun screenTransformation(param: MobileScreen): Observable<MobileScreen> {
        return widgetList.map { widgets ->
            val isSingleAccount = param.references?.find { it is PositionsReferenceDto } != null
            val views = param.views
                .filterNotNull()
                .map { view ->
                    if (view is AccountSummaryViewDto) {
                        AccountSummaryCardViewDto(view.data, view.ctaList, view.clickActionDto, isSingleAccount)
                    } else {
                        view
                    }
                }
                .applyAccountOptions()
                .toMutableList()
            val options = BehaviourOptions(clickAction = Action.OPEN_INDEX_CHART)
            views.add(0, MarketIndicesViewDto(null, null, options))
            val netAssets = views.removeAndReturnFirstIf { it is NetAssetsViewDto } as? NetAssetsViewDto
            val dayGain = views.removeAndReturnFirstIf { it is DayGainViewDto } as? DayGainViewDto
            val accountReferenceDto = param.references?.filterIsInstance<AccountReferenceDto>()?.get(0)
            val netSummaryAccountId = accountReferenceDto?.getNetSummaryAccountId(isSingleAccount)
            views.addNetSummaryViewDto(netAssets, dayGain, netSummaryAccountId)
            accountReferenceDto?.extractValuesFromViewToAccountReference(param.views)
            val updatedWidgets = widgets.updateAggregateValuesDto(netAssets, netSummaryAccountId)
            views.addAll(updatedWidgets)

            param.copy(views = views)
        }
    }

    private fun AccountReferenceDto.extractValuesFromViewToAccountReference(views: List<BaseScreenView?>) {
        data.forEach { accountDto ->
            if (accountDto.accountType == AccountType.Bank) {
                views.find { it is AccountSummaryViewDto && accountDto.accountUuid == it.data.accountUuid }
                    ?.let { viewDto ->
                        accountDto.availableBalance =
                            (viewDto as AccountSummaryViewDto).data.additionalSectionDtos.firstOrNull()?.value?.initial
                    }
            }
        }
    }

    private fun List<BaseScreenView>.applyAccountOptions(): List<BaseScreenView> {
        return userPreferences.getAccountsOptions().let { accountOptions ->
            filter { screen ->
                if (screen is AccountSummaryCardViewDto) {
                    accountOptions.getStateFor(screen.data.accountUuid)
                } else {
                    true
                }
            }.sortedBy {
                if (it is AccountSummaryCardViewDto) {
                    accountOptions.getIndexOf(it.data.accountUuid)
                } else {
                    0
                }
            }
        }
    }

    private fun AccountReferenceDto.getNetSummaryAccountId(isSingleAccount: Boolean): String? {
        val accountId = data.getOrNull(0)?.accountId?.takeIf { data.size == 1 }
        return accountId.takeIf { isSingleAccount }
    }

    private fun MutableList<BaseScreenView>.addNetSummaryViewDto(
        netAssets: NetAssetsViewDto?,
        dayGain: DayGainViewDto?,
        accountId: String?
    ) {
        safeLet(netAssets, dayGain) { assets, gain ->
            add(0, NetSummaryViewDto(assets.data, gain.data, accountId))
        }
    }

    private fun List<BaseScreenView>.updateAggregateValuesDto(
        netAssets: NetAssetsViewDto?,
        netSummaryAccountId: String?
    ): List<BaseScreenView> {
        return netAssets?.data?.value?.initial?.let { netAssetsValue ->
            map {
                if (it is AggregateValuesViewDto) {
                    AggregateValuesViewDto(
                        data = AggregateValuesDto(
                            netAssets = netAssetsValue,
                            accountId = netSummaryAccountId
                        )
                    )
                } else {
                    it
                }
            }
        } ?: this
    }
}
