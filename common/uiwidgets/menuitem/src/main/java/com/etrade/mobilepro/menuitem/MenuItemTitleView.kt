package com.etrade.mobilepro.menuitem

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.withStyledAttributes
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.visibleIf

class MenuItemTitleView : LinearLayout {

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    lateinit var titleTextView: TextView
    lateinit var labelPill: TextView

    /**
     * Property to get/set menu title.
     */
    var text: CharSequence
        get() = titleTextView.text
        set(value) {
            titleTextView.text = value
        }

    var showLabelPill: Boolean = false
        set(value) {
            labelPill.visibleIf(value)
        }

    private fun init(attributeSet: AttributeSet?) {
        inflate(R.layout.menu_item_textview_with_label_pill, true)
        titleTextView = findViewById(R.id.title)
        labelPill = findViewById(R.id.labelPill)
        context.withStyledAttributes(attributeSet, R.styleable.MenuItemTextViewWithLabelPill) {
            titleTextView.text = getString(R.styleable.MenuItemTextViewWithLabelPill_menu_title)
        }
    }
}
