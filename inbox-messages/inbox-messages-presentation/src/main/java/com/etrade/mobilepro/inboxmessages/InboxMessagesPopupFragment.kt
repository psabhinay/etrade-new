package com.etrade.mobilepro.inboxmessages

import android.app.Dialog
import android.os.Bundle
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.View
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.common.RequireLogin
import com.google.android.material.dialog.MaterialAlertDialogBuilder

@RequireLogin
class InboxMessagesPopupFragment : DialogFragment() {

    private val args: InboxMessagesPopupFragmentArgs by navArgs()

    override fun onStart() {
        super.onStart()

        (dialog?.findViewById<View>(android.R.id.message) as? TextView)?.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val message = SpannableString(args.message).also { Linkify.addLinks(it, Linkify.WEB_URLS); }

        return context?.let { ctx ->
            val builder = MaterialAlertDialogBuilder(ctx)
                .setTitle(R.string.please_note)
                .setMessage(message)
                .setPositiveButton(R.string.ok) { _, _ -> dismiss() }

            builder.create()
        } ?: return super.onCreateDialog(savedInstanceState)
    }
}
