package com.etrade.mobilepro.trade.data.stoploss

import com.etrade.mobilepro.trade.data.DAY_TRADING_FLAG
import com.etrade.mobilepro.trade.data.DAY_TRADING_VER
import com.etrade.mobilepro.trade.data.advanced.FORM_TARGET_KEY
import com.etrade.mobilepro.trade.data.advanced.OPTION_EXPIRATION_KEY
import com.etrade.mobilepro.trade.data.advanced.OPTION_STRIKE_PRICE_KEY
import com.etrade.mobilepro.trade.data.advanced.OPTION_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.OPT_LEVEL
import com.etrade.mobilepro.trade.data.advanced.ORDER_COUNT_KEY
import com.etrade.mobilepro.trade.data.advanced.PLATFORM_KEY
import com.etrade.mobilepro.trade.data.advanced.PLATFORM_VALUE
import com.etrade.mobilepro.trade.data.advanced.PREVIEW_X_KEY
import com.etrade.mobilepro.trade.data.advanced.PRIMARY_SECURITY_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.VALUE_SECURITY_TYPE_OPTION
import com.etrade.mobilepro.trade.data.form.input.DefaultStopLossOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossOptionTradeFormInputId
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
internal class StopLossOrderTradeDelegateOptionTest : AbstractStopLossOrderTradeDelegateTest<StopLossOptionTradeFormInputId>() {

    override val inputId: StopLossOptionTradeFormInputId = DefaultStopLossOptionTradeFormInputId

    @Test
    fun `PREVIEW - MARKET BUY_OPEN 1 AAPL--210115C00020000 - STOP_LIMIT_ON_QUOTE GOOD_FOR_60_DAYS`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.option,
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.symbol to "AAPL--210115C00020000:OPTN",
            inputId.strategy to inputId.strategyValueId.call,
            inputId.tradeLegs to "[{\"expiryDay\":\"15\",\"expiryMonth\":\"1\",\"expiryYear\":\"2021\",\"optionType\":\"1\",\"quantity\":1,\"strikePrice\":\"20\",\"symbol\":\"AAPL--210115C00020000\",\"transaction\":\"1\"}]",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.stopLoss,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.stopLimit,
            inputId.stopLossInputId.limitPriceForStopLimit to "5",
            inputId.stopLossInputId.stopPriceForStopLimit to "10",
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.sell,
            inputId.stopLossInputId.quantity to "1",
            inputId.stopLossInputId.symbol to "AAPL--210115C00020000:OPTN",
            inputId.stopLossInputId.allOrNone to "0"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.strategy to inputId.strategyValueId.call,
            inputId.tradeLegs to "[{\"expiryDay\":\"15\",\"expiryMonth\":\"1\",\"expiryYear\":\"2021\",\"optionType\":\"1\",\"quantity\":1,\"strikePrice\":\"20\",\"symbol\":\"AAPL--210115C00020000\",\"transaction\":\"1\"}]",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.symbol to "AAPL--210115C00020000",
            "${OPTION_EXPIRATION_KEY}0" to "1",
            "${OPTION_STRIKE_PRICE_KEY}0" to "20",
            "${OPTION_TYPE_KEY}0" to "1",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_OPTION,
            inputId.stopLossInputId.symbol to "AAPL--210115C00020000",
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.stopLimit,
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.sell,
            inputId.stopLossInputId.quantity to "1",
            inputId.stopLossInputId.allOrNone to "0",
            inputId.stopLossInputId.limitPrice to "5",
            inputId.stopLossInputId.stopPrice to "10",
            inputId.stopLossInputId.securityType to VALUE_SECURITY_TYPE_OPTION,
            "${OPTION_EXPIRATION_KEY}1" to "1",
            "${OPTION_STRIKE_PRICE_KEY}1" to "20",
            "${OPTION_TYPE_KEY}1" to "1",
            ORDER_COUNT_KEY to "2",
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "otacreateentry",
            PREVIEW_X_KEY to "65",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }

    @Test
    fun `PREVIEW - LIMIT SELL_OPEN 3 AAPL--210115C00120000 GOOD_FOR_DAY - STOP_ON_QUOTE GOOD_FOR_60_DAYS`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.option,
            inputId.symbol to "AAPL--210115C00120000:OPTN",
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.strategy to inputId.strategyValueId.call,
            inputId.tradeLegs to "[{\"expiryDay\":\"15\",\"expiryMonth\":\"1\",\"expiryYear\":\"2021\",\"optionType\":\"1\",\"quantity\":1,\"strikePrice\":\"120\",\"symbol\":\"AAPL--210115C00120000\",\"transaction\":\"3\"}]",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "5",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to "0",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.quantity to "3",
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.stopLoss,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.stop,
            inputId.stopLossInputId.stopPrice to "15",
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.buyToCover,
            inputId.stopLossInputId.quantity to "3",
            inputId.stopLossInputId.symbol to "AAPL--210115C00120000:OPTN",
            inputId.stopLossInputId.allOrNone to "0"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.strategy to inputId.strategyValueId.call,
            inputId.tradeLegs to "[{\"expiryDay\":\"15\",\"expiryMonth\":\"1\",\"expiryYear\":\"2021\",\"optionType\":\"1\",\"quantity\":1,\"strikePrice\":\"120\",\"symbol\":\"AAPL--210115C00120000\",\"transaction\":\"3\"}]",
            inputId.symbol to "AAPL--210115C00120000",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "5",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to "0",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.quantity to "3",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_OPTION,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.stop,
            inputId.stopLossInputId.stopPrice to "15",
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.quantity to "3",
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.buyToCover,
            inputId.stopLossInputId.symbol to "AAPL--210115C00120000",
            inputId.stopLossInputId.allOrNone to "0",
            "${OPTION_EXPIRATION_KEY}0" to "1",
            "${OPTION_STRIKE_PRICE_KEY}0" to "120",
            "${OPTION_TYPE_KEY}0" to "1",
            "${OPTION_EXPIRATION_KEY}1" to "1",
            "${OPTION_STRIKE_PRICE_KEY}1" to "120",
            "${OPTION_TYPE_KEY}1" to "1",
            inputId.stopLossInputId.securityType to VALUE_SECURITY_TYPE_OPTION,
            ORDER_COUNT_KEY to "2",
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "otacreateentry",
            PREVIEW_X_KEY to "65",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }

    @Test
    fun `PREVIEW - TRAILING_STOP_$ BUY_OPEN 1 AAPL--210115C00020000 GOOD_FOR_DAY - TRAILING_STOP_$ GOOD_FOR_60_DAYS`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.option,
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.strategy to inputId.strategyValueId.call,
            inputId.tradeLegs to "[{\"expiryDay\":\"15\",\"expiryMonth\":\"1\",\"expiryYear\":\"2021\",\"optionType\":\"1\",\"quantity\":1,\"strikePrice\":\"20\",\"symbol\":\"AAPL--210115C00020000\",\"transaction\":\"1\"}]",
            inputId.symbol to "AAPL--210115C00020000:OPTN",
            inputId.quantity to "1",
            inputId.action to inputId.actionValueId.buy,
            inputId.priceType to inputId.priceTypeValueId.trailingStopDollar,
            inputId.stopPrice to "5",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.stopLoss,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.trailingStopDollar,
            inputId.stopLossInputId.stopPrice to "5",
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.sell,
            inputId.stopLossInputId.quantity to "1",
            inputId.stopLossInputId.symbol to "AAPL--210115C00020000:OPTN",
            inputId.stopLossInputId.allOrNone to "0"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.strategy to inputId.strategyValueId.call,
            inputId.tradeLegs to "[{\"expiryDay\":\"15\",\"expiryMonth\":\"1\",\"expiryYear\":\"2021\",\"optionType\":\"1\",\"quantity\":1,\"strikePrice\":\"20\",\"symbol\":\"AAPL--210115C00020000\",\"transaction\":\"1\"}]",
            inputId.priceType to inputId.priceTypeValueId.trailingStopDollar,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.action to inputId.actionValueId.buy,
            inputId.symbol to "AAPL--210115C00020000",
            inputId.quantity to "1",
            inputId.offsetValue to "5",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_OPTION,
            "${OPTION_EXPIRATION_KEY}0" to "1",
            "${OPTION_STRIKE_PRICE_KEY}0" to "20",
            "${OPTION_TYPE_KEY}0" to "1",
            inputId.stopLossInputId.securityType to VALUE_SECURITY_TYPE_OPTION,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.trailingStopDollar,
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.sell,
            inputId.stopLossInputId.quantity to "1",
            inputId.stopLossInputId.symbol to "AAPL--210115C00020000",
            inputId.stopLossInputId.allOrNone to "0",
            inputId.stopLossInputId.offsetValue to "5",
            "${OPTION_EXPIRATION_KEY}1" to "1",
            "${OPTION_STRIKE_PRICE_KEY}1" to "20",
            "${OPTION_TYPE_KEY}1" to "1",
            ORDER_COUNT_KEY to "2",
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "otacreateentry",
            PREVIEW_X_KEY to "65",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }
}
