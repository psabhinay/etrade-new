package com.etrade.mobilepro.balance.section.viewfactory

import com.etrade.mobilepro.balance.data.dto.BalancesDetailsDto
import com.etrade.mobilepro.balance.section.BalancesActionType
import com.etrade.mobilepro.balance.section.BalancesLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.HeaderSectionItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.onValid

class CashAccountBalanceSectionViewFactory : BalancesSectionViewFactory {

    @SuppressWarnings("LongMethod")
    override fun createSectionView(
        detailsDto: BalancesDetailsDto,
        resources: BalancesLabelResources,
        actionClickListener: (BalancesActionType) -> Unit
    ): List<TableSectionItemLayout> {
        val sections = mutableListOf<TableSectionItemLayout>()

        // Cash Balance overview
        sections.add(createSummarySection(resources.header.netAccountVal, formatBalancesValue(detailsDto.netAccountVal, resources.defaultResources)))
        sections.add(createSummarySection(resources.cash.totalMarketValue, formatBalancesValue(detailsDto.mktSecVal, resources.defaultResources)))
        sections.add(createSummarySection(resources.cash.availableInvestment, formatBalancesValue(detailsDto.invCashVal, resources.defaultResources)))
        sections.add(createSummarySection(resources.cash.availableWithdrawal, formatBalancesValue(detailsDto.withdrawCashVal, resources.defaultResources)))
        sections.add(createSummarySection(resources.cash.netBalance, formatBalancesValue(detailsDto.holdingCashVal, resources.defaultResources)))

        onValid(detailsDto.cashcallVal) {
            // add  Cash Call
            sections.add(HeaderSectionItem(resources.header.cashCall))
            sections.add(createSummarySection(resources.cash.call, formatBalancesValue(detailsDto.cashcallVal, resources.defaultResources)))
        }

        // securities
        sections.add(HeaderSectionItem(resources.header.securities))
        sections.add(createSummarySection(resources.cash.securities, formatBalancesValue(detailsDto.marketSecLongVal, resources.defaultResources)))

        // Purchasing Power
        sections.add(HeaderSectionItem(resources.header.purchasingPower))
        sections.add(createSummarySection(resources.cash.settledInvestment, formatBalancesValue(detailsDto.settledCashforinv, resources.defaultResources)))
        sections.add(createSummarySection(resources.cash.unsettledInvestment, formatBalancesValue(detailsDto.unsettledCashforinv, resources.defaultResources)))

        // Funds withheld for open orders
        onValid(detailsDto.withheldCashVal) {
            sections.add(HeaderSectionItem(resources.header.fundsWithHeld))
            sections.add(createSummarySection(resources.cash.cashTitle, formatBalancesValue(detailsDto.withheldCashVal, resources.defaultResources)))
        }

        // Withdrawal
        sections.add(HeaderSectionItem(resources.header.fundsWithdrawal))
        sections.add(createSummarySection(resources.cash.cashTitle, formatBalancesValue(detailsDto.withdrawCashVal, resources.defaultResources)))

        // Cash Balance
        sections.add(HeaderSectionItem(resources.header.cashBalance))

        // check for holding - "H"
        sections.addAll(getSweepHoldingSection(detailsDto, resources))

        sections.add(createSummarySection(resources.cash.unInvestedCash, formatBalancesValue(detailsDto.uninvCashbal, resources.defaultResources)))
        sections.add(createSummarySection(resources.cash.holdingNetBalance, formatBalancesValue(detailsDto.holdingCashVal, resources.defaultResources)))

        // Funds on Hold
        sections.addAll(getFundsOnHoldSection(resources, detailsDto.fundswhpwrVal, detailsDto.fundsWhWithdrawVal))
        return sections
    }
}
