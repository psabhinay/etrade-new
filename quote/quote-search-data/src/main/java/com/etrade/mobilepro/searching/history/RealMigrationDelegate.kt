package com.etrade.mobilepro.searching.history

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet
import io.realm.RealmMigration

interface RealMigrationDelegate : RealmMigration {
    val oldVersion: Long
}

@Module
interface HistoryMigrationModule {

    @Binds
    @IntoSet
    fun provideMigrationFromVersion1(migration: RealmMigrationFromVersion1): RealMigrationDelegate

    @Binds
    @IntoSet
    fun provideMigrationFromVersion0(migration: RealmMigrationFromVersion0): RealMigrationDelegate
}
