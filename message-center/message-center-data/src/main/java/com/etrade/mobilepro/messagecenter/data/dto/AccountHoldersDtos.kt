package com.etrade.mobilepro.messagecenter.data.dto

import com.etrade.mobilepro.util.json.SingleToArray
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AccountHoldersDto(
    @SingleToArray
    @Json(name = "Holder")
    val holder: List<AccountHolder>?
)

@JsonClass(generateAdapter = true)
data class AccountHolder(
    @Json(name = "FirstName") val firstName: String?,
    @Json(name = "LastName") val lastName: String?
)
