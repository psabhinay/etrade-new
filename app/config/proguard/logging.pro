# java.lang.NoSuchFieldError: No instance field name of type Ljava/lang/String; in class Lcom/arcao/slf4j/timber/TimberLoggerAdapter;
#   or its superclasses (declaration of 'com.arcao.slf4j.timber.TimberLoggerAdapter' appears in /data/app/com.etrade.mobilepro.workinprogress.debug-1/base.apk)
-keepclassmembernames class org.slf4j.helpers.NamedLoggerBase { java.lang.String name; }


# Strip out Log.d and Log.v calls
-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
}