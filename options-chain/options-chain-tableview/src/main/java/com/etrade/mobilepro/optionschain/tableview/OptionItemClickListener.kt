package com.etrade.mobilepro.optionschain.tableview

import com.etrade.mobilepro.optionschains.api.Option

/**
 * Click listener for an option clicked
 */
interface OptionItemClickListener {
    fun onOptionClicked(option: Option)
}
