package com.etrade.mobilepro.quote.restapi

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.quote.dto.QuoteDataContainer
import com.etrade.mobilepro.quote.dto.QuoteRequestDto
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface QuoteApiClient {
    /**
     * Api to get Quote for one or more tickers via HTTP Post method
     * @param screenName Http Header param used to get the  bid ask exchange code information, based on 603c1 rule trade screen
     * must show exchange code, currently passing screen name as TRD - ( abbreviation for Trade ) would suffice.
     * @param requireEarningsDate Boolean flag, determines if earnings date should be part of Quote response
     * @param ehFlag Boolean flag, determines if extended hr quote should be included in response
     */
    @JsonMoshi
    @POST("/app/quote/getmobilequotes/{resource}.json")
    fun getQuotes(
        @Header("pageName") screenName: String,
        @Path("resource") resource: String,
        @Query("requireEarningsDate") requireEarningsDate: Boolean,
        @Query("EHFLAG") ehFlag: Boolean,
        @Body @JsonMoshi body: QuoteRequestDto
    ): Single<ServerResponseDto<QuoteDataContainer>>
}
