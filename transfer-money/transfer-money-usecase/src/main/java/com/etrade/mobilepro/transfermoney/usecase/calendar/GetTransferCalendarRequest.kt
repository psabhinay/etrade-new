package com.etrade.mobilepro.transfermoney.usecase.calendar

data class GetTransferCalendarRequest(
    val year: Int
)
