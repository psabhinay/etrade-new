package com.etrade.mobilepro.chart.model

import com.etrade.mobilepro.chart.util.setColorForLineDataSet
import com.github.mikephil.charting.data.LineData

class ChartLineData(private val timeFrame: ChartTimeFrame, private val showAsPercentage: Boolean, private val isOption: Boolean = false) {

    private var lowestValue: Double = Double.MAX_VALUE
    private var highestValue: Double = Double.NEGATIVE_INFINITY

    private var highestTimeInMillis = 0L

    private val lineDataSetList: MutableList<ChartLineSet> = mutableListOf()
    private var listOfLabels: MutableList<DisplayLabel>? = null

    fun addLineSet(chartLineSet: ChartLineSet) {
        lineDataSetList.add(chartLineSet)

        if (chartLineSet.lowestValue < lowestValue) {
            lowestValue = chartLineSet.lowestValue
        }
        if (chartLineSet.highestValue > highestValue) {
            highestValue = chartLineSet.highestValue
        }

        if (chartLineSet.highestTimeInMillis > highestTimeInMillis) {
            highestTimeInMillis = chartLineSet.highestTimeInMillis
        }

        if (timeFrame is ChartTimeFrame.OneWeek || timeFrame is ChartTimeFrame.OneMonth) {
            listOfLabels = chartLineSet.listOfLabels
        }
    }

    fun getLineData(colorResolver: ColorResolver): LineData {
        val dataSets = lineDataSetList.map {
            val lineSet = it.getLineSet()
            val lineColor = colorResolver.getColorInt(it.color)
            setColorForLineDataSet(lineSet, lineColor)
            return@map lineSet
        }
        return LineData(dataSets)
    }

    fun getConfiguration() = ChartConfiguration(
        timeFrame,
        lowestValue,
        highestValue,
        highestTimeInMillis,
        showAsPercentage,
        listOfLabels,
        isOption
    )
}
