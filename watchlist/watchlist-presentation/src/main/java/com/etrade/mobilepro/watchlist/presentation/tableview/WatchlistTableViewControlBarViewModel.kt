package com.etrade.mobilepro.watchlist.presentation.tableview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.tableview.controlbar.TableViewControlBarViewModel
import com.etrade.mobilepro.util.android.SingleLiveEvent
import javax.inject.Inject

class WatchlistTableViewControlBarViewModel @Inject constructor(
    private val applicationPreferences: ApplicationPreferences,
    private val userPreferences: UserPreferences,
    private val isAuth: UserAuthenticationState,
) : ViewModel(), TableViewControlBarViewModel {

    private lateinit var watchlist: Set<String>

    val clickEvents: MutableLiveData<CtaEvent> = SingleLiveEvent()

    private val _tooltipDismissed = MutableLiveData(false)
    val tooltipDismissed: LiveData<Boolean>
        get() = _tooltipDismissed

    var dropdownSelectedIndex: Int
        get() = applicationPreferences.watchlistSelectedIndex
        set(value) {
            applicationPreferences.watchlistSelectedIndex = value
            setDropdownSelectedText(watchlist.elementAtOrNull(value))
        }

    private val _tableTypeSelectedIndex = MutableLiveData(positionsViewIndex)
    override val tableTypeSelectedIndex: LiveData<Int>
        get() = _tableTypeSelectedIndex

    override val enableCustomizeSettings: LiveData<Boolean> =
        Transformations.map(_tableTypeSelectedIndex) { it == 0 }

    fun dismissTooltip() {
        _tooltipDismissed.value = true
    }

    private var positionsViewIndex: Int
        get() {
            return userPreferences.watchlistTableViewSelectedIndex
        }
        set(value) {
            userPreferences.watchlistTableViewSelectedIndex = value
            _tableTypeSelectedIndex.postValue(value)
        }

    val selectedText: String?
        get() = watchlist.elementAtOrNull(dropdownSelectedIndex)

    override val enableDropDown: Boolean = true

    private val _dropdownSelectedText = MutableLiveData<String?>(null)
    override val dropdownSelectedText: LiveData<String?>
        get() = _dropdownSelectedText

    override val enableTableTypeSelection: Boolean = true

    override fun onCustomizeViewClick() {
        clickEvents.value = CtaEvent.LaunchCustomizeColumns
    }

    override fun onTableTypeChange(index: Int) {
        positionsViewIndex = index
        clickEvents.value = CtaEvent.PositionModeTypeUpdated(index)
    }

    override fun onDropDownClick(onUnAuthFilterClickAction: (() -> Unit)?) {
        if (isAuth() || onUnAuthFilterClickAction == null) {
            clickEvents.value = CtaEvent.ShowDropdownSelector
        } else {
            onUnAuthFilterClickAction.invoke()
        }
    }

    override fun setDropdownSelectedText(selectedText: String?) {
        _dropdownSelectedText.postValue(selectedText)
    }

    fun init(watchlist: Set<String>) {
        this.watchlist = watchlist
        setDropdownSelectedText(selectedText)
    }
}
