package com.etrade.mobilepro.horizontallistpicker

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.dropdown.DropDownManager
import com.etrade.mobilepro.lenslayoutmanager.LensLayoutManager
import com.etrade.mobilepro.uiwidgets.horizontallistpicker.R
import com.etrade.mobilepro.uiwidgets.horizontallistpicker.databinding.ViewHorizontalListPickerBinding
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.inflater
import com.etrade.mobilepro.util.android.goneIf
import kotlinx.parcelize.Parcelize

private const val NUMBER_OF_ITEMS = 3
private const val SCALE = 1f

private const val PLACEHOLDER = ""

/**
 * A picker that displays 3 items on a single row, allowing a user to swipe, use arrows or selector to choose an item.
 */
class HorizontalListPicker : FrameLayout {

    private val itemAdapter: ItemAdapter = ItemAdapter()
    private val itemLayoutManager: LinearLayoutManager

    private val selectorTag = HorizontalListPicker::class.java.name
    private var _binding: ViewHorizontalListPickerBinding? = ViewHorizontalListPickerBinding.inflate(inflater, this, true)
    val binding: ViewHorizontalListPickerBinding
        get() = _binding!!
    /**
     * Items to display on the picker.
     */
    var items: List<String> = emptyList()
        set(value) {
            if (field == value) return
            field = value

            itemAdapter.items = items

            selector?.setupIfOpen()
            invalidateSelectorView()
        }

    /**
     * Index of the selected item position.
     */
    var selectedItemPosition: Int = 0
        set(value) {
            if (items.isEmpty()) { return }
            val reducedValue = items.reduceIndex(value)
            when {
                field == reducedValue -> return
                field > reducedValue -> post { _binding?.listView?.smoothScrollToPosition(reducedValue) }
                field < reducedValue -> post { _binding?.listView?.smoothScrollToPosition(reducedValue + 2) }
            }
            field = reducedValue
            onSelectedItemPositionChangeListener?.onNewSelectedItemPosition(reducedValue)

            selector?.setupIfOpen()
        }

    /**
     * Listener for the changes of [selectedItemPosition].
     */
    var onSelectedItemPositionChangeListener: OnSelectedItemPositionChangeListener? = null

    /**
     * Custom selector. If `null`, the view doesn't draw a UI for it.
     */
    var selector: DropDownManager<String>? = null
        set(value) {
            if (field == value) return
            field = value

            if (items.isNotEmpty()) {
                value?.setupIfOpen()
            }
            binding.selectorView.goneIf(value == null)
        }

    /**
     * Selector title. Default is empty.
     */
    var selectorTitle: String = ""

    constructor(context: Context) : super(context)

    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet)

    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    init {
        itemLayoutManager = LensLayoutManager(context).apply {
            orientation = RecyclerView.HORIZONTAL
            numberOfItems = NUMBER_OF_ITEMS
            scale = SCALE
        }

        binding.listView.apply {
            adapter = itemAdapter
            isSaveEnabled = false
            layoutManager = itemLayoutManager
            LinearSnapHelper().attachToRecyclerView(this)

            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        onListViewScrolled()
                    }
                }
            })
        }

        binding.selectorView.setOnClickListener { openSelector() }
        invalidateSelectorView()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        binding.listView.adapter = null
        _binding = null
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        when (ev?.action) {
            MotionEvent.ACTION_DOWN -> parent?.requestDisallowInterceptTouchEvent(true)
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> parent?.requestDisallowInterceptTouchEvent(false)
        }
        return super.onInterceptTouchEvent(ev)
    }

    fun setSelectorContentDescription(description: String) {
        binding.selectorView.contentDescription = description
    }

    override fun onSaveInstanceState(): Parcelable? = InstanceState(itemLayoutManager.findFirstCompletelyVisibleItemPosition(), super.onSaveInstanceState())

    override fun onRestoreInstanceState(state: Parcelable?) {
        val instanceState = state as InstanceState?
        super.onRestoreInstanceState(instanceState?.state)

        instanceState?.firstVisibleItemPosition
            ?.takeIf { it != RecyclerView.NO_POSITION }
            ?.let { binding.listView.scrollToPosition(it) }
    }

    private fun onListViewScrolled() {
        selectedItemPosition = (itemLayoutManager.findLastCompletelyVisibleItemPosition() + itemLayoutManager.findFirstCompletelyVisibleItemPosition()) / 2 - 1
    }

    private fun openSelector() {
        selector?.apply {
            setup()
            openDropDown()
        }
    }

    private fun invalidateSelectorView() {
        binding.selectorView.isEnabled = items.isNotEmpty()
    }

    private fun DropDownManager<String>.setupIfOpen() {
        if (isOpen(selectorTag)) {
            setup()
        }
    }

    private fun DropDownManager<String>.setup() {
        init(
            tag = selectorTag,
            title = selectorTitle,
            items = items,
            initialSelectedPosition = selectedItemPosition
        )
        setListener { position, _ -> selectedItemPosition = position }
    }

    /**
     * Listener for the changes of selected item position.
     */
    interface OnSelectedItemPositionChangeListener {

        /**
         * This callback is being called whenever new item [position] is selected.
         *
         * @param position new selected item position
         */
        fun onNewSelectedItemPosition(position: Int)
    }

    private class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(createItemView(parent)) {
        fun bindTo(item: String) {
            (itemView as TextView).text = item
        }
    }

    private inner class ItemAdapter : RecyclerView.Adapter<ViewHolder>() {

        var items: List<String> = emptyList()
            set(value) {
                field = listOf(PLACEHOLDER) + value + PLACEHOLDER
                notifyDataSetChanged()
            }

        override fun getItemCount(): Int = items.size
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(parent)

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.apply {
                bindTo(items[position])

                if (position != 0 && position != items.lastIndex) {
                    itemView.setOnClickListener { selectedItemPosition = position - 1 }
                } else {
                    itemView.setOnClickListener(null)
                }
            }
        }
    }

    @Parcelize
    private data class InstanceState(
        val firstVisibleItemPosition: Int,
        val state: Parcelable?
    ) : Parcelable
}

private fun createItemView(parent: ViewGroup): View {
    return parent.inflate(R.layout.view_horizontal_list_picker_item).apply {
        layoutParams.width = resources.displayMetrics.widthPixels / NUMBER_OF_ITEMS
    }
}

private fun List<*>.reduceIndex(index: Int): Int {
    return when {
        index < 0 -> 0
        index > lastIndex -> lastIndex
        else -> index
    }
}
