package com.etrade.mobilepro.dummy

import androidx.annotation.ColorRes
import com.etrade.eo.accountvalues.api.AccountValuesVO
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.R
import com.etrade.mobilepro.accountvalues.COMPLETE_VIEW_SUMMARY_STREAM_ID
import com.etrade.mobilepro.accountvalues.api.AccountValuesProvider
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult.CalculatedAccountValuesVO
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingText
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import com.etrade.mobilepro.streaming.client.side.calculations.ALL_BROKERAGE_STREAM_ID
import com.etrade.mobilepro.util.android.displayColor
import io.reactivex.Observable
import io.reactivex.Single
import java.math.BigDecimal

private val ACCOUNT_BAL_STREAM_PATTERN = Regex("(\\w+):(\\w+)")
private const val VALUE_AND_VALUE_PERCENT_DATA_PATTERN = "%s (%s)"
const val ACCOUNT_BAL_STREAM_ID = "ACCOUNT_BAL"
const val DAYS_GAIN_STREAM_ID = "DAYS_GAIN"
const val NET_DAYS_GAIN_STREAM_ID = "NET_DAYS_GAIN"

/**
 * Currently supports the following streams:
 * ACCOUNT_BAL:<acct id> - This is the net liquidation value of the account. Green for >= 0, red otherwise.
 * DAYS_GAIN:<acct id> - This is for today's P(rofit)&L(oss). Green for >= 0, red otherwise.
 * NET_ASSETS:<acct id> - Represents Net Assets value of all accounts that belong to a user.
 * TOTAL_GAIN:<acct id> - This is for total P(rofit)&L(oss). Green for >= 0, red otherwise.
 *
 * Where <acct id> - may be one of the following:
 * - id of a brokerage account that belongs to a user;
 * - [ALL_BROKERAGE_STREAM_ID] - synthetic account id that represent summary values of all brokerage accounts that belong to a user;
 * - [COMPLETE_VIEW_SUMMARY_STREAM_ID] - synthetic account id that represent summary values of all accounts that belong to a user;
 */
abstract class BaseStreamingTextManager constructor(
    private val accountValuesProvider: AccountValuesProvider,
    private val streamingStatusController: StreamingStatusController
) : StreamingTextManager {

    abstract fun Observable<AccountValuesVO>.getNetAssetsAccountValues(): Observable<StreamingText>

    override fun getStreamedText(
        streamId: String,
        initialValue: String,
        movementType: StreamingTextManager.MovementType,
        isAccountStreamingAllowed: Boolean
    ): Observable<StreamingText> {
        @ColorRes val initialColor = getDisplayColorRes(movementType)

        val accountValues = if (ACCOUNT_BAL_STREAM_PATTERN.matches(streamId)) {
            val match = ACCOUNT_BAL_STREAM_PATTERN.find(streamId)
            val valueType = match?.groups?.get(1)?.value
            val accountId = match?.groups?.get(2)?.value
            val isStreamingAllowed = isAccountStreamingAllowed && streamingStatusController.isStreamingToggleEnabled

            if (accountId != null && valueType != null && isStreamingAllowed) {
                getAccountValues(accountId, valueType)
                    ?.map {
                        StreamingText(it.text, it.color)
                    }?.let {
                        Observable.concat(Observable.just(StreamingText(initialValue, initialColor)), it)
                    }
            } else {
                null
            }
        } else {
            null
        }
        return accountValues ?: Single.just(StreamingText(initialValue, initialColor)).toObservable()
    }

    @ColorRes
    private fun getDisplayColorRes(movementType: StreamingTextManager.MovementType?) = when (movementType) {
        StreamingTextManager.MovementType.GAIN -> R.color.green
        StreamingTextManager.MovementType.LOSS -> R.color.red
        else -> R.color.textColorPrimary
    }

    // TODO use real-time flag properly
    private fun getAccountValues(accountId: String, valueType: String): Observable<StreamingText>? {
        val valuesObservable = accountValuesProvider.accountValues(accountId, isRealTimeQuotes = true)

        return when (valueType) {
            ACCOUNT_BAL_STREAM_ID -> valuesObservable.getNetAccountValues()
            DAYS_GAIN_STREAM_ID -> valuesObservable.getDaysGainAccountValues()
            NET_DAYS_GAIN_STREAM_ID -> valuesObservable.getNetDaysGainAccountValues()
            "NET_ASSETS" -> valuesObservable.getNetAssetsAccountValues()
            "TOTAL_GAIN" -> valuesObservable.getTotalGainAssetsAccountValues()
            else -> null
        }
    }

    internal fun Observable<AccountValuesVO>.getNetAccountValues() = map {
        val netLiqValue = it.netLiquidationValue ?: BigDecimal.ZERO
        val formattedText = MarketDataFormatter.formatMoneyValueWithCurrency(netLiqValue)
        createStreamingText(netLiqValue, formattedText)
    }

    private fun Observable<AccountValuesVO>.getDaysGainAccountValues(): Observable<StreamingText> =
        map { accountValues ->
            if (accountValues is CalculatedAccountValuesVO) {
                getDaysGainWithPercentStreamingText(accountValues)
            } else {
                getDaysGainStreamingText(accountValues)
            }
        }

    private fun Observable<AccountValuesVO>.getNetDaysGainAccountValues() = map { accountValues ->
        getDaysGainStreamingText(accountValues)
    }

    private fun Observable<AccountValuesVO>.getTotalGainAssetsAccountValues() = filter { it is CalculatedAccountValuesVO }
        .map {
            val calculatedValues = it as CalculatedAccountValuesVO
            val totalGain = calculatedValues.totalGain
            val formattedText = getFormattedValueWithPercent(totalGain, calculatedValues.totalGainPercent)
            createStreamingText(totalGain, formattedText)
        }

    internal fun createStreamingText(value: BigDecimal?, formattedText: String): StreamingText {
        @ColorRes val displayColor = value?.displayColor() ?: R.color.textColorPrimary
        return StreamingText(formattedText, displayColor)
    }

    private fun getFormattedValueWithPercent(value: BigDecimal?, percent: BigDecimal?): String {
        val formattedValue = MarketDataFormatter.formatMoneyValueWithCurrency(value)
        val formattedValuePercent = MarketDataFormatter.formatMarketPercData(percent)
        return String.format(VALUE_AND_VALUE_PERCENT_DATA_PATTERN, formattedValue, formattedValuePercent)
    }

    internal fun getDaysGainStreamingText(accountValues: AccountValuesVO): StreamingText {
        val formattedText = MarketDataFormatter.formatMoneyValueWithCurrency(accountValues.plDay)
        return createStreamingText(accountValues.plDay, formattedText)
    }

    internal fun getDaysGainWithPercentStreamingText(accountValues: CalculatedAccountValuesVO): StreamingText {
        val formattedText = getFormattedValueWithPercent(accountValues.plDay, accountValues.plDayPercent)
        return createStreamingText(accountValues.plDay, formattedText)
    }
}

fun createAccountBalanceStreamId(accountId: String?): String =
    createLightStreamerStreamId(ACCOUNT_BAL_STREAM_ID, accountId)

fun createLightStreamerStreamId(field: String?, streamableAccountId: String?): String =
    "$field:$streamableAccountId"
