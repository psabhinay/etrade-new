package com.etrade.mobilepro.movers.portfolio

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.R
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.dynamicui.DynamicViewType
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.movers.Movers
import com.etrade.mobilepro.movers.MoversRouter
import com.etrade.mobilepro.movers.portfolio.caching.PortfolioSnapshotRepo
import com.etrade.mobilepro.movers.portfolio.dto.PortfolioMoverViewDto
import com.etrade.mobilepro.movers.portfolio.viewfactory.PortfolioMoverViewFactory
import com.etrade.mobilepro.portfoliosnapshot.NeoPortfolioSnapshotResponse
import com.etrade.mobilepro.portfoliosnapshot.PortfolioSnapshotResponseData
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.Resource
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito

@ExperimentalCoroutinesApi
internal class PortfolioMoverViewFactoryTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    private val mockGetPortfolioMoversUseCase = mock<GetPortfolioMoversUseCase>()
    private val mockNavigation = mock<MoversRouter>()
    private val streamQuoteProvider = mock<StreamingQuoteProvider<Level1Field, Level1Data, Any>>()
    private val streamStatusProvider = mock<StreamingStatusController>()
    private val mockAccountDao = Mockito.mock(AccountDaoProvider::class.java)
    private val userPreferences = mock<UserPreferences>()
    private val portfolioMoverViewDto = PortfolioMoverViewDto("", provideDummyPortfolioMoverCTA())
    private val fakeAccountOverViewRequest = ServicePath.AccountOverView(
        "", "",
        widgetList = listOf(
            DynamicViewType.MARKET_INDICES.dtoName,
            DynamicViewType.MARKET_CHART.dtoName,
            DynamicViewType.MARKET_MOVERS.dtoName,
            DynamicViewType.NEWS_PORTFOLIO.dtoName,
            DynamicViewType.NEWS_BRIEFING.dtoName,
            DynamicViewType.FOOTER.dtoName
        )
    )
    private val mockRes = lazy { mockResourcesString() }.value
    private lateinit var sut: PortfolioMoverViewFactory
    private lateinit var genericLayout: GenericLayout

    private fun mockResourcesString() = mock<Resources> {
        on { getString(R.string.unknown_server_error) } doReturn ("Something went wrong. Please try again.")
        on { getString(R.string.portfolio_movers_no_positions) } doReturn ("No positions in this portfolio")
        on { getString(R.string.portfolio_movers_title) } doReturn ("Portfolio Movers")
        on { getString(R.string.view_portfolio) } doReturn ("View Portfolio")
        on {
            getString(
                eq(R.string.portfolio_deep_link),
                any()
            )
        } doReturn ("etrade://account/63468497/portfolio")
        on { getString(R.string.nasdaq_most_active) } doReturn ("NASDAQ most active")
    }

    private fun createUserViewModel(): UserViewModel {
        return mock {
            on { hasMoreThanOneBrokerageAccount } doReturn (true)
            on { accountList } doReturn (emptyList())
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockPortfolioSnapshotRepo(
        neoPortfolioSnapshotResponse: NeoPortfolioSnapshotResponse = NeoPortfolioSnapshotResponse(
            PortfolioSnapshotResponseData()
        )
    ) =
        mock<PortfolioSnapshotRepo> {
            on {
                getPortfolioMovers(
                    any(),
                    anyOrNull()
                )
            } doReturn Observable.just(Resource.Success(neoPortfolioSnapshotResponse)) as Observable<Resource<NeoPortfolioSnapshotResponse>>
        }

    @Before
    fun setUp() {
        Dispatchers.setMain(testCoroutineDispatcher)
        val viewModel = createUserViewModel()
        sut = PortfolioMoverViewFactory(
            mockGetPortfolioMoversUseCase,
            mockNavigation,
            streamQuoteProvider,
            streamStatusProvider,
            mockRes,
            viewModel,
            userPreferences
        )

        sut.setupValuesFromRequest(fakeAccountOverViewRequest)
        genericLayout = sut.createView(portfolioMoverViewDto)
    }

    @After
    fun cleanup() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `returns error message when movers call is not successful`() {
        runBlocking(testCoroutineDispatcher) {
            whenever(mockGetPortfolioMoversUseCase.execute(any())).thenReturn(
                ETResult.failure(
                    RuntimeException("Something went wrong!")
                )
            )
        }
        val marketMovers =
            genericLayout.viewModelFactory.create(genericLayout.viewModelClass) as Movers
        marketMovers.getMovers()
        marketMovers.viewState.observeForever {
            Assert.assertEquals(DynamicUiViewState.Error(), it)
        }
    }

    @Test
    fun `view model creation when portfolio movers service is successful`() {
        Assert.assertNotNull(genericLayout)
        val movers = genericLayout.viewModelFactory.create(genericLayout.viewModelClass) as Movers
        Assert.assertEquals(R.layout.layout_movers, movers.layoutId)
        Assert.assertNotNull(movers)
    }

    private fun provideDummyPortfolioMoverCTA(): List<CallToActionDto> =
        listOf(
            CallToActionDto(
                "View Portfolio",
                ClickActionDto("etrade://account/63468497/portfolio")
            )
        )
}
