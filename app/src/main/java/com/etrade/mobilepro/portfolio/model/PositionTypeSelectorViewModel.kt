package com.etrade.mobilepro.portfolio.model

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.portfolio.PortfolioPositionsDisplayMode
import com.etrade.mobilepro.portfolio.PortfolioSettingsRepository
import com.etrade.mobilepro.portfolio.PortfolioTableMode
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.tableview.controlbar.TableViewControlBarViewModel

class PositionTypeSelectorViewModel(
    private val resources: Resources,
    private val portfolioSettingsRepository: PortfolioSettingsRepository,
    positionTableMode: PositionTableMode
) : DynamicViewModel(R.layout.tableview_controlbar), TableViewControlBarViewModel {

    override val variableId: Int = BR.viewModel

    private val isPortfolioView: Boolean = positionTableMode == PositionTableMode.PORTFOLIO_VIEW

    var dropdownSelectedIndex: Int
        get() {
            return when {
                isPortfolioView -> portfolioSettingsRepository.tableMode
                else -> PortfolioTableMode.STANDARD
            }.index
        }
        set(value) {
            if (isPortfolioView) {
                portfolioSettingsRepository.tableMode = PortfolioTableMode.from(value)
                setDropdownSelectedText(selectedText)
            }
        }

    private val _tableTypeSelectedIndex = MutableLiveData<Int>(positionsViewIndex)
    override val tableTypeSelectedIndex: LiveData<Int>
        get() = _tableTypeSelectedIndex

    override val enableCustomizeSettings: LiveData<Boolean> =
        Transformations.map(_tableTypeSelectedIndex) { it == 0 }

    val enableTableView: LiveData<Boolean> =
        Transformations.map(enableCustomizeSettings) { it && isPortfolioView }

    private var positionsViewIndex: Int
        get() {
            return when {
                isPortfolioView -> portfolioSettingsRepository.positionsDisplayMode.index
                else -> PortfolioPositionsDisplayMode.TABLE.index
            }
        }
        set(value) {
            portfolioSettingsRepository.positionsDisplayMode =
                PortfolioPositionsDisplayMode.from(value)
            _tableTypeSelectedIndex.postValue(value)
        }

    val selectedText: String
        get() = resources.getStringArray(R.array.portfolio_table_view)[dropdownSelectedIndex]

    override val enableDropDown: Boolean
        get() = isPortfolioView

    private val _dropdownSelectedText = MutableLiveData(selectedText)
    override val dropdownSelectedText: LiveData<String?>
        get() = _dropdownSelectedText

    override val enableTableTypeSelection: Boolean
        get() = isPortfolioView

    override fun onCustomizeViewClick() {
        clickEvents.value = CtaEvent.LaunchCustomizeColumns
    }

    override fun onTableTypeChange(index: Int) {
        positionsViewIndex = index
        clickEvents.value = CtaEvent.PositionModeTypeUpdated(index)
    }

    override fun onDropDownClick(onUnAuthFilterClickAction: (() -> Unit)?) {
        clickEvents.value = CtaEvent.ShowDropdownSelector
    }

    override fun setDropdownSelectedText(selectedText: String?) {
        _dropdownSelectedText.postValue(selectedText)
    }
}
