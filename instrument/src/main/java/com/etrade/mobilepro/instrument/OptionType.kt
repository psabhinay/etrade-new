package com.etrade.mobilepro.instrument

enum class OptionType(
    val requestCode: String,
    val responseCode: String
) {
    CALL("1", "C"),
    PUT("2", "P"),
    UNKNOWN("", "");

    companion object {
        fun getOptionTypeFromResponse(optCallPut: String): OptionType {
            return values().firstOrNull { optCallPut == it.responseCode } ?: UNKNOWN
        }
    }
}
