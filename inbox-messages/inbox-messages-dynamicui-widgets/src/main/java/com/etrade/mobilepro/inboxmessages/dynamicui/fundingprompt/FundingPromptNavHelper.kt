package com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt

import com.etrade.mobile.accounts.dto.AccountDto
import com.etrade.mobilepro.dynamicui.api.CtaEvent

interface FundingPromptNavHelper {
    fun getNavigationDirections(appUrl: String, accountDto: AccountDto?): CtaEvent?
}
