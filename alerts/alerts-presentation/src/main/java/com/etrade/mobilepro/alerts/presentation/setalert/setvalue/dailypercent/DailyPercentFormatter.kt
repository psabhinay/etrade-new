package com.etrade.mobilepro.alerts.presentation.setalert.setvalue.dailypercent

import com.etrade.mobilepro.inputvalueview.InputFieldManager

internal class DailyPercentFormatter(private val dailyPercentFormat: String) : InputFieldManager.Formatter {
    override fun formatRawContent(rawContent: String): String {
        return if (!rawContent.endsWith("%") && rawContent.isNotBlank()) {
            String.format(dailyPercentFormat, rawContent)
        } else {
            rawContent
        }
    }
}
