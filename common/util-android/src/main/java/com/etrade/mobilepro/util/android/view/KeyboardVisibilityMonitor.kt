package com.etrade.mobilepro.util.android.view

import android.view.View
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.lifecycleScope
import com.etrade.mobilepro.util.android.extension.isKeyboardVisible
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.ref.WeakReference

private const val DEFAULT_SAMPLE_FREQUENCY_MS = 1000L
private const val MIN_SAMPLE_FREQUENCY_MS = 100L

private val logger: Logger = LoggerFactory.getLogger(KeyboardVisibilityMonitor::class.java)

/**
 * An utility class which monitors keyboard visibility events. It is an alternative to
 * [ViewCompat.setOnApplyWindowInsetsListener] since the latest requires us to let
 * the keyboard to resize the application.
 *
 * [ViewCompat.setOnApplyWindowInsetsListener] won't work on Activities using
 * android:windowSoftInputMode="adjustPan" which is the case for our app at the time of this comment.
 *
 * After getting an instance of [KeyboardVisibilityMonitor], you must explicitly call [startMonitoring]
 * in order to start getting keyboard visibility updates.
 *
 * This class will automatically stop monitoring the keyboard visibility when [lifecycleOwner]
 * calls its onDestroy method, but consumers can call [stopMonitoring] to manually stop it.
 *
 * Note: At the time of this comment and because of limitations of the Android APIs, neither this
 * utility nor [ViewCompat.setOnApplyWindowInsetsListener] along [WindowInsetsCompat] can detect
 * keyboard visibility changes in the following scenarios:
 *
 * - When the device is using a full-screen keyboard (e.x. device in landscape mode).
 * - When the device has the ability to show multi-windows and/or floating windows.
 * - When the device is foldable and has multiple screens.
 * - When the device is using an external keyboard (e.x. physical keyboard).
 * - And maybe other scenarios not related to regular screens, windows and software keyboard.
 *
 * @param targetViewRef A weak reference to a view which can access IME status. It can be any
 * view and not necessarily an [EditText].
 * @param lifecycleOwner A lifecycle owner required to scope [KeyboardVisibilityMonitor]'s actions to it.
 * @param sampleFrequencyMs Used to determine how often the keyboard visibility should be query.
 * @param notifyInitialVisibility If true, consumer will be notified about the initial keyboard
 * visibility state which may not relate to user interaction.
 * @param listener A callback to listen to keyboard visibility changes.
 */
class KeyboardVisibilityMonitor(
    private val targetViewRef: WeakReference<View>,
    private val lifecycleOwner: LifecycleOwner,
    private val sampleFrequencyMs: Long,
    private val notifyInitialVisibility: Boolean,
    private val listener: ((Boolean) -> Unit)? = null
) : LifecycleObserver {

    private var visibilityJob: Job? = null
    private var isMonitoringRequested = false

    init {
        lifecycleOwner.lifecycle.addObserver(this)
    }

    fun startMonitoring() {
        val targetView = targetViewRef.get() ?: return

        isMonitoringRequested = true
        visibilityJob = lifecycleOwner.lifecycleScope.launchWhenResumed {
            var isKeyboardVisible: Boolean? = null
            while (isActive) {
                if (isKeyboardVisible != targetView.isKeyboardVisible) {
                    if (notifyInitialVisibility || isKeyboardVisible != null) {
                        listener?.invoke(targetView.isKeyboardVisible)
                    }
                    isKeyboardVisible = targetView.isKeyboardVisible
                }
                delay(sampleFrequencyMs)
            }
        }
    }

    fun stopMonitoring() {
        isMonitoringRequested = false
        cancelVisibilityJob()
    }

    private fun cancelVisibilityJob() {
        visibilityJob?.cancel()
        visibilityJob = null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onResume() {
        if (visibilityJob == null && isMonitoringRequested) {
            startMonitoring()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onPause() {
        if (isMonitoringRequested) {
            cancelVisibilityJob()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    private fun onDestroy() {
        targetViewRef.clear()
        lifecycleOwner.lifecycle.removeObserver(this)
    }
}

class Builder {
    lateinit var targetView: View
    lateinit var lifecycleOwner: LifecycleOwner
    var sampleFrequencyMs: Long = DEFAULT_SAMPLE_FREQUENCY_MS
    var notifyInitialVisibility: Boolean = true
    var listener: ((isKeyboardVisible: Boolean) -> Unit)? = null

    fun build(): KeyboardVisibilityMonitor? {
        return when {
            ::targetView.isInitialized.not() -> {
                logger.error("Target view must not be null")
                null
            }
            ::lifecycleOwner.isInitialized.not() -> {
                logger.error("Lifecycle owner must not be null")
                null
            }
            sampleFrequencyMs < MIN_SAMPLE_FREQUENCY_MS -> {
                logger.error("Sample frequency must be at least $MIN_SAMPLE_FREQUENCY_MS ms")
                null
            }
            else -> {
                KeyboardVisibilityMonitor(
                    targetViewRef = WeakReference(targetView),
                    lifecycleOwner = lifecycleOwner,
                    sampleFrequencyMs = sampleFrequencyMs,
                    notifyInitialVisibility = notifyInitialVisibility,
                    listener = listener
                )
            }
        }
    }
}

fun buildKeyboardVisibilityMonitor(block: Builder.() -> Unit) =
    Builder().apply(block).build()
