package com.etrade.mobilepro.localytics

import android.app.Activity
import com.etrade.mobilepro.launcher.AppLauncherActivity

class DefaultLocalyticsHelper : LocalyticsHelper {
    override fun isAppLauncherActivity(activity: Activity?): Boolean {
        return activity is AppLauncherActivity
    }
}
