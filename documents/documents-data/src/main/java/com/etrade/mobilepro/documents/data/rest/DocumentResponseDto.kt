package com.etrade.mobilepro.documents.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class DocumentResponseDto(
    @Json(name = "mobile_response")
    val holder: DocumentResponseHolderDto?
)

@JsonClass(generateAdapter = true)
internal class DocumentResponseHolderDto(
    @Json(name = "views")
    val documents: List<DocumentDto?>?
)

@JsonClass(generateAdapter = true)
internal class DocumentDto(
    @Json(name = "data")
    val data: String?
)
