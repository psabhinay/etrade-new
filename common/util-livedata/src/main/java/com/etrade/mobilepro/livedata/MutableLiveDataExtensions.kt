package com.etrade.mobilepro.livedata

import androidx.lifecycle.MutableLiveData

fun <T> MutableLiveData<T>.postUpdateValue(checkIfChanged: Boolean = true, transform: (T) -> T): Boolean =
    updateValue(checkIfChanged, transform) { newValue -> postValue(newValue) }

/**
 * Sets [newValue] of a type [T] distinctly. Meaning that if the same value is already set, it will not trigger observers, attached to the instance of this
 * [MutableLiveData].
 *
 * @param newValue The candidate to become a new value.
 * @param T The type of the value.
 */
fun <T> MutableLiveData<T>.setDistinctValue(newValue: T) {
    if (value != newValue) {
        value = newValue
    }
}

fun <T> MutableLiveData<T>.updateValue(checkIfChanged: Boolean = true, transform: (T) -> T): Boolean =
    updateValue(checkIfChanged, transform) { newValue -> value = newValue }

private inline fun <T> MutableLiveData<T>.updateValue(checkIfChanged: Boolean, transform: (T) -> T, set: MutableLiveData<T>.(T) -> Unit): Boolean {
    return value?.let { oldValue ->
        val newValue = transform(oldValue)
        when {
            (!checkIfChanged || (newValue != oldValue)) -> {
                set(newValue)
                true
            }
            else -> false
        }
    } ?: false
}
