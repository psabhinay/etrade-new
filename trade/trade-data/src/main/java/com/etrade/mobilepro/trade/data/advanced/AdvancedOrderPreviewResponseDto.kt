package com.etrade.mobilepro.trade.data.advanced

import com.etrade.mobilepro.trade.api.advanced.AdvancedOrder
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderPreviewResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class AdvancedOrderPreviewResponseDto(
    @Json(name = "invest.trading.accounts")
    val accounts: List<AdvancedAccountDto>
) : BaseAdvancedOrderResponseDto(), AdvancedOrderPreviewResponse {

    override val previewId: String?
        get() = orderSvcResponse?.firstOrNull()?.previewId

    @delegate:Transient
    override val orders: List<AdvancedOrder> by lazy {
        createOrders { order }
    }

    @delegate:Transient
    private val accountMap: Map<String, AdvancedAccountDto> by lazy {
        accounts.associateBy { it.accountId }
    }

    override fun getAccount(accountId: String): String = requireNotNull(accountMap[accountId]).shortDescription
}
