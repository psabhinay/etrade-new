package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.isOption
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.etrade.mobilepro.trade.data.form.toLegDetailsDto
import com.etrade.mobilepro.trade.form.api.getTransactionType
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ConditionalTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractAction
import com.etrade.mobilepro.trade.form.api.input.extractPriceType
import com.etrade.mobilepro.trade.form.api.input.extractQuantity

internal class ConditionalSaveOrderRequestParametersProvider(
    private val conditionalOrderTradeFormInputId: ConditionalOrderTradeFormInputId,
    private val conditionalTradeFormInputId: ConditionalTradeFormInputId
) : SaveOrderRequestParametersProvider {

    override val path: String = "savepreparedgrouporder"

    override fun createBody(params: InputValuesSnapshot): InputValuesSnapshot {
        val value = mutableMapOf<String, Any?>()
        value["accountId"] = params[conditionalTradeFormInputId.accountId]
        value["orderGroupType"] = OrderType.CONDITIONAL_ONE_TRIGGER_OTHER.typeCode
        value["orderSvcReqList"] = createOrderList(params)
        return mapOf("value" to value)
    }

    @Suppress("UNCHECKED_CAST")
    private fun createOrderList(params: InputValuesSnapshot): List<InputValuesSnapshot> {
        return with(conditionalTradeFormInputId) {
            listOfNotNull(
                createOrder(params[order1] as? InputValuesSnapshot),
                createOrder(params[order2] as? InputValuesSnapshot),
                createOrder(params[order3] as? InputValuesSnapshot)
            )
        }
    }

    private fun createOrder(params: InputValuesSnapshot?): InputValuesSnapshot? {
        params ?: return null
        val order = mutableMapOf<String, Any?>()
        with(conditionalOrderTradeFormInputId) {
            order["allOrNone"] = params[allOrNone]
            order["orderTerm"] = params[term]
            order["stopPrice"] = params[stopPriceForStopLimit] ?: params[stopPrice]
            order["limitPrice"] = params[limitPriceForStopLimit] ?: params[limitPrice]

            extractPriceType(params)?.let { priceType ->
                order["priceType"] = priceType.typeCode
                extractSymbolInfo(params)?.let { info ->
                    val isOption = info.isOption
                    order["legRequestList"] = createLegs(params, info.symbol, isOption)
                    order["orderType"] = createOrderType(priceType, isOption).typeCode
                }
            }
        }
        return order
    }

    private fun createLegs(params: InputValuesSnapshot, symbol: String, isOption: Boolean): List<LegDetailsCommonDto> {
        return listOf(createLeg(params, symbol, isOption))
    }

    private fun createLeg(params: InputValuesSnapshot, symbol: String, isOption: Boolean): LegDetailsCommonDto {
        return with(conditionalOrderTradeFormInputId) {
            object : TradeLeg {
                override val symbol: String = symbol
                override val transactionType: TransactionType = requireNotNull(extractAction(params)?.getTransactionType(isOption))
                override val quantity: Int = requireNotNull(extractQuantity(params)?.toIntOrNull())
                override val displaySymbol: String = symbol
                override val isAMOption: Boolean = false // TODO: In scope of ETAND-15762
            }
        }.toLegDetailsDto(withProductType = true)
    }

    private fun createOrderType(priceType: PriceType, isOption: Boolean): OrderType {
        return when (priceType) {
            PriceType.HIDDEN_STOP,
            PriceType.TRAILING_STOP_DOLLAR, PriceType.TRAILING_STOP_PERCENT -> {
                if (isOption) {
                    OrderType.ADVANCE_OPTION
                } else {
                    OrderType.ADVANCE_EQUITY
                }
            }
            else -> {
                if (isOption) {
                    OrderType.SIMPLE_OPTION
                } else {
                    OrderType.EQUITY
                }
            }
        }
    }
}
