package com.etrade.mobilepro.trade.presentation.form.delegate.conditional

import android.content.Context
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.FormBuilder
import com.etrade.mobilepro.dynamic.form.api.form
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.input.CONDITIONAL_ORDER_SECURITY_TYPE_ID
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.SymbolInputParams
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.conditionalOrderActions
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.conditionalOrderFooter
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.conditionalOrderSymbol
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.observeSymbolChange
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.requiredStockFormInputs
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.requiredStockFormRules
import com.etrade.mobilepro.trade.presentation.usecase.CreateConditionalTradeFormParameter
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences

internal class ConditionalStockTradeFormFactoryDelegate(
    private val context: Context,
    private val inputId: ConditionalOrderTradeFormInputId,
    private val dependencies: FormFactoryDelegateDependencies,
    private val searchIntentFactory: SearchIntentFactory,
    private val tradingDefaultPreferences: TradingDefaultsPreferences
) : ConditionalFormFactoryDelegate {

    override fun createForm(
        parameter: CreateConditionalTradeFormParameter,
        symbolChangedListener: ConditionalFormFactoryDelegate.SymbolChangedListener,
        initialSymbol: WithSymbolInfo?
    ): Form {
        return form {
            conditionalOptionOrderHiddenInputs(parameter)

            verticalGroup {
                val symbolInputParams = SymbolInputParams(InstrumentType.validStocksAndOptionTradingTypes, inputId.symbol, inputId.symbolCoder)
                conditionalOrderSymbol(context, searchIntentFactory, inputId, parameter, symbolInputParams, initialSymbol)
                requiredStockFormInputs(context, inputId, dependencies, parameter, tradingDefaultPreferences)
                conditionalOrderFooter(context.resources, parameter.onActionClickListener)
            }

            rules {
                observeSymbolChange(inputId.symbol, symbolChangedListener, SecurityType.STOCK)
                requiredStockFormRules(context.resources, parameter.onActionClickListener, inputId, parameter.quoteWidgetViewModel)
            }

            actions(parameter.onActionClickListener) {
                conditionalOrderActions(context, dependencies, parameter.orderNumber)
            }
        }
    }

    private fun FormBuilder.conditionalOptionOrderHiddenInputs(parameter: CreateConditionalTradeFormParameter) {
        hidden(inputId.accountId) {
            value = parameter.accountId
        }
        hidden(inputId.securityType) {
            value = inputId.securityTypeValueId.stock
        }
        hidden(CONDITIONAL_ORDER_SECURITY_TYPE_ID) {
            value = SecurityType.STOCK.toString()
        }
    }
}
