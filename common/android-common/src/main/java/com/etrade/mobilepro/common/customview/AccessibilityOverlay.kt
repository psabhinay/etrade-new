package com.etrade.mobilepro.common.customview

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import com.etrade.mobilepro.common.R
import com.etrade.mobilepro.util.android.accessibility.processAccessibilityStreamingEvent
import com.etrade.mobilepro.util.formatAccessibilityDescription
import com.etrade.mobilepro.util.hasDollarPattern

private const val ID_DELIM = ","
private const val ID_DEFTYPE = "id"
private const val ID_DELIM_WITH_SPACE = ", "
private const val DEBOUNCE_DEFAULT_MS = 10000

class AccessibilityOverlay : View {
    private var accessibleIds = listOf<Int>()
    private val views = mutableSetOf<View>()
    private var accessibleGroupClickable = false
    private var parentClickable = false
    private var debounce = DEBOUNCE_DEFAULT_MS

    // This misbehaves if the view is an EditText or Button or otherwise derived
    // from TextView by voicing the content when the ViewGroup approach remains silent.
    @get:NonNull
    private val accessibilityText: String
        get() {
            val accessibilityBuilder = StringBuilder()
            var hasAnyDollarPattern = false
            accessibleIds.filter { it != 0 }.forEach { viewId ->
                (parent as? ViewGroup)?.findViewById<View>(viewId)
                    ?.takeIf { it.visibility == VISIBLE }?.run {
                        if (contentDescription.isNullOrEmpty() && this is TextView) {
                            if (text.isEmpty()) {
                                hint
                            } else {
                                text
                            }
                        } else {
                            contentDescription
                        }?.apply {
                            if (!hasAnyDollarPattern) {
                                hasAnyDollarPattern = toString().hasDollarPattern()
                            }
                        }?.let {
                            accessibilityBuilder.append(it).append(ID_DELIM_WITH_SPACE)
                        }
                    }
            }

            return if (hasAnyDollarPattern) {
                accessibilityBuilder.toString().formatAccessibilityDescription(listOf("."))
            } else {
                accessibilityBuilder.toString().formatAccessibilityDescription()
            }
        }

    private var mAccessibilityActionLabel: String? = null

    constructor(context: Context) : super(context) {
        init(context, null, 0, 0)
    }

    constructor(context: Context, @Nullable attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs, 0, 0)
    }

    constructor(
        context: Context,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        init(context, attrs, defStyleAttr, 0)
    }

    constructor(
        context: Context,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context, attrs, defStyleAttr, defStyleRes)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        accessibleIds.filter { it != 0 }.forEach {
            (parent as? ViewGroup)?.findViewById<View>(it)?.run {
                importantForAccessibility = IMPORTANT_FOR_ACCESSIBILITY_NO
                isFocusable = false
                isClickable = parentClickable
                (this as? TextView)?.subscribeAccessibilityUpdatesHandling()
                views.add(this)
            }
            isFocusable = true
            isLongClickable = false
            isClickable = accessibleGroupClickable
            importantForAccessibility = IMPORTANT_FOR_ACCESSIBILITY_YES
        }
        processAccessibilityEvent()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        views.clear()
    }

    private fun init(
        context: Context,
        @Nullable attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.AccessibilityOverlay,
            defStyleAttr, defStyleRes
        ).run {
            try {
                debounce =
                    getInteger(R.styleable.AccessibilityOverlay_debounce, DEBOUNCE_DEFAULT_MS)
                accessibleGroupClickable =
                    getBoolean(R.styleable.AccessibilityOverlay_accessible_group_clickable, false)
                parentClickable =
                    getBoolean(R.styleable.AccessibilityOverlay_parent_clickable, false)
                mAccessibilityActionLabel =
                    getString(R.styleable.AccessibilityOverlay_accessibility_action_label)
                if (mAccessibilityActionLabel.isNullOrBlank()) {
                    mAccessibilityActionLabel = context.getString(R.string.activate)
                }
                getString(R.styleable.AccessibilityOverlay_accessible_group)
            } finally {
                recycle()
            }
        }?.let { accessibleIds = extractAccessibleIds(context, it) }
    }

    private fun getActionText(): String = if (accessibleGroupClickable || parentClickable) {
        if (views.any { it.isSelected }) {
            context.getString(R.string.accessibility_info_selected)
        } else {
            mAccessibilityActionLabel ?: context.getString(R.string.accessibility_action_select)
        }
    } else {
        ""
    }

    private fun extractAccessibleIds(context: Context, viewIds: String): List<Int> {
        return viewIds.split(ID_DELIM.toRegex()).mapNotNull { viewId ->
            viewId.trim().takeIf { it.isNotEmpty() }?.let { trimmedViewId ->
                resources.getIdentifier(trimmedViewId, ID_DEFTYPE, context.packageName)
            }
        }
    }

    private fun TextView.subscribeAccessibilityUpdatesHandling() {
        addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // unused
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // unused
            }

            override fun afterTextChanged(s: Editable?) {
                post { processAccessibilityEvent() }
            }
        })
    }

    private fun processAccessibilityEvent() {
        processAccessibilityStreamingEvent(accessibilityText, getActionText(), debounce.toLong())
    }
}
