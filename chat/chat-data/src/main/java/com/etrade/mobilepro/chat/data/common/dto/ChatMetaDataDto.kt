package com.etrade.mobilepro.chat.data.common.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChatMetaDataDto(

    @Json(name = "buttonName")
    val buttonName: String? = null,

    @Json(name = "buttonId")
    val buttonId: String? = null,

    @Json(name = "affinity")
    val affinity: String? = null,

    @Json(name = "sessionKey")
    val sessionKey: String? = null
)
