package com.etrade.mobilepro.marketscalendar.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class EventsSummaryResponseDto(
    @Json(name = "data")
    val data: Data?
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "eventsSummaryResponseDM")
        val eventsSummaryResponseDm: EventsSummaryResponseDm?
    )

    @JsonClass(generateAdapter = true)
    data class EventsSummaryResponseDm(
        @Json(name = "eventsSummaryDataList")
        val eventsSummaryDataList: List<EventsSummaryData>?
    )

    @JsonClass(generateAdapter = true)
    data class EventsSummaryData(
        @Json(name = "dateTime")
        val dateTime: String?
    )
}
