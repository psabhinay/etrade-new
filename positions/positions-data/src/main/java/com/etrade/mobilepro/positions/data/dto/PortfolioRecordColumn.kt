package com.etrade.mobilepro.positions.data.dto

enum class PortfolioRecordColumn(val code: String) {

    ASK("14"),
    BASE_SYMBOL_PRICE("45"),
    BID("13"),
    DAYS_EXPIRATION("95"),
    DAYS_GAIN("27"),
    DAYS_GAIN_PCT("38"),
    DELTA("89"),
    EPS("20"),
    GAMMA("90"),
    IV_PCT("91"),
    LAST_TRADE("47"),
    MARK_TO_MARKET("123"),
    MARKET_CAP("31"),
    MARKET_VALUE("11"),
    PE_RATIO("21"),
    PREV_CLOSE("32"),
    PRICE_CHANGE("15"),
    PRICE_CHANGE_PCT("16"),
    PRICE_PAID("8"),
    QUANTITY("5"),
    SYMBOL("1"),
    THETA("92"),
    TOTAL_GAIN("9"),
    TOTAL_GAIN_PCT("10"),
    VEGA("93"),
    VOLUME("17"),
    WEEK_52_HIGH("18"),
    WEEK_52_LOW("19");

    companion object {
        val defaultColumn = SYMBOL
    }
}
