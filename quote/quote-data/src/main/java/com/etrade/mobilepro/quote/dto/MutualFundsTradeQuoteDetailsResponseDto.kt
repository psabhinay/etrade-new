package com.etrade.mobilepro.quote.dto

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MutualFundsTradeQuoteDetailsResponseDto(
    @Json(name = "FundDetails")
    val fundDetailsMutual: MutualFundsTradeQuoteDetailsDto?,

    @Json(name = "SelectedFundDetails")
    val selectedFundDetails: SelectedFundDetails?
) : BaseDataDto()

@Suppress("LongParameterList")
@JsonClass(generateAdapter = true)
class MutualFundsTradeQuoteDetailsDto(
    @Json(name = "productFundDetail")
    val productFundDetail: ProductFundDetailDto?,

    @Json(name = "redemption")
    val redemption: Redemption?,

    @Json(name = "timeOfLastTrade")
    val timeOfLastTrade: Long?,

    @Json(name = "purchaseReqIra")
    val iraPurchaseRequirement: PurchaseRequirement?,

    @Json(name = "purchaseReqNira")
    val nonIraPurchaseRequirement: PurchaseRequirement?,

    @Json(name = "salesCharge")
    val salesCharge: String?,

    @Json(name = "frontloadText")
    val frontLoadText: String?,

    @Json(name = "earlyRedemptionFee")
    val fundRedemptionFee: String?,

    @Json(name = "transactionFee")
    val transactionFee: String?,

    @Json(name = "fundDescription")
    val fundDescription: String?,

    @Json(name = "etradeEarlyRedemptionFee")
    val etradeEarlyRedemptionFee: String?,

    @Json(name = "deferredSalesCharges")
    val deferredSalesCharges: List<FeeStructure>?,

    @Json(name = "frontEndSalesCharges")
    val frontEndSalesCharges: List<FeeStructure>?,

    // Mutual Fund
    @Json(name = "netAssetValue")
    val netAssetValue: String?,

    @Json(name = "change")
    val change: String?,

    @Json(name = "percentChange")
    val percentChange: String?,

    @Json(name = "annualTotalReturn")
    val annualTotalReturn: String?,

    // MMF
    @Json(name = "sevenDayCurrentYield")
    val sevenDayCurrentYield: String?,

    @Json(name = "weightedAvgMaturity")
    val weightedAvgMaturity: String?,

    @Json(name = "affiliatedStatusCd")
    val affiliatedStatusCode: String?
)

@JsonClass(generateAdapter = true)
data class Redemption(
    @Json(name = "isFrontEnd")
    val isFrontEnd: String,

    @Json(name = "isSales")
    val isSales: String
)

@JsonClass(generateAdapter = true)
data class ProductFundDetailDto(
    @Json(name = "fundName")
    val fundName: String = "",

    @Json(name = "pricingParameterCd")
    val pricingParameterCd: String = "N",

    @Json(name = "cusip")
    val cusip: String = "",

    @Json(name = "streetStatus")
    val streetStatus: String = "",

    @Json(name = "ordCutOffTime")
    val ordCutOffTime: String = "",

    @Json(name = "frontLoadWaived")
    val frontLoadWaived: String = "",

    @Json(name = "typeCode")
    val typeCode: String = "",

    @Json(name = "symbol")
    val symbol: String = ""
)

@JsonClass(generateAdapter = true)
data class FeeStructure(
    @Json(name = "percent")
    val percent: String,

    @Json(name = "lowhigh")
    val lowhigh: String
)

@JsonClass(generateAdapter = true)
data class PurchaseRequirement(
    @Json(name = "minInitAmt")
    val minInitAmt: String?,

    @Json(name = "balPayAmt")
    val balPayAmt: String?
)

@JsonClass(generateAdapter = true)
class SelectedFundDetails(
    @Json(name = "fundDetailsList")
    val fundDetailsList: List<FundDetails>
)

@JsonClass(generateAdapter = true)
class FundDetails(
    @Json(name = "exchangeCode")
    val exchangeCode: String,
    @Json(name = "marketValue")
    val marketValue: String,
    @Json(name = "positionType")
    val positionType: String,
    @Json(name = "price")
    val price: String,
    @Json(name = "qty")
    val quantity: String,
    @Json(name = "securityName")
    val securityName: String,
    @Json(name = "symbol")
    val symbol: String,
    @Json(name = "typeCode")
    val typeCode: String
)
