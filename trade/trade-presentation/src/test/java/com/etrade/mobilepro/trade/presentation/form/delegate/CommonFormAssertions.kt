package com.etrade.mobilepro.trade.presentation.form.delegate

import com.etrade.mobilepro.dynamic.form.api.Action
import com.etrade.mobilepro.dynamic.form.api.ActionType
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import org.junit.Assert

private const val ACTION_BUTTONS = 2
internal const val VERTICAL_GROUP_SECTIONS = 3

internal fun assertValidActionButtons(actions: List<Action>?) {
    requireNotNull(actions)

    Assert.assertTrue(actions.size == ACTION_BUTTONS)

    actions.forEach {
        // Disabled initially unless dependent fields are set
        Assert.assertTrue(it.isEnabled.value != true)

        // Check type
        when (it.title) {
            PREVIEW_BUTTON -> Assert.assertTrue(it.actionType == ActionType.PRIMARY)
            SAVE_BUTTON -> Assert.assertTrue(it.actionType == ActionType.SECONDARY)
            else -> Assert.fail("Invalid button")
        }
    }
}

internal fun assertFormValues(testInput: Map<String, String>, form: Form?) {
    Assert.assertNotNull(form)
    testInput.entries.forEach { testRecord ->
        Assert.assertEquals("${testRecord.key} ", testRecord.value, form.getValue(testRecord.key))
    }
}

private fun Form?.getValue(id: String): Any? = this?.requireAnyInput(id)?.parameterValue?.value
