package com.etrade.mobilepro.accounts.util.android

import android.os.Parcel
import androidx.core.os.ParcelCompat.readBoolean
import androidx.core.os.ParcelCompat.writeBoolean
import com.etrade.mobile.accounts.billpay.BillPay
import kotlinx.parcelize.Parceler

object BillPayParceler : Parceler<BillPay> {

    override fun create(parcel: Parcel): BillPay = parcel.run {
        BillPay(
            isRestricted = readBoolean(this),
            isEnrolled = readBoolean(this),
            isPrimary = readBoolean(this),
            isEligible = readBoolean(this)
        )
    }

    override fun BillPay.write(parcel: Parcel, flags: Int) = parcel.run {
        writeBoolean(this, isRestricted)
        writeBoolean(this, isEnrolled)
        writeBoolean(this, isPrimary)
        writeBoolean(this, isEligible)
    }
}
