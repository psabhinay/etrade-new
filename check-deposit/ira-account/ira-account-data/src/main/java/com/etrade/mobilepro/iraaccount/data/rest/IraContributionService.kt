package com.etrade.mobilepro.iraaccount.data.rest

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface IraContributionService {

    @JsonMoshi
    @POST("app/cashtransfer/iraContributionInfo.json")
    suspend fun iraContributionInfo(
        @JsonMoshi @Body body: IraContributionInfoRequestDto,
        @Query("source") source: String = "ET_MOBILE"
    ): ServerResponseDto<IraContributionInfoResponseDto>
}
