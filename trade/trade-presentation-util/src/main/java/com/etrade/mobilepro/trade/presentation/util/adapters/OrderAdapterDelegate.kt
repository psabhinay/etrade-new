package com.etrade.mobilepro.trade.presentation.util.adapters

import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

interface OrderAdapterDelegate {
    fun createOrderSectionItems(order: Order): List<TableSectionItemLayout>
}
