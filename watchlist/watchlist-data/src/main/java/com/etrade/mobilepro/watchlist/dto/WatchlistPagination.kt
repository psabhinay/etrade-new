package com.etrade.mobilepro.watchlist.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class WatchlistPagination(
    @Json(name = "posMarker")
    val posMarker: String = "",
    @Json(name = "posPerPage")
    val posPerPage: Int = 100,
    @Json(name = "startPosNum")
    val startPosNum: Int = 1
)
