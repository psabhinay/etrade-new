package com.etrade.mobilepro.quoteapi

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.instrument.InstrumentType

interface MutualFundsTradeQuotesRepo {
    suspend fun loadFundsQuote(accountId: String, symbol: String): ETResult<MutualFundsTradeQuotesResult>
    suspend fun loadOwnedFundsQuote(accountId: String, symbol: String, instrumentType: InstrumentType): ETResult<MutualFundsTradeQuotesResult>
}
