package com.etrade.mobilepro.ui.multitoggle

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import androidx.annotation.ArrayRes
import androidx.annotation.ColorInt
import androidx.annotation.LayoutRes
import androidx.annotation.StyleableRes
import androidx.core.content.res.getBooleanOrThrow
import androidx.core.content.res.getColorOrThrow
import androidx.core.content.res.getDimensionPixelSizeOrThrow
import androidx.core.content.res.getDrawableOrThrow
import androidx.core.content.res.getIntOrThrow
import androidx.core.content.res.getResourceIdOrThrow
import androidx.core.content.withStyledAttributes
import androidx.core.view.ViewCompat
import androidx.core.view.children
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.util.android.extension.getFloat
import com.google.android.material.card.MaterialCardView

/**
 * Toggles will be evenly distributed within view
 */
const val MODE_EVEN = 1
private const val TAG_DIVIDER = "divider"
private const val SPACE = " "

/**
 * MultiToggle is a layout used to group related options. Layout and spacing is arranged to
 * convey that certain toggle buttons are part of a group.
 * It can display text buttons (use 'items' attribute in xml) or
 * image buttons (use 'imageItems' attribute). For images you can set [selectBackground] to false
 * That flag changes color filter for the drawable only. Also consider to set 'itemsDescriptions'
 * for the accessibility. For text buttons it sets formatted title as contentDescription by default.
 * Mixing items currently not yet supported (works only in 'list after list' sequential order)
 */
@SuppressLint("ResourceType")
class MultiToggle : MaterialCardView {
    private lateinit var linearLayout: LinearLayout

    /**
     * The current list of toggles. Do not modify directly, instead call [addToggle] and [inflateItems] methods
     * Please make sure titles ARE UNIQUE if using setToggleWithTitles
     */
    private val toggles = mutableListOf<Toggle>()
    val toggleIds: List<Int>
        get() = toggles.map { it.id }

    /**
     * Set if we are going to allow multiple selection or not. This will also call [.reset]
     * in order to prevent strange behaviour switching between multiple and single selection
     *
     */
    var multipleSelection: Boolean = false
        set(value) {
            field = value
            reset()
        }

    /**
     * Allow selected items to be de-selected. Defaults to true.
     *
     */
    var allowDeselection = true

    /**
     * Applies [selectedColor] and [unselectedColor] to the background if true (default)
     * and, otherwise, to the view content (e.g. text or image).
     *
     */
    var selectBackground = true

    /**
     * Set the color of the divider between toggles. Set to null to not include dividers
     */
    @ColorInt
    var dividerColor: Int? = null
        set(value) {
            field = value
            if (toggles.isNotEmpty()) {
                val views = ArrayList<View>()
                linearLayout.findViewsWithText(
                    views,
                    TAG_DIVIDER,
                    FIND_VIEWS_WITH_CONTENT_DESCRIPTION
                )
                views.forEach {
                    if (value == null) {
                        linearLayout.removeView(it)
                    } else {
                        it.setBackgroundColor(value)
                    }
                }
            }
        }

    /**
     * Set the color to show when the toggle is selected
     */
    @ColorInt
    var selectedColor: Int = 0
        set(value) {
            field = value
            adjustColorOfSelected()
        }

    /**
     * Set the color to show when the toggle is not selected
     */
    @ColorInt
    var unselectedColor: Int? = null

    /**
     * List of colors that will be used for each toggle item separately. Index of a color is equal to an index of an item.
     *
     * If number of items exceed this list's size, then [selectedColor] will be used for remaining items.
     */
    var selectedColors: List<Int> = emptyList()
        set(value) {
            field = value
            adjustColorOfSelected()
        }

    @LayoutRes
    private var layoutRes: Int = 0
    private var mode: Int = 0

    private var dividerWidth: Int = 0

    private var toggleItemForeground: Drawable? = null
    private var toggleItemPaddingPx: Int? = null

    private val onClickListener = OnClickListener { v ->
        val toggle = v.getTag(R.id.tb_toggle_id) as Toggle
        val currentSelection = toggle.isSelected
        if (!allowDeselection && currentSelection) {
            // Do nothing if deselection is not allowed,
            // and currently trying to deselect current selected item
        } else {
            setToggled(toggle.id, !toggle.isSelected)
            onToggledListener?.invoke(toggle, toggle.isSelected)
        }
    }

    /**
     * Listen for when toggles get selected and deselected
     */
    var onToggledListener: ((toggle: Toggle, selected: Boolean) -> Unit)? = null

    interface OnIndexToggledListener {
        fun onIndexToggled(index: Int)
    }

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        linearLayout = ToggleContainer(context)
        addView(linearLayout)

        withMultiToggleStyledAttributes(attrs) {
            mode = getIntOrThrow(R.styleable.MultiToggle_toggleMode)
            multipleSelection = getBooleanOrThrow(R.styleable.MultiToggle_multipleSelection)
            allowDeselection = getBooleanOrThrow(R.styleable.MultiToggle_allowDeselection)
            toggleItemPaddingPx =
                getDimensionPixelSizeOrThrow(R.styleable.MultiToggle_toggleItemPadding)
            toggleItemForeground =
                getDrawableOrThrow(R.styleable.MultiToggle_android_selectableItemBackground)
            selectedColor = getColorOrThrow(R.styleable.MultiToggle_selectedColor)
            selectBackground = getBoolean(R.styleable.MultiToggle_selectBackground, true)
            unselectedColor =
                getColor(R.styleable.MultiToggle_unselectedColor, 0).takeIf { it != 0 }
            layoutRes = getResourceIdOrThrow(R.styleable.MultiToggle_customLayout)
            dividerWidth = getDimensionPixelSizeOrThrow(R.styleable.MultiToggle_dividerWidth)
            dividerColor = getColor(R.styleable.MultiToggle_dividerColor, R.color.grey)
            selectedColors = getResourceId(R.styleable.MultiToggle_selectedColors, 0)
                .takeIf { it != 0 }
                ?.let { resources.getIntArray(it) }
                ?.toList()
                .orEmpty()

            // This must be last
            inflateItems()
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        linearLayout.children.forEach { view ->
            if (enabled) {
                view.isEnabled = true
                view.alpha = context.getFloat(R.dimen.alpha_enabled)
            } else {
                if (!view.isSelected) {
                    view.alpha = context.getFloat(R.dimen.alpha_disabled)
                }
                view.isEnabled = false
            }
        }
    }

    /**
     * Sets new [items] to be displayed as [Toggle] on this view. Replaces all previously added items.
     *
     * @param items list of titles for every [Toggle]
     * @param paddingPx padding for every [ToggleView]
     * @param descriptions list of text content descriptions for every [Toggle]
     */
    fun setTextItems(
        items: List<CharSequence>,
        paddingPx: Int,
        descriptions: List<String>? = null
    ) {
        linearLayout.removeAllViews()
        items.mapIndexed { index, item ->
            TextToggle(index, item, descriptions?.get(index) ?: item.toString())
        }.forEach { addToggle(it, paddingPx) }
    }

    /**
     * Sets new [itemsRes] to be displayed as [Toggle] on this view. Replaces all previously added items.
     *
     * @param itemsRes list of image resources for every [Toggle]
     * @param paddingPx padding for every [ToggleView]
     * @param descriptions list of image content descriptions for every [Toggle]
     */
    fun setImageItems(itemsRes: List<Int>, paddingPx: Int, descriptions: List<String>? = null) {
        linearLayout.removeAllViews()
        itemsRes.mapIndexed { index, imageRes ->
            ImageToggle(index, imageRes, descriptions?.get(index) ?: imageRes.toString())
        }.forEach { addToggle(it, paddingPx) }
    }

    /**
     * Add actions to the layout from the given string array resource id.
     *
     * @param paddingPx padding for each [ToggleView]
     * @param resId array res id of the text items to inflate
     * @param descriptionResId list of text content descriptions for every [Toggle]
     */
    private fun inflateTextItems(
        paddingPx: Int,
        @ArrayRes resId: Int,
        @ArrayRes descriptionResId: Int
    ) {
        val items = resources.getStringList(resId)
        val descriptionsList = getDescriptionsList(descriptionResId) ?: items
        setTextItems(items, paddingPx, descriptionsList)
    }

    /**
     * Add actions to the layout from the given int array of image resource id.
     * @param paddingPx padding for each [ToggleView]
     * @param resId array res id of the image items to inflate
     * @param descriptionResId list of image content descriptions for every [Toggle]
     */
    private fun inflateImageItems(
        paddingPx: Int,
        @ArrayRes resId: Int,
        @ArrayRes descriptionResId: Int
    ) {
        val items: TypedArray = resources.obtainTypedArray(resId)
        val itemsList = mutableListOf<Int>()
        for (i in 0 until items.length()) {
            itemsList.add(items.getResourceIdOrThrow(i))
        }
        items.recycle()
        setImageItems(itemsList, paddingPx, getDescriptionsList(descriptionResId))
    }

    /**
     * Get a list of selected toggles
     *
     * @return the selected toggles
     */
    private fun selectedToggles(): List<Toggle> {
        return toggles.filter { it.isSelected }
    }

    /**
     * Add a toggle to the layout
     *
     * @param toggle the toggle to add
     * @param paddingPx padding for the [ToggleView]
     */
    private fun addToggle(toggle: Toggle, paddingPx: Int) {
        toggles.add(toggle)
        val toggleView = toggle.createToggleView(context, paddingPx, layoutRes)
        toggleView.setOnClickListener(onClickListener)
        val dividerColor = dividerColor
        if (dividerColor != null && toggles.size > 1) {
            val divider = View(context)
            divider.importantForAccessibility = IMPORTANT_FOR_ACCESSIBILITY_NO
            divider.contentDescription = TAG_DIVIDER
            divider.setBackgroundColor(dividerColor)
            val params = LinearLayout.LayoutParams(dividerWidth, MATCH_PARENT)
            divider.layoutParams = params
            linearLayout.addView(divider)
        }
        if (mode == MODE_EVEN) {
            val params = LinearLayout.LayoutParams(0, WRAP_CONTENT, 1f)
            toggleView.layoutParams = params
        }
        linearLayout.addView(toggleView)

        ViewCompat.setAccessibilityDelegate(
            toggleView,
            ToggleViewAccessibilityDelegate(resources, allowDeselection)
        )
    }

    /**
     * Reset all toggles to unselected
     */
    private fun reset() {
        for (toggle in toggles) {
            toggle.isSelected = false
            toggle.toggleState()
        }
    }

    /**
     * Manually set the toggled state of the specified toggle.
     *
     * @param toggleId the id of the toggle
     * @param toggled true if should be toggled on, false otherwise
     */
    fun setToggled(toggleId: Int, toggled: Boolean) {
        if (!multipleSelection) {
            reset()
        }
        for (toggle in toggles) {
            if (toggle.id == toggleId) {
                toggle.isSelected = toggled
                toggle.toggleState()
                break
            }
        }
    }

    /**
     * Manually set the toggled state of the specified toggle by its index.
     * Will do nothing if index is out of bounds
     *
     * @param index the index of the toggle
     * @param toggled true if should be toggled on, false otherwise
     */
    fun setToggledByIndex(index: Int, toggled: Boolean) {
        val selectedToggleId = toggleIds.getOrNull(index) ?: return
        setToggled(selectedToggleId, toggled)
    }

    /**
     * Manually set the toggled state of the specified toggle.
     *
     * @param toggleTitle the title of the toggle
     * @param toggled true if should be toggled on, false otherwise
     */
    fun setToggledWithTitle(toggleTitle: String, toggled: Boolean) {
        if (!multipleSelection) {
            reset()
        }
        toggles.asSequence()
            .filterIsInstance<TextToggle>()
            .firstOrNull { it.title == toggleTitle }
            ?.apply {
                isSelected = toggled
                toggleState()
            }
    }

    private fun getDescriptionsList(descriptionResId: Int) = if (descriptionResId > 0) {
        resources.getStringList(descriptionResId)
    } else {
        null
    }

    private fun adjustColorOfSelected() {
        selectedToggles().forEach {
            linearLayout.findViewById<ToggleView>(it.id)
                .applyColor(getSelectedColor(it), selectBackground)
        }
    }

    private fun Toggle.toggleState() {
        val view = linearLayout.findViewById<ToggleView>(id)
        view.isSelected = isSelected
        val color = getSelectedColor(this)
        if (isSelected) {
            view.alpha = context.getFloat(R.dimen.alpha_enabled)
            view.applyColor(color, selectBackground)
        } else {
            unselectedColor?.let { view.applyColor(it, selectBackground) }
        }
    }

    private fun TypedArray.inflateItems() {
        // inflate text items
        getItemsAndInflate(
            R.styleable.MultiToggle_textItems,
            toggleItemPaddingPx ?: 0,
            isImageItems = false
        )
        // inflate image items
        getItemsAndInflate(
            R.styleable.MultiToggle_imageItems,
            toggleItemPaddingPx ?: 0,
            isImageItems = true
        )
    }

    private fun withMultiToggleStyledAttributes(
        attrs: AttributeSet?,
        block: TypedArray.() -> Unit
    ) {
        context.withStyledAttributes(
            set = attrs,
            attrs = R.styleable.MultiToggle,
            defStyleRes = R.style.MultiToggle
        ) {
            block.invoke(this)
        }
    }

    private fun TypedArray.getItemsAndInflate(
        @StyleableRes itemsRes: Int,
        paddingPx: Int,
        isImageItems: Boolean
    ) {
        getResourceId(itemsRes, 0)
            .takeIf { it != 0 }
            ?.also {
                val descriptionsResId = getResourceId(R.styleable.MultiToggle_itemsDescriptions, 0)
                if (isImageItems) {
                    inflateImageItems(paddingPx, it, descriptionsResId)
                } else {
                    inflateTextItems(paddingPx, it, descriptionsResId)
                }
            }
    }

    @ColorInt
    private fun getSelectedColor(toggle: Toggle): Int =
        selectedColors.getOrElse(toggle.id) { selectedColor }
}

@BindingAdapter("toggledIndex")
fun setToggledIndex(multiToggle: MultiToggle, index: Int) {
    multiToggle.setToggled(index, true)
}

@BindingAdapter("onToggle")
fun setOnToggleListener(multiToggle: MultiToggle, listener: MultiToggle.OnIndexToggledListener) {
    multiToggle.onToggledListener = { toggle, isSelected ->
        if (isSelected) {
            listener.onIndexToggled(toggle.id)
        }
    }
}

private fun Resources.getStringList(@ArrayRes resId: Int) = getStringArray(resId).toList()
