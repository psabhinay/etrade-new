package com.etrade.mobilepro.trade

import android.app.Activity
import android.content.Intent
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.navOptions
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dynamic.form.fragment.createFormValuesArgument
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.navigation.portfolioTabDirections
import com.etrade.mobilepro.order.details.router.OrderConfirmationAction
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.orders.OrderDetailsFragmentArgs
import com.etrade.mobilepro.orders.TradeOrderDetailsFragmentDirections
import com.etrade.mobilepro.trade.api.TradeOptionInfoType
import com.etrade.mobilepro.trade.presentation.TradeFragmentDirections
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.navigation.hasDestinationInBackStack
import com.squareup.moshi.Moshi
import javax.inject.Inject

private const val EXTENDED_HOURS_DISCLOSURE_URL = "/e/t/invest/ehdisclosureformsapps"
private const val OTC_DISCLOSURE_URL = "/etx/trd/tradingcenter?otcDisclosureSigned=false&isEtmobile=true"
private const val LIETF_DISCLOSURE_URL = "/etx/trd/tradingcenter?etnDisclosureSigned=false&isEtmobile=true "
private const val MRB_DISCLOSURE_URL = "/etx/trd/tradingcenter?mrbDisclosureSigned=false&isEtmobile=true"

class DefaultTradeRouter @Inject constructor(
    @Web private val baseUrl: String,
    private val moshi: Moshi,
    private val userViewModel: UserViewModel
) : TradeRouter {

    override val navGraphId: Int = R.id.trade_navigation_graph

    override fun openConfirmation(navController: NavController, orderId: String, action: OrderConfirmationAction) {
        navController.navigate(
            TradeOrderDetailsFragmentDirections.actionTradeOrderDetailsFragmentToOrderConfirmationFragment(orderId, action),
            navOptions {
                val destinationId = when {
                    navController.hasDestinationInBackStack(R.id.orderDetailsFragment) -> R.id.orderDetailsFragment
                    navController.graph.id == R.id.chartiq_internal_nav_graph ||
                        navController.graph.id == R.id.trade_bottom_sheet_navigation_graph -> R.id.tradeFragment
                    else -> R.id.tradeOrderDetailsFragment
                }
                popUpTo(destinationId) {
                    inclusive = true
                }
            }
        )
    }

    override fun openPreview(navController: NavController, argument: OrderDetailsArgument) {
        navController.navigate(
            R.id.action_tradeFragment_to_tradeOrderDetailsFragment,
            OrderDetailsFragmentArgs(argument).toBundle()
        )
    }

    override fun openCalculator(navController: NavController) {
        navController.navigate(TradeFragmentDirections.actionGlobalTradeCalculatorFragment())
    }

    override fun openPriceTypeDescriptions(navController: NavController) {
        navController.navigate(TradeFragmentDirections.actionGlobalLearnPriceTypesFragment())
    }

    override fun openPortfolio(activity: Activity, accountId: String) {
        val accountUuid = findAccountUuid(accountId)
        if (activity is MainActivity) {
            activity.findNavController(R.id.nav_host_fragment).run {
                if (accountUuid != null) {
                    navigateToPortfolio(accountId, accountUuid)
                } else {
                    popTrade()
                }
            }
        } else {
            activity.launchMainActivity()
            if (accountUuid != null) {
                userViewModel.navigateToPortfolio(accountId, accountUuid)
            }
        }
    }

    override fun openHelp(navController: NavController, tradeOptionInfoType: TradeOptionInfoType, learnOtherPriceTypes: Boolean) {
        navController.navigate(TradeFragmentDirections.actionToTradeHelpFragment(tradeOptionInfoType, learnOtherPriceTypes))
    }

    private fun Activity.launchMainActivity() {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        this.startActivity(intent)
    }

    override fun editConditionalOrder(
        navController: NavController,
        orderInputId: String,
        orderNumber: Int,
        accountId: String?,
        snapshot: InputValuesSnapshot?
    ) {
        val arguments = snapshot?.let { createFormValuesArgument(moshi, snapshot) }
        navController.navigate(
            TradeFragmentDirections.actionTradeFragmentToConditionalOrderFormFragment(
                accountId = accountId,
                FormFragmentFormValues = arguments,
                isMainTradePage = navController.graph.id == R.id.mainGraph,
                orderInputId = orderInputId,
                orderNumber = orderNumber
            )
        )
    }

    override fun exitConfirmation(activity: Activity) {
        activity.findNavController(R.id.nav_host_fragment).popBackStack()
    }

    private fun findAccountUuid(accountId: String): String? {
        return userViewModel.tradeEligibleAccounts.find {
            it.account?.accountId == accountId
        }?.account?.accountUuid
    }

    private fun NavController.navigateToPortfolio(accountId: String, accountUuid: String) {
        navigate(
            portfolioTabDirections(
                accountId = accountId,
                accountUuid = accountUuid
            )
        )
    }

    private fun NavController.popTrade() {
        popBackStack(R.id.trade_navigation_graph, true)
    }

    override fun openExtendedHoursDisclosure(navController: NavController, title: String) {
        navController.navigate(
            TradeFragmentDirections.actionTradeFragmentToExtendedHoursDisclosure(
                title = title,
                url = "${baseUrl}$EXTENDED_HOURS_DISCLOSURE_URL"
            )
        )
    }

    override fun openOtcDisclosure(navController: NavController, title: String) {
        navController.navigate(
            TradeFragmentDirections.actionTradeFragmentAgreement(
                title,
                "${baseUrl}$OTC_DISCLOSURE_URL"
            )
        )
    }

    override fun openMRBDisclosure(navController: NavController, title: String) {
        navController.navigate(
            TradeFragmentDirections.actionTradeFragmentAgreement(
                title,
                "${baseUrl}$MRB_DISCLOSURE_URL"
            )
        )
    }

    override fun openLiEtfDisclosure(navController: NavController, title: String) {
        navController.navigate(
            TradeFragmentDirections.actionTradeFragmentToLiEtfAgreement(
                title,
                "${baseUrl}$LIETF_DISCLOSURE_URL"
            )
        )
    }
}
