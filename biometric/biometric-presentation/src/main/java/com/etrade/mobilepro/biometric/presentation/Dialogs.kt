package com.etrade.mobilepro.biometric.presentation

import android.content.res.Resources
import android.text.InputType
import androidx.annotation.StringRes
import androidx.biometric.BiometricPrompt
import com.etrade.mobilepro.edittextdialog.EditTextDialogFragment

enum class BiometricPromptType { REGISTER, AUTHENTICATE }

private const val BIOMETRIC_FRAGMENT_TAG = "BIOMETRIC_FRAGMENT"
private const val PASSWORD_CHANGED_DIALOG_TAG = "PASSWORD_CHANGED_DIALOG_TAG"
private const val RSA_CONFIRM_DIALOG_TAG = "RSA_CONFIRM_DIALOG_TAG"
private const val PASSWORD_RSA_CONFIRM_DIALOG_TAG = "PASSWORD_RSA_CONFIRM_DIALOG_TAG"
private const val PASSWORD_CONFIRM_DIALOG_TAG = "PASSWORD_CONFIRM_DIALOG_TAG"

fun buildPasswordChangedDialog(
    positiveListener: ((String) -> Unit),
    dismissListener: (() -> Unit),
    showListener: (() -> Unit)
): EditTextDialogFragment.Builder =
    EditTextDialogFragment.Builder(PASSWORD_CHANGED_DIALOG_TAG).apply {
        hint(R.string.rsa_password_change_hint)
        title(R.string.rsa_password_change_title)
        message(R.string.rsa_password_change_message)
        positiveButton(R.string.enter, positiveListener)
        setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
        negativeButton(R.string.cancel)
        dismissListener(dismissListener)
        showListener(showListener)
        cancellable(false)
    }

@SuppressWarnings("LongParameterList")
fun buildRsaConfirmDialog(
    positiveListener: (String) -> Unit,
    negativeListener: () -> Unit,
    actionButton1Listener: () -> Unit,
    actionButton2Listener: () -> Unit,
    dismissListener: () -> Unit,
    showListener: () -> Unit
): EditTextDialogFragment.Builder =
    EditTextDialogFragment.Builder(RSA_CONFIRM_DIALOG_TAG).apply {
        hint(R.string.rsa_login_dialog_hint)
        title(R.string.rsa_login_dialog_title)
        message(R.string.rsa_login_dialog_body)
        positiveButton(R.string.enter, positiveListener)
        negativeButton(R.string.cancel, negativeListener)
        actionButton1(R.string.rsa_login_dialog_password_change, actionButton1Listener)
        actionButton2(R.string.rsa_login_dialog_no_longer_security_id, actionButton2Listener)
        setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
        dismissListener(dismissListener)
        showListener(showListener)
        cancellable(false)
    }

internal fun buildPasswordRsaConfirmDialog(positiveListener: ((String) -> Unit), negativeListener: (() -> Unit)): EditTextDialogFragment.Builder =
    EditTextDialogFragment.Builder(PASSWORD_RSA_CONFIRM_DIALOG_TAG).apply {
        hint(R.string.rsa_verification_hint)
        message(R.string.rsa_verification_title)
        positiveButton(R.string.enable, positiveListener)
        negativeButton(R.string.cancel, negativeListener)
        setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
        cancellable(false)
    }

internal fun buildPasswordConfirmDialog(positiveListener: ((String) -> Unit), negativeListener: (() -> Unit)): EditTextDialogFragment.Builder {
    return EditTextDialogFragment.Builder(PASSWORD_CONFIRM_DIALOG_TAG).apply {
        hint(R.string.password)
        title(R.string.password_verification_title)
        message(R.string.password_verification_message)
        positiveButton(R.string.enable, positiveListener)
        negativeButton(R.string.enable_biometrics_dialog_negative, negativeListener)
        setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
        cancellable(false)
    }
}

internal fun Resources.getBiometricPromptInfo(type: BiometricPromptType): BiometricPrompt.PromptInfo {
    return when (type) {
        BiometricPromptType.AUTHENTICATE -> {
            getBiometricPromptInfo(R.string.biometric_authentication, R.string.biometric_authentication_reminder)
        }
        BiometricPromptType.REGISTER -> {
            getBiometricPromptInfo(R.string.register_biometric_prompt_title, R.string.biometric_authentication_reminder)
        }
    }
}

private fun Resources.getBiometricPromptInfo(
    @StringRes titleRes: Int,
    @StringRes messageRes: Int,
    @StringRes cancelRes: Int = R.string.cancel
): BiometricPrompt.PromptInfo {
    return BiometricPrompt.PromptInfo.Builder().apply {
        setTitle(getString(titleRes))
        setDescription(getString(messageRes))
        setNegativeButtonText(getString(cancelRes))
    }.build()
}
