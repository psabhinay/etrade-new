package com.etrade.mobilepro.dagger.qualifiers

import javax.inject.Qualifier

/**
 * Qualifies an object as the one that is guarded by `ApiGuard`.
 */
@Qualifier
annotation class GuardedApi
