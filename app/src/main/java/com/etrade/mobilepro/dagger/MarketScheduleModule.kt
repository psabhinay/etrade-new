package com.etrade.mobilepro.dagger

import com.etrade.eo.marketschedule.MarketScheduleProviderImpl
import com.etrade.eo.marketschedule.api.MarketScheduleProvider
import com.etrade.eo.queueprocessor.SerializingScheduler
import com.etrade.eo.streaming.SubscriptionManager
import com.etrade.mobilepro.common.di.Level1Streamer
import com.etrade.mobilepro.marketschedule.MarketScheduleProviderHolder
import com.etrade.mobilepro.marketschedule.MarketScheduleProviderHolderImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class MarketScheduleModule {

    @Binds
    @Singleton
    abstract fun bindMarketScheduleProviderHolder(provider: MarketScheduleProviderHolderImpl): MarketScheduleProviderHolder

    companion object {

        @Provides
        internal fun provideMarketScheduleProvider(
            @Level1Streamer subscriptionManager: SubscriptionManager,
            serialScheduler: SerializingScheduler
        ): MarketScheduleProvider {
            return MarketScheduleProviderImpl(
                subscriptionManager = subscriptionManager,
                serialScheduler = serialScheduler
            )
        }
    }
}
