package com.etrade.mobilepro.data.mapper

import com.etrade.mobilepro.alerts.api.model.AlertGroup
import com.etrade.mobilepro.alerts.data.dto.InboxEventDto
import com.etrade.mobilepro.alerts.data.mapper.AlertDtoMapper
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class AlertDtoMapperTest {
    private val allNullsInput = InboxEventDto(
        null, null, null, null, null
    )
    private val testInput = InboxEventDto(
        id = "id1",
        subject = "subj1",
        wasRead = "1",
        alertCode = "1",
        occurredAtEpochTimestamp = 1552560423L
    )

    private val testInputAccountType1 = InboxEventDto(
        id = "id1",
        alertCode = "599"
    )

    private val testInputAccountType2 = InboxEventDto(
        id = "id1",
        alertCode = "598"
    )

    private val testInputAccountType3 = InboxEventDto(
        id = "id1",
        alertCode = "901"
    )

    private val testInputAccountType4 = InboxEventDto(
        id = "id1",
        alertCode = "900"
    )

    private val testInputMarketType1 = InboxEventDto(
        id = "id1",
        alertCode = "600"
    )

    private val testInputMarketType2 = InboxEventDto(
        id = "id1",
        alertCode = "899"
    )

    private val testInputMarketType3 = InboxEventDto(
        id = "id1",
        alertCode = "898"
    )

    private val sut = AlertDtoMapper()

    @Test
    fun `null alert code is mapped to null`() {
        val output = sut.map(allNullsInput)
        assertTrue(output == null)
    }

    @Test
    fun `success case`() {
        val output = sut.map(testInput)
        if (output != null) {
            assertTrue(output.id == "id1")
            assertTrue(output.subject == "subj1")
            assertTrue(output.status.wasRead)
        } else {
            assertFalse(true)
        }
    }

    @Test
    fun `date mapping`() {
        val output = sut.map(testInput)
        if (output != null) {
            assertTrue(output.occurredAtFormatted == "03/14/2019 | 06:47 AM EDT", output.occurredAtFormatted)
        } else {
            assertFalse(true)
        }
    }

    @Test
    fun accountAlertGroup() {
        val out1 = sut.map(testInputAccountType1)

        assertTrue(out1?.belongsTo() == AlertGroup.ACCOUNT)

        val out2 = sut.map(testInputAccountType2)

        assertTrue(out2?.belongsTo() == AlertGroup.ACCOUNT)

        val out3 = sut.map(testInputAccountType3)

        assertTrue(out3?.belongsTo() == AlertGroup.ACCOUNT)

        val out4 = sut.map(testInputAccountType4)

        assertTrue(out4?.belongsTo() == AlertGroup.ACCOUNT)

        val out5 = sut.map(testInputMarketType1)

        assertTrue(out5?.belongsTo() == AlertGroup.MARKET)

        val out6 = sut.map(testInputMarketType2)

        assertTrue(out6?.belongsTo() == AlertGroup.MARKET)

        val out7 = sut.map(testInputMarketType3)

        assertTrue(out7?.belongsTo() == AlertGroup.MARKET)
    }
}
