package com.etrade.mobilepro.util.android

import androidx.annotation.ColorRes
import java.math.BigDecimal

@ColorRes
fun BigDecimal.displayColor() = if (this.signum() < 0) {
    R.color.red
} else {
    R.color.green
}

interface ChangeColorPicker {
    @ColorRes
    fun getChangeColor(changeValue: BigDecimal?): Int
}

class DefaultChangeColorPicker(
    @ColorRes
    val positiveColor: Int,
    @ColorRes
    val negativeColor: Int,
    @ColorRes
    val neutralColor: Int
) : ChangeColorPicker {
    override fun getChangeColor(changeValue: BigDecimal?): Int = changeValue?.let {
        when (it.signum()) {
            -1 -> negativeColor
            else -> positiveColor
        }
    } ?: neutralColor
}
