package com.etrade.mobilepro.trade.calculator.view

import android.content.Context
import android.util.AttributeSet
import com.etrade.mobilepro.trade.calculator.R

class CalculatorAmountInputLayout : CalculatorInputLayout {

    constructor(ctx: Context) : super(ctx)

    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet)

    override val formatFieldStringRes = R.string.trade_calculator_amount
}
