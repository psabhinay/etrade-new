package com.etrade.mobilepro.dagger

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.di.IsTablet
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.etrade.mobilepro.util.android.darkmode.DefaultDarkModeChecker
import com.etrade.mobilepro.util.android.fragment.DefaultNavScopedStorage
import com.etrade.mobilepro.util.android.fragment.NavScopedStorageRegisterer
import com.etrade.mobilepro.util.android.fragment.OrientationLock
import com.etrade.mobilepro.util.android.fragment.OrientationLockImpl
import com.etrade.mobilepro.util.android.fragment.OrientationLockViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Singleton

/* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */

@Module
abstract class UtilAndroidModule {

    @Binds
    @Singleton
    abstract fun bindDarkModeChecker(checker: DefaultDarkModeChecker): DarkModeChecker

    @Binds
    @IntoMap
    @ViewModelKey(OrientationLockViewModel::class)
    abstract fun bindOrientationLockViewModel(viewModel: OrientationLockViewModel): ViewModel

    @Binds
    @Singleton
    abstract fun bindNavScopedStorage(storage: DefaultNavScopedStorage): NavScopedStorageRegisterer

    companion object {
        @Provides
        fun provideOrientationLock(
            @IsTablet isTablet: Boolean,
            resources: Resources,
            viewModelFactory: ViewModelProvider.Factory
        ): OrientationLock {
            return OrientationLockImpl(resources, viewModelFactory) { isTablet }
        }
    }
}
