package com.etrade.mobilepro.transfermoney.presentation.transfer

import com.etrade.mobilepro.common.di.DeviceType
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

internal const val SCREEN_TITLE_EXTERNAL_ACCOUNTS = "External Accounts"
internal const val EXTERNAL_ACCOUNTS_URL = "/etx/mm/movemoney/manage-accounts"

class ExternalAccountsWebViewFragment @Inject constructor(@DeviceType deviceType: String) : WebViewFragment(deviceType)
