package com.etrade.mobilepro.markets

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MARKETS_CALENDAR
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MARKETS_OVERVIEW_AUTHENTICATED
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MARKETS_OVERVIEW_UNAUTHENTICATED
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MARKETS_SCREENER
import com.etrade.mobilepro.tracking.TabsHostScreenViewModel
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelper
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

class MarketsViewModel @Inject constructor(
    override val viewPagerTabsTrackerHelper: ViewPagerTabsTrackerHelper,
    private val userViewModel: UserViewModel
) : ViewModel(), TabsHostScreenViewModel {

    override val tabNames: Array<String> =
        arrayOf(
            screenName,
            SCREEN_TITLE_MARKETS_CALENDAR,
            SCREEN_TITLE_MARKETS_SCREENER
        )

    val screenName: String
        get() = if (userViewModel.isAuthenticated) {
            SCREEN_TITLE_MARKETS_OVERVIEW_AUTHENTICATED
        } else {
            SCREEN_TITLE_MARKETS_OVERVIEW_UNAUTHENTICATED
        }

    private val tabsRequiringAuth = setOf(
        SCREEN_TITLE_MARKETS_CALENDAR,
        SCREEN_TITLE_MARKETS_SCREENER
    )
    private val _loginSignal = MutableLiveData<ConsumableLiveEvent<TabIndex>>()
    internal val loginSignal: LiveData<ConsumableLiveEvent<TabIndex>>
        get() = _loginSignal
    private val _openTabSignal = MutableLiveData<ConsumableLiveEvent<TabIndex>>()
    internal val openTabSignal: LiveData<ConsumableLiveEvent<TabIndex>>
        get() = _openTabSignal

    override fun trackTab(position: Int) {
        if (!userViewModel.isAuthenticated && tabRequiresAuth(position) && viewPagerTabsTrackerHelper.lastTabPosition != position) {
            _loginSignal.value = ConsumableLiveEvent(TabIndex(position))
            userViewModel.onLoginAbortedListenerSet.add(this::onLoginAborted)
        }
        viewPagerTabsTrackerHelper.trackTab(position, tabNames[position])
    }

    internal fun tabRequiresAuth(tabPosition: Int): Boolean = tabsRequiringAuth.contains(tabNames.elementAtOrNull(tabPosition))

    private fun onLoginAborted(): Boolean {
        _openTabSignal.value = ConsumableLiveEvent(TabIndex(0))
        return true
    }
}

@JvmInline
internal value class TabIndex(val index: Int)
