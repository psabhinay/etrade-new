package com.etrade.mobilepro.caching

import com.etrade.eo.core.rx.RetryWithBackoff
import com.etrade.mobilepro.util.Resource
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.concurrent.TimeUnit

class CachedResourceTest {
    private val testId = "TestId"
    private val domainItem = DomainItem(id = testId, data = listOf("elem1", "elem2"))
    private val networkItem = NetworkItem(id = testId, data = listOf("elem1, elem2"))
    private val dbItem = DbItem(id = testId)

    @Mock
    private lateinit var dataDelegate: CachedResource.DataSourceDelegate<DomainItem, NetworkItem, DbItem>

    @Mock
    private lateinit var controller: CachedResource.Controller<DomainItem>

    private lateinit var sut: CachedResource<DomainItem, NetworkItem, DbItem>

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        sut = CachedResource(
            testId, dataDelegate, controller,
            RetryWithBackoff(
                maxRetries = 1,
                delay = 1, timeUnit = TimeUnit.MILLISECONDS, failOnExhaustion = true
            )
        )
        whenever(dataDelegate.mapNetworkResponse(networkItem)).thenReturn(domainItem)
        whenever(dataDelegate.mapCacheResponse(dbItem)).thenReturn(domainItem)
    }

    @Test
    fun `loading an item should result in cache checking`() {
        whenever(dataDelegate.fetchFromDb(testId)).thenReturn(Single.just(dbItem))
        whenever(dataDelegate.fetchFromNetwork(testId)).thenReturn(Single.just(networkItem))
        whenever(dataDelegate.putToCache(domainItem)).thenReturn(Single.just(domainItem))
        val testObserver = TestObserver<Resource<DomainItem>>()

        sut.asObservable().blockingSubscribe(testObserver)

        verify(dataDelegate, times(1)).fetchFromDb(testId)
    }

    @Test
    fun `loading a NOT existent in cache item should result in network request and putting item to cache`() {
        whenever(dataDelegate.fetchFromDb(testId)).thenReturn(Single.error(NoSuchElementException()))
        whenever(dataDelegate.fetchFromNetwork(testId)).thenReturn(Single.just(networkItem))
        whenever(dataDelegate.putToCache(domainItem)).thenReturn(Single.just(domainItem))
        whenever(controller.shouldPutToCache(any())).thenReturn(true)
        val testObserver = TestObserver<Resource<DomainItem>>()

        sut.asObservable().blockingSubscribe(testObserver)

        verify(dataDelegate, times(1)).fetchFromNetwork(testId)
        verify(dataDelegate, times(1)).putToCache(any())
    }

    @Test
    fun `loading an existent in cache item should trigger calling the Controller's shouldRefresh()`() {
        whenever(dataDelegate.fetchFromDb(testId)).thenReturn(Single.just(dbItem))
        whenever(dataDelegate.fetchFromNetwork(testId)).thenReturn(Single.just(networkItem))
        whenever(dataDelegate.putToCache(domainItem)).thenReturn(Single.just(domainItem))
        val testObserver = TestObserver<Resource<DomainItem>>()

        sut.asObservable().blockingSubscribe(testObserver)

        verify(dataDelegate, times(1)).fetchFromDb(testId)
        verify(controller, times(1)).shouldRefresh(any())
    }

    @Test
    fun `loading an existent in cache item and the shouldRefresh() == FALSE should result in returned cached item, no network calls or cache updates`() {
        whenever(dataDelegate.fetchFromDb(testId)).thenReturn(Single.just(dbItem))
        whenever(dataDelegate.fetchFromNetwork(testId)).thenReturn(Single.just(networkItem))
        whenever(dataDelegate.putToCache(domainItem)).thenReturn(Single.just(domainItem))
        whenever(controller.shouldRefresh(any())).thenReturn(false)
        val testObserver = TestObserver<Resource<DomainItem>>()

        sut.asObservable().blockingSubscribe(testObserver)

        verify(dataDelegate, times(1)).fetchFromDb(testId)
        verify(dataDelegate, times(0)).fetchFromNetwork(any())
        verify(dataDelegate, times(0)).putToCache(any())
        testObserver.assertComplete()
        testObserver.assertValueCount(2)
        testObserver.assertValueAt(0) {
            it is Resource.Loading<DomainItem> && it.data == null
        }
        testObserver.assertValueAt(1) {
            it is Resource.Success<DomainItem> && it.data == domainItem
        }
    }

    @Test
    fun `loading an existent in cache item and the shouldRefresh() == TRUE should result in network call and cache update`() {
        whenever(dataDelegate.fetchFromDb(testId)).thenReturn(Single.just(dbItem))
        whenever(dataDelegate.fetchFromNetwork(testId)).thenReturn(Single.just(networkItem))
        whenever(dataDelegate.putToCache(domainItem)).thenReturn(Single.just(domainItem))
        whenever(controller.shouldRefresh(any())).thenReturn(true)
        whenever(controller.shouldPutToCache(any())).thenReturn(true)
        val testObserver = TestObserver<Resource<DomainItem>>()

        sut.asObservable().blockingSubscribe(testObserver)

        verify(dataDelegate, times(1)).fetchFromDb(testId)
        verify(dataDelegate, times(1)).fetchFromNetwork(testId)
        verify(dataDelegate, times(1)).putToCache(domainItem)
        testObserver.assertComplete()
        testObserver.assertValueCount(3)
        testObserver.assertValueAt(0) {
            it is Resource.Loading<DomainItem> && it.data == null
        }
        testObserver.assertValueAt(1) {
            it is Resource.Loading<DomainItem> && it.data == domainItem
        }
        testObserver.assertValueAt(2) {
            it is Resource.Success<DomainItem> && it.data == domainItem
        }
    }

    @Test
    fun `item present in cache, shouldRefresh() == TRUE, network failure should result in Failed state, no cache update`() {
        whenever(dataDelegate.fetchFromDb(testId)).thenReturn(Single.just(dbItem))
        whenever(dataDelegate.fetchFromNetwork(testId)).thenReturn(Single.error(IllegalStateException("boom!")))
        whenever(dataDelegate.putToCache(domainItem)).thenReturn(Single.just(domainItem))
        whenever(controller.shouldRefresh(any())).thenReturn(true)
        val testObserver = TestObserver<Resource<DomainItem>>()

        sut.asObservable().blockingSubscribe(testObserver)

        testObserver.assertComplete()
        testObserver.assertValueCount(3)
        testObserver.assertValueAt(0) {
            it is Resource.Loading<DomainItem> && it.data == null
        }
        testObserver.assertValueAt(1) {
            it is Resource.Loading<DomainItem> && it.data == domainItem
        }
        testObserver.assertValueAt(2) {
            it is Resource.Failed<DomainItem> && it.data == null && it.error is IllegalStateException && it.error?.message == "boom!"
        }
    }

    @Test
    fun `initial db fetch error should result in network request`() {
        whenever(dataDelegate.fetchFromDb(testId)).thenReturn(Single.error(IllegalStateException("boom!")))
        whenever(dataDelegate.fetchFromNetwork(testId)).thenReturn(Single.just(networkItem))
        whenever(dataDelegate.putToCache(domainItem)).thenReturn(Single.just(domainItem))
        whenever(controller.shouldRefresh(any())).thenReturn(true)
        val testObserver = TestObserver<Resource<DomainItem>>()

        sut.asObservable().blockingSubscribe(testObserver)

        verify(dataDelegate, times(1)).fetchFromNetwork(testId)
        testObserver.assertComplete()
        testObserver.assertValueCount(3)
        testObserver.assertValueAt(0) {
            it is Resource.Loading<DomainItem> && it.data == null
        }
        testObserver.assertValueAt(1) {
            it is Resource.Loading<DomainItem> && it.data == null
        }
        testObserver.assertValueAt(2) {
            it is Resource.Success<DomainItem> && it.data == domainItem
        }
    }

    @Test
    fun `db malfunction should result in network request and network-originated item delivery`() {
        whenever(dataDelegate.fetchFromDb(testId)).thenReturn(Single.error(IllegalStateException("boom!")))
        whenever(dataDelegate.fetchFromNetwork(testId)).thenReturn(Single.just(networkItem))
        whenever(dataDelegate.putToCache(domainItem)).thenReturn(Single.error(IllegalStateException("boom!")))
        whenever(controller.shouldRefresh(any())).thenReturn(true)
        val testObserver = TestObserver<Resource<DomainItem>>()

        sut.asObservable().blockingSubscribe(testObserver)

        verify(dataDelegate, times(1)).fetchFromNetwork(testId)
        testObserver.assertComplete()
        testObserver.assertValueCount(3)
        testObserver.assertValueAt(0) {
            it is Resource.Loading<DomainItem> && it.data == null
        }
        testObserver.assertValueAt(1) {
            it is Resource.Loading<DomainItem> && it.data == null
        }
        testObserver.assertValueAt(2) {
            it is Resource.Success<DomainItem> && it.data == domainItem
        }
    }

    @Test
    fun `if shouldPutToCache returns false, item should not be put into cache`() {
        whenever(dataDelegate.fetchFromDb(testId)).thenReturn(Single.just(dbItem))
        whenever(dataDelegate.fetchFromNetwork(testId)).thenReturn(Single.just(networkItem))
        whenever(controller.shouldRefresh(any())).thenReturn(true)
        whenever(controller.shouldPutToCache(any())).thenReturn(false)
        val testObserver = TestObserver<Resource<DomainItem>>()

        sut.asObservable().blockingSubscribe(testObserver)

        verify(dataDelegate, times(1)).fetchFromNetwork(testId)
        verify(dataDelegate, times(0)).putToCache(any())
        testObserver.assertComplete()
        testObserver.assertValueCount(3)
        testObserver.assertValueAt(0) {
            it is Resource.Loading<DomainItem> && it.data == null
        }
        testObserver.assertValueAt(1) {
            it is Resource.Loading<DomainItem> && it.data == domainItem
        }
        testObserver.assertValueAt(2) {
            it is Resource.Success<DomainItem> && it.data == domainItem
        }
    }

    private data class DomainItem(
        val id: String,
        val data: List<String>
    )

    private data class NetworkItem(
        val id: String?,
        val data: List<String>?
    )

    private data class DbItem(
        val id: String
    )
}
