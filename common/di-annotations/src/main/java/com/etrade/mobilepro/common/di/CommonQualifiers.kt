package com.etrade.mobilepro.common.di

import javax.inject.Qualifier

@Qualifier
annotation class AccountType

@Qualifier
annotation class AddExternalAccount

@Qualifier
annotation class ApplicationScope

@Qualifier
annotation class CheckDepositServiceAgreement

@Qualifier
annotation class ClientSideCalculations

@Qualifier
annotation class CompleteView

@Qualifier
annotation class CurrentEnvironmentName

@Qualifier
annotation class Default

@Qualifier
annotation class DefaultKeyStore

@Qualifier
annotation class DeviceInfo

@Qualifier
annotation class DeviceId

@Qualifier
annotation class DeviceType

@Qualifier
annotation class LocalDynamicScreen

@Qualifier
annotation class RemoteDynamicScreen

@Qualifier
annotation class CompleteViewDynamicScreen

@Qualifier
annotation class DynamicMenuScreen

@Qualifier
annotation class DynamicMenuScreenDataSourceDelegate

@Qualifier
annotation class EntryPointActivity

@Qualifier
annotation class EnvironmentName

@Qualifier
annotation class IsTablet

@Qualifier
annotation class OpenNewAccount

@Qualifier
annotation class PersonalNotifications

@Qualifier
annotation class Unauthenticated

@Qualifier
annotation class ViewFactory
