package com.etrade.mobilepro.chartengine

sealed class TimeFrame(private val periodCode: String, private val landscapePeriodCode: String, val displayName: String) {
    object Day : TimeFrame(".d00", ".d20", "1D")
    object ThreeDays : TimeFrame(".d01", ".d21", "3D")
    object OneWeek : TimeFrame(".d02", ".d22", "1W")
    object OneMonth : TimeFrame(".d03", ".d23", "1M")
    object ThreeMonths : TimeFrame(".d04", ".d24", "3M")
    object SixMonths : TimeFrame(".d05", ".d25", "6M")
    object OneYear : TimeFrame(".d08", ".d28", "1Y")
    object TwoYears : TimeFrame(".d09", ".d29", "2Y")
    object ThreeYears : TimeFrame(".d10", ".d30", "3Y")
    object ExtendedHour : TimeFrame(".d15", ".d35", "1D")

    fun getPeriodString(isLandScape: Boolean): String {
        return if (isLandScape) {
            landscapePeriodCode
        } else {
            periodCode
        }
    }
}

sealed class ChartType(val typeCode: String) {
    object Line : ChartType(".s4")
    object Bar : ChartType(".s3")
    object CandleStick : ChartType(".s2")
    object Mountain : ChartType(".s1")
}

fun getFrequencyCode(chartType: ChartType = ChartType.Line, timeFrame: TimeFrame): String {
    return when (timeFrame) {
        TimeFrame.ThreeDays -> ".f6"
        TimeFrame.OneWeek -> ""
        TimeFrame.OneMonth -> ".f2"
        TimeFrame.ThreeMonths -> ".f2"
        TimeFrame.SixMonths -> ".f2"
        TimeFrame.OneYear -> ".f1"
        TimeFrame.TwoYears -> ".f1"
        TimeFrame.ThreeYears -> ".f1"
        else ->
            if (chartType == ChartType.Bar || chartType == ChartType.CandleStick) {
                ".f4"
            } else {
                ".f6"
            }
    }
}
