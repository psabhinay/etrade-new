package com.etrade.mobilepro.dropdown

import android.content.res.Resources
import android.os.Parcelable
import com.etrade.mobilepro.util.WithContentDescription
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import kotlinx.parcelize.Parcelize

interface DropDownItem : Parcelable {
    val title: String
    val description: String?
    val isSingleLineDescription: Boolean
    val isEnabled: Boolean
    val contentDescription: String
}

@Parcelize
data class SimpleDropDownItem(
    override val title: String,
    override val description: String? = null,
    override val isSingleLineDescription: Boolean = true,
    override val isEnabled: Boolean = true,
    override val contentDescription: String = "$title\n${description ?: ""}"
) : DropDownItem

typealias DropDownItemConverter<T> = (T) -> DropDownItem

class CommonDropDownItemConverter(private val resources: Resources) {

    fun <T> convert(item: T): DropDownItem {
        return SimpleDropDownItem(
            title = item.toString(),
            contentDescription = when (item) {
                is WithContentDescription -> resources.getString(
                    R.string.label_dropdown_item_description,
                    item.contentDescription
                )
                else -> resources.getString(
                    R.string.label_dropdown_item_description,
                    resources.accountNameContentDescription(item.toString()) ?: item.toString()
                )
            }
        )
    }
}
