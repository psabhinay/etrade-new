package com.etrade.mobilepro.order.details.api

import com.etrade.mobilepro.orders.api.OrderStatus

data class OrderDetailsRequestParams(
    val accountId: String,
    val orderNumber: String,
    val orderStatus: OrderStatus
)
