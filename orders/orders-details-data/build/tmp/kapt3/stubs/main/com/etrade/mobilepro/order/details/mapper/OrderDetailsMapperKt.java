package com.etrade.mobilepro.order.details.mapper;

import com.etrade.eo.core.util.DateFormattingUtils;
import com.etrade.eo.core.util.MarketDataFormatter;
import com.etrade.mobilepro.instrument.InstrumentType;
import com.etrade.mobilepro.instrument.OptionType;
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem;
import com.etrade.mobilepro.order.details.api.OrderDetailDescription;
import com.etrade.mobilepro.order.details.api.OrderDetailDescriptionMapperType;
import com.etrade.mobilepro.order.details.api.OrderHistoryItem;
import com.etrade.mobilepro.order.details.api.StockPlanOrderCancelResponse;
import com.etrade.mobilepro.order.details.dto.EventDto;
import com.etrade.mobilepro.order.details.dto.LegDetailsDto;
import com.etrade.mobilepro.order.details.dto.OrderDetailsDto;
import com.etrade.mobilepro.order.details.dto.OrderHistory;
import com.etrade.mobilepro.order.details.dto.OrdersDto;
import com.etrade.mobilepro.order.details.dto.StockPlanOrderCancelResponseDto;
import com.etrade.mobilepro.order.details.dto.StockPlanOrderDetailsResponseDto;
import com.etrade.mobilepro.orders.api.AdvancedOrderType;
import com.etrade.mobilepro.orders.api.ContingentCondition;
import com.etrade.mobilepro.orders.api.ContingentQualifier;
import com.etrade.mobilepro.orders.api.EventType;
import com.etrade.mobilepro.orders.api.ExecInstruction;
import com.etrade.mobilepro.orders.api.MarketDestination;
import com.etrade.mobilepro.orders.api.MarketSession;
import com.etrade.mobilepro.orders.api.OrderAction;
import com.etrade.mobilepro.orders.api.OrderStatus;
import com.etrade.mobilepro.orders.api.OrderTerm;
import com.etrade.mobilepro.orders.api.OrderType;
import com.etrade.mobilepro.orders.api.OrigSysCode;
import com.etrade.mobilepro.orders.api.PriceType;
import com.etrade.mobilepro.orders.api.StrategyType;
import com.etrade.mobilepro.orders.api.TransactionType;
import com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto;
import com.etrade.mobilepro.trade.api.option.TradeLeg;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\u001a\f\u0010\u0005\u001a\u00020\u0001*\u00020\u0006H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"DATETIME_FORMAT", "", "EMPTY_VALUE", "QUANTITY_FORMAT", "RESERVELIMIT_QUANTITY_FORMAT", "resolveSymbol", "Lcom/etrade/mobilepro/orders/data/dto/AdvancedOrderDto;", "orders-details-data"})
public final class OrderDetailsMapperKt {
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RESERVELIMIT_QUANTITY_FORMAT = "%s/%sR";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String QUANTITY_FORMAT = "%s/%s";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATETIME_FORMAT = "%s %s";
    private static final java.lang.String EMPTY_VALUE = "--";
    
    /**
     * Work around : When Symbol is empty, Convert display name to ticker symbol
     */
    private static final java.lang.String resolveSymbol(com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto $this$resolveSymbol) {
        return null;
    }
}