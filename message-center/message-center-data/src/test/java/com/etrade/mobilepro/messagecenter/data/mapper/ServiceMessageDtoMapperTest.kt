package com.etrade.mobilepro.messagecenter.data.mapper

import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.api.model.ServiceMessage
import com.etrade.mobilepro.messagecenter.data.dto.ServiceMessageDetailsResponseDto
import com.etrade.mobilepro.messagecenter.data.dto.ServiceMessageFieldDto
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ServiceMessageDtoMapperTest {
    @Test
    fun testMapper() {
        val input = ServiceMessageDetailsResponseDto(
            accountId = "accId",
            payload = listOf(
                ServiceMessageFieldDto(
                    title = "a b  c   d",
                    value = "a\tb\t\tc\t\t\td"
                ),
                ServiceMessageFieldDto(),
                ServiceMessageFieldDto(
                    title = ServiceMessage.ACCOUNT_KEY,
                    value = "abra cadabra"
                )
            ),
            textFields = listOf(
                null,
                "\n   abc   \n",
                "a\tb\t\tc\t\t\td",
                "a b  c   d "
            )
        )
        val output = input.toServiceMessage(testMessage1)
        assertEquals("accId", output.accountId)
        assertEquals("--", output.subject)
        assertEquals(3, output.payload.size)
        assertEquals(true, output.payload.keys.contains("a bcd"))
        assertEquals("abcd", output.payload["a bcd"])
        assertEquals(true, output.payload.keys.contains("--"))
        assertEquals("--", output.payload["--"])
        assertEquals(3, output.messages.size)
        assertEquals("abc", output.messages[0])
        assertEquals("a b c d", output.messages[1])
        assertEquals("a b c d", output.messages[2])
        assertEquals("short name", output.payload[ServiceMessage.ACCOUNT_KEY])
    }

    @Test
    fun testMapperWithAlternativeAccountKeyInResponse() {
        val input = ServiceMessageDetailsResponseDto(
            accountId = "accId",
            payload = listOf(
                ServiceMessageFieldDto(
                    title = "a b  c   d",
                    value = "a\tb\t\tc\t\t\td"
                ),
                ServiceMessageFieldDto(),
                ServiceMessageFieldDto(
                    title = "Account Number",
                    value = "abra cadabra"
                )
            ),
            textFields = listOf(
                null,
                "\n   abc   \n",
                "a\tb\t\tc\t\t\td",
                "a b  c   d "
            )
        )
        val output = input.toServiceMessage(testMessage1)
        assertEquals("accId", output.accountId)
        assertEquals("--", output.subject)
        assertEquals(3, output.payload.size)
        assertEquals(true, output.payload.keys.contains("a bcd"))
        assertEquals("abcd", output.payload["a bcd"])
        assertEquals(true, output.payload.keys.contains("--"))
        assertEquals("--", output.payload["--"])
        assertEquals(3, output.messages.size)
        assertEquals("abc", output.messages[0])
        assertEquals("a b c d", output.messages[1])
        assertEquals("a b c d", output.messages[2])
        assertEquals("short name", output.payload[ServiceMessage.ACCOUNT_KEY])
    }

    private val testMessage1 = Message(
        id = "",
        accountId = "",
        accountShortName = "short name",
        descId = "",
        subjectId = "",
        caseTypeId = "",
        caseTypeDesc = "",
        caseNo = "",
        isUnread = false,
        status = "",
        createdAt = null,
        createdAtFormatted = "",
        readAt = null
    )
}
