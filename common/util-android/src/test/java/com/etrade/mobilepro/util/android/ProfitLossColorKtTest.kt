package com.etrade.mobilepro.util.android

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.Test
import java.math.BigDecimal

class ProfitLossColorKtTest {

    @Test
    fun `a P&L value of 0 displays as green`() {
        assertThat(BigDecimal.ZERO.displayColor(), equalTo(R.color.green))
    }

    @Test
    fun `a negative P&L value of displays as red`() {
        val negativeValue = (-2.3).toBigDecimal()
        assertThat(negativeValue.displayColor(), equalTo(R.color.red))
    }

    @Test
    fun `a positive P&L value displays as green`() {
        val positiveValue = 5.6.toBigDecimal()
        assertThat(positiveValue.displayColor(), equalTo(R.color.green))
    }
}
