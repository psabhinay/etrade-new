package com.etrade.mobilepro.chat.data.setup.repo

import com.etrade.mobilepro.chat.api.ChatParamsModel
import com.etrade.mobilepro.chat.api.setup.models.ChatActiveStatusModel
import com.etrade.mobilepro.chat.api.setup.models.contactId.ChatItemModel
import com.etrade.mobilepro.chat.api.setup.repo.ChatSetupRepo
import com.etrade.mobilepro.chat.api.setup.repo.ChatSetupResult
import com.etrade.mobilepro.chat.data.common.toChatProductItemsModel
import com.etrade.mobilepro.chat.data.common.toCreateChatContactIdModel
import com.etrade.mobilepro.chat.data.common.toGetChatActiveStatusModel
import com.etrade.mobilepro.chat.data.common.toGetChatContactIdModel
import com.etrade.mobilepro.chat.data.common.toGetChatOperatingHoursModel
import com.etrade.mobilepro.chat.data.common.toGetCustomerDetailsForMobileModel
import com.etrade.mobilepro.chat.data.setup.dto.request.ChatInParams
import com.etrade.mobilepro.chat.data.setup.dto.request.ChatSetupRequestBodyDto
import com.etrade.mobilepro.chat.data.setup.service.ChatSetupApiService
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.google.gson.JsonSyntaxException
import javax.inject.Inject

class ChatSetupRepoImpl @Inject constructor(private val setupApiService: ChatSetupApiService) : ChatSetupRepo {

    override suspend fun getChatActiveStatus(): ETResult<ChatActiveStatusModel> =
        runCatchingET { setupApiService.getChatActiveStatus().toGetChatActiveStatusModel() }

    override suspend fun setupChat(params: ChatParamsModel) = getChatOperatingHours(params)

    private suspend fun getChatOperatingHours(params: ChatParamsModel) =
        runWrappingErrors(
            {
                setupApiService.getChatOperatingHours().toGetChatOperatingHoursModel()
            },
            {
                val data = it.getOrNull()?.data
                val isInHoliday = data?.isHoliday ?: false
                val inOperatingHours = data?.inOperatingHours ?: false

                if (isInHoliday || !inOperatingHours) {
                    ChatSetupResult.NonOperatingHour(isInHoliday)
                } else {
                    onGetChatForMobileDetails(params)
                }
            }
        )

    private suspend fun onGetChatForMobileDetails(params: ChatParamsModel) =
        runWrappingErrors(
            {
                setupApiService.getCustomerDetailsForMobile(ChatSetupRequestBodyDto(ChatInParams()))
                    .toGetCustomerDetailsForMobileModel()
            },
            {
                val chatCustomerDetails = it.getOrNull()?.data?.chatCustomerDetails
                if (chatCustomerDetails != null) {
                    params.chatCustomerDetailsModel = chatCustomerDetails
                    getSubjectRequest(params)
                } else {
                    ChatSetupResult.Failure("customer accounts list is null!")
                }
            }
        )

    private suspend fun getSubjectRequest(params: ChatParamsModel) =
        runWrappingErrors(
            {
                setupApiService.getChatProducts().toChatProductItemsModel()
            },
            {
                it.getOrNull()?.let { model ->
                    params.chatProductItems = model
                    getChatContactId(params)
                } ?: ChatSetupResult.Failure(null)
            }
        )

    private suspend fun getChatContactId(params: ChatParamsModel) =
        runWrappingErrors(
            {
                setupApiService.getChatContactId().toGetChatContactIdModel()
            },
            {
                val chatContactId = it.getOrNull()!!
                val data = chatContactId.data
                val items = data?.messages?.chatItems ?: mutableListOf()
                val size = items.size
                params.metadata = data?.metadata
                var contactIdItem: ChatItemModel? = null
                if (size > 0) {
                    contactIdItem = items[0]
                }
                var contactId: String? = "-1"
                if (contactIdItem != null) {
                    contactId = contactIdItem.id
                }
                if (contactId != null && contactId != "-1") {
                    params.chatContactId = contactId
                    ChatSetupResult.Success
                } else {
                    createChatContactId(params)
                }
            }
        )

    private suspend fun createChatContactId(params: ChatParamsModel) =
        runWrappingErrors(
            {
                setupApiService.createChatContactId().toCreateChatContactIdModel()
            },
            {
                val model = it.getOrNull()
                val items = model?.data?.chatItems
                val size = items?.size ?: 0
                var contactIdItem: ChatItemModel? = null
                if (size > 0) {
                    contactIdItem = items?.get(0)
                }
                params.metadata = model?.metadata
                val contactId: String = contactIdItem?.id ?: "-1"
                if (contactId != "-1") {
                    params.chatContactId = contactId
                    ChatSetupResult.Success
                } else {
                    ChatSetupResult.Failure(" unable to create Chat Contact Id")
                }
            }
        )

    /**
     * This wrapper is intended to avoid code duplication for the chat API calls.
     *
     * @param apiCall asynchronous call that returns some result object
     * @param executeIfSuccess code that executes if result was successful catching errors
     *
     * @return [ChatSetupResult] object that wraps data from response result
     */
    private inline fun <T> runWrappingErrors(
        apiCall: () -> T,
        executeIfSuccess: (Result<T>) -> ChatSetupResult
    ): ChatSetupResult {
        val result = runCatching {
            apiCall.invoke()
        }
        return if (result.isSuccess) {
            try {
                executeIfSuccess.invoke(result)
            } catch (e: JsonSyntaxException) {
                ChatSetupResult.Failure(" unknown error")
            } catch (e: java.lang.NullPointerException) {
                ChatSetupResult.Failure(" unknown error")
            }
        } else {
            ChatSetupResult.Failure(result.exceptionOrNull()?.message)
        }
    }
}
