package com.etrade.mobilepro.pushnotification.presentation

import android.app.Activity
import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.etrade.mobilepro.common.createNotification
import com.etrade.mobilepro.pushnotification.api.SmartAlert
import com.etrade.mobilepro.searchingapi.items.EXTRA_QUOTE_TICKER
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.util.KeyValueStore
import javax.inject.Inject

private const val KEY_REQUEST_CODE = "SmartAlertNotificationFactoryImpl.key.request_code"

interface SmartAlertNotificationFactory {
    fun createFrom(alert: SmartAlert): Notification

    companion object {
        val EXTRA_SMART_ALERT_KEY = "${SmartAlertNotificationFactory::class.java}.key.smart_alert"
    }
}

class SmartAlertNotificationFactoryImpl @Inject constructor(
    private val context: Context,
    private val activityClass: Class<out Activity>,
    private val store: KeyValueStore
) : SmartAlertNotificationFactory {

    override fun createFrom(alert: SmartAlert): Notification {
        return buildNotification(alert)
    }

    private fun buildNotification(alert: SmartAlert): Notification {
        return createNotification(context).apply {
            setSmallIcon(R.drawable.ic_generic_notification)
            setContentTitle(context.getString(R.string.product_name))
            setContentText(alert.message)
            setContentIntent(buildPendingIntent(alert))
        }.build()
    }

    private fun buildPendingIntent(alert: SmartAlert): PendingIntent =
        with(Intent(context, activityClass)) {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            putExtra(SmartAlertNotificationFactory.EXTRA_SMART_ALERT_KEY, alert)
            addSymbolIfNecessary(alert)
            PendingIntent.getActivity(context, generateRequestCode(), this, PendingIntent.FLAG_IMMUTABLE)
        }

    private fun generateRequestCode(): Int {
        return (store.getInt(KEY_REQUEST_CODE, 0) + 1)
            .also { store.putInt(KEY_REQUEST_CODE, it) }
    }
}

private fun Intent.addSymbolIfNecessary(alert: SmartAlert) {
    if (alert is SmartAlert.Stock) {
        putExtra(
            EXTRA_QUOTE_TICKER,
            alert.toSymbol()
                .toBundle()
        )
    }
}
