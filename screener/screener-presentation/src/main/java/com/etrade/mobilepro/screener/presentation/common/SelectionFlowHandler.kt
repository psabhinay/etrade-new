package com.etrade.mobilepro.screener.presentation.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.screener.api.EtfFundGroup
import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.MutualFundGroup
import com.etrade.mobilepro.screener.api.repo.FundCategoriesRepo
import com.etrade.mobilepro.util.Resource
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

interface SelectionFlowHandler<T> {
    val stageData: LiveData<Resource<Pair<Int, DropDownMultiSelector.Selectable<T>>>>
    fun willSelectItem(pos: Int, item: DropDownMultiSelector.Selectable<T>)
    fun preloadItems(itemValue: T)
    fun didSelectItem(pos: Int? = null, item: DropDownMultiSelector.Selectable<T>? = null)
    fun clearSubscriptions()
}

class FundGroupSelectionFlowHandler<T>(private val fundCategoriesRepo: FundCategoriesRepo) : SelectionFlowHandler<T> {
    private val compositeDisposable = CompositeDisposable()
    private val _stageData = MutableLiveData<Resource<Pair<Int, DropDownMultiSelector.Selectable<T>>>>()
    override val stageData: LiveData<Resource<Pair<Int, DropDownMultiSelector.Selectable<T>>>> = _stageData

    override fun willSelectItem(pos: Int, item: DropDownMultiSelector.Selectable<T>) {
        compositeDisposable.clear()
        compositeDisposable.add(
            sourceForType(item.value)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = {
                        val nextValue = when (it) {
                            is Resource.Loading -> Resource.Loading(data = Pair(pos, item))
                            is Resource.Success -> Resource.Success(data = Pair(pos, item))
                            is Resource.Failed -> Resource.Failed(data = Pair(pos, item))
                        }
                        _stageData.value = nextValue
                    },
                    onError = {
                        _stageData.value = Resource.Failed(data = Pair(pos, item), error = it)
                    }
                )
        )
    }

    override fun preloadItems(itemValue: T) {
        compositeDisposable.add(
            sourceForType(itemValue)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = {
                        val nextValue: Resource<Pair<Int, DropDownMultiSelector.Selectable<T>>> = when (it) {
                            is Resource.Loading -> Resource.Loading()
                            is Resource.Success -> Resource.Success()
                            is Resource.Failed -> Resource.Failed()
                        }
                        _stageData.value = nextValue
                    },
                    onError = {
                        _stageData.value = Resource.Failed(error = it)
                    }
                )
        )
    }

    override fun didSelectItem(pos: Int?, item: DropDownMultiSelector.Selectable<T>?) {
        compositeDisposable.clear()
        _stageData.value = Resource.Success()
    }

    override fun clearSubscriptions() = compositeDisposable.clear()

    private fun sourceForType(item: T): Flowable<Resource<List<FundCategory>>> {
        if (item is EtfFundGroup) {
            return fundCategoriesRepo.loadEtfFundCategories(item)
        }
        if (item is MutualFundGroup) {
            return fundCategoriesRepo.loadMutualFundCategories(item)
        }
        throw IllegalArgumentException("Wrong group type of fund categories, $item")
    }
}
