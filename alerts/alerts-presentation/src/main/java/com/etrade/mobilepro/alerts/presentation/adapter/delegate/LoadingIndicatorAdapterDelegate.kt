package com.etrade.mobilepro.alerts.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.presentation.adapter.viewholder.AlertLoadingIndicatorViewHolder
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class LoadingIndicatorAdapterDelegate : AbsListItemAdapterDelegate<AlertItem.LoadingIndicator, AlertItem, AlertLoadingIndicatorViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): AlertLoadingIndicatorViewHolder {
        return AlertLoadingIndicatorViewHolder.from(parent)
    }

    override fun isForViewType(item: AlertItem, items: MutableList<AlertItem>, position: Int): Boolean {
        return item is AlertItem.LoadingIndicator
    }

    override fun onBindViewHolder(item: AlertItem.LoadingIndicator, holder: AlertLoadingIndicatorViewHolder, payloads: MutableList<Any>) {
        holder.bindTo()
    }
}
