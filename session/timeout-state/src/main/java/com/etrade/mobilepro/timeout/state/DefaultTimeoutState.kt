package com.etrade.mobilepro.timeout.state

import javax.inject.Inject

class DefaultTimeoutState @Inject constructor() : TimeoutState {
    override var shouldShowSarLogoutDialog: Boolean = false
    override var isShowingSessionDialog: Boolean = false
    override var shouldShowBiometricAuthentication: Boolean = true

    override fun isDialogPresent(): Boolean = shouldShowSarLogoutDialog || isShowingSessionDialog
}
