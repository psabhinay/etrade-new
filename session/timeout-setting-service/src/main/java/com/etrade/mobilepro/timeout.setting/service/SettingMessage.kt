package com.etrade.mobilepro.timeout.setting.service

data class SettingMessage(val status: String, val isSuccess: Boolean = false)
