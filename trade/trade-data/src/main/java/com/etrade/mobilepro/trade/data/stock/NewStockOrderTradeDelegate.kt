package com.etrade.mobilepro.trade.data.stock

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.order.details.api.OrderDetailsRepo
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.trade.api.OrderPlaceResponse
import com.etrade.mobilepro.trade.api.OrderPreviewResponse
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.stock.StockOrderPreviewResponse
import com.etrade.mobilepro.trade.api.stock.yourPrice
import com.etrade.mobilepro.trade.data.TradeDelegate
import com.etrade.mobilepro.trade.data.TradeOrderArguments
import com.etrade.mobilepro.trade.data.advanced.removeRedundantAdvancedOrderParams
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId

internal const val PREVIEW_NEW_STOCK_ORDER = "PreviewNewStockOrder"
internal const val PLACE_NEW_STOCK_ORDER = "PlaceNewStockOrder"

internal class NewStockOrderTradeDelegate(
    private val apiClient: StockTradingApiClient,
    private val inputId: StockTradeFormInputId,
    private val isTablet: Boolean,
    private val orderDetailsRepo: OrderDetailsRepo,
    private val user: User
) : TradeDelegate {

    override suspend fun previewOrder(map: InputValuesSnapshot, marketDestination: MarketDestination): ETResult<OrderPreviewResponse> {
        val mutableMap = map.toMutableMap().also {
            addOrderParams(it, isTablet, user.getUserId().toString(), marketDestination)
            addPreviewOrderParams(it)
            it.removeRedundantAdvancedOrderParams(inputId)
        }
        return mutableMap.makeStockOrder(apiClient, PREVIEW_NEW_STOCK_ORDER).mapCatching { StockOrderPreviewResponseWrapper(it) }
    }

    override suspend fun placeOrder(arguments: TradeOrderArguments, order: TradeOrder): ETResult<OrderPlaceResponse> {
        val response = order.response
        if (response !is StockOrderPreviewResponse) {
            return ETResult.failure(IllegalStateException("Order ${order.orderId} is not in preview state."))
        }

        return if (response.isOvernightTrading) {
            placeNewStockOrderOvernight(arguments, response)
        } else {
            placeNewStockOrderAuto(arguments, response)
        }.mapCatching { StockOrderPlaceResponseWrapper(it) }
            .onSuccess {
                arguments.params
                    .mapValues { it.value as? String? }
                    .takeIf { it.containsKey(inputId.orderId) }
                    ?.let { deleteSavedOrder(it) }
            }
    }

    private suspend fun placeNewStockOrderAuto(arguments: TradeOrderArguments, response: StockOrderPreviewResponse): ETResult<StockOrderResponseDto> {
        val mutableMap = arguments.params.toMutableMap().also {
            addOrderParams(it, isTablet, user.getUserId().toString(), arguments.marketDestination)
            addPlaceOrderParams(
                map = it,
                yourPrice = response.yourPrice
            )
        }
        return mutableMap.makeStockOrder(apiClient, PLACE_NEW_STOCK_ORDER)
    }

    private suspend fun placeNewStockOrderOvernight(arguments: TradeOrderArguments, response: StockOrderPreviewResponse): ETResult<StockOrderResponseDto> {
        val mutableMap = arguments.params.toMutableMap().also {
            addOrderParams(it, isTablet, user.getUserId().toString())
            addPlaceOrderParams(
                map = it,
                yourPrice = response.yourPrice,
                overnightIndicatorFlag = true
            )
        }
        return mutableMap.makeStockOrder(apiClient, PLACE_NEW_STOCK_ORDER)
    }

    private suspend fun deleteSavedOrder(params: Map<String, String?>) {
        orderDetailsRepo.deleteOrder(
            accountId = params[inputId.accountId].orEmpty(),
            orderId = params[inputId.orderId].orEmpty()
        )
    }
}
