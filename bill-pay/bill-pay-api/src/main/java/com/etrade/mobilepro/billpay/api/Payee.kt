package com.etrade.mobilepro.billpay.api

data class Payee(
    val payeeId: String? = null,
    val name: String,
    val nickName: String?,
    val phoneNumber: String?,
    val accountNumber: String,
    val leadDays: Int?,
    val isAddressAvailable: Boolean?,
    val address: PayeeAddress? = null,
    val isExistingPayee: Boolean = !payeeId.isNullOrEmpty(),
    val merchantInfo: MerchantInfo? = null,
    val merchantZipRequired: Boolean? = null,
    val category: String? = null
)

data class PayeeAddress(
    val address1: String?,
    val address2: String?,
    val city: String?,
    val state: String?,
    val zipCode: String?
)

data class MerchantInfo(
    val id: Int?,
    val merchantZip: String?,
    val merchantZip4: String?
)
