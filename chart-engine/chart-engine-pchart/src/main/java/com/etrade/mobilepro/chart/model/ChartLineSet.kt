package com.etrade.mobilepro.chart.model

import androidx.annotation.ColorRes
import com.etrade.mobilepro.chartengine.TimedChartPoint
import com.etrade.mobilepro.util.market.time.NEW_YORK_ZONE_ID
import com.etrade.mobilepro.util.math.percentageChange
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import org.threeten.bp.temporal.ChronoField
import java.util.concurrent.TimeUnit

private const val DAILY_LABEL_HOUR = 9L
private const val DAILY_LABEL_MIN = 45L

class ChartLineSet constructor(
    val label: String,
    @ColorRes val color: Int,
    private val baseClosingPrice: Double,
    private val showAsPercentage: Boolean,
    private val isOption: Boolean
) {

    var lowestValue: Double = Double.MAX_VALUE
    var highestValue: Double = Double.NEGATIVE_INFINITY
    var highestTimeInMillis = 0L

    private val listOfEntries: MutableList<Entry> = mutableListOf()

    val listOfLabels: MutableList<DisplayLabel> = mutableListOf()

    fun addPoint(point: TimedChartPoint, chartTimeFrame: ChartTimeFrame) {
        val changePercent = percentageChange(point.y, baseClosingPrice)
        val closePrice = point.y
        val zonedDateTime = point.x.atZone(NEW_YORK_ZONE_ID)
        val dateInMillis = zonedDateTime.toInstant().toEpochMilli()
        val dateInMinutes = TimeUnit.MILLISECONDS.toMinutes(dateInMillis)

        if (dateInMillis > highestTimeInMillis) highestTimeInMillis = dateInMillis

        val entryY = if (showAsPercentage) { changePercent } else { closePrice }
        val y: Float = entryY.toFloat()

        updateYAxisMinAndMax(entryY)

        when (chartTimeFrame) {
            is ChartTimeFrame.OneWeek -> {
                listOfEntries.add(Entry(listOfEntries.size.toFloat(), y))
                // store labels of all the 9:45 points since we need to display only a single label per day
                if (zonedDateTime.getLong(ChronoField.HOUR_OF_DAY) == DAILY_LABEL_HOUR &&
                    zonedDateTime.getLong(ChronoField.MINUTE_OF_HOUR) == DAILY_LABEL_MIN
                ) {
                    listOfLabels.add(DisplayLabel(listOfEntries.size, dateInMillis))
                }
            }
            is ChartTimeFrame.OneMonth -> {
                if (isOption) {
                    listOfEntries.add(Entry(dateInMinutes.toFloat(), y))
                } else {
                    listOfEntries.add(Entry(listOfEntries.size.toFloat(), y))
                    // add all the even x-axis points as labels
                    if (listOfEntries.size % 2 == 0 && listOfEntries.size > 0) {
                        listOfLabels.add(DisplayLabel(listOfEntries.size, dateInMillis))
                    }
                }
            }
            else -> {
                listOfEntries.add(Entry(dateInMinutes.toFloat(), y))
            }
        }
    }

    private fun updateYAxisMinAndMax(entryY: Double) {
        if (entryY < lowestValue) {
            lowestValue = entryY
        }
        if (entryY > highestValue) {
            highestValue = entryY
        }
    }

    fun getLineSet() = LineDataSet(listOfEntries, label)
}

class DisplayLabel(
    val index: Int,
    val timeInMillis: Long
)
