package com.etrade.mobilepro.menu

import com.etrade.mobilepro.dynamicui.DynamicScreenRpcService
import com.etrade.mobilepro.dynamicui.delegate.WithRealmDataSourceDelegate
import io.realm.Realm
import io.realm.RealmConfiguration
import javax.inject.Inject

class DynamicMenuDataSourceDelegate @Inject constructor(
    dynamicScreenRpcService: DynamicScreenRpcService
) : WithRealmDataSourceDelegate(dynamicScreenRpcService) {

    private val realmConfig = RealmConfiguration.Builder()
        .name("et.dynamicmenu")
        .deleteRealmIfMigrationNeeded()
        .build()

    override fun getRealmInstance(): Realm {
        return Realm.getInstance(realmConfig)
    }
}
