package com.etrade.mobilepro.tableviewutils.presentation

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import com.etrade.mobilepro.common.extension.screenWidthPixels
import com.etrade.mobilepro.tableviewutils.presentation.adapter.TableViewAdapter
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.util.android.extension.getActivity
import com.evrencoskun.tableview.TableView
import com.evrencoskun.tableview.adapter.AdapterDataSetChangedListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

private const val QUOTE_UPDATE_DELAY_IN_MILLIS = 500L

/**
 * A TableView implementation for Markets -> Overview screen -> Broader Indices widget.
 */
class BroaderIndicesTableView : TableView {
    private var adapterProvider: (() -> TableViewAdapter)? = null

    private var viewModelScope: CoroutineScope ? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun setScope(viewModelScope: CoroutineScope) {
        this.viewModelScope = viewModelScope
    }

    fun applyData(columnHeaderList: List<Cell>, rowHeaders: List<SymbolCell>, cellItems: List<List<DataCell>>) {
        val settingAdapterFirstTime = adapter == null
        if (settingAdapterFirstTime) {
            adapter = adapterProvider?.invoke()
            /**
             * https://medium.com/@dmstocking/how-one-linearlayoutmanager-bug-makes-diffutil-worthless-372a5b887dec.
             * We're disabling animations to prevent automatic scrolling when sorting items
             */
            columnSortHandler.isEnableAnimation = false
        }

        val tableAdapter: TableViewAdapter = adapter as TableViewAdapter
        val oldColumns = adapter.columnHeaderRecyclerViewAdapter.items
        val oldRowHeaders = adapter.rowHeaderRecyclerViewAdapter.items

        when {
            settingAdapterFirstTime -> {
                addCellDataChangedListener()
                adapter.setAllItems(columnHeaderList, rowHeaders, cellItems)
            }
            columnHeaderList == oldColumns &&
                rowHeaders == oldRowHeaders -> {
                tableAdapter.updateAllItems(columnHeaderList, rowHeaders, cellItems)
            }
            else -> {
                adapter.setAllItems(columnHeaderList, rowHeaders, cellItems)
                rowHeaderLayoutManager.scrollToPosition(0)
                cellLayoutManager.scrollToPosition(0)
            }
        }
    }

    /**
     * Ensure Broader Indices cells are populated the first time the cells are set.
     * Delay is needed before calling notifyDataSetChanged in order for the update to take effect.
     */
    private fun addCellDataChangedListener() {
        adapter.addAdapterDataSetChangedListener(
            object :
                AdapterDataSetChangedListener<List<Cell>, List<SymbolCell>, List<List<Cell>>>() {
                override fun onCellItemsChanged(cellItems: MutableList<MutableList<List<List<Cell>>>>?) {
                    super.onCellItemsChanged(cellItems)
                    viewModelScope?.launch {
                        delay(QUOTE_UPDATE_DELAY_IN_MILLIS)
                        adapter.cellRecyclerViewAdapter.notifyCellDataSetChanged()
                    }
                }
            }
        )
    }

    fun initTableViewParams(
        rowHeaderWidth: Int = R.dimen.default_tableview_row_header_width,
        tableViewListener: TableViewListener? = null,
        adapterProvider: () -> TableViewAdapter
    ) {
        val activity = context.getActivity() ?: return
        val outValue = TypedValue().apply {
            resources.getValue(rowHeaderWidth, this, true)
        }

        isIgnoreSelectionColors = true
        isSortable = true
        this.tableViewListener = tableViewListener
        this.setHasFixedWidth(true)
        this.rowHeaderWidth = (activity.screenWidthPixels * outValue.float).toInt()
        this.adapterProvider = adapterProvider
    }
}
