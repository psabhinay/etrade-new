@file:Suppress("TopLevelPropertyNaming")

package com.etrade.mobilepro.common.compose.theme

import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

/**
 * General dimensions
 */
object Dimens {
    val Size0dp = 0.dp // spacing_none
    val Size1dp = 1.dp // non-standard
    val Size4dp = 4.dp // spacing_xsmall
    val Size8dp = 8.dp // spacing_small
    val Size10dp = 10.dp // non-standard
    val Size12dp = 12.dp // non-standard
    val Size14dp = 14.dp // spacing_xsmall
    val Size16dp = 16.dp // spacing_medium
    val Size24dp = 24.dp // spacing_large
    val Size32dp = 32.dp // spacing_xlarge
    val Size40dp = 40.dp // spacing_xxlarge
    val Size48dp = 48.dp // spacing_xxxlarge
    val Size56dp = 56.dp // non-standard
    val Size64dp = 64.dp // spacing_huge
    val Size96dp = 96.dp // spacing_xhuge
}

/**
 * Font sizing
 */
object FontSizes {
    val Size0sp = 0.sp
    val Size14sp = 14.sp
    val Size16sp = 16.sp
    val Size18sp = 18.sp
    val Size20sp = 20.sp
    val Size24sp = 24.sp
}
