package com.etrade.mobilepro.alerts.presentation.manage

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.alerts.api.model.AlertAction
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.common.customview.SwipeRevealLayout
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MANAGE_ALERTS
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.util.android.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class ManageAlertViewModel @Inject constructor(
    private val alertsRepo: AlertsRepo,
    private val alertsHolder: UserAlertsHolder
) : ViewModel(), ScreenViewModel {
    private val compositeDisposable = CompositeDisposable()
    private var refreshDisposable: Disposable? = null
    private val _viewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState> = _viewState
    private val _viewEffects = SingleLiveEvent<ViewEffects>()
    val viewEffects: LiveData<ViewEffects> = _viewEffects

    init {
        refresh()
    }

    override val screenName: String? = SCREEN_TITLE_MANAGE_ALERTS

    fun refresh() {
        refreshDisposable?.dispose()
        _viewState.value = ViewState.Loading
        refreshDisposable = alertsRepo
            .getUserAlerts()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = {
                    alertsHolder.setAlerts(it)
                    _viewState.value = ViewState.Success(alertsHolder.getAlerts())
                },
                onError = {
                    _viewState.value = ViewState.Error.Loading
                }
            )
    }

    fun alertAction(alert: UserAlertItem, action: AlertAction) {
        when (action) {
            AlertAction.DELETE -> {
                removeAlertFromList(alert)
                compositeDisposable.add(
                    alertsRepo
                        .deleteUserAlert(alert)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeBy(
                            onComplete = {
                                _viewEffects.value = ViewEffects.AlertWasDeleted
                            },
                            onError = {
                                alertsHolder.insertAlert(alert)
                                _viewState.value = ViewState.Error.Deleting(alert, alertsHolder.getAlerts())
                            }
                        )
                )
            }
            AlertAction.SELECT -> {
                alertsHolder.toggleDeleteButtonVisibility(alert, isVisible = false)
                _viewState.value = ViewState.Success(alertsHolder.getAlerts())
                _viewEffects.value = ViewEffects.AlertWasTappedOn(alert)
            }
            AlertAction.MARK -> {
                // NO-OP, isn't currently used for managing alerts
            }
        }
    }

    fun deleteButtonAction(alert: UserAlertItem, action: SwipeRevealLayout.Action) {
        when (action) {
            SwipeRevealLayout.Action.OPEN -> alertsHolder.toggleDeleteButtonVisibility(alert, isVisible = true)
            SwipeRevealLayout.Action.CLOSE -> alertsHolder.toggleDeleteButtonVisibility(alert, isVisible = false)
            SwipeRevealLayout.Action.START_DRAG -> alertsHolder.hideAllDeleteButtons()
        }
        _viewState.value = ViewState.Success(alertsHolder.getAlerts())
    }

    fun removeAlertFromList(alert: UserAlertItem) {
        alertsHolder.deleteAlert(alert)
        _viewState.value = ViewState.Success(alertsHolder.getAlerts())
    }

    override fun onCleared() {
        refreshDisposable?.dispose()
        compositeDisposable.clear()
        super.onCleared()
    }

    sealed class ViewState {
        object Loading : ViewState()
        data class Success(
            val alerts: List<UserAlertItem>
        ) : ViewState()

        sealed class Error : ViewState() {
            object Loading : Error()
            data class Deleting(
                val failedAlert: UserAlertItem,
                val actualAlerts: List<UserAlertItem>
            ) : Error()
        }
    }

    sealed class ViewEffects {
        object AlertWasDeleted : ViewEffects()
        data class AlertWasTappedOn(
            val alert: UserAlertItem
        ) : ViewEffects()
    }
}
