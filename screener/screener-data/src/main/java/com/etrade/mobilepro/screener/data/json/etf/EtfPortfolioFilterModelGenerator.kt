package com.etrade.mobilepro.screener.data.json.etf

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfPortfolioFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val NET_ASSETS_FIELD = "TotalAssets"
private const val YIELD_FIELD = "Yield"

class EtfPortfolioFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val etfPortfolioFilter = screenerFilter as EtfPortfolioFilter
        val arguments = mutableListOf<Argument>()

        if (!etfPortfolioFilter.region.isAllItemsElement) {
            arguments.add(
                Argument(etfPortfolioFilter.regionPercentInvested.clauses, etfPortfolioFilter.region.id)
            )
        }

        if (!etfPortfolioFilter.country.isAllItemsElement) {
            arguments.add(
                Argument(etfPortfolioFilter.countryPercentInvested.clauses, etfPortfolioFilter.country.id)
            )
        }

        if (!etfPortfolioFilter.industry.isAllItemsElement) {
            arguments.add(
                Argument(etfPortfolioFilter.industryTypePercentInvested.clauses, etfPortfolioFilter.industry.id)
            )
        }

        if (!etfPortfolioFilter.isNetAssetEmpty()) {
            arguments.add(Argument(getNetAssetsClauses(etfPortfolioFilter), NET_ASSETS_FIELD))
        }

        if (!etfPortfolioFilter.isYieldEmpty()) {
            arguments.add(Argument(getYieldClauses(etfPortfolioFilter), YIELD_FIELD))
        }

        return PartialScreenerParams(arguments)
    }

    private fun getNetAssetsClauses(etfPortfolioFilter: EtfPortfolioFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (etfPortfolioFilter.netAssetsLessThan100) {
            clauses.addAll(netAssetsLessThan100Clauses)
        }
        if (etfPortfolioFilter.netAssets100to500) {
            clauses.addAll(netAssets100to500Clauses)
        }
        if (etfPortfolioFilter.netAssets500to1) {
            clauses.addAll(netAssets500to1Clauses)
        }
        if (etfPortfolioFilter.netAssets1to2) {
            clauses.addAll(netAssets1to2Clauses)
        }
        if (etfPortfolioFilter.netAssetsGreaterThan2) {
            clauses.addAll(netAssetsGreaterThan2Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private fun getYieldClauses(etfPortfolioFilter: EtfPortfolioFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (etfPortfolioFilter.yieldBottom20) {
            clauses.addAll(yieldBottom20Clauses)
        }
        if (etfPortfolioFilter.yieldBelowMid20) {
            clauses.addAll(yieldBelowMid20Clauses)
        }
        if (etfPortfolioFilter.yieldMid20) {
            clauses.addAll(yieldMid20Clauses)
        }
        if (etfPortfolioFilter.yieldAboveMid20) {
            clauses.addAll(yieldAboveMid20Clauses)
        }
        if (etfPortfolioFilter.yieldTop20) {
            clauses.addAll(yieldTop20Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private val netAssetsLessThan100Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("100000000"))
    )
    private val netAssets100to500Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("100000000")),
        Clause(OPERATOR_LESS_THAN, listOf("500000000"))
    )
    private val netAssets500to1Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("500000000")),
        Clause(OPERATOR_LESS_THAN, listOf("1000000000"))
    )
    private val netAssets1to2Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("1000000000")),
        Clause(OPERATOR_LESS_THAN, listOf("2000000000"))
    )
    private val netAssetsGreaterThan2Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("2000000000")),
        Clause(OPERATOR_LESS_THAN, listOf("100000000000"))
    )

    private val yieldBottom20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("21"))
    )
    private val yieldBelowMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("21")),
        Clause(OPERATOR_LESS_THAN, listOf("41"))
    )
    private val yieldMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("41")),
        Clause(OPERATOR_LESS_THAN, listOf("61"))
    )
    private val yieldAboveMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("61")),
        Clause(OPERATOR_LESS_THAN, listOf("81"))
    )
    private val yieldTop20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("81")),
        Clause(OPERATOR_LESS_THAN, listOf("101"))
    )
}
