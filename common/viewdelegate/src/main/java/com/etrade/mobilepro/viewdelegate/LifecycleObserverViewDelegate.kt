package com.etrade.mobilepro.viewdelegate

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner

class LifecycleObserverViewDelegate(initialObservers: Collection<LifecycleObserver> = emptySet()) : ViewDelegate {

    private val observers: MutableSet<LifecycleObserver> = HashSet(initialObservers)

    private var lifecycle: Lifecycle? = null

    fun dispose() {
        lifecycle = null
    }

    override fun observe(owner: LifecycleOwner) {
        val previousLifecycle = lifecycle

        lifecycle = owner.lifecycle.apply {
            observers.forEach {
                previousLifecycle?.removeObserver(it)
                addObserver(it)
            }
        }
    }

    operator fun plusAssign(observer: LifecycleObserver) {
        if (observers.add(observer)) {
            lifecycle?.addObserver(observer)
        }
    }
}
