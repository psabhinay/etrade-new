package com.etrade.mobilepro.dialog.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

operator fun AppDialogViewModel.plus(second: AppDialogViewModel): AppDialogViewModel {
    return object : AppDialogViewModel {
        override val dialog: LiveData<ConsumableLiveEvent<AppDialog?>?>
            get() = MediatorLiveData<ConsumableLiveEvent<AppDialog?>?>().apply {
                addSource(this@plus.dialog) { value = it }
                addSource(second.dialog) { value = it }
            }
    }
}
