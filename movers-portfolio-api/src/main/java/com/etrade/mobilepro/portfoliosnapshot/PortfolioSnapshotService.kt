package com.etrade.mobilepro.portfoliosnapshot

import com.etrade.eo.rest.retrofit.Scalar
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

const val PORTFOLIO_SNAPSHOT_URL = "/app/portfolio/getpfosnapshot.json"

interface PortfolioSnapshotService {
    @Scalar
    @POST(PORTFOLIO_SNAPSHOT_URL)
    fun getPortfolioMoversString(@Body @JsonMoshi request: PortfolioSnapshotRequestBody): Single<String>
}
