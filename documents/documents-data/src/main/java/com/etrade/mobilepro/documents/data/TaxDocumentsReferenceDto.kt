package com.etrade.mobilepro.documents.data

import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.documents.TaxDocument
import com.squareup.moshi.JsonClass
import org.threeten.bp.LocalDate

@JsonClass(generateAdapter = true)
class TaxDocumentsReferenceDto(
    override val data: List<TaxDocumentDto>
) : BaseReference<List<TaxDocument>>() {
    override fun convertToModel(): List<TaxDocument> = data.map {
        TaxDocument(
            reportDate = LocalDate.parse(it.reportDate, DOCUMENTS_TAX_DATE_FORMATTER_FOR_SERVER),
            amended = it.amended,
            year = it.year,
            documentId = it.documentId,
            title = it.title,
            accountUuid = it.accountUuid,
            institutionType = it.institutionType,
            postEdgeFolder = it.postEdgeFolder
        )
    }
}
