package com.etrade.mobilepro.orders

import androidx.navigation.NavController
import com.etrade.mobilepro.R
import com.etrade.mobilepro.accounts.util.android.view.AccountViewType
import com.etrade.mobilepro.home.AccountTab
import com.etrade.mobilepro.home.createAccountListByAccountId
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.order.details.router.OrderDetailsRouter
import com.etrade.mobilepro.order.details.router.StockPlanOrderDetailsArgument
import javax.inject.Inject

class OrderDetailsRouterImpl @Inject constructor() : OrderDetailsRouter {
    override fun openOrderDetails(navController: NavController, argument: OrderDetailsArgument) {
        val upAccountTabIndex = when (argument) {
            is StockPlanOrderDetailsArgument -> createAccountListByAccountId(AccountViewType.ESP_ACCOUNT_VIEW)?.indexOf(AccountTab.Orders)
            else -> null
        } ?: -1
        navController.navigate(R.id.orderDetailsFragment, OrderDetailsFragmentArgs(argument, upAccountTabIndex).toBundle())
    }
}
