package com.etrade.completeview.dto

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AccountSummaryCardViewDto(
    @Json(name = "data")
    override val data: AccountSummaryDto,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?,
    @Json(name = "action")
    override val clickActionDto: ClickActionDto? = null,
    val isSingleAccount: Boolean = false
) : BaseScreenView()
