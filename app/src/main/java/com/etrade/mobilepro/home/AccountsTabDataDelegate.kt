package com.etrade.mobilepro.home

class AccountsTabDataDelegate(val loadData: () -> Unit) {
    private var shouldFetch: Boolean = false

    fun initialize(inViewPager: Boolean = true) {
        if (!inViewPager || shouldFetch) {
            loadData()
        }
    }

    fun fetchData(isFragmentReady: Boolean) {
        if (isFragmentReady) {
            loadData()
        } else {
            shouldFetch = true
        }
    }
}
