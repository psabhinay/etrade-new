package com.etrade.mobilepro.screener.data.json.stock

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_EQUALS
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.stock.FundamentalsFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val PE_RATIO_OVER_TRAILING_12MONTHS_FIELD = "PERatioTrailing12"
private const val PE_RATIO_OVER_CURRENT_FISCAL_YEAR_FIELD = "PERatioCurrent"
private const val PE_RATIO_OVER_NEXT_FISCAL_YEAR_FIELD = "PERatioNext"
private const val PE_RATIO_BELOW_INDUSTRY_AVG_FIELD = "PERatioBelowIndustry"
private const val PE_RATIO_ABOVE_INDUSTRY_AVG_FIELD = "PERatioAboveIndustry"

private const val PRICE_BOOK_RATIO_FIELD = "PriceToBook"

class FundamentalsFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val fundamentalsFilter = screenerFilter as FundamentalsFilter

        val arguments = mutableListOf<Argument>()

        createPeRatioArguments(fundamentalsFilter, arguments)

        if (fundamentalsFilter.peRatioBelowIndustryAvg) {
            arguments.add(Argument(listOf(equalsTrueClause), PE_RATIO_BELOW_INDUSTRY_AVG_FIELD))
        }
        if (fundamentalsFilter.peRatioAboveIndustryAvg) {
            arguments.add(Argument(listOf(equalsTrueClause), PE_RATIO_ABOVE_INDUSTRY_AVG_FIELD))
        }

        if (fundamentalsFilter.isPriceBookRatioSelected()) {
            val clauses = getPbClauses(fundamentalsFilter)
            arguments.add(Argument(clauses, PRICE_BOOK_RATIO_FIELD))
        }

        return PartialScreenerParams(arguments)
    }

    private fun createPeRatioArguments(
        fundamentalsFilter: FundamentalsFilter,
        arguments: MutableList<Argument>
    ) {
        if (fundamentalsFilter.isOverPeriodSelected() && fundamentalsFilter.peRatioButtonsAreChecked()) {
            val clauses = getPeClauses(fundamentalsFilter)
            if (fundamentalsFilter.peRatioOverTrailing12Months) {
                arguments.add(Argument(clauses, PE_RATIO_OVER_TRAILING_12MONTHS_FIELD))
            }
            if (fundamentalsFilter.peRatioOverCurrentFiscalYear) {
                arguments.add(Argument(clauses, PE_RATIO_OVER_CURRENT_FISCAL_YEAR_FIELD))
            }
            if (fundamentalsFilter.peRatioOverNextFiscalYear) {
                arguments.add(Argument(clauses, PE_RATIO_OVER_NEXT_FISCAL_YEAR_FIELD))
            }
        }
    }

    private fun getPeClauses(fundamentalsFilter: FundamentalsFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (fundamentalsFilter.peRatio0to15x) {
            clauses.addAll(peRatio0to15)
        }
        if (fundamentalsFilter.peRatio15to25x) {
            clauses.addAll(peRatio15to25)
        }
        if (fundamentalsFilter.peRatio25to50x) {
            clauses.addAll(peRatio25to50)
        }
        if (fundamentalsFilter.peRatio50to100x) {
            clauses.addAll(peRatio50to100)
        }

        return clauses.mergeFollowingClauses()
    }

    private fun getPbClauses(fundamentalsFilter: FundamentalsFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (fundamentalsFilter.priceBookRatio0to1x) {
            clauses.addAll(pbRatio0to1)
        }
        if (fundamentalsFilter.priceBookRatio1to2x) {
            clauses.addAll(pbRatio1to2)
        }
        if (fundamentalsFilter.priceBookRatio2to3x) {
            clauses.addAll(pbRatio2to3)
        }
        if (fundamentalsFilter.priceBookRatioGreaterThan3x) {
            clauses.addAll(pbRatio3toMore)
        }

        return clauses.mergeFollowingClauses()
    }

    private val peRatio0to15 = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0.001")),
        Clause(OPERATOR_LESS_THAN, listOf("15"))
    )
    private val peRatio15to25 = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("15")),
        Clause(OPERATOR_LESS_THAN, listOf("25"))
    )
    private val peRatio25to50 = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("25")),
        Clause(OPERATOR_LESS_THAN, listOf("50"))
    )
    private val peRatio50to100 = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("50")),
        Clause(OPERATOR_LESS_THAN, listOf("100"))
    )

    private val pbRatio0to1 = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("1"))
    )
    private val pbRatio1to2 = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("1")),
        Clause(OPERATOR_LESS_THAN, listOf("2"))
    )
    private val pbRatio2to3 = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("2")),
        Clause(OPERATOR_LESS_THAN, listOf("3"))
    )
    private val pbRatio3toMore = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("3")),
        Clause(OPERATOR_LESS_THAN, listOf("100001"))
    ) // 100001 was used as max value

    private val equalsTrueClause = Clause(OPERATOR_EQUALS, listOf("1"))
}
