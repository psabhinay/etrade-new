package com.etrade.mobilepro.watchlist.repo

import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.watchlist.dto.AddWatchlistEntryRequestDto
import com.etrade.mobilepro.watchlist.dto.CreateWatchlistRequestDataDto
import com.etrade.mobilepro.watchlist.dto.CreateWatchlistRequestDto
import com.etrade.mobilepro.watchlist.dto.CreateWatchlistResponseDto
import com.etrade.mobilepro.watchlist.dto.DeleteWatchlistDetailDto
import com.etrade.mobilepro.watchlist.dto.DeleteWatchlistRequestDto
import com.etrade.mobilepro.watchlist.dto.EditWatchListEntryDto
import com.etrade.mobilepro.watchlist.dto.EditWatchlistEntriesDto
import com.etrade.mobilepro.watchlist.dto.SaveWatchlistEntriesRequestDto
import com.etrade.mobilepro.watchlist.dto.WatchlistEntriesRequest
import com.etrade.mobilepro.watchlist.dto.WatchlistEntriesValue
import com.etrade.mobilepro.watchlist.dto.WatchlistEntryDto
import com.etrade.mobilepro.watchlist.mapper.WatchlistDtoMapper
import com.etrade.mobilepro.watchlist.mapper.WatchlistEntryDtoMapper
import com.etrade.mobilepro.watchlist.mapper.toWatchlistEntry
import com.etrade.mobilepro.watchlist.rest.WatchlistApiClient
import com.etrade.mobilepro.watchlistapi.DuplicateSymbolException
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

private const val EMPTY_ERROR_CODE = "53"

class DefaultWatchlistRepo @Inject constructor(
    private val quoteRepo: MobileQuoteRepo,
    private val watchlistApiClient: WatchlistApiClient
) : WatchlistRepo {

    private val watchlistDtoMapper = WatchlistDtoMapper()
    private val watchlistEntryDtoMapper = WatchlistEntryDtoMapper()

    override fun getUserWatchlists(): Single<List<Watchlist>> {
        return watchlistApiClient.getWatchLists()
            .map { response ->
                response.mobileResponse.references?.firstOrNull()?.data?.map { dto ->
                    watchlistDtoMapper.map(dto)
                } ?: emptyList()
            }
    }

    override fun getWatchlistEntries(watchlistId: String, isExtendedHours: Boolean): Single<List<WatchlistEntry>> {
        return watchlistApiClient.getWatchListEntries(WatchlistEntriesRequest(WatchlistEntriesValue(watchlistId = watchlistId)))
            .flatMap { watchlistResponse ->
                if (watchlistResponse.data.messages?.code == EMPTY_ERROR_CODE) {
                    Single.just(emptyList())
                } else {
                    watchlistResponse.data.watchListEntries?.entries?.let { entries ->
                        if (isExtendedHours) {
                            val symbols = entries.mapNotNull { it.ticker }
                            getExtendedHoursWatchlistEntries(watchlistId, entries, symbols)
                        } else {
                            Single.just(
                                entries.map { watchlistEntry ->
                                    watchlistEntryDtoMapper.map(watchlistEntry, watchlistId)
                                }
                            )
                        }
                    } ?: Single.just(emptyList())
                }
            }
    }

    private fun getExtendedHoursWatchlistEntries(watchlistId: String, entries: List<WatchlistEntryDto>, symbols: List<String>): Single<List<WatchlistEntry>> {
        return if (symbols.isNotEmpty()) {
            Single.fromObservable(
                quoteRepo.loadMobileQuotes(symbols = symbols.joinToString(TICKERS_DELIMITER), extendedHours = true)
                    .map { quotes ->
                        val symbolToQuoteMap = quotes.associateBy { it.symbol }
                        entries.map { entry ->
                            val ticker = entry.ticker
                            if (ticker != null) {
                                val entryId = entry.entryId ?: entry.entryID
                                symbolToQuoteMap[ticker]?.toWatchlistEntry(entryId, true)?.apply {
                                    this.watchlistId = watchlistId
                                }
                                    ?: watchlistEntryDtoMapper.map(entry, watchlistId)
                            } else {
                                watchlistEntryDtoMapper.map(entry, watchlistId)
                            }
                        }
                    }
            )
        } else {
            Single.just(emptyList())
        }
    }

    override fun getWatchlistSymbols(watchlistId: String): Single<List<String>> {
        return getWatchlistEntries(watchlistId).map { entries -> entries.map { entry -> entry.ticker } }
    }

    override fun addToWatchList(watchlistId: String, ticker: String): Single<Boolean> {
        val addEntryStream = watchlistApiClient.addWatchListEntry(
            AddWatchlistEntryRequestDto(
                EditWatchListEntryDto(
                    watchlistID = watchlistId, symbol = ticker
                )
            )
        ).map {
            it.data?.watchlistEntry?.entries?.getOrNull(0)?.entryID != null
        }

        return getWatchlistEntries(watchlistId, false)
            .flatMap { entries ->
                if (entries.any { it.ticker == ticker }) {
                    throw DuplicateSymbolException()
                } else {
                    addEntryStream
                }
            }
    }

    override fun saveWatchList(watchlistId: String, entries: List<WatchlistEntry>, removedEntries: List<WatchlistEntry>): Single<Boolean> {
        val moved = prepareEditWatchlistEntriesDto(watchlistId, entries, "move")
        val deleted = prepareEditWatchlistEntriesDto(watchlistId, removedEntries, "delete")

        val entriesDto = EditWatchlistEntriesDto(
            mutableListOf<EditWatchListEntryDto>().apply {
                addAll(moved)
                addAll(deleted)
            }
        )

        return watchlistApiClient.saveWatchList(SaveWatchlistEntriesRequestDto(entriesDto)).map { it.data?.message?.code == "SUCCESS" }
    }

    override fun createWatchlist(watchlistName: String): Single<Watchlist> {
        return watchlistApiClient.createNewWatchList(CreateWatchlistRequestDto(CreateWatchlistRequestDataDto(watchlistName)))
            .map { response ->
                response.screen.references
                    ?.asSequence()
                    ?.filterIsInstance<CreateWatchlistResponseDto>()
                    ?.firstOrNull()
                    ?.convertToModel()
                    ?: throw IllegalArgumentException("Could not create a watchlist")
            }
    }

    override fun deleteWatchList(watchlistId: String): Single<Boolean> {
        return watchlistApiClient.deleteWatchList(DeleteWatchlistRequestDto(DeleteWatchlistDetailDto(watchlistId)))
            .map { it.data?.message?.code == "SUCCESS" }
    }

    private fun prepareEditWatchlistEntriesDto(watchlistId: String, entries: List<WatchlistEntry>, action: String): List<EditWatchListEntryDto> {
        return entries.mapIndexed { index, entry ->
            EditWatchListEntryDto(
                watchlistID = watchlistId,
                entryID = entry.entryID,
                symbol = entry.ticker,
                typeCode = entry.typeCode?.typeCode,
                action = action,
                newIndexID = index.toLong() + 1 // yes, indexes start from 1 ¯\_('')_/¯
            )
        }
    }

    // TODO replace with the specialized service call when its ready; in scope of ETAND-20092
    override fun getAllWatchlistsEntries(isExtendedHours: Boolean): Single<List<WatchlistEntry>> = getUserWatchlists()
        .flatMapObservable { watchlists ->
            Observable.fromIterable(watchlists)
        }
        .flatMapSingle { watchlist ->
            getWatchlistEntries(watchlist.id, isExtendedHours)
        }
        .collectInto(mutableListOf<WatchlistEntry>(), { accum, item -> accum.addAll(item) })
        .map { it.toList() }
}
