package com.etrade.mobilepro.sar

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.sar.service.DEFAULT_CHECK_INTERVAL_MILLIS
import com.etrade.mobilepro.sar.service.SarMessage
import com.etrade.mobilepro.sar.service.dto.SarResponseDto
import com.etrade.mobilepro.sar.service.repo.SarRepo
import com.etrade.mobilepro.session.api.SessionEvents
import com.etrade.mobilepro.session.api.SessionEvents.MSG_SESSION_SAR_TERMINATED
import com.etrade.mobilepro.session.api.SessionEvents.MSG_SESSION_TERMINATED
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.util.SESSION_COOKIE_NAME
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineScope
import okhttp3.Cookie
import okhttp3.CookieJar
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@Suppress("DEPRECATION")
@LooperMode(value = LooperMode.Mode.LEGACY)
@Config(application = Application::class)
class SarManagerTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testScope = TestCoroutineScope()
    private val mockObserver = mock<Observer<SessionEvents>>()

    private fun createSarClientRepo(result: ETResult<SarMessage>): SarRepo = mockk {
        coEvery { checkSar() } returns result
    }

    private fun mockLoggedIn(isLoggedIn: Boolean): UserAuthenticationState = { isLoggedIn }

    private fun mockCookieJar(cookies: List<Cookie> = emptyList()): CookieJar = mock {
        on { loadForRequest(anyOrNull()) } doReturn cookies
    }

    @Test
    fun `check if SAR manager terminates the session on SAR termination event`() {
        // Given
        val sarMessage = SarMessage(DEFAULT_CHECK_INTERVAL_MILLIS, terminateSession = true)
        val sut = SarManager(
            testScope,
            createSarClientRepo(ETResult.success(sarMessage)),
            mockLoggedIn(true),
            mockCookieJar(),
            ""
        )

        // When
        sut.start()
        sut.sessionEvent.observeForever(mockObserver)

        // Then
        verify(mockObserver, never()).onChanged(MSG_SESSION_TERMINATED)
        verify(mockObserver, times(1)).onChanged(MSG_SESSION_SAR_TERMINATED)
    }

    @Test
    fun `check if SAR manager terminates the session on SESSION termination event due to invalid cookie`() {
        // Given
        val domainName = "etrade.com"
        val baseUrl = "https://us.$domainName"
        val cookies = listOf(
            Cookie.Builder().apply {
                name(SESSION_COOKIE_NAME)
                value(LOGGED_OFF.removePrefix("$SESSION_COOKIE_NAME="))
                domain(domainName)
            }.build()
        )
        val sarMessage = SarMessage(DEFAULT_CHECK_INTERVAL_MILLIS, terminateSession = true)
        val sut = SarManager(
            testScope,
            createSarClientRepo(ETResult.success(sarMessage)),
            mockLoggedIn(true),
            mockCookieJar(cookies),
            baseUrl
        )

        // When
        sut.start()
        sut.sessionEvent.observeForever(mockObserver)

        // Then
        verify(mockObserver, never()).onChanged(MSG_SESSION_SAR_TERMINATED)
        verify(mockObserver, times(1)).onChanged(MSG_SESSION_TERMINATED)
    }

    @Test
    fun `check if SAR manager maintains the session on SAR check event`() {
        // Given
        val sarMessage = SarMessage(DEFAULT_CHECK_INTERVAL_MILLIS, terminateSession = false)
        val sut = SarManager(
            testScope,
            createSarClientRepo(ETResult.success(sarMessage)),
            mockLoggedIn(true),
            mockCookieJar(),
            ""
        )

        // When
        sut.start()
        sut.sessionEvent.observeForever(mockObserver)

        // Then
        verify(mockObserver, never()).onChanged(MSG_SESSION_SAR_TERMINATED)
    }

    @Test
    fun `check if SAR manager is not started then session on SAR termination event is not handled`() {
        // Given
        val sarMessage = SarMessage(DEFAULT_CHECK_INTERVAL_MILLIS, terminateSession = true)
        val sut = SarManager(
            testScope,
            createSarClientRepo(ETResult.success(sarMessage)),
            mockLoggedIn(true),
            mockCookieJar(),
            ""
        )

        // When
        sut.sessionEvent.observeForever(mockObserver)

        // Then
        verify(mockObserver, never()).onChanged(MSG_SESSION_SAR_TERMINATED)
    }

    @Test
    fun `check if SAR manager is not started when user not logged in`() {
        // Given
        val sarMessage = SarMessage(DEFAULT_CHECK_INTERVAL_MILLIS, terminateSession = true)
        val sut = SarManager(
            testScope,
            createSarClientRepo(ETResult.success(sarMessage)),
            mockLoggedIn(false),
            mockCookieJar(),
            ""
        )

        // When
        sut.start()
        sut.sessionEvent.observeForever(mockObserver)

        // Then
        verify(mockObserver, never()).onChanged(MSG_SESSION_SAR_TERMINATED)
    }

    @Test
    fun `test SAR response Dto`() {
        assert("sar_terminate.json", "CONCURRENT", true)
        assert("sar.json")
    }

    private fun assert(path: String, reason: String? = null, terminate: Boolean = false) {
        this::class.getObjectFromJson<SarResponseDto>(path).run {
            assertEquals(reason, terminateReason)
            assertEquals(30000L, nextCheckMillis)
            assertEquals(terminate, terminateSession)
        }
    }
}
