package com.etrade.mobilpro.presentation

import com.etrade.mobilepro.presentation.fixHref
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.`is`
import org.junit.Test

class ServerMessageUtilTest {

    @Test
    fun `fixHref replaces double quote path-only url`() {
        val html =
            """
                Relative <a href="/z/foo">Link 1</a>
            """.trimIndent()
        assertThat(
            html.fixHref("https://test.com"),
            `is`(
                """
                Relative <a href="https://test.com/z/foo">Link 1</a>
                """.trimIndent()
            )
        )
    }

    @Test
    fun `fixHref replaces single quote path-only url`() {
        val html =
            """
                <a href='/z/foo'>Link 1</a>
            """.trimIndent()
        assertThat(
            html.fixHref("https://test.com"),
            `is`(
                """
                <a href='https://test.com/z/foo'>Link 1</a>
                """.trimIndent()
            )
        )
    }

    @Test
    fun `fixHref ignores non-href attributes`() {
        val html =
            """
                <a attr1="foo" href='/z/foo' attr2='bar'>Link 1</a>
            """.trimIndent()
        assertThat(
            html.fixHref("https://test.com"),
            `is`(
                """
                <a attr1="foo" href='https://test.com/z/foo' attr2='bar'>Link 1</a>
                """.trimIndent()
            )
        )
    }

    @Test
    fun `fixHref ignores full urls`() {
        val html =
            """
                <a attr1="foo" href='https://test2.com/z/foo' attr2='bar'>Link 1</a>
            """.trimIndent()
        assertThat(
            html.fixHref("https://test.com"),
            `is`(
                """
                <a attr1="foo" href='https://test2.com/z/foo' attr2='bar'>Link 1</a>
                """.trimIndent()
            )
        )
    }

    @Test
    fun `fixHref replaces multiple relative urls`() {
        val html =
            """
                First Path <a attr="x" href="/z/foo">Link 1</a>
                Full <a href="https://x.y/z/bar">Link 2</a>
                Second Path <a href="/z/xxx">Link 3</a>
            """.trimIndent()
        assertThat(
            html.fixHref("https://test.com"),
            `is`(
                """
                First Path <a attr="x" href="https://test.com/z/foo">Link 1</a>
                Full <a href="https://x.y/z/bar">Link 2</a>
                Second Path <a href="https://test.com/z/xxx">Link 3</a>
                """.trimIndent()
            )
        )
    }

    @Test
    fun `fixHref fixes Hard-To-Buy server message`() {
        val html =
            "The underlying security is currently Hard to Borrow. If you acquire a short position through assignment or exercise, you will be assessed Hard-to-Borrow fees based on the opening transaction settlement date. To learn more, see the <a target=\"_blank\" href=\"/e/t/estation/pricing?id=1301024321\">HTB FAQ Page.</a>"
        val expected =
            "The underlying security is currently Hard to Borrow. If you acquire a short position through assignment or exercise, you will be assessed Hard-to-Borrow fees based on the opening transaction settlement date. To learn more, see the <a target=\"_blank\" href=\"https://us.etrade.com/e/t/estation/pricing?id=1301024321\">HTB FAQ Page.</a>"

        assertThat(html.fixHref("https://us.etrade.com"), `is`(expected))
    }
}
