package com.etrade.mobilepro.inputvalueview.validators

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class AlwaysValidValidatorTest {

    private val longString by lazy { "1".repeat(1000) }
    private val emptyString = ""
    private val blankString = " "

    @Test
    fun `always valid validator should return a valid value`() {
        val subject = AlwaysValidValidator()

        val longStringResult: InputFieldManager.Value = subject.validate(longString)
        assertTrue(longStringResult is InputFieldManager.Value.Valid)

        val emptyStringResult: InputFieldManager.Value = subject.validate(emptyString)
        assertTrue(emptyStringResult is InputFieldManager.Value.Valid)

        val blankStringResult: InputFieldManager.Value = subject.validate(blankString)
        assertTrue(blankStringResult is InputFieldManager.Value.Valid)
    }
}
