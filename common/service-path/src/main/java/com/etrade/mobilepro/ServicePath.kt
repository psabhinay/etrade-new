package com.etrade.mobilepro

import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.dynamicui.dto.request.AccountListRequestDto
import com.etrade.mobilepro.dynamicui.dto.request.AccountOverviewPayLoad
import com.etrade.mobilepro.dynamicui.dto.request.AccountOverviewRequestDto
import com.etrade.mobilepro.dynamicui.dto.request.BankAccountOverviewPayLoad
import com.etrade.mobilepro.dynamicui.dto.request.BankAccountOverviewRequestDto
import com.etrade.mobilepro.dynamicui.dto.request.CompleteViewRequestDto
import com.etrade.mobilepro.dynamicui.dto.request.DynamicMenuRequestDto
import com.etrade.mobilepro.dynamicui.dto.request.MainMenuRequestDto
import com.etrade.mobilepro.dynamicui.dto.request.MsCompleteViewRequestDto
import com.etrade.mobilepro.dynamicui.dto.request.OverviewRequestDto
import com.etrade.mobilepro.dynamicui.dto.request.StatementDocumentsDto
import com.etrade.mobilepro.dynamicui.dto.request.StatementDocumentsRequestDto
import com.etrade.mobilepro.dynamicui.dto.request.TaxDocumentsDto
import com.etrade.mobilepro.dynamicui.dto.request.TaxDocumentsRequestDto
import com.etrade.mobilepro.positions.data.dto.AllBrokeragePositionsDto
import com.etrade.mobilepro.positions.data.dto.AllBrokeragePositionsRequestDto
import com.etrade.mobilepro.positions.data.dto.IndividualPositions
import com.etrade.mobilepro.positions.data.dto.IndividualPositionsRequestDto
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.positions.data.dto.PortfolioTaxLotsRequestDto
import com.etrade.mobilepro.positions.data.dto.TaxLotRequestDto
import com.etrade.mobilepro.util.SortOrder
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.Locale

private const val PATH_SEGMENT_PHX = "phx"
private const val PATH_SEGMENT_ETM_SERVICES = "etm/services"
private const val PATH_SEGMENT_USER = "user"

private const val PATH_PHX_ETM_SERVICE = "$PATH_SEGMENT_PHX/$PATH_SEGMENT_ETM_SERVICES"
private const val ACCOUNT_V1 = "$PATH_PHX_ETM_SERVICE/v1/account"
private const val PORTFOLIO_V1 = "$PATH_PHX_ETM_SERVICE/v1/portfolio"
private const val USER_V1 = "$PATH_PHX_ETM_SERVICE/v1/$PATH_SEGMENT_USER"
private const val USER_V2 = "$PATH_SEGMENT_PHX/$PATH_SEGMENT_USER/$PATH_SEGMENT_ETM_SERVICES/v2/$PATH_SEGMENT_USER"

interface WithAccountId {
    val accountId: String
}

sealed class ServicePath(override val url: String) : ScreenRequest {

    override val contentKey: String
        get() = url

    object AccountList : ServicePath("$ACCOUNT_V1/accountList") {
        override fun createRequestBody(): Any = AccountListRequestDto()
    }

    abstract class BaseAccountOverView : ServicePath("$ACCOUNT_V1/accountOverview"), WithAccountId {
        abstract val accountUuid: String
        override val contentKey: String by lazy { "url:$accountUuid" }
    }

    data class AccountOverView(
        override val accountUuid: String,
        override val accountId: String,
        val widgetList: List<String>
    ) : BaseAccountOverView() {

        override fun createRequestBody(): Any = AccountOverviewRequestDto(AccountOverviewPayLoad(accountUuid, widgetList))
    }

    data class BankAccountOverView(
        override val accountUuid: String,
        override val accountId: String,
        val startDate: String,
        val endDate: String
    ) : BaseAccountOverView() {

        override fun createRequestBody(): Any = BankAccountOverviewRequestDto(
            BankAccountOverviewPayLoad(
                accountUuid = accountUuid,
                startDate = startDate,
                endDate = endDate
            )
        )
    }

    data class AllBrokeragePositions(val sortColumn: PortfolioRecordColumn, val sortOrder: SortOrder) : ServicePath("$PORTFOLIO_V1/all") {
        override fun createRequestBody(): Any {
            return AllBrokeragePositionsRequestDto(
                all = AllBrokeragePositionsDto(
                    sortColumn = sortColumn.code,
                    sortOrder = sortOrder.code
                )
            )
        }
    }

    object AuthenticatedSettings : ServicePath("$PATH_PHX_ETM_SERVICE/authenticatedSettings") {
        override fun createRequestBody(): Any? = null
    }

    data class Balances(val accountId: String, val userId: String) : ServicePath("$ACCOUNT_V1/balances") {
        override fun createRequestBody(): Any? = null
    }

    data class CompleteView(
        override val accountId: String
    ) : ServicePath("$ACCOUNT_V1/completeView"), WithAccountId {
        override fun createRequestBody(): Any = CompleteViewRequestDto()
    }

    object DynamicMenu : ServicePath("$USER_V1/mainMenu") {
        override fun createRequestBody(): DynamicMenuRequestDto = DynamicMenuRequestDto(
            mainMenu = MainMenuRequestDto(
                enableTaxDocuments = true,
                enableLearnMenu = true,
                enableDisclosuresMenu = true
            )
        )
    }

    /**
     * Service path for the Morgan Stanley integration complete view call
     */
    object MsCompleteView : ServicePath("$USER_V2/msCompleteView") {
        override fun createRequestBody() = MsCompleteViewRequestDto()
    }

    object OverView : ServicePath("$ACCOUNT_V1/overview") {
        override fun createRequestBody(): Any = OverviewRequestDto()
    }

    data class Portfolio(
        val accountUuid: String,
        val sortColumn: PortfolioRecordColumn,
        val sortOrder: SortOrder,
        val count: Int = 200,
        val nextPageToken: String? = null
    ) : ServicePath("$PORTFOLIO_V1/individual") {

        override val contentKey: String = "$url/$accountUuid/${sortColumn.code}/${sortOrder.code}"

        override fun createRequestBody(): Any {
            return IndividualPositionsRequestDto(
                individual = IndividualPositions(
                    accountUuid = accountUuid,
                    sortColumn = sortColumn.code,
                    sortOrder = sortOrder.code,
                    pageRecCount = count.toString(),
                    continuationToken = nextPageToken
                )
            )
        }
    }

    object Settings : ServicePath("$PATH_PHX_ETM_SERVICE/settings") {
        override fun createRequestBody(): Any? = null
    }

    data class TaxLots(val accountUuid: String, val positionId: String) : ServicePath("$PORTFOLIO_V1/lots") {
        override fun createRequestBody(): Any = PortfolioTaxLotsRequestDto(
            TaxLotRequestDto(
                accountUuid,
                positionId
            )
        )
    }

    data class StatementDocuments(
        val accountUuid: String,
        val startDate: LocalDate,
        val endDate: LocalDate,
        val appVersion: String
    ) : ServicePath("$PATH_PHX_ETM_SERVICE/v1/account/documents/statements") {
        private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US)

        override fun createRequestBody(): Any = StatementDocumentsRequestDto(
            StatementDocumentsDto(
                accountUuid, startDate.format(formatter), endDate.format(formatter), appVersion
            )
        )

        override val contentKey: String = "$url/$accountUuid/$startDate/$endDate"
    }

    data class TaxDocuments(
        val accountUuid: String,
        val year: Int,
        val appVersion: String
    ) : ServicePath("$PATH_PHX_ETM_SERVICE/v1/account/documents/taxDocuments") {
        override fun createRequestBody(): Any = TaxDocumentsRequestDto(
            TaxDocumentsDto(accountUuid, year.toString(), appVersion)
        )

        override val contentKey: String = "$url/$accountUuid/$year"
    }

    object MarketOverview : ServicePath("$PATH_PHX_ETM_SERVICE/marketOverview") {
        override fun createRequestBody(): Any? = null
    }

    data class AuthenticatedMarketOverview(
        val widgetList: List<String>
    ) : ServicePath("$PATH_PHX_ETM_SERVICE/authenticatedMarketOverview") {
        override fun createRequestBody(): Any? = null
    }
}
