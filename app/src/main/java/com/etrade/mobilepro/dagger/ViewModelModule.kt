@file:Suppress("TooManyFunctions")

package com.etrade.mobilepro.dagger

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.accounts.AccountsOrganizeBoardingViewModel
import com.etrade.mobilepro.alerts.presentation.AlertContainerViewModel
import com.etrade.mobilepro.alerts.presentation.AlertSelectionViewModel
import com.etrade.mobilepro.alerts.presentation.AlertsSharedViewModel
import com.etrade.mobilepro.alerts.presentation.account.AccountAlertsViewModel
import com.etrade.mobilepro.alerts.presentation.detail.AlertDetailViewModel
import com.etrade.mobilepro.alerts.presentation.detail.UserAlertDetailViewModel
import com.etrade.mobilepro.alerts.presentation.manage.ManageAlertViewModel
import com.etrade.mobilepro.alerts.presentation.market.MarketAlertsViewModel
import com.etrade.mobilepro.alerts.presentation.setalert.SetAlertViewModel
import com.etrade.mobilepro.appwidget.watchlist.selection.WatchlistSelectionViewModel
import com.etrade.mobilepro.balance.BalancesViewModel
import com.etrade.mobilepro.balance.viewmodel.AccountBalanceViewModel
import com.etrade.mobilepro.bank.BankHomeViewModel
import com.etrade.mobilepro.barcode.viewmodel.BarcodeReaderViewModel
import com.etrade.mobilepro.barcode.viewmodel.BarcodeResultsViewModel
import com.etrade.mobilepro.barcode.viewmodel.BarcodeSharedViewModel
import com.etrade.mobilepro.barcode.viewmodel.BarcodeViewModel
import com.etrade.mobilepro.biometric.presentation.enabler.BiometricEnablerViewModel
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvViewModel
import com.etrade.mobilepro.brokerage.AllBrokerageAccountsViewModel
import com.etrade.mobilepro.chart.netassets.NetAssetsChartViewModel
import com.etrade.mobilepro.chart.performance.PerformanceChartViewModel
import com.etrade.mobilepro.chartiq.presentation.ChartIqToolbarViewModel
import com.etrade.mobilepro.checkdeposit.presentation.activity.CheckDepositActivityViewModel
import com.etrade.mobilepro.checkdeposit.presentation.confirmation.CheckDepositConfirmationViewModel
import com.etrade.mobilepro.checkdeposit.presentation.deposit.CheckDepositViewModel
import com.etrade.mobilepro.checkdeposit.presentation.details.DepositDetailsViewModel
import com.etrade.mobilepro.checkdeposit.presentation.serviceagreement.CheckDepositServiceAgreementViewModel
import com.etrade.mobilepro.companyoverview.CompanyOverviewViewModel
import com.etrade.mobilepro.completeview.CompleteViewViewModel
import com.etrade.mobilepro.completeview.edit.EditCompleteViewViewModel
import com.etrade.mobilepro.dagger.factory.ViewModelInjectionFactory
import com.etrade.mobilepro.dagger.factory.ViewModelInjectionTracker
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.devtemplate.DevTemplateViewModel
import com.etrade.mobilepro.earnings.viewModel.EarningsDetailsViewModel
import com.etrade.mobilepro.education.EducationViewModel
import com.etrade.mobilepro.feature.presentation.viewmodel.FeatureFlagsViewModel
import com.etrade.mobilepro.feedback.presentation.FeedbackViewModel
import com.etrade.mobilepro.formsandapplications.presentation.FormsAndApplicationsViewModel
import com.etrade.mobilepro.home.AccountsTabHostViewModel
import com.etrade.mobilepro.home.BottomNavigationViewModel
import com.etrade.mobilepro.home.ToolbarViewModel
import com.etrade.mobilepro.home.model.AppActionViewModel
import com.etrade.mobilepro.home.model.MainActivityViewModel
import com.etrade.mobilepro.home.overview.edit.OverViewEditViewModel
import com.etrade.mobilepro.home.overview.viewmodel.OverviewViewModel
import com.etrade.mobilepro.inboxmessages.InboxMessagesViewModel
import com.etrade.mobilepro.launcher.AppLauncherViewModel
import com.etrade.mobilepro.level2.QuoteLevel2ViewModel
import com.etrade.mobilepro.markets.MarketsViewModel
import com.etrade.mobilepro.markets.edit.MarketsOverviewEditViewModel
import com.etrade.mobilepro.markets.futures.MarketsFuturesViewModel
import com.etrade.mobilepro.markets.movers.MarketsMoversViewModel
import com.etrade.mobilepro.markets.overview.MarketsOverviewViewModel
import com.etrade.mobilepro.marketscalendar.MarketsCalendarViewModel
import com.etrade.mobilepro.menu.viewmodel.AccountDetailsMenuViewModel
import com.etrade.mobilepro.menu.viewmodel.CustomerServiceMenuPageViewModel
import com.etrade.mobilepro.menu.viewmodel.MainMenuPageViewModel
import com.etrade.mobilepro.menu.viewmodel.ProfileMenuPageViewModel
import com.etrade.mobilepro.menu.viewmodel.TransferMenuPageViewModel
import com.etrade.mobilepro.news.presentation.NewsContainerViewModel
import com.etrade.mobilepro.news.presentation.NewsSharedViewModel
import com.etrade.mobilepro.news.presentation.company.CompanyNewsViewModel
import com.etrade.mobilepro.news.presentation.detail.TextNewsViewModel
import com.etrade.mobilepro.news.presentation.market.MarketNewsViewModel
import com.etrade.mobilepro.news.presentation.portfolio.PortfolioNewsViewModel
import com.etrade.mobilepro.news.presentation.watchlist.WatchlistNewsViewModel
import com.etrade.mobilepro.onboarding.LocalyticsOnboardingViewModel
import com.etrade.mobilepro.onboarding.OnboardingViewModel
import com.etrade.mobilepro.optionschain.presentation.OptionsChainConditionalOrderDisclosureViewModel
import com.etrade.mobilepro.optionschain.presentation.OptionsChainViewModel
import com.etrade.mobilepro.optionschain.tableview.OptionChainTableViewModel
import com.etrade.mobilepro.orders.sharedviewmodel.OrderSharedViewModel
import com.etrade.mobilepro.orders.viewmodel.OrdersListViewModel
import com.etrade.mobilepro.orders.viewmodel.OrdersViewModel
import com.etrade.mobilepro.personalnotifications.viewmodel.PersonalNotificationsViewModel
import com.etrade.mobilepro.portfolio.PortfolioToolTipViewModel
import com.etrade.mobilepro.portfolio.PortfolioViewModel
import com.etrade.mobilepro.portfolio.edit.EditPortfolioTableViewModel
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quote.screen.presentation.QuoteChartViewModel
import com.etrade.mobilepro.quote.screen.presentation.QuoteTabsViewModel
import com.etrade.mobilepro.quote.search.history.QuoteSearchHistoryViewModel
import com.etrade.mobilepro.quotelookup.SearchViewModel
import com.etrade.mobilepro.quotelookup.options.StockOptionsViewModel
import com.etrade.mobilepro.quotes.QuoteBottomSheetFragmentViewModel
import com.etrade.mobilepro.quotes.QuoteViewModel
import com.etrade.mobilepro.researchreport.ReportsViewModel
import com.etrade.mobilepro.review.AppReviewViewModel
import com.etrade.mobilepro.settings.viewmodel.SettingsViewModel
import com.etrade.mobilepro.timeout.widget.BiometricTimeoutMainActivityDelegateViewModel
import com.etrade.mobilepro.timeout.widget.SharedSessionTerminatedViewModel
import com.etrade.mobilepro.tooltip.TooltipViewModel
import com.etrade.mobilepro.trade.calculator.TradeCalculatorViewModel
import com.etrade.mobilepro.trade.help.TradeHelpViewModel
import com.etrade.mobilepro.trade.mutualfund.presentation.MutualFundTradeViewModel
import com.etrade.mobilepro.trade.optionlegselect.OptionLegSelectViewModel
import com.etrade.mobilepro.trade.optionlegselect.TradeLegBidAskViewModel
import com.etrade.mobilepro.trade.presentation.TradeConditionalOrderDisclosureViewModel
import com.etrade.mobilepro.trade.presentation.TradeViewModel
import com.etrade.mobilepro.trade.presentation.conditional.ConditionalOrderFormViewModel
import com.etrade.mobilepro.trade.presentation.confirmation.OrderConfirmationViewModel
import com.etrade.mobilepro.trade.presentation.util.SharedTradeViewModel
import com.etrade.mobilepro.tradingdefaults.TradingDefaultsViewModel
import com.etrade.mobilepro.transactions.viewmodel.TransactionsViewModel
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetClicksViewModel
import com.etrade.mobilepro.usersubscriptionpresentation.UserSubscriptionViewModel
import com.etrade.mobilepro.viewmodel.BackButtonInterceptorViewModel
import com.etrade.mobilepro.watchlist.presentation.EditWatchlistColumnsViewModel
import com.etrade.mobilepro.watchlist.presentation.EditWatchlistViewModel
import com.etrade.mobilepro.watchlist.presentation.tableview.WatchlistTableViewControlBarViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import org.chromium.customtabsclient.shared.NavigableWebViewModel

@Suppress("unused", "LargeClass")
@Module(
    includes = [
        BillPayViewModelModule::class,
        IraAccountViewModels::class,
        MarketsCalendarViewModelModule::class,
        MessageCenterViewModelModule::class,
        ScreenersViewModelModule::class,
        TradeModule::class,
        TransferMoneyViewModelModule::class
    ]
)
abstract class ViewModelModule {

    /* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */

    @Binds
    @IntoMap
    @ViewModelKey(AccountAlertsViewModel::class)
    abstract fun bindAccountAlertsViewModel(viewModel: AccountAlertsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountBalanceViewModel::class)
    abstract fun bindAccountBalanceViewModel(viewModel: AccountBalanceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountDetailsMenuViewModel::class)
    abstract fun bindAccountDetailsMenuViewModel(viewModel: AccountDetailsMenuViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountsOrganizeBoardingViewModel::class)
    abstract fun bindAccountsOrganizeBoardingViewModel(viewModel: AccountsOrganizeBoardingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountsTabHostViewModel::class)
    abstract fun bindAccountsTabHostViewModel(viewModel: AccountsTabHostViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ActionSheetClicksViewModel::class)
    abstract fun bindActionSheetClicksViewModel(viewModel: ActionSheetClicksViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AlertContainerViewModel::class)
    abstract fun bindAlertContainerViewModel(viewModel: AlertContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AlertDetailViewModel::class)
    abstract fun bindAlertDetailViewModel(viewModel: AlertDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AlertSelectionViewModel::class)
    abstract fun bindAlertSelectionViewModel(viewModel: AlertSelectionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AlertsSharedViewModel::class)
    abstract fun bindAlertsSharedViewModel(viewModel: AlertsSharedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AllBrokerageAccountsViewModel::class)
    abstract fun bindAllBrokerageAccountsViewModel(viewModel: AllBrokerageAccountsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AppActionViewModel::class)
    abstract fun bindAppActionViewModel(viewModel: AppActionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AppLauncherViewModel::class)
    abstract fun bindAppLauncherViewModel(viewModel: AppLauncherViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AppReviewViewModel::class)
    abstract fun bindAppReviewViewModel(viewModel: AppReviewViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BackButtonInterceptorViewModel::class)
    abstract fun bindBackButtonInterceptorViewModel(viewModel: BackButtonInterceptorViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BalancesViewModel::class)
    abstract fun bindBalancesViewModel(viewModel: BalancesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BankHomeViewModel::class)
    abstract fun bindBankHomeViewModel(viewModel: BankHomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BarcodeReaderViewModel::class)
    abstract fun bindBarcodeReaderViewModel(viewModel: BarcodeReaderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BarcodeResultsViewModel::class)
    abstract fun bindBarcodeResultViewModel(viewModel: BarcodeResultsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BarcodeSharedViewModel::class)
    abstract fun bindBarcodeSharedViewModel(viewModel: BarcodeSharedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BarcodeViewModel::class)
    abstract fun bindBarcodeViewModel(viewModel: BarcodeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BiometricEnablerViewModel::class)
    abstract fun bindBiometricEnablerViewModel(viewModel: BiometricEnablerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConditionalOrderFormViewModel::class)
    abstract fun bindConditionalOrderFormViewModel(viewModel: ConditionalOrderFormViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BiometricTimeoutMainActivityDelegateViewModel::class)
    abstract fun bindBiometricTimeoutMainActivityDelegateViewModel(viewModel: BiometricTimeoutMainActivityDelegateViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BloombergTvViewModel::class)
    abstract fun bindBloombergtvViewModel(viewModel: BloombergTvViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BottomNavigationViewModel::class)
    abstract fun bindBottomNavigationViewModel(viewModel: BottomNavigationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChartIqToolbarViewModel::class)
    abstract fun bindChartIqToolbarViewModel(viewModel: ChartIqToolbarViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CheckDepositViewModel::class)
    abstract fun bindCheckDepositViewModel(viewModel: CheckDepositViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CheckDepositConfirmationViewModel::class)
    abstract fun bindCheckDepositConfirmationViewModel(viewModel: CheckDepositConfirmationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CheckDepositServiceAgreementViewModel::class)
    abstract fun bindCheckDepositServiceAgreementViewModel(viewModel: CheckDepositServiceAgreementViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CheckDepositActivityViewModel::class)
    abstract fun bindCheckDepositActivityViewModel(viewModel: CheckDepositActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CompanyNewsViewModel::class)
    abstract fun bindCompanyNewsViewModel(viewModel: CompanyNewsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CompanyOverviewViewModel::class)
    abstract fun bindCompanyOverviewViewModel(viewModel: CompanyOverviewViewModel): ViewModel

    @FlowPreview
    @ExperimentalCoroutinesApi
    @Binds
    @IntoMap
    @ViewModelKey(CompleteViewViewModel::class)
    abstract fun bindCompleteViewViewModel(viewModel: CompleteViewViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CustomerServiceMenuPageViewModel::class)
    abstract fun bindCustomerServiceMenuPageViewModel(viewModel: CustomerServiceMenuPageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DepositDetailsViewModel::class)
    abstract fun bindDepositDetailsViewModel(viewModel: DepositDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DevTemplateViewModel::class)
    abstract fun bindDevTemplateViewModel(viewModel: DevTemplateViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EarningsDetailsViewModel::class)
    abstract fun bindEarningsDetailsViewModel(viewModel: EarningsDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditCompleteViewViewModel::class)
    abstract fun bindEditCompleteViewViewModel(viewModel: EditCompleteViewViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditPortfolioTableViewModel::class)
    abstract fun bindEditPortfolioTableViewModel(viewModel: EditPortfolioTableViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditWatchlistViewModel::class)
    abstract fun bindEditWatchlistViewModel(viewModel: EditWatchlistViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditWatchlistColumnsViewModel::class)
    abstract fun bindEditWatchlistColumnsViewModel(viewModel: EditWatchlistColumnsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EducationViewModel::class)
    abstract fun bindEducationViewModel(viewModel: EducationViewModel): ViewModel

    @ExperimentalCoroutinesApi
    @Binds
    @IntoMap
    @ViewModelKey(FeatureFlagsViewModel::class)
    abstract fun bindFeatureFlagViewModel(viewModel: FeatureFlagsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FeedbackViewModel::class)
    abstract fun bindFeedbackViewModel(viewModel: FeedbackViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FormsAndApplicationsViewModel::class)
    abstract fun bindFormsAndApplicationsViewModel(viewModel: FormsAndApplicationsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InboxMessagesViewModel::class)
    abstract fun bindInboxMessagesViewModel(viewModel: InboxMessagesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewsSharedViewModel::class)
    abstract fun bindNewsSharedViewModel(viewModel: NewsSharedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainMenuPageViewModel::class)
    abstract fun bindMainMenuPageViewModel(viewModel: MainMenuPageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ManageAlertViewModel::class)
    abstract fun bindManageAlertViewModel(viewModel: ManageAlertViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MarketsViewModel::class)
    abstract fun bindMarketsViewModel(viewModel: MarketsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MarketNewsViewModel::class)
    abstract fun bindMarketNewsViewModel(viewModel: MarketNewsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MarketsOverviewViewModel::class)
    abstract fun bindMarketsOverviewViewModel(viewModel: MarketsOverviewViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MarketsFuturesViewModel::class)
    abstract fun bindMarketsFuturesViewModel(viewModel: MarketsFuturesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MarketAlertsViewModel::class)
    abstract fun bindMarketAlertsViewModel(viewModel: MarketAlertsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MarketsCalendarViewModel::class)
    abstract fun bindMarketsCalendarViewModel(viewModel: MarketsCalendarViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MarketsMoversViewModel::class)
    abstract fun bindMarketsMoversViewModel(viewModel: MarketsMoversViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MarketsOverviewEditViewModel::class)
    abstract fun bindMarketsOverviewEditViewModel(viewModel: MarketsOverviewEditViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MutualFundTradeViewModel::class)
    abstract fun bindMutualFundTradeViewModel(viewModel: MutualFundTradeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NavigableWebViewModel::class)
    abstract fun bindNavigableViewModel(viewModel: NavigableWebViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NetAssetsChartViewModel::class)
    abstract fun bindNetAssetsChartViewModel(viewModel: NetAssetsChartViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewsContainerViewModel::class)
    abstract fun bindNewsViewModel(viewModel: NewsContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OnboardingViewModel::class)
    abstract fun bindOnboardingViewModel(viewModel: OnboardingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LocalyticsOnboardingViewModel::class)
    abstract fun bindLocalyticsOnboardingViewModel(viewModel: LocalyticsOnboardingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OptionsChainConditionalOrderDisclosureViewModel::class)
    abstract fun bindOptionsChainConditionalOrderDisclosureViewModel(viewModel: OptionsChainConditionalOrderDisclosureViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OptionChainTableViewModel::class)
    abstract fun bindOptionChainTableViewModel(viewModel: OptionChainTableViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OptionLegSelectViewModel::class)
    abstract fun bindOptionLegSelectViewModel(viewModel: OptionLegSelectViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OptionsChainViewModel::class)
    abstract fun bindOptionsChainViewModel(viewModel: OptionsChainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderConfirmationViewModel::class)
    abstract fun bindOrderConfirmationViewModel(viewModel: OrderConfirmationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrdersViewModel::class)
    abstract fun bindOrdersViewModel(viewModel: OrdersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrdersListViewModel::class)
    abstract fun bindOpenOrdersViewModel(viewModel: OrdersListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderSharedViewModel::class)
    abstract fun bindOrderSharedViewModel(viewModel: OrderSharedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OverViewEditViewModel::class)
    abstract fun bindOverViewEditViewModel(viewModel: OverViewEditViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OverviewViewModel::class)
    abstract fun bindOverviewViewModel(viewModel: OverviewViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PerformanceChartViewModel::class)
    abstract fun bindPerformanceChartViewModel(viewModel: PerformanceChartViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PersonalNotificationsViewModel::class)
    abstract fun bindPersonalNotificationsViewModel(viewModel: PersonalNotificationsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PortfolioNewsViewModel::class)
    abstract fun bindPortfolioNewsViewModel(viewModel: PortfolioNewsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PortfolioToolTipViewModel::class)
    abstract fun bindPortfolioTooltipViewModel(viewModel: PortfolioToolTipViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PortfolioViewModel::class)
    abstract fun bindPortfolioViewModel(viewModel: PortfolioViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileMenuPageViewModel::class)
    abstract fun bindProfileMenuPageViewModel(viewModel: ProfileMenuPageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QuoteBottomSheetFragmentViewModel::class)
    abstract fun bindQuoteActivityViewModel(viewModel: QuoteBottomSheetFragmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QuoteChartViewModel::class)
    abstract fun bindQuoteChartViewModel(viewModel: QuoteChartViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QuoteLevel2ViewModel::class)
    abstract fun bindQuoteLevel2ViewModel(viewModel: QuoteLevel2ViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QuoteSearchHistoryViewModel::class)
    abstract fun bindQuoteSearchHistoryViewModel(viewModel: QuoteSearchHistoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QuoteTabsViewModel::class)
    abstract fun bindQuoteTabsViewModel(viewModel: QuoteTabsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QuotesWidgetViewModel::class)
    abstract fun bindQuotesViewModel(viewModel: QuotesWidgetViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QuoteViewModel::class)
    abstract fun bindQuoteViewModel(viewModel: QuoteViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReportsViewModel::class)
    abstract fun bindReportsViewModel(viewModel: ReportsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(viewModel: SearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SetAlertViewModel::class)
    abstract fun bindSetAlertViewModel(viewModel: SetAlertViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SettingsViewModel::class)
    abstract fun bindSettingsViewModel(viewModel: SettingsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SharedSessionTerminatedViewModel::class)
    abstract fun bindSessionTerminatedViewModel(viewModel: SharedSessionTerminatedViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SharedTradeViewModel::class)
    abstract fun bindSharedTradeViewModel(viewModel: SharedTradeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StockOptionsViewModel::class)
    abstract fun bindStockOptionsViewModel(viewModel: StockOptionsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TextNewsViewModel::class)
    abstract fun bindTextNewsViewModel(viewModel: TextNewsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ToolbarViewModel::class)
    abstract fun bindToolbarViewModel(viewModel: ToolbarViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TooltipViewModel::class)
    abstract fun bindTooltipViewModel(viewModel: TooltipViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TradeCalculatorViewModel::class)
    abstract fun bindTradeCalculatorViewModel(viewModel: TradeCalculatorViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TradeConditionalOrderDisclosureViewModel::class)
    abstract fun bindTradeConditionalOrderDisclosureViewModel(viewModel: TradeConditionalOrderDisclosureViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TradeHelpViewModel::class)
    internal abstract fun bindTradeHelpViewModel(viewModel: TradeHelpViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TradeLegBidAskViewModel::class)
    abstract fun bindTradeLegBidAskViewModel(viewModel: TradeLegBidAskViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TradeViewModel::class)
    abstract fun bindTradeViewModel(viewModel: TradeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TradingDefaultsViewModel::class)
    abstract fun bindTradingDefaultsViewModel(viewModel: TradingDefaultsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransactionsViewModel::class)
    abstract fun bindTransactionsViewModel(viewModel: TransactionsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransferMenuPageViewModel::class)
    abstract fun bindTransferMenuPageViewModel(viewModel: TransferMenuPageViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserAlertDetailViewModel::class)
    abstract fun bindUserAlertDetailViewModel(viewModel: UserAlertDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserSubscriptionViewModel::class)
    abstract fun bindUserSubscriptionViewModel(viewModel: UserSubscriptionViewModel): ViewModel

    @Binds
    abstract fun bindViewModelInjectingTracker(tracker: DefaultViewModelInjectionTracker): ViewModelInjectionTracker

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelInjectionFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(WatchlistNewsViewModel::class)
    abstract fun bindWatchlistNewsViewModel(viewModel: WatchlistNewsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WatchlistSelectionViewModel::class)
    abstract fun bindWatchlistSelectionViewModel(viewModel: WatchlistSelectionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WatchlistTableViewControlBarViewModel::class)
    abstract fun bindWatchlistTableViewControlBarViewModel(viewModel: WatchlistTableViewControlBarViewModel): ViewModel
}
