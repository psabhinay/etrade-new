package org.chromium.customtabsclient.shared

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.navigation.navArgs
import dagger.android.AndroidInjection
import java.net.URLDecoder
import javax.inject.Inject

/**
 * Transparent proxy activity which decides how to open an URL. It can use [WebviewActivity] for
 * internal web pages and [WebViewHelper] for external web pages.
 *
 * Note: This is class is likely called using deeplinks defined in the app's nav graph.
 */
class WebViewProxyActivity : AppCompatActivity() {

    @Inject
    lateinit var webViewHelper: WebViewHelper

    private val args: WebViewProxyActivityArgs? by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        args?.webUrl?.let {
            val externalBrowser = args?.externalUrlView ?: false
            webViewHelper.openCustomTab(decodeText(it), ContextCompat.getColor(this, R.color.plum), externalBrowser)
            finish()
        }

        args?.webViewUrl?.let { url ->
            val title = args?.webViewTitle ?: ""
            startActivity(
                WebviewActivity.intent(
                    this,
                    decodeText(url),
                    decodeText(title),
                    args?.upIconResId ?: 0,
                    args?.externalUrlView ?: false
                )
            )
            finish()
        }
    }

    private fun decodeText(text: String): String {
        return URLDecoder.decode(text, "utf-8")
    }
}
