package com.etrade.mobilepro.searchingapi

import com.etrade.mobilepro.searchingapi.items.SearchQuery

interface SearchQueryValidator {
    fun makeValidOrEmpty(query: SearchQuery): SearchQuery
}
