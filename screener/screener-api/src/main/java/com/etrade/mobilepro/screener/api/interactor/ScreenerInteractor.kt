package com.etrade.mobilepro.screener.api.interactor

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.result.ScreenerResult
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import io.reactivex.Single

interface ScreenerInteractor {
    fun preScreenResults(): Observable<Resource<Int>>
    fun retryPrescreen()
    fun getFiltersSnapshot(): List<ScreenerFilter>

    fun combinedScreenersResult(): Single<ScreenerResult>
}
