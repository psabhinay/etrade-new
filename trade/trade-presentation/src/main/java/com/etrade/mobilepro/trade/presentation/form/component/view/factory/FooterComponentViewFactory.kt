package com.etrade.mobilepro.trade.presentation.form.component.view.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.ui.factory.ComponentViewFactory
import com.etrade.mobilepro.trade.presentation.form.component.FooterComponent
import com.etrade.mobilepro.trade.presentation.form.component.view.FooterComponentView

internal class FooterComponentViewFactory : ComponentViewFactory<FooterComponent, FooterComponentView> {
    override fun createView(context: Context): FooterComponentView =
        FooterComponentView(context)
}
