@file:Suppress("unused")

package com.etrade.mobilepro.dynamicui.presentation

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData

@BindingAdapter("onDynamicUiViewStateChange")
fun handleDynamicUiViewStateLoading(view: View, viewState: LiveData<DynamicUiViewState>?) {
    when (viewState?.value) {
        DynamicUiViewState.Loading -> view.visibility = View.VISIBLE
        else -> view.visibility = View.GONE
    }
}

@BindingAdapter("onDynamicUiViewStateCompleted")
fun handleDynamicUiViewStateCompleted(view: View, viewState: LiveData<DynamicUiViewState>?) {
    when (viewState?.value) {
        DynamicUiViewState.Loading -> view.visibility = View.GONE
        else -> view.visibility = View.VISIBLE
    }
}

@BindingAdapter("onDynamicUiViewStateSuccess")
fun handleDynamicUiViewStateSuccess(view: View, viewState: LiveData<DynamicUiViewState>?) {
    when (viewState?.value) {
        DynamicUiViewState.Success -> view.visibility = View.VISIBLE
        else -> {
            view.visibility = View.GONE
        }
    }
}

@BindingAdapter("onDynamicUiViewStateError")
fun handleDynamicUiViewStateError(view: View, viewState: LiveData<DynamicUiViewState>?) {
    when (viewState?.value) {
        is DynamicUiViewState.Error -> view.visibility = View.VISIBLE
        else -> view.visibility = View.GONE
    }
}

@BindingAdapter("onDynamicUiViewStateNotSuccess")
fun handleDynamicUiViewStateNotSuccess(view: View, viewState: LiveData<DynamicUiViewState>?) {
    when (viewState?.value) {
        DynamicUiViewState.Success -> view.visibility = View.GONE
        else -> view.visibility = View.VISIBLE
    }
}

@BindingAdapter("onDynamicUiViewStateWarning")
fun handleDynamicUiViewStateWarning(view: View, viewState: LiveData<DynamicUiViewState>?) {

    when (val state = viewState?.value) {
        is DynamicUiViewState.Warning -> {
            view.visibility = View.VISIBLE
            if (view is TextView) {
                view.text = state.message
            }
        }
        else -> view.visibility = View.GONE
    }
}

@BindingAdapter("onDynamicUiViewStateHideOnError")
fun onDynamicUiViewStateHideOnError(view: View, viewState: LiveData<DynamicUiViewState>?) {
    if (viewState?.value is DynamicUiViewState.Error) {
        view.visibility = View.GONE
    }
}

@BindingAdapter("onDynamicUiViewStateHideOnSuccess")
fun onDynamicUiViewStateHideOnSuccess(view: View, viewState: LiveData<DynamicUiViewState>?) {
    if (viewState?.value is DynamicUiViewState.Success) {
        view.visibility = View.GONE
    }
}
