package com.etrade.mobilepro.orders.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.databinding.ItemOrderSectionHeaderBinding
import com.etrade.mobilepro.news.presentation.adapter.EmptyNewsItemViewHolder
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem
import com.etrade.mobilepro.order.details.presentation.OrderOptionsRatio
import com.etrade.mobilepro.quote.presentation.AffiliatedProductAction
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quote.presentation.databinding.QuoteWidgetBidAskBinding
import com.etrade.mobilepro.quoteapi.FundsTradeTitleViewItem
import com.etrade.mobilepro.quoteapi.QuoteHeaderViewItem
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem
import com.etrade.mobilepro.trade.presentation.VARIABLE_ID_QUOTE_ITEM
import com.etrade.mobilepro.trade.presentation.databinding.TradeItemOrderLegBinding
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.viewBinding

class OrderDetailAdapter(
    private val lifecycleOwner: LifecycleOwner,
    private var orderItems: MutableList<OrderDetailItemInterface>,
    private val viewModelProvider: ViewModelProvider,
    private val onAffiliatedProductClickListener: AffiliatedProductAction
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val bindingViewType = setOf(
        R.layout.item_order_normalized_ratios,
        R.layout.item_order_detail_section,
        R.layout.quote_widget_for_order,
        R.layout.quote_view_base,
        R.layout.quote_view_base_with_retry
    )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is OrderBidAskViewHolder -> {
                val bidAskItem = orderItems[position] as? OrderDetailDisplayItem.OrderBidAskItem
                (bidAskItem as? OrderDetailDisplayItem.OrderBidAskItem.DynamicOrderBidAskItemWithSymbol)?.let {
                    it.updateWithViewModel(getQuotesWidgetViewModel(it.symbol))
                }
                holder.bindTo(bidAskItem)
            }
            is OrderDividerViewHolder -> { /* Do Nothing */ }
            is OrderLegDetailViewHolder -> holder.bindTo(orderItems[position])
            is OrderSectionHeaderViewHolder -> holder.bindTo(orderItems[position] as? OrderDetailDisplayItem.OrderSectionHeader)
            else -> {
                var orderItem = orderItems[position]
                if (orderItem is OrderDetailDisplayItem.OrderSectionItem && position == orderItems.size - 1) {
                    orderItem = orderItem.copy(dividerVisibility = View.GONE)
                }
                onBindOrderDetailBindingViewHolder(holder as OrderDetailBindingViewHolder, orderItem)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = if (viewType in bindingViewType) {
        onDataBindingView(parent, viewType)
    } else {
        onNonDataBindingView(parent, viewType)
    }

    private fun onDataBindingView(parent: ViewGroup, viewType: Int): OrderDetailBindingViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context), viewType, parent, false
        )
        binding.lifecycleOwner = lifecycleOwner
        return OrderDetailBindingViewHolder(
            binding,
            when (viewType) {
                R.layout.quote_view_base_with_retry -> VARIABLE_ID_QUOTE_ITEM
                R.layout.quote_view_base -> BR.quoteItem
                R.layout.item_order_normalized_ratios -> BR.normalizedRatios
                else -> BR.orderItem
            }
        )
    }

    private fun onNonDataBindingView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.trade_item_order_leg -> OrderLegDetailViewHolder(
                parent.viewBinding(
                    TradeItemOrderLegBinding::inflate
                )
            )
            R.layout.quote_widget_bid_ask -> OrderBidAskViewHolder(
                parent.viewBinding(
                    QuoteWidgetBidAskBinding::inflate
                ),
                lifecycleOwner
            )
            R.layout.item_order_section_header -> OrderSectionHeaderViewHolder(
                parent.viewBinding(
                    ItemOrderSectionHeaderBinding::inflate
                )
            )
            R.layout.item_divider -> OrderDividerViewHolder(parent.inflate(viewType))
            else -> EmptyNewsItemViewHolder(parent.inflate(viewType))
        }
    }

    @Suppress("ComplexMethod") // nothing to extract
    override fun getItemViewType(position: Int): Int {
        return when (orderItems[position]) {
            is OrderDetailDisplayItem.OrderBidAskItem -> R.layout.quote_widget_bid_ask
            is OrderDetailDisplayItem.OrderExtraLegItem, is OrderDetailDisplayItem.OrderLegItem -> R.layout.trade_item_order_leg
            is OrderDetailDisplayItem.OrderSectionHeader -> R.layout.item_order_section_header
            is OrderOptionsRatio -> R.layout.item_order_normalized_ratios
            is OrderDetailDisplayItem.OrderSectionItem -> R.layout.item_order_detail_section
            is OrderDetailDisplayItem.OrderSectionDividerItem -> R.layout.item_divider
            is QuoteWidgetItem -> R.layout.quote_widget_for_order
            is QuoteHeaderViewItem -> R.layout.quote_view_base
            is QuotesWidgetViewModel -> R.layout.quote_view_base_with_retry
            else -> R.layout.item_empty_news
        }
    }

    override fun getItemCount(): Int {
        return orderItems.size
    }

    private fun onBindOrderDetailBindingViewHolder(holder: OrderDetailBindingViewHolder, orderItem: OrderDetailItemInterface) {
        when (orderItem) {
            is QuoteWidgetItem -> {
                val viewModel = getQuotesWidgetViewModel(orderItem.symbol)
                viewModel.updateQuotes(orderItem)
                holder.bindWithAffiliatedAction(viewModel, BR.affiliatedProductAction, onAffiliatedProductClickListener)
            }
            is FundsTradeTitleViewItem, is QuotesWidgetViewModel -> {
                holder.bindWithAffiliatedAction(orderItem, BR.affiliatedProductAction, onAffiliatedProductClickListener)
            }
            else -> {
                holder.bindTo(orderItem)
            }
        }
    }

    private fun getQuotesWidgetViewModel(symbol: String): QuotesWidgetViewModel {
        val key = QuotesWidgetViewModel::class.java.name + symbol
        return viewModelProvider.get(key, QuotesWidgetViewModel::class.java)
    }
}
