package com.etrade.mobilepro.ui_demo.actionsheet.default

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.ui_demo.R
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetViewModel

class DefaultStyleViewModel : ActionSheetViewModel {
    private val _actions = MutableLiveData<List<ActionItem>>()
    override val actions: LiveData<List<ActionItem>>
        get() = _actions
    private val _result = MutableLiveData<String>()
    internal val result: LiveData<String>
        get() = _result

    override fun selectAction(item: ActionItem) {
        _result.value = "$item selected"
        _actions.value = emptyList()
    }

    override fun cancelActions() {
        _result.value = "cancelled"
        _actions.value = emptyList()
    }

    fun showActionSheet() {
        _actions.value = listOf(
            ActionItem(
                id = 0,
                displayNameId = R.string.action_sheet_item1
            ),
            ActionItem(
                id = 1,
                displayNameId = R.string.action_sheet_item2
            )
        )
    }
}
