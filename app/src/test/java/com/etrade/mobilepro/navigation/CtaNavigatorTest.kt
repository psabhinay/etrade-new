package com.etrade.mobilepro.navigation

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.dynamicui.api.CtaEvent.AccountsTabNavigation
import com.etrade.mobilepro.dynamicui.api.CtaEvent.NavDirectionsNavigation
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

const val samplePortfolioCtaString = "etrade://account/1234567/portfolio"
const val sampleSavedOrdersCtaString = "etrade://account/1234567/orders/saved"
const val sampleOpenOrdersCtaString = "etrade://account/1234567/orders/open"
const val sampleOpenHoldingsCtaString = "etrade://account/1234567/holdings?uuid=UUID"
const val sampleOpenOverviewCtaString = "etrade://account/1234567/overview?uuid=UUID"

val ctaDestinationPortfolio = AccountsTabNavigation("1234567", "portfolio", null)
val ctaDestinationOrderSaved = AccountsTabNavigation("1234567", "orders", "saved")
val ctaDestinationOrderOpen = AccountsTabNavigation("1234567", "orders", "open")
val ctaDestinationHoldings = NavDirectionsNavigation(
    MainGraphDirections.actionHomeToAccountTabs("1234567", "UUID", "holdings", null)
)
val ctaDestinationBankOverview = NavDirectionsNavigation(
    MainGraphDirections.actionHomeToBank("UUID", "1234567")
)

@RunWith(AndroidJUnit4::class)
@Config(application = android.app.Application::class)
class CtaNavigatorTest {

    @Test
    fun `test sample cta portfolio destination`() {
        assertEquals(ctaDestinationPortfolio, getTabDestination(samplePortfolioCtaString))
    }

    @Test
    fun `test sample cta saved orders destination`() {
        assertEquals(ctaDestinationOrderSaved, getTabDestination(sampleSavedOrdersCtaString))
    }

    @Test
    fun `test sample cta opened orders destination`() {
        assertEquals(ctaDestinationOrderOpen, getTabDestination(sampleOpenOrdersCtaString))
    }

    @Test
    fun `test sample cta opened holdings destination`() {
        assertEquals(ctaDestinationHoldings, getNavDirections(sampleOpenHoldingsCtaString))
    }

    @Test
    fun `test sample cta opened bank view destination`() {
        assertEquals(ctaDestinationBankOverview, getNavDirections(sampleOpenOverviewCtaString, isBankAccount = true))
    }
}
