package com.etrade.mobilepro.util.updater

import androidx.core.database.sqlite.transaction
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.portfolio.PortfolioPositionsDisplayMode
import com.etrade.mobilepro.portfolio.data.PortfolioColumnRepo
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream

@RunWith(AndroidJUnit4::class)
@Config(application = android.app.Application::class)
class PortfolioColumnMigrationSuite {

    private val repo = InMemoryPortfolioColumnRepo()
    private val migration: Migration = PortfolioColumnMigration(repo)

    private val standardViewColumns: List<Pair<Int, PortfolioColumn>>
        get() = listOf(
            10 to PortfolioColumn.LAST_PRICE,
            11 to PortfolioColumn.DAYS_GAIN_DOLLAR,
            13 to PortfolioColumn.TOTAL_GAIN_DOLLAR,
            12 to PortfolioColumn.DAYS_GAIN_PERCENT,
            14 to PortfolioColumn.TOTAL_GAIN_PERCENT,
            16 to PortfolioColumn.BID,
            17 to PortfolioColumn.ASK,
            18 to PortfolioColumn.QUANTITY,
            19 to PortfolioColumn.PRICE_PAID,
            20 to PortfolioColumn.MARKET_VALUE,
            21 to PortfolioColumn.CHANGE_PERCENT,
            22 to PortfolioColumn.CHANGE_DOLLAR,
            23 to PortfolioColumn.TODAYS_CLOSE,
            24 to PortfolioColumn.VOLUME,
            25 to PortfolioColumn.MARKET_CAP,
            26 to PortfolioColumn.PRICE_EARNING_RATIO,
            27 to PortfolioColumn.EARNINGS_PER_SHARE,
            28 to PortfolioColumn.FIFTY_TWO_WEEK_HIGH,
            29 to PortfolioColumn.FIFTY_TWO_WEEK_LOW
        ).shuffled()

    private val optionsViewColumns: List<Pair<Int, PortfolioColumn>>
        get() = listOf(
            15 to PortfolioColumn.MARK,
            18 to PortfolioColumn.QUANTITY,
            22 to PortfolioColumn.CHANGE_DOLLAR,
            19 to PortfolioColumn.PRICE_PAID,
            13 to PortfolioColumn.TOTAL_GAIN_DOLLAR,
            11 to PortfolioColumn.DAYS_GAIN_DOLLAR,
            37 to PortfolioColumn.BASE_SYMBOL_AND_PRICE,
            36 to PortfolioColumn.DAYS_TO_EXPIRE,
            16 to PortfolioColumn.BID,
            17 to PortfolioColumn.ASK,
            10 to PortfolioColumn.LAST_PRICE,
            31 to PortfolioColumn.DELTA,
            32 to PortfolioColumn.GAMMA,
            34 to PortfolioColumn.THETA,
            33 to PortfolioColumn.VEGA,
            30 to PortfolioColumn.IMPLIED_VOLATILITY_PERCENT
        ).shuffled()

    private val listViewColumns: List<Pair<Int, PortfolioColumn>>
        get() = listOf(
            19 to PortfolioColumn.PRICE_PAID,
            18 to PortfolioColumn.QUANTITY,
            11 to PortfolioColumn.DAYS_GAIN_DOLLAR,
            12 to PortfolioColumn.DAYS_GAIN_PERCENT,
            13 to PortfolioColumn.TOTAL_GAIN_DOLLAR,
            14 to PortfolioColumn.TOTAL_GAIN_PERCENT
        ).shuffled()

    private val tileViewColumns: List<Pair<Int, PortfolioColumn>>
        get() = listOf(
            10 to PortfolioColumn.LAST_PRICE,
            11 to PortfolioColumn.DAYS_GAIN_DOLLAR,
            12 to PortfolioColumn.DAYS_GAIN_PERCENT
        ).shuffled()

    @Test
    fun `test migration success`() = withDatabaseValues(
        standardViewColumns,
        optionsViewColumns,
        listViewColumns,
        tileViewColumns
    ) { expected, actual ->
        actual.clear()
        repo.saveCalled = false
        repo.savePortfolioColumns = true
        migration.migrate(databaseFile)
        assertTrue(repo.saveCalled)
        assertEquals(expected, actual)
    }

    @Test
    fun `test empty watchlist`() =
        withDatabaseValues(emptyList(), emptyList(), emptyList(), emptyList()) { _, actual ->
            actual.clear()
            repo.saveCalled = false
            repo.savePortfolioColumns = true
            migration.migrate(databaseFile)
            assertFalse(repo.saveCalled)
            assertTrue(actual.isEmpty())
        }

    @Test
    fun `test repo save failed`() =
        withDatabaseValues(
            standardViewColumns,
            optionsViewColumns,
            listViewColumns,
            tileViewColumns
        ) { _, actual ->
            actual.clear()
            repo.saveCalled = false
            repo.savePortfolioColumns = false
            migration.migrate(databaseFile)
            assertTrue(repo.saveCalled)
            assertTrue(actual.isEmpty())
        }

    private fun withDatabaseValues(
        standardColumns: List<Pair<Int, PortfolioColumn>>,
        optionsColumns: List<Pair<Int, PortfolioColumn>>,
        listColumns: List<Pair<Int, PortfolioColumn>>,
        tileColumns: List<Pair<Int, PortfolioColumn>>,
        block: suspend (List<PortfolioColumn>, MutableList<PortfolioColumn>) -> Unit
    ) {
        withExistingDatabaseFile({ database ->
            database.transaction {
                execSQL("CREATE TABLE IF NOT EXISTS PORTFOLIOTBL( EntryId INTEGER PRIMARY KEY AUTOINCREMENT, Objectdata TEXT NOT NULL)")
                execSQL("CREATE TABLE IF NOT EXISTS PORTFOLIOOPTTBL( EntryId INTEGER PRIMARY KEY AUTOINCREMENT, Objectdata TEXT NOT NULL)")
                execSQL("CREATE TABLE IF NOT EXISTS PORTFOLIOLISTTBL( EntryId INTEGER PRIMARY KEY AUTOINCREMENT, Objectdata TEXT NOT NULL)")
                execSQL("CREATE TABLE IF NOT EXISTS PORTFOLIOTILETBL( EntryId INTEGER PRIMARY KEY AUTOINCREMENT, Objectdata TEXT NOT NULL)")
                execSQL("REPLACE INTO PORTFOLIOTBL(Objectdata) VALUES(?)", arrayOf(serialize(standardColumns.map { it.first })))
                execSQL("REPLACE INTO PORTFOLIOOPTTBL(Objectdata) VALUES(?)", arrayOf(serialize(optionsColumns.map { it.first })))
                execSQL("REPLACE INTO PORTFOLIOLISTTBL(Objectdata) VALUES(?)", arrayOf(serialize(listColumns.map { it.first })))
                execSQL("REPLACE INTO PORTFOLIOTILETBL(Objectdata) VALUES(?)", arrayOf(serialize(tileColumns.map { it.first })))
            }
        }) {
            block(standardColumns.map { it.second }, repo.standardViewColumns)
            block(optionsColumns.map { it.second }, repo.optionsViewColumns)
            block(listColumns.map { it.second }, repo.listViewColumns)
            block(tileColumns.map { it.second }, repo.tileViewColumns)
        }
    }

    private fun serialize(values: List<Int>): ByteArray {
        return ByteArrayOutputStream().use { bytes ->
            ObjectOutputStream(bytes).use {
                it.writeObject(values)
                bytes.toByteArray()
            }
        }
    }

    private class InMemoryPortfolioColumnRepo : PortfolioColumnRepo {

        val standardViewColumns: MutableList<PortfolioColumn> = mutableListOf()
        val optionsViewColumns: MutableList<PortfolioColumn> = mutableListOf()
        val listViewColumns: MutableList<PortfolioColumn> = mutableListOf()
        val tileViewColumns: MutableList<PortfolioColumn> = mutableListOf()

        var saveCalled: Boolean = false
        var savePortfolioColumns: Boolean = true

        override suspend fun getPortfolioColumns(
            positionTableMode: PositionTableMode,
            viewType: PortfolioTableViewType,
            portfolioDisplayMode: PortfolioPositionsDisplayMode
        ) = getColumns(portfolioDisplayMode, viewType)

        override suspend fun savePortfolioColumns(
            positionTableMode: PositionTableMode,
            viewType: PortfolioTableViewType,
            columns: List<PortfolioColumn>,
            portfolioDisplayMode: PortfolioPositionsDisplayMode
        ) {
            saveCalled = true
            if (savePortfolioColumns) {
                getColumns(portfolioDisplayMode, viewType).apply {
                    clear()
                    addAll(columns)
                }
            }
        }

        override fun addOnColumnsDataChangedListener(listener: PortfolioColumnRepo.OnColumnsDataChangedListener) {
            throw UnsupportedOperationException()
        }

        override fun removeOnColumnsDataChangedListener(listener: PortfolioColumnRepo.OnColumnsDataChangedListener) {
            throw UnsupportedOperationException()
        }

        private fun getColumns(
            portfolioDisplayMode: PortfolioPositionsDisplayMode,
            viewType: PortfolioTableViewType
        ) = when (portfolioDisplayMode) {
            PortfolioPositionsDisplayMode.LIST -> listViewColumns
            PortfolioPositionsDisplayMode.TILE -> tileViewColumns
            PortfolioPositionsDisplayMode.TABLE -> when (viewType) {
                PortfolioTableViewType.STANDARD_VIEW -> standardViewColumns
                PortfolioTableViewType.OPTIONS_VIEW -> optionsViewColumns
            }
        }
    }
}
