package com.etrade.mobilepro.watchlist.presentation

import android.content.Context
import android.content.Intent
import com.etrade.mobilepro.editlist.EditListActivity
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.editlist.intent.BaseIntentBuilder
import com.etrade.mobilepro.editlist.intent.ListActionBuilder
import com.etrade.mobilepro.editlist.intent.ListEditDslMarker

/**
 * Lets a user to manage a watchlist items.
 *
 * [Designs](https://etrade.invisionapp.com/d/main#/console/15411687/320304793/preview)
 */
class EditWatchlistActivity : EditListActivity<WatchlistEntryWrapper>() {

    override val viewModelClass: Class<out EditListViewModel<WatchlistEntryWrapper>> = EditWatchlistViewModel::class.java

    override fun onCreate(viewModel: EditListViewModel<WatchlistEntryWrapper>) {
        binding.toolbarTitle.text = getString(R.string.watchlist_manage_title)
        return with(intent) {
            (viewModel as EditWatchlistViewModel).prepareViewModel(
                requireNotNull(
                    getStringExtra(
                        EXTRA_WATCHLIST_ID
                    )
                )
            )
        }
    }

    companion object {

        const val EXTRA_WATCHLIST_ID = "extra.watchlistId"

        /**
         * Prepares an [Intent] to start [EditWatchlistActivity].
         *
         * @param context a context of the application package implementing this class
         * @param init initialization block
         *
         * @return prepared intent
         */
        fun intent(context: Context, init: IntentBuilder.() -> Unit): Intent {
            return IntentBuilder(context).apply(init).intent
        }
    }

    /**
     * Builds an [Intent] to start [EditWatchlistActivity].
     *
     * @param context a context of the application package implementing this class
     */
    @ListEditDslMarker
    class IntentBuilder(context: Context) : BaseIntentBuilder(Intent(context, EditWatchlistActivity::class.java)) {

        init {
            listAction {
                textResId = R.string.watchlist_delete_title
            }
        }

        /**
         * Provide a watchlist's parameters.
         */
        fun watchlist(init: WatchlistBuilder.() -> Unit) = WatchlistBuilder(intent).init()

        /**
         * Provide a list's action parameters.
         */
        fun listAction(init: ListActionBuilder.() -> Unit) = ListActionBuilder(intent).init()
    }

    /**
     * Setup watchlist data to be displayed.
     *
     * @param intent intent that is being prepared
     */
    class WatchlistBuilder(private val intent: Intent) {
        var watchlistId: String
            get() = requireNotNull(intent.getStringExtra(EXTRA_WATCHLIST_ID))
            set(value) {
                intent.putExtra(EXTRA_WATCHLIST_ID, value)
            }
    }
}
