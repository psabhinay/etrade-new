package com.etrade.mobilepro.chart.cache

import com.etrade.mobilepro.chart.util.getShouldCacheTime
import com.etrade.mobilepro.chartengine.ChartRequest
import com.etrade.mobilepro.chartengine.SymbolChartData
import com.etrade.mobilepro.chartengine.TimeFrame
import com.etrade.mobilepro.util.market.time.getCurrentTimeInMillis
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class CachedChartControllerTest {

    private lateinit var sut: CachedChartController

    @Before
    fun setUp() {
        sut = CachedChartController()
    }

    @Test
    fun `verify chart cached result should refresh for all time frames`() {
        testShouldRefresh(sut, ChartRequest("", TimeFrame.Day))
        testShouldRefresh(sut, ChartRequest("", TimeFrame.ThreeDays))
        testShouldRefresh(sut, ChartRequest("", TimeFrame.OneWeek))
        testShouldRefresh(sut, ChartRequest("", TimeFrame.OneMonth))
        testShouldRefresh(sut, ChartRequest("", TimeFrame.ThreeMonths))
        testShouldRefresh(sut, ChartRequest("", TimeFrame.SixMonths))
        testShouldRefresh(sut, ChartRequest("", TimeFrame.OneYear))
        testShouldRefresh(sut, ChartRequest("", TimeFrame.TwoYears))
        testShouldRefresh(sut, ChartRequest("", TimeFrame.ThreeYears))
        testShouldRefresh(sut, ChartRequest("", TimeFrame.ExtendedHour))
    }

    private fun testShouldRefresh(sut: CachedChartController, request: ChartRequest) {
        val shouldCacheTimeInSeconds = getShouldCacheTime(request)
        val shouldCacheTimeInMillis = TimeUnit.SECONDS.toMillis(shouldCacheTimeInSeconds)

        val cachedSymbolChartDataFromPast =
            SymbolChartData("", emptyList(), 0.0, System.currentTimeMillis() - shouldCacheTimeInMillis, shouldCacheTimeInSeconds)
        assertTrue(
            "should refresh when cached at ${cachedSymbolChartDataFromPast.cachedAt} and current time is ${System.currentTimeMillis()}",
            sut.shouldRefresh(cachedSymbolChartDataFromPast)
        )

        val cachedSymbolChartDataFromFuture =
            SymbolChartData("", emptyList(), 0.0, System.currentTimeMillis() + Random.nextLong(), shouldCacheTimeInSeconds)
        assertTrue(
            "should refresh when cached at ${cachedSymbolChartDataFromPast.cachedAt} and current time is ${System.currentTimeMillis()}",
            sut.shouldRefresh(cachedSymbolChartDataFromFuture)
        )
    }

    @Test
    fun `verify chart cached result should not refresh if we cached recently`() {
        val currentTime = getCurrentTimeInMillis()
        val cachedSymbolFromTheFuture = SymbolChartData(
            queryValue = "",
            displayData = emptyList(),
            yesterdaysClose = 0.0,
            cachedAt = currentTime,
            shouldCachedTimeInSeconds = 10
        )

        assertFalse(
            "should not refresh if we just cached",
            sut.shouldRefresh(cachedSymbolFromTheFuture)
        )
    }
}
