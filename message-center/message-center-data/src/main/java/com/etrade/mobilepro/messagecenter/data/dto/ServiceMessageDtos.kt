package com.etrade.mobilepro.messagecenter.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "SecureMsgGetDetailsResponse", strict = false)
class ServiceMessageDetailsResponseDto(
    @field:Element(name = "acctnumber", required = false)
    var accountId: String? = null,
    @field:Element(name = "procdefId", required = false)
    var procDefId: String? = null,
    @field:Element(name = "subject", required = false)
    var subject: String? = null,
    @field:Element(name = "procinstid", required = false)
    var procInstId: String? = null,
    @field:Element(name = "mode", required = false)
    var mode: String? = null,
    @field:ElementList(name = "Matrix", required = false)
    var payload: List<ServiceMessageFieldDto?>? = null,
    @field:ElementList(name = "messages", required = false)
    var textFields: List<String?>? = null,
    @field:Element(name = "Error", required = false)
    var error: ServiceMessageErrorDto? = null
)

@Root(strict = false, name = "item")
class ServiceMessageFieldDto(
    @field:Element(name = "itemName", required = false)
    var title: String? = null,
    @field:Element(name = "itemValue", required = false)
    var value: String? = null
)

@Root(strict = false, name = "Error")
class ServiceMessageErrorDto(
    @field:Element(name = "ErrorType", required = false)
    var type: String? = null,
    @field:Element(name = "ErrorDescription", required = false)
    var desc: String? = null,
    @field:Element(name = "ErrorCode", required = false)
    var code: String? = null
)
