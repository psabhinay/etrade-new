package com.etrade.mobilepro.mod.api

interface MarketOnDemand {

    fun companyOverview(symbol: String): CompanyOverview

    fun getFundReports(symbol: String): List<Report>
}
