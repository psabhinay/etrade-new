package com.etrade.mobilepro.portfolio.action

import com.etrade.mobilepro.R
import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.api.isShort
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import javax.inject.Inject

/**
 * Provides [ActionItem] for different types of instruments.
 */
interface PortfolioRecordActionProvider {

    /**
     * Gets actions for a [PortfolioRecord] and the current state of tax lots.
     *
     * @param portfolioRecord the portfolio record
     * @param optionsPositionsCount the number of option positions this user has
     *
     * @return list of available actions
     */
    fun getActions(portfolioRecord: PortfolioRecord, optionsPositionsCount: Int): List<ActionItem>
}

class PortfolioRecordActionProviderImpl @Inject constructor() : PortfolioRecordActionProvider {

    override fun getActions(
        portfolioRecord: PortfolioRecord,
        optionsPositionsCount: Int
    ): List<ActionItem> {
        return when (portfolioRecord.instrument.instrumentType) {
            InstrumentType.ETF -> createStockActions() // ETF Tax lots is similar to Stocks
            InstrumentType.EQ, InstrumentType.INDX -> createStockActions(portfolioRecord.position.isShort)
            InstrumentType.MF, InstrumentType.MMF -> createMutualFundsActions()
            InstrumentType.OPTN, InstrumentType.OPTNC, InstrumentType.OPTNP -> createOptionAction(
                portfolioRecord.instrument,
                optionsPositionsCount,
                portfolioRecord.position.isShort
            )
            else -> emptyList()
        }
    }

    private fun createMutualFundsActions(): List<ActionItem> = listOfNotNull(
        createDetailsAction(),
        createBuyAction(),
        createSellAction(),
        createTaxLotsAction(),
        ActionItem(
            R.id.action_exchange,
            R.string.portfolio_record_action_exchange,
            R.drawable.ic_tableview_transfer
        )
    )

    @Suppress("LongMethod")
    private fun createOptionAction(
        instrument: Instrument,
        optionsPositionsCount: Int,
        isShort: Boolean = false
    ): List<ActionItem> {
        return listOfNotNull(
            createDetailsAction(),
            ActionItem(
                R.id.action_buy,
                if (isShort) R.string.portfolio_record_action_buy_close else R.string.portfolio_record_action_buy_open,
                R.drawable.ic_circle_add
            ),
            ActionItem(
                R.id.action_sell,
                if (isShort) R.string.portfolio_record_action_sell_open else R.string.portfolio_record_action_sell_close,
                R.drawable.ic_circle_remove
            ),
            ActionItem(
                R.id.action_roll_option,
                R.string.portfolio_record_action_roll_option,
                R.drawable.ic_tableview_roll1,
                listOf(instrument.displaySymbol)
            ),
            if (optionsPositionsCount > 1) {
                ActionItem(
                    R.id.action_roll_two_options,
                    R.string.portfolio_record_action_roll_two_options,
                    R.drawable.ic_tableview_roll2
                )
            } else {
                null
            },
            createTaxLotsAction()
        )
    }

    private fun createStockActions(isShort: Boolean = false): List<ActionItem> {
        return listOfNotNull(
            createDetailsAction(),
            createBuyAction(isShort),
            createSellAction(isShort),
            createTaxLotsAction(),
            ActionItem(R.id.action_set_alert, R.string.portfolio_record_action_set_alert, R.drawable.ic_tableview_alerts)
        )
    }

    private fun createBuyAction(isShort: Boolean = false): ActionItem {
        val displayNameId = if (isShort) {
            R.string.portfolio_record_action_buy_to_cover
        } else {
            R.string.portfolio_record_action_buy
        }
        return ActionItem(R.id.action_buy, displayNameId, R.drawable.ic_circle_add)
    }

    private fun createSellAction(isShort: Boolean = false): ActionItem {
        val displayNameId = if (isShort) {
            R.string.portfolio_record_action_sell_short
        } else {
            R.string.portfolio_record_action_sell
        }
        return ActionItem(R.id.action_sell, displayNameId, R.drawable.ic_circle_remove)
    }

    private fun createDetailsAction(): ActionItem = ActionItem(R.id.action_see_details, R.string.portfolio_record_action_details, R.drawable.ic_tableview_quote)

    private fun createTaxLotsAction(): ActionItem = ActionItem(
        R.id.action_tax_lot_show,
        R.string.portfolio_record_action_tax_lots_show,
        R.drawable.ic_tableview_taxlots
    )
}
