package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.address

import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.StatesOfUS
import com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.ActionState
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.livedata.updateValue

class UsStateDelegate(
    private val repo: BillPayRepo
) {

    internal val loadStatesActionState: MutableLiveData<ActionState<StatesOfUS>> =
        MutableLiveData(ActionState(isLoading = false))

    suspend fun loadStateList() {
        setLoadStatesActionLoading()
        repo.getStateList().fold(
            onSuccess = ::onLoadStatesSuccess,
            onFailure = ::onLoadStatesFail
        )
    }

    private fun setLoadStatesActionLoading() =
        loadStatesActionState.updateValue { it.copy(isLoading = true) }

    private fun onLoadStatesSuccess(usStateList: StatesOfUS) {
        loadStatesActionState.updateValue {
            ActionState(result = ETResult.success(usStateList))
        }
    }

    private fun onLoadStatesFail(cause: Throwable) {
        loadStatesActionState.updateValue {
            it.copy(
                isLoading = false,
                result = ETResult.failure(cause)
            )
        }
    }
}
