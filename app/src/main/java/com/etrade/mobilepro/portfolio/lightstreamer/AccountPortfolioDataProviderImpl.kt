package com.etrade.mobilepro.portfolio.lightstreamer

import com.etrade.eo.streaming.SubscriptionMode
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import io.reactivex.Observable
import javax.inject.Inject

internal const val SYMBOL = "SYMBOL"
internal const val INSTRUMENT_TYPE = "INSTR_TYPE"
internal const val INSTRUMENT_ID = "INSTRUMENT_ID"
internal const val ACCOUNT_NUMBER = "ACCOUNT_NUMBER"
internal const val REFRESH_REQUIRED = "REFRESH_REQUIRED"

class AccountPortfolioDataProviderImpl @Inject constructor(
    private val streamIdFactory: AccountPortfolioStreamIdFactory,
    private val subscriptionManagerHelper: SubscriptionManagerHelper<PortfolioUpdateEvent>
) : AccountPortfolioDataProvider {

    private val fields = setOf(REFRESH_REQUIRED, SYMBOL, INSTRUMENT_TYPE, INSTRUMENT_ID, ACCOUNT_NUMBER)

    override fun getAccountPortfolioEvents(accountId: String): Observable<PortfolioUpdateEvent> {
        return streamIdFactory.getStreamIds(accountId)
            .map { streamId ->
                subscriptionManagerHelper.subscribeTo(streamId, fields, SubscriptionMode.DISTINCT)
            }.reduce { acc, observable ->
                acc.mergeWith(observable)
            }
    }
}
