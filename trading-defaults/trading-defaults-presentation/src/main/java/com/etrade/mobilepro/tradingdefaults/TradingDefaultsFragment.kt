package com.etrade.mobilepro.tradingdefaults

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.tradingdefaults.databinding.FragmentTradingDefaultsBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

class TradingDefaultsFragment @Inject constructor(
    private val viewModelProvider: ViewModelProvider.Factory
) : Fragment(R.layout.fragment_trading_defaults) {

    private val binding by viewBinding(FragmentTradingDefaultsBinding::bind)
    private val viewModel: TradingDefaultsViewModel by navGraphViewModels(R.id.trading_defaults_nav_graph) { viewModelProvider }
    private val tradingDefaultsAdapter = TradingDefaultsAdapter()

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        activity?.apply {
            toolbarActionEndView?.apply {
                setToolbarActionTextAndContentDescription(R.string.apply)
                setOnClickListener {
                    viewModel.saveChanges()
                }
                visibility = View.VISIBLE
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        binding.rvItems.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = tradingDefaultsAdapter
        }

        setupViewModel()
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        viewModel.showValuePicker.value?.peekContent()?.also {
            DefaultDropDownMultiSelector.applySettings(parentFragmentManager, it)
        }
    }

    private fun setupViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is TradingDefaultsViewState.Idle -> {
                        binding.progress.hide()
                        tradingDefaultsAdapter.updateItems(it.items)
                    }
                    is TradingDefaultsViewState.Saving -> {
                        binding.progress.show()
                    }
                    is TradingDefaultsViewState.Saved -> {
                        findNavController().navigateUp()
                    }
                }
            }
        )
        viewModel.showValuePicker.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.also { settings ->
                    DefaultDropDownMultiSelector(parentFragmentManager, settings).open()
                }
            }
        )
    }
}
