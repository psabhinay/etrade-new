package com.etrade.mobilepro.dagger

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.marketscalendar.MarketsCalendarFragment
import com.etrade.mobilepro.marketscalendar.fragment.EarningsCalendarEventDetailsFragment
import com.etrade.mobilepro.marketscalendar.fragment.EarningsMarketsCalendarFragment
import com.etrade.mobilepro.marketscalendar.fragment.EconomicCalendarEventDetailsFragment
import com.etrade.mobilepro.marketscalendar.fragment.EconomicMarketsCalendarFragment
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MarketsCalendarFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(EarningsMarketsCalendarFragment::class)
    internal abstract fun bindEarningsMarketsCalendarFragment(fragment: EarningsMarketsCalendarFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EconomicMarketsCalendarFragment::class)
    internal abstract fun bindEventsMarketsCalendarFragment(fragment: EconomicMarketsCalendarFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EarningsCalendarEventDetailsFragment::class)
    internal abstract fun bindEarningsCalendarEventDetailsFragment(fragment: EarningsCalendarEventDetailsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EconomicCalendarEventDetailsFragment::class)
    internal abstract fun bindEconomicCalendarEventDetailsFragment(fragment: EconomicCalendarEventDetailsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MarketsCalendarFragment::class)
    internal abstract fun bindMarketsCalendarFragment(fragment: MarketsCalendarFragment): Fragment
}
