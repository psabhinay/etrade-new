package com.etrade.mobilepro.dynamicui.presentation

sealed class DynamicUiViewState {
    object Loading : DynamicUiViewState()
    data class Error(val message: String? = "Something went wrong!") : DynamicUiViewState()
    object Success : DynamicUiViewState()
    data class Warning(val message: String) : DynamicUiViewState()
}
