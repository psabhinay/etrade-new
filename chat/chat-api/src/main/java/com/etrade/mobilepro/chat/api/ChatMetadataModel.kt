package com.etrade.mobilepro.chat.api

import java.io.Serializable

data class ChatMetadataModel(
    var affinity: String? = null,
    var buttonId: String? = null,
    var buttonName: String? = null,
    var sessionKey: String? = null
) : Serializable
