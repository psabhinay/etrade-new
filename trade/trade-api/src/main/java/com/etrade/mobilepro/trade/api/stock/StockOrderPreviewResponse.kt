package com.etrade.mobilepro.trade.api.stock

import com.etrade.mobilepro.trade.api.OrderPreviewResponse

interface StockOrderPreviewResponse : OrderPreviewResponse, StockOrderResponse {
    val isOvernightTrading: Boolean
}
