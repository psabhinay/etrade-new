package com.etrade.mobilepro.trade.optionlegselect

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.optionschains.api.BaseOption
import com.etrade.mobilepro.optionschains.api.Option
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuotesParameter
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuotesUseCase
import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.util.invoke
import java.math.BigDecimal
import javax.inject.Inject

interface GetOptionQuotesUseCase : UseCase<GetMobileQuotesParameter, ETResult<List<Option>>>

class GetOptionQuotesUseCaseImpl @Inject constructor(
    private val getMobileQuotesUseCase: GetMobileQuotesUseCase
) : GetOptionQuotesUseCase {

    override suspend fun execute(parameter: GetMobileQuotesParameter): ETResult<List<Option>> {
        return getMobileQuotesUseCase(parameter)
            .map { quotes ->
                quotes.map { quote ->
                    MobileQuoteAdapter(quote)
                }
            }
    }

    private class MobileQuoteAdapter(private val quote: MobileQuote) : BaseOption() {

        override val displaySymbol: String
            get() = quote.description
        override val strikePrice: BigDecimal
            get() = throwNotIntendedForUsageException()
        override val symbol: String
            get() = quote.symbol
        override val isInTheMoney: Boolean
            get() = throwNotIntendedForUsageException()

        override val bid: BigDecimal
            get() = quote.quotePrice.bid.toBigDecimal()
        override val bidSize: String
            get() = quote.quotePrice.bidSize
        override val ask: BigDecimal
            get() = quote.quotePrice.ask.toBigDecimal()
        override val askSize: String
            get() = quote.quotePrice.askSize
        override val volume: String
            get() = quote.quotePrice.volume
        override val openInterest: String
            get() = quote.openInterest
        override val lastPrice: BigDecimal
            get() = quote.quotePrice.lastPrice.toBigDecimal()
        override val change: BigDecimal
            get() = quote.quotePrice.change.toBigDecimal()
        override val isAMOption: Boolean = false

        private fun throwNotIntendedForUsageException(): Nothing {
            throw UnsupportedOperationException("Not intended for usage. Implement if necessary")
        }
    }
}
