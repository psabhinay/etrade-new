package com.etrade.mobilepro.stockplan

private const val DEFAULT_EXERCISE_STOCK_PLAN_URL = "/etx/sp/stockplanMobile/#/mobileExercise"
private const val DEFAULT_SELL_STOCK_PLAN_URL = "/etx/sp/stockplanMobile/#/mobileSell"
private const val DEFAULT_STOCK_ORDERS_URL = "/etx/sp/stockplanMobile/#/mobileViewOrder?ch_id=mobile-app&s_id=etcs&c_id=stockplan-vieworder-page"
private const val STOCK_PLAN_BASE_URL = "/etx/sp/stockplanMobile/?accountIndex="
private const val EXERCISE_URL_TAIL = "#/mobileExercise"
private const val ORDERS_URL_TAIL = "#/mobileViewOrder?ch_id=mobile-app&s_id=etcs&c_id=stockplan-vieworder-page"
private const val SELL_URL_TAIL = "#/mobileSell"
private const val TABLET_BASE_URL = "/etx/sp/stockplan/?tablet&neo.skin=blank&accountIndex="
private const val TABLET_EXERCISE_URL_TAIL = "#/exercise"
private const val TABLET_SELL_URL_TAIL = "#/sell"
private const val DEFAULT_OVERVIEW_STOCK_PLAN_URL = "/etx/sp/stockplanMobile/#/mobileOverview"
private const val OVERVIEW_URL_TAIL = "#/mobileOverview"
private const val TABLET_OVERVIEW_URL_TAIL = "#/overview"

enum class StockPlanWebviewPage(
    val urlTail: String,
    val urlTailTablet: String,
    val defaultPlanUrl: String
) {

    SELL(SELL_URL_TAIL, TABLET_SELL_URL_TAIL, DEFAULT_SELL_STOCK_PLAN_URL),
    EXERCISE(EXERCISE_URL_TAIL, TABLET_EXERCISE_URL_TAIL, DEFAULT_EXERCISE_STOCK_PLAN_URL),
    ORDERS(ORDERS_URL_TAIL, ORDERS_URL_TAIL, DEFAULT_STOCK_ORDERS_URL),
    OVERVIEW(OVERVIEW_URL_TAIL, TABLET_OVERVIEW_URL_TAIL, DEFAULT_OVERVIEW_STOCK_PLAN_URL);

    companion object {
        fun getUrl(
            page: StockPlanWebviewPage,
            accountIndex: String?,
            baseWebUrl: String,
            isTabletView: Boolean
        ): String {
            return getUrlHelper(accountIndex, baseWebUrl, page, isTabletView)
        }

        private fun getUrlHelper(
            accountIndex: String?,
            baseWebUrl: String,
            page: StockPlanWebviewPage,
            isTabletView: Boolean
        ): String {
            return if (isTabletView) {
                baseWebUrl + TABLET_BASE_URL + accountIndex + page.urlTailTablet
            } else {
                baseWebUrl + STOCK_PLAN_BASE_URL + accountIndex + page.urlTail
            }
        }
    }
}
