package com.etrade.mobilepro.order.details.api

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.orders.api.OrigSysCode

data class OrderCancelRequest(
    val accountId: String = "",
    val userId: String = "",
    val platform: String = "",
    val instrumentType: InstrumentType = InstrumentType.UNKNOWN,
    val orderTerm: String = "",
    val origSysCode: OrigSysCode = OrigSysCode.ORIG_SYS_UNSPECIFIED,
    val orderDetailsItem: BaseOrderDetailsItem.OrderDetailsItem
)
