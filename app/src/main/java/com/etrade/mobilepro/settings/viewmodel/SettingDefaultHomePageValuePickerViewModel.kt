package com.etrade.mobilepro.settings.viewmodel

import android.content.res.Resources
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.dropdown.DropDownSelector
import com.etrade.mobilepro.dropdown.expandableselect.DefaultDropDownExpandableSelector
import com.etrade.mobilepro.dropdown.expandableselect.DropDownExpandableSelector
import com.etrade.mobilepro.landing.api.LandingPage
import com.etrade.mobilepro.landing.api.repo.LandingRepo
import com.etrade.mobilepro.landing.usecase.LandingInteractor
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.livedata.waitForFirst
import com.etrade.mobilepro.settings.viewmodel.feature.HasDropDown
import com.etrade.mobilepro.settings.viewmodel.feature.SettingDropDownViewState
import com.etrade.mobilepro.tracking.SCREEN_TITLE_DEFAULT_HOME
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.valuepicker.ValuePickerWidgetItem
import kotlinx.coroutines.launch

class SettingDefaultHomePageValuePickerViewModel(
    override val title: String,
    override val layoutId: Int,
    private val landingRepo: LandingRepo,
    private val landingInteractor: LandingInteractor,
    private val dropDownListScreenTitle: String,
    private val strings: Strings,
    private val tracker: Tracker,
    private val resources: Resources
) : SettingDynamicViewModel(title, null, layoutId), ValuePickerWidgetItem, HasDropDown {
    override var onDropDownSelectedListener: ((title: String, description: String) -> Unit)? = null
    override val value: String?
        get() = (viewState.value ?: viewState.waitForFirst())?.let {
            if (it is ViewState && it.selectedItem != null) {
                getLandingPageTitleForSettingsScreen(it.selectedItem)
            } else {
                it.title
            }
        }
    override val contentDescription: String?
        get() = (viewState.value ?: viewState.waitForFirst())?.let {
            if (it is ViewState && it.selectedItem != null) {
                getLandingPageContentDescriptionForSettingsScreen(it.selectedItem)
            } else {
                it.contentDescription
            }
        }
    override val dropDownTag: String
        get() = "${this.javaClass.canonicalName}_home_page_dropdown"
    private val _viewState = MutableLiveData<ViewState>()
    override val viewState: LiveData<out SettingDropDownViewState>
        get() = _viewState
    private val defaultHomePagesData: LiveData<List<DropDownExpandableSelector.Selectable<LandingPage>>> = Transformations.map(_viewState) { viewState ->
        landingRepo.getLandingState().pages.mapToSelectable(
            level = 0,
            enabledComparator = { candidatePage ->
                landingInteractor.getEnabledState(candidatePage)
            },
            selectedComparator = { candidatePage ->
                candidatePage isSameAs viewState.selectedItem
            }
        ) + DropDownExpandableSelector.Selectable<LandingPage>(
            title = "",
            value = LandingPage.BottomInfoBox(),
            selected = false,
            ghostText = strings.bottomGhostText,
            enabled = false
        )
    }

    init {
        viewModelScope.launch {
            _viewState.value = ViewState(
                selectedItem = landingRepo.getLandingState().selected
            )
        }
    }

    override fun showValuePickerDropDown() {
        _viewState.updateValue(checkIfChanged = false) {
            it.copy(openDropDownSelectorWithTag = ConsumableLiveEvent(content = dropDownTag))
        }

        tracker.screen(SCREEN_TITLE_DEFAULT_HOME)
    }

    override fun createDropDownSelector(fragmentManager: FragmentManager): DropDownSelector =
        DefaultDropDownExpandableSelector(
            supportFragmentManager = fragmentManager,
            settings = DropDownExpandableSelector.Settings(
                tag = dropDownTag,
                title = dropDownListScreenTitle,
                itemsData = defaultHomePagesData,
                hideSingleSubItems = true,
                selectListener = { _, selected ->
                    landingPageSelected(selected.value)
                    onDropDownSelectedListener?.apply {
                        val value = selected.value
                        val title = getLandingPageTitleForSettingsScreen(value)
                        val description = getLandingPageContentDescriptionForSettingsScreen(value)
                        invoke(title, description)
                    }
                }
            )
        )

    private data class ViewState(
        val selectedItem: LandingPage? = null,
        override val openDropDownSelectorWithTag: ConsumableLiveEvent<String>? = null
    ) : SettingDropDownViewState {
        override val title: String?
            get() = selectedItem?.title
        override val contentDescription: String?
            get() = selectedItem?.contentDescription
    }

    private fun landingPageSelected(item: LandingPage) {
        viewModelScope.launch {
            landingRepo.setSelectedPage(item)
        }
        _viewState.updateValue {
            it.copy(selectedItem = item)
        }
    }

    private fun getLandingPageTitleForSettingsScreen(item: LandingPage): String = when (item.id) {
        LandingPage.ALL_ACCOUNTS_ID,
        LandingPage.ALL_BROKERAGE_ID,
        LandingPage.WATCHLIST_ID -> item.title
        LandingPage.PORTFOLIO_ID ->
            if (item.uuid.isEmpty()) {
                strings.portfolioTitle
            } else {
                "${strings.portfolioTitle} - ${item.title}"
            }
        else -> throw IllegalArgumentException("Unknown id = ${item.id} at getLandingPageTitleForSettingsScreen")
    }

    private fun getFormattedAccountDescription(title: String) = resources.accountNameContentDescription(title)

    private fun getLandingPageContentDescriptionForSettingsScreen(item: LandingPage): String = "$title ${when (item.id) {
        LandingPage.ALL_ACCOUNTS_ID,
        LandingPage.ALL_BROKERAGE_ID,
        LandingPage.WATCHLIST_ID -> item.title
        LandingPage.PORTFOLIO_ID ->
            if (item.uuid.isEmpty()) {
                strings.portfolioTitle
            } else {
                "${strings.portfolioTitle} - ${getFormattedAccountDescription(item.title)}"
            }
        else -> item.title
    }}"

    private fun List<LandingPage>.mapToSelectable(
        level: Int,
        enabledComparator: (LandingPage) -> Boolean,
        selectedComparator: (LandingPage) -> Boolean
    ): List<DropDownExpandableSelector.Selectable<LandingPage>> =
        this.map { page ->
            DropDownExpandableSelector.Selectable(
                level = level,
                title = page.title,
                value = page,
                enabled = enabledComparator.invoke(page),
                selected = selectedComparator.invoke(page),
                subItems = page.subItems?.mapToSelectable(level + 1, enabledComparator, selectedComparator),
                contentDescriptionFormatter = { it.contentDescription }
            )
        }

    class Strings(val portfolioTitle: String, val bottomGhostText: String)
}
