package com.etrade.mobilepro.appwidget.settings

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.appwidget.api.settings.WidgetId
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettingsRepo
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.livedata.skip
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class WidgetSettingsViewModel @Inject constructor(
    private val settingsRepo: WidgetSettingsRepo,
    resources: Resources
) : ViewModel() {
    private var settingsSavingJob: Job? = null
    private val _viewState = MutableLiveData<ViewState>()
    internal val refreshRateSelectorItemsData = Transformations.map(_viewState) { viewState ->
        WidgetSettings.RefreshRate.values().map { entry ->
            DropDownMultiSelector.Selectable(
                title = resources.getString(entry.asStringRes()),
                value = entry,
                selected = viewState.settings?.refreshRate == entry
            )
        }
    }
    internal val layoutSelectorItemsData = Transformations.map(_viewState) { viewState ->
        WidgetSettings.Layout.values().map { entry ->
            DropDownMultiSelector.Selectable(
                title = resources.getString(entry.asStringRes()),
                value = entry,
                selected = viewState.settings?.layout == entry
            )
        }
    }
    internal val applySettingsSignal: LiveData<ConsumableLiveEvent<Unit>> = Transformations.map(_viewState.distinctUntilChanged().skip(1)) {
        ConsumableLiveEvent(Unit)
    }
    internal val viewState: LiveData<ViewState> get() = _viewState

    internal fun fetch(widgetId: WidgetId?) {
        if (widgetId == null) { return }
        viewModelScope.launch {
            _viewState.value = ViewState(
                settings = settingsRepo.loadSettings(widgetId),
                widgetId = widgetId
            )
        }
    }

    internal fun updateOpacity(value: Float) {
        _viewState.updateValue {
            it.copy(settings = it.settings?.copy(opacity = value.toInt()))
        }
        saveSettings()
    }

    internal fun updateRefreshRate(value: WidgetSettings.RefreshRate) {
        _viewState.updateValue {
            it.copy(settings = it.settings?.copy(refreshRate = value))
        }
        saveSettings()
    }

    internal fun updateLayout(value: WidgetSettings.Layout) {
        _viewState.updateValue {
            it.copy(settings = it.settings?.copy(layout = value))
        }
        saveSettings()
    }

    private fun saveSettings() {
        settingsSavingJob?.cancel()
        settingsSavingJob = viewModelScope.launch {
            _viewState.value?.let { currentViewState ->
                if (currentViewState.settings != null && currentViewState.widgetId != null) {
                    settingsRepo.saveSettings(currentViewState.widgetId, currentViewState.settings)
                }
            }
        }
    }

    internal data class ViewState(
        val settings: WidgetSettings?,
        val widgetId: WidgetId?
    )
}
