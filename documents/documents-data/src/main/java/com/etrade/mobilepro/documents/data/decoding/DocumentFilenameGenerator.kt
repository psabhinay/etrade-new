package com.etrade.mobilepro.documents.data.decoding

import com.etrade.mobilepro.documents.Document

interface DocumentFilenameGenerator {
    fun generateFilename(document: Document): String
}
