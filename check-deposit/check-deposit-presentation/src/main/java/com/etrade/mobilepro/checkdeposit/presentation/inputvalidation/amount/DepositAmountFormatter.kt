package com.etrade.mobilepro.checkdeposit.presentation.inputvalidation.amount

import com.etrade.mobilepro.inputvalueview.InputFieldManager

internal class DepositAmountFormatter : InputFieldManager.Formatter {
    override fun formatRawContent(rawContent: String): String {
        return "$$rawContent"
    }
}
