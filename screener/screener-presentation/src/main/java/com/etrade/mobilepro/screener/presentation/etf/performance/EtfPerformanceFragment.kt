package com.etrade.mobilepro.screener.presentation.etf.performance

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.etf.EtfPerformanceFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentEtfPerformanceNavReturnBinding
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentEtfPerformanceWithinCategoryBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class EtfPerformanceFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<EtfPerformanceFilter, EtfPerformanceViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {
    private val withinCategory by viewBinding(PartialFragmentEtfPerformanceWithinCategoryBinding::bind)
    private val navReturn by viewBinding(PartialFragmentEtfPerformanceNavReturnBinding::bind)
    override val layoutRes: Int = R.layout.fragment_etf_performance
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.ETF
    override val viewModel by viewModels<EtfPerformanceViewModel> { viewModelFactory }
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<EtfPerformanceFilter, EtfPerformanceViewModel.ViewState, Boolean>>
        get() = mapOf(
            withinCategory.bottomTwentyPercent to WithinCategory.BOTTOM_20,
            withinCategory.belowMidTwentyPercent to WithinCategory.BELOW_MID_20,
            withinCategory.midTwentyPercent to WithinCategory.MID_20,
            withinCategory.aboveMidTwentyPercent to WithinCategory.ABOVE_MID_20,
            withinCategory.topTwentyPercent to WithinCategory.TOP_20,
            withinCategory.oneMonth to WithinCategoryPeriod.ONE_MONTH,
            withinCategory.threeMonths to WithinCategoryPeriod.THREE_MONTHS,
            withinCategory.sixMonths to WithinCategoryPeriod.SIX_MONTHS,
            withinCategory.ytd to WithinCategoryPeriod.YTD,
            withinCategory.oneYear to WithinCategoryPeriod.ONE_YEAR,
            withinCategory.threeYears to WithinCategoryPeriod.THREE_YEARS,
            withinCategory.fiveYears to WithinCategoryPeriod.FIVE_YEARS,
            withinCategory.tenYears to WithinCategoryPeriod.TEN_YEARS,
            navReturn.navReturnBottomTwentyPercent to NavCategory.BOTTOM_20,
            navReturn.navReturnBelowMidTwentyPercent to NavCategory.BELOW_MID_20,
            navReturn.navReturnMidTwentyPercent to NavCategory.MID_20,
            navReturn.navReturnAboveMidTwentyPercent to NavCategory.ABOVE_MID_20,
            navReturn.navReturnTopTwentyPercent to NavCategory.TOP_20,
            navReturn.navReturnOneMonth to NavCategoryPeriod.ONE_MONTH,
            navReturn.navReturnThreeMonths to NavCategoryPeriod.THREE_MONTHS,
            navReturn.navReturnSixMonths to NavCategoryPeriod.SIX_MONTHS,
            navReturn.navReturnYtd to NavCategoryPeriod.YTD,
            navReturn.navReturnOneYear to NavCategoryPeriod.ONE_YEAR,
            navReturn.navReturnThreeYears to NavCategoryPeriod.THREE_YEARS,
            navReturn.navReturnFiveYears to NavCategoryPeriod.FIVE_YEARS,
            navReturn.navReturnTenYears to NavCategoryPeriod.TEN_YEARS
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                updateWithinCategoryBlock(viewState = it)
                updateWithinOverLastBlock(viewState = it)
                updateNavCategoryBlock(viewState = it)
                updateNavOverLastBlock(viewState = it)
            }
        )
    }

    private fun updateWithinCategoryBlock(viewState: EtfPerformanceViewModel.ViewState) = viewState.filter.apply {
        withinCategory.apply {
            bottomTwentyPercent.isChecked = withinCategoryBottom20
            belowMidTwentyPercent.isChecked = withinCategoryBelowMid20
            midTwentyPercent.isChecked = withinCategoryMid20
            aboveMidTwentyPercent.isChecked = withinCategoryAboveMid20
            topTwentyPercent.isChecked = withinCategoryTop20
        }
    }

    private fun updateWithinOverLastBlock(viewState: EtfPerformanceViewModel.ViewState) = viewState.filter.apply {
        withinCategory.apply {
            oneMonth.isChecked = withinCategoryOverLast1Month
            threeMonths.isChecked = withinCategoryOverLast3Months
            sixMonths.isChecked = withinCategoryOverLast6Months
            ytd.isChecked = withinCategoryOverLastYTD
            oneYear.isChecked = withinCategoryOverLast1Year
            threeYears.isChecked = withinCategoryOverLast3Years
            fiveYears.isChecked = withinCategoryOverLast5Years
            tenYears.isChecked = withinCategoryOverLast10Years
        }
    }

    private fun updateNavCategoryBlock(viewState: EtfPerformanceViewModel.ViewState) = viewState.filter.apply {
        navReturn.apply {
            navReturnBottomTwentyPercent.isChecked = navReturnBottom20
            navReturnBelowMidTwentyPercent.isChecked = navReturnBelowMid20
            navReturnMidTwentyPercent.isChecked = navReturnMid20
            navReturnAboveMidTwentyPercent.isChecked = navReturnAboveMid20
            navReturnTopTwentyPercent.isChecked = navReturnTop20
        }
    }

    private fun updateNavOverLastBlock(viewState: EtfPerformanceViewModel.ViewState) = viewState.filter.apply {
        navReturn.apply {
            navReturnOneMonth.isChecked = navReturnOverLast1Month
            navReturnThreeMonths.isChecked = navReturnOverLast3Months
            navReturnSixMonths.isChecked = navReturnOverLast6Months
            navReturnYtd.isChecked = navReturnOverLastYTD
            navReturnOneYear.isChecked = navReturnOverLast1Year
            navReturnThreeYears.isChecked = navReturnOverLast3Years
            navReturnFiveYears.isChecked = navReturnOverLast5Years
            navReturnTenYears.isChecked = navReturnOverLast10Years
        }
    }
}
