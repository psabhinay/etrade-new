package com.etrade.mobilepro.onboarding

import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.localytics.androidx.InAppCampaign

private const val AB_TESTING_LOCALYTICS_ONBOARDING_THRESHOLD_KEY = "show_percentage"
internal fun shouldShowLocalyticsOnboarding(campaign: InAppCampaign, preferences: ApplicationPreferences): Boolean {
    val threshold = campaign.attributes[AB_TESTING_LOCALYTICS_ONBOARDING_THRESHOLD_KEY]?.toIntOrNull() ?: 0
    return preferences.onboardingABValue <= threshold
}
