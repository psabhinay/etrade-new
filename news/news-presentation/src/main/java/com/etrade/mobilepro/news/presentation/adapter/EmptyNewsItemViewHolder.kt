package com.etrade.mobilepro.news.presentation.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

class EmptyNewsItemViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView)
