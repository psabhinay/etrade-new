package com.etrade.mobilepro.bloombergtv.presentation.delegate

import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvActivity
import dev.chrisbanes.insetter.applyInsetter

internal class FullScreenModeArrangerDelegate : BloombergTvActivityDelegate {
    override fun init(activity: BloombergTvActivity) {
        WindowInsetsControllerCompat(activity.window, activity.binding.root).systemBarsBehavior =
            WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        ViewCompat.setOnApplyWindowInsetsListener(activity.binding.root) { v, insets ->
            v.applyInsetter {
                type(statusBars = true, navigationBars = true) {
                    padding()
                }
            }
            insets
        }
        WindowCompat.setDecorFitsSystemWindows(activity.window, false)
    }
}
