package com.etrade.mobilepro.dagger

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.provider.Settings
import android.webkit.CookieManager
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import androidx.navigation.NavDirections
import androidx.preference.PreferenceManager
import com.etrade.eo.app.preferences.SharedPreferencesStorage
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.eo.webviewcookiejar.SharedCookieManager
import com.etrade.mobile.power.json.JsonFactory.createGsonUtil
import com.etrade.mobile.power.json.JsonUtil
import com.etrade.mobilepro.ETApplication
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.account.dao.AccountDao
import com.etrade.mobilepro.accounts.AccountOrderItem
import com.etrade.mobilepro.accounts.util.android.view.AccountViewType
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.androidconfig.api.repo.AndroidConfigRepo
import com.etrade.mobilepro.androidconfig.data.repo.DefaultAndroidConfigRepo
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.application.ApplicationLifecycle
import com.etrade.mobilepro.applicationinfo.DefaultApplicationInfo
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.baseactivity.ActivityNetworkConnectionDelegate
import com.etrade.mobilepro.baseactivity.ActivitySessionDelegate
import com.etrade.mobilepro.baseactivity.DefaultActivityDelegate
import com.etrade.mobilepro.cache.CacheClearHandler
import com.etrade.mobilepro.common.DISCLOSURES
import com.etrade.mobilepro.common.DisclosureOverlayHandler
import com.etrade.mobilepro.common.OverlayFragmentManager
import com.etrade.mobilepro.common.OverlayFragmentManagerHolder
import com.etrade.mobilepro.common.RestorableViewModelDelegate
import com.etrade.mobilepro.common.SearchViewHandler
import com.etrade.mobilepro.common.di.AddExternalAccount
import com.etrade.mobilepro.common.di.Api
import com.etrade.mobilepro.common.di.AppsFlyer
import com.etrade.mobilepro.common.di.CheckDepositServiceAgreement
import com.etrade.mobilepro.common.di.CurrentEnvironmentName
import com.etrade.mobilepro.common.di.DefaultKeyStore
import com.etrade.mobilepro.common.di.DeviceId
import com.etrade.mobilepro.common.di.DeviceInfo
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.di.EDocWeb
import com.etrade.mobilepro.common.di.EntryPointActivity
import com.etrade.mobilepro.common.di.EnvironmentName
import com.etrade.mobilepro.common.di.Express
import com.etrade.mobilepro.common.di.IsTablet
import com.etrade.mobilepro.common.di.MobileEtrade
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.di.Mod
import com.etrade.mobilepro.common.di.OpenNewAccount
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.getOverlayIntentForUrlAction
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.common.viewmodel.SnackbarViewModel
import com.etrade.mobilepro.completeview.edit.CompleteViewRepo
import com.etrade.mobilepro.completeview.edit.DefaultCompleteViewWidgetRepo
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dao.DefaultRealmAccountListRepo
import com.etrade.mobilepro.dao.PositionsDaoProvider
import com.etrade.mobilepro.dialog.viewmodel.DialogViewModel
import com.etrade.mobilepro.hidecontent.HideContentOverlayDelegate
import com.etrade.mobilepro.hidecontent.HideContentOverlayDelegateImpl
import com.etrade.mobilepro.home.AccountsSelectedTabListener
import com.etrade.mobilepro.home.DefaultAccountsSelectedTabListener
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.home.createAccountListByAccountId
import com.etrade.mobilepro.home.model.AlertsCountViewModel
import com.etrade.mobilepro.home.overview.edit.Overview
import com.etrade.mobilepro.home.overview.edit.OverviewWidgetRepo
import com.etrade.mobilepro.home.overview.edit.OverviewWidgetRepoImpl
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.imageloader.glide.GlideImageLoader
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.LocalyticsInboxMessagesRepository
import com.etrade.mobilepro.login.data.CacheClearHandlerImpl
import com.etrade.mobilepro.markets.edit.DefaultMarketsOverviewWidgetRepo
import com.etrade.mobilepro.markets.edit.MarketsOverview
import com.etrade.mobilepro.msaccounts.api.MsPreferencesRepo
import com.etrade.mobilepro.navigation.MainNavigationImpl
import com.etrade.mobilepro.navigation.portfolioTabDirections
import com.etrade.mobilepro.onboarding.OnboardingAction
import com.etrade.mobilepro.onboarding.OnboardingActionImpl
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.preferences.KeyValStoreApplicationPreferences
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.quotelookup.QuoteLookupActivity
import com.etrade.mobilepro.realm.DefaultRealmDaoProvider
import com.etrade.mobilepro.realm.RealmManager
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.session.api.UserConsumerKeyInterface
import com.etrade.mobilepro.streaming.market.status.DefaultMarketTradeStatusDelegate
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.tier.CustomerTierRepo
import com.etrade.mobilepro.tier.CustomerTierRepoImpl
import com.etrade.mobilepro.tier.CustomerTierService
import com.etrade.mobilepro.timeout.state.TimeoutState
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelper
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelperImpl
import com.etrade.mobilepro.tracking.trackers.LocalyticsTracker
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.user.session.manager.SessionState
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.usersession.api.LoginSignalHandler
import com.etrade.mobilepro.usersession.api.UserSessionStateProvider
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.userviewmodel.UserViewModelRouter
import com.etrade.mobilepro.util.AuthenticationStatusProvider
import com.etrade.mobilepro.util.FireBaseNonFatalEventLogger
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.DeviceRootChecker
import com.etrade.mobilepro.util.android.coroutine.CoroutineContextProvider
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import com.etrade.mobilepro.util.android.recyclerviewutil.DefaultSelectionTracker
import com.etrade.mobilepro.util.android.recyclerviewutil.SelectionTracker
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.debuglog.RemoteDebugLogger
import com.scottyab.rootbeer.RootBeer
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import org.chromium.customtabsclient.shared.WebViewHelper
import retrofit2.Retrofit
import java.net.CookieHandler
import javax.inject.Singleton

private const val PREF_KEY = "etmp.preferences"

@Module
class ApplicationModule(private val application: ETApplication) {
    @Singleton
    @Provides
    internal fun provideContext(): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    @EntryPointActivity
    internal fun provideMainActivityClass(): Class<out Activity> = MainActivity::class.java

    @Provides
    @Singleton
    internal fun provideHideContentOverlayDelegate(
        hideContentOverlayDelegateImpl: HideContentOverlayDelegateImpl
    ): HideContentOverlayDelegate {
        return hideContentOverlayDelegateImpl
    }

    @Singleton
    @Provides
    internal fun providesETApplication(): ETApplication {
        return application
    }

    @Singleton
    @Provides
    internal fun providesResources(context: Context) = context.resources

    @Singleton
    @Provides
    internal fun provideConnectionNotifier(context: Context) = ConnectionNotifier(context)

    @Singleton
    @Provides
    internal fun provideSnackbarUtilFactory(connectionNotifier: ConnectionNotifier, context: Context) = SnackbarUtilFactory(connectionNotifier, context)

    @Singleton
    @Provides
    @Web
    fun providesWebBaseUrl(appConfigRepo: AppConfigRepo): String = appConfigRepo.getAppConfig().baseUrls.web

    @Singleton
    @Provides
    @EDocWeb
    fun providesEDocWebBaseUrl(appConfigRepo: AppConfigRepo): String = appConfigRepo.getAppConfig().baseUrls.eDocWeb

    @Singleton
    @Provides
    @Express
    fun providesExpressBaseUrl(appConfigRepo: AppConfigRepo): String = appConfigRepo.getAppConfig().baseUrls.express

    @Singleton
    @Provides
    @MobileTrade
    fun providesMobileBaseUrl(appConfigRepo: AppConfigRepo): String = appConfigRepo.getAppConfig().baseUrls.mobileTrade

    @Singleton
    @Provides
    @Mod
    fun provideMarketOnDemandBaseUrl(appConfigRepo: AppConfigRepo): String = appConfigRepo.getAppConfig().baseUrls.marketOnDemand

    @Singleton
    @Provides
    @Api
    fun provideApiBaseUrl(appConfigRepo: AppConfigRepo): String = appConfigRepo.getAppConfig().baseUrls.api

    @Singleton
    @Provides
    @MobileEtrade
    fun provideMobileEtradeBaseUrl(appConfigRepo: AppConfigRepo): String = appConfigRepo.getAppConfig().baseUrls.mobileETrade

    @Singleton
    @Provides
    @EnvironmentName
    fun provideEnvironmentName(resources: Resources): String = resources.getString(R.string.environmentName)

    @Singleton
    @Provides
    @AppsFlyer
    fun provideAppsFlyerBaseUrl(resources: Resources): String = resources.getString(R.string.apps_flyer_request_url)

    @Singleton
    @Provides
    @CurrentEnvironmentName
    fun provideCurrentEnvironmentName(appConfigRepo: AppConfigRepo): String = appConfigRepo.getAppConfig().environment.asString

    @Singleton
    @Provides
    internal fun provideCookieManager(): SharedCookieManager {
        val sharedCookieManager = SharedCookieManager(CookieManager.getInstance())
        CookieHandler.setDefault(sharedCookieManager)
        return sharedCookieManager
    }

    @Provides
    @Singleton
    @DefaultKeyStore
    internal fun providesDefaultKeyValueStorage(context: Context): KeyValueStorage {
        return SharedPreferencesStorage(PreferenceManager.getDefaultSharedPreferences(context))
    }

    @Provides
    @Singleton
    internal fun providesKeyValueStorage(context: Context): KeyValueStorage {
        return SharedPreferencesStorage(context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE))
    }

    /**
     * @SuppressLint("HardwareIds") is used to keep compatibility with endpoint e/t/mobile/UpdateAndroidRegId which seems
     * to accept the device id format provided by Settings.Secure.ANDROID_ID. Back-end team mentioned that the endpoint does not
     * have any length or format limitation at application layer level but that other sub-system maybe does.
     */
    @SuppressLint("HardwareIds")
    @Provides
    @Singleton
    internal fun provideApplicationPreferences(
        keyValueStorage: KeyValueStorage,
        context: Context
    ): ApplicationPreferences {
        return KeyValStoreApplicationPreferences(
            keyValueStorage = keyValueStorage,
            androidIdProvider = { Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID) }
        )
    }

    @Provides
    @Singleton
    internal fun provideUserPreferences(keyValueStorage: KeyValueStorage): UserPreferences {
        return UserPreferences(keyValueStorage)
    }

    @Provides
    @Singleton
    internal fun providesJsonUtil(): JsonUtil = createGsonUtil()

    @Provides
    @Singleton
    internal fun providesRealmDaoProvider(): CachedDaoProvider =
        DefaultRealmDaoProvider(RealmManager)

    @Provides
    internal fun providesAccountDaoProvider(daoProvider: CachedDaoProvider): AccountDaoProvider =
        daoProvider

    @Provides
    internal fun providesPositionsDaoProvider(daoProvider: CachedDaoProvider): PositionsDaoProvider =
        daoProvider

    @Provides
    @Singleton
    internal fun provideCustomDebugLogger(): RemoteDebugLogger = FireBaseNonFatalEventLogger()

    @Provides
    @Singleton
    internal fun providesCustomTabHelper(context: Context): WebViewHelper {
        return WebViewHelper(context)
    }

    @Provides
    @Singleton
    internal fun providesAlertsCountViewModel(
        alertsRepo: AlertsRepo,
        sessionState: SessionState
    ): AlertsCountViewModel =
        AlertsCountViewModel(alertsRepo, sessionState)

    @Provides
    @Singleton
    @Suppress("LongParameterList")
    internal fun providesUserViewModel(
        accountListRepo: AccountListRepo,
        resources: Resources,
        user: User,
        userViewModelRouter: UserViewModelRouter,
        timeoutState: TimeoutState,
        restorableViewModelDelegate: RestorableViewModelDelegate
    ): UserViewModel =
        UserViewModel(accountListRepo, resources, user, userViewModelRouter, timeoutState, restorableViewModelDelegate)

    @Provides
    internal fun provideUserSessionState(userViewModel: UserViewModel): UserSessionStateProvider =
        object : UserSessionStateProvider {
            override val userSessionState: LiveData<ConsumableLiveEvent<SessionStateChange>>
                get() = userViewModel.userSessionState

            override fun observeLoginState(
                viewLifecycleOwner: LifecycleOwner,
                loginSignalHandler: LoginSignalHandler?,
                block: () -> Unit
            ) {
                userViewModel.observeLoginState(viewLifecycleOwner, loginSignalHandler, block)
            }
        }

    @Provides
    @Singleton
    fun provideAuthenticationStatusProvider(userViewModel: UserViewModel) = object : AuthenticationStatusProvider {
        override fun isAuthenticated(): Boolean = userViewModel.isAuthenticated
    }

    @Provides
    @Singleton
    internal fun providesSnackBarViewModel(): SnackbarViewModel = SnackbarViewModel()

    @Provides
    @Singleton
    internal fun providesDialogViewModel(resources: Resources): DialogViewModel = DialogViewModel(resources)

    @Provides
    internal fun providesApplicationInfo(
        context: Context,
        applicationPreference: ApplicationPreferences,
        connectionNotifier: ConnectionNotifier
    ): ApplicationInfo {
        return DefaultApplicationInfo(context, applicationPreference, connectionNotifier)
    }

    @Provides
    fun provideAccountListProvider(): AccountListRepo = DefaultRealmAccountListRepo()

    @Provides
    @Singleton
    internal fun providesCoroutineContextProvider(): DispatcherProvider = CoroutineContextProvider()

    @Singleton
    @Provides
    internal fun provideMainNavigation(userViewModel: UserViewModel): MainNavigation = MainNavigationImpl(userViewModel)

    @Singleton
    @Provides
    internal fun provideSearchViewInterface(): SearchViewHandler = object :
        SearchViewHandler {

        override fun toggleSearchView(shouldBeAttached: Boolean, activity: FragmentActivity?) {
            (activity as QuoteLookupActivity).toggleSearchView(shouldBeAttached)
        }

        override fun toggleLookupViewPager(shouldShowPager: Boolean, activity: FragmentActivity?) {
            (activity as QuoteLookupActivity).toggleViewPager(shouldShowPager)
        }
    }

    @Singleton
    @Provides
    internal fun provideDisclosureOverlayInterface(): DisclosureOverlayHandler = object :
        DisclosureOverlayHandler {

        override fun getDisclosureOverlayIntent(context: FragmentActivity?): Intent? {
            return context?.let { getOverlayIntentForUrlAction(DISCLOSURES, context.getString(R.string.disclosures), it) }
        }
    }

    @Provides
    internal fun provideSelectionTracker(): SelectionTracker = DefaultSelectionTracker()

    @Provides
    @OpenNewAccount
    internal fun provideOpenNewAccountUrl(@Web baseUrl: String, resources: Resources): String = baseUrl.plus(resources.getString(R.string.url_open_new_account))

    @Provides
    @DeviceId
    internal fun provideDeviceId(applicationInfo: ApplicationInfo): String = applicationInfo.deviceId()

    @Provides
    @DeviceType
    internal fun provideDeviceType(applicationInfo: ApplicationInfo): String = applicationInfo.deviceType

    @Singleton
    @Provides
    @Overview
    internal fun provideOverviewWidgetRepo(storage: KeyValueStorage, resources: Resources): OverviewWidgetRepo = OverviewWidgetRepoImpl(storage, resources)

    @Singleton
    @Provides
    @MarketsOverview
    internal fun provideMarketOverviewWidgetRepo(storage: KeyValueStorage, resources: Resources, user: User): OverviewWidgetRepo =
        DefaultMarketsOverviewWidgetRepo(storage, resources, user)

    @Reusable
    @Provides
    @CompleteViewRepo
    internal fun provideCompleteViewWidgetRepo(
        storage: KeyValueStorage,
        resources: Resources,
        msPreferences: MsPreferencesRepo,
        accountListRepo: AccountListRepo,
    ): OverviewWidgetRepo =
        DefaultCompleteViewWidgetRepo(accountListRepo, resources, msPreferences, storage)

    @Provides
    @CheckDepositServiceAgreement
    internal fun provideCheckDepositServiceAgreement(@MobileEtrade baseUrl: String, resources: Resources): String =
        baseUrl.plus(resources.getString(R.string.url_check_deposit_service_agreement))

    @Provides
    @Singleton
    internal fun provideImageLoader(context: Context, client: OkHttpClient, userConsumerKey: UserConsumerKeyInterface): ImageLoader =
        GlideImageLoader(context, client, userConsumerKey)

    @Provides
    @Singleton
    internal fun provideTradingDefaultsPreferences(resources: Resources, keyValueStorage: KeyValueStorage): TradingDefaultsPreferences {
        return TradingDefaultsPreferences(resources, keyValueStorage)
    }

    @Provides
    @Singleton
    internal fun provideActivityDelegate(
        userSessionHandler: UserSessionHandler,
        applicationLifecycle: ApplicationLifecycle,
        connectionNotifier: ConnectionNotifier
    ): ActivitySessionDelegate {
        return getDefaultActivityDelegate(userSessionHandler, applicationLifecycle, connectionNotifier)
    }

    @Provides
    @Singleton
    internal fun provideActivityNetworkConnectionDelegate(
        userSessionHandler: UserSessionHandler,
        applicationLifecycle: ApplicationLifecycle,
        connectionNotifier: ConnectionNotifier
    ): ActivityNetworkConnectionDelegate {
        return getDefaultActivityDelegate(userSessionHandler, applicationLifecycle, connectionNotifier)
    }

    private fun getDefaultActivityDelegate(
        userSessionHandler: UserSessionHandler,
        applicationLifecycle: ApplicationLifecycle,
        connectionNotifier: ConnectionNotifier
    ) = DefaultActivityDelegate(userSessionHandler, applicationLifecycle, connectionNotifier)

    @Provides
    @IsTablet
    fun provideIsTablet(applicationInfo: ApplicationInfo): Boolean = applicationInfo.isTablet

    @Provides
    @DeviceInfo
    fun provideDeviceInfo(applicationInfo: ApplicationInfo): String =
        "Version: ${applicationInfo.appVersionName()} OS Version: ${applicationInfo.operationSystemVersion()} " +
            "Device: ${applicationInfo.deviceDetails()}"

    @Provides
    fun provideOnboardingNavigation(): OnboardingAction = OnboardingActionImpl()

    @Provides
    fun provideEtTracker(): Tracker = LocalyticsTracker(application)

    @Provides
    fun provideCustomerTierRepo(customerTierService: CustomerTierService): CustomerTierRepo = CustomerTierRepoImpl(customerTierService)

    @Provides
    fun provideAccountsOrganizeItems(dao: AccountDao): LiveData<List<AccountOrderItem>> = dao.getAllAccounts().map(::mapAccountListToOrderAccountList)

    @Singleton
    @Provides
    internal fun provideAccountsSelectedTabListener(): AccountsSelectedTabListener = DefaultAccountsSelectedTabListener()

    @Provides
    internal fun provideViewPagerTabsTrackerHelper(tracker: Tracker): ViewPagerTabsTrackerHelper = ViewPagerTabsTrackerHelperImpl(tracker)

    @Provides
    internal fun provideInboxMessagesRepository(): InboxMessagesRepository =
        LocalyticsInboxMessagesRepository()

    @Provides
    @AddExternalAccount
    internal fun provideAddExternalAccountUrl(@Web baseUrl: String, resources: Resources): String =
        baseUrl.plus(resources.getString(R.string.url_add_external_account))

    @Provides
    @Singleton
    internal fun provideDeviceRootChecker(): DeviceRootChecker = object : DeviceRootChecker {
        private val rootBeer = RootBeer(application)

        override fun isRooted(): Boolean =
            rootBeer.detectTestKeys() || rootBeer.checkForSuBinary() || rootBeer.checkSuExists() || rootBeer.checkForRWPaths()
    }

    @Provides
    @Singleton
    internal fun provideMarketStatusHandler(appConfigRepo: AppConfigRepo): MarketTradeStatusDelegate =
        DefaultMarketTradeStatusDelegate(appConfigRepo.getAppConfig().isCircuitBreakEnabled)

    @Provides
    @Singleton
    internal fun provideOverlayFragmentManagerHolder(): OverlayFragmentManagerHolder = object : OverlayFragmentManagerHolder {
        override var overlayManager: OverlayFragmentManager? = null
    }

    @Provides
    @Singleton
    internal fun provideCacheClearHandler(context: Context): CacheClearHandler =
        CacheClearHandlerImpl(context)

    @Provides
    @Singleton
    internal fun provideAndroidConfigRepo(@MobileEtrade retrofit: Retrofit): AndroidConfigRepo =
        DefaultAndroidConfigRepo(retrofit)

    @Provides
    @Singleton
    internal fun provideUserViewModelRouter(accountsSelectedTabListener: AccountsSelectedTabListener): UserViewModelRouter =
        object : UserViewModelRouter {
            override fun provideMarketDataAgreementDirection(title: String, message: String): NavDirections =
                MainGraphDirections.actionToMarketDataAgreement(title, message)

            override fun providePortfolioTabDirection(
                accountId: String,
                accountUuid: String
            ): NavDirections = portfolioTabDirections(accountId, accountUuid)

            override fun provideActionToSelectedAccountNavDirections(
                account: Account?,
                selectedAccountViewType: AccountViewType,
                currentAccountViewType: AccountViewType
            ) = MainGraphDirections.actionToSelectedAccountTabIndexOrTitle(
                tabTitle = getAccountTabTitle(selectedAccountViewType, currentAccountViewType),
                accountId = account?.accountId
            )

            private fun getAccountTabTitle(
                selectedAccountViewType: AccountViewType,
                currentAccountViewType: AccountViewType,
            ): String? {
                val tabListOld = createAccountListByAccountId(currentAccountViewType) ?: return null
                val tabListNew = createAccountListByAccountId(selectedAccountViewType) ?: return null
                val tabOldTitle = tabListOld[accountsSelectedTabListener.selectedTabIndex].title
                return tabListNew.find { it.title == tabOldTitle }?.title
            }
        }
}

private fun mapAccountListToOrderAccountList(accounts: List<Account>): List<AccountOrderItem> =
    accounts.map { AccountOrderItem(it.accountShortName, it.accountUuid) }
