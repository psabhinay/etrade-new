package com.etrade.mobilepro.dropdown.expandableselect.adapter

import com.etrade.mobilepro.dropdown.expandableselect.DropDownExpandableSelector
import com.etrade.mobilepro.dropdown.expandableselect.ExpandableItemState
import com.etrade.mobilepro.dropdown.expandableselect.addNewSelected
import com.etrade.mobilepro.dropdown.expandableselect.clearAllSelected
import com.etrade.mobilepro.dropdown.expandableselect.findInTree
import com.etrade.mobilepro.dropdown.expandableselect.findPathToItem
import com.etrade.mobilepro.util.guard
import java.util.Stack

internal interface UpdatesConsumer<T> {
    fun dispatchUpdates(renderedItems: List<DropDownExpandableSelector.Selectable<T>>)
}

/**
 * This class is responsible for translating tree structure of items into displayed list structure of them
 * It stores base items list and provides "rendered" version of them depending of user actions
 */
internal class ExpandablesHandler<T>(
    lastExpandedItem: ExpandableItemState?,
    private var baseItems: List<DropDownExpandableSelector.Selectable<T>>,
    private val consumer: UpdatesConsumer<T>,
    private val updateListener: ((index: Int, length: Int, newIndex: Int) -> Unit)
) {
    init {
        consumer.dispatchUpdates(expandToPath(path = getInitialPath(lastExpandedItem)))
    }

    fun applyNewSelected(newItems: List<DropDownExpandableSelector.Selectable<T>>) {
        val selected = newItems.findInTree(Stack()) { it.selected } guard {
            return
        }
        if (selected.state != DropDownExpandableSelector.Selectable.State.NONE) {
            baseItems = baseItems.clearAllSelected().addNewSelected { it.value == selected.value } guard { return }
            consumer.dispatchUpdates(expandToPath(path = getInitialPath()))
        }
    }

    fun itemWasClicked(index: Int, item: DropDownExpandableSelector.Selectable<T>) {
        val renderedItems = expandToPath(path = getPathToItem(item))
        val newClickedItemIndex = renderedItems.indexOfFirst { item.hashCodeWithoutExpandedState == it.hashCodeWithoutExpandedState }
        consumer.dispatchUpdates(renderedItems)
        val sign = if (item.isExpanded) { -1 } else { 1 }
        updateListener.invoke(index, (item.subItems?.size ?: 0) * sign, newClickedItemIndex)
    }

    private fun expandToPath(path: Stack<DropDownExpandableSelector.Selectable<T>>): List<DropDownExpandableSelector.Selectable<T>> {
        if (path.empty()) {
            return baseItems
        }
        val result = mutableListOf<DropDownExpandableSelector.Selectable<T>>()
        result.addAll(baseItems)
        path.reversed().forEach { itemToExpand ->
            val index = result.indexOf(itemToExpand)
            if (index >= 0 && itemToExpand.subItems != null) {
                result[index] = result[index].copy(isExpanded = true)
                result.addAll(index + 1, itemToExpand.subItems)
            }
        }
        return result
    }

    private fun getInitialPath(lastExpandedItem: ExpandableItemState? = null): Stack<DropDownExpandableSelector.Selectable<T>> {
        val path = Stack<DropDownExpandableSelector.Selectable<T>>()
        if (lastExpandedItem == null) {
            baseItems.findInTree(path) { it.selected }
        } else {
            val lastItem = baseItems.findInTree(path) { it.hashCodeWithoutExpandedState == lastExpandedItem.hashCodeValue }
            if (!lastExpandedItem.isExpanded && lastItem != null) {
                path.remove(lastItem)
            }
        }
        return path
    }

    private fun getPathToItem(item: DropDownExpandableSelector.Selectable<T>): Stack<DropDownExpandableSelector.Selectable<T>> = baseItems.findPathToItem(item)
}
