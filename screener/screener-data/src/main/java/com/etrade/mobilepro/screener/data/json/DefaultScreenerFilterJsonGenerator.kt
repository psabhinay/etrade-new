package com.etrade.mobilepro.screener.data.json

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_LIKE
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerFilterJsonGenerator
import com.etrade.mobilepro.screener.api.ScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.SortArgument
import com.etrade.mobilepro.screener.api.result.RequestToResultParameterMapper
import com.squareup.moshi.Moshi
import javax.inject.Inject
import javax.inject.Provider

private const val FIRST_ROW = 0 // default value for each results number request
private const val ROW_COUNT = 100 // default value for each results number request

private val mutualFundDefaultArguments = listOf(
    Argument(
        clauses = listOf(
            Clause(
                operator = OPERATOR_LIKE,
                values = listOf("%")
            )
        ),
        clausesGroups = emptyList(),
        field = "SuperCategory"
    ),
    Argument(
        clauses = listOf(
            Clause(
                operator = OPERATOR_LIKE,
                values = listOf("%")
            )
        ),
        field = "FundCategory"
    )
)

class DefaultScreenerFilterJsonGenerator @Inject constructor(
    private val moshi: Moshi,
    private val filterGenerators: Map<Class<*>, @JvmSuppressWildcards Provider<FilterModelGenerator>>,
    private val requestToResultParameterMapper: RequestToResultParameterMapper
) : ScreenerFilterJsonGenerator {

    private fun getResultFields(group: ScreenerType.Group): MutableSet<String> = when (group) { // default value for each results number request
        ScreenerType.Group.STOCK -> mutableSetOf("Symbol")
        ScreenerType.Group.MUTUAL_FUND -> mutableSetOf(
            "Symbol", "FundName", "MorningstarRatingSinceIncep",
            "FundCategory", "MorningstarAsOfDate", "MorningstarRatingOutOfFunds_3Year",
            "TotalAssets", "ExpenseRatio", "Alpha", "Beta", "SharpeRatioResultView", "RSquared"
        )
        ScreenerType.Group.ETF -> mutableSetOf("Symbol")
    }

    override fun generateFiltersJson(
        filtersCollection: Collection<ScreenerFilter>,
        group: ScreenerType.Group
    ): String {
        val partialScreenerParamsCollection = mutableListOf<PartialScreenerParams>()

        for (filter in filtersCollection) {
            if (filterGenerators[filter.javaClass]?.get() != null) {
                val filterJsonGenerator = filterGenerators[filter.javaClass]?.get() as FilterModelGenerator
                partialScreenerParamsCollection.add(filterJsonGenerator.toModel(filter))
            } else {
                continue
            }
        }

        return generateJson(partialScreenerParamsCollection, group, filtersCollection)
    }

    private fun generateJson(
        partialScreenerParamsCollection: Collection<PartialScreenerParams>,
        group: ScreenerType.Group,
        filtersCollection: Collection<ScreenerFilter>
    ): String {
        val resultFields = getResultFields(group)
        val requestedFields = partialScreenerParamsCollection.flatMap { it.arguments }.map { it.field }
        val extraFields = filtersCollection.filter { it.isNotEmpty() }.flatMap { it.customResultFields() }
        val gatheredFields = requestedFields + extraFields
        val mappedResultFields = requestToResultParameterMapper.map(group, gatheredFields)
        val sortArgument = SortArgument("Symbol", "0")
        resultFields.addAll(mappedResultFields)

        val arguments = partialScreenerParamsCollection.flatMap { it.arguments }
        val argumentsWithDefaults = appendDefaultArguments(arguments, group)

        val screenersParams = ScreenerParams(
            argumentsWithDefaults,
            FIRST_ROW,
            ROW_COUNT,
            resultFields.toList(),
            listOf(sortArgument)
        )

        val jsonAdapter = moshi.adapter(ScreenerParams::class.java)

        return jsonAdapter.toJson(screenersParams)
    }

    private fun appendDefaultArguments(arguments: List<Argument>, group: ScreenerType.Group): List<Argument> =
        if (arguments.isNotEmpty()) {
            arguments
        } else {
            when (group) {
                ScreenerType.Group.STOCK -> arguments
                ScreenerType.Group.MUTUAL_FUND -> mutualFundDefaultArguments
                ScreenerType.Group.ETF -> arguments
            }
        }
}
