package com.etrade.mobilepro.tableviewutils.presentation.adapter

interface HasTableViewAdapter {
    fun setTableViewAdapter(tableViewAdapter: TableViewAdapter)
}
