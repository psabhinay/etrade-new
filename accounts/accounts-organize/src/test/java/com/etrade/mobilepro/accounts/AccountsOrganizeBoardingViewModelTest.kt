package com.etrade.mobilepro.accounts

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class AccountsOrganizeBoardingViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    private val mockApplicationPreferences: ApplicationPreferences = mock {
        on { homeLandingCount } doReturn COUNT_TO_SHOW_ACCOUNT_ORGANIZATION
        on { shouldDisplayOnboarding } doReturn true
        on { shouldShowAccountsOrganizeBoarding } doReturn true
    }

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `viewModel order items upon creation`() {
        // Given
        val mockObserver = mock<Observer<List<AccountOrderItem>>>()
        val items = createMockAccounts()
        val sut = buildAccountsOrganizeBoardingViewModel(accounts = items)
        sut.listItems.observeForever(mockObserver)

        // When
        sut.fetchItems()

        // Then
        verify(mockObserver).onChanged(eq(items.toOrderAccountList()))
    }

    @Test
    fun `viewModel view state when order of items is changed`() {
        // Given
        val mockObserver = mock<Observer<ConsumableLiveEvent<Unit>>>()
        val items = createMockAccounts(size = 5)
        val sut = buildAccountsOrganizeBoardingViewModel(accounts = items)
        sut.accountsOrderingChanged.observeForever(mockObserver)

        // When
        sut.fetchItems()
        sut.moveItem(0, 3)
        sut.applyChanges()

        // Then
        verify(mockObserver).onChanged(eq(ConsumableLiveEvent(Unit)))
    }

    @Test
    fun `viewModel AccountOrganizeOnboardState change based on authentication state`() {

        // Given
        val mockObserver = mock<Observer<AccountOrganizeOnboardState>>()
        val items = createMockAccounts()
        val sut = buildAccountsOrganizeBoardingViewModel(accounts = items)
        sut.showAccountsOrganizeBoarding.observeForever(mockObserver)

        // When
        sut.userSessionStateChange(SessionStateChange.UNAUTHENTICATED)
        sut.userSessionStateChange(SessionStateChange.AUTHENTICATED)

        // Then
        inOrder(mockObserver, mockObserver) {
            verify(mockObserver)
                .onChanged(eq(AccountOrganizeOnboardState(show = false, showDialogs = false)))
            verify(mockObserver)
                .onChanged(eq(AccountOrganizeOnboardState(show = false, showDialogs = true)))
        }
    }

    @Test
    fun `viewModel AccountOrganizeOnboardState change when user has two or more accounts`() {

        // Given
        val mockObserver = mock<Observer<AccountOrganizeOnboardState>>()
        val items = createMockAccounts(size = 4)
        val sut = buildAccountsOrganizeBoardingViewModel(accounts = items)
        sut.showAccountsOrganizeBoarding.observeForever(mockObserver)

        // When
        sut.userSessionStateChange(SessionStateChange.UNAUTHENTICATED)
        sut.userSessionStateChange(SessionStateChange.AUTHENTICATED)

        // Then
        inOrder(mockObserver, mockObserver) {
            verify(mockObserver)
                .onChanged(eq(AccountOrganizeOnboardState(show = false, showDialogs = false)))
            verify(mockObserver)
                .onChanged(eq(AccountOrganizeOnboardState(show = true, showDialogs = false)))
        }
    }

    private fun buildAccountsOrganizeBoardingViewModel(
        mockApplicationPreferences: ApplicationPreferences = this.mockApplicationPreferences,
        accounts: List<Account> = emptyList(),
    ) = AccountsOrganizeBoardingViewModel(
        applicationPreferences = mockApplicationPreferences,
        accountOrderRepo = mockMsAccountsRepo(accounts.toOrderAccountList()),
        accountRepo = getMockAccountRepo(accounts)
    )

    private fun createMockAccounts(size: Int = 1): List<Account> {
        val items = mutableListOf<Account>()
        repeat(size) {
            items.add(Account(accountShortName = "abcd-6789$it", encAccountId = "123456$it"))
        }
        return items
    }

    private fun getMockAccountRepo(mockAccounts: List<Account>): AccountListRepo = mock {
        on { getAccountList() } doReturn mockAccounts
    }

    private fun mockMsAccountsRepo(accounts: List<AccountOrderItem>) = mockk<AccountOrderRepo> {
        coEvery { getAccounts() } returns (accounts)
        coEvery { saveAccounts(any()) } returns Unit
    }
}
