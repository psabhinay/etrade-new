package com.etrade.mobilepro.screener.data.result

import android.content.res.Resources
import com.etrade.mobilepro.screener.api.result.PrescreenedQuote
import com.etrade.mobilepro.screener.data.R
import javax.inject.Inject

class DefaultResultColumnTitleProvider @Inject constructor(
    private val resources: Resources
) : ResultColumnTitleProvider {
    @Suppress("LongMethod", "ComplexMethod") // nothing to divide, method is simple
    override fun getTitle(column: ResultColumn, items: Collection<PrescreenedQuote>): String =
        when (column) {
            ResultColumn.LastPrice -> resources.getString(R.string.last)
            ResultColumn.ChangeDollar -> resources.getString(R.string.change_dollar)
            ResultColumn.ChangePercent -> resources.getString(R.string.change_percent)
            ResultColumn.Volume -> resources.getString(R.string.volume)
            ResultColumn.MarketCap -> resources.getString(R.string.market_cap)
            ResultColumn.Description -> resources.getString(R.string.fund_name)
            ResultColumn.EtfDescription -> resources.getString(R.string.fund_name)
            ResultColumn.MorningStarRating -> {
                val morningStarRatings = items.maxByOrNull {
                    it.prescreenData?.morningstarAsOfDate ?: ""
                }?.prescreenData?.morningstarAsOfDate
                if (morningStarRatings.isNullOrEmpty()) {
                    resources.getString(R.string.morningstar_rating_with_out_as_of)
                } else {
                    resources.getString(
                        R.string.morningstar_rating_as_of_simple,
                        morningStarRatings
                    )
                }
            }
            ResultColumn.FundCategory -> resources.getString(R.string.fund_profile_title_fund_category)
            ResultColumn.TotalAssets -> resources.getString(R.string.total_net_assets_title_short_dollar)
            ResultColumn.ExpenseRatio -> resources.getString(R.string.expense_ratio_title)
            ResultColumn.ThreeYearsAlpha -> resources.getString(R.string.three_years_alpha)
            ResultColumn.ThreeYearsBeta -> resources.getString(R.string.three_years_beta)
            ResultColumn.ThreeYearsSharpeRatio -> resources.getString(R.string.three_years_sharpe_ratio)
            ResultColumn.ThreeYearsRSquare -> resources.getString(R.string.three_years_r_square)
            ResultColumn.Price4WeeksSP500 -> resources.getString(R.string.price_4weeks_sp500)
            ResultColumn.Price13WeeksSP500 -> resources.getString(R.string.price_13weeks_sp500)
            ResultColumn.Price52WeeksSP500 -> resources.getString(R.string.price_52weeks_sp500)
            ResultColumn.PeRatioTrailing12 -> resources.getString(R.string.pe_ratio_trailing_12)
            ResultColumn.PeRatioCurrentYear -> resources.getString(R.string.pe_ratio_current_year)
            ResultColumn.PeRatioNextYear -> resources.getString(R.string.pe_ratio_next_year)
            ResultColumn.PriceToBook -> resources.getString(R.string.price_to_book)
            ResultColumn.EpsGrowthAnnualCurrent -> resources.getString(R.string.eps_growth_annual_current)
            ResultColumn.EpsGrowthAnnualNext -> resources.getString(R.string.eps_growth_annual_next)
            ResultColumn.RevenueGrowthAnnualCurrent -> resources.getString(R.string.revenue_growth_annual_current)
            ResultColumn.RevenueGrowthAnnualNext -> resources.getString(R.string.revenue_growth_annual_next)
            ResultColumn.EpsGrowthQuarterly -> resources.getString(R.string.eps_growth_quarterly)
            ResultColumn.RevenueGrowthQuarterly -> resources.getString(R.string.revenue_growth_quarterly)
            ResultColumn.DividendYield -> resources.getString(R.string.dividend_yield)
            ResultColumn.Price4WeeksIndustry -> resources.getString(R.string.price_4weeks_industry)
            ResultColumn.Price13WeeksIndustry -> resources.getString(R.string.price_13weeks_industry)
            ResultColumn.Price52WeeksIndustry -> resources.getString(R.string.price_52weeks_industry)
            ResultColumn.PriceChange52WeeksLow -> resources.getString(R.string.price_change_52weeks_low)
            ResultColumn.PriceChange52WeeksHigh -> resources.getString(R.string.price_change_52weeks_high)
            ResultColumn.MarketReturnCategory1Month -> resources.getString(R.string.market_return_category_1month)
            ResultColumn.MarketReturnCategory3Months -> resources.getString(R.string.market_return_category_3months)
            ResultColumn.MarketReturnCategory6Months -> resources.getString(R.string.market_return_category_6months)
            ResultColumn.MarketReturnCategoryYTD -> resources.getString(R.string.market_return_category_ytd)
            ResultColumn.MarketReturnCategory1Year -> resources.getString(R.string.market_return_category_1year)
            ResultColumn.MarketReturnCategory3Years -> resources.getString(R.string.market_return_category_3years)
            ResultColumn.MarketReturnCategory5Years -> resources.getString(R.string.market_return_category_5years)
            ResultColumn.MarketReturnCategory10Years -> resources.getString(R.string.market_return_category_10years)
            ResultColumn.MarketReturnCategorySinceInception -> resources.getString(R.string.market_return_category_since_inception)
            ResultColumn.NavReturnCategory1Month -> resources.getString(R.string.nav_return_category_1month)
            ResultColumn.NavReturnCategory3Months -> resources.getString(R.string.nav_return_category_3months)
            ResultColumn.NavReturnCategory6Months -> resources.getString(R.string.nav_return_category_6months)
            ResultColumn.NavReturnCategoryYTD -> resources.getString(R.string.nav_return_category_ytd)
            ResultColumn.NavReturnCategory1Year -> resources.getString(R.string.nav_return_category_1year)
            ResultColumn.NavReturnCategory3Years -> resources.getString(R.string.nav_return_category_3years)
            ResultColumn.NavReturnCategory5Years -> resources.getString(R.string.nav_return_category_5years)
            ResultColumn.NavReturnCategory10Years -> resources.getString(R.string.nav_return_category_10years)
            ResultColumn.NavReturnCategorySinceInception -> resources.getString(R.string.nav_return_category_since_inception)
            ResultColumn.StandardDeviation -> resources.getString(R.string.standard_deviation_column_title)
            ResultColumn.Yield -> resources.getString(R.string.yield)
            ResultColumn.DistributionYield -> resources.getString(R.string.distribution_yield)
            ResultColumn.CategoryReturn1Year -> resources.getString(R.string.category_return_1year)
            ResultColumn.CategoryReturn5Years -> resources.getString(R.string.category_return_5years)
            ResultColumn.CategoryReturn10Years -> resources.getString(R.string.category_return_10years)
            ResultColumn.TurnOverRatio -> resources.getString(R.string.turn_over_ratio)
            ResultColumn.PortfolioConcentration -> resources.getString(R.string.portfolio_concentration)
            ResultColumn.AverageMarketCap -> resources.getString(R.string.average_market_cap)
            ResultColumn.FrontLoad -> resources.getString(R.string.front_load)
            ResultColumn.DeferLoad -> resources.getString(R.string.defer_load)
            ResultColumn.RedemptionFee -> resources.getString(R.string.redemption_fee)
            ResultColumn.Volume30DaysAvg -> resources.getString(R.string.volume_30_days_avg)
        }
}
