package com.etrade.mobilepro.chartiq.data.repo

import com.etrade.mobilepro.chart.util.resolveEndpointType
import com.etrade.mobilepro.chartiq.api.repo.ChartIqDataRepo
import com.etrade.mobilepro.chartiq.api.rest.ChartIqApiClient
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.util.AuthenticationStatusProvider
import javax.inject.Inject

class DefaultChartIqDataRepo @Inject constructor(
    private val apiClient: ChartIqApiClient,
    private val authStatusProvider: AuthenticationStatusProvider
) : ChartIqDataRepo {
    override suspend fun getChartData(chartQuery: String?): ETResult<String> {
        val endpointType = resolveEndpointType(authStatusProvider.isAuthenticated()).typeCode
        return runCatchingET {
            apiClient.getChartData(endpointType, chartQuery ?: throw IllegalArgumentException("null chartQuery argument"))
        }
    }

    override suspend fun getAllViews(): ETResult<String> = runCatchingET {
        apiClient.getAllViews()
    }

    override suspend fun getStudies(): ETResult<String> = runCatchingET {
        apiClient.getStudies()
    }

    override suspend fun getDrawing(symbol: String?): ETResult<String> = runCatchingET {
        apiClient.getDrawing(symbol ?: throw IllegalArgumentException("null argument"))
    }

    override suspend fun saveView(requestBody: String?): ETResult<String> = runCatchingET {
        apiClient.postRequest("/app/user/saveview.json", requestBody ?: throw IllegalArgumentException("null argument"))
    }

    override suspend fun deleteView(requestBody: String?): ETResult<String> = runCatchingET {
        apiClient.postRequest("/app/user/deleteview.json", requestBody ?: throw IllegalArgumentException("null argument"))
    }

    override suspend fun saveDrawing(requestBody: String?): ETResult<String> = runCatchingET {
        apiClient.postRequest("/app/user/savedrawing.json", requestBody ?: throw IllegalArgumentException("null argument"))
    }

    override suspend fun deleteDrawing(requestBody: String?): ETResult<String> = runCatchingET {
        apiClient.postRequest("/app/user/deleteDrawing.json", requestBody ?: throw IllegalArgumentException("null argument"))
    }

    override suspend fun getOpenOrders(requestBody: String?): ETResult<String> = runCatchingET {
        apiClient.postRequest(
            "/app/vieworders/getorders.json?w_id=neo-widgets-ViewOrders-Overlay1", requestBody ?: throw IllegalArgumentException("null argument")
        )
    }
}
