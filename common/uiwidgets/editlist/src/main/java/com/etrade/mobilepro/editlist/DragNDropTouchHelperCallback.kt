package com.etrade.mobilepro.editlist

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

open class DragNDropTouchHelperCallback(private val viewModel: EditListViewModel<*>) : ItemTouchHelper.Callback() {

    var isDragFinished: Boolean = true
        private set

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        return makeMovementFlags(ItemTouchHelper.UP or ItemTouchHelper.DOWN, 0)
    }

    // in some cases ItemTouchHelper not call onMove and that leads to unexpected items jumps
    // so swapItems changed to itemMove
    // reference https://jira.corp.etradegrp.com/browse/ETAND-8934
    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return viewModel.moveItem(viewHolder.bindingAdapterPosition, target.bindingAdapterPosition)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        // does nothing
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        super.onSelectedChanged(viewHolder, actionState)
        isDragFinished = viewHolder == null
    }

    override fun isLongPressDragEnabled(): Boolean = false
}
