package com.etrade.mobilepro.inputvalueview.validators

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.inputvalueview.InputFieldManager.Value

/**
 * RequiredFieldValidator check if the given value is not blank
 */
class RequiredFieldValidator(private val validationErrorDesc: String) : InputFieldManager.Validator {
    override fun validate(value: String): Value {
        return if (value.isNotBlank()) {
            Value.Valid(value)
        } else {
            Value.Invalid(value, validationErrorDesc)
        }
    }
}
