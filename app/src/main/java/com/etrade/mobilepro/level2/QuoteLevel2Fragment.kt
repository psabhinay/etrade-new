package com.etrade.mobilepro.level2

import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.databinding.FragmentQuoteLevel2Binding
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.level2.adapter.QuoteLevel2Adapter
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.streaming.StreamingSessionDelegateHelper
import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2BookType
import com.etrade.mobilepro.usersubscriptionapi.BookFilter
import com.etrade.mobilepro.usersubscriptionpresentation.UserSubscriptionViewModel
import com.etrade.mobilepro.usersubscriptionpresentation.UserSubscriptionsWebViewActivity
import com.etrade.mobilepro.usersubscriptionpresentation.getDisplayString
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.binding.dataBinding
import com.etrade.mobilepro.util.android.extension.addDividerItemDecoration
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

private const val BOOK_FILTER_SELECTOR = "BOOK_FILTER_SELECTOR"

@RequireLogin
class QuoteLevel2Fragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    @Web private val baseWebUrl: String,
    private val streamingSessionDelegateHelper: StreamingSessionDelegateHelper
) : Fragment(R.layout.fragment_quote_level2) {

    private val binding by dataBinding(FragmentQuoteLevel2Binding::bind)

    private val quoteWidgetViewModel: QuotesWidgetViewModel by viewModels { viewModelFactory }
    private val viewModel: QuoteLevel2ViewModel by viewModels { viewModelFactory }
    private val userSubscriptionViewModel: UserSubscriptionViewModel by viewModels { viewModelFactory }
    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private val args: QuoteLevel2FragmentArgs by navArgs()

    private val manageSubscriptionsRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            userSubscriptionViewModel.getSubscriptions(args.quoteTicker.type.isOption)
            userSubscriptionViewModel.checkAndUpdateMda()
        }

    private lateinit var bookFilterSelector: DropDownMultiSelector<BookFilter>
    private lateinit var quoteLevel2Adapter: QuoteLevel2Adapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val symbol = args.quoteTicker
        setUpViewModels(symbol)

        quoteLevel2Adapter = QuoteLevel2Adapter()
        quoteLevel2Adapter.setHasStableIds(true)

        binding.level2Rv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = quoteLevel2Adapter
            itemAnimator = null // to avoid recycler blinking on streaming values
            addDividerItemDecoration(R.drawable.list_item_divider_light_grey)
        }
    }

    override fun onStart() {
        super.onStart()
        streamingSessionDelegateHelper.startLevel2StreamingSession()
    }

    override fun onStop() {
        super.onStop()
        streamingSessionDelegateHelper.stopLevel2StreamingSession()
    }

    private fun setUpBookFilterSelector() {
        bookFilterSelector = DefaultDropDownMultiSelector(
            parentFragmentManager,
            DropDownMultiSelector.Settings(
                tag = BOOK_FILTER_SELECTOR,
                multiSelectEnabled = false,
                title = getString(R.string.book_filter),
                itemsData = userSubscriptionViewModel.bookFilters,
                selectListener = { _, selectable ->
                    if (!selectable.value.requiresSubscription) {
                        userSubscriptionViewModel.setBookFilter(selectable.value)
                    }
                },
                actionButtonSettings = DropDownMultiSelector.ActionButtonSettings(getString(R.string.manage_subscriptions), this::onManageSubscriptions),
                snackbarUtilFactory = snackbarUtilFactory,
                loadingState = userSubscriptionViewModel.viewState
            )
        )

        binding.filterContainer.setOnClickListener {
            bookFilterSelector.open()
        }
    }

    private fun updateFilterContentDescription(value: String) {
        binding.filterContainer.contentDescription = "$value, ${getString(
            R.string.label_dropdown_description,
            getString(R.string.book_filter)
        )}"
    }

    private fun onManageSubscriptions() {
        context?.let {
            manageSubscriptionsRequest.launch(UserSubscriptionsWebViewActivity.intent(it, baseWebUrl))
        }
    }

    private fun setUpViewModels(symbol: SearchResultItem.Symbol) {
        binding.obj = quoteWidgetViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        quoteWidgetViewModel.getQuotes(symbol.underlyerSymbol)
        userSubscriptionViewModel.bookFilters.removeObservers(viewLifecycleOwner)
        viewModel.setSymbol(symbol)
        setUpBookFilterSelector()
        setupObservers(symbol)
    }

    @Suppress("LongMethod")
    private fun setupObservers(symbol: SearchResultItem.Symbol) {
        userSubscriptionViewModel.selectedBookFilter.observe(
            viewLifecycleOwner,
            Observer {
                if (it.bookType == Level2BookType.NO_BOOK) {
                    binding.valueBookFilter.text =
                        getString(R.string.please_select_filter).also(::updateFilterContentDescription)
                    viewModel.haltStreaming()
                } else {
                    binding.valueBookFilter.text =
                        it.getDisplayString(resources).also(::updateFilterContentDescription)
                    viewModel.streamLevel2(it.bookType)
                }
            }
        )
        if (userSubscriptionViewModel.selectedBookFilter.value == null) {
            userSubscriptionViewModel.getSubscriptions(symbol.type.isOption)
        }
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                handleState(it)
            }
        )
        userSubscriptionViewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                if (it is Resource.Failed) {
                    handleFailure(resources.getString(R.string.error_message_general))
                }
            }
        )
        quoteWidgetViewModel.errorMessage.observe(
            viewLifecycleOwner,
            Observer<ConsumableLiveEvent<String>> {
                it.getNonConsumedContent()?.also { message -> snackBarUtil.errorSnackbar(message) }
            }
        )
    }

    private fun handleState(viewState: QuoteLevel2ViewModel.ViewState) {
        when (viewState) {
            is QuoteLevel2ViewModel.ViewState.Loading -> {
                binding.level2Pb.show()
                binding.level2Rv.visibility = View.GONE
                binding.level2NoData.visibility = View.GONE
            }
            is QuoteLevel2ViewModel.ViewState.Success -> {
                quoteLevel2Adapter.submitQuotes(viewState.result)
                binding.level2Pb.hide()
                binding.level2Rv.visibility = View.VISIBLE
                binding.level2NoData.visibility = View.GONE
            }
            is QuoteLevel2ViewModel.ViewState.Error -> {
                handleFailure(viewState.message)
            }
        }
    }

    private fun handleFailure(message: String) {
        snackBarUtil.errorSnackbar(message)
        binding.level2Pb.hide()
        binding.level2Rv.visibility = View.GONE
        binding.level2NoData.visibility = View.VISIBLE
    }
}
