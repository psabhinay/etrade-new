package com.etrade.mobilepro.msaccounts.presentation

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import com.etrade.msaccounts.dto.MsAccountsEnrollmentViewDto
import javax.inject.Inject

class MsAccountsEnrollmentViewFactory @Inject constructor(
    @Web private val baseUrl: String
) : DynamicUiViewFactory {
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is MsAccountsEnrollmentViewDto) {
            return object : GenericLayout {
                override val viewModelFactory: ViewModelProvider.Factory
                    get() = viewModelFactory {
                        MsAccountsEnrollmentViewModel(baseUrl, dto)
                    }
                override val viewModelClass: Class<out DynamicViewModel>
                    get() = MsAccountsEnrollmentViewModel::class.java
                override val uniqueIdentifier: String
                    get() = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("MsAccountsEnrollmentViewFactory registered to unknown type ${dto.javaClass.canonicalName}")
        }
    }
}
