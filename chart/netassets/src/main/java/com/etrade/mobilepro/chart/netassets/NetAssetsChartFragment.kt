package com.etrade.mobilepro.chart.netassets

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.barchartview.DataEntry
import com.etrade.mobilepro.chart.netassets.databinding.FragmentNetAssetsBinding
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.formatters.safeFormatVolume
import com.etrade.mobilepro.util.mapDateAccessibility
import com.google.android.material.snackbar.Snackbar
import org.threeten.bp.Month
import org.threeten.bp.format.TextStyle
import java.util.Locale
import javax.inject.Inject
import kotlin.math.absoluteValue
import kotlin.math.roundToInt
import kotlin.math.sign

@RequireLogin
class NetAssetsChartFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.fragment_net_assets) {

    private val binding by viewBinding(FragmentNetAssetsBinding::bind)

    private val viewModel: NetAssetsChartViewModel by viewModels { viewModelFactory }

    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.accountId = arguments?.getString("accountId")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.chartView.apply {
            xAxisValueFormatter = ::formatXAxis
            yAxisValueFormatter = ::formatYAxis
            setNoDataTextColor(ContextCompat.getColor(context, R.color.textColorPrimary))
        }

        viewModel.chartState.observe(
            viewLifecycleOwner,
            Observer {
                onChartState(it)
            }
        )
    }

    private fun onChartState(state: NetAssetsChartViewModel.ChartState) {
        when (state) {
            is NetAssetsChartViewModel.ChartState.Idle -> displayChart(state.entries)
            is NetAssetsChartViewModel.ChartState.Loading -> displayChartLoading()
            is NetAssetsChartViewModel.ChartState.Error -> displayChartError()
        }
    }

    private fun displayChart(entries: List<DataEntry>) {
        with(binding.chartView) {
            add(entries, true)
            isClickable = true
            importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
            contentDescription = getChartContentDescription(entries, xAxisValueFormatter)
            visibility = View.VISIBLE
        }
        binding.chartLoadingView.visibility = View.GONE
    }

    private fun getChartContentDescription(
        list: List<DataEntry>,
        xAxisValueFormatter: ((Float) -> String)?
    ): String {
        return list.map {
            val xAxisValue = xAxisValueFormatter?.invoke(it.x) ?: it.x.toString()
            getString(
                R.string.two_params_description,
                xAxisValue.mapDateAccessibility(),
                it.y.toString()
            )
        }.takeIf { it.isNotEmpty() }?.reduce { acc, s -> "$acc $s" } ?: ""
    }

    private fun displayChartLoading() {
        with(binding) {
            chartView.visibility = View.GONE
            chartLoadingView.visibility = View.VISIBLE
        }
    }

    private fun displayChartError() {
        binding.chartLoadingView.visibility = View.GONE
        with(binding.chartView) {
            visibility = View.VISIBLE
            importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
            contentDescription = resources.getString(R.string.no_chart_data_available)
        }
        snackBarUtil.snackbar(getString(R.string.error_message_general), Snackbar.LENGTH_SHORT)?.show()
    }
}

internal fun formatXAxis(value: Float): String {
    val monthIndex = value.roundToInt() % NUMBER_OF_MONTHS
    val month = if (monthIndex == 0) { Month.DECEMBER } else { Month.of(monthIndex) }
    return month.getDisplayName(TextStyle.SHORT, Locale.US)
}

internal fun formatYAxis(value: Float): String {
    val sign = if (value.sign < 0) { "-" } else { "" }
    val formatted = safeFormatVolume(value.absoluteValue.toBigDecimal())
    return "$sign$formatted"
}
