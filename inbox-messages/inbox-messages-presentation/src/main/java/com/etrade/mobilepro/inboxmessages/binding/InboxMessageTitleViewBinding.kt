package com.etrade.mobilepro.inboxmessages.binding

import android.view.View
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.menuitem.MenuItemTitleView

class InboxMessageTitleViewBinding(
    private val onItemClickListener: ((InboxMessage) -> Unit)? = null
) : InboxMessageBinding {

    override val layoutRes: Int = com.etrade.mobilepro.inboxmessages.R.layout.inbox_message_dynamic_title_view

    override fun bind(item: InboxMessage, view: View) {
        if (view is MenuItemTitleView) {
            view.text = item.title
        }

        view.setOnClickListener {
            onItemClickListener?.invoke(item)
        }
    }
}
