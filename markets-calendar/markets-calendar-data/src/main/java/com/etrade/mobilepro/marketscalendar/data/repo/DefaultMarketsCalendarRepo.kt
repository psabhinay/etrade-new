package com.etrade.mobilepro.marketscalendar.data.repo

import com.etrade.mobilepro.marketscalendar.api.model.EventsEarnings
import com.etrade.mobilepro.marketscalendar.api.model.EventsEconomic
import com.etrade.mobilepro.marketscalendar.api.model.EventsRequest
import com.etrade.mobilepro.marketscalendar.api.model.EventsResponse
import com.etrade.mobilepro.marketscalendar.api.model.EventsSummaryResponse
import com.etrade.mobilepro.marketscalendar.api.repo.MarketsCalendarRepo
import com.etrade.mobilepro.marketscalendar.data.mapper.mapToEventsEarningsResponse
import com.etrade.mobilepro.marketscalendar.data.mapper.mapToEventsEconomicResponse
import com.etrade.mobilepro.marketscalendar.data.mapper.mapToEventsRequestBody
import com.etrade.mobilepro.marketscalendar.data.mapper.mapToEventsSummaryResponse
import com.etrade.mobilepro.marketscalendar.data.rest.MarketsCalendarApiService
import io.reactivex.Single
import javax.inject.Inject

class DefaultMarketsCalendarRepo @Inject constructor(
    private val apiService: MarketsCalendarApiService
) : MarketsCalendarRepo {

    override fun getEventsEarningsRx(request: EventsRequest): Single<EventsResponse<EventsEarnings>> {
        val requestBody = request.mapToEventsRequestBody()
        return apiService.getEventsEarningsRx(requestBody)
            .map {
                it.mapToEventsEarningsResponse()
            }
    }

    override fun getEventsEconomic(request: EventsRequest): Single<EventsResponse<EventsEconomic>> {
        val requestBody = request.mapToEventsRequestBody()
        return apiService.getEventsEconomic(requestBody)
            .map {
                it.mapToEventsEconomicResponse()
            }
    }

    override fun getEventsSummary(request: EventsRequest): Single<EventsSummaryResponse> {
        val requestBody = request.mapToEventsRequestBody()
        return apiService.getEventsSummary(requestBody)
            .map {
                it.mapToEventsSummaryResponse()
            }
    }

    override suspend fun getEventsEarnings(request: EventsRequest): EventsResponse<EventsEarnings> {
        return apiService.getEventsEarnings(body = request.mapToEventsRequestBody()).mapToEventsEarningsResponse()
    }
}
