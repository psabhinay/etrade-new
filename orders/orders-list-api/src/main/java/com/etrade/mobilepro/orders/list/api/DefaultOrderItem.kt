package com.etrade.mobilepro.orders.list.api

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.ExecInstruction
import com.etrade.mobilepro.orders.api.MarketSession
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import org.threeten.bp.Instant
import java.math.BigDecimal

/**
 * OrderItem object used by orders list.
 * @param orderNumber orderNumber id used to fetch order details
 * @param date an instant of date placed
 * @param orderStatus order status string combination of state and advanced order type
 * @param advancedOrderType advanced order type of this order
 * @param orderDescriptions list of order descriptions to display
 * @param orderType orderType used to determine which detail service to use
 */
data class DefaultOrderItem(
    override val orderNumber: String,
    override val symbols: List<String>,
    override val orderStatus: OrderStatus,
    val displaySymbol: String,
    val date: Instant?,
    val orderDescriptions: List<OrderDetailsDescription>,
    val advancedOrderType: AdvancedOrderType,
    val orderType: OrderType,
    val origSysCode: OrigSysCode,
    val hasDependentFlag: Boolean,
    val dependentOrderNumber: Int,
    val orderTerm: OrderTerm,
    val goodThroughDateFlag: Boolean,
    val stopPrice: BigDecimal,
    val hiddenFlag: Boolean,
    val qtyReserveShow: Int,
    val instrumentType: InstrumentType,
    val priceType: PriceType,
    val orderSpecialAttribCd: String,
    val marketSession: MarketSession,
    val isMultiLeg: Boolean = orderDescriptions.size > 1,
    val execInstruction: ExecInstruction,
    val advancedOrderSymbol: String?,
    val advancedOrderDisplaySymbol: String?
) : OrderItem

/**
 * OrderDetailsDescription used by UI to display information of each leg or each leg in a group of orders.
 * @param fillQuantityValue just quantity value or a fraction of filled quantity over quantity placed
 * @param transactionType transaction type of this order
 * @param displaySymbol displaySymbol being traded
 */
data class OrderDetailsDescription(
    val fillQuantityValue: String,
    val transactionType: TransactionType,
    val displaySymbol: String
)
