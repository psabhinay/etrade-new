package com.etrade.mobilepro.streaming.api

enum class Level1Field {
    ASK,
    ASK_EXCHANGE,
    ASK_SIZE,
    BETA,
    BID,
    BID_EXCHANGE,
    BID_SIZE,
    CHANGE,
    CHANGE_PERCENT,
    CLOSING_MARK,
    DAY_CHANGE,
    DAY_CHANGE_PERCENT,
    DAY_HIGH,
    DAY_LOW,
    DELTA,
    DIVIDEND,
    DIVIDEND_YIELD,
    EPS,
    GAMMA,
    IMPLIED_VOLATILITY,
    INTRINSIC_VALUE,
    LAST_PRICE,
    LAST_PRICE_EXCHANGE,
    LAST_VOLUME,
    MARK,
    MARKET_CAP,
    OPEN_INTEREST,
    OPEN_PRICE,
    RHO,
    PREVIOUS_CLOSE_PRICE,
    PRICE_TO_EARNINGS, // "P/E"
    THETA,
    TICK_INDICATOR,
    TIMESTAMP_SINCE_EPOCH,
    TRADE_HALT_REASON,
    TRADE_STATUS,
    VEGA,
    VOLUME,
    VOLUME_TEN_DAY_AVERAGE,
    YEAR_HIGH,
    YEAR_HIGH_DATE,
    YEAR_LOW,
    YEAR_LOW_DATE;
}
