package com.etrade.mobilepro.appwidget

import android.content.Context
import com.etrade.mobilepro.appwidget.widgetcontainer.DefaultAppWidgetProvider
import javax.inject.Inject

class DefaultAppWidgetHandler @Inject constructor(private val context: Context) : AppWidgetHandler {
    override fun refresh() = DefaultAppWidgetProvider.refresh(context)
}
