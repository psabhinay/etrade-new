package com.etrade.mobilepro.screener.api.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class Stock

@Qualifier
annotation class MutualFund

@Qualifier
annotation class Etf
