package com.etrade.mobilepro.instrument

interface WithSymbol {
    val symbol: String
}

val WithSymbol.isOption: Boolean
    get() = isSymbolLongOption(symbol)

val WithSymbol.displaySymbol: String
    get() = symbol.takeIf { isOption }?.let { getDisplaySymbolFromOsiKey(it) } ?: symbol
