package com.etrade.mobilepro.trade.presentation

import com.etrade.mobilepro.inboxmessages.CampaignAssetsTheme
import com.etrade.mobilepro.inboxmessages.InboxAttributes.MESSAGE_TYPE
import com.etrade.mobilepro.inboxmessages.InboxAttributes.SYMBOL
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.InboxMessagesStaticPayload
import com.etrade.mobilepro.inboxmessages.MessagePlace
import com.etrade.mobilepro.inboxmessages.MessageType
import com.etrade.mobilepro.inboxmessages.UserType
import com.etrade.mobilepro.trade.presentation.form.delegate.hasHaltedSymbol
import kotlinx.collections.immutable.ImmutableMap
import kotlinx.collections.immutable.persistentMapOf
import org.junit.Assert
import org.junit.Test
import java.util.Date

class TradeViewModelTest {

    private val apple = TestMessage(
        persistentMapOf(
            MESSAGE_TYPE to MessageType.TRADE_INFORMATION.type,
            SYMBOL to "AAPL"
        )
    )

    private val tesla = TestMessage(
        persistentMapOf(
            MESSAGE_TYPE to MessageType.TRADE_INFORMATION.type,
            SYMBOL to "TSLA"
        )
    )

    private val kraft = TestMessage(
        persistentMapOf(
            MESSAGE_TYPE to MessageType.TRADE_INFORMATION.type,
            SYMBOL to "KHC"
        )
    )

    @Test
    fun `should halt symbol single`() {
        val result = Pair("AAPL", InboxMessagesStaticPayload(listOf(apple))).hasHaltedSymbol()
        Assert.assertEquals(result, true)
    }

    @Test
    fun `should halt symbol multiple`() {
        val result = Pair("KHC", InboxMessagesStaticPayload(listOf(apple, tesla, kraft))).hasHaltedSymbol()
        Assert.assertEquals(result, true)
    }

    @Test
    fun `should halt symbol empty string`() {
        val result = Pair("", InboxMessagesStaticPayload(listOf(apple, tesla, kraft))).hasHaltedSymbol()
        Assert.assertEquals(result, false)
    }

    @Test
    fun `should halt symbol null`() {
        val result = Pair(null, InboxMessagesStaticPayload(listOf(apple, tesla, kraft))).hasHaltedSymbol()
        Assert.assertEquals(result, false)
    }

    @Test
    fun `should halt symbol no match`() {
        val result = Pair("AAPL", InboxMessagesStaticPayload(listOf(tesla))).hasHaltedSymbol()
        Assert.assertEquals(result, false)
    }

    @Test
    fun `should halt symbol empty list`() {
        val result = Pair("KHC", InboxMessagesStaticPayload(listOf())).hasHaltedSymbol()
        Assert.assertEquals(result, false)
    }

    private class TestMessage(override val rawAttributes: ImmutableMap<String, String>) : InboxMessage {
        override val id: Long = 0

        override val title: String
            get() = "Trading Paused"

        override val subtitle: String
            get() = "Tap for more information"

        override val message: String
            get() = ""

        override val type: MessageType
            get() = MessageType.TRADE_INFORMATION

        override val place: MessagePlace?
            get() = null

        override val deepLink: String?
            get() = null

        override val userType: List<UserType>?
            get() = null

        override val assetHeight: Int?
            get() = null

        override val thumbnailUrl: String
            get() = ""

        override val hasThumbnail: Boolean
            get() = false

        override val isSimpleInboxMessage: Boolean
            get() = false

        override val theme: CampaignAssetsTheme?
            get() = CampaignAssetsTheme.LIGHT_THEME

        override val backgroundColor: String?
            get() = null

        override val receivedDate: Date
            get() = Date()

        override val arrow: Boolean
            get() = false

        override val icon: Boolean
            get() = false

        override val headLine: String?
            get() = null

        override val bodyText: String?
            get() = null

        override val symbol: String?
            get() = rawAttributes[SYMBOL]
    }
}
