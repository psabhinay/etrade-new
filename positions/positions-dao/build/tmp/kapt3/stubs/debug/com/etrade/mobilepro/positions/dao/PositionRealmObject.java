package com.etrade.mobilepro.positions.dao;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0013\n\u0002\u0010\u000b\n\u0002\b\u0015\n\u0002\u0018\u0002\n\u0002\bN\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0016\u0018\u00002\u00020\u00012\u00020\u0002B\u0099\u0002\u0012\b\b\u0002\u0010\u0003\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0004\u0012\b\b\u0002\u0010\b\u001a\u00020\u0004\u0012\b\b\u0002\u0010\t\u001a\u00020\u0004\u0012\b\b\u0002\u0010\n\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0004\u0012\b\b\u0002\u0010\f\u001a\u00020\u0004\u0012\b\b\u0002\u0010\r\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0004\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0018\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0018\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0018\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u001c\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u0004\u0012\b\b\u0002\u0010\u001e\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010 J\u009f\u0002\u0010z\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0006\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00042\b\b\u0002\u0010\b\u001a\u00020\u00042\b\b\u0002\u0010\t\u001a\u00020\u00042\b\b\u0002\u0010\n\u001a\u00020\u00042\b\b\u0002\u0010\u000b\u001a\u00020\u00042\b\b\u0002\u0010\f\u001a\u00020\u00042\b\b\u0002\u0010\r\u001a\u00020\u00042\b\b\u0002\u0010\u000e\u001a\u00020\u00042\b\b\u0002\u0010\u000f\u001a\u00020\u00042\b\b\u0002\u0010\u0010\u001a\u00020\u00042\b\b\u0002\u0010\u0011\u001a\u00020\u00042\b\b\u0002\u0010\u0012\u001a\u00020\u00042\b\b\u0002\u0010\u0013\u001a\u00020\u00042\b\b\u0002\u0010\u0014\u001a\u00020\u00042\b\b\u0002\u0010\u0015\u001a\u00020\u00042\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00042\b\b\u0002\u0010\u0017\u001a\u00020\u00182\b\b\u0002\u0010\u0019\u001a\u00020\u00182\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u00182\b\b\u0002\u0010\u001b\u001a\u00020\u00042\b\b\u0002\u0010\u001c\u001a\u00020\u00042\b\b\u0002\u0010\u001d\u001a\u00020\u00042\b\b\u0002\u0010\u001e\u001a\u00020\u00042\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010{J\u0010\u0010|\u001a\u00020}2\b\u0010~\u001a\u0004\u0018\u00010\u0000R\u001a\u0010\u0006\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R\u001a\u0010\u0005\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\"\"\u0004\b&\u0010$R\u0016\u0010\'\u001a\u0004\u0018\u00010\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b(\u0010\"R\u001c\u0010\u001f\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010\"\"\u0004\b*\u0010$R\u001a\u0010\b\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\"\"\u0004\b,\u0010$R\u0016\u0010-\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b/\u00100R\u001a\u0010\n\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\"\"\u0004\b2\u0010$R\u0016\u00103\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b4\u00100R\u0016\u00105\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b6\u00100R\u001a\u0010\u0012\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\"\"\u0004\b8\u0010$R\u001a\u0010\u0011\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b9\u0010\"\"\u0004\b:\u0010$R\u001a\u0010\u0017\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b;\u0010<\"\u0004\b=\u0010>R\u001c\u0010\u0016\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b?\u0010\"\"\u0004\b@\u0010$R\u0016\u0010A\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\bB\u00100R\u001a\u0010\f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bC\u0010\"\"\u0004\bD\u0010$R\u0016\u0010E\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\bF\u00100R\u001a\u0010\r\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bG\u0010\"\"\u0004\bH\u0010$R\u001a\u0010\u0019\u001a\u00020\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bI\u0010<\"\u0004\bJ\u0010>R\u001e\u0010\u001a\u001a\u0004\u0018\u00010\u0018X\u0096\u000e\u00a2\u0006\u0010\n\u0002\u0010O\u001a\u0004\bK\u0010L\"\u0004\bM\u0010NR\u0016\u0010P\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\bQ\u00100R\u001a\u0010\u001d\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bR\u0010\"\"\u0004\bS\u0010$R\u0016\u0010T\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\bU\u00100R\u001a\u0010\u001e\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bV\u0010\"\"\u0004\bW\u0010$R\u001a\u0010\u001b\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bX\u0010\"\"\u0004\bY\u0010$R\u001e\u0010\u0003\u001a\u00020\u00048\u0016@\u0016X\u0097\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bZ\u0010\"\"\u0004\b[\u0010$R\u0016\u0010\\\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\b]\u00100R\u001a\u0010\u0007\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b^\u0010\"\"\u0004\b_\u0010$R\u0016\u0010`\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\ba\u00100R\u001a\u0010\u000b\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bb\u0010\"\"\u0004\bc\u0010$R\u001a\u0010\u001c\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bd\u0010\"\"\u0004\be\u0010$R\u001a\u0010\t\u001a\u00020\u0004X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bf\u0010\"\"\u0004\bg\u0010$R\u0016\u0010h\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\bi\u00100R\u001a\u0010\u0010\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bj\u0010\"\"\u0004\bk\u0010$R\u001a\u0010\u000f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bl\u0010\"\"\u0004\bm\u0010$R\u001a\u0010\u000e\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bn\u0010\"\"\u0004\bo\u0010$R\u001a\u0010\u0015\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bp\u0010\"\"\u0004\bq\u0010$R\u0016\u0010r\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\bs\u00100R\u0016\u0010t\u001a\u0004\u0018\u00010.8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\bu\u00100R\u001a\u0010\u0014\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bv\u0010\"\"\u0004\bw\u0010$R\u001a\u0010\u0013\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bx\u0010\"\"\u0004\by\u0010$\u00a8\u0006\u007f"}, d2 = {"Lcom/etrade/mobilepro/positions/dao/PositionRealmObject;", "Lio/realm/RealmObject;", "Lcom/etrade/mobilepro/positions/api/Position;", "positionId", "", "accountUuid", "accountId", "pricePaidRaw", "basisPrice", "symbol", "commissionRaw", "quantityRaw", "displayQuantityRaw", "feesRaw", "todayQuantity", "todayPricePaid", "todayCommissions", "daysGainValue", "daysGainPercentageRaw", "totalGainValue", "totalGainPercentageRaw", "todaysClose", "daysQuantity", "daysPurchase", "", "hasLots", "inTheMoneyFlag", "optionUnderlier", "strikePrice", "markToMarket", "marketValueRaw", "baseSymbolPrice", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccountId", "()Ljava/lang/String;", "setAccountId", "(Ljava/lang/String;)V", "getAccountUuid", "setAccountUuid", "baseSymbolAndPrice", "getBaseSymbolAndPrice", "getBaseSymbolPrice", "setBaseSymbolPrice", "getBasisPrice", "setBasisPrice", "commission", "Ljava/math/BigDecimal;", "getCommission", "()Ljava/math/BigDecimal;", "getCommissionRaw", "setCommissionRaw", "daysGain", "getDaysGain", "daysGainPercentage", "getDaysGainPercentage", "getDaysGainPercentageRaw", "setDaysGainPercentageRaw", "getDaysGainValue", "setDaysGainValue", "getDaysPurchase", "()Z", "setDaysPurchase", "(Z)V", "getDaysQuantity", "setDaysQuantity", "displayQuantity", "getDisplayQuantity", "getDisplayQuantityRaw", "setDisplayQuantityRaw", "fees", "getFees", "getFeesRaw", "setFeesRaw", "getHasLots", "setHasLots", "getInTheMoneyFlag", "()Ljava/lang/Boolean;", "setInTheMoneyFlag", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "mark", "getMark", "getMarkToMarket", "setMarkToMarket", "marketValue", "getMarketValue", "getMarketValueRaw", "setMarketValueRaw", "getOptionUnderlier", "setOptionUnderlier", "getPositionId", "setPositionId", "pricePaid", "getPricePaid", "getPricePaidRaw", "setPricePaidRaw", "quantity", "getQuantity", "getQuantityRaw", "setQuantityRaw", "getStrikePrice", "setStrikePrice", "getSymbol", "setSymbol", "todayClose", "getTodayClose", "getTodayCommissions", "setTodayCommissions", "getTodayPricePaid", "setTodayPricePaid", "getTodayQuantity", "setTodayQuantity", "getTodaysClose", "setTodaysClose", "totalGain", "getTotalGain", "totalGainPercentage", "getTotalGainPercentage", "getTotalGainPercentageRaw", "setTotalGainPercentageRaw", "getTotalGainValue", "setTotalGainValue", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/etrade/mobilepro/positions/dao/PositionRealmObject;", "updateWithStreamUpdatedObject", "", "positionRealmObject", "positions-dao_debug"})
public class PositionRealmObject extends io.realm.RealmObject implements com.etrade.mobilepro.positions.api.Position {
    @org.jetbrains.annotations.NotNull()
    @io.realm.annotations.PrimaryKey()
    private java.lang.String positionId;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String accountUuid;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String accountId;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String pricePaidRaw;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String basisPrice;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String symbol;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String commissionRaw;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String quantityRaw;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String displayQuantityRaw;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String feesRaw;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String todayQuantity;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String todayPricePaid;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String todayCommissions;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String daysGainValue;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String daysGainPercentageRaw;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String totalGainValue;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String totalGainPercentageRaw;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String todaysClose;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String daysQuantity;
    private boolean daysPurchase;
    private boolean hasLots;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean inTheMoneyFlag;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String optionUnderlier;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String strikePrice;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String markToMarket;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String marketValueRaw;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String baseSymbolPrice;
    
    public PositionRealmObject() {
        super();
    }
    
    public PositionRealmObject(@org.jetbrains.annotations.NotNull()
    java.lang.String positionId, @org.jetbrains.annotations.NotNull()
    java.lang.String accountUuid, @org.jetbrains.annotations.NotNull()
    java.lang.String accountId, @org.jetbrains.annotations.NotNull()
    java.lang.String pricePaidRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String basisPrice, @org.jetbrains.annotations.NotNull()
    java.lang.String symbol, @org.jetbrains.annotations.NotNull()
    java.lang.String commissionRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String quantityRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String displayQuantityRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String feesRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String todayQuantity, @org.jetbrains.annotations.NotNull()
    java.lang.String todayPricePaid, @org.jetbrains.annotations.NotNull()
    java.lang.String todayCommissions, @org.jetbrains.annotations.NotNull()
    java.lang.String daysGainValue, @org.jetbrains.annotations.NotNull()
    java.lang.String daysGainPercentageRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String totalGainValue, @org.jetbrains.annotations.NotNull()
    java.lang.String totalGainPercentageRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String todaysClose, @org.jetbrains.annotations.Nullable()
    java.lang.String daysQuantity, boolean daysPurchase, boolean hasLots, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean inTheMoneyFlag, @org.jetbrains.annotations.NotNull()
    java.lang.String optionUnderlier, @org.jetbrains.annotations.NotNull()
    java.lang.String strikePrice, @org.jetbrains.annotations.NotNull()
    java.lang.String markToMarket, @org.jetbrains.annotations.NotNull()
    java.lang.String marketValueRaw, @org.jetbrains.annotations.Nullable()
    java.lang.String baseSymbolPrice) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getPositionId() {
        return null;
    }
    
    public void setPositionId(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAccountUuid() {
        return null;
    }
    
    public final void setAccountUuid(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAccountId() {
        return null;
    }
    
    public final void setAccountId(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPricePaidRaw() {
        return null;
    }
    
    public final void setPricePaidRaw(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getBasisPrice() {
        return null;
    }
    
    public final void setBasisPrice(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getSymbol() {
        return null;
    }
    
    public void setSymbol(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCommissionRaw() {
        return null;
    }
    
    public final void setCommissionRaw(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuantityRaw() {
        return null;
    }
    
    public final void setQuantityRaw(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDisplayQuantityRaw() {
        return null;
    }
    
    public final void setDisplayQuantityRaw(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFeesRaw() {
        return null;
    }
    
    public final void setFeesRaw(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTodayQuantity() {
        return null;
    }
    
    public final void setTodayQuantity(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTodayPricePaid() {
        return null;
    }
    
    public final void setTodayPricePaid(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTodayCommissions() {
        return null;
    }
    
    public final void setTodayCommissions(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDaysGainValue() {
        return null;
    }
    
    public final void setDaysGainValue(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDaysGainPercentageRaw() {
        return null;
    }
    
    public final void setDaysGainPercentageRaw(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTotalGainValue() {
        return null;
    }
    
    public final void setTotalGainValue(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTotalGainPercentageRaw() {
        return null;
    }
    
    public final void setTotalGainPercentageRaw(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTodaysClose() {
        return null;
    }
    
    public final void setTodaysClose(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDaysQuantity() {
        return null;
    }
    
    public final void setDaysQuantity(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public final boolean getDaysPurchase() {
        return false;
    }
    
    public final void setDaysPurchase(boolean p0) {
    }
    
    public final boolean getHasLots() {
        return false;
    }
    
    public final void setHasLots(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Boolean getInTheMoneyFlag() {
        return null;
    }
    
    public void setInTheMoneyFlag(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOptionUnderlier() {
        return null;
    }
    
    public final void setOptionUnderlier(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStrikePrice() {
        return null;
    }
    
    public final void setStrikePrice(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMarkToMarket() {
        return null;
    }
    
    public final void setMarkToMarket(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMarketValueRaw() {
        return null;
    }
    
    public final void setMarketValueRaw(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBaseSymbolPrice() {
        return null;
    }
    
    public final void setBaseSymbolPrice(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getDaysGain() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getTotalGain() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getPricePaid() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getCommission() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getFees() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getQuantity() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getDisplayQuantity() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getDaysGainPercentage() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getTotalGainPercentage() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getTodayClose() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.String getBaseSymbolAndPrice() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getMark() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.math.BigDecimal getMarketValue() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"LongParameterList", "LongMethod"})
    public final com.etrade.mobilepro.positions.dao.PositionRealmObject copy(@org.jetbrains.annotations.NotNull()
    java.lang.String positionId, @org.jetbrains.annotations.NotNull()
    java.lang.String accountUuid, @org.jetbrains.annotations.NotNull()
    java.lang.String accountId, @org.jetbrains.annotations.NotNull()
    java.lang.String pricePaidRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String basisPrice, @org.jetbrains.annotations.NotNull()
    java.lang.String symbol, @org.jetbrains.annotations.NotNull()
    java.lang.String commissionRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String quantityRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String displayQuantityRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String feesRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String todayQuantity, @org.jetbrains.annotations.NotNull()
    java.lang.String todayPricePaid, @org.jetbrains.annotations.NotNull()
    java.lang.String todayCommissions, @org.jetbrains.annotations.NotNull()
    java.lang.String daysGainValue, @org.jetbrains.annotations.NotNull()
    java.lang.String daysGainPercentageRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String totalGainValue, @org.jetbrains.annotations.NotNull()
    java.lang.String totalGainPercentageRaw, @org.jetbrains.annotations.NotNull()
    java.lang.String todaysClose, @org.jetbrains.annotations.Nullable()
    java.lang.String daysQuantity, boolean daysPurchase, boolean hasLots, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean inTheMoneyFlag, @org.jetbrains.annotations.NotNull()
    java.lang.String optionUnderlier, @org.jetbrains.annotations.NotNull()
    java.lang.String strikePrice, @org.jetbrains.annotations.NotNull()
    java.lang.String markToMarket, @org.jetbrains.annotations.NotNull()
    java.lang.String marketValueRaw, @org.jetbrains.annotations.Nullable()
    java.lang.String baseSymbolPrice) {
        return null;
    }
    
    public final void updateWithStreamUpdatedObject(@org.jetbrains.annotations.Nullable()
    com.etrade.mobilepro.positions.dao.PositionRealmObject positionRealmObject) {
    }
}