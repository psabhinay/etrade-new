package com.etrade.mobilepro.viewmodel

import androidx.lifecycle.LiveData

/**
 * Marks a view model that has one or more blocking operations that forbid any other interactions with the view model while an operation is in progress.
 */
interface BlockingLoadingIndicatorViewModel {

    /**
     * Indicator of a blocking operations. If `true` then one or more blocking operations are currently in progress, `false` all interractions with the view
     * model are possible.
     */
    val blockingLoadingIndicator: LiveData<Boolean>
}
