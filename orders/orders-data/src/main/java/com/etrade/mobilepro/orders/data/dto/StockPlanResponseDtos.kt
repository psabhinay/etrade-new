package com.etrade.mobilepro.orders.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "viewordersresponse", strict = false)
data class StockPlanOrdersResponseDto(
    @field:Element(name = "Errormessage", required = false)
    var errorMessage: String? = null,

    @field:Element(name = "orderstatus", required = false)
    var orderStatus: String? = null,

    @field:ElementList(name = "OrderList")
    var orders: List<StockPlanOrderDto>? = null,

    @field:ElementList(name = "AccountList")
    var accountList: List<AccountDto>? = null
)

@Root(name = "Account", strict = false)
data class AccountDto(
    @field:Element(name = "symbol", required = false)
    var symbol: String? = null,
    @field:Element(name = "isDefault", required = false)
    var isDefault: String? = null
)

@Root(name = "Order", strict = false)
data class StockPlanOrderDto(
    @field:Element(name = "date", required = false)
    var date: String? = null,
    @field:Element(name = "plantype", required = false)
    var planType: String? = null,
    @field:Element(name = "ordertype", required = false)
    var orderType: String? = null,
    @field:Element(name = "pricetype", required = false)
    var priceType: String? = null,
    @field:Element(name = "executionprice", required = false)
    var executionPrice: String? = null,
    @field:Element(name = "sharestoexercise", required = false)
    var sharesToExercise: String? = null,
    @field:Element(name = "Exercised", required = false)
    var exercised: String? = null,
    @field:Element(name = "sharestosell", required = false)
    var sharesToSell: String? = null,
    @field:Element(name = "sold", required = false)
    var sold: String? = null,
    @field:Element(name = "ordernumber", required = false)
    var orderNumber: String? = null,
    @field:Element(name = "accountindex", required = false)
    var accountIndex: String? = null,
    @field:Element(name = "orderstatus", required = false)
    var orderStatus: String? = null
)
