package com.etrade.mobilepro.screener.presentation.etf.risk

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.etf.EtfRiskFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Etf
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class EtfRiskViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @Etf screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<EtfRiskFilter, EtfRiskViewModel.ViewState>(aggregator, ScreenerType.Etf.Risk, screenerInteractor) {
    override fun provideInitialState(initialFilter: EtfRiskFilter): ViewState = ViewState(filter = initialFilter)

    data class ViewState(
        override val filter: EtfRiskFilter
    ) : BaseFilterScreenViewModel.ViewState<EtfRiskFilter>
}
