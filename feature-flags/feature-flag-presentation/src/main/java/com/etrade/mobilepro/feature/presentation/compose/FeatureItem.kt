package com.etrade.mobilepro.feature.presentation.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.etrade.mobilepro.feature.presentation.R
import com.etrade.mobilepro.feature.presentation.viewmodel.FeatureFlagItemViewModel
import com.etrade.mobilepro.feature.presentation.viewmodel.yesOrNoRes

@Composable
@Suppress("LongMethod")
internal fun FeatureItem(
    viewModel: FeatureFlagItemViewModel,
    initialStatus: String = stringResource(
        R.string.feature_flags_provider_status_default,
        stringResource(viewModel.defaultValue.yesOrNoRes())
    )
) = Column(
    Modifier
        .fillMaxWidth()
        .wrapContentHeight()
        .background(MaterialTheme.colors.background)
        .padding(8.dp)
) {
    Row {
        Text(
            text = viewModel.title ?: viewModel.key,
            style = MaterialTheme.typography.h6,
            color = MaterialTheme.colors.onBackground,
            modifier = Modifier.weight(1f)
        )
        if (viewModel.remoteConfig) {
            Icon(
                painter = painterResource(id = R.drawable.ic_remote_config),
                contentDescription = stringResource(R.string.feature_flags_remote_config_desc),
                tint = MaterialTheme.colors.primary,
                modifier = Modifier.padding(start = 8.dp)
            )
        }
    }

    Row(verticalAlignment = Alignment.CenterVertically) {
        Column(Modifier.weight(1f)) {
            Text(
                text = stringResource(R.string.feature_flags_key_display, viewModel.key),
                style = MaterialTheme.typography.subtitle2,
                color = MaterialTheme.colors.onBackground
            )
            val providerStatus: String by viewModel.providerStatus
                .collectAsState(initial = initialStatus)
            if (viewModel.explanation?.isNotBlank() == true) {
                Text(
                    text = viewModel.explanation,
                    style = MaterialTheme.typography.body1,
                    color = MaterialTheme.colors.onBackground,
                    modifier = Modifier.padding(start = 8.dp)
                )
            }
            Text(
                text = providerStatus,
                style = MaterialTheme.typography.subtitle2,
                color = MaterialTheme.colors.onBackground
            )
        }
        FeatureSwitch(viewModel, Modifier.padding(start = 8.dp))
    }
}

@Preview
@Composable
private fun FeatureItemEnabled() = FeatureItem(itemViewModel("Item"), defaultItemStatus())

@Preview
@Composable
private fun FeatureItemRemote() =
    FeatureItem(itemViewModel("RemoteItem", remoteConfig = true), defaultItemStatus())

@Preview
@Composable
private fun FeatureItemLongKey() =
    FeatureItem(itemViewModel("VeryVeryVerboseItemKey"), defaultItemStatus())

@Preview
@Composable
private fun FeatureItemNoExplanation() =
    FeatureItem(itemViewModel("Item", explanation = null), defaultItemStatus())
