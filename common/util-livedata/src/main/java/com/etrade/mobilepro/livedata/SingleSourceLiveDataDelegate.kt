package com.etrade.mobilepro.livedata

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer

class SingleSourceLiveDataDelegate<T>(initialSource: LiveData<T>) {

    private var lastAddedSource: LiveData<T> = initialSource
    private val _value = MediatorLiveData<T>()

    private val valueObserver = Observer<T> { text ->
        _value.value = text
    }

    val value: LiveData<T>
        get() = _value

    init {
        _value.addSource(initialSource, valueObserver)
    }

    fun updateSource(textSource: LiveData<T>) {
        _value.removeSource(lastAddedSource)
        _value.addSource(textSource, valueObserver)
        lastAddedSource = textSource
    }
}
