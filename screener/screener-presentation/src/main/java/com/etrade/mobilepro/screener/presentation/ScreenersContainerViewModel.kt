package com.etrade.mobilepro.screener.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.tracking.SCREEN_TITLE_ETF_SCREENER
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MUTUAL_FUND_SCREENER
import com.etrade.mobilepro.tracking.SCREEN_TITLE_STOCK_SCREENER
import com.etrade.mobilepro.tracking.TabsHostScreenViewModel
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelper
import com.etrade.mobilepro.user.session.manager.SessionState
import com.etrade.mobilepro.user.session.manager.SessionStateChangeListener
import com.etrade.mobilepro.util.AuthenticationStatusProvider
import javax.inject.Inject

class ScreenersContainerViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    override val viewPagerTabsTrackerHelper: ViewPagerTabsTrackerHelper,
    private val sessionState: SessionState,
    authenticationStatusProvider: AuthenticationStatusProvider
) : ViewModel(), TabsHostScreenViewModel, SessionStateChangeListener {

    override val tabNames: Array<String> = arrayOf(SCREEN_TITLE_STOCK_SCREENER, SCREEN_TITLE_MUTUAL_FUND_SCREENER, SCREEN_TITLE_ETF_SCREENER)

    private val _viewState = MutableLiveData<ViewState>()
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    init {
        aggregator.init()
        _viewState.value = ViewState(isAuthenticated = authenticationStatusProvider.isAuthenticated())
        sessionState.addListener(this)
    }

    override fun onCleared() {
        sessionState.removeListener(this)

        super.onCleared()
    }

    override fun onSessionStateChange(state: SessionStateChange) {
        _viewState.value = ViewState(isAuthenticated = state == SessionStateChange.AUTHENTICATED)
    }

    internal data class ViewState(
        val isAuthenticated: Boolean
    )
}
