package com.etrade.mobilepro.util.delegate

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SuppressedInitialSuite {

    @Test
    fun `check null initial value set with no subsequent calls`() {
        checkCalledTimes(0, null)
    }

    @Test
    fun `check non-null initial value set with no subsequent calls`() {
        checkCalledTimes(0, "test")
    }

    @Test
    fun `check callback called after null initial value reset`() {
        val initial = null
        checkCalledTimes(1, initial, Array<String?>(1) { initial }.toList())
    }

    @Test
    fun `check callback called after non-null initial value reset`() {
        val initial = "test"
        checkCalledTimes(1, initial, Array<String?>(1) { initial }.toList())
    }

    @Test
    fun `check callback called after null initial value reset multiple times`() {
        val initial = null
        checkCalledTimes(1, initial, Array<String?>(5) { initial }.toList())
    }

    @Test
    fun `check callback called after non-null initial value reset multiple times`() {
        val initial = "test"
        checkCalledTimes(1, initial, Array<String?>(5) { initial }.toList())
    }

    @Test
    fun `check callback called after null initial value changed with arbitrary values`() {
        val initial = null
        checkCalledTimes(5, initial, listOf("abc", null, "test", null, "xyz"))
    }

    @Test
    fun `check callback called after non-null initial value changed with arbitrary values`() {
        val initial = "test"
        checkCalledTimes(5, initial, listOf("abc", null, "test", null, "xyz"))
    }

    private fun checkCalledTimes(expected: Int, initial: String?, values: List<String?> = emptyList()) {
        var actual = 0
        val foo = Foo(initial) { actual++ }
        values.forEach { foo.bar = it }
        assertEquals(expected, actual)
    }

    private class Foo(initialValue: String?, expected: () -> Unit) {
        var bar: String? by suppressInitial(initialValue, expected)
    }
}
