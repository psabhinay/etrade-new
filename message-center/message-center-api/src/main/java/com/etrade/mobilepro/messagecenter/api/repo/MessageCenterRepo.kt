package com.etrade.mobilepro.messagecenter.api.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.messagecenter.api.model.AccountHolderRequest
import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.api.model.SendNewMessageRequest
import com.etrade.mobilepro.messagecenter.api.model.ServiceMessage
import com.etrade.mobilepro.messagecenter.api.model.SupportMessage

interface MessageCenterRepo {
    suspend fun getMessageList(): ETResult<List<Message>>
    suspend fun getServiceMessageDetails(message: Message): ETResult<ServiceMessage>
    suspend fun getSupportMessageDetails(message: Message): ETResult<SupportMessage>
    suspend fun sendReply(caseNo: String, msgId: String, content: String): ETResult<List<SupportMessage.Communication>>
    suspend fun getAccountHoldersFirstNameLastName(request: AccountHolderRequest): ETResult<String>
    suspend fun sendNewMessage(request: SendNewMessageRequest): ETResult<Unit>
}
