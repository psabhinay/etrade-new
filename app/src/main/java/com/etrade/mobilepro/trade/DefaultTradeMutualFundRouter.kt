package com.etrade.mobilepro.trade

import androidx.navigation.NavController
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.QuoteGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.quoteapi.FundsTradeQuoteItem
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesResult
import com.etrade.mobilepro.quotes.MutualFundViewType
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.disclosures.MutualFundTradeDisclosureConditions
import com.etrade.mobilepro.trade.mutualfund.presentation.MutualFundTradeType
import com.etrade.mobilepro.trade.presentation.TradeFragmentDirections
import com.etrade.mobilepro.trade.router.TradeMutualFundRouter
import javax.inject.Inject

private const val PROSPECTUS_URL = "https://doc.morningstar.com/DocDetail.aspx?clientid=etrade&key=2fc038529bc433a6&investmenttype=1&doctype=prospectus&ticker="

class DefaultTradeMutualFundRouter @Inject constructor() : TradeMutualFundRouter {

    override fun openDisclosures(navController: NavController, data: MutualFundsTradeQuotesResult?) {
        val conditions: MutualFundTradeDisclosureConditions? = data?.let {
            if (data is FundsTradeQuoteItem) {
                MutualFundTradeDisclosureConditions(
                    isFrontEndOrSalesFeeFund = data.isFrontEndOrSalesFeeFund,
                    isMoneyMarketFund = data.instrumentType == InstrumentType.MMF,
                    isNoLoadNoTransactionFeeFund = data.isNoLoadNoTransactionFeeFund,
                    hasEarlyRedemptionFee = data.hasEarlyRedemptionFee
                )
            } else {
                null
            }
        }

        navController.navigate(TradeFragmentDirections.actionTradeFragmentToMfDisclosure(conditions))
    }

    override fun openProspectus(navController: NavController, ticker: String) {
        val url = PROSPECTUS_URL.plus(ticker)

        navController.navigate(TradeFragmentDirections.actionTradeFragmentToMfProspectus(url))
    }

    override fun openFundsPerformancePage(navController: NavController, symbol: SearchResultItem.Symbol) {
        when (navController.graph.id) {
            R.id.mainGraph -> navController.navigate(
                MainGraphDirections.actionToLaunchMutualFunds(
                    symbol,
                    MutualFundViewType.PERFORMANCE
                )
            )
            R.id.quoteGraph -> navController.navigate(QuoteGraphDirections.actionLaunchQuote(symbol, MutualFundViewType.PERFORMANCE))
            else -> { /*Do nothing*/
            }
        }
    }

    override fun openMutualFundSell(navController: NavController, accountId: String) {
        navController.navigate(TradeFragmentDirections.actionTradeFragmentToMutualFundTradeFragment(accountId, MutualFundTradeType.SELL))
    }

    override fun openMutualFundExchangeSell(navController: NavController, accountId: String) {
        navController.navigate(TradeFragmentDirections.actionTradeFragmentToMutualFundTradeFragment(accountId, MutualFundTradeType.EXCHANGE_SELL))
    }

    override fun openMutualFundExchangeBuy(navController: NavController, accountId: String, symbol: WithSymbolInfo) {
        navController.navigate(
            TradeFragmentDirections.actionTradeFragmentToMutualFundTradeFragment(
                accountId,
                MutualFundTradeType.EXCHANGE_BUY,
                symbol.symbol,
                symbol.instrumentType.typeCode
            )
        )
    }
}
