package com.etrade.mobilepro.login.data.restclient.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false, name = "LogoutResponse")
internal data class LogoutResponse(
    @field:Element(name = "Result")
    var result: SessionResult? = null
)
