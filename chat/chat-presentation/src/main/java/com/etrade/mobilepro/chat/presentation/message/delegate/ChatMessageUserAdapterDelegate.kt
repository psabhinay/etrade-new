package com.etrade.mobilepro.chat.presentation.message.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.presentation.databinding.ChatItemMessageUserBinding
import com.etrade.mobilepro.chat.presentation.message.viewHolder.ChatMessageUserViewHolder
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class ChatMessageUserAdapterDelegate : AbsListItemAdapterDelegate<ChatMessage.User, ChatMessage, ChatMessageUserViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup) =
        ChatMessageUserViewHolder(parent.viewBinding(ChatItemMessageUserBinding::inflate))

    override fun isForViewType(item: ChatMessage, items: MutableList<ChatMessage>, position: Int) = item is ChatMessage.User

    override fun onBindViewHolder(item: ChatMessage.User, holder: ChatMessageUserViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }
}
