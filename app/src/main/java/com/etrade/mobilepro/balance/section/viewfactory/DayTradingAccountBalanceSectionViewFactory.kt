package com.etrade.mobilepro.balance.section.viewfactory

import com.etrade.mobilepro.balance.data.dto.BalancesDetailsDto
import com.etrade.mobilepro.balance.section.BalancesActionType
import com.etrade.mobilepro.balance.section.BalancesLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.HeaderSectionItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.onValid

class DayTradingAccountBalanceSectionViewFactory : BalancesSectionViewFactory {

    @SuppressWarnings("LongMethod")
    override fun createSectionView(
        detailsDto: BalancesDetailsDto,
        resources: BalancesLabelResources,
        actionClickListener: (BalancesActionType) -> Unit
    ): List<TableSectionItemLayout> {
        val sections = mutableListOf<TableSectionItemLayout>()

        sections.addAll(getMarginBalanceOverView(resources, detailsDto.netAccountVal, detailsDto.mktSecVal))

        // Cash Purchasing power
        sections.addAll(getCashPurchasingPower(resources, detailsDto))

        // Margin Call section
        sections.addAll(getMarginCallSections(detailsDto, resources, actionClickListener))

        // securities
        sections.addAll(getMarginSecuritiesSection(resources, detailsDto.marketSecLongVal, detailsDto.mktSecshortVal))

        // Purchasing Power
        sections.addAll(
            getMarginPurchasingPowerSection(
                resources,
                detailsDto.marginSecVal,
                detailsDto.cashsecVal,
                detailsDto.margineqVal,
                detailsDto.marginEqPer
            )
        )

        // Day Trade Section
        sections.add(HeaderSectionItem(resources.header.dayTradingPurchasingPower))
        sections.add(createSummarySection(resources.margin.marginableSecurities, formatBalancesValue(detailsDto.dtMarginSecVal, resources.defaultResources)))
        sections.add(createSummarySection(resources.margin.nonMarginableSecuritiesOrOptions, formatBalancesValue(detailsDto.dtCashSecVal, resources.defaultResources)))
        sections.add(createSummarySection(resources.margin.dayTradeStatus, formatBalancesValue(detailsDto.dtStatus, resources.defaultResources, addPrefixDollar = false)))

        // Funds withheld for open orders
        sections.addAll(getFundsWithHeldSection(resources, detailsDto.withheldCashVal, detailsDto.withheldMarginVal))

        // Funds withheld - day trading
        onValid(detailsDto.dtWithheldCashVal) {
            sections.add(
                createSummarySection(
                    resources.margin.dayTradeFundsWithheldCash,
                    formatBalancesValue(detailsDto.dtWithheldCashVal, resources.defaultResources)
                )
            )
        }
        onValid(detailsDto.dtwithheldMarginVal) {
            sections.add(
                createSummarySection(
                    resources.margin.dayTradeFundsWithheldMargin,
                    formatBalancesValue(detailsDto.dtwithheldMarginVal, resources.defaultResources)
                )
            )
        }

        // Withdrawal
        sections.addAll(getMarginWithdrawalSection(resources, detailsDto.withdrawCashVal, detailsDto.withdrawMarginVal))

        // Cash Margin Balance
        sections.add(HeaderSectionItem(resources.header.cashMarginBalance))

        // check for holding - "H"
        sections.addAll(getSweepHoldingSection(detailsDto, resources))

        sections.add(createSummarySection(resources.margin.balance, formatBalancesValue(detailsDto.marginbalVal, resources.defaultResources)))
        sections.add(createSummarySection(resources.margin.netMarginBalance, formatBalancesValue(detailsDto.netCashmarbal, resources.defaultResources)))
        // Short Reserve
        onValid(detailsDto.shortresVal) {
            sections.add(createSummarySection(resources.margin.shortReserve, formatBalancesValue(detailsDto.shortresVal, resources.defaultResources)))
        }

        // Funds on Hold
        sections.addAll(getFundsOnHoldSection(resources, detailsDto.fundswhpwrVal, detailsDto.fundsWhWithdrawVal))
        return sections
    }

    private fun getCashPurchasingPower(
        resources: BalancesLabelResources,
        detailsDto: BalancesDetailsDto
    ): List<TableSectionItemLayout> {
        val sections = mutableListOf<TableSectionItemLayout>()
        sections.add(HeaderSectionItem(resources.header.purchasingPower))
        sections.add(createSummarySection(resources.margin.marginableSecurities, formatBalancesValue(detailsDto.marginSecVal, resources.defaultResources)))
        sections.add(createSummarySection(resources.margin.nonMarginableSecuritiesOrOptions, formatBalancesValue(detailsDto.cashsecVal, resources.defaultResources)))
        sections.add(createSummarySection(resources.margin.dayTradingPurchasingPower, formatBalancesValue(detailsDto.dtMarginSecVal, resources.defaultResources)))
        sections.add(createSummarySection(resources.margin.maxAvailableWithdrawal, formatBalancesValue(detailsDto.totalWithdrawCashval, resources.defaultResources)))
        sections.add(createSummarySection(resources.margin.netCashMarginbalance, formatBalancesValue(detailsDto.netCashmarbal, resources.defaultResources)))
        return sections
    }
}
