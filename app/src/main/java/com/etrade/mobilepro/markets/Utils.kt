package com.etrade.mobilepro.markets

import android.content.res.Resources
import com.etrade.mobilepro.R
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell

internal fun Resources.tableHeaders(): List<Cell> {
    val headers = getStringArray(R.array.markets_table_headers)
    val cellHeaderId1 = "1"
    val cellHeaderId2 = "2"
    val cellHeaderId3 = "3"
    return listOf(
        Cell(cellHeaderId1, headers[0]),
        Cell(cellHeaderId2, headers[1]),
        Cell(cellHeaderId3, headers[2])
    )
}
