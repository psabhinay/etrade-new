package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.barcode.api.BarcodeHistoryRepo
import com.etrade.mobilepro.barcode.data.BarcodeApiService
import com.etrade.mobilepro.barcode.data.DefaultBarcodeHistoryRepo
import com.etrade.mobilepro.common.di.MobileTrade
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class BarcodeModule {

    @Provides
    @Singleton
    internal fun provideBarcodeApiClient(@MobileTrade retrofit: Retrofit): BarcodeApiService = retrofit.create(BarcodeApiService::class.java)

    @Provides
    @Singleton
    internal fun provideBarcodeHistoryRepo(): BarcodeHistoryRepo = DefaultBarcodeHistoryRepo()
}
