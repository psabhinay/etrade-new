package com.etrade.mobilepro.screener.presentation.stock

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.datasource.ScreenerSetsDataSource
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Stock
import com.etrade.mobilepro.screener.presentation.base.BaseScreenersViewModel
import com.etrade.mobilepro.tracking.Tracker
import javax.inject.Inject

class StockScreenersViewModel @Inject constructor(
    screenerSetsDataSource: ScreenerSetsDataSource,
    @Stock screenerInteractor: ScreenerInteractor,
    tracker: Tracker
) : BaseScreenersViewModel(
    screenerGroup = ScreenerType.Group.STOCK,
    screenerSetsDataSource = screenerSetsDataSource,
    screenerInteractor = screenerInteractor,
    tracker = tracker
)
