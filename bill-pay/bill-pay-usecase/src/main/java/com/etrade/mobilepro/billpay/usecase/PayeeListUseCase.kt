package com.etrade.mobilepro.billpay.usecase

import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.SessionId
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.util.UseCase
import java.util.Locale
import javax.inject.Inject

interface PayeeListUseCase : UseCase<SessionId, ETResult<List<Payee>>>

class PayeeListUseCaseImpl @Inject constructor(
    private val billPayRepo: BillPayRepo
) : PayeeListUseCase {
    override suspend fun execute(parameter: SessionId): ETResult<List<Payee>> = billPayRepo.getPayeesList(parameter.value).map {
        unsortedList ->
        unsortedList.sortedBy { it.name.lowercase(Locale.ROOT) }
    }
}
