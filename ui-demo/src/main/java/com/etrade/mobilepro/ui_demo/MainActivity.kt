package com.etrade.mobilepro.ui_demo

import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.ui_demo.databinding.ActivityMainBinding
import com.etrade.mobilepro.ui_demo.databinding.AdapterItemBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.localytics.androidx.Localytics
import java.io.File

class MainActivity : BaseDemoActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)
        val toolbar: Toolbar = binding.toolbar
        setSupportActionBar(toolbar)

        with(binding.list) {
            setupRecyclerViewDivider(R.drawable.list_item_divider)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = Adapter(getActivityList())
        }
    }

    private fun getActivityList(): List<ActivityInfo> {
        val pm = this.packageManager

        val info = pm.getPackageInfo(applicationContext.packageName, PackageManager.GET_ACTIVITIES)

        return info.activities.filterNot { it.name == this.javaClass.name }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.action_onboarding -> {
                val localFile = File("/android_asset/onboarding/index.html")
                Localytics.forceInAppMessage(localFile)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Localytics.onNewIntent(this, intent)
        val data: Uri? = intent?.data
        Toast.makeText(this, data.toString(), Toast.LENGTH_LONG).show()
    }
}

private class Adapter(val items: List<ActivityInfo>) : RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.viewBinding(AdapterItemBinding::inflate))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }
}

private class ViewHolder(private val binding: AdapterItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(info: ActivityInfo) {
        with(binding.title) {
            text =
                info.labelRes.takeIf { it != 0 }?.let { resources.getString(it) } ?: info.name

            setOnClickListener {
                context.startActivity(Intent(context, Class.forName(info.name)))
            }
        }
    }
}
