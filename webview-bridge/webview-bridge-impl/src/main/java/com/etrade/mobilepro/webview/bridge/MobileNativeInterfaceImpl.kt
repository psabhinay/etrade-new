package com.etrade.mobilepro.webview.bridge

import android.webkit.JavascriptInterface
import org.json.JSONObject
import org.slf4j.LoggerFactory

const val DEFAULT_INTERFACE_NAME = "MobileNativeInterface"

/**
 * This class represents the interface between E*TRADE website pages that use the "MobileNativeInterface" to initiate actions such as
 * opening a quote page, or navigating to a native application view.
 * The interface should be registered with the web view using the [DEFAULT_INTERFACE_NAME] constant unless a web page requires otherwise.
 */
@Suppress("unused") // These functions will be called from a WebKit web view initialized with this Javascript Interface
class MobileNativeInterfaceImpl(
    private val openQuoteHandler: OpenQuoteHandler? = null,
    private val openUrlHandler: OpenUrlHandler? = null
) {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    @JavascriptInterface
    fun openNativeQuote(json: String) {
        logger.debug("Received request to open quote: $json")
        val obj = JSONObject(json)
        openQuoteHandler?.openQuote(symbol = obj.getString("symbol"))
    }

    @JavascriptInterface
    fun openURL(json: String) {
        logger.debug("Received request to open url: $json")
        val obj = JSONObject(json)
        openUrlHandler?.openUrl(
            url = obj.getString("url"),
            isPdf = obj.getBoolean("ispdf")
        )
    }
}
