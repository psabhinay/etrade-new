package com.etrade.mobilepro.uiwidgets.tablesectionsview.model

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.ColorInt
import com.etrade.mobilepro.uiwidgets.tablesectionsview.databinding.ItemSectionRowBinding
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.etrade.mobilepro.util.formatCurrencyAmount
import com.etrade.mobilepro.util.formatDash

open class SummarySectionItem(
    val label: String,
    val value: String,
    @ColorInt val textColor: Int? = null,
    private val labelDescription: String? = null,
    private val valueDescription: String? = null
) : TableSectionItemLayout {

    override fun getRowView(linearParams: LinearLayout.LayoutParams, parent: ViewGroup): View =
        parent.viewBinding(ItemSectionRowBinding::inflate).apply {
            root.layoutParams = linearParams
            tvLhs.text = label
            tvRhs.text = value
            tvLhs.contentDescription = labelDescription?.formatDash() ?: label.formatDash()
            tvRhs.contentDescription = root.context.accountNameContentDescription(valueDescription ?: value).formatCurrencyAmount()
            textColor?.let { color ->
                // text Color applied to the values only
                tvRhs.setTextColor(color)
            }
        }.root
}

fun createSummarySection(
    label: String,
    value: String,
    @ColorInt color: Int? = null,
    labelDescription: String? = null,
    valueDescription: String? = null
): TableSectionItemLayout {
    return SummarySectionItem(label, value, color, labelDescription, valueDescription)
}
