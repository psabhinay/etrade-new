package com.etrade.mobilepro.messagecenter.presentation.messagelist.router

import androidx.navigation.NavDirections
import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.presentation.common.parcelize
import com.etrade.mobilepro.messagecenter.presentation.messagelist.MessageCenterListFragmentDirections
import org.slf4j.LoggerFactory
import javax.inject.Inject

interface MessageDetailsRouter {
    fun getMessageDetailRoute(message: Message): NavDirections
}

private const val SUPPORT_MESSAGE_CASE_TYPE_1 = "-1001"

private val serviceMessageTypes = setOf(
    "2009", // check book reorder
    "9031", // mobile deposit request
    "9006", // correspondence
    "3000", // asset transfer request
    "1067", // new account request
    "2047", // deposit research
    "2001", // atm services
    "1006", // wire request
    "9026", // bill pay add funding
    "9025", // bill pay inactivate account,
    "1030", // IRA/ESA/QRP distribution
    "9008", // digital security
    "9022", // bill pay enrollment
    "9036", // stock plan activation
    "1003", // check request
    "1028", // change uninvested cash option
    "1061", // client reporting cert request
    "1053" // option upgrade
)
class DefaultMessageDetailsRouter @Inject constructor() : MessageDetailsRouter {

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    override fun getMessageDetailRoute(message: Message): NavDirections =
        when (message.caseTypeId) {
            in serviceMessageTypes -> MessageCenterListFragmentDirections.actionMessageCenterListToMessageServiceDetails(
                message = message.parcelize()
            )
            SUPPORT_MESSAGE_CASE_TYPE_1 -> MessageCenterListFragmentDirections.actionMessageCenterListToMessageSupportDetails(
                message = message.parcelize()
            )
            else -> {
                logger.error("Unknown case type id: ${message.caseTypeId} in message $message\n Can't find details route.")
                MessageCenterListFragmentDirections.actionMessageCenterListToMessageServiceDetails(
                    message = message.parcelize()
                )
            }
        }
}
