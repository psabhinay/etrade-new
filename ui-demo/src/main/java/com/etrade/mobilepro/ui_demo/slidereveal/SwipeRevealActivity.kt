package com.etrade.mobilepro.ui_demo.slidereveal

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.ui_demo.databinding.ActivitySwipeRevealBinding
import com.etrade.mobilepro.util.android.binding.viewBinding

class SwipeRevealActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivitySwipeRevealBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        with(binding) {
            deleteButton.setOnClickListener {
                log.appendNewLineText("Delete!")
            }
            printPropertiesButton.setOnClickListener {
                log.appendNewLineText("Delete btn is: ${deleteButton.visibilityDesc()}")
            }
            swipeReveal.actionListener = {
                log.appendNewLineText(it.toString())
            }
        }
    }
}

private fun TextView.appendNewLineText(text: String?) {
    this.append("\n$text")
}

private fun View.visibilityDesc(): String = when (this.visibility) {
    View.VISIBLE -> "Visible"
    View.INVISIBLE -> "Invisible"
    View.GONE -> "Gone"
    else -> "Unknown"
}
