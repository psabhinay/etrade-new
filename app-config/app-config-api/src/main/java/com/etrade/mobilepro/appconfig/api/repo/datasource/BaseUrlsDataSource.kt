package com.etrade.mobilepro.appconfig.api.repo.datasource

import com.etrade.mobilepro.appconfig.api.AppEnvironment
import com.etrade.mobilepro.appconfig.api.BaseUrls

interface BaseUrlsDataSource {
    fun getBaseUrls(environment: AppEnvironment): BaseUrls
    fun saveBaseUrls(baseUrls: BaseUrls, environment: AppEnvironment)
}
