package com.etrade.mobilepro.screener.presentation.mutualfund.result

import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.MutualFund
import com.etrade.mobilepro.screener.data.result.ResultColumnTitleProvider
import com.etrade.mobilepro.screener.data.result.ResultColumnsProvider
import com.etrade.mobilepro.screener.data.result.ResultRowDataProvider
import com.etrade.mobilepro.screener.presentation.base.BaseScreenerResultViewModel
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import javax.inject.Inject

class MutualFundResultsViewModel @Inject constructor(
    @MutualFund interactor: ScreenerInteractor,
    columnsProvider: ResultColumnsProvider,
    columnTitleProvider: ResultColumnTitleProvider,
    rowDataProvider: ResultRowDataProvider,
    quoteDetailItemsMapper: FundQuoteDetailItemsMapper,
    streamingController: StreamingSubscriptionController,
    streamingStatusController: StreamingStatusController
) : BaseScreenerResultViewModel(
    interactor,
    columnsProvider,
    columnTitleProvider,
    rowDataProvider,
    quoteDetailItemsMapper,
    streamingController,
    streamingStatusController
)
