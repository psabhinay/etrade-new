package com.etrade.mobilepro.preferences

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobile.accounts.defaultaccount.WithAccountOptions

/*
FP related preferences, will probably move to something like fingerprint preferences
private const val ENCRYPTED_USERNAME = "encryptedUsername"
private const val ENCRYPTED_PASSWORD = "encryptedPassword"
private const val ET_LOGIN_RSA_USER="U_PWD_RSA"
private const val FP_FAIL_COUNT = "fpFailCount"
private const val ET_LOGIN_WITH_FP="loginWithFP"
private const val FP_DISCLAIMER_DIALOG_SHOWN = "fidoDisclaimerDialog"*/

private const val TIME_OUT_KEY = "timeout"

private const val DEFAULT_ACCOUNT_USER_ID = "defaultAccountUserId"

private const val RECENT_WATCHLIST_ID = "recentWatchListId"
private const val RECENT_WATCHLIST_NAME = "recentWatchListName"

private const val IS_SWEEP_ACTIVITY_ENABLED = "isSweepActivityEnabled"
private const val THIRTY_MINUTES_IN_SECONDS = 1800

private const val DEFAULT_LANDING_PAGE_ID = "default_landing_page_ID"
private const val DEFAULT_LANDING_PAGE_UUID = "default_landing_page_UUID"
private const val DEFAULT_LANDING_PAGE_ACCOUNT_ID = "default_landing_page_ACCOUNT_ID"

private const val PREF_KEY_ACCOUNTS_ORDER_STATUS = "PREF_KEY_ACCOUNTS_ORDER_STATUS"
private const val ACCOUNTS_SEPARATOR = ","
private const val STATUS_SEPARATOR = "::"
private const val UUID_POSITION = 0
private const val STATUS_POSITION = 1

private const val HAS_ADDED_TO_WATCHLIST = "hasAddedToWatchlist"

private const val PF_MOVERS_WIDGET_ACCOUNT_UUID = "pf_movers_widget_account_id"
private const val PF_NEWS_WIDGET_ACCOUNT_UUID = "pf_news_widget_account_id"
private const val KEY_WATCHLIST_TABLEVIEW_SELECTED_INDEX = "watchlistTableViewSelectedIndex"

@Suppress("TooManyFunctions")
class UserPreferences(private val keyValueStorage: KeyValueStorage) {

    var isSweepActivityEnabled: Boolean
        get() {
            return keyValueStorage.getBooleanValue(IS_SWEEP_ACTIVITY_ENABLED, false)
        }
        set(value) {
            keyValueStorage.putBooleanValue(IS_SWEEP_ACTIVITY_ENABLED, value)
        }

    var watchlistTableViewSelectedIndex: Int by keyValueStorage.preference(KEY_WATCHLIST_TABLEVIEW_SELECTED_INDEX, 0)

    fun getTimeOutInSeconds(): Int {
        return keyValueStorage.getIntValue(TIME_OUT_KEY, THIRTY_MINUTES_IN_SECONDS)
    }

    fun setTimeOutInSeconds(value: Int) {
        keyValueStorage.putIntValue(TIME_OUT_KEY, value)
    }

    fun getSavedUserName(): String? {
        return keyValueStorage.getStringValue(DEFAULT_ACCOUNT_USER_ID, "")
    }

    fun setSavedUserName(userName: String) {
        keyValueStorage.putStringValue(DEFAULT_ACCOUNT_USER_ID, userName)
    }

    /**
     * Get the most recent watchlist if available.
     *
     * @return A [LiveData] containing the id and name of the watchlist.
     */
    val recentWatchlistData: LiveData<Pair<String?, String?>>
        get() = _recentWatchlistData
    private val _recentWatchlistData = MutableLiveData(getRecentWatchlist())

    /**
     * Get the most recent watchlist if available.
     *
     * @return A [Pair] containing the id and name of the watchlist.
     */
    fun getRecentWatchlist(defaultId: String? = null, defaultName: String? = null): Pair<String?, String?> {
        val id = keyValueStorage.getStringValue(RECENT_WATCHLIST_ID, null) ?: defaultId
        val name = keyValueStorage.getStringValue(RECENT_WATCHLIST_NAME, null) ?: defaultName
        return Pair(id, name)
    }

    fun setRecentWatchlist(id: String, name: String?) {
        keyValueStorage.putStringValue(RECENT_WATCHLIST_ID, id)
        keyValueStorage.putStringValue(RECENT_WATCHLIST_NAME, name)
        _recentWatchlistData.value = Pair(id, name)
    }

    fun getDefaultLandingPage(): LandingPageParams = LandingPageParams(
        pageId = keyValueStorage.getStringValue(DEFAULT_LANDING_PAGE_ID),
        pageUuid = keyValueStorage.getStringValue(DEFAULT_LANDING_PAGE_UUID),
        accountId = keyValueStorage.getStringValue(DEFAULT_LANDING_PAGE_ACCOUNT_ID)
    )

    fun setDefaultLandingPage(landingParams: LandingPageParams) {
        keyValueStorage.putStringValue(DEFAULT_LANDING_PAGE_ID, landingParams.pageId)
        keyValueStorage.putStringValue(DEFAULT_LANDING_PAGE_UUID, landingParams.pageUuid)
        keyValueStorage.putStringValue(DEFAULT_LANDING_PAGE_ACCOUNT_ID, landingParams.accountId)
    }

    fun getAccountsOptions(): List<WithAccountOptions> {
        return keyValueStorage.getStringValue(PREF_KEY_ACCOUNTS_ORDER_STATUS, null)
            ?.split(ACCOUNTS_SEPARATOR)
            ?.map {
                val split = it.split(STATUS_SEPARATOR)
                split[UUID_POSITION] to (split[STATUS_POSITION] == "true")
            }.orEmpty()
            .map { (uuid, status) ->
                object : WithAccountOptions {
                    override val accountUuid: String
                        get() = uuid
                    override val isEnabled: Boolean
                        get() = status
                }
            }
    }

    fun setAccountsOptions(value: List<WithAccountOptions>) {
        val list = if (value.isEmpty()) null else value.map {
            it.accountUuid to it.isEnabled
        }
        keyValueStorage.putStringValue(
            PREF_KEY_ACCOUNTS_ORDER_STATUS,
            list?.joinToString(ACCOUNTS_SEPARATOR) { (uuid, state) ->
                uuid.plus("$STATUS_SEPARATOR$state")
            }
        )
    }

    fun getHasAddedToWatchlist(): Boolean {
        return keyValueStorage.getBooleanValue(HAS_ADDED_TO_WATCHLIST, false)
    }

    fun setHasAddedToWatchlist(value: Boolean) {
        keyValueStorage.putBooleanValue(HAS_ADDED_TO_WATCHLIST, value)
    }

    var pfMoversWidgetAccountUuid: String?
        get() {
            return keyValueStorage.getStringValue(PF_MOVERS_WIDGET_ACCOUNT_UUID, null)
        }
        set(value) {
            keyValueStorage.putStringValue(PF_MOVERS_WIDGET_ACCOUNT_UUID, value)
        }

    var pfNewsWidgetAccountUuid: String?
        get() {
            return keyValueStorage.getStringValue(PF_NEWS_WIDGET_ACCOUNT_UUID, null)
        }
        set(value) {
            keyValueStorage.putStringValue(PF_NEWS_WIDGET_ACCOUNT_UUID, value)
        }
}
