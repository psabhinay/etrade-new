@file:Suppress("TooManyFunctions")

package com.etrade.mobilepro.ui_demo.dagger

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dagger.factory.ViewModelInjectionFactory
import com.etrade.mobilepro.dagger.factory.ViewModelInjectionTracker
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.feature.presentation.viewmodel.FeatureFlagsViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelInjectionFactory): ViewModelProvider.Factory

    /* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP ViewModel bindings IN THIS FILE ORGANIZED ALPHABETICALLY */

    @ExperimentalCoroutinesApi
    @Binds
    @IntoMap
    @ViewModelKey(FeatureFlagsViewModel::class)
    abstract fun bindFeatureFlagViewModel(viewModel: FeatureFlagsViewModel): ViewModel

    companion object {
        @Singleton
        @Provides
        fun provideViewModelInjectionTracker(): ViewModelInjectionTracker =
            object : ViewModelInjectionTracker {
                override fun trackScreenIfApplicable(viewModel: Any) {}
            }
    }
}
