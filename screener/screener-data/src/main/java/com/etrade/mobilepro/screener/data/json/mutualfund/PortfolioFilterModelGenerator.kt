package com.etrade.mobilepro.screener.data.json.mutualfund

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.PortfolioFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val NET_ASSETS_FIELD = "TotalAssets"
private const val YIELD_FIELD = "DistributionYield"

class PortfolioFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val portfolioFilter = screenerFilter as PortfolioFilter
        val arguments = mutableListOf<Argument>()

        if (portfolioFilter.isNetAssetsSelected()) {
            arguments.add(
                Argument(getNetAssetsClauses(portfolioFilter), NET_ASSETS_FIELD)
            )
        }
        if (portfolioFilter.isYieldSelected()) {
            arguments.add(
                Argument(getYieldClauses(portfolioFilter), YIELD_FIELD)
            )
        }

        return PartialScreenerParams(arguments)
    }

    private fun getNetAssetsClauses(portfolioFilter: PortfolioFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (portfolioFilter.netAssetsLessThan100) {
            clauses.addAll(netAssetsLessThan100Clauses)
        }
        if (portfolioFilter.netAssets100to500) {
            clauses.addAll(netAssets100to500Clauses)
        }
        if (portfolioFilter.netAssets500to1) {
            clauses.addAll(netAssets500to1Clauses)
        }
        if (portfolioFilter.netAssets1to2) {
            clauses.addAll(netAssets1to2Clauses)
        }
        if (portfolioFilter.netAssetsGreaterThan2) {
            clauses.addAll(netAssetsGreaterThan2Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private fun getYieldClauses(portfolioFilter: PortfolioFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (portfolioFilter.distributionYieldLessThan3) {
            clauses.addAll(distributionYieldLessThan3Clauses)
        }
        if (portfolioFilter.distributionYield3to5) {
            clauses.addAll(distributionYield3to5Clauses)
        }
        if (portfolioFilter.distributionYieldGreaterThan5) {
            clauses.addAll(distributionYieldGreaterThan5Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private val netAssetsLessThan100Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("100000000"))
    )
    private val netAssets100to500Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("100000000")),
        Clause(OPERATOR_LESS_THAN, listOf("500000000"))
    )
    private val netAssets500to1Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("500000000")),
        Clause(OPERATOR_LESS_THAN, listOf("1000000000"))
    )
    private val netAssets1to2Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("1000000000")),
        Clause(OPERATOR_LESS_THAN, listOf("2000000000"))
    )
    private val netAssetsGreaterThan2Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("2000000000")),
        Clause(OPERATOR_LESS_THAN, listOf("100000000"))
    )

    private val distributionYieldLessThan3Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("3"))
    )
    private val distributionYield3to5Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("3")),
        Clause(OPERATOR_LESS_THAN, listOf("5"))
    )
    private val distributionYieldGreaterThan5Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("5")),
        Clause(OPERATOR_LESS_THAN, listOf("100"))
    )
}
