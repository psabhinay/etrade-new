package com.etrade.mobilepro.portfolio.lightstreamer

interface AccountPortfolioStreamIdFactory {

    fun getStreamIds(accountId: String): Set<String>
}
