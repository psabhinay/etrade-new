package com.etrade.mobilepro.trade.data.tradedataextension

import com.etrade.mobilepro.testutil.FileReader.Companion.getStringFromFile
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.DAY_TRADING_FLAG
import com.etrade.mobilepro.trade.data.DAY_TRADING_VER
import com.etrade.mobilepro.trade.data.createOptionTradeBody
import com.etrade.mobilepro.trade.data.form.TradeLegsCoder
import com.etrade.mobilepro.trade.data.form.input.DefaultOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.stock.TRUE
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.util.ValueCoder
import com.squareup.moshi.Moshi
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TradeDataExtensionsTest {

    private val inputId: OptionTradeFormInputId = DefaultOptionTradeFormInputId
    private val moshi = Moshi.Builder().build()
    private val coder: ValueCoder<List<TradeLeg>> = TradeLegsCoder(moshi)
    private val isTablet = false

    @Test
    fun `test createOptionTradeBody with securityType = option and priceType = stopLimitOnQuote`() {
        val tradeLegs = getStringFromFile("option-single-trade-leg.json", TradeDataExtensionsTest::class.java)
        val inputMap: Map<String, String?> = mapOf(
            inputId.accountId to "83301187",
            inputId.securityType to "option",
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.allOrNone to null,
            inputId.priceType to "4",
            inputId.term to "3",
            inputId.strategy to "2",
            inputId.limitPriceForStopLimit to "3",
            inputId.stopPriceForStopLimit to "2",
            inputId.tradeLegs to tradeLegs,
            DAY_TRADING_FLAG to TRUE,
            DAY_TRADING_VER to "1"
        )

        val body = inputMap.createOptionTradeBody(coder, DefaultOptionTradeFormInputId.securityTypeValueId, moshi, isTablet)
        val expectedBody = getStringFromFile("option-request-body-stop-limit-on-quote.json", TradeDataExtensionsTest::class.java)
        assertEquals(expectedBody, body)
    }

    @Test
    fun `test createOptionTradeBody with securityType = option and priceType = netDebit and 2 legs`() {
        val tradeLegs = getStringFromFile("option-2-trade-legs.json", TradeDataExtensionsTest::class.java)
        val inputMap: Map<String, String?> = mapOf(
            inputId.accountId to "83301187",
            inputId.securityType to "option",
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.allOrNone to null,
            inputId.priceType to "6",
            inputId.term to "2",
            inputId.strategy to "9",
            inputId.limitPrice to "6.2",
            inputId.tradeLegs to tradeLegs
        )

        val body = inputMap.createOptionTradeBody(coder, DefaultOptionTradeFormInputId.securityTypeValueId, moshi, isTablet)
        val expectedBody = getStringFromFile("option-request-body-net-debit.json", TradeDataExtensionsTest::class.java)
        assertEquals(expectedBody, body)
    }

    @Test
    fun `test createOptionTradeBody with securityType = option and priceType = stopOnQuote`() {
        val tradeLegs = getStringFromFile("option-single-trade-leg.json", TradeDataExtensionsTest::class.java)
        val inputMap: Map<String, String?> = mapOf(
            inputId.accountId to "83301187",
            inputId.securityType to "option",
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.allOrNone to null,
            inputId.priceType to "3",
            inputId.term to "3",
            inputId.strategy to "2",
            inputId.stopPrice to "5",
            inputId.tradeLegs to tradeLegs
        )

        val body = inputMap.createOptionTradeBody(coder, DefaultOptionTradeFormInputId.securityTypeValueId, moshi, isTablet)
        val expectedBody = getStringFromFile("option-request-body-stop-on-quote.json", TradeDataExtensionsTest::class.java)
        assertEquals(expectedBody, body)
    }

    @Test
    fun `test createOptionTradeBody with securityType = option and priceType = trailingStopDollar`() {
        val tradeLegs = getStringFromFile("option-single-trade-leg.json", TradeDataExtensionsTest::class.java)
        val inputMap: Map<String, String?> = mapOf(
            inputId.accountId to "83301187",
            inputId.securityType to "option",
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.allOrNone to null,
            inputId.priceType to "19",
            inputId.term to "3",
            inputId.strategy to "2",
            inputId.stopPrice to "2",
            inputId.tradeLegs to tradeLegs
        )

        val body = inputMap.createOptionTradeBody(coder, DefaultOptionTradeFormInputId.securityTypeValueId, moshi, isTablet)
        val expectedBody = getStringFromFile("option-request-body-trailing-stop-dollar.json", TradeDataExtensionsTest::class.java)
        assertEquals(expectedBody, body)
    }

    @Test
    fun `test createOptionTradeBody with securityType = stockAndOptions and priceType = market and 2 legs`() {
        val tradeLegs = getStringFromFile("stock-and-option-2-trade-legs.json", TradeDataExtensionsTest::class.java)
        val inputMap: Map<String, String?> = mapOf(
            inputId.accountId to "83301187",
            inputId.securityType to "stockAndOptions",
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.allOrNone to null,
            inputId.priceType to "1",
            inputId.term to "3",
            inputId.strategy to "18",
            inputId.tradeLegs to tradeLegs
        )

        val body = inputMap.createOptionTradeBody(coder, DefaultOptionTradeFormInputId.securityTypeValueId, moshi, isTablet)
        val expectedBody = getStringFromFile("stocks-and-options-request-body-market.json", TradeDataExtensionsTest::class.java)
        assertEquals(expectedBody, body)
    }

    @Test
    fun `test createOptionTradeBody with securityType = stockAndOptions and priceType = netCredit and 3 legs`() {
        val tradeLegs = getStringFromFile("stock-and-option-3-trade-legs.json", TradeDataExtensionsTest::class.java)
        val inputMap: Map<String, String?> = mapOf(
            inputId.accountId to "83301187",
            inputId.securityType to "stockAndOptions",
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.allOrNone to null,
            inputId.priceType to "7",
            inputId.term to "3",
            inputId.strategy to "18",
            inputId.limitPrice to "5",
            inputId.tradeLegs to tradeLegs
        )

        val body = inputMap.createOptionTradeBody(coder, DefaultOptionTradeFormInputId.securityTypeValueId, moshi, isTablet)
        val expectedBody = getStringFromFile("stocks-and-options-request-body-net-credit.json", TradeDataExtensionsTest::class.java)
        assertEquals(expectedBody, body)
    }

    @Test
    fun `test createOptionTradeBody with securityType = option and priceType = stopLimitOnQuote and isTable`() {
        val tradeLegs = getStringFromFile("option-single-trade-leg.json", TradeDataExtensionsTest::class.java)
        val inputMap: Map<String, String?> = mapOf(
            inputId.accountId to "83301187",
            inputId.securityType to "option",
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.allOrNone to null,
            inputId.priceType to "4",
            inputId.term to "3",
            inputId.strategy to "2",
            inputId.limitPriceForStopLimit to "3",
            inputId.stopPriceForStopLimit to "2",
            inputId.tradeLegs to tradeLegs
        )

        val body = inputMap.createOptionTradeBody(coder, DefaultOptionTradeFormInputId.securityTypeValueId, moshi, isTablet = true)
        val expectedBody = getStringFromFile("option-request-body-stop-limit-on-quote-isTablet.json", TradeDataExtensionsTest::class.java)
        assertEquals(expectedBody, body)
    }
}
