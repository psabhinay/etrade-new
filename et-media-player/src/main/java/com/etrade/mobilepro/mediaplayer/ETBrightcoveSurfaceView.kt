package com.etrade.mobilepro.mediaplayer

import android.content.Context
import com.brightcove.player.view.BrightcoveSurfaceView

/**
 * This class is needed to fix a bug where the video zooms in on content loading stopped. This may be a bug within the Brightcove API.
 *
 * See: https://jira.corp.etradegrp.com/browse/ETAND-3125
 */
class ETBrightcoveSurfaceView(context: Context) : BrightcoveSurfaceView(context) {

    override fun setVideoSize(videoWidth: Int, videoHeight: Int) {
        if (videoWidth != 0 && videoHeight != 0) {
            super.setVideoSize(videoWidth, videoHeight)
        }
    }
}
