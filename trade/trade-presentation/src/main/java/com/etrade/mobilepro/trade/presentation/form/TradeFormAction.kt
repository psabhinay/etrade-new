package com.etrade.mobilepro.trade.presentation.form

import android.content.Intent
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesResult
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.tooltip.AppTooltip
import com.etrade.mobilepro.trade.api.AccountMode

sealed class TradeFormAction {
    class ApprovalLevel(val accountMode: AccountMode) : TradeFormAction()
    object Calculator : TradeFormAction()
    class ConditionalOrder(val inputId: String, val orderNumber: Int, val accountId: String?, val snapshot: InputValuesSnapshot?) : TradeFormAction()
    class Dialog(val dialog: AppDialog? = null) : TradeFormAction()
    object DisplayPriceTypeDescriptions : TradeFormAction()
    class ErrorMessage(val message: String) : TradeFormAction()
    class MutualFundExchangeBuyEntry(val accountId: String, val symbol: WithSymbolInfo) : TradeFormAction()
    class MutualFundExchangeSellEntry(val accountId: String) : TradeFormAction()
    class MutualFundDisclosure(val fundsData: MutualFundsTradeQuotesResult?) : TradeFormAction()
    class MutualFundProspectus(val symbol: String) : TradeFormAction()
    class MutualFundSellEntry(val accountId: String) : TradeFormAction()
    class OptionLegSelect(val newSearch: Boolean = true, val symbol: SearchResultItem.Symbol? = null) : TradeFormAction()
    object SaveOrder : TradeFormAction()
    class ShowFundsPerformance(val symbol: SearchResultItem.Symbol) : TradeFormAction()
    class StockExchangeKeys(val title: String) : TradeFormAction()
    object SubmitOrder : TradeFormAction()
    class SymbolLookup(val intent: Intent, val symbolInputId: String) : TradeFormAction()
    class Tooltip(val tooltip: AppTooltip) : TradeFormAction()
}
