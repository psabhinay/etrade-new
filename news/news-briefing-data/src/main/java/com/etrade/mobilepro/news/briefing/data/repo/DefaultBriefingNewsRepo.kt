package com.etrade.mobilepro.news.briefing.data.repo

import com.etrade.mobilepro.caching.ResponseModel
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.briefing.api.BriefingNewsRepo
import com.etrade.mobilepro.news.briefing.data.dto.BriefingNewsDto
import com.etrade.mobilepro.news.briefing.data.mapper.BriefingNewsMapper
import com.etrade.mobilepro.news.briefing.data.rest.BriefingNewsRequest
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.textutil.TextUtil
import io.reactivex.Observable
import org.simpleframework.xml.convert.AnnotationStrategy
import org.simpleframework.xml.core.Persister
import java.io.IOException
import javax.inject.Inject

class DefaultBriefingNewsRepo @Inject constructor(
    private val cachingDataDelegate: SimpleCachedResource.DataSourceDelegate<ResponseModel, BriefingNewsRequest>
) : BriefingNewsRepo {

    private val briefingNewsMapper = BriefingNewsMapper {
        TextUtil.convertTextWithHmlTagsToSpannable(it).toString()
    }

    override fun getBriefingNews(cacheExpiration: Long?): Observable<Resource<List<NewsItem>>> {
        return SimpleCachedResource(
            id = BriefingNewsRequest,
            dataSourceDelegate = cachingDataDelegate,
            controller = object : SimpleCachedResource.Controller<ResponseModel> {
                override fun shouldRefresh(itemFromCache: ResponseModel): Boolean {
                    return cacheExpiration?.let {
                        System.currentTimeMillis() - itemFromCache.retrievedTimestamp >= it
                    } ?: true
                }
            },
            showCachedData = true
        ).asObservable().map { cachedResponse ->
            try {
                cachedResponse.map { data ->
                    data?.response?.let {
                        parseBriefingNewsResponse(it)?.let { dto ->
                            briefingNewsMapper.map(dto).takeIf { items ->
                                items.isNotEmpty()
                            } ?: listOf(NewsItem.Empty)
                        }
                    }
                }
            } catch (e: IOException) {
                Resource.Failed<List<NewsItem>>(error = e)
            }
        }
    }

    private fun parseBriefingNewsResponse(xml: String): BriefingNewsDto? {
        val clazz = BriefingNewsDto::class.java
        val persister = Persister(AnnotationStrategy())
        return persister.read(clazz, xml, false)
    }
}
