package com.etrade.mobilepro.transfermoney.api.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.transfermoney.api.TransferCalendarHolidays

interface TransferCalendarRepo {

    suspend fun getCalendar(): ETResult<List<TransferCalendarHolidays>>
}
