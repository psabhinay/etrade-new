package com.etrade.mobilepro.uiwidgets.actionsheet

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.util.android.extension.addDividerItemDecoration
import com.etrade.mobilepro.util.android.extension.inflate
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

abstract class ActionItemViewHolder(val containerView: View) : RecyclerView.ViewHolder(containerView) {
    abstract fun bindTo(item: ActionItem)
}

sealed class ActionSheetStyle {
    open fun onFragmentCreate(bottomSheetDialogFragment: BottomSheetDialogFragment) {
        bottomSheetDialogFragment.setStyle(DialogFragment.STYLE_NO_FRAME, R.style.ActionSheetDialogFragment)
    }
    open fun setupRecyclerView(rv: RecyclerView) {
        rv.addItemDecoration(
            DividerItemDecoration(
                rv.context,
                DividerItemDecoration.VERTICAL
            ).apply {
                ContextCompat.getDrawable(rv.context, R.drawable.action_item_divider)?.also {
                    setDrawable(it)
                }
            }
        )
    }
    /**
     * This layout should contain a recycler view with id content_rv
     */
    open val layoutId: Int = R.layout.fragment_action_sheet_dialog

    abstract fun createViewHolder(parent: ViewGroup): ActionItemViewHolder
    abstract val name: String

    object DefaultBubbles : ActionSheetStyle() {
        override fun createViewHolder(parent: ViewGroup): ActionItemViewHolder {
            return object : ActionItemViewHolder(parent.inflate(R.layout.item_action)) {
                override fun bindTo(item: ActionItem) {
                    (itemView as TextView).apply {
                        setCompoundDrawablesWithIntrinsicBounds(item.iconId, 0, 0, 0)
                        text = context.getString(item.displayNameId, *item.displayNameArgs.toTypedArray())
                    }
                }
            }
        }
        override val name: String = "default"
    }

    object ExternalAccountVerification : ActionSheetStyle() { // TODO rename if used in other place(s) additionally to TransferMoney ext. account verification
        override fun onFragmentCreate(bottomSheetDialogFragment: BottomSheetDialogFragment) {
            bottomSheetDialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.ActionSheetDialogFragmentRounded)
        }
        override fun setupRecyclerView(rv: RecyclerView) {
            rv.addDividerItemDecoration(R.drawable.thin_divider)
        }
        override fun createViewHolder(parent: ViewGroup): ActionItemViewHolder {
            return object : ActionItemViewHolder(parent.inflate(R.layout.item_external_accout_verification_method)) {
                override fun bindTo(item: ActionItem) {
                    containerView.findViewById<TextView>(R.id.title).setText(item.displayNameId)
                    if (item.descriptionId != 0) { containerView.findViewById<TextView>(R.id.description).setText(item.descriptionId) }
                }
            }
        }
        override val name: String = "external_account_verification"
        override val layoutId: Int = R.layout.fragment_action_sheet_dialog_in_container
    }
}

private const val BUNDLE_STYLE_KEY = "action_sheet_style_key"
internal fun Bundle?.extractActionSheetStyle(): ActionSheetStyle {
    return if (this == null) {
        ActionSheetStyle.DefaultBubbles
    } else {
        when (this.getString(BUNDLE_STYLE_KEY)) {
            ActionSheetStyle.DefaultBubbles.name -> ActionSheetStyle.DefaultBubbles
            ActionSheetStyle.ExternalAccountVerification.name -> ActionSheetStyle.ExternalAccountVerification
            else -> ActionSheetStyle.DefaultBubbles
        }
    }
}

internal fun Bundle?.putActionSheetStyle(style: ActionSheetStyle) = this?.putString(BUNDLE_STYLE_KEY, style.name)
