package com.etrade.mobilepro.orders

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.DatePicker
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ScrollOnTopListener
import com.etrade.mobilepro.databinding.FragmentOrdersBinding
import com.etrade.mobilepro.databinding.IncludeOrdersFilterBinding
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.home.AccountTabNavigationListener
import com.etrade.mobilepro.home.AccountsTabDataDelegate
import com.etrade.mobilepro.home.AccountsTabHostFragmentArgs
import com.etrade.mobilepro.home.OnAccountsTabSelectionListener
import com.etrade.mobilepro.orders.adapter.OrderFilterItem
import com.etrade.mobilepro.orders.adapter.OrdersPagingDataAdapter
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.list.api.OrderItem
import com.etrade.mobilepro.orders.sharedviewmodel.OrderSharedViewModel
import com.etrade.mobilepro.orders.viewmodel.OrdersViewModel
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.instantiate
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.extension.updateTextAndDescription
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.formatters.americanSimpleDateFormatter
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import javax.inject.Inject

private const val FILTER_ORDER_STATUS_TAG = "orders_filter_order_status_tag"

internal const val ORDER_DETAILS_ARGUMENT = "orderDetailsArgument"
const val MIN_YEAR = 1900
const val MIN_MONTH = 1
const val MIN_DAY = 1

private val logger: Logger = LoggerFactory.getLogger(OrdersFragment::class.java)

@Suppress("LargeClass")
@RequireLogin
internal class OrdersFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.fragment_orders), AccountTabNavigationListener, ScrollOnTopListener, OnAccountsTabSelectionListener {

    private val binding by viewBinding(FragmentOrdersBinding::bind)
    private val orderStatusBinding by viewBinding(IncludeOrdersFilterBinding::bind)

    private val args: AccountsTabHostFragmentArgs by navArgs()
    private val sharedViewModel: OrderSharedViewModel by activityViewModels { viewModelFactory }
    private val viewModel: OrdersViewModel by viewModels { viewModelFactory }
    private val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { activity?.findViewById(R.id.nav_host_fragment) }
        )
    }
    private var snackBar: Snackbar? = null
    private val ordersPagingDataAdapter: OrdersPagingDataAdapter
        get() = binding.recyclerviewOrders.adapter as OrdersPagingDataAdapter

    private val dataDelegate = AccountsTabDataDelegate {
        viewModel.run {
            if (shouldRefresh) {
                refreshList()
                shouldRefresh = false
            }
        }
    }

    private val loadStateListener: (CombinedLoadStates) -> Unit = {
        if (it.append.endOfPaginationReached && ordersPagingDataAdapter.itemCount < 1) {
            showEmptyState()
        } else {
            hideEmptyState()
            if (viewModel.isDualPane &&
                viewModel.isFirstItemSelected() &&
                ordersPagingDataAdapter.itemCount > 0
            ) {
                ordersPagingDataAdapter.peek(0)?.let { orderItem ->
                    setSelectedItemAndPositionAndNavigate(orderItem, 0)
                } ?: showEmptyState()
            }
        }
    }

    private var currentPagingJob: Job? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpSharedViewModel()
        setUpViewModels()
        setupPullToRefresh()
        setupOrdersRecyclerView()
        dataDelegate.initialize(savedInstanceState != null)
        setupStartDate()
        setupEndDate()
        setupActivityFilterDropDown()
    }

    private fun setupPullToRefresh() {
        binding.swipeRefresh.setOnRefreshListener {
            refreshList(forceRefresh = true)
        }
    }

    private fun setupOrdersRecyclerView() {
        binding.recyclerviewOrders.run {
            adapter = OrdersPagingDataAdapter(
                selectItemListener = { selectedItem, position ->
                    setSelectedItemAndPositionAndNavigate(selectedItem, position)
                },
                selectionChecker = {
                    viewModel.isPositionSelected(it)
                }
            )
            layoutManager = LinearLayoutManager(context)
            setupRecyclerViewDivider(R.drawable.thin_divider)
        }

        ordersPagingDataAdapter.removeLoadStateListener(loadStateListener)
        ordersPagingDataAdapter.addLoadStateListener(loadStateListener)
    }

    @SuppressLint("DefaultLocale")
    override fun onAccountTabNavigated(additionalArgs: String?) {
        additionalArgs?.let {
            viewModel.setSelectedStatusFilter(OrderStatus.valueOf(it.uppercase()))
        }
    }

    private fun setupActivityFilterDropDown() {
        parentFragmentManager.let { fragmentManager ->
            val dropDownManager = BottomSheetSelector<OrderFilterItem>(fragmentManager, resources)
            dropDownManager.init(
                tag = FILTER_ORDER_STATUS_TAG,
                title = getString(R.string.status),
                items = viewModel.getStatusList(),
                initialSelectedPosition = viewModel.getSelectedStatusFilterIndex()
            )
            dropDownManager.setListener { _, selected ->
                viewModel.setSelectedStatusFilter(selected.status)
            }
            orderStatusBinding.textStatus.setOnClickListener {
                dropDownManager.openDropDown()
            }
        }
    }

    private fun formatDateString(date: LocalDate) = americanSimpleDateFormatter.format(date)

    private fun setupStartDate() {
        with(binding.dateRangeSelector.dateRangeStartDate) {
            updateTextAndDescription(
                formatDateString(viewModel.fromDate),
                R.string.label_start_date
            )
            setOnClickListener {
                viewModel.setShowStartDate()
            }
        }
    }

    private fun showStartDate() {
        context?.let {
            val startDate = viewModel.fromDate
            val datePickerDialog = DatePickerDialog(
                it,
                R.style.MaterialDatePicker,
                { _, year, monthOfYear, dayOfMonth ->
                    viewModel.fromDate = LocalDate.of(year, monthOfYear + 1, dayOfMonth)
                    binding.dateRangeSelector.dateRangeStartDate.updateTextAndDescription(
                        formatDateString(viewModel.fromDate),
                        R.string.label_start_date
                    )
                    refreshList()
                    viewModel.setShowStartDate(false)
                },
                startDate.year, startDate.monthValue - 1, startDate.dayOfMonth
            )
            updateSelectableDateRange(datePickerDialog.datePicker, true)
            datePickerDialog.setOnCancelListener {
                viewModel.setShowStartDate(false)
            }
            datePickerDialog.show()
        }
    }

    private fun setupEndDate() {
        with(binding.dateRangeSelector.dateRangeEndDate) {
            updateTextAndDescription(
                formatDateString(viewModel.toDate),
                R.string.label_end_date
            )
            setOnClickListener {
                viewModel.setShowEndDate()
            }
        }
    }

    private fun showEndDate() {
        context?.let {
            val endDate = viewModel.toDate
            val datePickerDialog = DatePickerDialog(
                it,
                R.style.MaterialDatePicker,
                { _, year, monthOfYear, dayOfMonth ->
                    viewModel.toDate = LocalDate.of(year, monthOfYear + 1, dayOfMonth)
                    binding.dateRangeSelector.dateRangeEndDate.updateTextAndDescription(
                        formatDateString(viewModel.toDate),
                        R.string.label_end_date
                    )
                    refreshList()
                    viewModel.setShowEndDate(false)
                },
                endDate.year, endDate.monthValue - 1, endDate.dayOfMonth
            )
            updateSelectableDateRange(datePickerDialog.datePicker, false)
            datePickerDialog.setOnCancelListener {
                viewModel.setShowEndDate(false)
            }
            datePickerDialog.show()
        }
    }

    private fun updateSelectableDateRange(datePicker: DatePicker, isStart: Boolean) {
        if (isStart) {
            val endDate = viewModel.toDate
            datePicker.maxDate = endDate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
            val minDate = LocalDate.of(MIN_YEAR, MIN_MONTH, MIN_DAY)
            datePicker.minDate = minDate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
        } else {
            val startDate = viewModel.fromDate
            datePicker.minDate = startDate.atStartOfDay(ZoneId.systemDefault()).toInstant().toEpochMilli()
        }
    }

    private fun setUpSharedViewModel() {
        sharedViewModel.isRefreshListRequested.observe(
            viewLifecycleOwner,
            Observer { event ->
                event.consume {
                    if (it) {
                        refreshList(forceRefresh = true)
                    }
                }
            }
        )

        sharedViewModel.refreshListMessage.observe(
            viewLifecycleOwner,
            Observer { event ->
                event.consume {
                    if (viewModel.isDualPane) {
                        snackbarUtil.errorSnackbar(it)
                    }
                }
            }
        )
    }

    private fun refreshList(forceRefresh: Boolean = false) {
        args.accountId?.let {
            removeOrderDetailFragment()
            hideEmptyState()

            currentPagingJob?.cancel()
            currentPagingJob = viewLifecycleOwner.lifecycleScope.launchWhenResumed {
                viewModel.fetchOrders(it, forceRefresh).collectLatest { pagingData ->
                    viewModel.setSelectedItemPositionInDualPane(0)
                    ordersPagingDataAdapter.submitData(pagingData)
                }
            }
        }
    }

    private fun setUpViewModels() {
        viewModel.run {
            selectedStatus.observe(
                viewLifecycleOwner,
                Observer { selectedStatus ->
                    orderStatusBinding.textStatus.updateTextAndDescription(
                        getLabelForSelectedStatus(
                            selectedStatus
                        ),
                        R.string.label_status_description
                    )
                    if (selectedStatus != lastSelectedStatus) {
                        lastSelectedStatus = selectedStatus
                        refreshList()
                    }
                }
            )

            refreshItemAtIndexesEvent.observe(
                viewLifecycleOwner,
                Observer {
                    it.peekContent().forEach { changedItemIndex ->
                        ordersPagingDataAdapter.notifyItemChanged(changedItemIndex)
                    }
                }
            )

            ordersFlow?.let { flow ->
                currentPagingJob?.cancel()
                currentPagingJob = viewLifecycleOwner.lifecycleScope.launchWhenResumed {
                    flow.collectLatest {
                        ordersPagingDataAdapter.submitData(it)
                    }
                }
            }

            viewLifecycleOwner.lifecycleScope.launchWhenStarted {
                ordersPagingDataAdapter.loadStateFlow.collectLatest {
                    handleRefreshState(it.refresh)

                    handleNetworkState(it.append)
                }
            }

            showStartDate.observe(viewLifecycleOwner) {
                if (it) this@OrdersFragment.showStartDate()
            }

            showEndDate.observe(viewLifecycleOwner) {
                if (it) this@OrdersFragment.showEndDate()
            }
        }
    }

    private fun handleNetworkState(state: LoadState) {
        when (state) {
            is LoadState.Loading -> {
                ordersPagingDataAdapter.isLoading = true
            }

            is LoadState.NotLoading -> {
                ordersPagingDataAdapter.isLoading = false
            }

            is LoadState.Error -> {
                ordersPagingDataAdapter.isLoading = false
                logger.error("Order list network failure", state.error)
                snackBar = snackbarUtil.retrySnackbar(
                    actionOnRetry = {
                        ordersPagingDataAdapter.retry()
                    }
                )
            }
        }
    }

    private fun handleRefreshState(state: LoadState) {
        when (state) {
            is LoadState.Loading -> {
                if (!binding.swipeRefresh.isRefreshing) {
                    binding.progressbarLoadingIndicator.show()
                }
                snackBar?.dismiss()
            }

            is LoadState.NotLoading -> {
                binding.progressbarLoadingIndicator.hide()
                snackBar?.dismiss()
                binding.swipeRefresh.isRefreshing = false
            }

            is LoadState.Error -> {
                binding.progressbarLoadingIndicator.hide()
                binding.swipeRefresh.isRefreshing = false
                logger.error("Order list refresh network failure", state.error)
                snackBar = snackbarUtil.retrySnackbar(
                    actionOnRetry = {
                        ordersPagingDataAdapter.retry()
                    }
                )
            }
        }
    }

    private fun showEmptyState() {
        binding.textEmptyState.visibility = View.VISIBLE
        removeOrderDetailFragment()
    }

    private fun hideEmptyState() {
        binding.textEmptyState.visibility = View.GONE
    }

    private fun navigate() {
        args.accountId?.let { accountId ->
            if (!viewModel.isDualPane) {
                viewModel.getOrderDetailArgument(accountId)?.let {
                    findNavController().navigate(R.id.orderDetailsFragment, bundleOf(ORDER_DETAILS_ARGUMENT to it))
                }
            } else {
                val fragmentManager = childFragmentManager
                val fragmentTransaction = fragmentManager.beginTransaction()
                val fragment = fragmentManager.fragmentFactory.instantiate(OrderDetailsFragment::class)
                Bundle().apply {
                    putParcelable(ORDER_DETAILS_ARGUMENT, viewModel.getOrderDetailArgument(accountId))
                    fragment.arguments = this
                }

                fragmentTransaction.replace(R.id.order_details_fragment, fragment, "OrderDetailsFragment")
                fragmentTransaction.commit()
            }
        }
    }

    private fun removeOrderDetailFragment() {
        childFragmentManager.findFragmentByTag("OrderDetailsFragment")?.let {
            childFragmentManager.beginTransaction().remove(it).commit()
        }
    }

    private fun setSelectedItemAndPositionAndNavigate(selectedItem: OrderItem, position: Int) {
        viewModel.setSelectedItemPositionInDualPane(position)
        viewModel.setSelectedOrderItem(selectedItem)
        navigate()
    }

    override val isOnTop get() = binding.recyclerviewOrders.computeVerticalScrollOffset() == 0

    override fun scrollUp() {
        binding.recyclerviewOrders.apply {
            stopScroll()
            smoothScrollToPosition(0)
        }
    }

    override fun onTabSelected() {
        dataDelegate.fetchData(view != null)
    }

    override fun onDestroyView() {
        binding.swipeRefresh.setOnRefreshListener(null)
        ordersPagingDataAdapter.removeLoadStateListener(loadStateListener)
        binding.recyclerviewOrders.adapter = null
        super.onDestroyView()
    }
}
