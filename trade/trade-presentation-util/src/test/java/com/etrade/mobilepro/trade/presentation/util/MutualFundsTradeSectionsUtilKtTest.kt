package com.etrade.mobilepro.trade.presentation.util

import android.content.res.Resources
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quoteapi.FundsFee
import com.etrade.mobilepro.quoteapi.FundsTradeSummaryItem
import com.etrade.mobilepro.quoteapi.SalesChargeType
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.HeaderSectionItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.SummarySectionItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

internal class MutualFundsTradeSectionsUtilKtTest {

    private fun mockResourcesString() = mock<Resources> {
        on { getString(R.string.empty_default) } doReturn ("--")
        on { getString(R.string.trade_label_last_trade_timestamp) } doReturn ("Last Trade Timestamp")
        on { getString(R.string.trade_label_shares_information) } doReturn ("Shares Information")
        on { getString(R.string.trade_label_cash_position) } doReturn ("Cash Positions")
        on { getQuantityString(R.plurals.trade_label_share_information_shares, 1, 1) } doReturn ("Test")
        on { getString(R.string.trade_label_cash_position_market_value) } doReturn ("Test")
        on { getString(R.string.trade_label_margin_positions) } doReturn ("Test")
        on { getString(R.string.trade_label_margin_positions_market_value) } doReturn ("Test")
        on { getString(R.string.trade_label_sub_min_investment) } doReturn ("Subsequent Minimum Investment")
        on { getString(R.string.trade_label_ira_sub_min_investment) } doReturn ("IRA Subsequent Minimum Investment")
        on { getString(R.string.trade_label_ira_init_min_investment) } doReturn ("IRA Initial Minimum Investment")
        on { getString(R.string.trade_label_init_min_investment) } doReturn ("Initial Minimum Investment")
        on { getString(R.string.trade_label_cut_off_time) } doReturn ("Test")
        on { getString(R.string.trade_label_sales_charge) } doReturn ("Sales Charge")
        on { getString(R.string.trade_sales_charge_back_end) } doReturn ("Back End")
        on { getString(R.string.trade_sales_charge_front_end) } doReturn ("Front End")
        on { getString(R.string.trade_sales_charge_front_end_load) } doReturn ("Front-end Load (Waived at E*TRADE)")
        on { getString(R.string.trade_label_fund_redemption_fee) } doReturn ("Test")
        on { getString(R.string.trade_label_transaction_fee) } doReturn ("Test")
        on { getString(R.string.trade_label_early_redemption_fee) } doReturn ("Test")
        on { getString(R.string.trade_label_deferred_charge) } doReturn ("Deferred Sales Charge")
        on { getString(R.string.trade_label_front_end_charge) } doReturn ("Front End Sales Charge")
        on { getString(R.string.trade_sales_charge_none) } doReturn ("None")
    }

    private fun getMockFundsTradeSummaryItem(
        type: InstrumentType = InstrumentType.MF,
        salesChargeType: SalesChargeType = SalesChargeType.NONE,
        isFundsOwned: Boolean = false,
        iraInitialInvestment: String? = null,
        iraSubsequentInvestment: String? = null,
        initialInvestment: String = "",
        subsequentInvestment: String = ""
    ) = FundsTradeSummaryItem(
        annualAvgReturn = "",
        change = "",
        deferredSalesCharge = mockFundsFeeList(),
        etradeEarlyRedemptionFee = "",
        frontEndSalesCharge = mockFundsFeeList(),

        frontLoadText = "",
        fundRedemptionFee = "",
        fundName = "",
        lastTradeTime = "08:00 PM EDT",
        initialInvestment = initialInvestment,
        instrumentType = type,
        iraInitialInvestment = iraInitialInvestment,
        iraSubsequentInvestment = iraSubsequentInvestment,
        netAssetValue = "",
        ordCutOffTime = "",
        salesCharge = "",
        salesChargeType = salesChargeType,
        sevenDayCurrentYield = "",
        subsequentInvestment = subsequentInvestment,
        transactionFee = "",
        weightedAvgMaturity = "",
        positions = emptyList(),
        isFundOwned = isFundsOwned
    )

    @Test
    fun testMutualFundsQuoteSummaryItems() {
        val result = toMutualFundsQuoteSummaryItems(
            mockResourcesString(),
            getMockFundsTradeSummaryItem(
                initialInvestment = "1000.00",
                subsequentInvestment = "1234.56"
            )
        )
        assert(result.isNotEmpty())
        result.assertIfHeaderSectionItem("Shares Information")
        result.assertSummarySectionItem("Initial Minimum Investment", "1000.00")
        result.assertSummarySectionItem("Subsequent Minimum Investment", "1234.56")
        result.assertSummarySectionItem("Sales Charge", "None")
        result.assertSummarySectionItem("Sales Charge", "None")
        result.assertHeaderSectionItem("Deferred Sales Charge")
        result.assertHeaderSectionItem("Front End Sales Charge")
        result.assertSummarySectionItem("> \$250,000", "0.00%")
        result.assertSummarySectionItem("\$0 - \$49,999", "5.75%")
    }

    @Test
    fun testIraMutualFundsQuoteSummaryItems() {
        val result = toMutualFundsQuoteSummaryItems(
            mockResourcesString(),
            getMockFundsTradeSummaryItem(
                iraInitialInvestment = "1000.00",
                iraSubsequentInvestment = "1234.56",
                salesChargeType = SalesChargeType.BACK_END
            )
        )
        assert(result.isNotEmpty())
        result.assertIfHeaderSectionItem("Shares Information")
        result.assertSummarySectionItem("IRA Initial Minimum Investment", "1000.00")
        result.assertSummarySectionItem("IRA Subsequent Minimum Investment", "1234.56")
        result.assertSummarySectionItem("Sales Charge", "Back End")
    }

    @Test
    fun testOwnedMutualFundsSummaryItems() {
        val result = toMutualFundsQuoteSummaryItems(
            mockResourcesString(),
            getMockFundsTradeSummaryItem(salesChargeType = SalesChargeType.FRONT_END, isFundsOwned = true)
        )
        assert(result.isNotEmpty())
        result.assertHeaderSectionItem("Shares Information")
        result.assertSummarySectionItem("Sales Charge", "Front End")
    }

    @Test
    fun testSalesChargeFrontEndLoadMutualFundsSummaryItems() {
        val result = toMutualFundsQuoteSummaryItems(
            mockResourcesString(),
            getMockFundsTradeSummaryItem(salesChargeType = SalesChargeType.FRONT_END_LOAD, isFundsOwned = true)
        )
        assert(result.isNotEmpty())
        result.assertHeaderSectionItem("Shares Information")
        result.assertSummarySectionItem("Sales Charge", "Front-end Load (Waived at E*TRADE)")
    }

    @Test
    fun testMoneyMarketFundsSummaryItems() {
        val result = toMutualFundsQuoteSummaryItems(mockResourcesString(), getMockFundsTradeSummaryItem(InstrumentType.MMF))
        assert(result.isNotEmpty())
    }

    private fun List<TableSectionItemLayout>.assertHeaderSectionItem(title: String) {
        assert(
            any {
                (it is HeaderSectionItem) && it.title == title
            }
        )
    }

    private fun List<TableSectionItemLayout>.assertIfHeaderSectionItem(title: String) {
        assert(
            none {
                (it is HeaderSectionItem) && it.title == title
            }
        )
    }

    private fun List<TableSectionItemLayout>.assertSummarySectionItem(label: String, value: String) {
        val summarySectionItem: SummarySectionItem = first {
            (it is SummarySectionItem) && (it.label == label)
        } as SummarySectionItem
        assertEquals(summarySectionItem.value, value)
    }

    private fun mockFundsFeeList(): List<FundsFee>? = listOf(
        FundsFee(range = "\$0 - \$49,999", percent = "5.75%"),
        FundsFee(range = "> \$250,000", percent = "0.00%")
    )
}
