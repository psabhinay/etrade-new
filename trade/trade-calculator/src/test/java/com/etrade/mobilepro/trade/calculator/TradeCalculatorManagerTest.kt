package com.etrade.mobilepro.trade.calculator

import org.junit.Assert.assertEquals
import org.junit.Test

internal class TradeCalculatorManagerTest {

    @Test
    fun `toValueDigital should format string to decimal value`() {
        assertEquals("0.01235", "test0.01234567879test".toValueDigital())
        assertEquals("54321.00", "\$54321/Share".toValueDigital())
        assertEquals("0.00", "abc0".toValueDigital())
        assertEquals("341234.90", "000000341234.9".toValueDigital())
        assertEquals(DEFAULT_CALCULATOR_VALUE, "".toValueDigital())
        assertEquals("12345.67", "12345,67/share".toValueDigital())
        assertEquals("0.00", "initial0,00".toValueDigital())
    }

    @Test
    fun `calculateResult should calculate valid integer`() {
        assertEquals("1818", calculateResult("98794812", "$54321.4241/Share"))
        assertEquals(CALCULATOR_ZERO, calculateResult(CALCULATOR_ZERO, "$0.04/Share"))
        assertEquals(INVALID_DATA, calculateResult("51", "$0.00"))
        assertEquals("1", calculateResult("40", "21"))
        assertEquals("2", calculateResult("40", "$20/Share"))
        assertEquals("9", calculateResult("81", "$9.00/Share"))
        assertEquals("1", calculateResult("4", "2.00001/Share"))
        assertEquals("9009009009009009009009009009009009009009", calculateResult("10000000000000000000000000000000000000000", "$1.11/Share"))
    }

    @Test
    fun `removeLastCharacter should remove last character if any`() {
        assertEquals("qwert", "qwerty".removeLastCharacter())
        assertEquals("", "1".removeLastCharacter())
        assertEquals("", "".removeLastCharacter())
    }
}
