package com.etrade.mobilepro.news.data.repo.datasource

import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.data.dto.NewsDto
import com.etrade.mobilepro.news.data.rest.NewsApiClient
import io.reactivex.Single

internal class MarketNewsDataSource(private val newsApiClient: NewsApiClient) : NewsPagingSource() {
    override fun getNewsRange(startIndex: Int, size: Int, requestParams: NewsRequest?): Single<NewsDto> {
        return newsApiClient.getMarketNews(startIndex, size)
    }
}
