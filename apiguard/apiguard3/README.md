# API guard

## Description

Filters unwanted API calls, gathering required telemetry information from a client and sending it to a backend service for analysis and decision-making. Current
implementation uses [Shape Security][1] solution to deal with these tasks. See [Links](#links) for more details.


## Usage

### Initialization

The first step is to initialize API guard itself. This must be done once, on an application startup. The easiest way to do this is to call `ApiGuard.init`
method from `Application.onCreate` method. You must provide valid configuration file for Shape's `APIGuard` (see [Configuration](#configuration) for more
details).

```Kotlin
class MyApplication : Application() {
	override fun onCreate() {
	    // ...
	    ApiGuard.init(this, R.raw.apiguard)
	    // ...
	}
}
```

By default,  `ApiGuard` will use `default` environment from a configuration file. If required, you can override this behavior calling `ApiGuard.init` method
with optional environment name parameter:

```Kotlin
ApiGuard.init(this, R.raw.apiguard, "environment_name")
```

The second step is to register API call interceptor to your `OkHttpClient`.

```Kotlin
val client = OkHttpClient.Builder()
	// ...
	.addInterceptor(ApiGuardInterceptor())
	// ...
	build()
```

*NOTE: You may want to restrict usage of this interceptor to a particular HTTP client, that is used solely for those API calls, that require filtering of
unwanted traffic.*


### Configuration

JSON file, containing configuration for `APIGuard`, must be placed in `res/raw` folder. There are two elements in this file that you may want to modify:
`updateURL` and `updateURLMap`.

To add additional environment to a configuration file, add it to `updateURLMap` object of JSON document.

```JSON
{
	"...": "...",
	"updateURL": "https://existing_environment.example.com/init",
	"updateURLMap": {
	    "default": "https://existing_environment.example.com/init",
	    "environment_name": "https://new_environment.example.com/init"
	}
}
```

See [Initialization](#initialization) to know how to initialize API guard with a new environment.


## Links

Implementation details: https://confluence.corp.etradegrp.com/pages/viewpage.action?pageId=135033642
Implementation discussion: https://bitbucket.etrade.com/projects/AP/repos/android_et/pull-requests/494/overview

[1]: https://www.shapesecurity.com
[2]: https://confluence.corp.etradegrp.com/pages/viewpage.action?pageId=135033642&preview=/135033642/141400001/APIGuard_Android_332.pdf
