package com.etrade.mobilepro.order.details.router

interface WithOrderConfirmationAction {

    val orderConfirmationAction: OrderConfirmationAction
}
