package com.etrade.mobilepro.uiwidgets.groupscrollrecyclerview

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView

/**
 * A [RecyclerView] to be used in conjunction with [VerticalGroupScrollListener] to prevent more than one instance added.
 */
class VerticalGroupScrollRecyclerView : RecyclerView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var verticalGroupScrollListenerAdded = false

    var onScrollStoppedListener: OnScrollStoppedListener? = null

    override fun addOnScrollListener(listener: OnScrollListener) {
        if (listener is VerticalGroupScrollListener) {
            if (!verticalGroupScrollListenerAdded) {
                // Only allow one VerticalGroupScrollListener to be added at any given time.
                verticalGroupScrollListenerAdded = true
                super.addOnScrollListener(listener)
            }
        } else {
            super.addOnScrollListener(listener)
        }
    }

    override fun removeOnScrollListener(listener: OnScrollListener) {
        if (listener is VerticalGroupScrollListener) {
            if (verticalGroupScrollListenerAdded) {
                verticalGroupScrollListenerAdded = false
                onScrollStoppedListener?.onScrollStopped(this)
                super.removeOnScrollListener(listener)
            }
        } else {
            super.removeOnScrollListener(listener)
        }
    }

    interface OnScrollStoppedListener {
        fun onScrollStopped(recyclerView: RecyclerView)
    }
}
