package com.etrade.mobilepro.dynamicui.api

interface DynamicWidget {

    val type: Type

    enum class Type {
        LOGIN,
        DEFAULT,
        MARKET_INDICES,
        NEWS_BRIEFING,
        MOVERS,
        FUTURES,
        NEWS
    }
}
