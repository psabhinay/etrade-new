package com.etrade.mobilepro.chart.formatters

import com.github.mikephil.charting.formatter.ValueFormatter

class YAxisValuePercentageFormatter : ValueFormatter() {

    /**
     * Called when a value from an axis is to be formatted
     * before being drawn. For performance reasons, avoid excessive calculations
     * and memory allocations inside this method.
     */
    override fun getFormattedValue(value: Float): String {
        return "%.1f%%".format(value)
    }
}
