package com.etrade.mobilepro.common.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.util.AppMessage
import com.etrade.mobilepro.util.TextMessage
import com.etrade.mobilepro.viewmodel.AppMessageViewModel
import javax.inject.Inject

class SnackbarViewModel @Inject constructor() : AppMessageViewModel {

    override val appMessageSignal: LiveData<AppMessage?>
        get() = _appMessageSignal

    private val _appMessageSignal: MutableLiveData<AppMessage?> = MutableLiveData()

    override fun dismissAppMessage() {
        _appMessageSignal.value = null
    }

    fun postAppMessage(message: CharSequence) {
        _appMessageSignal.postValue(TextMessage(message, TextMessage.Duration.SHORT))
    }
}
