package com.etrade.mobilepro.settings.viewmodel

import android.content.res.Resources
import androidx.fragment.app.FragmentManager
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.valuepicker.ValuePickerWidgetItem

abstract class SettingValuePickerPayload(
    override val title: String,
    override val layoutId: Int,
    private val resources: Resources
) : SettingDynamicViewModel(
    title,
    null,
    layoutId
),
    ValuePickerWidgetItem {
    open val ghostText: String?
        get() = null
    open val dropDownListScreenTitle: String
        get() = title
    abstract val items: List<Any>?
    abstract var position: Int
    private var dropDownManager: BottomSheetSelector<Any>? = null
    open val onSelected: ((Any) -> Unit)? = null
    override var onDropDownSelectedListener: ((title: String, description: String) -> Unit)? = null

    fun initValuePickerDropDown(frm: FragmentManager?) {
        frm?.also {
            dropDownManager = BottomSheetSelector(it, resources)

            items?.also { values ->
                dropDownManager?.init("VALUE_PICKER_$title", dropDownListScreenTitle, values, position, null, ghostText)

                dropDownManager?.setListener { position, selected ->
                    this.position = position
                    onSelected?.invoke(selected)
                    val selectedTitle = selected.toString()
                    onDropDownSelectedListener?.invoke(selectedTitle, "$title $selectedTitle")
                }
            }
        }
    }

    override fun showValuePickerDropDown() {
        items?.size?.also {
            if (it > 1) {
                dropDownManager?.openDropDown()
            }
        }
    }
}
