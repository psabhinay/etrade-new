package com.etrade.mobilepro.trade.api.stock

import com.etrade.mobilepro.trade.api.OrderPlaceResponse

interface StockOrderPlaceResponse : OrderPlaceResponse, StockOrderResponse {
    override val orders: List<ConfirmedStockOrder>
}
