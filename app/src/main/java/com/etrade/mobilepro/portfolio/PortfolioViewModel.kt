package com.etrade.mobilepro.portfolio

import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.common.di.RemoteDynamicScreen
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.portfolio.lightstreamer.AccountPortfolioDataProvider
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.positions.PositionListViewModel
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.squareup.moshi.Moshi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Provider

private const val THROTTLE_INTERVAL_SEC = 30L
private const val INITIAL_DELAY = 5L

/**
 * View model for [PortfolioFragment].
 */
class PortfolioViewModel @Inject constructor(
    @RemoteDynamicScreen
    dynamicScreenRepo: DynamicScreenRepo,
    moshi: Moshi,
    tradingDefaultsPreferences: TradingDefaultsPreferences,
    tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    darkModeChecker: DarkModeChecker,
    portfolioSettingsRepository: PortfolioSettingsRepository,
    private val accountPortfolioDataProvider: AccountPortfolioDataProvider
) : PositionListViewModel(
    dynamicScreenRepo,
    moshi,
    tradingDefaultsPreferences,
    darkModeChecker,
    tradeFormParametersBuilderProvider,
    portfolioSettingsRepository,
    PositionTableMode.PORTFOLIO_VIEW
) {
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    private var accountUuid: String? = null
    private var accountId: String? = null

    private var disposable: Disposable? = null

    override fun getScreenRequest(sortInfo: SortInfo): ScreenRequest =
        ServicePath.Portfolio(checkNotNull(accountUuid), sortInfo.column, sortInfo.order)

    fun provideParameters(accountUuid: String, accountId: String) {
        this.accountUuid = accountUuid
        this.accountId = accountId
    }

    fun subscribeForPortfolioUpdates(fromDataFetch: Boolean = true) {
        val accountId: String = this.accountId ?: return
        if (disposable == null || disposable?.isDisposed == true) {
            disposable = accountPortfolioDataProvider.getAccountPortfolioEvents(accountId)
                .throttleFirst(THROTTLE_INTERVAL_SEC, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .delaySubscription(
                    if (fromDataFetch) {
                        THROTTLE_INTERVAL_SEC
                    } else {
                        INITIAL_DELAY
                    },
                    TimeUnit.SECONDS
                )
                .subscribeBy(
                    onNext = { event ->
                        logger.debug("Portfolio update event: $event")
                        _dataUpdated.value = true.consumable()
                    },
                    onError = {
                        logger.error("Unable to subscribe to portfolio events", it)
                    }
                )
        }
    }

    fun pauseSubscription() {
        disposable?.dispose()
    }

    override fun onCleared() {
        super.onCleared()
        disposable?.dispose()
        disposable = null
    }
}
