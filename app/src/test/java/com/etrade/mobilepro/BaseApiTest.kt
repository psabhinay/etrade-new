package com.etrade.mobilepro

import com.etrade.mobilepro.dagger.DaggerTestApplicationComponent
import com.etrade.mobilepro.dagger.TestApplicationComponent
import org.junit.Before

abstract class BaseApiTest {
    lateinit var testApplicationComponent: TestApplicationComponent

    @Before
    open fun setUp() {
        testApplicationComponent = DaggerTestApplicationComponent.builder()
            .build()
        testApplicationComponent.inject(this)
    }
}
