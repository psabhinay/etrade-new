package com.etrade.mobilepro.marketscalendar.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

const val ECONOMIC_EVENT = "F"
const val EARNINGS_EVENT = "E"
const val ALL_SYMBOLS_EVENTS = "*"

@JsonClass(generateAdapter = true)
class EventsRequestBody(
    @Json(name = "value")
    val value: Value
) {

    @JsonClass(generateAdapter = true)
    data class Value(
        @Json(name = "dateRangeSet")
        val dateRangeSet: List<DateRange>,
        @Json(name = "symbolSet")
        val symbolSet: List<String>,
        @Json(name = "eventFilterEnable")
        val eventFilterEnable: String?,
        @Json(name = "eventTypeSet")
        val eventTypeSet: List<String>
    )

    @JsonClass(generateAdapter = true)
    data class DateRange(
        @Json(name = "startDate")
        val startDate: Date,
        @Json(name = "endDate")
        val endDate: Date
    )

    @JsonClass(generateAdapter = true)
    data class Date(
        @Json(name = "year")
        val year: Int,
        @Json(name = "month")
        val month: Int,
        @Json(name = "dayOfMonth")
        val dayOfMonth: Int,
        @Json(name = "hourOfDay")
        val hourOfDay: Int,
        @Json(name = "minute")
        val minute: Int,
        @Json(name = "second")
        val second: Int
    )
}
