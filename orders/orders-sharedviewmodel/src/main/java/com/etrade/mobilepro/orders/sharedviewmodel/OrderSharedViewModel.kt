package com.etrade.mobilepro.orders.sharedviewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import javax.inject.Inject

class OrderSharedViewModel @Inject constructor() : ViewModel() {

    private val _isRefreshListRequested = MutableLiveData(ConsumableLiveEvent(false))
    private val _isRefreshDetailRequested = MutableLiveData<ConsumableLiveEvent<OrderDetailsArgument>>()
    private val _refreshListMessage = MutableLiveData<ConsumableLiveEvent<String>>()
    private val _tradeCanceledSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()

    val refreshListMessage: LiveData<ConsumableLiveEvent<String>>
        get() = _refreshListMessage

    val isRefreshListRequested: LiveData<ConsumableLiveEvent<Boolean>>
        get() = _isRefreshListRequested

    val isRefreshDetailRequested: LiveData<ConsumableLiveEvent<OrderDetailsArgument>>
        get() = _isRefreshDetailRequested

    var resetOrderDetailRequested = false

    val tradeCanceledSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _tradeCanceledSignal

    fun refreshOrderDetailWith(orderDetailsArguments: OrderDetailsArgument) {
        _isRefreshDetailRequested.postValue(ConsumableLiveEvent(orderDetailsArguments))
    }

    fun requireRefreshOrderList(snackBarMessage: String? = null) {
        _isRefreshListRequested.value = ConsumableLiveEvent(true)
        snackBarMessage?.let {
            _refreshListMessage.value = ConsumableLiveEvent(it)
        }
    }

    fun sendTradeCanceledSignal() {
        _tradeCanceledSignal.value = Unit.consumable()
    }

    fun setRefreshListComplete() {
        _isRefreshListRequested.value = ConsumableLiveEvent(false)
    }
}
