package com.etrade.mobilepro.inboxmessages

interface InAppMessage {

    val eventName: String?

    val title: String?

    val subtitle: String?

    val callToAction: String?

    val deepLink: String?

    val rawAttributes: Map<String, String>
}
