package com.etrade.mobilepro.bloombergtv.presentation

import com.etrade.mobilepro.bloombergtv.api.BloombergLiveData
import com.etrade.mobilepro.bloombergtv.api.BloombergTvRepository
import com.etrade.mobilepro.bloombergtv.api.BloombergTvResponse
import com.etrade.mobilepro.bloombergtv.api.ErrMsgBuffer
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

const val successfulLoadedUrl = ""

class BloombergTvViewModelTest {

    @Mock
    private lateinit var bloombergTvRepository: BloombergTvRepository

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun `successful url loading`() {
        val successfulBloombergTvResponse = BloombergTvResponse(
            BloombergLiveData("", "", "", "", "", successfulLoadedUrl),
            null,
            ErrMsgBuffer("")
        )

        whenever(bloombergTvRepository.getBloombergtvResponse()).thenReturn(Single.just(successfulBloombergTvResponse))

        bloombergTvRepository.getBloombergtvResponse().subscribeBy(
            onSuccess = {
                assertThat(it.bloombergLiveData.url, equalTo(successfulLoadedUrl))
            },
            onError = {
                // we shouldn't be there
            }
        )
    }
}
