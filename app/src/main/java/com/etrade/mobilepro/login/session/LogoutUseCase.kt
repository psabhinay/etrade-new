package com.etrade.mobilepro.login.session

import com.etrade.mobilepro.util.UseCase

/**
 *  Used to implement a synchronous flow of Logout service call followed by cleanup of cookies etc.
 *
 */
interface LogoutUseCase : UseCase<Unit, Unit>
