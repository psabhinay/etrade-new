package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.trade.api.SaveOrderResponse

internal class SaveOrderGeneralResponse(
    dto: SaveOrderPreparedOrderDto?,
    private val messages: List<ServerMessage>?
) : SaveOrderResponse {
    override val accountId: String = dto?.accountId.orEmpty()
    override val orderId: String = dto?.preparedId.orEmpty()
    override val warnings: Collection<ServerMessage>
        get() = messages?.filter { it.type == ServerMessage.Type.WARN } ?: emptyList()
}
