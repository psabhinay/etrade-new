# E*TRADE Android Application

1. [Versioning](#versioning)
1. [Builds](#builds)
    1. [Environments](#environments)
    1. [Publishing Builds](#publishing)

<a name="versioning"></a>
## Versioning

The application follows [semantic versioning.](https://semver.org/)

The APK derives its versions from the *gradle/scripts/versioning.gradle* file. Version names are based off of the branch name and commit count.

Debuggable (non-release) APKs will use the build timestamp as their version code. Release APK will use the commit count.

<a name="builds"></a>
## Builds

<a name="environments"></a>
## Environments

Environment configurations are baked in at build-time and there is a buildType per environment, e.g. UAT, SIT

<a name="publishing"></a>
## Publishing Builds

[Creating Builds](https://confluence.corp.etradegrp.com/display/MI/Creating+Builds)
