package com.etrade.mobilepro.trade.data.customorder

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class CustomOrderEditPreviewResponseDto(
    @Json(name = "customOrderChangePreview")
    override val order: CustomOrderDto?
) : CustomOrderResponse()
