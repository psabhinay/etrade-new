package com.etrade.mobilepro.positions.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PortfolioTaxLotsRequestDto(
    @Json(name = "lots")
    val lots: TaxLotRequestDto
)

@JsonClass(generateAdapter = true)
class TaxLotRequestDto(
    @Json(name = "accountUuid")
    val accountUuid: String,
    @Json(name = "positionId")
    val positionId: String
)
