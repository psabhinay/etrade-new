package com.etrade.mobilepro.watchlist.viewmodel

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.rx.test.RxImmediateSchedulerRule
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import com.etrade.mobilepro.stub.streaming.TestStreamingSubscriptionController
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.timer.TimeoutTimer
import com.etrade.mobilepro.util.color.DataColor
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.etrade.mobilepro.watchlist.overview.viewmodel.ViewState
import com.etrade.mobilepro.watchlist.overview.viewmodel.WatchlistOverviewViewModel
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.WatchlistColumn
import com.etrade.mobilepro.watchlist.usecase.ResolveMutualFundTimestampUseCase
import com.etrade.mobilepro.watchlist.usecase.ResolveTimestampUseCase
import com.etrade.mobilepro.watchlist.usecase.WatchlistNullStateUseCase
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnRepo
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.math.BigDecimal

private const val WATCHLIST_ID = "1161254318"

@ExperimentalCoroutinesApi
class WatchlistOverviewViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    private lateinit var sut: WatchlistOverviewViewModel

    private var dataColorResolver: DataColorResolver = mock {
        on { resolveDataColor(anyOrNull()) }.thenReturn(DataColor.NEUTRAL)
        on { resolveFilledDataColor(anyOrNull()) }.thenReturn(DataColor.NEUTRAL)
    }

    @Mock
    private lateinit var watchlistNullStateUseCase: WatchlistNullStateUseCase

    @Mock
    private lateinit var preferences: UserPreferences

    @Mock
    private lateinit var watchlistRepo: WatchlistRepo

    @Mock
    private lateinit var watchlistColumnRepo: WatchlistColumnRepo

    @Mock
    private lateinit var quoteRepo: MobileQuoteRepo

    @Mock
    private lateinit var userViewModel: UserViewModel

    @Mock
    private lateinit var resources: Resources

    @Mock
    private lateinit var streamingController: StreamingSubscriptionController

    @Mock
    private lateinit var streamingStatusController: StreamingStatusController

    @Mock
    private lateinit var walkthroughStatesViewModel: WalkthroughStatesViewModel

    @Mock
    private lateinit var applicationPreferences: ApplicationPreferences

    @Mock
    private lateinit var resolveTimestamp: ResolveTimestampUseCase

    @Mock
    private lateinit var resolveMutualTimestamp: ResolveMutualFundTimestampUseCase

    @Mock
    private lateinit var inboxMessagesRepository: InboxMessagesRepository

    @Mock
    private lateinit var timeoutTimer: TimeoutTimer

    private val mockedSession = MutableLiveData<ConsumableLiveEvent<SessionStateChange>>()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase = mock {
        onBlocking { execute(anyOrNull()) }.thenReturn(false)
    }

    private val tracker: Tracker = mock()
    private val user: User = mock()

    @Before
    fun setUp() {
        Dispatchers.setMain(testCoroutineDispatcher)

        MockitoAnnotations.openMocks(this)
        whenever(watchlistRepo.getUserWatchlists()).thenReturn(
            Single.fromCallable {
                generateMockWatchlists()
            }
        )
        runBlockingTest { whenever(resolveTimestamp.execute(any())).thenReturn(ETResult.success("")) }
        runBlockingTest { whenever(resolveMutualTimestamp.execute(any())).thenReturn(ETResult.success("")) }
        runBlockingTest { whenever(watchlistNullStateUseCase.execute(any())).thenReturn(false) }

        whenever(userViewModel.userSessionState).thenReturn(mockedSession)
        whenever(walkthroughStatesViewModel.state).thenReturn(MutableLiveData())
        whenever(applicationPreferences.shouldDisplayWatchListTooltip).thenReturn(false)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `authenticated users restore their recent watch list from preference`() {
        whenever(watchlistRepo.getWatchlistEntries(any(), any())).thenReturn(Single.fromCallable { emptyList<WatchlistEntry>() })

        whenever(preferences.getRecentWatchlist()).thenReturn(WATCHLIST_ID to null)
        whenever(preferences.recentWatchlistData).thenReturn(MutableLiveData(WATCHLIST_ID to null))
        whenever(resources.getString(any())).thenReturn("device watchlist")
        whenever(applicationPreferences.watchlistSortState).thenReturn("")

        mockedSession.value = ConsumableLiveEvent(SessionStateChange.AUTHENTICATED)

        sut = WatchlistOverviewViewModel(
            dataColorResolver = dataColorResolver,
            watchlistNullStateUseCase = watchlistNullStateUseCase,
            inboxMessagesRepository = inboxMessagesRepository,
            isExtendedHoursOnUseCase = isExtendedHoursOnUseCase,
            watchlistRepo = watchlistRepo,
            watchlistColumnRepo = watchlistColumnRepo,
            quoteRepo = quoteRepo,
            userPreferences = preferences,
            resources = resources,
            streamingController = streamingController,
            streamingStatusController = streamingStatusController,
            tracker = tracker,
            user = user,
            resolveTimestamp = resolveTimestamp,
            resolveMutualFundTimestamp = resolveMutualTimestamp,
            watchlistId = null,
            marketStatusDelegate = mock(),
            walkthroughStatesViewModel = walkthroughStatesViewModel,
            applicationPreferences = applicationPreferences,
            timeoutTimer = timeoutTimer
        )
        sut.getUserWatchlists()

        sut.selectedWatchList.test().assertValue { it.id == WATCHLIST_ID } // correct watch list selected
        assertEquals("selected index is 4", 4, sut.getIndexOfSelectedWatchlist())
        assertEquals("5 fetched user watch lists", 5, sut.watchlists.value?.size)
    }

    @Test
    fun `test symbol update from streamingQuoteProvider`() = runBlockingTest(testCoroutineDispatcher) {
        whenever(streamingStatusController.isStreamingToggleEnabled).thenReturn(true)
        whenever(watchlistRepo.getWatchlistEntries(any(), any())).thenReturn(Single.just(provideMockWatchlistEntries()))
        whenever(watchlistColumnRepo.getColumns()).thenReturn(provideMockWatchlistColumns())
        whenever(preferences.getRecentWatchlist()).thenReturn(WATCHLIST_ID to null)
        whenever(preferences.recentWatchlistData).thenReturn(MutableLiveData(WATCHLIST_ID to null))
        whenever(resources.getString(any())).thenReturn("Test string")
        whenever(applicationPreferences.watchlistSortState).thenReturn("")

        val throttlerSkipMs = 500L
        val mockObserver = mock<Observer<ViewState>>()
        val mockLevel1Data = mock<Level1Data> {
            on { lastPrice } doReturn "311.25"
        }
        val stubStreamingController = TestStreamingSubscriptionController()

        val vm = WatchlistOverviewViewModel(
            dataColorResolver = dataColorResolver,
            watchlistNullStateUseCase = watchlistNullStateUseCase,
            inboxMessagesRepository = inboxMessagesRepository,
            isExtendedHoursOnUseCase = isExtendedHoursOnUseCase,
            watchlistRepo = watchlistRepo,
            watchlistColumnRepo = watchlistColumnRepo,
            quoteRepo = quoteRepo,
            userPreferences = preferences,
            resources = resources,
            streamingController = stubStreamingController,
            streamingStatusController = streamingStatusController,
            tracker = tracker,
            user = user,
            resolveTimestamp = resolveTimestamp,
            resolveMutualFundTimestamp = resolveMutualTimestamp,
            watchlistId = null,
            marketStatusDelegate = mock(),
            walkthroughStatesViewModel = walkthroughStatesViewModel,
            applicationPreferences = applicationPreferences,
            timeoutTimer = timeoutTimer
        )
        vm.viewState.observeForever(mockObserver)
        vm.selectedWatchList.observeForever { }
        vm.refreshCurrentWatchList()
        stubStreamingController.emitStreamData("AAPL", InstrumentType.UNKNOWN, mockLevel1Data)
        delay(throttlerSkipMs)
        val result = vm.viewState.value as ViewState.FetchEntries.Success
        assertEquals(result.tableViewData.cellItems.size, 3)
        assertEquals(result.tableViewData.cellItems[1][0].displayText, mockLevel1Data.lastPrice)
    }

    private fun provideMockWatchlistEntries() =
        listOf(
            WatchlistEntry(
                ticker = "AMD",
                entryID = "AMD",
                lastPrice = BigDecimal("49.26")
            ),
            WatchlistEntry(
                ticker = "AAPL",
                entryID = "AAPL",
                lastPrice = BigDecimal("308.95")
            ),
            WatchlistEntry(
                ticker = "INTC",
                entryID = "INTC",
                lastPrice = BigDecimal("65.69")
            )
        )

    private fun provideMockWatchlistColumns() = listOf<WatchlistColumn>(WatchlistColumn.LastPrice)

    private fun generateMockWatchlists(): List<Watchlist> {
        return listOf(
            Watchlist(DEVICE_WATCHLIST_ID, "DEVICE WATCHLIST NAME"),
            Watchlist("12345", ""),
            Watchlist("67890", ""),
            Watchlist("11111", ""),
            Watchlist("1161254318", "THE ONE")
        )
    }
}
