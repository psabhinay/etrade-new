package com.etrade.mobilepro.dynamicui.dto.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MsCompleteViewRequestDto(
    @Json(name = "msCompleteView")
    val msCompleteView: MsCompleteViewBodyDto = MsCompleteViewBodyDto()
)

@JsonClass(generateAdapter = true)
data class MsCompleteViewBodyDto(
    @Json(name = "enableTotalsConditionalDisplay")
    val enableTotalsConditionalDisplay: Boolean = true
)
