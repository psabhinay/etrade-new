package com.etrade.mobilepro.quote.mapper

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.PricingParameterCode
import com.etrade.mobilepro.quote.dto.FeeStructure
import com.etrade.mobilepro.quote.dto.FundDetails
import com.etrade.mobilepro.quote.dto.MutualFundsTradeQuoteDetailsResponseDto
import com.etrade.mobilepro.quote.dto.ProductFundDetailDto
import com.etrade.mobilepro.quoteapi.FundsFee
import com.etrade.mobilepro.quoteapi.FundsTradeQuoteItem
import com.etrade.mobilepro.quoteapi.FundsTradeSummaryItem
import com.etrade.mobilepro.quoteapi.FundsTradeTitleViewItem
import com.etrade.mobilepro.quoteapi.MutualFundPosition
import com.etrade.mobilepro.quoteapi.MutualFundTradeDetails
import com.etrade.mobilepro.quoteapi.MutualPositionType.CASH
import com.etrade.mobilepro.quoteapi.MutualPositionType.MARGIN
import com.etrade.mobilepro.quoteapi.ProductFundDetail
import com.etrade.mobilepro.quoteapi.QuoteChangeWidgetItem
import com.etrade.mobilepro.quoteapi.QuoteTemplateResources
import com.etrade.mobilepro.quoteapi.SalesChargeType
import com.etrade.mobilepro.quoteapi.isAffiliatedProduct
import com.etrade.mobilepro.util.android.parseSafeLocalTime
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.safeParseBigDecimal
import org.threeten.bp.format.DateTimeFormatter
import java.math.BigDecimal
import java.util.Locale
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private val MutualFundLabelRawFormatter = DateTimeFormatter.ofPattern("HHmm", Locale.US)
private val MutualFundLabelDisplayFormatter = DateTimeFormatter.ofPattern("hh:mm a 'ET'")
internal const val NO_FEE = "None"
private const val MARGIN_TYPE = "2"

class MutualFundsTradeQuoteMapper @Inject constructor(private val templateResources: QuoteTemplateResources) {

    fun map(jsonDto: MutualFundsTradeQuoteDetailsResponseDto): FundsTradeQuoteItem {
        val fundDetails = mapFundDetails(jsonDto)
        return mapFundDetailsSection(fundDetails)
    }

    @SuppressWarnings("LongMethod")
    private fun mapFundDetails(
        jsonDto: MutualFundsTradeQuoteDetailsResponseDto
    ): MutualFundTradeDetails {
        val fundDetail = jsonDto.fundDetailsMutual
        val selectedFund = jsonDto.selectedFundDetails?.fundDetailsList
        return MutualFundTradeDetails(
            productFundDetail = fundDetail?.productFundDetail?.toFundDetail(),
            timeOfLastTradeInMillis = fundDetail?.timeOfLastTrade,
            initialInvestment = fundDetail?.nonIraPurchaseRequirement?.minInitAmt,
            subsequentInvestment = fundDetail?.nonIraPurchaseRequirement?.balPayAmt,
            salesCharge = fundDetail?.salesCharge,
            fundRedemptionFee = fundDetail?.fundRedemptionFee,
            transactionFee = fundDetail?.transactionFee,
            etradeEarlyRedemptionFee = fundDetail?.etradeEarlyRedemptionFee,
            initialIra = fundDetail?.iraPurchaseRequirement?.minInitAmt,
            subsequentIra = fundDetail?.iraPurchaseRequirement?.balPayAmt,
            fundDescription = fundDetail?.fundDescription,
            frontLoadText = fundDetail?.frontLoadText,
            hasEarlyRedemptionFee = NO_FEE != fundDetail?.etradeEarlyRedemptionFee,
            isFrontEndFeesFund = "1" == fundDetail?.redemption?.isFrontEnd,
            isSaleChargedFund = "1" == fundDetail?.redemption?.isSales,
            netAssetValue = fundDetail?.netAssetValue,
            change = fundDetail?.change,
            changePercent = fundDetail?.percentChange,
            annualTotalReturn = fundDetail?.annualTotalReturn,
            sevenDayCurrentYield = fundDetail?.sevenDayCurrentYield,
            weightedAvgMaturity = fundDetail?.weightedAvgMaturity,
            deferredSalesCharges = fundDetail?.deferredSalesCharges?.toFundsFeeList(),
            frontEndSalesCharges = fundDetail?.frontEndSalesCharges?.toFundsFeeList(),
            positionDetails = selectedFund?.toPositions().orEmpty(),
            isFundOwned = selectedFund != null,
            affiliatedStatusCode = fundDetail?.affiliatedStatusCode
        )
    }

    private fun List<FeeStructure>.toFundsFeeList(): List<FundsFee>? = map {
        FundsFee(range = it.lowhigh, percent = it.percent)
    }

    private fun ProductFundDetailDto.toFundDetail(): ProductFundDetail? = ProductFundDetail(
        symbol = symbol,
        fundName = fundName,
        pricingParameterCd = PricingParameterCode.from(pricingParameterCd),
        cusip = cusip,
        streetStatus = streetStatus,
        ordCutOffTime = ordCutOffTime,
        isFrontLoadWaived = frontLoadWaived.toIntOrNull() == 2,
        typeCode = InstrumentType.from(typeCode)
    )

    @SuppressWarnings("LongMethod")
    private fun mapFundDetailsSection(it: MutualFundTradeDetails): FundsTradeQuoteItem {
        val title = "${it.productFundDetail?.symbol ?: templateResources.valueMissing} - ${it.productFundDetail?.fundName ?: templateResources.valueMissing}"

        val lastTradeTime = it.timeOfLastTradeInMillis?.formatTimeInMillisToTimeWithZone()

        val cutOffTime = it.productFundDetail?.ordCutOffTime?.formattedTimeStampValue()
        val changeBigDecimal = it.change?.safeParseBigDecimal() ?: BigDecimal.ZERO
        val changeVal = MarketDataFormatter.formatMarketDataCustom(changeBigDecimal)
        val changeColor = if (changeBigDecimal >= BigDecimal.ZERO) {
            templateResources.positiveChangeColorRes
        } else {
            templateResources.negativeChangeColorRes
        }
        val change = QuoteChangeWidgetItem(
            change = it.change ?: templateResources.valueMissing,
            changePercent = it.changePercent ?: templateResources.valueMissing
        )

        val headerViewItem = FundsTradeTitleViewItem(
            label = title,
            changeVal = change,
            changeColorVal = changeColor,
            netAssetValue = it.netAssetValue ?: templateResources.valueMissing,
            netAssetValueColor = templateResources.neutralChangeColorRes,
            timeStampLabel = it.timeOfLastTradeInMillis?.toFundTimeLabel() ?: templateResources.valueMissing,
            priceFormat = templateResources.priceFormat,
            priceWithoutSizeFormat = templateResources.sizeFormat,
            changeFormat = templateResources.changeFormat,
            contentDescriptionFormat = templateResources.contentDescriptionFormat,
            isAffiliatedProduct = it.isAffiliatedProduct()
        )

        val summaryItem = FundsTradeSummaryItem(
            annualAvgReturn = it.annualTotalReturn?.formattedPriceValue() ?: templateResources.valueMissing,
            instrumentType = it.productFundDetail?.typeCode ?: InstrumentType.MF,
            fundName = it.productFundDetail?.fundName ?: templateResources.valueMissing,
            frontLoadText = it.frontLoadText,
            change = String.format(templateResources.changeFormat, changeVal, change.changePercent),
            etradeEarlyRedemptionFee = it.etradeEarlyRedemptionFee ?: templateResources.valueMissing,
            netAssetValue = it.netAssetValue?.formattedAmountValue() ?: templateResources.valueMissing,
            lastTradeTime = lastTradeTime ?: templateResources.valueMissing,
            sevenDayCurrentYield = it.sevenDayCurrentYield?.formattedPriceValue() ?: templateResources.valueMissing,
            iraInitialInvestment = it.initialIra,
            iraSubsequentInvestment = it.subsequentIra,
            initialInvestment = it.initialInvestment?.formattedAmountValue() ?: templateResources.valueMissing,
            subsequentInvestment = it.subsequentInvestment?.formattedAmountValue() ?: templateResources.valueMissing,
            salesCharge = it.salesCharge ?: templateResources.valueMissing,
            salesChargeType = getSalesCharge(it),
            fundRedemptionFee = it.fundRedemptionFee?.formatIfAmount() ?: templateResources.valueMissing,
            transactionFee = it.transactionFee?.formatIfAmount() ?: templateResources.valueMissing,
            ordCutOffTime = cutOffTime ?: templateResources.valueMissing,
            deferredSalesCharge = it.deferredSalesCharges,
            frontEndSalesCharge = it.frontEndSalesCharges,
            weightedAvgMaturity = it.weightedAvgMaturity?.formattedPriceValue() ?: templateResources.valueMissing,
            positions = it.positionDetails,
            isFundOwned = it.isFundOwned
        )

        return FundsTradeQuoteItem(
            hasEarlyRedemptionFee = it.hasEarlyRedemptionFee,
            headerItem = headerViewItem,
            instrumentType = it.productFundDetail?.typeCode ?: InstrumentType.EQ,
            isFrontEndOrSalesFeeFund = (it.isFrontEndFeesFund && !it.isLoadWaived) || it.isSaleChargedFund,
            isLoadWaived = it.isLoadWaived,
            isNoLoadNoTransactionFeeFund = it.isNoLoadNoTransFeeFund,
            summaryItem = summaryItem,
            symbol = it.productFundDetail?.symbol ?: templateResources.valueMissing
        )
    }

    private fun getSalesCharge(it: MutualFundTradeDetails): SalesChargeType {
        return if (it.isFrontEndFeesFund && !it.isLoadWaived) {
            SalesChargeType.FRONT_END
        } else if (it.isFrontEndFeesFund && it.isLoadWaived) {
            SalesChargeType.FRONT_END_LOAD
        } else if (it.isSaleChargedFund) {
            SalesChargeType.BACK_END
        } else {
            SalesChargeType.NONE
        }
    }

    private fun Long.toFundTimeLabel(): String = DateFormattingUtils.formatToShortDate(TimeUnit.SECONDS.toMillis(this)).let {
        String.format(templateResources.nasdaqAsOfDateTimeFormat, it)
    }

    private fun String.formattedAmountValue(): String? = MarketDataFormatter.formatMarketDataWithCurrency(this.safeParseBigDecimal())

    private fun String.formatIfAmount(): String? = if (this != NO_FEE) {
        this.formattedAmountValue()
    } else {
        this
    }

    private fun String.formattedPriceValue(): String? = MarketDataFormatter.formatPriceToBeUpdated(this.safeParseBigDecimal())

    private fun List<FundDetails>.toPositions(): List<MutualFundPosition> = this.map {
        MutualFundPosition(
            type = if (it.positionType == MARGIN_TYPE) {
                MARGIN
            } else {
                CASH
            },
            quantity = it.quantity,
            marketValue = it.marketValue.formattedAmountValue() ?: templateResources.valueMissing
        )
    }
}

// Formats value "1600" to "4:00PM ET"
private fun String.formattedTimeStampValue(): String? {
    val localTime = this.parseSafeLocalTime(MutualFundLabelRawFormatter)
    return localTime?.format(MutualFundLabelDisplayFormatter)
}

private fun Long.formatTimeInMillisToTimeWithZone(): String {
    val timeInMillis = TimeUnit.SECONDS.toMillis(this)
    return DateFormattingUtils.formatTimeWithoutSecondsWithShowingTimezone(timeInMillis)
}
