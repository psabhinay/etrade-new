package com.etrade.mobilepro.usersubscriptiondata

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.usersubscriptionapi.MarketDataAgreementResponse
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.GET

interface UserSubscriptionApiClient {

    @JsonMoshi
    @GET("/webapiusr/rtq/getquoteagreement.json")
    suspend fun getMda(): MarketDataAgreementResponse

    @JsonMoshi
    @GET("/webapiusr/rtq/getusersubscription.json")
    suspend fun getSubscriptions(): ServerResponseDto<SubscriptionDataDto>
}
