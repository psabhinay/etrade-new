package com.etrade.mobilepro.instrument

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = false)
enum class InstrumentType(private vararg val typeCodes: String) {

    INDX("INDX"),
    EQ("EQ", "EQUITY"),
    OPTN("OPTN", "OPTION"),
    OPTNP("OPTNP"),
    OPTNC("OPTNC"),
    MF("MF", "MUTUAL_FUND"),
    MMF("MMF", "MONEY_MARKET_FUND"),
    BOND("BOND"),
    ETF("ETF"),
    UNKNOWN("UNKNOWN");

    val isIndex: Boolean
        get() = this == INDX
    val isOption: Boolean
        get() = this == OPTN || this == OPTNC || this == OPTNP
    val isMutualFundOrMoneyMarketFund: Boolean
        get() = this == MF || this == MMF
    val isMutualFundFund: Boolean
        get() = this == MF
    val isMoneyMarketFund: Boolean
        get() = this == MMF
    val isEquity: Boolean
        get() = this == EQ
    val updateWithStreamingData: Boolean
        get() = this != BOND
    val optionType: String?
        get() = when (this) {
            OPTNC -> "Call"
            OPTNP -> "Put"
            else -> null
        }
    val typeCode: String
        get() = typeCodes.first()

    companion object {

        val validStocksTradingTypes = setOf(EQ, ETF)
        val validStocksAndOptionTradingTypes = setOf(EQ, OPTN, OPTNP, OPTNC, ETF, UNKNOWN)
        val validOptionsTradingTypes = setOf(INDX, EQ, OPTN, OPTNP, OPTNC, ETF, UNKNOWN)
        val validMutualFundsTradingTypes = setOf(MF, MMF)

        /**
         * Creates instrument type from a [typeCode]. If no instrument type matches provided code the method returns [UNKNOWN] type.
         *
         * @param typeCode type code
         *
         * @return instrument type
         */
        fun from(typeCode: String?): InstrumentType {
            if (typeCode == null) {
                return UNKNOWN
            }
            return values().firstOrNull { it.typeCodes.contains(typeCode) } ?: UNKNOWN
        }
    }
}
