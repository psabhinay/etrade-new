package com.etrade.mobilepro.uiwidgets.actionsheet

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

class ActionSheetClicksViewModel @Inject constructor() : ViewModel() {

    private val _actionSheetClicks = MutableLiveData<ConsumableLiveEvent<ActionItem>>()
    val actionSheetClicks: LiveData<ConsumableLiveEvent<ActionItem>>
        get() = _actionSheetClicks

    private val _actionSheetCancelled = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val actionSheetCancelled: LiveData<ConsumableLiveEvent<Unit>>
        get() = _actionSheetCancelled

    fun onActionSheetClicked(item: ActionItem) {
        _actionSheetClicks.value = ConsumableLiveEvent(item)
    }

    fun onActionSheetCancelled() {
        _actionSheetCancelled.value = ConsumableLiveEvent(Unit)
    }
}
