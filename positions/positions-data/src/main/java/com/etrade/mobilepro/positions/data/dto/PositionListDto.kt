package com.etrade.mobilepro.positions.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PositionListDto(
    @Json(name = "accountUuid")
    internal val primaryAccountUuid: String?,
    @Json(name = "account_uuid")
    internal val secondaryAccountUuid: String?,
    @Json(name = "positions")
    val positions: List<PositionDto>
) {
    val accountUuid: String?
        get() = primaryAccountUuid ?: secondaryAccountUuid
}

@JsonClass(generateAdapter = true)
class PositionDto(
    @Json(name = "position_id")
    val id: String
)
