package com.etrade.mobilepro.uiwidgets.tablesectionsview.model

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorInt
import com.etrade.mobilepro.uiwidgets.tablesectionsview.databinding.ItemSectionMultilineRowBinding
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.etrade.mobilepro.util.formatDash

private const val SIXTY_PERCENT_WEIGHT = 0.6f
private const val FORTY_PERCENT_WEIGHT = 0.4f

class SummarySectionMultiLineItem(
    val label: String,
    val value: String,
    @ColorInt val textColor: Int?,
    val description: String? = null
) : TableSectionItemLayout {

    override fun getRowView(linearParams: LinearLayout.LayoutParams, parent: ViewGroup): View =
        parent.viewBinding(ItemSectionMultilineRowBinding::inflate).apply {
            root.layoutParams = linearParams
            tvLhs.text = label
            tvRhs.text = value
            val valueDescription = root.context.accountNameContentDescription(value)
            root.contentDescription =
                description?.formatDash() ?: "${label.formatDash()} $valueDescription"

            textColor?.let { color ->
                // text Color applied to the values only
                tvRhs.setTextColor(color)
            }
            tvLhs.applyViewParams(SIXTY_PERCENT_WEIGHT)
            tvRhs.applyViewParams(FORTY_PERCENT_WEIGHT)
        }.root

    private fun TextView.applyViewParams(weight: Float) {
        this.apply {
            layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, weight).apply { gravity = Gravity.BOTTOM }
        }
    }
}

fun createMultiLineSummarySection(label: String, value: String, @ColorInt color: Int? = null, description: String? = null): TableSectionItemLayout {
    return SummarySectionMultiLineItem(label, value, color, description)
}
