package com.etrade.mobilepro.searching.symbol.cache

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.searching.SearchingRealmModule
import com.etrade.mobilepro.searching.rest.SymbolSearchApiClient
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmConfiguration

class CachedSymbolsSearchDataDelegate(private val apiClient: SymbolSearchApiClient) : SimpleCachedResource.DataSourceDelegate<List<SearchResultItem.Symbol>, SearchQuery> {
    private val realmConfig = RealmConfiguration.Builder()
        .name("symbols.search")
        .modules(SearchingRealmModule())
        .build()

    override fun fetchFromNetwork(id: SearchQuery): Single<List<SearchResultItem.Symbol>> {
        return apiClient.searchSymbol(id.value.trim())
            .map {
                it.data?.payload ?: emptyList()
            }.map {
                it.map { item ->
                    SearchResultItem.Symbol(
                        title = item.name ?: "",
                        instrumentId = item.instrumentId ?: "",
                        description = item.description ?: "",
                        type = item.type?.let { symbolType -> InstrumentType.valueOf(symbolType) } ?: InstrumentType.UNKNOWN,
                        cachedAt = System.currentTimeMillis(),
                        queryValue = id.value,
                        displaySymbol = item.name ?: "",
                        securityType = item.securityType ?: ""
                    )
                }
            }
    }

    override fun fetchFromDb(id: SearchQuery): Single<List<SearchResultItem.Symbol>> {
        return Single.fromCallable {
            Realm.getInstance(realmConfig).use { realm ->
                realm.where(SymbolDb::class.java)
                    .equalTo("queryValue", id.value)
                    .findAll()
                    .map {
                        realm.copyFromRealm(it)
                    }
            }.map {
                SearchResultItem.Symbol(
                    title = it.title,
                    instrumentId = it.instrumentId,
                    description = it.description,
                    type = InstrumentType.valueOf(it.type),
                    cachedAt = it.cachedAt,
                    queryValue = it.queryValue,
                    displaySymbol = it.displaySymbol,
                    securityType = it.securityType
                )
            }
        }
    }

    override fun putToCache(fetchedResult: List<SearchResultItem.Symbol>): Single<List<SearchResultItem.Symbol>> {
        return Single.fromCallable {
            Realm.getInstance(realmConfig).use { autoClosableRealm ->
                autoClosableRealm.executeTransaction { realm ->
                    val listToSave = fetchedResult.map {
                        SymbolDb(
                            title = it.title,
                            instrumentId = it.instrumentId,
                            description = it.description,
                            type = it.type.typeCode,
                            cachedAt = System.currentTimeMillis(),
                            queryValue = it.queryValue,
                            displaySymbol = it.displaySymbol,
                            securityType = it.securityType,
                            settlementSessionCd = it.settlementSessionCd.code
                        )
                    }
                    realm.insertOrUpdate(listToSave)
                }
            }
            fetchedResult
        }
    }
}
