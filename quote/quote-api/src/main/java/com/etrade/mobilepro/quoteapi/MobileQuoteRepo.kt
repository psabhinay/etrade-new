package com.etrade.mobilepro.quoteapi

import io.reactivex.Observable

interface MobileQuoteRepo {

    /**
     * Performs a request for a quote, with default retry policy. If response contains more than one quote, returns only first one.
     *
     * @param symbolTitle Symbol name to look quote for
     * @param requireEarningsDate Boolean flag, which determines to require earnings date from server or not
     * @param extendedHours Boolean flag, which determines to require extended hours from server or not
     * @param requireExchangeCode Boolean flag, determines if exchange code should be included - (per 603c1 rule trade screen needs to display exchange code)
     * @param autoRetry Boolean flag, determines if repo will autoretry fetching with default logic
     */
    @Suppress("LongParameterList") // every parameter is required
    fun loadMobileQuote(
        symbolTitle: String,
        requireEarningsDate: Boolean = true,
        extendedHours: Boolean = false,
        requireExchangeCode: Boolean = false
    ): Observable<MobileQuote>

    /**
     * Performs a request for a quotes, with default retry policy.
     *
     * @param symbols Symbols' names to look quotes for
     * @param requireEarningsDate Boolean flag, which determines to require earnings date from server or not
     * @param extendedHours Boolean flag, which determines to require extended hours from server or not
     * @param requireExchangeCode Boolean flag, determines if exchange code should be included - (per 603c1 rule trade screen needs to display exchange code)
     * @param autoRetry Boolean flag, determines if repo will autoretry fetching with default logic
     */
    @Suppress("LongParameterList") // every parameter is required
    fun loadMobileQuotes(
        symbols: String,
        requireEarningsDate: Boolean = true,
        extendedHours: Boolean = false,
        requireExchangeCode: Boolean = false
    ): Observable<List<MobileQuote>>
}
