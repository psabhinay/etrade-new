package com.etrade.mobilepro.trade.form.api

import com.etrade.mobilepro.instrument.InstrumentType

enum class SecurityType {
    CONDITIONAL,
    MUTUAL_FUND,
    OPTION,
    STOCK,
    STOCK_AND_OPTIONS
}

fun InstrumentType.toSecurityType(): SecurityType {
    return when {
        isMutualFundOrMoneyMarketFund -> SecurityType.MUTUAL_FUND
        isOption -> SecurityType.OPTION
        else -> SecurityType.STOCK
    }
}
