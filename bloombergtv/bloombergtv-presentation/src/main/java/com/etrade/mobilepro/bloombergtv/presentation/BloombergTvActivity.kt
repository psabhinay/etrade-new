package com.etrade.mobilepro.bloombergtv.presentation

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.baseactivity.ActivitySessionDelegate
import com.etrade.mobilepro.bloombergtv.common.ViewState
import com.etrade.mobilepro.bloombergtv.presentation.databinding.ActivityBloombergtvBinding
import com.etrade.mobilepro.bloombergtv.presentation.delegate.CallStateDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.ConnectionNotifierDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.DrawOverlayRequestDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.FullScreenModeArrangerDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.MediaPlayerDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.ServicesStateDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.VendorSpecificAdjustmentsDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.VideoControllerDelegate
import com.etrade.mobilepro.common.LoginChecker
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.viewmodel.SnackbarViewModel
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.android.AndroidInjection
import javax.inject.Inject

private const val INTERNATIONAL_USER_ERROR_MARK = "non-US"

@RequireLogin
class BloombergTvActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var snackbarViewModel: SnackbarViewModel

    @Inject
    lateinit var snackbarUtilFactory: SnackbarUtilFactory

    @Inject
    lateinit var activitySessionDelegate: ActivitySessionDelegate

    @Inject
    lateinit var loginChecker: LoginChecker

    internal val binding by viewBinding(ActivityBloombergtvBinding::inflate)

    internal val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ this }, { binding.bloombergtvContainer }) }

    private val viewModel: BloombergTvViewModel by viewModels { viewModelFactory }

    private lateinit var mediaPlayerDelegate: MediaPlayerDelegate

    private lateinit var callStateDelegate: CallStateDelegate

    private lateinit var connectionNotifierDelegate: ConnectionNotifierDelegate

    private val vendorSpecificAdjustmentsDelegate = VendorSpecificAdjustmentsDelegate()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        initDelegates()
        bindViewModel()
    }

    private fun initDelegates() {
        val videoControllerDelegate = VideoControllerDelegate(
            drawOverlayRequestDelegate = DrawOverlayRequestDelegate(),
            viewModel = viewModel
        )
        mediaPlayerDelegate = MediaPlayerDelegate(viewModel, videoControllerDelegate)
        callStateDelegate = CallStateDelegate(onCallStarted = mediaPlayerDelegate::pause)
        connectionNotifierDelegate = ConnectionNotifierDelegate(onConnectivityChange = mediaPlayerDelegate::enablePausePlayButton)
        activitySessionDelegate.observeTimeoutEvent(this)
        listOf(
            FullScreenModeArrangerDelegate(),
            callStateDelegate,
            connectionNotifierDelegate,
            ServicesStateDelegate(),
            mediaPlayerDelegate,
            vendorSpecificAdjustmentsDelegate
        ).forEach { it.init(this) }
    }

    override fun onStart() {
        super.onStart()
        loginChecker.check(this)
        mediaPlayerDelegate.preparePlayer()
    }

    override fun onResume() {
        super.onResume()
        mediaPlayerDelegate.start()
    }

    override fun onPause() {
        mediaPlayerDelegate.pause()
        super.onPause()
    }

    override fun onStop() {
        mediaPlayerDelegate.releasePlayer()
        super.onStop()
    }

    override fun onDestroy() {
        vendorSpecificAdjustmentsDelegate.dismantle()
        callStateDelegate.dismantle()
        super.onDestroy()
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        activitySessionDelegate.onUserInteraction(this)
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(this) {
            when (it) {
                ViewState.Loading -> {
                    binding.loadingIndicator.visibility = View.VISIBLE
                }
                is ViewState.Success -> {
                    handleSuccess()
                }
                is ViewState.Failure -> {
                    handleFailure(it)
                }
            }
        }
        viewModel.init()
    }

    private fun handleFailure(it: ViewState.Failure) {
        binding.loadingIndicator.visibility = View.GONE
        if (it.error.contains(INTERNATIONAL_USER_ERROR_MARK)) {
            MaterialAlertDialogBuilder(this)
                .setCancelable(false)
                .setMessage(getString(R.string.bloomberg_international_error_message))
                .setPositiveButton(R.string.ok) { _, _ -> onBackPressed() }
                .show()
        } else {
            snackbarViewModel.postAppMessage(getString(R.string.error_message_general))
            onBackPressed()
        }
    }

    private fun handleSuccess() {
        vendorSpecificAdjustmentsDelegate.handleVendorSpecifics()
    }
}
