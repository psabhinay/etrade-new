package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.common.RestorableState
import com.etrade.mobilepro.common.RestorableViewModelDelegate
import com.etrade.mobilepro.common.RestorableViewModelDelegateImpl
import com.etrade.mobilepro.common.RestoreTabPositionDelegate
import com.etrade.mobilepro.common.RestoreTabPositionDelegateImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class RestorableModule {

    @Binds
    abstract fun bindRestorableViewModelDelegate(delegate: RestorableViewModelDelegateImpl): RestorableViewModelDelegate

    @Binds
    abstract fun bindRestoreTabPositionDelegate(delegate: RestoreTabPositionDelegateImpl): RestoreTabPositionDelegate

    companion object {
        @Provides
        @Singleton
        internal fun providesRestorableState(): RestorableState = RestorableState()
    }
}
