package com.etrade.mobilepro.portfolio.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.etrade.mobilepro.portfolio.GetPortfolioNextPageParameter
import com.etrade.mobilepro.portfolio.GetPortfolioNextPageResult
import com.etrade.mobilepro.portfolio.GetPortfolioNextPageUseCase
import com.etrade.mobilepro.portfolio.model.PageLoadingState
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.util.executeBlocking
import java.lang.IllegalStateException

class PositionIdDataSource(
    private val accountUuid: String,
    private val sortInfo: SortInfo,
    private val initialPositionIds: List<String>,
    private val initialNextPageToken: String?,
    private val getPortfolioNextPageUseCase: GetPortfolioNextPageUseCase
) : PagingSource<String, String>() {

    val hasNextPage: Boolean
        get() = nextPageToken != null

    val pageLoadingState: LiveData<PageLoadingState>
        get() = _pageLoadingState

    private val _pageLoadingState: MutableLiveData<PageLoadingState> = MutableLiveData(PageLoadingState.Idle(false))

    private var nextPageToken: String? = initialNextPageToken

    override fun getRefreshKey(state: PagingState<String, String>): String? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.nextKey
        }
    }

    override suspend fun load(params: LoadParams<String>): LoadResult<String, String> {
        _pageLoadingState.postValue(PageLoadingState.Loading)
        val result = if (params.key == null) {
            GetPortfolioNextPageResult(initialPositionIds, initialNextPageToken)
        } else {
            getPortfolioNextPageUseCase.executeBlocking(
                GetPortfolioNextPageParameter(
                    accountUuid = accountUuid,
                    sortInfo = sortInfo,
                    count = params.loadSize,
                    nextPageToken = params.key ?: initialNextPageToken
                )
            ).data
        }

        return if (result == null) {
            _pageLoadingState.postValue(PageLoadingState.Error)
            LoadResult.Error(IllegalStateException("empty result"))
        } else {
            nextPageToken = result.nextPageToken
            _pageLoadingState.postValue(PageLoadingState.Idle(result.nextPageToken != null))
            LoadResult.Page(
                result.positionIds,
                prevKey = null,
                nextKey = result.nextPageToken
            )
        }
    }
}
