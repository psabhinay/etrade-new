package com.etrade.mobilepro.alerts.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.alerts.api.model.AlertAction
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.presentation.adapter.delegate.AlertAdapterDelegate
import com.etrade.mobilepro.alerts.presentation.adapter.delegate.AlertDividerAdapterDelegate
import com.etrade.mobilepro.alerts.presentation.adapter.delegate.LoadingIndicatorAdapterDelegate
import com.etrade.mobilepro.alerts.presentation.adapter.viewholder.AlertViewHolder
import com.etrade.mobilepro.common.customview.SwipeRevealLayout
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class AlertsAdapter(
    private val thresholdValue: Int,
    private val thresholdReachedListener: (() -> Unit)?,
    readStatusChecker: ((AlertItem.Alert) -> Boolean)?,
    alertListener: ((AlertItem.Alert, AlertAction) -> Unit)?,
    swipeRevealListener: ((AlertItem.Alert, SwipeRevealLayout.Action) -> Unit)?,
    dragLocker: (() -> Boolean)?
) : ListDelegationAdapter<MutableList<AlertItem>>() {
    private var thresholdWasNotifiedOnce = false
    private var itemsAreDirty = false
    init {
        delegatesManager.addDelegate(AlertDividerAdapterDelegate())
        delegatesManager.addDelegate(AlertAdapterDelegate(readStatusChecker, alertListener, swipeRevealListener, dragLocker))
        delegatesManager.addDelegate(LoadingIndicatorAdapterDelegate())
        items = mutableListOf()
    }

    fun markDirty() { // we do not need diff utils on next submit and want to rebind all viewholders on refresh
        itemsAreDirty = true
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        (holder as? AlertViewHolder)?.onDetached()
        super.onViewDetachedFromWindow(holder)
    }

    fun submitAlerts(alerts: List<AlertItem>) {
        thresholdWasNotifiedOnce = false
        if (itemsAreDirty) {
            items.clear()
            items.addAll(alerts)
            notifyDataSetChanged()
            itemsAreDirty = false
        } else {
            dispatchUpdates(items, alerts, AlertsDiffCallback(items, alerts))
            items.clear()
            items.addAll(alerts)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any?>) {
        if (!thresholdWasNotifiedOnce && position >= items.size - thresholdValue - 1) {
            thresholdReachedListener?.invoke()
            thresholdWasNotifiedOnce = true
        }
        super.onBindViewHolder(holder, position, payloads)
    }
}
