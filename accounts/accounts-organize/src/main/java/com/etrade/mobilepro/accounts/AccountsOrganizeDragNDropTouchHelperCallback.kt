package com.etrade.mobilepro.accounts

import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.editlist.DragNDropTouchHelperCallback
import com.etrade.mobilepro.editlist.EditListViewModel

private const val SCALE_FACTOR_GROW = 1.12F
private const val SCALE_FACTOR_SHRINK = 1F
private const val SCALE_ANIMATION_DURATION = 100L

class AccountsOrganizeDragNDropTouchHelperCallback(viewModel: EditListViewModel<*>) : DragNDropTouchHelperCallback(viewModel) {

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)

        shrinkView(viewHolder.itemView)
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        super.onSelectedChanged(viewHolder, actionState)

        if (actionState == ItemTouchHelper.ACTION_STATE_DRAG) {
            viewHolder?.itemView?.apply { growView(this) }
        }
    }

    private fun growView(view: View) {
        view.animate().scaleX(SCALE_FACTOR_GROW).scaleY(SCALE_FACTOR_GROW).duration = SCALE_ANIMATION_DURATION
    }

    private fun shrinkView(view: View) {
        view.animate().scaleX(SCALE_FACTOR_SHRINK).scaleY(SCALE_FACTOR_SHRINK).duration = SCALE_ANIMATION_DURATION
    }
}
