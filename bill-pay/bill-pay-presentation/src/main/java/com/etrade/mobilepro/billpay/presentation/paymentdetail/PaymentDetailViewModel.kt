package com.etrade.mobilepro.billpay.presentation.paymentdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import kotlinx.coroutines.launch
import javax.inject.Inject

class PaymentDetailViewModel @Inject constructor(
    private val repo: BillPayRepo
) : ViewModel() {

    private val _viewState = MutableLiveData<PaymentDetailViewState>()
    internal val viewState: LiveData<PaymentDetailViewState> get() = _viewState

    init {
        _viewState.value = PaymentDetailViewState()
    }

    internal fun cancelPayment(sessionId: String, paymentId: String) {
        _viewState.value = PaymentDetailViewState(isLoading = true)
        viewModelScope.launch {
            repo.cancelPayment(sessionId, paymentId).fold(
                onSuccess = {
                    _viewState.value = PaymentDetailViewState(paymentCancelled = ConsumableLiveEvent(Unit))
                },
                onFailure = {
                    _viewState.value = PaymentDetailViewState(error = ConsumableLiveEvent(Unit))
                }
            )
        }
    }
}

internal data class PaymentDetailViewState(
    val error: ConsumableLiveEvent<Unit>? = null,
    val isLoading: Boolean = false,
    val paymentCancelled: ConsumableLiveEvent<Unit>? = null
)
