package com.etrade.mobilepro.data.mapper

import com.etrade.mobilepro.alerts.api.model.StatusValue
import com.etrade.mobilepro.alerts.api.model.StatusValue.AlertStatus.NONE
import com.etrade.mobilepro.alerts.api.model.UserAlertStringResources
import com.etrade.mobilepro.alerts.data.dto.SetAlertDetailXmlDto
import com.etrade.mobilepro.alerts.data.dto.SetAlertResponseXmlDto
import com.etrade.mobilepro.alerts.data.mapper.SetAlertDetailDtoMapper
import com.etrade.mobilepro.alerts.data.mapper.SetAlertDtoMapper
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

private const val errorMessage = "This is the error message"
private val setAlertResponse
    get() = SetAlertResponseXmlDto(
        "true",
        errorMessage,
        "true",
        listOf()
    )
private val userAlertsStrings = UserAlertStringResources(
    "targetPrice",
    "dailyPercent",
    "peTarget",
    "dailyVolume",
    "news"
)
private const val testSymbol = "Symbol"
private val xmlDto = SetAlertDetailXmlDto(
    "delete",
    "lastEdited",
    testSymbol,
    "deleteInfo",
    "content",
    "edit",
    "peTargetHigh",
    "none",
    "targetPriceLow",
    "none",
    "dailyPercentHigh",
    "none",
    "targetPriceHigh",
    "none",
    "peTargetLow",
    "none",
    "dailyVolume",
    "none",
    "dailyPercentLow",
    "none"
)

internal class SetAlertDtoMapperTest {
    private val mapper = SetAlertDtoMapper(userAlertsStrings)

    @Test
    fun `mapper should work`() {
        val output = mapper.map(setAlertResponse)

        assertTrue(output.hasError)
        assertEquals(errorMessage, output.errorMessage)
        assertTrue(output.hasAlerts)
        println(output.alerts.map { it.hashCode() })
        assertTrue(output.alerts.isEmpty())
    }

    @Test
    fun `should ignore null alerts`() {
        val input = setAlertResponse.apply {
            alerts = listOf(null, xmlDto, null, xmlDto)
        }

        val output = mapper.map(input)

        assertEquals(2, output.alerts.size)
    }
}

internal class SetAlertDetailDtoMapperTest {
    private val mapper = SetAlertDetailDtoMapper(userAlertsStrings)

    @Test
    fun `mapper should work`() {
        val output = mapper.map(xmlDto)

        assertEquals(testSymbol, output?.symbol)
        assertEquals("content", output?.content)
        assertEquals("delete", output?.delete)
        assertEquals("deleteInfo", output?.deleteInfo)
        assertEquals("edit", output?.edit)
        assertEquals("lastEdited", output?.lastEdited)

        assertEquals(StatusValue("targetPriceHigh", NONE), output?.targetPriceHigh)
        assertEquals(StatusValue("targetPriceLow", NONE), output?.targetPriceLow)
        assertEquals(StatusValue("dailyPercentHigh", NONE), output?.dailyPercentHigh)
        assertEquals(StatusValue("dailyPercentLow", NONE), output?.dailyPercentLow)
        assertEquals(StatusValue("peTargetHigh", NONE), output?.peTargetHigh)
        assertEquals(StatusValue("peTargetLow", NONE), output?.peTargetLow)
        assertEquals(StatusValue("dailyVolume", NONE), output?.dailyVolume)

        assertEquals("$testSymbol: ", output?.description)
        assertEquals(false, output?.isDeleteBtnVisible)
    }

    @Test
    fun `should show targets when high status are enabled`() {
        val input = SetAlertDetailXmlDto().apply {
            symbol = testSymbol
            targetPriceHighStatus = "enabled"
            dailyPercentHighStatus = "enabled"
            peTargetHighStatus = "enabled"
        }

        val output = mapper.map(input)

        userAlertsStrings.let {
            assertEquals(
                "$testSymbol: ${it.targetPrice}, ${it.dailyPercent}, ${it.peTarget}",
                output?.description
            )
        }
    }

    @Test
    fun `should show targets when low status are enabled`() {
        val input = SetAlertDetailXmlDto().apply {
            symbol = testSymbol
            targetPriceLowStatus = "enabled"
            dailyPercentLowStatus = "enabled"
            peTargetLowStatus = "enabled"
        }

        val output = mapper.map(input)

        userAlertsStrings.let {
            assertEquals(
                "$testSymbol: ${it.targetPrice}, ${it.dailyPercent}, ${it.peTarget}",
                output?.description
            )
        }
    }

    @Test
    fun `should show dailyVolume if dailyVolume status is enabled`() {
        val input = SetAlertDetailXmlDto().apply {
            symbol = testSymbol
            dailyVolumeStatus = "enabled"
        }

        val output = mapper.map(input)

        assertEquals(
            "$testSymbol: ${userAlertsStrings.dailyVolume}",
            output?.description
        )
    }
}
