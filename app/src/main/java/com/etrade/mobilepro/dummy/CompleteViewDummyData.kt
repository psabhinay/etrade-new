package com.etrade.mobilepro.dummy

import com.etrade.completeview.dto.AccountSummaryDto
import com.etrade.completeview.dto.AccountSummarySectionDto
import com.etrade.completeview.dto.AccountSummaryViewDto
import com.etrade.mobilepro.accountslist.data.dto.AccountReferenceDto
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.backends.mgs.MOVEMENT_TYPE_NEUTRAL
import com.etrade.mobilepro.backends.mgs.StreamingValueDto
import java.util.Random

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 *
 * TODO: Replace all uses of this class before publishing your app.
 */
@SuppressWarnings("MagicNumber")
object CompleteViewDummyData {

    /**
     * An array of sample (dummy) items.
     */
    val ITEMS: MutableList<AccountSummaryViewDto> = mutableListOf()

    val REFERENCE_DTOS: MutableList<AccountReferenceDto> = mutableListOf()

    private const val COUNT = 30

    private val random = Random()

    init {
        // Add some sample items.
        for (i in 1..COUNT) {
            ITEMS.add(createDummyBankSummary(i, "Brokerage Account - $i"))
        }

        for (i in 1..COUNT) {
            REFERENCE_DTOS.add(createDummyAccountReferenceDto())
        }
    }

    fun createDummySummary(accountId: String?): List<AccountSummaryViewDto> {
        return listOf(createDummyBankSummary(Integer.valueOf(accountId ?: "0"), null, true))
    }

    fun createDummyBankSummary(position: Int, name: String?, makeTwoCtas: Boolean = false): AccountSummaryViewDto {
        return AccountSummaryViewDto(
            AccountSummaryDto(
                accountUuid = position.toString(),
                name = name,
                accountSourceLabel = null,
                detailLabel = "Total Balance",
                detailValue = getFormattedCurrency(),
                additionalSectionDtos = makeBankDetails()
            ),
            if (makeTwoCtas) makeTwoCtas() else null
        )
    }

    private fun makeBankDetails(): List<AccountSummarySectionDto> {
        return listOf(
            AccountSummarySectionDto(
                title = "Available Balance",
                value = StreamingValueDto(
                    initial = getFormattedCurrency(),
                    streamId = null,
                    movementType = null
                ),
                valueDetail = null
            ),
            AccountSummarySectionDto(
                title = "YTD Interest Paid",
                value = StreamingValueDto(
                    initial = getFormattedCurrency(),
                    streamId = null,
                    movementType = null
                ),
                valueDetail = null
            )
        )
    }

    private fun makeDetails(): List<AccountSummarySectionDto> {
        val builder = mutableListOf<AccountSummarySectionDto>()
        val sectionCount = if (random.nextBoolean()) 2 else 3
        for (i in 0 until sectionCount) {
            val (label, value, detail) = when (i) {
                0 -> Triple(
                    "Day's Gain",
                    getFormattedCurrency(),
                    getPercentage()
                )
                1 -> Triple(
                    "Total Gain",
                    getFormattedCurrency(),
                    getPercentage()
                )
                2 -> Triple("Cash", getFormattedCurrency(), null)
                else -> Triple("???", "???", "???")
            }
            builder.add(
                AccountSummarySectionDto(
                    label,
                    StreamingValueDto(initial = value, streamId = null, movementType = MOVEMENT_TYPE_NEUTRAL), detail
                )
            )
        }
        return builder
    }

    private fun makeCta() = listOf(
        CallToActionDto(
            "View Portfolio",
            ClickActionDto("etrade://account/63468497/portfolio")
        )
    )

    private fun makeTwoCtas() = listOf(
        CallToActionDto("Open Orders (3)", ClickActionDto("etrade://account/63468497/orders/open")),
        CallToActionDto("Saved Orders (4)", ClickActionDto("etrade://account/63468497/orders/saved"))
    )
}
