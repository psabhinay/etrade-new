@file:JvmName("CalendarEvents")

package com.etrade.mobilepro.quote.screen.presentation.util

import android.content.Context
import com.etrade.mobilepro.marketscalendar.util.getCalendarEventTitle
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.util.android.CalendarEvent
import com.etrade.mobilepro.util.android.addToCalendar

fun addToCalendar(context: Context, quoteItem: QuoteItem.QuoteDetailItem) {
    val nextEarningsDate = requireNotNull(quoteItem.nextEarningsDateRaw)
    addToCalendar(context, CalendarEvent(context.resources.getCalendarEventTitle(quoteItem.symbol), nextEarningsDate))
}
