package com.etrade.mobilepro.ui.multitoggle

import android.content.res.Resources

@Suppress("ComplexMethod")
internal fun mapToggleAccessibility(res: Resources, title: CharSequence?): String {
    val resId: Int? = when (title) {
        "1D" -> R.string.tab_1d_description
        "1W" -> R.string.tab_1w_description
        "1M" -> R.string.tab_1m_description
        "3M" -> R.string.tab_3m_description
        "6M" -> R.string.tab_6m_description
        "1Y" -> R.string.tab_1y_description
        "2Y" -> R.string.tab_2y_description
        "3Y" -> R.string.tab_3y_description
        "ETF" -> R.string.tab_etf_description
        else -> null
    }
    val titleText = resId?.let {
        res.getString(it)
    } ?: title.toString()

    return res.getString(R.string.label_button, titleText)
}
