package com.etrade.mobilepro.appwidget.common

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import com.etrade.eo.core.util.MarketDataFormatter.formatMarketData
import com.etrade.eo.core.util.MarketDataFormatter.formatMarketPercData
import com.etrade.mobilepro.appwidget.R
import com.etrade.mobilepro.appwidget.api.WatchlistEntryTuple
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.widgetcontainer.DefaultAppWidgetProvider
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.util.android.ChangeColorPicker
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import org.threeten.bp.LocalDate
import java.math.BigDecimal

internal fun Context.getPlacedWidgetIds(): IntArray =
    ComponentName(this, DefaultAppWidgetProvider::class.java)
        .let(AppWidgetManager.getInstance(this)::getAppWidgetIds)

internal fun getIndexChangeColorPicker(): ChangeColorPicker = object : ChangeColorPicker {
    override fun getChangeColor(changeValue: BigDecimal?): Int = changeValue?.let {
        when (it.signum()) {
            -1 -> R.color.appwidget_index_change_loss
            else -> R.color.appwidget_index_change_profit
        }
    } ?: R.color.white
}

internal fun formatIndexChange(
    context: Context,
    change: BigDecimal?,
    changePercent: BigDecimal?,
    splitInTwoLines: Boolean = false,
): String {
    val changeValue = formatMarketData(change)
    val changePercentValue = formatMarketPercData(changePercent)
    val changesString = if (splitInTwoLines) {
        R.string.appwidget_index_changes_placeholder_2_lines
    } else {
        R.string.appwidget_index_changes_placeholder
    }
    return context.getString(changesString, changeValue, changePercentValue)
}

internal fun List<WatchlistEntry>.toTuples(): List<WatchlistEntryTuple> = map { it to null }

internal fun List<WatchlistEntryTuple>.getEntries(): List<WatchlistEntry> = map { (entry, _) -> entry }

internal fun getLayoutResId(layoutSettings: WidgetSettings.Layout): Int {
    return when (layoutSettings) {
        WidgetSettings.Layout.TILES -> R.id.gvWatchlist
        WidgetSettings.Layout.LIST -> R.id.lvWatchlist
    }
}

@Suppress("LongMethod", "LongParameterList")
internal fun Level1Data.copy(
    ask: String? = this.ask,
    askExchange: String? = this.askExchange,
    askSize: String? = this.askSize,
    bid: String? = this.bid,
    bidExchange: String? = this.bidExchange,
    bidSize: String? = this.bidSize,
    change: String? = this.change,
    changePercent: String? = this.changePercent,
    closingMark: String? = this.closingMark,
    dayHigh: String? = this.dayHigh,
    dayLow: String? = this.dayLow,
    earningsPerShare: String? = this.earningsPerShare,
    lastPrice: String? = this.lastPrice,
    lastPriceExchange: String? = this.lastPriceExchange,
    lastVolume: String? = this.lastVolume,
    mark: String? = this.mark,
    openPrice: String? = this.openPrice,
    previousClosePrice: String? = this.previousClosePrice,
    priceToEarnings: String? = this.priceToEarnings,
    timestampSinceEpoch: Long? = this.timestampSinceEpoch,
    volume: String? = this.volume,
    tickIndicator: Short? = this.tickIndicator,
    yearHigh: String? = this.yearHigh,
    yearHighDate: LocalDate? = this.yearHighDate,
    yearLow: String? = this.yearLow,
    yearLowDate: LocalDate? = this.yearLowDate
): Level1Data {
    return object : Level1Data {
        override val ask: String? = ask
        override val askExchange: String? = askExchange
        override val askSize: String? = askSize
        override val bid: String? = bid
        override val bidExchange: String? = bidExchange
        override val bidSize: String? = bidSize
        override val change: String? = change
        override val changePercent: String? = changePercent
        override val closingMark: String? = closingMark
        override val dayHigh: String? = dayHigh
        override val dayLow: String? = dayLow
        override val earningsPerShare: String? = earningsPerShare
        override val lastPrice: String? = lastPrice
        override val lastPriceExchange: String? = lastPriceExchange
        override val lastVolume: String? = lastVolume
        override val mark: String? = mark
        override val openPrice: String? = openPrice
        override val previousClosePrice: String? = previousClosePrice
        override val priceToEarnings: String? = priceToEarnings
        override val timestampSinceEpoch: Long? = timestampSinceEpoch
        override val volume: String? = volume
        override val tickIndicator: Short? = tickIndicator
        override val yearHigh: String? = yearHigh
        override val yearHighDate: LocalDate? = yearHighDate
        override val yearLow: String? = yearLow
        override val yearLowDate: LocalDate? = yearLowDate
    }
}
