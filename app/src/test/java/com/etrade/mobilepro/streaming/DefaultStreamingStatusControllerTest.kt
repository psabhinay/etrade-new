package com.etrade.mobilepro.streaming

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.appwidget.AppWidgetHandler
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdateListener
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test

class DefaultStreamingStatusControllerTest {

    private val user = mock<User>()
    private val keyValStorage = mock<KeyValueStorage>()
    private val appWidgetHandler: AppWidgetHandler = mock()
    private val tested = DefaultStreamingStatusController(appWidgetHandler, user, keyValStorage)

    @Test
    fun `invoke listener if subscribed to streaming status and value changes`() {
        val listener: StreamingStatusUpdateListener = mock()
        tested.subscribeForStreamingStatusUpdates(listener)
        whenever(user.isRealtimeUser).thenReturn(true)
        whenever(keyValStorage.getBooleanValue(any(), any())).thenReturn(false)

        tested.isStreamingToggleEnabled = true

        verify(listener).invoke(eq(StreamingStatusUpdate.StreamingToggleUpdate(true)))
        verify(appWidgetHandler).refresh()
    }
}
