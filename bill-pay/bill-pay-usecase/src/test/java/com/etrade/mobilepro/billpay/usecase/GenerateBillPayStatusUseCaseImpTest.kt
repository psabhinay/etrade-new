package com.etrade.mobilepro.billpay.usecase

import com.etrade.mobile.accounts.billpay.BillPay
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.BillPayStatus
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.math.BigInteger

class GenerateBillPayStatusUseCaseImpTest {

    @Test
    fun `empty account list generate non-enrolled status`() {
        testData(
            emptyList(),
            expected = BillPayStatus.NotEnrolled
        )
    }

    @Test
    fun `enrolled accounts generate supported status`() {
        testData(
            TestAccount(isEnrolled = true),
            TestAccount(isEnrolled = true),
            expected = BillPayStatus.Supported
        )
    }

    @Test
    fun `enrolled accounts with restricted and non-eligible accounts generate supported status`() {
        testData(
            TestAccount(isEnrolled = true),
            TestAccount(isEnrolled = true),
            TestAccount(isEligible = false, isEnrolled = false),
            TestAccount(isRestricted = true, isEnrolled = true),
            expected = BillPayStatus.Supported
        )
    }

    @Test
    fun `eligible accounts generate non-enrolled status`() {
        testData(
            TestAccount(isEligible = true),
            TestAccount(isEligible = true),
            expected = BillPayStatus.NotEnrolled
        )
    }

    @Test
    fun `eligible accounts with restricted and non-eligible accounts generate non-enrolled status`() {
        testData(
            TestAccount(isEligible = true),
            TestAccount(isEligible = true),
            TestAccount(isRestricted = true, isEnrolled = true),
            TestAccount(isEnrolled = false, isEligible = false),
            expected = BillPayStatus.NotEnrolled
        )
    }

    @Test
    fun `enrolled restricted accounts generate non-eligible status`() {
        testData(
            TestAccount(isEnrolled = true, isRestricted = true),
            TestAccount(isEnrolled = true, isRestricted = true),
            expected = BillPayStatus.NotEligible
        )
    }

    @Test
    fun `eligible restricted accounts generate non-eligible status`() {
        testData(
            TestAccount(isEligible = true, isRestricted = true),
            TestAccount(isEligible = true, isRestricted = true),
            expected = BillPayStatus.NotEligible
        )
    }

    @Test
    fun `non-eligible accounts generate non-eligible status`() {
        testData(
            TestAccount(isEligible = false),
            TestAccount(isEligible = false),
            expected = BillPayStatus.NotEligible
        )
    }

    private fun testData(vararg accountList: AccountDetails, expected: BillPayStatus) {
        testData(accountList.toList(), expected)
    }

    private fun testData(accountList: List<AccountDetails>, expected: BillPayStatus) {
        val useCase = GenerateBillPayStatusUseCaseImp()

        val result = runBlocking { useCase.execute(accountList.toList()) }

        assertNotNull(result)
        assertTrue(result.isSuccess)
        assertEquals(expected, result.getOrThrow())
    }
}

private class TestAccount(
    isRestricted: Boolean = false,
    isEnrolled: Boolean = false,
    isEligible: Boolean = false
) : AccountDetails {
    override val nickname: String? = ""
    override val instNo: BigInteger = BigInteger.ZERO
    override val accountCategory: String = ""
    override val accountId: String = ""
    override val accountIdKey: String = ""
    override val maskedAccountId: String = ""
    override val accountMode: String = ""
    override val accountName: String = ""
    override val accountShortName: String = ""
    override val accountLongName: String = ""
    override val mobileDescription: String = ""
    override val accountValue: BigDecimal? = null
    override val accountType: String = ""
    override val institutionType: String = ""
    override val accountStatus: String = ""
    override val restrictionLevel: String = ""
    override val closedDate: Long = 0L
    override val brokerage: AccountDetails.Brokerage? = null
    override val billPay: BillPay? = BillPay(
        isRestricted = isRestricted,
        isEligible = isEligible,
        isEnrolled = isEnrolled,
        isPrimary = false
    )
}
