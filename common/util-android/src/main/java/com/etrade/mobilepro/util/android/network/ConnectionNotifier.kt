package com.etrade.mobilepro.util.android.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import androidx.annotation.AnyThread
import androidx.core.content.getSystemService
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.util.debounce
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(ConnectionNotifier::class.java)
private const val CONNECTIVITY_DEBOUNCE_TIME_MS = 500L
private const val DELAY_FOR_CONNECTIVITY_STATUS_MS = 500L

class ConnectionNotifier(private val context: Context) {

    private val connectivityManager: ConnectivityManager?
        get() = context.getSystemService()

    var connected: Boolean = false
    var connectionType: String? = null

    fun createNotifier(connectionNotification: ConnectionNotification): LifecycleObserver? =
        connectivityManager?.let {
            ConnectionObserver(it, connectionNotification)
        }

    @AnyThread
    interface ConnectionNotification {
        fun onNetworkAvailable()
        fun onNetworkLost()
    }

    private inner class ConnectionObserver(
        private val connectivityManager: ConnectivityManager,
        private val connectionNotification: ConnectionNotification
    ) : DefaultLifecycleObserver {

        private val mainScope = MainScope()

        private val debouncedConnectivityNotifier = debounce<Boolean>(
            waitMs = CONNECTIVITY_DEBOUNCE_TIME_MS,
            coroutineScope = mainScope
        ) { isConnected ->
            logger.debug("Network connected: $isConnected")
            if (isConnected) {
                connectionNotification.onNetworkAvailable()
            } else {
                connectionNotification.onNetworkLost()
            }
        }

        private val networkCallback: ConnectivityManager.NetworkCallback =
            object : ConnectivityManager.NetworkCallback() {

                override fun onAvailable(network: Network) {
                    connected = true
                    logger.debug("Network available: $network")
                    notifyConnectionStatus()
                }

                override fun onLost(network: Network) {
                    connected = false
                    logger.debug("Network lost: $network")
                    notifyConnectionStatus()
                }

                override fun onCapabilitiesChanged(
                    network: Network,
                    networkCapabilities: NetworkCapabilities
                ) {
                    connectionType = when {
                        networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> "Mobile"
                        networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> "Wifi"
                        else -> "Other"
                    }
                    logger.debug("Network capabilities changed: $network")
                    notifyConnectionStatus()
                }
            }

        override fun onCreate(owner: LifecycleOwner) {
            connectivityManager.registerDefaultNetworkCallback(networkCallback)
        }

        override fun onResume(owner: LifecycleOwner) {
            notifyConnectionStatus()
        }

        override fun onDestroy(owner: LifecycleOwner) {
            mainScope.cancel()
            connectivityManager.unregisterNetworkCallback(networkCallback)
        }

        private fun notifyConnectionStatus() = mainScope.launch {
            delay(DELAY_FOR_CONNECTIVITY_STATUS_MS)
            debouncedConnectivityNotifier(connected)
        }
    }
}
