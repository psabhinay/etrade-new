package com.etrade.mobilepro.util

interface ValueCoder<T> {
    fun encode(value: T): Any
    fun decode(encoded: Any): T?
}
