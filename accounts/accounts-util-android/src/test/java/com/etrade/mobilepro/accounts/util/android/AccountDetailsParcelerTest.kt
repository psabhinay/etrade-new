package com.etrade.mobilepro.accounts.util.android

import android.os.Parcel
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobile.accounts.billpay.BillPay
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobile.accounts.details.isEquals
import org.junit.Test
import org.junit.runner.RunWith
import java.math.BigDecimal
import java.math.BigInteger

@RunWith(AndroidJUnit4::class)
class AccountDetailsParcelerTest {

    @Test
    internal fun `data A can be serialized and deserialized`() {
        testData(dataA)
    }

    @Test
    internal fun `data B can be serialized and deserialized`() {
        testData(dataB)
    }

    private fun testData(data: AccountDetails) {
        val parcel = Parcel.obtain()
        AccountDetailsParceler.run { data.write(parcel, 0) }
        parcel.setDataPosition(0)
        val result = AccountDetailsParceler.create(parcel)
        assert(data.isEquals(result))
    }

    private val dataA = object : AccountDetails {
        override val accountIdKey: String = "a1"
        override val maskedAccountId: String = "a2"
        override val accountMode: String = "a3"
        override val accountName: String = "a4"
        override val accountShortName: String = "a5"
        override val accountLongName: String = "a6"
        override val mobileDescription: String = "a7"
        override val accountValue: BigDecimal? = BigDecimal(10)
        override val institutionType: String = "a8"
        override val accountStatus: String = "a9"
        override val restrictionLevel: String = "a10"
        override val closedDate: Long = 20L
        override val brokerage: AccountDetails.Brokerage? = object : AccountDetails.Brokerage {
            override val cashAvailableForWithdrawal: BigDecimal? = null
            override val marginAvailableForWithdrawal: BigDecimal? = null
            override val purchasingPower: BigDecimal? = BigDecimal(100)
            override val totalAvailableForWithdrawal: BigDecimal? = null
            override val marginBalance: BigDecimal? = BigDecimal(200)
            override val netCashBalance: BigDecimal? = null
            override val cmAccount: Boolean? = null
            override val contributionDetails: List<AccountDetails.ContributionDetails> = listOf(
                object : AccountDetails.ContributionDetails {
                    override val contributionAmount: BigDecimal? = BigDecimal(300)
                    override val taxYear: Int = 11
                }
            )
        }
        override val billPay: BillPay? = BillPay(
            isRestricted = false,
            isEnrolled = true,
            isPrimary = true,
            isEligible = false
        )
        override val accountId: String = "a11"
        override val nickname: String? = "a12"
        override val instNo: BigInteger = BigInteger.valueOf(30)
        override val accountType: String = "a13"
        override val accountCategory: String = "a14"
    }

    private val dataB = object : AccountDetails {
        override val accountIdKey: String = "a1"
        override val maskedAccountId: String = "a2"
        override val accountMode: String = "a3"
        override val accountName: String = "a4"
        override val accountShortName: String = "a5"
        override val accountLongName: String = "a6"
        override val mobileDescription: String = "a7"
        override val accountValue: BigDecimal? = null
        override val institutionType: String = "a8"
        override val accountStatus: String = "a9"
        override val restrictionLevel: String = "a10"
        override val closedDate: Long = 20L
        override val brokerage: AccountDetails.Brokerage? = null
        override val billPay: BillPay? = null
        override val accountId: String = "a11"
        override val nickname: String? = null
        override val instNo: BigInteger = BigInteger.valueOf(30)
        override val accountType: String = "a13"
        override val accountCategory: String = "a14"
    }
}
