package com.etrade.mobilepro.watchlistapi

sealed class WatchlistEvent
object WatchlistUpdated : WatchlistEvent()
object WatchlistEntryAdded : WatchlistEvent()
data class WatchListDeleted(val watchlistId: String) : WatchlistEvent()

interface WatchlistEventListener {
    fun onWatchlistEvent(event: WatchlistEvent)
}

interface WatchlistEventNotifier {
    fun addListener(listener: WatchlistEventListener)
    fun removeListener(listener: WatchlistEventListener)
}
