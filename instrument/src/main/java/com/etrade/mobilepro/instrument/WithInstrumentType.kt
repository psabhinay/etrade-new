package com.etrade.mobilepro.instrument

/**
 * Marks an entity that has [InstrumentType].
 */
interface WithInstrumentType {

    /**
     * Instrument type.
     */
    val instrumentType: InstrumentType
}
