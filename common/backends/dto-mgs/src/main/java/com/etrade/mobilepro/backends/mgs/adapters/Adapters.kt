package com.etrade.mobilepro.backends.mgs.adapters

import com.etrade.mobilepro.backends.mgs.BaseMeta
import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory

fun getBaseScreenViewAdapterFactory(): PolymorphicJsonAdapterFactory<BaseScreenView> =
    PolymorphicJsonAdapterFactory.of(BaseScreenView::class.java, "type")
        .withDefaultValue(object : BaseScreenView() {
            override val data: Any? = null
            override val ctaList: List<CallToActionDto>? = null
        })

fun getBaseReferenceAdapterFactory(): PolymorphicJsonAdapterFactory<BaseReference<*>> =
    PolymorphicJsonAdapterFactory.of(BaseReference::class.java, "type")

fun getBaseMetaAdapterFactory(): PolymorphicJsonAdapterFactory<BaseMeta> =
    PolymorphicJsonAdapterFactory.of(BaseMeta::class.java, "type")
