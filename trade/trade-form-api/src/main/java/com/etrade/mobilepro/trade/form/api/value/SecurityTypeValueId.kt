package com.etrade.mobilepro.trade.form.api.value

import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.util.MutualConverter

interface SecurityTypeValueId {
    val conditional: String
    val mutualFund: String
    val option: String
    val stock: String
    val stockAndOptions: String
    val converter: MutualConverter<String, SecurityType>
}
