package com.etrade.mobilepro.inboxmessages

import android.net.Uri
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.util.android.extension.uriEncode
import com.etrade.mobilepro.util.formatters.toWebViewDeepLink
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import java.lang.StringBuilder

private const val DEEP_LINK_GENERIC_LINK = "etrade://show/genericlink?"
private const val DEEP_LINK_GENERIC_LINK_PARAM_URL = "urlToLoad"
private const val DEEP_LINK_GENERIC_LINK_PARAM_TITLE = "title"
private const val DEEP_LINK_GENERIC_LINK_FULL_URL_PREFIX = "http"

fun String.getDeepLinkCtaEvent(baseUrl: String): CtaEvent {
    val uri = Uri.parse(this)
    return if (uri.isValidGenericLinkFormat()) {
        val embeddedUrl = uri.getEmbeddedUrl(baseUrl)
        val title = uri.getTitle().orEmpty()
        val url = if (uri.hasFullUrl()) {
            embeddedUrl.toWebViewDeepLink(
                webViewTitle = title
            )
        } else {
            embeddedUrl.toWebViewDeepLink(
                baseUrl = baseUrl,
                webViewTitle = title
            )
        }
        CtaEvent.ActionEvent(
            url = url.orEmpty(),
            title = title
        )
    } else {
        CtaEvent.UriEvent(Uri.parse(this))
    }
}

fun Uri.isValidGenericLinkFormat(): Boolean {
    return this.toString().startsWith(DEEP_LINK_GENERIC_LINK)
}

fun Uri.getResolvedGenericDeepLink(baseUrl: String): String? {
    val title = getTitle().orEmpty()
    val resolvedUrl = getEmbeddedUrl(baseUrl)
    return resolvedUrl.toWebViewDeepLink(webViewTitle = title)
}

internal fun Uri.hasFullUrl(): Boolean {
    val parametersMap = getUriParameters()
    val url = parametersMap[DEEP_LINK_GENERIC_LINK_PARAM_URL]
    return url?.startsWith(DEEP_LINK_GENERIC_LINK_FULL_URL_PREFIX) == true
}

internal fun Uri.getTitle(): String? {
    val parametersMap = getUriParameters()
    return parametersMap[DEEP_LINK_GENERIC_LINK_PARAM_TITLE]
}

internal fun Uri.getExtraUriParamsAsQueryString(): String? {
    val parametersMap = getUriParameters().toMutableMap()
    parametersMap.remove(DEEP_LINK_GENERIC_LINK_PARAM_URL)
    parametersMap.remove(DEEP_LINK_GENERIC_LINK_PARAM_TITLE)
    return if (parametersMap.isEmpty()) {
        null
    } else {
        parametersMap.map { (k, v) -> "$k=${v?.uriEncode()}" }.joinToString("&")
    }
}

internal fun Uri.getEmbeddedUrl(baseUrl: String): String? {
    val parametersMap = getUriParameters()
    val embeddedUrl = parametersMap[DEEP_LINK_GENERIC_LINK_PARAM_URL] ?: return null
    val urlToLoad = if (this.hasFullUrl()) {
        embeddedUrl.toHttpUrlOrNull()
    } else {
        baseUrl.plus(embeddedUrl).toHttpUrlOrNull()
    } ?: return null

    val queryBuilder = StringBuilder()
    urlToLoad.query?.let {
        queryBuilder.append("$it&")
    }
    getExtraUriParamsAsQueryString()?.let {
        queryBuilder.append(it)
    }
    val httpBuilder = HttpUrl.Builder().scheme(urlToLoad.scheme).host(urlToLoad.host)
        .encodedPath(urlToLoad.encodedPath)
    if (queryBuilder.isNotEmpty()) {
        httpBuilder.encodedQuery(queryBuilder.toString())
    }
    return httpBuilder.build().toString()
}

private fun Uri.getUriParameters(): Map<String, String?> {
    val parameterNames = queryParameterNames
    val parametersMap: MutableMap<String, String?> = mutableMapOf()
    parameterNames.forEach { key ->
        parametersMap[key] = getQueryParameter(key)
    }
    return parametersMap
}
