# HorizontalListPicker

Horizontal list picker that displays 3 items on a single, scrollable row allowing a user to pick a specific value (in the middle).

##Usage

To use the picker without a drop down selector you just need to provide items. For example:

```kotlin
pickerView.items = IntArray(10) { it }.map { "Item $it" }
```

![Without Selector](./doc/without-selector.png)

To add a drop down selector, provide an implementation of it. For example:

```kotlin
pickerView.selectorTitle = "Select Item" // the title will be showed on drop down selector
pickerView.selector = BottomSheetSelector(supportFragmentManager) // the selector's implementation
```

![With Selector](./doc/with-selector.png)
