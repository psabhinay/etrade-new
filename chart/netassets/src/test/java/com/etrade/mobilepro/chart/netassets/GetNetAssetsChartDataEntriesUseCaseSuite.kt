package com.etrade.mobilepro.chart.netassets

import com.etrade.mobilepro.barchartview.DataEntry
import com.etrade.mobilepro.chart.api.ChartLine
import com.etrade.mobilepro.chart.api.ChartPoint
import com.etrade.mobilepro.chart.netassets.data.NetAssetsChartRepo
import com.etrade.mobilepro.chart.netassets.model.NetAssetsChartData
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.executeBlocking
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class GetNetAssetsChartDataEntriesUseCaseSuite {

    private val expectedColor = 0xAACCEE

    @Test
    fun `verify net assets chart resource failed`() {
        val expectedException = RuntimeException("Message")
        val resource = executeUseCase(Resource.Failed(error = expectedException))
        assertTrue(resource is Resource.Failed, "Was ${resource.javaClass}")
        assertEquals(expectedException, (resource as Resource.Failed).error)
    }

    @Test
    fun `verify net assets chart resource successful`() {
        val points = listOf(
            TestChartPoint("20190305", "10.0"),
            TestChartPoint("20190405", "5.0"),
            TestChartPoint("20190505", "0.0"),
            TestChartPoint("20190605", "-5.0"),
            TestChartPoint("20190705", "5.0"),
            TestChartPoint("20190805", "15.0")
        )

        val resource = executeUseCase(Resource.Success(NetAssetsChartData(listOf(ChartLine("", points)))))

        assertTrue(resource is Resource.Success, "Was ${resource.javaClass}")
        assertFalse(resource.isEmpty())

        resource.data?.forEachIndexed { index, entry ->
            assertEquals(index + 3f, entry.x)
            assertEquals(points[index].rawY.toFloat(), entry.y)
            assertEquals(expectedColor, entry.barColor)
            assertEquals(expectedColor, entry.valueColor)
            assertEquals(expectedColor, entry.valueDescriptionColor)
        }
    }

    @Test
    fun `verify net assets chart resource successful but no data`() {
        val resource = executeUseCase(Resource.Success())
        assertTrue(resource is Resource.Failed, "Was ${resource.javaClass}")
    }

    @Test
    fun `verify net assets chart resource successful but empty accounts`() {
        val resource = executeUseCase(Resource.Success(NetAssetsChartData(emptyList())))
        assertTrue(resource is Resource.Failed, "Was ${resource.javaClass}")
    }

    @Test
    fun `verify net assets chart year change`() {
        for (i in 1..6) {
            val points = ((6 + i)..(11 + i)).map {
                val year = if (it > NUMBER_OF_MONTHS) { 2020 } else { 2019 }
                val month = if (it == NUMBER_OF_MONTHS) { 12 } else { it % NUMBER_OF_MONTHS }
                TestChartPoint("$year${month.toString().padStart(2, '0')}05", i.toString())
            }
            val resource = executeUseCase(Resource.Success(NetAssetsChartData(listOf(ChartLine("", points)))))

            assertTrue(resource is Resource.Success, "Was ${resource.javaClass}")
            assertFalse(resource.isEmpty())

            resource.data?.forEachIndexed { index, entry ->
                assertEquals(i + index + 6f, entry.x, "Failed i = $i")
            }
        }
    }

    @Test
    fun `verify net assets chart has more data than needed`() {
        val points = listOf(
            TestChartPoint("20190305", "10.0"),
            TestChartPoint("20190405", "5.0"),
            TestChartPoint("20190505", "0.0"),
            TestChartPoint("20190605", "-5.0"),
            TestChartPoint("20190705", "5.0"),
            TestChartPoint("20190805", "15.0"),
            TestChartPoint("20190905", "20.0"),
            TestChartPoint("20191005", "25.0")
        )

        val resource = executeUseCase(Resource.Success(NetAssetsChartData(listOf(ChartLine("", points)))))

        assertTrue(resource is Resource.Success, "Was ${resource.javaClass}")
        assertFalse(resource.isEmpty())

        resource.data?.apply {
            assertEquals(6, size)
            forEachIndexed { index, entry ->
                assertEquals(index + 5f, entry.x)
                assertEquals(points[index + 2].rawY.toFloat(), entry.y)
                assertEquals(expectedColor, entry.barColor)
                assertEquals(expectedColor, entry.valueColor)
                assertEquals(expectedColor, entry.valueDescriptionColor)
            }
        }
    }

    @Test
    fun `verify net assets chart has less data than needed`() {
        val points = listOf(
            TestChartPoint("20190305", "10.0"),
            TestChartPoint("20190405", "5.0"),
            TestChartPoint("20190505", "0.0"),
            TestChartPoint("20190605", "-5.0")
        )

        val resource = executeUseCase(Resource.Success(NetAssetsChartData(listOf(ChartLine("", points)))))

        assertTrue(resource is Resource.Success, "Was ${resource.javaClass}")
        assertFalse(resource.isEmpty())

        resource.data?.forEachIndexed { index, entry ->
            assertEquals(index + 3f, entry.x)
            assertEquals(points[index].rawY.toFloat(), entry.y)
            assertEquals(expectedColor, entry.barColor)
            assertEquals(expectedColor, entry.valueColor)
            assertEquals(expectedColor, entry.valueDescriptionColor)
        }
    }

    private fun executeUseCase(repoResult: Resource<NetAssetsChartData>): Resource<List<DataEntry>> {
        return createUseCase(repoResult).executeBlocking(GetNetAssetsChartDataEntriesParameter(""))
    }

    private fun createUseCase(repoResult: Resource<NetAssetsChartData>): GetNetAssetsChartDataEntriesUseCase {
        val repo = mock<NetAssetsChartRepo>()
        runBlocking { whenever(repo.getNetAssetsChart(any())).thenReturn(repoResult) }
        return GetNetAssetsChartDataEntriesUseCaseImpl(repo, expectedColor)
    }

    private class TestChartPoint(override val rawX: String, override val rawY: String) : ChartPoint
}
