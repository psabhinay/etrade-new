package org.chromium.customtabsclient.shared

import android.content.Context
import android.content.Intent
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

private const val EXTRA_URL = "extra.url"
private const val EXTRA_UP_ICON_RES_ID = "extra.upIconId"
private const val EXTRA_EXTERNAL_URL_VIEW = "extra.externalUrlView"
private const val EXTRA_TITLE = "extra.titleStr"

internal fun Intent.getUrl(): String? = getStringExtra(EXTRA_URL)

internal fun Intent.getTitle(): String? = getStringExtra(EXTRA_TITLE)

internal fun Intent.getUpIconResId(): Int = getIntExtra(EXTRA_UP_ICON_RES_ID, 0)

internal fun Intent.getExternalUrlView(): Boolean = getBooleanExtra(EXTRA_EXTERNAL_URL_VIEW, false)

fun <T : WebviewActivity> Context.createWebViewActivityIntent(
    activityClass: Class<T>,
    url: String,
    @StringRes titleResId: Int = 0,
    @DrawableRes upIconResId: Int = 0,
    externalUrlView: Boolean = false
): Intent {
    val title = if (titleResId == 0) {
        null
    } else {
        getString(titleResId)
    }
    return createWebViewActivityIntent(activityClass, url, title, externalUrlView, upIconResId)
}

fun <T : WebviewActivity> Context.createWebViewActivityIntent(
    activityClass: Class<T>,
    url: String,
    title: String? = null,
    externalUrlView: Boolean = false,
    @DrawableRes upIconResId: Int = 0
): Intent {
    return Intent(this, activityClass).apply {
        putExtra(EXTRA_URL, url)
        putExtra(EXTRA_TITLE, title)
        putExtra(EXTRA_EXTERNAL_URL_VIEW, externalUrlView)
        putExtra(EXTRA_UP_ICON_RES_ID, upIconResId)
        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    }
}
