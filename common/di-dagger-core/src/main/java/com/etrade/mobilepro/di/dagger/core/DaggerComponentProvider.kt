package com.etrade.mobilepro.di.dagger.core

interface DaggerComponentProvider {
    fun <T : Any> getComponent(componentClass: Class<T>): T?
}
