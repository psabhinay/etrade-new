package com.etrade.mobilepro.chat.presentation.common

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class ChatEndReceiver : BroadcastReceiver() {

    companion object {
        val listeners: MutableSet<ChatEndListener> = mutableSetOf()
    }

    override fun onReceive(context: Context?, intent: Intent) {
        listeners.forEach { it.onChatEnded() }
    }

    fun subscribe(endChatListener: ChatEndListener) {
        listeners.add(endChatListener)
    }

    fun unsubscribe(endChatListener: ChatEndListener) {
        listeners.remove(endChatListener)
    }
}

interface ChatEndListener {
    fun onChatEnded()
}
