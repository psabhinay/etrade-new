package com.etrade.mobilepro.streaming.lightstreamer.level2

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.StreamIdFactory
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class Level2StreamIdFactoryTest {

    private val factory: StreamIdFactory<Level2BookType> = Level2StreamIdFactory(Parameters())

    @Test
    fun `check option`() {
        assertEquals("L2OPTION:ETFC:OPTALL:5", factory.createStreamId("ETFC", InstrumentType.OPTN))
    }

    @Test
    fun `check non-option`() {
        assertEquals("L2STOCK:ETFC:ARC:5", factory.createStreamId("ETFC", InstrumentType.INDX, Level2BookType.ARCA))
    }

    private class Parameters(override val bookCount: Int = 5) : Level2StreamIdFactory.Parameters
}
