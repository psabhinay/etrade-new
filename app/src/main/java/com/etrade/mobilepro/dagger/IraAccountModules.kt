package com.etrade.mobilepro.dagger

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.checkdeposit.presentation.contributiondetails.IraContributionAmountsViewModel
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.iraaccount.api.repo.IraContributionDetailRepo
import com.etrade.mobilepro.iraaccount.data.repo.DefaultIraContributionDetailRepo
import com.etrade.mobilepro.iraaccount.data.rest.IraContributionService
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
abstract class IraAccountModule {

    @Binds
    @Singleton
    abstract fun bindIraContributionRepo(impl: DefaultIraContributionDetailRepo): IraContributionDetailRepo

    companion object {
        @Provides
        @Singleton
        fun provideIraContributionService(@MobileTrade retrofit: Retrofit): IraContributionService = retrofit.create(IraContributionService::class.java)
    }
}

@Module
interface IraAccountViewModels {
    @Binds
    @IntoMap
    @ViewModelKey(IraContributionAmountsViewModel::class)
    fun bindIraContributionViewModel(impl: IraContributionAmountsViewModel): ViewModel
}
