package com.etrade.mobilepro.tableviewutils.presentation

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.evrencoskun.tableview.ITableView
import com.evrencoskun.tableview.adapter.ITableAdapter
import com.evrencoskun.tableview.adapter.recyclerview.ColumnHeaderRecyclerViewAdapter
import com.evrencoskun.tableview.sort.SortState

fun ITableView.updateSortViewState(sortInfo: SortStateInfo) {
    val adapter = adapter ?: return
    adapter.updateSymbolViewState(sortInfo)
    adapter.columnHeaderRecyclerViewAdapter.updateViewSortState(sortInfo)
}

private fun ITableAdapter.updateSymbolViewState(sortInfo: SortStateInfo) {
    cornerView?.apply {
        val cornerTextView = findViewById<TextView>(R.id.cornerHeaderTextview)
        val cornerImageView = findViewById<ImageView>(R.id.cornerHeaderSortButton)

        cornerImageView?.rotateSortArrow(
            if (sortInfo.sortedColumnPosition == null) sortInfo.sortState else SortState.UNSORTED
        )

        contentDescription = "${cornerTextView?.text} ${
        if (sortInfo.sortedColumnPosition == null && sortInfo.sortState != SortState.UNSORTED) {
            "${resources.getString(R.string.sorted)} ${sortInfo.sortState.name}"
        } else { SortState.UNSORTED.name }
        }"
    }
}

private fun ColumnHeaderRecyclerViewAdapter<Any>.updateViewSortState(sortInfo: SortStateInfo) {
    items.forEachIndexed { index, _ ->
        val columnSortState =
            if (sortInfo.sortedColumnPosition == index) sortInfo.sortState else SortState.UNSORTED
        columnSortHelper.setSortingStatus(index, columnSortState)
    }
}

private const val ANGLE_UP = 0f
private const val ANGLE_DOWN = 180f

fun ImageView.rotateSortArrow(sortState: SortState) {
    when (sortState) {
        SortState.ASCENDING -> {
            // change icon to up arrow starting with rotation at 180f end at 360f and set to 0f
            if (visibility == View.VISIBLE) {
                flip(ANGLE_DOWN, ANGLE_UP)
            } else {
                rotation = ANGLE_UP
                setImageResource(R.drawable.ic_sort_arrow_up)
                visibility = View.VISIBLE
            }
        }
        SortState.DESCENDING -> {
            // change icon to down arrow starting with rotation at 0f end at 180f
            flip(ANGLE_UP, ANGLE_DOWN)
            visibility = View.VISIBLE
        }
        else -> {
            // unsorted to ascending start rotation at 0f up arrow drawable
            visibility = View.INVISIBLE
        }
    }
}

/**
 * Method defines visible Symbols on the screen within [ITableView].
 *
 * @return set of Pair<Symbol, InstrumentType>; In case of [ITableView] is not initialized empty Set will be returned.
 */
fun ITableView.getVisibleSymbols(): Set<Symbol> {
    val rowLayoutManager = rowHeaderRecyclerView.layoutManager as? LinearLayoutManager ?: return emptySet()
    val firstVisible = rowLayoutManager.findFirstVisibleItemPosition()
    val lastVisible = rowLayoutManager.findLastVisibleItemPosition()
    return getVisibleSymbols(firstVisible, lastVisible)
}

/**
 * Method defines visible Symbols on the screen within [ITableView].
 *
 * @param firstVisiblePosition index of the first visible row.
 * @param lastVisiblePosition index of the last visible row.
 *
 * @return set of Pair<Symbol, InstrumentType>; In case of [ITableView] is not initialized empty Set will be returned.
 */
fun ITableView.getVisibleSymbols(firstVisiblePosition: Int, lastVisiblePosition: Int): Set<Symbol> {
    val rowAdapter = adapter.rowHeaderRecyclerViewAdapter

    if (rowAdapter == null ||
        firstVisiblePosition == RecyclerView.NO_POSITION ||
        lastVisiblePosition == RecyclerView.NO_POSITION
    ) {
        return emptySet()
    }

    return rowAdapter.items
        .subList(firstVisiblePosition, lastVisiblePosition + 1)
        .map { item ->
            Symbol(item as SymbolCell)
        }
        .toSet()
}

private fun View.flip(oldRotation: Float, newRotation: Float) {
    if (rotation == newRotation) {
        return
    }
    rotation = oldRotation
    animate().rotation(rotation + ANGLE_DOWN).withEndAction {
        rotation = newRotation
    }.start()
}

data class SortStateInfo(
    val sortState: SortState = SortState.UNSORTED,
    val sortedColumnPosition: Int? = null, // If [sortedColumnPosition] is null, that means that sorting applied by the row header column.
    val stateChanged: ConsumableLiveEvent<Boolean> = ConsumableLiveEvent(true)
)

fun SortStateInfo.toPreferenceString(columns: List<String>): String =
    "${sortState.name}/${sortedColumnPosition?.let { columns.getOrNull(it) }}"

fun String.toSortStateInfo(columns: List<String>): SortStateInfo {
    val strings = this.split("/")
    val state = runCatching {
        Pair(SortState.valueOf(strings[0]), columns.indexOf(strings[1]).takeIf { it != -1 })
    }.getOrDefault(Pair(SortState.UNSORTED, null))
    return SortStateInfo(state.first, state.second)
}
