package com.etrade.mobilepro.util.formatters

/**
 * This function only supports full url. Url will be opened in an external Browser.
 * */
fun String?.toWebDeepLink(): String? {
    return if (this.isNullOrEmpty()) {
        null
    } else {
        "etrade://web/$this"
    }
}

/**
 * This function supports two types of url: relative url and full url.
 * If baseUrl is not empty or null, the final url will be combined by baseUrl and relative url.
 * If baseUrl is null or empty, it will be considered as a full url.
 * Both relative url and full url will be opened in web view.
 * */
fun String?.toWebViewDeepLink(baseUrl: String? = null, externalWebViewTitle: String? = null, webViewTitle: String? = null): String? {
    return if (this.isNullOrEmpty()) {
        null
    } else {
        val title = if (webViewTitle.isNullOrEmpty()) {
            externalWebViewTitle ?: this
        } else {
            webViewTitle
        }

        "etrade://webview?title=$title&url=${baseUrl?.plus(this) ?: this}"
    }
}
