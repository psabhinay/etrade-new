package com.etrade.mobilepro.common

interface ScrollOnTopListener {

    val isOnTop: Boolean

    fun scrollUp()
}
