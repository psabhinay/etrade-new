package com.etrade.mobilepro.barcode.adapter

import android.view.LayoutInflater
import android.view.View
import com.etrade.mobilepro.barcode.api.BarcodeItem
import com.etrade.mobilepro.common.customview.SwipeRevealLayout
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import org.threeten.bp.format.DateTimeFormatter

class BarcodeResultAdapter(
    layoutInflater: LayoutInflater,
    itemClickListener: (BarcodeItem) -> Unit,
    itemDeleteClickListener: (BarcodeItem) -> Unit,
    upcStringTemplate: String,
    dateFormat: DateTimeFormatter
) : ListDelegationAdapter<MutableList<BarcodeItem>>() {

    private var lastOpenPosition: Int? = null
    private val swipeActionListener: (BarcodeItem, Int, SwipeRevealLayout.Action) -> Unit = { item, position, action ->
        when (action) {
            SwipeRevealLayout.Action.CLOSE -> {
                item.isDeleteButtonVisible = false

                lastOpenPosition = null
            }
            SwipeRevealLayout.Action.OPEN -> {
                item.isDeleteButtonVisible = true

                lastOpenPosition = position
            }
            SwipeRevealLayout.Action.START_DRAG -> {
                resetSwipeToDeleteActions()
            }
        }
    }

    init {
        delegatesManager.addDelegate(
            BarcodeResultAdapterDelegate(
                layoutInflater,
                itemViewClickListener = View.OnClickListener { itemClickListener(it.tag as BarcodeItem) },
                deleteItemClickListener = View.OnClickListener {
                    lastOpenPosition = null
                    itemDeleteClickListener(it.tag as BarcodeItem)
                },
                swipeActionListener = swipeActionListener,
                upcStringTemplate = upcStringTemplate,
                dateFormat = dateFormat
            )
        )

        items = mutableListOf()
    }

    fun updateItems(items: List<BarcodeItem>) {
        dispatchUpdates(this.items, items, BarcodeItemDiffCallback(this.items, items))

        this.items.clear()
        this.items.addAll(items)
    }

    fun resetSwipeToDeleteActions() {
        lastOpenPosition?.also {
            items.getOrNull(it)?.isDeleteButtonVisible = false
            notifyItemChanged(it)
        }

        lastOpenPosition = null
    }
}
