package com.etrade.mobilepro.screener.presentation.stock.technicals.percentinput

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EmptySource
import org.junit.jupiter.params.provider.ValueSource

private const val FORMAT = "%s%%"
internal class PercentFormatterTest {
    private lateinit var sut: PercentFormatter

    @BeforeEach
    fun setup() {
        sut = PercentFormatter(FORMAT)
    }

    @ParameterizedTest
    @ValueSource(strings = ["0", "1", "60", "100"])
    fun formatRawContent(input: String) {
        val result = sut.formatRawContent(input)
        assertEquals("$input%", result)
    }

    @ParameterizedTest
    @ValueSource(strings = ["0%", "1%", "60%", "100%"])
    fun formatRawContentWithPercent(input: String) {
        val result = sut.formatRawContent(input)
        assertEquals(result, input)
    }

    @ParameterizedTest
    @EmptySource
    @ValueSource(strings = [" "])
    fun formatRawContentNull(input: String) {
        val result = sut.formatRawContent(input)
        assertEquals(result, input)
    }
}
