package com.etrade.mobilepro.mod

import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class CompanyOverviewUrlBuilderTest {

    private val sut = MarketOnDemandUrlBuilder("https://some.test.com/")
    private val authToken =
        CompanyOverviewUrlBuilderTest::class.java.getResource("auth_token.txt").readText()
    private val symbol = "AAPL"
    private val expectedUrl =
        CompanyOverviewUrlBuilderTest::class.java.getResource("company_overview_url.txt").readText()
            .toHttpUrlOrNull()

    @Test
    fun constructCompanyLookupUrl() {
        val companyOverviewUrl =
            sut.constructUrl("Research/API/Mobile/CompanyLookup/json", symbol, authToken)
        assertEquals(companyOverviewUrl, expectedUrl)
    }
}
