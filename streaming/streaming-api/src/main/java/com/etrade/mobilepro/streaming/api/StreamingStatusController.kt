package com.etrade.mobilepro.streaming.api

interface StreamingStatusController {

    var isStreamingToggleEnabled: Boolean

    fun subscribeForStreamingStatusUpdates(listener: StreamingStatusUpdateListener)

    fun unsubscribeFromStreamingStatusUpdates(listener: StreamingStatusUpdateListener)
}

sealed class StreamingStatusUpdate {
    data class StreamingToggleUpdate(val enabled: Boolean) : StreamingStatusUpdate()
    data class StreamingSubscriptionUpdate(val isRealTime: Boolean) : StreamingStatusUpdate()
}

typealias StreamingStatusUpdateListener = (StreamingStatusUpdate) -> Unit
