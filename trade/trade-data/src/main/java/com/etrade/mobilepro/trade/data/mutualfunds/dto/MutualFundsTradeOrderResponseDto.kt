package com.etrade.mobilepro.trade.data.mutualfunds.dto

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.etrade.mobilepro.backends.neo.warnings
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.orders.api.DividendReInvestmentType
import com.etrade.mobilepro.orders.api.FundsOrderActionType
import com.etrade.mobilepro.orders.api.FundsQuantityType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrder
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrderResponse
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsTradeLeg
import com.etrade.mobilepro.trade.data.util.maskAccountId
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
internal data class MutualFundsTradeOrderResponseDto(
    @Json(name = "mobilemfExchangePreview")
    val exchangePreviewOrderDto: MutualFundsTradeOrderDto?,
    @Json(name = "mobilemfBuyPreview")
    val buyPreviewOrderDto: MutualFundsTradeOrderDto?,
    @Json(name = "mobilemfSellPreview")
    val sellPreviewOrderDto: MutualFundsTradeOrderDto?,
    @Json(name = "mobilemfExchangePlace")
    val exchangePlaceOrderDto: MutualFundsTradeOrderDto?,
    @Json(name = "mobilemfBuyPlace")
    val buyPlaceOrderDto: MutualFundsTradeOrderDto?,
    @Json(name = "mobilemfSellPlace")
    val sellPlaceOrderDto: MutualFundsTradeOrderDto?
) : BaseDataDto(), MutualFundsOrderResponse {

    override val orders: List<MutualFundsOrder>
        get() = orderDto?.let { createOrders(it) }.orEmpty()

    override val warnings: Collection<ServerMessage>
        get() = messages?.warnings.orEmpty()

    val orderDto: MutualFundsTradeOrderDto? by lazy {
        exchangePreviewOrderDto
            ?: buyPreviewOrderDto
            ?: sellPreviewOrderDto
            ?: exchangePlaceOrderDto
            ?: buyPlaceOrderDto
            ?: sellPlaceOrderDto
    }

    private fun createOrders(orderDto: MutualFundsTradeOrderDto): List<MutualFundsOrder> {
        return listOfNotNull(
            createPrimaryOrder(orderDto),
            createExchangeOrder(orderDto)
        )
    }

    private fun createPrimaryOrder(orderDto: MutualFundsTradeOrderDto): MutualFundsOrder? {
        val primaryLegIndex = 0
        val leg = orderDto.leg(primaryLegIndex) ?: return null
        return object : MutualFundsOrder {
            override val orderId: String = requireNotNull(orderDto.orderOrPreviewId(primaryLegIndex))
            override val account: String = orderDto.account
            override val legs: List<MutualFundsTradeLeg> = listOf(leg)
            override val commission: BigDecimal? = orderDto.commission
            override val commissionMessage: String? = orderDto.commissionMsg
            override val dividendReInvestmentType: DividendReInvestmentType = orderDto.dividendReinvestmentType
            override val orderActionType: FundsOrderActionType = when (orderDto.orderAction) {
                "1" -> FundsOrderActionType.BUY
                "2" -> FundsOrderActionType.SELL
                "5" -> FundsOrderActionType.EXCHANGE
                else -> FundsOrderActionType.BUY
            }
            override val priceTypeMessage: String = orderDto.priceType
            override val quantityType: FundsQuantityType = orderDto.fundsQuantityType(leg)
        }
    }

    private fun createExchangeOrder(orderDto: MutualFundsTradeOrderDto): MutualFundsOrder? {
        val exchangeLegIndex = 1
        val leg = orderDto.leg(exchangeLegIndex) ?: return null
        return object : MutualFundsOrder {
            override val orderId: String = orderDto.orderOrPreviewId(exchangeLegIndex) ?: "1"
            override val account: String = orderDto.account
            override val legs: List<MutualFundsTradeLeg> = listOf(leg)
            override val commission: BigDecimal? = orderDto.commission
            override val commissionMessage: String? = orderDto.commissionMsg
            override val dividendReInvestmentType: DividendReInvestmentType = orderDto.dividendReinvestmentType
            override val orderActionType: FundsOrderActionType = FundsOrderActionType.BUY
            override val priceTypeMessage: String = orderDto.priceType
            override val quantityType: FundsQuantityType = orderDto.fundsQuantityType(leg)
        }
    }
}

@JsonClass(generateAdapter = true)
internal data class MutualFundsTradeOrderDto(
    @Json(name = "commissionMsg")
    val commissionMsg: String?,

    @Json(name = "commission")
    val commissionRaw: String?,

    @Json(name = "dividentReinvest")
    val dividentReinvest: String = "",

    @Json(name = "investmentAmount")
    val investmentAmount: String = "",

    @Json(name = "marginLevelCd")
    val marginLevelCode: String = "",

    @Json(name = "fundToSell")
    val fundToSell: String = "",

    @Json(name = "fundFamily")
    val fundFamily: String = "",

    @Json(name = "isPmEligible")
    val isPmEligible: String? = null,

    @Json(name = "houseExcessEquityNew")
    val houseExcessEquityNew: String = "",

    @Json(name = "houseExcessEquityCurr")
    val houseExcessEquityCurr: String = "",

    @Json(name = "houseExcessEquityChange")
    val houseExcessEquityChange: String = "",

    @Json(name = "timePlacedMS")
    val timePlacedMillis: String?,

    @Json(name = "pageSourceName")
    val pageSourceName: String = "",

    @Json(name = "accountId")
    val accountId: String = "",

    @Json(name = "acctDesc")
    val accountDescription: String = "",

    @Json(name = "orderAction")
    val orderAction: String = "",

    @Json(name = "priceType")
    val priceType: String = "",

    @Json(name = "positionQuantity")
    val positionQuantity: String?,

    @Json(name = "aipFlag")
    val aipFlag: String = "",

    @Json(name = "flConsentNeeded")
    val flConsentNeeded: Boolean = false,

    @Json(name = "eoConsentNeeded")
    val eoConsentNeeded: Boolean = false,

    @Json(name = "shortDescription")
    val shortDescription: String?,

    @Json(name = "orderSource")
    val orderSource: String?,

    @Json(name = "clearingInstitution")
    val clearingInstitution: String = "",

    @Json(name = "preClearanceCode")
    val preClearanceCode: String = "",

    @Json(name = "orderBpSummary")
    val orderBuyingPowerSummary: BuyingPowerSummary?,

    @Json(name = "overnightHoldFlag")
    val overnightHoldFlag: String?,

    @Json(name = "legDetails")
    val legDetails: List<LegDetails>?,

    @Json(name = "previewIds")
    val previewIds: List<FundOrderId>?,

    @Json(name = "orderIds")
    val orderIds: List<FundOrderId>?,

    @Json(name = "employee")
    val isEmployee: Boolean = false
)

private val MutualFundsTradeOrderDto.account: String
    get() = accountId.maskAccountId()

private val MutualFundsTradeOrderDto.commission: BigDecimal?
    get() = commissionRaw?.safeParseBigDecimal()

private val MutualFundsTradeOrderDto.dividendReinvestmentType: DividendReInvestmentType
    get() = when (dividentReinvest) {
        "OM_REINVEST" -> DividendReInvestmentType.RE_INVEST
        "OM_DEPOSIT" -> DividendReInvestmentType.DEPOSIT_FUND
        else -> DividendReInvestmentType.HOLDING
    }

@Suppress("ComplexMethod")
private fun MutualFundsTradeOrderDto.fundsQuantityType(leg: MutualFundsTradeLeg): FundsQuantityType {
    fun getEntirePositionType(positionQuantity: String?): FundsQuantityType {
        return when (positionQuantity) {
            "1" -> FundsQuantityType.ENTIRE_POSITION
            "2" -> FundsQuantityType.ENTIRE_CASH_POSITION
            "3" -> FundsQuantityType.ENTIRE_MARGIN_POSITION
            else -> FundsQuantityType.ENTIRE_POSITION
        }
    }

    return when (leg.legPositionType) {
        "4" -> getEntirePositionType(positionQuantity)
        "3" -> FundsQuantityType.DOLLARS
        "2", "5" -> FundsQuantityType.SHARES
        else -> FundsQuantityType.UNKNOWN
    }
}

private fun MutualFundsTradeOrderDto.leg(index: Int): MutualFundsTradeLeg? {
    fun getTotalQuantity(cashQty: String?, marginQty: String?): BigDecimal? {
        return if (cashQty != null || marginQty != null) {
            val cashVal = cashQty?.safeParseBigDecimal() ?: BigDecimal.ZERO
            val marginVal = marginQty?.safeParseBigDecimal() ?: BigDecimal.ZERO
            cashVal.plus(marginVal)
        } else {
            null
        }
    }

    fun LegDetails.toMutualFundsTradeLeg(): MutualFundsTradeLeg {
        return object : MutualFundsTradeLeg {
            override val legPositionType: String? = quantityTypeRaw
            override val action: TransactionType = when (transaction) {
                "OM_BUY" -> TransactionType.BUY_OPEN
                "OM_SELL" -> TransactionType.SELL_CLOSE
                else -> TransactionType.UNKNOWN
            }
            override val quantity: BigDecimal? = getTotalQuantity(quantityRaw?.cash, quantityRaw?.margin)
            override val symbol: String = symbolRaw.orEmpty()
            override val instrumentType: InstrumentType = InstrumentType.from(typeCode)
        }
    }

    return legDetails?.getOrNull(index)?.toMutualFundsTradeLeg()
}

private fun MutualFundsTradeOrderDto.orderOrPreviewId(index: Int): String? = (orderIds?.getOrNull(index) ?: previewIds?.getOrNull(index))?.id
