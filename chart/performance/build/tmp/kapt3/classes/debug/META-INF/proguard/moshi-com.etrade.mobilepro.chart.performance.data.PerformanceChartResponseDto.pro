-if class com.etrade.mobilepro.chart.performance.data.PerformanceChartResponseDto
-keepnames class com.etrade.mobilepro.chart.performance.data.PerformanceChartResponseDto
-if class com.etrade.mobilepro.chart.performance.data.PerformanceChartResponseDto
-keep class com.etrade.mobilepro.chart.performance.data.PerformanceChartResponseDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
