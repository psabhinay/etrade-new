package com.etrade.mobilepro.screener.api.datasource

import com.etrade.mobilepro.screener.api.EtfFundGroup
import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.FundFamily

interface EtfFundDataSource {
    fun getAllFundFamiliesItem(): FundFamily
    fun getFundFamilies(): List<FundFamily>

    fun getAllFundGroupsItem(): EtfFundGroup
    fun getFundGroups(): List<EtfFundGroup>

    fun getAllFundCategoriesItem(): FundCategory
}
