package com.etrade.mobilepro.transfermoney.presentation.confirmation

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.setupToolbarWithUpButton
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.transfermoney.presentation.R
import com.etrade.mobilepro.transfermoney.presentation.TransferMoneyNavigationFlowViewModel
import com.etrade.mobilepro.transfermoney.presentation.databinding.TransferFragmentConfirmationBinding
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

@RequireLogin
class TransferMoneyConfirmationFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory
) : OverlayBaseFragment() {

    private val navigationFlowViewModel: TransferMoneyNavigationFlowViewModel by navGraphViewModels(R.id.transfer_money_nav_graph)
    private val args: TransferMoneyConfirmationFragmentArgs by navArgs()

    override val layoutRes: Int = R.layout.transfer_fragment_confirmation
    private val binding by viewBinding(TransferFragmentConfirmationBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setConfirmationView()
        if (savedInstanceState == null) { binding.confirmationView.playAnimation() }

        viewModels<TransferMoneyConfirmationViewModel> { viewModelFactory }.value
    }

    override fun onStart() {
        super.onStart()
        (activity as? AppCompatActivity)?.setupToolbarWithUpButton(titleResId = R.string.confirmation, upButtonResId = R.drawable.ic_close)
    }

    @Suppress("LongMethod")
    private fun setConfirmationView() = binding.confirmationView.apply {
        args.confirmation.apply {
            setAmountText(amount)
            setConfirmationText(message)

            val sections = mutableListOf(
                createSummarySection(
                    label = getString(R.string.transfer_from_label),
                    value = fromAccount,
                    valueDescription = requireContext().accountNameContentDescription(fromAccount)
                ),
                createSummarySection(
                    label = getString(R.string.transfer_to_label),
                    value = toAccount,
                    valueDescription = requireContext().accountNameContentDescription(toAccount)
                ),
                createSummarySection(getString(R.string.schedule_date), scheduleDate)
            )
            if (confirmationNumber.isNotBlank()) {
                sections.add(
                    createSummarySection(
                        label = getString(R.string.transfer_confirmation_number),
                        value = confirmationNumber,
                        valueDescription = confirmationNumber.characterByCharacter()
                    )
                )
            }
            setDetailsTable(sections)

            setActionButton(
                getString(R.string.transfer_view),
                View.OnClickListener {
                    navigationFlowViewModel.switchToTransferActivity()
                    navigationFlowViewModel.showNewActivityDetails(confirmationNumber)
                    findNavController().popBackStack()
                }
            )
        }
    }
}
