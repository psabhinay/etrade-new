package com.etrade.mobilepro.arr.data

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.arr.api.AnalystResearchReportRepo
import com.etrade.mobilepro.mod.api.Report
import io.reactivex.Single
import javax.inject.Inject

class DefaultAnalystResearchReportRepo @Inject constructor(
    private val analystResearchReportService: AnalystResearchReportService
) : AnalystResearchReportRepo {

    override fun getAnalystReports(symbol: String): Single<List<Report>> {
        return analystResearchReportService.getAnalystResearchReportListUrl(symbol)
            .map {
                it.text?.let { text ->
                    text
                }
            }
            .flatMap { url ->
                analystResearchReportService.getAnalystResearchReports(url)
                    .map {
                        it.results?.filter { report ->
                            report.reportAvailable == 1
                        }?.map { report ->
                            Report(report.provider, report.reportUrl, report.reportDate)
                        }?.sortedByDescending { report ->
                            DateFormattingUtils.parseDateFromString(report.reportDate)
                        }
                    }
            }
    }
}
