package com.etrade.mobilepro.chat.presentation.message.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.presentation.databinding.ChatItemMessageLoadingBinding
import com.etrade.mobilepro.chat.presentation.message.viewHolder.ChatMessageLoadingViewHolder
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class ChatMessageLoadingAdapterDelegate : AbsListItemAdapterDelegate<ChatMessage.Loading, ChatMessage, ChatMessageLoadingViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup) =
        ChatMessageLoadingViewHolder(parent.viewBinding(ChatItemMessageLoadingBinding::inflate))

    override fun isForViewType(item: ChatMessage, items: MutableList<ChatMessage>, position: Int) = item is ChatMessage.Loading

    override fun onBindViewHolder(item: ChatMessage.Loading, holder: ChatMessageLoadingViewHolder, payloads: MutableList<Any>) {
        /* do nothing */
    }
}
