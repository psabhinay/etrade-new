package com.etrade.mobilepro.tableviewutils.presentation.data

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.tableviewutils.api.ColumnRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private const val COLUMNS_SEPARATOR = ","

/**
 * Base implementation of [ColumnRepo] that uses [KeyValueStorage] to store columns. It does not support removing or adding of columns.
 *
 * @param storage key-value storage to use to store columns
 */
abstract class BaseColumnRepo<T : Any>(private val storage: KeyValueStorage) :
    ColumnRepo<T> {

    /**
     * Key for a [storage].
     */
    protected abstract val storageKey: String

    /**
     * Map of all column names to columns.
     */
    protected abstract val allColumns: Map<String, T>

    /**
     * Default order of columns.
     */
    protected abstract val defaultColumnsOrder: List<String>

    private val allColumnIds: Map<T, String> by lazy { allColumns.entries.associate { it.value to it.key } }

    private var columnsOrder: List<String>
        get() = storage.getStringValue(storageKey, null)?.split(COLUMNS_SEPARATOR) ?: defaultColumnsOrder
        set(value) {
            storage.putStringValue(storageKey, value.joinToString(COLUMNS_SEPARATOR))
        }

    final override suspend fun getColumns(): List<T> = withContext(Dispatchers.IO) {
        columnsOrder.let { order ->
            order + defaultColumnsOrder.filterNot { order.contains(it) } // add columns that are missing from columnsOrder
        }.mapNotNull { allColumns[it] }
    }

    final override suspend fun saveColumns(columns: List<T>) = withContext(Dispatchers.IO) {
        if (allColumnIds.keys != columns.toSet()) {
            throw IllegalArgumentException("Removing or adding columns is not supported. Make sure to save all columns.")
        }
        columnsOrder = columns.mapNotNull { allColumnIds[it] }
    }

    final override fun getColumnsAsString(): List<String> {
        return columnsOrder
    }
}
