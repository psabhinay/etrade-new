package com.etrade.mobilepro.streaming.base

import com.etrade.eo.streaming.SubscriptionManager
import com.etrade.eo.streaming.SubscriptionMode
import com.etrade.eo.streaming.UpdateInfo
import com.etrade.eo.streaming.UpdateResponder
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.TestScheduler
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.util.concurrent.TimeUnit

private const val DEFAULT_STREAM_ID = "streamId"

class SubscriptionManagerHelperTest {

    @Test
    fun `check multiple subscriptions to the same stream return the same object`() {
        val helper = createSubscriptionManagerHelper(mockSubscriptionManager(TestScheduler()))
        val subscription1 = helper.subscribeTo(DEFAULT_STREAM_ID, setOf("field1", "field2", "field3"), SubscriptionMode.MERGE)
        val subscription2 = helper.subscribeTo(DEFAULT_STREAM_ID, setOf("field3", "field2", "field1"), SubscriptionMode.MERGE)
        assertEquals(subscription1, subscription2)
    }

    @Test
    fun `check multiple subscriptions to the same stream with different modes return different objects`() {
        val helper = createSubscriptionManagerHelper(mockSubscriptionManager(TestScheduler()))
        val subscription1 = helper.subscribeTo(DEFAULT_STREAM_ID, setOf("field1", "field2", "field3"), SubscriptionMode.MERGE)
        val subscription2 = helper.subscribeTo(DEFAULT_STREAM_ID, setOf("field3", "field2", "field1"), SubscriptionMode.COMMAND)
        assertNotEquals(subscription1, subscription2)
    }

    @Test
    fun `check multiple subscriptions to the same stream with extra columns`() {
        checkColumnsMerge(setOf("field1", "field2", "field3"), setOf("field1", "field3", "field4"), 2)
    }

    @Test
    fun `check multiple subscriptions to the same stream with fewer columns`() {
        checkColumnsMerge(setOf("field1", "field2", "field3"), setOf("field1", "field3"), 1)
    }

    @Test
    fun `check multiple subscriptions to the same stream with different columns`() {
        checkColumnsMerge(setOf("field1", "field2", "field3"), setOf("field4", "field5"), 2)
    }

    @Test
    fun `check multiple subscriptions to the same stream with same columns`() {
        checkColumnsMerge(setOf("field1", "field2", "field3"), setOf("field1", "field2", "field3"), 1)
    }

    private fun checkColumnsMerge(originalFields: Set<String>, extraFields: Set<String>, times: Int) {
        val scheduler = TestScheduler()
        val manager = mockSubscriptionManager(scheduler)
        val helper = createSubscriptionManagerHelper(manager)

        val actual1 = mutableListOf<UpdateInfo>()
        val observable1 = helper.subscribeTo(DEFAULT_STREAM_ID, originalFields, SubscriptionMode.MERGE)
        val subscription1 = observable1.subscribe { actual1.add(it) }

        scheduler.advanceTimeBy(DELAY_TIME, TimeUnit.MILLISECONDS)

        val actual2 = mutableListOf<UpdateInfo>()
        val observable2 = helper.subscribeTo(DEFAULT_STREAM_ID, extraFields, SubscriptionMode.MERGE)
        val subscription2 = observable2.subscribe { actual2.add(it) }

        assertEquals(observable1, observable2)
        scheduler.advanceTimeBy(DELAY_TIME, TimeUnit.MILLISECONDS)

        subscription1.dispose()
        val sizeBeforeExtraFields = actual1.size - actual2.size

        scheduler.advanceTimeBy(DELAY_TIME, TimeUnit.MILLISECONDS)
        subscription2.dispose()
        scheduler.advanceTimeBy(DELAY_TIME, TimeUnit.MILLISECONDS)

        assertNotEquals(actual1.last(), actual2.last())

        checkUpdates(actual1.subList(0, sizeBeforeExtraFields), originalFields)

        val allFields = originalFields + extraFields
        checkUpdates(actual1.subList(sizeBeforeExtraFields, actual1.size), allFields)
        checkUpdates(actual2, allFields)

        verify(manager, times(times)).unSubscribe(any<Set<String>>(), any())

        val observable3 = helper.subscribeTo(DEFAULT_STREAM_ID, originalFields, SubscriptionMode.MERGE)
        assertNotEquals(observable1, observable3, "Previous observable must be disposed. New subscriptions to the same stream must return new observable.")
    }

    private fun checkUpdates(updates: List<UpdateInfo>, expected: Set<String>) {
        for (update in updates) {
            assertEquals(expected.size, update.getNumFields())
            expected.forEach {
                assertTrue(update.isValueChanged(it))
            }
        }
    }

    private fun createSubscriptionManagerHelper(manager: SubscriptionManager): SubscriptionManagerHelper<UpdateInfo> {
        return DefaultSubscriptionManagerHelper(manager) { it }
    }

    private fun mockSubscriptionManager(scheduler: Scheduler): SubscriptionManager {
        return mock {
            val subscribers = mutableMapOf<UpdateResponder, Disposable>()

            on { subscribe(any<Set<String>>(), any()) }.then { invocation ->
                val responder = invocation.getArgument(1) as UpdateResponder
                Observable.interval(INTERVAL, TimeUnit.MILLISECONDS, scheduler)
                    .subscribe {
                        responder.onUpdate(
                            mockUpdateInfo(it, responder.getSubscriptionFields())
                        )
                    }
                    .also { subscribers[responder] = it }
            }

            on { unSubscribe(any<Set<String>>(), any()) }.then { invocation ->
                val responder = invocation.getArgument(1) as UpdateResponder
                assertNotNull(subscribers.remove(responder)?.apply { dispose() })
            }
        }
    }

    private fun mockUpdateInfo(number: Long, fields: Set<String>): UpdateInfo {
        return mock {
            on { getItemPos() }.thenReturn(number.toInt())
            on { getNumFields() }.thenReturn(fields.size)

            // Does not do actual check but indicates if the field is present in the update.
            on { isValueChanged(any<String>()) }.then {
                fields.contains(it.getArgument(0) as String)
            }
        }
    }
}
