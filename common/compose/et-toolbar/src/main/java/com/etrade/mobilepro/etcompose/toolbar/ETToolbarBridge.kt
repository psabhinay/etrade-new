package com.etrade.mobilepro.etcompose.toolbar

import android.view.View
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import com.etrade.mobilepro.common.compose.theme.EtradeTheme
import com.etrade.mobilepro.common.toolBarContainer

private const val WL_TOOLBAR_BASE_TAG = "WATCH LIST TOOLBAR"

/**
 * Adds and removes a custom view to the activity toolbar.
 * On fragment's destroyView call removeToolbar() to remove previously added custom toolbar view that was added by exactly the @param fragment
 * Should not be created before onCreateView is called
 */
class ETToolbarBridge(private val fragment: Fragment) {
    private val toolbarTag = "$WL_TOOLBAR_BASE_TAG$fragment"
    private val customToolbarView: View?
        get() = fragment.activity?.toolBarContainer?.findViewWithTag(toolbarTag)

    /**
     * Supposed to be called from SideEffect {} in onCreateView
     * If the custom view was already set by the fragment then it does nothing
     */
    fun updateToolbar(content: @Composable () -> Unit) {
        if (customToolbarView == null) {
            fragment.activity?.toolBarContainer?.addView(
                ComposeView(fragment.requireContext())
                    .apply {
                        setContent { EtradeTheme(content = content) }
                        tag = toolbarTag
                    }
            )
            customToolbarView?.bringToFront()
        }
    }

    fun removeToolbar() {
        val toolbarView = fragment.activity?.toolBarContainer?.findViewWithTag<View>(toolbarTag)
        toolbarView?.let { fragment.activity?.toolBarContainer?.removeView(it) }
    }
}
