package com.etrade.mobilepro.trade.form.api

import com.etrade.mobilepro.orders.api.TransactionType

enum class TradeAction {
    BUY,
    BUY_TO_COVER,
    EXCHANGE,
    SELL,
    SELL_SHORT
}

fun TransactionType.getTradeAction() = when (this) {
    TransactionType.BUY_OPEN, TransactionType.BUY_OPEN_OPT -> TradeAction.BUY
    TransactionType.SELL_CLOSE, TransactionType.SELL_CLOSE_OPT -> TradeAction.SELL
    TransactionType.SELL_OPEN, TransactionType.SELL_OPEN_OPT -> TradeAction.SELL_SHORT
    TransactionType.BUY_CLOSE, TransactionType.BUY_CLOSE_OPT -> TradeAction.BUY_TO_COVER
    TransactionType.UNKNOWN -> TradeAction.BUY
}

@Suppress("ComplexMethod")
fun TradeAction.getTransactionType(isOption: Boolean): TransactionType {
    return if (isOption) {
        when (this) {
            TradeAction.BUY -> TransactionType.BUY_OPEN_OPT
            TradeAction.BUY_TO_COVER -> TransactionType.BUY_CLOSE_OPT
            TradeAction.SELL -> TransactionType.SELL_CLOSE_OPT
            TradeAction.SELL_SHORT -> TransactionType.SELL_OPEN_OPT
            else -> TransactionType.UNKNOWN
        }
    } else {
        when (this) {
            TradeAction.BUY -> TransactionType.BUY_OPEN
            TradeAction.BUY_TO_COVER -> TransactionType.BUY_CLOSE
            TradeAction.SELL -> TransactionType.SELL_CLOSE
            TradeAction.SELL_SHORT -> TransactionType.SELL_OPEN
            else -> TransactionType.UNKNOWN
        }
    }
}
