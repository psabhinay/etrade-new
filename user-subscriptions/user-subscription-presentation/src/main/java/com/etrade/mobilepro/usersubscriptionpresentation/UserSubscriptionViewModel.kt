package com.etrade.mobilepro.usersubscriptionpresentation

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2BookType
import com.etrade.mobilepro.usersubscriptionapi.BookFilter
import com.etrade.mobilepro.usersubscriptionapi.UserSubscriptionRepo
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.util.ArrayList
import javax.inject.Inject

class UserSubscriptionViewModel @Inject constructor(
    private val userSubscriptionRepo: UserSubscriptionRepo,
    private val user: User,
    private val resources: Resources,
    private val streamingStatusController: StreamingStatusController
) : ViewModel() {

    private val mutableViewState = MutableLiveData<Resource<BookFilter>>()
    val viewState: LiveData<Resource<BookFilter>>
        get() = mutableViewState

    private var _userMdaState: MutableLiveData<ConsumableLiveEvent<Boolean>> = MutableLiveData()
    private val logger = LoggerFactory.getLogger(UserSubscriptionViewModel::class.java)

    val userMdaState: LiveData<ConsumableLiveEvent<Boolean>>
        get() = _userMdaState

    private val mutableBookFilters = MutableLiveData<List<DropDownMultiSelector.Selectable<BookFilter>>>()
    val bookFilters: LiveData<List<DropDownMultiSelector.Selectable<BookFilter>>>
        get() = mutableBookFilters

    private val mutableSelectedBookFilter = MutableLiveData<BookFilter>()
    val selectedBookFilter: LiveData<BookFilter>
        get() = mutableSelectedBookFilter

    private var filterList: List<BookFilter> = emptyList()

    fun setBookFilter(bookType: BookFilter?) {
        mutableSelectedBookFilter.value = bookType ?: BookFilter(Level2BookType.NO_BOOK, false)
        mutableBookFilters.value = filterList.map {
            DropDownMultiSelector.Selectable(
                it.getDisplayString(resources),
                it,
                it == selectedBookFilter.value,
                !it.requiresSubscription
            )
        }
    }

    fun getSubscriptions(isOption: Boolean) {
        mutableViewState.value = Resource.Loading()
        viewModelScope.launch {
            userSubscriptionRepo.getBookFilters(isOption).onSuccess { result ->
                mutableViewState.value = Resource.Success()
                filterList = result.sortedWith(compareBy({ it.requiresSubscription }, { it.getDisplayString(resources) }))
                setBookFilter(
                    if (filterList.contains(selectedBookFilter.value)) {
                        selectedBookFilter.value
                    } else {
                        filterList.firstOrNull { filter ->
                            !filter.requiresSubscription
                        }
                    }
                )
            }.onFailure {
                onLoadingFailure(it, isOption)
            }
        }
    }

    @SuppressWarnings("MagicNumber") // We know the size of the array list before filling it
    private fun onLoadingFailure(it: Throwable, isOption: Boolean) {
        mutableViewState.value = Resource.Failed()
        logger.error("Failed to retrieve subscription book filters", it)
        filterList = if (!isOption) {
            val bookFilters = ArrayList<BookFilter>(5)
            bookFilters.add(BookFilter(Level2BookType.ARCA, true))
            bookFilters.add(BookFilter(Level2BookType.NSDQ, true))
            bookFilters.add(BookFilter(Level2BookType.MONTAGE, true))
            bookFilters.add(BookFilter(Level2BookType.NYSE_OPEN, true))
            bookFilters.add(BookFilter(Level2BookType.MONTAGE, true))
            bookFilters
        } else {
            val bookFilters = ArrayList<BookFilter>(1)
            bookFilters.add(BookFilter(Level2BookType.MONTAGE, true))
            bookFilters
        }
    }

    fun checkUserIsRealTimeStreaming() {
        if (user.isRealtimeUser) {
            _userMdaState.postValue(ConsumableLiveEvent(true))
        } else {
            checkAndUpdateMda()
        }
    }

    fun checkAndUpdateMda() {
        viewModelScope.launch {
            userSubscriptionRepo.loadUserMda().onSuccess {
                val isRealtime = it.data?.isRealtime == true
                user.isRealtimeUser = isRealtime
                streamingStatusController.isStreamingToggleEnabled = isRealtime
                _userMdaState.value = ConsumableLiveEvent(isRealtime)
            }.onFailure {
                logger.error("Failed to retrieve user MDA status", it)
                user.isRealtimeUser = false
                streamingStatusController.isStreamingToggleEnabled = false
                _userMdaState.value = ConsumableLiveEvent(false)
            }
        }
    }
}
