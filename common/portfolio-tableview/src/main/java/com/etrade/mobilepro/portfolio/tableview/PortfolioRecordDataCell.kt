package com.etrade.mobilepro.portfolio.tableview

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithInstrumentType
import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell
import com.etrade.mobilepro.util.color.DataColor

class PortfolioRecordDataCell(
    rowId: String,
    displayText: String,
    value: Any,
    dataColor: DataColor,
    override val instrumentType: InstrumentType,
    val baseValue: Any?, // we need this property to be able to make a decision if we should update DateCell
    displayTextContentDescription: String = displayText
) : DataCell(rowId, displayText, value, dataColor, displayTextContentDescription), WithInstrumentType
