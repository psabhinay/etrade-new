package com.etrade.mobilepro.trade.presentation.confirmation

import com.etrade.mobilepro.trade.presentation.util.OrderDisplayItem

sealed class OrderConfirmationItem {
    class Divider(val label: CharSequence) : OrderConfirmationItem()
    class Order(val displayItem: OrderDisplayItem) : OrderConfirmationItem()
}
