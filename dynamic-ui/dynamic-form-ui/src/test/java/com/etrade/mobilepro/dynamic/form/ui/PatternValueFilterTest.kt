package com.etrade.mobilepro.dynamic.form.ui

import androidx.core.text.toSpanned
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class PatternValueFilterTest {

    private val filter = PatternValueFilter(createDecimalPattern(5))

    @Test
    fun `check valid deletion`() {
        listOf(
            "",
            "1",
            "12.",
            "123.45"
        ).forEach { dest ->
            val dend = dest.length
            for (dstart in dend - 1 downTo 0) {
                assertNull("dest=$dest, dstart=$dstart, dend=$dend", filter.filter("", 0, 0, dest.toSpanned(), dstart, dend))
                assertNull("dest=$dest, dstart=$dstart, dend=$dend", filter.filter("123", 1, 1, dest.toSpanned(), dstart, dend))
            }
        }
    }

    @Test
    fun `check whole part deletion deletion`() {
        assertEquals("0", filter.filter("", 0, 0, "123.45".toSpanned(), 0, 3))
        assertNull(filter.filter("", 0, 0, "123.45".toSpanned(), 0, 4))
    }

    @Test
    fun `check valid append`() {
        listOf(
            "",
            "1",
            "12.",
            "123.45"
        ).forEach { dest ->
            assertNull(filter.filter("123", 0, 3, dest.toSpanned(), dest.length, dest.length))
        }
    }

    @Test
    fun `check valid replace`() {
        assertNull(filter.filter("0.12345", 0, 7, "".toSpanned(), 0, 0))
        assertNull(filter.filter("0.12345", 0, 7, "1".toSpanned(), 0, 1))
        assertNull(filter.filter("0.12345", 0, 7, "12.".toSpanned(), 1, 3))
        assertNull(filter.filter("1.2345", 0, 6, "123.45".toSpanned(), 2, 5))
    }

    @Test
    fun `check partial replace`() {
        assertEquals("0.12345", filter.filter("0.123456789", 0, 11, "".toSpanned(), 0, 0))
        assertEquals("0.12345", filter.filter("0.123456789", 0, 11, "123.45678".toSpanned(), 2, 9))
        assertEquals("0.12", filter.filter("0.123456789", 0, 11, "123.45678".toSpanned(), 2, 6))
        assertEquals("0.", filter.filter(".", 0, 1, "".toSpanned(), 0, 0))
    }
}
