package com.etrade.mobilepro.movers

import android.view.View
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.android.accessibility.processAccessibilityStreamingEvent

@BindingAdapter("processMoverDescription")
fun View.processMoverDescription(moverItem: MoverItem?) {
    moverItem?.let {
        processAccessibilityStreamingEvent(
            "${it.displaySymbol.characterByCharacter()}, " +
                "${it.priceDescription ?: it.price}, " +
                "${it.gainDescription ?: it.gain}, " +
                "${it.optionsExpiryDate ?: ""}, " +
                (it.optionsStrikePricePlusType ?: "")
        )
    }
}
