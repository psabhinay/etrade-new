package com.etrade.mobilepro.sar.service

const val DEFAULT_CHECK_INTERVAL_MILLIS = 30000L

data class SarMessage(
    val nextCheckInterval: Long,
    val errorMsg: String? = null,
    val terminateSession: Boolean = false,
    val isCookieError: Boolean = false
)
