package org.chromium.customtabsclient.shared

import androidx.browser.customtabs.CustomTabsClient

/**
 * Callback for events when connecting and diconnecting from Custom Tabs Service.
 */
interface CustomTabServiceConnectionCallback {
    /**
     * Called when the service is connected.
     * @param client a CustomTabsClient
     */
    fun onServiceConnected(client: CustomTabsClient)

    /**
     * Called when the service is disconnected.
     */
    fun onServiceDisconnected()
}
