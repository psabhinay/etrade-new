package com.etrade.mobilepro.trade.presentation.form.delegate.dependencies

import android.content.res.Resources
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.ConditionalPriceTypeValueId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.PriceInputVisibilityProvider

internal open class StockOptionsFactoryDelegateDependenciesImpl(
    inputId: OptionTradeFormInputId,
    resources: Resources
) : FormFactoryDelegateDependencies {

    private val priceTypeValueId: PriceTypeValueId = inputId.priceTypeValueId
    private val termValueId: OrderTermValueId = inputId.termValueId

    override val saveAndPreviewActionRequiredInputs = setOf(
        inputId.accountId,
        inputId.securityType,
        inputId.lookupSymbol,
        inputId.priceType,
        inputId.term,
        inputId.limitPrice,
        inputId.limitPriceForStopLimit,
        inputId.stopPrice,
        inputId.stopPriceForStopLimit
    )

    override val allOrNoneValidPriceTypes = listOf(
        priceTypeValueId.even,
        priceTypeValueId.limit,
        priceTypeValueId.netCredit,
        priceTypeValueId.netDebit,
        priceTypeValueId.stopLimit
    )

    override val allOrNoneInvalidOrderTerms = listOf(
        termValueId.fillOrKill,
        termValueId.immediateOrCancel
    )

    override val limitPriceValidPriceTypes = listOf(
        priceTypeValueId.limit,
        priceTypeValueId.netCredit,
        priceTypeValueId.netDebit
    )

    override val stopPriceValidPriceTypes = listOfNotNull(
        priceTypeValueId.stop,
        priceTypeValueId.trailingStopPercent,
        priceTypeValueId.trailingStopDollar,
        (priceTypeValueId as? ConditionalPriceTypeValueId)?.hiddenStop
    )

    override val priceTypePriceLabelMap = mapOf(
        priceTypeValueId.limit to resources.getString(R.string.trade_hint_limit_price),
        priceTypeValueId.netCredit to resources.getString(R.string.trade_hint_price),
        priceTypeValueId.netDebit to resources.getString(R.string.trade_hint_price),
        priceTypeValueId.stop to resources.getString(R.string.trade_hint_stop_price),
        priceTypeValueId.stopLimit to resources.getString(R.string.trade_hint_stop_price),
        priceTypeValueId.trailingStopDollar to resources.getString(R.string.trade_hint_trailing_stop_dollar),
        priceTypeValueId.trailingStopPercent to resources.getString(R.string.trade_hint_trailing_stop_percent)
    ) + if (priceTypeValueId is ConditionalPriceTypeValueId) {
        mapOf<String, String>(priceTypeValueId.hiddenStop to resources.getString(R.string.trade_hint_trailing_stop_dollar))
    } else {
        emptyMap()
    }

    override val limitOrderTermLabelMap = mapOf(
        termValueId.goodUntilCancel to resources.getString(R.string.trade_term_gtc),
        termValueId.fillOrKill to resources.getString(R.string.trade_term_fok),
        termValueId.immediateOrCancel to resources.getString(R.string.trade_term_ioc)
    )

    override val isLimitPriceInputVisible: PriceInputVisibilityProvider = { it in limitPriceValidPriceTypes }

    override val isStopPriceInputVisible: PriceInputVisibilityProvider = { it in stopPriceValidPriceTypes }

    override val isStopLimitRowVisible: PriceInputVisibilityProvider = { it == priceTypeValueId.stopLimit }

    override val limitPriceLabel: String = resources.getString(R.string.trade_hint_limit_price)

    override val stopPriceLabel: String = resources.getString(R.string.trade_hint_stop_price)

    override val allowShortSellUpdatedTerms: Boolean = true
}
