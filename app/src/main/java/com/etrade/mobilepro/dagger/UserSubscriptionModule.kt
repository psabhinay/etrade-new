package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.usersubscriptionapi.UserSubscriptionRepo
import com.etrade.mobilepro.usersubscriptiondata.DefaultUserSubscriptionRepo
import com.etrade.mobilepro.usersubscriptiondata.UserSubscriptionApiClient
import com.etrade.mobilepro.usersubscriptionpresentation.SubscriptionStreamingToggle
import com.etrade.mobilepro.userviewmodel.UserViewModel
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class UserSubscriptionModule {
    @Provides
    @Singleton
    internal fun provideUserSubscriptionApiClient(@Web retrofit: Retrofit): UserSubscriptionApiClient = retrofit.create(UserSubscriptionApiClient::class.java)

    @Provides
    @Singleton
    internal fun provideUserSubscriptionRepo(apiClient: UserSubscriptionApiClient): UserSubscriptionRepo = DefaultUserSubscriptionRepo(apiClient)

    @Provides
    internal fun provideUserStreamingToggle(userViewModel: UserViewModel, streamingStatusController: StreamingStatusController): SubscriptionStreamingToggle =
        object : SubscriptionStreamingToggle {
            override fun toggle(toggle: Boolean) {
                userViewModel.streamingToggleStateChange(toggle)
                streamingStatusController.isStreamingToggleEnabled = toggle
            }
        }
}
