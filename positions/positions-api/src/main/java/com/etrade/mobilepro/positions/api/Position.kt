package com.etrade.mobilepro.positions.api

import com.etrade.mobilepro.instrument.WithSymbol
import java.math.BigDecimal

interface Position : WithSymbol {
    val positionId: String
    val daysGain: BigDecimal?
    val totalGain: BigDecimal?
    val pricePaid: BigDecimal?
    val commission: BigDecimal?
    val fees: BigDecimal?
    val quantity: BigDecimal?
    val displayQuantity: BigDecimal?
    val daysGainPercentage: BigDecimal?
    val marketValue: BigDecimal?
    val totalGainPercentage: BigDecimal?
    val todayClose: BigDecimal?
    val baseSymbolAndPrice: String?
    val mark: BigDecimal?
    val inTheMoneyFlag: Boolean?
}

val Position.isShort
    get() = quantity?.signum() ?: 0 < 0
