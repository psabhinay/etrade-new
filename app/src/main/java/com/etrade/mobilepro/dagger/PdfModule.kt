package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.pdf.api.PdfRepository
import com.etrade.mobilepro.pdf.data.DefaultPdfRepositoryImpl
import com.etrade.mobilepro.pdf.data.PdfRestClient
import com.etrade.mobilepro.pdf.presentation.PdfViewModelFactoryFactory
import com.etrade.mobilepro.pdf.presentation.PdfViewModelFactoryFactoryImp
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
abstract class PdfModule {

    @Binds
    @Singleton
    abstract fun providesPdfRepository(defaultPdfRepositoryImpl: DefaultPdfRepositoryImpl): PdfRepository

    @Binds
    @Singleton
    abstract fun bindPdfViewModelFactoryFactory(factory: PdfViewModelFactoryFactoryImp): PdfViewModelFactoryFactory

    companion object {
        @Provides
        @Singleton
        fun providesPdfRepositoryClient(@MobileTrade retrofit: Retrofit): PdfRestClient = retrofit.create(PdfRestClient::class.java)
    }
}
