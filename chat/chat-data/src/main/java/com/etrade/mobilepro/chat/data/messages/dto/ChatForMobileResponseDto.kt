package com.etrade.mobilepro.chat.data.messages.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChatForMobileResponseDto(

    @Json(name = "data")
    val data: MobileChatDataDto?,

    @Json(name = "success")
    val success: Boolean?
)

@JsonClass(generateAdapter = true)
data class MobileChatDataDto(
    @Json(name = "ChatResponse")
    val chatInfoModel: ChatInfoDto?,

    @Json(name = "Messages")
    val messages: String?
)

@JsonClass(generateAdapter = true)
data class ChatInfoDto(
    @Json(name = "sessionID")
    val sessionID: String?
)
