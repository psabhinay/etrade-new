package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.eo.rest.retrofit.Scalar
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path

internal interface SaveOrderApiClient {

    @JsonMoshi
    @POST("/app/preparedorder/{path}.json")
    @Headers("Content-Type: application/json")
    suspend fun saveOrder(@Path("path") path: String, @Scalar @Body body: String): ServerResponseDto<SaveOrderResponseDto>
}
