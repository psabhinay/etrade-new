package com.etrade.mobilepro.pdf.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.pdf.presentation.databinding.PdfViewLayoutBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class PdfFragment @Inject constructor(
    private val pdfViewModelFactoryFactory: PdfViewModelFactoryFactory,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : Fragment() {
    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }) { view } }
    private var _binding: PdfViewLayoutBinding? = null
    private val binding get() = _binding!!
    private val args: PdfFragmentArgs by navArgs()
    private val pdfViewModel: PdfViewModel by viewModels { pdfViewModelFactoryFactory.create(args.url) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = PdfViewLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewModel()
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    private fun setupViewModel() {
        pdfViewModel.pdfViewStateLiveData.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is PdfViewModel.PdfViewState.Loading -> {
                        binding.loadingIndicator.show()
                    }
                    is PdfViewModel.PdfViewState.Error -> {
                        binding.loadingIndicator.hide()
                        snackBarUtil.errorSnackbar()
                    }
                    is PdfViewModel.PdfViewState.Success -> {
                        binding.loadingIndicator.hide()
                        binding.pdfView.loadFile(it.pdfFile)
                    }
                }
            }
        )
    }
}
