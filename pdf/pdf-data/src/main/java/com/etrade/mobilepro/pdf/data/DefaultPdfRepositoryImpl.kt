package com.etrade.mobilepro.pdf.data

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.pdf.api.PdfRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.io.InputStream
import javax.inject.Inject

class DefaultPdfRepositoryImpl @Inject constructor(
    private val pdfRestClient: PdfRestClient
) : PdfRepository {

    override suspend fun downloadPdf(url: String, filePath: File): ETResult<File> {
        return pdfRestClient.runCatchingET {
            downloadPdfFromUrl(url)
        }.mapCatching { responseBody ->
            withContext(Dispatchers.IO) {
                responseBody.byteStream().toFile(filePath)
                filePath
            }
        }
    }

    private fun InputStream.toFile(path: File) {
        use { input ->
            path.outputStream().use { input.copyTo(it) }
        }
    }
}
