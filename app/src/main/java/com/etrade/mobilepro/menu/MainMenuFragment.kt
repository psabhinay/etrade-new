package com.etrade.mobilepro.menu

import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.debugmenu.api.TripleEventSequenceDetector
import com.etrade.mobilepro.landing.api.LandingNavigationHelper
import com.etrade.mobilepro.menu.helper.MainMenuPageHelper
import com.etrade.mobilepro.menu.viewmodel.MainMenuPageViewModel
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.userviewmodel.UserViewModel
import javax.inject.Inject

class MainMenuFragment @Inject constructor(
    private val applicationPreferences: ApplicationPreferences,
    viewModelFactory: ViewModelProvider.Factory,
    userViewModel: UserViewModel,
    private val detector: TripleEventSequenceDetector
) : AppBaseMenuFragment(viewModelFactory, userViewModel) {

    override val menuPageViewModel by viewModels<MainMenuPageViewModel> { viewModelFactory }

    private val mainMenuPageHelper: MainMenuPageHelper by lazy {
        MainMenuPageHelper(this, applicationPreferences, viewModelFactory)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        detector.setup {
            findNavController().navigate(MainMenuFragmentDirections.actionMenuFragmentToDebugmenuFragment())
        }

        if (menuPageViewModel.viewState.value == null) {
            menuPageViewModel.loadDynamicMenuPageItems()
        }

        menuPageViewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is MainMenuPageViewModel.ViewState.Loading -> binding.loadingIndicator.visibility = VISIBLE
                    is MainMenuPageViewModel.ViewState.Success -> binding.loadingIndicator.visibility = GONE
                }
            }
        )
    }

    private fun TextView?.applyPadding() {
        val padding = context?.resources?.getDimensionPixelOffset(R.dimen.spacing_small) ?: 0
        this?.setPadding(padding, 0, padding, 0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        setupToolbarActionStart()
    }

    override fun onResume() {
        super.onResume()
        saveMenuDirection(null)
        setupDebugMenuListener()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mainMenuPageHelper.dismissTooltip()
    }

    override fun navigate(appAction: Uri) {
        when (appAction.path) {
            MenuDeepLink.QUOTES.path -> mainMenuPageHelper.openQuotes()
            else -> super.navigate(appAction)
        }
    }

    override fun onMenuItemClick(menuItem: TextMenuItem) {
        super.onMenuItemClick(menuItem)
        menuItem.localyticsTag?.let {
            menuPageViewModel.trackScreen(it.toString())
        }
    }

    private fun setupToolbarActionStart() {
        requireActivity().apply {
            toolbarActionEndView?.apply {
                visibility = VISIBLE
                setText(R.string.button_text_logon)

                setOnClickListener {
                    val authState = userViewModel.peekUserSessionState

                    if (authState is SessionStateChange.AUTHENTICATED) {
                        userViewModel.userSessionStateChange(SessionStateChange.UNAUTHENTICATED)
                    } else if (authState is SessionStateChange.UNAUTHENTICATED) {
                        startLoginActivity()
                    }
                }
            }

            userViewModel.peekUserSessionState?.let { updateLogonLogoffTitle(it) }

            userViewModel.userSessionState.observe(
                viewLifecycleOwner,
                Observer {
                    updateLogonLogoffTitle(it.peekContent())
                }
            )
        }
    }

    private fun updateLogonLogoffTitle(sessionStateChange: SessionStateChange) {
        activity?.apply {
            toolbarActionEndView?.apply {
                visibility = VISIBLE
                when (sessionStateChange) {
                    is SessionStateChange.AUTHENTICATED -> {
                        setToolbarActionTextAndContentDescription(R.string.button_text_logoff)
                    }
                    is SessionStateChange.UNAUTHENTICATED -> {
                        setToolbarActionTextAndContentDescription(R.string.button_text_logon)
                    }
                    else -> {
                        return
                    }
                }
            }
        }
    }

    private fun startLoginActivity() {
        findNavController().navigate(R.id.activity_login, LandingNavigationHelper.createShouldHandleLandingBundle())
    }

    private fun setupDebugMenuListener() {
        toolbarTitleView?.apply {
            setOnClickListener {
                detector.onEvent()
            }
            applyPadding()
        }
    }
}
