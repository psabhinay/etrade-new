package com.etrade.mobilepro.trade.data.mutualfunds

import com.etrade.mobilepro.trade.api.mutualfunds.ConfirmedMutualFundsOrder
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrder
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrderPlaceResponse
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrderResponse
import com.etrade.mobilepro.trade.data.mutualfunds.dto.MutualFundsTradeOrderResponseDto
import com.etrade.mobilepro.trade.data.util.formatOrderPlacedTime

internal class MutualFundsOrderPlaceResponseWrapper(
    responseDto: MutualFundsTradeOrderResponseDto
) : MutualFundsOrderPlaceResponse, MutualFundsOrderResponse by responseDto {
    override val orders: List<ConfirmedMutualFundsOrder> = responseDto.orders.map { it.confirmedOrder(responseDto.orderDto?.timePlacedMillis) }
}

private fun MutualFundsOrder.confirmedOrder(timePlacedMillis: String?): ConfirmedMutualFundsOrder {
    return object : ConfirmedMutualFundsOrder, MutualFundsOrder by this {
        override val confirmationOrderId: String? = orderId
        override val placedDate: String? = formatOrderPlacedTime(timePlacedMillis?.toLongOrNull())
    }
}
