package com.etrade.mobilepro.ui_demo.tooltip

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.tooltip.Tooltip
import com.etrade.mobilepro.tooltip.TooltipGravity
import com.etrade.mobilepro.ui_demo.R
import com.etrade.mobilepro.ui_demo.databinding.ActivityTooltipBinding
import com.etrade.mobilepro.util.android.binding.viewBinding

class TooltipActivity : AppCompatActivity(), View.OnClickListener {

    private val binding by viewBinding(ActivityTooltipBinding::inflate)

    private val tooltip: Tooltip = Tooltip()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(binding.root)

        binding.topLeftButton.setOnClickListener(this)
        binding.topRightButton.setOnClickListener(this)
        binding.bottomLeftButton.setOnClickListener(this)
        binding.bottomRightButton.setOnClickListener(this)
        binding.centerButton.setOnClickListener(this)

        for (i in 0..10) {
            binding.itemsContainer.addView(TextView(this).apply {
                text = "Test: $i"
                resources.getDimensionPixelSize(R.dimen.spacing_medium)
                    .let { setPadding(it, it, it, it) }
                setOnClickListener(this@TooltipActivity)
            })
        }
    }

    override fun onClick(v: View?) {
        v?.let {
            tooltip.show {
                target = it
                text = "Swipe left and right to get\nPortfolios, Balances, Orders,\nand Transactions"
                gravity = getGravity(it.id)
            }
        }
    }

    private fun getGravity(id: Int): TooltipGravity = when (id) {
        R.id.top_left_button, R.id.top_right_button, R.id.center_button -> TooltipGravity.BOTTOM
        R.id.bottom_left_button, R.id.bottom_right_button -> TooltipGravity.TOP
        else -> TooltipGravity.RIGHT
    }
}
