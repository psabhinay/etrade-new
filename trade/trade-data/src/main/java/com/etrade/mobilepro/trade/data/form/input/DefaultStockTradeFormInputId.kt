package com.etrade.mobilepro.trade.data.form.input

import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.data.form.CommonAllOrNoneValueCoder
import com.etrade.mobilepro.trade.data.form.DefaultSymbolInputValueCoder
import com.etrade.mobilepro.trade.data.form.value.StockActionValueId
import com.etrade.mobilepro.trade.data.form.value.StockOrderTermValueId
import com.etrade.mobilepro.trade.data.form.value.StockPriceTypeValueId
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.util.ValueCoder

object DefaultStockTradeFormInputId :
    StockTradeFormInputId,
    TradeFormInputId by DefaultTradeFormInputId,
    WithAdvancedTradeFormInputId by DefaultAdvancedTradeFormInputId {
    override val accountId: String = "AccountId"
    override val action: String = "Action"
    override val actionValueId: ActionValueId = StockActionValueId
    override val allOrNone: String = "AllOrNone"
    override val allOrNoneValueCoder: ValueCoder<Boolean> = CommonAllOrNoneValueCoder
    override val limitPrice: String = "LimitPrice"
    override val limitPriceForStopLimit: String = "LimitPriceForStopLimit"
    override val orderId: String = "OrderId"
    override val originalQuantity = "originalQuantity"
    override val priceType: String = "PriceType"
    override val priceTypeValueId: PriceTypeValueId = StockPriceTypeValueId
    override val positionLotId: String = "lotid"
    override val quantity: String = "Quantity"
    override val stopPrice: String = "StopPrice"
    override val stopPriceForStopLimit: String = "StopPriceForStopLimit"
    override val symbol: String = "Ticker"
    override val symbolCoder: ValueCoder<WithSymbolInfo> = DefaultSymbolInputValueCoder
    override val term: String = "Term"
    override val termValueId: OrderTermValueId = StockOrderTermValueId
    override val walkthroughFormCompleted: String = "WalkthroughFormCompleted"
}
