package com.etrade.mobilepro.billpay.presentation.paymentlist

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.Payment
import com.etrade.mobilepro.billpay.api.PaymentStatus
import com.etrade.mobilepro.billpay.presentation.paymentdetail.ParcelablePayment
import com.etrade.mobilepro.billpay.presentation.paymentdetail.toParcelable
import com.etrade.mobilepro.billpay.usecase.PaymentListRequestParameters
import com.etrade.mobilepro.billpay.usecase.PaymentListUseCase
import com.etrade.mobilepro.common.Restorable
import com.etrade.mobilepro.common.RestorableViewModelDelegate
import com.etrade.mobilepro.livedata.updateValue
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.threeten.bp.Instant
import org.threeten.bp.temporal.ChronoUnit

private val logger = LoggerFactory.getLogger(PaymentListViewModel::class.java)

class PaymentListViewModel @AssistedInject constructor(
    private val paymentStatusFilterMap: Map<PaymentStatus, PaymentStatusFilterItem>,
    private val resources: Resources,
    private val paymentListUseCase: PaymentListUseCase,
    @Assisted private val sessionId: String?,
    @Assisted private val allAccountList: List<AccountDetails>,
    restorableDelegate: RestorableViewModelDelegate
) : ViewModel() {

    private val allAccountsMap = allAccountList.map { it.accountId to it }.toMap()

    private val _viewState: MutableLiveData<ViewState> = MutableLiveData()
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    internal val paymentStatusFilterList: List<PaymentStatusFilterItem>
        get() = paymentStatusFilterMap.values.toList()

    internal val selectedPaymentStatusFilterIndex: Int?
        get() = paymentStatusFilterMap.keys.indexOf(viewState.value?.selectedStatus)

    internal val dateRangeFilterLabelList: List<String>
        get() = DateRangeFilter.labels(resources)

    internal val selectedDateRangeFilterIndex: Int?
        get() = viewState.value?.selectedDateRange?.ordinal ?: DateRangeFilter.DAYS_30.ordinal

    private val paymentListRequest: PaymentListRequestParameters?
        get() {
            return sessionId?.let {
                val selectedDateRangeFilter = viewState.value?.selectedDateRange ?: DateRangeFilter.DAYS_30
                PaymentListRequestParameters(
                    status = viewState.value?.selectedStatus ?: PaymentStatus.ALL,
                    numOfDays = selectedDateRangeFilter.days.times(2),
                    startDate = Instant.now().minus(selectedDateRangeFilter.days.toLong(), ChronoUnit.DAYS),
                    sessionId = it,
                    allAccounts = allAccountsMap
                )
            }
        }

    @AssistedFactory
    interface Factory {
        fun create(
            sessionId: String?,
            allAccountList: List<AccountDetails>?
        ): PaymentListViewModel
    }

    init {
        restorableDelegate.init(this::class.java, viewState, null)
        val defaultViewState = ViewState()
        _viewState.value = restorableDelegate.getRestoredState(this::class.java) as? ViewState ?: defaultViewState

        fetchData()
    }

    internal fun refresh() {
        fetchData(refresh = true)
    }

    private fun fetchData(refresh: Boolean = false) {
        paymentListRequest?.let { requestParams ->
            _viewState.updateValue { state -> state.copy(isLoading = !refresh, isRefreshing = refresh, error = null) }
            viewModelScope.launch {
                paymentListUseCase.execute(requestParams).fold(
                    onSuccess = {
                        _viewState.updateValue { state -> state.copy(isLoading = false, isRefreshing = false, items = it) }
                    },
                    onFailure = {
                        logger.error(it.localizedMessage)
                        _viewState.updateValue { state ->
                            state.copy(
                                isLoading = false,
                                isRefreshing = false,
                                error = Error(it) { fetchData(refresh = refresh) }
                            )
                        }
                    }
                )
            }
        }
    }

    fun onInitBundleData(status: PaymentStatus, selectedDateRange: String) {
        if (_viewState.updateValue { state ->
            state.copy(
                    selectedStatus = status,
                    selectedDateRange = DateRangeFilter.getDateRangeByLabel(resources, selectedDateRange)
                )
        }
        ) {
            fetchData()
        }
    }

    fun onPaymentStatusSelected(status: PaymentStatus) {
        if (_viewState.updateValue { state -> state.copy(selectedStatus = status) }) {
            fetchData()
        }
    }

    fun onDateRangeSelected(selectedDateRange: String) {
        if (_viewState.updateValue { state -> state.copy(selectedDateRange = DateRangeFilter.getDateRangeByLabel(resources, selectedDateRange)) }) {
            fetchData()
        }
    }

    fun findParcelablePaymentById(paymentId: String, accountName: String?): ParcelablePayment? {
        return _viewState.value?.items
            ?.first { it.paymentId == paymentId }
            ?.run { toParcelable(accountName, payeeAccountNumber) }
    }
}

internal data class ViewState(
    val isLoading: Boolean = false,
    val isRefreshing: Boolean = false,
    val items: List<Payment>? = null,
    val selectedStatus: PaymentStatus = PaymentStatus.ALL,
    val selectedDateRange: DateRangeFilter = DateRangeFilter.DAYS_30,
    val error: Error? = null
) : Restorable

internal class Error(
    val exception: Throwable,
    val retryAction: (() -> Unit)? = null
)
