package com.etrade.mobilepro.util.android.extension

import android.widget.TextView
import com.etrade.mobilepro.util.android.R

fun TextView.displayDropDownArrow(show: Boolean) {
    if (show) {
        setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown_arrow, 0)
        setPadding(0, 0, 0, 0)
    } else {
        setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0)
        setPadding(0, 0, resources.getDimension(R.dimen.spacing_medium).toInt(), 0)
    }
}
