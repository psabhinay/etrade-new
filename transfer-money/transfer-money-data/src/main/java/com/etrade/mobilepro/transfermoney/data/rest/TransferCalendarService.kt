package com.etrade.mobilepro.transfermoney.data.rest

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.GET

interface TransferCalendarService {

    @JsonMoshi
    @GET("app/cashtransfer/calendar.json")
    suspend fun getCalendar(): ServerResponseDto<TransferCalendarResponseDto>
}
