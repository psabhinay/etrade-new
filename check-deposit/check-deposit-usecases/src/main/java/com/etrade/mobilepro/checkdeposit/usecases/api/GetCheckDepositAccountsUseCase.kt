package com.etrade.mobilepro.checkdeposit.usecases.api

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.util.UseCase

interface GetCheckDepositAccountsUseCase : UseCase<Unit, ETResult<EligibleAccounts>>
