package com.etrade.mobilepro.completeview.edit

import android.content.res.Resources
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.dynamicui.DynamicViewType
import com.etrade.mobilepro.home.overview.edit.BaseOverviewWidgetRepo
import com.etrade.mobilepro.home.overview.edit.OverviewWidget
import com.etrade.mobilepro.msaccounts.api.MsPreferencesRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import okhttp3.internal.toImmutableList
import okhttp3.internal.toImmutableMap
import javax.inject.Inject
import javax.inject.Qualifier

private const val ENABLED_WIDGETS_STORAGE_KEY = "ENABLED_COMPLETE_VIEW_WIDGETS_STORAGE_KEY"
private const val SAVED_WIDGETS_STORAGE_KEY = "SAVED_COMPLETE_VIEW_WIDGETS_STORAGE_KEY"
private const val STORAGE_KEY_WITH_BROKERAGE = "_WITH_BROKERAGE_ACCOUNTS"
private const val STORAGE_KEY_WITH_MS_ACCOUNTS = "_WITH_MS_ACCOUNTS"

@Qualifier
annotation class CompleteViewRepo

class DefaultCompleteViewWidgetRepo @Inject constructor(
    private val accountListRepo: AccountListRepo,
    private val resources: Resources,
    private val msPreferencesRepo: MsPreferencesRepo,
    storage: KeyValueStorage
) : BaseOverviewWidgetRepo(storage) {

    private val isMsAccountWidgetsAvailable: Boolean
        get() = msPreferencesRepo.msUserPreferences.value.isUserEnrolled

    private val hasBrokerage: Boolean
        get() = accountListRepo.getAccountList().any {
            it.isBrokerageAccount()
        }

    override val enabledWidgetsStorageKey: String
        get() = ENABLED_WIDGETS_STORAGE_KEY.applyStorageKeySuffixes()

    override val savedWidgetsStorageKey: String
        get() = SAVED_WIDGETS_STORAGE_KEY.applyStorageKeySuffixes()

    override val allWidgets: Flow<Map<String, OverviewWidget>>
        get() = msPreferencesRepo.msUserPreferences
            .map { it.isUserEnrolled }
            .distinctUntilChanged()
            .map { msAvailable ->

                val widgets = mutableMapOf<String, OverviewWidget>()

                if (msAvailable) {
                    widgets.putAll(msAccountWidgets)
                }
                if (hasBrokerage) {
                    widgets.putAll(brokerageWidgets)
                }
                widgets.putAll(accountWidgets)

                widgets.toImmutableMap()
            }

    private val brokerageWidgets: Map<String, OverviewWidget>
        get() =
            mapOf(
                DynamicViewType.PORTFOLIO_MOVERS.dtoName to OverviewWidget(
                    DynamicViewType.PORTFOLIO_MOVERS.dtoName,
                    resources.getString(R.string.portfolio_movers)
                ),
                DynamicViewType.NEWS_PORTFOLIO.dtoName to OverviewWidget(
                    DynamicViewType.NEWS_PORTFOLIO.dtoName,
                    resources.getString(R.string.portfolio_news)
                ),
            )

    private val accountWidgets: Map<String, OverviewWidget>
        get() = mapOf(
            DynamicViewType.MARKET_MOVERS.dtoName to OverviewWidget(
                DynamicViewType.MARKET_MOVERS.dtoName,
                resources.getString(R.string.market_movers)
            ),
            DynamicViewType.NEWS_BRIEFING.dtoName to OverviewWidget(
                DynamicViewType.NEWS_BRIEFING.dtoName,
                resources.getString(R.string.briefing_news)
            ),
        )

    private val msAccountWidgets: Map<String, OverviewWidget>
        get() = mapOf(
            DynamicViewType.MS_ACCOUNTS_AGGREGATE_VALUES.dtoName to OverviewWidget(
                DynamicViewType.MS_ACCOUNTS_AGGREGATE_VALUES.dtoName,
                resources.getString(R.string.complete_view_widget_title_aggregate_values)
            ),
            DynamicViewType.MS_ACCOUNTS_SUMMARY.dtoName to OverviewWidget(
                DynamicViewType.MS_ACCOUNTS_SUMMARY.dtoName,
                resources.getString(R.string.complete_view_widget_title_ms_accounts)
            ),
        )

    override val defaultWidgetsOrder: List<String>
        get() {
            val widgets = mutableListOf<String>()

            if (isMsAccountWidgetsAvailable) {
                widgets.addAll(
                    listOf(
                        DynamicViewType.MS_ACCOUNTS_AGGREGATE_VALUES.dtoName,
                        DynamicViewType.MS_ACCOUNTS_SUMMARY.dtoName,
                    )
                )
            }

            if (hasBrokerage) {
                widgets.addAll(
                    listOf(
                        DynamicViewType.PORTFOLIO_MOVERS.dtoName,
                        DynamicViewType.NEWS_PORTFOLIO.dtoName,
                    )
                )
            }
            widgets.addAll(
                listOf(
                    DynamicViewType.MARKET_MOVERS.dtoName,
                    DynamicViewType.NEWS_BRIEFING.dtoName,
                )
            )

            return widgets.toImmutableList()
        }

    private fun String.applyStorageKeySuffixes(): String {
        var key = this
        if (hasBrokerage) {
            key += STORAGE_KEY_WITH_BROKERAGE
        }
        if (isMsAccountWidgetsAvailable) {
            key += STORAGE_KEY_WITH_MS_ACCOUNTS
        }
        return key
    }
}
