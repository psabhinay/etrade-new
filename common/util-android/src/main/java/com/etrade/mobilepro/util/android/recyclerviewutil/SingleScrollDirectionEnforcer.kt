package com.etrade.mobilepro.util.android.recyclerviewutil

import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
import kotlin.math.abs

/**
 * Solution copied from https://bladecoder.medium.com/fixing-recyclerview-nested-scrolling-in-opposite-direction-f587be5c1a04
 *
 * Needed because the default recycler view scroll detection does not check for dy > dx to find out
 * if the motion was more vertical or more horizontal.
 */

private const val HALF_PIXEL_OFFSET = 0.5f

fun RecyclerView.enforceSingleScrollDirection() {
    val enforcer = SingleScrollDirectionEnforcer()
    addOnItemTouchListener(enforcer)
    addOnScrollListener(enforcer)
}

private class SingleScrollDirectionEnforcer : RecyclerView.OnScrollListener(), OnItemTouchListener {

    private var scrollState = RecyclerView.SCROLL_STATE_IDLE
    private var scrollPointerId = -1
    private var initialTouchX = 0
    private var initialTouchY = 0
    private var dx = 0
    private var dy = 0

    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
        when (e.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                scrollPointerId = e.getPointerId(0)
                initialTouchX = (e.x + HALF_PIXEL_OFFSET).toInt()
                initialTouchY = (e.y + HALF_PIXEL_OFFSET).toInt()
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
                val actionIndex = e.actionIndex
                scrollPointerId = e.getPointerId(actionIndex)
                initialTouchX = (e.getX(actionIndex) + HALF_PIXEL_OFFSET).toInt()
                initialTouchY = (e.getY(actionIndex) + HALF_PIXEL_OFFSET).toInt()
            }
            MotionEvent.ACTION_MOVE -> {
                val index = e.findPointerIndex(scrollPointerId)
                if (index >= 0 && scrollState != RecyclerView.SCROLL_STATE_DRAGGING) {
                    val x = (e.getX(index) + HALF_PIXEL_OFFSET).toInt()
                    val y = (e.getY(index) + HALF_PIXEL_OFFSET).toInt()
                    dx = x - initialTouchX
                    dy = y - initialTouchY
                }
            }
        }
        return false
    }

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        // NO OP
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        // NO OP
    }

    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
        val oldState = scrollState
        scrollState = newState
        if (oldState == RecyclerView.SCROLL_STATE_IDLE && newState == RecyclerView.SCROLL_STATE_DRAGGING) {
            val canScrollHorizontally = recyclerView.layoutManager?.canScrollHorizontally() ?: false
            val canScrollVertically = recyclerView.layoutManager?.canScrollVertically() ?: false
            if (canScrollHorizontally != canScrollVertically) {
                // we should not scroll horizontally if the drag motion was more vertical
                val shouldNotScrollHorizontally = canScrollHorizontally && abs(dy) > abs(dx)
                // we should not scroll vertically if the drag motion was more horizontal
                val shouldNotScrollVertically = canScrollVertically && abs(dx) > abs(dy)
                if (shouldNotScrollHorizontally || shouldNotScrollVertically) {
                    recyclerView.stopScroll()
                }
            }
        }
    }
}
