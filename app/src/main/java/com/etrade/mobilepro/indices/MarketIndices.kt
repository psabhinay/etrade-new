package com.etrade.mobilepro.indices

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.indices.dto.BehaviourOptions
import com.etrade.mobilepro.indices.usecase.GetTopIndicesUseCase
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.data.response.FutureQuote
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.util.android.CallToAction
import com.etrade.mobilepro.util.android.ChangeColorPicker
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.TimestampEvent
import com.etrade.mobilepro.util.android.TimestampEventProvider
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.formatters.formatMarketPercentDataCustom
import com.etrade.mobilepro.util.invoke
import com.etrade.mobilepro.util.market.time.NEW_YORK_ZONE_ID
import com.etrade.mobilepro.util.market.time.isMarketTime
import com.etrade.mobilepro.util.safeParseBigDecimal
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.coroutines.rx2.await
import org.threeten.bp.Instant
import org.threeten.bp.format.DateTimeFormatter
import java.util.Locale

private const val TOP_INDEX_NUM = 3

class MarketIndices(
    private val getTopIndicesUseCase: GetTopIndicesUseCase,
    private val marketsService: MoverService,
    private val changeColorPicker: ChangeColorPicker,
    private val marketTradeStatusDelegate: MarketTradeStatusDelegate,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController,
    dowJonesColor: Int,
    nasdaqColor: Int,
    sP500Color: Int,
    callToAction: CallToAction?,
    defaultErrorMessage: String,
    private val useMarketChangeColor: Boolean,
    private val showFutures: Boolean,
    behaviourOptions: BehaviourOptions = BehaviourOptions()
) : BaseIndices(dowJonesColor, nasdaqColor, sP500Color, callToAction, defaultErrorMessage, behaviourOptions),
    LifecycleObserver,
    TimestampEventProvider {

    private val formatter = DateTimeFormatter.ofPattern("MM/dd/yy h:mma", Locale.US).withZone(NEW_YORK_ZONE_ID)
    private val symbolSubscriptionFields: Set<Level1Field> = StockData.fields()
    private val streamingCompositeDisposable = CompositeDisposable()

    override val timestampEvents = MutableLiveData<ConsumableLiveEvent<TimestampEvent>>()

    override suspend fun fetchData(): ETResult<IndicesData> = runCatchingET {
        streamingCompositeDisposable.clear()

        val topIndices = getTopIndicesUseCase(TOP_INDEX_NUM).getOrThrow()

        val updatedTopIndices = if (useMarketChangeColor) {
            topIndices.map {
                it.copy(indicatorColor = it.textColorRes)
            }
        } else {
            topIndices
        }

        if (streamingStatusController.isStreamingToggleEnabled) {
            subscribeToIndicesUpdates(updatedTopIndices)
        }

        var futureIndices: Pair<List<IndexDataWithColor>?, Futures?>? = null
        if (showFutures && !isMarketTime()) {
            futureIndices = getFutureIndices()
        }

        IndicesData(updatedTopIndices, futureIndices?.first, futureIndices?.second)
    }

    override fun isStreamingActive(): Boolean {
        return streamingCompositeDisposable.size() > 0
    }

    @Suppress("LongMethod")
    private suspend fun getFutureIndices(): Pair<List<IndexDataWithColor>?, Futures?> {
        val response = marketsService.getMarketFutures().await().quotes
        var futures: Futures? = null
        val indicesList = response?.mapIndexedNotNull { index, quote ->
            if (index == 0) {
                futures = resolveFutures(quote)
            }
            var indexDataWithColor: IndexDataWithColor? = null
            quote.symbol?.let { symbol ->
                quote.price?.let { price ->
                    val change = quote.change?.safeParseBigDecimal()
                    val textColor = changeColorPicker.getChangeColor(change)
                    val percentChangeWithoutSign = quote.percentchange?.trim()?.dropLast(1)
                    val percentChange: String = MarketDataFormatter.formatMarketPercentDataCustom(percentChangeWithoutSign?.toBigDecimalOrNull())
                    val formattedChange: String = MarketDataFormatter.formatMarketDataCustom(change)
                    indexDataWithColor = IndexDataWithColor(
                        "/$symbol",
                        getFuturesDisplaySymbol(symbol),
                        price,
                        "$formattedChange ($percentChange)",
                        textColorRes = textColor,
                        indicatorColor = getIndicatorColor(index),
                        isPositiveChange = quote.change?.safeParseBigDecimal()?.signum() != -1
                    )
                }
            }
            indexDataWithColor
        }
        return indicesList to futures
    }

    private fun resolveFutures(quote: FutureQuote): Futures {
        val millis = Instant.from(formatter.parse("${quote.lastTradeDate} ${quote.lastTradeTime}")).toEpochMilli()
        val formattedDateTime = DateFormattingUtils.formatFullShortTimeDateWithSlashes(millis)
        return Futures(formattedDateTime, quote.expireDate)
    }

    private fun getFuturesDisplaySymbol(symbol: String): String {
        return when (symbol) {
            "YM" -> "DOW"
            "NQ" -> "NASDAQ"
            "ES" -> "S&P 500"
            else -> symbol
        }
    }

    private fun subscribeToIndicesUpdates(indicesList: List<IndexDataWithColor>? = indices.value) {
        streamingCompositeDisposable.clear()
        indicesList?.map { index -> index.ticker }?.forEach { symbol ->
            streamingCompositeDisposable.add(
                streamingQuoteProvider.getQuote(symbol, InstrumentType.INDX, symbolSubscriptionFields).subscribeBy { data ->
                    data.timestampSinceEpoch?.let {
                        logger.debug("timestampSinceEpoch: $it")
                        timestampEvents.postValue(TimestampEvent(it).consumable())
                    }
                    val lastPrice = data.lastPrice ?: return@subscribeBy
                    indices.value?.let { allItems ->
                        val updatedItems = mutableListOf<IndexDataWithColor>()
                        allItems.forEach { indexItem ->
                            updatedItems.add(
                                if (indexItem.ticker == symbol) {
                                    createIndexDataWithColor(
                                        IndexDataParameter(
                                            symbol = symbol,
                                            symbolDesc = indexItem.displayName,
                                            price = lastPrice,
                                            change = if (data is StockData) {
                                                data.dayChange
                                            } else {
                                                data.change
                                            },
                                            percentChange = if (data is StockData) {
                                                data.dayChangePercent
                                            } else {
                                                data.changePercent
                                            },
                                            indicatorColor = indexItem.indicatorColor,
                                            changeColorPicker = changeColorPicker,
                                            useMarketChangeColor = useMarketChangeColor
                                        )
                                    )
                                } else {
                                    indexItem
                                }
                            )
                        }
                        onIndicesUpdated(updatedItems)
                    }

                    marketTradeStatusDelegate.onMarketStatusUpdate(data)
                }
            )
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onLifeCyclePause() {
        /** We want to unsubscribe from streaming */
        streamingCompositeDisposable.clear()
        marketTradeStatusDelegate.resetMarketStatus()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onLifeCycleResume() {
        /** We need to subscribe streaming here because gets unsubscribe onPause lifecycle */
        if (streamingStatusController.isStreamingToggleEnabled) {
            subscribeToIndicesUpdates()
        } else {
            streamingCompositeDisposable.clear()
        }
    }
}
