package com.etrade.mobilepro.quote.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class QuoteRequestDto(
    @Json(name = "value")
    val value: Any
)
