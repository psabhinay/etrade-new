package com.etrade.mobilepro.quotes.util

import androidx.navigation.NavController
import com.etrade.mobilepro.R
import com.etrade.mobilepro.trade.router.QuoteRouter
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import dagger.Reusable
import javax.inject.Inject

class QuoteRouterStub @Inject constructor(
    private val pendingQuoteMessageHelper: PendingQuoteMessageHelper
) : QuoteRouter {
    override fun popQuotePage(navController: NavController, inclusive: Boolean) {
        navController.popBackStack(R.id.quotePage, inclusive)
    }

    override fun popQuotePageWithMessage(navController: NavController, inclusive: Boolean, message: String) {
        pendingQuoteMessageHelper.message = message.consumable()
        popQuotePage(navController, inclusive)
    }
}

@Reusable
class PendingQuoteMessageHelper @Inject constructor() {
    var message: ConsumableLiveEvent<String>? = null
}
