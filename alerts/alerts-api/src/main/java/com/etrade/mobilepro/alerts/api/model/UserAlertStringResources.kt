package com.etrade.mobilepro.alerts.api.model

class UserAlertStringResources(
    val targetPrice: String,
    val dailyPercent: String,
    val peTarget: String,
    val dailyVolume: String,
    val news: String
)
