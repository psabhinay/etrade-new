package com.etrade.mobilepro.menu.dto

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MenuDto(
    override val ctaList: List<CallToActionDto>? = null,

    @Json(name = "data")
    override val data: DataDetail,

    @Json(name = "type")
    override val type: String

) : BaseScreenView()

@JsonClass(generateAdapter = true)
data class MenuItem(
    @Json(name = "cta_action")
    val ctaAction: ClickActionDto? = null,

    val icon: String? = null,

    val label: String = "",

    @Json(name = "display_type")
    val menuType: String = "",

    @Json(name = "sublabel")
    val subLabel: String? = null
)

@JsonClass(generateAdapter = true)
class DataDetail(
    @Json(name = "menu_items")
    val dynamicMenuItems: List<MenuItem>
)
