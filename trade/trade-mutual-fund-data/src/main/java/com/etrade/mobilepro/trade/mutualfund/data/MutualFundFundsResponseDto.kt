package com.etrade.mobilepro.trade.mutualfund.data

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MutualFundFundsResponseDto(
    @Json(name = "FundsOwned")
    val fundsOwned: FundsOwned?,
    @Json(name = "FundFamilyList")
    val fundFamilyList: FundsOwned?
) : BaseDataDto() {
    @JsonClass(generateAdapter = true)
    class FundsOwned(
        @Json(name = "funds")
        val funds: List<Fund>?
    )

    @JsonClass(generateAdapter = true)
    class Fund(
        @Json(name = "securityName")
        val securityName: String,
        @Json(name = "symbol")
        val symbol: String,
        @Json(name = "typeCode")
        val typeCode: String
    )
}
