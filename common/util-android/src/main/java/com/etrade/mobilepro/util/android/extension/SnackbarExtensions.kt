package com.etrade.mobilepro.util.android.extension

import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

fun Snackbar.addOnShown(block: () -> Unit): Snackbar {
    addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar?>() {
        override fun onShown(transientBottomBar: Snackbar?) {
            block()
            super.onShown(transientBottomBar)
        }
    })
    return this
}
