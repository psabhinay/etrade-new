package com.etrade.mobilepro.dynamicui

import com.etrade.completeview.dto.AccountSummaryViewDto
import com.etrade.completeview.dto.DayGainViewDto
import com.etrade.completeview.dto.NetAssetsViewDto
import com.etrade.mobilepro.accountslist.data.dto.AccountsViewDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.bloombergtv.widget.dto.BloombergWidgetViewDto
import com.etrade.mobilepro.chart.linechart.dto.MarketChartViewDto
import com.etrade.mobilepro.home.overview.dto.OverviewFooterViewDto
import com.etrade.mobilepro.indices.dto.MarketIndicesViewDto
import com.etrade.mobilepro.login.data.dto.LoginViewDto
import com.etrade.mobilepro.markets.broader.dto.BroaderMarketIndicesViewDto
import com.etrade.mobilepro.markets.earnings.TodayEarningsViewDto
import com.etrade.mobilepro.markets.futures.widget.MarketsFuturesViewDto
import com.etrade.mobilepro.menu.dto.MenuDto
import com.etrade.mobilepro.movers.markets.dto.MarketMoverViewDto
import com.etrade.mobilepro.movers.portfolio.dto.PortfolioMoverViewDto
import com.etrade.mobilepro.news.api.dto.NewsWidgetBriefingDto
import com.etrade.mobilepro.news.api.dto.NewsWidgetPortfolioDto
import com.etrade.mobilepro.news.api.dto.NewsWidgetTodayDto
import com.etrade.mobilepro.personalnotifications.data.dto.PersonalNotificationsListViewDto
import com.etrade.mobilepro.positions.data.dto.PortfolioTaxLotsViewDto
import com.etrade.mobilepro.positions.data.dto.PositionListViewDto
import com.etrade.msaccounts.dto.AggregateValuesViewDto
import com.etrade.msaccounts.dto.MsAccountsEnrollmentViewDto
import com.etrade.msaccounts.dto.MsAccountsSummaryViewDto

enum class DynamicViewType(
    override val dtoName: String,
    override val dtoClass: Class<out BaseScreenView>
) : DynamicEnum<BaseScreenView> {

    ACCOUNT_LIST("account_list", AccountsViewDto::class.java),
    ACCOUNT_PORTFOLIO_LIST("account_portfolio_list", PositionListViewDto::class.java),
    ACCOUNT_SUMMARY("account_summary", AccountSummaryViewDto::class.java),
    BLOOMBERG_TV("bloomberg_tv", BloombergWidgetViewDto::class.java),
    BROADER_INDICES("broader_indices", BroaderMarketIndicesViewDto::class.java),
    DIVIDER("divider", DynamicDividerViewDto::class.java),
    DYNAMIC_MENU("menu_section", MenuDto::class.java),
    EARNINGS_TODAY("earnings_today", TodayEarningsViewDto::class.java),
    FOOTER("footer_timestamp_brokercheck", OverviewFooterViewDto::class.java),
    LOGIN("login", LoginViewDto::class.java),
    MARKET_INDICES("market_indices", MarketIndicesViewDto::class.java),
    MARKET_CHART("market_chart", MarketChartViewDto::class.java),
    MARKET_FUTURES("market_futures", MarketsFuturesViewDto::class.java),
    MARKET_MOVERS("market_movers", MarketMoverViewDto::class.java),
    MS_ACCOUNTS_AGGREGATE_VALUES("ms_accounts_aggregate_values", AggregateValuesViewDto::class.java),
    MS_ACCOUNTS_ENROLLMENT("ms_accounts_enrollment", MsAccountsEnrollmentViewDto::class.java),
    MS_ACCOUNTS_SUMMARY("ms_accounts_summary", MsAccountsSummaryViewDto::class.java),
    NET_ASSETS_SUMMARY("net_assets_summary", NetAssetsViewDto::class.java),
    NET_GAIN_SUMMARY("net_gain_summary", DayGainViewDto::class.java),
    NEWS_BRIEFING("news_briefing", NewsWidgetBriefingDto::class.java),
    NEWS_PORTFOLIO("news_portfolio", NewsWidgetPortfolioDto::class.java),
    NEWS_TODAY("news_today", NewsWidgetTodayDto::class.java),
    PERSONAL_NOTIFICATIONS_LIST("personal_notifications_list", PersonalNotificationsListViewDto::class.java),
    PORTFOLIO_MOVERS("portfolio_movers", PortfolioMoverViewDto::class.java),
    POSITIONS_LIST("positions_list", PositionListViewDto::class.java),
    POSITIONS_LOTS_LIST("positions_lots_list", PortfolioTaxLotsViewDto::class.java)
}
