package com.etrade.mobilepro.dynamic.form.api

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class DropDownOptionTest {

    /**
     * We need to be able to compare lists of drop down options. This feature is required for some forms.
     */
    @Test
    fun `verify equals() does deep comparison`() {
        val dd1 = DropDownOption("id", "name", "desc")
        val dd2 = DropDownOption("id", "name", "desc")
        val dd3 = DropDownOption("id1", "name1")

        assertEquals(dd1, dd2)
        assertNotEquals(dd1, dd3)
        assertNotEquals(dd2, dd3)
    }

    /**
     * DropDownOption.toString() method must return DropDownOption.name because it is being used by DropDownManager to render the list of items.
     */
    @Test
    fun `verify toString() returns name property`() {
        assertEquals("name", DropDownOption("id", "name").toString())
    }
}
