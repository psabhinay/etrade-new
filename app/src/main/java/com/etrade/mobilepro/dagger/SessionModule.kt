package com.etrade.mobilepro.dagger

import android.app.AlarmManager
import android.app.Application
import android.content.Context
import com.etrade.eo.rest.RetrofitBuilderFacade
import com.etrade.eo.webview.cookiejar.api.CookieStoreCleaner
import com.etrade.eo.webviewcookiejar.SharedCookieManager
import com.etrade.mobilepro.application.ApplicationLifecycle
import com.etrade.mobilepro.application.ApplicationLifecycleObserver
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.common.LoginChecker
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dagger.qualifiers.GuardedApi
import com.etrade.mobilepro.login.api.repo.LoginRepo
import com.etrade.mobilepro.login.api.repo.LogoutRepo
import com.etrade.mobilepro.login.data.DefaultLoginRepo
import com.etrade.mobilepro.login.data.DefaultLogoutRepo
import com.etrade.mobilepro.login.session.DefaultLogoutCase
import com.etrade.mobilepro.login.session.LogoutUseCase
import com.etrade.mobilepro.login.session.UserSessionHandlerImpl
import com.etrade.mobilepro.sar.SarManager
import com.etrade.mobilepro.sar.SimultaneousAccessRestriction
import com.etrade.mobilepro.sar.service.repo.DefaultSarRepo
import com.etrade.mobilepro.sar.service.repo.SarRepo
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.session.api.UserConsumerKeyInterface
import com.etrade.mobilepro.session.ping.repo.DefaultPingSessionRepo
import com.etrade.mobilepro.session.ping.repo.PingSessionRepo
import com.etrade.mobilepro.timeout.setting.service.repo.DefaultTimeoutSettingRepo
import com.etrade.mobilepro.timeout.setting.service.repo.TimeoutSettingRepo
import com.etrade.mobilepro.timeout.state.DefaultTimeoutState
import com.etrade.mobilepro.timeout.state.TimeoutState
import com.etrade.mobilepro.timeout.widget.BiometricTimeoutMainActivityDelegate
import com.etrade.mobilepro.timeout.widget.DefaultBiometricTimeoutMainActivityDelegate
import com.etrade.mobilepro.user.session.manager.SessionState
import com.etrade.mobilepro.user.session.manager.SessionTimeoutManager
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.user.session.manager.UserSessionTimeoutManager
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */
@Module(
    includes = [
        ApplicationModule::class,
        StreamingModule::class
    ]
)
abstract class SessionModule {

    @Binds
    @Singleton
    abstract fun bindApplicationLifecycle(applicationLifecycleObserver: ApplicationLifecycleObserver): ApplicationLifecycle

    @Binds
    @Singleton
    abstract fun bindActivityLifecycleCallbacks(applicationLifecycleObserver: ApplicationLifecycleObserver): Application.ActivityLifecycleCallbacks

    @Binds
    @Singleton
    abstract fun bindUserSessionHandler(handler: UserSessionHandlerImpl): UserSessionHandler

    @Binds
    @Singleton
    abstract fun bindUserSessionTimeoutManager(userSessionTimeoutManager: UserSessionTimeoutManager): SessionTimeoutManager

    @Binds
    @Singleton
    abstract fun bindTimeoutState(timeoutState: DefaultTimeoutState): TimeoutState

    @Binds
    @Singleton
    abstract fun bindBiometricTimeoutMainActivityDelegate(biometricTimeoutLoginActivityDelegate: DefaultBiometricTimeoutMainActivityDelegate):
        BiometricTimeoutMainActivityDelegate

    @Binds
    @Singleton
    abstract fun bindSimultaneousAccessRestriction(sarManager: SarManager): SimultaneousAccessRestriction

    @Suppress("TooManyFunctions")
    companion object {

        @Provides
        internal fun providesAuthState(user: User): UserAuthenticationState = { user.isLoggedIn() }

        @Provides
        @Singleton
        internal fun providesCookieStoreCleaner(sharedCookieManager: SharedCookieManager): CookieStoreCleaner = sharedCookieManager

        @Provides
        @Singleton
        @GuardedApi
        internal fun providesGuardedMobileTradeRetrofit(@GuardedApi retrofitBuilderFacade: RetrofitBuilderFacade, @MobileTrade baseUrl: String): Retrofit {
            return retrofitBuilderFacade
                .baseUrl(baseUrl)
                .build()
        }

        @Provides
        @Singleton
        internal fun providesLoginChecker(user: User): LoginChecker = LoginChecker(user)

        @Provides
        @Singleton
        internal fun providesLoginRepo(applicationInfo: ApplicationInfo, @GuardedApi retrofit: Retrofit, user: User): LoginRepo =
            DefaultLoginRepo(user, applicationInfo, retrofit)

        @Provides
        @Singleton
        internal fun providesPingSessionRepo(@MobileTrade retrofit: Retrofit, user: User, applicationInfo: ApplicationInfo): PingSessionRepo =
            DefaultPingSessionRepo(retrofit, user) {
                applicationInfo.deviceId()
            }

        @Provides
        @Singleton
        internal fun providesLogoutRepo(@MobileTrade retrofit: Retrofit): LogoutRepo = DefaultLogoutRepo(retrofit)

        @Provides
        @Singleton
        internal fun providesLogoutUseCase(logoutRepo: LogoutRepo, cookieStoreCleaner: CookieStoreCleaner): LogoutUseCase =
            DefaultLogoutCase(logoutRepo, cookieStoreCleaner)

        @Provides
        @Singleton
        internal fun providesSarRepo(@Web retrofit: Retrofit): SarRepo = DefaultSarRepo(retrofit)

        @Provides
        internal fun providesUserConsumerKey(user: User): UserConsumerKeyInterface = user

        @Provides
        internal fun providesSessionState(
            userSessionHandler: UserSessionHandler
        ): SessionState = userSessionHandler as SessionState

        @Provides
        internal fun providesTimeoutSettingRepo(
            @MobileTrade retrofit: Retrofit
        ): TimeoutSettingRepo = DefaultTimeoutSettingRepo(retrofit)

        @Provides
        @Singleton
        internal fun providesAlarmManager(context: Context): AlarmManager {
            return context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        }
    }
}
