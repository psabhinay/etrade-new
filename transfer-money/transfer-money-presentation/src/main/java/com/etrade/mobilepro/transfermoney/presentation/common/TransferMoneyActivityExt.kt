package com.etrade.mobilepro.transfermoney.presentation.common

import android.content.Context
import com.etrade.mobilepro.transfermoney.api.TransferMoneyActivity
import com.etrade.mobilepro.transfermoney.presentation.R

private const val NONE = "None"
private const val DEFAULT_VALUE = "--"
private const val SCHEDULED = "Scheduled"

internal fun TransferMoneyActivity.combineAmountAndOccurrence(context: Context): String = if (this.isRecurring()) {
    context.getString(R.string.transfer_amount_format_with_occurrence, this.amount, this.frequency)
} else {
    this.amount
}

internal fun TransferMoneyActivity.isScheduled(): Boolean = this.status == SCHEDULED

internal fun TransferMoneyActivity.isRecurring(): Boolean = this.frequency != NONE && this.frequency != DEFAULT_VALUE

internal fun TransferMoneyActivity.parcelize(): ParcelableTransferMoneyActivity = ParcelableTransferMoneyActivity(
    amount = amount,
    date = date,
    frequency = frequency,
    fromAccount = fromAccount,
    confirmationNumber = confirmationNumber,
    referenceNumber = referenceNumber,
    status = status,
    toAccount = toAccount,
    transferType = transferType,
    id = id,
    appName = appName,
    memo = memo
)
