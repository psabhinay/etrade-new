package com.etrade.mobilepro.watchlist.usecase

import com.etrade.eo.core.util.DateFormattingUtils.formatFullDateTime
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.util.domain.data.resolveTimeStamp
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import java.util.concurrent.TimeUnit
import javax.inject.Inject

interface ResolveTimestampUseCase : UseCase<List<WatchlistEntry>, ETResult<String?>>

class ResolveTimestampUseCaseImpl @Inject constructor() : ResolveTimestampUseCase {
    override suspend fun execute(parameter: List<WatchlistEntry>): ETResult<String?> {
        return runCatchingET {
            parameter
                .filter { it.typeCode?.isMutualFundOrMoneyMarketFund?.not() ?: false }
                .resolveTimeStamp(
                    transform = { lastTradeTimeStamp },
                    timeStampFormatter = { formatFullDateTime(TimeUnit.SECONDS.toMillis(it)) }
                )
        }
    }
}
