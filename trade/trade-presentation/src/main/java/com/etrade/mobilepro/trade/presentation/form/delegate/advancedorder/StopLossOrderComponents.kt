package com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder

import android.content.res.Resources
import androidx.lifecycle.map
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.InputHolder
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.displaySymbol
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.shared.StopLossInputId
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.editPriceType
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.price
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.priceType
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.stopLimitPrices
import com.etrade.mobilepro.trade.presentation.usecase.TradeFormParameter
import com.etrade.mobilepro.trade.presentation.util.formatLegDetails

internal fun GroupBuilder.stopLossOrderForm(
    advancedFormInputId: WithAdvancedTradeFormInputId,
    params: StopLossFormParams,
    displayStopLossLegText: GroupBuilder.() -> Unit
) {
    verticalGroup {
        displayStopLossLegText()
        stopLossPrices(params)
        term(params.resources, params.stopLossInputId)

        visibleIf {
            inputValue<DropDownOption?>(advancedFormInputId.advancedOrderType).map {
                it?.id == advancedFormInputId.advancedOrderTypeValueId.stopLoss
            }
        }
    }
}

internal fun InputHolder.stopLossFormHiddenInputAndLegDisplayText(
    inputId: StopLossInputId,
    quantity: String?,
    resources: Resources,
    symbol: WithSymbolInfo?,
    transactionParams: StopLossFormInput<TransactionType>
): CharSequence {

    setHiddenInputValues(inputId, transactionParams.id, StopLossFormInput(inputId.quantity, quantity), symbol)

    return resources.formatLegDetails(transactionParams.value, symbol?.displaySymbol ?: "", quantity)
}

internal fun RuleListBuilder.hiddenAllOrNoneRule(primaryInputId: WithPriceTradeFormInputId, stopLossInputId: StopLossInputId) {
    rule {
        inputValue<Boolean?>(primaryInputId.allOrNone).observeForever {
            val encoded = stopLossInputId.allOrNoneValueCoder.encode(it ?: false)
            requireAnyInput(stopLossInputId.allOrNone).setParameterValue(encoded)
        }
    }
}

internal fun <T> InputHolder.setHiddenInputValues(
    inputId: T,
    transactionId: String,
    quantityParams: StopLossFormInput<String?>,
    symbol: WithSymbolInfo?
) where T : WithActionTradeFormInputId, T : WithSymbolTradeFormInputId {

    requireAnyInput(inputId.action).setParameterValue(transactionId)
    quantityParams.value?.let { requireAnyInput(quantityParams.id).setParameterValue(it) }
    symbol?.let {
        val encodedSymbol = inputId.symbolCoder.encode(it)
        requireAnyInput(inputId.symbol).setParameterValue(encodedSymbol)
    }
}

private fun GroupBuilder.stopLossPrices(params: StopLossFormParams) {

    val inputId = params.stopLossInputId
    val valueId = inputId.priceTypeValueId
    val dependencies = params.dependencies

    horizontalGroup {
        priceType(
            params.resources,
            inputId,
            params.parameter.walkthroughViewModel.state,
            params.parameter.editPriceType(inputId),
            securityType = params.securityType,
        )
        price(inputId.stopPrice, inputId, dependencies.priceTypePriceLabelMap, null, dependencies.isStopPriceInputVisible)
    }
    stopLimitPrices(inputId, params.resources, valueId)
}

private fun GroupBuilder.term(resources: Resources, inputId: StopLossInputId) {

    select(inputId.term) {
        title = resources.getString(R.string.trade_hint_term)
        options {
            option(inputId.termValueId.goodForDay) {
                title = resources.getString(R.string.trade_term_day)
            }
            option(inputId.termValueId.goodUntilCancel) {
                title = resources.getString(R.string.trade_term_gtc)
            }
        }
    }
}

internal data class StopLossFormParams(
    val resources: Resources,
    val primaryInputId: TradeFormInputId,
    val dependencies: FormFactoryDelegateDependencies,
    val parameter: TradeFormParameter,
    val stopLossInputId: StopLossInputId,
    val securityType: SecurityType
)

internal data class StopLossFormInput<T>(
    val id: String,
    val value: T
)
