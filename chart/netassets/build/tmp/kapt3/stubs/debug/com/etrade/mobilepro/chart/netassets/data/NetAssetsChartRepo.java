package com.etrade.mobilepro.chart.netassets.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J%\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u00a6@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\b\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\t"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/data/NetAssetsChartRepo;", "", "getNetAssetsChart", "Lcom/etrade/mobilepro/util/Resource;", "Lcom/etrade/mobilepro/chart/netassets/model/NetAssetsChartData;", "accounts", "", "", "(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "netassets_debug"})
public abstract interface NetAssetsChartRepo {
    
    @org.jetbrains.annotations.Nullable()
    public abstract java.lang.Object getNetAssetsChart(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.etrade.mobilepro.util.Resource<com.etrade.mobilepro.chart.netassets.model.NetAssetsChartData>> continuation);
}