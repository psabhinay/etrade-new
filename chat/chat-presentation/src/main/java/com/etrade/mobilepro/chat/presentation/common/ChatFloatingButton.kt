package com.etrade.mobilepro.chat.presentation.common

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PixelFormat
import android.view.GestureDetector
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import com.etrade.mobilepro.chat.presentation.R
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.FLOATING_BUTTON_MAX_MESSAGES_NUMBER
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.FLOATING_BUTTON_WINDOW_MARGIN_X
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.FLOATING_BUTTON_WINDOW_MARGIN_Y

class ChatFloatingButton(
    val context: Context,
    val listener: OnFloatingButtonTouchListener
) {

    private var lastAction = 0
    private var initialX = 0
    private var initialY = 0
    private var initialTouchX = 0f
    private var initialTouchY = 0f

    var unreadMessages = 0
        private set
    var isDisplayed = false
    private lateinit var params: WindowManager.LayoutParams

    private val windowManager
        get() = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager

    private lateinit var chatFloatingButtonView: View
    private lateinit var unreadMessageView: TextView

    @SuppressLint("InflateParams")
    fun setupFloatingButton() {
        resetUnreadMessages()
        // Inflate the chat head layout we created
        chatFloatingButtonView = LayoutInflater.from(context).inflate(R.layout.chat_floating_button, null)
        unreadMessageView = chatFloatingButtonView.findViewById(R.id.chat_unread_count_text_view)

        val layoutFlag: Int = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY

        // Add the view to the window.
        params = WindowManager.LayoutParams(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT,
            layoutFlag,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )

        // Specify the chat head position
        params.x = FLOATING_BUTTON_WINDOW_MARGIN_X
        params.y = FLOATING_BUTTON_WINDOW_MARGIN_Y
        params.gravity = Gravity.BOTTOM or Gravity.END

        // Drag and move chat head using user's touch action.
        val gestureDetector = GestureDetector(context, SingleTapConfirm())
        chatFloatingButtonView.setOnTouchListener(object : View.OnTouchListener {

            @SuppressLint("ClickableViewAccessibility")
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                if (gestureDetector.onTouchEvent(event)) { // Open the chat conversation click.
                    listener.onTouchFloatingButton()
                    return true
                } else {
                    if (handleEvent(event)) {
                        return true
                    }
                }
                return false
            }
        })
    }

    private fun handleEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                // remember the initial position.
                initialX = params.x
                initialY = params.y
                // get the touch location
                initialTouchX = event.rawX
                initialTouchY = event.rawY
                lastAction = event.action
                return true
            }
            MotionEvent.ACTION_UP -> {
                lastAction = event.action
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                // Calculate the X and Y coordinates of the view.
                params.x = initialX - (event.rawX - initialTouchX).toInt()
                params.y = initialY - (event.rawY - initialTouchY).toInt()
                // Update the layout with new X & Y coordinate
                windowManager.updateViewLayout(chatFloatingButtonView, params)
                lastAction = event.action
                return true
            }
        }
        return false
    }

    private fun resetUnreadMessages() {
        unreadMessages = 0
        updateUnreadMessages(unreadMessages)
    }

    fun updateUnreadMessages(count: Int) {
        if (!isDisplayed) {
            return
        }
        unreadMessages += count
        unreadMessageView.text = unreadMessages.toString()
        when {
            unreadMessages == 0 -> {
                unreadMessageView.visibility = View.GONE
            }
            unreadMessages > FLOATING_BUTTON_MAX_MESSAGES_NUMBER -> {
                unreadMessageView.visibility = View.VISIBLE
                unreadMessageView.text = context.getString(R.string.chat_max_unread_message_number)
            }
            else -> {
                unreadMessageView.visibility = View.VISIBLE
                unreadMessageView.text = unreadMessages.toString()
            }
        }
        updateContentDescription()
    }

    fun show() {
        if (!isDisplayed) {
            windowManager.addView(chatFloatingButtonView, params)
            unreadMessageView.visibility = View.GONE
            isDisplayed = true
            updateContentDescription()
        }
    }

    fun remove() {
        resetUnreadMessages()
        if (isDisplayed) {
            windowManager.removeView(chatFloatingButtonView)
            isDisplayed = false
        }
    }

    private fun updateContentDescription() {
        chatFloatingButtonView.contentDescription =
            context.resources.getQuantityString(R.plurals.chat_floating_button_description, unreadMessages, unreadMessages)
    }

    private class SingleTapConfirm : GestureDetector.SimpleOnGestureListener() {
        override fun onSingleTapUp(event: MotionEvent?) = true
    }
}

interface OnFloatingButtonTouchListener {

    fun onTouchFloatingButton()
}
