package com.etrade.mobilepro.util

/**
 * If the given string starts with a "/", return it with the given defaultBaseUrl prepended.
 * Otherwise, return the given string.
 */
fun String?.prependBaseUrl(defaultBaseUrl: String): String {
    this ?: return defaultBaseUrl
    return """^/(.*)""".toRegex().replace(this) { matchResult ->
        val (path) = matchResult.destructured
        "$defaultBaseUrl/$path"
    }
}
