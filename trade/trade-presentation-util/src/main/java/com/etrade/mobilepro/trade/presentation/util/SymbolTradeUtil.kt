package com.etrade.mobilepro.trade.presentation.util

import android.content.res.Resources
import androidx.annotation.StringRes
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.AppDialogBuilder
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo

fun WithSymbolInfo.isEligibleForOptionTrade(isStocksAndOptions: Boolean): Boolean {
    return when (isStocksAndOptions) {
        true -> InstrumentType.validStocksAndOptionTradingTypes.contains(instrumentType)
        else -> InstrumentType.validOptionsTradingTypes.contains(instrumentType)
    }
}

fun Resources.createNotEligibleSymbolDialog(@StringRes message: Int, init: AppDialogBuilder.() -> Unit = { /* intentionally blank */ }): AppDialog {
    return appDialog {
        titleRes = R.string.trade_error_invalid_symbol_dialog_title
        messageRes = message
        resourcesPositiveAction {
            labelRes = R.string.ok
        }
        init()
    }
}
