package com.etrade.mobilepro.appwidget.widgetcontainer

internal enum class AdapterType {
    Indices,
    Watchlist
}
