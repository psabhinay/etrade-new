package com.etrade.mobilepro.portfolio.data

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.tableviewutils.api.ColumnRepo
import com.etrade.mobilepro.tableviewutils.presentation.data.BaseColumnRepo
import javax.inject.Inject

/**
 * This repository handles everything related to columns of a portfolio entries in tile view.
 */
interface TileViewColumnRepo : ColumnRepo<PortfolioColumn>

private const val COLUMN_LAST_PRICE = "LastPrice"
private const val COLUMN_DAYS_GAIN_DOLLAR = "DaysGainDollar"
private const val COLUMN_DAYS_GAIN_PERCENT = "ColumnDaysGainPercent"

class TileViewColumnRepoImpl @Inject constructor(storage: KeyValueStorage) :
    BaseColumnRepo<PortfolioColumn>(storage), TileViewColumnRepo {

    override val storageKey: String = "portfolioListViewTableColumns"

    override val allColumns: Map<String, PortfolioColumn> = mapOf(
        COLUMN_LAST_PRICE to PortfolioColumn.LAST_PRICE,
        COLUMN_DAYS_GAIN_DOLLAR to PortfolioColumn.DAYS_GAIN_DOLLAR,
        COLUMN_DAYS_GAIN_PERCENT to PortfolioColumn.DAYS_GAIN_PERCENT
    )

    override val defaultColumnsOrder: List<String> = listOf(
        COLUMN_LAST_PRICE,
        COLUMN_DAYS_GAIN_DOLLAR,
        COLUMN_DAYS_GAIN_PERCENT
    )
}
