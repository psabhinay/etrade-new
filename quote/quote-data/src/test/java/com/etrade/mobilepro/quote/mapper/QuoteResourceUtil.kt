package com.etrade.mobilepro.quote.mapper

import com.etrade.mobilepro.quoteapi.QuoteTemplateResources

private const val GREEN = 0xFF00FF00.toInt()
private const val RED = 0xFFFF0000.toInt()

internal fun getQuoteResources(): QuoteTemplateResources = QuoteTemplateResources(
    valueMissing = "--",
    titleFormat = "%1\$s - %2\$s",
    asOfDateTimeFormat = "As of %1\$s",
    nasdaqAsOfDateTimeFormat = "NASDAQ as of %1\$s",
    positiveChangeColorRes = GREEN,
    negativeChangeColorRes = RED,
    neutralChangeColorRes = 0,
    dateFormat = "%1s/%2s/%3s",
    peFormat = "%.1fx",
    changeFormat = "%s (/%2s$%%)",
    closingPrice = "Closing Price",
    underlyerPrice = "Underlyer Price",
    underlyerClosingPrice = "Underlyer Closing Price",
    fundClassTitle = "Class",
    fundDescriptionFormat = "Category: %1\$s\\n\\n%2\$s",
    fundDescriptionNotFound = "Fund description not found",
    fundPerformanceCategoryFormat = "Morningstar %1\$s",
    priceFormat = "price is %1s",
    sizeFormat = "size is %1s",
    contentDescriptionFormat = "Symbol %1s, %2s, Change %3s, Volume %4s, %5s"
)
