package com.etrade.mobilepro.pushnotification.data.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.pushnotification.api.AlertPushSettings
import com.etrade.mobilepro.pushnotification.api.repo.AlertPushSettingsRepo
import com.etrade.mobilepro.pushnotification.data.rest.PushSettingsApiClient
import com.etrade.mobilepro.util.KeyValueStore
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import javax.inject.Inject

private const val STOCK_ALERTS_ON_MATCH = "STK_ALRTS=ON"
private const val STOCK_ALERTS_OFF_MATCH = "STK_ALRTS=OFF"
private const val ACCOUNT_ALERTS_ON_MATCH = "ACCT_ALRTS=ON"
private const val ACCOUNT_ALERTS_OFF_MATCH = "ACCT_ALRTS=OFF"
private const val SUCCESS_FLAG = "Success"
private const val SEPARATOR_FLAG = "|"
private const val DEVICE_ID_START = "DEVID="
private const val FCM_TOKEN_START = "TOKEN="
private const val KEY_TOKEN = "DefaultAlertPushSettingsRepo.key.token"
// thread safe
class DefaultAlertPushSettingsRepo @Inject constructor(
    private val deviceIdProvider: () -> String,
    private val alertNotificationDataProvider: () -> String?,
    private val apiClient: PushSettingsApiClient,
    private val store: KeyValueStore
) : AlertPushSettingsRepo {
    private val localCopy = mutableMapOf<String, Boolean>()
    private var wasSuccessfullyUpdatedOnce = false
    private val mutex = Mutex()
    override fun clearCache() = runBlocking {
        mutex.withLock {
            localCopy.clear()
        }
    }

    override fun getCurrentSettings(): AlertPushSettings = runBlocking {
        mutex.withLock {
            if (localAndRemoteIdsDiffer() && !wasSuccessfullyUpdatedOnce) {
                AlertPushSettings(
                    stockAlertsEnabled = false,
                    accountAlertsEnabled = false
                )
            } else {
                AlertPushSettings(
                    stockAlertsEnabled = localCopy.getOrPut(STOCK_ALERTS_ON_MATCH) {
                        alertNotificationDataProvider.invoke()?.contains(STOCK_ALERTS_ON_MATCH, ignoreCase = true) == true
                    },
                    accountAlertsEnabled = localCopy.getOrPut(ACCOUNT_ALERTS_ON_MATCH) {
                        alertNotificationDataProvider.invoke()?.contains(ACCOUNT_ALERTS_ON_MATCH, ignoreCase = true) == true
                    }
                )
            }
        }
    }

    private fun localAndRemoteIdsDiffer(): Boolean = when (val id = PushSettingsId.fromAlertNotification(alertNotificationDataProvider.invoke())) {
        is PushSettingsId.DeviceId -> deviceIdProvider.invoke() != id.value
        is PushSettingsId.FcmToken -> store.getString(KEY_TOKEN) != id.value
        PushSettingsId.None -> true
    }

    override suspend fun setAlertPushSettings(settings: AlertPushSettings): ETResult<AlertPushSettings> = runCatchingET {
        val response = apiClient.setAlertPushSettings(
            deviceId = deviceIdProvider.invoke(),
            isAccountAlertEnabled = booleanToStringParam(settings.accountAlertsEnabled),
            isStockAlertEnabled = booleanToStringParam(settings.stockAlertsEnabled),
            firebaseCloudMessagingToken = getRegisteredToken().getOrNull()
        )
        require(response.result == SUCCESS_FLAG) { "Could not set alert push settings." }
        mutex.withLock {
            wasSuccessfullyUpdatedOnce = true
            localCopy[STOCK_ALERTS_ON_MATCH] = settings.stockAlertsEnabled
            localCopy[ACCOUNT_ALERTS_ON_MATCH] = settings.accountAlertsEnabled
        }
        settings
    }

    override suspend fun getRegisteredToken(): ETResult<String> {
        return runCatchingET {
            store.getString(KEY_TOKEN) ?: throw NoSuchElementException("Smart alerts token not found")
        }
    }

    override suspend fun registerToken(token: String): ETResult<Unit> {
        return runCatchingET {
            apiClient.subscribeToSmartAlerts(deviceIdProvider.invoke(), token)
                .let {
                    require(it.result == SUCCESS_FLAG) { "Could not register smart alerts token" }
                    store.putString(KEY_TOKEN, token)
                }
        }
    }

    override suspend fun unregisterCurrentToken(): ETResult<Unit> {
        return runCatchingET { store.putString(KEY_TOKEN, null) }
    }

    private fun booleanToStringParam(value: Boolean): String = if (value) {
        "Y"
    } else {
        "N"
    }
}

private sealed class PushSettingsId {
    class DeviceId(val value: String) : PushSettingsId()
    class FcmToken(val value: String) : PushSettingsId()
    object None : PushSettingsId()

    companion object {
        fun fromAlertNotification(alertNotificationToken: String?): PushSettingsId {
            val rawInput = alertNotificationToken ?: return None
            val input = rawInput.replace(STOCK_ALERTS_ON_MATCH, "")
                .replace(STOCK_ALERTS_OFF_MATCH, "")
                .replace(ACCOUNT_ALERTS_ON_MATCH, "")
                .replace(ACCOUNT_ALERTS_OFF_MATCH, "")
                .replace(SEPARATOR_FLAG, "")
            return when {
                input.contains(DEVICE_ID_START) && !input.contains(FCM_TOKEN_START) -> DeviceId(input.replace(DEVICE_ID_START, ""))
                input.contains(FCM_TOKEN_START) && !input.contains(DEVICE_ID_START) -> FcmToken(input.replace(FCM_TOKEN_START, ""))
                else -> None
            }
        }
    }
}
