package com.etrade.mobilepro.util.android.coroutine

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel

fun CoroutineScope.safeCancel() {
    if (coroutineContext[Job] != null) {
        cancel()
    }
}
