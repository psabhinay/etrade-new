package com.etrade.mobilepro.trade.presentation.form.delegate

import android.content.Context
import com.etrade.mobilepro.dynamic.form.api.FormBuilder
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.input.ContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.contingentOrderForm
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.contingentOrderHiddenInputs
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.createOptionSymbolExternalAction
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.observeContingentSymbolChanges
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.util.ValueCoder

internal class ContingentOptionTradeFormFactoryDelegate(
    private val context: Context,
    private val searchIntentFactory: SearchIntentFactory,
    private val inputId: ContingentOptionTradeFormInputId,
    securityType: SecurityType,
    tradeLegsCoder: ValueCoder<List<TradeLeg>>,
    formFactoryDelegateDependencies: FormFactoryDelegateDependencies,
    tradeFormTypeUpdateDelegate: TradeFormTypeUpdateDelegate<OptionTradeFormInputId>,
    tradingDefaultsPreferences: TradingDefaultsPreferences
) : OptionTradeFormFactoryDelegate(
    context = context,
    dependencies = formFactoryDelegateDependencies,
    securityType = securityType,
    inputId = inputId,
    tradeLegsCoder = tradeLegsCoder,
    tradeFormTypeUpdateDelegate = tradeFormTypeUpdateDelegate,
    tradingDefaultsPreferences = tradingDefaultsPreferences
) {

    override val formType: TradeFormType = TradeFormType(SecurityType.OPTION, AdvancedTradeType.Contingent)

    override fun advancedOrderFormInputs(groupBuilder: GroupBuilder, parameter: CreateTradeFormParameter) {
        with(groupBuilder) {
            contingentOrderForm(context, inputId, parameter) { symbolInputParams ->
                createOptionSymbolExternalAction(
                    context,
                    symbolInputParams,
                    inputId,
                    searchIntentFactory,
                    parameter.onActionClickListener
                )
            }
        }
    }

    private fun RuleListBuilder.setContingentOptionHiddenInputs(inputId: ContingentOptionTradeFormInputId) {
        rule {
            inputValue<List<TradeLeg>?>(inputId.tradeLegs).observeForever { legs ->
                legs?.firstOrNull()?.let {
                    requireAnyInput(inputId.action).setParameterValue(it.transactionType.typeCode.toString())
                    requireAnyInput(inputId.quantity).setParameterValue(it.quantity.toString())
                    val symbol = object : WithSymbolInfo {
                        override val instrumentType: InstrumentType = InstrumentType.OPTN
                        override val symbol: String = it.symbol
                    }
                    val encodedSymbol = inputId.symbolCoder.encode(symbol)
                    requireAnyInput(inputId.symbol).setParameterValue(encodedSymbol)
                }
            }
        }
    }

    override fun advancedOrderRules(ruleBuilder: RuleListBuilder, updateForm: TradeFormFactoryDelegate.OnFormChangeListener) {
        super.advancedOrderRules(ruleBuilder, updateForm)
        ruleBuilder.apply {
            setContingentOptionHiddenInputs(inputId)
            observeContingentSymbolChanges(inputId)
        }
    }

    override fun hiddenInput(formBuilder: FormBuilder) {
        super.hiddenInput(formBuilder)
        formBuilder.apply {
            contingentOrderHiddenInputs(inputId)
            hidden(inputId.action)
            hidden(inputId.quantity)
            hidden(inputId.symbol)
        }
    }
}
