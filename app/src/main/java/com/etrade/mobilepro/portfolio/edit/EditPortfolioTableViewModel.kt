package com.etrade.mobilepro.portfolio.edit

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.portfolio.PortfolioPositionsDisplayMode
import com.etrade.mobilepro.portfolio.data.PortfolioColumnRepo
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.tracking.SCREEN_TITLE_PORTFOLIO_CUSTOMIZE
import com.etrade.mobilepro.tracking.ScreenViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class EditPortfolioTableViewModel @Inject constructor(
    private val portfolioColumnRepo: PortfolioColumnRepo,
    private val resources: Resources
) : EditListViewModel<PortfolioColumnWrapper>(), ScreenViewModel {

    override val screenName: String? = SCREEN_TITLE_PORTFOLIO_CUSTOMIZE

    private lateinit var positionTableMode: PositionTableMode
    private lateinit var portfolioTableViewType: PortfolioTableViewType

    override fun onFetchItems(viewState: MutableLiveData<ViewState>) {
        viewModelScope.launch {
            provideItems(
                portfolioColumnRepo.getPortfolioColumns(
                    positionTableMode,
                    portfolioTableViewType,
                    PortfolioPositionsDisplayMode.TABLE
                ).map { PortfolioColumnWrapper(it, resources) }
            )
            viewState.value = ViewState.Idle
        }
    }

    override fun onApplyChanges(
        items: List<PortfolioColumnWrapper>,
        removedItems: List<PortfolioColumnWrapper>,
        viewState: MutableLiveData<ViewState>
    ) {
        viewModelScope.launch {
            portfolioColumnRepo.savePortfolioColumns(
                positionTableMode,
                portfolioTableViewType,
                items.map { it.column },
                PortfolioPositionsDisplayMode.TABLE
            )
            viewState.value = ViewState.Success()
        }
    }

    fun prepareViewModel(
        positionTableMode: PositionTableMode,
        tableViewType: PortfolioTableViewType
    ) {
        this.positionTableMode = positionTableMode
        this.portfolioTableViewType = tableViewType
    }
}
