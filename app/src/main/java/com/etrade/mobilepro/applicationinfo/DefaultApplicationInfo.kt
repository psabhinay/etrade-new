package com.etrade.mobilepro.applicationinfo

import android.content.Context
import android.os.Build
import com.etrade.mobilepro.BuildConfig
import com.etrade.mobilepro.R
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import com.etrade.mobilepro.util.formatters.majorMinorVersion
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DefaultApplicationInfo @Inject constructor(
    val context: Context,
    val applicationPreferences: ApplicationPreferences,
    val connectionNotifier: ConnectionNotifier
) : ApplicationInfo {

    override val isTablet: Boolean by lazy { context.resources.getBoolean(R.bool.is_tablet) }

    override val deviceType: String by lazy { if (isTablet) "AND-TAB" else "AND-PHN" }

    private val formattedApplicationInfo: String

    init {
        /**
         * Server requires a specific format of ApplicationInfo to be added in header along with user-agent
         * iOS Example: AppVersion:10.4.0 DevicePlatform:x86_64 OSVersion:13.4 DType:iPhone
         * */
        val sb = StringBuilder()
        sb.append("AppVersion:".plus(appVersionName().plus(" "))) // keep the same value with the one which put in header
        sb.append("DevicePlatform:".plus(platform().plus(" ")))
        sb.append("OSVersion:".plus(operationSystemVersion().plus(" ")))
        sb.append("DType:".plus(deviceType))
        formattedApplicationInfo = sb.toString()
    }

    override fun connectionType() = connectionNotifier.connectionType

    override fun appVersionCode(): String {
        return "${BuildConfig.VERSION_CODE}"
    }

    override fun appVersionName(): String {
        return BuildConfig.VERSION_NAME
    }

    override val appMajorMinorVersion: String
        by lazy { appVersionName().majorMinorVersion }

    override fun operationSystemVersion(): String {
        return Build.VERSION.RELEASE
    }

    override fun deviceDetails(): String {
        return "${Build.MANUFACTURER} ${Build.MODEL}"
    }

    override fun platform(): String {
        return "Android"
    }

    override fun deviceId(): String = applicationPreferences.deviceId

    override fun toString(): String {
        return formattedApplicationInfo
    }
}
