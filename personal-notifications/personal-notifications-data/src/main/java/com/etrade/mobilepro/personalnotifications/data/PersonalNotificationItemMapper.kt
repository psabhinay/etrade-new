package com.etrade.mobilepro.personalnotifications.data

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.MessagePlace
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.Localytics.Banner
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.Localytics.EmergencyBroadcast
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.Localytics.Inline
import com.etrade.mobilepro.personalnotifications.data.dto.ExternalAccountVerificationDto
import com.etrade.mobilepro.personalnotifications.data.dto.PersonalNotificationsListViewDto
import com.etrade.mobilepro.personalnotifications.data.dto.TransactionDto

fun PersonalNotificationsListViewDto.mapToPersonalNotificationList(): List<PersonalNotification> {
    // External Account Verification
    val externalAccountVerification = arrayListOf<PersonalNotification.ExternalAccountVerification>()
    if (this.data?.externalAccountVerification?.isNotEmpty() == true) {
        externalAccountVerification.add(
            PersonalNotification.ExternalAccountVerification(
                generateIdentifierForExternalAccountVerification(this.data.externalAccountVerification)
            )
        )
    }

    // Pending Transactions
    val pendingTransfers = arrayListOf<PersonalNotification.PendingTransaction>()
    this.data?.pendingTransactions.orEmpty().forEach { account ->
        account.transactions?.map { transaction ->
            pendingTransfers.add(
                PersonalNotification.PendingTransaction(
                    generateIdentifierForPendingTransfer(transaction, account.accountUuid),
                    account.accountUuid,
                    transaction.amount?.let { MarketDataFormatter.formatCurrencyValue(it) } ?: "",
                    transaction.fundAvailabilityDate ?: ""
                )
            )
        }
    }.also { pendingTransfers.sortBy { it.fundAvailabilityDate } }

    return externalAccountVerification + pendingTransfers
}

fun List<InboxMessage>.getEbmNotification(): List<PersonalNotification> {
    val ebmMessage = find { it.place == MessagePlace.NOTIFICATIONS_INFORMATION }
    return ebmMessage?.let { listOf(EmergencyBroadcast(it)) }.orEmpty()
}

fun List<InboxMessage>.getInlineAndBannerNotifications(): List<PersonalNotification> {
    val inlineNotifications = asSequence().filter { it.place == MessagePlace.NOTIFICATIONS_INLINE }
        .sortedBy { it.receivedDate }.map { Inline(it) }
    val bannerNotifications = asSequence().filter { it.place == MessagePlace.NOTIFICATIONS_BANNER }
        .sortedByDescending { it.receivedDate }.map { Banner(it) }
    return (inlineNotifications + bannerNotifications).toList()
}

private fun generateIdentifierForExternalAccountVerification(
    externalAccountVerificationDto: List<ExternalAccountVerificationDto>
): String {
    return externalAccountVerificationDto.fold("") { identifier, dto ->
        identifier + dto.accountId
    }
}

private fun generateIdentifierForPendingTransfer(
    transactionDto: TransactionDto,
    accountUuid: String
) =
    "$accountUuid${transactionDto.activityId}${transactionDto.fundAvailabilityDate}".trim()
