package com.etrade.mobilepro.billpay.usecase

import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.SessionId
import com.etrade.mobilepro.common.result.ETResult
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class PayeeListUseCaseImplTest {

    private val sessionId = SessionId("123456")

    private val orderedPayeeList = listOf(
        Payee("1", "best2", "", "1235", "0", -1, false),
        Payee("2", "Test1", "", "1234", "0", -1, false),
        Payee("3", "test2", "", "1235", "0", -1, false),
        Payee("4", "Zest1", "", "1234", "0", -1, false)
    )

    private val mockRepo: BillPayRepo = mock {
        onBlocking { getPayeesList(sessionId.value) } doReturn ETResult.success(
            listOf(
                Payee("4", "Zest1", "", "1234", "0", -1, false),
                Payee("3", "test2", "", "1235", "0", -1, false),
                Payee("1", "best2", "", "1235", "0", -1, false),
                Payee("2", "Test1", "", "1234", "0", -1, false)
            )
        )
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `Payee list should be in ascending order`() {
        runBlockingTest {
            PayeeListUseCaseImpl(mockRepo).execute(sessionId).getOrThrow().forEachIndexed { index, payee ->
                assertTrue(payee == orderedPayeeList[index])
            }
        }
    }
}
