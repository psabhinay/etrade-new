package com.etrade.mobilepro.alerts.presentation.market

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.alerts.api.model.AlertGroup
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.alerts.presentation.base.AlertsDeleter
import com.etrade.mobilepro.alerts.presentation.base.AlertsHolder
import com.etrade.mobilepro.alerts.presentation.base.BaseAlertsViewModel
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.android.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MarketAlertsViewModel @Inject constructor(
    alertsRepo: AlertsRepo,
    user: User,
    alertsHolder: AlertsHolder,
    alertsDeleter: AlertsDeleter
) : BaseAlertsViewModel(alertsRepo, user, alertsHolder, alertsDeleter) {
    private val _viewState = MutableLiveData<ViewState>()
    private val _viewEffects = SingleLiveEvent<ViewEffects>()
    val viewEffects: LiveData<ViewEffects> = _viewEffects
    val viewState: LiveData<ViewState> = _viewState
    override val alertGroup: AlertGroup = AlertGroup.MARKET
    override val headerItems: List<AlertItem> = listOf(AlertItem.Divider(true))
    private val compositeDisposable = CompositeDisposable()

    override fun onLoadingStart() {
        _viewState.postValue(ViewState.Loading)
    }

    override fun onAlertsChanged(alerts: List<AlertItem>) {
        _viewState.postValue(ViewState.Success(alerts, hasOnlyHeader(alerts))) // we specifically need to enqueue
    }

    override fun onLoadingFailed(it: Throwable, clearLoadedItems: Boolean) {
        _viewEffects.value = ViewEffects.LoadingFailed
        _viewState.postValue(ViewState.Error.LoadingError(it.message, clearLoadedItems))
    }

    override fun onDeletionFailed(it: Throwable, alert: AlertItem.Alert, actualAlerts: List<AlertItem>) {
        _viewEffects.value = ViewEffects.AlertDeleteFailed
        _viewState.postValue(ViewState.Error.DeletionError(it.message, alert, actualAlerts))
    }

    override fun onDeletionSuccess(alert: AlertItem.Alert) {
        _viewEffects.value = ViewEffects.AlertsWereDeleted
    }

    override fun hasErrorState(): Boolean = _viewState.value is ViewState.Error

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    sealed class ViewState {
        object Loading : ViewState()

        data class Success(
            val alerts: List<AlertItem>,
            val showNoAlertsWarning: Boolean
        ) : ViewState()

        sealed class Error : ViewState() {
            data class LoadingError(
                val msg: String?,
                val wasIntendedToClearItems: Boolean
            ) : Error()

            data class DeletionError(
                val msg: String?,
                val alert: AlertItem.Alert,
                val alerts: List<AlertItem>
            ) : Error()
        }
    }

    internal sealed class ManageButtonState {
        object Loading : ManageButtonState()
        data class Success(val isEnabled: Boolean) : ManageButtonState()
        object Error : ManageButtonState()
    }

    private fun hasOnlyHeader(list: List<AlertItem>): Boolean {
        return list.size <= 1
    }
}
