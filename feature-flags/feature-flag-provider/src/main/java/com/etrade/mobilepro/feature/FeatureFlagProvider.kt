package com.etrade.mobilepro.feature

import kotlinx.coroutines.flow.Flow

interface FeatureFlagProvider : FeatureFlagRepo {

    val title: String

    fun hasFeature(feature: Feature): Flow<Boolean>
}
