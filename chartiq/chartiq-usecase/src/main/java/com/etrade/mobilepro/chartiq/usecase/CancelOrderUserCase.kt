package com.etrade.mobilepro.chartiq.usecase

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.chartiq.api.model.OrderDetails
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.util.UseCase

sealed class CancelOrderResult {
    object CancelNotNeeded : CancelOrderResult()
    data class CancelSubmitted(val messages: List<ServerMessage>) : CancelOrderResult()
}

interface CancelOrderUserCase : UseCase<OrderDetails, ETResult<CancelOrderResult>>
