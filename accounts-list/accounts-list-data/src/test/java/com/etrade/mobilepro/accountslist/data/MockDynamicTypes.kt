package com.etrade.mobilepro.accountslist.data

import com.etrade.mobilepro.accountslist.data.dto.AccountReferenceDto
import com.etrade.mobilepro.accountslist.data.dto.AccountsViewDto
import com.etrade.mobilepro.backends.mgs.BaseMeta
import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.dynamicui.DynamicEnum
import com.etrade.mobilepro.dynamicui.addSubtypes
import com.etrade.msaccounts.dto.UserPreferencesDto
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory

enum class MockDynamicViewType(
    override val dtoName: String,
    override val dtoClass: Class<out BaseScreenView>
) : DynamicEnum<BaseScreenView> {

    ACCOUNT_LIST("account_list", AccountsViewDto::class.java)
}

enum class MockDynamicReferenceType(
    override val dtoName: String,
    override val dtoClass: Class<out BaseReference<*>>
) : DynamicEnum<BaseReference<*>> {
    ACCOUNTS("accounts", AccountReferenceDto::class.java),
    USER_PREFERENCES("user_preferences", UserPreferencesDto::class.java)
}

enum class MockDynamicMetaType(
    override val dtoName: String,
    override val dtoClass: Class<out BaseMeta>
) : DynamicEnum<BaseMeta>

internal fun createBaseScreenViewAdapter(): PolymorphicJsonAdapterFactory<BaseScreenView> {
    return addSubtypes(
        PolymorphicJsonAdapterFactory.of(BaseScreenView::class.java, "type")
            .withDefaultValue(object : BaseScreenView() {
                override val data: Any? = null
                override val ctaList: List<CallToActionDto>? = null
            }),
        MockDynamicViewType.values().toList()
    )
}

internal fun createBaseReferenceAdapter(): PolymorphicJsonAdapterFactory<BaseReference<*>> {
    return addSubtypes(
        PolymorphicJsonAdapterFactory.of(BaseReference::class.java, "type"),
        MockDynamicReferenceType.values().toList()
    )
}

internal fun createDynamicMetaTypeAdapter(): PolymorphicJsonAdapterFactory<BaseMeta> {
    return addSubtypes(
        PolymorphicJsonAdapterFactory.of(BaseMeta::class.java, "type"),
        MockDynamicMetaType.values().toList()
    )
}
