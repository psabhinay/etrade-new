package com.etrade.mobilepro.dynamic.referencehelper

import com.etrade.mobilepro.backends.mgs.BaseReference

/**
 * Helper interface to work with [BaseReference] objects.
 */
interface ReferenceHelper {

    /**
     * Persists a [reference].
     *
     * @param reference the reference to persist
     */
    fun persist(reference: BaseReference<*>)
}
