package com.etrade.mobilepro.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class NullableExtKtTest {
    @Test
    fun guardWithNull() {
        val output = sut(input = null)
        assertEquals("NULL", output)
    }

    @Test
    fun guardWithNotNull() {
        val output = sut(input = 1)
        assertEquals("1", output)
    }

    fun sut(input: Int?): String {
        val result = input guard {
            return "NULL"
        }
        return result.toString()
    }
}
