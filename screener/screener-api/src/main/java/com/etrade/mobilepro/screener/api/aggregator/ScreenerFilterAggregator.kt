package com.etrade.mobilepro.screener.api.aggregator

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType
import io.reactivex.Observable

interface ScreenerFilterAggregator {
    fun init()
    /**
     * Puts an updated filter into the aggregator
     * @param type Type of Screener
     * @param filter Updated filter
     * @param triggerRequest this flags denotes if screenerFilterRequests() Observable should emit new event( = network request should be triggered).
     *              There are certain cases when we don't want to trigger network request after the filter update:
     *              for example not all required fields of the filter are set (user has not set the price value, but toggled time period button or vice versa)
     */
    fun put(type: ScreenerType, filter: ScreenerFilter, triggerRequest: Boolean)
    fun <T : ScreenerFilter> get(type: ScreenerType): T
    fun clear(type: ScreenerType)
    fun screenerFilterRequests(): Observable<ScreenerFilterRequest>
    fun generateRequestStringRepresentation(group: ScreenerType.Group): String
    fun getFilterSnapshot(group: ScreenerType.Group): List<ScreenerFilter>
}

data class ScreenerFilterRequest(
    val type: ScreenerType,
    val filter: String,
    val fireNetworkRequest: Boolean
)
