package com.etrade.mobilepro.portfolio.delegate

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.portfolio.PortfolioPositionsDisplayMode
import com.etrade.mobilepro.portfolio.PortfolioSettingsRepository
import com.etrade.mobilepro.portfolio.PortfolioTableMode
import com.etrade.mobilepro.portfolio.data.PortfolioColumnRepo
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.util.SortOrder
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class PortfolioSortDelegateTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val portfolioColumnRepo: PortfolioColumnRepo = mock()

    private val mockObserver = mock<Observer<SortInfo>>()

    private val defaultSortInfo = SortInfo(PortfolioRecordColumn.SYMBOL, SortOrder.ASCENDING)

    @Test
    fun `when sort by Column with the portfolio sort state should match the input sort`() {

        // given
        val sortInfo = SortInfo(PortfolioRecordColumn.SYMBOL, SortOrder.DESCENDING)
        val sut = PortfolioSortDelegate(portfolioColumnRepo, getMockRepo())
        sut.sortInfo.observeForever(mockObserver)

        // when
        sut.sortColumn(sortInfo)

        // then
        verify(mockObserver).onChanged(eq(sortInfo))
    }

    @Test
    fun `when sort by Column, portfolio sort state should match expected sort`() {

        val sut = PortfolioSortDelegate(portfolioColumnRepo, getMockRepo())
        // given
        sut.sortInfo.observeForever(mockObserver)

        // when
        sut.sortColumn(PortfolioRecordColumn.LAST_TRADE)

        // then
        val sortInfo = SortInfo(PortfolioRecordColumn.LAST_TRADE, SortOrder.ASCENDING)
        verify(mockObserver).onChanged(eq(sortInfo))
    }

    @Test
    fun `repeated sort by column on single portfolio page then sort order will be opposite of previous sort order`() {

        val sut = PortfolioSortDelegate(portfolioColumnRepo, getMockRepo())
        // given
        sut.isEndOfPage = { true } // Single Page
        sut.localSortInfo.observeForever(mockObserver)

        // when
        sut.sortColumn(PortfolioRecordColumn.TOTAL_GAIN)
        sut.sortColumn(PortfolioRecordColumn.TOTAL_GAIN)

        // then
        val sortInfo = SortInfo(PortfolioRecordColumn.TOTAL_GAIN, SortOrder.DESCENDING)
        verify(mockObserver).onChanged(eq(sortInfo))
    }

    @Test
    fun `local sort is initialized to default sort by symbol in ascending order`() {

        val sut = PortfolioSortDelegate(portfolioColumnRepo, getMockRepo())
        // given
        sut.localSortInfo.observeForever(mockObserver)

        // when
        sut.setLocalSort()

        // then
        verify(mockObserver).onChanged(eq(defaultSortInfo))
    }

    @Test
    fun `setting of positions view type changes the Table view index value`() {

        // given
        val sut1 = PortfolioSortDelegate(
            portfolioColumnRepo,
            getMockRepo()
        )

        // when
        sut1.setPositionsViewType(isAllBrokerageView = true)

        // then
        assertEquals(PositionTableMode.ALL_BROKERAGE_VIEW, sut1.positionTableMode)
        assertEquals(0, sut1.tableViewIndex)

        // given
        val sut2 = PortfolioSortDelegate(
            portfolioColumnRepo,
            getMockRepo(PortfolioTableMode.OPTIONS, PortfolioPositionsDisplayMode.LIST)
        )

        // when
        sut2.setPositionsViewType(isAllBrokerageView = false)

        // then
        assertEquals(PositionTableMode.PORTFOLIO_VIEW, sut2.positionTableMode)
        assertEquals(1, sut2.tableViewIndex)
    }

    private fun getMockRepo(
        mockPortfolioTableMode: PortfolioTableMode = PortfolioTableMode.STANDARD,
        mockPositionsDisplayMode: PortfolioPositionsDisplayMode = PortfolioPositionsDisplayMode.TABLE,
        mockSortInfo: SortInfo = defaultSortInfo
    ): PortfolioSettingsRepository {
        return mock {
            on { tableMode } doReturn mockPortfolioTableMode
            on { positionsDisplayMode } doReturn mockPositionsDisplayMode
            on { sortInfo } doReturn mockSortInfo
        }
    }
}
