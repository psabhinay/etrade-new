package com.etrade.mobilepro.news.presentation.filter.portfolio

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.news.api.filter.FilterDataSource
import com.etrade.mobilepro.util.NetworkState
import javax.inject.Inject

class PortfolioFilterDataSource @Inject constructor(
    accountListRepo: AccountListRepo,
    private val mapper: (@JvmSuppressWildcards List<@JvmSuppressWildcards Account>) -> @JvmSuppressWildcards List<@JvmSuppressWildcards PortfolioFilterItem>
) : FilterDataSource<PortfolioFilterItem> {
    private var _selectedItem = MutableLiveData<PortfolioFilterItem>()
    private val _networkState = MutableLiveData<NetworkState>(NetworkState.Success)
    private val _items by lazy {
        createItemsLiveData(accountListRepo)
    }

    private fun createItemsLiveData(accountListRepo: AccountListRepo): MutableLiveData<List<PortfolioFilterItem>> {
        val loadedItems = mapper.invoke(accountListRepo.getAccountList())
        return MutableLiveData(loadedItems)
    }

    init {
        // Default to ALL
        _selectedItem.value = mapper.invoke(emptyList())[0]
    }

    override fun refresh() = Unit // this view model is always up to date

    override val networkState: LiveData<NetworkState> = _networkState

    override val items: LiveData<List<PortfolioFilterItem>>
        get() = _items

    override val selectedItem: LiveData<PortfolioFilterItem> = _selectedItem

    override fun getSelectedItemIndex(): Int {
        val selected = selectedItem.value ?: throw IllegalStateException("No selected filter item")
        val values = items.value ?: throw IllegalStateException("Filter list is null")
        return values.indexOf(selected)
    }

    override fun setSelectedItem(item: PortfolioFilterItem) {
        this._selectedItem.value = item
    }

    override fun clear() {
        val defaultItem = mapper.invoke(emptyList())[0]
        _selectedItem.value = defaultItem
        _items.value = listOf(defaultItem)

        if (_networkState.value == NetworkState.Loading) {
            _networkState.value = NetworkState.Success
        }
    }

    override fun findAndSelectItemByAccountId(accountId: String, isStockPlan: Boolean?) {
        _items.value?.let { loadedItems ->
            // If there's only one item there's nothing to select, because it contains only "All". This is initial value.
            if (loadedItems.size > 1) {
                loadedItems.find { loadedItem ->
                    loadedItem.accountId == accountId && isStockPlan?.let { it == loadedItem.isStockPlan } ?: true
                }?.let {
                    setSelectedItem(it)
                }
            }
        }
    }
}
