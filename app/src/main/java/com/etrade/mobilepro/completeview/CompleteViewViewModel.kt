package com.etrade.mobilepro.completeview

import android.content.res.Resources
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.common.di.CompleteViewDynamicScreen
import com.etrade.mobilepro.completeview.model.AccountSummaryViewModel
import com.etrade.mobilepro.dynamicui.ViewAllAccountsViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicLayout
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.dynamicui.api.InboxMessagesWidgetPlace
import com.etrade.mobilepro.inboxmessages.dynamicui.bannerwidget.BannerWidgetViewModel
import com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt.FundingPromptViewModel
import com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt.rearrangeFundingPromptsToTheirAccountSummaries
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.msaccounts.api.MsAccountsRepo
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsEnrollmentViewModel
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsErrorViewModel
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsSummaryViewLayout
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsSummaryViewModel
import com.etrade.mobilepro.msaccounts.presentation.aggregate.AggregateValuesViewLayout
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate
import com.etrade.mobilepro.tableview.OnCustomizeSettingsClickListener
import com.etrade.mobilepro.tableview.TableFooter
import com.etrade.mobilepro.tracking.SCREEN_TITLE_COMPLETE_VIEW
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.etrade.mobilepro.util.getTimestampString
import com.etrade.mobilepro.util.guard
import com.etrade.msaccounts.dto.MsAccountSummarySectionDtoType
import com.etrade.msaccounts.dto.MsAccountsSummaryViewDto
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.rx2.asFlow
import okhttp3.internal.toImmutableList
import org.slf4j.LoggerFactory
import javax.inject.Inject

@ExperimentalCoroutinesApi
class CompleteViewViewModel @Inject constructor(
    private val resources: Resources,
    @CompleteViewDynamicScreen
    private val dynamicScreenRepo: DynamicScreenRepo,
    private val loginPreferences: LoginPreferencesRepo,
    private val darkModeChecker: DarkModeChecker,
    private val streamingStatusController: StreamingStatusController,
    private val userViewModel: UserViewModel,
    private val msAccountsRepo: MsAccountsRepo,
    private val inboxMessagesRepo: InboxMessagesRepository
) : ViewModel(), ScreenViewModel, TableFooter, OnCustomizeSettingsClickListener {

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    var lastLayouts: List<GenericLayout> = emptyList()

    private var completeViewJob: Job? = null

    private val _viewStates = MutableLiveData<ViewState>()

    private val _primaryTimestamp: MutableLiveData<CharSequence> = MutableLiveData()
    private val _isFooterVisible: MutableLiveData<Boolean> = MutableLiveData(false)

    private val timestamp: String
        get() = getTimestampString(resources)

    val viewState: LiveData<ViewState> = _viewStates

    private val _customizeClickedSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val customizeClickedSignal: LiveData<ConsumableLiveEvent<Unit>> get() = _customizeClickedSignal

    private val _disclosuresClickedSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val disclosuresClickedSignal: LiveData<ConsumableLiveEvent<Unit>> get() = _disclosuresClickedSignal

    private val streamingUpdateListener: (StreamingStatusUpdate) -> Unit = {
        (viewState.value as? ViewState.Success)?.let {
            it.shouldRefresh = true
        }
    }

    private val msCompleteViewReloadEvent = MutableStateFlow<Long>(0)

    override val primaryTimestamp: LiveData<CharSequence>
        get() = _primaryTimestamp

    override val secondaryTimestamp: LiveData<CharSequence> = MutableLiveData()

    override val isFooterVisible: LiveData<Boolean>
        get() = _isFooterVisible

    override val screenName = SCREEN_TITLE_COMPLETE_VIEW

    init {
        getCompleteView()
        streamingStatusController.subscribeForStreamingStatusUpdates(streamingUpdateListener)
    }

    fun getCompleteView(clearViews: Boolean = true) {
        if (clearViews) {
            lastLayouts = emptyList()
            _isFooterVisible.value = false
        }
        completeViewJob?.apply {
            if (!isCancelled) {
                val message = "getCompleteView job cancelled in progress"
                logger.debug(message)
                cancel(message)
            }
        }
        completeViewJob = launchCompleteViewJob()
    }

    private fun launchCompleteViewJob(): Job = viewModelScope.launch {
        userViewModel.defaultAccountId?.let { defaultAccountId ->
            dynamicScreenRepo.getScreen(
                ServicePath.CompleteView(defaultAccountId),
                showCachedData = loginPreferences.isLoginForSameUser
            )
                .asFlow()
                .combine(getMsCompleteView()) { etAccounts, msAccounts ->
                    combineCompleteViews(etAccounts, msAccounts)
                }
                .catch { e ->
                    logger.error("getCompleteView failed", e)
                    _viewStates.postValue(ViewState.Error)
                }.collect { screenResource ->
                    val screens = screenResource.data.orEmpty()
                        .rearrangeFundingPromptsToTheirAccountSummaries(InboxMessagesWidgetPlace.COMPLETE_VIEW)
                        .rearrangeLocalyticsBannerCard()
                    when (screenResource) {
                        is Resource.Success -> postResult(screens) { ViewState.Success(it, uiMode = darkModeChecker.uiMode) }
                        is Resource.Loading -> postResult(screens) { ViewState.Loading(lastLayouts) }
                        is Resource.Failed -> _viewStates.postValue(ViewState.Error)
                    }
                }
        }
    }

    private fun List<GenericLayout>.rearrangeLocalyticsBannerCard(): List<GenericLayout> {
        val bannerCard = this.find { it.viewModelClass == BannerWidgetViewModel::class.java } guard {
            return this
        }
        val resultingList = this.filterNot { it.viewModelClass == BannerWidgetViewModel::class.java }.toMutableList()
        val viewAllAccountsViewIndex = this.indexOfLast { it.viewModelClass == ViewAllAccountsViewModel::class.java }
        val lastAccountSummaryViewIndex = this.indexOfLast { it is AccountSummaryLayout }

        if (viewAllAccountsViewIndex > 0) {
            resultingList.add(viewAllAccountsViewIndex + 1, bannerCard)
        } else {
            resultingList.add(lastAccountSummaryViewIndex + 1, bannerCard)
        }
        return resultingList
    }

    fun reloadMsCompleteView() {
        logger.debug("Reloading MS CompleteView")
        lastLayouts = lastLayouts.filterNot {
            it.viewModelClass == MsAccountsEnrollmentViewModel::class.java || it.viewModelClass == MsAccountsErrorViewModel::class.java
        }
        msCompleteViewReloadEvent.value = System.currentTimeMillis()
    }

    override fun onCleared() {
        streamingStatusController.unsubscribeFromStreamingStatusUpdates(streamingUpdateListener)
        super.onCleared()
    }

    override fun onDisclosuresViewClicked(v: View) {
        _disclosuresClickedSignal.postValue(ConsumableLiveEvent(Unit))
    }

    override fun onCustomizeViewClicked(v: View) {
        _customizeClickedSignal.postValue(ConsumableLiveEvent(Unit))
    }

    private fun postResult(layouts: List<GenericLayout>, state: (List<GenericLayout>) -> ViewState) {
        val stateOutput = state(layouts)
        if (stateOutput is ViewState.Success) {
            lastLayouts = stateOutput.views
        }
        _viewStates.value = stateOutput
        _primaryTimestamp.value = timestamp
    }

    fun updateFooterVisibility() {
        _isFooterVisible.value = viewState.value is ViewState.Success
    }

    fun dismissInboxMessage(message: InboxMessage) {
        viewModelScope.launch {
            inboxMessagesRepo.dismissMessage(message)
        }
        lastLayouts = lastLayouts.removeBannerWidget()
        val viewState = (viewState.value as ViewState.Success)
        _viewStates.value = viewState.copy(
            views = viewState.views.removeBannerWidget(),
        ).also { it.shouldRefresh = false }
    }

    private fun List<GenericLayout>.removeBannerWidget() =
        this.toMutableList().let { layouts ->
            layouts.removeIf { it.viewModelClass == BannerWidgetViewModel::class.java }
            layouts
        }

    private fun getMsCompleteView(): Flow<Resource<List<GenericLayout>>> {
        return msCompleteViewReloadEvent
            .flatMapLatest {
                // Reload whenever a new reload event is emitted
                msAccountsRepo.getMsCompleteView()
            }
    }

    private fun combineCompleteViews(
        etAccounts: Resource<List<GenericLayout>>,
        msAccounts: Resource<List<GenericLayout>>
    ): Resource<List<GenericLayout>> {
        return when {
            etAccounts is Resource.Loading || msAccounts is Resource.Loading -> Resource.Loading()
            etAccounts is Resource.Failed -> etAccounts
            else -> etAccounts.map {
                it.orEmpty().updateMsAccountWidgets(msAccounts.data.orEmpty())
            }
        }
    }

    private fun List<GenericLayout>.updateMsAccountWidgets(msAccounts: List<GenericLayout>): List<GenericLayout> {
        val result = map { layout ->
            if (layout is AggregateValuesViewLayout) {
                layout.updateAggregateValuesViewDto(msAccounts.getMsAccountsSummaryDto())
            }
            msAccounts.firstOrNull { it.viewModelClass == layout.viewModelClass } ?: layout
        }.toMutableList()

        // if Ms Enrollment widget is present we want to add it before the first Account Summary widget
        result.insertCardFromSource(msAccounts, MsAccountsEnrollmentViewModel::class.java, result.indexOfFirst { it.isAccountSummary })
        // if Ms Error widget is present we want to add it after the last Account Summary widget
        result.insertCardFromSource(msAccounts, MsAccountsErrorViewModel::class.java, result.indexOfLast { it.isAccountSummary } + 1)

        return result.toImmutableList()
    }

    private fun MutableList<GenericLayout>.insertCardFromSource(
        source: List<GenericLayout>,
        viewModelClass: Class<out DynamicLayout>,
        insertPosition: Int
    ) {
        source.firstOrNull { it.viewModelClass == viewModelClass }
            ?.let { enrollmentLayout ->
                if (insertPosition != -1) {
                    add(insertPosition, enrollmentLayout)
                }
            }
    }

    private val GenericLayout.isAccountSummary: Boolean
        get() = viewModelClass == AccountSummaryViewModel::class.java || viewModelClass == FundingPromptViewModel::class.java

    private fun List<GenericLayout>.getMsAccountsSummaryDto() =
        (firstOrNull { it.viewModelClass == MsAccountsSummaryViewModel::class.java } as? MsAccountsSummaryViewLayout)?.dto

    private fun AggregateValuesViewLayout.updateAggregateValuesViewDto(msAccountsSummaryDto: MsAccountsSummaryViewDto?) {
        msAccountsSummaryDto?.let { msSummaryDto ->
            val totalAssets =
                msSummaryDto.getTotalSummaryInitialValue(MsAccountSummarySectionDtoType.TOTAL_ASSETS)
            val totalLiabilities =
                msSummaryDto.getTotalSummaryInitialValue(MsAccountSummarySectionDtoType.TOTAL_LIABILITIES)
            dto = dto.copy(
                msAccountsTotalAssets = totalAssets,
                msAccountsTotalLiabilities = totalLiabilities
            )
        }
    }

    private fun MsAccountsSummaryViewDto.getTotalSummaryInitialValue(type: MsAccountSummarySectionDtoType) =
        data.accountAdditionalLabels.firstOrNull { it.title == type.labelTitle }

    sealed class ViewState {
        data class Loading(val views: List<GenericLayout> = emptyList()) : ViewState()

        data class Success(val views: List<GenericLayout>, val uiMode: Int) : ViewState() {
            var shouldRefresh: Boolean = true
                get() {
                    val current = field
                    field = false
                    return current
                }
        }

        object Error : ViewState()
    }
}
