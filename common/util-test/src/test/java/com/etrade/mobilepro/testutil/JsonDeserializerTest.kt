package com.etrade.mobilepro.testutil

import com.squareup.moshi.JsonClass
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class JsonDeserializerTest {
    @Test
    fun testJsonResourceParsing() {
        val testObject: TestObject = JsonDeserializerTest::class.getObjectFromJson("testObject.json")
        assertEquals(testObject.someKey, "someValue")
    }

    @JsonClass(generateAdapter = true)
    internal data class TestObject(val someKey: String?)
}
