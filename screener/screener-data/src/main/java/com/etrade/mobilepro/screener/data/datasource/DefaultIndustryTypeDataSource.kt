package com.etrade.mobilepro.screener.data.datasource

import android.content.res.Resources
import com.etrade.mobilepro.screener.api.Industry
import com.etrade.mobilepro.screener.api.IndustryType
import com.etrade.mobilepro.screener.api.datasource.IndustryTypeDataSource
import com.etrade.mobilepro.screener.data.R
import javax.inject.Inject

private const val STOCK_ID = "Stock"
private const val BOND_ID = "Bond"
private const val MUNI_ID = "Muni"
class DefaultIndustryTypeDataSource @Inject constructor(private val resources: Resources) : IndustryTypeDataSource {
    private val allIndustries by lazy {
        Industry(
            id = "",
            title = resources.getString(R.string.all_industries_short),
            isAllItemsElement = true
        )
    }
    private val allTypes by lazy {
        IndustryType(
            id = resources.getString(R.string.industry_type_all),
            title = resources.getString(R.string.industry_type_all),
            isAllItemsElement = true
        )
    }
    private val allIndustryTypes by lazy {
        listOf(
            allTypes,
            IndustryType(
                id = STOCK_ID,
                title = resources.getString(R.string.industry_type_stock)
            ),
            IndustryType(
                id = BOND_ID,
                title = resources.getString(R.string.industry_type_bond)
            ),
            IndustryType(
                id = MUNI_ID,
                title = resources.getString(R.string.industry_type_muni)
            )
        )
    }

    private val stockIndustries by lazy {
        listOf(
            allIndustries,
            Industry(
                id = "Industry_Exposure_Stock_BasicMaterials",
                title = resources.getString(R.string.basic_materials)
            ),
            Industry(
                id = "Industry_Exposure_Stock_CommunicationServices",
                title = resources.getString(R.string.communication_services)
            ),
            Industry(
                id = "Industry_Exposure_Stock_ConsumerCyclical",
                title = resources.getString(R.string.sector_consumer_cyclical)
            ),
            Industry(
                id = "Industry_Exposure_Stock_ConsumerDefensive",
                title = resources.getString(R.string.consumer_defensive)
            ),
            Industry(
                id = "Industry_Exposure_Stock_Energy",
                title = resources.getString(R.string.sector_energy)
            ),
            Industry(
                id = "Industry_Exposure_Stock_FinancialServices",
                title = resources.getString(R.string.financial_services)
            ),
            Industry(
                id = "Industry_Exposure_Stock_Healthcare",
                title = resources.getString(R.string.sector_healthcare)
            ),
            Industry(
                id = "Industry_Exposure_Stock_Industrials",
                title = resources.getString(R.string.sector_industrials)
            ),
            Industry(
                id = "Industry_Exposure_Stock_RealEstate",
                title = resources.getString(R.string.real_estate)
            ),
            Industry(
                id = "Industry_Exposure_Stock_Technology",
                title = resources.getString(R.string.sector_technology)
            )
        )
    }

    private val bondIndustries by lazy {
        listOf(
            allIndustries,
            Industry(
                id = "Industry_Exposure_Bond_Government",
                title = resources.getString(R.string.government)
            ),
            Industry(
                id = "Industry_Exposure_Bond_GovernmentRelated",
                title = resources.getString(R.string.government_related)
            ),
            Industry(
                id = "Industry_Exposure_Bond_MunicipalTaxable",
                title = resources.getString(R.string.municipal_taxable)
            ),
            Industry(
                id = "Industry_Exposure_Bond_MunicipalTaxExempt",
                title = resources.getString(R.string.municipal_tax_exempt)
            ),
            Industry(
                id = "Industry_Exposure_Bond_BankLoan",
                title = resources.getString(R.string.bank_loan)
            ),
            Industry(
                id = "Industry_Exposure_Bond_Convertible",
                title = resources.getString(R.string.convertible)
            ),
            Industry(
                id = "Industry_Exposure_Bond_CorporateBond",
                title = resources.getString(R.string.corporate_bond)
            ),
            Industry(
                id = "Industry_Exposure_Bond_PreferredStock",
                title = resources.getString(R.string.preferred_stock)
            ),
            Industry(
                id = "Industry_Exposure_Bond_AgencyMortgageBacked",
                title = resources.getString(R.string.agency_mortgage_backed)
            ),
            Industry(
                id = "Industry_Exposure_Bond_NonAgencyResidentialMortgageBacked",
                title = resources.getString(R.string.non_agency_residential_mortgage_backed)
            ),
            Industry(
                id = "Industry_Exposure_Bond_CommercialMortgageBacked",
                title = resources.getString(R.string.commercial_mortgage_backed)
            ),
            Industry(
                id = "Industry_Exposure_Bond_CoveredBond",
                title = resources.getString(R.string.covered_bond)
            ),
            Industry(
                id = "Industry_Exposure_Bond_AssetBacked",
                title = resources.getString(R.string.asset_backed)
            ),
            Industry(
                id = "Industry_Exposure_Bond_Cash",
                title = resources.getString(R.string.cash_and_equivalents)
            ),
            Industry(
                id = "Industry_Exposure_Bond_Swap",
                title = resources.getString(R.string.swap)
            ),
            Industry(
                id = "Industry_Exposure_Bond_FutureForward",
                title = resources.getString(R.string.future_forward)
            ),
            Industry(
                id = "Industry_Exposure_Bond_OptionWarrent",
                title = resources.getString(R.string.option_warrant)
            )
        )
    }

    private val muniIndustries by lazy {
        listOf(
            allIndustries,
            Industry(
                id = "Industry_Exposure_Muni_AdvanceRefunded",
                title = resources.getString(R.string.advance_refunded)
            ),
            Industry(
                id = "Industry_Exposure_Muni_Education",
                title = resources.getString(R.string.education)
            ),
            Industry(
                id = "Industry_Exposure_Muni_GeneralObligation",
                title = resources.getString(R.string.general_obligation)
            ),
            Industry(
                id = "Industry_Exposure_Muni_Health",
                title = resources.getString(R.string.health)
            ),
            Industry(
                id = "Industry_Exposure_Muni_Housing",
                title = resources.getString(R.string.housing)
            ),
            Industry(
                id = "Industry_Exposure_Muni_Industrial",
                title = resources.getString(R.string.industrial)
            ),
            Industry(
                id = "Industry_Exposure_Muni_MiscRevenue",
                title = resources.getString(R.string.misc_revenue)
            ),
            Industry(
                id = "Industry_Exposure_Muni_NonStateAppropriatedTobacco",
                title = resources.getString(R.string.non_state_appr_tobaco)
            ),
            Industry(
                id = "Industry_Exposure_Muni_StateAppropriatedTobacco",
                title = resources.getString(R.string.state_appr_tobaco)
            ),
            Industry(
                id = "Industry_Exposure_Muni_Transportation",
                title = resources.getString(R.string.transportation)
            ),
            Industry(
                id = "ndustry_Exposure_Muni_Utilities",
                title = resources.getString(R.string.sector_utilities)
            ),
            Industry(
                id = "Industry_Exposure_Muni_WaterSewer",
                title = resources.getString(R.string.water_sewer)
            )
        )
    }

    override fun getAllTypesItem(): IndustryType = allTypes

    override fun getIndustryTypes(): List<IndustryType> = allIndustryTypes

    override fun getAllIndustriesItem(): Industry = allIndustries

    override fun getIndustriesOfType(type: IndustryType): List<Industry> = when (type.id) {
        STOCK_ID -> stockIndustries
        BOND_ID -> bondIndustries
        MUNI_ID -> muniIndustries
        else -> listOf(allIndustries)
    }
}
