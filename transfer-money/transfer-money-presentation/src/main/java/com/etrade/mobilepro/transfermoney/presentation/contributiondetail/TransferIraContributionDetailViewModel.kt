package com.etrade.mobilepro.transfermoney.presentation.contributiondetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.transfermoney.api.TransferIraContribution
import com.etrade.mobilepro.transfermoney.api.repo.TransferIraContributionRepo
import javax.inject.Inject

class TransferIraContributionDetailViewModel @Inject constructor(
    private val repo: TransferIraContributionRepo
) : ViewModel() {
    private val _iraDetailsRetry = MutableLiveData<Unit>().apply { value = Unit }
    private val _selectedAccount = MutableLiveData<FundsFlowAccount>()

    internal val iraContributionDetails: LiveData<ViewState> =
        _selectedAccount.distinctUntilChanged().combineLatest(_iraDetailsRetry).switchMap { (selectedAccount, _) ->
            liveData(context = viewModelScope.coroutineContext) {
                emit(ViewState.Loading)
                if (selectedAccount?.isIraAccount == true) {
                    val iraDetails = repo.getIraContributionInfo(selectedAccount.accountId).fold(
                        onSuccess = {
                            ViewState.Success(it)
                        },
                        onFailure = {
                            ViewState.Error {
                                _iraDetailsRetry.value = Unit
                            }
                        }
                    )
                    emit(iraDetails)
                }
            }
        }

    fun setSelectedAccount(selectedAccount: FundsFlowAccount) {
        _selectedAccount.value = selectedAccount
    }

    internal sealed class ViewState {
        object Loading : ViewState()
        data class Success(
            val contributionDetails: TransferIraContribution
        ) : ViewState()
        class Error(
            val retry: () -> Unit
        ) : ViewState()
    }
}
