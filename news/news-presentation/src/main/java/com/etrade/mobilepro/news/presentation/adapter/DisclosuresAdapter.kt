package com.etrade.mobilepro.news.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.util.android.extension.inflate

class DisclosuresAdapter(private val disclosuresItemListener: (() -> Unit)? = null) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        DisclosuresItemViewHolder(
            parent.inflate(R.layout.item_disclosures),
            disclosuresItemListener
        )

    override fun getItemCount() = 1
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) = Unit
}
