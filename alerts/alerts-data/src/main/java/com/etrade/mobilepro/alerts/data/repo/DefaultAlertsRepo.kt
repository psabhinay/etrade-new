package com.etrade.mobilepro.alerts.data.repo

import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.api.model.AlertMessage
import com.etrade.mobilepro.alerts.api.model.AlertValueType
import com.etrade.mobilepro.alerts.api.model.NewsAlertValueType
import com.etrade.mobilepro.alerts.api.model.SetAlertResponse
import com.etrade.mobilepro.alerts.api.model.SetNewsAlertResponse
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.alerts.api.model.UserAlertStringResources
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.alerts.data.dto.AlertsCountRequest
import com.etrade.mobilepro.alerts.data.mapper.AlertContentDtoMapper
import com.etrade.mobilepro.alerts.data.mapper.AlertDtoMapper
import com.etrade.mobilepro.alerts.data.mapper.SetAlertDtoMapper
import com.etrade.mobilepro.alerts.data.mapper.SetNewsAlertDtoMapper
import com.etrade.mobilepro.alerts.data.rest.AlertApiClient
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.util.json.boolToInt
import com.etrade.mobilepro.util.json.boolToIntOrNull
import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import java.util.concurrent.TimeUnit

private const val STARTING_ID = "0"
private const val MAX_COUNT = 100

class DefaultAlertsRepo(
    private val apiClient: AlertApiClient,
    strings: UserAlertStringResources,
    private val mainThreadScheduler: Scheduler
) : AlertsRepo {
    private val alertMapper = AlertDtoMapper()
    private val alertContentMapper = AlertContentDtoMapper()
    private val setAlertMapper = SetAlertDtoMapper(strings)
    private val setNewsAlertMapper = SetNewsAlertDtoMapper()

    private val memCachedAlerts = mutableMapOf<String, MutableSet<AlertItem.Alert>>()

    override suspend fun findAlertByMessage(
        userId: Long?,
        alertMessage: String
    ): ETResult<AlertItem.Alert?> = runCatchingET {
        val userIdParam = requireNotNull(userId?.toString())
        val startingTimeStamp = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())

        findAlertByMessageRecursively(userIdParam, STARTING_ID, startingTimeStamp, alertMessage)
    }

    override fun loadNext(
        userId: String,
        startingEventId: String,
        startingTimeStamp: Long,
        maxCount: Int
    ): Single<List<AlertItem.Alert>> =
        apiClient.getInboxAlerts(userId, startingEventId, startingTimeStamp, maxCount, 1)
            .toObservable()
            .map {
                it.events ?: emptyList()
            }
            .map { list ->
                list.mapNotNull(alertMapper::map)
            }
            .observeOn(mainThreadScheduler)
            .doOnNext {
                putInMemCache(userId, it)
            }
            .firstOrError()

    override fun loadAlertsCount(): Single<Int> {
        return apiClient.getAlertsCount(AlertsCountRequest())
            .map { it.data.alertInbox.numberOfAlerts }
    }

    override fun loadAlertContent(userId: String, alertId: String): Single<AlertMessage> =
        apiClient.getAlertContent(userId, alertId, isWebView = true)
            .toObservable()
            .map {
                it.message ?: throw IllegalArgumentException("Empty payload")
            }
            .map {
                alertContentMapper.map(it)
            }
            .firstOrError()

    override fun deleteAlert(userId: String, alertId: String): Completable =
        apiClient.deleteAlert(userId, alertId)
            .toObservable()
            .firstOrError()
            .flatMapCompletable {
                Completable.complete()
            }

    override fun setAlert(
        sessionToken: String,
        symbol: String,
        values: Map<AlertValueType, String>
    ): Single<SetAlertResponse> =
        apiClient.setAlert(
            sessionToken = sessionToken,
            symbol = symbol,
            targetPriceLow = values[AlertValueType.LOW_TARGET_PRICE] ?: "",
            targetPriceHigh = values[AlertValueType.HIGH_TARGET_PRICE] ?: "",
            dailyPercentLow = values[AlertValueType.LOW_DAILY_PERCENT] ?: "",
            dailyPercentHigh = values[AlertValueType.HIGH_DAILY_PERCENT] ?: "",
            peTargetLow = values[AlertValueType.LOW_PE_TARGET] ?: "",
            peTargetHigh = values[AlertValueType.HIGH_PE_TARGET] ?: "",
            dailyVolume = values[AlertValueType.DAILY_VOLUME] ?: ""
        )
            .toObservable()
            .map { setAlertMapper.map(it) }
            .firstOrError()

    override fun setNewsAlert(
        sessionToken: String,
        symbol: String,
        values: Map<NewsAlertValueType, Boolean>
    ): Single<SetNewsAlertResponse> {
        val setNewsAlertValues =
            values.entries.associate { entry -> entry.key.requestParamName to boolToInt(entry.value) }

        return apiClient.setNewsAlert(
            sessionToken = sessionToken,
            symbol = symbol,
            switchParams = setNewsAlertValues
        )
            .toObservable()
            .map { setNewsAlertMapper.map(it) }
            .firstOrError()
    }

    override fun getUserAlerts(): Single<List<UserAlertItem>> {
        return Single.zip(
            getAlerts(), getNewsAlerts(),
            BiFunction { alertsResponse, newsAlertsResponse ->
                alertsResponse.alerts + newsAlertsResponse.alerts
            }
        )
    }

    override fun deleteUserAlert(alert: UserAlertItem): Completable {
        return if (alert is UserAlertItem.Alert) {
            apiClient.deleteUserAlert(deleteInfo = alert.deleteInfo).toObservable()
                .firstOrError()
                .flatMapCompletable {
                    Completable.complete()
                }
        } else {
            apiClient.deleteUserNewsAlert(deleteInfo = alert.deleteInfo).toObservable()
                .firstOrError()
                .flatMapCompletable {
                    Completable.complete()
                }
        }
    }

    override fun updateAlert(
        sessionToken: String,
        symbol: String,
        deleteInfo: String,
        edit: String,
        lastEdited: String,
        values: Map<AlertValueType, String>
    ): Single<SetAlertResponse> =
        apiClient.updateUserAlert(
            sessionToken = sessionToken,
            symbol = symbol,
            deleteInfo = deleteInfo,
            edit = edit,
            lastEdited = lastEdited,
            targetPriceLow = values[AlertValueType.LOW_TARGET_PRICE] ?: "",
            targetPriceHigh = values[AlertValueType.HIGH_TARGET_PRICE] ?: "",
            dailyPercentLow = values[AlertValueType.LOW_DAILY_PERCENT] ?: "",
            dailyPercentHigh = values[AlertValueType.HIGH_DAILY_PERCENT] ?: "",
            peTargetLow = values[AlertValueType.LOW_PE_TARGET] ?: "",
            peTargetHigh = values[AlertValueType.HIGH_PE_TARGET] ?: "",
            dailyVolume = values[AlertValueType.DAILY_VOLUME] ?: ""
        )
            .toObservable()
            .map { setAlertMapper.map(it) }
            .firstOrError()

    override fun updateNewsAlert(
        sessionToken: String,
        symbol: String,
        deleteInfo: String,
        edit: String,
        lastEdited: String,
        values: Map<NewsAlertValueType, Boolean>
    ): Single<SetNewsAlertResponse> =
        apiClient.updateUserNewsAlert(
            sessionToken = sessionToken,
            symbol = symbol,
            deleteInfo = deleteInfo,
            edit = edit,
            lastEdited = lastEdited,
            upDown = boolToIntOrNull(values[NewsAlertValueType.UP_DOWN]),
            inPlay = boolToIntOrNull(values[NewsAlertValueType.IN_PLAY]),
            earning = boolToIntOrNull(values[NewsAlertValueType.EARNINGS]),
            splitNotice = boolToIntOrNull(values[NewsAlertValueType.SPLIT_NOTICE])
        )
            .toObservable()
            .map { setNewsAlertMapper.map(it) }
            .firstOrError()

    private fun getAlerts(): Single<SetAlertResponse> =
        apiClient.getAlerts()
            .toObservable()
            .map { setAlertMapper.map(it) }
            .firstOrError()

    private fun getNewsAlerts(): Single<SetNewsAlertResponse> =
        apiClient.getNewsAlerts()
            .toObservable()
            .map { setNewsAlertMapper.map(it) }
            .firstOrError()

    private tailrec suspend fun findAlertByMessageRecursively(
        userId: String,
        startingEventId: String,
        startingTimeStamp: Long,
        alertMessage: String
    ): AlertItem.Alert? {
        val alertsChunk = apiClient.getInboxAlertsSuspend(
            userId,
            startingEventId,
            startingTimeStamp,
            MAX_COUNT,
            1
        )
        val alerts = alertsChunk.events?.mapNotNull(alertMapper::map)
        if (alerts.isNullOrEmpty()) {
            return null
        }
        putInMemCache(userId, alerts)
        val alert = alerts.find { it.subject.trim() == alertMessage.trim() }
        return if (alert != null) {
            alert
        } else {
            val last = alerts.minByOrNull { it.occurredAt } ?: alerts.last()
            findAlertByMessageRecursively(userId, last.id, last.occurredAt, alertMessage)
        }
    }

    private fun findInMemCachedAlerts(userId: String, alertMessage: String): AlertItem.Alert? =
        memCachedAlerts[userId]?.filter { it.subject == alertMessage }
            ?.maxByOrNull { it.occurredAt }

    private fun putInMemCache(userId: String, alerts: List<AlertItem.Alert>) {
        val set = memCachedAlerts.getOrPut(userId) { mutableSetOf() }
        set.addAll(alerts)
    }
}
