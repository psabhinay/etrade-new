package com.etrade.mobilepro.dagger

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.marketscalendar.viewmodel.CalendarEventDetailsViewModel
import com.etrade.mobilepro.marketscalendar.viewmodel.EarningsEventsMarketsCalendarViewModel
import com.etrade.mobilepro.marketscalendar.viewmodel.EconomicEventsMarketsCalendarViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MarketsCalendarViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CalendarEventDetailsViewModel::class)
    abstract fun bindCalendarEventDetailsViewModel(viewModel: CalendarEventDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EarningsEventsMarketsCalendarViewModel::class)
    abstract fun bindEarningsEventsMarketsCalendarViewModel(viewModel: EarningsEventsMarketsCalendarViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EconomicEventsMarketsCalendarViewModel::class)
    abstract fun bindEconomicEventsMarketsCalendarViewModel(viewModel: EconomicEventsMarketsCalendarViewModel): ViewModel
}
