package com.etrade.mobilepro.menu.builder

import android.app.Activity
import android.net.Uri
import android.os.Build
import androidx.annotation.DrawableRes
import androidx.navigation.NavDirections
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.menu.TextMenuItem
import com.etrade.mobilepro.menupage.R
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.P])
class TextMenuItemBuilderTest {

    private lateinit var initialMenuPageItemBuilder: TextMenuItemBuilder

    @Mock
    private lateinit var appUriAction: Uri

    @Mock
    private lateinit var navDirections: NavDirections

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        initialMenuPageItemBuilder = TextMenuItemBuilder()
        val setup: TextMenuItemBuilder.() -> Unit = {
            text = "Core Portfolios"
            subtext = "Need help getting started?"
            @DrawableRes
            icon = R.drawable.ic_settings
            showRightIcon = true
            showNewPill = true
            contentDescription = "Core Portfolios ".plus("Need help getting started?")
            menuActions = mutableListOf()
            shouldSaveDirections = true
            clickable = true
        }
        initialMenuPageItemBuilder.setup()
    }

    @Test
    fun `test menu page item builder`() {
        val menuPageItem = initialMenuPageItemBuilder.build()
        initialMenuPageItemBuilder.verifyMenuPageItemBuilder(menuPageItem)
    }

    @Test
    fun `test action`() {
        val onActionClick: (Activity) -> Unit = { /* Intentionally blank. */ }
        initialMenuPageItemBuilder.action(onActionClick)
        val expectedMenuAction = initialMenuPageItemBuilder.menuActions[initialMenuPageItemBuilder.menuActions.size - 1]
        assertTrue((expectedMenuAction as TextMenuItem.MenuAction.MenuActivityAction).action == onActionClick)
    }

    @Test
    fun `test nav direction`() {
        initialMenuPageItemBuilder.navDirections(navDirections)
        val expectedNavDirection = initialMenuPageItemBuilder.menuActions[initialMenuPageItemBuilder.menuActions.size - 1]
        assertTrue((expectedNavDirection as TextMenuItem.MenuAction.MenuNavDirectionsAction).navDirections == navDirections)
    }

    @Test
    fun `test uri app action`() {
        initialMenuPageItemBuilder.appAction(appUriAction)
        val expectedAppAction = initialMenuPageItemBuilder.menuActions[initialMenuPageItemBuilder.menuActions.size - 1]
        assertTrue((expectedAppAction as TextMenuItem.MenuAction.MenuAppAction).uri == appUriAction)
    }

    @Test
    fun `test string app action`() {
        val url = "https://us.uat.etrade.com"
        initialMenuPageItemBuilder.appAction(url)
        val expectedStringAppAction = initialMenuPageItemBuilder.menuActions[initialMenuPageItemBuilder.menuActions.size - 1]
        assertTrue((expectedStringAppAction as TextMenuItem.MenuAction.MenuAppAction).uri.toString() == url)
    }

    private fun TextMenuItemBuilder.verifyMenuPageItemBuilder(expectMenuPageItem: TextMenuItem) {
        assertTrue(id == expectMenuPageItem.id)
        assertTrue(text == expectMenuPageItem.text)
        assertTrue(subtext == expectMenuPageItem.subtext)
        assertTrue(icon == expectMenuPageItem.icon)
        assertTrue(showRightIcon == expectMenuPageItem.showRightIcon)
        assertTrue(showNewPill == expectMenuPageItem.showNewPill)
        assertTrue(contentDescription == expectMenuPageItem.contentDescription)
        assertTrue(menuActions.size == expectMenuPageItem.menuActions.size)
        assertTrue(shouldSaveDirections == expectMenuPageItem.shouldSaveDirections)
        assertTrue(clickable == expectMenuPageItem.clickable)
    }
}
