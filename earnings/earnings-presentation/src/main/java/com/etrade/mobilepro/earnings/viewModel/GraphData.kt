package com.etrade.mobilepro.earnings.viewModel

import androidx.annotation.ColorRes
import com.etrade.eo.core.util.isZero
import com.etrade.mobilepro.barchartview.DataEntry
import com.etrade.mobilepro.earnings.EarningsDetailData
import java.math.BigDecimal
import java.text.DecimalFormat

private val GRAPH_VALUE_TEMPLATE: DecimalFormat = DecimalFormat("#.###")

data class GraphData(
    override val x: Float,
    override val y: Float,
    override val barColor: Int,
    override val valueColor: Int,
    override val valueDescriptionColor: Int,
    val xFormatted: String,
    val valueDescriptionFormatted: String,
    val quarter: String,
    val isFuturePeriod: Boolean,
    val earningsDetailData: EarningsDetailData
) : DataEntry

data class GraphDataResources constructor(
    @ColorRes val purpleColor: Int,
    @ColorRes val greenColor: Int,
    @ColorRes val redColor: Int,
    @ColorRes val purpleWithOpacityColor: Int,
    @ColorRes val lightBlack: Int,
    val marketGraphXTemplate: String
)

private fun getValueDescription(item: EarningsDetailData): BigDecimal = item.surpriseMean
    ?.let { surprise ->
        item.actualValue
            ?.let { actual ->
                actual - surprise
            }
    } ?: BigDecimal.ZERO

private fun getYValue(item: EarningsDetailData): BigDecimal {
    return if (item.estMean?.isZero() == true) {
        item.actualValue ?: BigDecimal.ZERO
    } else {
        item.estMean ?: BigDecimal.ZERO
    }
}

@ColorRes
private fun getValueDescriptionColor(isFuturePeriod: Boolean, graphDataResources: GraphDataResources, valueDescription: BigDecimal): Int {
    return when {
        isFuturePeriod -> graphDataResources.lightBlack
        valueDescription > BigDecimal.ZERO -> graphDataResources.greenColor
        else -> graphDataResources.redColor
    }
}

private fun getXFormatted(item: EarningsDetailData, graphDataResources: GraphDataResources): String {
    return item.quarter?.let { quarter ->
        item.fiscalPeriodYear?.let { year ->
            graphDataResources.marketGraphXTemplate.format(quarter, year.drop(2))
        }
    } ?: ""
}

private fun getValueDescriptionFormatted(valueDescription: BigDecimal): String {
    return if (valueDescription.isZero()) {
        ""
    } else {
        valueDescription
            .let { GRAPH_VALUE_TEMPLATE.format(valueDescription) }
            .let { "($it)" }
    }
}

@ColorRes
private fun getBarColor(isFuturePeriod: Boolean, graphDataResources: GraphDataResources): Int {
    return if (isFuturePeriod) {
        graphDataResources.purpleWithOpacityColor
    } else {
        graphDataResources.purpleColor
    }
}

fun createGraphData(
    index: Int,
    item: EarningsDetailData,
    isFuturePeriod: Boolean,
    graphDataResources: GraphDataResources
): GraphData {
    val valueDescription: BigDecimal = getValueDescription(item)
    val barColor: Int = getBarColor(isFuturePeriod, graphDataResources)
    val valueDescriptionColor: Int = getValueDescriptionColor(isFuturePeriod, graphDataResources, valueDescription)
    val xFormatted: String = getXFormatted(item, graphDataResources)
    val valueDescriptionFormatted: String = getValueDescriptionFormatted(valueDescription)

    return GraphData(
        x = index.toFloat(),
        y = getYValue(item).toFloat(),
        barColor = barColor,
        valueColor = graphDataResources.lightBlack,
        valueDescriptionColor = valueDescriptionColor,
        xFormatted = xFormatted,
        valueDescriptionFormatted = valueDescriptionFormatted,
        quarter = item.quarter ?: "",
        isFuturePeriod = isFuturePeriod,
        earningsDetailData = item
    )
}
