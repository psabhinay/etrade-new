package com.etrade.mobilepro.documents

import androidx.navigation.NavController
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.documents.router.DocumentsRouter
import org.threeten.bp.LocalDate
import javax.inject.Inject

class DefaultDocumentsRouter @Inject constructor() : DocumentsRouter {
    override fun navigateToDetails(navController: NavController, item: Document) =
        navController.navigate(MainGraphDirections.actionDocumentListToDetails(documentId = item.documentId))

    override fun openTaxYearSelector(navController: NavController, minYear: Int, maxYear: Int, value: Int) {
        navController.navigate(MainGraphDirections.openDocumentsTaxYearSelector(minYear, maxYear, value))
    }

    override fun openDateRangeSelector(navController: NavController, date: LocalDate, valueKey: String, minDate: LocalDate?, maxDate: LocalDate?) {
        navController.navigate(MainGraphDirections.openDocumentsDateRangeSelector(date, valueKey, minDate, maxDate))
    }
}
