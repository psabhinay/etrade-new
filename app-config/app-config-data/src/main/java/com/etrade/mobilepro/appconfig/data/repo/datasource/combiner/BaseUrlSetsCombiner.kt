package com.etrade.mobilepro.appconfig.data.repo.datasource.combiner

import com.etrade.mobilepro.appconfig.api.BaseUrls

/**
 * Combines base url sets into one.
 * Sometimes backend-originated baseUrls are not full
 */

interface BaseUrlSetsCombiner {
    /**
     * @param incomingBaseUrls BaseUrls coming from storage or network. Can be not full
     * @param defaultBaseUrls Default baseUrls, source for missing fields of incomingBaseUrls
     *
     */
    fun combine(incomingBaseUrls: BaseUrls, defaultBaseUrls: BaseUrls): BaseUrls
}
