package com.etrade.mobilepro.pushnotification.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.pushnotification.api.AlertPushSettings
import com.etrade.mobilepro.pushnotification.api.repo.AlertPushSettingsRepo
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class NotificationOptInViewModel @Inject constructor(
    private val alertPushSettingsRepo: AlertPushSettingsRepo
) : ViewModel() {

    private val _viewState = MutableLiveData<ViewState>()
    internal val viewState: LiveData<ViewState>
        get() = _viewState
    private val logger: Logger by lazy { LoggerFactory.getLogger(NotificationOptInViewModel::class.java) }

    fun setAlerts(stockAlerts: Boolean, accountAlerts: Boolean) {
        if (viewState.value != ViewState.Loading) {
            viewModelScope.launch {
                _viewState.value = ViewState.Loading
                val alertPushSettings = AlertPushSettings(stockAlerts, accountAlerts)
                alertPushSettingsRepo
                    .setAlertPushSettings(alertPushSettings)
                    .onSuccess {
                        _viewState.value = ViewState.Success
                    }
                    .onFailure {
                        logger.error("Error while setting push notification alerts", it)
                        _viewState.value = ViewState.Failure
                    }
            }
        }
    }
}

internal sealed class ViewState {
    object Loading : ViewState()
    object Success : ViewState()
    object Failure : ViewState()
}
