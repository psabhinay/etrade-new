package com.etrade.mobilepro.login.presentation

import android.content.Context
import androidx.core.content.ContextCompat
import com.etrade.mobilepro.login.api.repo.LoginDataSource
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.login.presentation.cta.LoginCtaAction
import com.etrade.mobilepro.login.presentation.cta.LoginCtaItem
import com.etrade.mobilepro.login.presentation.cta.LoginViewCtaListener
import com.etrade.mobilepro.login.presentation.cta.LoginViewProvider
import com.etrade.mobilepro.login.presentation.router.LoginAuxRouter
import com.etrade.mobilepro.util.android.extension.startDialerActivity
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import org.chromium.customtabsclient.shared.WebViewHelper
import javax.inject.Inject

class DefaultLoginCtaViewProvider @Inject constructor(
    private val context: Context,
    private val dataSource: LoginDataSource,
    private val webViewHelper: WebViewHelper,
    private val loginPreferences: LoginPreferencesRepo,
    private val auxRouter: LoginAuxRouter
) : LoginViewCtaListener, LoginViewProvider {

    override fun onCtaEvent(actionItem: LoginCtaItem, snackBarUtil: () -> SnackBarUtil) {
        when (actionItem.action) {
            LoginCtaAction.SIGN_UP -> onSignUp()
            LoginCtaAction.FORGOT_PASSWORD -> onForgotPassword()
            LoginCtaAction.FORGOT_USERNAME -> onForgotUsername()
            LoginCtaAction.CHECK_BOX_REMEMBER_USERNAME -> {
                setRememberUserNameEnabled(actionItem.isChecked ?: false)
            }
            LoginCtaAction.CHECK_BOX_USE_CODE -> {
                setUseSecurityCodeEnabled(actionItem.isChecked ?: false)
            }
            LoginCtaAction.CALL_ETRADE_SUPPORT -> launchDialer(snackBarUtil)
            LoginCtaAction.PRIVACY_POLICY -> onPrivacyPolicy()
            LoginCtaAction.VIEW_EULA -> auxRouter.navigateToEula(context)
        }
    }

    private fun launchDialer(snackBarUtil: () -> SnackBarUtil) {
        context.startDialerActivity(context.getString(R.string.support_phone_number)) {
            snackBarUtil().errorSnackbar()
        }
    }

    private val linkColor: Int by lazy {
        ContextCompat.getColor(context, R.color.plum)
    }

    override fun isRememberUserNameEnabled(): Boolean = loginPreferences.isRememberUserNameEnabled

    override fun isUseSecurityCodeEnabled(): Boolean = loginPreferences.isSecurityCodeEnabled

    override suspend fun getUserName() = loginPreferences.getUserName()

    override suspend fun setUserName(name: String) {
        loginPreferences.setUserName(name)
    }

    private fun onSignUp() {
        launchUrlInChromeTab(
            dataSource.getUrl(LoginDataSource.ExternalLocation.SIGN_UP), linkColor
        )
    }

    private fun onForgotUsername() {
        launchUrlInChromeTab(
            dataSource.getUrl(LoginDataSource.ExternalLocation.FORGOT_USERNAME),
            linkColor
        )
    }

    private fun onForgotPassword() {
        launchUrlInChromeTab(
            dataSource.getUrl(LoginDataSource.ExternalLocation.FORGOT_PWD),
            linkColor
        )
    }

    private fun onPrivacyPolicy() {
        launchUrlInChromeTab(
            dataSource.getUrl(LoginDataSource.ExternalLocation.PRIVACY_POLICY),
            linkColor
        )
    }

    private fun setRememberUserNameEnabled(enabled: Boolean) {
        loginPreferences.isRememberUserNameEnabled = enabled
    }

    private fun setUseSecurityCodeEnabled(enabled: Boolean) {
        loginPreferences.isSecurityCodeEnabled = enabled
    }

    private fun launchUrlInChromeTab(url: String, colorToolbar: Int) {
        webViewHelper.openCustomTab(url, colorToolbar)
    }
}
