package com.etrade.mobilepro.chart.rest

import com.etrade.mobilepro.chart.dto.AggregationChartRequestDto
import com.etrade.mobilepro.chart.dto.AggregationChartResponseDto
import com.etrade.mobilepro.chart.dto.ChartDataDto
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface PchartApiClient {
    @JsonMoshi
    @GET("/phx/pchart/{endpointType}/graphs/{chartRequest}.txt")
    fun getChart(
        @Path("endpointType") endpointType: String,
        @Path("chartRequest") chartRequest: String
    ): Single<ChartDataDto>

    @JsonMoshi
    @POST("/phx/pchart/{endpointType}/webapiagg/aggregator")
    fun getAggregationChart(
        @Path("endpointType") endpointType: String,
        @JsonMoshi @Body requestBody: AggregationChartRequestDto
    ): Single<AggregationChartResponseDto>
}
