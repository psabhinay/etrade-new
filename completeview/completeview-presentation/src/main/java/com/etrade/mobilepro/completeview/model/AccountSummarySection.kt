package com.etrade.mobilepro.completeview.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.livedata.SingleSourceLiveDataDelegate
import com.etrade.mobilepro.streaming.api.StreamingText

class AccountSummarySection(initialTitle: String, valueSource: LiveData<StreamingText>) {

    private val _title = MutableLiveData(initialTitle)
    private val valueDelegate =
        SingleSourceLiveDataDelegate(valueSource)

    val title: LiveData<String>
        get() = _title
    val value: LiveData<StreamingText>
        get() = valueDelegate.value

    fun update(title: String?, textSource: LiveData<StreamingText>) {
        title?.let { _title.value = it }
        valueDelegate.updateSource(textSource)
    }
}
