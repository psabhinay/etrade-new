package com.etrade.mobilepro.messagecenter.api.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.messagecenter.api.model.ReferenceBook

interface MessageCenterReferenceBookDataSource {
    suspend fun fetchReferenceBook(): ETResult<ReferenceBook>
    fun getCachedReferenceBook(): ReferenceBook?
    fun clearCache()
}
