package com.etrade.mobilepro.mod.auth

import com.etrade.mobilepro.encryption.api.Base64Processor
import com.etrade.mobilepro.encryption.api.CipherAuthenticator
import com.etrade.mobilepro.encryption.api.EncryptionProvider
import com.etrade.mobilepro.encryption.api.SecretKeyMaterial
import com.etrade.mobilepro.encryption.api.SecretKeyRepo
import com.etrade.mobilepro.encryption.api.SymmetricAlgorithmSpec
import com.etrade.mobilepro.encryption.data.AesSymmetricAlgorithmSpec
import com.etrade.mobilepro.encryption.data.DefaultEncryptionProvider
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.suspendCancellableCoroutine
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.security.MessageDigest
import java.util.Base64
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import kotlin.coroutines.resume

private const val CIPHERTEXT = "EWrnryH1da8j7yU8.Qm97jllwK5ut6WuFONUxnrMLu05slQ0q/X17M6t63w=="
private const val PLAINTEXT = "raw_text_string"
private const val TEST_SECRET_KEY = "==?=?=@@@@@@####¢¢#¢#¢"
private const val DIGEST_ALGORITHM = "SHA-1"
private const val SECRET_KEY_ALGORITHM = "AES"
private const val AES_KEY_LENGTH = 16

private const val DESIRED_ENCRYPTION_COUNT = 5

internal class DefaultEncryptionProviderTest {

    private val algorithmSpec: SymmetricAlgorithmSpec = AesSymmetricAlgorithmSpec()

    private val secretKeyRepo: SecretKeyRepo = mock {
        on { algorithmSpec } doReturn algorithmSpec
        on { commonKey } doReturn getKey(isUserAuthenticationRequired = false)
        on { secureKey } doReturn getKey(isUserAuthenticationRequired = true)
    }

    private val base64Processor: Base64Processor = object : Base64Processor {
        override fun encode(input: ByteArray): String {
            return Base64.getEncoder().encodeToString(input)
        }

        override fun decode(input: String): ByteArray {
            return Base64.getDecoder().decode(input)
        }
    }

    @Test
    fun `encrypted text is successfully decrypted`() {
        testEncryptionProvider { encryptionProvider ->
            val encryptedInput = encryptionProvider.encrypt(PLAINTEXT, secretKeyRepo.commonKey) { it }
            val decryptedInput = encryptionProvider.decrypt(encryptedInput, secretKeyRepo.commonKey) { it }
            assertEquals(PLAINTEXT, decryptedInput)
        }
    }

    @Test
    fun `encrypt multiple inputs succeeds`() {
        testEncryptionProvider { encryptionProvider ->
            for (i in 0..DESIRED_ENCRYPTION_COUNT) {
                assertNotNull(encryptionProvider.encrypt("foo_$i", secretKeyRepo.commonKey) { it })
            }
        }
    }

    @Test
    fun `check encryption use of authenticator`() {
        testEncryptionProvider { provider ->
            var authenticatedTimes = 0
            val authenticate: suspend (Cipher) -> Cipher = { cipher ->
                suspendCancellableCoroutine {
                    authenticatedTimes++
                    it.resume(cipher)
                }
            }

            val encrypted = provider.encrypt(PLAINTEXT, secretKeyRepo.secureKey, authenticate)
            val decrypted = provider.decrypt(encrypted, secretKeyRepo.secureKey, authenticate)
            assertEquals(2, authenticatedTimes)
            assertEquals(PLAINTEXT, decrypted)
        }
    }

    @Test
    fun `check cancelling of operation when authentication is canceled`() {
        checkThrowsException<CancellationException>(
            operation = { authenticate: CipherAuthenticator -> encrypt(PLAINTEXT, secretKeyRepo.secureKey, authenticate) },
            proceed = { cancel() }
        )
        checkThrowsException<CancellationException>(
            operation = { authenticate: CipherAuthenticator -> decrypt(CIPHERTEXT, secretKeyRepo.secureKey, authenticate) },
            proceed = { cancel() }
        )
    }

    @Test
    fun `check authentication rethrows custom exception`() {
        checkThrowsException<CustomException>(
            operation = { authenticate: CipherAuthenticator -> encrypt(PLAINTEXT, secretKeyRepo.secureKey, authenticate) },
            proceed = { cancel(CustomException()) }
        )
        checkThrowsException<CustomException>(
            operation = { authenticate: CipherAuthenticator -> decrypt(CIPHERTEXT, secretKeyRepo.secureKey, authenticate) },
            proceed = { cancel(CustomException()) }
        )
    }

    @Test
    fun `check hash function returns same result`() {
        testEncryptionProvider { encryptionProvider ->
            assertEquals("47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU=", encryptionProvider.hash(""))
            assertEquals("tU5Pb5cDCXZP40j8JEmT+8deyc8AVe+X9t5gN6pbkDE=", encryptionProvider.hash("Text$123"))
        }
    }

    private inline fun <reified T : Throwable> checkThrowsException(
        crossinline operation: suspend EncryptionProvider.(authenticate: CipherAuthenticator) -> String,
        crossinline proceed: CancellableContinuation<Cipher>.() -> Unit
    ) {
        assertThrows<T> {
            testEncryptionProvider { provider ->
                provider.operation {
                    suspendCancellableCoroutine {
                        it.proceed()
                    }
                }
            }
        }
    }

    private fun testEncryptionProvider(test: suspend (EncryptionProvider) -> Unit) {
        val encryptionProvider = DefaultEncryptionProvider(secretKeyRepo, base64Processor)
        runBlocking {
            test(encryptionProvider)
        }
    }

    private fun getKey(isUserAuthenticationRequired: Boolean): SecretKeyMaterial {
        val sha = MessageDigest.getInstance(DIGEST_ALGORITHM)
        val key = sha.digest(TEST_SECRET_KEY.toByteArray()).copyOf(AES_KEY_LENGTH)

        return SecretKeyMaterial(SecretKeySpec(key, SECRET_KEY_ALGORITHM), isUserAuthenticationRequired)
    }

    private class CustomException : Exception()
}
