package com.etrade.mobilepro.quote.screen.presentation.fees

import android.content.res.Resources
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.quoteapi.GetQuoteParameter
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuoteUseCase
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.invoke
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

class FundFeesViewViewModel @AssistedInject constructor(
    private val resources: Resources,
    private val getQuoteUseCase: GetMobileQuoteUseCase,
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase,
    private val fundQuoteDetailItemsMapper: FundQuoteDetailItemsMapper,
    @Assisted private val symbol: SearchResultItem.Symbol,
    @Assisted private val mobileQuoteStateFlow: StateFlow<MobileQuote>?
) : ViewModel(), LifecycleObserver {

    private val _feesAndExpenseData = MutableLiveData<Resource<FeeAndExpenseData>>()
    val feesAndExpenseLiveData: LiveData<Resource<FeeAndExpenseData>> = _feesAndExpenseData

    private val logger = LoggerFactory.getLogger(this.javaClass)

    fun configureFeeData() {
        if (mobileQuoteStateFlow == null) {
            fetchFees()
        } else {
            viewModelScope.launch {
                mobileQuoteStateFlow.collect {
                    if (it.symbol.isNotBlank()) {
                        onQuoteSuccess(it)
                    }
                }
            }
        }
    }

    private fun fetchFees() {
        viewModelScope.launch {
            getQuoteUseCase(GetQuoteParameter(symbol, isExtendedHoursOnUseCase()))
                .onSuccess {
                    onQuoteSuccess(it)
                }
                .onFailure { error ->
                    onQuoteFetchFailed(error)
                }
        }
    }

    private fun onQuoteSuccess(quoteData: MobileQuote) {
        val fundFeesAndExpensesItem = fundQuoteDetailItemsMapper.getFeesAndExpensesItem(quoteData.fundDetails)
        val result = getSections(fundFeesAndExpensesItem, resources)
        _feesAndExpenseData.postValue(Resource.Success(FeeAndExpenseData(result)))
    }

    private fun onQuoteFetchFailed(error: Throwable) {
        logger.error("Unable to fetch quote", error)
        _feesAndExpenseData.postValue(Resource.Failed(error = error))
    }

    fun configureSymbol() {
        if (!symbol.isEmpty()) {
            fetchFees()
        }
    }

    @AssistedFactory
    interface Factory {
        fun create(
            symbol: SearchResultItem.Symbol,
            mobileQuoteStateFlow: StateFlow<MobileQuote>?
        ): FundFeesViewViewModel
    }
}
