package com.etrade.mobilepro.login.usecase

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.login.api.repo.LoginRepo
import com.etrade.mobilepro.login.api.repo.LoginSuccess
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class LoginUseCaseTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val accountListUseCase: AccountListUseCase = mock()
    private val loginRepo: LoginRepo = mock()
    private val loginPreferences: LoginPreferencesRepo = mock()
    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    private lateinit var sut: LogInUseCaseImp

    @Before
    fun setup() {
        Dispatchers.setMain(testCoroutineDispatcher)
        MockitoAnnotations.initMocks(this)
        runBlockingTest {
            whenever(accountListUseCase.execute(any())).thenReturn(true)
            whenever(loginRepo.login(TEST_USER_NAME, TEST_PASSWORD)).thenReturn(
                ETResult.success(LoginSuccess(TEST_USER_ID, isInternationalUser = false, isRsaUser = false, isRealTimeUser = false))
            )
            whenever(loginPreferences.getUserName()).thenReturn(ETResult.success(""), ETResult.success(TEST_USER_NAME))
            whenever(loginPreferences.isLoginForSameUser).thenReturn(false, true)
        }

        sut = LogInUseCaseImp(
            loginRepo = loginRepo,
            accountListUseCase = accountListUseCase,
            loginResultUseCase = mock(),
            loginPreferences = loginPreferences,
            cacheClearHandler = mock()
        )
    }

    @After
    fun cleanup() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `login should return success`() {
        runBlockingTest {
            val expected = LoginSuccess(TEST_USER_ID, isInternationalUser = false, isRsaUser = false, isRealTimeUser = false)
            val actual = sut.execute(
                LoginRequest(
                    username = TEST_USER_NAME,
                    password = TEST_PASSWORD,
                    shouldHandleLanding = false,
                    shouldOfferBiometric = false,
                    authenticator = mock()
                )
            )
            verify(loginRepo).login(TEST_USER_NAME, TEST_PASSWORD)
            assert(expected == actual)
        }
    }

    @Test
    fun `second login for same user should mark login for same user`() {
        runBlockingTest {
            val firstExpected = LoginSuccess(TEST_USER_ID, isInternationalUser = false, isRsaUser = false, isRealTimeUser = false)
            val secondExpected = LoginSuccess(TEST_USER_ID, isInternationalUser = false, isRsaUser = false, isRealTimeUser = false)

            val firstLogin = sut.execute(
                LoginRequest(
                    username = TEST_USER_NAME,
                    password = TEST_PASSWORD,
                    shouldHandleLanding = false,
                    shouldOfferBiometric = false,
                    authenticator = mock()
                )
            )
            assertFalse(sut.isLoginForPreviousUser)

            val secondLogin = sut.execute(
                LoginRequest(
                    username = TEST_USER_NAME,
                    password = TEST_PASSWORD,
                    shouldHandleLanding = false,
                    shouldOfferBiometric = false,
                    authenticator = mock()
                )
            )
            verify(loginRepo, times(2)).login(TEST_USER_NAME, TEST_PASSWORD)
            assertTrue(sut.isLoginForPreviousUser)

            assertEquals(firstExpected, firstLogin)
            assertEquals(secondExpected, secondLogin)
        }
    }

    companion object {
        private const val TEST_USER_ID = "12345678"
        private const val TEST_USER_NAME = "testUserName"
        private const val TEST_PASSWORD = "Password"
    }
}
