package com.etrade.mobilepro.viewdelegate

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.viewmodel.NavigationSignal
import com.etrade.mobilepro.viewmodel.NavigationSignalViewModel

class NavigationSignalViewDelegate(
    private val navigationSignalViewModel: NavigationSignalViewModel,
    private val findNavController: () -> NavController
) : ViewDelegate {

    override fun observe(owner: LifecycleOwner) {
        navigationSignalViewModel.navigationSignal.observe(owner, Observer { onNavigationSignal(it) })
    }

    private fun onNavigationSignal(event: ConsumableLiveEvent<NavigationSignal>) {
        event.consume {
            it(findNavController())
        }
    }
}
