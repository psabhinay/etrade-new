package com.etrade.mobilepro.login.widget

import android.app.Dialog
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.etrade.mobilepro.login.api.repo.LoginResultCode
import com.etrade.mobilepro.login.api.repo.LoginResultCode.ACCESS_DENIED
import com.etrade.mobilepro.login.api.repo.LoginResultCode.CAP1_CONFLICTED_USER
import com.etrade.mobilepro.login.api.repo.LoginResultCode.INVALID_CREDENTIALS
import com.etrade.mobilepro.login.api.repo.LoginResultCode.LOCKED_OUT
import com.etrade.mobilepro.login.api.repo.LoginResultCode.PASSWORD_EXPIRED
import com.etrade.mobilepro.login.api.repo.LoginResultCode.WARNING
import com.etrade.mobilepro.login.api.repo.LoginResultCode.WRONG_PASSWORD
import com.etrade.mobilepro.login.widget.LoginErrorCtaAction.CALL_ETRADE
import com.etrade.mobilepro.login.widget.LoginErrorCtaAction.DISMISS_ERROR_DIALOG
import com.etrade.mobilepro.login.widget.LoginErrorCtaAction.DISMISS_WARNING_DIALOG
import com.etrade.mobilepro.login.widget.LoginErrorCtaAction.RECOVER_USERNAME
import com.etrade.mobilepro.login.widget.LoginErrorCtaAction.RESET_PASSWORD
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.parcelize.Parcelize

class LoginResultDialogFragment :
    DialogFragment(), LoginResultDialog {
    override var onButtonClickListener: ((LoginErrorCtaAction) -> Unit)? = null

    private val message: String?
        get() = arguments?.getString(KEY_MESSAGE, null)

    private val resultData: LoginResultCodeData
        get() = arguments?.getParcelable(KEY_CODE) ?: LoginResultCodeData()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return requireContext().let { ctx ->
            isCancelable = false
            MaterialAlertDialogBuilder(ctx)
                .defaultBaseErrorDialog(message)
                .updatedErrorDialog(resultData.loginResultCode) {
                    onButtonClickListener?.invoke(it)
                }
                .create()
        }
    }

    companion object {

        private const val TAG = "LoginResultDialog"
        private const val KEY_MESSAGE = "message"
        private const val KEY_CODE = "code"

        fun show(
            fragmentManager: FragmentManager,
            message: String,
            loginResultCode: LoginResultCode
        ): LoginResultDialog? {

            dismiss(fragmentManager)
            return LoginResultDialogFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY_MESSAGE, message)
                    putParcelable(
                        KEY_CODE,
                        LoginResultCodeData(loginResultCode)
                    )
                }
                show(fragmentManager, TAG)
            }
        }

        /**
         * Restores a dialog from [fragmentManager] if it is present.
         *
         * @param fragmentManager fragment manager to look into
         *
         * @return a dialog if available
         */
        fun restore(fragmentManager: FragmentManager): LoginResultDialog? {
            return fragmentManager.findFragmentByTag(TAG) as? LoginResultDialogFragment
        }

        /**
         * Dismisses a dialog if it is showing, but may lose it's state if a state of [fragmentManager] is saved.
         *
         * @param fragmentManager fragment manager that may host a dialog
         */
        fun dismiss(fragmentManager: FragmentManager) {
            if (fragmentManager.isStateSaved) {
                return
            }
            (fragmentManager.findFragmentByTag(TAG) as? LoginResultDialogFragment)?.dismiss()
        }
    }
}

private fun MaterialAlertDialogBuilder.defaultBaseErrorDialog(message: String?): MaterialAlertDialogBuilder {
    return this.setMessage(message)
        .setTitle(R.string.title_error)
        .setIcon(R.drawable.ic_error_red_24px)
}

/**
 * This method adds or updates title, click listeners based on Error code
 */
private fun MaterialAlertDialogBuilder.updatedErrorDialog(
    loginResultCode: LoginResultCode,
    clickHandler: (LoginErrorCtaAction) -> Unit
): MaterialAlertDialogBuilder {

    return when (loginResultCode) {

        ACCESS_DENIED, CAP1_CONFLICTED_USER -> {
            this.setTitle(R.string.login_error_ui_title_call_us)
            this.setPositiveButton(R.string.ok) { _, _ -> clickHandler(CALL_ETRADE) }
            this.setNegativeButton(R.string.cancel) { _, _ -> clickHandler(DISMISS_ERROR_DIALOG) }
        }

        INVALID_CREDENTIALS, WRONG_PASSWORD -> {
            this.setPositiveButton(R.string.login_error_ui_recover_user_id) { _, _ -> clickHandler(RECOVER_USERNAME) }
            this.setNegativeButton(R.string.login_error_ui_reset_password) { _, _ -> clickHandler(RESET_PASSWORD) }
            this.setNeutralButton(R.string.ok) { _, _ -> clickHandler(DISMISS_ERROR_DIALOG) }
        }

        LOCKED_OUT, PASSWORD_EXPIRED -> {
            this.setPositiveButton(R.string.login_error_ui_reset_password) { _, _ -> clickHandler(RESET_PASSWORD) }
            this.setNegativeButton(R.string.cancel) { _, _ -> clickHandler(DISMISS_ERROR_DIALOG) }
        }

        WARNING -> {
            setIcon(R.drawable.ic_warning_yellow_24px)
            setTitle(R.string.title_warning)
            this.setPositiveButton(R.string.ok) { _, _ -> clickHandler(DISMISS_WARNING_DIALOG) }
        }

        // for all other cases default to Error Dialog
        else -> this.apply {
            setPositiveButton(R.string.ok) { _, _ -> clickHandler.invoke(DISMISS_ERROR_DIALOG) }
        }
    }
}

@Parcelize
private data class LoginResultCodeData(val loginResultCode: LoginResultCode = WARNING) : Parcelable
