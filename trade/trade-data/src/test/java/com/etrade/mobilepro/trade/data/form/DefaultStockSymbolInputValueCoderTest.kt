package com.etrade.mobilepro.trade.data.form

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class DefaultStockSymbolInputValueCoderTest {

    private val testCases = listOf(
        Symbol("AAPL", InstrumentType.EQ),
        Symbol("AAPL--200501C00275000", InstrumentType.OPTNC),
        Symbol("AAAAX", InstrumentType.MF)
    )

    @Test
    fun `check coder full format`() {
        testCases.forEach { expected ->
            val actual = DefaultSymbolInputValueCoder.decode(DefaultSymbolInputValueCoder.encode(expected))
            assertEquals(expected, actual)
        }
    }

    @Test
    fun `check decoding of partial value`() {
        testCases.forEach { symbol ->
            val actual = DefaultSymbolInputValueCoder.decode(symbol.symbol)
            assertEquals(symbol.copy(instrumentType = InstrumentType.UNKNOWN), actual)
        }
    }

    @Test
    fun `check decoding of empty value`() {
        assertNull(DefaultSymbolInputValueCoder.decode(""))
        assertNull(DefaultSymbolInputValueCoder.decode(" "))
        assertNull(DefaultSymbolInputValueCoder.decode(" \n "))
    }
}
