package com.etrade.mobilepro.debugmenu.data.rest

import com.etrade.eo.rest.retrofit.Xml
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

internal interface DebugMenuApiClient {
    @Xml
    @GET("/etmobile/env.xml")
    suspend fun requestAppEnvironmentEndpoints(@Header("Authorization") credentials: String): Response<EnvDto>
}
