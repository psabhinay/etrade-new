package com.etrade.mobilepro.common

import android.content.Intent
import androidx.fragment.app.FragmentActivity

interface DisclosureOverlayHandler {
    fun getDisclosureOverlayIntent(context: FragmentActivity?): Intent?
}
