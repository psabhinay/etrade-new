package com.etrade.mobilepro.balance.data.rest

import com.etrade.eo.rest.retrofit.Scalar
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

const val BALANCE_PATH = "/e/t/mobile/MobileAccountBalances"
interface BalancesService {
    @Scalar
    @FormUrlEncoded
    @POST(BALANCE_PATH)
    fun getBalances(@Field("UserId") userId: String, @Field("AccountId") accountId: String): Single<String>
}
