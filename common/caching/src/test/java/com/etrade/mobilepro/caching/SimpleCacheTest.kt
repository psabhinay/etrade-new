package com.etrade.mobilepro.caching

import com.etrade.mobilepro.caching.coroutine.NO_EXPIRATION
import com.etrade.mobilepro.caching.coroutine.SimpleCache
import com.etrade.mobilepro.caching.coroutine.toDataKey
import com.etrade.mobilepro.caching.coroutine.toTimestampKey
import com.etrade.mobilepro.util.KeyValueStore
import com.etrade.mobilepro.util.Serializer
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doNothing
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.stub
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.concurrent.TimeUnit

private const val SERIALIZED = "1"
private const val RAW = 1
private const val KEY = "key"
private const val EXPIRATION_TIME = 50L

@ExperimentalCoroutinesApi
class SimpleCacheTest {

    @Mock
    private lateinit var serializer: Serializer<Int>
    @Mock
    private lateinit var store: KeyValueStore

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        serializer.stub {
            onGeneric { serialize(RAW) } doReturn(SERIALIZED)
            onGeneric { deserialize(SERIALIZED) } doReturn(RAW)
        }
    }

    @Test
    fun `non-cached resource returns failure with NoSuchElement exception`() {
        store.stub {
            on { getString(KEY, null) } doReturn null
        }
        val sut = SimpleCache(EXPIRATION_TIME, serializer, store)

        runBlockingTest {
            val result = sut.get(SERIALIZED)
            assert(result.isFailure)
            assert(result.exceptionOrNull() is NoSuchElementException)
        }
    }

    @Test
    fun `cached resource is returned`() {
        store.stub {
            on { getString(anyOrNull(), eq(null)) } doReturn SERIALIZED
        }
        val sut = SimpleCache(EXPIRATION_TIME, serializer, store)

        runBlockingTest {
            val result = sut.get(SERIALIZED)
            assert(result.isSuccess)
            assert(result.getOrNull() == RAW)
        }
    }

    @Test
    fun `cached resource is removed from the storage`() {
        doNothing().`when`(store).putString(anyOrNull(), eq(null))
        doReturn(null).`when`(store).getString(anyOrNull(), eq(null))

        store.stub {
            on { getString(KEY, null) } doReturn null
        }

        val sut = SimpleCache(NO_EXPIRATION, serializer, store)

        runBlockingTest {
            val removeResult = sut.remove(KEY)
            assert(removeResult.isSuccess)
            verify(store, times(2)).putString(anyOrNull(), eq(null))

            val getResult = sut.get(KEY)
            assert(getResult.isFailure)
            assert(getResult.exceptionOrNull() is NoSuchElementException)
        }
    }

    @Test
    fun `non-cached resource is put to cache and can be retrieved`() {
        store.stub {
            on { getString(KEY, null) } doReturn null
        }
        val sut = SimpleCache(EXPIRATION_TIME, serializer, store)

        runBlockingTest {
            sut.get(SERIALIZED).also { result ->
                assert(result.isFailure)
                assert(result.exceptionOrNull() is NoSuchElementException)
            }
            sut.put(RAW, KEY)
            store.stub {
                on { getString(anyOrNull(), eq(null)) } doReturn SERIALIZED
            }
            sut.get(SERIALIZED).also { result ->
                assert(result.isSuccess)
                assert(result.getOrNull() == RAW)
            }
        }
    }

    @Test
    fun `expired cached resource is treated as non-cached`() {
        store.stub {
            on { getString(KEY.toDataKey(), null) } doReturn SERIALIZED
            on { getString(KEY.toTimestampKey(), null) } doReturn (
                System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(100)
                ).toString()
        }
        val sut = SimpleCache(EXPIRATION_TIME, serializer, store)

        runBlockingTest {
            assert(!sut.isCached(KEY))
        }
    }
}
