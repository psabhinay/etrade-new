package com.etrade.mobilepro.uiwidgets.actionsheet

import androidx.lifecycle.LiveData

interface ActionSheetViewModel {

    val actions: LiveData<List<ActionItem>>

    fun selectAction(item: ActionItem)
    fun cancelActions()
}
