package com.etrade.mobilepro.formsandapplications.api

/**
 * Describes a displayable document.
 *
 * @param title reference title
 * @param shortTitle short title for reference
 * @param url url to open in WebView
 */
data class FormsAndApplicationsReference(val title: Int, val shortTitle: Int, val url: String, val analyticsTitle: String? = null)
