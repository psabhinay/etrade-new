package com.etrade.mobilepro.messagecenter.data.repo

import com.etrade.mobilepro.messagecenter.api.model.Message
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.threeten.bp.Instant

class DefaultMessageListReducerTest {
    @Test
    fun `reducer should remove previous messages with the same caseNo and same and older date`() {
        val sut = DefaultMessageListReducer()
        val input = listOf(testMessage2CaseNo1, testMessage2CaseNo1, testMessage1CaseNo1Newest, testMessage3)

        val output = sut.reduceMessageList(input)

        assertEquals(2, output.size)
        assertEquals(testMessage1CaseNo1Newest, output[0])
        assertEquals(testMessage3, output[1])
    }

    @Test
    fun `reducer should do sorting by createdAt (descending)`() {
        val sut = DefaultMessageListReducer()
        val input = listOf(testMessage2CaseNo1, testMessage2CaseNo1, testMessage1CaseNo1Newest, testMessage3)

        val output = sut.reduceMessageList(input)

        assertEquals(2, output.size)
        assertEquals(testMessage1CaseNo1Newest, output[0])
        assertEquals(testMessage3, output[1])

        val input2 = listOf(testMessage2CaseNo1, testMessage3, testMessage2CaseNo1, testMessage1CaseNo1Newest, testMessage4Oldest)

        val output2 = sut.reduceMessageList(input2)

        assertEquals(3, output2.size)
        assertEquals(testMessage1CaseNo1Newest, output2[0])
        assertEquals(testMessage3, output2[1])
        assertEquals(testMessage4Oldest, output2[2])
    }

    @Test
    fun `Order in which messages come does not matter`() {
        val sut = DefaultMessageListReducer()
        val input1 = listOf(testMessage1CaseNo1Newest, testMessage2CaseNo1, testMessage4CaseNo1Oldest)
        val input2 = listOf(testMessage4CaseNo1Oldest, testMessage1CaseNo1Newest, testMessage2CaseNo1)
        val input3 = listOf(testMessage2CaseNo1, testMessage4CaseNo1Oldest, testMessage1CaseNo1Newest)

        val output1 = sut.reduceMessageList(input1)
        val output2 = sut.reduceMessageList(input2)
        val output3 = sut.reduceMessageList(input3)

        assertEquals(1, output1.size)
        assertEquals(1, output2.size)
        assertEquals(1, output3.size)

        assertEquals(testMessage1CaseNo1Newest, output1[0])
        assertEquals(testMessage1CaseNo1Newest, output2[0])
        assertEquals(testMessage1CaseNo1Newest, output3[0])
    }

    private val testMessage1CaseNo1Newest = Message(
        id = "id1",
        accountId = "",
        accountShortName = "short name",
        descId = "",
        subjectId = "",
        caseTypeId = "",
        caseTypeDesc = "",
        caseNo = "caseNo1",
        isUnread = false,
        status = "",
        createdAt = Instant.parse("2017-12-03T10:15:30.000Z"),
        createdAtFormatted = "",
        readAt = null
    )
    private val testMessage2CaseNo1 = Message(
        id = "id2",
        accountId = "",
        accountShortName = "short name",
        descId = "",
        subjectId = "",
        caseTypeId = "",
        caseTypeDesc = "",
        caseNo = "caseNo1",
        isUnread = false,
        status = "",
        createdAt = Instant.parse("2008-12-03T10:15:30.000Z"),
        createdAtFormatted = "",
        readAt = null
    )
    private val testMessage4CaseNo1Oldest = Message(
        id = "id2",
        accountId = "",
        accountShortName = "short name",
        descId = "",
        subjectId = "",
        caseTypeId = "",
        caseTypeDesc = "",
        caseNo = "caseNo1",
        isUnread = false,
        status = "",
        createdAt = Instant.parse("2008-11-02T10:15:30.000Z"),
        createdAtFormatted = "",
        readAt = null
    )
    private val testMessage3 = Message(
        id = "id3",
        accountId = "",
        accountShortName = "short name",
        descId = "",
        subjectId = "",
        caseTypeId = "",
        caseTypeDesc = "",
        caseNo = "caseNo2",
        isUnread = false,
        status = "",
        createdAt = Instant.parse("2006-12-03T10:15:30.000Z"),
        createdAtFormatted = "",
        readAt = null
    )

    private val testMessage4Oldest = Message(
        id = "id4",
        accountId = "",
        accountShortName = "short name",
        descId = "",
        subjectId = "",
        caseTypeId = "",
        caseTypeDesc = "",
        caseNo = "caseNo3",
        isUnread = false,
        status = "",
        createdAt = Instant.parse("2005-12-03T10:15:30.000Z"),
        createdAtFormatted = "",
        readAt = null
    )
}
