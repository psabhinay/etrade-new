package com.etrade.mobilepro.news.presentation.portfolio

import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.api.repo.NewsRepo
import com.etrade.mobilepro.news.presentation.base.BaseNewsViewModel
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentLoader
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsFontAdjustHandler
import com.etrade.mobilepro.news.presentation.filter.portfolio.PortfolioFilterItem
import com.etrade.mobilepro.tracking.SCREEN_TITLE_PORTFOLIO_NEWS
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.util.android.recyclerviewutil.SelectionTracker
import com.etrade.mobilepro.util.getStockPlanSymbolFromTitle
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

class PortfolioNewsViewModel @Inject constructor(
    newsRepo: NewsRepo,
    statusRepo: ReadStatusRepo,
    contentLoader: TextNewsContentLoader,
    fontAdjustHandler: TextNewsFontAdjustHandler,
    selectionTracker: SelectionTracker
) : BaseNewsViewModel(newsRepo, statusRepo, contentLoader, fontAdjustHandler, selectionTracker), ScreenViewModel {

    override val screenName: String = SCREEN_TITLE_PORTFOLIO_NEWS

    private var prevFilterItem: PortfolioFilterItem? = null
    override fun getNewsType(): NewsType = NewsType.PORTFOLIO

    @ExperimentalCoroutinesApi
    fun applyFilter(selectedFilterItem: PortfolioFilterItem) {
        if (selectedFilterItem == prevFilterItem) {
            return
        }
        this.prevFilterItem = selectedFilterItem
        val requestParams = if (selectedFilterItem.isStockPlan) {
            NewsRequest.Symbol(listOf(getStockPlanSymbolFromTitle(selectedFilterItem.title)))
        } else {
            NewsRequest.Portfolio(selectedFilterItem.filterResourceId, includeStockPlan = selectedFilterItem.isBaseAllPortfoliosItem)
        }
        refreshWithParams(requestParams)
    }
}
