package com.etrade.mobilepro.dagger

import androidx.navigation.NavController
import com.etrade.eo.netbidaskcalculator.DefaultNetBidAskCalculator
import com.etrade.eo.netbidaskcalculator.api.NetBidAskCalculator
import com.etrade.mobile.math.calculateGCD
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamic.form.fragment.createFormValuesArgument
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.quotelookup.SearchModule
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.DefaultTradeMutualFundRouter
import com.etrade.mobilepro.trade.DefaultTradeRouter
import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.trade.api.SaveOrderRepo
import com.etrade.mobilepro.trade.api.TradeAccountRepo
import com.etrade.mobilepro.trade.api.TradeRepo
import com.etrade.mobilepro.trade.api.option.OptionTradeRepo
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.approvallevel.ApprovalLevelFragmentDirections
import com.etrade.mobilepro.trade.approvallevel.router.ApprovalLevelRouter
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.api.ConditionalOrderDisclosureRepo
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.data.ConditionalOrderDisclosureRepoImpl
import com.etrade.mobilepro.trade.data.DefaultTradeFormParametersBuilder
import com.etrade.mobilepro.trade.data.DefaultTradeRepo
import com.etrade.mobilepro.trade.data.LocalTradeAccountRepo
import com.etrade.mobilepro.trade.data.form.DefaultSymbolInputValueCoder
import com.etrade.mobilepro.trade.data.form.DefaultTradeFormInputIdMap
import com.etrade.mobilepro.trade.data.form.TradeLegsCoder
import com.etrade.mobilepro.trade.data.form.input.DefaultConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultConditionalTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultMutualFundTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultStockTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultStopLossOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultStopLossStockTradeFormInputId
import com.etrade.mobilepro.trade.data.form.value.DefaultSecurityTypeValueId
import com.etrade.mobilepro.trade.data.option.DefaultOptionTradeRepo
import com.etrade.mobilepro.trade.data.saveorder.SaveOrderRepoImpl
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.form.api.TradeFormInputIdMap
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ConditionalTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.MutualFundTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.SecurityTypeValueId
import com.etrade.mobilepro.trade.mutualfund.api.MutualFundTradeRepo
import com.etrade.mobilepro.trade.mutualfund.data.DefaultMutualFundTradeRepo
import com.etrade.mobilepro.trade.optionlegselect.DefaultOptionTradeActionItemProvider
import com.etrade.mobilepro.trade.optionlegselect.DefaultStockTradeActionItemProvider
import com.etrade.mobilepro.trade.optionlegselect.GetOptionQuotesUseCase
import com.etrade.mobilepro.trade.optionlegselect.GetOptionQuotesUseCaseImpl
import com.etrade.mobilepro.trade.optionlegselect.OptionLegSelectFragmentDirections
import com.etrade.mobilepro.trade.optionlegselect.OptionTradeActionItemProvider
import com.etrade.mobilepro.trade.optionlegselect.StockTradeActionItemProvider
import com.etrade.mobilepro.trade.optionlegselect.TradeActionItemProvider
import com.etrade.mobilepro.trade.presentation.TradeFragmentDirections
import com.etrade.mobilepro.trade.presentation.form.ConditionalOrderFormFactory
import com.etrade.mobilepro.trade.presentation.form.StaticConditionalOrderFormFactory
import com.etrade.mobilepro.trade.presentation.form.StaticTradeFormFactory
import com.etrade.mobilepro.trade.presentation.form.TradeFormFactory
import com.etrade.mobilepro.trade.presentation.form.account.DefaultSelectedAccountDelegate
import com.etrade.mobilepro.trade.presentation.usecase.CreateConditionalTradeFormUseCase
import com.etrade.mobilepro.trade.presentation.usecase.CreateConditionalTradeFormUseCaseImpl
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormUseCase
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormUseCaseImpl
import com.etrade.mobilepro.trade.presentation.usecase.PreviewOrderUseCase
import com.etrade.mobilepro.trade.presentation.usecase.PreviewOrderUseCaseImpl
import com.etrade.mobilepro.trade.presentation.usecase.SaveOrderUseCase
import com.etrade.mobilepro.trade.presentation.usecase.SaveOrderUseCaseImpl
import com.etrade.mobilepro.trade.presentation.util.adapters.DefaultOrderAdapter
import com.etrade.mobilepro.trade.presentation.util.adapters.OrderAdapter
import com.etrade.mobilepro.trade.router.OptionTradeRouter
import com.etrade.mobilepro.trade.router.TradeMutualFundRouter
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.util.ValueCoder
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP EVERYTHING IN THIS FILE ORGANIZED ALPHABETICALLY */

@Suppress("TooManyFunctions")
@Module(
    includes = [
        ApplicationModule::class,
        NetworkModule::class,
        SearchModule::class
    ]
)
interface TradeModule {

    @Binds
    fun bindConditionalOrderDisclosureRepo(repo: ConditionalOrderDisclosureRepoImpl): ConditionalOrderDisclosureRepo

    @Binds
    @Singleton
    fun bindConditionalOrderFormFactory(factory: StaticConditionalOrderFormFactory): ConditionalOrderFormFactory

    @Binds
    @Singleton
    fun bindCreateConditionalOrderFormUseCase(useCase: CreateConditionalTradeFormUseCaseImpl): CreateConditionalTradeFormUseCase

    @Binds
    @Singleton
    fun bindCreateTradeFormUseCaseImpl(useCase: CreateTradeFormUseCaseImpl): CreateTradeFormUseCase

    @Binds
    @Singleton
    fun bindGetOptionQuotesUseCase(useCase: GetOptionQuotesUseCaseImpl): GetOptionQuotesUseCase

    @Binds
    @Singleton
    fun bindMutualFundTradeRepo(repo: DefaultMutualFundTradeRepo): MutualFundTradeRepo

    @Binds
    @Singleton
    fun bindOptionTradeRepo(repo: DefaultOptionTradeRepo): OptionTradeRepo

    @Binds
    @Singleton
    fun bindOrderAdapter(adapter: DefaultOrderAdapter): OrderAdapter

    @Binds
    @Singleton
    fun bindPreviewOrderUseCase(useCase: PreviewOrderUseCaseImpl): PreviewOrderUseCase

    @Binds
    @Singleton
    fun bindSaveOrderRepository(saveOrderRepo: SaveOrderRepoImpl): SaveOrderRepo

    @Binds
    @Singleton
    fun bindSaveOrderUseCase(useCase: SaveOrderUseCaseImpl): SaveOrderUseCase

    @Binds
    @Singleton
    fun bindTradeAccountRepo(repo: LocalTradeAccountRepo): TradeAccountRepo

    @Binds
    @Singleton
    fun bindTradeFormSelectedAccountIdFactory(factory: DefaultSelectedAccountDelegate.Factory): SelectedAccountDelegate.Factory

    @Binds
    @Singleton
    fun bindTradeFormFactory(factory: StaticTradeFormFactory): TradeFormFactory

    @Binds
    @Singleton
    fun bindTradeFormInputIdMap(map: DefaultTradeFormInputIdMap): TradeFormInputIdMap

    @Binds
    fun bindTradeFormParametersBuilder(builder: DefaultTradeFormParametersBuilder): TradeFormParametersBuilder

    @Binds
    @Singleton
    fun bindTradeLegsCoder(coder: TradeLegsCoder): ValueCoder<List<TradeLeg>>

    @Binds
    @Singleton
    fun bindTradeMutualFundRouter(router: DefaultTradeMutualFundRouter): TradeMutualFundRouter

    @Binds
    @Singleton
    fun bindTradeRepo(repo: DefaultTradeRepo): TradeRepo

    @Binds
    @Singleton
    fun bindTradeRouter(tradeRouterImpl: DefaultTradeRouter): TradeRouter

    companion object {

        @Provides
        @Singleton
        fun provideApprovalLevelRouter(): ApprovalLevelRouter {
            return object : ApprovalLevelRouter {
                override fun navigateToApprovalLevelFragment(navController: NavController, accountMode: AccountMode) {
                    navController.navigate(TradeFragmentDirections.actionTradeFragmentToApprovalLevelFragment(accountMode))
                }

                override fun navigateToNewMessage(navController: NavController) {
                    navController.navigate(ApprovalLevelFragmentDirections.actionApprovalLevelFragmentToNewMessageFragment())
                }
            }
        }

        @Provides
        fun provideConditionalOrderTradeFormInputId(): ConditionalOrderTradeFormInputId = DefaultConditionalOrderTradeFormInputId

        @Provides
        fun provideConditionalTradeFormInputId(): ConditionalTradeFormInputId = DefaultConditionalTradeFormInputId

        @Provides
        fun provideContingentStockTradeFormInputId(): ContingentStockTradeFormInputId = DefaultContingentStockTradeFormInputId

        @Provides
        fun provideContingentOptionTradeFormInputId(): ContingentOptionTradeFormInputId = DefaultContingentOptionTradeFormInputId

        @Provides
        fun provideMutualFundTradeFormInputId(): MutualFundTradeFormInputId = DefaultMutualFundTradeFormInputId

        @Provides
        @Singleton
        fun provideNetBidAskCalculator(): NetBidAskCalculator = DefaultNetBidAskCalculator {
            calculateGCD(*it)
        }

        @Provides
        fun provideOptionTradeFormInputId(): OptionTradeFormInputId = DefaultOptionTradeFormInputId

        @Provides
        @Singleton
        fun provideOptionTradeRouter(moshi: Moshi): OptionTradeRouter {
            return object : OptionTradeRouter {

                override val tradeFragmentId: Int = R.id.tradeFragment

                override fun openOptionDisclosures(navController: NavController) {
                    navController.navigate(OptionLegSelectFragmentDirections.actionOptionLegSelectFragmentToTradeOptionsChainDisclosureDialog())
                }

                override fun openOptionLegSelect(
                    navController: NavController,
                    inputValues: InputValuesSnapshot,
                    symbol: SearchResultItem.Symbol?,
                    transactionType: TransactionType?
                ) {
                    navController.navigate(
                        TradeFragmentDirections.actionTradeFragmentToOptionLegSelectFragment(
                            createFormValuesArgument(moshi, inputValues),
                            symbol,
                            transactionType ?: TransactionType.UNKNOWN
                        )
                    )
                }

                override fun openTradeFromLegSelect(navController: NavController, formMap: InputValuesSnapshot) {
                    navController.navigate(
                        OptionLegSelectFragmentDirections.actionOptionLegSelectFragmentToTradeFragment(createFormValuesArgument(moshi, formMap))
                    )
                }
            }
        }

        @Provides
        fun provideStockTradeFormInputId(): StockTradeFormInputId = DefaultStockTradeFormInputId

        @Provides
        fun provideStopLossStockTradeFormInputId(): StopLossStockTradeFormInputId = DefaultStopLossStockTradeFormInputId

        @Provides
        fun provideStopLossOptionTradeFormInputId(): StopLossOptionTradeFormInputId = DefaultStopLossOptionTradeFormInputId

        @Provides
        fun provideSymbolInfoCoder(): ValueCoder<WithSymbolInfo> = DefaultSymbolInputValueCoder

        @Provides
        fun provideTradeFormSecurityTypeValueId(): SecurityTypeValueId = DefaultSecurityTypeValueId

        @Provides
        @OptionTradeActionItemProvider
        fun provideOptionTradeActionItemProvider(): TradeActionItemProvider = DefaultOptionTradeActionItemProvider

        @Provides
        @StockTradeActionItemProvider
        fun provideStockTradeActionItemProvider(): TradeActionItemProvider = DefaultStockTradeActionItemProvider
    }
}
