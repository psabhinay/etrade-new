package com.etrade.mobilepro.bank

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.bank.BankHomeViewModel.ViewState
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.Resource.Failed
import com.etrade.mobilepro.util.Resource.Loading
import com.etrade.mobilepro.util.Resource.Success
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class BankHomeViewModelTest {
    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()
    private lateinit var bankHomeViewModel: BankHomeViewModel
    private val cachedDynamicScreenRepo = mock<DynamicScreenRepo>()

    @Before
    fun setUp() {
        bankHomeViewModel = createViewModel(cachedDynamicScreenRepo)
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets view state to success when fetch screen service response is successful`() {
        runBlockingTest {
            whenever(
                cachedDynamicScreenRepo.getScreen(
                    ServicePath.BankAccountOverView(
                        accountUuid = "1235",
                        accountId = "1234",
                        getBankAccountStartDate(),
                        getBankAccountEndDate(),
                    ),
                ),
            ).thenReturn(createMapping(screenResponseState = ScreenResponseState.SUCCESS))

            bankHomeViewModel.fetchScreen("1234", "1235", true)

            bankHomeViewModel.viewState.observeForever {
                Assert.assertTrue(it is ViewState.Success)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets view state to error when fetch screen service response is failure`() {
        runBlockingTest {
            whenever(
                cachedDynamicScreenRepo.getScreen(
                    ServicePath.BankAccountOverView(
                        accountUuid = "1235",
                        accountId = "1234",
                        getBankAccountStartDate(),
                        getBankAccountEndDate()
                    )
                )
            ).thenReturn(createMapping(screenResponseState = ScreenResponseState.ERROR))

            bankHomeViewModel.fetchScreen("1234", "1235", true)

            bankHomeViewModel.viewState.observeForever {
                Assert.assertTrue(it is ViewState.Error)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets view state to loading when fetch screen service response is loading`() {
        runBlockingTest {
            whenever(
                cachedDynamicScreenRepo.getScreen(
                    ServicePath.BankAccountOverView(
                        accountUuid = "1235",
                        accountId = "1234",
                        getBankAccountStartDate(),
                        getBankAccountEndDate()
                    )
                )
            ).thenReturn(createMapping(screenResponseState = ScreenResponseState.LOADING))

            bankHomeViewModel.fetchScreen("1234", "1235", true)

            bankHomeViewModel.viewState.observeForever {
                Assert.assertTrue(it is ViewState.Loading)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `sets view state to error when fetch screen service fails with no response`() {
        runBlockingTest {
            whenever(
                cachedDynamicScreenRepo.getScreen(
                    ServicePath.BankAccountOverView(
                        accountUuid = "1235",
                        accountId = "1234",
                        getBankAccountStartDate(),
                        getBankAccountEndDate()
                    )
                )
            ).thenReturn(Observable.error(RuntimeException("Bad things")))

            bankHomeViewModel.fetchScreen("1234", "1235", true)

            bankHomeViewModel.viewState.observeForever {
                Assert.assertTrue(it is ViewState.Error)
            }
        }
    }

    @Test
    @ExperimentalCoroutinesApi
    fun `fetch screen service is not called when forceRefresh is false`() {
        runBlockingTest {
            whenever(
                cachedDynamicScreenRepo.getScreen(
                    ServicePath.BankAccountOverView(
                        accountUuid = "1235",
                        accountId = "1234",
                        getBankAccountStartDate(),
                        getBankAccountEndDate()
                    )
                )
            ).thenReturn(createMapping(screenResponseState = ScreenResponseState.SUCCESS))

            bankHomeViewModel.fetchScreen("1234", "1235", false)

            verify(cachedDynamicScreenRepo, never()).getScreen(any(), any(), any())
        }
    }

    private fun createMapping(screenResponseState: ScreenResponseState): Observable<Resource<List<GenericLayout>>> {
        return when {
            ScreenResponseState.SUCCESS == screenResponseState -> {
                Observable.just(Success(emptyList()))
            }
            ScreenResponseState.LOADING == screenResponseState -> {
                Observable.just(Loading(emptyList()))
            }
            else -> Observable.just(Failed(emptyList()))
        }
    }
}
