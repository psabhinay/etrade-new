package com.etrade.mobilepro.screener.presentation.base

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.HasDataCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.HasSymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.sorting.SortableTableViewListener
import com.etrade.mobilepro.tableviewutils.presentation.sorting.TableDataSortable

class ScreenerResultTableViewListener(
    sortable: TableDataSortable,
    private val onClickAction: (String, InstrumentType) -> Unit
) : SortableTableViewListener(sortable) {

    override fun onRowHeaderClicked(rowHeaderView: RecyclerView.ViewHolder, row: Int) {
        (rowHeaderView as? HasSymbolCell)?.cell?.let { symbolCell ->
            onClickAction.invoke(symbolCell.displayText, symbolCell.instrumentType)
        }
    }

    override fun onCellClicked(cellView: RecyclerView.ViewHolder, column: Int, row: Int) {
        (cellView as? HasDataCell)?.cell?.let { dataCell ->
            dataCell as? ResultDataCell
        }?.also {
            onClickAction.invoke(it.symbol, it.instrumentType)
        }
    }
}
