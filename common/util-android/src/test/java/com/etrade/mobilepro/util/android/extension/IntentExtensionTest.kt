package com.etrade.mobilepro.util.android.extension

import android.content.Intent
import android.net.Uri
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class IntentExtensionTest {

    @Test
    fun `should return intent with proper parameters for dial action`() {
        MatcherAssert.assertThat(Intent().toDialAction().action, equalTo(Intent.ACTION_DIAL))
        MatcherAssert.assertThat(Intent().toDialAction().data, `is`(nullValue()))
        MatcherAssert.assertThat(Intent().toDialAction(TEST_TEL_NUMBER).data, equalTo(Uri.parse("tel:$TEST_TEL_NUMBER")))
    }

    companion object {
        private const val TEST_TEL_NUMBER = "3248521"
    }
}
