package com.etrade.mobilepro.orders

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder

fun Fragment.showAffiliatedProductDialog(onDialogDismissed: (() -> Unit)? = null) {
    MaterialAlertDialogBuilder(requireContext())
        .setOnDismissListener { onDialogDismissed?.invoke() }
        .setCancelable(false)
        .setTitle(getString(R.string.affiliated_product_title))
        .setMessage(getString(R.string.affiliated_product_body))
        .setPositiveButton(getString(R.string.ok)) { dialogInterface, _ ->
            onDialogDismissed?.invoke()
            dialogInterface.dismiss()
        }
        .show()
}
