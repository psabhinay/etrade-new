package com.etrade.mobilepro.education.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.education.EducationItem
import com.etrade.mobilepro.education.adapter.delegate.EducationAdapterDelegate
import com.etrade.mobilepro.education.adapter.delegate.EducationOptionsRiskDisclosureAdapterDelegate
import com.etrade.mobilepro.education.adapter.delegate.EducationSectionAdapterDelegate
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class EducationAdapter(
    levelTemplate: String,
    itemClickListener: (EducationItem) -> Unit,
    riskClickListener: View.OnClickListener,
    disclosureClickListener: View.OnClickListener
) : ListDelegationAdapter<MutableList<EducationItem>>() {

    init {
        delegatesManager.addDelegate(
            EducationAdapterDelegate(
                View.OnClickListener {
                    itemClickListener.invoke(it.tag as EducationItem)
                }
            )
        )
        delegatesManager.addDelegate(
            EducationSectionAdapterDelegate(
                levelTemplate,
                View.OnClickListener {
                    itemClickListener.invoke(it.tag as EducationItem)
                }
            )
        )
        delegatesManager.addDelegate(
            EducationOptionsRiskDisclosureAdapterDelegate(
                riskClickListener,
                disclosureClickListener
            )
        )

        items = mutableListOf()
    }

    fun updateItems(items: List<EducationItem>) {
        dispatchUpdates(this.items, items, EducationItemDiffCallback(this.items, items))
        this.items.clear()
        this.items.addAll(items)
    }

    override fun getItemCount(): Int = items.size + 1

    override fun getItemViewType(position: Int): Int {
        return delegatesManager.getItemViewType(items, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegatesManager.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        return delegatesManager.onBindViewHolder(items, position, holder)
    }
}
