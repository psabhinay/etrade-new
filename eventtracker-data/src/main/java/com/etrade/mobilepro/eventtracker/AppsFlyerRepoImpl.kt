package com.etrade.mobilepro.eventtracker

import android.content.Context
import com.appsflyer.AppsFlyerLib
import com.etrade.mobilepro.eventtrackerapi.AppsFlyerRepo
import com.etrade.mobilepro.eventtrackerapi.AppsFlyerRequestApi
import com.etrade.mobilepro.eventtrackerapi.dto.AppsFlyerRequestDto
import com.etrade.mobilepro.eventtrackerapi.dto.SubjectIdentities
import com.etrade.mobilepro.eventtrackerdata.R
import org.slf4j.LoggerFactory
import retrofit2.HttpException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.UUID
import javax.inject.Inject

class AppsFlyerRepoImpl @Inject constructor(
    private val appsFlyerRequestApi: AppsFlyerRequestApi,
    private val context: Context
) : AppsFlyerRepo {

    private val logger by lazy { LoggerFactory.getLogger(AppsFlyerRepoImpl::class.java) }

    override val appsFlyerId: String
        get() = trackerInstance.getAppsFlyerUID(context)

    private val trackerInstance = AppsFlyerLib.getInstance()

    // Due to the GDPR legislation, we do not want to track international users. This function tells AppsFlyer to erase
    // all data that has been collected by a given user. Requests to erase can take up to 30 days for AppsFlyer to process
    override suspend fun sendAppsFlyerEraseRequest(adId: String) {
        val time = System.currentTimeMillis()
        val subjectRequestId = UUID.randomUUID().toString()
        val dateFormatter = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale("UTC"))
        val date = dateFormatter.format(Date(time)) + "Z"
        val body = AppsFlyerRequestDto(
            subjectRequestId = subjectRequestId,
            subjectRequestType = "erasure",
            submittedRequestTime = date,
            subjectIdentities = listOf(
                SubjectIdentities(
                    identityType = "android_advertising_id",
                    identityValue = adId,
                    identityFormat = "raw"
                )
            ),
            propteryId = "com.etrade.mobilepro.activity",
            statusCallbackUrl = emptyList()
        )
        val apiKey = context.getString(R.string.apps_flyer_api_key)
        try {
            appsFlyerRequestApi.makeAppsFlyerRequest(apiKey, body)
        } catch (e: HttpException) {
            logger.error(e.message)
        }
    }

    override suspend fun trackEvent(event: String, params: Map<String, Any>) {
        trackerInstance.trackEvent(context, event, params)
    }
}
