package com.etrade.mobilepro.checkdeposit.usecases.validation

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.checkdeposit.deposit.service.DepositImageResponseDto
import com.etrade.mobilepro.checkdeposit.deposit.service.RemoteCheckDepositService
import com.etrade.mobilepro.checkdeposit.usecases.deposit.CheckDepositResult
import com.etrade.mobilepro.checkdeposit.usecases.deposit.DepositImageRequest
import com.etrade.mobilepro.checkdeposit.usecases.deposit.DepositImageUseCaseImpl
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.util.invoke
import com.etrade.mobilepro.util.json.BigDecimalAdapter
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class DepositImageUseCaseTest {

    private fun mockServiceCall(path: String): RemoteCheckDepositService {
        return mock {
            onBlocking { depositimage(any()) } doReturn
                DepositImageUseCaseTest::class.getObjectFromJson(
                    path,
                    ServerResponseDto::class.java,
                    DepositImageResponseDto::class.java,
                    adapters = listOf(BigDecimalAdapter)
                )
        }
    }

    private fun testUseCase(path: String, verify: ((value: CheckDepositResult) -> CheckDepositResult, (exception: Throwable) -> Unit) -> Unit) {
        val service = mockServiceCall(path)
        val case = DepositImageUseCaseImpl("", service)

        val result = runBlocking {
            case(
                DepositImageRequest(
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                )
            )
        }

        Assertions.assertNotNull(result)

        val onSuccess: (value: CheckDepositResult) -> CheckDepositResult = mock()
        val onFailure: (exception: Throwable) -> Unit = mock()

        result.fold(
            onSuccess = onSuccess,
            onFailure = onFailure
        )

        verify(onSuccess, onFailure)
    }

    @Test
    fun `result should be Invalid if errors present`() {
        testUseCase("depositimage_invalid.json") { onSuccess, onFailure ->
            verify(onFailure, never()).invoke(any())
            verify(onSuccess).invoke(
                CheckDepositResult.Invalid(
                    message = "It appears the amount you entered does not match the amount we detected on the front of the check.  Please re-enter the amount or take a new photo of the front of the check.",
                    code = "Amounts_Match",
                    frontImageFieldToFlag = true,
                    rearImageFieldToFlag = false,
                    amountFieldToFlag = true
                )
            )
        }
    }

    @Test
    fun `result should be Valid if errors present`() {
        testUseCase("depositimage_valid.json") { onSuccess, onFailure ->
            verify(onFailure, never()).invoke(any())
            verify(onSuccess).invoke(any<CheckDepositResult.Valid>())
        }
    }
}
