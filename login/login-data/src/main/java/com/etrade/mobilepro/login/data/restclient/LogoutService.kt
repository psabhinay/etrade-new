package com.etrade.mobilepro.login.data.restclient

import com.etrade.eo.rest.retrofit.Xml
import com.etrade.mobilepro.login.data.restclient.dto.LogoutResponse
import retrofit2.http.POST

internal interface LogoutService {
    @Xml
    @POST("/e/t/mobile/logout")
    suspend fun logout(): LogoutResponse
}
