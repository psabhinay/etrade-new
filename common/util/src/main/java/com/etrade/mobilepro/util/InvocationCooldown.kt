package com.etrade.mobilepro.util

import androidx.annotation.VisibleForTesting

/**
 * Supports a "cooldown" for invoking an action, such that the action will not be invoked if called during a cooldown time interval.
 * The cooldown interval begins when [startCooldown] is called, and ends after the given duration.
 */
class InvocationCooldown @VisibleForTesting internal constructor(val clock: () -> Long) {
    // TODO: Use a java.time.Clock when we have access to JDK 1.8 APIs
    constructor() : this({ System.currentTimeMillis() })

    private var cooldownEndMillis: Long = 0L

    /**
     * Invokes the given action if called outside of the cooldown period.
     */
    fun invokeIfReady(action: () -> Unit) {
        if (clock() > cooldownEndMillis) action()
    }

    /**
     * Starts the cooldown period, to end after the given duration.
     */
    fun startCooldown(durationInMillis: Long = DEFAULT_COOLDOWN_MILLIS) {
        cooldownEndMillis = clock() + durationInMillis
    }

    companion object {
        const val DEFAULT_COOLDOWN_MILLIS: Long = 1000L
    }
}
