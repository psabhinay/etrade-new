package com.etrade.mobilepro.util.android.recyclerviewutil

import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.util.android.view.scrollToView

/**
 * Utility class which task is to locate a view into another View (ViewGroup) or into a RecyclerView
 * using LinearLayoutManager. It will traverse its target container until it finds the View which meets
 * a given delegate, otherwise it will return null.
 *
 * @param awaitForMainThread Suspend function which is required to suspend execution in order to wait for
 * scrolling or RecyclerView rendering events.
 */
class ViewLocator(private val awaitForMainThread: suspend () -> Unit) {

    /**
     * Locate a View which may be inside a LinearLayoutManager and must meet the specified predicate.
     *
     * @param layoutManager The [LinearLayoutManager] which may contain the required view.
     * @param predicate The condition which a candidate View must meet in order to be considered as the
     * target View.
     * @return The target view or null if no View was found or meet the given predicate.
     */
    suspend fun locateView(
        layoutManager: LinearLayoutManager,
        scrollWithOffset: Boolean = false,
        predicate: (View) -> Boolean
    ): View? {
        val traverseResult = traverseLayoutManager(layoutManager, predicate)

        if (traverseResult != null) {
            scrollToView(traverseResult.view, layoutManager, traverseResult.adapterPosition, scrollWithOffset)
            // Await for scroll execution, so the target view is fully visible before
            // returning it to consumers.
            awaitForMainThread()
        }

        return traverseResult?.view
    }

    /**
     * Locate a View which may be inside a LinearLayoutManager and must meet the specified predicate.
     *
     * @param rootView The View which may contain the required View.
     * @param scrollView The optional [NestedScrollView] which hosts [rootView] and may be required for
     * scrolling and focus the target View.
     * @param predicate The condition which a candidate View must meet in order to be considered as the
     * target View.
     * @return The target view or null if no View was found or meet the given predicate.
     */
    suspend fun locateView(
        rootView: View,
        scrollView: NestedScrollView? = null,
        predicate: (View) -> Boolean
    ): View? {
        val targetView = traverseView(rootView, predicate)

        if (targetView != null && scrollView != null) {
            scrollToView(targetView, scrollView)
            // Await for scroll execution, so the target view is fully visible before
            // returning it to consumers.
            awaitForMainThread()
        }

        return targetView
    }

    private suspend fun traverseLayoutManager(
        linearLayoutManager: LinearLayoutManager,
        predicate: (View) -> Boolean
    ): TraverseResult? {
        var traverseResult: TraverseResult? = null
        var latestScrolledPosition: Int

        while (traverseResult == null) {
            val firstVisiblePosition = linearLayoutManager.findFirstVisibleItemPosition()
            val lastVisiblePosition = linearLayoutManager.findLastVisibleItemPosition()
                .takeIf { it > RecyclerView.NO_POSITION }
                ?: firstVisiblePosition
            latestScrolledPosition = lastVisiblePosition

            for (i in firstVisiblePosition..lastVisiblePosition) {
                traverseResult = linearLayoutManager.findViewByPosition(i)
                    ?.let { traverseView(it, predicate) }
                    ?.let { TraverseResult(it, latestScrolledPosition) }
                if (traverseResult != null) { break }
            }

            if (traverseResult != null || lastVisiblePosition == linearLayoutManager.itemCount - 1) {
                break
            } else {
                latestScrolledPosition = lastVisiblePosition + 1
                linearLayoutManager.scrollToPosition(latestScrolledPosition)
                // Await for scroll execution, so next RecyclerView items are instantiated and we
                // can traverse them.
                awaitForMainThread()
            }
        }

        return traverseResult
    }

    private fun traverseView(view: View, predicate: (View) -> Boolean): View? {
        var targetView: View? = null

        if (predicate(view)) {
            targetView = view
        } else if (view is ViewGroup) {
            view.children.forEach { childView ->
                val candidateView = traverseView(childView, predicate)
                if (candidateView != null) {
                    targetView = candidateView
                    return@forEach
                }
            }
        }

        return targetView
    }
}

private class TraverseResult(
    val view: View,
    val adapterPosition: Int
)
