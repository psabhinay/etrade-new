package com.etrade.mobilepro.movers.compose

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.etrade.mobilepro.movers.MoverItem
import com.etrade.mobilepro.movers.R

@Composable
@Suppress("LongMethod")
fun MoverCell(item: MoverItem, modifier: Modifier = Modifier) {
    Card(
        modifier = modifier.padding(all = dimensionResource(R.dimen.spacing_xsmall)),
        backgroundColor = colorResource(
            if (item.isPositiveGain) {
                R.color.green
            } else {
                R.color.red
            }
        )
    ) {
        Column(
            modifier = Modifier.padding(
                vertical = dimensionResource(R.dimen.spacing_xsmall),
                horizontal = dimensionResource(R.dimen.spacing_small)
            )
        ) {
            Text(item.displaySymbol, style = moverSymbolTextStyle())
            Row {
                item.optionsExpiryDate?.let {
                    Text(it, style = moverOptionsDataTextStyle())
                }
                Spacer(
                    Modifier.size(
                        height = dimensionResource(R.dimen.spacing_medium),
                        width = dimensionResource(R.dimen.spacing_small)
                    )
                )
                item.optionsStrikePricePlusType?.let {
                    Text(it, style = moverOptionsDataTextStyle())
                }
            }
            Text(
                item.price,
                style = moverPriceTextStyle(),
                modifier = Modifier.semantics {
                    contentDescription = item.priceDescription ?: item.price
                }
            )
            Text(
                item.gain,
                style = moverGainTextStyle(),
                modifier = Modifier.semantics {
                    contentDescription = item.gainDescription ?: item.gain
                }
            )
            Text(item.gainPercentage, style = moverGainTextStyle())
        }
    }
}

// once all et redesign language is ported to Compose use that and remove these
@Composable
@Suppress("MagicNumber")
private fun moverSymbolTextStyle() = MaterialTheme.typography.subtitle2.copy(
    color = colorResource(R.color.white),
    fontSize = 14.sp,
    fontWeight = FontWeight(500),
)

@Composable
@Suppress("MagicNumber")
private fun moverPriceTextStyle() = MaterialTheme.typography.subtitle2.copy(
    color = colorResource(R.color.white),
    fontSize = 16.sp,
    fontWeight = FontWeight(500)
)

@Composable
@Suppress("MagicNumber")
private fun moverGainTextStyle() = MaterialTheme.typography.subtitle2.copy(
    color = colorResource(R.color.white),
    fontSize = 12.sp,
    fontWeight = FontWeight.Normal
)

@Composable
@Suppress("MagicNumber")
private fun moverOptionsDataTextStyle() = MaterialTheme.typography.subtitle2.copy(
    color = colorResource(R.color.white),
    fontSize = 10.sp,
    fontWeight = FontWeight.Normal
)
