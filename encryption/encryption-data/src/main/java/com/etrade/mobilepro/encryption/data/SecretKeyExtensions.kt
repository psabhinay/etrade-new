package com.etrade.mobilepro.encryption.data

import android.os.Build
import android.security.keystore.KeyInfo
import android.security.keystore.KeyProperties
import com.etrade.mobilepro.util.android.isEmulator
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory

/**
 * Returns `true` if the key is inside secure hardware.
 *
 * @see KeyInfo.isInsideSecureHardware
 */
internal val SecretKey.isInsideSecureHardware: Boolean
    get() {
        if (isEmulator()) {
            return true
        }

        val factory = SecretKeyFactory.getInstance(algorithm, KEY_STORE)
        val keyInfo = factory.getKeySpec(this, KeyInfo::class.java) as KeyInfo

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            keyInfo.securityLevel in listOf(
                KeyProperties.SECURITY_LEVEL_STRONGBOX,
                KeyProperties.SECURITY_LEVEL_TRUSTED_ENVIRONMENT,
                KeyProperties.SECURITY_LEVEL_UNKNOWN_SECURE,
            )
        } else {
            @Suppress("deprecation")
            keyInfo.isInsideSecureHardware
        }
    }
