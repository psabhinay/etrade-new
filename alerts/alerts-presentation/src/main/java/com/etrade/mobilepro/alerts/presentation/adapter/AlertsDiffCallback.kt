package com.etrade.mobilepro.alerts.presentation.adapter

import androidx.recyclerview.widget.DiffUtil
import com.etrade.mobilepro.alerts.api.model.AlertItem

class AlertsDiffCallback(
    private val oldItems: List<AlertItem>,
    private val newItems: List<AlertItem>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldAlert = oldItems[oldItemPosition]
        val newAlert = newItems[newItemPosition]
        return checkIfSameAlert(oldAlert, newAlert) || oldAlert == newAlert
    }

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }

    private fun checkIfSameAlert(oldAlert: AlertItem, newAlert: AlertItem): Boolean {
        return oldAlert is AlertItem.Alert && newAlert is AlertItem.Alert && oldAlert.id == newAlert.id
    }
}
