package com.etrade.mobilepro.confirmationview

import android.content.Context
import android.graphics.drawable.AnimatedVectorDrawable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.etrade.mobilepro.confirmationview.databinding.ConfirmationviewViewSuccessBinding
import com.etrade.mobilepro.util.android.extension.inflater

class SuccessView : FrameLayout {
    private var _binding: ConfirmationviewViewSuccessBinding? = ConfirmationviewViewSuccessBinding.inflate(inflater, this, true)
    val binding: ConfirmationviewViewSuccessBinding
        get() = _binding!!

    constructor(context: Context) : super(context)

    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet)

    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    fun setTitle(text: String) {
        binding.title.text = text
    }

    fun playAnimation() {
        binding.tickIcon.visibility = View.GONE
        binding.tickIconAnimated.visibility = View.VISIBLE
        (binding.tickIconAnimated.drawable as? AnimatedVectorDrawable)?.start()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        _binding = null
    }
}
