package com.etrade.mobilepro.orders.list.api

import com.etrade.mobilepro.orders.api.OrderStatus
import org.threeten.bp.LocalDate

data class OrderParams(
    val accountId: String,
    val fromDate: LocalDate?,
    val toDate: LocalDate,
    val orderStatus: OrderStatus,
    val isStockPlan: Boolean,
    val accountIndex: String?,
    val forceRefresh: Boolean = false
)
