-if class com.etrade.mobilepro.order.details.dto.OrderDetailsRequestValueDto
-keepnames class com.etrade.mobilepro.order.details.dto.OrderDetailsRequestValueDto
-if class com.etrade.mobilepro.order.details.dto.OrderDetailsRequestValueDto
-keep class com.etrade.mobilepro.order.details.dto.OrderDetailsRequestValueDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class com.etrade.mobilepro.order.details.dto.OrderDetailsRequestValueDto
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class com.etrade.mobilepro.order.details.dto.OrderDetailsRequestValueDto
-keepclassmembers class com.etrade.mobilepro.order.details.dto.OrderDetailsRequestValueDto {
    public synthetic <init>(java.lang.String,java.lang.String,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
