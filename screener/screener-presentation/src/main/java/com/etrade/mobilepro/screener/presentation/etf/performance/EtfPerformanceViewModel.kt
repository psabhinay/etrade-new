package com.etrade.mobilepro.screener.presentation.etf.performance

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.etf.EtfPerformanceFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Etf
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class EtfPerformanceViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @Etf screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<EtfPerformanceFilter, EtfPerformanceViewModel.ViewState>(
    aggregator,
    ScreenerType.Etf.Performance,
    screenerInteractor
) {
    override fun provideInitialState(initialFilter: EtfPerformanceFilter): ViewState = ViewState(filter = initialFilter)

    data class ViewState(
        override val filter: EtfPerformanceFilter
    ) : BaseFilterScreenViewModel.ViewState<EtfPerformanceFilter>
}
