package com.etrade.mobilepro.watchlist.news.filter

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.news.api.filter.FilterDataSourceWithForcedUpdate
import com.etrade.mobilepro.watchlistapi.Watchlist
import javax.inject.Inject

class WatchlistFilterViewModel @Inject constructor(
    val filterDataSource: FilterDataSourceWithForcedUpdate<Watchlist>
) : ViewModel()
