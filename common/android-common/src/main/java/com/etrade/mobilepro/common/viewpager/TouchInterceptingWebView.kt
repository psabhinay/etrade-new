package com.etrade.mobilepro.common.viewpager

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.webkit.WebView

/**
 * This is an interactive WebView that will be used inside a view group that has touch interactions, e.g. chartIqWebView
 * with a lot of touch events within a ViewPager or LazyColumn.
 * Purpose of this is to disallow parent from intercepting the touch event and allow all of them to be passed to the
 * WebView itself.
 */
class TouchInterceptingWebView : WebView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        when (ev?.action) {
            MotionEvent.ACTION_DOWN -> parent?.requestDisallowInterceptTouchEvent(true)
        }
        return super.onInterceptTouchEvent(ev)
    }
}
