package com.etrade.mobilepro.markets

import android.os.Bundle
import androidx.navigation.NavController
import com.etrade.mobilepro.marketscalendar.MarketCalendarRouter
import com.etrade.mobilepro.marketscalendar.fragment.EarningsCalendarEventDetailsFragmentArgs
import com.etrade.mobilepro.marketscalendar.fragment.EconomicCalendarEventDetailsFragmentArgs
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails
import javax.inject.Inject

class DefaultMarketCalendarRouter @Inject constructor() : MarketCalendarRouter {
    override fun navigateToEarningEventDetails(navController: NavController, item: EventDetails) =
        navController.navigate(MarketsFragmentDirections.marketsCalendarToCalendarEarningsEventDetails(item))

    override fun navigateToEconomicEventDetails(navController: NavController, item: EventDetails) =
        navController.navigate(MarketsFragmentDirections.marketsCalendarToCalendarEconomicEventDetails(item))

    override fun getEarningEventDetailsItem(arguments: Bundle?): EventDetails? = arguments?.let { EarningsCalendarEventDetailsFragmentArgs.fromBundle(it).item }

    override fun getEconomicEventDetailsItem(arguments: Bundle?): EventDetails? =
        arguments?.let { EconomicCalendarEventDetailsFragmentArgs.fromBundle(it).item }
}
