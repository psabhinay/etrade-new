package com.etrade.mobilepro.chat.presentation.message

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.api.ChatParamsRequestModel
import com.etrade.mobilepro.chat.api.ChatState
import com.etrade.mobilepro.chat.api.message.repo.ChatMessagesRepo
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory

private val logger: Logger = LoggerFactory.getLogger(ChatMessagesViewModel::class.java)

class ChatMessagesViewModel @AssistedInject constructor(
    private val chatMessagesRepo: ChatMessagesRepo,
    @Assisted private val chatParams: ChatParamsRequestModel?
) : ViewModel() {

    private val _viewState = MediatorLiveData<ViewState>()
    val viewState: LiveData<ViewState>
        get() = _viewState

    @AssistedFactory
    interface Factory {
        fun create(chatParams: ChatParamsRequestModel?): ChatMessagesViewModel
    }

    fun onChatStateChange(it: ChatState?) = it?.mapToViewState()

    /**
     * Flag to differentiate between Intentional transfer or transfer due agent disconnect.
     * used to show appropriate message based on type of Transfer,
     * ChatTransferred - Intentional Transfer
     * AgentDisconnect - UnIntentional Transfer
     * Queue Update events is sent following either one of the above Event.
     */
    var agentDisconnected = false
        private set

    @SuppressWarnings("ComplexMethod")
    private fun ChatState.mapToViewState() = when (this) {
        is ChatState.ChatTransferredMessage -> _viewState.value = ViewState.ChatTransferredView(messages).also { agentDisconnected = false }
        is ChatState.MessagesShown -> _viewState.value = ViewState.MessagesShown(messages)
        is ChatState.QueueShown -> _viewState.value = ViewState.QueueShown(estimatedWaitTime).also { agentDisconnected = isAgentDisconnected }
        is ChatState.SendMessageError -> _viewState.value = ViewState.SendMessageError(messages, message)
        is ChatState.Error -> _viewState.value = ViewState.Error()
        ChatState.TerminatedByAgent -> _viewState.value = ViewState.Terminated(false)
        ChatState.TerminatedByUser -> terminateByUser()
        ChatState.Unknown -> _viewState.value = ViewState.Undefined
        else -> { /* Intentionally left blank, Do nothing */ }
    }

    fun refresh(forced: Boolean = false) {
        if (chatParams != null && (_viewState.value == null || forced)) {
            _viewState.value = ViewState.Loading
            viewModelScope.launch {
                chatMessagesRepo.getMobileChat(chatParams).onSuccess {
                    val data = it.data
                    val sessionId = data?.chatInfoModel?.sessionID
                    if (sessionId == null) {
                        logger.error(data?.messages)
                        _viewState.value = ViewState.Error(data?.messages)
                    } else {
                        _viewState.postValue(ViewState.StartChatService(sessionId))
                    }
                }.onFailure {
                    logger.error("Fail to get mobile chat", it)
                    _viewState.value = ViewState.Error()
                }
            }
        } else if (_viewState.value !is ViewState.Error) {
            _viewState.value = ViewState.StartChatService()
        }
    }

    fun terminateByUser() {
        val navToChatSetup = _viewState.value is ViewState.QueueShown
        _viewState.value = ViewState.Terminated(navToChatSetup)
    }

    sealed class ViewState {

        object Loading : ViewState()

        class Terminated(val navToChatSetup: Boolean = false) : ViewState()

        data class Error(val message: String? = null) : ViewState()

        class StartChatService(val sessionId: String? = null) : ViewState()

        class SendMessageError(val messages: List<ChatMessage>, val message: String) : ViewState()

        class MessagesShown(val messages: List<ChatMessage>) : ViewState()

        class ChatTransferredView(val messages: List<ChatMessage>) : ViewState()

        class QueueShown(val estimatedWaitTimeInSeconds: Int) : ViewState()

        // Empty items or unrecognized events received.
        object Undefined : ViewState()
    }
}
