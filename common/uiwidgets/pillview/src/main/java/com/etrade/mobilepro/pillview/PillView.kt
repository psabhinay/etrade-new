package com.etrade.mobilepro.pillview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.etrade.mobilepro.pillview.databinding.ViewPillBinding

private const val FADE_DURATION_MS = 150L

class PillView : FrameLayout {

    private var binding: ViewPillBinding =
        ViewPillBinding.inflate(LayoutInflater.from(context), this, true)

    constructor(ctx: Context) : super(ctx)

    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet)

    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    fun setState(state: State) {
        when (state) {
            State.Hidden -> fadeOut()
            is State.Shown -> {
                binding.loadingPb.visibility = View.INVISIBLE
                if (visibility == View.VISIBLE) {
                    updateWithFading(state.text)
                } else {
                    fadeIn(state.text)
                }
            }
            is State.Loading -> {
                binding.loadingPb.visibility = View.VISIBLE
                binding.value.text = state.text
                fadeIn(state.text)
            }
        }
    }

    private fun fadeOut() {
        if (alpha == 0F || visibility == View.INVISIBLE) {
            return
        }
        animate()
            .alpha(0.0F)
            .setDuration(FADE_DURATION_MS)
            .withEndAction {
                visibility = View.INVISIBLE
            }
            .start()
    }

    private fun fadeIn(text: CharSequence?) {
        if (alpha == 1F || visibility == View.VISIBLE) {
            return
        }
        binding.value.text = text
        animate()
            .alpha(1.0F)
            .setDuration(FADE_DURATION_MS)
            .withEndAction {
                visibility = View.VISIBLE
            }
            .start()
    }

    private fun updateWithFading(text: CharSequence?) {
        visibility = View.VISIBLE
        animate()
            .alpha(0.0F)
            .setDuration(FADE_DURATION_MS)
            .withEndAction {
                binding.value.text = text
                animate()
                    .alpha(1.0f)
                    .setDuration(FADE_DURATION_MS)
                    .start()
            }
            .start()
    }

    sealed class State {
        object Hidden : State()
        data class Loading(
            val text: CharSequence?
        ) : State()
        data class Shown(
            val text: CharSequence?
        ) : State()
    }
}
