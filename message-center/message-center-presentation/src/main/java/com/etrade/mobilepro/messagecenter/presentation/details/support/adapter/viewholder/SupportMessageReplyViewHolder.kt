package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.viewholder

import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterItemSupportMessageReplyBinding
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.Reply

internal class SupportMessageReplyViewHolder(
    private val binding: MessageCenterItemSupportMessageReplyBinding,
    replyContentListener: (String) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    init {
        binding.newMessageText.doAfterTextChanged { replyContentListener.invoke(it?.toString() ?: "") }
    }
    fun bindTo(item: Reply) {
        binding.newMessageText.setText(item.content)
    }
}
