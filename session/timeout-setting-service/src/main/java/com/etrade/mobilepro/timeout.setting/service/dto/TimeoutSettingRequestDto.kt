package com.etrade.mobilepro.timeout.setting.service.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TimeoutSettingRequestDto(
    @Json(name = "value")
    val value: RequestDetail
)

@JsonClass(generateAdapter = true)
data class RequestDetail(
    @Json(name = "profileItemName")
    val profileItemName: String,
    @Json(name = "profileItemValue")
    val profileItemValue: String
)
