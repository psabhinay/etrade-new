package com.etrade.mobilepro.overlay

import android.os.Bundle
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.text.util.LinkifyCompat
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

abstract class TextOverlayFragment<VM>(clazz: Class<VM>) : Fragment() where VM : ViewModel, VM : TextOverlayViewModel {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var snackbarUtilFactory: SnackbarUtilFactory

    protected val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    protected lateinit var viewModel: VM
    protected val viewModelClass: Class<VM> = clazz
    protected abstract val layoutRes: Int

    private fun viewModelProvider() = ViewModelProvider(this, viewModelFactory)

    abstract fun getText()

    protected abstract val overlayTextLoading: ContentLoadingProgressBar

    protected abstract val contentText: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = viewModelProvider().get(viewModelClass)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.text.observe(viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> {
                    overlayTextLoading.hide()
                    contentText.text = it.data
                    LinkifyCompat.addLinks(contentText, Linkify.ALL)
                }
                is Resource.Loading -> {
                    overlayTextLoading.show()
                    contentText.text = ""
                }
                is Resource.Failed -> {
                    overlayTextLoading.hide()
                    snackbarUtil.retrySnackbar { getText() }
                }
            }
        }
    }
}
