package com.etrade.mobilepro.marketscalendar.util

import com.etrade.mobilepro.marketscalendar.api.model.EventsRequest
import com.etrade.mobilepro.marketscalendar.data.dto.ALL_SYMBOLS_EVENTS
import org.threeten.bp.LocalDate

fun createRequestForMarketsEvents(
    startDate: LocalDate,
    endDate: LocalDate,
    eventType: String,
    eventFilterEnable: String?
): EventsRequest {
    return EventsRequest(
        eventsRequestValue = EventsRequest.EventsRequestValue(
            eventsRequestDateRange = EventsRequest.EventsRequestDateRange(
                startDate,
                endDate
            ),
            symbolSet = listOf(ALL_SYMBOLS_EVENTS),
            eventFilterEnable = eventFilterEnable,
            eventTypeSet = listOf(eventType)
        )
    )
}
