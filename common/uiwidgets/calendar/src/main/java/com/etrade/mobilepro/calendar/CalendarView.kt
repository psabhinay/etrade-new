package com.etrade.mobilepro.calendar

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.withStyledAttributes
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.etrade.mobilepro.calendar.adapter.WeekDaysTitleAdapter
import com.etrade.mobilepro.calendar.adapter.WeeksViewPagerAdapter
import com.etrade.mobilepro.calendar.databinding.ViewCalendarBinding
import com.etrade.mobilepro.util.delegate.getValue
import com.etrade.mobilepro.util.delegate.setValue
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import org.threeten.bp.format.TextStyle
import java.util.Locale
import kotlin.math.roundToInt

private const val ROWS_COUNT_FOR_TWO_WEEKS = 2
private const val ROWS_COUNT_FOR_MONTH = 6

class CalendarView : FrameLayout {

    private lateinit var arrowLeft: AppCompatImageView
    private lateinit var arrowRight: AppCompatImageView
    private lateinit var daysViewpager: ViewPager
    private lateinit var monthYear: TextView
    private lateinit var weekDaysTitleRV: RecyclerView

    private val weeksViewPagerAdapter: WeeksViewPagerAdapter = WeeksViewPagerAdapter()

    /**
     * When user clicks on any date in the calendar we could obtain date in this callback
     */
    var onDateSelected: ((LocalDate) -> Unit)? by weeksViewPagerAdapter::onClickListener

    /**
     * Defines current date for calendar
     */
    var currentDate: LocalDate by weeksViewPagerAdapter::currentDate

    /**
     * Defines selected date for calendar
     */
    var selectedDate: LocalDate? by weeksViewPagerAdapter::selectedDate

    /**
     * Calendar view uses this [dayFormatter] to display each day on calendar grid
     */
    var dayFormatter: ((LocalDate) -> String) by weeksViewPagerAdapter::dayFormatter

    /**
     * If you need to mark some days as disabled but let user to select them
     */
    var disabledDaysPredicate: (LocalDate) -> Boolean by weeksViewPagerAdapter::disabledDaysPredicate

    /**
     * If you need to mark some days as disabled and disallow selection
     */
    var clickableDaysPredicate: (LocalDate) -> Boolean by weeksViewPagerAdapter::clickableDaysPredicate

    /**
     * When user changes calendar period this callback will be called with start, end dates rage and position that is represents [ViewPager.getCurrentItem]
     */
    var onCalendarRangeSelected: ((LocalDate, LocalDate, Int) -> Unit)? = null

    /**
     * Used to display current month in calendar header
     */
    var monthYearFormatter: ((LocalDate) -> String) = { date -> "${date.month.getDisplayName(TextStyle.FULL, Locale.US)} ${date.year}" }

    /**
     * Current calendar page, value from [ViewPager.getCurrentItem]
     */
    val currentCalendarPage: Int
        get() = daysViewpager.currentItem

    private var mode: CalendarMode by weeksViewPagerAdapter::mode

    private var weekDayHeight: Int by weeksViewPagerAdapter::weekDayHeight

    private val pageChangeListener = object : ViewPager.SimpleOnPageChangeListener() {

        override fun onPageSelected(position: Int) {
            onCalendarRangeSelected?.invoke(
                mode.getFirstDateOfRange(currentDate, position),
                mode.getLastDateOfRange(currentDate, position),
                position
            )

            val date: LocalDate = getDateForMonthYearTitle(position, currentDate)

            monthYearFormatter.also { setMonthYear(date, it) }
        }
    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    /**
     * To mark days in calendar with events
     */
    fun updateCalendarEvents(items: Set<LocalDate>) {
        weeksViewPagerAdapter.updateCalendarEvents(items)
    }

    /**
     * Set calendar current page
     */
    fun setCalendarSelectedRange(position: Int) {
        monthYearFormatter.also { setMonthYear(getDateForMonthYearTitle(position, currentDate), it) }

        daysViewpager.apply {
            clearOnPageChangeListeners()
            setCurrentItem(position, false)
            addOnPageChangeListener(pageChangeListener)
        }
    }

    /**
     * Update calendar mode
     */
    fun updateCalendarMode(mode: CalendarMode) {
        this.mode = mode

        setCalendarHeight(mode, weekDayHeight)
        setMonthYear(
            getDateForMonthYearTitle(daysViewpager.currentItem, currentDate),
            monthYearFormatter
        )
    }

    fun refresh() {
        weeksViewPagerAdapter.notifyDataSetChanged()
    }

    private fun init(attrs: AttributeSet? = null) {
        setupBinding()

        context.withStyledAttributes(attrs, R.styleable.CalendarView) {
            weekDayHeight = getDimension(
                R.styleable.CalendarView_week_day_height,
                context.resources.getDimension(R.dimen.spacing_xxxlarge)
            ).roundToInt()
        }

        setCalendarHeight(mode, weekDayHeight)

        weekDaysTitleRV.apply {
            layoutManager = GridLayoutManager(context, DayOfWeek.values().size, RecyclerView.VERTICAL, false)
            adapter = WeekDaysTitleAdapter()
        }

        daysViewpager.apply {
            adapter = weeksViewPagerAdapter
            addOnPageChangeListener(pageChangeListener)
        }

        setCalendarSelectedRange(MIDDLE_OF_CALENDAR_RANGE)
        initViewControls()
    }

    private fun setupBinding() {
        val binding = ViewCalendarBinding.inflate(LayoutInflater.from(context), this, true)
        arrowLeft = binding.arrowLeft
        arrowRight = binding.arrowRight
        daysViewpager = binding.daysViewpager
        monthYear = binding.monthYear
        weekDaysTitleRV = binding.weekDaysTitleRv
    }

    private fun setCalendarHeight(mode: CalendarMode, height: Int) {
        daysViewpager.layoutParams.height = when (mode) {
            CalendarMode.TWO_WEEKS -> height * ROWS_COUNT_FOR_TWO_WEEKS
            CalendarMode.MONTH -> height * ROWS_COUNT_FOR_MONTH
        }
    }

    private fun getDateForMonthYearTitle(position: Int, currentDate: LocalDate): LocalDate {
        // hit to last date in week to get month for two weeks (zero based)
        return mode.getFirstDateOfRange(currentDate, position).plusDays(DayOfWeek.values().size - 1L)
    }

    private fun initViewControls() {
        monthYear.setOnClickListener {
            daysViewpager.setCurrentItem(MIDDLE_OF_CALENDAR_RANGE, false)

            weeksViewPagerAdapter.handleSelectedDate(currentDate)
        }
        arrowLeft.setOnClickListener { daysViewpager.setCurrentItem(daysViewpager.currentItem - 1, true) }
        arrowRight.setOnClickListener { daysViewpager.setCurrentItem(daysViewpager.currentItem + 1, true) }
    }

    private fun setMonthYear(date: LocalDate, monthYearFormatter: (LocalDate) -> String) {
        monthYear.text = monthYearFormatter(date)
    }
}
