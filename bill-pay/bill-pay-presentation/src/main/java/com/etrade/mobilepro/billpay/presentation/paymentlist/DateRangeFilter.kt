package com.etrade.mobilepro.billpay.presentation.paymentlist

import android.content.res.Resources
import androidx.annotation.StringRes
import com.etrade.mobilepro.billpay.presentation.R

@Suppress("MagicNumber")
enum class DateRangeFilter(@StringRes val labelRes: Int, val days: Int, val rangeName: String) {
    DAYS_30(R.string.bill_pay_status_datarange_30days, 30, "DAYS_30"),
    DAYS_60(R.string.bill_pay_status_datarange_60days, 60, "DAYS_60"),
    DAYS_90(R.string.bill_pay_status_datarange_90days, 90, "DAYS_90"),
    DAYS_180(R.string.bill_pay_status_datarange_6months, 180, "DAYS_180"),
    DAYS_365(R.string.bill_pay_status_datarange_12months, 365, "DAYS_365");

    companion object {
        fun labels(resources: Resources): List<String> = values()
            .map { resources.getString(it.labelRes) }

        private fun getIndexByLabel(resources: Resources, title: String) = labels(resources).indexOf(title)

        fun getDateRangeByLabel(resources: Resources, title: String) = values()[getIndexByLabel(resources, title)]

        fun fromRangeName(rangeName: String): DateRangeFilter? {
            return values().firstOrNull { rangeName == it.rangeName }
        }
    }
}
