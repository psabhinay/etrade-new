package com.etrade.mobilepro.landing.api.repo

import com.etrade.mobilepro.landing.api.LandingPage

interface LandingPagesDataSource : UserAccountsDataSource {
    fun getLandingPages(): List<LandingPage>
    fun getDefaultPage(): LandingPage
    @Suppress("LongParameterList")
    fun createPageFrom(
        id: String,
        uuid: String = "",
        accountId: String? = null,
        title: String? = null,
        contentDescription: String? = null,
        subItems: List<LandingPage>? = null
    ): LandingPage
}
