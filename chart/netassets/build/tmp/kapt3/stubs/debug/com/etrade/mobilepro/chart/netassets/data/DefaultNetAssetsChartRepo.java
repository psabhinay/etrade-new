package com.etrade.mobilepro.chart.netassets.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nH\u0002J%\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000fR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0010"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/data/DefaultNetAssetsChartRepo;", "Lcom/etrade/mobilepro/chart/netassets/data/NetAssetsChartRepo;", "retrofit", "Lretrofit2/Retrofit;", "(Lretrofit2/Retrofit;)V", "chartApiClient", "Lcom/etrade/mobilepro/chart/netassets/data/NetAssetsChartApiClient;", "createRequest", "Lcom/etrade/mobilepro/chart/data/ChartRequestDto;", "accounts", "", "", "getNetAssetsChart", "Lcom/etrade/mobilepro/util/Resource;", "Lcom/etrade/mobilepro/chart/netassets/model/NetAssetsChartData;", "(Ljava/util/List;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "netassets_debug"})
public final class DefaultNetAssetsChartRepo implements com.etrade.mobilepro.chart.netassets.data.NetAssetsChartRepo {
    private final com.etrade.mobilepro.chart.netassets.data.NetAssetsChartApiClient chartApiClient = null;
    
    @javax.inject.Inject()
    public DefaultNetAssetsChartRepo(@org.jetbrains.annotations.NotNull()
    @com.etrade.mobilepro.common.di.MobileTrade()
    retrofit2.Retrofit retrofit) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getNetAssetsChart(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> accounts, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.etrade.mobilepro.util.Resource<com.etrade.mobilepro.chart.netassets.model.NetAssetsChartData>> continuation) {
        return null;
    }
    
    private final com.etrade.mobilepro.chart.data.ChartRequestDto createRequest(java.util.List<java.lang.String> accounts) {
        return null;
    }
}