package com.etrade.mobilepro.movers.widget

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.etrade.mobilepro.compose.components.ThickDivider
import com.etrade.mobilepro.compose.components.UiStateBox
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.movers.MoverGridData
import com.etrade.mobilepro.movers.MoverItem
import com.etrade.mobilepro.movers.compose.FixedMoverGrid
import com.etrade.mobilepro.movers.compose.MoverGrid

@SuppressWarnings("LongParameterList")
@Composable
fun MoversWidget(
    title: String,
    data: MoverGridData,
    modifier: Modifier,
    state: DynamicUiViewState,
    onRetry: () -> Unit,
    onItemTap: ((MoverItem) -> Unit)?
) {
    MoversWidgetSkeleton(title, data, modifier, state, onRetry) {
        MoverGrid(data, onTap = onItemTap)
    }
}

@SuppressWarnings("LongParameterList", "FunctionNaming")
fun LazyListScope.MoversWidgetItem(
    title: String,
    data: MoverGridData,
    modifier: Modifier,
    state: DynamicUiViewState,
    onRetry: () -> Unit,
    onItemTap: ((MoverItem) -> Unit)?
) {
    item {
        MoversWidgetSkeleton(title, data, modifier, state, onRetry) {
            FixedMoverGrid(data, onTap = onItemTap)
        }
    }
}

@SuppressWarnings("LongParameterList")
@Composable
private fun MoversWidgetSkeleton(
    title: String,
    data: MoverGridData,
    modifier: Modifier,
    state: DynamicUiViewState,
    onRetry: () -> Unit,
    content: @Composable () -> Unit,
) {
    if (data.items.isNotEmpty()) {
        Column(modifier = modifier) {
            ThickDivider()
            Text(
                title,
                style = moverWidgetTitleTextStyle(),
                modifier = Modifier.padding(
                    top = dimensionResource(R.dimen.spacing_medium),
                    start = dimensionResource(R.dimen.spacing_medium),
                    end = dimensionResource(R.dimen.spacing_medium)
                )
            )
            UiStateBox(
                state = state,
                onRetry = onRetry,
                modifier = Modifier.fillMaxWidth(),
                successContent = content
            )
        }
    }
}

// once all et redesign language is ported to Compose use that and remove this
@Composable
@Suppress("MagicNumber")
private fun moverWidgetTitleTextStyle() = MaterialTheme.typography.subtitle2.copy(
    fontSize = 18.sp,
    fontWeight = FontWeight(500),
)
