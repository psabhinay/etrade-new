package com.etrade.mobilepro.dao.realm

import androidx.lifecycle.LiveData
import io.realm.RealmChangeListener
import io.realm.RealmModel
import io.realm.RealmResults

open class RealmLiveData<T : RealmModel>(
    private val results: RealmResults<T>
) : LiveData<List<T>>() {
    init {
        value = results
    }

    private val listener = RealmChangeListener<RealmResults<T>> {
        results ->
        value = results.realm.copyFromRealm(results)
    }

    override fun onActive() {
        results.addChangeListener(listener)

        value = results.realm.copyFromRealm(results)
    }

    override fun onInactive() {
        results.removeChangeListener(listener)
    }
}

fun <T : RealmModel> RealmResults<T>.asLiveData(): LiveData<List<T>> = RealmLiveData(this)
