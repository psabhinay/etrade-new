package com.etrade.mobilepro.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import com.etrade.mobilepro.home.overview.viewfactory.FullTimestampFormatterType
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.TimestampEvent
import com.etrade.mobilepro.util.android.TimestampEventProvider
import com.etrade.mobilepro.util.android.consume
import java.util.concurrent.TimeUnit
import javax.inject.Inject

interface HasTimeStamp {
    val timestamp: LiveData<String>
}

interface TimeStampReporter : HasTimeStamp {
    fun addTimestampEventProviders(providers: List<TimestampEventProvider>)
    fun refresh()
}

private const val QUOTE_DELAY_MINUTES = 15L

class DefaultTimeStampReporter @Inject constructor(
    private val user: User,
    private val timestampFormatter: FullTimestampFormatterType
) : TimeStampReporter {
    private val quotesTimeDelayMs = TimeUnit.MINUTES.toMillis(QUOTE_DELAY_MINUTES)
    private var lastTimestamp = 0L
    private val aggregatedTimestampEvents = MediatorLiveData<String>().apply {
        value = timestampFormatter(getCurrentTimestamp())
    }
    private val eventsObserver = Observer<ConsumableLiveEvent<TimestampEvent>> {
        it.consume { event ->
            if (event.timestamp > lastTimestamp) {
                lastTimestamp = event.timestamp
                aggregatedTimestampEvents.value = timestampFormatter(event.timestamp)
            }
        }
    }
    override val timestamp: LiveData<String>
        get() = aggregatedTimestampEvents

    override fun addTimestampEventProviders(providers: List<TimestampEventProvider>) = providers.forEach { provider ->
        aggregatedTimestampEvents.addSource(provider.timestampEvents, eventsObserver)
    }

    override fun refresh() {
        aggregatedTimestampEvents.value = timestampFormatter(getCurrentTimestamp())
    }

    private fun getCurrentTimestamp(): Long {
        var timestamp = System.currentTimeMillis()
        if (!user.isRealtimeUser) {
            timestamp -= quotesTimeDelayMs
        }
        return timestamp
    }
}
