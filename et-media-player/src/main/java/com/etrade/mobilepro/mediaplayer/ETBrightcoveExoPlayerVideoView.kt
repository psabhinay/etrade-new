package com.etrade.mobilepro.mediaplayer

import android.content.Context
import android.util.AttributeSet
import com.brightcove.player.view.BrightcoveExoPlayerVideoView
import com.brightcove.player.view.RenderView

/**
 * This class is needed to fix a bug where the video zooms in on content loading stopped. This may be a bug within the Brightcove API.
 *
 * See: https://jira.corp.etradegrp.com/browse/ETAND-3125
 */
class ETBrightcoveExoPlayerVideoView : BrightcoveExoPlayerVideoView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun getRenderViewClass(): Class<out RenderView> {
        return ETBrightcoveSurfaceView::class.java
    }
}
