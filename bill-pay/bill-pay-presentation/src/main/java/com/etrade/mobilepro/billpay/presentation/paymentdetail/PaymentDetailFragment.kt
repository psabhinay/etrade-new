package com.etrade.mobilepro.billpay.presentation.paymentdetail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.billpay.api.PaymentStatus
import com.etrade.mobilepro.billpay.presentation.BillPayViewModel
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.billpay.presentation.common.NavigationViewModel
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayFragmentPaymentDetailBinding
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

private val DIALOG_TAG = "${PaymentDetailFragment::class.java.canonicalName}.cancel_payment_tag"
private const val DIALOG_REQUEST_CODE = 334

@RequireLogin
class PaymentDetailFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : OverlayBaseFragment() {

    private val binding by viewBinding(BillPayFragmentPaymentDetailBinding::bind)

    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.bill_pay_nav_graph)
    private val sharedViewModel: BillPayViewModel by navGraphViewModels(R.id.bill_pay_nav_graph) { viewModelFactory }
    private val viewModel: PaymentDetailViewModel by viewModels { viewModelFactory }

    override val layoutRes: Int = R.layout.bill_pay_fragment_payment_detail
    private val args: PaymentDetailFragmentArgs by navArgs()
    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private val parentSnackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { activity },
            { view }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        setupViewModel()
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                DIALOG_REQUEST_CODE -> {
                    viewModel.cancelPayment(
                        requireNotNull(sharedViewModel.sessionId),
                        requireNotNull(args.paymentDetail.id)
                    )
                }
            }
        }
    }

    private fun setupFragment() {
        binding.paymentDetailSections.setContent(
            args.paymentDetail.toListOfTableSectionItemLayout(
                resources
            )
        )
        if (args.paymentDetail.isActionButtonsVisible) {
            binding.paymentDetailCancel.visibility = View.VISIBLE
            binding.paymentDetailModify.visibility = View.VISIBLE
        }

        binding.paymentDetailModify.setOnClickListener {
            args.paymentDetail.let {
                findNavController().navigate(PaymentDetailFragmentDirections.billPayActionPaymentdetailfragmentToPaymenteditfragment(it))
            }
        }
        binding.paymentDetailCancel.setOnClickListener { showCancelDialog() }
    }

    private fun setupViewModel() {
        viewModel.viewState.observe(viewLifecycleOwner, Observer { render(it) })
    }

    private fun showCancelDialog() {
        val dialog = CustomDialogFragment.newInstance(
            title = getString(R.string.bill_pay_cancel_payment),
            message = getString(R.string.bill_pay_cancel_payment_message),
            cancelTitle = getString(R.string.no),
            okTitle = getString(R.string.yes),
            requestCode = DIALOG_REQUEST_CODE
        )

        dialog.show(parentFragmentManager, DIALOG_TAG)
    }

    private fun render(state: PaymentDetailViewState) {
        handleError(state.error)
        handleViewVisibility(state)
        handlePaymentCancelled(state.paymentCancelled)
    }

    private fun handleError(error: ConsumableLiveEvent<Unit>?) {
        error?.getNonConsumedContent()?.let {
            snackBarUtil.errorSnackbar()
        }
    }

    private fun handleViewVisibility(state: PaymentDetailViewState) {
        if (
            args.paymentDetail.status == PaymentStatus.SCHEDULED &&
            !state.isLoading &&
            state.paymentCancelled?.peekContent() == null
        ) {
            binding.paymentDetailModify.visibility = View.VISIBLE
            binding.paymentDetailCancel.visibility = View.VISIBLE
        } else {
            binding.paymentDetailModify.visibility = View.INVISIBLE
            binding.paymentDetailCancel.visibility = View.INVISIBLE
        }

        if (state.isLoading) {
            binding.paymentDetailSv.visibility = View.INVISIBLE
            binding.progressBar.show()
        } else {
            binding.paymentDetailSv.visibility = View.VISIBLE
            binding.progressBar.hide()
        }
    }

    private fun handlePaymentCancelled(paymentCancelled: ConsumableLiveEvent<Unit>?) {
        paymentCancelled?.getNonConsumedContent()?.let {
            parentSnackBarUtil.snackbar(getString(R.string.bill_pay_payment_cancelled), duration = Snackbar.LENGTH_LONG)
            navigationViewModel.refreshPayments()
            findNavController().popBackStack()
        }
    }
}
