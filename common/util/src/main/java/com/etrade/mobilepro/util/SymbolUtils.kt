package com.etrade.mobilepro.util

import java.util.regex.Pattern

private val symbolInStockPlanRegex: Pattern by lazy { Pattern.compile("\\((.*?)\\)") }
fun getStockPlanSymbolFromTitle(input: String): String {
    val m = symbolInStockPlanRegex.matcher(input)
    return if (m.find()) {
        m.group(1) ?: ""
    } else {
        ""
    }
}
