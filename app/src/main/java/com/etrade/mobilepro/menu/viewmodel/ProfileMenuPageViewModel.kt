package com.etrade.mobilepro.menu.viewmodel

import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.R
import com.etrade.mobilepro.R.string.menu_url_account_upgrade
import com.etrade.mobilepro.R.string.menu_url_change_password
import com.etrade.mobilepro.R.string.menu_url_change_username
import com.etrade.mobilepro.R.string.menu_url_drip
import com.etrade.mobilepro.R.string.menu_url_extended_hours_trade_agreement
import com.etrade.mobilepro.R.string.menu_url_manage_beneficiaries
import com.etrade.mobilepro.R.string.menu_url_manage_debit_cards
import com.etrade.mobilepro.R.string.menu_url_manage_subscriptions
import com.etrade.mobilepro.R.string.menu_url_update_personal_information
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.common.di.EDocWeb
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.ProfileMenuFragmentDirections
import com.etrade.mobilepro.menu.builder.TextMenuItemBuilder
import com.etrade.mobilepro.menu.builder.menuPage
import com.etrade.mobilepro.msaccounts.api.MsPreferencesRepo
import com.etrade.mobilepro.msaccounts.api.getSettingsCta
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsWebViewDirections
import com.etrade.mobilepro.tracking.SCREEN_TITLE_DIVIDEND_REINVESTMENT
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MARGIN_ACCOUNT_UPGRADE
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MY_PROFILE_MANAGE_BENEFICIARIES
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MY_PROFILE_MANAGE_DEBIT_CARD
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MY_PROFILE_MANAGE_SUBSCRIPTIONS
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MY_PROFILE_OPTIONS_UPGRADE_BROKERAGE
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MY_PROFILE_OPTIONS_UPGRADE_IRA
import com.etrade.mobilepro.tracking.SCREEN_TITLE_UPDATE_PERSONAL_INFORMATION
import com.etrade.mobilepro.tracking.SCREEN_TITLE_VIEW_ACCOUNT_NUMBER
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.prependBaseUrl
import javax.inject.Inject

class ProfileMenuPageViewModel @Inject constructor(
    resources: Resources,
    tracker: Tracker,
    @Web private val baseUrl: String,
    @EDocWeb private val eDocBaseUrl: String,
    private val msPreferencesRepo: MsPreferencesRepo
) : AppBaseMenuPageViewModel(resources, tracker) {

    override val menuPageItems: LiveData<List<MenuItem>> by lazy { MutableLiveData(getMenuPageItems()) }

    @SuppressWarnings("LongMethod")
    private fun getMenuPageItems(): List<MenuItem> {
        return menuPage {
            // Update Personal Information
            textWithArrowItem(R.id.profileMenuUpdatePersonalInformation) {
                text(R.string.menu_update_personal_information)
                accountWebNav(url(menu_url_update_personal_information))
                trackScreen(SCREEN_TITLE_UPDATE_PERSONAL_INFORMATION)
            }
            // Change Username
            textWithArrowItem(R.id.profileMenuChangeUsername) {
                text(R.string.menu_change_username_title)
                accountWebNav(url(menu_url_change_username))
            }
            // Change Password
            textWithArrowItem(R.id.profileMenuChangePassword) {
                text(R.string.menu_change_password_title)
                accountWebNav(url(menu_url_change_password))
            }

            // Manage Beneficiaries
            textWithArrowItem(R.id.profileMenuManageBeneficiaries) {
                text(R.string.menu_manage_beneficiaries_title)
                accountWebNav(url(menu_url_manage_beneficiaries))
                trackScreen(SCREEN_TITLE_MY_PROFILE_MANAGE_BENEFICIARIES)
            }

            // Morgan Stanley Settings
            val msPreferences = msPreferencesRepo.msUserPreferences.value
            val msSettingsCta = msPreferences.getSettingsCta()
            if (msPreferences.isMsEligible && msSettingsCta != null) {
                textWithArrowItem(R.id.profileMenuMorganStanleySettings) {
                    text = msSettingsCta.title
                    navigateToMsAccountsWebView(msSettingsCta)
                    shouldSaveDirections = false
                }
            }

            divider()

            // View Account Number
            textWithArrowItem {
                navDirections(ProfileMenuFragmentDirections.actionProfileMenuToAccountDetails())
                text(R.string.menu_account_details_title)
                trackScreen(SCREEN_TITLE_VIEW_ACCOUNT_NUMBER)
            }
            // Dividend Reinvestment (DRIP)
            textWithArrowItem(R.id.profileMenuDrip) {
                text(R.string.forms_and_applications_dividend_reinvestment)
                accountWebNav(
                    url(menu_url_drip),
                    R.string.forms_and_applications_dividend_reinvestment_short
                )
                trackScreen(SCREEN_TITLE_DIVIDEND_REINVESTMENT)
            }
            // Options Approval Upgrade (Brokerage)
            textWithArrowItem(R.id.profileMenuBrokerageUpgrade) {
                text(R.string.forms_and_applications_options_brokerage)
                accountWebNav(
                    upgradeUrl(BROKERAGE_UPGRADE),
                    R.string.forms_and_applications_options_brokerage_short
                )
                trackScreen(SCREEN_TITLE_MY_PROFILE_OPTIONS_UPGRADE_BROKERAGE)
            }
            // Options Approval Upgrade (IRA)
            textWithArrowItem(R.id.profileMenuIraUpgrade) {
                text(R.string.forms_and_applications_options_ira)
                accountWebNav(
                    upgradeUrl(IRA_UPGRADE),
                    R.string.forms_and_applications_options_ira_short
                )
                trackScreen(SCREEN_TITLE_MY_PROFILE_OPTIONS_UPGRADE_IRA)
            }
            // Margin Account Upgrade
            textWithArrowItem(R.id.profileMenuMarginUpgrade) {
                text(R.string.forms_and_applications_margin_account_upgrade)
                accountWebNav(
                    upgradeUrl(MARGIN_UPGRADE),
                    R.string.forms_and_applications_margin_account_upgrade_short
                )
                trackScreen(SCREEN_TITLE_MARGIN_ACCOUNT_UPGRADE)
            }
            // Manage Subscriptions
            textWithArrowItem(R.id.profileMenuManageSubscriptions) {
                text(R.string.manage_subscriptions)
                accountWebNav(url(menu_url_manage_subscriptions))
                trackScreen(SCREEN_TITLE_MY_PROFILE_MANAGE_SUBSCRIPTIONS)
            }
            // Manage Your Debit Card
            textWithArrowItem(R.id.profileMenuManageDebitCard) {
                text(R.string.menu_manage_debit_card_title)
                accountWebNav(url(menu_url_manage_debit_cards))
                trackScreen(SCREEN_TITLE_MY_PROFILE_MANAGE_DEBIT_CARD)
            }
            // Extended Hours Disclosure
            textWithArrowItem(R.id.profileMenuExtendedHoursDisclosure) {
                text(R.string.menu_extended_hours_trade_agreement_title)
                extendedHoursDisclosureWebNav(url(menu_url_extended_hours_trade_agreement))
            }
            // Paperless Settings
            if (eDocBaseUrl.isNotBlank()) {
                textWithArrowItem(R.id.profileMenuPaperlessSettings) {
                    text(R.string.menu_paperless_settings_title)
                    accountWebNav(url(R.string.menu_url_paperless_settings, eDocBaseUrl))
                }
            }
        }
    }

    private fun url(@StringRes id: Int, baseUrl: String = this.baseUrl) = resources.getString(id, baseUrl)

    private fun upgradeUrl(index: Int): String = resources.getString(menu_url_account_upgrade, baseUrl, index)

    private fun TextMenuItemBuilder.navigateToMsAccountsWebView(cta: CallToActionDto) {
        val webViewUrl = cta.clickActionDto.webViewUrl.prependBaseUrl(baseUrl)
        val navDirection = MsAccountsWebViewDirections.actionHomeToMsaccountsWebView(
            title = cta.title ?: "",
            url = webViewUrl
        )
        navDirections(navDirection)
    }

    companion object {
        private const val MARGIN_UPGRADE = 1
        private const val BROKERAGE_UPGRADE = 2
        private const val IRA_UPGRADE = 3
    }
}
