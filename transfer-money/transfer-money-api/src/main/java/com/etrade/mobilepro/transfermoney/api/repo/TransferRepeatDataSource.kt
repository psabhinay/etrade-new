package com.etrade.mobilepro.transfermoney.api.repo

import com.etrade.mobilepro.transfermoney.api.TransferRepeat

interface TransferRepeatDataSource {
    suspend fun getTransferRepeats(): List<TransferRepeat>
}
