package com.etrade.mobilepro.tableviewutils.presentation.util

import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell

class ColumnRowSizeCache constructor(
    private val defaultWidth: Int,
    private val defaultRowHeight: Int,
    defaultColumnHeaderHeight: Int,
    private val measureCellWidth: (cell: DataCell, row: Int, column: Int) -> Int,
    private val measureHeaderWidth: (cell: Cell, position: Int) -> Int,
    private val measureColumnHeaderHeight: (cell: Cell) -> Int,
    private val measureRowHeaderHeight: (cell: Cell, row: Int) -> Int,
    private val measureCornerViewHeight: () -> Int
) {

    var widths: IntArray = IntArray(0) { defaultWidth }

    // K - RowHeaderCell hash code; V - row height value
    private var rowHeights = mutableMapOf<Int, Int>()
    var columnHeaderHeight = defaultColumnHeaderHeight
        private set

    fun measure(
        parentWidth: Int,
        columns: List<Cell>,
        rowHeaderItems: List<Cell>,
        rows: List<List<DataCell>>,
        getCustomWidth: (Int, Cell) -> Int?
    ) {
        measureColumnWidths(columns, rows, getCustomWidth)
        adjustColumnWidthsToMatchParentWidth(parentWidth)
        measureColumnHeaderAndCornerViewHeight(columns)
        measureRowHeights(rowHeaderItems)
    }

    operator fun get(column: Int): Int = widths.getOrNull(column) ?: defaultWidth

    fun getRowHeight(rowHeaderCell: Cell): Int = rowHeights[rowHeaderCell.hashCode()] ?: defaultRowHeight

    private fun measureWidth(columnIndex: Int, rowIndex: Int, cell: DataCell): Int {
        var width = widths[columnIndex]
        val desiredWidth = measureCellWidth(cell, rowIndex, columnIndex)

        if (width < desiredWidth) {
            width = desiredWidth
        }

        return width
    }

    private fun measureColumnWidths(
        columns: List<Cell>,
        rows: List<List<DataCell>>,
        getCustomWidth: (Int, Cell) -> Int?
    ) {
        if (widths.size != columns.size) {
            widths = IntArray(columns.size) { defaultWidth }
        }

        columns.forEachIndexed { columnIndex, columnHeader ->
            val measuredColumnWidth = measureHeaderWidth(columnHeader, columnIndex)

            if (measuredColumnWidth > widths[columnIndex]) {
                widths[columnIndex] = measuredColumnWidth
            }
            rows.forEachIndexed { rowIndex, row ->
                widths[columnIndex] = getCustomWidth(columnIndex, columnHeader) ?: measureWidth(
                    columnIndex,
                    rowIndex,
                    row[columnIndex]
                )
            }
        }
    }

    private fun measureColumnHeaderAndCornerViewHeight(columns: List<Cell>) {
        measureCornerViewHeight().apply {
            if (this > columnHeaderHeight) {
                columnHeaderHeight = this
            }
        }
        columns.forEach { columnHeader ->
            val height = measureColumnHeaderHeight(columnHeader)
            if (height > columnHeaderHeight) {
                columnHeaderHeight = height
            }
        }
    }

    private fun measureRowHeights(rowHeaderItems: List<Cell>) {
        rowHeights.clear()
        rowHeaderItems.forEachIndexed { index, cell ->
            rowHeights[cell.hashCode()] = measureRowHeaderHeight(cell, index)
        }
    }

    /**
     * It is used to adjust column widths in case the total width of all columns is less than table view width.
     * So in such way the final total width should be equal to table view width.
     *
     * * @return `true` if any column width value is changed, `false` otherwise.
     */
    internal fun adjustColumnWidthsToMatchParentWidth(parentWidth: Int): Boolean {
        val columnAmount = widths.size
        val extraSpaceWidth = parentWidth - widths.sum()

        return if (columnAmount > 0 && extraSpaceWidth > 0) {
            val adjustmentValue = extraSpaceWidth / columnAmount
            val lastColumnAdjustment = extraSpaceWidth.rem(columnAmount) + adjustmentValue
            widths.forEachIndexed { index, width ->
                val adjustment = if (index == columnAmount - 1) lastColumnAdjustment else adjustmentValue
                widths[index] = width + adjustment
            }

            true
        } else {
            false
        }
    }
}
