package com.etrade.mobilepro.completeview

import android.content.res.Resources
import androidx.annotation.LayoutRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import com.etrade.completeview.dto.AccountSummaryDto
import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.backends.mgs.MOVEMENT_TYPE_NEUTRAL
import com.etrade.mobilepro.backends.mgs.StreamingValueDto
import com.etrade.mobilepro.completeview.model.AccountSummaryData
import com.etrade.mobilepro.completeview.model.AccountSummarySection
import com.etrade.mobilepro.completeview.model.AccountSummaryViewModel
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dummy.createAccountBalanceStreamId
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.api.DynamicProperty
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.flip
import com.etrade.mobilepro.inboxmessages.dynamicui.api.lowcash.LowCashPrompt
import com.etrade.mobilepro.portfolio.model.dto.getExtendPortfolioSummaryCta
import com.etrade.mobilepro.streaming.api.StreamingText
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import com.etrade.mobilepro.streaming.client.side.calculations.ALL_BROKERAGE_STREAM_ID
import com.etrade.mobilepro.util.android.CallToAction
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory

abstract class AbstractAccountSummaryViewFactory(
    private val daoProvider: CachedDaoProvider,
    private val resources: Resources
) : DynamicUiViewFactory {

    @get:LayoutRes
    abstract val accountSummaryLayoutRes: Int

    abstract val streamType: StreamType

    abstract val streamingTextManager: StreamingTextManager

    protected fun getStreamableAccountId(account: Account?): String? {
        return when {
            account == null && streamType == StreamType.ALL_BROKERAGE_VIEW -> ALL_BROKERAGE_STREAM_ID
            account?.accountType != AccountType.ESP -> account?.accountId
            else -> null // "ESP" accounts are not supported for streaming now, null - to avoid collision with other accountIds
        }
    }

    protected fun checkStreamingPermissionByAccountId(accountId: String?, isAccountStreamingAllowed: Boolean?) =
        accountId == ALL_BROKERAGE_STREAM_ID || isAccountStreamingAllowed ?: false

    @Suppress("LongMethod", "LongParameterList") // just step by step creation
    protected fun toAccountSummary(
        dto: AccountSummaryDto,
        ctaList: List<CallToAction>?,
        clickActionDto: ClickActionDto?,
        accountSourceLabel: String?,
        showName: Boolean = true
    ): AccountSummaryViewModel {
        return AccountSummaryViewModel(
            accountSummaryData = dto.toAccountSummaryData(),
            showName = showName,
            clickAction = clickActionDto?.toClickAction(),
            accountSourceLabel = accountSourceLabel,
            initialCallToAction = ctaList,
            layoutRes = accountSummaryLayoutRes,
            daoProvider = daoProvider,
            resources = resources,
            additionSectionBuilder = {
                createAdditionalSections(it, dto)
            }
        ).apply {
            initialDetailValueSource = getValueAsStreamingText(dto.detailValue, account)
        }
    }

    private fun createAdditionalSections(
        account: Account?,
        dto: AccountSummaryDto
    ): List<AccountSummarySection> {
        val accountId = getStreamableAccountId(account)
        return (dto.additionalSectionDtos + dto.extraDetailsDtos)
            .toAccountSummarySections(
                streamingTextManager,
                accountId,
                isAccountStreamingAllowed = checkStreamingPermissionByAccountId(
                    accountId,
                    account?.isAccountStreamingAllowed
                )
            )
    }

    private fun getValueAsStreamingText(
        value: String?,
        account: Account?
    ): LiveData<StreamingText> {
        val streamableAccountId = getStreamableAccountId(account)
        val streamId = createAccountBalanceStreamId(streamableAccountId)
        val streamingValueDto = StreamingValueDto(value, streamId, MOVEMENT_TYPE_NEUTRAL)
        return getStreamingText(
            streamingTextManager,
            streamingValueDto,
            isAccountStreamingAllowed = checkStreamingPermissionByAccountId(streamableAccountId, account?.isAccountStreamingAllowed)
        )
    }

    protected fun getAccount(accountUuid: String?): Account? = accountUuid?.let {
        daoProvider.getAccountDao().getAccountWithUuid(it)
    }

    protected inner class AccountSummaryDelegate(
        private val dto: AccountSummaryDto,
        private val ctaList: List<CallToActionDto>?,
        private val clickActionDto: ClickActionDto?
    ) : AccountSummaryLayout(dto.accountUuid), DynamicProperty {

        override var shouldHide: Boolean = false

        override val accountSourceLabel = dto.accountSourceLabel?.takeIf { it.isNotEmpty() }

        override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
            toAccountSummary(
                dto,
                ctaList?.map { it.toCallToAction() },
                clickActionDto,
                accountSourceLabel,
                shouldHide.flip()
            )
        }

        override val uniqueIdentifier: String = super.uniqueIdentifier + dto.extraAugmentationInfo.toString()

        override fun refresh(viewModel: DynamicViewModel) {
            if (viewModel is AccountSummaryViewModel) {
                updateViewModelWithDto(dto, viewModel, ctaList)

                val actions = ctaList?.map { it.toCallToAction() }
                viewModel.updateActions(actions)
            }
        }

        override val viewModelClass = AccountSummaryViewModel::class.java
    }

    protected fun updateViewModelWithDto(
        dto: AccountSummaryDto,
        viewModel: AccountSummaryViewModel,
        ctaList: List<CallToActionDto>?
    ) {
        val account = getAccount(dto.accountUuid)

        val detailValue = getValueAsStreamingText(dto.detailValue, account)
        viewModel.updateDetails(dto.detailLabel, detailValue)

        val additionalSections = createAdditionalSections(account, dto)
        viewModel.updateAdditionalSections(additionalSections)

        viewModel.updatePrompt(dto.extraAugmentationInfo.filterIsInstance<LowCashPrompt>().firstOrNull())

        val actions = ctaList?.getExtendPortfolioSummaryCta()
        viewModel.updateActions(actions?.headerViewCtaList)
    }
}

private fun AccountSummaryDto.toAccountSummaryData() = AccountSummaryData(
    accountUuid = accountUuid,
    accountName = name,
    label = detailLabel,
    cashPrompt = extraAugmentationInfo.filterIsInstance<LowCashPrompt>().firstOrNull()
)

enum class StreamType {
    ALL_BROKERAGE_VIEW, COMPLETE_VIEW, PORTFOLIO_VIEW;
}
