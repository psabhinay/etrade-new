package com.etrade.mobilepro.preferences

import kotlin.random.Random

private const val ONBOARDING_AB_MAX = 100

// until is exclusive, if we receive 0 from LL we should not show anything
internal fun getOnboardingAbValue() = Random.nextInt(from = 1, until = ONBOARDING_AB_MAX + 1)
