package com.etrade.mobilepro.trade.api.advanced

import com.etrade.mobilepro.trade.api.OrderResponse

interface AdvancedOrderResponse : OrderResponse {
    override val orders: List<AdvancedOrder>
}
