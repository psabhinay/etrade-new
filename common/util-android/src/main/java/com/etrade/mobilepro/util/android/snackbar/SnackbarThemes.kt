package com.etrade.mobilepro.util.android.snackbar

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import com.etrade.mobilepro.util.android.R
import com.google.android.material.snackbar.Snackbar

fun makeThemedSnackbar(view: View, message: CharSequence, duration: Int, theme: SnackbarTheme): Snackbar {
    val snackBar = Snackbar.make(view, message, duration)
    snackBar.setActionTextColor(theme.actionColor)
    snackBar.view.findViewById<TextView>(com.google.android.material.R.id.snackbar_text).setTextColor(theme.textColor)
    theme.backgroundColor?.let { backgroundColor ->
        snackBar.view.setBackgroundColor(backgroundColor)
    }
    return snackBar
}

fun createSnackbarTheme(context: Context): SnackbarTheme {
    val primaryColor = ContextCompat.getColor(context, R.color.snack_bar_primary)
    val primaryTextInverse = ContextCompat.getColor(context, R.color.snack_bar_primary_inverse)
    val backgroundColor = ContextCompat.getColor(context, R.color.snack_bar_background)
    return SnackbarTheme(primaryTextInverse, primaryColor, backgroundColor)
}

data class SnackbarTheme(
    @ColorInt
    val textColor: Int,
    @ColorInt
    val actionColor: Int,
    @ColorInt
    val backgroundColor: Int?
)
