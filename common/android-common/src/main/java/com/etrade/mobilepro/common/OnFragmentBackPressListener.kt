package com.etrade.mobilepro.common

interface OnFragmentBackPressListener {

    fun onBackPressed(): Boolean
}
