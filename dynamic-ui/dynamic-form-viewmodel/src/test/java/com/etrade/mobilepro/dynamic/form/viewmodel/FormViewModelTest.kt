package com.etrade.mobilepro.dynamic.form.viewmodel

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.util.ErrorMessage
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class FormViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Before
    fun setUp() {
        Dispatchers.setMain(testCoroutineDispatcher)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `check initial state`() {
        TestFormViewModel(mockResources(), mock()).run {
            assertEquals(false, loadingIndicator.value)
            assertNull(appMessageSignal.value)
            assertNull(form.value)
        }
    }

    @Test
    fun `check loading indicator on fetch`() {
        checkLoadingIndicator(ETResult.success(MutableLiveData(Form(emptyList(), emptyList(), emptyList()))))
        checkLoadingIndicator(ETResult.failure(RuntimeException()))
    }

    private fun checkLoadingIndicator(createStockTradeFormUseCaseResult: ETResult<LiveData<Form>>) = runBlockingTest(testCoroutineDispatcher) {
        createViewModel(createStockTradeFormUseCaseResult).run {
            val loadingIndicatorObserver = loadingIndicator.test()
            fetch()
            assertEquals(listOf(false, true, false), loadingIndicatorObserver.assertHistorySize(3).valueHistory())
        }
    }

    @Test
    fun `check error state`() = runBlockingTest(testCoroutineDispatcher) {
        createViewModel(ETResult.failure(RuntimeException())).run {
            val errorObserver = appMessageSignal.test()
            fetch()
            errorObserver.assertValue {
                (it as? ErrorMessage)?.message == R.string.error_message_general.toString()
            }
        }
    }

    private suspend fun createViewModel(createStockTradeFormUseCaseResult: ETResult<LiveData<Form>>): FormViewModel {
        val createStockTradeFormUseCase = mock<CreateFormUseCase>().apply {
            whenever(execute(any())).thenReturn(createStockTradeFormUseCaseResult)
        }
        return TestFormViewModel(mockResources(), createStockTradeFormUseCase)
    }

    private class TestFormViewModel(resources: Resources, createFormUseCase: CreateFormUseCase) : FormViewModel(resources, createFormUseCase) {

        override fun onActionClick(actionId: Any) = Unit

        override fun addDefaultValuesIfNeeded(form: Form, initialValues: InputValuesSnapshot): InputValuesSnapshot = initialValues

        override fun createParameter(
            initialValues: InputValuesSnapshot,
            defaultValues: InputValuesSnapshot,
            disableIfNoChange: Boolean,
            onActionClickListener: OnActionClickListener
        ): CreateFormUseCaseParameter = mock()
    }
}
