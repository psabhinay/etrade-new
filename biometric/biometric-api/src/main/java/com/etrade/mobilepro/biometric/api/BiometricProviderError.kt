package com.etrade.mobilepro.biometric.api

enum class BiometricProviderError {

    /**
     * The user does not have any biometrics enrolled.
     */
    ERROR_NO_BIOMETRICS,

    /**
     * The device does not have a biometric sensor.
     */
    ERROR_HW_NOT_PRESENT,

    /**
     * The hardware is unavailable. Try again later.
     */
    ERROR_HW_UNAVAILABLE,

    /**
     * There is no hardware backed keystore
     */
    ERROR_BIOMETRIC_NO_HW_BACKED_KEYSTORE,

    /**
     * To show generic error message
     */
    ERROR_BIOMETRIC_GENERIC
}
