package com.etrade.mobilepro.backends.neo

import com.etrade.mobilepro.backends.api.ServerMessage
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.util.Locale

@JsonClass(generateAdapter = true)
data class MessageDto(

    @Json(name = "code")
    val code: String?,

    @Json(name = "type")
    val type: String?,

    @Json(name = "subType")
    val subType: String?,

    @Json(name = "priority")
    val priority: Int?,

    @Json(name = "text")
    val text: String?
)

val List<MessageDto?>.errors: List<ServerMessage>
    get() = toServerMessages(ServerMessage.Type.ERROR)

val List<MessageDto?>.warnings: List<ServerMessage>
    get() = toServerMessages(ServerMessage.Type.WARN)

fun MessageDto.toServerMessage(): ServerMessage {
    return ServerMessage(
        type = messageTypeFromString(type),
        text = text.orEmpty(),
        priority = priority ?: 0,
        code = code
    )
}

internal fun messageTypeFromString(input: String?): ServerMessage.Type {
    val raw = input?.uppercase(Locale.ROOT) ?: return ServerMessage.Type.ERROR
    ServerMessage.Type.values().forEach {
        if (it.name == raw) {
            return it
        }
    }
    return ServerMessage.Type.ERROR
}

private fun List<MessageDto?>.toServerMessages(type: ServerMessage.Type): List<ServerMessage> {
    return asSequence()
        .mapNotNull { it?.toServerMessage() }
        .filter { it.type == type }
        .toList()
}
