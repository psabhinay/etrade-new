package com.etrade.mobilepro.screener.presentation.mutualfund.fundprofile

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.FundFamily
import com.etrade.mobilepro.screener.api.MutualFundGroup
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.datasource.MutualFundDataSource
import com.etrade.mobilepro.screener.api.filter.mutualfund.FundProfileFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.MutualFund
import com.etrade.mobilepro.screener.api.repo.FundCategoriesRepo
import com.etrade.mobilepro.screener.api.toggle
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import com.etrade.mobilepro.screener.presentation.common.SelectionFlowHandler
import javax.inject.Inject

class FundProfileViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @MutualFund screenerInteractor: ScreenerInteractor,
    private val fundDataSource: MutualFundDataSource,
    private val fundCategoriesRepo: FundCategoriesRepo,
    private val fundGroupSelectionFlowHandler: SelectionFlowHandler<MutualFundGroup>
) : BaseFilterScreenViewModel<FundProfileFilter, FundProfileViewModel.ViewState>(
    aggregator,
    ScreenerType.MutualFund.FundProfile,
    screenerInteractor
),
    SelectionFlowHandler<MutualFundGroup> by fundGroupSelectionFlowHandler {
    internal var prevFamiliesSelection: Set<FundFamily>? = null
    internal var prevCategorySelection: Set<FundCategory>? = null

    override fun provideInitialState(initialFilter: FundProfileFilter): ViewState {
        if (initialFilter.isEmpty()) {
            preloadItems(fundDataSource.getAllFundGroupsItem())
        }
        return ViewState(filter = initialFilter)
    }

    internal val fundFamilyData: LiveData<List<DropDownMultiSelector.Selectable<FundFamily>>> = Transformations.map(viewState) {
        fundDataSource.getFundFamilies().map { fundFamily ->
            DropDownMultiSelector.Selectable(
                title = fundFamily.title,
                value = fundFamily,
                selected = it.filter.fundFamilies.contains(fundFamily)
            )
        }
    }

    internal val etfFundGroupData: LiveData<List<DropDownMultiSelector.Selectable<MutualFundGroup>>> = Transformations.map(viewState) {
        fundDataSource.getFundGroups().map { fundGroup ->
            DropDownMultiSelector.Selectable(
                title = fundGroup.title,
                value = fundGroup,
                selected = fundGroup.id == it.filter.fundGroup.id
            )
        }
    }

    internal val fundCategoryData: LiveData<List<DropDownMultiSelector.Selectable<FundCategory>>> = Transformations.map(viewState) {
        fundCategoriesRepo.getLoadedMutualFundCategories(it.filter.fundGroup).map { fundCategory ->
            DropDownMultiSelector.Selectable(
                title = fundCategory.title,
                value = fundCategory,
                selected = it.filter.fundCategories.contains(fundCategory)
            )
        }
    }

    internal fun fundFamilySelected(fundFamily: FundFamily) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    fundFamilies = it.filter.fundFamilies.toggle(fundFamily, fundDataSource.getAllFundFamiliesItem())
                )
            )
        }
    }

    internal fun fundGroupSelected(fundGroup: MutualFundGroup) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    fundGroup = fundGroup,
                    fundCategories = setOf(fundDataSource.getAllFundCategoriesItem())
                )
            )
        }
    }

    internal fun fundCategorySelected(fundCategory: FundCategory) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    fundCategories = it.filter.fundCategories.toggle(fundCategory, fundDataSource.getAllFundCategoriesItem())
                )
            )
        }
    }

    internal fun fundCategoryCleared() {
        updateState {
            it.copy(filter = it.filter.copy(fundCategories = setOf(fundDataSource.getAllFundCategoriesItem())))
        }
    }

    internal fun fundFamilyCleared() {
        updateState {
            it.copy(filter = it.filter.copy(fundFamilies = setOf(fundDataSource.getAllFundFamiliesItem())))
        }
    }

    internal fun morningStarRatingToggled(rating: Int, isChecked: Boolean) {
        updateState {
            it.copy(
                filter = it.filter.copy(
                    morningStarRating = if (isChecked) { it.filter.morningStarRating + rating } else { it.filter.morningStarRating - rating }
                )
            )
        }
    }

    internal fun familySelectionCanceled(previousValues: Set<FundFamily>?) {
        previousValues?.let { previousSelection ->
            updateState {
                it.copy(
                    filter = it.filter.copy(
                        fundFamilies = previousSelection
                    )
                )
            }
        }
    }

    internal fun categorySelectionCancelled(previousValues: Set<FundCategory>?) {
        previousValues?.let { previousSelection ->
            updateState {
                it.copy(
                    filter = it.filter.copy(
                        fundCategories = previousSelection
                    )
                )
            }
        }
    }

    internal fun backupFamiliesSelection() {
        prevFamiliesSelection = viewState.value?.filter?.fundFamilies?.toSet()
    }

    internal fun backupCategoriesSelection() {
        prevCategorySelection = viewState.value?.filter?.fundCategories?.toSet()
    }

    data class ViewState(
        override val filter: FundProfileFilter
    ) : BaseFilterScreenViewModel.ViewState<FundProfileFilter>
}
