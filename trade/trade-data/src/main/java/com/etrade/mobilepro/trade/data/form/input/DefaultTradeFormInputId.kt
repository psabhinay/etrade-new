package com.etrade.mobilepro.trade.data.form.input

import com.etrade.mobilepro.trade.data.form.value.DefaultSecurityTypeValueId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.SecurityTypeValueId

internal object DefaultTradeFormInputId : TradeFormInputId {
    override val accountId: String = "accountId"
    override val securityType: String = "securityType"
    override val securityTypeValueId: SecurityTypeValueId = DefaultSecurityTypeValueId
}
