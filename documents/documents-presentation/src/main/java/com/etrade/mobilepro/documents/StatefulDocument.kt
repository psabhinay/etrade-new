package com.etrade.mobilepro.documents

data class StatefulDocument(
    val document: Document,
    val status: Status
) {
    sealed class Status {
        object Loading : Status()
        object Available : Status()
        object Unavailable : Status()
        class Failed(val retry: () -> Unit) : Status()
    }
}
