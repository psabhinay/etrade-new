package com.etrade.mobilepro.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import com.etrade.mobilepro.dialog.CustomDialogFragment.Companion.RESULT_KEY_REQUEST_CODE
import com.etrade.mobilepro.dialog.CustomDialogFragment.Companion.RESULT_KEY_TEXT_AREA_TAPPED_END
import com.etrade.mobilepro.dialog.CustomDialogFragment.Companion.RESULT_KEY_TEXT_AREA_TAPPED_START
import com.etrade.mobilepro.util.android.clicklisteners.OnDialogDebounceClickListener
import com.etrade.mobilepro.util.android.textutil.setClickableSpan
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory

private const val EXTRA_KEY_TITLE = "EXTRA_KEY_TITLE"
private const val EXTRA_KEY_MESSAGE = "EXTRA_KEY_MESSAGE"
private const val EXTRA_KEY_OK_TITLE = "EXTRA_KEY_OK_TITLE"
private const val EXTRA_KEY_CANCEL_TITLE = "EXTRA_KEY_CANCEL_TITLE"
private const val EXTRA_KEY_NEUTRAL_TITLE = "EXTRA_KEY_NEUTRAL_TITLE"
private const val EXTRA_KEY_FRAGMENT_TAG = "EXTRA_KEY_FRAGMENT_TAG"
private const val EXTRA_KEY_CLICKABLE_RANGES = "clickable ranges"
private const val EXTRA_KEY_SHOULD_DISMISS_ON_TEXT_TAPS = "clickable ranges tap dismisses"
private const val EXTRA_KEY_REQUEST_CODE = "requestCode"

class CustomDialogFragment : DialogFragment() {

    private val title: String by lazy { arguments?.getString(EXTRA_KEY_TITLE) ?: "" }
    private val message: CharSequence by lazy { arguments?.getCharSequence(EXTRA_KEY_MESSAGE) ?: "" }
    private val okTitle: String by lazy { arguments?.getString(EXTRA_KEY_OK_TITLE) ?: "" }
    private val cancelTitle: String? by lazy { arguments?.getString(EXTRA_KEY_CANCEL_TITLE) }
    private val neutralTitle: String? by lazy { arguments?.getString(EXTRA_KEY_NEUTRAL_TITLE) }
    private val fragmentId: String? by lazy { arguments?.getString(EXTRA_KEY_FRAGMENT_TAG) }
    private val shouldDismissOnMessageTextTap: Boolean by lazy { arguments?.getBoolean(EXTRA_KEY_SHOULD_DISMISS_ON_TEXT_TAPS) ?: false }
    private val clickableRanges: List<Pair<Int, Int>> by lazy {
        (arguments?.getIntegerArrayList(EXTRA_KEY_CLICKABLE_RANGES) ?: emptyList<Int>()).run {
            val result = mutableListOf<Pair<Int, Int>>()
            for (i in this.indices step 2) {
                result.add(Pair(first = this[i], second = this[i + 1]))
            }
            result.toList()
        }
    }
    private val logger: Logger = LoggerFactory.getLogger(CustomDialogFragment::class.java)

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val context = requireContext()
        return createAndSetupMaterialDialog(context)
    }

    override fun show(manager: FragmentManager, tag: String?) {
        try {
            super.show(manager, tag)
        } catch (ex: IllegalStateException) {
            logger.error("Open dialog failed.", ex)
        }
    }

    private fun createAndSetupMaterialDialog(context: Context): AlertDialog {
        return MaterialAlertDialogBuilder(context).setTitle(title).setMessage(message).let {
            setOkButton(it)
            setCancelButton(it)
            setNeutralButton(it)
            it.create().apply {
                performAdditionalSetupOnShow()
            }
        }
    }

    private fun AlertDialog.performAdditionalSetupOnShow() {
        setOnShowListener { dialog ->
            val messageTextView = ((dialog as AlertDialog).findViewById(android.R.id.message) as? TextView)
            if (message is SpannableString) {
                messageTextView?.movementMethod = LinkMovementMethod.getInstance()
            }
            if (clickableRanges.isNotEmpty()) {
                val spannableString = SpannableString(message).apply {
                    clickableRanges.forEach { clickableRange ->
                        setClickableSpan(start = clickableRange.first, end = clickableRange.second) {
                            val bundle = resultBundle().apply {
                                putInt(RESULT_KEY_TEXT_AREA_TAPPED_START, clickableRange.first)
                                putInt(RESULT_KEY_TEXT_AREA_TAPPED_END, clickableRange.second)
                            }
                            setFragmentResult(CUSTOM_RESULT_TEXT_AREA_TAPPED, bundle)
                            if (shouldDismissOnMessageTextTap) {
                                dismiss()
                            }
                        }
                    }
                }
                messageTextView?.text = spannableString
                messageTextView?.movementMethod = LinkMovementMethod.getInstance()
            }
        }
    }

    private fun setOkButton(builder: MaterialAlertDialogBuilder) {
        builder.setPositiveButton(
            okTitle,
            OnDialogDebounceClickListener({ _, _ ->
                dismiss()
                setFragmentResult(CUSTOM_RESULT_OK, resultBundle())
            })
        )
    }

    private fun setNeutralButton(builder: MaterialAlertDialogBuilder) {
        neutralTitle?.also { title ->
            builder.setNeutralButton(title) { _, _ ->
                dismiss()
                setFragmentResult(CUSTOM_RESULT_NEUTRAL, resultBundle())
            }
        }
    }

    private fun setCancelButton(builder: MaterialAlertDialogBuilder) {
        cancelTitle?.also { title ->
            builder.setNegativeButton(title) { _, _ ->
                dismiss()
                setFragmentResult(CUSTOM_RESULT_CANCELLED, resultBundle())
            }
        }
    }

    private val requestCode: Int? by lazy { arguments?.getInt(EXTRA_KEY_REQUEST_CODE) }

    private fun resultBundle() = Bundle().apply {
        requestCode?.let { putInt(RESULT_KEY_REQUEST_CODE, it) }
    }

    companion object {
        @Suppress("LongParameterList", "LongMethod")
        fun newInstance(
            title: String? = null,
            message: CharSequence? = null,
            okTitle: String,
            cancelTitle: String? = null,
            neutralTitle: String? = null,
            cancelable: Boolean = true,
            fragmentId: String? = null,
            clickableMessageRanges: List<Pair<Int, Int>> = emptyList(),
            shouldDismissOnMessageTextTap: Boolean = false,
            requestCode: Int = 0,
        ): CustomDialogFragment {
            val fragment = CustomDialogFragment()
            fragment.isCancelable = cancelable
            val rangesArrayList = clickableMessageRanges.fold(
                initial = ArrayList<Int>(clickableMessageRanges.size * 2),
                operation = { accumulated, pair ->
                    accumulated.add(pair.first)
                    accumulated.add(pair.second)
                    accumulated
                }
            )
            fragment.arguments = packToBundle(
                title,
                message,
                okTitle,
                cancelTitle,
                neutralTitle,
                fragmentId,
                rangesArrayList,
                shouldDismissOnMessageTextTap,
                requestCode
            )

            return fragment
        }

        @Suppress("LongParameterList")
        private fun packToBundle(
            title: String?,
            message: CharSequence?,
            okTitle: String,
            cancelTitle: String?,
            neutralTitle: String?,
            fragmentId: String?,
            rangesArrayList: ArrayList<Int>,
            shouldDismissOnMessageTextTap: Boolean,
            requestCode: Int
        ) = Bundle().apply {
            putString(EXTRA_KEY_TITLE, title)
            putCharSequence(EXTRA_KEY_MESSAGE, message)
            putString(EXTRA_KEY_OK_TITLE, okTitle)
            putString(EXTRA_KEY_CANCEL_TITLE, cancelTitle)
            putString(EXTRA_KEY_NEUTRAL_TITLE, neutralTitle)
            putString(EXTRA_KEY_FRAGMENT_TAG, fragmentId)
            putIntegerArrayList(EXTRA_KEY_CLICKABLE_RANGES, rangesArrayList)
            putBoolean(EXTRA_KEY_SHOULD_DISMISS_ON_TEXT_TAPS, shouldDismissOnMessageTextTap)
            putInt(EXTRA_KEY_REQUEST_CODE, requestCode)
        }

        const val CUSTOM_RESULT_OK = "customResultOk"
        const val CUSTOM_RESULT_NEUTRAL = "customResultNeutral"
        const val CUSTOM_RESULT_TEXT_AREA_TAPPED = "customResultTextAreaTapped"
        const val CUSTOM_RESULT_CANCELLED = "customResultCancelled"

        const val RESULT_KEY_REQUEST_CODE = "resultCode"
        const val RESULT_KEY_TEXT_AREA_TAPPED_START = "tap_start"
        const val RESULT_KEY_TEXT_AREA_TAPPED_END = "tap_end"
    }
}

val Bundle.customDialogRequestCode: Int
    get() = getInt(RESULT_KEY_REQUEST_CODE)

val Bundle.customDialogTapStart: Int
    get() = getInt(RESULT_KEY_TEXT_AREA_TAPPED_START)

val Bundle.customDialogTapEnd: Int
    get() = getInt(RESULT_KEY_TEXT_AREA_TAPPED_END)
