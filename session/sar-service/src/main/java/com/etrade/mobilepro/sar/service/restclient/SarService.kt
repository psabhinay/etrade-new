package com.etrade.mobilepro.sar.service.restclient

import com.etrade.mobilepro.sar.service.dto.SarResponseDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.GET

interface SarService {

    /**
     * Api to get SAR status of the current user
     */
    @JsonMoshi
    @GET("login/sar")
    suspend fun getSarStatus(): SarResponseDto
}
