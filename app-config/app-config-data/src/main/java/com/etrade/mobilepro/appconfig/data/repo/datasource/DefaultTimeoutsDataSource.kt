package com.etrade.mobilepro.appconfig.data.repo.datasource

import com.etrade.mobilepro.appconfig.api.Timeouts
import org.threeten.bp.Duration

private const val DEFAULT_CONNECTION_TIMEOUT = 30L
private const val DEFAULT_READ_TIMEOUT = 60L
private const val DEFAULT_WRITE_TIMEOUT = 30L

internal val defaultTimeouts = Timeouts(
    connection = Duration.ofSeconds(DEFAULT_CONNECTION_TIMEOUT),
    read = Duration.ofSeconds(DEFAULT_READ_TIMEOUT),
    write = Duration.ofSeconds(DEFAULT_WRITE_TIMEOUT),
    limitSessionTo30Sec = false
)
