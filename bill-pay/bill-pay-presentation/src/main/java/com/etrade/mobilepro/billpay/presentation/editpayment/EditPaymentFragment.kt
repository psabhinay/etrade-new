package com.etrade.mobilepro.billpay.presentation.editpayment

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.PaymentResult
import com.etrade.mobilepro.billpay.presentation.BillPayViewModel
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.billpay.presentation.common.EMPTY_STRING
import com.etrade.mobilepro.billpay.presentation.common.NavigationViewModel
import com.etrade.mobilepro.billpay.presentation.common.accountDetailsContentDescriptionFormatter
import com.etrade.mobilepro.billpay.presentation.common.formatAccountName
import com.etrade.mobilepro.billpay.presentation.common.formatCurrency
import com.etrade.mobilepro.billpay.presentation.common.formatName
import com.etrade.mobilepro.billpay.presentation.common.getAmountError
import com.etrade.mobilepro.billpay.presentation.common.payeeNameFormatter
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayFragmentEditPaymentBinding
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.setupToolbarWithUpButton
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.transfermoney.widget.CALENDAR_REQUEST_KEY
import com.etrade.mobilepro.transfermoney.widget.TransferMoneyCalendarDialogFragment
import com.etrade.mobilepro.transfermoney.widget.extractCalendarDialogResult
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.accessibility.bindTextInputButtonAccessibility
import com.etrade.mobilepro.util.android.accessibility.toUSCurrency
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.clearFocusSafely
import com.etrade.mobilepro.util.android.extension.hideSoftKeyboard
import com.etrade.mobilepro.util.android.hideSoftKeyboard
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.android.textutil.CurrencyTextWatcher
import com.etrade.mobilepro.util.formatters.americanSimpleDateFormatter
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

private val TAG_FROM = "${EditPaymentFragment::class.java.canonicalName}_drop_down_from"

private val DUPLICATE_PAYMENT_TAG = "${EditPaymentFragment::class.java.canonicalName}_duplicate_dialog_tag"

@RequireLogin
class EditPaymentFragment @Inject constructor(
    private val assistedViewModelFactory: EditPaymentViewModelFactoryFactory,
    private val sharedViewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : OverlayBaseFragment() {

    private val binding by viewBinding(BillPayFragmentEditPaymentBinding::bind)

    private val sharedViewModel: BillPayViewModel by navGraphViewModels(R.id.bill_pay_nav_graph) { sharedViewModelFactory }
    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.bill_pay_nav_graph)
    private val parentSnackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { activity },
            { view }
        )
    }
    private lateinit var currencyTextWatcher: CurrencyTextWatcher

    private val selectedPayment by lazy {
        selectedPaymentArgs.currentPayment
    }

    private val selectedPayee: Payee? by lazy {
        sharedViewModel.payees.value?.firstOrNull { payee -> payee.name == selectedPayment?.payeeName }
    }

    private val selectedPaymentArgs: EditPaymentFragmentArgs by navArgs()

    private val viewModel: EditPaymentViewModel by viewModels {
        assistedViewModelFactory.create(
            validAccountList = sharedViewModel.viewState.value?.validAccounts ?: emptyList(),
            selectedPayee = selectedPayee,
            originAccount = selectedPayment?.accountId,
            currentPayment = selectedPayment
        )
    }

    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    private lateinit var transferFromSelector: DropDownMultiSelector<AccountDetails>

    override val layoutRes: Int = R.layout.bill_pay_fragment_edit_payment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(CALENDAR_REQUEST_KEY) { _, bundle ->
            bundle.extractCalendarDialogResult()
                .let { viewModel.onScheduleDateSelected(it.selectedDate) }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        bindViewModel()

        if (savedInstanceState != null) {
            getCalendarBuilder()?.restore(this)
        }
    }

    override fun onStart() {
        super.onStart()
        (activity as? AppCompatActivity)?.setupToolbarWithUpButton(titleResId = R.string.modify, upButtonResId = R.drawable.ic_close)
    }

    private fun setupView() {
        renderSelectedPayee(selectedPayee)
        setupFromAccountDropDown()
        setupAmountField()
        setupListeners()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                renderLoading(it.isLoading)
                if (it.paymentSucceed) {
                    handlePaymentUpdated()
                } else {
                    render(it)
                }
                it?.consumablePaymentResult?.let { paymentResult ->
                    handleError(paymentResult)
                }
            }
        )
    }

    private fun handlePaymentUpdated() {
        navigationViewModel.refreshPayments()
        findNavController().popBackStack(R.id.billPayContainerFragment, false)
        parentSnackBarUtil.snackbar(getString(R.string.bill_pay_edit_payment_success), duration = Snackbar.LENGTH_LONG)
    }

    private fun renderLoading(loading: Boolean) {
        if (loading) {
            showLoading()
        } else {
            hideLoading()
        }
    }

    private fun handleError(result: PaymentResult) {
        when (result) {
            PaymentResult.Error.DuplicateEntry -> showDuplicatePaymentDialog()
            PaymentResult.Error.Generic -> snackBarUtil.errorSnackbar(duration = Snackbar.LENGTH_LONG)
            else -> { // Do nothing
            }
        }
    }

    private fun showDuplicatePaymentDialog() {
        CustomDialogFragment.newInstance(
            title = getString(R.string.please_note),
            message = getString(R.string.bill_pay_duplicate_payment),
            okTitle = getString(android.R.string.ok)
        ).show(childFragmentManager, DUPLICATE_PAYMENT_TAG)
    }

    private fun render(paymentForm: PaymentForm) = with(paymentForm) {

        binding.paymentForm.scheduleLayout.isEnabled =
            selectedPayee != null && selectedAccount != null
        binding.paymentForm.schedule.setText(
            selectedScheduleDate?.format(
                americanSimpleDateFormatter
            ) ?: getString(R.string.empty_default)
        )
        binding.paymentForm.transferFrom.setText(
            selectedAccount?.formatAccountName() ?: getString(R.string.empty_default)
        )

        if (selectedAccount == null && validAccounts.size == 1) {
            viewModel.onAccountSelected(validAccounts.first())
        }

        binding.paymentForm.memoEditText.setText(memo)
        binding.updateBillPayment.isEnabled = viewModel.canCollectData()

        bindEditPaymentAccessibility(this)
    }

    private fun showLoading() {
        binding.progressBar.show()
        binding.content.visibility = View.INVISIBLE
    }

    private fun hideLoading() {
        binding.progressBar.hide()
        binding.content.visibility = View.VISIBLE
    }

    private fun renderSelectedPayee(payee: Payee?) {
        binding.paymentForm.transferTo.setText(
            payee?.formatName() ?: getString(R.string.empty_default)
        )
        binding.paymentForm.transferTo.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.grey
            )
        )
        binding.paymentForm.toLayout.isEnabled = false
    }

    private fun setupFromAccountDropDown() {
        transferFromSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = TAG_FROM,
                multiSelectEnabled = false,
                title = getString(R.string.from),
                itemsData = getDropDownAccounts(),
                selectListener = { _, item -> viewModel.onAccountSelected(item.value) }
            )
        )
    }

    private fun setListenersToMemo() {
        binding.paymentForm.memoEditText.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) {
                viewModel.setMemo(binding.paymentForm.memoEditText.text.toString())
            }
        }

        binding.paymentForm.memoEditText.doAfterTextChanged {
            val text = it?.toString() ?: return@doAfterTextChanged
            viewModel.setMemo(text)
            binding.paymentForm.memoEditText.setSelection(text.length)
        }

        binding.paymentForm.memoEditText.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                v.hideSoftKeyboard()
                viewModel.setMemo(binding.paymentForm.memoEditText.text.toString())
                true
            } else {
                false
            }
        }
    }

    private fun setupListeners() {
        setListenersToMemo()
        binding.paymentForm.transferFrom.setOnClickListener { transferFromSelector.open() }
        binding.paymentForm.schedule.setOnClickListener {
            getCalendarBuilder()?.show(this)
        }

        binding.updateBillPayment.setOnClickListener {
            viewModel.setMemo(binding.paymentForm.memoEditText.text.toString())
            viewModel.updateScheduledPayment(requireNotNull(sharedViewModel.sessionId))
        }
    }

    private fun setupAmountField() {
        currencyTextWatcher = CurrencyTextWatcher(
            editText = binding.paymentForm.amount,
            initialAmountInDollar = viewModel.viewState.value?.amount
        ) {
            viewModel.onAmountEntered(it)
        }

        binding.paymentForm.amount.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                binding.paymentForm.amount.clearFocusSafely()
                binding.paymentForm.memoEditText.requestFocus()
                activity?.hideSoftKeyboard()
            }
            false
        }

        binding.paymentForm.amount.setOnFocusChangeListener { _, hasFocus ->
            binding.paymentForm.amountLayout.error = if (!hasFocus) {
                resources.getAmountError(currencyTextWatcher.rawValue)
            } else {
                EMPTY_STRING
            }
        }
    }

    @Suppress("LongMethod")
    private fun bindEditPaymentAccessibility(paymentForm: PaymentForm) {
        binding.paymentForm.toLayout.bindTextInputButtonAccessibility(
            getString(
                R.string.accessibility_label_to,
                context?.accountNameContentDescription(
                    selectedPayee?.let {
                        resources.payeeNameFormatter.invoke(
                            it
                        )
                    } ?: ""
                )
            )
        )
        binding.paymentForm.fromLayout.bindTextInputButtonAccessibility(
            getString(
                R.string.accessibility_label_from,
                context?.accountNameContentDescription(
                    paymentForm.selectedAccount?.accountShortName ?: ""
                )
            )
        )
        binding.paymentForm.amountLayout.apply {
            val label = getString(R.string.amount)
            val description = paymentForm.amount?.toUSCurrency()
            bindTextInputButtonAccessibility(
                description = "$label ${description ?: ""}",
                actionLabel = context.getString(R.string.edit_action)
            )
        }
        binding.paymentForm.scheduleLayout.apply {
            val label = getString(R.string.schedule_date)
            val description = paymentForm.selectedScheduleDate?.let {
                americanSimpleDateFormatter.format(it)
            } ?: ""
            bindTextInputButtonAccessibility("$label $description")
        }
        binding.paymentForm.memoLayout.apply {
            val label = getString(R.string.bill_pay_memo_hint)
            val description = paymentForm.memo ?: ""
            bindTextInputButtonAccessibility("$label $description")
        }
    }

    private fun getDropDownAccounts(): LiveData<List<DropDownMultiSelector.Selectable<AccountDetails>>> {
        return Transformations.map(viewModel.viewState) {
            it.validAccounts.map { account ->
                DropDownMultiSelector.Selectable(
                    title = account.formatAccountName(),
                    value = account,
                    selected = account == viewModel.viewState.value?.selectedAccount,
                    descriptionFormatter = { item ->
                        getString(
                            R.string.bill_pay_available_cash_placeholder,
                            formatCurrency(item.brokerage?.cashAvailableForWithdrawal)
                        )
                    },
                    titleColor = ContextCompat.getColor(requireContext(), R.color.colorAccent),
                    contentDescriptionFormatter = resources::accountDetailsContentDescriptionFormatter
                )
            }
        }
    }

    private fun getCalendarBuilder(): TransferMoneyCalendarDialogFragment.Builder? {
        return viewModel.viewState.value?.let { state ->
            if (state.selectedAccount != null && selectedPayee != null) {
                TransferMoneyCalendarDialogFragment.Builder(
                    isOriginAccountExternal = viewModel.isSelectedAccountExternal,
                    isDestinationAccountExternal = viewModel.isPayeeAccountExternal,
                    leadDays = selectedPayee?.leadDays,
                    selectedDate = state.selectedScheduleDate
                )
            } else {
                null
            }
        }
    }
}
