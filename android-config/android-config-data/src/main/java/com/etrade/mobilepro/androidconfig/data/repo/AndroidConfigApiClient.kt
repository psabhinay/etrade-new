package com.etrade.mobilepro.androidconfig.data.repo

import com.etrade.eo.rest.retrofit.Xml
import com.etrade.mobilepro.androidconfig.data.dto.AndroidConfigInfoDto
import retrofit2.http.GET

internal interface AndroidConfigApiClient {

    @Xml
    @GET("/mobilepro/android/android.xml")
    suspend fun getAndroidConfigInfo(): AndroidConfigInfoDto
}
