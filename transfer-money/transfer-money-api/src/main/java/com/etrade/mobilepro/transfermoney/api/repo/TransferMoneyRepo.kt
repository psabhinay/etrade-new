package com.etrade.mobilepro.transfermoney.api.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.etrade.mobilepro.transfermoney.api.TransferMoneyActivity
import com.etrade.mobilepro.transfermoney.api.TransferMoneyConfirm
import com.etrade.mobilepro.transfermoney.api.TransferMoneyPreview

interface TransferMoneyRepo {
    suspend fun getAccounts(userId: String): ETResult<List<TransferMoneyAccount>>
    suspend fun getActivity(userId: String): ETResult<List<TransferMoneyActivity>>

    suspend fun getPreview(request: PreviewRequest): ETResult<TransferMoneyPreview>

    /**
     * @param userId - user Id, can be obtained from the User class
     * @param transferReferenceNumber - reference number of the transfer. It is obtained from the above getActivity() method result
     * @param deleteSeries - set to true if in case of scheduled recurring transfer you want to delete the entire series.
     *                       Set to false if you want to delete only one occurrence
     */
    suspend fun deleteTransfer(userId: String, transferReferenceNumber: String, deleteSeries: Boolean): ETResult<String>

    suspend fun getConfirm(request: ConfirmRequest): ETResult<TransferMoneyConfirm>
}
