package com.etrade.mobilepro.markets

import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel

data class OverviewItem(
    val symbol: String,
    val displayName: String,
    val value: String,
    val change: String,
    val changePercent: String,
    val isGreen: Boolean = true
) : DynamicViewModel(R.layout.market_overview_item) {

    override val variableId: Int = BR.obj
}
