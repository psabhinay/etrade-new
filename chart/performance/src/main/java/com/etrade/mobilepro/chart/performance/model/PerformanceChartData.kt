package com.etrade.mobilepro.chart.performance.model

import com.etrade.mobilepro.chart.api.ChartLine

class PerformanceChartData(
    val accounts: List<ChartLine>,
    val indexes: List<ChartLine>
)
