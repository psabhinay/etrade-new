package com.etrade.mobilepro.inboxmessages

import com.etrade.mobilepro.common.result.ETResult

interface InboxMessagesRepository {

    suspend fun getMessages(type: MessageType, place: MessagePlace?, userType: UserType?): ETResult<List<InboxMessage>>

    fun getMessagesBlocking(type: MessageType, place: MessagePlace?, userType: UserType?): ETResult<List<InboxMessage>>

    suspend fun dismissMessage(message: InboxMessage)

    suspend fun setMessageSend(message: InboxMessage)

    suspend fun setMessageOpen(message: InboxMessage)

    suspend fun setMessageViewed(message: InboxMessage)

    fun triggerMessage(triggerName: String, onMessageClose: (() -> Unit)?)

    fun triggerMessageCustomUi(triggerName: String, onMessageTriggered: (InAppMessage) -> Unit)
}
