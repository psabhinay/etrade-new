package com.etrade.mobilepro.util.market.time

import org.threeten.bp.ZoneId

val NEW_YORK_ZONE_ID: ZoneId = ZoneId.of("America/New_York")
