package com.etrade.mobilepro.orders.caching

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.dynamicui.CachedResponseModel
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.data.dto.OrderDataDto
import com.etrade.mobilepro.orders.data.dto.OrdersResponseDto
import com.etrade.mobilepro.orders.data.dto.StockPlanOrdersResponseDto
import com.etrade.mobilepro.orders.list.api.OrderItem
import com.etrade.mobilepro.orders.list.api.OrderItemsAndKey
import com.etrade.mobilepro.orders.list.api.OrderParamsLoadSizeAndKey
import com.etrade.mobilepro.orders.list.api.OrdersRepo
import com.etrade.mobilepro.orders.list.mapper.OrderItemMapper
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.parseXml
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import kotlinx.coroutines.rx2.awaitLast
import javax.inject.Inject

class CachedOrdersRepo @Inject constructor(
    private val moshi: Moshi,
    private val cachedDataSourceDelegate: SimpleCachedResource.DataSourceDelegate<CachedResponseModel, OrderParamsLoadSizeAndKey>
) : OrdersRepo {
    override suspend fun getOrders(orderParamsLoadSizeAndKey: OrderParamsLoadSizeAndKey, cacheExpiration: Long?): OrderItemsAndKey {
        return SimpleCachedResource(
            orderParamsLoadSizeAndKey, cachedDataSourceDelegate,
            object : SimpleCachedResource.Controller<CachedResponseModel> {
                override fun shouldRefresh(itemFromCache: CachedResponseModel) = cacheExpiration?.let {
                    System.currentTimeMillis() - itemFromCache.retrievedTimestamp >= it
                } ?: true
            }
        )
            .asObservable()
            .map { cachedResponse ->
                if (cachedResponse is Resource.Failed) {
                    OrderItemsAndKey(errorThrowable = cachedResponse.error)
                } else if (cachedResponse is Resource.Loading || cachedResponse.data == null) {
                    OrderItemsAndKey(isLoading = true)
                } else {
                    cachedResponse.extractOrderItemsAndKey(orderParamsLoadSizeAndKey)
                }
            }.awaitLast()
    }

    private fun Resource<CachedResponseModel>.extractOrderItemsAndKey(
        orderParamsLoadSizeAndKey: OrderParamsLoadSizeAndKey
    ): OrderItemsAndKey {
        val mapper = OrderItemMapper()
        var key = orderParamsLoadSizeAndKey.key
        val orderItems = if (orderParamsLoadSizeAndKey.orderParams.isStockPlan) {
            data?.response?.parseXml<StockPlanOrdersResponseDto>()?.let { xml -> mapper.mapStockPlanOrders(xml) }
        } else {
            val serverResponseDto = getServerResponseDto(data?.response)
            val ordersResponse = serverResponseDto?.extractOrdersResponse(orderParamsLoadSizeAndKey)

            key = ordersResponse?.end.takeIf { end -> end != "0" }
            ordersResponse?.orderList?.map { orderItemDto -> mapper.map(orderItemDto) }
        }?.applySort(orderParamsLoadSizeAndKey) ?: emptyList()

        return OrderItemsAndKey(orderItems, key, false)
    }

    private fun ServerResponseDto<OrderDataDto>.extractOrdersResponse(
        orderParamsLoadSizeAndKey: OrderParamsLoadSizeAndKey
    ): OrdersResponseDto? =
        if (orderParamsLoadSizeAndKey.orderParams.orderStatus == OrderStatus.SAVED) {
            data?.savedOrderResponse
        } else {
            data?.ordersResponse
        }

    private fun List<OrderItem>.applySort(orderParamsLoadSizeAndKey: OrderParamsLoadSizeAndKey): List<OrderItem> {
        return if (orderParamsLoadSizeAndKey.orderParams.orderStatus == OrderStatus.ALL_BY_SYMBOL) {
            sortedWith(Comparator { o1, o2 -> o1.symbols.first().compareTo(o2.symbols.first()) })
        } else {
            this
        }
    }

    private fun getServerResponseDto(response: String?): ServerResponseDto<OrderDataDto>? {
        val adapter: JsonAdapter<ServerResponseDto<OrderDataDto>> =
            moshi.adapter(Types.newParameterizedType(ServerResponseDto::class.java, OrderDataDto::class.java))
        return response?.let { adapter.fromJson(response) }
    }
}
