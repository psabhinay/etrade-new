package com.etrade.mobilepro.util.updater

import androidx.core.database.sqlite.transaction
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.WatchlistColumn
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnInterface
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnRepo
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream

@RunWith(AndroidJUnit4::class)
@Config(application = android.app.Application::class)
class WatchlistColumnMigrationSuite {

    private val repo = InMemoryWatchlistColumnRepo()
    private val migration: Migration = WatchlistColumnMigration(repo)

    private val columns: List<Pair<Int, WatchlistColumn>>
        get() = listOf(
            10 to WatchlistColumn.LastPrice,
            11 to WatchlistColumn.ChangeDollar,
            12 to WatchlistColumn.ChangePercent,
            13 to WatchlistColumn.Volume,
            14 to WatchlistColumn.Bid,
            15 to WatchlistColumn.Ask,
            16 to WatchlistColumn.DayHigh,
            17 to WatchlistColumn.DayLow,
            18 to WatchlistColumn.FiftyTwoWeekHigh,
            19 to WatchlistColumn.FiftyTwoWeekLow,
            20 to WatchlistColumn.PriceEarningRatio,
            21 to WatchlistColumn.EarningsPerShare
        ).shuffled()

    @Test
    fun `test migration success`() = withDatabaseValues(columns) { expected ->
        repo.columns.clear()
        repo.saveCalled = false
        repo.saveWatchlistColumns = true
        migration.migrate(databaseFile)
        assertTrue(repo.saveCalled)
        assertEquals(expected, repo.columns)
    }

    @Test
    fun `test empty watchlist`() = withDatabaseValues(emptyList()) {
        repo.columns.clear()
        repo.saveCalled = false
        repo.saveWatchlistColumns = true
        migration.migrate(databaseFile)
        assertFalse(repo.saveCalled)
        assertTrue(repo.columns.isEmpty())
    }

    @Test
    fun `test repo save failed`() = withDatabaseValues(columns) {
        repo.columns.clear()
        repo.saveCalled = false
        repo.saveWatchlistColumns = false
        migration.migrate(databaseFile)
        assertTrue(repo.saveCalled)
        assertTrue(repo.columns.isEmpty())
    }

    private fun withDatabaseValues(columns: List<Pair<Int, WatchlistColumn>>, block: suspend (List<WatchlistColumn>) -> Unit) {
        withExistingDatabaseFile({ database ->
            database.transaction {
                execSQL("CREATE TABLE IF NOT EXISTS WATCHLISTTBL( EntryId INTEGER PRIMARY KEY AUTOINCREMENT, Objectdata TEXT NOT NULL)")
                execSQL("REPLACE INTO WATCHLISTTBL(Objectdata) VALUES(?)", arrayOf(serialize(columns.map { it.first })))
            }
        }) {
            block(columns.map { it.second })
        }
    }

    private fun serialize(values: List<Int>): ByteArray {
        return ByteArrayOutputStream().use { bytes ->
            ObjectOutputStream(bytes).use {
                it.writeObject(values)
                bytes.toByteArray()
            }
        }
    }

    private class InMemoryWatchlistColumnRepo : WatchlistColumnRepo {

        val columns: MutableList<WatchlistColumnInterface> = mutableListOf()

        var saveCalled: Boolean = false
        var saveWatchlistColumns: Boolean = true

        override suspend fun getColumns(): List<WatchlistColumnInterface> = columns

        override suspend fun saveColumns(columns: List<WatchlistColumnInterface>) {
            saveCalled = true
            if (saveWatchlistColumns) {
                this.columns.apply {
                    clear()
                    addAll(columns)
                }
            }
        }

        override fun getColumnsAsString(): List<String> {
            return emptyList()
        }
    }
}
