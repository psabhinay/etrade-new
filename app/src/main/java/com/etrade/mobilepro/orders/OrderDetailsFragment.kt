package com.etrade.mobilepro.orders

import android.app.Dialog
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.eo.corelibandroid.createViewModel
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.OnNavigateUpListener
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.databinding.FragmentOrderDetailsBinding
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.showDialog
import com.etrade.mobilepro.dynamic.form.api.Action
import com.etrade.mobilepro.dynamic.form.fragment.createFormValuesArgument
import com.etrade.mobilepro.home.KEY_HIDE_BOTTOM_BAR
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.order.details.api.OrderData
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel
import com.etrade.mobilepro.order.details.router.OrderConfirmationAction
import com.etrade.mobilepro.order.details.router.PreviewOrderDetailsArgument
import com.etrade.mobilepro.order.details.router.WithOrderConfirmationAction
import com.etrade.mobilepro.order.details.router.WithSnackBarMessage
import com.etrade.mobilepro.orders.adapter.OrderDetailAdapter
import com.etrade.mobilepro.orders.sharedviewmodel.OrderSharedViewModel
import com.etrade.mobilepro.orders.viewmodel.OrderDetailsViewDelegateMap
import com.etrade.mobilepro.progressoverlay.ProgressDialogFragment
import com.etrade.mobilepro.quote.presentation.AffiliatedProductAction
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.swiperefresh.MultiSwipeRefreshLayout
import com.etrade.mobilepro.tooltip.TOOLTIP_INDETERMINATE_DURATION
import com.etrade.mobilepro.tooltip.Tooltip
import com.etrade.mobilepro.tooltip.TooltipGravity
import com.etrade.mobilepro.tooltip.TooltipViewModel
import com.etrade.mobilepro.trade.START_DESTINATION
import com.etrade.mobilepro.trade.TRADE_ORDER_DETAILS_FRAGMENT
import com.etrade.mobilepro.trade.presentation.KEY_TRADE_ORDER_TYPE
import com.etrade.mobilepro.trade.presentation.util.OptionsApprovalUpgradeViewDelegate
import com.etrade.mobilepro.trade.router.QuoteRouter
import com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer.getBottomSheetNavContainer
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.awaitForMainThread
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.extension.viewCoroutineScope
import com.etrade.mobilepro.util.android.goneIf
import com.etrade.mobilepro.util.android.recyclerviewutil.ViewLocator
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.ViewDelegate
import com.etrade.mobilepro.walkthrough.api.TradePlacingAction
import com.etrade.mobilepro.walkthrough.api.TradePlacingState
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.etrade.mobilepro.walkthrough.presentation.createConditionalSignal
import com.google.android.material.snackbar.Snackbar
import com.squareup.moshi.Moshi
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Provider

private const val BUTTON_TRANSLATION = "buttonTranslation"
private const val RECYCLER_TRANSLATION = "recyclerTranslation"
private const val WAITING_FOR_TRADE_RESULT = "waitingForTradeResult"

@Suppress("TooManyFunctions", "LargeClass")
@RequireLogin
open class OrderDetailsFragment @Inject constructor(
    snackbarUtilFactory: SnackbarUtilFactory,
    orderDetailsViewModelFactory: OrderDetailsViewModel.Factory,
    private val moshi: Moshi,
    private val quoteRouter: QuoteRouter,
    private val viewModelFactory: ViewModelProvider.Factory,
    private val viewDelegateMap: OrderDetailsViewDelegateMap,
    walkthroughViewModelProvider: Provider<WalkthroughStatesViewModel>
) : Fragment(R.layout.fragment_order_details), OnNavigateUpListener {

    private val binding by viewBinding(FragmentOrderDetailsBinding::bind)

    protected val sharedViewModel: OrderSharedViewModel by activityViewModels { viewModelFactory }

    private val viewDelegates: List<ViewDelegate> by lazy {
        listOf(
            OptionsApprovalUpgradeViewDelegate(
                requireContext(),
                viewModel.optionsApprovalUpgradeUrl
            )
        )
    }

    private val args: OrderDetailsFragmentArgs
        get() = OrderDetailsFragmentArgs.fromBundle(requireArguments())

    private val viewModel: OrderDetailsViewModel by createViewModel {
        orderDetailsViewModelFactory.create(
            delegate = viewDelegateMap.getDelegate(args.orderDetailsArgument)
        )
    }

    private val walkthroughViewModel: WalkthroughStatesViewModel = walkthroughViewModelProvider.get()
    private val tooltipViewModel: TooltipViewModel by activityViewModels()

    private var buttonTranslation: Float = 0f
    private var recyclerTranslation: Float = 0f

    private val isDualPane: Boolean by lazy {
        resources.getBoolean(R.bool.is_dual_pane) && parentFragment is OrdersFragment
    }

    private val tradeDestinationChangedListener: NavController.OnDestinationChangedListener =
        NavController.OnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.orderDetailsFragment && waitingForTradeResult && sharedViewModel.resetOrderDetailRequested) {
                resetOrderDetails()
                waitingForTradeResult = false
            }
        }

    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private var snackBar: Snackbar? = null
    private lateinit var pullToRefresh: MultiSwipeRefreshLayout
    private val orderItems: MutableList<OrderDetailItemInterface> = mutableListOf()
    private lateinit var viewAdapter: OrderDetailAdapter
    private val viewLocator: ViewLocator by lazy { ViewLocator(::awaitForMainThread) }

    private var openDialog: Dialog? = null

    private var waitingForTradeResult: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findNavController().addOnDestinationChangedListener(tradeDestinationChangedListener)

        pullToRefresh = view.findViewById(R.id.swipe_refresh)

        setupViewModel(viewModel)
        viewModel.fetch(args.orderDetailsArgument)

        bindViewModel()
        setupViewAdapter()
        setupSwipeRefreshLayout()
        setUpSharedViewModel()
    }

    override fun onDestroy() {
        super.onDestroy()
        openDialog?.dismiss()
        openDialog = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        findNavController().removeOnDestinationChangedListener(tradeDestinationChangedListener)
    }

    override fun onNavigateUp(): Boolean {
        return findNavController().run {
            args.upAccountTabIndex.takeUnless { it < 0 }?.let { index ->
                navigate(
                    MainGraphDirections.actionToSelectedAccountTabIndexOrTitle(tabIndex = index),
                    navOptions {
                        popUpTo(R.id.accountTabHostFragment) {
                            inclusive = true
                        }
                    }
                )
                true
            } ?: false
        }
    }

    @Suppress("LongMethod")
    private fun setUpSharedViewModel() {
        sharedViewModel.isRefreshDetailRequested.observe(
            viewLifecycleOwner,
            Observer { event ->
                event.consume {
                    viewModel.fetch(it)
                    arguments = args.copy(orderDetailsArgument = it).toBundle()
                }
            }
        )
        sharedViewModel.tradeCanceledSignal.observe(
            viewLifecycleOwner,
            Observer { event ->
                event.consume {
                    val navController = findNavController()
                    when (navController.graph.id) {
                        R.id.chartiq_internal_nav_graph ->
                            navController.popBackStack(R.id.chartIqFragment, false)
                        R.id.mainGraph ->
                            navController.popBackStack(R.id.orderDetailsFragment, false)
                        R.id.quoteGraph ->
                            quoteRouter.popQuotePage(navController)
                        R.id.trade_bottom_sheet_navigation_graph ->
                            getBottomSheetNavContainer()?.dismiss()
                        else -> { /*Do nothing*/ }
                    }
                }
            }
        )
    }

    protected open fun setupViewModel(viewModel: OrderDetailsViewModel) {
        viewModel.viewModelProvider = ViewModelProvider(this, viewModelFactory)
    }

    protected open fun onOrderConfirmed(orderId: String, orderConfirmationAction: OrderConfirmationAction) = Unit

    @CallSuper
    protected open fun onOrderDeleted(orderId: String) {
        sharedViewModel.requireRefreshOrderList()
        resetOrderDetails()
    }

    private fun resetOrderDetails() {
        sharedViewModel.resetOrderDetailRequested = false
        if (isDualPane) {
            pullToRefresh.visibility = View.INVISIBLE
        } else {
            findNavController().popBackStack()
        }
    }

    private fun bindViewModel() {
        viewModel.openShowAffiliatedDialog.observe(viewLifecycleOwner, { showAffiliatedDialog(it) })
        viewModel.actions.observe(viewLifecycleOwner, Observer { onActions(it) })
        viewModel.dialog.observe(viewLifecycleOwner, Observer { onDialog(it) })
        viewModel.errorMessage.observe(viewLifecycleOwner, Observer { onErrorMessage(it) })
        viewModel.items.observe(viewLifecycleOwner, Observer { onItems(it) })
        viewModel.loadingIndicator.observe(viewLifecycleOwner, Observer { onLoadingIndicator(it) })
        viewModel.noActionsMessage.observe(viewLifecycleOwner, Observer { onNoActionsMessage(it) })
        viewModel.orderConfirmedSignal.observe(viewLifecycleOwner, Observer { onOrderConfirmedSignal(it) })
        viewModel.orderDeletedSignal.observe(viewLifecycleOwner, Observer { onOrderDeletedSignal(it) })
        viewModel.orderCanceledSignal.observeBy(viewLifecycleOwner) { onOrderCanceledSignal(it) }
        viewModel.openTradeSignal.observe(viewLifecycleOwner, Observer { onOpenTradeSignal(it) })
        viewModel.openPreviewSignal.observe(viewLifecycleOwner, Observer { onOpenPreviewSignal(it) })
        viewModel.withSnackBarMessageSignal.observe(viewLifecycleOwner, Observer { onWithSnackBarMessage(it) })
        walkthroughViewModel.createConditionalSignal<TradePlacingState, List<Action>, List<Action>>(
            viewModel.actions, TradePlacingState.HighlightTrade
        ).observeBy(viewLifecycleOwner) { signal ->
            if (signal.isConsumed) { return@observeBy }
            onTradePlacingState(signal)
        }

        if (isDualPane) {
            viewModel.resetActionStates()
        }

        viewDelegates.forEach { it.observe(viewLifecycleOwner) }
        viewModel.lifecycleObserverViewDelegate.observe(viewLifecycleOwner)
    }

    private fun getQuotesWidgetViewModel(symbol: String): QuotesWidgetViewModel {
        val key = QuotesWidgetViewModel::class.java.name + symbol
        return ViewModelProvider(this, viewModelFactory).get(key, QuotesWidgetViewModel::class.java)
    }

    private fun onWithSnackBarMessage(event: ConsumableLiveEvent<WithSnackBarMessage>?) {
        event?.consume {
            it.snackBarMessage?.let { message -> snackBarUtil.errorSnackbar(message) }
        }
    }

    private fun onTradePlacingState(signal: ConsumableLiveEvent<TradePlacingState>) {
        viewLifecycleOwner.lifecycleScope.launch {
            awaitForMainThread()
            val targetView = viewLocator.locateView(
                binding.viewOrderActions.actionsView,
                binding.scrollView
            ) { candidateView ->
                candidateView is TextView &&
                    candidateView.text == resources.getString(R.string.orders_action_place_order)
            } ?: return@launch

            val onTooltipDismissed = {
                signal.consume()
                walkthroughViewModel.onAction(TradePlacingAction.TradeHighlighted)
            }

            awaitForMainThread()
            tooltipViewModel.show {
                target = targetView
                text = resources.getString(R.string.trade_walkthrough_place_order_tooltip)
                gravity = TooltipGravity.TOP
                yOffset = -resources.getDimensionPixelOffset(R.dimen.spacing_small)
                durationParams = Tooltip.DurationParams(viewCoroutineScope, TOOLTIP_INDETERMINATE_DURATION)
                onExplicitDismiss = onTooltipDismissed
                onImplicitDismiss = onTooltipDismissed
            }
        }
    }

    private fun onActions(actions: List<Action>) {
        with(binding.viewOrderActions.actionsView) {
            render(actions, viewLifecycleOwner)
            visibility = if (actions.isEmpty()) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }
    }

    private fun onDialog(dialog: AppDialog?) {
        openDialog?.dismiss()
        openDialog = dialog?.let { context?.showDialog(it) }
    }

    private fun onErrorMessage(errorMessage: ConsumableLiveEvent<ErrorMessage>) {
        errorMessage.consume {
            val action = it.retryAction
            if (action != null) {
                snackBar = snackBarUtil.retrySnackbar(actionOnRetry = action)
            }
        }
    }

    private fun onItems(items: List<OrderDetailItemInterface>) {
        binding.viewOrderActions.root.visibility = View.VISIBLE
        updateDetailData(items)
    }

    private fun onLoadingIndicator(loading: Boolean) {
        if (pullToRefresh.isRefreshing) {
            if (!loading) {
                pullToRefresh.isRefreshing = false
            }
        } else {
            if (loading && !isDualPane) {
                ProgressDialogFragment.show(childFragmentManager, false)
            } else {
                ProgressDialogFragment.dismiss(childFragmentManager)
            }
        }
    }

    private fun onNoActionsMessage(message: CharSequence?) {
        binding.viewOrderActions.noActionsView.text = message
        binding.viewOrderActions.noActionsView.goneIf(message == null)
    }

    private fun onOrderConfirmedSignal(signal: ConsumableLiveEvent<String>) {
        signal.consume { orderId ->
            val argument = args.orderDetailsArgument
            val action = if (argument is WithOrderConfirmationAction) {
                argument.orderConfirmationAction
            } else {
                OrderConfirmationAction.VIEW_PORTFOLIO
            }
            onOrderConfirmed(orderId, action)
        }
    }

    private fun onOrderDeletedSignal(signal: ConsumableLiveEvent<String>) {
        signal.consume { orderId ->
            onOrderDeleted(orderId)
        }
    }

    private fun onOrderCanceledSignal(signal: ConsumableLiveEvent<String>) {
        signal.consume { sharedViewModel.requireRefreshOrderList() }
    }

    private fun onOpenTradeSignal(signal: ConsumableLiveEvent<OrderData>) {
        signal.consume { orderData ->
            with(orderData) {
                val navController = findNavController()
                val navDirection: NavDirections

                val formValues = createFormValuesArgument(moshi, formValues)
                if (navController.graph.id == R.id.mainGraph) {
                    waitingForTradeResult = true

                    navDirection = MainGraphDirections.actionGlobalTradeBottomSheetFragment(
                        FormFragmentFormValues = formValues,
                        FormFragmentInputsDisabled = inputsDisabled,
                        tradeOrderType = tradeOrderType
                    )

                    navDirection.arguments.putSerializable(KEY_TRADE_ORDER_TYPE, tradeOrderType)
                } else {
                    navDirection = MainGraphDirections.actionLaunchTrade(formValues, inputsDisabled, tradeOrderType)
                }

                navController.navigate(navDirection)
            }
        }
    }

    private fun onOpenPreviewSignal(signal: ConsumableLiveEvent<PreviewOrderDetailsArgument>) {
        signal.consume { argument ->
            val args = OrderDetailsFragmentArgs(argument).toBundle()
            args.putString(START_DESTINATION, TRADE_ORDER_DETAILS_FRAGMENT)
            args.putBoolean(KEY_HIDE_BOTTOM_BAR, true)
            findNavController().navigate(R.id.action_launch_trade, args)
        }
    }

    private fun setupViewAdapter() {
        viewAdapter = OrderDetailAdapter(
            viewLifecycleOwner,
            orderItems,
            ViewModelProvider(this, viewModelFactory),
            onAffiliatedProductClickListener = AffiliatedProductAction { viewModel.showAffiliatedDialog() }
        )

        with(binding.orderdetailsRecyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = viewAdapter
            setupRecyclerViewDivider(R.drawable.thin_divider)
        }
    }

    private fun setupSwipeRefreshLayout() {
        pullToRefresh.setOnRefreshListener {
            snackBar?.dismiss()
            viewModel.refresh()
        }
        pullToRefresh.setSwipeableChildren(
            binding.viewOrderActions.root,
            binding.orderdetailsRecyclerView
        )
    }

    private fun updateDetailData(data: List<OrderDetailItemInterface>) {
        orderItems.clear()
        orderItems.addAll(data)
        viewAdapter.notifyDataSetChanged()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPause() {
        super.onPause()
        buttonTranslation = binding.viewOrderActions.root.translationY
        recyclerTranslation = binding.orderdetailsRecyclerView.translationY
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putFloat(BUTTON_TRANSLATION, buttonTranslation)
        outState.putFloat(RECYCLER_TRANSLATION, recyclerTranslation)
        outState.putBoolean(WAITING_FOR_TRADE_RESULT, waitingForTradeResult)
        super.onSaveInstanceState(outState)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            binding.viewOrderActions.root.translationY = it.getFloat(BUTTON_TRANSLATION)
            binding.orderdetailsRecyclerView.translationY = it.getFloat(RECYCLER_TRANSLATION)
            waitingForTradeResult = it.getBoolean(WAITING_FOR_TRADE_RESULT)
        }
        super.onViewStateRestored(savedInstanceState)
    }

    private fun showAffiliatedDialog(value: Boolean?) {
        value?.run {
            if (value) {
                showAffiliatedProductDialog { viewModel.closeAffiliatedDialog() }
            }
        }
    }
}
