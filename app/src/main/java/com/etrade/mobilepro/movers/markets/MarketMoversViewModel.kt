package com.etrade.mobilepro.movers.markets

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamicui.api.Diffable
import com.etrade.mobilepro.movers.MoverItem
import com.etrade.mobilepro.movers.Movers
import com.etrade.mobilepro.movers.MoversRouter
import com.etrade.mobilepro.quote.deriveDayChange
import com.etrade.mobilepro.quote.deriveDayChangePercent
import com.etrade.mobilepro.quote.getFormattedLastPrice
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.util.android.CallToAction

class MarketMoversViewModel(
    private val getMarketMoversUseCase: GetMarketMoversUseCase,
    moversRouter: MoversRouter,
    streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    streamingStatusController: StreamingStatusController,
    title: String,
    callToAction: CallToAction? = null,
    resources: Resources
) : Movers(
    layoutId = R.layout.layout_movers,
    title = title,
    showActionView = true,
    callToAction = callToAction,
    moversRouter = moversRouter,
    streamingQuoteProvider = streamingQuoteProvider,
    streamingStatusController = streamingStatusController,
    resources = resources,
    shouldDisplayOptionsData = false
),
    Diffable {

    override val subtitle = MutableLiveData(resources.getString(R.string.nasdaq_most_active))

    override val variableId: Int = BR.obj

    override suspend fun fetchData(): ETResult<List<MoverItem>> {
        return getMarketMoversUseCase.execute(Unit).fold(
            onSuccess = {
                ETResult.success(onSuccess(it))
            },
            onFailure = {
                ETResult.failure(it)
            }
        )
    }

    // Assumes that there will be only one MarketMoversViewModel in a list
    override fun isSameItem(other: Any): Boolean = other.javaClass == javaClass

    override fun hasSameContents(other: Any): Boolean = false

    private fun onSuccess(response: List<MobileQuote>): List<MoverItem> {
        logger.debug("Fetched ${response.size} quotes from server")
        return response.map { quote ->
            createMoverItem(
                symbol = quote.symbol,
                displaySymbol = quote.symbol,
                price = quote.getFormattedLastPrice(),
                change = quote.deriveDayChange(),
                decimalPercentChange = quote.deriveDayChangePercent()?.toBigDecimalOrNull()
            )
        }
    }
}
