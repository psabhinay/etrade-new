package com.etrade.mobilepro.quote.screen.presentation.adapter

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.screen.presentation.BR

class QuoteBindingViewHolder(
    private val binding: ViewDataBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(item: Any?) {
        binding.setVariable(BR.quoteItem, item)
        binding.executePendingBindings()
    }
}
