package com.etrade.mobilepro.alerts.presentation.setalert.setvalue.dailypercent

import android.text.InputFilter
import com.etrade.mobilepro.inputvalueview.InputFieldManager

private const val PERCENT_FIELD_LENGTH_FOCUSED = 6
private const val PERCENT_FIELD_LENGTH_NOT_FOCUSED = 7

class DailyPercentCustomizer : InputFieldManager.Customizer {
    override fun updateInputFilters(event: InputFieldManager.Event): List<InputFilter> =
        when (event) {
            InputFieldManager.Event.ON_INIT -> emptyList()
            InputFieldManager.Event.ON_FOCUS_GAINED -> listOf(InputFilter.LengthFilter(PERCENT_FIELD_LENGTH_FOCUSED))
            InputFieldManager.Event.ON_FOCUS_LOST -> listOf(InputFilter.LengthFilter(PERCENT_FIELD_LENGTH_NOT_FOCUSED))
        }
}
