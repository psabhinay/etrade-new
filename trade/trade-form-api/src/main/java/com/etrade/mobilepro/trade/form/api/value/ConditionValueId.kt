package com.etrade.mobilepro.trade.form.api.value

interface ConditionValueId {
    val greaterThanOrEqualTo: String
    val lessThanOrEqualTo: String
}
