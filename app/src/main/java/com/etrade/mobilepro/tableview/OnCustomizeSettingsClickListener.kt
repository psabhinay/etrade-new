package com.etrade.mobilepro.tableview

import android.view.View

/**
 * This goes under a table view.
 */
interface OnCustomizeSettingsClickListener {

    /**
     * Called when a customize view is clicked.
     */
    fun onCustomizeViewClicked(v: View)
}
