package com.etrade.mobilepro.news.filter.portfolio

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.news.presentation.filter.portfolio.PortfolioFilterDataSource
import com.etrade.mobilepro.news.presentation.filter.portfolio.PortfolioFilterItem
import com.etrade.mobilepro.util.NetworkState
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.mockk.mockkStatic
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations

private const val ALL_PORTFOLIOS_STRING = "All"
private const val ACC_ONE_SHORT_NAME = "short_name_1"
private const val ENC_ALL_ACC_ID = "abrakadabra_all"
private const val ENC_ACC_ONE_ID = "abrakadabra_one"
private const val AS_LIVE_DATA_PACKAGE = "com.etrade.mobilepro.caching.androidx.ExtensionKt"

class PortfolioFilterDataSourceTest {
    private val accountOne = Account(accountShortName = ACC_ONE_SHORT_NAME, encAccountId = ENC_ACC_ONE_ID)
    private val defaultFilterItem = PortfolioFilterItem(ALL_PORTFOLIOS_STRING, ENC_ALL_ACC_ID, isBaseAllPortfoliosItem = true, accountId = "")
    private val filterItemOne = PortfolioFilterItem(ACC_ONE_SHORT_NAME, ENC_ACC_ONE_ID, isBaseAllPortfoliosItem = false, accountId = "")

    private val accounts = listOf(accountOne)
    private val filters = listOf(defaultFilterItem, filterItemOne)

    private fun mockRepo(accounts: List<Account> = emptyList()) = mock<AccountListRepo> {
        on { getAccountList() } doReturn (accounts)
    }

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var sut: PortfolioFilterDataSource

    @Mock
    private lateinit var mapper: (List<Account>) -> List<PortfolioFilterItem>

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        mockkStatic(AS_LIVE_DATA_PACKAGE)
        whenever(mapper.invoke(accounts)).thenReturn(filters)
        whenever(mapper.invoke(emptyList())).thenReturn(listOf(defaultFilterItem))
    }

    @Test
    fun `initially sut has ALL item selected`() {
        sut = PortfolioFilterDataSource(mockRepo(accounts), mapper)

        sut.selectedItem
            .test()
            .assertHasValue()
            .assertValue(defaultFilterItem)
    }

    @Test
    fun `successful accounts retrieving non empty results in networkState == Success and updated filter items of size + 1`() {
        sut = PortfolioFilterDataSource(mockRepo(accounts), mapper)

        sut.items
            .test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it == filters
            }

        sut.networkState
            .test()
            .assertHasValue()
            .assertValue(NetworkState.Success)
    }

    @Test
    fun `successful accounts retrieving empty results in networkState == Success and updated filter items of size == 1`() {
        sut = PortfolioFilterDataSource(mockRepo(), mapper)

        sut.items
            .test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it.size == 1 && it[0] == defaultFilterItem
            }

        sut.networkState
            .test()
            .assertHasValue()
            .assertValue(NetworkState.Success)
    }

    @Test
    fun `item selection results in changed result of getSelectedItemIndex`() {
        sut = PortfolioFilterDataSource(mockRepo(accounts), mapper)

        sut.items
            .test()
            .awaitValue()
            .assertValue {
                it.size == 2 && it[0] == defaultFilterItem && it[1] == filterItemOne
            }

        assertTrue("default selection is 0", sut.getSelectedItemIndex() == 0)

        sut.setSelectedItem(filterItemOne)

        assertTrue("new selection is 1", sut.getSelectedItemIndex() == 1)
    }
}
