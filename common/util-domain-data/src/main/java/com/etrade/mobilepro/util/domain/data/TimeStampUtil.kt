package com.etrade.mobilepro.util.domain.data

fun <T> Iterable<T>.resolveTimeStamp(
    transform: T.() -> Long?,
    timeStampFormatter: (Long) -> String
): String? {
    return mapNotNull { it.transform() }
        .maxOrNull()
        ?.let { timeStampFormatter(it) }
}
