package com.etrade.mobilepro.billpay.data.rest

import com.etrade.mobilepro.billpay.api.MerchantInfo
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.PayeeAddress
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

private const val NULL_IN_STRING = "null"
@JsonClass(generateAdapter = true)
data class SavePayeeRequestDto(
    @Json(name = "value")
    val value: PayeeDetailDto
) {
    companion object {
        fun fromPayee(payee: Payee, sessionId: String): SavePayeeRequestDto =
            SavePayeeRequestDto(
                PayeeDetailDto(
                    address = fromApi(payee.address),
                    isAddressAvailable = payee.isAddressAvailable,
                    accountNumber = payee.accountNumber,
                    phoneNumber = payee.phoneNumber,
                    nickName = if (payee.nickName.isNullOrEmpty()) { NULL_IN_STRING } else { payee.nickName },
                    name = payee.name,
                    sessionId = sessionId,
                    leadDays = null,
                    eBillActivationStatus = null,
                    payeeId = payee.payeeId,
                    merchantInfo = fromApi(payee.merchantInfo),
                    merchantZipRequired = payee.merchantZipRequired,
                    category = payee.category
                )
            )

        private fun fromApi(payeeAddress: PayeeAddress?): PayeeAddressDto? {
            val address = payeeAddress ?: return null
            return PayeeAddressDto(
                address1 = address.address1,
                address2 = address.address2,
                city = address.city,
                state = address.state,
                zipCode = address.zipCode
            )
        }

        private fun fromApi(merchantInfo: MerchantInfo?): MerchantInfoDto? {
            val apiData = merchantInfo ?: return null
            return MerchantInfoDto(
                merchantId = apiData.id,
                merchantZipCode = apiData.merchantZip,
                merchantZipCode4 = apiData.merchantZip4
            )
        }
    }
}
