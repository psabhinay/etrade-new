package com.etrade.mobilepro.trade.data.contingent

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.MutableInputValuesSnapshot
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.trade.api.AdvancedTradeType.Contingent
import com.etrade.mobilepro.trade.api.OrderPlaceResponse
import com.etrade.mobilepro.trade.api.OrderPreviewResponse
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderPreviewResponse
import com.etrade.mobilepro.trade.data.TradeDelegate
import com.etrade.mobilepro.trade.data.TradeOrderArguments
import com.etrade.mobilepro.trade.data.TradeRequestParamsUpdater
import com.etrade.mobilepro.trade.data.advanced.ADVANCED_ORDER_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.AdvancedTradingApiClient
import com.etrade.mobilepro.trade.data.advanced.AdvancedTradingApiClient.Companion.PATH_CONTINGENT
import com.etrade.mobilepro.trade.data.advanced.INITIAL_STOP_PRICE_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_COUNT_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_TRIGGER_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.POSITION_EFFECT_KEY
import com.etrade.mobilepro.trade.data.advanced.PREVIEW_ID_KEY
import com.etrade.mobilepro.trade.data.advanced.PRIMARY_SECURITY_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.SIDE_KEY
import com.etrade.mobilepro.trade.data.advanced.addSymbol
import com.etrade.mobilepro.trade.data.advanced.getAdvancedOrderPlaceTradeParams
import com.etrade.mobilepro.trade.data.advanced.getAdvancedOrderPreviewTradeParams
import com.etrade.mobilepro.trade.data.advanced.place
import com.etrade.mobilepro.trade.data.advanced.preview
import com.etrade.mobilepro.trade.data.advanced.removeRedundantAdvancedOrderParams
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.etrade.mobilepro.trade.data.extractAction
import com.etrade.mobilepro.trade.data.fixAdvancedOrderOptionAction
import com.etrade.mobilepro.trade.data.fixLimitStopPriceParam
import com.etrade.mobilepro.trade.data.stock.StockOrderConstants
import com.etrade.mobilepro.trade.form.api.getTransactionType
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithContingentOrderInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractPriceType
import com.etrade.mobilepro.util.filterNotNullValues
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

internal class ContingentOrderTradeDelegate<T>(
    private val inputId: T,
    private val apiClient: AdvancedTradingApiClient,
    private val tradeRequestParamsUpdater: TradeRequestParamsUpdater? = null,
    private val moshi: Moshi
) : TradeDelegate where T : WithContingentOrderInputId,
                        T : WithAdvancedTradeFormInputId,
                        T : WithPriceTradeFormInputId,
                        T : WithActionTradeFormInputId {

    override suspend fun previewOrder(map: InputValuesSnapshot, marketDestination: MarketDestination): ETResult<OrderPreviewResponse> {
        return apiClient.preview(PATH_CONTINGENT, preparePreviewParameters(map), Contingent)
    }

    override suspend fun placeOrder(arguments: TradeOrderArguments, order: TradeOrder): ETResult<OrderPlaceResponse> {
        val response = order.response
        require(response is AdvancedOrderPreviewResponse)
        return apiClient.place(PATH_CONTINGENT, preparePlaceParameters(arguments.params, response.previewId, response.orders[0].orderType.typeCode), Contingent)
    }

    private fun preparePlaceParameters(snapshot: InputValuesSnapshot, previewId: String?, orderType: String): Map<String, String> {
        val result = getOrderParams(snapshot)
        result.putAll(
            getAdvancedOrderPlaceTradeParams(
                formTarget = "${PATH_CONTINGENT}preview",
                tradeX = "55"
            )
        )

        val isOption = inputId is OptionTradeFormInputId
        result.extractAction(isOption, inputId)?.getTransactionType(isOption)?.let {
            result["${SIDE_KEY}0"] = it.orderAction.value
            result["${POSITION_EFFECT_KEY}0"] = it.positionEffect
        }
        result["${INITIAL_STOP_PRICE_KEY}0"] = result[inputId.stopPrice]
        result["${ORDER_TYPE_KEY}0"] = orderType
        result["${ORDER_TRIGGER_KEY}0"] = result[inputId.priceType]
        result["${ADVANCED_ORDER_TYPE_KEY}0"] = AdvancedOrderType.AO_CONTINGENT.typeCode
        result[PREVIEW_ID_KEY] = previewId
        result[StockOrderConstants.OVERNIGHT_INDICATOR_FLAG] = "0"
        result[ORDER_COUNT_KEY] = "1"

        return result.also {
            it.removeRedundantAdvancedOrderParams(inputId)
            tradeRequestParamsUpdater?.updatePlaceOrderParams(it)
        }.mapValues {
            it.value as? String
        }.filterNotNullValues()
    }

    private fun preparePreviewParameters(snapshot: InputValuesSnapshot): Map<String, String> {
        val result = getOrderParams(snapshot)
        result.putAll(
            getAdvancedOrderPreviewTradeParams(
                formTarget = "contingentcreateentry",
                previewX = "53"
            )
        )
        return result.also { tradeRequestParamsUpdater?.updatePreviewOrderParams(it) }
            .mapValues { it.value as? String }
            .filterNotNullValues()
    }

    private fun getOrderParams(snapshot: InputValuesSnapshot): MutableMap<String, Any?> {
        return snapshot.toMutableMap().apply {
            fixLimitStopPriceParam(inputId, inputId.extractPriceType(this))
            removeRedundantAdvancedOrderParams(inputId)
            fixOptionLegSymbol()
            fixAdvancedOrderOptionAction(inputId)
            val contingentInputId = inputId.contingentOrderInputId
            val contingentSymbol = contingentInputId.extractSymbol(snapshot)
            put(contingentInputId.contingentSymbol, contingentSymbol)

            addSymbol(inputId, PRIMARY_SECURITY_TYPE_KEY, 0)
            addSymbol(contingentInputId, contingentInputId.securityType)
        }
    }

    private fun MutableInputValuesSnapshot.fixOptionLegSymbol() {
        if (inputId is OptionTradeFormInputId) {
            val type = Types.newParameterizedType(List::class.java, LegDetailsCommonDto::class.java)
            val adapter: JsonAdapter<List<LegDetailsCommonDto>?> = moshi.adapter(type)
            val legDetails = adapter.fromJson(this[inputId.tradeLegs].toString())?.firstOrNull()
            legDetails?.let { dto ->
                this[inputId.symbol] = inputId.symbolCoder.encode(object : WithSymbolInfo {
                    override val instrumentType: InstrumentType = InstrumentType.OPTN
                    override val symbol: String = dto.symbol
                })
            }
        }
    }
}
