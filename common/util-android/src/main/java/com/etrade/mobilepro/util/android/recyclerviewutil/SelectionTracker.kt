package com.etrade.mobilepro.util.android.recyclerviewutil

import androidx.recyclerview.widget.RecyclerView

interface SelectionTracker {
    var selectionChangedListener: ((Int, Int) -> Unit)?
    val current: Int
    fun itemWasSelectedAt(position: Int)
}

class DefaultSelectionTracker : SelectionTracker {
    private var _current: Int = RecyclerView.NO_POSITION
    override val current: Int
        get() = _current
    override var selectionChangedListener: ((Int, Int) -> Unit)? = null

    override fun itemWasSelectedAt(position: Int) {
        selectionChangedListener?.invoke(_current, position)
        _current = position
    }
}
