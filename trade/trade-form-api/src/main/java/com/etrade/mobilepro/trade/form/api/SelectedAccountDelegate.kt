package com.etrade.mobilepro.trade.form.api

import com.etrade.mobilepro.trade.api.TradeAccount

interface SelectedAccountDelegate {
    fun getCurrentAccountId(securityType: SecurityType): String?
    fun setUserSelectedAccountId(securityType: SecurityType, selectedAccountId: String)

    fun onSecurityType(securityType: SecurityType)

    interface Factory {
        fun create(accounts: List<TradeAccount>, fallbackAccountId: String?): SelectedAccountDelegate
    }
}
