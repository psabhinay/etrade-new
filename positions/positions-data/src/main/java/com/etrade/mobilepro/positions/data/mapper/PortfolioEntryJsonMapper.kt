package com.etrade.mobilepro.positions.data.mapper

import com.etrade.eo.core.util.toBigDecimalOrDefault
import com.etrade.mobilepro.positions.api.PortfolioEntry
import com.etrade.mobilepro.positions.data.dto.PortfolioEntryDto
import java.math.BigDecimal

internal class PortfolioEntryJsonMapper {
    private val positionMapper = PortfolioPositionMapper()
    fun map(jsonDto: PortfolioEntryDto): PortfolioEntry {
        jsonDto.totalCash.toBigDecimalOrDefault()
        return PortfolioEntry(
            accountId = jsonDto.accountId ?: "",
            totalCash = jsonDto.totalCash?.toBigDecimalOrNull() ?: BigDecimal.ZERO,
            positions = (jsonDto.positions ?: emptyList()).map { positionMapper.map(it) }
        )
    }
}
