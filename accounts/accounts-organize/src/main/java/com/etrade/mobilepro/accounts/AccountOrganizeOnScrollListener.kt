package com.etrade.mobilepro.accounts

import android.animation.Animator
import android.view.View
import androidx.recyclerview.widget.RecyclerView

private const val ANIMATION_DURATION = 100L
private const val ALPHA_VISIBLE = 1F
private const val ALPHA_INVISIBLE = 0F

class AccountOrganizeOnScrollListener(private val gradientView: View) : RecyclerView.OnScrollListener() {

    private var inAnimation: Boolean = false

    private val animationListener: Animator.AnimatorListener = object : Animator.AnimatorListener {
        override fun onAnimationRepeat(animation: Animator?) {
            // nop
        }

        override fun onAnimationEnd(animation: Animator?) {
            inAnimation = false
        }

        override fun onAnimationCancel(animation: Animator?) {
            inAnimation = false
        }

        override fun onAnimationStart(animation: Animator?) {
            inAnimation = true
        }
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (inAnimation) {
            return
        }

        if (recyclerView.canScrollVertically(1) && gradientView.alpha < ALPHA_VISIBLE) {
            gradientView.animate().alpha(ALPHA_VISIBLE).setDuration(ANIMATION_DURATION).setListener(animationListener).start()
        } else if (!recyclerView.canScrollVertically(1) && gradientView.alpha == ALPHA_VISIBLE) {
            gradientView.animate().alpha(ALPHA_INVISIBLE).setDuration(ANIMATION_DURATION).setListener(animationListener).start()
        }
    }
}
