package com.etrade.mobilepro.home

import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import com.etrade.mobilepro.common.OverlayFragmentManager
import com.etrade.mobilepro.home.model.PUSH_NOTIFICATION_PROMPT_IN_KEY
import com.etrade.mobilepro.home.model.PromptInDialogsHelper

class OverlayFragmentHelper(
    private val defaultFragmentManager: FragmentManager,
    private val promptInDialogsHelper: PromptInDialogsHelper
) {

    private var nowShowingTag: String? = null
    private var dialog: DialogFragment? = null

    fun isCurrentFragment(tag: String?) =
        defaultFragmentManager.findFragmentByTag(tag) != null

    @Suppress("LongParameterList")
    fun onOverlayFragment(
        visible: Boolean,
        containerView: View,
        getFragment: () -> Fragment,
        showDialogs: Boolean = false,
        tag: String? = null,
        showAsDialogFragment: Boolean = false,
        fm: FragmentManager? = null
    ) {
        if (showAsDialogFragment) {
            when {
                visible -> showDialogOverlayFragment(tag, getFragment, fm)
                tag == nowShowingTag -> hideDialogOverlayFragment()
            }
        } else {
            when {
                visible -> showOverlayFragment(tag, containerView, getFragment)
                tag == nowShowingTag -> hideOverlayFragment(containerView, showDialogs)
            }
        }
    }

    private fun showOverlayFragment(tag: String?, containerView: View, getFragment: () -> Fragment) {
        nowShowingTag = tag

        val id = containerView.id
        defaultFragmentManager.commit {
            defaultFragmentManager.findFragmentById(id)?.let {
                remove(it)
            }
            add(id, getFragment(), tag)
        }
        containerView.visibility = View.VISIBLE
    }

    private fun hideOverlayFragment(containerView: View, showDialogs: Boolean) {
        defaultFragmentManager.findFragmentById(containerView.id)?.let {
            defaultFragmentManager.commit { remove(it) }
            containerView.visibility = View.GONE
        }

        if (showDialogs) {
            promptInDialogsHelper.showPendingDialog(PUSH_NOTIFICATION_PROMPT_IN_KEY, defaultFragmentManager)
        }
        nowShowingTag = null
    }

    private fun showDialogOverlayFragment(tag: String?, getFragment: () -> Fragment, customFragmentManager: FragmentManager?) {
        nowShowingTag = tag
        dialog = getFragment() as? DialogFragment
        dialog?.show(customFragmentManager ?: defaultFragmentManager, tag)
    }

    private fun hideDialogOverlayFragment() {
        dialog?.dismiss()
        nowShowingTag = null
    }
}

fun OverlayFragmentHelper.toOverlayFragmentManager(containerView: View): OverlayFragmentManager {
    return object : OverlayFragmentManager {
        override fun manageFragment(
            visible: Boolean,
            fragmentProvider: () -> Fragment,
            showDialogs: Boolean,
            tag: String?,
            showAsDialogFragment: Boolean,
            fragmentManager: FragmentManager?
        ) {
            onOverlayFragment(visible, containerView, fragmentProvider, showDialogs, tag, fm = fragmentManager, showAsDialogFragment = showAsDialogFragment)
        }
    }
}
