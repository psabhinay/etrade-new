package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.filter.etf.EtfFundProfileFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfPerformanceFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfPortfolioFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfRiskFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfTechnicalsFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.ExpensesFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.FeesFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.FundProfileFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.PerformanceFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.PortfolioFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.RiskFilter
import com.etrade.mobilepro.screener.api.filter.stock.EarningsDividendsFilter
import com.etrade.mobilepro.screener.api.filter.stock.FundamentalsFilter
import com.etrade.mobilepro.screener.api.filter.stock.MarketSegmentFilter
import com.etrade.mobilepro.screener.api.filter.stock.OpinionsFilter
import com.etrade.mobilepro.screener.api.filter.stock.PriceVolumeFilter
import com.etrade.mobilepro.screener.api.filter.stock.TechnicalsFilter
import com.etrade.mobilepro.screener.data.json.etf.EtfFundProfileFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.etf.EtfPerformanceFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.etf.EtfPortfolioFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.etf.EtfRiskFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.etf.EtfTechnicalsFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.mutualfund.ExpensesFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.mutualfund.FeesFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.mutualfund.FundProfileFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.mutualfund.PerformanceFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.mutualfund.PortfolioFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.mutualfund.RiskFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.stock.EarningsDividendsFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.stock.FundamentalsFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.stock.MarketSegmentFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.stock.OpinionsFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.stock.PriceVolumeFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.stock.TechnicalsFilterModelGenerator
import dagger.Binds
import dagger.Module
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

@Module
abstract class ScreenersJsonGeneratorsModule {

    @Binds
    @IntoMap
    @ClassKey(EarningsDividendsFilter::class)
    abstract fun bindEarningsDividendsFilterValue(earningsDividendsFilterModelGenerator: EarningsDividendsFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(FundamentalsFilter::class)
    abstract fun bindFundamentalsFilterValue(fundamentalsFilterModelGenerator: FundamentalsFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(MarketSegmentFilter::class)
    abstract fun bindMarketSegmentFilterValue(marketSegmentFilterModelGenerator: MarketSegmentFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(OpinionsFilter::class)
    abstract fun bindOptionsFilterValue(opinionsFilterModelGenerator: OpinionsFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(PriceVolumeFilter::class)
    abstract fun bindPriceVolumeFilterValue(priceVolumeFilterModelGenerator: PriceVolumeFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(TechnicalsFilter::class)
    abstract fun bindTechnicalsFilterValue(technicalsFilterModelGenerator: TechnicalsFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(ExpensesFilter::class)
    abstract fun bindExpensesFilterValue(expensesFilterModelGenerator: ExpensesFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(FeesFilter::class)
    abstract fun bindFeesFilterValue(feesFilterModelGenerator: FeesFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(FundProfileFilter::class)
    abstract fun bindFundProfileFilterValue(fundProfileFilterModelGenerator: FundProfileFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(PerformanceFilter::class)
    abstract fun bindPerformanceFilterValue(performanceFilterModelGenerator: PerformanceFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(PortfolioFilter::class)
    abstract fun bindPortfolioFilterValue(portfolioFilterModelGenerator: PortfolioFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(RiskFilter::class)
    abstract fun bindRiskFilterValue(riskFilterModelGenerator: RiskFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(EtfFundProfileFilter::class)
    abstract fun bindEtfFundProfileFilterValue(etfFundProfileFilterModelGenerator: EtfFundProfileFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(EtfPerformanceFilter::class)
    abstract fun bindEtfPerformanceFilterValue(riskFilterModelGenerator: EtfPerformanceFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(EtfPortfolioFilter::class)
    abstract fun bindEtfPortfolioFilterValue(etfPortfolioFilterModelGenerator: EtfPortfolioFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(EtfRiskFilter::class)
    abstract fun bindEtfRiskFilterValue(etfRiskFilterModelGenerator: EtfRiskFilterModelGenerator): FilterModelGenerator

    @Binds
    @IntoMap
    @ClassKey(EtfTechnicalsFilter::class)
    abstract fun bindEtfTechnicalsFilterValue(riskFilterModelGenerator: EtfTechnicalsFilterModelGenerator): FilterModelGenerator
}
