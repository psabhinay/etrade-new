package com.etrade.mobilepro.common

/**
 * This annotation is supposed to be processed in Activity when it is going to become visible to the user.
 * The purpose is to prevent user from being landed at fragment that requires authentication.
 * For example, if authenticated user puts application in background and system kills the process and then user opens it again.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class RequireLogin
