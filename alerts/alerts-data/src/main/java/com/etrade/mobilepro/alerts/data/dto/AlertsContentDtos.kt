package com.etrade.mobilepro.alerts.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false, name = "GetAlertResponse")
data class AlertContentResponseDto(
    @field:Element(name = "Message", required = false)
    var message: AlertMessageDto? = null
)

@Root(strict = false, name = "Message")
data class AlertMessageDto(
    @field:Element(name = "MessageId", required = false)
    var id: String? = null,
    @field:Element(name = "CreatedDate", required = false)
    var createdAt: String? = null,
    @field:Element(name = "ReadDate", required = false)
    var readAt: String? = null,
    @field:Element(name = "Subject", required = false)
    var subject: String? = null,
    @field:Element(name = "MessageText", required = false)
    var content: String? = null
)
