package com.etrade.mobilepro.dummy

import com.etrade.mobilepro.chart.api.ChartLine
import com.etrade.mobilepro.chart.api.ChartPoint
import com.etrade.mobilepro.chart.netassets.data.NetAssetsChartRepo
import com.etrade.mobilepro.chart.netassets.model.NetAssetsChartData
import com.etrade.mobilepro.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DummyNetAssetsChartRepo @Inject constructor() : NetAssetsChartRepo {

    @SuppressWarnings("MagicNumber")
    override suspend fun getNetAssetsChart(accounts: List<String>): Resource<NetAssetsChartData> = withContext(Dispatchers.Default) {
        delay(1000L)

        accounts.map {
            ChartLine(
                it,
                listOf(
                    DummyChartPoint("20191005", "719704.0"),
                    DummyChartPoint("20191105", "667236.0"),
                    DummyChartPoint("20191205", "1398898.0"),
                    DummyChartPoint("20200105", "496196.0"),
                    DummyChartPoint("20200205", "523608.0"),
                    DummyChartPoint("20200305", "873018.0")
                )
            )
        }.let { Resource.Success(NetAssetsChartData(it)) }
    }

    private class DummyChartPoint(override val rawX: String, override val rawY: String) : ChartPoint
}
