package com.etrade.mobilepro.bloombergtv.presentation.delegate.pip

import android.view.KeyEvent

internal class KeyEventDelegate(
    private val showUI: () -> Unit,
    private val hideUI: () -> Unit,
    private val startPlay: () -> Unit,
    private val isPlaying: () -> Boolean,
    private val fallback: (KeyEvent) -> Boolean
) {
    private val expectedShowCodes = setOf(KeyEvent.KEYCODE_HEADSETHOOK, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE, KeyEvent.KEYCODE_SPACE)
    // don't show the controls for volume adjustment
    private val expectedNonShowCodes = setOf(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_VOLUME_MUTE)

    fun dispatchKeyEvent(event: KeyEvent): Boolean {
        val keyCode = event.keyCode
        val uniqueDown = event.repeatCount == 0 && event.action == KeyEvent.ACTION_DOWN
        return processKeyCode(keyCode, uniqueDown, event)
    }

    private fun processKeyCode(
        keyCode: Int,
        uniqueDown: Boolean,
        event: KeyEvent
    ): Boolean = when {
        expectedShowCodes.contains(keyCode) -> showPressed(uniqueDown)
        keyCode == KeyEvent.KEYCODE_MEDIA_PLAY -> playPressed(uniqueDown)
        keyCode == KeyEvent.KEYCODE_MEDIA_STOP || keyCode == KeyEvent.KEYCODE_MEDIA_PAUSE -> stopPressed(uniqueDown)
        expectedNonShowCodes.contains(keyCode) -> fallback(event)
        keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_MENU -> backPressed(uniqueDown)
        else -> unexpectedKeyPressed(event)
    }

    private fun unexpectedKeyPressed(event: KeyEvent): Boolean {
        showUI()
        return fallback(event)
    }

    private fun backPressed(uniqueDown: Boolean): Boolean {
        if (uniqueDown) { hideUI() }
        return true
    }

    private fun stopPressed(uniqueDown: Boolean): Boolean {
        if (uniqueDown && !isPlaying()) {
            showUI()
        }
        return true
    }

    private fun showPressed(uniqueDown: Boolean): Boolean {
        if (uniqueDown) showUI()
        return true
    }

    private fun playPressed(uniqueDown: Boolean): Boolean {
        if (uniqueDown && !isPlaying()) {
            startPlay()
            showUI()
        }
        return true
    }
}
