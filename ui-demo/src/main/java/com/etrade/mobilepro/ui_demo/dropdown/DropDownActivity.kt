package com.etrade.mobilepro.ui_demo.dropdown

import android.os.Bundle
import android.widget.Switch
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dropdown.expandableselect.DefaultDropDownExpandableSelector
import com.etrade.mobilepro.dropdown.expandableselect.DropDownExpandableSelector
import com.etrade.mobilepro.ui_demo.databinding.ActivityDropdownBinding
import com.etrade.mobilepro.util.android.binding.viewBinding

class DropDownActivity : AppCompatActivity() {
    private val binding by viewBinding(ActivityDropdownBinding::inflate)

    private val viewModel by lazy { ViewModelProvider(this).get(DropDownViewModel::class.java) }
    private lateinit var expandableDropDown: DropDownExpandableSelector<DummyItem>
    private lateinit var expandableSingleDropDown: DropDownExpandableSelector<DummyItem>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupExpandableDropDown()
        bindViewModel()
        with(binding) {
            openExpandableDropDown.setOnClickListener {
                expandableDropDown.open()
            }
            openExpandableSingleDropDown.setOnClickListener {
                expandableSingleDropDown.open()
            }
            hideSingleItemSwitch.setOnClickListener { view ->
                expandableSingleDropDown.close()
                viewModel.singleItemSwitchChanged((view as Switch).isChecked)
            }
        }
    }

    private fun setupExpandableDropDown() {
        expandableDropDown = DefaultDropDownExpandableSelector(
            supportFragmentManager = supportFragmentManager,
            settings = DropDownExpandableSelector.Settings(
                tag = "EXPANDABLE_TAG",
                title = "Expandable drop down list",
                itemsData = viewModel.expandableItemsData,
                hideSingleSubItems = false,
                selectListener = {_, selected ->
                    viewModel.expandableItemSelected(selected.value)
                },
                onCancelListener = { fallbackSet ->
                    viewModel.expandableItemSelected(fallbackSet?.firstOrNull())
                }
            ),
            fallBackValuesProvider = {
                viewModel.getFallBackValues()
            }
        )
    }

    private fun setupExpandableDropDownHideSingleItem(hideSingleItem: Boolean) {
        expandableSingleDropDown = DefaultDropDownExpandableSelector(
            supportFragmentManager = supportFragmentManager,
            settings = DropDownExpandableSelector.Settings(
                tag = "EXPANDABLE_SINGLE_ELEM_TAG",
                title = "Expandable Single Sub Item",
                itemsData = viewModel.expandableSingleItemsData,
                hideSingleSubItems = hideSingleItem,
                selectListener = {_, selected ->
                    viewModel.expandableItemSelected(selected.value)
                },
                onCancelListener = { fallbackSet ->
                    viewModel.expandableItemSelected(fallbackSet?.firstOrNull())
                }
            ),
            fallBackValuesProvider = {
                viewModel.getFallBackSingleValues()
            }
        )
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(this, Observer {
            binding.selectedExpandable.text = it.selectedItem?.title
            setupExpandableDropDownHideSingleItem(it.isSingleItemHidden)
        })
    }
}
