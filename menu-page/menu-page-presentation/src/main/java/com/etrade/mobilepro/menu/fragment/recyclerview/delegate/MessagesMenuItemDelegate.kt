package com.etrade.mobilepro.menu.fragment.recyclerview.delegate

import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.dynamicui.api.DynamicLayout
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.MessagesMenuItem
import com.etrade.mobilepro.menu.fragment.recyclerview.holder.MessagesMenuItemViewHolder
import com.etrade.mobilepro.menupage.presentation.databinding.ViewMenuInboxMessagesBinding
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class MessagesMenuItemDelegate(
    private val lifecycleOwner: LifecycleOwner,
    private val toDynamicViewModels: (List<GenericLayout>) -> List<DynamicLayout>
) : AbsListItemAdapterDelegate<MessagesMenuItem, MenuItem, MessagesMenuItemViewHolder>() {

    override fun isForViewType(item: MenuItem, items: MutableList<MenuItem>, position: Int): Boolean {
        return item is MessagesMenuItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): MessagesMenuItemViewHolder {
        return MessagesMenuItemViewHolder(
            lifecycleOwner,
            toDynamicViewModels,
            parent.viewBinding(ViewMenuInboxMessagesBinding::inflate)
        )
    }

    override fun onBindViewHolder(item: MessagesMenuItem, holder: MessagesMenuItemViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }
}
