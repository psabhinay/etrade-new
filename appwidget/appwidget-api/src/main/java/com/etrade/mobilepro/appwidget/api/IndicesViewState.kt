package com.etrade.mobilepro.appwidget.api

import com.etrade.mobilepro.appwidget.api.settings.WidgetId
import com.etrade.mobilepro.marketsapi.Index
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.watchlistapi.WatchlistEntry

typealias WatchlistEntryTuple = Pair<WatchlistEntry, Level1Data?>

data class IndicesViewState(
    val isLoading: Boolean = false,
    val indices: List<Index>? = null,
    val watchlistEntryTuples: List<WatchlistEntryTuple>? = null,
    val watchlistId: String? = null,
    val formattedTimestamp: String? = null,
    val isError: Boolean = false,
    val widgetId: WidgetId = WidgetId(0) // currently only one settings set is supported
)

val IndicesViewState?.isUninitialized: Boolean
    get() = this == null ||
        indices == null && watchlistEntryTuples == null &&
        !isLoading && !isError
