package com.etrade.mobilepro.inboxmessages.dynamicui.api

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.MobileScreen

interface DynamicScreenContentUpdater {
    fun updateWithInboxMessages(place: InboxMessagesWidgetPlace?, input: MobileScreen?): List<BaseScreenView>
}
