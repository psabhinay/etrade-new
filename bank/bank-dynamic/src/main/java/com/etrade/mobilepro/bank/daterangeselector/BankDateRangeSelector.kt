package com.etrade.mobilepro.bank.daterangeselector

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import org.threeten.bp.LocalDate

class BankDateRangeSelector(
    val resources: Resources,
    private val startDate: LiveData<LocalDate>,
    private val endDate: LiveData<LocalDate>
) : GenericLayout {

    override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
        BankDateRangeSelectorViewModel(resources, startDate, endDate)
    }

    override val viewModelClass: Class<out DynamicViewModel> = BankDateRangeSelectorViewModel::class.java

    override val uniqueIdentifier: String = javaClass.name
}
