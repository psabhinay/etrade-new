package com.etrade.mobilepro.completeview

import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.util.ClickAction
import com.etrade.mobilepro.util.android.CallToAction

fun CallToActionDto.toCallToAction(): CallToAction = CallToAction(label, clickActionDto.toClickAction())

fun ClickActionDto.toClickAction(): ClickAction = ClickAction(
    webUrl = this.toWebDeepLink(),
    webViewUrl = this.toWebViewDeepLink(),
    appUrl = appUrl
)
