package com.etrade.mobilepro.movers.widget

import android.content.Context
import android.content.res.Resources
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.movers.MoverItem
import com.etrade.mobilepro.movers.Movers
import com.etrade.mobilepro.movers.MoversRouter
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.util.android.textutil.extractUnderlyingSymbol
import com.etrade.mobilepro.util.android.textutil.prepareOptionTicketDatePrice
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import kotlinx.coroutines.rx2.await
import javax.inject.Inject

private const val MAX_ITEMS_IN_WIDGET = 6

class MoversWidgetViewModel @Inject constructor(
    private val moversRouter: MoversRouter,
    private val watchlistRepo: WatchlistRepo,
    streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    streamingStatusController: StreamingStatusController,
    resources: Resources,
) : Movers(
    layoutId = 0, // unused
    title = "", // set manually in @Composable
    showActionView = false,
    callToAction = null,
    moversRouter = moversRouter,
    streamingQuoteProvider = streamingQuoteProvider,
    streamingStatusController = streamingStatusController,
    resources = resources,
    shouldDisplayOptionsData = true,
    streamSnapshotsToFlow = true
) {
    override val variableId: Int = 0 // unused
    override val ignoreEmptyFetchedMoversWarning: Boolean = true
    private var currentWatchlistId: String? = null

    fun refresh(watchlistId: String = DEVICE_WATCHLIST_ID, forced: Boolean = false) {
        if (watchlistId == currentWatchlistId && !forced) { return }
        currentWatchlistId = watchlistId
        getMovers()
    }

    override suspend fun fetchData(): ETResult<List<MoverItem>> {
        return runCatchingET {
            getSelectedWatchlistEntries()
                .map {
                    it.sortedByDescending { it.changePercent?.abs() }.take(MAX_ITEMS_IN_WIDGET)
                }
                .await()
                .map {
                    val (optionsExpiryDate, optionsPrice) = prepareOptionTicketDatePrice(it.ticker, it.typeCode, it.description)
                    createMoverItem(
                        symbol = it.ticker,
                        displaySymbol = it.underlyingSymbol?.takeIf { it.isNotBlank() }
                            ?: extractUnderlyingSymbol(it.description, it.typeCode) ?: it.ticker,
                        price = MarketDataFormatter.formatMarketDataCustom(it.lastPrice),
                        change = MarketDataFormatter.formatMarketPercDataWithSign(it.change),
                        decimalPercentChange = it.changePercent,
                        optionExpiryDate = optionsExpiryDate,
                        optionsStrikePricePlusType = optionsPrice
                    )
                }
        }
    }

    private fun getSelectedWatchlistEntries() = if (Watchlist.isAllWatchlistsItemId(currentWatchlistId)) {
        watchlistRepo
            .getAllWatchlistsEntries()
    } else {
        watchlistRepo
            .getWatchlistEntries(currentWatchlistId ?: DEVICE_WATCHLIST_ID)
    }

    fun onItemTapped(context: Context, item: MoverItem) {
        val instrumentType = if (item.optionsExpiryDate != null) {
            InstrumentType.OPTN
        } else {
            InstrumentType.EQ
        }
        moversRouter.navigateToQuoteDetails(context, item.ticker, instrumentType)
    }
}
