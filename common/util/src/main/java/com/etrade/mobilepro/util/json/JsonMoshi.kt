package com.etrade.mobilepro.util.json

/**
 * Uses Moshi to serialize the JSON, as opposed to Json annotation using Gson
 */
@Retention(AnnotationRetention.RUNTIME)
annotation class JsonMoshi
