package com.etrade.mobilepro.dropdown

interface DropDownSelector {
    fun open()
    fun close()
}
