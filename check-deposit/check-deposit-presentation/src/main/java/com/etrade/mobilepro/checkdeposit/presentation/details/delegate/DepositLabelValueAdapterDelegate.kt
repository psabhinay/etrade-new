package com.etrade.mobilepro.checkdeposit.presentation.details.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.checkdeposit.presentation.databinding.AdapterDepositDetailLabelValueBinding
import com.etrade.mobilepro.checkdeposit.presentation.details.DepositDetail
import com.etrade.mobilepro.checkdeposit.presentation.details.holder.DepositDetailLabelValueViewHolder
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class DepositLabelValueAdapterDelegate : AbsListItemAdapterDelegate<DepositDetail.LabelValue, DepositDetail, DepositDetailLabelValueViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): DepositDetailLabelValueViewHolder {
        return DepositDetailLabelValueViewHolder(
            parent.viewBinding(
                AdapterDepositDetailLabelValueBinding::inflate
            )
        )
    }

    override fun isForViewType(item: DepositDetail, items: MutableList<DepositDetail>, position: Int): Boolean = item is DepositDetail.LabelValue

    override fun onBindViewHolder(item: DepositDetail.LabelValue, holder: DepositDetailLabelValueViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }
}
