package com.etrade.mobilepro.util.android.footnotespan

import android.graphics.Canvas
import android.graphics.Paint
import android.text.Layout
import android.text.style.LeadingMarginSpan
import android.widget.TextView
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans
import kotlin.math.max
import kotlin.math.roundToInt

class FootnoteSectionContext(val symbolSuffix: String = ". ") {

    private val footnoteSpans: MutableList<FootnoteSpan> = mutableListOf()

    private var leadingMargin: Int = 0

    /**
     * Creates a footnote span in this context (so that they will all share the same margin.)
     * updateMargin must be called after all of the footnotes are added.
     */
    fun addFootnote(symbol: String): LeadingMarginSpan {
        return FootnoteSpan(symbol).also { footnoteSpans.add(it) }
    }

    /**
     * Calculates the maximum margin using the given paint. This must be called before the spans are
     * drawn or the margins will be incorrect.
     */
    fun updateMargin(paint: Paint) {
        leadingMargin = footnoteSpans.maxOfOrNull {
            it.measure(paint)
        } ?: 0
    }

    /**
     * Creates a footnote span in this context (so that they will all share the same margin) and
     * updates the maximum margin using this this TextView's paint.
     *
     * If all footnotes in this context are added using this method, then updateMargin should not be used.
     */
    fun addFootnote(textView: TextView, symbol: String): LeadingMarginSpan {
        return addFootnote(symbol).also {
            leadingMargin = max(leadingMargin, it.measure(textView))
        }
    }

    private fun LeadingMarginSpan.measure(paint: Paint): Int =
        paint.measureText((this as FootnoteSpan).symbolWithSuffix).roundToInt()

    private fun LeadingMarginSpan.measure(textView: TextView): Int = measure(textView.paint)

    private inner class FootnoteSpan(
        symbol: String
    ) : LeadingMarginSpan {

        val symbolWithSuffix = "$symbol$symbolSuffix"

        override fun getLeadingMargin(first: Boolean): Int {
            return leadingMargin
        }

        override fun drawLeadingMargin(
            canvas: Canvas,
            paint: Paint,
            x: Int,
            dir: Int,
            top: Int,
            baseline: Int,
            bottom: Int,
            text: CharSequence?,
            start: Int,
            end: Int,
            first: Boolean,
            layout: Layout?
        ) {
            if (first) {
                val orgStyle = paint.style
                paint.style = Paint.Style.FILL
                val width = paint.measureText(symbolWithSuffix)
                canvas.drawText(
                    symbolWithSuffix,
                    (leadingMargin + x - width) * dir,
                    bottom - paint.descent(),
                    paint
                )
                paint.style = orgStyle
            }
        }
    }
}

fun FootnoteSectionContext.buildFootnote(
    textView: TextView,
    symbol: String,
    text: String
): CharSequence {
    return buildSpannedString { inSpans(addFootnote(textView, symbol)) { append(text) } }
}
