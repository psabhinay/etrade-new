package com.etrade.mobilepro.marketscalendar.adapter.holder

import android.view.View
import com.etrade.mobilepro.marketscalendar.databinding.AdapterEconomicDetailsBinding
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails

class EconomicEventDetailsViewHolder(
    private val binding: AdapterEconomicDetailsBinding,
    noValue: String
) : CalendarEventViewHolder(binding.adapterEventItem, noValue, rootView = binding.root) {

    override fun bind(item: EventDetails) {
        super.bind(item)
        binding.adapterEventItem.icArrow.visibility = View.GONE
        binding.description.text = item.description
    }
}
