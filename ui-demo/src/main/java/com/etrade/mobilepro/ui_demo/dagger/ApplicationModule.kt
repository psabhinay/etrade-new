package com.etrade.mobilepro.ui_demo.dagger

import android.content.Context
import com.etrade.mobilepro.common.di.ApplicationScope
import com.etrade.mobilepro.ui_demo.UIDemoApplication
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: UIDemoApplication) {
    @Singleton
    @Provides
    internal fun provideContext(): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    internal fun provideUIDemoApplication(): UIDemoApplication {
        return application
    }

    @Singleton
    @Provides
    internal fun providesResources(context: Context) = context.resources

    @Singleton
    @Provides
    @ApplicationScope
    fun provideApplicationCoroutineScope(): CoroutineScope =
        CoroutineScope(SupervisorJob() + Dispatchers.Main)
}
