package com.etrade.mobilepro.trade.data.stoploss

import com.etrade.mobilepro.trade.data.DAY_TRADING_FLAG
import com.etrade.mobilepro.trade.data.DAY_TRADING_VER
import com.etrade.mobilepro.trade.data.advanced.FORM_TARGET_KEY
import com.etrade.mobilepro.trade.data.advanced.OPT_LEVEL
import com.etrade.mobilepro.trade.data.advanced.ORDER_COUNT_KEY
import com.etrade.mobilepro.trade.data.advanced.PLATFORM_KEY
import com.etrade.mobilepro.trade.data.advanced.PLATFORM_VALUE
import com.etrade.mobilepro.trade.data.advanced.PREVIEW_X_KEY
import com.etrade.mobilepro.trade.data.advanced.PRIMARY_SECURITY_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.VALUE_SECURITY_TYPE_STOCK
import com.etrade.mobilepro.trade.data.form.input.DefaultStopLossStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossStockTradeFormInputId
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
internal class StopLossOrderTradeDelegateStockTest : AbstractStopLossOrderTradeDelegateTest<StopLossStockTradeFormInputId>() {

    override val inputId: StopLossStockTradeFormInputId = DefaultStopLossStockTradeFormInputId

    @Test
    fun `PREVIEW - MARKET BUY 3 AAPL - STOP_LIMIT_ON_QUOTE GOOD_FOR_60_DAYS`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.stock,
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.stopLoss,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.stopLimit,
            inputId.stopLossInputId.limitPriceForStopLimit to "100",
            inputId.stopLossInputId.stopPriceForStopLimit to "150",
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.sell,
            inputId.stopLossInputId.quantity to "3",
            inputId.stopLossInputId.symbol to "AAPL:EQ",
            inputId.stopLossInputId.allOrNone to "0"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.stopLimit,
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.sell,
            inputId.stopLossInputId.quantity to "3",
            inputId.stopLossInputId.symbol to "AAPL",
            inputId.stopLossInputId.allOrNone to "0",
            inputId.stopLossInputId.limitPrice to "100",
            inputId.stopLossInputId.stopPrice to "150",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_STOCK,
            inputId.stopLossInputId.securityType to VALUE_SECURITY_TYPE_STOCK,
            ORDER_COUNT_KEY to "2",
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "otacreateentry",
            PREVIEW_X_KEY to "65",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }

    @Test
    fun `PREVIEW - LIMIT SELL_SHORT 3 AAPL GOOD_FOR_DAY - STOP_ON_QUOTE GOOD_FOR_60_DAYS`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.stock,
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "100",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.stopLoss,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.stop,
            inputId.stopLossInputId.stopPrice to "140",
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.buyToCover,
            inputId.stopLossInputId.quantity to "3",
            inputId.stopLossInputId.symbol to "AAPL:EQ",
            inputId.stopLossInputId.allOrNone to "0"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "100",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.stop,
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.buyToCover,
            inputId.stopLossInputId.quantity to "3",
            inputId.stopLossInputId.symbol to "AAPL",
            inputId.stopLossInputId.allOrNone to "0",
            inputId.stopLossInputId.stopPrice to "140",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_STOCK,
            inputId.stopLossInputId.securityType to VALUE_SECURITY_TYPE_STOCK,
            ORDER_COUNT_KEY to "2",
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "otacreateentry",
            PREVIEW_X_KEY to "65",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }

    @Test
    fun `PREVIEW - TRAILING_STOP_$ SELL_SHORT 3 AAPL GOOD_FOR_DAY - TRAILING_STOP_$ GOOD_FOR_60_DAYS`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.stock,
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.trailingStopDollar,
            inputId.stopPrice to "5",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.stopLoss,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.trailingStopDollar,
            inputId.stopLossInputId.stopPrice to "10",
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.buyToCover,
            inputId.stopLossInputId.quantity to "3",
            inputId.stopLossInputId.symbol to "AAPL:EQ",
            inputId.stopLossInputId.allOrNone to "0"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.trailingStopDollar,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.trailingStopDollar,
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.buyToCover,
            inputId.stopLossInputId.quantity to "3",
            inputId.stopLossInputId.symbol to "AAPL",
            inputId.stopLossInputId.allOrNone to "0",
            inputId.offsetValue to "5",
            inputId.stopLossInputId.offsetValue to "10",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_STOCK,
            inputId.stopLossInputId.securityType to VALUE_SECURITY_TYPE_STOCK,
            ORDER_COUNT_KEY to "2",
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "otacreateentry",
            PREVIEW_X_KEY to "65",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }

    @Test
    fun `PREVIEW - STOP_LIMIT_ON_QUOTE SELL_SHORT 3 AAPL GOOD_FOR_DAY - TRAILING_STOP_$ GOOD_FOR_60_DAYS`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.stock,
            inputId.symbol to "AAPL:EQ",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.stopLimit,
            inputId.limitPriceForStopLimit to "100",
            inputId.stopPriceForStopLimit to "130",
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to "0",
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.stopLoss,
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.trailingStopPercent,
            inputId.stopLossInputId.stopPrice to "2",
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.buyToCover,
            inputId.stopLossInputId.quantity to "3",
            inputId.stopLossInputId.symbol to "AAPL:EQ",
            inputId.stopLossInputId.allOrNone to "0"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.sellShort,
            inputId.limitPrice to "100",
            inputId.stopPrice to "130",
            inputId.quantity to "3",
            inputId.priceType to inputId.priceTypeValueId.stopLimit,
            inputId.term to inputId.termValueId.goodForDay,
            inputId.allOrNone to "0",
            inputId.stopLossInputId.priceType to inputId.stopLossInputId.priceTypeValueId.trailingStopPercent,
            inputId.stopLossInputId.term to inputId.stopLossInputId.termValueId.goodUntilCancel,
            inputId.stopLossInputId.action to inputId.stopLossInputId.actionValueId.buyToCover,
            inputId.stopLossInputId.quantity to "3",
            inputId.stopLossInputId.symbol to "AAPL",
            inputId.stopLossInputId.allOrNone to "0",
            inputId.stopLossInputId.offsetValue to "2",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_STOCK,
            inputId.stopLossInputId.securityType to VALUE_SECURITY_TYPE_STOCK,
            ORDER_COUNT_KEY to "2",
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "otacreateentry",
            PREVIEW_X_KEY to "65",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }
}
