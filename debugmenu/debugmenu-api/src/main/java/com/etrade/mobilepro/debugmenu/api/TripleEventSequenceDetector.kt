package com.etrade.mobilepro.debugmenu.api

import org.threeten.bp.Duration

private const val DEFAULT_TRIPLE_EVENT_INTERVAL = 500L

interface TripleEventSequenceDetector {
    /**
     * Setups detector
     * @param timeout - Time interval during which onEvent() method should be called three times or more in order to trigger the listener
     *                  time interval will start from the first onEvent() call. After it ends the state resets.
     *                  This method can be called once
     * @param listener - If the condition above fulfills it is triggered once.
     */
    fun setup(timeout: Duration = Duration.ofMillis(DEFAULT_TRIPLE_EVENT_INTERVAL), listener: () -> Unit)

    /**
     * Emits event, that should be counted during the defined time interval
     */
    fun onEvent()
}
