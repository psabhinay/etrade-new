package com.etrade.mobilepro.trade.data.customorder

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class CustomOrderCommonRequestBodyDto(
    @Json(name = "value")
    val content: CustomOrderCommonRequestDto
)

@JsonClass(generateAdapter = true)
internal data class CustomOrderCommonRequestDto(
    @Json(name = "accountId")
    val accountId: String,
    @Json(name = "acctDesc")
    val acctDesc: String,
    @Json(name = "allOrNone")
    val allOrNone: String,
    @Json(name = "clearancecode")
    val clearancecode: String,
    @Json(name = "dayTradingFlag")
    val dayTradingFlag: String,
    @Json(name = "dayTradingVer")
    val dayTradingVer: String,
    @Json(name = "destMktCode")
    val destMktCode: String,
    @Json(name = "limitPrice")
    val limitPrice: String,
    @Json(name = "optionLevelCd")
    val optionLevelCd: String,
    @Json(name = "orderNumber")
    val orderNumber: String,
    @Json(name = "orderSource")
    val orderSource: String,
    @Json(name = "orderTerm")
    val orderTerm: String,
    @Json(name = "orderType")
    val orderType: String,
    @Json(name = "origSysCodeReq")
    val origSysCodeReq: String,
    @Json(name = "originalOrderNo")
    val originalOrderNo: String,
    @Json(name = "overnightIndicatorFlag")
    val overnightIndicatorFlag: String,
    @Json(name = "overrideRestrictedCd")
    val overrideRestrictedCd: String,
    @Json(name = "previewId")
    val previewId: String,
    @Json(name = "priceType")
    val priceType: String,
    @Json(name = "reOrder")
    val reOrder: Boolean,
    @Json(name = "requestType")
    val requestType: String,
    @Json(name = "stopPrice")
    val stopPrice: String,
    @Json(name = "strategyType")
    val strategyType: String,
    @Json(name = "underlier")
    val underlier: String
)
