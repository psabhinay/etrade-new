package com.etrade.mobilepro.checkdeposit.presentation.confirmation

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.tracking.SCREEN_TITLE_CHECK_DEPOSIT_CONFIRMATION
import com.etrade.mobilepro.tracking.ScreenViewModel
import javax.inject.Inject

class CheckDepositConfirmationViewModel @Inject constructor() : ViewModel(), ScreenViewModel {

    override val screenName: String? = SCREEN_TITLE_CHECK_DEPOSIT_CONFIRMATION
}
