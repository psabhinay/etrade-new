package com.etrade.mobilepro.screener.api.repo

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.result.PrescreenData
import io.reactivex.Single

interface ScreenersRepo {
    /**
     * Returns matches count
     */
    fun preScreenResults(group: ScreenerType.Group, requestData: String): Single<Int>

    /**
     * Returns list of symbols
     */
    fun resultingPrescreenData(group: ScreenerType.Group, requestData: String): Single<List<PrescreenData>>
}
