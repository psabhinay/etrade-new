package com.etrade.mobilepro.biometric.presentation.enabler

import androidx.biometric.BiometricPrompt

private const val BIOMETRIC_GENERAL_ERROR_CODE = 1001
private const val BIOMETRIC_EMPTY_PASSWORD_CODE = 1002
private const val BIOMETRIC_INVALID_PASSWORD_CODE = 1003
private const val BIOMETRIC_LOGIN_SERVICE_ERROR_CODE = 1004
private const val BIOMETRIC_LOGIN_SERVICE_FAILURE_CODE = 1005

enum class BiometricEnablerError(val biometricConstant: Int) {
    ERROR_HW_UNAVAILABLE(BiometricPrompt.ERROR_HW_UNAVAILABLE),

    ERROR_UNABLE_TO_PROCESS(BiometricPrompt.ERROR_UNABLE_TO_PROCESS),

    ERROR_TIMEOUT(BiometricPrompt.ERROR_TIMEOUT),

    ERROR_NO_SPACE(BiometricPrompt.ERROR_NO_SPACE),

    ERROR_CANCELED(BiometricPrompt.ERROR_CANCELED),

    ERROR_LOCKOUT(BiometricPrompt.ERROR_LOCKOUT),

    ERROR_VENDOR(BiometricPrompt.ERROR_VENDOR),

    ERROR_LOCKOUT_PERMANENT(BiometricPrompt.ERROR_LOCKOUT_PERMANENT),

    ERROR_USER_CANCELED(BiometricPrompt.ERROR_USER_CANCELED),

    ERROR_NO_BIOMETRICS(BiometricPrompt.ERROR_NO_BIOMETRICS),

    ERROR_HW_NOT_PRESENT(BiometricPrompt.ERROR_HW_NOT_PRESENT),

    ERROR_NEGATIVE_BUTTON(BiometricPrompt.ERROR_NEGATIVE_BUTTON),

    ERROR_NO_DEVICE_CREDENTIAL(BiometricPrompt.ERROR_NO_DEVICE_CREDENTIAL),

    BIOMETRIC_EMPTY_PASSWORD(BIOMETRIC_EMPTY_PASSWORD_CODE),

    BIOMETRIC_INVALID_PASSWORD(BIOMETRIC_INVALID_PASSWORD_CODE),

    BIOMETRIC_LOGIN_SERVICE_ERROR(BIOMETRIC_LOGIN_SERVICE_ERROR_CODE),

    BIOMETRIC_LOGIN_SERVICE_FAILURE(BIOMETRIC_LOGIN_SERVICE_FAILURE_CODE),

    BIOMETRIC_GENERAL_ERROR(BIOMETRIC_GENERAL_ERROR_CODE);

    companion object {
        /**
         * Creates an BiometricEnablerError from a [BiometricPrompt].
         * @param biometricConstant ranging from 1-14
         */
        fun fromBiometricConstants(biometricConstant: Int?) =
            values().firstOrNull { biometricConstant == it.biometricConstant } ?: BIOMETRIC_GENERAL_ERROR
    }
}
