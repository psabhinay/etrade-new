package com.etrade.mobilepro.quoteapi

class QuoteContentLoadingFailed(msg: String?) : RuntimeException(msg)

class QuoteEmptyResponse(msg: String?) : RuntimeException(msg)
