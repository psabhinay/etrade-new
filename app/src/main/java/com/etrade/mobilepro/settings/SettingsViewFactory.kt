package com.etrade.mobilepro.settings

import android.content.Context
import android.content.res.Resources
import android.net.Uri
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.BuildConfig
import com.etrade.mobilepro.R
import com.etrade.mobilepro.appconfig.api.LIGHT_STREAMER_LEVEL1_IDENTIFIER
import com.etrade.mobilepro.appconfig.api.LIGHT_STREAMER_LEVEL2_IDENTIFIER
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.landing.api.repo.LandingRepo
import com.etrade.mobilepro.landing.usecase.LandingInteractor
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.pushnotification.api.repo.AlertPushSettingsRepo
import com.etrade.mobilepro.pushnotification.usecase.ManageSmartAlertSubscriptionUseCase
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.settings.dto.SettingDto
import com.etrade.mobilepro.settings.dto.SettingsViewDto
import com.etrade.mobilepro.settings.dto.SettingsViewType
import com.etrade.mobilepro.settings.viewmodel.SettingActionDynamicRefreshViewModel
import com.etrade.mobilepro.settings.viewmodel.SettingActionDynamicViewModel
import com.etrade.mobilepro.settings.viewmodel.SettingDefaultHomePageValuePickerViewModel
import com.etrade.mobilepro.settings.viewmodel.SettingDynamicViewModel
import com.etrade.mobilepro.settings.viewmodel.SettingSessionTimeOutValuePickerViewModel
import com.etrade.mobilepro.settings.viewmodel.SettingTextDynamicViewModel
import com.etrade.mobilepro.settings.viewmodel.SettingToggleViewModel
import com.etrade.mobilepro.settings.viewmodel.feature.ViewModelAction
import com.etrade.mobilepro.settings.viewmodel.feature.ViewModelActionId
import com.etrade.mobilepro.streaming.StreamingStatusNotifierDelegate
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.themeselection.NightModePreferences
import com.etrade.mobilepro.timeout.setting.service.repo.TimeoutSettingRepo
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.user.session.manager.SessionTimeoutManager
import com.etrade.mobilepro.usersubscriptionapi.UserSubscriptionRepo
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

class SettingsViewFactory @Inject constructor(
    private val res: Resources,
    private val context: Context,
    private val user: User,
    private val userPreferences: UserPreferences,
    private val streamingStatusController: StreamingStatusController,
    private val streamingStatusNotifierDelegate: StreamingStatusNotifierDelegate,
    private val timeoutSettingRepo: TimeoutSettingRepo,
    private val userSessionTimeoutManager: SessionTimeoutManager,
    private val landingRepo: LandingRepo,
    private val landingInteractor: LandingInteractor,
    private val loginPreferences: LoginPreferencesRepo,
    private val userSubscriptionRepo: UserSubscriptionRepo,
    private val alertPushSettingsRepo: AlertPushSettingsRepo,
    private val manageAlertSubscription: ManageSmartAlertSubscriptionUseCase,
    private val tracker: Tracker,
    private val appConfigRepo: AppConfigRepo,
    private val nightModePreferences: NightModePreferences
) : DynamicUiViewFactory {

    private fun getPickerDynamicModel(type: SettingsViewType?, dto: SettingDto): DynamicViewModel {
        return when (type) {
            SettingsViewType.SETTINGS_PICKER_SESSION_TIME_OUT -> SettingSessionTimeOutValuePickerViewModel(
                dto.title,
                res,
                R.layout.adapter_setting_picker,
                user,
                userPreferences,
                timeoutSettingRepo,
                userSessionTimeoutManager
            )
            SettingsViewType.SETTINGS_PICKER_DEFAULT_HOME_PAGE -> SettingDefaultHomePageValuePickerViewModel(
                dto.title,
                R.layout.adapter_setting_picker,
                landingRepo,
                landingInteractor,
                dto.secondaryTitle ?: dto.title,
                SettingDefaultHomePageValuePickerViewModel.Strings(
                    portfolioTitle = context.getString(R.string.landing_page_portfolio),
                    bottomGhostText = context.getString(R.string.landing_selection_desc)
                ),
                tracker,
                res
            )
            else -> throw DynamicUiViewFactoryException("SettingsViewFactory registered to unknown type")
        }
    }

    @SuppressWarnings("LongMethod", "ComplexMethod")
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is SettingsViewDto) {
            return object : GenericLayout {

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    with(dto) {
                        when (viewType) {
                            SettingsViewType.SETTINGS_TEXT_VERSION -> {
                                val versionName = getAppVersion()
                                SettingTextDynamicViewModel(
                                    title = data.title,
                                    value = " $versionName",
                                    alterValue = getAppVersionAndStreamersInfo(versionName),
                                    layoutId = R.layout.adapter_setting_text
                                )
                            }
                            SettingsViewType.SETTINGS_ACTION_END_USER_LICENSE_AGREEMENT,
                            SettingsViewType.SETTINGS_ACTION_PRIVACY_POLICY,
                            SettingsViewType.SETTINGS_ACTION_DISCLOSURES,
                            SettingsViewType.SETTINGS_ACTION_TRADING_DEFAULTS -> SettingActionDynamicViewModel(
                                data.title,
                                data.value,
                                R.layout.adapter_setting_action,
                                dto.ctaList?.first()?.clickActionDto?.appUrl,
                                dto.ctaList?.first()?.clickActionDto?.toWebViewDeepLink()
                                    ?.let { Uri.parse(it) },
                                dto.ctaList?.first()?.clickActionDto?.toWebDeepLink()
                                    ?.let { Uri.parse(it) },
                                viewType,
                                tracker
                            )
                            SettingsViewType.SETTINGS_TOGGLE_BIOMETRIC_AUTH -> SettingToggleViewModel(
                                data.title,
                                R.layout.adapter_setting_toggle,
                                streamingStatusController,
                                streamingStatusNotifierDelegate,
                                loginPreferences,
                                viewType,
                                alertPushSettingsRepo = alertPushSettingsRepo,
                                userSubscriptionRepo = userSubscriptionRepo,
                                user = user,
                                actionSet = setOf(ViewModelAction.EnableBiometric()),
                                manageAlertSubscription = manageAlertSubscription,
                                tracker = tracker
                            )
                            SettingsViewType.SETTINGS_TOGGLE_REMEMBER_USER_ID -> SettingToggleViewModel(
                                data.title,
                                R.layout.adapter_setting_toggle,
                                streamingStatusController,
                                streamingStatusNotifierDelegate,
                                loginPreferences,
                                viewType,
                                alertPushSettingsRepo = alertPushSettingsRepo,
                                userSubscriptionRepo = userSubscriptionRepo,
                                user = user,
                                actionSet = setOf(),
                                manageAlertSubscription = manageAlertSubscription,
                                tracker = tracker
                            )
                            SettingsViewType.SETTINGS_TOGGLE_STREAMING -> SettingToggleViewModel(
                                data.title,
                                R.layout.adapter_setting_toggle,
                                streamingStatusController,
                                streamingStatusNotifierDelegate,
                                loginPreferences,
                                viewType,
                                alertPushSettingsRepo = alertPushSettingsRepo,
                                userSubscriptionRepo = userSubscriptionRepo,
                                user = user,
                                actionSet = setOf(
                                    ViewModelAction.Popup(
                                        ViewModelActionId.START_MARKET_DATA_AGREEMENT,
                                        ViewModelActionId.MARKET_DATA_AGREEMENT_RESULT,
                                        title = res.getString(R.string.please_note),
                                        message = res.getString(R.string.mda_toggle_message)
                                    ),
                                    ViewModelAction.Popup(
                                        ViewModelActionId.SHOW_UNABLE_TO_ENABLE_STREAMING_POPUP,
                                        ViewModelActionId.SHOW_UNABLE_TO_ENABLE_STREAMING_POPUP,
                                        title = res.getString(R.string.error),
                                        message = res.getString(R.string.mda_unable_to_enable_stream),
                                        okTitle = res.getString(R.string.ok),
                                        showCancelButton = false
                                    )
                                ),
                                manageAlertSubscription = manageAlertSubscription,
                                tracker = tracker
                            )
                            SettingsViewType.SETTINGS_PICKER_SESSION_TIME_OUT,
                            SettingsViewType.SETTINGS_PICKER_DEFAULT_HOME_PAGE -> getPickerDynamicModel(
                                viewType,
                                data
                            )
                            SettingsViewType.SETTINGS_TOGGLE_STOCK_ALERTS -> SettingToggleViewModel(
                                data.title,
                                R.layout.adapter_setting_toggle,
                                streamingStatusController,
                                streamingStatusNotifierDelegate,
                                loginPreferences,
                                viewType,
                                alertPushSettingsRepo = alertPushSettingsRepo,
                                userSubscriptionRepo = userSubscriptionRepo,
                                user = user,
                                actionSet = setOf(
                                    ViewModelAction.Popup(
                                        ViewModelActionId.SHOW_STOCK_ALERT_POPUP,
                                        ViewModelActionId.STOCK_ALERT_POPUP_RESULT,
                                        title = res.getString(R.string.please_note),
                                        message = res.getString(R.string.stock_alert_push_popup_message),
                                        showAlertTermsAndConditions = true
                                    )
                                ),
                                manageAlertSubscription = manageAlertSubscription,
                                tracker = tracker
                            )
                            SettingsViewType.SETTINGS_TOGGLE_ACCOUNT_ALERTS -> SettingToggleViewModel(
                                data.title,
                                R.layout.adapter_setting_toggle,
                                streamingStatusController,
                                streamingStatusNotifierDelegate,
                                loginPreferences,
                                viewType,
                                alertPushSettingsRepo = alertPushSettingsRepo,
                                userSubscriptionRepo = userSubscriptionRepo,
                                user = user,
                                actionSet = setOf(
                                    ViewModelAction.Popup(
                                        ViewModelActionId.SHOW_ACCOUNT_ALERT_POPUP,
                                        ViewModelActionId.ACCOUNT_ALERT_POPUP_RESULT,
                                        title = res.getString(R.string.please_note),
                                        message = res.getString(R.string.account_alert_push_popup_message),
                                        showAlertTermsAndConditions = true
                                    )
                                ),
                                manageAlertSubscription = manageAlertSubscription,
                                tracker = tracker
                            )
                            SettingsViewType.SETTINGS_ACTION_THEME_SELECTION -> SettingActionDynamicRefreshViewModel(
                                data.title,
                                data.value,
                                R.layout.adapter_setting_action_with_value,
                                dto.ctaList?.first()?.clickActionDto?.appUrl,
                                dto.ctaList?.first()?.clickActionDto?.toWebViewDeepLink()
                                    ?.let { Uri.parse(it) },
                                dto.ctaList?.first()?.clickActionDto?.toWebDeepLink()
                                    ?.let { Uri.parse(it) },
                                viewType,
                                tracker,
                                nightModePreferences
                            )
                            else -> throw DynamicUiViewFactoryException("SettingsViewFactory registered to unknown type")
                        }
                    }
                }

                override val viewModelClass: Class<out DynamicViewModel> =
                    SettingDynamicViewModel::class.java

                override val uniqueIdentifier: String = "${javaClass.name}_${dto.type}"
            }
        } else {
            throw DynamicUiViewFactoryException("SettingsViewFactory registered to unknown type")
        }
    }

    private fun getAppVersion() =
        context.packageManager.getPackageInfo(context.packageName, 0).versionName

    private fun getAppVersionAndStreamersInfo(versionName: String): String {
        val appConfig = appConfigRepo.getAppConfig()
        return " $versionName (${BuildConfig.VERSION_CODE})" +
            " ${appConfig.environment.asString} -" +
            " (L1:${appConfig.streamings[LIGHT_STREAMER_LEVEL1_IDENTIFIER]?.instanceServer}," +
            " L2:${appConfig.streamings[LIGHT_STREAMER_LEVEL2_IDENTIFIER]?.instanceServer})"
    }
}
