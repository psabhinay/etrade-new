package com.etrade.mobilepro.inboxmessages.dynamicui.data.fundingprompt

import com.etrade.mobile.accounts.dto.AccountDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.dynamicui.api.InboxMessagesWidgetPlace
import com.etrade.mobilepro.inboxmessages.dynamicui.data.findAccountDto
import com.etrade.mobilepro.inboxmessages.dynamicui.data.findCorrespondingInboxMessage
import com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt.FundingPromptViewDto

@Suppress("LongParameterList") // no reasonable way to divide the method
internal fun createFundingPrompt(
    fundingPromptDtoFactory: FundingPromptDtoFactory,
    view: BaseScreenView,
    accounts: List<AccountDto>,
    messages: ETResult<List<InboxMessage>>,
    place: InboxMessagesWidgetPlace,
    isSystemInDarkMode: Boolean
): FundingPromptViewDto? {
    val account = findAccountDto(view, accounts) ?: return null
    val promptCode = account.promptsForFunding ?: return null
    val message = findCorrespondingInboxMessage(messages, promptCode, isSystemInDarkMode) ?: return null
    return fundingPromptDtoFactory.create(place, account, message, view)
}
