package com.etrade.mobilepro.util.android.extension

import android.os.Bundle
import android.widget.ScrollView
import android.widget.TextView

private const val LINE_START_OFFSET = "lineStartOffset"

/**
 * Util method to restore scroll position of a TextView child with a lineStartOffset.
 */
fun ScrollView.restoreTextViewScrollPosition(textView: TextView, savedInstanceState: Bundle?) {
    savedInstanceState?.getInt(LINE_START_OFFSET)?.let {
        post {
            val firstVisibleLineOffset = textView.layout.getLineForOffset(it)
            val pixelOffset = textView.layout.getLineTop(firstVisibleLineOffset)
            this.scrollTo(0, pixelOffset)
        }
    }
}

/**
 * Util method to get line start offset of a TextView inside the parent scroll view.
 */
fun ScrollView.saveLineStartOffset(textView: TextView, savedInstanceState: Bundle?) {
    savedInstanceState?.apply {
        putInt(LINE_START_OFFSET, textView.layout.getLineStart(textView.layout.getLineForVertical(scrollY)))
    }
}
