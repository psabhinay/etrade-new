package com.etrade.mobilepro.portfolio

import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.util.SortOrder
import com.evrencoskun.tableview.sort.SortState

internal val optionFields: Set<Level1Field> = setOf(
    Level1Field.ASK,
    Level1Field.BID,
    Level1Field.CHANGE,
    Level1Field.CHANGE_PERCENT,
    Level1Field.CLOSING_MARK,
    Level1Field.DELTA,
    Level1Field.EPS,
    Level1Field.GAMMA,
    Level1Field.IMPLIED_VOLATILITY,
    Level1Field.LAST_PRICE,
    Level1Field.MARK,
    Level1Field.PREVIOUS_CLOSE_PRICE,
    Level1Field.PRICE_TO_EARNINGS,
    Level1Field.THETA,
    Level1Field.TICK_INDICATOR,
    Level1Field.VEGA,
    Level1Field.VOLUME,
    Level1Field.YEAR_HIGH,
    Level1Field.YEAR_LOW
)
internal val stockFields: Set<Level1Field> = setOf(
    Level1Field.ASK,
    Level1Field.BID,
    Level1Field.CHANGE,
    Level1Field.CHANGE_PERCENT,
    Level1Field.EPS,
    Level1Field.LAST_PRICE,
    Level1Field.MARK,
    Level1Field.MARKET_CAP,
    Level1Field.PREVIOUS_CLOSE_PRICE,
    Level1Field.PRICE_TO_EARNINGS,
    Level1Field.TICK_INDICATOR,
    Level1Field.VOLUME,
    Level1Field.YEAR_HIGH,
    Level1Field.YEAR_LOW,
    Level1Field.TRADE_STATUS,
    Level1Field.TRADE_HALT_REASON
)

internal fun SortOrder.toSortState(): SortState {
    return when (this) {
        SortOrder.ASCENDING -> SortState.ASCENDING
        SortOrder.DESCENDING -> SortState.DESCENDING
        SortOrder.UNDEFINED -> SortState.UNSORTED
    }
}
