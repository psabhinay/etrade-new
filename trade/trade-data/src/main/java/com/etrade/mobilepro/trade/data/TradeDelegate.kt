package com.etrade.mobilepro.trade.data

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.trade.api.OrderPlaceResponse
import com.etrade.mobilepro.trade.api.OrderPreviewResponse
import com.etrade.mobilepro.trade.api.TradeOrder

internal interface TradeDelegate {

    suspend fun previewOrder(map: InputValuesSnapshot, marketDestination: MarketDestination): ETResult<OrderPreviewResponse>

    suspend fun placeOrder(arguments: TradeOrderArguments, order: TradeOrder): ETResult<OrderPlaceResponse>
}
