package com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt

import android.content.res.Resources
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.Diffable
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.inboxmessages.dynamicui.BR
import com.etrade.mobilepro.inboxmessages.dynamicui.R
import com.etrade.mobilepro.inboxmessages.dynamicui.api.fundingprompt.FundingPrompt

class FundingPromptViewModel(
    private val resources: Resources,
    private val fundingPromptDto: FundingPromptViewDto,
    val navHelper: FundingPromptNavHelper,
    val accountSourceLabel: String?
) : DynamicViewModel(R.layout.inbox_message_funding_prompt), Diffable {
    override val variableId: Int = BR.obj
    var fundingPrompt = fundingPromptDto.data as? FundingPrompt
    val heading
        get() = fundingPrompt?.account?.accountShortName
    val image
        get() = fundingPrompt?.image
    val accountUuid
        get() = fundingPrompt?.account?.accountUuid

    fun widgetClicked() {
        fundingPromptDto.ctaList?.firstOrNull()?.clickActionDto?.appUrl?.let {
            clickEvents.value = CtaEvent.UriEvent(it)
        }
    }

    fun accountClicked() {
        val ctaEvent = fundingPromptDto.clickActionDto?.appUrl?.let {
            navHelper.getNavigationDirections(it, fundingPrompt?.account)
        }
            ?: fundingPromptDto.clickActionDto?.webViewUrl?.let { CtaEvent.UriEvent(it) }
            ?: fundingPromptDto.clickActionDto?.webUrl?.let { CtaEvent.UriEvent(it) }

        ctaEvent?.let { clickEvents.postValue(it) }
    }

    fun refresh(fundingPromptDto: FundingPromptViewDto?) {
        fundingPromptDto?.let {
            fundingPrompt = it.data as? FundingPrompt
        }
    }

    override fun isSameItem(other: Any): Boolean =
        other.javaClass == javaClass && accountUuid == (other as FundingPromptViewModel).accountUuid

    override fun hasSameContents(other: Any): Boolean = false

    fun onInfoClicked() {
        clickEvents.postValue(
            CtaEvent.InfoDialogEvent(
                resources.getString(R.string.complete_view_account_summary_info_title),
                resources.getString(R.string.complete_view_account_summary_info_content)
            )
        )
    }
}
