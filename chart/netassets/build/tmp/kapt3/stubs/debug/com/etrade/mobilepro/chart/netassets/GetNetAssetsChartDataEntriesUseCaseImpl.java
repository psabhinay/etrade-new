package com.etrade.mobilepro.chart.netassets;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0019\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b2\u0006\u0010\u000b\u001a\u00020\fH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ\u001e\u0010\u000e\u001a\u00020\n*\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00052\b\b\u0001\u0010\u0011\u001a\u00020\u0005H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0012"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/GetNetAssetsChartDataEntriesUseCaseImpl;", "Lcom/etrade/mobilepro/chart/netassets/GetNetAssetsChartDataEntriesUseCase;", "netAssetsChartRepo", "Lcom/etrade/mobilepro/chart/netassets/data/NetAssetsChartRepo;", "barColor", "", "(Lcom/etrade/mobilepro/chart/netassets/data/NetAssetsChartRepo;I)V", "execute", "Lcom/etrade/mobilepro/util/Resource;", "", "Lcom/etrade/mobilepro/barchartview/DataEntry;", "parameter", "Lcom/etrade/mobilepro/chart/netassets/GetNetAssetsChartDataEntriesParameter;", "(Lcom/etrade/mobilepro/chart/netassets/GetNetAssetsChartDataEntriesParameter;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "toDataEntry", "Lcom/etrade/mobilepro/chart/api/ChartPoint;", "index", "color", "netassets_debug"})
public final class GetNetAssetsChartDataEntriesUseCaseImpl implements com.etrade.mobilepro.chart.netassets.GetNetAssetsChartDataEntriesUseCase {
    private final com.etrade.mobilepro.chart.netassets.data.NetAssetsChartRepo netAssetsChartRepo = null;
    private final int barColor = 0;
    
    @javax.inject.Inject()
    public GetNetAssetsChartDataEntriesUseCaseImpl(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.chart.netassets.data.NetAssetsChartRepo netAssetsChartRepo, @androidx.annotation.ColorRes()
    @com.etrade.mobilepro.common.di.PrimaryColor()
    int barColor) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object execute(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.chart.netassets.GetNetAssetsChartDataEntriesParameter parameter, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.etrade.mobilepro.util.Resource<? extends java.util.List<? extends com.etrade.mobilepro.barchartview.DataEntry>>> continuation) {
        return null;
    }
    
    private final com.etrade.mobilepro.barchartview.DataEntry toDataEntry(com.etrade.mobilepro.chart.api.ChartPoint $this$toDataEntry, int index, @androidx.annotation.ColorRes()
    int color) {
        return null;
    }
}