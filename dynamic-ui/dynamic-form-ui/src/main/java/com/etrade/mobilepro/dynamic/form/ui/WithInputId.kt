package com.etrade.mobilepro.dynamic.form.ui

interface WithInputId {
    val inputId: String?
}
