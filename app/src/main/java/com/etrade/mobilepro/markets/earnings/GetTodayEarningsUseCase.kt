package com.etrade.mobilepro.markets.earnings

import android.content.res.Resources
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.marketscalendar.api.model.EventsEarnings
import com.etrade.mobilepro.marketscalendar.api.model.EventsRequest
import com.etrade.mobilepro.marketscalendar.api.repo.MarketsCalendarRepo
import com.etrade.mobilepro.marketscalendar.data.dto.EARNINGS_EVENT
import com.etrade.mobilepro.marketscalendar.util.getCalendarEventTitle
import com.etrade.mobilepro.marketscalendar.viewmodel.EVENTS_FULL_DATE_TIME_FORMATTER
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails
import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.util.market.time.NEW_YORK_ZONE_ID
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter
import java.util.Locale
import javax.inject.Inject

private const val TODAY_EARNINGS_COUNT = 3

interface GetTodayEarningsUseCase : UseCase<EventsRequest, ETResult<List<EventDetails>?>>

class GetTodayEarningsUseCaseImpl @Inject constructor(
    private val marketsCalendarRepo: MarketsCalendarRepo,
    private val resources: Resources
) : GetTodayEarningsUseCase {

    private val eventsDateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy '|' hh:mm a z", Locale.US)

    override suspend fun execute(parameter: EventsRequest): ETResult<List<EventDetails>?> = withContext(Dispatchers.IO) {
        runCatchingET {
            marketsCalendarRepo
                .getEventsEarnings(parameter)
                .eventsResponseData
                ?.eventsResponseDm
                ?.eventsDataList
                ?.map(::mapEventItem)
                ?.sorted()
                ?.take(TODAY_EARNINGS_COUNT) // For now, these are simply the first three in the earnings list for the day
        }
    }

    private fun getEventDate(item: EventsEarnings, formatter: DateTimeFormatter): String? {
        return item.dateTime?.let {
            val time: LocalDateTime = LocalDateTime.parse(it, EVENTS_FULL_DATE_TIME_FORMATTER)

            time.atZone(NEW_YORK_ZONE_ID).format(formatter)
        }
    }

    private fun mapEventItem(item: EventsEarnings): EventDetails {
        return object : EventDetails(
            title = item.symbol?.let(resources::getCalendarEventTitle),
            date = getEventDate(item, eventsDateTimeFormatter),
            eventType = EARNINGS_EVENT,
            symbol = item.symbol
        ) {
            override fun getDateTime(date: String?): LocalDateTime? {
                return date?.let { LocalDateTime.parse(it, eventsDateTimeFormatter) }
            }
        }
    }
}
