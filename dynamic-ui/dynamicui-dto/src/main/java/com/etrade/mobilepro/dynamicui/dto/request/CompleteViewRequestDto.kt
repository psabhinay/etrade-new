package com.etrade.mobilepro.dynamicui.dto.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class CompleteViewRequestDto(
    @Json(name = "completeView")
    val completeView: String = "",
    @Json(name = "displayPositions")
    val displayPositions: Boolean = true,
    @Json(name = "displayAcctSrcLabel")
    val displayAcctSrcLabel: Boolean = true
)
