package com.etrade.mobilepro.movers

import android.content.Context
import android.view.View
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.instrument.InstrumentType

interface MoversRouter {
    fun navigateToQuoteDetails(view: View, symbol: String, instrumentType: InstrumentType)
    fun navigateToQuoteDetails(context: Context, symbol: String, instrumentType: InstrumentType)
    fun getTabDest(ctaString: String): CtaEvent?
}
