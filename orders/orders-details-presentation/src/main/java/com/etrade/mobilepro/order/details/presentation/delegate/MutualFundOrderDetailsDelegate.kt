package com.etrade.mobilepro.order.details.presentation.delegate

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dynamic.form.api.Action
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderCancelRequest
import com.etrade.mobilepro.order.details.api.OrderData
import com.etrade.mobilepro.order.details.api.OrderDetailDescription
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.api.OrderDetailsRepo
import com.etrade.mobilepro.order.details.api.OrderDetailsRequestParams
import com.etrade.mobilepro.order.details.api.OrderLegItemInterface
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel.ActionsArgument
import com.etrade.mobilepro.order.details.presentation.R
import com.etrade.mobilepro.order.details.presentation.helper.cancelOrderAction
import com.etrade.mobilepro.order.details.presentation.helper.newOrderAction
import com.etrade.mobilepro.order.details.router.GeneralOrderDetailsArgument
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.orders.extensions.android.asSingleMessage
import com.etrade.mobilepro.orders.extensions.android.cancelOrderDialog
import com.etrade.mobilepro.orders.extensions.android.cancelSuccessDialog
import com.etrade.mobilepro.orders.presentation.OrderButtonVisibilityManager
import com.etrade.mobilepro.orders.presentation.mutualFundOrderButtonVisibilityManager
import com.etrade.mobilepro.orders.presentation.section.viewfactory.MutualOrderSectionViewFactory
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.rx2.await
import kotlinx.coroutines.rx2.awaitLast
import javax.inject.Inject
import javax.inject.Provider

class MutualFundOrderDetailsDelegate @Inject constructor(
    private val orderDetailsRepo: OrderDetailsRepo,
    private val orderDetailSectionViewFactory: MutualOrderSectionViewFactory,
    private val resource: Resources,
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    private val user: User
) : OrderDetailsViewModel.BaseOrderDetailsDelegate() {

    override val noActionsMessage: CharSequence?
        get() = resource.getString(R.string.orders_message_not_supported_order)

    override val loadingIndicators: Set<LiveData<Boolean>>
        get() = setOf(cancelLoadingIndicator)

    private val cancelLoadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)

    private var firstOrderDetailsItem: BaseOrderDetailsItem.OrderDetailsItem? = null

    override suspend fun loadItems(
        argument: OrderDetailsArgument,
        quotes: Deferred<Map<String, OrderDetailItemInterface>>
    ): ETResult<OrderDetailsViewModel.OrderDetails> {

        require(argument is GeneralOrderDetailsArgument)

        val accountId = argument.accountId
        val orderNumber = argument.orderNumber
        val observable = if (argument.orderStatus == OrderStatus.SAVED) {
            orderDetailsRepo.getSavedOrderDetails(OrderDetailsRequestParams(accountId, orderNumber, OrderStatus.SAVED))
        } else {
            orderDetailsRepo.getOrderDetails(OrderDetailsRequestParams(accountId, orderNumber, OrderStatus.ALL))
        }

        return observable
            .runCatchingET { awaitLast() }
            .mapCatching { orderItemDetailList -> createMutualFundOrderDetailsView(quotes.await(), orderItemDetailList) }
    }

    private fun createMutualFundOrderDetailsView(
        quote: Map<String, OrderDetailItemInterface>,
        orderItemDetailList: List<BaseOrderDetailsItem>
    ): OrderDetailsViewModel.OrderDetails {
        val first = (orderItemDetailList[0] as? BaseOrderDetailsItem.OrderDetailsItem)
        firstOrderDetailsItem = first
        return OrderDetailsViewModel.OrderDetails(
            first?.toOrderDetailsItems(quote).orEmpty(),
            first?.orderStatus ?: OrderStatus.OPEN,
            first
        )
    }

    private fun BaseOrderDetailsItem.OrderDetailsItem.toOrderDetailsItems(
        quotes: Map<String, OrderDetailItemInterface>
    ): List<OrderDetailItemInterface> {

        val results = mutableListOf<OrderDetailItemInterface>()

        val orderInfoItems = orderDetailSectionViewFactory.createOrderSectionView(this)

        orderDescriptions.mapIndexed { index, orderLeg ->
            val orderDetailsSection = createOrderDetailsSections(orderInfoItems, orderLeg)

            val isNotLastLeg = isNotLastLeg(tradeLegs.size, index)
            results.addAll(mergeOrderItemsWithLegs(quotes, orderLeg, orderDetailsSection, isNotLastLeg))
        }

        return results
    }

    private fun mergeOrderItemsWithLegs(
        quotes: Map<String, OrderDetailItemInterface>,
        orderLeg: OrderDetailDescription,
        orderDetailsSection: OrderDetailItemInterface,
        isNotLastLeg: Boolean
    ): List<OrderDetailItemInterface> {
        return listOfNotNull(
            quotes[orderLeg.displaySymbol],
            orderLeg.toOrderLegDisplayItem(),
            orderDetailsSection,
            if (isNotLastLeg) {
                OrderDetailDisplayItem.OrderSectionDividerItem
            } else null
        )
    }

    private fun isNotLastLeg(legsCount: Int, index: Int) = legsCount - index > 1

    private fun createOrderDetailsSections(
        orderInfoItems: List<TableSectionItemLayout>,
        leg: OrderDetailDescription
    ): OrderDetailItemInterface {
        val legInfoItems = orderDetailSectionViewFactory.createOrderLegSectionView(leg)
        val fees = orderDetailSectionViewFactory.mutualFundFeesSection(firstOrderDetailsItem)
        return OrderDetailDisplayItem.OrderSectionItem(orderInfoItems + legInfoItems + fees)
    }

    override fun createActions(argument: ActionsArgument, manager: OrderButtonVisibilityManager): List<Action> {
        val orderDetailsArgument = argument.orderDetailsArgument
        require(orderDetailsArgument is GeneralOrderDetailsArgument)

        return with(manager) {
            listOfNotNull(
                newOrderAction(resource) {
                    openTradeSignal(formOrderData(orderDetailsArgument))
                },
                cancelOrderAction(resource) {
                    confirmCancel(orderDetailsArgument)
                }
            )
        }
    }

    override fun createButtonVisibilityManager(argument: OrderDetailsArgument): OrderButtonVisibilityManager {
        require(argument is GeneralOrderDetailsArgument)
        return mutualFundOrderButtonVisibilityManager(argument.orderStatus)
    }

    private fun createOrderCancelRequest(argument: GeneralOrderDetailsArgument): OrderCancelRequest? {
        return firstOrderDetailsItem?.let {
            argument.run {
                OrderCancelRequest(
                    accountId,
                    user.getUserId().toString(),
                    "android",
                    it.orderDescriptions[0].typeCode,
                    it.term.typeCode,
                    it.origSysCode,
                    it
                )
            }
        }
    }

    private fun confirmCancel(argument: GeneralOrderDetailsArgument) {
        mutableDialog.value = resource.cancelOrderDialog(
            positiveAction = {
                cancelOrder(argument)
                resetActionStates()
            },
            negativeAction = ::resetActionStates
        )
    }

    private fun cancelOrder(argument: GeneralOrderDetailsArgument) {
        val request = createOrderCancelRequest(argument) ?: return

        executeOperation(
            loadingIndicator = cancelLoadingIndicator,
            operation = {
                executeCancel(request)
            },
            onSuccess = {
                onSuccessCancelOrder(it)
                refresh()
            }
        )
    }

    private fun onSuccessCancelOrder(serverMessages: List<ServerMessage>) {
        val error = serverMessages.filter { it.type == ServerMessage.Type.ERROR }
        val dialogMessage = if (error.isNullOrEmpty()) {
            resource.getString(R.string.orders_dialog_cancel_order_success_message)
        } else {
            serverMessages.asSingleMessage()
        }
        mutableDialog.value = resource.cancelSuccessDialog(dialogMessage, ::resetActionStates)
    }

    private suspend fun executeCancel(
        orderCancelRequest: OrderCancelRequest
    ): ETResult<List<ServerMessage>> {
        return orderDetailsRepo.cancelOrder(orderCancelRequest).runCatchingET { await() }
    }

    private fun createFormParameters(
        argument: GeneralOrderDetailsArgument
    ): InputValuesSnapshot {
        return tradeFormParametersBuilderProvider.get().apply {
            accountId = argument.accountId
        }.create(SecurityType.MUTUAL_FUND)
    }

    private fun formOrderData(argument: GeneralOrderDetailsArgument): OrderData {
        val params = createFormParameters(argument)
        return OrderData(params, emptyArray(), TradeOrderType.NEW)
    }
}

private fun OrderDetailDescription.toOrderLegDisplayItem(): OrderLegItemInterface {
    return OrderDetailDisplayItem.OrderLegItem(
        displaySymbol = displaySymbol,
        displayQuantity = displayQuantity,
        transactionType = transactionType
    )
}
