package com.etrade.mobilepro.util.rx

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import io.reactivex.ObservableSource
import kotlinx.coroutines.rx2.awaitLast

/**
 * Extension method for safely running awaitLast() and wrapping the item inside Result
 */
suspend fun <T> ObservableSource<T>.awaitResult(): ETResult<T> {
    return runCatchingET { awaitLast() }
}
