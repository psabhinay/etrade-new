package com.etrade.mobilepro.timeout.widget

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.setupBlurView
import com.etrade.mobilepro.timeout.widget.databinding.FragmentSessionTerminatedBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

class SessionTerminatedFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory
) : Fragment(R.layout.fragment_session_terminated), SessionDialogFragment {

    private val binding by viewBinding(FragmentSessionTerminatedBinding::bind)

    private val sharedSessionTerminatedViewModel: SharedSessionTerminatedViewModel by activityViewModels { viewModelFactory }

    override var onButtonClickListener: (() -> Unit)? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBlurView(binding.blurView, overlayColor = R.color.black_sixty_opacity)

        binding.gotIt.setOnClickListener {
            sharedSessionTerminatedViewModel.hideSessionTerminatedPage()
            onButtonClickListener?.invoke()
        }
    }
}
