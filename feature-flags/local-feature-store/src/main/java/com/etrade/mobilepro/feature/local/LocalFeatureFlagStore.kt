package com.etrade.mobilepro.feature.local

import android.content.res.Resources
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import com.etrade.mobilepro.feature.Feature
import com.etrade.mobilepro.feature.FeatureFlagStore
import com.etrade.mobilepro.feature.local.qualifier.FeatureFlagDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

const val FLAG_STORE_NAME = "feature_flags"

@Singleton
class LocalFeatureFlagStore @Inject constructor(
    @FeatureFlagDataStore
    private val dataStore: DataStore<Preferences>,
    private val resources: Resources
) : FeatureFlagStore {

    private val Feature.prefKey
        get() = booleanPreferencesKey(key)

    override val title: String
        get() = resources.getString(R.string.local_feature_flag_store_title)

    override suspend fun clearFeatures() {
        dataStore.edit { prefs ->
            prefs.clear()
        }
    }

    override suspend fun removeFeature(feature: Feature) {
        dataStore.edit { prefs ->
            prefs.remove(feature.prefKey)
        }
    }

    override suspend fun setFeatureEnabled(feature: Feature, enabled: Boolean) {
        dataStore.edit { prefs ->
            prefs[feature.prefKey] = enabled
        }
    }

    override fun isFeatureEnabled(feature: Feature): Flow<Boolean> {
        return dataStore.data.map { prefs ->
            prefs[feature.prefKey] ?: feature.defaultValue
        }.distinctUntilChanged()
    }

    override fun hasFeature(feature: Feature): Flow<Boolean> {
        return dataStore.data.map { prefs ->
            prefs.contains(feature.prefKey)
        }.distinctUntilChanged()
    }
}
