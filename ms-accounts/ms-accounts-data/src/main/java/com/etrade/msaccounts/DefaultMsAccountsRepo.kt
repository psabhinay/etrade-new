package com.etrade.msaccounts

import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.common.di.RemoteDynamicScreen
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.msaccounts.api.MsAccountsRepo
import com.etrade.mobilepro.msaccounts.api.MsPreferencesRepo
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.rx2.asFlow
import org.slf4j.LoggerFactory
import javax.inject.Inject

class DefaultMsAccountsRepo @Inject constructor(
    @RemoteDynamicScreen
    private val dynamicScreenRepo: DynamicScreenRepo,
    private val msPreferencesRepo: MsPreferencesRepo,
    private val createErrorLayout: () -> GenericLayout,
    private val isUserEnrolled: (List<GenericLayout>) -> Boolean,
) : MsAccountsRepo {

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    @ExperimentalCoroutinesApi
    override fun getMsCompleteView(): Flow<Resource<List<GenericLayout>>> =
        msPreferencesRepo.msUserPreferences
            .map { it.isMsEligible }
            .distinctUntilChanged()
            .flatMapLatest { eligible ->
                if (eligible) {
                    getScreen()
                } else {
                    flowOf(Resource.Success(emptyList()))
                }
            }
            .flowOn(Dispatchers.IO)

    private fun getScreen(): Flow<Resource<List<GenericLayout>>> {
        return dynamicScreenRepo.getScreen(ServicePath.MsCompleteView)
            .map(this::addErrorLayout)
            .checkEnrollment()
            .asFlow()
    }

    private fun Observable<Resource<List<GenericLayout>>>.checkEnrollment() = doOnNext { resource ->
        val isEnrolled = when (resource) {
            is Resource.Success -> resource.data?.let { layouts -> isUserEnrolled(layouts) }
            is Resource.Failed -> false
            else -> null
        }
        isEnrolled?.let {
            msPreferencesRepo.updateMSUserPreferences(
                msPreferencesRepo.msUserPreferences.value.copy(
                    isUserEnrolled = it
                )
            )
        }
    }

    private fun addErrorLayout(resource: Resource<List<GenericLayout>>): Resource<List<GenericLayout>> {
        return if (resource is Resource.Failed) {
            resource.map { it.orEmpty() + createErrorLayout() }
        } else {
            resource
        }
    }
}
