package com.etrade.mobilepro.screener.presentation.mutualfund.performance

import com.etrade.mobilepro.screener.api.filter.mutualfund.PerformanceFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class Category : ScreenerFilterEvent<PerformanceFilter, PerformanceViewModel.ViewState, Boolean> {
    BOTTOM_20 {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(bottom20 = eventValue))
    },
    BELOW_MID_20 {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(belowMid20 = eventValue))
    },
    MID_20 {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(mid20 = eventValue))
    },
    ABOVE_MID_20 {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(aboveMid20 = eventValue))
    },
    TOP_20 {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(top20 = eventValue))
    };

    override fun conditionalUpdate(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): ScreenerFilterEvent.Result<PerformanceViewModel.ViewState> {
        var newState = update(viewState, eventValue)
        if (!newState.filter.isPerformanceValuesSelected()) {
            newState = clearPeriodButtons(newState)
        }
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = (newState.filter.isPerformanceValuesSelected() && newState.filter.isPeriodSelected()) ||
                (newState.filter.isEmpty() && viewState.filter.isNotEmpty())
        )
    }

    private fun clearPeriodButtons(viewState: PerformanceViewModel.ViewState): PerformanceViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                overLast1Month = false,
                overLast3Months = false,
                overLast6Months = false,
                overLastYTD = false,
                overLast1Year = false,
                overLast3Years = false,
                overLast5Years = false,
                overLast10Years = false
            )
        )
}

internal enum class Period : ScreenerFilterEvent<PerformanceFilter, PerformanceViewModel.ViewState, Boolean> {
    ONE_MONTH {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(overLast1Month = eventValue && viewState.filter.isPerformanceValuesSelected()))
    },
    THREE_MONTHS {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(overLast3Months = eventValue && viewState.filter.isPerformanceValuesSelected()))
    },
    SIX_MONTHS {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(overLast6Months = eventValue && viewState.filter.isPerformanceValuesSelected()))
    },
    YTD {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(overLastYTD = eventValue && viewState.filter.isPerformanceValuesSelected()))
    },
    ONE_YEAR {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(overLast1Year = eventValue && viewState.filter.isPerformanceValuesSelected()))
    },
    THREE_YEARS {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(overLast3Years = eventValue && viewState.filter.isPerformanceValuesSelected()))
    },
    FIVE_YEARS {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(overLast5Years = eventValue && viewState.filter.isPerformanceValuesSelected()))
    },
    TEN_YEARS {
        override fun update(viewState: PerformanceViewModel.ViewState, eventValue: Boolean): PerformanceViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(overLast10Years = eventValue && viewState.filter.isPerformanceValuesSelected()))
    }
}
