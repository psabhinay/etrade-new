package com.etrade.mobilepro.home.overview.edit

import com.etrade.mobilepro.editlist.EditListItem

class OverviewWidget(
    val widgetName: String,
    override val displayText: CharSequence,
    override val isChecked: Boolean = true
) : EditListItem {
    override val dragHandleContentDescription = "$contentDescription. Double Tap and hold to change overview detail order"
}
