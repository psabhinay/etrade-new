package com.etrade.mobilepro.transfermoney.presentation.preview

import android.os.Parcelable
import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.etrade.mobilepro.transfermoney.api.TransferRepeat
import com.etrade.mobilepro.transfermoney.presentation.common.TransferMoneyAccountParceler
import com.etrade.mobilepro.transfermoney.usecase.preview.GetQuickTransferPreviewRequest
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import org.threeten.bp.LocalDate

@Parcelize
data class ParcelableGetQuickTransferPreviewRequest(
    override val userId: String,
    override val platform: String,
    override val scheduleDate: LocalDate,
    override val amount: String,
    override val fromAccount: @WriteWith<TransferMoneyAccountParceler> TransferMoneyAccount,
    override val toAccount: @WriteWith<TransferMoneyAccountParceler> TransferMoneyAccount,
    override val frequency: TransferRepeat
) : GetQuickTransferPreviewRequest(userId, platform, scheduleDate, fromAccount, toAccount, amount, frequency), Parcelable
