package com.etrade.mobilepro.quote.screen.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.news.presentation.databinding.ItemNewsFooterBinding
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.util.android.accessibility.bindButtonAccessibility

class NewsFooterViewHolder(private val binding: ItemNewsFooterBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bindTo(
        item: QuoteItem.QuoteNewsFooterItem?,
        viewAllNewsClickListener: (() -> Unit)?,
        disclosureClickListener: (() -> Unit)?
    ) {
        item ?: return
        with(binding.viewAllNews) {
            bindButtonAccessibility()
            setOnClickListener {
                viewAllNewsClickListener?.invoke()
            }
        }
        with(binding.disclosuresTitle) {
            bindButtonAccessibility()
            setOnClickListener {
                disclosureClickListener?.invoke()
            }
        }
    }
}
