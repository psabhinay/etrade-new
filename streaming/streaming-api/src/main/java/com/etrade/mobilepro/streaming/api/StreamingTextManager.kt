package com.etrade.mobilepro.streaming.api

import io.reactivex.Observable

interface StreamingTextManager {
    enum class MovementType {
        LOSS, NEUTRAL, GAIN
    }

    fun getStreamedText(
        streamId: String,
        initialValue: String = "--",
        movementType: MovementType = MovementType.NEUTRAL,
        isAccountStreamingAllowed: Boolean
    ): Observable<StreamingText>
}
