package com.etrade.mobilepro.marketscalendar.data.mapper

import com.etrade.mobilepro.marketscalendar.api.model.EventsEarnings
import com.etrade.mobilepro.marketscalendar.api.model.EventsEconomic
import com.etrade.mobilepro.marketscalendar.api.model.EventsRequest
import com.etrade.mobilepro.marketscalendar.api.model.EventsSummaryResponse
import com.etrade.mobilepro.marketscalendar.data.dto.ALL_SYMBOLS_EVENTS
import com.etrade.mobilepro.marketscalendar.data.dto.EventsEarningsData
import com.etrade.mobilepro.marketscalendar.data.dto.EventsEconomicData
import com.etrade.mobilepro.marketscalendar.data.dto.EventsRequestBody
import com.etrade.mobilepro.marketscalendar.data.dto.EventsResponseDto
import com.etrade.mobilepro.marketscalendar.data.dto.EventsSummaryResponseDto
import com.etrade.mobilepro.testutil.getObjectFromJson
import org.junit.Assert.assertEquals
import org.junit.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalTime

class EventsMapperTest {

    @Test
    fun `map to events request body`() {
        val startDate = LocalDate.now()
        val endDate = LocalDate.now()
        val request = EventsRequest(
            eventsRequestValue = EventsRequest.EventsRequestValue(
                eventsRequestDateRange = EventsRequest.EventsRequestDateRange(
                    startDate,
                    endDate
                ),
                symbolSet = listOf(ALL_SYMBOLS_EVENTS),
                eventFilterEnable = "test_filter",
                eventTypeSet = listOf("test_type")
            )
        )

        val requestBody = request.mapToEventsRequestBody()
        val formattedStartDate = getDateForEventsRequestBody(request.eventsRequestValue.eventsRequestDateRange.startDate, LocalTime.MIN.hour, LocalTime.MIN.minute, LocalTime.MIN.second)
        val formattedEndDate = getDateForEventsRequestBody(request.eventsRequestValue.eventsRequestDateRange.endDate, LocalTime.MAX.hour, LocalTime.MAX.minute, LocalTime.MAX.second)
        assertEquals(formattedStartDate, requestBody.value.dateRangeSet[0].startDate)
        assertEquals(formattedEndDate, requestBody.value.dateRangeSet[0].endDate)
        assertEquals(request.eventsRequestValue.symbolSet[0], requestBody.value.symbolSet[0])
        assertEquals(request.eventsRequestValue.eventFilterEnable, requestBody.value.eventFilterEnable)
        assertEquals(request.eventsRequestValue.eventTypeSet[0], requestBody.value.eventTypeSet[0])
    }

    @Test
    fun `map to events summary response`() {
        val expectedEventsSummaryResponse: EventsSummaryResponseDto = EventsMapperTest::class.getObjectFromJson(
            "summary_events.json"
        )
        val response = expectedEventsSummaryResponse.mapToEventsSummaryResponse()
        val expectedEventsSummaryList: List<EventsSummaryResponseDto.EventsSummaryData> =
            expectedEventsSummaryResponse.data?.eventsSummaryResponseDm?.eventsSummaryDataList as List<EventsSummaryResponseDto.EventsSummaryData>
        val eventsSummaryDataList: List<EventsSummaryResponse.EventsSummary> =
            response.eventsSummaryResponseData?.eventsSummaryResponseDm?.eventsSummaryList as List<EventsSummaryResponse.EventsSummary>
        val zippedList = expectedEventsSummaryList.zip(eventsSummaryDataList)
        zippedList.forEach { pair ->
            val expectEventsSummay = pair.first
            val eventsSummary = pair.second
            assertEquals(expectEventsSummay.dateTime, eventsSummary.dateTime)
        }
    }

    @Test
    fun `map to events economic response`() {
        val expectedEconomicResponse: EventsResponseDto<EventsEconomicData> = EventsMapperTest::class.getObjectFromJson(
            "economic_events.json",
            EventsResponseDto::class.java,
            EventsEconomicData::class.java
        )
        val response = expectedEconomicResponse.mapToEventsEconomicResponse()
        val expectedEventsDataList: List<EventsEconomicData> = expectedEconomicResponse.dataDto?.eventsResponseDm?.eventsDataList as List<EventsEconomicData>
        val economicDataList: List<EventsEconomic> = response.eventsResponseData?.eventsResponseDm?.eventsDataList as List<EventsEconomic>
        val zippedList = expectedEventsDataList.zip(economicDataList)
        zippedList.forEach { pair ->
            val expectEventsData = pair.first
            val eventsEarning = pair.second
            assertEquals(expectEventsData.ecEconDefinition, eventsEarning.ecEconDefinition)
            assertEquals(expectEventsData.ecEconDesc, eventsEarning.ecEconDesc)
            assertEquals(expectEventsData.ecEconName, eventsEarning.ecEconName)
            assertEquals(expectEventsData.ecEcondetActual, eventsEarning.ecEcondetActual)
            assertEquals(expectEventsData.ecEcondetConcensus, eventsEarning.ecEcondetConcensus)
            assertEquals(expectEventsData.ecEcondetName, eventsEarning.ecEcondetName)
            assertEquals(expectEventsData.ecSeStartdate, eventsEarning.ecSeStartdate)
        }
    }

    @Test
    fun `map to events earnings response`() {
        val expectedEventsEarningsResponse: EventsResponseDto<EventsEarningsData> = EventsMapperTest::class.getObjectFromJson(
            "earnings_events.json",
            EventsResponseDto::class.java,
            EventsEarningsData::class.java
        )
        val response = expectedEventsEarningsResponse.mapToEventsEarningsResponse()
        val expectedEventsDataList: List<EventsEarningsData> =
            expectedEventsEarningsResponse.dataDto?.eventsResponseDm?.eventsDataList as List<EventsEarningsData>
        val eventsDataList: List<EventsEarnings> = response.eventsResponseData?.eventsResponseDm?.eventsDataList as List<EventsEarnings>
        val zippedList = expectedEventsDataList.zip(eventsDataList)
        zippedList.forEach { pair ->
            val expectEventsData = pair.first
            val eventsEarning = pair.second
            assertEquals(expectEventsData.dateTime, eventsEarning.dateTime)
            assertEquals(expectEventsData.epsactualValue, eventsEarning.epsactualValue)
            assertEquals(expectEventsData.epsannounceDate, eventsEarning.epsannounceDate)
            assertEquals(expectEventsData.epsannounceTime, eventsEarning.epsannounceTime)
            assertEquals(expectEventsData.epsestMean, eventsEarning.epsestMean)
            assertEquals(expectEventsData.symbol, eventsEarning.symbol)
        }
    }

    private fun getDateForEventsRequestBody(
        date: LocalDate,
        hourOfDay: Int,
        minute: Int,
        second: Int
    ): EventsRequestBody.Date = EventsRequestBody.Date(
        year = date.year,
        month = date.monthValue,
        dayOfMonth = date.dayOfMonth,
        hourOfDay = hourOfDay,
        minute = minute,
        second = second
    )
}
