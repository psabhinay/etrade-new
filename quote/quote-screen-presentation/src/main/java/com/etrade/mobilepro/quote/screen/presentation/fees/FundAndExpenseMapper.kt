package com.etrade.mobilepro.quote.screen.presentation.fees

import android.content.res.Resources
import com.etrade.mobilepro.quote.screen.presentation.R
import com.etrade.mobilepro.quoteapi.FundQuoteItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.HeaderSectionItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createMultiLineSummarySection

@Suppress("LongMethod")
fun getSections(feesAndExpensesItem: FundQuoteItem.FeesAndExpensesItem, resources: Resources) =
    mutableListOf<TableSectionItemLayout>().apply {
        add(HeaderSectionItem(resources.getString(R.string.quote_transaction_fees)))
        add(createMultiLineSummarySection(resources.getString(R.string.sales_charge), feesAndExpensesItem.salesCharge))
        add(createMultiLineSummarySection(resources.getString(R.string.fund_redemption_fee), feesAndExpensesItem.fundRedemptionFee))

        add(HeaderSectionItem(resources.getString(R.string.quote_etrade_fees)))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_etrade_transaction_fee), feesAndExpensesItem.etradeTransactionFee))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_etrade_early_redemption_fee), feesAndExpensesItem.etradeEarlyRedemptionFee))

        add(HeaderSectionItem(resources.getString(R.string.quote_investment_minimums)))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_availability), feesAndExpensesItem.availability))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_non_ira_initial), feesAndExpensesItem.initialInvestment))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_additional), feesAndExpensesItem.subsequentInvestment))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_initial_ira), feesAndExpensesItem.initialIra))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_additional), feesAndExpensesItem.subsequentIra))

        add(HeaderSectionItem(resources.getString(R.string.quote_fund_fees)))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_net_expense_ratio), feesAndExpensesItem.netExpenseRatio))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_gross_expense_ratio), feesAndExpensesItem.grossExpenseRatio))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_management), feesAndExpensesItem.managementFee))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_administration), feesAndExpensesItem.administrationFee))
        add(createMultiLineSummarySection(resources.getString(R.string.quote_twelve_b_1), feesAndExpensesItem.twelveB1Fee))
    }

data class FeeAndExpenseData(
    val data: List<TableSectionItemLayout>
)
