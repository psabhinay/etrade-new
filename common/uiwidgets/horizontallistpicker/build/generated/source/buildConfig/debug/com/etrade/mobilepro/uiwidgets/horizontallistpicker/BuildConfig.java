/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.etrade.mobilepro.uiwidgets.horizontallistpicker;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.etrade.mobilepro.uiwidgets.horizontallistpicker";
  public static final String BUILD_TYPE = "debug";
}
