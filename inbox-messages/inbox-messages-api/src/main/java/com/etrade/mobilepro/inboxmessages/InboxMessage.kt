package com.etrade.mobilepro.inboxmessages

import java.util.Date

interface InboxMessage {

    val id: Long

    val title: String

    val subtitle: String

    val message: String

    val type: MessageType

    val place: MessagePlace?

    val deepLink: String?

    val userType: List<UserType>?

    val assetHeight: Int?

    val thumbnailUrl: String

    val hasThumbnail: Boolean

    // It only has title, subtitle is empty. it maps to menu item title view
    val isSimpleInboxMessage: Boolean

    val theme: CampaignAssetsTheme?

    val rawAttributes: Map<String, String>

    val backgroundColor: String?

    val receivedDate: Date

    val arrow: Boolean

    val icon: Boolean

    val headLine: String?

    val bodyText: String?

    val symbol: String?
}

enum class MessageType(val type: String) {

    DASHBOARD_INFORMATION("dashboard_information"),
    DYNAMIC_NAVIGATION("dynamic_navigation"),
    CONTEXTUAL("contextual"),
    TRADE_INFORMATION("trade_information"),
    DYNAMIC_NOTIFICATIONS("dynamic_notifications");

    companion object {
        fun convert(type: String?): MessageType? {
            return values().find { it.type == type }
        }
    }
}

enum class MessagePlace(val place: String) {

    MENU("menu"),
    MENU_REFER_A_FRIEND("menu_refer_a_friend"),
    FUNDING_PROMPT_COMPLETE_VIEW("unfunded_account_balance_all_accounts"),
    FUNDING_PROMPT_OVERVIEW_PORTFOLIO("unfunded_account_balance_positions_and_dashboard"),
    LOW_CASH_COMPLETE_VIEW("low_cash_account_balance_all_accounts"),
    LOW_CASH_OVERVIEW_PORTFOLIO("low_cash_account_balance_positions_and_dashboard"),
    CUSTOMER_SERVICE("customer_service"),
    COMPLETE_VIEW_BANNER("complete_view_banner"),
    NOTIFICATIONS_INLINE("notifications_inline"),
    NOTIFICATIONS_BANNER("notifications_banner"),
    NOTIFICATIONS_INFORMATION("notifications_information");

    companion object {
        fun convert(place: String?): MessagePlace? {
            return values().find { it.place == place }
        }
    }
}
