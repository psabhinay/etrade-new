package com.etrade.mobilepro.optionschains.data.repo

import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.instrument.ExpirationDate
import com.etrade.mobilepro.instrument.SettlementSessionCode
import com.etrade.mobilepro.optionschains.api.OptionsChain
import com.etrade.mobilepro.optionschains.api.OptionsChainException
import com.etrade.mobilepro.optionschains.api.OptionsChainRepo
import com.etrade.mobilepro.optionschains.data.dto.ExpirationDateDto
import com.etrade.mobilepro.optionschains.data.dto.OptionsChainDto
import com.etrade.mobilepro.optionschains.data.dto.OptionsChainPidDto
import com.etrade.mobilepro.optionschains.data.dto.OptionsChainRequestDto
import com.etrade.mobilepro.optionschains.data.dto.OptionsChainRequestValueDto
import com.etrade.mobilepro.optionschains.data.dto.toOptionsChain
import com.etrade.mobilepro.optionschains.data.rest.OptionsChainApiClient
import javax.inject.Inject

/**
 * Repository class for returning api responses for options chains
 */
class DefaultOptionsChainRepoImpl @Inject constructor(
    private val optionsChainApiClient: OptionsChainApiClient
) : OptionsChainRepo {

    override suspend fun getOptionsChain(symbol: String, expirationDate: ExpirationDate?): ETResult<OptionsChain> {
        val input = OptionsChainInput(symbol, 2, expirationDate?.year, expirationDate?.month, expirationDate?.day)
        val isAMOptionsChain = expirationDate?.settlementSessionCode == SettlementSessionCode.AM
        val requestObj: OptionsChainRequestDto = createValueObject(input)
        return getOptionsChains(requestObj, input.createPath())
            .mapCatching { response ->
                val error = response.extractError()
                val optionsChainData = response.data?.optionChainsResponse?.toOptionsChain(isAMOptionsChain)
                when {
                    error != ServerError.NoError -> throw OptionsChainException(optionsChainData, error.extractMessage())
                    optionsChainData == null -> throw IllegalArgumentException("Data is missing from the response.")
                    else -> optionsChainData
                }
            }
    }

    /**
     * Helper function to await result from observable
     * @param input request input
     * @param path path of api
     */
    private suspend fun getOptionsChains(input: OptionsChainRequestDto, path: String): ETResult<ServerResponseDto<OptionsChainDto>> {
        return optionsChainApiClient.runCatchingET {
            getFullOptionsChainsWithQuotes(input, path)
        }
    }

    /**
     * Create value object. Value is the high level json attribute
     */
    private fun createValueObject(input: OptionsChainInput): OptionsChainRequestDto {
        val optionsChainRequest = OptionsChainRequestValueDto(
            allMonths = "0",
            leapsOnly = "0",
            skipAdjusted = "1",
            chainType = "1",
            priceType = "1",
            strikeRange = "1",
            skipLeaps = "0",
            displayBy = "1",
            optionScalabilityCd = "0",
            ed = input.toExpirationDateDto(),
            pid = input.toOptionsChainPidDto()
        )
        return OptionsChainRequestDto(optionsChainRequest, "null")
    }

    private fun OptionsChainInput.toExpirationDateDto(): ExpirationDateDto {
        return ExpirationDateDto(
            year = year,
            month = month,
            day = day,
            expiryTypeCode = expiryTypeCode
        )
    }

    private fun OptionsChainInput.toOptionsChainPidDto(): OptionsChainPidDto = OptionsChainPidDto(symbol, "WILDCARD")
}
