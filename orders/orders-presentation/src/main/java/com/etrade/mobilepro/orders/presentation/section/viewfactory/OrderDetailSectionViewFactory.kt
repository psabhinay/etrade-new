package com.etrade.mobilepro.orders.presentation.section.viewfactory

import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

interface OrderDetailSectionViewFactory {
    fun createSectionView(
        detailsDto: BaseOrderDetailsItem.OrderDetailsItem,
        resources: OrderDetailLabelResources,
        isConditionalOrder: Boolean
    ): List<TableSectionItemLayout>
}

interface AdvancedOrderDetailSectionViewFactory {
    fun createSectionView(advancedOrderItem: BaseOrderDetailsItem.AdvancedOrderItem, resources: OrderDetailLabelResources): List<TableSectionItemLayout>
}

interface StockPlanOrderDetailSectionViewFactory {
    fun createSectionView(detailsDto: BaseOrderDetailsItem.StockPlanOrderDetailsItem, resources: OrderDetailLabelResources): List<TableSectionItemLayout>
}
