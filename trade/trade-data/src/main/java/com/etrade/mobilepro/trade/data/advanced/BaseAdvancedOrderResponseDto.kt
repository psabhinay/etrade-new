package com.etrade.mobilepro.trade.data.advanced

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.instrument.isSymbolLongOption
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.OrderAction
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.OrderLeg
import com.etrade.mobilepro.trade.api.advanced.AdvancedOptionOrder
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrder
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderResponse
import com.etrade.mobilepro.trade.api.advanced.AdvancedStockOrder
import com.etrade.mobilepro.trade.api.advanced.ConfirmedAdvancedOptionOrder
import com.etrade.mobilepro.trade.api.advanced.ConfirmedAdvancedOrder
import com.etrade.mobilepro.trade.api.advanced.ConfirmedAdvancedStockOrder
import com.etrade.mobilepro.trade.data.util.formatOrderPlacedTime
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

private const val HIDDEN_STOP_ORDER_CODE = "3"
private const val TRAILING_STOP_ORDER_CODE = "4"
private const val TRAILING_STOP_DOLLAR_CODE = "1"
private const val TRAILING_STOP_PERCENT_CODE = "2"

internal abstract class BaseAdvancedOrderResponseDto(
    @field:Json(name = "AdvancedOrder")
    var advancedOrders: List<AdvancedOrderDto>? = null,
    @field:Json(name = "invest.trading.errors")
    var errorsRaw: List<AdvancedOrderServerMessageDto>? = null,
    @field:Json(name = "ExecInstruction")
    var execInstructions: List<AdvancedExecInstructionDto>? = null,
    @field:Json(name = "LegInfo")
    var legInfos: List<AdvancedLegInfoDto>? = null,
    @field:Json(name = "OrderDetail")
    var orderDetails: List<AdvancedOrderDetailDto>? = null,
    @field:Json(name = "OrderGroupSvcResponse")
    var orderGroupSvcResponse: AdvancedOrderGroupSvcResponseDto? = null,
    @field:Json(name = "OrderSvcResponse")
    var orderSvcResponse: List<AdvancedOrderSvcResponseDto>? = null,
    @field:Json(name = "ProductId")
    var productIds: List<AdvancedProductIdDto>? = null,
    @field:Json(name = "invest.trading.warnings")
    var warningsRaw: List<AdvancedOrderServerMessageDto>? = null
) : AdvancedOrderResponse {

    final override val warnings: Collection<ServerMessage>
        get() = warningsRaw.toServerMessages(ServerMessage.Type.WARN)

    val errors: Collection<ServerMessage>
        get() = errorsRaw.toServerMessages(ServerMessage.Type.ERROR)

    protected abstract fun getAccount(accountId: String): String

    protected fun <T : Order> createOrders(order: OrderProducer.() -> T): List<T> {
        val execInstructions = execInstructions.orEmpty()
        val legInfos = legInfos.orEmpty()
        val orderSvcResponse = orderSvcResponse.orEmpty()
        val productIds = productIds.orEmpty()

        return orderDetails?.mapIndexed { index, detailDto ->
            RawOrder(
                accountTitle = getAccount(detailDto.accountId),
                detailDto = detailDto,
                instructionDto = execInstructions[index],
                legInfoDto = legInfos[index],
                productIdDto = productIds[index],
                responseDto = orderSvcResponse[index],
                groupSvcResponseDto = orderGroupSvcResponse,
                advancedOrderDto = advancedOrders?.getOrNull(index)
            ).order()
        }.orEmpty()
    }
}

@JsonClass(generateAdapter = true)
class AdvancedAccountDto(
    @Json(name = "accountid")
    val rawAccountId: String,
    @Json(name = "shortDescription")
    val shortDescription: String
) {
    val accountId: String
        get() = rawAccountId.filter { it.isDigit() }
}

@JsonClass(generateAdapter = true)
internal class AdvancedOrderGroupSvcResponseDto(
    @Json(name = "OrderNumber")
    val orderNumber: String
)

private fun AdvancedOrderServerMessageDto.toServerMessage(type: ServerMessage.Type): ServerMessage {
    return ServerMessage(
        type = type,
        text = message.replace("&lt;", "<").replace("&gt;", ">"),
        priority = 0,
        code = code
    )
}

private fun Iterable<AdvancedOrderServerMessageDto>?.toServerMessages(type: ServerMessage.Type): Collection<ServerMessage> {
    return this?.map { it.toServerMessage(type) }.orEmpty()
}

private fun String.toNullableBigDecimal(): BigDecimal? = takeIf { it != "0" }?.safeParseBigDecimal()

internal interface OrderProducer {
    val order: AdvancedOrder
    val confirmedOrder: ConfirmedAdvancedOrder
}

private class RawOrder(
    private val accountTitle: String,
    private val detailDto: AdvancedOrderDetailDto,
    private val advancedOrderDto: AdvancedOrderDto?,
    private val instructionDto: AdvancedExecInstructionDto,
    private val legInfoDto: AdvancedLegInfoDto,
    private val productIdDto: AdvancedProductIdDto,
    private val responseDto: AdvancedOrderSvcResponseDto,
    private val groupSvcResponseDto: AdvancedOrderGroupSvcResponseDto?
) : OrderProducer {

    override val order: AdvancedOrder
        get() = createOrder(::createStockOrder, ::createOptionOrder)

    override val confirmedOrder: ConfirmedAdvancedOrder
        get() = createOrder(::createConfirmedStockOrder, ::createConfirmedOptionOrder)

    private inline fun <T : Order> createOrder(stockOrder: () -> T, optionOrder: () -> T): T {
        return if (isSymbolLongOption(productIdDto.symbol)) {
            optionOrder()
        } else {
            stockOrder()
        }
    }

    private fun createConfirmedOptionOrder(): ConfirmedAdvancedOptionOrder {
        return object : ConfirmedAdvancedOptionOrder, AdvancedOptionOrder by createOptionOrder() {
            override val confirmationOrderId: String? = groupSvcResponseDto?.orderNumber
            override val placedDate: String? = formatOrderPlacedTime(responseDto.timePlaced)
        }
    }

    private fun createConfirmedStockOrder(): ConfirmedAdvancedStockOrder {
        return object : ConfirmedAdvancedStockOrder, AdvancedStockOrder by createStockOrder() {
            override val confirmationOrderId: String? = groupSvcResponseDto?.orderNumber
            override val placedDate: String? = formatOrderPlacedTime(responseDto.timePlaced)
        }
    }

    private fun createOptionOrder(): AdvancedOptionOrder {
        return object : AdvancedOptionOrder {
            override val account: String = accountTitle
            override val legs: List<OrderLeg> = listOf(createOrderLeg(legInfoDto, productIdDto, true))
            override val priceType: PriceType = resolvePriceType()
            override val orderTrigger: String = detailDto.orderTrigger
            override val term: OrderTerm = OrderTerm.mapOrderTerm(detailDto.orderTerm)
            override val allOrNone: Boolean = instructionDto.aonFlag == "1"
            override val limitPrice: BigDecimal? = detailDto.limitPrice.toNullableBigDecimal()
            override val stopPrice: BigDecimal? = detailDto.stopPrice.toNullableBigDecimal()
            override val fee: BigDecimal? = responseDto.totalFee.safeParseBigDecimal()
            override val cost: BigDecimal? = responseDto.orderValue.toNullableBigDecimal()
            override val strategy: StrategyType = StrategyType.UNKNOWN
            override val orderType: OrderType = OrderType.fromTypeCode(detailDto.orderType)
            override val advOrderType: AdvancedOrderType? = advancedOrderDto?.aoOrderType?.let { AdvancedOrderType.fromTypeCode(it) }
            override val yourPrice: String? = resolveYourPrice()
        }
    }

    private fun resolveYourPrice(): String? = when (advancedOrderDto?.aoOrderType) {
        TRAILING_STOP_ORDER_CODE -> advancedOrderDto.initialStopPrice
        HIDDEN_STOP_ORDER_CODE -> advancedOrderDto.initialStopPrice
        else -> null
    }

    /**
     * This method is the work around if the advanced order type field is set,
     * for all such cases orderTrigger will be set to default one. In short we should ignore order trigger for advanced orders.
     */
    private fun resolvePriceType(): PriceType = when (advancedOrderDto?.aoOrderType) {
        TRAILING_STOP_ORDER_CODE -> resolveTrailingStopPriceType(advancedOrderDto)
        HIDDEN_STOP_ORDER_CODE -> PriceType.HIDDEN_STOP
        else -> PriceType.mapPriceType(detailDto.orderTrigger)
    }

    private fun resolveTrailingStopPriceType(advancedOrderDto: AdvancedOrderDto): PriceType {
        return when (advancedOrderDto.offsetType) {
            TRAILING_STOP_DOLLAR_CODE -> PriceType.TRAILING_STOP_DOLLAR
            TRAILING_STOP_PERCENT_CODE -> PriceType.TRAILING_STOP_PERCENT
            else -> PriceType.UNKNOWN
        }
    }

    private fun createStockOrder(): AdvancedStockOrder {
        return object : AdvancedStockOrder {
            override val account: String = accountTitle
            override val legs: List<OrderLeg> = listOf(createOrderLeg(legInfoDto, productIdDto, false))
            override val priceType: PriceType = resolvePriceType()
            override val orderTrigger: String = detailDto.orderTrigger
            override val term: OrderTerm = OrderTerm.mapOrderTerm(detailDto.orderTerm)
            override val allOrNone: Boolean = instructionDto.aonFlag == "1"
            override val limitPrice: BigDecimal? = detailDto.limitPrice.toNullableBigDecimal()
            override val stopPrice: BigDecimal? = detailDto.stopPrice.toNullableBigDecimal()
            override val fee: BigDecimal? = responseDto.totalFee.safeParseBigDecimal()
            override val cost: BigDecimal? = responseDto.orderValue.toNullableBigDecimal()
            override val borrowRate: String? = null
            override val estDailyBorrowInterest: String? = null
            override val yourPrice: String? = resolveYourPrice()
            override val orderType: OrderType = OrderType.fromTypeCode(detailDto.orderType)
            override val advOrderType: AdvancedOrderType? = advancedOrderDto?.aoOrderType?.let { AdvancedOrderType.fromTypeCode(it) }
            override val otcTier: String? = null
        }
    }

    private fun createOrderLeg(legInfoDto: AdvancedLegInfoDto, productIdDto: AdvancedProductIdDto, isOption: Boolean): OrderLeg {
        return object : OrderLeg {
            override val action: TransactionType = TransactionType.from(OrderAction.fromString(legInfoDto.orderAction), legInfoDto.positionEffect, isOption)
            override val quantity: BigDecimal? = legInfoDto.quantity.safeParseBigDecimal()
            override val symbol: String = productIdDto.symbol
        }
    }

    private fun formatOrderPlacedTime(timeMs: String?): String? = timeMs?.toLongOrNull()?.let { formatOrderPlacedTime(it) }
}
