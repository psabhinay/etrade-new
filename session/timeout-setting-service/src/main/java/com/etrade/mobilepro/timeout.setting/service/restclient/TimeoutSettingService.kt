package com.etrade.mobilepro.timeout.setting.service.restclient

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.timeout.setting.service.dto.ResponseDataDto
import com.etrade.mobilepro.timeout.setting.service.dto.TimeoutSettingRequestDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.POST

interface TimeoutSettingService {

    /**
     * Api to update session timeout of the current user
     */
    @JsonMoshi
    @POST("/app/user/setmobileuserprofileitem.json")
    suspend fun setUserSessionTimeout(@JsonMoshi @Body request: TimeoutSettingRequestDto): ServerResponseDto<ResponseDataDto>
}
