package com.etrade.mobilepro.pnl.calculations

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.util.domain.data.Gain
import org.junit.jupiter.api.Test
import java.math.BigDecimal

private val INVALID_VALUE = null
private val VALID_VALUE = BigDecimal("2.0")

class PnlCalculationsTest {

    @Test
    fun `test calculateTotalGain`() {
        listOf(
            listOf(InstrumentType.UNKNOWN, INVALID_VALUE, VALID_VALUE, VALID_VALUE, VALID_VALUE, VALID_VALUE, null),
            listOf(InstrumentType.UNKNOWN, VALID_VALUE, INVALID_VALUE, VALID_VALUE, VALID_VALUE, VALID_VALUE, null),
            listOf(InstrumentType.UNKNOWN, VALID_VALUE, VALID_VALUE, INVALID_VALUE, VALID_VALUE, VALID_VALUE, null),
            listOf(InstrumentType.UNKNOWN, VALID_VALUE, VALID_VALUE, VALID_VALUE, INVALID_VALUE, VALID_VALUE, null),
            listOf(InstrumentType.UNKNOWN, VALID_VALUE, VALID_VALUE, VALID_VALUE, VALID_VALUE, INVALID_VALUE, null),
            listOf(InstrumentType.UNKNOWN, VALID_VALUE, VALID_VALUE, BigDecimal("0"), BigDecimal("0"), BigDecimal("0"), null),
            listOf(InstrumentType.UNKNOWN, BigDecimal("6"), VALID_VALUE, VALID_VALUE, VALID_VALUE, VALID_VALUE, Gain(BigDecimal("4"), BigDecimal("50")))
        ).forEach { testParams ->
            val result = calculateTotalGain(
                instrumentType = testParams[0] as InstrumentType,
                currentPriceDecimal = testParams[1] as BigDecimal?,
                pricePaidDecimal = testParams[2] as BigDecimal?,
                quantityDecimal = testParams[3] as BigDecimal?,
                commissionDecimal = testParams[4] as BigDecimal?,
                feesDecimal = testParams[5] as BigDecimal?
            )
            val expectedResult = testParams[6] as Gain?
            println("Test result = $result, expected result = $expectedResult")
            assert(
                result == expectedResult ||
                    (
                        expectedResult?.dollar?.compareTo(result?.dollar) == 0 &&
                            expectedResult.percent.compareTo(result?.percent) == 0
                        )
            )
        }
    }
}
