package com.etrade.mobilepro.bank.transactions

import android.content.res.Resources
import com.etrade.mobilepro.bank.R
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import javax.inject.Inject

class BankTransactionViewFactory @Inject constructor(private val resources: Resources) : DynamicUiViewFactory {

    private val emptyValue: String by lazy { resources.getString(R.string.empty_default) }
    private val emptyDate: String by lazy { resources.getString(R.string.empty_date_default) }

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        (dto as? BankTransactionViewDto)?.data?.let {
            return BankTransactionGenericLayout(it, emptyValue, resources.getString(R.string.accessibility_empty_amount_description), emptyDate)
        } ?: throw DynamicUiViewFactoryException("BankTransactionViewFactory registered to unknown type")
    }
}
