package com.etrade.mobilepro.bank;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.etrade.mobilepro.bank.databinding.BankActionsBindingImpl;
import com.etrade.mobilepro.bank.databinding.BankDateRangeSelectorBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_BANKACTIONS = 1;

  private static final int LAYOUT_BANKDATERANGESELECTOR = 2;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(2);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.etrade.mobilepro.bank.R.layout.bank_actions, LAYOUT_BANKACTIONS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.etrade.mobilepro.bank.R.layout.bank_date_range_selector, LAYOUT_BANKDATERANGESELECTOR);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_BANKACTIONS: {
          if ("layout/bank_actions_0".equals(tag)) {
            return new BankActionsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for bank_actions is invalid. Received: " + tag);
        }
        case  LAYOUT_BANKDATERANGESELECTOR: {
          if ("layout/bank_date_range_selector_0".equals(tag)) {
            return new BankDateRangeSelectorBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for bank_date_range_selector is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(8);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    result.add(new com.etrade.mobilepro.dynamicui.DataBinderMapperImpl());
    result.add(new com.etrade.mobilepro.inboxmessages.DataBinderMapperImpl());
    result.add(new com.etrade.mobilepro.inboxmessages.dynamicui.DataBinderMapperImpl());
    result.add(new com.etrade.mobilepro.themeselection.DataBinderMapperImpl());
    result.add(new com.etrade.mobilepro.transactions.DataBinderMapperImpl());
    result.add(new com.etrade.mobilepro.ui.multitoggle.DataBinderMapperImpl());
    result.add(new com.etrade.mobilepro.util.android.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(2);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "obj");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(2);

    static {
      sKeys.put("layout/bank_actions_0", com.etrade.mobilepro.bank.R.layout.bank_actions);
      sKeys.put("layout/bank_date_range_selector_0", com.etrade.mobilepro.bank.R.layout.bank_date_range_selector);
    }
  }
}
