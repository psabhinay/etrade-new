package com.etrade.mobilepro.trade.api.mutualfunds

import com.etrade.mobilepro.trade.api.OrderPreviewResponse

interface MutualFundsOrderPreviewResponse : OrderPreviewResponse, MutualFundsOrderResponse
