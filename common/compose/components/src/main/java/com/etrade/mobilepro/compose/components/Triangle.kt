package com.etrade.mobilepro.compose.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.GenericShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import com.etrade.mobilepro.common.compose.theme.Dimens

@Composable
fun Triangle(
    modifier: Modifier = Modifier,
    size: Dp = Dimens.Size8dp,
    backgroundColor: Color = MaterialTheme.colors.surface,
) {
    Box(
        modifier = modifier
            .width(size)
            .height(size)
            .clipToBounds()
            .background(backgroundColor, TriangleShape)
    )
}

private val TriangleShape = GenericShape { size, _ ->
    moveTo(size.width / 2f, 0f)
    lineTo(size.width, size.height)
    lineTo(0f, size.height)
}
