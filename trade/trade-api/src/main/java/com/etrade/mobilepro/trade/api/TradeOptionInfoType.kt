package com.etrade.mobilepro.trade.api

import androidx.annotation.Keep

@Keep
enum class TradeOptionInfoType {
    SECURITY_TYPE,
    PRICE_TYPE,
    PRICE_TYPE_OPTIONS,
    ORDER_TYPE,
    ORDER_TYPE_MF,
    ORDER_TYPE_OPTIONS,
    SECURITY_TYPE_OPTIONS
}
