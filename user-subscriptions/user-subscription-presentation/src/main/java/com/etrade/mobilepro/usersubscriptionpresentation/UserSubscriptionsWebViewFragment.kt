package com.etrade.mobilepro.usersubscriptionpresentation

import android.webkit.JavascriptInterface
import android.webkit.WebView
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.styledalertdialog.StyledAlertDialog
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

@RequireLogin
class UserSubscriptionsWebViewFragment @Inject constructor(
    @DeviceType deviceType: String,
    private val user: User,
    private val streamingStatusController: StreamingStatusController
) : WebViewFragment(deviceType) {

    override fun setupWebView(webView: WebView) {
        super.setupWebView(webView)
        webView.addJavascriptInterface(MdaSubscriptionJSInterface(), "mdaInterface")
    }

    private fun autoEnrollUser() {
        user.isRealtimeUser = true
        streamingStatusController.isStreamingToggleEnabled = true
        context?.let {
            StyledAlertDialog(it).showAlert(
                title = getString(R.string.please_note),
                message = getString(R.string.streaming_enabled),
                isCancelable = true,
                positiveAnswerAction = {
                    activity?.finish()
                }
            )
        }
    }

    inner class MdaSubscriptionJSInterface {
        @JavascriptInterface
        fun autoEnrollmentComplete() {
            autoEnrollUser()
        }
    }
}
