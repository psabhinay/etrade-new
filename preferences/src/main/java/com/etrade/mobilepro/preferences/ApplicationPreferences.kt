package com.etrade.mobilepro.preferences

import androidx.lifecycle.MutableLiveData

interface ApplicationPreferences {

    var appVersion: Int

    var isBarcodeTermsAgreed: Boolean

    val deviceId: String

    // Indicate how many times user taps into menu page item when New pill feature is available.
    val menuItemClickCounts: MenuItemClickCountsPreference

    var lastLoginMajorMinorVersion: String

    var versionLoginCount: Int

    var shouldSyncChartIqData: Boolean

    var shouldDisplayOnboarding: Boolean

    var isLocalyticsOnboardingShow: Boolean

    val shouldDisplayAccountTabHostTooltip: Boolean

    val shouldDisplayMarketsTooltip: Boolean

    var displayPortfolioControlBarToolTip: Boolean

    var displayPortfolioSettingsToolTip: Boolean

    var displayPortfolioMoreInfoToolTip: Boolean

    val displayPortfolioTooltip: MutableLiveData<Boolean>

    var shouldDisplayWatchListTooltip: Boolean

    val shouldDisplayMenuTooltip: Boolean

    val shouldDisplayCompleteViewTooltip: Boolean

    var shouldDisplayEULA: Boolean

    var shouldDisplayQuoteLookupOnCompanyNewsTab: Boolean

    var shouldShowAccountsOrganizeBoarding: Boolean

    var homeLandingCount: Int

    var shouldShowLandingSelection: Boolean

    var appEnvironment: String

    var connectionTimeoutSec: Int

    var readTimeoutSec: Int

    var writeTimeoutSec: Int

    var limitSessionTo30Sec: Boolean

    var isCircuitBreakEnabled: Boolean

    var isAdvancedOrderDisclosureSigned: Boolean

    var isConditionalOrderDisclosureSigned: Boolean

    var lastVisitedAccountsTabIndex: Int

    var watchlistSelectedIndex: Int

    /**
     * Used for storing selected accountId at the moment of leaving [AccountsTabHostFragment]
     * to be able to restore the current state on later returning on "Accounts" tab.
     *
     * @return accountId or empty string if the latest page on "Accounts" bottom menu was not [AccountsTabHostFragment].
     */
    var lastVisitedAccountsTabAccountId: String

    var webUrl: String
    var eDocWebUrl: String
    var expressUrl: String
    var mobileTradeUrl: String
    var mobileETradeUrl: String
    var marketOnDemandUrl: String
    var apiUrl: String
    var level1StreamerUrl: String
    var level2StreamerUrl: String
    val onboardingABValue: Int
    var shouldTrackWithAppsFlyer: Boolean
    var watchlistSortState: String
}
