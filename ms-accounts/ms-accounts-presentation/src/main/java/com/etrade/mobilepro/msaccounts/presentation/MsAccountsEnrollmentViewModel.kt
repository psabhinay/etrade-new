package com.etrade.mobilepro.msaccounts.presentation

import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.Diffable
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.util.prependBaseUrl
import com.etrade.msaccounts.dto.MsAccountsEnrollmentViewDto

class MsAccountsEnrollmentViewModel(
    private val baseUrl: String,
    private val dto: MsAccountsEnrollmentViewDto
) : DynamicViewModel(R.layout.ms_accounts_enrollment), Diffable {

    override val variableId: Int = BR.obj

    fun onDisplayAccountsClicked() {
        dto.ctaList?.firstOrNull()?.apply {
            val webViewUrl = clickActionDto.webViewUrl.prependBaseUrl(baseUrl)
            logger.debug("onDisplayAccountsClicked webViewUrl: $webViewUrl")

            clickEvents.postValue(
                CtaEvent.NavDirectionsNavigation(
                    MsAccountsWebViewDirections.actionHomeToMsaccountsWebView(
                        title = title ?: "",
                        url = webViewUrl
                    )
                )
            )
        }
    }

    // Assumes that there will be only one MsAccountsEnrollmentViewModel in a list
    override fun isSameItem(other: Any) =
        other.javaClass == javaClass

    override fun hasSameContents(other: Any) =
        dto == (other as? MsAccountsEnrollmentViewModel)?.dto
}
