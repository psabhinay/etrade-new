package com.etrade.mobilepro.billpay.usecase

import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.BillPayStatus
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.SessionId
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.util.UseCase
import javax.inject.Inject

interface BillPayInitialSetupUseCase : UseCase<Unit, ETResult<BillPayInitalDataLoaderResult>>

class BillPayInitialSetupUseCaseImpl @Inject constructor(
    private val repo: BillPayRepo,
    private val payeeListUseCase: PayeeListUseCase,
    private val filterBillPayAccounts: FilterBillPayAccountsUseCase,
    private val generateBillPayStatus: GenerateBillPayStatusUseCase
) : BillPayInitialSetupUseCase {
    override suspend fun execute(parameter: Unit): ETResult<BillPayInitalDataLoaderResult> = runCatchingET {
        val allAccounts = repo.getAccounts().getOrThrow()
        val validAccounts = filterBillPayAccounts.execute(allAccounts).getOrThrow()
        val status = generateBillPayStatus.execute(allAccounts).getOrThrow()

        var sessionId: String? = null
        var payeeList: List<Payee> = emptyList()
        if (status == BillPayStatus.Supported) {
            sessionId = repo.getBillPaySessionId().getOrThrow()
            payeeList = payeeListUseCase.execute(SessionId(sessionId)).getOrThrow()
        }

        BillPayInitalDataLoaderResult(status, allAccounts, validAccounts, sessionId, payeeList)
    }
}

data class BillPayInitalDataLoaderResult(
    val status: BillPayStatus,
    val allAccounts: List<AccountDetails> = emptyList(),
    val validAccounts: List<AccountDetails> = emptyList(),
    val sessionId: String? = null,
    val payees: List<Payee> = emptyList()
)
