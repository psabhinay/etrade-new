package com.etrade.mobilepro.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NavigationRes
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.accounts.util.android.view.AccountViewType
import com.etrade.mobilepro.common.toolbarView
import com.etrade.mobilepro.home.model.MARKET_DATA_AGREEMENT_KEY
import com.etrade.mobilepro.home.model.PromptInDialogsHelper
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.userviewmodel.UserViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

private const val INITIAL_LANDING_PAGE = "initialLandingPage"

class AccountsHomeFragment : Fragment() {

    @Inject
    lateinit var user: User

    @Inject
    lateinit var userViewModel: UserViewModel
    private val args: AccountsHomeFragmentArgs by navArgs()

    @Inject
    lateinit var dialogsHelper: PromptInDialogsHelper

    private var initialLandingPage: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
            initialLandingPage = it.getBoolean(INITIAL_LANDING_PAGE)
        }
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        return null
    }

    override fun onResume() {
        super.onResume()
        when (userViewModel.peekUserSessionState) {
            is SessionStateChange.AUTHENTICATED -> {
                activity?.let { onNavigateToHome() }
            }
            else -> {
                findNavController().navigate(R.id.action_home_to_overview)
                // Drop down reset.
                activity?.toolbarView?.setOnClickListener(null)
            }
        }
        initialLandingPage = false
    }

    @NavigationRes
    @Suppress("ComplexMethod", "LongMethod")
    private fun getAccountsHomeInitialNavDestination(accountViewType: AccountViewType): Int? {
        if (findNavController().currentDestination?.id != R.id.accountsHomeFragment) {
            return null
        }
        return when (accountViewType) {
            AccountViewType.UNAUTHENTICATED_VIEW -> R.id.action_home_to_overview
            AccountViewType.SINGLE_BROKERAGE_ACCOUNT_VIEW -> args.accountId?.run {
                if (initialLandingPage) {
                    R.id.action_home_to_account_tabs
                } else {
                    R.id.action_home_to_completeview
                }
            } ?: if (userViewModel.hasMoreThanOneAccount) { R.id.action_home_to_account_tabs } else { R.id.action_home_to_completeview }

            AccountViewType.BANK_ACCOUNT_VIEW -> if (userViewModel.isNavigationToAccount) {
                R.id.action_home_to_bank
            } else {
                R.id.action_home_to_completeview
            }
            AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW -> R.id.action_home_to_completeview
            AccountViewType.MANAGED_ACCOUNT_VIEW, AccountViewType.ESP_ACCOUNT_VIEW -> args.accountId?.run {
                if (initialLandingPage) {
                    R.id.action_home_to_account_tabs
                } else {
                    R.id.action_home_to_completeview
                }
            } ?: R.id.action_home_to_completeview

            AccountViewType.ALL_BROKERAGE_ACCOUNT_VIEW -> R.id.action_home_to_all_brokerage
        }
    }

    private fun onNavigateToHome() {
        val destination = getAccountsHomeInitialNavDestination(userViewModel.lastSelectedAccountViewType)
        destination?.let {
            when (destination) {
                R.id.action_home_to_account_tabs -> navigateToAccountTabs()
                R.id.action_home_to_bank -> navigateToBankAccount()
                R.id.action_home_to_all_brokerage -> navigateToAllBrokerage()
                else -> findNavController().navigate(it)
            }
        } ?: if (findNavController().currentDestination?.id == R.id.action_home_to_completeview) {
            findNavController().navigate(R.id.action_home_to_account_tabs)
        }
        dialogsHelper.showPendingDialog(MARKET_DATA_AGREEMENT_KEY, parentFragmentManager)
    }

    private fun navigateToBankAccount() {
        val account = requireNotNull(userViewModel.currentSelectedAccount.value?.account)
        val action = MainGraphDirections.actionHomeToBank(account.accountUuid, account.accountId)
        navigateWithPopUpOptions(action)
    }

    private fun navigateToAllBrokerage() {
        navigateWithPopUpOptions(MainGraphDirections.actionHomeToAllBrokerage())
    }

    private fun navigateWithPopUpOptions(navDirections: NavDirections) {
        val behaveAsTopLevelDestination = !userViewModel.hasMoreThanOneAccount

        val options = NavOptions.Builder().apply {
            if (behaveAsTopLevelDestination) {
                setPopUpTo(R.id.mainGraph, true)
            } else {
                setPopUpTo(R.id.completeviewFragment, false)
            }
        }.build()
        findNavController().navigate(navDirections, options)
    }

    private fun navigateToAccountTabs() {
        val accountId = userViewModel.currentSelectedAccount.value?.account?.accountId
        val accountUuid = userViewModel.currentSelectedAccount.value?.account?.accountUuid
        val action = MainGraphDirections.actionHomeToAccountTabs(
            accountId,
            accountUuid,
            args.tabTitle,
            additionalArguments = args.additionalArguments
        )
        val behaveAsTopLevelDestination = !userViewModel.hasMoreThanOneAccount

        val options = NavOptions.Builder().apply {
            if (behaveAsTopLevelDestination) {
                setPopUpTo(R.id.mainGraph, true)
            } else {
                setPopUpTo(R.id.completeviewFragment, false)
            }
        }.build()

        findNavController().navigate(action, options)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(INITIAL_LANDING_PAGE, initialLandingPage)
        super.onSaveInstanceState(outState)
    }
}
