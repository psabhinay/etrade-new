package com.etrade.mobilepro.billpay.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PayeeRequestDto(
    @Json(name = "value")
    val value: RequestSessionDto
) {
    companion object {
        fun toPayeeRequest(sessionId: String): PayeeRequestDto =
            PayeeRequestDto(
                RequestSessionDto(sessionId = sessionId)
            )

        fun toDeletePayeeRequest(sessionId: String, payeeId: String): PayeeRequestDto =
            PayeeRequestDto(
                RequestSessionDto(sessionId = sessionId, payeeId = payeeId)
            )
    }
}

@JsonClass(generateAdapter = true)
class RequestSessionDto(
    @Json(name = "sessionId")
    val sessionId: String = "",
    @Json(name = "payeeId")
    val payeeId: String? = null
)
