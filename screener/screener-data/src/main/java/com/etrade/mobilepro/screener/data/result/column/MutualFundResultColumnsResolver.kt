package com.etrade.mobilepro.screener.data.result.column

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.ExpensesFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.FeesFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.FundProfileFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.PerformanceFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.PortfolioFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.RiskFilter
import com.etrade.mobilepro.screener.data.result.ResultColumn
import com.etrade.mobilepro.screener.data.result.ResultColumnsResolver
import javax.inject.Inject

private val commonForAllMutualFundResults = setOf(
    ResultColumn.Description,
    ResultColumn.MorningStarRating,
    ResultColumn.FundCategory,
    ResultColumn.TotalAssets,
    ResultColumn.ExpenseRatio
)

class MutualFundResultColumnsResolver @Inject constructor() : ResultColumnsResolver {

    override fun getColumns(filter: ScreenerFilter): Set<ResultColumn> = commonForAllMutualFundResults +
        when (filter) {
            is FundProfileFilter -> emptySet()
            is PerformanceFilter -> getSpecificColumns(filter)
            is ExpensesFilter -> getSpecificColumns(filter)
            is FeesFilter -> getSpecificColumns(filter)
            is RiskFilter -> getSpecificColumns(filter)
            is PortfolioFilter -> getSpecificColumns(filter)
            else -> throw IllegalArgumentException("$filter is wrong for mutual fund result columns resolver")
        }

    private fun getSpecificColumns(filter: PerformanceFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.isPerformanceValuesSelected() && filter.isPeriodSelected()) {
            result.add(ResultColumn.DistributionYield)
            result.add(ResultColumn.CategoryReturn1Year)
            result.add(ResultColumn.CategoryReturn5Years)
            result.add(ResultColumn.CategoryReturn10Years)
        }
        return result
    }

    private fun getSpecificColumns(filter: ExpensesFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.isNotEmpty()) {
            result.add(ResultColumn.DistributionYield)
            result.add(ResultColumn.CategoryReturn1Year)
            result.add(ResultColumn.CategoryReturn5Years)
            result.add(ResultColumn.CategoryReturn10Years)
            result.add(ResultColumn.FrontLoad)
            result.add(ResultColumn.DeferLoad)
            result.add(ResultColumn.RedemptionFee)
        }
        return result
    }

    private fun getSpecificColumns(filter: FeesFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.isNotEmpty()) {
            result.add(ResultColumn.FrontLoad)
            result.add(ResultColumn.DeferLoad)
            result.add(ResultColumn.RedemptionFee)
        }
        return result
    }

    private fun getSpecificColumns(filter: RiskFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.isNotEmpty()) {
            result.add(ResultColumn.ThreeYearsAlpha)
            result.add(ResultColumn.ThreeYearsBeta)
            result.add(ResultColumn.ThreeYearsSharpeRatio)
            result.add(ResultColumn.ThreeYearsRSquare)
        }
        return result
    }

    private fun getSpecificColumns(filter: PortfolioFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.isNotEmpty()) {
            result.add(ResultColumn.TurnOverRatio)
            result.add(ResultColumn.PortfolioConcentration)
            result.add(ResultColumn.AverageMarketCap)
        }
        if (filter.isYieldSelected()) {
            result.add(ResultColumn.DistributionYield)
        }
        return result
    }
}
