package com.etrade.mobilepro.session.api

@Suppress("MagicNumber")
enum class SessionEvents(val code: Int) {
    /**
     * Indicates session termination due to device idle timeout
     */
    MSG_SESSION_TERMINATED(1000),
    /**
     * Indicates session termination due to SAR message
     */
    MSG_SESSION_SAR_TERMINATED(1001),
    /**
     * Indicates session timeout alarm manager cancel
     */
    MSG_SESSION_TIMEOUT_ALARM_CANCELLED(1002),

    /**
     * Indicates that no session termination expected
     */
    MSG_NO_SESSION_TIMEOUT(0)
}
