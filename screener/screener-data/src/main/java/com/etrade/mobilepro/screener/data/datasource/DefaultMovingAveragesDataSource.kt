package com.etrade.mobilepro.screener.data.datasource

import android.content.res.Resources
import com.etrade.mobilepro.screener.api.MovingAverage
import com.etrade.mobilepro.screener.api.datasource.MovingAveragesDataSource
import com.etrade.mobilepro.screener.data.R
import javax.inject.Inject

class DefaultMovingAveragesDataSource @Inject constructor(resources: Resources) : MovingAveragesDataSource {
    private val emptyMovingAverage by lazy {
        MovingAverage(
            id = "",
            title = resources.getString(R.string.average),
            isAllItemsElement = true
        )
    }

    private val movingAveragesList by lazy {
        listOf(
            MovingAverage(
                id = "LastPriceVS5DaySMA",
                title = resources.getString(R.string.sma_5_day)
            ),
            MovingAverage(
                id = "LastPriceVS10DaySMA",
                title = resources.getString(R.string.sma_10_day)
            ),
            MovingAverage(
                id = "LastPriceVS20DaySMA",
                title = resources.getString(R.string.sma_20_day)
            ),
            MovingAverage(
                id = "LastPriceVS50DaySMA",
                title = resources.getString(R.string.sma_50_day)
            ),
            MovingAverage(
                id = "LastPriceVS100DaySMA",
                title = resources.getString(R.string.sma_100_day)
            ),
            MovingAverage(
                id = "LastPriceVS200DaySMA",
                title = resources.getString(R.string.sma_200_day)
            ),
            MovingAverage(
                id = "LastPriceVS5DayEMA",
                title = resources.getString(R.string.ema_5_day)
            ),
            MovingAverage(
                id = "LastPriceVS10DayEMA",
                title = resources.getString(R.string.ema_10_day)
            ),
            MovingAverage(
                id = "LastPriceVS20DayEMA",
                title = resources.getString(R.string.ema_20_day)
            ),
            MovingAverage(
                id = "LastPriceVS50DayEMA",
                title = resources.getString(R.string.ema_50_day)
            ),
            MovingAverage(
                id = "LastPriceVS100DayEMA",
                title = resources.getString(R.string.ema_100_day)
            ),
            MovingAverage(
                id = "LastPriceVS200DayEMA",
                title = resources.getString(R.string.ema_200_day)
            )
        )
    }
    override fun getEmptyItem(): MovingAverage = emptyMovingAverage

    override fun getMovingAverages(): List<MovingAverage> = movingAveragesList
}
