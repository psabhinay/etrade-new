package com.etrade.mobilepro.transfermoney.presentation.external

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.transfermoney.presentation.R
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

private val verificationMethod = mapOf(
    R.string.transfer_instant_verification to "INSTANT",
    R.string.transfer_manual_verification to "MANUAL"
)

class ExternalAccountVerificationMethodActionSheetViewModel @Inject constructor() : ViewModel(), ActionSheetViewModel {
    private val _actions = MutableLiveData<List<ActionItem>>()
    private val _addExternalAccountNavSignal = MutableLiveData<ConsumableLiveEvent<String>>()
    override val actions: LiveData<List<ActionItem>>
        get() = _actions
    internal val addExternalAccountNavSignal: LiveData<ConsumableLiveEvent<String>>
        get() = _addExternalAccountNavSignal

    override fun selectAction(item: ActionItem) {
        _actions.value = emptyList()
        _addExternalAccountNavSignal.postValue(ConsumableLiveEvent(verificationMethod.getOrDefault(item.id, "")))
    }

    override fun cancelActions() {
        _actions.value = emptyList()
    }

    fun showExternalVerificationMethodSelector() {
        _actions.value = listOf(
            ActionItem(
                id = R.string.transfer_instant_verification,
                displayNameId = R.string.transfer_instant_verification,
                descriptionId = R.string.transfer_instant_verification_description
            ),
            ActionItem(
                id = R.string.transfer_manual_verification,
                displayNameId = R.string.transfer_manual_verification,
                descriptionId = R.string.transfer_manual_verification_description
            )
        )
    }
}
