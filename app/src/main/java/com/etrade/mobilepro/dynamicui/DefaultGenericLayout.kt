package com.etrade.mobilepro.dynamicui

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicWidget
import com.etrade.mobilepro.dynamicui.api.GenericLayout

class DefaultGenericLayout(
    override val viewModelClass: Class<out DynamicViewModel>,
    override val viewModelFactory: ViewModelProvider.Factory
) : GenericLayout, DynamicWidget {
    override val type = DynamicWidget.Type.DEFAULT

    override val uniqueIdentifier: String = javaClass.name
}
