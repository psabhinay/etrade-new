package com.etrade.mobilepro.screener.api.filter.etf

import com.etrade.mobilepro.screener.api.MovingAverage
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class EtfTechnicalsFilter(
    val priceMovingBelow: Boolean = false,
    val priceMovingAbove: Boolean = false,
    val average: MovingAverage,

    val bottom20: Boolean = false,
    val belowMid20: Boolean = false,
    val mid20: Boolean = false,
    val aboveMid20: Boolean = false,
    val top20: Boolean = false
) : ScreenerFilter {
    fun isEtfTechnicalsValuesSelected(): Boolean = bottom20 || belowMid20 ||
        mid20 || aboveMid20 || top20

    override fun isEmpty(): Boolean = !priceMovingAbove && !priceMovingBelow && average.isAllItemsElement &&
        !isEtfTechnicalsValuesSelected()

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.ETF
}
