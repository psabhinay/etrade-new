package com.etrade.mobilepro.screener.api.result

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithInstrumentType
import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell
import com.etrade.mobilepro.util.color.DataColor

private const val TWO_LINES_CHARS_COUNT_THRESHOLD = 17

open class ResultDataCell(
    value: Any,
    displayText: String,
    override val instrumentType: InstrumentType,
    val symbol: String,
    dataColor: DataColor = DataColor.NEUTRAL,
    val hasEllipsize: Boolean = false
) : DataCell(
    rowId = symbol,
    value = value,
    displayText = displayText,
    dataColor = dataColor
),
    WithInstrumentType {

    override fun getContent(): Any = value

    val isWorthSplittingInTwoLines = displayText.length > TWO_LINES_CHARS_COUNT_THRESHOLD
}
