package com.etrade.mobilepro.tableviewutils.presentation.sorting

interface TableDataSortable {

    fun sortTable(column: Int?)
}
