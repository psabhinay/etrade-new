package com.etrade.mobilepro.stub.streaming

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import io.reactivex.Observable

class TestStreamingSubscriptionController : StreamingSubscriptionController {

    private lateinit var onUpdateCallback: (symbol: String, instrumentType: InstrumentType, data: Level1Data) -> Unit

    fun emitStreamData(symbol: String, instrumentType: InstrumentType, data: Level1Data) {
        onUpdateCallback(symbol, instrumentType, data)
    }

    override fun initSubscription(
        fieldsProvider: (InstrumentType) -> Set<Level1Field>,
        symbolsSource: Observable<Set<Symbol>>,
        onUpdateCallback: (symbol: String, instrumentType: InstrumentType, data: Level1Data) -> Unit
    ) {
        this.onUpdateCallback = onUpdateCallback
    }

    override fun resumeSubscription() {}

    override fun pauseSubscription() {}

    override fun cancelSubscription() {}
}
