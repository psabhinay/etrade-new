package com.etrade.mobilepro.transactions.api

import com.etrade.mobilepro.transactions.TransactionsResponseInfo

class TransactionsResponse(
    val queryValue: String,
    val transactionsResponseInfo: TransactionsResponseInfo,
    var fetchedAt: Long = 0
)
