package com.etrade.mobilepro.quotelookup.interaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import com.etrade.mobilepro.searchingapi.SearchInteractor
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject

class DefaultSearchInteractor @Inject constructor() : SearchInteractor {
    private val searchResultsSubject: Subject<SearchResult> = PublishSubject.create<SearchResult>().toSerialized()

    override fun searchResultItemSelected(searchType: SearchType, item: SearchResultItem) {
        searchResultsSubject.onNext(SearchResult(searchType, item))
    }

    override fun searchResultItemAsLiveData(searchType: SearchType): LiveData<SearchResultItem> =
        LiveDataReactiveStreams.fromPublisher(searchResultItemAsObservable(searchType).toFlowable(BackpressureStrategy.LATEST))

    override fun searchResultItemUntilChangedAsLiveData(searchType: SearchType): LiveData<SearchResultItem> =
        LiveDataReactiveStreams.fromPublisher(searchResultItemUntilChangedAsObservable(searchType).toFlowable(BackpressureStrategy.LATEST))

    override fun searchResultItemUntilChangedAsObservable(searchType: SearchType): Observable<SearchResultItem> {
        return searchResultsSubject
            .filter {
                it.type == searchType
            }
            .map {
                it.item
            }
            .distinctUntilChanged()
    }

    override fun searchResultItemAsObservable(searchType: SearchType): Observable<SearchResultItem> {
        return searchResultsSubject
            .filter {
                it.type == searchType
            }
            .map {
                it.item
            }
    }

    private data class SearchResult(
        val type: SearchType,
        val item: SearchResultItem
    )
}
