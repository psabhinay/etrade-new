package com.etrade.mobilepro.alerts.presentation.manage

import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import javax.annotation.concurrent.NotThreadSafe

interface UserAlertsHolder {
    fun setAlerts(newAlerts: List<UserAlertItem>)
    fun getAlerts(): List<UserAlertItem>
    fun deleteAlert(alert: UserAlertItem): Boolean
    fun insertAlert(alert: UserAlertItem)
    fun toggleDeleteButtonVisibility(alert: UserAlertItem, isVisible: Boolean)
    fun hideAllDeleteButtons()
}

@NotThreadSafe
class DefaultUserAlertsHolder : UserAlertsHolder {
    private val alerts = mutableListOf<UserAlertItem>()
    private val userAlertsComparator by lazy {
        Comparator<UserAlertItem> { first, second ->
            if (first.symbol == second.symbol) {
                when {
                    first::class == second::class -> first.description.compareTo(second.description)
                    first is UserAlertItem.NewsAlert -> 1
                    second is UserAlertItem.NewsAlert -> -1
                    else -> 0
                }
            } else {
                first.symbol.compareTo(second.symbol)
            }
        }
    }

    override fun setAlerts(newAlerts: List<UserAlertItem>) {
        alerts.clear()
        alerts.addAll(newAlerts)
        alerts.sortWith(userAlertsComparator)
    }

    override fun getAlerts(): List<UserAlertItem> = alerts

    override fun deleteAlert(alert: UserAlertItem): Boolean {
        val toDelete = alerts.firstOrNull {
            UserAlertItem.isSameDeleteInfo(alert, it)
        }
        return alerts.remove(toDelete)
    }

    override fun insertAlert(alert: UserAlertItem) {
        alerts.add(alert)
        alerts.sortWith(userAlertsComparator)
    }

    override fun toggleDeleteButtonVisibility(alert: UserAlertItem, isVisible: Boolean) {
        hideAllDeleteButtons()
        val newOpenedIndex = alerts.indexOf(alert)
        if (newOpenedIndex >= 0) {
            alerts[newOpenedIndex] = alert.copyAndToggleDeleteBtn(isDeleteBtnVisible = isVisible)
        }
    }

    override fun hideAllDeleteButtons() {
        val currentlyOpenedIndex = alerts.indexOfFirst {
            it.isDeleteBtnVisible
        }
        if (currentlyOpenedIndex >= 0) {
            val currentlyOpened = alerts[currentlyOpenedIndex]
            alerts[currentlyOpenedIndex] = currentlyOpened.copyAndToggleDeleteBtn(isDeleteBtnVisible = false)
        }
    }
}
