package com.etrade.mobilepro.settings.viewmodel.feature

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

/**
 * There has to be one unique [ViewModelActionId] and [requestCode]
 * There should be one [ViewModelActionId] to represent [ViewModelAction] from ViewModel to Fragment
 * and there might be another one to represent action from Fragment to ViewModel
 *
 * @param requestCode passed to DialogFragment or startActivityForResult to provide communication between them and [ViewModelAction]
 *
 */
@SuppressWarnings("MagicNumber")
enum class ViewModelActionId(val requestCode: Int) {
    SHOW_ERROR_SNACK_BAR(10000),
    START_ENABLING_BIOMETRIC(10001),
    BIOMETRIC_RESULT(10002),
    START_MARKET_DATA_AGREEMENT(10003),
    MARKET_DATA_AGREEMENT_RESULT(10004),
    SHOW_STOCK_ALERT_POPUP(10005),
    STOCK_ALERT_POPUP_RESULT(10006),
    SHOW_ACCOUNT_ALERT_POPUP(10007),
    ACCOUNT_ALERT_POPUP_RESULT(10008),
    SHOW_UNABLE_TO_ENABLE_STREAMING_POPUP(10009);

    companion object {
        fun getActionByRequestCode(requestCode: Int): ViewModelActionId? {
            return values().find { it.requestCode == requestCode }
        }
    }
}

/**
 * [ViewModelAction] might represent a generic or specific action between [DynamicViewModel] and Fragment
 *
 * @param viewModelActionId is used to find out related [ViewModel]
 * @param viewModelResultActionId is used when we need to show DialogFragment or Activity for result
 *
 */
sealed class ViewModelAction(open val viewModelActionId: ViewModelActionId, open val viewModelResultActionId: ViewModelActionId) {
    data class Popup(
        override val viewModelActionId: ViewModelActionId,
        override val viewModelResultActionId: ViewModelActionId,
        val title: String,
        val message: String,
        val okTitle: String? = null,
        val cancelTitle: String? = null,
        val showCancelButton: Boolean = true,
        val showAlertTermsAndConditions: Boolean = false
    ) : ViewModelAction(viewModelActionId, viewModelResultActionId)
    data class EnableBiometric(
        override val viewModelActionId: ViewModelActionId = ViewModelActionId.START_ENABLING_BIOMETRIC,
        override val viewModelResultActionId: ViewModelActionId = ViewModelActionId.BIOMETRIC_RESULT
    ) : ViewModelAction(viewModelActionId, viewModelResultActionId)
    data class ErrorSnackBar(
        override val viewModelActionId: ViewModelActionId = ViewModelActionId.SHOW_ERROR_SNACK_BAR,
        override val viewModelResultActionId: ViewModelActionId = ViewModelActionId.SHOW_ERROR_SNACK_BAR
    ) : ViewModelAction(viewModelActionId, viewModelResultActionId)

    override fun hashCode(): Int {
        return viewModelActionId.hashCode()
    }

    override fun equals(other: Any?): Boolean = other is ViewModelAction && viewModelActionId == other.viewModelActionId
}

/**
 *  Shows if the result of ViewModel action is Success, Failed or Cancelled
 *
 */
sealed class ViewModelActionResultType {
    object Success : ViewModelActionResultType()
    object Cancel : ViewModelActionResultType()
    data class Error(val message: String) : ViewModelActionResultType()
}

/**
 * Represent result of [ViewModelAction] and passed from [Fragment] to [DynamicViewModel]
 *
 * @param resultType show if result is success, failed or cancelled
 *
 */
sealed class ViewModelActionResult(open val resultType: ViewModelActionResultType) {
    data class ToggleEnablingResult(
        override val resultType: ViewModelActionResultType,
        val enable: Boolean
    ) : ViewModelActionResult(resultType)

    data class NetworkBoundToggleEnablingResult(
        override val resultType: ViewModelActionResultType,
        val enable: Boolean
    ) : ViewModelActionResult(resultType)
}

/**
 * Provide generic way to send actions from [ViewModel] to [Fragment] and get the result of that action
 *
 */
interface HasAction {
    /**
     * All the actions that [ViewModel] can send to Fragment
     */
    val actionSet: Set<ViewModelAction>

    /**
     * Fragment will observe this and [ViewModel] can send action to Fragment
     */
    val actionSignal: LiveData<ConsumableLiveEvent<ViewModelAction>>

    /**
     * Get called when [Fragment] send action result to ViewModel
     */
    fun onActionResult(viewModelActionResult: ViewModelActionResult)
}

/**
 * Find [ViewModelAction] from set by [ViewModelActionId]
 *
 * @param viewModelActionId
 *
 * @return found [ViewModelAction]
 */
fun Set<ViewModelAction>.getVmActionById(viewModelActionId: ViewModelActionId): ViewModelAction? {
    return this.find { it.viewModelActionId == viewModelActionId }
}

/**
 * Define map type to track all ViewModels which implements [HasAction] by [ViewModelAction]
 *
 */
typealias HasActionsMap = HashMap<ViewModelAction, HasAction>

/**
 * Provide to send [ViewModelActionResult] from [Fragment] to [ViewModel]
 *
 */
fun HasActionsMap.sendActionResult(viewModelActionId: ViewModelActionId, viewModelActionResult: ViewModelActionResult) {
    this[getVmAction(viewModelActionId)]?.onActionResult(viewModelActionResult)
}

/**
 * Find [ViewModelAction] from [HasActionsMap] keys by [ViewModelActionId]
 *
 */
fun HasActionsMap.getVmAction(viewModelActionId: ViewModelActionId): ViewModelAction? {
    return keys.find {
        it.viewModelActionId == viewModelActionId || it.viewModelResultActionId == viewModelActionId
    }
}
