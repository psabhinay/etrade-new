package com.etrade.mobilepro.order.details.api

import com.etrade.mobilepro.trade.api.option.TradeLeg

typealias OrderDetailDescriptionMapperType =
    (@JvmSuppressWildcards OrderDetailDescription, @JvmSuppressWildcards Boolean) -> (@JvmSuppressWildcards TradeLeg)
