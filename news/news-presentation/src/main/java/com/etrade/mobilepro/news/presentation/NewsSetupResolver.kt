package com.etrade.mobilepro.news.presentation

import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

interface NewsSetupResolver {
    fun showSearch(activity: FragmentActivity?, fragment: Fragment): Boolean
    fun hasOptionsMenu(detailsContainer: View?, parentFragment: Fragment?): Boolean
    fun shouldHaveOptionsMenuWithDetails(isDualPane: Boolean, parentFragment: Fragment?): Boolean
    fun containerShouldInflateCommonMenu(activity: FragmentActivity?): Boolean
}
