package com.etrade.mobilepro.formsandapplications.data

import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.formsandapplications.api.FormsAndApplicationsReference
import com.etrade.mobilepro.formsandapplications.api.FormsAndApplicationsRepository
import com.etrade.mobilepro.tracking.SCREEN_TITLE_DIVIDEND_REINVESTMENT
import com.etrade.mobilepro.tracking.SCREEN_TITLE_DOCUMENT_UPLOAD
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MARGIN_ACCOUNT_UPGRADE
import com.etrade.mobilepro.tracking.SCREEN_TITLE_OPTIONS_APPROVAL_UPGRADE_BROKERAGE
import com.etrade.mobilepro.tracking.SCREEN_TITLE_OPTIONS_APPROVAL_UPGRADE_IRA
import com.etrade.mobilepro.trade.accountupgrade.util.getApprovalLevelUpgradeUrl
import com.etrade.mobilepro.trade.accountupgrade.util.getMarginAccountUpgradeUrl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DefaultFormsAndApplicationsRepository @Inject constructor(@Web private val baseUrl: String) : FormsAndApplicationsRepository {

    @Suppress("MagicNumber", "LongMethod")
    override suspend fun getFormsAndApplications(): ETResult<List<FormsAndApplicationsReference>> {
        return withContext(Dispatchers.Unconfined) {
            ETResult.success(
                listOf(
                    FormsAndApplicationsReference(
                        R.string.forms_and_applications_options_ira,
                        R.string.forms_and_applications_options_ira_short,
                        getApprovalLevelUpgradeUrl(isIra = true),
                        SCREEN_TITLE_OPTIONS_APPROVAL_UPGRADE_IRA
                    ),
                    FormsAndApplicationsReference(
                        R.string.forms_and_applications_options_brokerage,
                        R.string.forms_and_applications_options_brokerage_short,
                        getApprovalLevelUpgradeUrl(isIra = false),
                        SCREEN_TITLE_OPTIONS_APPROVAL_UPGRADE_BROKERAGE
                    ),
                    FormsAndApplicationsReference(
                        R.string.forms_and_applications_margin_account_upgrade,
                        R.string.forms_and_applications_margin_account_upgrade_short,
                        getMarginAccountUpgradeUrl(),
                        SCREEN_TITLE_MARGIN_ACCOUNT_UPGRADE
                    ),
                    FormsAndApplicationsReference(
                        R.string.forms_and_applications_documents_upload,
                        R.string.forms_and_applications_documents_upload_short,
                        "$baseUrl/etx/hw/customerservice/uploaddoc#!/upload",
                        SCREEN_TITLE_DOCUMENT_UPLOAD
                    ),
                    FormsAndApplicationsReference(
                        R.string.forms_and_applications_dividend_reinvestment,
                        R.string.forms_and_applications_dividend_reinvestment_short,
                        "$baseUrl/etx/pw/v2/drips?ploc=2017-nav-Accounts",
                        SCREEN_TITLE_DIVIDEND_REINVESTMENT
                    )
                )
            )
        }
    }
}
