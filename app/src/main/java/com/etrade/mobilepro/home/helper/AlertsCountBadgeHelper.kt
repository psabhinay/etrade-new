package com.etrade.mobilepro.home.helper

import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.R
import com.etrade.mobilepro.home.model.AlertsCountViewModel
import com.etrade.mobilepro.ui.bottomnavigationview.BottomNavigationView
import com.etrade.mobilepro.util.android.extension.dpToPx
import javax.inject.Inject

private const val MAX_DISPLAYABLE_ALERT_COUNT = 300
private const val BADGE_VERTICAL_OFFSET_DP = 4

interface AlertsCountBadgeHelper {
    fun registerWith(lifecycleOwner: LifecycleOwner, bottomNavigationView: BottomNavigationView)
}

class DefaultAlertsCountBadgeHelper @Inject constructor(
    private val viewModel: AlertsCountViewModel
) : AlertsCountBadgeHelper {

    override fun registerWith(lifecycleOwner: LifecycleOwner, bottomNavigationView: BottomNavigationView) {
        viewModel.viewState.observe(
            lifecycleOwner,
            Observer {
                updateAlertsBadge(bottomNavigationView, it.count)
            }
        )
    }

    private fun updateAlertsBadge(bottomNavigationView: BottomNavigationView, count: Int) {
        if (count <= 0) {
            bottomNavigationView.removeBadge(R.id.menuFragment)
            return
        }
        with(bottomNavigationView.getOrCreateBadge(R.id.menuFragment)) {
            backgroundColor = ContextCompat.getColor(bottomNavigationView.context, R.color.red)
            badgeTextColor = ContextCompat.getColor(bottomNavigationView.context, R.color.badge_text_color)
            verticalOffset = bottomNavigationView.context.dpToPx(BADGE_VERTICAL_OFFSET_DP)
            number = minOf(count, MAX_DISPLAYABLE_ALERT_COUNT)
        }
    }
}
