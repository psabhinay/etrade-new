package com.etrade.mobilepro.markets.data

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.markets.data.response.Quote
import com.etrade.mobilepro.marketsapi.Index
import com.etrade.mobilepro.util.safeParseBigDecimal

internal fun Quote.toIndex(): Index? {
    val symbol = symbol
    val price = price?.safeParseBigDecimal()
    val symbolDesc = symbolDesc
    val change = change?.safeParseBigDecimal()
    val timestamp = timestamp
    val volume = volume?.safeParseBigDecimal()
    val percentChange = percentChange?.dropLast(1)?.safeParseBigDecimal()
        ?.let { MarketDataFormatter.formatMarketPercData(it) }
        ?.safeParseBigDecimal()

    if (price == null || volume == null) { return null }
    if (percentChange == null || change == null) { return null }
    if (symbol.isNullOrEmpty() || symbolDesc.isNullOrEmpty()) { return null }
    if (timestamp.isNullOrEmpty()) { return null }

    return Index(
        ticker = symbol,
        displayName = symbolDesc,
        price = price,
        volume = volume,
        change = change,
        percentChange = percentChange,
        instrumentType = InstrumentType.from(productType),
        timeStamp = timestamp
    )
}
