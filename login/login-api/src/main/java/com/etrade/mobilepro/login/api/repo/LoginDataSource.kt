package com.etrade.mobilepro.login.api.repo

interface LoginDataSource {
    fun getUrl(location: ExternalLocation): String

    enum class ExternalLocation {
        SIGN_UP, FORGOT_PWD, FORGOT_USERNAME, PRIVACY_POLICY
    }
}
