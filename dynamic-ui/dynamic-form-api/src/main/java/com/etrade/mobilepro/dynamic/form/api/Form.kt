package com.etrade.mobilepro.dynamic.form.api

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.MutableInputValuesSnapshot

class Form(
    val groups: List<Group>,
    val actions: List<Action>,
    private val hiddenInputs: List<HiddenInput>
) : BaseInputHolder(groups.mapInputs() + hiddenInputs.mapInputs()), InputValueHolder {

    override fun collectValuesTo(snapshot: MutableInputValuesSnapshot) {
        groups { collectValuesTo(snapshot) }
        hiddenInputs { parameterValue.value?.let { snapshot[id] = it } }
    }

    override fun injectValuesFrom(map: InputValuesSnapshot) {
        groups { injectValuesFrom(map) }
        hiddenInputs { map[id]?.let(::setParameterValue) }
    }

    override fun disabledFields(list: List<String>) = groups { disabledFields(list) }

    override fun resetValues() {
        groups { resetValues() }
        hiddenInputs { resetValue() }
    }

    private inline fun groups(block: Group.() -> Unit) = groups.forEach(block)

    private inline fun hiddenInputs(block: HiddenInput.() -> Unit) = hiddenInputs.forEach(block)
}

private fun Iterable<Group>.mapInputs(): Map<String, Input<Any>> {
    val map = mutableMapOf<String, Input<Any>>()
    forEach {
        map.putAll(it.inputMap)
    }
    return map.toMap()
}
