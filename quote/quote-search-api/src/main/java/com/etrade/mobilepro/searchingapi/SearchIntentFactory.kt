package com.etrade.mobilepro.searchingapi

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.core.app.ActivityOptionsCompat
import com.etrade.mobilepro.searchingapi.items.SearchResultItem

const val EXTRA_SELECTED_SYMBOL = "extra.selectedSymbol"

interface SearchIntentFactory {
    fun createIntent(context: Context, params: StartSearchParams, reorderToFront: Boolean = true): Intent
    fun createSearchIntentAndOptions(activity: Activity, params: StartSearchParams): Pair<Intent, ActivityOptionsCompat?>
}

fun symbolFromSearchResult(resultCode: Int, data: Intent?): SearchResultItem.Symbol? {
    return data?.takeIf { resultCode == Activity.RESULT_OK }?.searchResultSymbol
}

val Intent.searchResultSymbol: SearchResultItem.Symbol?
    get() = getParcelableExtra(EXTRA_SELECTED_SYMBOL)
