package com.etrade.mobilepro.util.android.binding

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.util.android.R
import com.etrade.mobilepro.util.android.accessibility.processAccessibilityStreamingEvent
import com.etrade.mobilepro.util.android.footnotespan.FootnoteSectionBuilder
import com.etrade.mobilepro.util.formatCurrencyAmount

const val QUOTE_ACCESSIBILITY_DEBOUNCE = 30000L

private const val NO_RESOURCE_COLOR = 0

@BindingAdapter("liveBackgroundColor")
fun View.liveBackgroundColor(@ColorRes color: Int) {
    val displayColor = if (color != NO_RESOURCE_COLOR) {
        color
    } else {
        android.R.color.transparent
    }
    setBackgroundColor(ContextCompat.getColor(context, displayColor))
}

@BindingAdapter("liveTextColor")
fun TextView.liveTextColor(@ColorRes color: Int) {
    val displayColor = if (color != NO_RESOURCE_COLOR) {
        color
    } else {
        R.color.primaryColor
    }
    setTextColor(ContextCompat.getColor(context, displayColor))
}

@BindingAdapter("showFirstPart")
fun TextView.showFirstPart(value: String?) {
    value?.split("\\s".toRegex(), 2)?.let {
        text = it[0]
        contentDescription = it[0].formatCurrencyAmount()
    }
}

@BindingAdapter("showSecondPart")
fun TextView.showSecondPart(value: String?) {
    value?.split("\\s".toRegex(), 2)?.let {
        if (it.size > 1) {
            text = it[1]
            visibility = View.VISIBLE
        } else {
            visibility = View.GONE
        }
    }
}

@BindingAdapter("headerLiveTextColor")
fun TextView.headerLiveTextColor(@ColorRes color: Int) {
    val displayColor = when (color) {
        R.color.green -> R.color.green_light
        R.color.red -> R.color.red_light
        else -> R.color.white_non_inversion
    }
    setTextColor(ContextCompat.getColor(context, displayColor))
}

@BindingAdapter("processContentDescription")
fun View.processContentDescription(@Suppress("UNUSED_PARAMETER") streamedValue: Any? = null) {
    contentDescription?.let { processAccessibilityStreamingEvent(it.toString()) }
}

@BindingAdapter("processQuoteContentDescription")
fun View.processQuoteContentDescription(contentDescription: String?) {
    contentDescription?.let { processAccessibilityStreamingEvent(accessibilityText = it, debounce = QUOTE_ACCESSIBILITY_DEBOUNCE) }
}

@BindingAdapter("footnotes")
fun TextView.setFootnotes(footnotesBuilder: FootnoteSectionBuilder?) {
    text = footnotesBuilder?.build(paint)
}

@BindingAdapter("visibleIfDataIsNotNull")
fun setVisibilityNotNull(v: View, data: Any?) {
    v.visibility = if (data != null) {
        View.VISIBLE
    } else {
        View.GONE
    }
}

@BindingAdapter("visibleIfDataIsNull")
fun setVisibilityNull(v: View, data: Any?) {
    v.visibility = if (data != null) {
        View.GONE
    } else {
        View.VISIBLE
    }
}

@BindingAdapter("android:src")
fun ImageView.setImage(@DrawableRes imageResId: Int) {
    setImageResource(imageResId)
}
