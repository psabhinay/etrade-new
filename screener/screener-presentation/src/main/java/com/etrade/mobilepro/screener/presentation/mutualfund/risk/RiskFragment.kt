package com.etrade.mobilepro.screener.presentation.mutualfund.risk

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.mutualfund.RiskFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.FragmentRiskBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class RiskFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<RiskFilter, RiskViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {

    private val binding by viewBinding(FragmentRiskBinding::bind)
    override val layoutRes: Int = R.layout.fragment_risk
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND
    override val viewModel by viewModels<RiskViewModel> { viewModelFactory }
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<RiskFilter, RiskViewModel.ViewState, Boolean>>
        get() = mapOf(
            binding.lessVolatile to ThreeYearBeta.LESS_VOLATILE,
            binding.asVolatile to ThreeYearBeta.AS_VOLATILE,
            binding.moreVolatile to ThreeYearBeta.MORE_VOLATILE,
            binding.bottomTwentyPercent to StandardDeviation.BOTTOM_20,
            binding.belowMidTwentyPercent to StandardDeviation.BELOW_MID_20,
            binding.midTwentyPercent to StandardDeviation.MID_20,
            binding.aboveMidTwentyPercent to StandardDeviation.ABOVE_MID_20,
            binding.topTwentyPercent to StandardDeviation.TOP_20
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                it.filter.apply {
                    binding.lessVolatile.isChecked = lessVolatile
                    binding.asVolatile.isChecked = asVolatile
                    binding.moreVolatile.isChecked = moreVolatile
                    binding.bottomTwentyPercent.isChecked = bottom20
                    binding.belowMidTwentyPercent.isChecked = belowMid20
                    binding.midTwentyPercent.isChecked = mid20
                    binding.aboveMidTwentyPercent.isChecked = aboveMid20
                    binding.topTwentyPercent.isChecked = top20
                }
            }
        )
    }
}
