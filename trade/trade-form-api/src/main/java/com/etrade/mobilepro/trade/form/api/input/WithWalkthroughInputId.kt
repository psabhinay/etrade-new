package com.etrade.mobilepro.trade.form.api.input

interface WithWalkthroughInputId {
    val walkthroughFormCompleted: String
}
