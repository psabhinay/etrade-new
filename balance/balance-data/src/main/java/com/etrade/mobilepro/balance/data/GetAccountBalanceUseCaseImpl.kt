package com.etrade.mobilepro.balance.data

import com.etrade.mobilepro.balance.api.AccountBalance
import com.etrade.mobilepro.balance.api.GetAccountBalanceParameter
import com.etrade.mobilepro.balance.api.GetAccountBalanceUseCase
import com.etrade.mobilepro.balance.data.repo.AccountBalancesRepo
import com.etrade.mobilepro.balance.data.repo.BalanceRequest
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.session.api.User
import kotlinx.coroutines.rx2.awaitLast
import javax.inject.Inject

private const val CACHE_EXPIRATION_TIME = 15000L

class GetAccountBalanceUseCaseImpl @Inject constructor(
    private val accountBalancesRepo: AccountBalancesRepo,
    private val user: User
) : GetAccountBalanceUseCase {

    override suspend fun execute(parameter: GetAccountBalanceParameter): ETResult<AccountBalance> {
        return accountBalancesRepo.runCatchingET {
            val userId = user.getUserId()?.toString()!! // intentional NPE
            getBalances(
                request = BalanceRequest(parameter.accountId, userId),
                cacheExpiration = CACHE_EXPIRATION_TIME.takeUnless { parameter.forceRefresh }
            ).awaitLast()
                ?.data
                ?.balances
                ?.balanceRealTime!!
        }
    }
}
