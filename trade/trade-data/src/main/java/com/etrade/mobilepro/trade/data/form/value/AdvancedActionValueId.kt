package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.util.MutualConverter

internal object AdvancedActionValueId : ActionValueId {
    override val buy: String = TransactionType.BUY_OPEN.typeCode.toString()
    override val sell: String = TransactionType.SELL_CLOSE.typeCode.toString()
    override val sellShort: String = TransactionType.SELL_OPEN.typeCode.toString()
    override val buyToCover: String = TransactionType.BUY_CLOSE.typeCode.toString()
    override val exchange: String = ""
    override val converter: MutualConverter<String, TradeAction> by lazy { createConverter() }
}

internal object AdvancedOrderOptionsActionValueId : ActionValueId {
    override val buy: String = "1"
    override val buyToCover: String = "3"
    override val exchange: String = ""
    override val sell: String = "4"
    override val sellShort: String = "2"
    override val converter: MutualConverter<String, TradeAction> by lazy { createConverter() }
}
