package com.etrade.mobilepro.usersubscriptiondata

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SubscriptionResponseDto(
    @Json(name = "smUser")
    val smUser: String? = null,
    @Json(name = "sysdate")
    val sysdate: String? = null,
    @Json(name = "data")
    val data: SubscriptionDataDto? = null
)

@JsonClass(generateAdapter = true)
data class SubscriptionDataDto(
    @Json(name = "userSubscriptionResponse")
    val userResponse: UserSubscriptionDto? = null
) : BaseDataDto()

@JsonClass(generateAdapter = true)
data class UserSubscriptionDto(
    @Json(name = "goodBalance")
    val goodBalance: Boolean? = null,
    @Json(name = "toolList")
    val toolList: List<UserSubscriptionToolListDto>? = emptyList(),
    @Json(name = "billingAccount")
    val billingAccount: String? = null,
    @Json(name = "accountList")
    val accountList: List<UserSubscriptionAccountListDto>? = emptyList(),
    @Json(name = "newCustomer")
    val newCustomer: Boolean? = null,
    @Json(name = "waived")
    val waived: Boolean? = null
)

@JsonClass(generateAdapter = true)
data class UserSubscriptionToolListDto(
    @Json(name = "toolSetId")
    val toolSetId: String? = null,
    @Json(name = "toolSetName")
    val toolSetName: String? = null,
    @Json(name = "feeType")
    val feeType: String? = null,
    @Json(name = "classification")
    val classification: String? = null,
    @Json(name = "fees")
    val fees: String? = null,
    @Json(name = "prereqNotMet")
    val prereqNotMet: Boolean? = null,
    @Json(name = "prereqToolSetId")
    val prereqToolSetId: String? = null,
    @Json(name = "eligible")
    val eligible: Boolean? = null
)

@JsonClass(generateAdapter = true)
data class UserSubscriptionAccountListDto(
    @Json(name = "accountId")
    val accountId: String? = null,
    @Json(name = "displayName")
    val displayName: String? = null,
    @Json(name = "balance")
    val balance: Double? = null
)
