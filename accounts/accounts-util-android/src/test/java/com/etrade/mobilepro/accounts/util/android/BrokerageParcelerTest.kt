package com.etrade.mobilepro.accounts.util.android

import android.os.Parcel
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobile.accounts.details.isEquals
import org.junit.Test
import org.junit.runner.RunWith
import java.math.BigDecimal

@RunWith(AndroidJUnit4::class)
class BrokerageParcelerTest {

    @Test
    internal fun `data A can be serialized and deserialized`() {
        testData(dataA)
    }

    @Test
    internal fun `data B can be serialized and deserialized`() {
        testData(dataB)
    }

    private fun testData(data: AccountDetails.Brokerage) {
        val parcel = Parcel.obtain()
        BrokerageParceler.run { data.write(parcel, 0) }
        parcel.setDataPosition(0)
        val result = BrokerageParceler.create(parcel)
        assert(data.isEquals(result))
    }

    private val dataA = object : AccountDetails.Brokerage {
        override val cashAvailableForWithdrawal: BigDecimal? = BigDecimal(10)
        override val marginAvailableForWithdrawal: BigDecimal? = BigDecimal(20)
        override val purchasingPower: BigDecimal? = BigDecimal(30)
        override val totalAvailableForWithdrawal: BigDecimal? = BigDecimal(40)
        override val marginBalance: BigDecimal? = BigDecimal(50)
        override val netCashBalance: BigDecimal? = BigDecimal(60)
        override val cmAccount: Boolean? = true
        override val contributionDetails: List<AccountDetails.ContributionDetails> = listOf(
            object : AccountDetails.ContributionDetails {
                override val contributionAmount: BigDecimal? = BigDecimal(10)
                override val taxYear: Int = 20
            },
            object : AccountDetails.ContributionDetails {
                override val contributionAmount: BigDecimal? = BigDecimal(30)
                override val taxYear: Int = 40
            }
        )
    }

    private val dataB = object : AccountDetails.Brokerage {
        override val cashAvailableForWithdrawal: BigDecimal? = null
        override val marginAvailableForWithdrawal: BigDecimal? = null
        override val purchasingPower: BigDecimal? = BigDecimal(30)
        override val totalAvailableForWithdrawal: BigDecimal? = null
        override val marginBalance: BigDecimal? = BigDecimal(50)
        override val netCashBalance: BigDecimal? = null
        override val cmAccount: Boolean? = null
        override val contributionDetails: List<AccountDetails.ContributionDetails> = emptyList()
    }
}
