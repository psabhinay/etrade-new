package com.etrade.mobilepro.onboarding

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class LocalyticsOnboardingViewModel @Inject constructor(
    private val messageRepo: InboxMessagesRepository
) : ViewModel() {

    fun triggerOnboardingMessage(inAppMessageClose: () -> Unit) {
        viewModelScope.launch {
            messageRepo.triggerMessage("Onboarding", inAppMessageClose)
        }
    }
}
