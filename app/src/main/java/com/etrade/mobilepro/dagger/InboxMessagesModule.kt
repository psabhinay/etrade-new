package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.fundingprompt.DefaultFundingPromptNavHelper
import com.etrade.mobilepro.inboxmessages.dynamicui.api.DynamicScreenContentUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.data.DefaultDynamicScreenContentUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.data.WidgetListUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.data.bannerwidget.CompleteViewBannerWidgetListUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.data.fundingprompt.CompleteViewScreenFundingPromptWidgetListUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.data.fundingprompt.DefaultFundingPromptDtoFactory
import com.etrade.mobilepro.inboxmessages.dynamicui.data.fundingprompt.FundingPromptDtoFactory
import com.etrade.mobilepro.inboxmessages.dynamicui.data.fundingprompt.OverviewScreenFundingPromptWidgetListUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.data.lowcash.CompleteViewScreenLowCashWidgetListUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.data.lowcash.DefaultLowCashPromptFactory
import com.etrade.mobilepro.inboxmessages.dynamicui.data.lowcash.LowCashPromptFactory
import com.etrade.mobilepro.inboxmessages.dynamicui.data.lowcash.OverviewScreenLowCashWidgetListUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt.FundingPromptNavHelper
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet
import javax.inject.Singleton

@Module
interface InboxMessagesModule {

    @Binds
    @IntoSet
    fun bindCompleteViewWidgetListUpdater(updater: CompleteViewScreenFundingPromptWidgetListUpdater): WidgetListUpdater

    @Binds
    @IntoSet
    fun bindOverviewWidgetListUpdater(updater: OverviewScreenFundingPromptWidgetListUpdater): WidgetListUpdater

    @Binds
    @IntoSet
    fun bindCompleteViewWidgetListLowCashUpdater(updater: CompleteViewScreenLowCashWidgetListUpdater): WidgetListUpdater

    @Binds
    @IntoSet
    fun bindOverviewWidgetListLowCashUpdater(updater: OverviewScreenLowCashWidgetListUpdater): WidgetListUpdater

    @Binds
    @IntoSet
    fun bindCompleteViewBannerWidgetListUpdater(updater: CompleteViewBannerWidgetListUpdater): WidgetListUpdater

    @Binds
    fun bindDynamicScreenContentUpdater(updater: DefaultDynamicScreenContentUpdater): DynamicScreenContentUpdater

    @Binds
    fun bindFundingPromptFactory(factory: DefaultFundingPromptDtoFactory): FundingPromptDtoFactory

    @Binds
    fun bindLowCashPromptFactory(factory: DefaultLowCashPromptFactory): LowCashPromptFactory

    @Binds
    @Singleton
    fun bindFundingPromptNavHelper(navHelper: DefaultFundingPromptNavHelper): FundingPromptNavHelper
}
