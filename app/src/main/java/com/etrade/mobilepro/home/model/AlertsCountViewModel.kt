package com.etrade.mobilepro.home.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.user.session.manager.SessionState
import com.etrade.mobilepro.user.session.manager.SessionStateChangeListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class AlertsCountViewModel @Inject constructor(
    private val alertsRepo: AlertsRepo,
    private val sessionState: SessionState
) : ViewModel(), SessionStateChangeListener {
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)
    private val compositeDisposable = CompositeDisposable()
    private val _viewState = MutableLiveData<ViewState>().apply { value = ViewState() }
    val viewState: LiveData<ViewState> = _viewState

    init {
        sessionState.addListener(this)
    }

    public override fun onCleared() {
        compositeDisposable.clear()
        sessionState.removeListener(this)
    }

    override fun onSessionStateChange(state: SessionStateChange) {
        when (state) {
            SessionStateChange.UNAUTHENTICATED -> reset()
            SessionStateChange.AUTHENTICATED -> updateAlertsCount()
        }
    }

    fun updateAlertsCount() {
        compositeDisposable.add(
            alertsRepo
                .loadAlertsCount()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = { count ->
                        _viewState.value = ViewState(count)
                    },
                    onError = {
                        logger.error(it.message)
                    }
                )
        )
    }

    fun getUserAlertsCount(): Int = _viewState.value?.count ?: 0

    fun reset() {
        compositeDisposable.clear()
        _viewState.value = ViewState()
    }

    data class ViewState(
        val count: Int = 0
    )
}
