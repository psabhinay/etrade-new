package com.etrade.mobilepro.portfolio.extendedsummary

import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.backends.mgs.StreamingValueDto
import com.etrade.mobilepro.completeview.getStreamingText
import com.etrade.mobilepro.portfolio.model.PortfolioExtendedSummaryModel
import com.etrade.mobilepro.portfolio.model.dto.ParcelablePortfolioSummaryItemDto
import com.etrade.mobilepro.streaming.api.StreamingText
import com.etrade.mobilepro.streaming.api.StreamingTextManager

internal fun PortfolioExtendedSummaryModel.toAccountSummarySections(
    streamingTextManager: StreamingTextManager
): List<StreamableSection> {
    return this.extraSummarySections.map {
        StreamableSection(
            it.title,
            getStreamingText(
                streamingTextManager,
                it.toStreamingValueDto(),
                isAccountStreamingAllowed = isStreamingAllowed
            )
        )
    }
}

internal fun ParcelablePortfolioSummaryItemDto.toStreamingValueDto(): StreamingValueDto =
    StreamingValueDto(
        initial = initialValue,
        streamId = streamId,
        movementType = movementType
    )

@BindingAdapter("portfolioStreamingText")
fun bindPortfolioStreamingText(view: TextView, textDto: LiveData<StreamingText>?) {
    textDto?.let {
        view.text = it.value?.text
        it.value?.color?.let { color ->
            view.setTextColor(ContextCompat.getColor(view.context, color))
        }
    }
}
