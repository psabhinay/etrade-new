package com.etrade.mobilepro.toolbar

import android.content.res.Resources
import android.text.SpannableString
import android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE
import android.text.TextUtils
import android.text.style.AbsoluteSizeSpan
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.R
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.invisibleIf
import javax.inject.Inject

typealias ToolbarSignal = (Toolbar) -> Unit

interface ToolbarUpdater {
    val toolbarSignal: LiveData<ConsumableLiveEvent<ToolbarSignal>>

    val isShowingTitle: Boolean
}

/**
 * Toolbar controller component to update the Account display name or WatchList display name
 */
class ToolBarController @Inject constructor() : ToolbarUpdater {

    override val toolbarSignal: LiveData<ConsumableLiveEvent<ToolbarSignal>>
        get() = _toolbarSignal

    override val isShowingTitle: Boolean
        get() = toolbarData.showTitle

    private val _toolbarSignal = MutableLiveData<ConsumableLiveEvent<ToolbarSignal>>()

    private var toolbarData: ToolbarDataModel = ToolbarDataModel()

    fun updateTitle(
        titleText: String? = toolbarData.titleText,
        showTitle: Boolean = toolbarData.showTitle
    ) {
        toolbarData = ToolbarDataModel(titleText, showTitle)
        onToolbarUpdate()
    }

    private fun onToolbarUpdate() {
        _toolbarSignal.value = ConsumableLiveEvent { toolbar ->
            toolbarData.let {
                val toolbarTitleTextView = toolbar.findViewById<TextView>(R.id.toolbarTitle)
                toolbarTitleTextView.apply {
                    text = it.getToolbarContent(resources)
                    val linesToSet = if (it.showTitle) 2 else 1
                    if (lineCount != linesToSet) {
                        setLines(linesToSet)
                    }
                    invisibleIf(text.isNullOrBlank())
                }
            }
        }
    }

    private fun ToolbarDataModel.getToolbarContent(resources: Resources): CharSequence? {
        if (!showTitle) {
            return null
        }
        val netAssetsLabel =
            resources.getString(R.string.complete_view_net_assets_label)
        val quantityLabel = titleText ?: ""
        val netAssetsSpannable = SpannableString(netAssetsLabel)
        netAssetsSpannable.setSpan(
            AbsoluteSizeSpan(resources.getDimensionPixelSize(R.dimen.font_size_xsmall)),
            0,
            netAssetsLabel.length,
            SPAN_INCLUSIVE_INCLUSIVE
        )
        val quantitySpannable = SpannableString(quantityLabel)
        quantitySpannable.setSpan(
            AbsoluteSizeSpan(resources.getDimensionPixelSize(R.dimen.font_size_medium)),
            0,
            quantityLabel.length,
            SPAN_INCLUSIVE_INCLUSIVE
        )
        return TextUtils.concat(netAssetsSpannable, "\n", quantitySpannable)
    }
}
