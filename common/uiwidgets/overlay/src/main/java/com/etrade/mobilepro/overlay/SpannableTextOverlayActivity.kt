package com.etrade.mobilepro.overlay

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.baseactivity.ActivitySessionDelegate
import com.etrade.mobilepro.common.getNavigationButtonDescription
import com.etrade.mobilepro.common.getToolbarTitleContentDescription
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.common.toolbarView
import com.etrade.mobilepro.overlay.databinding.ActivitySpannableTextOverlayBinding
import com.etrade.mobilepro.util.android.accessibility.populateAccessibilityEvent
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.restoreTextViewScrollPosition
import com.etrade.mobilepro.util.android.extension.saveLineStartOffset
import com.etrade.mobilepro.util.android.textutil.TextUtil
import dagger.android.AndroidInjection
import javax.inject.Inject

const val SPANNABLE_TEXT_OVERLAY_REQUEST_CODE_WITH_POSITIVE = 422

open class SpannableTextOverlayActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivitySpannableTextOverlayBinding::inflate)

    @Inject
    lateinit var activitySessionDelegate: ActivitySessionDelegate

    private var positive: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(binding.root)

        setSupportActionBar(toolbarView)

        var title: String?
        var text: String?
        @DrawableRes var negative: Int?

        intent.apply {
            title = getStringExtra(EXTRA_TITLE)
            text = getStringExtra(EXTRA_TEXT)
            negative = getIntExtra(EXTRA_NEGATIVE, 0)
            positive = getStringExtra(EXTRA_POSITIVE)
        }

        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
            setDisplayHomeAsUpEnabled(true)
            negative?.also {
                setHomeAsUpIndicator(it)
                setHomeActionContentDescription(getNavigationButtonDescription(it))
            }
            positive?.also { toolbarActionEndView?.setUpPositiveToolbarAction(it) }
        }

        toolbarTitleView?.text = title
        title?.let {
            toolbarTitleView?.contentDescription = getToolbarTitleContentDescription(it, this)
        }

        with(binding) {
            Linkify.addLinks(contentText, Linkify.ALL)

            contentText.populateAccessibilityEvent {
                isClickable = false
                isLongClickable = false
            }
            contentText.movementMethod = LinkMovementMethod.getInstance()
            contentText.text = text?.let { TextUtil.convertTextWithHmlTagsToSpannable(it) }
        }

        activitySessionDelegate.observeTimeoutEvent(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        activitySessionDelegate.onUserInteraction(this)
    }

    companion object {
        const val EXTRA_TITLE = "EXTRA_TITLE"
        const val EXTRA_TEXT = "EXTRA_TEXT"
        private const val EXTRA_NEGATIVE = "EXTRA_ICON"
        private const val EXTRA_POSITIVE = "EXTRA_POSITIVE"

        @Suppress("LongParameterList")
        fun intent(
            context: Context,
            title: String,
            text: String,
            @DrawableRes negative: Int? = null,
            positive: String? = null
        ): Intent {
            val intent = Intent(context, SpannableTextOverlayActivity::class.java)

            return intent.apply {
                putExtra(EXTRA_TITLE, title)
                putExtra(EXTRA_TEXT, text)
                putExtra(EXTRA_NEGATIVE, negative)
                putExtra(EXTRA_POSITIVE, positive)
            }
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        binding.scrollView.restoreTextViewScrollPosition(binding.contentText, savedInstanceState)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        binding.scrollView.saveLineStartOffset(binding.contentText, outState)
    }

    private fun TextView.setUpPositiveToolbarAction(actionText: String) {
        setToolbarActionTextAndContentDescription(actionText)
        visibility = View.VISIBLE
        setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }
}
