package com.etrade.mobilepro.appwidget.widgetcontainer

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.Context
import android.view.View
import android.widget.RemoteViews
import com.etrade.mobilepro.appwidget.R
import com.etrade.mobilepro.appwidget.api.IndicesViewState
import com.etrade.mobilepro.appwidget.api.WatchlistEntryTuple
import com.etrade.mobilepro.appwidget.api.isUninitialized
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettingsRepo
import com.etrade.mobilepro.appwidget.common.getBroadcastIntentWith
import com.etrade.mobilepro.appwidget.common.getLayoutResId
import com.etrade.mobilepro.appwidget.indices.getWidgetIndicesRefreshPendingIntent
import com.etrade.mobilepro.marketsapi.Index
import javax.inject.Inject

class WidgetViewStateHandler @Inject constructor(
    private val context: Context,
    private val widgetSettingsRepo: WidgetSettingsRepo
) {
    private val appWidgetManager: AppWidgetManager = AppWidgetManager.getInstance(context)

    internal fun initViewState(appWidgetIds: IntArray) {
        handleViewState(IndicesViewState(), appWidgetIds)
    }

    internal fun handleViewState(viewState: IndicesViewState, appWidgetIds: IntArray) {

        val widgetSettings = widgetSettingsRepo.loadSettingsBlocking(viewState.widgetId)

        val remoteViews =
            if (viewState.isUninitialized) {
                WidgetRemoteViewsFactory(context, widgetSettings).create()
            } else {
                RemoteViews(context.packageName, R.layout.appwidget_widget_layout)
            }

        if (viewState.isLoading || viewState.isUninitialized) {
            remoteViews.setViewVisibility(R.id.pbIndices, View.VISIBLE)
            remoteViews.setViewVisibility(R.id.lvIndices, View.INVISIBLE)
            remoteViews.setViewVisibility(R.id.emptyIndices, View.INVISIBLE)
            remoteViews.handleWatchlistLayoutVisibility(widgetSettings.layout, View.INVISIBLE)
            remoteViews.setViewVisibility(R.id.emptyWatchlist, View.INVISIBLE)
        } else {
            remoteViews.setViewVisibility(R.id.pbIndices, View.INVISIBLE)
            remoteViews.handleIndices(viewState.indices, appWidgetIds)
            remoteViews.handleWatchlistEntries(viewState.watchlistEntryTuples, widgetSettings.layout, appWidgetIds)
            remoteViews.handleTimestamp(viewState.formattedTimestamp)
        }

        remoteViews.setupClickListeners(viewState)

        if (viewState.isUninitialized) {
            appWidgetManager.updateAppWidget(appWidgetIds, remoteViews)
            scheduleWidgetRefresh(context, widgetSettings)
        } else {
            appWidgetManager.partiallyUpdateAppWidget(appWidgetIds, remoteViews)
        }
    }

    private fun RemoteViews.setupClickListeners(viewState: IndicesViewState) {
        setOnClickPendingIntent(R.id.ivRefresh, getWidgetIndicesRefreshPendingIntent(context))
        viewState.watchlistId?.let {
            setOnClickPendingIntent(R.id.tvViewAll, getPendingIntentToOpenWatchlist(it))
        }
        setOnClickPendingIntent(R.id.ivSettings, getPendingIntentToOpenWidgetSettings(viewState.widgetId.id))
        setOnClickPendingIntent(R.id.ivAdd, getPendingIntentToAddSymbolToWatchlist())
        setOnClickPendingIntent(R.id.logo_iv, getPendingIntentToAccountsPage())
    }

    private fun RemoteViews.handleIndices(indices: List<Index>?, appWidgetIds: IntArray) {
        if (indices.isNullOrEmpty()) {
            setViewVisibility(R.id.emptyIndices, View.VISIBLE)
            setViewVisibility(R.id.lvIndices, View.INVISIBLE)
        } else {
            setViewVisibility(R.id.emptyIndices, View.INVISIBLE)
            setViewVisibility(R.id.lvIndices, View.VISIBLE)
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.lvIndices)
        }
    }

    private fun RemoteViews.handleWatchlistEntries(
        watchlistEntryTuples: List<WatchlistEntryTuple>?,
        layoutSetting: WidgetSettings.Layout,
        appWidgetIds: IntArray,
    ) {
        if (watchlistEntryTuples.isNullOrEmpty()) {
            setViewVisibility(R.id.emptyWatchlist, View.VISIBLE)
            handleWatchlistLayoutVisibility(layoutSetting, View.INVISIBLE)
        } else {
            setViewVisibility(R.id.emptyWatchlist, View.INVISIBLE)
            handleWatchlistLayoutVisibility(layoutSetting, View.VISIBLE)
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, getLayoutResId(layoutSetting))
        }
    }

    private fun RemoteViews.handleWatchlistLayoutVisibility(layoutSetting: WidgetSettings.Layout, visibility: Int) {
        when (layoutSetting) {
            WidgetSettings.Layout.TILES -> {
                setViewVisibility(R.id.gvWatchlist, visibility)
                setViewVisibility(R.id.lvWatchlist, View.GONE)
            }
            WidgetSettings.Layout.LIST -> {
                setViewVisibility(R.id.lvWatchlist, visibility)
                setViewVisibility(R.id.gvWatchlist, View.GONE)
            }
        }
    }

    private fun RemoteViews.handleTimestamp(timestamp: String?) {
        timestamp?.let {
            setTextViewText(R.id.tvTimestamp, it)
        }
    }

    private fun getPendingIntentToOpenWatchlist(selectedWatchlistId: String): PendingIntent =
        context.getBroadcastIntentWith {
            action = ACTION_OPEN_WATCHLIST
            putExtra(EXTRA_WATCHLIST_ID, selectedWatchlistId)
        }

    private fun getPendingIntentToOpenWidgetSettings(widgetId: Int): PendingIntent =
        context.getBroadcastIntentWith {
            action = ACTION_OPEN_WIDGET_SETTINGS
            putExtra(ACTION_OPEN_WIDGET_SETTINGS, widgetId)
        }

    private fun getPendingIntentToAddSymbolToWatchlist(): PendingIntent =
        context.getBroadcastIntentWith {
            action = ACTION_ADD_SYMBOL_TO_WATCHLIST
        }

    private fun getPendingIntentToAccountsPage(): PendingIntent =
        context.getBroadcastIntentWith {
            action = ACTION_OPEN_ACCOUNTS
        }
}
