package com.etrade.mobilepro.bank.transactions

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.transactions.Transaction
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

private val LOCALDATE_DISPLAYED_DATE_FORMAT: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yy")
private const val IN_PROCESS_LABEL = "In Process"

class BankTransactionGenericLayout(
    private val data: BankTransactionDataDto,
    private val emptyValue: String,
    private val emptyAmountDescription: String,
    private val emptyDate: String
) : GenericLayout {

    private var inProcessLabel: String? = null

    val transactionDate: LocalDate? by lazy {
        data.transactionDate?.let {
            if (it.trim() == IN_PROCESS_LABEL) {
                inProcessLabel = IN_PROCESS_LABEL
                null
            } else {
                LocalDate.parse(it, LOCALDATE_DISPLAYED_DATE_FORMAT)
            }
        }
    }

    override val viewModelFactory = viewModelFactory {
        val amount = data.transactionAmount.toString()
        val formattedAmount = getFormattedAmount(amount)
        BankTransactionViewModel(
            Transaction.create(
                data.transactionType ?: emptyValue,
                data.transactionDescription ?: emptyValue,
                amount,
                formattedAmount,
                emptyAmountDescription,
                transactionDate?.format(createAmericanSimpleDateFormatter()) ?: inProcessLabel ?: emptyDate
            )
        )
    }

    override val viewModelClass = BankTransactionViewModel::class.java

    override val uniqueIdentifier: String = "${javaClass.name}_$data"

    private fun getFormattedAmount(amount: String?) = MarketDataFormatter.formatMoneyValueWithCurrency(amount?.toBigDecimalOrNull())
}
