package com.etrade.mobilepro.biometric.presentation

import android.os.Bundle
import androidx.biometric.BiometricPrompt
import androidx.biometric.BiometricPrompt.AuthenticationResult
import androidx.biometric.BiometricPrompt.CryptoObject
import androidx.biometric.BiometricPrompt.ERROR_CANCELED
import androidx.biometric.BiometricPrompt.ERROR_HW_UNAVAILABLE
import androidx.biometric.BiometricPrompt.ERROR_LOCKOUT
import androidx.biometric.BiometricPrompt.ERROR_LOCKOUT_PERMANENT
import androidx.biometric.BiometricPrompt.ERROR_NEGATIVE_BUTTON
import androidx.biometric.BiometricPrompt.ERROR_NO_BIOMETRICS
import androidx.biometric.BiometricPrompt.ERROR_USER_CANCELED
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import org.slf4j.LoggerFactory
import javax.crypto.Cipher
import javax.inject.Inject

private const val ERROR_MESSAGE_TAG = "ERROR_MESSAGE"
private const val BIOMETRIC_GENERAL_ERROR_CODE = 1001

private const val INVOKE_ON_FAIL = 1
private const val INVOKE_ON_CANCEL = 2

private const val USER_INITIATED_KEY = "isUserInitiated"
private val logger = LoggerFactory.getLogger(BiometricAuthenticationFragment::class.java)

class BiometricAuthenticationFragment @Inject constructor() : Fragment(), BiometricAuthenticator {

    override var onAuthenticationSuccess: ((Cipher) -> Unit)? = null
    override var onAuthenticationFail: ((reason: Int?) -> Unit)? = null
    override var onAuthenticationCancel: (() -> Unit)? = null

    var doFinally: (() -> Unit)? = null

    private lateinit var biometricPrompt: BiometricPrompt

    private var isCancelHandled = false

    private var isUserInitiated: Boolean = BiometricAuthenticator.USER_INITIATED_BY_DEFAULT

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initPrompt()

        savedInstanceState?.run {
            isUserInitiated = getBoolean(USER_INITIATED_KEY, BiometricAuthenticator.USER_INITIATED_BY_DEFAULT)
        }
        setupDialogResults()
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (val requestCode = bundle.customDialogRequestCode) {
                ERROR_LOCKOUT_PERMANENT,
                ERROR_LOCKOUT,
                ERROR_NO_BIOMETRICS,
                BIOMETRIC_GENERAL_ERROR_CODE -> {
                    invokeCallback(INVOKE_ON_FAIL, requestCode)
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(USER_INITIATED_KEY, isUserInitiated)
    }

    override fun authenticate(cipher: Cipher, type: BiometricPromptType, isUserInitiated: Boolean) {
        if (isStateSaved) {
            onAuthenticationCancel?.invoke()
            return
        }

        this.isUserInitiated = isUserInitiated

        val promptInfo = resources.getBiometricPromptInfo(type)
        val cryptoObject = CryptoObject(cipher)

        biometricPrompt.authenticate(promptInfo, cryptoObject)
    }

    private fun initPrompt() {
        biometricPrompt = BiometricPrompt(
            this, requireActivity()::runOnUiThread,
            object : BiometricPrompt.AuthenticationCallback() {

                override fun onAuthenticationSucceeded(result: AuthenticationResult) {
                    val cipher = requireNotNull(result.cryptoObject?.cipher) {
                        "BiometricPrompt.AuthenticationResult misses crypto object."
                    }
                    onAuthenticationSuccess(cipher)
                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    logger.debug("onAuthenticationError errorCode:$errorCode")
                    if (!isCancelHandled) {
                        handleErrorCode(errorCode, errString)
                    }
                }

                private fun handleErrorCode(errorCode: Int, errString: CharSequence) {
                    logger.debug("handleErrorCode errorCode:$errorCode")
                    when (errorCode) {
                        ERROR_HW_UNAVAILABLE -> {
                            /*
                            Silently fail. When not user initiated, this is something like from a
                             lock screen.
                             */
                        }
                        ERROR_CANCELED,
                        ERROR_NEGATIVE_BUTTON,
                        ERROR_USER_CANCELED -> {
                            invokeCallback(INVOKE_ON_CANCEL, errorCode)
                        }
                        ERROR_LOCKOUT_PERMANENT -> {
                            showErrorMessage(errorCode, getString(R.string.biometrics_permanent_lockout_dialog_message))
                        }
                        ERROR_LOCKOUT -> {
                            showErrorMessage(errorCode, getString(R.string.biometrics_temporary_lockout_dialog_message))
                        }
                        ERROR_NO_BIOMETRICS -> {
                            showErrorMessage(errorCode, getString(R.string.biometric_missing_enrollment))
                        }
                        else -> showErrorMessage(BIOMETRIC_GENERAL_ERROR_CODE, errString.toString())
                    }
                    isCancelHandled = true
                }
            }
        )
    }

    private fun showErrorMessage(errorCode: Int, errorMessage: String) {
        if (isUserInitiated) {
            val dialog = CustomDialogFragment.newInstance(
                message = errorMessage,
                okTitle = getString(android.R.string.ok),
                requestCode = errorCode
            )
            dialog.show(parentFragmentManager, ERROR_MESSAGE_TAG)
        } else {
            invokeCallback(errorCode)
        }
    }

    private fun onAuthenticationSuccess(cipher: Cipher) {
        onAuthenticationSuccess?.invoke(cipher)
        doFinally?.invoke()
    }

    private fun invokeCallback(type: Int, failureReason: Int? = null) {
        when (type) {
            INVOKE_ON_FAIL -> {
                onAuthenticationFail?.invoke(failureReason)
                doFinally?.invoke()
            }
            INVOKE_ON_CANCEL -> {
                onAuthenticationCancel?.invoke()
                doFinally?.invoke()
            }
            else -> {
                onAuthenticationFail?.invoke(failureReason)
            }
        }
    }
}
