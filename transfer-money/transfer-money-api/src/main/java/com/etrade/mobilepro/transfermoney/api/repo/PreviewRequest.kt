package com.etrade.mobilepro.transfermoney.api.repo

import com.etrade.mobilepro.transfermoney.api.TransferRepeat

data class PreviewRequest(
    val userId: String,
    val platform: String,
    val scheduleDate: String,
    val fromAcctId: String,
    val fromAcctType: String,
    val toAcctId: String,
    val toAcctType: String,
    val amount: String,
    val frequency: TransferRepeat
)
