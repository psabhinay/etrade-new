package com.etrade.mobilepro.balance.section.viewfactory

import com.etrade.mobilepro.balance.data.dto.BalancesDetailsDto
import com.etrade.mobilepro.balance.section.BalancesActionType
import com.etrade.mobilepro.balance.section.BalancesLabelResources
import com.etrade.mobilepro.balance.section.BalancesResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.HeaderSectionItem
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.SummarySectionItemWithActionIcon
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.onValid
import com.etrade.mobilepro.util.safeLet

private const val NEGATIVE_SIGN = '-'

@SuppressWarnings("LongMethod")
fun getMarginCallSections(
    detailsDto: BalancesDetailsDto,
    resources: BalancesLabelResources,
    actionClickListener: (BalancesActionType) -> Unit
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    var isHeaderMarginCallAdded = false
    // FED CALL
    onValid(detailsDto.fedcallVal) {
        // add label Margin Call
        sections.add(HeaderSectionItem(resources.header.marginCall))
        isHeaderMarginCallAdded = true
        sections.add(
            createSummarySectionWithMoreInfoIcon(
                resources.margin.fedCall,
                detailsDto.fedcallVal,
                resources,
                BalancesActionType.MORE_INFO_FED_CALL,
                actionClickListener
            )
        )
    }

    // House CALL
    onValid(detailsDto.housecallVal) {
        // add label Margin Call
        if (!isHeaderMarginCallAdded) {
            sections.add(HeaderSectionItem(resources.header.marginCall))
            isHeaderMarginCallAdded = true
        }
        sections.add(
            createSummarySectionWithMoreInfoIcon(
                resources.margin.houseCall,
                detailsDto.housecallVal,
                resources,
                BalancesActionType.MORE_INFO_MAINTENANCE_CALL,
                actionClickListener
            )
        )
    }

    // Equity CALL
    onValid(detailsDto.minEqCallVal) {
        // add label Margin Call
        if (!isHeaderMarginCallAdded) {
            sections.add(HeaderSectionItem(resources.header.marginCall))
            isHeaderMarginCallAdded = true
        }
        sections.add(createSummarySection(resources.margin.minEquityCall, formatBalancesValue(detailsDto.minEqCallVal, resources.defaultResources), resources.defaultResources.marginCallTextColor))
    }
    return sections
}

fun getMarginSecuritiesSection(
    resources: BalancesLabelResources,
    marketSecLongVal: String?,
    mktSecshortVal: String?
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    sections.add(HeaderSectionItem(resources.header.securities))
    sections.add(createSummarySection(resources.margin.securities, formatBalancesValue(marketSecLongVal, resources.defaultResources)))
    onValid(mktSecshortVal) {
        sections.add(createSummarySection(resources.margin.shortSecurities, formatBalancesValue(mktSecshortVal, resources.defaultResources)))
    }

    return sections
}

fun getSweepHoldingSection(
    detailsDto: BalancesDetailsDto,
    resources: BalancesLabelResources
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    if (detailsDto.sweepCashholding != "H") {
        sections.add(createSummarySection(resources.cash.sweepDeposit, formatBalancesValue(detailsDto.sweepCashholding ?: "0.00", resources.defaultResources)))
    }
    return sections
}

fun getFundsOnHoldSection(
    resources: BalancesLabelResources,
    fundsWithheld: String?,
    fundsWithdrawal: String?
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    // Funds on Hold
    sections.add(HeaderSectionItem(resources.header.fundsHold))
    sections.add(createSummarySection(resources.cash.purchasingPowerFundsWithheld, formatBalancesValue(fundsWithheld, resources.defaultResources)))
    sections.add(createSummarySection(resources.cash.purchasingPowerFundsWithdrawal, formatBalancesValue(fundsWithdrawal, resources.defaultResources)))
    return sections
}

fun getMarginPurchasingPowerSection(
    resources: BalancesLabelResources,
    marginSecVal: String?,
    cashsecVal: String?,
    margineqVal: String?,
    marginEqPer: String?
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    sections.add(HeaderSectionItem(resources.header.purchasingPower))
    sections.add(createSummarySection(resources.margin.marginableSecurities, formatBalancesValue(marginSecVal, resources.defaultResources)))
    sections.add(createSummarySection(resources.margin.nonMarginableSecuritiesOrOptions, formatBalancesValue(cashsecVal, resources.defaultResources)))
    sections.add(createSummarySection(resources.margin.equity, formatBalancesValue(margineqVal, resources.defaultResources)))
    sections.add(createSummarySection(resources.margin.equityPercentage, formatBalancesValue(marginEqPer, resources.defaultResources, addPrefixDollar = false, addSuffix = true)))
    return sections
}

fun getFundsWithHeldSection(
    resources: BalancesLabelResources,
    withheldCashVal: String?,
    withheldMarginVal: String?
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    var isHeaderAdded = false
    onValid(withheldCashVal) {
        sections.add(HeaderSectionItem(resources.header.fundsWithHeld))
        sections.add(createSummarySection(resources.cash.cashTitle, formatBalancesValue(withheldCashVal, resources.defaultResources)))
        isHeaderAdded = true
    }

    onValid(withheldMarginVal) {
        if (!isHeaderAdded) {
            sections.add(HeaderSectionItem(resources.header.fundsWithHeld))
        }
        sections.add(createSummarySection(resources.margin.marginTitle, formatBalancesValue(withheldMarginVal, resources.defaultResources)))
    }
    return sections
}

fun getMarginWithdrawalSection(
    resources: BalancesLabelResources,
    withdrawCashVal: String?,
    withdrawMarginVal: String?
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    sections.add(HeaderSectionItem(resources.header.fundsWithdrawal))
    sections.add(createSummarySection(resources.cash.cashTitle, formatBalancesValue(withdrawCashVal, resources.defaultResources)))
    sections.add(createSummarySection(resources.margin.marginTitle, formatBalancesValue(withdrawMarginVal, resources.defaultResources)))
    return sections
}

fun getMarginBalanceOverView(
    resources: BalancesLabelResources,
    netAccountVal: String?,
    totalVal: String?
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    sections.add(createSummarySection(resources.header.netAccountVal, formatBalancesValue(netAccountVal, resources.defaultResources)))
    sections.add(createSummarySection(resources.cash.totalMarketValue, formatBalancesValue(totalVal, resources.defaultResources)))
    return sections
}

@SuppressWarnings("LongMethod")
fun getMarginAccountBalanceSection(
    resources: BalancesLabelResources,
    detailsDto: BalancesDetailsDto,
    actionClickListener: (BalancesActionType) -> Unit
): List<TableSectionItemLayout> {
    val sections = mutableListOf<TableSectionItemLayout>()
    // Purchasing Power
    sections.add(HeaderSectionItem(resources.header.purchasingPower))
    sections.add(createSummarySection(resources.margin.marginableSecurities, formatBalancesValue(detailsDto.marginSecVal, resources.defaultResources)))
    sections.add(createSummarySection(resources.margin.nonMarginableSecuritiesOrOptions, formatBalancesValue(detailsDto.cashsecVal, resources.defaultResources)))
    sections.add(createSummarySection(resources.margin.maxAvailableWithdrawal, formatBalancesValue(detailsDto.totalWithdrawCashval, resources.defaultResources)))
    sections.add(createSummarySection(resources.margin.netCashMarginbalance, formatBalancesValue(detailsDto.netCashmarbal, resources.defaultResources)))

    // Margin Call section
    sections.addAll(getMarginCallSections(detailsDto, resources, actionClickListener))

    // securities
    sections.addAll(getMarginSecuritiesSection(resources, detailsDto.marketSecLongVal, detailsDto.mktSecshortVal))

    // Margin Purchasing Power
    sections.addAll(
        getMarginPurchasingPowerSection(
            resources,
            detailsDto.marginSecVal,
            detailsDto.cashsecVal,
            detailsDto.margineqVal,
            detailsDto.marginEqPer
        )
    )

    // Funds withheld for open orders
    sections.addAll(getFundsWithHeldSection(resources, detailsDto.withheldCashVal, detailsDto.withheldMarginVal))

    // Withdrawal
    sections.addAll(getMarginWithdrawalSection(resources, detailsDto.withdrawCashVal, detailsDto.withdrawMarginVal))

    // Cash Margin Balance
    sections.add(HeaderSectionItem(resources.header.cashMarginBalance))

    // check for holding - "H"
    sections.addAll(getSweepHoldingSection(detailsDto, resources))

    sections.add(createSummarySection(resources.margin.balance, formatBalancesValue(detailsDto.marginbalVal, resources.defaultResources)))
    sections.add(createSummarySection(resources.margin.netMarginBalance, formatBalancesValue(detailsDto.netCashmarbal, resources.defaultResources)))
    // Short Reserve
    onValid(detailsDto.shortresVal) {
        sections.add(createSummarySection(resources.margin.shortReserve, formatBalancesValue(detailsDto.shortresVal, resources.defaultResources)))
    }

    // Funds on Hold
    sections.addAll(getFundsOnHoldSection(resources, detailsDto.fundswhpwrVal, detailsDto.fundsWhWithdrawVal))

    // Day trading - trade counts
    safeLet(detailsDto.dtCount, detailsDto.dtAllowedCt) { ct, allowedCt ->
        sections.add(HeaderSectionItem(resources.header.dayTrading))
        sections.add(createSummarySection(resources.margin.dayTradesForLast5Days, ct))
        sections.add(createSummarySection(resources.margin.dayTradesAllowUntilPatternDayTradingQualification, allowedCt))
    }
    return sections
}

fun formatBalancesValue(unformattedString: String?, resources: BalancesResources, addPrefixDollar: Boolean = true, addSuffix: Boolean = false): String {
    return unformattedString?.let {
        // check for Prefix
        val prefixDollar = if (addPrefixDollar) {
            resources.currencySign
        } else {
            ""
        }

        // Check for suffix
        val suffix = if (addSuffix) {
            resources.percentageSign
        } else {
            ""
        }

        // check for negative
        if (it.startsWith(NEGATIVE_SIGN)) {
            NEGATIVE_SIGN + prefixDollar + it.substring(1) + suffix
        } else {
            prefixDollar + it + suffix
        }
    } ?: resources.emptyPriceDefault
}

fun createSummarySectionWithMoreInfoIcon(
    label: String,
    value: String?,
    resources: BalancesLabelResources,
    actionType: BalancesActionType,
    actionClickListener: (BalancesActionType) -> Unit
): TableSectionItemLayout = SummarySectionItemWithActionIcon(
    label,
    formatBalancesValue(value, resources.defaultResources),
    resources.defaultResources.marginCallTextColor,
    actionIconDrawable = resources.defaultResources.infoIcon,
    actionIconDescription = resources.defaultResources.infoIconDescription,
    onActionIconClick = { actionClickListener.invoke(actionType) }
)
