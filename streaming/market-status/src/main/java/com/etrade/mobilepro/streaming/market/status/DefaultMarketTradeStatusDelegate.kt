package com.etrade.mobilepro.streaming.market.status

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

private const val MARKET_TRADE_HALT_CODE = "H"

class DefaultMarketTradeStatusDelegate @Inject constructor(private val isCircuitBreakerEnabled: Boolean) : MarketTradeStatusDelegate {
    private val _marketHaltSignal = MutableLiveData<ConsumableLiveEvent<MarketTradeStatus>>()

    override val marketStatus: LiveData<ConsumableLiveEvent<MarketTradeStatus>>
        get() = _marketHaltSignal

    override fun onMarketStatusUpdate(level1Data: Level1Data) {
        // Market Trade Halt is currently available for Equities only.
        if (level1Data is StockData) {
            val event = if (level1Data.isMarketHalted()) {
                ConsumableLiveEvent(MarketTradeStatus(true, level1Data.tradeHaltReason))
            } else {
                ConsumableLiveEvent(MarketTradeStatus())
            }
            _marketHaltSignal.postValue(event)
        }
    }

    override fun resetMarketStatus() {
        _marketHaltSignal.value = ConsumableLiveEvent(MarketTradeStatus(false))
    }

    private fun StockData.isMarketHalted(): Boolean {
        return tradeStatus?.trim() == MARKET_TRADE_HALT_CODE || isCircuitBreakerEnabled
    }
}
