package com.etrade.mobilepro.inboxmessages

import android.content.Context
import android.content.res.Configuration
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.themeselection.DARK_MODE
import com.etrade.mobilepro.themeselection.FOLLOWING_SYSTEM_PREFERENCES
import com.etrade.mobilepro.themeselection.LIGHT_MODE
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import kotlin.coroutines.CoroutineContext

class InboxMessagesDynamicView(
    @Web private val baseUrl: String,
    private val imageLoader: ImageLoader,
    private val messages: MutableLiveData<List<InboxMessage>> = MutableLiveData(),
    private val repository: InboxMessagesRepository,
    private val context: Context,
    messageType: MessageType,
    place: MessagePlace? = null,
    userType: UserType? = null
) : GenericLayout {

    override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
        InboxMessagesDynamicViewModel(
            baseUrl = baseUrl,
            imageLoader = imageLoader,
            messages = messages,
            repository = repository,
            context = context
        )
    }

    override val viewModelClass: Class<out DynamicViewModel> =
        InboxMessagesDynamicViewModel::class.java

    override val uniqueIdentifier: String = "${javaClass.name}_${messageType.type}_${place?.place}_${userType?.type}"
}

fun <T> List<T>.addInboxMessagesView(position: Int, view: T): List<T> {
    return toMutableList().apply { add(position, view) }
}

inline fun <reified P, T> List<T>.removeInboxMessagesView(): List<T> {
    return toMutableList().apply { removeAll { it is P } }
}

inline fun <reified P : T, T> List<T>.addOrRemoveInboxMessagesView(
    position: Int = 0,
    view: P?
): List<T> {
    return if (view == null) {
        removeInboxMessagesView<P, T>()
    } else {
        addInboxMessagesView(position, view)
    }
}

@Suppress("LongParameterList", "LongMethod")
fun inboxMessagesDynamicViewLiveData(
    baseUrl: String,
    imageLoader: ImageLoader,
    type: MessageType,
    coroutineContext: CoroutineContext,
    repository: InboxMessagesRepository,
    context: Context,
    place: MessagePlace? = null,
    userType: UserType? = null,
    uiMode: Int? = null
): LiveData<InboxMessagesDynamicPayload> {
    val messages = MutableLiveData<List<InboxMessage>>(emptyList())
    val darkOrLightUiMode = uiMode?.let { mapUiModeToDarkOrLight(it, context) }
    return liveData(coroutineContext) {
        repository.getMessages(type, place, userType).onSuccess {
            emit(
                it.filter { inboxMessage ->
                    inboxMessage.theme.matchesWith(darkOrLightUiMode)
                }
            )
        }.onFailure {
            emit(emptyList<InboxMessage>())
        }
    }.switchMap { messages.apply { value = it } }
        .map {
            if (it.isEmpty()) {
                InboxMessagesDynamicPayload()
            } else {
                InboxMessagesDynamicPayload(
                    InboxMessagesDynamicView(
                        baseUrl,
                        imageLoader,
                        messages,
                        repository,
                        context,
                        type,
                        place,
                        userType
                    )
                )
            }
        }
}

data class InboxMessagesDynamicPayload(val view: InboxMessagesDynamicView? = null)

private fun CampaignAssetsTheme?.matchesWith(uiMode: Int?): Boolean {
    if (this != null && uiMode != null) {
        return (this == CampaignAssetsTheme.LIGHT_THEME && uiMode == LIGHT_MODE) ||
            (this == CampaignAssetsTheme.DARK_THEME && uiMode == DARK_MODE)
    }
    return true
}

private fun mapUiModeToDarkOrLight(uiMode: Int, context: Context): Int {
    if (uiMode != FOLLOWING_SYSTEM_PREFERENCES) {
        return uiMode
    }
    return if (isSystemInDarkMode(context)) DARK_MODE else LIGHT_MODE
}

private fun isSystemInDarkMode(context: Context): Boolean =
    context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
