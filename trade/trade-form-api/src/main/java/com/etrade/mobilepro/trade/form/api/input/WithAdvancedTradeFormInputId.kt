package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.form.api.value.AdvancedOrderTypeValueId

interface WithAdvancedTradeFormInputId {
    val advancedOrder: String
    val advancedOrderType: String
    val advancedOrderTypeValueId: AdvancedOrderTypeValueId
}

fun WithAdvancedTradeFormInputId.extractAdvancedOrderType(snapshot: InputValuesSnapshot): AdvancedTradeType {
    val advancedOrderType = snapshot[advancedOrderType] as? String
    return advancedOrderType?.let { advancedOrderTypeValueId.converter.toValue(it) } ?: AdvancedTradeType.None
}
