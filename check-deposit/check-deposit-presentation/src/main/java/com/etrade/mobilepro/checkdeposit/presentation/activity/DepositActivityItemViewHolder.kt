package com.etrade.mobilepro.checkdeposit.presentation.activity

import android.view.View
import android.view.accessibility.AccessibilityNodeInfo
import android.view.accessibility.AccessibilityNodeInfo.ACTION_CLICK
import android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction
import androidx.recyclerview.widget.RecyclerView
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.checkdeposit.activity.service.DepositActivityItemDto
import com.etrade.mobilepro.checkdeposit.presentation.DepositStatus
import com.etrade.mobilepro.checkdeposit.presentation.R
import com.etrade.mobilepro.checkdeposit.presentation.databinding.ItemDepositActivityBinding

class DepositActivityItemViewHolder(
    val binding: ItemDepositActivityBinding,
    private val depositActivityItemListener: ((DepositActivityItemDto, String) -> Unit)?
) : RecyclerView.ViewHolder(binding.root) {
    private var item: DepositActivityResultItem? = null

    init {
        binding.depositActivityItemContainer.setOnClickListener {
            item?.let { item ->
                depositActivityItemListener?.invoke(item.depositActivityItem, item.uiDepositAccount)
                bindTo(item)
            }
        }
        binding.root.setAccessibilityDelegate(object : View.AccessibilityDelegate() {
            override fun onInitializeAccessibilityNodeInfo(host: View?, info: AccessibilityNodeInfo?) {
                super.onInitializeAccessibilityNodeInfo(host, info)
                val description = binding.root.resources.getString(R.string.show_more_details)
                val customAction = AccessibilityAction(ACTION_CLICK, description)
                info?.addAction(customAction)
            }
        })
    }

    fun bindTo(depositActivityResultItem: DepositActivityResultItem) {
        item = depositActivityResultItem

        with(binding) {
            depositStatus.text =
                depositActivityResultItem.depositActivityItem.status?.let {
                root.context.getString(DepositStatus.labelFromValue(it))
            } ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
            depositValue.text = depositActivityResultItem.uiDepositValue
            depositAccount.text = depositActivityResultItem.uiDepositAccount
            depositDate.text = depositActivityResultItem.uiDepositDate
        }
    }
}
