package com.etrade.mobilepro.pushnotification.presentation

import android.app.Notification
import android.content.Context
import androidx.core.app.NotificationManagerCompat
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.pushnotification.usecase.SmartAlertSubscriptionUseCase
import com.etrade.mobilepro.util.KeyValueStore
import com.etrade.mobilepro.util.flatMap
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

private const val KEY_NOTIFICATION_ID = "SmartAlertService.key.notification_id"
private val logger = LoggerFactory.getLogger(SmartAlertService::class.java)

class SmartAlertService constructor(
    private val notificationFactory: SmartAlertNotificationFactory,
    private val smartAlertSubscription: SmartAlertSubscriptionUseCase,
    private val store: KeyValueStore,
    private val context: Context
) : MessagingService {

    private val scope = CoroutineScope(Dispatchers.Main + SupervisorJob())

    override fun onDestroy() {
        scope.cancel()
    }

    override fun onNewToken(token: String) {
        scope.launch {
            smartAlertSubscription
                .unsubscribe()
                .flatMap { smartAlertSubscription.subscribeWith(token) }
                .onFailure {
                    logger.error(
                        "New firebase token was received but subscription could not be updated",
                        it
                    )
                }
        }
    }

    override fun onMessageReceived(message: RemoteMessage) {
        message.data
            .toSmartAlert()
            ?.let(notificationFactory::createFrom)
            ?.also(::showNotification)
            ?: run { logger.error("Firebase message was received but could not be handled as a smart alert $message") }
    }

    private fun showNotification(notification: Notification) {
        NotificationManagerCompat
            .from(context)
            .notify(generateNotificationId(), notification)
    }

    private fun generateNotificationId(): Int {
        return (store.getInt(KEY_NOTIFICATION_ID, 0) + 1)
            .also { store.putInt(KEY_NOTIFICATION_ID, it) }
    }
}
