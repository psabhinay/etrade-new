package com.etrade.mobilepro.screener.presentation.stock.marketsegment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.screener.api.Identifiable
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.stock.MarketSegmentFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.databinding.FragmentMarketSegmentBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class MarketSegmentFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<MarketSegmentFilter, MarketSegmentViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {

    private val binding by viewBinding(FragmentMarketSegmentBinding::bind)
    override val layoutRes: Int = R.layout.fragment_market_segment
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.STOCK
    override val viewModel by viewModels<MarketSegmentViewModel> { viewModelFactory }
    private val dropDownManager by lazy {
        BottomSheetSelector<Identifiable>(parentFragmentManager, resources)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
    }

    private fun setupFragment() {
        setupDropDownManager(viewModel.lastBottomSheetData)
        binding.sectorSelector.setOnClickListener {
            viewModel.chooseSectorTapped()
        }
        binding.industrySelector.setOnClickListener {
            viewModel.chooseIndustryTapped()
        }
        setupCapToggleListeners()
    }

    @Suppress("LongMethod")
    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                with(binding) {
                    sectorSelector.selectedValue = it.filter.sector.title
                    industrySelector.selectedValue = it.filter.industry.title
                    industrySelector.selectionEnabled = !it.filter.sector.isAllItemsElement
                    microCap.isChecked = it.filter.microCap
                    smallCap.isChecked = it.filter.smallCap
                    midCap.isChecked = it.filter.midCap
                    largeCap.isChecked = it.filter.largeCap
                    megaCap.isChecked = it.filter.megaCap
                }
            }
        )
        viewModel.viewEffects.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is MarketSegmentViewModel.ViewEffects.BottomSelectorWithData -> {
                        setupDropDownManager(it)
                        dropDownManager.openDropDown()
                    }
                }
            }
        )
        viewModel.viewEffects.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is MarketSegmentViewModel.ViewEffects.BottomSelectorWithData -> {
                        setupDropDownManager(it)
                        dropDownManager.openDropDown()
                    }
                }
            }
        )
    }

    private fun setupDropDownManager(item: MarketSegmentViewModel.ViewEffects.BottomSelectorWithData?) {
        item?.let {
            dropDownManager.init(
                tag = it.tag,
                items = it.items,
                title = getString(it.titleRes),
                initialSelectedPosition = it.initialSelectedPosition,
                dividerPosition = it.dividerAt
            )
            dropDownManager.setListener { position, item ->
                viewModel.dropDownItemSelected(position, item)
            }
        }
    }

    private fun setupCapToggleListeners() {
        binding.apply {
            microCap.checkedStateListener = { isChecked, byUser ->
                if (byUser) {
                    viewModel.capToggled(MarketSegmentViewModel.Cap.MICRO, isChecked)
                }
            }
            smallCap.checkedStateListener = { isChecked, byUser ->
                if (byUser) {
                    viewModel.capToggled(MarketSegmentViewModel.Cap.SMALL, isChecked)
                }
            }
            midCap.checkedStateListener = { isChecked, byUser ->
                if (byUser) {
                    viewModel.capToggled(MarketSegmentViewModel.Cap.MID, isChecked)
                }
            }
            largeCap.checkedStateListener = { isChecked, byUser ->
                if (byUser) {
                    viewModel.capToggled(MarketSegmentViewModel.Cap.LARGE, isChecked)
                }
            }
            megaCap.checkedStateListener = { isChecked, byUser ->
                if (byUser) {
                    viewModel.capToggled(MarketSegmentViewModel.Cap.MEGA, isChecked)
                }
            }
        }
    }
}
