package com.etrade.mobilepro.billpay.data.repo

import com.etrade.mobile.accounts.billpay.BillPay
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.data.rest.BillPayDto
import com.etrade.mobilepro.billpay.data.rest.BrokerageDto
import com.etrade.mobilepro.billpay.data.rest.EnrollmentDetailsResponseDto
import java.math.BigDecimal
import java.math.BigInteger

internal fun EnrollmentDetailsResponseDto.toAccountList(): List<AccountDetails> {
    return dataDto.enrollmentDisplayDto.accountList.map {
        object : AccountDetails {
            override val nickname: String? = null
            override val instNo: BigInteger = it.instNo?.toBigIntegerOrNull() ?: BigInteger.ZERO
            override val accountCategory: String = ""
            override val accountId: String = it.accountId ?: ""
            override val accountIdKey: String = it.accountIdKey ?: ""
            override val maskedAccountId: String = it.maskedAccountId ?: ""
            override val accountMode: String = it.accountMode ?: ""
            override val accountName: String = it.accountName ?: ""
            override val accountShortName: String = it.accountShortName ?: ""
            override val accountLongName: String = it.accountLongName ?: ""
            override val mobileDescription: String = it.mobileDescription ?: ""
            override val accountValue: BigDecimal? = null
            override val accountType: String = it.accountType ?: ""
            override val institutionType: String = it.institutionType ?: ""
            override val accountStatus: String = it.accountStatus ?: ""
            override val restrictionLevel: String = it.restrictionLevel ?: ""
            override val closedDate: Long = it.closedDate ?: 0L
            override val brokerage: AccountDetails.Brokerage? = it.brokerage?.toApi()
            override val billPay: BillPay = it.billPay.toApi()
        }
    }
}

internal fun BillPayDto?.toApi(): BillPay {
    val dto = this
    return BillPay(
        isRestricted = dto?.isRestricted ?: false,
        isEnrolled = dto?.isEnrolled ?: false,
        isPrimary = dto?.isPrimary ?: false,
        isEligible = dto?.isEligible ?: false
    )
}

internal fun BrokerageDto.toApi(): AccountDetails.Brokerage {
    val dto = this
    return object : AccountDetails.Brokerage {
        override val cashAvailableForWithdrawal: BigDecimal? = dto.cashAvailableForWithdrawal?.let { value ->
            if (value >= BigDecimal.ZERO) { value } else { BigDecimal.ZERO }
        }
        override val marginAvailableForWithdrawal: BigDecimal? = dto.marginAvailableForWithdrawal
        override val purchasingPower: BigDecimal? = dto.purchasingPower
        override val totalAvailableForWithdrawal: BigDecimal? = dto.totalAvailableForWithdrawal
        override val marginBalance: BigDecimal? = dto.marginBalance
        override val netCashBalance: BigDecimal? = dto.netCashBalance
        override val cmAccount: Boolean? = dto.isCmAccount
        override val contributionDetails: List<AccountDetails.ContributionDetails> = emptyList()
    }
}
