package com.etrade.mobilepro.chat.api.message

import androidx.annotation.Keep
import com.etrade.mobilepro.util.json.FallbackEnum
import com.squareup.moshi.Json

@FallbackEnum(name = "Unknown")
@Keep
enum class ChatRetrieveMessageType {
    @Json(name = "AgentNotTyping")
    AgentNotTyping,
    @Json(name = "AgentDisconnect")
    AgentDisconnect,
    @Json(name = "AgentTyping")
    AgentTyping,
    @Json(name = "QueueUpdate")
    QueueUpdate,
    @Json(name = "ChatEstablished")
    ChatEstablished,
    @Json(name = "ChatMessage")
    ChatMessage,
    @Json(name = "ChatEnded")
    ChatEnded,
    @Json(name = "ChatRequestSuccess")
    ChatRequestSuccess,
    @Json(name = "ChatRequestFail")
    ChatRequestFail,
    @Json(name = "ChatTransferred")
    ChatTransferred,
    @Json(name = "TransferToButtonInitiated")
    TransferToButtonInitiated,
    @Json(name = "Unknown")
    Unknown,
}
