package com.etrade.mobilepro.bloombergtv.widget

import android.graphics.Bitmap
import android.media.MediaPlayer
import android.os.Handler
import android.os.Looper
import android.view.TextureView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.bloombergtv.api.BloombergTvRepository
import com.etrade.mobilepro.bloombergtv.common.BloombergTvPipStatusHelper
import com.etrade.mobilepro.dynamicui.api.AttachStateAware
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.rx2.await
import java.lang.ref.WeakReference

class BloombergWidgetViewModel(
    private val bloombergTvPipStatusHelper: BloombergTvPipStatusHelper,
    private val repo: BloombergTvRepository
) : DynamicViewModel(R.layout.bloomberg_widget), AttachStateAware {

    override val variableId: Int = BR.obj
    private val _dynamicViewState = MutableLiveData<DynamicUiViewState>()
    val dynamicViewState: LiveData<DynamicUiViewState>
        get() = _dynamicViewState

    private var player: MediaPlayer? = null
    private var refreshJob: Job? = null

    private val _thumbnail = MutableLiveData<Bitmap?>()
    val thumbnail: LiveData<Bitmap?>
        get() = _thumbnail

    private val handler = Handler(Looper.getMainLooper())

    // In Android to be used TextureView should be really inflated and visible on screen.
    // That's why we do not create one programmatically but hold a weak reference to the visible one.
    // Hardware acceleration should be enabled in the manifest in order to use texture views inside scrolling containers.
    private var textureView: WeakReference<TextureView>? = null

    override fun onAttached(holder: RecyclerView.ViewHolder) {
        if (thumbnail.value == null && dynamicViewState.value !is DynamicUiViewState.Error) {
            textureView = WeakReference(holder.itemView.findViewById<TextureView>(R.id.textureView1))
            refreshJob = refreshThumbnail(textureView?.get())
        }
    }

    override fun onDetached(holder: RecyclerView.ViewHolder) {
        dismantlePlayer()
    }

    override fun onCleared() {
        dismantlePlayer()
        handler.removeCallbacksAndMessages(null)
        super.onCleared()
    }

    fun refresh() {
        dismantlePlayer()
        _thumbnail.value = null
        _dynamicViewState.value = DynamicUiViewState.Loading
        // because we are inside recycler view onAttached will be called subsequently
    }

    fun onRetryClick() {
        refresh()
        refreshJob = refreshThumbnail(textureView?.get())
    }

    fun onNavigateToBloombergTv() {
        if (bloombergTvPipStatusHelper.isPipMode || dynamicViewState.value is DynamicUiViewState.Error) {
            return
        }
        clickEvents.postValue(CtaEvent.BloombergTvEvent)
    }

    private fun dismantlePlayer() {
        refreshJob?.cancel()
        player?.setOnPreparedListener(null)
        player?.setOnErrorListener(null)
        player = null
    }

    private val exceptionHandler = CoroutineExceptionHandler { _, error ->
        logger.error(error.message)
        _dynamicViewState.value = DynamicUiViewState.Error()
    }

    @Suppress("LongMethod")
    private fun refreshThumbnail(textureView: TextureView?): Job = viewModelScope.launch(exceptionHandler) {
        if (textureView == null) { return@launch }
        _dynamicViewState.value = DynamicUiViewState.Loading
        val tvUrl = repo.getBloombergtvResponse().await().bloombergLiveData.url
        if (tvUrl.isEmpty()) {
            _dynamicViewState.value = DynamicUiViewState.Error()
            return@launch
        }
        val playerFactory = PlayerFactory(tvUrl, textureView, viewModelScope)
        player = playerFactory.createMediaPlayer(
            thumbnailFactoryCreator = { mediaPlayer ->
                ThumbnailFactory(
                    handler = handler,
                    mediaPlayer = mediaPlayer,
                    onThumbnailCreated = { bmp ->
                        _thumbnail.value = bmp
                        _dynamicViewState.value = DynamicUiViewState.Success
                        mediaPlayer.stop()
                        mediaPlayer.release()
                        dismantlePlayer()
                    },
                    onThumbnailingFailed = {
                        _dynamicViewState.value = DynamicUiViewState.Error()
                    }
                )
            },
            onError = {
                _dynamicViewState.value = DynamicUiViewState.Error()
            }
        )
        player?.prepareAsync()
    }
}
