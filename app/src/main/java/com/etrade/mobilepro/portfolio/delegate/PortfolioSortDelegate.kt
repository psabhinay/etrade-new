package com.etrade.mobilepro.portfolio.delegate

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.livedata.DistinctLiveData
import com.etrade.mobilepro.portfolio.PortfolioPositionsDisplayMode
import com.etrade.mobilepro.portfolio.PortfolioSettingsRepository
import com.etrade.mobilepro.portfolio.PortfolioTableMode
import com.etrade.mobilepro.portfolio.data.PortfolioColumnRepo
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.util.android.SingleLiveEvent
import javax.inject.Inject

class PortfolioSortDelegate @Inject constructor(
    portfolioColumnRepo: PortfolioColumnRepo,
    private val portfolioSettingsRepository: PortfolioSettingsRepository
) : BasePositionSortDelegate(portfolioColumnRepo) {

    // Determines if the positions list is complete or partial, Set by Component that manages the list.
    var isEndOfPage: () -> Boolean = { false }

    var positionsViewIndex: Int
        get() {
            return when {
                isPortfolioView -> portfolioSettingsRepository.positionsDisplayMode.index
                else -> PortfolioPositionsDisplayMode.TABLE.index
            }
        }
        set(value) {
            val mode = PortfolioPositionsDisplayMode.from(value)
            portfolioSettingsRepository.positionsDisplayMode = mode
            _positionsDisplayMode.value = mode
            updateColumns()
        }

    private var _positionViewType: PositionTableMode = PositionTableMode.PORTFOLIO_VIEW

    override val positionTableMode: PositionTableMode
        get() = _positionViewType

    val positionsDisplayMode: LiveData<PortfolioPositionsDisplayMode>
        get() = _positionsDisplayMode

    val isPortfolioView: Boolean
        get() = positionTableMode == PositionTableMode.PORTFOLIO_VIEW

    private val _positionsDisplayMode = MutableLiveData<PortfolioPositionsDisplayMode>(
        PortfolioPositionsDisplayMode.from(positionsViewIndex)
    )

    private val _sortInfo: MutableLiveData<SortInfo> = SingleLiveEvent()

    // Each update triggers a Portfolio page refresh (i.e performs a network call)
    override val sortInfo: LiveData<SortInfo>
        get() = _sortInfo

    private val _localSortInfo: MutableLiveData<SortInfo> = DistinctLiveData(portfolioSettingsRepository.sortInfo)

    /**
     * Local sorting information is being updated whenever selected column or order is changed.
     * This event is set when all Portfolio positions fits within one page otherwise sortInfo is set
     */
    val localSortInfo: LiveData<SortInfo>
        get() = _localSortInfo

    var tableViewIndex: Int
        get() {
            return when {
                isPortfolioView -> portfolioSettingsRepository.tableMode
                else -> PortfolioTableMode.STANDARD
            }.index
        }
        set(value) {
            if (isPortfolioView) {
                portfolioSettingsRepository.tableMode = PortfolioTableMode.from(value)
                tableViewType = viewTypeFrom(value)
            }
        }

    override var tableViewType: PortfolioTableViewType = viewTypeFrom(tableViewIndex)
        private set(value) {
            if (value == field) {
                return
            }
            field = value
            updateColumns()
        }

    init {
        _localSortInfo.observeForever { sortInfo ->
            sortInfo?.let { portfolioSettingsRepository.sortInfo = it }
        }
    }

    private fun viewTypeFrom(index: Int): PortfolioTableViewType =
        PortfolioTableViewType.values()[index]

    override val sortViewState: LiveData<SortStateInfo>
        get() = _localSortInfo.map { info ->
            getSortStateInfo(info)
        }

    override fun sortColumn(newState: SortInfo) {
        if (isEndOfPage()) {
            _localSortInfo.value = newState
        } else {
            _sortInfo.value = newState
        }
    }

    override fun getOldSortInfo(): SortInfo? = _localSortInfo.value

    fun setLocalSort() {
        _sortInfo.value?.apply { _localSortInfo.postValue(this) }
    }

    fun setPositionsViewType(isAllBrokerageView: Boolean) {
        _positionViewType = if (isAllBrokerageView) {
            PositionTableMode.ALL_BROKERAGE_VIEW
        } else {
            PositionTableMode.PORTFOLIO_VIEW
        }
        val portfolioPositionsDisplayMode = PortfolioPositionsDisplayMode.from(positionsViewIndex)
        if (_positionsDisplayMode.value != portfolioPositionsDisplayMode) {
            _positionsDisplayMode.postValue(portfolioPositionsDisplayMode)
        }
    }

    fun conditionalColumnSort(newState: SortInfo) {
        if (localSortInfo.value != newState) {
            sortColumn(newState)
        }
    }

    override fun updateColumns() {
        super.updateColumns()
        _localSortInfo.value?.let {
            sortColumn(portfolioSettingsRepository.sortInfo)
            sortColumn(SortInfo(it.column, it.order))
        }
    }
}
