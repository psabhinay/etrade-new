package com.etrade.mobilepro.optionschains.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ExpirationDateDto(
    @Json(name = "year")
    val year: Int?,
    @Json(name = "month")
    val month: Int?,
    @Json(name = "day")
    val day: Int?,
    @Json(name = "expiryTypeCd")
    val expiryTypeCode: Int?,
    @Json(name = "settlementSessionCd")
    val settlementSessionCode: Int? = null
)
