package com.etrade.mobilepro.walkthrough.presentation.symboltracking

import com.etrade.mobilepro.statemachine.EtStateMachine
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingAction
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingState
import com.tinder.StateMachine
import javax.inject.Inject

class SymbolTrackingStateMachine @Inject constructor() : EtStateMachine<SymbolTrackingState, SymbolTrackingAction> {

    private val stateMachine = StateMachine.create<SymbolTrackingState, SymbolTrackingAction, SymbolTrackingState> {
        initialState(SymbolTrackingState.Idle)
        state<SymbolTrackingState.Idle> {
            on<SymbolTrackingAction.StartWalkthrough> {
                transitionTo(SymbolTrackingState.DisplayInfoMessage, SymbolTrackingState.DisplayInfoMessage)
            }
            on<SymbolTrackingAction.Canceled> {
                transitionTo(SymbolTrackingState.Idle, SymbolTrackingState.Idle)
            }
        }
        state<SymbolTrackingState.DisplayInfoMessage> {
            on<SymbolTrackingAction.InfoMessageDisplayed> {
                transitionTo(SymbolTrackingState.LookupSymbol, SymbolTrackingState.LookupSymbol)
            }
            on<SymbolTrackingAction.Canceled> {
                transitionTo(SymbolTrackingState.Idle, SymbolTrackingState.Idle)
            }
        }
        state<SymbolTrackingState.LookupSymbol> {
            on<SymbolTrackingAction.SymbolSelected> {
                transitionTo(
                    SymbolTrackingState.HighlightWatchlistActionInQuotePage,
                    SymbolTrackingState.HighlightWatchlistActionInQuotePage
                )
            }
            on<SymbolTrackingAction.Canceled> {
                transitionTo(SymbolTrackingState.Idle, SymbolTrackingState.Idle)
            }
        }
        state<SymbolTrackingState.HighlightWatchlistActionInQuotePage> {
            on<SymbolTrackingAction.SymbolAddedToWatchlist> {
                transitionTo(SymbolTrackingState.DisplayWatchlistPage, SymbolTrackingState.DisplayWatchlistPage)
            }
            on<SymbolTrackingAction.Canceled> {
                transitionTo(SymbolTrackingState.Idle, SymbolTrackingState.Idle)
            }
        }
        state<SymbolTrackingState.DisplayWatchlistPage> {
            on<SymbolTrackingAction.WatchlistPageDisplayed> {
                transitionTo(
                    SymbolTrackingState.HighlightWatchlistActionInWatchlistPage,
                    SymbolTrackingState.HighlightWatchlistActionInWatchlistPage
                )
            }
            on<SymbolTrackingAction.Canceled> {
                transitionTo(SymbolTrackingState.Idle, SymbolTrackingState.Idle)
            }
        }
        state<SymbolTrackingState.HighlightWatchlistActionInWatchlistPage> {
            on<SymbolTrackingAction.WatchlistActionInWatchlistPageHighlighted> {
                transitionTo(SymbolTrackingState.Idle, SymbolTrackingState.Idle)
            }
            on<SymbolTrackingAction.Canceled> {
                transitionTo(SymbolTrackingState.Idle, SymbolTrackingState.Idle)
            }
        }
        onTransition {
            val validTransition = it as? com.tinder.StateMachine.Transition.Valid ?: return@onTransition
            val newState = validTransition.sideEffect ?: return@onTransition
            onStateChange?.invoke(newState)
        }
    }

    override val currentState: SymbolTrackingState
        get() = stateMachine.state

    override var onStateChange: ((SymbolTrackingState) -> Unit)? = null

    override fun transition(action: SymbolTrackingAction) {
        stateMachine.transition(action)
    }
}
