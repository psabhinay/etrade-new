package com.etrade.mobilepro.feedback.data

import com.etrade.mobilepro.feedback.api.FeedbackRequest
import com.etrade.mobilepro.feedback.api.FeedbackResponse
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.POST

interface FeedbackService {

    @JsonMoshi
    @POST("/app/salesforce/mobileFeedback.json")
    suspend fun getFeedbackResponse(@Body @JsonMoshi feedbackRequest: FeedbackRequest): FeedbackResponse
}
