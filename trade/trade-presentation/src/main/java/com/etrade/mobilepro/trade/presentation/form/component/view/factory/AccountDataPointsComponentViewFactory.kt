package com.etrade.mobilepro.trade.presentation.form.component.view.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.ui.factory.ComponentViewFactory
import com.etrade.mobilepro.trade.presentation.form.component.AccountDataPointsComponent
import com.etrade.mobilepro.trade.presentation.form.component.view.AccountDataPointsComponentView

internal class AccountDataPointsComponentViewFactory : ComponentViewFactory<AccountDataPointsComponent, AccountDataPointsComponentView> {
    override fun createView(context: Context): AccountDataPointsComponentView =
        AccountDataPointsComponentView(context)
}
