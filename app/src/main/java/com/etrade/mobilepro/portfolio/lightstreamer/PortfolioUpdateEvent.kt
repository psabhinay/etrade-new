package com.etrade.mobilepro.portfolio.lightstreamer

interface PortfolioUpdateEvent {
    val symbol: String?
    val instrumentId: String?
    val instrumentType: String?
    val accountId: String?
}
