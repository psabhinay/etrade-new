package com.etrade.mobilepro.billpay.usecase

import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.util.UseCase
import javax.inject.Inject

/**
 * Given a list of accounts, filters all the elements that can be used for bill pay transactions.
 */
interface FilterBillPayAccountsUseCase : UseCase<List<AccountDetails>, ETResult<List<AccountDetails>>>

class FilterBillPayAccountsUseCaseImp @Inject constructor() : FilterBillPayAccountsUseCase {

    override suspend fun execute(parameter: List<AccountDetails>): ETResult<List<AccountDetails>> {
        val usableAccounts = parameter.filter { isBillPayUsable(it) }

        return ETResult.success(usableAccounts)
    }

    private fun isBillPayUsable(account: AccountDetails): Boolean {
        return account.billPay?.run {
            !isRestricted && isEnrolled
        } ?: false
    }
}
