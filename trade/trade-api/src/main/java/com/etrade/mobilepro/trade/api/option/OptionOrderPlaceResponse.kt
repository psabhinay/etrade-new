package com.etrade.mobilepro.trade.api.option

import com.etrade.mobilepro.trade.api.OrderPlaceResponse

interface OptionOrderPlaceResponse : OptionOrderResponse, OrderPlaceResponse {
    override val orders: List<ConfirmedOptionOrder>
}
