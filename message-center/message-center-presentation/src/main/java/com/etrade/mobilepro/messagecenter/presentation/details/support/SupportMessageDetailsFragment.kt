package com.etrade.mobilepro.messagecenter.presentation.details.support

import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.messagecenter.presentation.R
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterFragmentMessageSupportDetailsBinding
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.SupportMessageAdapter
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.SupportMessageItem
import com.etrade.mobilepro.messagecenter.presentation.messagelist.IS_CHAT_LAUNCHED_FROM_MESSAGE_CENTER
import com.etrade.mobilepro.messagecenter.presentation.messagelist.MESSAGE
import com.etrade.mobilepro.messagecenter.presentation.messagelist.MessageCenterListViewModel
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.showSoftKeyboard
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

private const val CANCEL_REPLY_REQUEST_CODE = 0xCACDE
private val CANCEL_DIALOG_TAG = "${SupportMessageDetailsFragment::class.java.canonicalName}_cancel_dialog_tag"
private const val SCROLL_SHOW_KEYBOARD_DELAY_MS = 100L

@RequireLogin
class SupportMessageDetailsFragment @Inject constructor(
    viewModelProvider: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : OverlayBaseFragment() {

    private val binding by viewBinding(MessageCenterFragmentMessageSupportDetailsBinding::bind) { onDestroyBinding() }

    override val layoutRes: Int = R.layout.message_center_fragment_message_support_details

    private val viewModel by viewModels<SupportMessageDetailsViewModel> { viewModelProvider }
    private val messageListViewModel by navGraphViewModels<MessageCenterListViewModel>(R.id.message_center_nav_graph) { viewModelProvider }
    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private var snackbar: Snackbar? = null
    private val args by navArgs<SupportMessageDetailsFragmentArgs>()
    private val adapter by lazy {
        SupportMessageAdapter { replyContent ->
            viewModel.replyContentChanged(replyContent)
            binding.sendReply.isEnabled = replyContent.isNotBlank()
        }
    }
    private val handler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
        viewModel.refresh(args.message.toDomain(), forced = false)
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                CANCEL_REPLY_REQUEST_CODE -> viewModel.cancelTapped()
            }
        }
    }

    private fun MessageCenterFragmentMessageSupportDetailsBinding.onDestroyBinding() {
        handler.removeCallbacksAndMessages(null)
        recyclerView.adapter = null
    }

    private fun setupFragment() {
        with(binding) {
            recyclerView.layoutManager = object : LinearLayoutManager(requireContext()) {
                override fun requestChildRectangleOnScreen(
                    parent: RecyclerView,
                    child: View,
                    rect: Rect,
                    immediate: Boolean
                ): Boolean = false

                override fun requestChildRectangleOnScreen(
                    parent: RecyclerView,
                    child: View,
                    rect: Rect,
                    immediate: Boolean,
                    focusedChildVisible: Boolean
                ): Boolean = false
            }
            recyclerView.adapter = adapter
            recyclerView.itemAnimator = null
            reply.setOnClickListener {
                viewModel.replyTapped()
            }
            chatWithUs.setOnClickListener {
                navigateToChat()
            }
            cancelButton.setOnClickListener {
                val dialog = CustomDialogFragment.newInstance(
                    title = getString(R.string.message_center_cancel_reply_title),
                    message = getString(R.string.message_center_cancel_reply_message),
                    okTitle = getString(R.string.yes),
                    cancelTitle = getString(R.string.no),
                    requestCode = CANCEL_REPLY_REQUEST_CODE
                )
                dialog.show(parentFragmentManager, CANCEL_DIALOG_TAG)
            }
            sendReply.setOnClickListener {
                viewModel.sendReplyTapped()
            }
        }
    }

    private fun navigateToChat() {
        val navController = findNavController()
        if (navController.currentDestination?.id == R.id.messageCenterMessageSupportDetailsFragment) {
            navController.navigate(
                SupportMessageDetailsFragmentDirections
                    .actionSupportMessageToChatSetup(
                        bundleOf(
                            IS_CHAT_LAUNCHED_FROM_MESSAGE_CENTER to true,
                            MESSAGE to args.message
                        )
                    )
            )
        }
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                handleStatus(it.status)
                handleDataPayload(it.items)
                handleBottomButtonsState(it)
                if (it.messageSent == true) {
                    snackBarUtil.snackbar(getString(R.string.message_center_message_has_been_sent), Snackbar.LENGTH_SHORT)
                }
                if (it.needToRefreshList == true) {
                    messageListViewModel.refresh(forced = true)
                }
            }
        )
    }

    private fun handleStatus(status: SupportMessageViewState.Status) =
        when (status) {
            SupportMessageViewState.Status.Idle -> {
                snackbar?.dismiss()
                binding.recyclerView.visibility = View.VISIBLE
                binding.loadingPb.hide()
            }
            SupportMessageViewState.Status.Loading -> {
                snackbar?.dismiss()
                binding.recyclerView.visibility = View.GONE
                binding.buttonLayout.visibility = View.GONE
                binding.loadingPb.show()
            }
            is SupportMessageViewState.Status.Error -> {
                binding.loadingPb.hide()
                snackbar = snackBarUtil.retrySnackbar { status.retry.invoke() }
                binding.recyclerView.visibility = View.GONE
            }
        }

    private fun handleDataPayload(items: List<SupportMessageItem>) = adapter.submitItems(items) { replyBlockIsVisible ->
        handler.post {
            if (replyBlockIsVisible) {
                binding.recyclerView.scrollToPosition(0)
                binding.recyclerView.postOnAnimationDelayed(
                    {
                        binding.root.findViewById<View?>(R.id.new_message_text)?.apply {
                            requestFocus()
                            showSoftKeyboard()
                        }
                    },
                    SCROLL_SHOW_KEYBOARD_DELAY_MS
                )
            }
        }
    }

    private fun handleBottomButtonsState(state: SupportMessageViewState) {
        binding.buttonLayout.visibility = if (state.bottomButtonsAreVisible()) {
            switchBottomButtons(state.bottomButtonsStatus)
            View.VISIBLE
        } else {
            View.GONE
        }
        binding.sendReply.isEnabled = state.getReplyContent().isNotBlank()
    }

    private fun switchBottomButtons(bottomButtonsStatus: SupportMessageViewState.BottomButtonsStatus) =
        when (bottomButtonsStatus) {
            SupportMessageViewState.BottomButtonsStatus.Hidden -> {
                // nop
            }
            SupportMessageViewState.BottomButtonsStatus.ReplyInvitation -> {
                with(binding) {
                    reply.visibility = View.VISIBLE
                    chatWithUs.visibility = View.VISIBLE
                    sendReply.visibility = View.GONE
                    cancelButton.visibility = View.GONE
                }
            }
            SupportMessageViewState.BottomButtonsStatus.SendAndCancel -> {
                with(binding) {
                    reply.visibility = View.GONE
                    chatWithUs.visibility = View.GONE
                    sendReply.visibility = View.VISIBLE
                    cancelButton.visibility = View.VISIBLE
                }
            }
        }
}
