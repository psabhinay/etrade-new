// Code generated by moshi-kotlin-codegen. Do not edit.
@file:Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION",
    "RedundantExplicitType", "LocalVariableName", "RedundantVisibilityModifier",
    "PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.backends.neo.MessageDto
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.lang.NullPointerException
import kotlin.Boolean
import kotlin.String
import kotlin.Suppress
import kotlin.Unit
import kotlin.collections.List
import kotlin.collections.emptySet
import kotlin.text.buildString

public class OrderDetailsResponseDtoJsonAdapter(
  moshi: Moshi
) : JsonAdapter<OrderDetailsResponseDto>() {
  private val options: JsonReader.Options = JsonReader.Options.of("viewOrderEvents",
      "PreparedOrderDetails", "Messages", "Status")

  private val nullableOrderDetailsDtoAdapter: JsonAdapter<OrderDetailsDto?> =
      moshi.adapter(OrderDetailsDto::class.java, emptySet(), "viewOrderEvents")

  private val nullableListOfNullableMessageDtoAdapter: JsonAdapter<List<MessageDto?>?> =
      moshi.adapter(Types.newParameterizedType(List::class.java, MessageDto::class.java),
      emptySet(), "messages")

  private val nullableStringAdapter: JsonAdapter<String?> = moshi.adapter(String::class.java,
      emptySet(), "status")

  public override fun toString(): String = buildString(45) {
      append("GeneratedJsonAdapter(").append("OrderDetailsResponseDto").append(')') }

  public override fun fromJson(reader: JsonReader): OrderDetailsResponseDto {
    var viewOrderEvents: OrderDetailsDto? = null
    var preparedOrderDetails: OrderDetailsDto? = null
    var messages: List<MessageDto?>? = null
    var messagesSet: Boolean = false
    var status: String? = null
    var statusSet: Boolean = false
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> viewOrderEvents = nullableOrderDetailsDtoAdapter.fromJson(reader)
        1 -> preparedOrderDetails = nullableOrderDetailsDtoAdapter.fromJson(reader)
        2 -> {
          messages = nullableListOfNullableMessageDtoAdapter.fromJson(reader)
          messagesSet = true
        }
        3 -> {
          status = nullableStringAdapter.fromJson(reader)
          statusSet = true
        }
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    val result: OrderDetailsResponseDto
    result = OrderDetailsResponseDto(
        viewOrderEvents = viewOrderEvents,
        preparedOrderDetails = preparedOrderDetails
    )
    result.messages = if (messagesSet) messages else result.messages
    result.status = if (statusSet) status else result.status
    return result
  }

  public override fun toJson(writer: JsonWriter, value_: OrderDetailsResponseDto?): Unit {
    if (value_ == null) {
      throw NullPointerException("value_ was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("viewOrderEvents")
    nullableOrderDetailsDtoAdapter.toJson(writer, value_.viewOrderEvents)
    writer.name("PreparedOrderDetails")
    nullableOrderDetailsDtoAdapter.toJson(writer, value_.preparedOrderDetails)
    writer.name("Messages")
    nullableListOfNullableMessageDtoAdapter.toJson(writer, value_.messages)
    writer.name("Status")
    nullableStringAdapter.toJson(writer, value_.status)
    writer.endObject()
  }
}
