package com.etrade.mobilepro.searching.symbol

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.searchingapi.SearchQueryValidator
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.util.Resource
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class DefaultSymbolSearchRepoTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var dataDelegate: SimpleCachedResource.DataSourceDelegate<List<SearchResultItem.Symbol>, SearchQuery>

    @Mock
    private lateinit var controller: SimpleCachedResource.Controller<List<SearchResultItem.Symbol>>

    @Mock
    private lateinit var validator: SearchQueryValidator

    private lateinit var sut: DefaultSymbolSearchRepo

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        sut = DefaultSymbolSearchRepo(controller, dataDelegate, validator)
    }

    @Test
    fun `valid empty query for searchAsLiveData will return Success result with empty list`() {
        whenever(validator.makeValidOrEmpty(any())).thenReturn(
            SearchQuery(
                SearchType.SYMBOL,
                ""
            )
        )

        val result = sut.searchAsLiveData(SearchQuery(SearchType.SYMBOL)).value

        assertTrue(result is Resource.Success<*>)
        assertTrue(result?.data?.isEmpty() == true)
    }

    @Test
    fun `valid empty query for searchAsObservable will return Success result with empty list`() {
        whenever(validator.makeValidOrEmpty(any())).thenReturn(
            SearchQuery(
                SearchType.SYMBOL,
                ""
            )
        )

        val result = sut.searchAsObservable(SearchQuery(SearchType.SYMBOL)).blockingFirst()

        assertTrue(result is Resource.Success<*>)
        assertTrue(result?.data?.isEmpty() == true)
    }
}
