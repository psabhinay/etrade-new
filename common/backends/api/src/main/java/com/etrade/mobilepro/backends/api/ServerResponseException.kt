package com.etrade.mobilepro.backends.api

class ServerResponseException(val error: ServerError, throwable: Throwable? = null, cause: String? = null) : RuntimeException(cause, throwable)
