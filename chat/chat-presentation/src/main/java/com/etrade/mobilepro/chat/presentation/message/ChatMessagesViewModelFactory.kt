package com.etrade.mobilepro.chat.presentation.message

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.chat.api.ChatParamsRequestModel
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

interface ChatMessagesViewModelFactory {
    fun create(chatParamsRequest: ChatParamsRequestModel?): ViewModelProvider.Factory
}

class ChatMessagesViewModelFactoryImpl @Inject constructor(
    private val factory: ChatMessagesViewModel.Factory
) : ChatMessagesViewModelFactory {
    override fun create(chatParamsRequest: ChatParamsRequestModel?): ViewModelProvider.Factory =
        viewModelFactory { factory.create(chatParamsRequest) }
}
