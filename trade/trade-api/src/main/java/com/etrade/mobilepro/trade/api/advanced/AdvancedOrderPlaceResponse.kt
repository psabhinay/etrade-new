package com.etrade.mobilepro.trade.api.advanced

import com.etrade.mobilepro.trade.api.OrderPlaceResponse

interface AdvancedOrderPlaceResponse : OrderPlaceResponse, AdvancedOrderResponse {
    override val orders: List<ConfirmedAdvancedOrder>
}
