package com.etrade.mobilepro.billpay.presentation.paymentlist.adapter

import android.content.res.Resources
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayPaymentItemRowBinding
import com.etrade.mobilepro.billpay.presentation.getLabel
import com.etrade.mobilepro.billpay.presentation.paymentlist.PaymentListItem
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription
import com.etrade.mobilepro.util.android.extension.inflater
import com.etrade.mobilepro.util.formatters.americanSimpleDateFormatter
import com.etrade.mobilepro.util.formatters.formatPayeeName

internal class PaymentListViewHolder(
    private val binding: BillPayPaymentItemRowBinding,
    private val clickListener: (PaymentListItem) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    constructor(parent: ViewGroup, clickListener: (PaymentListItem) -> Unit) : this(
        BillPayPaymentItemRowBinding.inflate(parent.inflater, parent, false),
        clickListener
    )

    private val resources: Resources = binding.root.context.resources

    private var item: PaymentListItem? = null

    init {
        binding.root.setOnClickListener {
            item?.let { item ->
                clickListener.invoke(item)
            }
        }
    }

    internal fun bind(paymentListItem: PaymentListItem) {
        this.item = paymentListItem

        val payToText = paymentListItem.payeeName?.let {
            formatPayeeName(
                it,
                paymentListItem.accountNumber,
                paymentListItem.payeeNickname
            )
        }

        binding.paymentStatus.text = resources.getText(paymentListItem.status.getLabel())
        binding.paymentAmount.text =
            paymentListItem.amount?.let { MarketDataFormatter.formatCurrencyValue(it) }
            ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
        binding.paymentPayto.text = payToText
        binding.paymentPayto.contentDescription =
            resources.accountNameContentDescription(payToText) ?: payToText
        binding.paymentDate.text =
            paymentListItem.getPaymentDate()?.format(americanSimpleDateFormatter)?.toString()
    }
}
