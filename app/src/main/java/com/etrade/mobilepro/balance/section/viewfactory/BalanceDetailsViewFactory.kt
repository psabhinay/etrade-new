package com.etrade.mobilepro.balance.section.viewfactory

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.balance.data.repo.AccountBalancesRepo
import com.etrade.mobilepro.balance.section.BalanceSectionMapper
import com.etrade.mobilepro.balance.section.BalancesDetailsViewModel
import com.etrade.mobilepro.balance.section.dto.BalanceDetailsViewDto
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class BalanceDetailsViewFactory @Inject constructor(private val balancesRepo: AccountBalancesRepo, private val sectionMapper: BalanceSectionMapper) :
    DynamicUiViewFactory {

    private val logger: Logger = LoggerFactory.getLogger(BalanceDetailsViewFactory::class.java)

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is BalanceDetailsViewDto) {
            return object : GenericLayout {

                override val viewModelClass = BalancesDetailsViewModel::class.java

                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is BalancesDetailsViewModel) {
                        viewModel.refresh()
                    } else {
                        logger.error("view model is not of type BalancesDetailsViewModel")
                    }
                }

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    toDetailsView(dto)
                }

                override val uniqueIdentifier: String = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("BalanceDetailsViewFactory registered to unknown type")
        }
    }

    private fun toDetailsView(dto: BalanceDetailsViewDto): DynamicViewModel {
        return BalancesDetailsViewModel(
            balancesRepo,
            accountId = dto.accountId,
            sectionMapper = sectionMapper,
            userId = dto.userId
        ).apply { fetch() }
    }
}
