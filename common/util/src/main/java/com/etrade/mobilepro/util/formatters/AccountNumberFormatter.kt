package com.etrade.mobilepro.util.formatters

private const val ACCOUNT_ID_VISIBLE_PART_LENGTH = 4

fun formatAccountName(accountName: String, accountId: String): String = when {
    accountId.length >= ACCOUNT_ID_VISIBLE_PART_LENGTH -> "$accountName -${getLastFourDigitOfAccountId(accountId)}"
    else -> accountName
}

fun getLastFourDigitOfAccountId(accountId: String): String = accountId.takeLast(ACCOUNT_ID_VISIBLE_PART_LENGTH)
