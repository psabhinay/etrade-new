package com.etrade.mobilepro.chat.data.messages.dto

import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ChatRetrieveMessageDto(

    @Json(name = "data")
    val data: ChatRetrievedMessageDataDto,

    @Json(name = "success")
    val success: Boolean
)

@JsonClass(generateAdapter = true)
data class ChatRetrievedMessageDataDto(
    @Json(name = "items")
    val items: List<ChatRetrieveMessageItemDto>,

    @Json(name = "sequence")
    val sequence: Int?
)

@JsonClass(generateAdapter = true)
data class ChatRetrieveMessageItemDto(
    @Json(name = "type")
    val type: ChatRetrieveMessageType,

    @Json(name = "message")
    val message: ChatRetrievedMessageDetailsDto
)

@JsonClass(generateAdapter = true)
data class ChatRetrievedMessageDetailsDto(

    @Json(name = "agentId")
    val agentId: String? = null,

    @Json(name = "connectionTimeout")
    val connectionTimeout: Long? = null,

    @Json(name = "name")
    val name: String? = null,

    @Json(name = "queuePosition")
    val queuePosition: Int? = null,

    @Json(name = "sneakPeekEnabled")
    val sneakPeekEnabled: Boolean? = null,

    @Json(name = "text")
    val text: String? = null,

    @Json(name = "transcriptSaveEnabled")
    val transcriptSaveEnabled: Boolean? = null,

    @Json(name = "url")
    val url: String? = null,

    @Json(name = "userId")
    val userId: String? = null,

    @Json(name = "visitorId")
    val visitorId: String? = null,

    // ChatRequest Fail
    @Json(name = "estimatedWaitTime")
    val estimatedWaitTime: Int? = null,

    @Json(name = "position")
    val position: Int? = null,

    // Chat queue update
    @Json(name = "reason")
    val reason: String? = null
)
