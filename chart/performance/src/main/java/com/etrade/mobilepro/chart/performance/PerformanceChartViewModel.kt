package com.etrade.mobilepro.chart.performance

import androidx.annotation.ColorRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.chart.data.ChartParams
import com.etrade.mobilepro.chart.data.GetChartLineDataParameter
import com.etrade.mobilepro.chart.data.GetChartLineDataUseCase
import com.etrade.mobilepro.chart.model.ChartLineData
import com.etrade.mobilepro.chart.model.ChartTimeFrame
import com.etrade.mobilepro.common.di.PrimaryColor
import com.etrade.mobilepro.tracking.SCREEN_TITLE_PERFORMANCE_CHART
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.delegate.suppressInitial
import com.etrade.mobilepro.util.executeBlocking
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Named

class PerformanceChartViewModel @Inject constructor(
    private val markets: List<ChartParams>,
    @Named("Performance") private val getChartLineDataUseCase: GetChartLineDataUseCase,
    @ColorRes @PrimaryColor private val performanceLineColor: Int
) : ViewModel(), ScreenViewModel {

    override val screenName: String? = SCREEN_TITLE_PERFORMANCE_CHART

    val marketNames: LiveData<List<CharSequence>> = MutableLiveData(markets.map { it.displayName })

    val chartState: LiveData<ChartState>
        get() = _chartState

    val selectedMarkets: LiveData<List<Boolean>>
        get() = _selectedMarkets

    val selectedTimeInterval: LiveData<Int>
        get() = _selectedTimeInterval

    private val timeFrames: List<ChartTimeFrame> = listOf(
        ChartTimeFrame.ThreeMonths,
        ChartTimeFrame.SixMonths,
        ChartTimeFrame.OneYear,
        ChartTimeFrame.TwoYears,
        ChartTimeFrame.ThreeYears
    )

    val timeFrameNames: LiveData<List<CharSequence>> = MutableLiveData(timeFrames.map { it.timeFrame.displayName })

    var accountId: String? by suppressInitial(null) { loadChart() }

    private var selectedMarketsRaw: MutableList<Boolean> = mutableListOf(true, false, false)
    private var selectedTimeIntervalRaw: Int = 1

    private val _chartState: MutableLiveData<ChartState> = MutableLiveData()
    private val _selectedMarkets: MutableLiveData<List<Boolean>> = MutableLiveData(selectedMarketsRaw)
    private val _selectedTimeInterval: MutableLiveData<Int> = MutableLiveData(selectedTimeIntervalRaw)

    private val disposable = CompositeDisposable()

    private val selectedAccounts: List<ChartParams>
        get() = accountId?.let { listOf(ChartParams(it, it, performanceLineColor)) } ?: listOf(ChartParams("", "", performanceLineColor))

    private val selectedMarketTickers: List<ChartParams>
        get() = selectedMarketsRaw.asSequence()
            .withIndex()
            .filter { it.value }
            .map { markets[it.index] }
            .toList()

    private val selectedTimeFrame: ChartTimeFrame
        get() = timeFrames[selectedTimeIntervalRaw]

    private val logger: Logger by lazy { LoggerFactory.getLogger(PerformanceChartViewModel::class.java) }

    fun updateMarketSelection(index: Int, selected: Boolean) {
        selectedMarketsRaw[index] = selected
        _selectedMarkets.postValue(selectedMarketsRaw)
        loadChart()
    }

    fun updateSelectedTimeInterval(selectedIndex: Int) {
        selectedTimeIntervalRaw = selectedIndex
        _selectedTimeInterval.postValue(selectedTimeIntervalRaw)
        loadChart()
    }

    private fun loadChart() {
        _chartState.postValue(ChartState.Loading())
        disposable.clear()
        disposable.add(
            getChartLineDataUseCase.executeBlocking(GetChartLineDataParameter(selectedMarketTickers, selectedAccounts, selectedTimeFrame)).subscribeBy(
                onNext = { resource ->
                    val state = when (resource) {
                        is Resource.Success -> resource.data?.let { ChartState.Idle(it) } ?: ChartState.Error()
                        is Resource.Loading -> ChartState.Loading(resource.data)
                        is Resource.Failed -> ChartState.Error(resource.data).also { logger.warn("Chart is not loaded completely.", resource.error) }
                    }
                    _chartState.postValue(state)
                },
                onError = {
                    logger.warn("Unable to load chart.", it)
                    _chartState.postValue(ChartState.Error())
                }
            )
        )
    }

    sealed class ChartState {
        data class Idle(val lineData: ChartLineData) : ChartState()
        data class Loading(val lineData: ChartLineData? = null) : ChartState()
        data class Error(val lineData: ChartLineData? = null) : ChartState()
    }
}
