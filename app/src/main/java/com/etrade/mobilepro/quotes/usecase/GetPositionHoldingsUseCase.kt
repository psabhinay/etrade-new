package com.etrade.mobilepro.quotes.usecase

import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.holdingsapi.PositionsHoldingsRepo
import com.etrade.mobilepro.quote.mapper.MobileQuotePageItemMapper
import com.etrade.mobilepro.quoteapi.GetQuoteParameter
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.quoteapi.QuoteItemInterface
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.UseCase
import kotlinx.coroutines.rx2.awaitLast
import javax.inject.Inject

interface GetPositionHoldingsUseCase : UseCase<GetQuoteParameter, ETResult<List<QuoteItemInterface>>>

class GetPositionHoldingsUseCaseImpl @Inject constructor(
    private val daoProvider: AccountDaoProvider,
    private val repo: PositionsHoldingsRepo,
    private val mapper: MobileQuotePageItemMapper,
    private val userViewModel: UserViewModel
) : GetPositionHoldingsUseCase {

    override suspend fun execute(parameter: GetQuoteParameter): ETResult<List<QuoteItemInterface>> = runCatchingET {
        if (userViewModel.isAuthenticated) {
            val accounts = daoProvider.getAccountDao().getAllAccountsNonAsync()

            val accountByType = accounts.groupBy { it.accountType }
            val brokerageAccounts = accountByType[AccountType.Brokerage]?.map { it.accountId }
            val espAccounts = accountByType[AccountType.ESP]?.map { it.accountId }

            getHoldings(
                brokerageAccounts = brokerageAccounts?.joinToString(",").orEmpty(),
                espAccounts = espAccounts?.joinToString(",").orEmpty(),
                symbol = parameter.symbol.underlyerSymbol
            )
        } else {
            emptyList()
        }
    }

    private suspend fun getHoldings(brokerageAccounts: String, espAccounts: String, symbol: String): List<QuoteItemInterface> {
        val item = mapper.map(repo.loadHoldings(brokerageAccounts, espAccounts, symbol).awaitLast())
        return if (item is QuoteItem.QuoteHoldings) {
            listOf(item)
        } else {
            emptyList()
        }
    }
}
