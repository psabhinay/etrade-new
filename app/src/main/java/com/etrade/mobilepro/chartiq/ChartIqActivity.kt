package com.etrade.mobilepro.chartiq

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.navArgs
import com.etrade.mobilepro.R
import com.etrade.mobilepro.baseactivity.ActivitySessionDelegate
import com.etrade.mobilepro.chartiq.presentation.ChartIqFragmentArgs
import com.etrade.mobilepro.chartiq.presentation.ChartIqToolbarViewModel
import com.etrade.mobilepro.common.LoginChecker
import com.etrade.mobilepro.common.getToolbarTitleContentDescription
import com.etrade.mobilepro.common.setToolbarIcon
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.common.toolbarView
import dagger.android.AndroidInjection
import javax.inject.Inject

class ChartIqActivity : AppCompatActivity() {

    @Inject
    lateinit var fragmentInjectionFactory: FragmentFactory

    @Inject
    lateinit var activitySessionDelegate: ActivitySessionDelegate

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var loginChecker: LoginChecker

    private val toolbarViewModel: ChartIqToolbarViewModel by viewModels { viewModelFactory }

    private val args: ChartIqActivityArgs by navArgs()

    private val hostNavFragment: NavHostFragment?
        get() = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as? NavHostFragment

    private val isLastPageInStack: Boolean
        get() = hostNavFragment?.childFragmentManager?.backStackEntryCount == 0

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        supportFragmentManager.fragmentFactory = fragmentInjectionFactory
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chartiq)
        activitySessionDelegate.observeTimeoutEvent(this)
        setUpNavigation()
        setupToolbar()
        setupViewModels()
    }

    override fun onStart() {
        super.onStart()
        loginChecker.check(this)
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        activitySessionDelegate.onUserInteraction(this)
    }

    private fun setupViewModels() {
        toolbarViewModel.toolbarBackAction.observe {
            setToolbarTitle(it.title)
            setupToolbar(
                it.isLastPage,
                View.OnClickListener { _ ->
                    it.action.invoke()
                }
            )
        }
    }

    private fun setUpNavigation() {
        val navController = findNavController(R.id.nav_host_fragment)
        navController.setGraph(
            R.navigation.chartiq_internal_nav_graph,
            ChartIqFragmentArgs(args.symbol).toBundle()
        )
        navController.addOnDestinationChangedListener { _, destination, _ ->
            destination.label?.let {
                setToolbarTitle(it)
            }
            setupToolbar()
        }
    }

    private fun setToolbarTitle(title: CharSequence) {
        toolbarTitleView?.text = title
        toolbarTitleView?.contentDescription = getToolbarTitleContentDescription(title, this)
    }

    private fun setupToolbar(isLastPageParam: Boolean? = null, navigationOnClickListenerParam: View.OnClickListener? = null) {
        val isLastPage = isLastPageParam ?: isLastPageInStack
        toolbarView.setToolbarIcon(isLastPage, true)
        toolbarView.setContentInsetsAbsolute(0, toolbarView.contentInsetStartWithNavigation)
        val navigationOnClickListener = navigationOnClickListenerParam ?: View.OnClickListener { onBackPressed(isLastPage) }
        toolbarView.setNavigationOnClickListener(navigationOnClickListener)
    }

    private fun onBackPressed(isLastPageInStack: Boolean) {
        if (isLastPageInStack) {
            finish()
        } else {
            hostNavFragment?.findNavController()?.popBackStack()
        }
    }

    private fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) = observe(this@ChartIqActivity, Observer { onChanged(it) })
}
