package com.etrade.mobilepro.screener.presentation.stock.opinions

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.stock.OpinionsFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Stock
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class OpinionsViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @Stock screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<OpinionsFilter, OpinionsViewModel.ViewState>(aggregator, ScreenerType.Stock.Opinions, screenerInteractor) {
    override fun provideInitialState(initialFilter: OpinionsFilter): ViewState = ViewState(filter = initialFilter)

    data class ViewState(
        override val filter: OpinionsFilter
    ) : BaseFilterScreenViewModel.ViewState<OpinionsFilter>
}
