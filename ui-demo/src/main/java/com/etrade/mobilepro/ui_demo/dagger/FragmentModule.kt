package com.etrade.mobilepro.ui_demo.dagger

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import com.etrade.mobilepro.dagger.factory.FragmentInjectionFactory
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.feature.presentation.FeatureFlagsFragment
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi

@Module
abstract class FragmentModule {

    @Binds
    abstract fun bindFragmentFactory(factory: FragmentInjectionFactory): FragmentFactory

    @ExperimentalCoroutinesApi
    @Binds
    @IntoMap
    @FragmentKey(FeatureFlagsFragment::class)
    abstract fun bindFeatureFlagFragment(fragment: FeatureFlagsFragment): Fragment
}
