package com.etrade.mobilepro.transfermoney.data.repo

import com.etrade.eo.core.util.MarketDataFormatter.formatMarketDataWithCurrency
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.etrade.mobilepro.transfermoney.api.TransferMoneyActivity
import com.etrade.mobilepro.transfermoney.api.TransferMoneyConfirm
import com.etrade.mobilepro.transfermoney.api.TransferMoneyPreview
import com.etrade.mobilepro.transfermoney.api.TransferRepeat
import com.etrade.mobilepro.transfermoney.api.TransferType
import com.etrade.mobilepro.transfermoney.api.repo.ConfirmRequest
import com.etrade.mobilepro.transfermoney.api.repo.PreviewRequest
import com.etrade.mobilepro.transfermoney.api.repo.TransferMoneyRepo
import com.etrade.mobilepro.transfermoney.data.DefaultTransferMoneyActivity
import com.etrade.mobilepro.transfermoney.data.rest.TransferMoneyApiClient
import com.etrade.mobilepro.util.formatters.safeParseStringToDate
import com.etrade.mobilepro.util.formatters.shortSimpleDateFormat
import com.etrade.mobilepro.util.json.coerceToBool
import com.etrade.mobilepro.util.safeParseBigDecimal
import java.math.BigInteger
import javax.inject.Inject

private const val DEFAULT_VALUE_TO_SHOW_FOR_MISSING_DATA = "--"
private const val LAST_DIGITS_AMOUNT = 4

class DefaultTransferMoneyRepo @Inject constructor(
    private val apiClient: TransferMoneyApiClient
) : TransferMoneyRepo {

    override suspend fun getAccounts(userId: String): ETResult<List<TransferMoneyAccount>> = runCatchingET {
        apiClient.getQuickTransferAccounts(userId).accounts
            ?.filter { it.accountId != null }
            ?.map {
                TransferMoneyAccount(
                    title = it.displayName ?: "",
                    accountId = it.accountId ?: "",
                    nickname = it.displayName ?: "",
                    instNo = BigInteger.ZERO,
                    accountType = it.accountType ?: "",
                    accountCategory = it.accountType ?: "",
                    canBeFrom = coerceToBool(it.isFromAccount, true),
                    canBeTo = coerceToBool(it.isToAccount, true)
                )
            } ?: emptyList()
    }

    override suspend fun getActivity(userId: String): ETResult<List<TransferMoneyActivity>> = runCatchingET {
        apiClient.getTransferActivities().mobileResponse?.references
            ?.filter { it?.referenceType == "transferActivities" }
            ?.firstOrNull()?.referenceData?.map {
                DefaultTransferMoneyActivity(
                    amount = it?.amount?.safeParseBigDecimal()?.let { amountBigDecimal ->
                        formatMarketDataWithCurrency(amountBigDecimal)
                    } ?: DEFAULT_VALUE_TO_SHOW_FOR_MISSING_DATA,
                    date = it?.date?.parseAndFormatDate() ?: DEFAULT_VALUE_TO_SHOW_FOR_MISSING_DATA,
                    frequency = it?.repeat?.let { transferFrequency ->
                        getDisplayValueForFrequency(TransferRepeat.fromTransferFrequencyResponse(transferFrequency))
                    } ?: DEFAULT_VALUE_TO_SHOW_FOR_MISSING_DATA,
                    fromAccount = getFromAccount(it?.fromAccountShortName, it?.from),
                    confirmationNumber = it?.confirmationNumber ?: "",
                    referenceNumber = it?.reference ?: "",
                    status = it?.status ?: DEFAULT_VALUE_TO_SHOW_FOR_MISSING_DATA,
                    toAccount = it?.toAccountShortName ?: "",
                    transferType = TransferType.fromTransferTypeResponse(it?.transferType),
                    id = it?.id ?: DEFAULT_VALUE_TO_SHOW_FOR_MISSING_DATA,
                    appName = it?.appName ?: "",
                    memo = it?.memo ?: DEFAULT_VALUE_TO_SHOW_FOR_MISSING_DATA,
                )
            } ?: emptyList()
    }

    override suspend fun getPreview(request: PreviewRequest): ETResult<TransferMoneyPreview> = runCatchingET {
        apiClient.getQuickTransferPreview(
            request.userId,
            request.platform,
            request.scheduleDate,
            request.fromAcctId,
            request.fromAcctType,
            request.toAcctId,
            request.toAcctType,
            request.amount,
            getApiValueForFrequency(request.frequency)
        ).let {
            val error = it.result?.fault?.errors?.firstOrNull()
            TransferMoneyPreview(
                it.fundHoldInfoMessage, it.scheduledDate, it.amount, error?.errorCode?.toString(), error?.errorDescription,
                it.headerMessage, it.footerMessage
            )
        }
    }

    override suspend fun deleteTransfer(userId: String, transferReferenceNumber: String, deleteSeries: Boolean): ETResult<String> = runCatchingET {
        apiClient
            .cancelTransfer(userId, transferReferenceNumber, deleteSeries)
            .let {
                it.resultDto?.code ?: ""
            }
    }

    override suspend fun getConfirm(request: ConfirmRequest): ETResult<TransferMoneyConfirm> = runCatchingET {
        apiClient.getQuickTransferConfirm(
            request.userId,
            request.scheduleDate,
            request.fromAcctId,
            request.toAcctId,
            request.amount,
            getApiValueForFrequency(request.frequency),
            request.platform,
            request.iRAContribYear
        ).let {
            TransferMoneyConfirm(
                errorDescription = it.result?.fault?.errors?.firstOrNull()?.errorDescription,
                confirmationNumber = it.referenceNumber ?: DEFAULT_VALUE_TO_SHOW_FOR_MISSING_DATA,
                headerMessage = it.headerMessage,
                message = it.fundHoldInfoMessage,
                footerMessage = it.footerMessage
            )
        }
    }

    @Suppress("ComplexMethod")
    private fun getApiValueForFrequency(frequency: TransferRepeat): String {
        return when (frequency) {
            TransferRepeat.None -> "One time"
            TransferRepeat.Week -> "Once a week"
            TransferRepeat.TwoWeeks -> "Every two weeks"
            TransferRepeat.FourWeeks -> "Every four weeks"
            TransferRepeat.Month -> "Once a month"
            TransferRepeat.EndOfMonth -> "End of month"
            TransferRepeat.TwoMonths -> "Every two months"
            TransferRepeat.ThreeMonths -> "Once a quarter"
            TransferRepeat.FourMonths -> "Every four months"
            TransferRepeat.SixMonths -> "Twice a year"
            TransferRepeat.Year -> "Once a year"
        }
    }

    @Suppress("ComplexMethod")
    private fun getDisplayValueForFrequency(frequency: TransferRepeat): String {
        return when (frequency) {
            TransferRepeat.None -> "None"
            TransferRepeat.Week -> "Every week"
            TransferRepeat.TwoWeeks -> "Every two weeks"
            TransferRepeat.FourWeeks -> "Every four weeks"
            TransferRepeat.Month -> "Every month"
            TransferRepeat.EndOfMonth -> "End of every month"
            TransferRepeat.TwoMonths -> "Every 2 months"
            TransferRepeat.ThreeMonths -> "Every 3 months"
            TransferRepeat.FourMonths -> "Every 4 months"
            TransferRepeat.SixMonths -> "Every 6 months"
            TransferRepeat.Year -> "Every year"
        }
    }

    private fun getFromAccount(shortName: String?, fromAccount: String?): String {
        if (!shortName.isNullOrEmpty()) {
            return shortName
        }
        if (!fromAccount.isNullOrEmpty()) {
            return "...${fromAccount.takeLast(LAST_DIGITS_AMOUNT)}"
        }
        return ""
    }

    private fun String.parseAndFormatDate(): String? =
        safeParseStringToDate(this)?.let { shortSimpleDateFormat.get().format(it) }
}
