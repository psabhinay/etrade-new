package com.etrade.mobilepro.calendar

import com.etrade.mobilepro.calendar.adapter.PAGER_ITEMS_COUNT
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import org.threeten.bp.temporal.TemporalAdjusters
import org.threeten.bp.temporal.WeekFields
import java.util.Locale

// to allow calendar viewPager scroll to left and right from the start
const val MIDDLE_OF_CALENDAR_RANGE = PAGER_ITEMS_COUNT / 2 + 1

enum class CalendarMode : CalendarPeriodFunctions {

    TWO_WEEKS {
        override fun getFirstDateOfRange(date: LocalDate, position: Int): LocalDate = date
            .with(TemporalAdjusters.previousOrSame(WeekFields.of(Locale.US).firstDayOfWeek))
            .plusWeeks(getOffset(MIDDLE_OF_CALENDAR_RANGE, position) * 2)

        override fun getLastDateOfRange(date: LocalDate, position: Int): LocalDate = getFirstDateOfRange(date, position)
            .plusDays(DayOfWeek.values().size * 2 - 1L)
    },
    MONTH {
        override fun getFirstDateOfRange(date: LocalDate, position: Int): LocalDate = date
            .withDayOfMonth(1)
            .plusMonths(getOffset(MIDDLE_OF_CALENDAR_RANGE, position))
            .with(TemporalAdjusters.previousOrSame(WeekFields.of(Locale.US).firstDayOfWeek))

        override fun getLastDateOfRange(date: LocalDate, position: Int): LocalDate = date
            .withDayOfMonth(1).plusMonths(getOffset(MIDDLE_OF_CALENDAR_RANGE, position)).with(TemporalAdjusters.lastDayOfMonth())
    }
}
