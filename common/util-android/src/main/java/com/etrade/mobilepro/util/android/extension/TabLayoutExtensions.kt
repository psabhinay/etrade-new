package com.etrade.mobilepro.util.android.extension

import com.etrade.mobilepro.util.android.R
import com.google.android.material.tabs.TabLayout

/**
 * Util method to set content description for tabs, this will set the content description
 * with the current state of the TabLayout so in selection changes it should be called again
 */
fun TabLayout.setContentDescriptionForTabs(forceSelectedIndex: Int? = null) {
    val condition = if (forceSelectedIndex != null && forceSelectedIndex >= 0) {
        { index: Int, _: TabLayout.Tab -> index == forceSelectedIndex }
    } else {
        { _: Int, tab: TabLayout.Tab -> tab.isSelected }
    }
    forEachTab { index, tab ->
        val postfix = if (condition(index, tab)) {
            ""
        } else {
            resources.getString(R.string.tab_unselected)
        }
        tab.contentDescription = resources.getString(R.string.label_tab_selected, tab.text, postfix)
    }
}

/**
 * These click listeners are called before onPageSelected callback,
 * Content description is reset to "TABNAME tab" to avoid TalkBack narration "TABNAME tab unselected selected" on selection.
 * onPageSelected callback is called after the narration happens
 */
fun TabLayout.setupInnerClickListenersForAccessibility() {
    forEachTab { _, tab ->
        tab.view.setOnClickListener {
            tab.contentDescription = resources.getString(R.string.label_tab_selected, tab.text, "")
        }
    }
}

fun TabLayout.forEachTab(action: (Int, TabLayout.Tab) -> Unit) {
    for (i in 0 until tabCount) {
        val currentTab = getTabAt(i)
        if (currentTab == null) {
            throw IndexOutOfBoundsException("There is no tab at index $i")
        } else {
            action(i, currentTab)
        }
    }
}
