package com.etrade.mobilepro.screener.presentation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.RestoreTabPositionDelegate
import com.etrade.mobilepro.screener.presentation.adapter.ScreenersPagerAdapter
import com.etrade.mobilepro.screener.presentation.databinding.FragmentScreenersContainerBinding
import com.etrade.mobilepro.tracking.initTabsTracking
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

class ScreenersContainerFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val restoreTabPositionDelegate: RestoreTabPositionDelegate
) : Fragment(R.layout.fragment_screeners_container) {
    private val binding by viewBinding(FragmentScreenersContainerBinding::bind)

    private val viewModel by viewModels<ScreenersContainerViewModel> { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                setupFragment(it.isAuthenticated)
            }
        )
    }

    private fun setupFragment(isAuthenticated: Boolean) {
        if (isAuthenticated) {
            binding.screenersViewPager.visibility = View.VISIBLE
            binding.tabToggle.visibility = View.VISIBLE
            binding.screenersViewPager.isSaveEnabled = false
            val pagerAdapter = ScreenersPagerAdapter(this)
            binding.screenersViewPager.apply {
                initTabsTracking(viewModel, binding.tabToggle)
                adapter = pagerAdapter
            }

            restoreTabPositionDelegate.init(this, binding.screenersViewPager)
        } else {
            binding.apply {
                screenersViewPager.visibility = View.GONE
                tabToggle.visibility = View.GONE
                screenersViewPager.adapter = null
            }
        }
    }
}
