package com.etrade.mobilepro.util.math

const val PERCENT_MULTIPLIER = 100
const val BOND_MULTIPLIER = 100

/**
 * Calculates change in percentage between the [value] and it's [base].
 *
 * For instance change between value 150 and base 100 is 50%.
 *
 * @param value the value
 * @param base the base against the value is compared
 *
 * @return a percentage (value - base) / base * 100
 */
fun percentageChange(value: Double, base: Double): Double = (value - base) / base * PERCENT_MULTIPLIER
