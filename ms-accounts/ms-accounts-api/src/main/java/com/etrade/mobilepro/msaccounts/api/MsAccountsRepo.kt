package com.etrade.mobilepro.msaccounts.api

import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.util.Resource
import kotlinx.coroutines.flow.Flow

interface MsAccountsRepo {
    fun getMsCompleteView(): Flow<Resource<List<GenericLayout>>>
}
