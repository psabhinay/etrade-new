package com.etrade.mobilepro.tradingdefaults.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.tradingdefaults.TradingDefaultsItem
import com.etrade.mobilepro.tradingdefaults.viewholder.TradingDefaultsPickerViewHolder
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class TradingDefaultsPickerAdapterDelegate :
    AbsListItemAdapterDelegate<TradingDefaultsItem.PickerValue, TradingDefaultsItem, TradingDefaultsPickerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): TradingDefaultsPickerViewHolder {
        return TradingDefaultsPickerViewHolder.from(parent)
    }

    override fun isForViewType(item: TradingDefaultsItem, items: MutableList<TradingDefaultsItem>, position: Int): Boolean =
        item is TradingDefaultsItem.PickerValue

    override fun onBindViewHolder(item: TradingDefaultsItem.PickerValue, holder: TradingDefaultsPickerViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }
}
