package com.etrade.mobilepro.tracking.params

private const val WATCH_LIST_NULL_STATE = "Watch List Null State"

class WatchListNullStateAnalyticsParams(
    override val name: String = WATCH_LIST_NULL_STATE,
    override val params: Map<String, String> = emptyMap()
) : AnalyticsParams
