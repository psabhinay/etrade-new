package com.etrade.mobilepro.portfolio

import android.os.Message
import androidx.lifecycle.MutableLiveData
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.dao.InstrumentRealmObject
import com.etrade.mobilepro.instrument.data.updateWithLevel1Data
import com.etrade.mobilepro.instrument.isOption
import com.etrade.mobilepro.portfolio.livedata.InstrumentsLiveData
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.portfolio.model.createComparator
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import com.etrade.mobilepro.util.SortOrder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.math.BigDecimal

internal fun InstrumentsLiveData.getUpdatedInstrument(symbol: String, data: Level1Data): Instrument? {
    val instruments = value ?: return null
    val instrumentToUpdate = instruments[symbol] ?: return null
    return (instrumentToUpdate as InstrumentRealmObject).updateWithLevel1Data(data)
}

internal fun MutableLiveData<CtaEvent>.postClickEvent(messageType: Int, position: Int, actions: List<ActionItem>? = null) {
    postValue(
        CtaEvent.MessageEvent(
            Message.obtain().apply {
                what = messageType
                arg1 = position
                actions?.let { obj = it }
            }
        )
    )
}

internal suspend fun applySort(
    records: List<PortfolioRecord>,
    sortInfo: SortInfo
): List<PortfolioRecord> = withContext(Dispatchers.Default) {
    records.sortedWith(sortInfo.createComparator())
}

private val defaultSortInfoForListAndTileView = SortInfo(
    PortfolioRecordColumn.SYMBOL,
    SortOrder.ASCENDING
)

internal suspend fun applySort(
    records: List<PortfolioRecord>,
    pair: Pair<SortInfo, PortfolioPositionsDisplayMode>
): List<PortfolioRecord> = withContext(Dispatchers.Default) {
    val sortInfo = if (pair.second == PortfolioPositionsDisplayMode.TABLE) {
        pair.first
    } else {
        defaultSortInfoForListAndTileView
    }
    applySort(records, sortInfo)
}

internal fun extractBaseSymbolAndPrice(
    instrument: Instrument?,
    it: Position,
    instruments: Map<String, Instrument>
) = if (instrument?.isOption == true) {
    extractSymbolFromBaseSymbolPrice(it.baseSymbolAndPrice)?.let { symbol ->
        val underLyer = instruments[symbol]
        formatBaseSymbolPrice(underLyer?.symbol, underLyer?.lastPrice)
    }
} else {
    null
}

private fun formatBaseSymbolPrice(symbol: String?, price: BigDecimal?): String? {
    return if (symbol != null && price != null) {
        "$symbol / ${MarketDataFormatter.formatCurrencyValueWithOutDollarSign(price)}"
    } else {
        null
    }
}

private fun extractSymbolFromBaseSymbolPrice(baseSymbolAndPrice: String?) =
    baseSymbolAndPrice?.split("/")?.getOrNull(0)?.trim()
