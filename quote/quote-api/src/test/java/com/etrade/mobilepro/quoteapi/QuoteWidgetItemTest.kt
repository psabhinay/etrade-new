package com.etrade.mobilepro.quoteapi

import com.etrade.eo.streaming.UpdateInfo
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.lightstreamer.level1.Level1Column
import com.etrade.mobilepro.streaming.lightstreamer.level1.LightstreamerStockData
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.LooperMode

private const val GREEN = 0xFF00FF00.toInt()
private const val RED = 0xFFFF0000.toInt()

@RunWith(RobolectricTestRunner::class)
@Suppress("DEPRECATION")
@LooperMode(value = LooperMode.Mode.LEGACY)
class QuoteWidgetItemTest {

    private val resources = QuoteTemplateResources(
        valueMissing = "--",
        titleFormat = "%1\$s - %2\$s",
        asOfDateTimeFormat = "As of %1\$s",
        nasdaqAsOfDateTimeFormat = "NASDAQ as of %1\$s",
        positiveChangeColorRes = GREEN,
        negativeChangeColorRes = RED,
        neutralChangeColorRes = 0,
        changeFormat = "%s (%2s%%)",
        dateFormat = "%1s/%2s/%3s",
        peFormat = "%.1fx",
        closingPrice = "Closing Price",
        underlyerPrice = "Underlyer Price",
        underlyerClosingPrice = "Underlyer Closing Price",
        fundClassTitle = "Class",
        fundDescriptionFormat = "Category: %1\$s\\n\\n%2\$s",
        fundDescriptionNotFound = "Fund description not found",
        fundPerformanceCategoryFormat = "Morningstar %1\$s",
        priceFormat = "price is %1s",
        sizeFormat = "size is %1s",
        contentDescriptionFormat = "Symbol %1s, Price %2s, Change %3s, Volume %4s, %5s"
    )

    @Test
    fun `check item update`() {
        createItem()
            .apply {
                updateItem(
                    createLevel1Data(
                        mapOf(
                            Level1Column.LAST to "1.25",
                            Level1Column.LAST_CHANGE to "-0.25",
                            Level1Column.LAST_CHANGE_PCT to "-20.00",
                            Level1Column.BID to "1.00",
                            Level1Column.BID_SIZE to "1.00",
                            Level1Column.ASK to "1.00",
                            Level1Column.ASK_SIZE to "1.00",
                            Level1Column.LAST_TIME_MS to "946758896000",
                            Level1Column.ACCUM_VOLUME to "123K"
                        )
                    )
                )
            }
            .run {
                assertEquals("1.25", lastPrice.value?.price)
                assertEquals("-0.25", change.value?.change)
                assertEquals("-20.00", change.value?.changePercent)
                assertEquals(RED, changeColor.value)
                assertEquals("123K", volume.value)
                assertEquals("As of 03:34 PM EST 01/01/2000", asOfDateTime.value)
                assertEquals("1.00", bidPrice.value?.price)
                assertEquals("100.00", bidPrice.value?.size)
                assertEquals("1.00", askPrice.value?.price)
                assertEquals("100.00", askPrice.value?.size)
            }
    }

    @Test
    fun `check last price extension`() {
        checkExtension(
            mapOf(
                Level1Column.LAST to "1.00",
                Level1Column.TICK_INDICATOR to "0"
            ),
            createQuotePriceWidgetItem(), { toLastPrice(it, resources) }
        ) {
            assertEquals("1.00", price)
            assertEquals("A", exchangeCode)
            assertEquals("0", size)
            assertEquals(resources.neutralChangeColorRes, priceColor)
        }
    }

    @Test
    fun `check change extension`() {
        checkExtension(
            mapOf(
                Level1Column.LAST_CHANGE to "1.00",
                Level1Column.LAST_CHANGE_PCT to "100.00"
            ),
            createQuoteChangeWidgetItem(), { toChange(it) }
        ) {
            assertEquals("1.00", change)
            assertEquals("100.00", changePercent)
        }

        checkExtension(mapOf(Level1Column.LAST_CHANGE to "1.00"), createQuoteChangeWidgetItem(), { toChange(it) }) {
            assertEquals("1.00", change)
            assertEquals("0.00", changePercent)
        }

        checkExtension(mapOf(Level1Column.LAST_CHANGE_PCT to "100.00"), createQuoteChangeWidgetItem(), { toChange(it) }) {
            assertEquals("0.00", change)
            assertEquals("100.00", changePercent)
        }
    }

    @Test
    fun `check change color extension`() {
        checkExtension(mapOf(Level1Column.LAST_CHANGE to "0"), RED, { toChangeColor(it, resources) }) {
            assertEquals(GREEN, this)
        }

        checkExtension(mapOf(Level1Column.LAST_CHANGE to "1.23"), RED, { toChangeColor(it, resources) }) {
            assertEquals(GREEN, this)
        }

        checkExtension(mapOf(Level1Column.LAST_CHANGE to "-1.23"), GREEN, { toChangeColor(it, resources) }) {
            assertEquals(RED, this)
        }

        checkExtension(emptyMap(), GREEN, { toChangeColor(it, resources) }) {
            assertEquals(GREEN, this)
        }

        checkExtension(emptyMap(), RED, { toChangeColor(it, resources) }) {
            assertEquals(RED, this)
        }
    }

    @Test
    fun `check bid price extension`() {
        checkExtension(
            mapOf(
                Level1Column.BID to "1.00",
                Level1Column.BID_SIZE to "1.00"
            ),
            createQuotePriceWidgetItem(), { toBidPrice(it) }
        ) {
            assertEquals("1.00", price)
            assertEquals("100.00", size)
        }

        checkExtension(
            mapOf(
                Level1Column.BID to "1.00"
            ),
            createQuotePriceWidgetItem(), { toBidPrice(it) }
        ) {
            assertEquals("1.00", price)
            assertEquals("0", size)
        }

        checkExtension(
            mapOf(
                Level1Column.BID_SIZE to "1.00"
            ),
            createQuotePriceWidgetItem(), { toBidPrice(it) }
        ) {
            assertEquals("0.00", price)
            assertEquals("100.00", size)
        }
    }

    @Test
    fun `check ask price extension`() {
        checkExtension(
            mapOf(
                Level1Column.ASK to "1.00",
                Level1Column.ASK_SIZE to "1.00"
            ),
            createQuotePriceWidgetItem(), { toAskPrice(it) }
        ) {
            assertEquals("1.00", price)
            assertEquals("100.00", size)
        }

        checkExtension(
            mapOf(
                Level1Column.ASK to "1.00"
            ),
            createQuotePriceWidgetItem(), { toAskPrice(it) }
        ) {
            assertEquals("1.00", price)
            assertEquals("0", size)
        }

        checkExtension(
            mapOf(
                Level1Column.ASK_SIZE to "1.00"
            ),
            createQuotePriceWidgetItem(), { toAskPrice(it) }
        ) {
            assertEquals("0.00", price)
            assertEquals("100.00", size)
        }
    }

    @Test
    fun `check extended hours use day change`() {
        checkExtension(
            mapOf(
                Level1Column.MARKET_STATUS to "CLO",
                Level1Column.EXT_HOURS_LAST to "100.00",
                Level1Column.LAST to "50.00",
                Level1Column.LAST_CHANGE to "30.00",
                Level1Column.EXT_HOURS_LAST_CHANGE to "50.00"
            ),
            createQuoteChangeWidgetItem(), { toChange(it, true) }
        ) {
            assertEquals("80.00", change)
            assertEquals("400.0000", changePercent)
        }

        checkExtension(
            mapOf(
                Level1Column.MARKET_STATUS to "CLO",
                Level1Column.EXT_HOURS_LAST to "10.00",
                Level1Column.LAST to "50.00",
                Level1Column.LAST_CHANGE to "30.00",
                Level1Column.EXT_HOURS_LAST_CHANGE to "-40.00"
            ),
            GREEN, { toChangeColor(it, resources, true) }
        ) {
            assertEquals(RED, this)
        }

        checkExtension(
            mapOf(
                Level1Column.MARKET_STATUS to "CLO",
                Level1Column.EXT_HOURS_LAST to "40.00",
                Level1Column.LAST to "50.00",
                Level1Column.LAST_CHANGE to "30.00",
                Level1Column.EXT_HOURS_LAST_CHANGE to "-10.00"
            ),
            createQuoteChangeWidgetItem(), { toChange(it, true) }
        ) {
            assertEquals("20.00", change)
            assertEquals("100.0000", changePercent)
        }
    }

    private fun <T> checkExtension(map: Map<Level1Column, String>, prototype: T, extension: Level1Data.(T) -> T?, assert: T.() -> Unit) {
        val actual = createLevel1Data(map).extension(prototype)
        assertNotNull(actual)
        actual?.run(assert)
    }

    private fun createLevel1Data(map: Map<Level1Column, String>): Level1Data = LightstreamerStockData(createUpdateInfo(map), emptySet(), InstrumentType.EQ)

    private fun createUpdateInfo(map: Map<Level1Column, String>): UpdateInfo {
        val columnToValue = map.mapKeys { (key, _) -> key.column }
        return mock {
            whenever(it.isValueChanged(any<String>())).thenReturn(true)
            whenever(it.getNewValue(any<String>())).thenAnswer { invocation ->
                columnToValue[invocation.arguments[0] as String]
            }
        }
    }

    private fun createQuotePriceWidgetItem(): QuotePriceWidgetItem = QuotePriceWidgetItem("0.00", GREEN, "A", "0")

    private fun createQuoteChangeWidgetItem(): QuoteChangeWidgetItem = QuoteChangeWidgetItem("0.00", "0.00")

    private fun createItem(): QuoteWidgetItem {
        return QuoteWidgetItem(
            title = "title",
            symbol = "UBER",
            instrumentType = InstrumentType.EQ,
            initialLastPrice = createQuotePriceWidgetItem(),
            initialChange = createQuoteChangeWidgetItem(),
            initialChangeColor = GREEN,
            initialVolume = "0",
            initialAsOfDateTime = "As of 03:34 PM EST 01/01/2000",
            initialBidPrice = createQuotePriceWidgetItem(),
            initialAskPrice = createQuotePriceWidgetItem(),
            showVolume = true,
            showBidPrice = true,
            showAskPrice = true,
            resources = resources,
            securityType = "",
            titleContentDescription = "",
            isAffiliatedProduct = false
        )
    }
}
