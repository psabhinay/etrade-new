package com.etrade.mobilepro.balance.section.viewfactory

import com.etrade.mobilepro.balance.data.dto.BalancesDetailsDto
import com.etrade.mobilepro.balance.section.BalancesActionType
import com.etrade.mobilepro.balance.section.BalancesLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

class MarginAccountBalanceSectionViewFactory : BalancesSectionViewFactory {
    override fun createSectionView(
        detailsDto: BalancesDetailsDto,
        resources: BalancesLabelResources,
        actionClickListener: (BalancesActionType) -> Unit
    ): List<TableSectionItemLayout> {
        val sections = mutableListOf<TableSectionItemLayout>()

        // Balance Overview
        sections.addAll(getMarginBalanceOverView(resources, detailsDto.netAccountVal, detailsDto.mktSecVal))

        // Balance details
        sections.addAll(getMarginAccountBalanceSection(resources, detailsDto, actionClickListener))
        return sections
    }
}
