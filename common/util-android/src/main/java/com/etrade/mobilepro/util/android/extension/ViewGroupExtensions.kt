package com.etrade.mobilepro.util.android.extension

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.viewbinding.ViewBinding

/**
 * Inflates [layoutResId] using this [ViewGroup] as a root. Use [attachToRoot] parameter to attach a view to the root.
 *
 * @param layoutResId layout resource ID
 * @param attachToRoot `true` if the view should be attached to the root, `false` (default) otherwise
 *
 * @return newly inflated view
 */
fun ViewGroup.inflate(@LayoutRes layoutResId: Int, attachToRoot: Boolean = false): View =
    inflater.inflate(layoutResId, this, attachToRoot)

val ViewGroup.inflater: LayoutInflater
    get() = LayoutInflater.from(context)

inline fun <T : ViewBinding> ViewGroup.viewBinding(factory: (LayoutInflater, ViewGroup, Boolean) -> T) =
    factory(inflater, this, false)
