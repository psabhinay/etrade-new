package com.etrade.mobilepro.screener.api.filter.stock

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class OpinionsFilter(
    val downgraded: Boolean = false,
    val upgraded: Boolean = false
) : ScreenerFilter {
    override fun isEmpty(): Boolean = !upgraded && !downgraded

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.STOCK
}
