package com.etrade.mobilepro.dialog.viewmodel

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

interface AppDialogViewModel {
    val dialog: LiveData<ConsumableLiveEvent<AppDialog?>?>
}
