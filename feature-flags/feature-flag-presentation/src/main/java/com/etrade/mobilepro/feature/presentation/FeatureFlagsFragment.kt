package com.etrade.mobilepro.feature.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.compose.theme.EtradeTheme
import com.etrade.mobilepro.feature.presentation.compose.FeatureList
import com.etrade.mobilepro.feature.presentation.viewmodel.FeatureFlagsViewModel
import javax.inject.Inject

class FeatureFlagsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory
) : Fragment() {

    private val viewModel: FeatureFlagsViewModel by viewModels { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(inflater.context).apply {
        // Dispose the Composition when the view's LifecycleOwner
        // is destroyed
        setViewCompositionStrategy(DisposeOnViewTreeLifecycleDestroyed)
        setContent {
            EtradeTheme {
                FeatureList(
                    viewModel = viewModel,
                    modifier = Modifier.fillMaxSize()
                )
            }
        }
    }
}
