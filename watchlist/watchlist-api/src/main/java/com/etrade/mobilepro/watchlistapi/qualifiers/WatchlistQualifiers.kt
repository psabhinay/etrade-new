package com.etrade.mobilepro.watchlistapi.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class DefaultWatchlistRepository

@Qualifier
annotation class DeviceWatchlistRepository

@Qualifier
annotation class CombinedWatchlistRepository

@Qualifier
annotation class HasAddedToWatchlist
