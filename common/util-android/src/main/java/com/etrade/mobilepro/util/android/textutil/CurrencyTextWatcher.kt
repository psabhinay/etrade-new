package com.etrade.mobilepro.util.android.textutil

import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.widget.EditText
import java.math.BigDecimal

const val NO_DIGIT_LIMIT = Int.MAX_VALUE

private const val DEFAULT_DIGITS = 12
private const val DEFAULT_DECIMAL_DIGITS = 2
private const val EMPTY_STRING = ""
private const val DEFAULT_INPUT_TYPE = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
private const val PENNY_DIVISOR = 100

class CurrencyTextWatcher(
    autoInstall: Boolean = true,
    inputType: Int = DEFAULT_INPUT_TYPE,
    initialAmountInDollar: BigDecimal? = null,
    private val editText: EditText,
    private val maxDigits: Int = DEFAULT_DIGITS,
    private val maxDecimalDigits: Int = DEFAULT_DECIMAL_DIGITS,
    private val rawValueListener: ((BigDecimal) -> Unit)? = null
) : TextWatcher {

    private var _rawValue: BigDecimal = initialAmountInDollar ?: BigDecimal.ZERO
    val rawValue: BigDecimal get() = _rawValue

    private var skipIteration: Boolean = false
    private var lastDigitsInput: String = EMPTY_STRING
    private val digitsRegex by lazy { Regex("[^0-9]") }

    init {
        if (autoInstall) {
            editText.inputType = inputType
            editText.addTextChangedListener(this)
            placeCursorToTheEnd()
            if (editText.text.isNullOrEmpty() && initialAmountInDollar != null) {
                editText.setText(initialAmountInDollar.toPlainString())
            }
        }
    }

    override fun afterTextChanged(editable: Editable?) {
        if (!skipIteration) {
            skipIteration = true
            processCurrentText(editable.toString())
        } else {
            skipIteration = false
        }
    }

    private fun processCurrentText(text: String) = text
        .replace(digitsRegex, EMPTY_STRING)
        .takeIf { it.isNotEmpty() }
        ?.let { digitsInput ->
            if (digitsInput.length > maxDigits) {
                lastDigitsInput
            } else {
                lastDigitsInput = digitsInput
                digitsInput
            }
        }
        ?.apply { onRawValueChanged(BigDecimal(this).divide(BigDecimal(PENNY_DIVISOR))) }
        ?.run { formatToCurrency(this, maxDecimalDigits) }
        ?.apply(editText::setText)
        ?.apply { placeCursorToTheEnd() }
        ?: run {
            onRawValueChanged(rawValue)
            editText.setText(EMPTY_STRING)
        }

    private fun indexOfLastDigit(value: String): Int {
        var result = 0

        value.forEachIndexed { i, c ->
            if (c.isDigit()) {
                result = i
            }
        }

        return result
    }

    private fun placeCursorToTheEnd() {
        val text = editText.text.toString()
        val cursorPosition = indexOfLastDigit(text) + 1

        if (text.length >= cursorPosition) {
            editText.setSelection(cursorPosition)
        }
    }

    private fun onRawValueChanged(value: BigDecimal) {
        _rawValue = value
        rawValueListener?.invoke(value)
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { /*Not implemented*/ }
    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { /*Not implemented*/ }

    companion object {
        fun noLimit(
            autoInstall: Boolean = true,
            editText: EditText,
            initialValue: BigDecimal? = null,
            rawValueListener: ((BigDecimal) -> Unit)? = null
        ) = CurrencyTextWatcher(
            autoInstall = autoInstall,
            editText = editText,
            maxDigits = NO_DIGIT_LIMIT,
            initialAmountInDollar = initialValue,
            rawValueListener = rawValueListener
        )
    }
}
