package com.etrade.mobilepro.stockplan.holdings

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.di.IsTablet
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.inboxmessages.addInboxMessagesView
import com.etrade.mobilepro.order.details.router.OrderDetailsRouter
import com.etrade.mobilepro.stockplan.AccountIndexHandler
import com.etrade.mobilepro.stockplan.R
import com.etrade.mobilepro.stockplan.StockPlanWebViewFragment
import com.etrade.mobilepro.stockplan.StockPlanWebviewPage
import com.etrade.mobilepro.stockplan.databinding.StockplanHoldingsViewBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

@RequireLogin
class HoldingsFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    @Web private val baseWebUrl: String,
    @DeviceType private val deviceType: String,
    @IsTablet private val isTabletView: Boolean,
    private val accountIndexHandler: AccountIndexHandler,
    mainNavigation: MainNavigation,
    orderDetailsRouter: OrderDetailsRouter
) : StockPlanWebViewFragment(viewModelFactory, deviceType, mainNavigation, orderDetailsRouter) {

    private val binding by viewBinding(StockplanHoldingsViewBinding::bind)
    override val navigableBinding
        get() = binding.navigableContainer

    override val layoutRes = R.layout.stockplan_holdings_view

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addInboxMessagesView(this, binding.inboxMessagesContainer, viewModelFactory)
    }

    override fun getUrl(): String {
        return accountIndexHandler.getAccountIndex()?.let {
            return StockPlanWebviewPage.getUrl(StockPlanWebviewPage.OVERVIEW, it, baseWebUrl, isTabletView)
        } ?: baseWebUrl + StockPlanWebviewPage.OVERVIEW.defaultPlanUrl
    }
}
