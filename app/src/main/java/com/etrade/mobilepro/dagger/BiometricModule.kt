package com.etrade.mobilepro.dagger

import android.content.Context
import androidx.biometric.BiometricManager
import com.etrade.mobilepro.biometric.api.BiometricsProvider
import com.etrade.mobilepro.biometric.data.DefaultBiometricsProvider
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticationDelegate
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticationDelegateImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class BiometricModule {

    @Binds
    @Singleton
    abstract fun bindBiometricAuthenticationDelegate(provider: BiometricAuthenticationDelegateImpl): BiometricAuthenticationDelegate

    @Binds
    abstract fun bindBiometricsProvider(provider: DefaultBiometricsProvider): BiometricsProvider

    companion object {
        @Provides
        fun provideBiometricManager(context: Context): BiometricManager {
            return BiometricManager.from(context)
        }
    }
}
