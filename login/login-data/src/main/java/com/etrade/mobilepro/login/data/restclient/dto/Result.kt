package com.etrade.mobilepro.login.data.restclient.dto

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element

internal data class SessionResult(

    @field:Attribute(name = "code")
    var code: Int? = null,

    @field:Element(name = "Fault")
    var fault: SessionFault? = null

)

internal data class SessionFault(

    @field:Element(name = "Error", required = false)
    var error: SessionError? = null
)

internal data class SessionError(

    @field:Element(name = "ErrorCode")
    var code: Int? = null,

    @field:Element(name = "ErrorType")
    var type: Int? = null,

    @field:Element(name = "ErrorDescription")
    var description: String? = null
)
