package com.etrade.mobilepro.appwidget.indices

import android.content.Context
import android.widget.RemoteViews
import androidx.annotation.WorkerThread
import androidx.core.content.res.ResourcesCompat
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.appwidget.R
import com.etrade.mobilepro.appwidget.common.BaseRemoteViewsFactory
import com.etrade.mobilepro.util.android.ChangeColorPicker

private const val REMOTEVIEWS_COMMAND_DELAY_IN_MILLIS = 25L

@WorkerThread
internal class IndicesRemoteViewsFactory(
    private val context: Context,
    private val changeColorPicker: ChangeColorPicker
) : BaseRemoteViewsFactory<IndexItem> {

    override fun create(item: IndexItem): RemoteViews {
        return RemoteViews(context.packageName, R.layout.appwidget_item_index)
            .apply {
                val index = item.index

                setTextViewText(R.id.tvDisplayName, index.displayName)
                setTextViewText(R.id.tvPrice, MarketDataFormatter.formatMarketData(index.price))
                setTextViewText(R.id.tvChanges, item.formattedIndexChange)

                changeColorPicker.getChangeColor(index.change)
                    .let { ResourcesCompat.getColor(context.resources, it, null) }
                    .let { setTextColor(R.id.tvChanges, it) }
            }
            .also {
                // Readme.md#Note-5
                if (item.position == 0) { Thread.sleep(REMOTEVIEWS_COMMAND_DELAY_IN_MILLIS) }
            }
    }
}
