package com.etrade.mobilepro.watchlist.dto

import com.etrade.mobile.power.backends.neo.BaseUsData
import com.etrade.mobile.power.backends.neo.BaseUsResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SaveWatchlistEntriesResponseDto(
    @Json(name = "data") override var data: SaveWatchlistEntriesDataDto?
) : BaseUsResponse()

@JsonClass(generateAdapter = true)
data class SaveWatchlistEntriesDataDto(
    // No idea why, but message object is here with name as "watchlistEntries"
    @Json(name = "watchlistEntries")
    var message: WatchlistMessage
) : BaseUsData()

@JsonClass(generateAdapter = true)
data class SaveWatchlistEntriesRequestDto(
    @Json(name = "value")
    val value: EditWatchlistEntriesDto
) : BaseUsData()

@JsonClass(generateAdapter = true)
data class EditWatchlistEntriesDto(
    @Json(name = "watchlistEditDetails")
    val watchlistEditDetails: List<EditWatchListEntryDto>?
)
