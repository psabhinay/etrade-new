package com.etrade.mobilepro.trade.presentation.form.delegate

import android.content.Context
import com.etrade.mobilepro.dynamic.form.api.FormBuilder
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.input.ContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.contingentOrderForm
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.contingentOrderHiddenInputs
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.createStockSymbolExternalAction
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.observeContingentSymbolChanges
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter

internal class ContingentStockTradeFormFactoryDelegate(
    private val context: Context,
    private val searchIntentFactory: SearchIntentFactory,
    private val inputId: ContingentStockTradeFormInputId,
    priceTypeDependenciesProvider: FormFactoryDelegateDependencies,
    tradeFormTypeUpdateDelegate: TradeFormTypeUpdateDelegate<StockTradeFormInputId>
) : StockTradeFormFactoryDelegate(
    context = context,
    dependencies = priceTypeDependenciesProvider,
    inputId = inputId,
    searchIntentFactory = searchIntentFactory,
    tradeFormTypeUpdateDelegate = tradeFormTypeUpdateDelegate
) {

    override val formType: TradeFormType = TradeFormType(SecurityType.STOCK, AdvancedTradeType.Contingent)

    override fun advancedOrderFormInputs(groupBuilder: GroupBuilder, parameter: CreateTradeFormParameter) {
        with(groupBuilder) {
            contingentOrderForm(context, inputId, parameter) { symbolInputParams ->
                createStockSymbolExternalAction(
                    context,
                    symbolInputParams,
                    inputId,
                    searchIntentFactory,
                    parameter.onActionClickListener
                )
            }
        }
    }

    override fun advancedOrderRules(ruleBuilder: RuleListBuilder, updateForm: TradeFormFactoryDelegate.OnFormChangeListener) {
        super.advancedOrderRules(ruleBuilder, updateForm)
        ruleBuilder.apply {
            observeContingentSymbolChanges(inputId)
        }
    }

    override fun hiddenInput(formBuilder: FormBuilder) {
        super.hiddenInput(formBuilder)
        formBuilder.contingentOrderHiddenInputs(inputId)
    }
}
