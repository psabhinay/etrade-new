package com.etrade.mobilepro.util.formatters

import com.etrade.eo.core.util.MarketDataFormatter
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class MarketDataFormatterExtensionsTest {
    @Test
    fun testMarketDataFormatterCustom() {
        // test positive values
        assertEquals("1.23", MarketDataFormatter.formatMarketDataCustom(BigDecimal("1.23213")))
        assertEquals("0.9876", MarketDataFormatter.formatMarketDataCustom(BigDecimal("0.98764321")))
        assertEquals("1.00", MarketDataFormatter.formatMarketDataCustom(BigDecimal("0.9999999999")))

        // test negative values behavior
        assertEquals("-30.12", MarketDataFormatter.formatMarketDataCustom(BigDecimal("-30.1234567")))
        assertEquals("-0.7631", MarketDataFormatter.formatMarketDataCustom(BigDecimal("-0.763142123")))
        assertEquals("-0.9999", MarketDataFormatter.formatMarketDataCustom(BigDecimal("-0.99993")))

        // test trailing zeroes omitted
        assertEquals("50.00", MarketDataFormatter.formatMarketDataCustom(BigDecimal("50.0012345")))
        assertEquals("0.67", MarketDataFormatter.formatMarketDataCustom(BigDecimal("0.670003213")))
    }

    @Test
    fun testMarketDataFormatterCustomWithoutDollarSign() {
        assertEquals("1,234,567.00", MarketDataFormatter.formatMarketDataCustomWithoutDollarSign(BigDecimal("1234567")))
        assertEquals("0.5678", MarketDataFormatter.formatMarketDataCustomWithoutDollarSign(BigDecimal("0.56784321")))
        assertEquals("-2,001,234,567.00", MarketDataFormatter.formatMarketDataCustomWithoutDollarSign(BigDecimal("-2001234567")))
        assertEquals("-0.5433", MarketDataFormatter.formatMarketDataCustomWithoutDollarSign(BigDecimal("-0.54326")))
    }

    @Test
    fun testMarketDataFormatterVolumeCustom() {
        assertEquals("10K", MarketDataFormatter.formatVolumeCustom(BigDecimal("10000")))
        assertEquals("500M", MarketDataFormatter.formatVolumeCustom(BigDecimal("500456789")))
        assertEquals("3B", MarketDataFormatter.formatVolumeCustom(BigDecimal("3439127389")))
        assertEquals("7T", MarketDataFormatter.formatVolumeCustom(BigDecimal("7455123456789")))
        assertEquals("1234", MarketDataFormatter.formatVolumeCustom(BigDecimal("1234")))
        assertEquals("9999", MarketDataFormatter.formatVolumeCustom(BigDecimal("9999")))
    }
}
