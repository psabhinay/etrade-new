package com.etrade.mobilepro.login.data.restclient

import com.etrade.eo.rest.retrofit.Scalar
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST

interface LoginService {

    @Suppress("LongParameterList")
    @FormUrlEncoded
    @Scalar
    @POST("/login_mobile.fcc")
    suspend fun login(
        @Header("appversioncode") appversionCode: String,
        @Field("TARGET") target: String,
        @Field("user") username: String,
        @Field("password") password: String,
        @Field("countrylangselect") language: String
    ): String
}
