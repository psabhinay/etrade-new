package com.etrade.mobilepro.accounts.util.android

import android.os.Parcel
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobile.accounts.billpay.BillPay
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BillPayParcelerTest {

    @Test
    fun `data A can be serialized and deserialized`() {
        testData(dataA)
    }

    @Test
    fun `data B can be serialized and deserialized`() {
        testData(dataB)
    }

    @Test
    fun `data C can be serialized and deserialized`() {
        testData(dataC)
    }

    @Test
    fun `data D can be serialized and deserialized`() {
        testData(dataD)
    }

    private fun testData(data: BillPay) {
        val parcel = Parcel.obtain()
        BillPayParceler.run { data.write(parcel, 0) }
        parcel.setDataPosition(0)
        val result = BillPayParceler.create(parcel)
        assertEquals(data, result)
    }

    private val dataA = BillPay(
        isRestricted = true,
        isEnrolled = true,
        isPrimary = true,
        isEligible = true
    )

    private val dataB = BillPay(
        isRestricted = false,
        isEnrolled = false,
        isPrimary = false,
        isEligible = false
    )

    private val dataC = BillPay(
        isRestricted = false,
        isEnrolled = true,
        isPrimary = true,
        isEligible = false
    )

    private val dataD = BillPay(
        isRestricted = true,
        isEnrolled = false,
        isPrimary = false,
        isEligible = true
    )
}
