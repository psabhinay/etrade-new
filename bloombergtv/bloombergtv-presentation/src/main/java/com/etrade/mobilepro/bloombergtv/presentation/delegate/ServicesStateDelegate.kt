package com.etrade.mobilepro.bloombergtv.presentation.delegate

import android.content.Intent
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvActivity
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvAudioService
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvService
import com.etrade.mobilepro.util.android.extension.isServiceRunning

internal class ServicesStateDelegate : BloombergTvActivityDelegate {
    override fun init(activity: BloombergTvActivity) {
        if (activity.isServiceRunning(BloombergTvAudioService::class.java.name)) {
            activity.stopService(Intent(activity, BloombergTvAudioService::class.java))
        }

        if (activity.isServiceRunning(BloombergTvService::class.java.name)) {
            activity.stopService(Intent(activity, BloombergTvService::class.java))
        }
    }
}
