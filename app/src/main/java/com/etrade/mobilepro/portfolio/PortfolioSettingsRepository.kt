package com.etrade.mobilepro.portfolio

import com.etrade.mobilepro.portfolio.model.SortInfo

/**
 * Keeps all settings of portfolio pages in one place.
 */
interface PortfolioSettingsRepository {

    /**
     * A mode for a portfolio positions table.
     */
    var tableMode: PortfolioTableMode

    /**
     * A mode for a portfolio positions.
     */
    var positionsDisplayMode: PortfolioPositionsDisplayMode

    /**
     * Sort info by column and order for portfolio positions
     */
    var sortInfo: SortInfo

    /**
     * Expand/Collapse info for each account
     */
    fun setExpandStatus(accountId: String, value: PositionsState)
    fun getExpandStatus(accountId: String): PositionsState
}
