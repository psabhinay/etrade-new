package com.etrade.mobilepro.dynamic.form.api

import androidx.lifecycle.LiveData

/**
 * A UI component that can be used as part of a dynamic form.
 */
interface Component {
    val isVisible: LiveData<Boolean>
}
