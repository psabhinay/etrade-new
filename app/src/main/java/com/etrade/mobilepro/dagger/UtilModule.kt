package com.etrade.mobilepro.dagger

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.util.DatabaseLocator
import com.etrade.mobilepro.util.DefaultDatabaseLocator
import com.etrade.mobilepro.util.KeyValueStore
import com.etrade.mobilepro.util.updater.AppUpdater
import com.etrade.mobilepro.util.updater.DefaultAppUpdater
import com.etrade.mobilepro.util.updater.DeviceWatchlistMigration
import com.etrade.mobilepro.util.updater.Migration
import com.etrade.mobilepro.util.updater.PortfolioColumnMigration
import com.etrade.mobilepro.util.updater.WatchlistColumnMigration
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet

/* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */

@Module
abstract class UtilModule {

    @Binds
    abstract fun bindAppUpdater(appUpdater: DefaultAppUpdater): AppUpdater

    @Binds
    abstract fun bindDatabaseLocator(databaseLocator: DefaultDatabaseLocator): DatabaseLocator

    @Binds
    @IntoSet
    abstract fun bindDeviceWatchlistMigration(deviceWatchlistMigration: DeviceWatchlistMigration): Migration

    @Binds
    @IntoSet
    abstract fun bindPortfolioColumnMigration(portfolioColumnMigration: PortfolioColumnMigration): Migration

    @Binds
    @IntoSet
    abstract fun bindWatchlistColumnMigration(watchlistColumnMigration: WatchlistColumnMigration): Migration

    companion object {
        @Provides
        fun keyValueStore(keyValueStorage: KeyValueStorage): KeyValueStore {
            return object : KeyValueStore {
                override fun getString(key: String, default: String?): String? {
                    return keyValueStorage.getStringValue(key, default)
                }

                override fun putString(key: String, value: String?) {
                    keyValueStorage.putStringValue(key, value)
                }

                override fun getInt(key: String, default: Int): Int {
                    return keyValueStorage.getIntValue(key, default)
                }

                override fun putInt(key: String, value: Int) {
                    keyValueStorage.putIntValue(key, value)
                }

                override fun getBoolean(key: String, default: Boolean): Boolean {
                    return keyValueStorage.getBooleanValue(key, default)
                }

                override fun putBoolean(key: String, value: Boolean) {
                    keyValueStorage.putBooleanValue(key, value)
                }
            }
        }
    }
}
