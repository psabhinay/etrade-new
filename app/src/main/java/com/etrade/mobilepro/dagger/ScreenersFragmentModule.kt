package com.etrade.mobilepro.dagger

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.screener.presentation.ScreenersContainerFragment
import com.etrade.mobilepro.screener.presentation.disclosure.DisclosureFragment
import com.etrade.mobilepro.screener.presentation.etf.EtfScreenersFragment
import com.etrade.mobilepro.screener.presentation.etf.fundprofile.EtfFundProfileFragment
import com.etrade.mobilepro.screener.presentation.etf.performance.EtfPerformanceFragment
import com.etrade.mobilepro.screener.presentation.etf.portfolio.EtfPortfolioFragment
import com.etrade.mobilepro.screener.presentation.etf.result.EtfResultsFragment
import com.etrade.mobilepro.screener.presentation.etf.risk.EtfRiskFragment
import com.etrade.mobilepro.screener.presentation.etf.technicals.EtfTechnicalsFragment
import com.etrade.mobilepro.screener.presentation.mutualfund.MutualFundScreenersFragment
import com.etrade.mobilepro.screener.presentation.mutualfund.expenses.ExpensesFragment
import com.etrade.mobilepro.screener.presentation.mutualfund.fees.FeesFragment
import com.etrade.mobilepro.screener.presentation.mutualfund.fundprofile.FundProfileFragment
import com.etrade.mobilepro.screener.presentation.mutualfund.performance.PerformanceFragment
import com.etrade.mobilepro.screener.presentation.mutualfund.portfolio.MutualFundPortfolioFragment
import com.etrade.mobilepro.screener.presentation.mutualfund.result.MutualFundResultsFragment
import com.etrade.mobilepro.screener.presentation.mutualfund.risk.RiskFragment
import com.etrade.mobilepro.screener.presentation.stock.StockScreenersFragment
import com.etrade.mobilepro.screener.presentation.stock.earningsanddividends.EarningsAndDividendsFragment
import com.etrade.mobilepro.screener.presentation.stock.fundamentals.FundamentalsFragment
import com.etrade.mobilepro.screener.presentation.stock.marketsegment.MarketSegmentFragment
import com.etrade.mobilepro.screener.presentation.stock.opinions.OpinionsFragment
import com.etrade.mobilepro.screener.presentation.stock.pricevolume.PriceVolumeFragment
import com.etrade.mobilepro.screener.presentation.stock.results.StockResultsFragment
import com.etrade.mobilepro.screener.presentation.stock.technicals.TechnicalsFragment
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("TooManyFunctions")
@Module
abstract class ScreenersFragmentModule {
    @Binds
    @IntoMap
    @FragmentKey(EtfScreenersFragment::class)
    internal abstract fun bindEtfScreenersFragment(fragment: EtfScreenersFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MutualFundScreenersFragment::class)
    internal abstract fun bindMutualFundScreenersFragment(fragment: MutualFundScreenersFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(StockScreenersFragment::class)
    internal abstract fun bindStockScreenersFragment(fragment: StockScreenersFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ScreenersContainerFragment::class)
    internal abstract fun bindScreenersContainerFragment(fragment: ScreenersContainerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DisclosureFragment::class)
    internal abstract fun bindDisclosureFragment(fragment: DisclosureFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MarketSegmentFragment::class)
    internal abstract fun bindMarketSegmentFragment(fragment: MarketSegmentFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PriceVolumeFragment::class)
    internal abstract fun bindPriceVolumeFragment(fragment: PriceVolumeFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MutualFundPortfolioFragment::class)
    internal abstract fun bindMutualFundPortfolioFragment(fragment: MutualFundPortfolioFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(OpinionsFragment::class)
    internal abstract fun bindOpinionsFragment(fragment: OpinionsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(FeesFragment::class)
    internal abstract fun bindFeesFragment(fragment: FeesFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(FundamentalsFragment::class)
    internal abstract fun bindFundamentalsFragment(fragment: FundamentalsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TechnicalsFragment::class)
    internal abstract fun bindTechnicalsFragment(fragment: TechnicalsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EtfRiskFragment::class)
    internal abstract fun bindEtfRiskFragment(fragment: EtfRiskFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(RiskFragment::class)
    internal abstract fun bindRiskFragment(fragment: RiskFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EarningsAndDividendsFragment::class)
    internal abstract fun bindEarningsAndDividendsFragment(fragment: EarningsAndDividendsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EtfPerformanceFragment::class)
    internal abstract fun bindEtfPerformanceFragment(fragment: EtfPerformanceFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(PerformanceFragment::class)
    internal abstract fun bindPerformanceFragment(fragment: PerformanceFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(ExpensesFragment::class)
    internal abstract fun bindExpensesFragment(fragment: ExpensesFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EtfPortfolioFragment::class)
    internal abstract fun bindEtfPortfolioFragment(fragment: EtfPortfolioFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(FundProfileFragment::class)
    internal abstract fun bindFundProfileFragment(fragment: FundProfileFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EtfFundProfileFragment::class)
    internal abstract fun bindEtfFundProfileFragment(fragment: EtfFundProfileFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EtfTechnicalsFragment::class)
    internal abstract fun bindEtfTechnicalsFragment(fragment: EtfTechnicalsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(StockResultsFragment::class)
    internal abstract fun bindStockResultsFragment(fragment: StockResultsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(MutualFundResultsFragment::class)
    internal abstract fun bindMfResultsFragment(fragment: MutualFundResultsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(EtfResultsFragment::class)
    internal abstract fun bindEtfResultsFragment(fragment: EtfResultsFragment): Fragment
}
