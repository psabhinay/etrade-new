package com.etrade.mobilepro.trade.data.customorder

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class CustomOrderStrategyResponseBodyDto(
    @Json(name = "customOrderStrategy")
    val content: CustomOrderStrategyResponseDto?
) : BaseDataDto()

@JsonClass(generateAdapter = true)
internal data class CustomOrderStrategyResponseDto(
    @Json(name = "ordStrategy")
    val ordStrategy: String,
    val legRequestList: List<LegDetailsCommonDto>
)
