package com.etrade.mobilepro.login.usecase

private const val SECURITY_ID_LENGTH = 6

fun extractPassword(userSecret: String, isRsaUser: Boolean): String {
    return if (isRsaUser) {
        userSecret.substring(0, userSecret.length - SECURITY_ID_LENGTH)
    } else {
        userSecret
    }
}
