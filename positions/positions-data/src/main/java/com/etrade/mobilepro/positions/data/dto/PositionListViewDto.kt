package com.etrade.mobilepro.positions.data.dto

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PositionListViewDto(
    @Json(name = "data")
    override val data: PositionListDto?,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?
) : BaseScreenView()
