package com.etrade.mobilepro.documents

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.common.RequireLogin
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId

@RequireLogin
class DateRangeDocumentSelectorFragment : DialogFragment() {

    private val args: DateRangeDocumentSelectorFragmentArgs by navArgs()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val datePicker = DatePickerDialog(
            requireContext(),
            R.style.MaterialDatePicker,
            { _, year, monthOfYear, dayOfMonth ->
                findNavController().previousBackStackEntry?.savedStateHandle?.set<LocalDate>(
                    args.valueKey,
                    LocalDate.of(year, monthOfYear + 1, dayOfMonth)
                )
            },
            args.date.year,
            args.date.monthValue - 1,
            args.date.dayOfMonth
        )

        args.minDate?.atStartOfDay(ZoneId.systemDefault())?.toInstant()?.toEpochMilli()?.let {
            datePicker.datePicker.minDate = it
        }

        args.maxDate?.atStartOfDay(ZoneId.systemDefault())?.toInstant()?.toEpochMilli()?.let {
            datePicker.datePicker.maxDate = it
        }

        datePicker.setCanceledOnTouchOutside(false)

        return datePicker
    }
}
