package com.etrade.mobilepro.util

import java.lang.ref.Reference

fun <T, R> Reference<T>.reference(block: (T) -> R): R? = get()?.let(block)
