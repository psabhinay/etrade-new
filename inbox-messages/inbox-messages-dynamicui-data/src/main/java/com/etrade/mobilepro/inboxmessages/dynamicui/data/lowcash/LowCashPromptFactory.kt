package com.etrade.mobilepro.inboxmessages.dynamicui.data.lowcash

import com.etrade.mobilepro.inboxmessages.InboxAttributes
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.dynamicui.api.lowcash.LowCashPrompt
import javax.inject.Inject

interface LowCashPromptFactory {
    fun create(message: InboxMessage): LowCashPrompt?
}

class DefaultLowCashPromptFactory @Inject constructor() : LowCashPromptFactory {
    override fun create(message: InboxMessage): LowCashPrompt? {
        val inboxMap = message.rawAttributes
        val height = when (inboxMap[InboxAttributes.ASSET_HEIGHT]) {
            LowCashPrompt.Height.SMALL.value -> LowCashPrompt.Height.SMALL
            else -> return null
        }
        val deepLink = message.deepLink ?: return null
        if (!message.hasThumbnail) {
            return null
        }
        val imageUrl = message.thumbnailUrl
        return LowCashPrompt(height, imageUrl, deepLink)
    }
}
