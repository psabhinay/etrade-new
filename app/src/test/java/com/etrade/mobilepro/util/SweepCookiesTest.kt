package com.etrade.mobilepro.util

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import okhttp3.Request
import org.junit.Test

class SweepCookiesTest {

    @Test
    fun shouldSweepCookies() {
        val request = mock<Request>()
        whenever(request.header("Cookie")).thenReturn("aaa=12345; bbb=67890; ccc=1111; ddd=0000")
        val result = request.sweepCookies(setOf("bbb", "ccc"))
        assertEquals("aaa=12345; ddd=0000", result)
    }

    @Test
    fun shouldSweepLastCookie() {
        val request = mock<Request>()
        whenever(request.header("Cookie")).thenReturn("aaa=12345")
        val result = request.sweepCookies(setOf("aaa"))
        assertTrue(result.isEmpty())
    }

    @Test
    fun shouldSweepNothingAtEmptyCookies() {
        val request = mock<Request>()
        whenever(request.header("Cookie")).thenReturn("")
        val result = request.sweepCookies(setOf("aaa"))
        assertTrue(result.isEmpty())
    }

    @Test
    fun shouldSweepNothingAtNullCookies() {
        val request = mock<Request>()
        whenever(request.header("Cookie")).thenReturn(null)
        val result = request.sweepCookies(setOf("aaa"))
        assertTrue(result.isEmpty())
    }
}
