package com.etrade.mobilepro.marketscalendar.fragment

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.marketscalendar.MarketCalendarRouter
import com.etrade.mobilepro.marketscalendar.R
import com.etrade.mobilepro.marketscalendar.api.model.EventsEconomic
import com.etrade.mobilepro.marketscalendar.data.dto.ECONOMIC_EVENT
import com.etrade.mobilepro.marketscalendar.viewmodel.EconomicEventsMarketsCalendarViewModel
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

@RequireLogin
class EconomicMarketsCalendarFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    router: MarketCalendarRouter
) : BaseResultMarketsCalendarFragment<EventsEconomic, EconomicEventsMarketsCalendarViewModel>(viewModelFactory, snackbarUtilFactory, router) {

    override val noResultMessage: Int = R.string.no_result_events

    override val title: Int = R.string.events_details

    override val createViewModel: (ViewModelProvider) -> EconomicEventsMarketsCalendarViewModel =
        { it.get(ECONOMIC_EVENT, EconomicEventsMarketsCalendarViewModel::class.java) }
}
