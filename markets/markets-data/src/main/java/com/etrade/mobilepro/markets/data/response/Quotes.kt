package com.etrade.mobilepro.markets.data.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Quotes(
    @Json(name = "symbol")
    val symbol: String? = null,
    @Json(name = "price")
    val price: String? = null,
    @Json(name = "percentchange")
    val percentchange: String? = null,
    @Json(name = "change")
    val change: String? = null,
    @Json(name = "timestring2")
    val timestamp: String? = null
)
