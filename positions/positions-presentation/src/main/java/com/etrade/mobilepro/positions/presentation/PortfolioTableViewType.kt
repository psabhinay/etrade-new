package com.etrade.mobilepro.positions.presentation

/**
 * Represents view type of a portfolio positions table.
 *
 * Ordering of enum constants matches ordering of tabs. Be mindful when changing it.
 */
enum class PortfolioTableViewType {
    STANDARD_VIEW,
    OPTIONS_VIEW
}
