package com.etrade.mobilepro.watchlist.rest

import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.util.json.JsonMoshi
import com.etrade.mobilepro.watchlist.dto.AddWatchListEntryResponseDto
import com.etrade.mobilepro.watchlist.dto.AddWatchlistEntryRequestDto
import com.etrade.mobilepro.watchlist.dto.CreateWatchlistRequestDto
import com.etrade.mobilepro.watchlist.dto.DeleteWatchlistRequestDto
import com.etrade.mobilepro.watchlist.dto.DeleteWatchlistResponseDto
import com.etrade.mobilepro.watchlist.dto.SaveWatchlistEntriesRequestDto
import com.etrade.mobilepro.watchlist.dto.SaveWatchlistEntriesResponseDto
import com.etrade.mobilepro.watchlist.dto.WatchlistEntriesRequest
import com.etrade.mobilepro.watchlist.dto.WatchlistEntriesResponseDto
import com.etrade.mobilepro.watchlist.dto.WatchlistRequestDto
import com.etrade.mobilepro.watchlist.dto.WatchlistResponseDto
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Watchlist NEO service interface
 */
interface WatchlistApiClient {

    @JsonMoshi
    @POST("/phx/portfolio/etm/services/v2/watchList/all")
    fun getWatchLists(@Body @JsonMoshi requestBody: WatchlistRequestDto? = WatchlistRequestDto()): Single<WatchlistResponseDto>

    @JsonMoshi
    @POST("/app/watchlist/getMBLWatchListEntries.json")
    fun getWatchListEntries(@Body @JsonMoshi requestBody: WatchlistEntriesRequest): Single<WatchlistEntriesResponseDto>

    @JsonMoshi
    @POST("/app/watchlist/createWatchListEntry.json")
    fun addWatchListEntry(@Body @JsonMoshi requestBody: AddWatchlistEntryRequestDto): Single<AddWatchListEntryResponseDto>

    @JsonMoshi
    @POST("/app/watchlist/editWatchListEntries.json")
    fun saveWatchList(@Body @JsonMoshi requestBody: SaveWatchlistEntriesRequestDto): Single<SaveWatchlistEntriesResponseDto>

    @JsonMoshi
    @POST("/phx/etm/services/v1/watchList/create")
    fun createNewWatchList(@Body @JsonMoshi requestBody: CreateWatchlistRequestDto): Single<BaseMobileResponse>

    @JsonMoshi
    @POST("/app/watchlist/deleteWL.json")
    fun deleteWatchList(@Body @JsonMoshi requestBody: DeleteWatchlistRequestDto): Single<DeleteWatchlistResponseDto>
}
