package com.etrade.mobilepro.checkdeposit.presentation.details.holder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.checkdeposit.presentation.databinding.AdapterDepositFooterMessageBinding

class DepositDetailFooterMessageViewHolder(private val binding: AdapterDepositFooterMessageBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(text: String) {
        binding.message.text = text
    }
}
