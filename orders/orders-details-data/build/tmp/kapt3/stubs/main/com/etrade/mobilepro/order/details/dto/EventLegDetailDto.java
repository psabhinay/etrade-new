package com.etrade.mobilepro.order.details.dto;

import com.etrade.mobilepro.backends.neo.BaseDataDto;
import com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto;
import com.etrade.mobilepro.orders.data.dto.AuditInfoDto;
import com.etrade.mobilepro.orders.data.dto.HeaderDto;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b0\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\u009b\u0001\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0001\u0010\b\u001a\u00020\u0003\u0012\b\b\u0001\u0010\t\u001a\u00020\u0003\u0012\b\b\u0001\u0010\n\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u000b\u001a\u00020\u0003\u0012\b\b\u0001\u0010\f\u001a\u00020\u0003\u0012\b\b\u0001\u0010\r\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u000e\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u000f\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0010\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0011\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0012J\t\u0010#\u001a\u00020\u0003H\u00c6\u0003J\t\u0010$\u001a\u00020\u0003H\u00c6\u0003J\t\u0010%\u001a\u00020\u0003H\u00c6\u0003J\t\u0010&\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\'\u001a\u00020\u0003H\u00c6\u0003J\t\u0010(\u001a\u00020\u0003H\u00c6\u0003J\t\u0010)\u001a\u00020\u0003H\u00c6\u0003J\t\u0010*\u001a\u00020\u0003H\u00c6\u0003J\t\u0010+\u001a\u00020\u0003H\u00c6\u0003J\t\u0010,\u001a\u00020\u0003H\u00c6\u0003J\t\u0010-\u001a\u00020\u0003H\u00c6\u0003J\t\u0010.\u001a\u00020\u0003H\u00c6\u0003J\t\u0010/\u001a\u00020\u0003H\u00c6\u0003J\t\u00100\u001a\u00020\u0003H\u00c6\u0003J\t\u00101\u001a\u00020\u0003H\u00c6\u0003J\u009f\u0001\u00102\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00032\b\b\u0003\u0010\u0006\u001a\u00020\u00032\b\b\u0003\u0010\u0007\u001a\u00020\u00032\b\b\u0003\u0010\b\u001a\u00020\u00032\b\b\u0003\u0010\t\u001a\u00020\u00032\b\b\u0003\u0010\n\u001a\u00020\u00032\b\b\u0003\u0010\u000b\u001a\u00020\u00032\b\b\u0003\u0010\f\u001a\u00020\u00032\b\b\u0003\u0010\r\u001a\u00020\u00032\b\b\u0003\u0010\u000e\u001a\u00020\u00032\b\b\u0003\u0010\u000f\u001a\u00020\u00032\b\b\u0003\u0010\u0010\u001a\u00020\u00032\b\b\u0003\u0010\u0011\u001a\u00020\u0003H\u00c6\u0001J\u0013\u00103\u001a\u0002042\b\u00105\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00106\u001a\u000207H\u00d6\u0001J\t\u00108\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014R\u0011\u0010\u0011\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0014R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0014R\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0014R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0014R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0014R\u0011\u0010\b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0014R\u0011\u0010\u000b\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0014R\u0011\u0010\f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0014R\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0014R\u0011\u0010\u000e\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0014R\u0011\u0010\u000f\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010\u0014R\u0011\u0010\t\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\u0014R\u0011\u0010\u0010\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0014\u00a8\u00069"}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/EventLegDetailDto;", "", "averageFillPrice", "", "commissionAmount", "filledQuantity", "optStrikePrice", "orderAction", "otherFee", "secFee", "iofFee", "otherFeeCode", "positionEffect", "qtyReserveShow", "quantityType", "quantityValue", "typeCode", "displaySymbol", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAverageFillPrice", "()Ljava/lang/String;", "getCommissionAmount", "getDisplaySymbol", "getFilledQuantity", "getIofFee", "getOptStrikePrice", "getOrderAction", "getOtherFee", "getOtherFeeCode", "getPositionEffect", "getQtyReserveShow", "getQuantityType", "getQuantityValue", "getSecFee", "getTypeCode", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "orders-details-data"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class EventLegDetailDto {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String averageFillPrice = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String commissionAmount = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String filledQuantity = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String optStrikePrice = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String orderAction = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String otherFee = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String secFee = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String iofFee = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String otherFeeCode = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String positionEffect = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String qtyReserveShow = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String quantityType = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String quantityValue = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String typeCode = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String displaySymbol = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.EventLegDetailDto copy(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "averageFillPrice")
    java.lang.String averageFillPrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "commissionAmount")
    java.lang.String commissionAmount, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "filledQuantity")
    java.lang.String filledQuantity, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optStrikePrice")
    java.lang.String optStrikePrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderAction")
    java.lang.String orderAction, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "otherFee")
    java.lang.String otherFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "secFee")
    java.lang.String secFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "iofFee")
    java.lang.String iofFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "otherFeeCode")
    java.lang.String otherFeeCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "positionEffect")
    java.lang.String positionEffect, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "qtyReserveShow")
    java.lang.String qtyReserveShow, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "quantityType")
    java.lang.String quantityType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "quantityValue")
    java.lang.String quantityValue, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "typeCode")
    java.lang.String typeCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "displaySymbol")
    java.lang.String displaySymbol) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public EventLegDetailDto(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "averageFillPrice")
    java.lang.String averageFillPrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "commissionAmount")
    java.lang.String commissionAmount, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "filledQuantity")
    java.lang.String filledQuantity, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "optStrikePrice")
    java.lang.String optStrikePrice, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderAction")
    java.lang.String orderAction, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "otherFee")
    java.lang.String otherFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "secFee")
    java.lang.String secFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "iofFee")
    java.lang.String iofFee, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "otherFeeCode")
    java.lang.String otherFeeCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "positionEffect")
    java.lang.String positionEffect, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "qtyReserveShow")
    java.lang.String qtyReserveShow, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "quantityType")
    java.lang.String quantityType, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "quantityValue")
    java.lang.String quantityValue, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "typeCode")
    java.lang.String typeCode, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "displaySymbol")
    java.lang.String displaySymbol) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAverageFillPrice() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCommissionAmount() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFilledQuantity() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOptStrikePrice() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOrderAction() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOtherFee() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSecFee() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getIofFee() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getOtherFeeCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component10() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPositionEffect() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQtyReserveShow() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component12() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuantityType() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component13() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getQuantityValue() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component14() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTypeCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component15() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDisplaySymbol() {
        return null;
    }
}