package com.etrade.mobilepro.billpay.usecase

import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.BillPayStatus
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.util.UseCase
import javax.inject.Inject

/**
 * Generates a bill pay status based on a list of accounts.
 */
interface GenerateBillPayStatusUseCase : UseCase<List<AccountDetails>, ETResult<BillPayStatus>>

class GenerateBillPayStatusUseCaseImp @Inject constructor() : GenerateBillPayStatusUseCase {
    override suspend fun execute(parameter: List<AccountDetails>): ETResult<BillPayStatus> {
        var isSupported = false
        var isEligible = false

        for (account in parameter) {
            if (account.billPay?.isRestricted == false) {
                if (account.billPay?.isEnrolled == true) {
                    isSupported = true
                    break
                } else if (account.billPay?.isEligible == true) {
                    isEligible = true
                    break
                }
            }
        }

        val status = when {
            isSupported -> BillPayStatus.Supported
            isEligible || parameter.isEmpty() -> BillPayStatus.NotEnrolled
            else -> BillPayStatus.NotEligible
        }

        return ETResult.success(status)
    }
}
