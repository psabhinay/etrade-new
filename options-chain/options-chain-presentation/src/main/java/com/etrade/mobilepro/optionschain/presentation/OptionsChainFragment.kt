package com.etrade.mobilepro.optionschain.presentation

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.optionschain.presentation.databinding.OptionsChainFragmentBinding
import com.etrade.mobilepro.optionschain.tableview.OptionChainTableViewFragment
import com.etrade.mobilepro.optionschains.api.Option
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.presentation.ConditionalOrderDisclosureViewDelegate
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetClicksViewModel
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetViewDelegate
import com.etrade.mobilepro.util.android.binding.dataBinding
import com.etrade.mobilepro.util.android.clicklisteners.OnViewDebounceClickListener
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.BlockingLoadingIndicatorViewDelegate
import com.etrade.mobilepro.viewdelegate.ViewDelegate
import javax.inject.Inject

class OptionsChainFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val optionsChainRouter: OptionsChainRouter,
    private val isAuthenticated: UserAuthenticationState
) : OptionChainTableViewFragment(
    R.layout.options_chain_fragment,
    viewModelFactory = viewModelFactory,
    snackbarUtilFactory = snackbarUtilFactory,
    optionsChainDatePickerId = R.id.options_chain_date_picker,
    optionsChainSwipeId = R.id.options_chain_swipe_refresh_view,
    optionsChainTableId = R.id.options_chain_table
) {

    private val viewModel: OptionsChainViewModel by viewModels { viewModelFactory }
    private val quoteWidgetViewModel: QuotesWidgetViewModel by viewModels { viewModelFactory }
    private val disclosureViewModel: OptionsChainConditionalOrderDisclosureViewModel by viewModels { viewModelFactory }
    private val actionSheetClicksViewModel: ActionSheetClicksViewModel by viewModels { viewModelFactory }

    private val actionSheetViewDelegate: ActionSheetViewDelegate by lazy {
        ActionSheetViewDelegate(
            viewModel,
            actionSheetClicksViewModel,
            childFragmentManager
        )
    }

    private val viewDelegates: Iterable<ViewDelegate> by lazy {
        listOf(
            actionSheetViewDelegate,
            BlockingLoadingIndicatorViewDelegate(disclosureViewModel, childFragmentManager),
            ConditionalOrderDisclosureViewDelegate(
                requireContext(),
                snackBarUtil,
                disclosureViewModel
            )
        )
    }

    private val args: OptionsChainFragmentArgs by navArgs()

    private val symbol: String get() = args.quoteTicker.underlyerSymbol
    private val type: InstrumentType get() = args.quoteTicker.type

    private val binding by dataBinding(OptionsChainFragmentBinding::bind)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionSheetViewDelegate.onCreate(savedInstanceState)
        loadPage(symbol)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.obj = quoteWidgetViewModel
        binding.disclosureButton.setOnClickListener(
            OnViewDebounceClickListener(
                View.OnClickListener {
                    findNavController().navigate(OptionsChainFragmentDirections.actionToOptionsChainDialog())
                }
            )
        )
        setUpViewModels()
    }

    override fun onOptionChainPageLoad() {
        quoteWidgetViewModel.getQuotes(symbol)
    }

    override fun onOptionChainPageRefresh() {
        quoteWidgetViewModel.getQuotes(symbol)
    }

    private fun setUpViewModels() {
        viewDelegates.forEach {
            it.observe(viewLifecycleOwner)
        }
        viewModel.run {
            navEvent.observe(viewLifecycleOwner, Observer { it.consume(::navigateToLegSelectOption) })
            navToQuoteDetailsSignal.observe(viewLifecycleOwner, Observer { it.consume(::navigateToQuote) })
        }
        viewModel.disclosureViewModel = disclosureViewModel
        quoteWidgetViewModel.errorMessage.observe(viewLifecycleOwner, errorObserver)
    }

    override fun onOptionClicked(option: Option) {
        viewModel.selectOption(option, type)
    }

    private fun navigateToLegSelectOption(navInfo: OptionsChainViewModel.NavInfo) {
        if (isAuthenticated()) {
            navigateToOptionLegSelect(navInfo)
        } else {
            navigateToLogin(navInfo)
        }
    }

    private fun navigateToOptionLegSelect(navInfo: OptionsChainViewModel.NavInfo) {
        if (navInfo.transactionType == null) {
            snackBarUtil.errorSnackbar(getString(R.string.error_message_general))
            return
        }

        navInfo.optionChainType?.let {
            optionsChainRouter.navigateToOptionLegSelect(
                navArgs = TradeNavArgs(
                    navController = findNavController(),
                    symbol = args.quoteTicker,
                    transactionType = navInfo.transactionType,
                    securityType = it.toSecurityType(),
                    option = navInfo.option
                )
            )
        }
    }

    private fun navigateToLogin(navInfo: OptionsChainViewModel.NavInfo) {
        viewModel.savePendingNavInfo(navInfo)
        optionsChainRouter.navigateToLogin(findNavController())
    }

    private fun navigateToQuote(selectedSymbol: SearchResultItem.Symbol) {
        optionsChainRouter.navigateToQuoteDetails(findNavController(), selectedSymbol.copy(underlyerSymbol = symbol, underlyingTypeCode = type))
    }
}
