package com.etrade.mobilepro.optionschain.tableview

import javax.annotation.concurrent.NotThreadSafe

@NotThreadSafe
class RowHeightCache {

    // K - row position, V - height value
    private val rowHeights = mutableMapOf<Int, Int>()

    fun getRowHeight(rowPosition: Int) = rowHeights[rowPosition] ?: 0

    fun findMaxHeight(): Int = rowHeights.values.maxOrNull() ?: 0

    fun updateRowHeight(height: Int, rowPosition: Int) {
        val maxHeight = getRowHeight(rowPosition)
        if (height > maxHeight) {
            rowHeights[rowPosition] = height
        }
    }

    fun clearAllData() {
        rowHeights.clear()
    }
}
