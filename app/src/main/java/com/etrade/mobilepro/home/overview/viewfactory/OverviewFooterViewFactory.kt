package com.etrade.mobilepro.home.overview.viewfactory

import androidx.annotation.ColorInt
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.HasTimeStamp
import com.etrade.mobilepro.common.TimeStampReporter
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicWidget
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.home.overview.dto.OverviewFooterViewDto
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.TimestampEventProvider
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import org.chromium.customtabsclient.shared.WebViewHelper
import javax.inject.Inject
import javax.inject.Provider

class OverviewFooterViewFactory @Inject constructor(
    private val webViewHelper: WebViewHelper,
    private val userViewModel: Provider<UserViewModel>,
    private val timeStampReporter: TimeStampReporter
) : DynamicUiViewFactory {

    private fun isCustomizeViewVisible(): Boolean = with(userViewModel.get()) {
        isAuthenticated && !accountList.isNullOrEmpty()
    }

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is OverviewFooterViewDto) {
            return OverviewFooterViewDelegate(
                viewModelFactory {
                    OverviewFooterViewModel(webViewHelper, isCustomizeViewVisible(), timeStampReporter)
                }
            )
        } else {
            throw DynamicUiViewFactoryException("OverviewFooterViewFactory registered to unknown type")
        }
    }
}

class OverviewFooterViewDelegate(
    override val viewModelFactory: ViewModelProvider.Factory
) : GenericLayout, DynamicWidget {

    override val type = DynamicWidget.Type.DEFAULT
    override val viewModelClass = OverviewFooterViewModel::class.java

    override val uniqueIdentifier: String = javaClass.name

    override fun refresh(viewModel: DynamicViewModel) {
        if (viewModel is OverviewFooterViewModel) {
            viewModel.refresh()
        }
    }
}

class OverviewFooterViewModel(
    private val webViewHelper: WebViewHelper,
    val customizeViewVisible: Boolean,
    private val timeStampReporter: TimeStampReporter
) : DynamicViewModel(R.layout.item_overview_list_footer), HasTimeStamp by timeStampReporter {

    override val variableId: Int = BR.obj

    fun addTimestampEventProviders(providers: List<TimestampEventProvider>) {
        timeStampReporter.addTimestampEventProviders(providers)
    }

    fun refresh() {
        timeStampReporter.refresh()
    }

    fun checkBrokerClicked(url: String, @ColorInt titleBarColor: Int) {
        webViewHelper.openCustomTab(url, titleBarColor)
    }

    fun customizeViewClicked() {
        clickEvents.value = CtaEvent.CustomizeEvent
    }

    fun onDisclosuresClick() {
        clickEvents.value = CtaEvent.Disclosures
    }
}
