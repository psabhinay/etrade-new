package com.etrade.mobilepro.trade.data.mutualfunds

import com.etrade.mobilepro.backends.api.FRONT_LOAD_MF_BREAKPOINT_WARNING
import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.orders.api.FundsOrderActionType
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrderPreviewResponse
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrderResponse
import com.etrade.mobilepro.trade.data.mutualfunds.dto.MutualFundsTradeOrderResponseDto

internal class MutualFundsOrderPreviewResponseWrapper(
    responseDto: MutualFundsTradeOrderResponseDto
) : MutualFundsOrderPreviewResponse, MutualFundsOrderResponse by responseDto {

    override val warnings: Collection<ServerMessage> = responseDto.warnings + additionalMsg(responseDto)

    private fun additionalMsg(response: MutualFundsTradeOrderResponseDto): List<ServerMessage> {
        val order = response.orders.firstOrNull()
        return listOfNotNull(
            when (order?.orderActionType) {
                FundsOrderActionType.BUY, FundsOrderActionType.EXCHANGE -> order.commissionMessage?.let { ServerMessage(ServerMessage.Type.WARN, it, 0) }
                else -> null
            },
            if (response.orderDto?.flConsentNeeded == true) {
                ServerMessage(ServerMessage.Type.WARN, "", 0, FRONT_LOAD_MF_BREAKPOINT_WARNING)
            } else {
                null
            }
        )
    }
}
