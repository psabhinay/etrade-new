package com.etrade.mobilepro.chat.data.repo

import com.etrade.mobilepro.chat.api.ChatGeolocationModel
import com.etrade.mobilepro.chat.api.ChatMetadataModel
import com.etrade.mobilepro.chat.api.ChatParamsDataModel
import com.etrade.mobilepro.chat.api.ChatParamsRequestModel
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType
import com.etrade.mobilepro.chat.api.message.models.ChatRequestModel
import com.etrade.mobilepro.chat.api.setup.models.ChatProductItemModel
import com.etrade.mobilepro.chat.api.setup.models.ChatProductItemsModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatAccountModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatCustomerAccountModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatCustomerDetailsModel
import com.etrade.mobilepro.chat.data.common.dto.ChatMetaDataDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatForMobileResponseDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatInfoDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatRetrieveMessageDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatRetrieveMessageItemDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatRetrievedMessageDataDto
import com.etrade.mobilepro.chat.data.messages.dto.ChatRetrievedMessageDetailsDto
import com.etrade.mobilepro.chat.data.messages.dto.MobileChatDataDto
import com.etrade.mobilepro.chat.data.setup.dto.chatActiveStatus.ChatActiveStatusDataDto
import com.etrade.mobilepro.chat.data.setup.dto.chatActiveStatus.ChatActiveStatusDto
import com.etrade.mobilepro.chat.data.setup.dto.chatActiveStatus.ChatActiveStatusMetadataDto
import com.etrade.mobilepro.chat.data.setup.dto.contactId.ChatContactIdDataDto
import com.etrade.mobilepro.chat.data.setup.dto.contactId.ChatContactIdDto
import com.etrade.mobilepro.chat.data.setup.dto.contactId.ChatContactIdMessagesDto
import com.etrade.mobilepro.chat.data.setup.dto.contactId.ChatItemDto
import com.etrade.mobilepro.chat.data.setup.dto.getChatOperatingHours.ChatOperatingHoursData
import com.etrade.mobilepro.chat.data.setup.dto.getChatOperatingHours.ChatOperatingHoursDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatAccountHolderListDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatCustomerAccountListDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatCustomerDetailsDataDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatCustomerDetailsDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatCustomerDetailsForMobileDto
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatGeoLocationDto
import com.etrade.mobilepro.chat.data.setup.dto.getProducts.ChatProductMessagesDto
import com.etrade.mobilepro.chat.data.setup.dto.getProducts.ChatProductsDataDto
import com.etrade.mobilepro.chat.data.setup.dto.getProducts.ChatProductsDto

internal const val TEST_PRODUCT_ID = "98908983"
internal const val TEST_USER_ID = "293847293"
internal const val TEST_CHAT_CONTACT_ID = "12345678"
internal const val TEST_FIRST_NAME = "TEST_FIRST_NAME"
internal const val TEST_LAST_NAME = "TEST_LAST_NAME"
internal const val TEST_ACC_NUM = "TEST_ACC_NUM"
internal const val TEST_GEOLOCATION_CITY = "New York"
internal const val TEST_GEOLOCATION_COUNTRY_CODE = "US"
internal const val TEST_BUTTON_ID = "TEST_BUTTON_ID"
internal const val TEST_BUTTON_NAME = "TEST_BUTTON_NAME"
internal const val TEST_ACCOUNT_DESCRIPTION = "TEST_ACCOUNT_DESCRIPTION"
internal const val TEST_ERROR_MESSAGE = "testErrorMessage"
internal const val TEST_RETRIEVED_MESSAGE = "TEST_RETRIEVED_MESSAGE"
internal const val TEST_SESSION_ID = "TEST_SESSION_ID"
internal const val TEST_SUCCESS = true
internal const val TEST_MESSAGES = "TEST_MESSAGES"
internal const val TEST_POST_MESSAGE = "TEST_POST_MESSAGE"
internal val chatTestCustomerAccountModel = ChatCustomerAccountModel(
    accNum = TEST_ACC_NUM, instDescription = TEST_ACCOUNT_DESCRIPTION,
    accounts = listOf(
        ChatAccountModel(firstname = TEST_FIRST_NAME, lastname = TEST_LAST_NAME)
    )
)
internal val chatTestGeolocationModel = ChatGeolocationModel(countryCode = TEST_GEOLOCATION_COUNTRY_CODE, city = TEST_GEOLOCATION_CITY)
internal val chatTestCustomerDetailsModel = ChatCustomerDetailsModel(
    userId = TEST_USER_ID,
    customerAccounts = listOf(chatTestCustomerAccountModel),
    userGeolocation = chatTestGeolocationModel
)
internal val chatProductItemModel = ChatProductItemModel(id = TEST_PRODUCT_ID)
internal val chatProductItemsModel = ChatProductItemsModel(listOf(chatProductItemModel))
internal val chatTestMetadataModel = ChatMetadataModel(buttonId = TEST_BUTTON_ID, buttonName = TEST_BUTTON_NAME)
internal val chatTestMessagesData = ChatRequestModel.GetMessages(TEST_SESSION_ID, chatTestMetadataModel)
internal val chatTestPostMessageData = ChatRequestModel.PostMessage(TEST_SESSION_ID, chatTestMetadataModel, TEST_POST_MESSAGE)
internal val chatTestTerminateData = ChatRequestModel.TerminateChat(TEST_SESSION_ID, chatTestMetadataModel)
internal val chatTestRequestParams = ChatParamsRequestModel(
    ChatParamsDataModel(
        firstName = TEST_FIRST_NAME,
        lastName = TEST_LAST_NAME,
        productID = TEST_PRODUCT_ID,
        contactID = TEST_CHAT_CONTACT_ID,
        accountNo = TEST_ACC_NUM,
        isOH = "false",
        isBusinessHrs = "true",
        isStockPlan = "false",
        metadata = chatTestMetadataModel,
        chatGeoLoc = chatTestGeolocationModel
    )
)
internal val chatTestActiveStatusDto = ChatActiveStatusDto(
    ChatActiveStatusDataDto(
        isActive = true,
        metadata = ChatActiveStatusMetadataDto(buttonName = TEST_BUTTON_NAME, buttonId = TEST_BUTTON_ID)
    )
)
internal val chatTestOperatingHoursDto = ChatOperatingHoursDto(
    ChatOperatingHoursData(
        isHoliday = false,
        inOperatingHours = true
    )
)
internal val chatTestNonOperatingHoursDto = ChatOperatingHoursDto(
    ChatOperatingHoursData(
        isHoliday = false,
        inOperatingHours = false
    )
)
internal val chatTestProductsDto = ChatProductsDto(
    ChatProductsDataDto(
        messages = listOf(ChatProductMessagesDto(TEST_PRODUCT_ID))
    )
)
internal val chatTestContactIdDto = ChatContactIdDto(
    ChatContactIdDataDto(
        messages = ChatContactIdMessagesDto(listOf(ChatItemDto(TEST_PRODUCT_ID))),
        metadata = ChatMetaDataDto(buttonName = TEST_BUTTON_NAME, buttonId = TEST_BUTTON_ID)
    )
)
internal val chatTestGeolocationDto = ChatGeoLocationDto(countryCode = TEST_GEOLOCATION_COUNTRY_CODE, city = TEST_GEOLOCATION_CITY)
internal val chatTestCustomerDetailsForMobileDto = ChatCustomerDetailsForMobileDto(
    ChatCustomerDetailsDataDto(
        ChatCustomerDetailsDto(
            userId = TEST_USER_ID, chatGeoLocationDto = chatTestGeolocationDto,
            chatCustomerAccountList = listOf(
                ChatCustomerAccountListDto(
                    accNum = TEST_ACC_NUM, isOH = false, instDescription = TEST_ACCOUNT_DESCRIPTION,
                    acctHolderList = listOf(
                        ChatAccountHolderListDto(firstname = TEST_FIRST_NAME, lastname = TEST_LAST_NAME)
                    )
                )
            )
        )
    )
)
internal val chatTestForMobileDto = ChatForMobileResponseDto(
    data = MobileChatDataDto(
        chatInfoModel = ChatInfoDto(TEST_SESSION_ID),
        messages = TEST_MESSAGES
    ),
    success = true
)
internal val chatTestRetrievedMessageDto = ChatRetrieveMessageDto(
    data = ChatRetrievedMessageDataDto(
        listOf(
            ChatRetrieveMessageItemDto(
                type = ChatRetrieveMessageType.ChatMessage,
                message = ChatRetrievedMessageDetailsDto(text = TEST_RETRIEVED_MESSAGE)
            )
        ),
        1
    ),
    success = true
)
