package com.etrade.mobilepro.appconfig.api.repo

import com.etrade.mobilepro.appconfig.api.AppConfig
import com.etrade.mobilepro.appconfig.api.AppEnvironment
import com.etrade.mobilepro.appconfig.api.BaseUrls
import com.etrade.mobilepro.appconfig.api.Timeouts

interface AppConfigRepo {
    fun getAppConfig(): AppConfig
    fun setEnvironment(environment: AppEnvironment, baseUrls: BaseUrls)
    fun setMarketCircuitBreaker(enabled: Boolean)
    fun setTimeouts(timeouts: Timeouts)
}
