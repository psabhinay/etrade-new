package com.etrade.mobilepro.trade.data

internal object CommonOrderConstants {
    const val PLATFORM = "platform"

    // Order Request Json Field Names
    const val ORDER_TYPE = "orderType"
    const val PREPARED_ID = "preparedId"
    const val REORDER = "reOrder"
}
