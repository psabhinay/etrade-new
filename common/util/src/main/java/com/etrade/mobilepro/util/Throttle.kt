package com.etrade.mobilepro.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

inline fun <T> throttleLast(
    skipMs: Long = 500L,
    coroutineScope: CoroutineScope,
    crossinline destinationFunction: (T) -> Unit
): (T) -> Unit {
    var throttleJob: Job? = null
    return { param: T ->
        if (throttleJob?.isCompleted != false) {
            throttleJob = coroutineScope.launch {
                delay(skipMs)
                destinationFunction(param)
            }
        }
    }
}

inline fun <T> debounce(
    waitMs: Long = 300L,
    coroutineScope: CoroutineScope,
    crossinline destinationFunction: (T) -> Unit
): (T) -> Unit {
    var debounceJob: Job? = null
    return { param: T ->
        debounceJob?.cancel()
        debounceJob = coroutineScope.launch {
            delay(waitMs)
            destinationFunction(param)
        }
    }
}
