package org.chromium.customtabsclient.shared

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.getToolbarTitleContentDescription
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer.getBottomSheetNavContainer
import com.etrade.mobilepro.util.android.binding.viewBinding
import org.chromium.customtabsclient.shared.databinding.FragmentWebviewBinding
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject
import kotlin.math.roundToInt

private const val EXTRA_WEB_VIEW_CONTENT_PROGRESS_PERCENT = "EXTRA_WEB_VIEW_CONTENT_PROGRESS_PERCENT"
private const val TEL_URL_PREFIX = "tel:"

/**
 * Fragment used to navigate to our web sites.
 */
open class WebViewFragment @Inject constructor(
    @DeviceType private val deviceType: String
) : Fragment(), LifecycleObserver {

    protected open val args: WebViewFragmentArgs by navArgs()
    protected val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    private val _binding by viewBinding(FragmentWebviewBinding::bind)
    protected open val webviewBinding: FragmentWebviewBinding
        get() = _binding
    protected val webViewReference: WebView
        get() = webviewBinding.webView

    protected open val layoutRes: Int = R.layout.fragment_webview

    /**
     * Some web pages do not handle JavaScript variable state restoration gracefully, in which case it's better to simply reload the page in entirety.
     */
    protected open val restoreState: Boolean = true

    private val title: String by lazy { args.title ?: "" }
    private val isTitleFixed: Boolean by lazy { args.title != null }
    private val hideTitle: Boolean by lazy { args.hideTitle }

    protected open fun getUrl(): String = args.url ?: ""
    protected open val headers: Map<String, String> get() = emptyMap()

    protected var loading: Boolean = false
        set(value) {
            field = value
            if (view != null) {
                if (value) {
                    webviewBinding.loadingIndicator.show()
                } else {
                    webviewBinding.loadingIndicator.hide()
                }
            }
        }

    private var webViewContentProgressPercent: Float? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(layoutRes, container, false)

    override fun onPause() {
        super.onPause()
        webViewContentProgressPercent = calculateWebViewContentProgressPercent(webViewReference)
    }

    override fun onResume() {
        super.onResume()
        setTitleAndContentDescription()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupWebView(webViewReference)
        refresh(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireActivity().lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onActivityCreated() {
        requireActivity().lifecycle.removeObserver(this)
    }

    private fun refresh(savedInstanceState: Bundle?) {
        loading = true

        if (restoreState) {
            if (savedInstanceState == null) {
                loadUrl(webViewReference)
            } else {
                webViewContentProgressPercent = savedInstanceState.getFloat(EXTRA_WEB_VIEW_CONTENT_PROGRESS_PERCENT)
                webViewReference.restoreState(savedInstanceState)
            }
        } else {
            loadUrl(webViewReference)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        if (restoreState) {
            webViewContentProgressPercent?.let {
                outState.putFloat(EXTRA_WEB_VIEW_CONTENT_PROGRESS_PERCENT, it)
            }
            if (view != null) {
                webViewReference.saveState(outState)
            }
        }
    }

    open fun setupWebView(webView: WebView) {
        webView.apply {
            webViewClient = WebViewClientImpl()
            applySettings(settings)
        }
    }

    open fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?) = false

    // It is deprecated. Remove it once same method in webViewClient no longer needed (min SDK = Android 7)
    open fun shouldOverrideUrlLoading(view: WebView?, url: String?) = false

    protected open fun onWebViewRequest(request: WebResourceRequest?) { /* no op */ }

    @SuppressLint("SetJavaScriptEnabled")
    open fun applySettings(settings: WebSettings) {
        settings.apply {
            builtInZoomControls = true
            displayZoomControls = false
            javaScriptEnabled = true
            allowContentAccess = false
            allowFileAccess = false
            userAgentString = "$userAgentString DType:$deviceType"
        }
    }

    protected open fun loadUrl(webView: WebView) = webView.loadUrl(getUrl(), headers)

    private fun calculateWebViewContentProgressPercent(content: WebView): Float {
        val positionTopView = content.top.toFloat()
        val contentHeight = content.contentHeight.toFloat()
        val currentScrollPosition = content.scrollY.toFloat()
        return if (contentHeight == 0f) {
            0f
        } else {
            (currentScrollPosition - positionTopView) / contentHeight
        }
    }

    private fun setTitleAndContentDescription() {
        if (isTitleFixed && !hideTitle) {
            val toolbarTitleContentDescription =
                if (title.isNotEmpty()) {
                    getToolbarTitleContentDescription(title, requireContext())
                } else {
                    null
                }
            getBottomSheetNavContainer()?.apply {
                // If in bottom sheet, update its title/description
                titleText = title
                titleTextContentDescription = toolbarTitleContentDescription
            } ?: activity?.toolbarTitleView?.apply {
                // Else, update parent activity title/description
                text = title
                contentDescription = toolbarTitleContentDescription
            }
        }
    }

    open inner class WebViewClientImpl : WebViewClient() {
        // to support shouldOverrideUrlLoading call before Android 7
        @Suppress("DEPRECATION")
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return when {
                args.externalUrlView -> {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))

                    true
                }
                url?.startsWith(TEL_URL_PREFIX) == true -> {
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))

                    true
                }
                else -> {
                    if (this@WebViewFragment.shouldOverrideUrlLoading(view, url)) {
                        true
                    } else {
                        super.shouldOverrideUrlLoading(view, url)
                    }
                }
            }
        }

        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            return if (args.externalUrlView) {
                startActivity(Intent(Intent.ACTION_VIEW, request?.url))

                true
            } else {
                if (this@WebViewFragment.shouldOverrideUrlLoading(view, request)) {
                    true
                } else {
                    super.shouldOverrideUrlLoading(view, request)
                }
            }
        }

        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            super.onReceivedError(view, request, error)
            loading = false
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            loading = false
        }

        override fun onPageCommitVisible(view: WebView, url: String) {
            super.onPageCommitVisible(view, url)

            // moving to onPageCommitVisible, web_view.contentHeight is 0 when onPageFinished
            webViewContentProgressPercent?.also { percent ->
                view.scrollTo(0, calculateY(view, percent))
                webViewContentProgressPercent = null
            }
        }

        override fun shouldInterceptRequest(view: WebView?, request: WebResourceRequest?): WebResourceResponse? {
            activity?.runOnUiThread {
                onWebViewRequest(request)
            }
            return super.shouldInterceptRequest(view, request)
        }

        private fun calculateY(content: WebView, progressToRestore: Float): Int {
            val webViewSize = content.contentHeight - content.top
            val positionInWebView = webViewSize * progressToRestore
            return (content.top + positionInWebView).roundToInt()
        }
    }
}
