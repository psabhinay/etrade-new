package com.etrade.mobilepro.instrument

import java.math.BigDecimal

interface Instrument : WithSymbolInfo {
    val displaySymbol: String
    val symbolDescription: String
    val ask: BigDecimal?
    val bid: BigDecimal?
    val priceEarningRation: BigDecimal?
    val earningsPerShare: BigDecimal?
    val fiftyTwoWeekHigh: BigDecimal?
    val fiftyTwoWeekLow: BigDecimal?
    val lastPrice: BigDecimal?
    val tickIndicator: Short?
    val marketCap: BigDecimal?
    val volume: BigDecimal?
    val daysChange: BigDecimal?
    val daysChangePercentage: BigDecimal?
    val delta: BigDecimal?
    val gamma: BigDecimal?
    val theta: BigDecimal?
    val vega: BigDecimal?
    val impliedVolatilityPercentage: BigDecimal?
    val daysToExpire: Int?
    val underlyingSymbolInfo: WithSymbolInfo?
    val previousClose: BigDecimal?
}

val Instrument.safeUnderlyingSymbolInfo: WithSymbolInfo
    get() {
        val underlying = underlyingSymbolInfo
        if (isOption && underlying != null && underlying.instrumentType != InstrumentType.UNKNOWN) {
            return underlying
        }
        return this
    }
