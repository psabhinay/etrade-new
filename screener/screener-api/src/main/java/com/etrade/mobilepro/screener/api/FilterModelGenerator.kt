package com.etrade.mobilepro.screener.api

interface FilterModelGenerator {
    fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams
}
