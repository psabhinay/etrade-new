package com.etrade.mobilepro.chat.data.messages.dto

import androidx.annotation.Keep
import com.etrade.mobilepro.chat.data.setup.dto.getCustomerDetailsForMobile.ChatGeoLocationDto

@Keep
data class ChatParamsRequestDto(var value: ChatParamsDto)

@Keep
data class ChatParamsDto(
    var metadata: ChatMetadataDto?,
    var productID: String? = null,
    var cid: String? = null,
    var lastName: String? = null,
    var isStockPlan: String? = null,
    var isOH: String? = null,
    var isBusinessHrs: String? = null,
    var firstName: String? = null,
    var contactID: String? = null,
    var chatSource: String? = null,
    var chatInfo: String? = null,
    var acctInstName: String? = null,
    var accountNo: String? = null,
    var accountHolderTier: String? = null,
    val chatGeoLoc: ChatGeoLocationDto? = null
)
