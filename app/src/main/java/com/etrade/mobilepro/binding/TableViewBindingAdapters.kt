package com.etrade.mobilepro.binding

import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.tableviewutils.presentation.updateSortViewState
import com.evrencoskun.tableview.TableView
import com.evrencoskun.tableview.listener.ITableViewListener

@BindingAdapter("hasFixedWidth")
fun bindHasFixedWidth(view: TableView, hasFixedWidth: Boolean?) {
    view.setHasFixedWidth(hasFixedWidth ?: false)
}

@BindingAdapter("ignoreSelectedColors")
fun bindIgnoreSelectedColors(view: TableView, ignoreSelectedColors: Boolean?) {
    view.isIgnoreSelectionColors = ignoreSelectedColors ?: false
}

@BindingAdapter("rowWidthPercentage")
fun bindRowWidthPercentage(view: TableView, percentage: Float?) {
    if (percentage != null) {
        view.rowHeaderWidth = (view.context.resources.displayMetrics.widthPixels * percentage).toInt()
    }
}

@BindingAdapter("tableViewListener")
fun bindTableViewListener(view: TableView, tableViewListener: ITableViewListener?) {
    view.tableViewListener = tableViewListener
}

@BindingAdapter("sortStateInfo")
fun setSortStateInfo(tv: TableView, sortInfo: SortStateInfo?) {
    tv.updateSortViewState(sortInfo ?: SortStateInfo())
}
