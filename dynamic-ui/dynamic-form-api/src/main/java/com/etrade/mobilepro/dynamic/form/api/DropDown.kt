package com.etrade.mobilepro.dynamic.form.api

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.dynamic.form.api.util.toDropDownOption
import com.etrade.mobilepro.livedata.combineWith
import com.etrade.mobilepro.livedata.setDistinctValue

/**
 * @param options list of available dropdown values
 * @param initialOptionId initially selected value which unlike defaultSelectedOptionId doesn't survive reset.
 * @param defaultSelectedOptionId option that dropdown falls back to if the dropdown selection is reset.
 * This option is also selected by default when dropdown is created if initialOptionId is not specified.
 * @param doNotReset prevents selected dropdown value from being reset
 */
class DropDown(
    options: LiveData<List<DropDownOption>>,
    initialOptionId: String?,
    private val defaultSelectedOptionId: String?,
    private val doNotReset: Boolean = false
) {

    constructor(
        options: List<DropDownOption>,
        initialOptionId: String?,
        defaultSelectedOptionId: String?,
        doNotReset: Boolean
    ) : this(MutableLiveData(options), initialOptionId, defaultSelectedOptionId, doNotReset)

    constructor(
        options: LiveData<List<Int>>,
        initialOptionId: String?,
        defaultSelectedOptionId: Int?,
        resources: Resources
    ) : this(options.map { list -> list.map { it.toDropDownOption(resources) } }, initialOptionId, defaultSelectedOptionId?.toString())

    constructor(
        options: List<Int>,
        initialOptionId: String?,
        defaultSelectedOptionId: Int?,
        resources: Resources
    ) : this(MutableLiveData(options), initialOptionId, defaultSelectedOptionId, resources)

    private val selectedOptionId: MutableLiveData<String?> = MutableLiveData(initialOptionId ?: defaultSelectedOptionId)

    val selectedOption: LiveData<DropDownOption?> = options.combineWith(selectedOptionId) { options, selectedOptionId ->
        var candidate = options.find { it.id == selectedOptionId }
        if (candidate == null && selectedOptionId != null) {
            candidate = options.find { it.id == previouslySelectedOptionId }
        }
        candidate
    }
    val optionList: LiveData<DropDownOptionList> = options.combineWith(selectedOption) { options, selectedOption ->
        DropDownOptionList(
            dropDownOptions = options,
            selectedOptionIndex = options.indexOf(selectedOption)
        )
    }

    private var previouslySelectedOptionId: String? = null

    fun resetValue() {
        if (!doNotReset) {
            selectOption(defaultSelectedOptionId)
        }
    }

    fun selectOption(option: DropDownOption) = selectOption(option.id)

    fun selectOption(id: String?) {
        previouslySelectedOptionId = selectedOptionId.value
        selectedOptionId.setDistinctValue(id)
    }
}
