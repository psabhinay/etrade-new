package com.etrade.mobilepro.completeview

import androidx.lifecycle.LiveData
import androidx.lifecycle.toLiveData
import com.etrade.completeview.dto.AccountSummarySectionDto
import com.etrade.eo.core.util.nullToEmpty
import com.etrade.mobilepro.backends.mgs.MOVEMENT_TYPE_GAIN
import com.etrade.mobilepro.backends.mgs.MOVEMENT_TYPE_LOSS
import com.etrade.mobilepro.backends.mgs.MOVEMENT_TYPE_NEUTRAL
import com.etrade.mobilepro.backends.mgs.StreamingValueDto
import com.etrade.mobilepro.completeview.model.AccountSummarySection
import com.etrade.mobilepro.streaming.api.StreamingText
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import io.reactivex.BackpressureStrategy

fun List<AccountSummarySectionDto>.toAccountSummarySections(
    streamingTextManager: StreamingTextManager,
    accountId: String?,
    isAccountStreamingAllowed: Boolean
): List<AccountSummarySection> {
    return map {
        it.toAccountSummarySection(streamingTextManager, accountId, isAccountStreamingAllowed)
    }
}

fun AccountSummarySectionDto.toAccountSummarySection(
    streamingTextManager: StreamingTextManager,
    accountId: String?,
    isAccountStreamingAllowed: Boolean
): AccountSummarySection = AccountSummarySection(
    title,
    getStreamingText(
        streamingTextManager,
        toStreamingValueDto(accountId),
        isAccountStreamingAllowed = isAccountStreamingAllowed
    )
)

fun AccountSummarySectionDto.toStreamingValueDto(accountId: String?): StreamingValueDto {
    return StreamingValueDto(
        initial = "${value.initial}${valueDetail?.let { "\n($it) " }.nullToEmpty()}",
        streamId = "${value.streamId}:$accountId",
        movementType = value.movementType ?: MOVEMENT_TYPE_NEUTRAL
    )
}

fun getStreamingText(
    streamingTextManager: StreamingTextManager,
    streamingValueDto: StreamingValueDto,
    isAccountStreamingAllowed: Boolean
): LiveData<StreamingText> {
    return streamingTextManager.getStreamedText(
        streamId = streamingValueDto.streamId.nullToEmpty(),
        initialValue = streamingValueDto.initial ?: "--",
        movementType = streamingValueDto.getResolvedMovementType(),
        isAccountStreamingAllowed = isAccountStreamingAllowed
    )
        .toFlowable(BackpressureStrategy.LATEST)
        .toLiveData()
}

fun StreamingValueDto.getResolvedMovementType(): StreamingTextManager.MovementType {
    return when (movementType) {
        MOVEMENT_TYPE_LOSS -> StreamingTextManager.MovementType.LOSS
        MOVEMENT_TYPE_GAIN -> StreamingTextManager.MovementType.GAIN
        else -> StreamingTextManager.MovementType.NEUTRAL
    }
}
