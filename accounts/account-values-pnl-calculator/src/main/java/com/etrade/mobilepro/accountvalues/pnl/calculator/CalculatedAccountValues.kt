package com.etrade.mobilepro.accountvalues.pnl.calculator

import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult.CalculatedAccountValuesVO

data class CalculatedAccountValues(
    val accountValues: CalculatedAccountValuesVO,
    val timeStamp: Long
)
