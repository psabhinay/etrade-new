package com.etrade.mobilepro.balance.caching

import com.etrade.mobilepro.balance.data.AccountBalances
import com.etrade.mobilepro.balance.data.dto.AccountBalancesResponse
import com.etrade.mobilepro.balance.data.repo.AccountBalancesRepo
import com.etrade.mobilepro.balance.data.repo.BalanceRequest
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.dynamicui.CachedResponseModel
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import org.simpleframework.xml.convert.AnnotationStrategy
import org.simpleframework.xml.core.Persister
import java.io.IOException
import javax.inject.Inject

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AccountBalancesRepoImpl @Inject constructor(
    private val cachingDataDelegate: SimpleCachedResource.DataSourceDelegate<CachedResponseModel, BalanceRequest>
) : AccountBalancesRepo {
    override fun getBalances(request: BalanceRequest, cacheExpiration: Long?): Observable<Resource<AccountBalances>> =
        SimpleCachedResource(
            id = request,
            dataSourceDelegate = cachingDataDelegate,
            controller = object : SimpleCachedResource.Controller<CachedResponseModel> {
                override fun shouldRefresh(itemFromCache: CachedResponseModel): Boolean {
                    return cacheExpiration?.let {
                        System.currentTimeMillis() - itemFromCache.retrievedTimestamp >= it
                    } ?: true
                }
            },
            showCachedData = true
        ).asObservable().map { cachedResponse ->
            try {
                val accountBalance = cachedResponse.data?.response?.let {
                    getBalanceFromResponseString(it)
                }

                when (cachedResponse) {
                    is Resource.Loading -> Resource.Loading()
                    is Resource.Success -> Resource.Success(accountBalance)
                    is Resource.Failed -> {
                        Resource.Failed(accountBalance, cachedResponse.error)
                    }
                }
            } catch (e: IOException) {
                Resource.Failed<AccountBalances>(error = e)
            }
        }

    private fun getBalanceFromResponseString(xml: String): AccountBalances? {
        val clazz = AccountBalancesResponse::class.java
        val persister = Persister(AnnotationStrategy())
        val deserilizedClass = persister.read(clazz, xml, false)
        return deserilizedClass?.balances?.let { it1 -> AccountBalances(it1) }
    }
}
