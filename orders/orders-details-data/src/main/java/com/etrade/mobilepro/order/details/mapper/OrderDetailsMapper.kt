package com.etrade.mobilepro.order.details.mapper

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.eo.core.util.isZero
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.OptionType
import com.etrade.mobilepro.instrument.getConstructedOsiSymbol
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderDetailDescription
import com.etrade.mobilepro.order.details.api.OrderDetailDescriptionMapperType
import com.etrade.mobilepro.order.details.api.OrderHistoryItem
import com.etrade.mobilepro.order.details.api.StockPlanOrderCancelResponse
import com.etrade.mobilepro.order.details.dto.EventDto
import com.etrade.mobilepro.order.details.dto.LegDetailsDto
import com.etrade.mobilepro.order.details.dto.OrderDetailsDto
import com.etrade.mobilepro.order.details.dto.OrderHistory
import com.etrade.mobilepro.order.details.dto.OrdersDto
import com.etrade.mobilepro.order.details.dto.StockPlanOrderCancelResponseDto
import com.etrade.mobilepro.order.details.dto.StockPlanOrderDetailsResponseDto
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.ContingentCondition
import com.etrade.mobilepro.orders.api.ContingentQualifier
import com.etrade.mobilepro.orders.api.EventType
import com.etrade.mobilepro.orders.api.ExecInstruction
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.orders.api.MarketSession
import com.etrade.mobilepro.orders.api.OrderAction
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto
import com.etrade.mobilepro.orders.data.dto.parseOrderStatusForStockPlan
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.util.safeParseBigDecimal
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

const val RESERVELIMIT_QUANTITY_FORMAT = "%s/%sR"
const val QUANTITY_FORMAT = "%s/%s"
const val DATETIME_FORMAT = "%s %s"
private const val EMPTY_VALUE = MarketDataFormatter.EMPTY_FORMATTED_VALUE

@Suppress("LargeClass")
class OrderDetailsMapper(private val orderDetailDescriptionMapperType: OrderDetailDescriptionMapperType) {
    fun map(orderDetailsDto: OrderDetailsDto): List<BaseOrderDetailsItem> {
        val orderDetails = orderDetailsDto.orderDetails
        val orders = orderDetails.orders
        val events = orderDetailsDto.events

        val orderDetailsItemList = orders.map { order ->
            val orderEvents = events.filter {
                it.header.groupOrderNo == order.header.groupOrderNo
            }
            val advancedOrderItem = getAdvancedOrderItem(order)
            val priceType = getPriceType(order, advancedOrderItem)
            val orderType = OrderType.fromTypeCode(order.header.orderType)
            val legDetailsDto = order.legDetails
            val orderDescriptions = getOrderDescriptions(legDetailsDto, events)
            val orderQuantity = getQuantity(orderDescriptions, priceType)
            val executionEvents = events.filter {
                it.header.eventType == EventType.EXECUTED.typeCode || it.header.eventType == EventType.DONE_TRADE_EXECUTED.typeCode
            }
            val marketSession = MarketSession.getMarketSession(order.header.marketSession.toIntOrNull())
            val execInstruction = order.header.execInstruction.run { ExecInstruction(aonFlag == 1, nhFlag == 1, dnrFlag == 1, dniFlag == 1) }
            val goodTillDateFlag = order.header.gtdFlag == 1
            val hasPartialExecution = orderDescriptions.any { it.isPartialOrder } ||
                (executionEvents.isNotEmpty() && executionEvents.size < legDetailsDto.size)
            val tradeLegs = createLegs(orderDescriptions, hasPartialExecution)
            BaseOrderDetailsItem.OrderDetailsItem(
                orderStatus = OrderStatus.fromStatusCode(order.header.orderStatus),
                orderType = orderType,
                priceType = priceType,
                quantity = orderQuantity,
                orderNumber = orderDetails.orderNumber,
                orderPlacedDate = order.header.timePlaced.toLongOrNull()?.takeIf { it != 0L }?.let { getTimeString(it) },
                executedDate = getEventDate(orderEvents, EventType.EXECUTED),
                term = OrderTerm.getOrderTerm(order.header.orderTerm, marketSession, goodTillDateFlag, execInstruction, order.header.orderSpecialAttribCd),
                priceExecuted = getLatestExecutionPrice(legDetailsDto, order.header.netPrice),
                commission = calculateCommissionFees(legDetailsDto),
                estimatedCommission = order.header.totalCommission.safeParseBigDecimal(),
                estimatedTotalCost = order.header.orderValue.safeParseBigDecimal(),
                cancelledDate = getEventDate(orderEvents, EventType.CANCEL_CONFIRMED),
                cancelledRequestedDate = getEventDate(orderEvents, EventType.CANCEL_REQUESTED),
                account = orderDetails.accountId,
                orderDescriptions = orderDescriptions,
                limitPrice = order.header.limitPrice.safeParseBigDecimal()?.takeIf { it.signum() > 0 },
                expiredData = getEventDate(orderEvents, EventType.EXPIRED),
                orderAction = orderDescriptions[0].transactionType,
                netAsk = order.header.netAsk.safeParseBigDecimal() ?: BigDecimal.ZERO,
                netBid = order.header.netBid.safeParseBigDecimal() ?: BigDecimal.ZERO,
                stopPrice = order.header.stopPrice.safeParseBigDecimal()?.takeIf { it.signum() > 0 },
                strategyType = StrategyType.fromTypeCode(order.header.strategyType).takeIf { it != StrategyType.UNKNOWN },
                systemRejectedData = getEventDate(orderEvents, EventType.SYSTEM_REJECTED),
                orderSavedDate = order.auditInfo.createUtc.toLongOrNull()?.takeIf { it != 0L }?.let { getDateString(it) },
                preparedOrderId = orderDetailsDto.preparedOrderId,
                hasPartialExecution = hasPartialExecution,
                goodTillDateFlag = goodTillDateFlag,
                gtdExpirationData = order.header.expireDate.toLongOrNull()?.takeIf { it != 0L }?.let { getDateString(it) },
                marketSession = marketSession,
                marketDestination = MarketDestination.getMarketDestination(order.header.destMktCode),
                origSysCode = OrigSysCode.fromOrderDetailResponse(order.header.origSysCode),
                execInstruction = execInstruction,
                limitOffsetValue = order.header.limitOffsetValue.safeParseBigDecimal(),
                advancedItem = advancedOrderItem,
                hasDependentFlag = order.header.hasDependentFlag.toIntOrNull() == 1,
                dependentOrderNumber = order.header.dependentOrderNo.toIntOrNull() ?: Int.MIN_VALUE,
                hiddenFlag = order.header.hiddenFlag.toIntOrNull() == 1,
                orderSpecialAttribCd = order.header.orderSpecialAttribCd,
                tradeLegs = tradeLegs
            )
        }

        val result = mutableListOf<BaseOrderDetailsItem>()
        orderDetailsItemList.map {
            result.add(it)
            it.advancedItem?.let { advancedItem ->
                if (AdvancedOrderType.contingentOrderTypes.contains(advancedItem.orderType)) {
                    result.add(advancedItem)
                }
            }
        }
        return result
    }

    private fun createLegs(orderDescriptions: List<OrderDetailDescription>, hasPartialExecution: Boolean) =
        mutableListOf<TradeLeg>().apply {
            orderDescriptions.forEach {
                add(orderDetailDescriptionMapperType(it, hasPartialExecution))
            }
        }

    @Suppress("LongMethod") // Lots of values being mapped
    fun mapStockPlanOrderDetails(detailsDto: StockPlanOrderDetailsResponseDto, symbol: String): BaseOrderDetailsItem.StockPlanOrderDetailsItem {
        // NOTE: The use of lists for singular values is due to the server occasionally returning a response in which there are
        // duplicate entries for each value. This confuses SimpleXML and causes it to fail in parsing unless the element is mapped to a list.
        return BaseOrderDetailsItem.StockPlanOrderDetailsItem(
            orderHistoryList = detailsDto.orderHistoryList?.map { mapOrderHistory(it) } ?: emptyList(),
            orderNumber = detailsDto.orderNumber?.firstOrNull() ?: EMPTY_VALUE,
            orderType = detailsDto.orderType?.firstOrNull() ?: EMPTY_VALUE,
            orderStatus = parseOrderStatusForStockPlan(detailsDto.orderStatus?.firstOrNull()),
            description = detailsDto.description?.firstOrNull()?.trim() ?: EMPTY_VALUE,
            term = detailsDto.term?.firstOrNull() ?: EMPTY_VALUE,
            gtdExpiration = detailsDto.gtdExpiration?.firstOrNull() ?: EMPTY_VALUE,
            sharesExercised = detailsDto.sharesExercised?.firstOrNull() ?: EMPTY_VALUE,
            sharesSold = detailsDto.sharesSold?.firstOrNull() ?: EMPTY_VALUE,
            sharesToExercise = detailsDto.sharesToExercise?.firstOrNull() ?: EMPTY_VALUE,
            sharesToSell = detailsDto.sharesToSell?.firstOrNull() ?: EMPTY_VALUE,
            estSharesToSell = detailsDto.estSharesToSell?.firstOrNull() ?: EMPTY_VALUE,
            priceType = detailsDto.priceType?.firstOrNull() ?: EMPTY_VALUE,
            limitPrice = detailsDto.limitPrice?.firstOrNull() ?: EMPTY_VALUE,
            grossProceeds = detailsDto.grossProceeds?.firstOrNull()?.trim() ?: EMPTY_VALUE,
            estGrossProceeds = detailsDto.estGrossProceeds?.firstOrNull()?.trim() ?: EMPTY_VALUE,
            totalPrice = detailsDto.totalPrice?.firstOrNull() ?: EMPTY_VALUE,
            commission = detailsDto.commission?.firstOrNull() ?: EMPTY_VALUE,
            secFee = detailsDto.secFee?.firstOrNull() ?: EMPTY_VALUE,
            brokerAssistFee = detailsDto.brokerAssistFee?.firstOrNull() ?: EMPTY_VALUE,
            disbursementFee = detailsDto.disbursementFee?.firstOrNull() ?: EMPTY_VALUE,
            taxesWithheld = detailsDto.taxesWithheld?.firstOrNull() ?: EMPTY_VALUE,
            estNetProceeds = detailsDto.estNetProceeds?.firstOrNull()?.trim() ?: EMPTY_VALUE,
            netProceeds = detailsDto.netProceeds?.firstOrNull()?.trim() ?: EMPTY_VALUE,
            exerciseValue = detailsDto.exerciseValue?.firstOrNull() ?: EMPTY_VALUE,
            sharesToKeep = detailsDto.sharesToKeep?.firstOrNull() ?: EMPTY_VALUE,
            estSharesToKeep = detailsDto.estSharesToKeep?.firstOrNull() ?: EMPTY_VALUE,
            sharesIssued = detailsDto.sharesIssued?.firstOrNull() ?: EMPTY_VALUE,
            sharesToIssue = detailsDto.sharesToIssue?.firstOrNull() ?: EMPTY_VALUE,
            appreciationSharesValue = detailsDto.appreciationSharesValue?.firstOrNull() ?: EMPTY_VALUE,
            estAppreciationSharesValue = detailsDto.estAppreciationSharesValue?.firstOrNull() ?: EMPTY_VALUE,
            taxSharesValue = detailsDto.taxSharesValue?.firstOrNull() ?: EMPTY_VALUE,
            estTaxSharesValue = detailsDto.estTaxSharesValue?.firstOrNull() ?: EMPTY_VALUE,
            planPrice = detailsDto.planPrice?.firstOrNull() ?: EMPTY_VALUE,
            grantType = detailsDto.grantType?.firstOrNull() ?: EMPTY_VALUE,
            planNumber = detailsDto.planNumber?.firstOrNull() ?: EMPTY_VALUE,
            planDate = detailsDto.planDate?.firstOrNull() ?: EMPTY_VALUE,
            vestPeriod = detailsDto.vestPeriod?.firstOrNull() ?: EMPTY_VALUE,
            costBasis = detailsDto.costBasis?.firstOrNull() ?: EMPTY_VALUE,
            planType = detailsDto.planType?.firstOrNull() ?: EMPTY_VALUE,
            symbol = symbol
        )
    }

    private fun mapOrderHistory(dto: OrderHistory): OrderHistoryItem {
        return OrderHistoryItem(
            price = dto.price ?: "",
            transaction = dto.transaction ?: "",
            sellQuantity = dto.sellQuantity ?: "",
            date = trimUnwantedDateCharacters(dto.date)
        )
    }

    private fun trimUnwantedDateCharacters(text: String?): String {
        return text?.replace(" <br /> ", "")?.replace("&nbsp;", " ") ?: EMPTY_VALUE
    }

    @SuppressWarnings("LongMethod")
    private fun getAdvancedOrderItem(order: OrdersDto): BaseOrderDetailsItem.AdvancedOrderItem? {
        val advancedOrderType = AdvancedOrderType.fromTypeCode(order.advancedOrder.aoType)
        return if (advancedOrderType != AdvancedOrderType.UNKNOWN) {
            val contingentCondition = ContingentCondition.fromOffsetType(order.advancedOrder.offsetType)
            val contingentQualifier = ContingentQualifier.fromFollowPrice(order.advancedOrder.followPrice)
            val advancedValue = order.advancedOrder.offsetValue
            val displaySymbol = order.advancedOrder.aoCondDisplaySymbol
            val symbol = order.advancedOrder.resolveSymbol()
            val offsetType = order.advancedOrder.offsetType
            var price: BigDecimal? = if (advancedOrderType == AdvancedOrderType.AO_SIMPLE_STOP) { // potentially there are more cases
                order.advancedOrder.initialStopPrice.safeParseBigDecimal()
            } else {
                null
            }
            var offsetValue: BigDecimal? = null
            if (AdvancedOrderType.trailOrderTypes.contains(advancedOrderType)) {
                val initialStopPrice = order.advancedOrder.initialStopPrice.safeParseBigDecimal()
                val priceTrail = order.advancedOrder.priceTrail.safeParseBigDecimal() ?: BigDecimal.ZERO
                price = if (!priceTrail.isZero()) {
                    priceTrail
                } else {
                    initialStopPrice
                }
                offsetValue = order.advancedOrder.offsetValue.safeParseBigDecimal()
            }
            BaseOrderDetailsItem.AdvancedOrderItem(
                contingentQualifier, contingentCondition, advancedValue, symbol, displaySymbol, advancedOrderType, offsetType, offsetValue, price
            )
        } else {
            null
        }
    }

    private fun getQuantity(orderDescriptions: List<OrderDetailDescription>, priceType: PriceType): String {
        var filledQuantity = BigDecimal.ZERO
        var quantityValue = BigDecimal.ZERO
        var quantityReserveShow = BigDecimal.ZERO
        orderDescriptions.forEach {
            filledQuantity = filledQuantity.add(it.fillQuantity.safeParseBigDecimal() ?: BigDecimal.ZERO)
            quantityValue = quantityValue.add(it.quantity.safeParseBigDecimal() ?: BigDecimal.ZERO)
            quantityReserveShow = quantityReserveShow.add(it.qtyReserveShow)
        }

        return when {
            priceType == PriceType.RESERVE_LIMIT -> {
                String.format(RESERVELIMIT_QUANTITY_FORMAT, quantityValue, quantityReserveShow)
            }
            filledQuantity != quantityValue && filledQuantity != BigDecimal.ZERO -> {
                String.format(QUANTITY_FORMAT, filledQuantity, quantityValue)
            }
            else -> {
                quantityValue.toString()
            }
        }
    }

    private fun getEventDate(events: List<EventDto>, eventType: EventType): String? =
        // collect the latest or most recent matched event.
        events.lastOrNull {
            it.header.eventType == eventType.typeCode
        }?.takeIf {
            it.header.eventTimeEST != 0L
        }?.let {
            getTimeString(it.header.eventTimeEST)
        }

    private fun getLatestExecutionPrice(legs: List<LegDetailsDto>, netPrice: String): BigDecimal {
        val result: BigDecimal = if (legs.size == 1) {
            legs[0].averageFillPrice.safeParseBigDecimal() ?: BigDecimal.ZERO
        } else {
            netPrice.safeParseBigDecimal() ?: BigDecimal.ZERO
        }
        // This should be the case for partial executed single leg and also fully executed leg.
        return result.abs()
    }

    private fun getPriceType(firstOrder: OrdersDto, advancedOrderItem: BaseOrderDetailsItem.AdvancedOrderItem?): PriceType {
        advancedOrderItem?.let {
            if (it.orderType == AdvancedOrderType.AO_CONTINGENT ||
                (firstOrder.header.origSysCode == OrigSysCode.ORIG_SYS_CODE_OH.numberCode && it.orderType == AdvancedOrderType.AO_CONTINGENT_CANCEL)
            ) {
                return PriceType.mapPriceType(firstOrder.header.orderTrigger)
            } else if (it.orderType in AdvancedOrderType.orderTypesForPriceType) {
                return PriceType.getPriceTypeForAdvancedOrderType(
                    it.orderType,
                    it.offsetType,
                    firstOrder.legDetails[0].orderAction,
                    firstOrder.header.orderTrigger
                )
            } else if (it.orderType == AdvancedOrderType.AO_TRAIL_STOP_LIMIT) {
                return PriceType.TRAILING_STOP_LIMIT
            }
        }
        val typeCode = firstOrder.header.orderTrigger
        val isReserveLimit = firstOrder.legDetails[0].qtyReserveShow.toInt() > 0
        val isLimitOffsetPositive = firstOrder.header.limitOffsetValue.toDoubleOrNull() ?: Double.MIN_VALUE > 0
        val isHiddenLimit = firstOrder.header.hiddenFlag.run {
            when {
                isBlank() -> false
                else -> firstOrder.header.hiddenFlag.toIntOrNull() ?: Int.MIN_VALUE > 0
            }
        }
        return PriceType.getPriceType(typeCode, isHiddenLimit, isReserveLimit, isLimitOffsetPositive)
    }

    private fun getOrderDescriptions(legDetails: List<LegDetailsDto>, events: List<EventDto>): List<OrderDetailDescription> {
        return legDetails.map { legDetail ->
            val displayQuantityValue = if (legDetail.filledQuantity != legDetail.quantityValue && legDetail.filledQuantity != "0") {
                String.format(QUANTITY_FORMAT, legDetail.filledQuantity, legDetail.quantityValue)
            } else {
                legDetail.quantityValue
            }
            val orderAction: OrderAction = OrderAction.fromString(legDetail.orderAction)
            val transactionType = TransactionType.from(orderAction, legDetail.positionEffect, InstrumentType.from(legDetail.typeCode).isOption)
            val event = events.find {
                it.eventLegDetails[0].displaySymbol == legDetail.displaySymbol &&
                    it.header.eventType == EventType.EXECUTED.typeCode || it.header.eventType == EventType.DONE_TRADE_EXECUTED.typeCode
            }
            OrderDetailDescription(
                displayQuantityValue,
                legDetail.filledQuantity,
                legDetail.quantityValue,
                transactionType,
                legDetail.displaySymbol,
                legDetail.originalSymbol,
                legDetail.underlyingProductId,
                OptionType.getOptionTypeFromResponse(legDetail.optCallPut),
                InstrumentType.from(legDetail.typeCode),
                legDetail.optExpireDate,
                legDetail.optExpireYearMonth.takeLast(2),
                legDetail.optExpireYearMonth.take(2),
                legDetail.optStrikePrice,
                displayQuantityValue.contains("/"),
                getTimeString(event?.header?.eventTimeEST) ?: "--",
                event?.header?.price?.safeParseBigDecimal(),
                getCommissionFee(legDetail),
                legDetail.qtyReserveShow.safeParseBigDecimal() ?: BigDecimal.ZERO
            )
        }
    }

    fun mapStockOrderCancelResponse(dto: StockPlanOrderCancelResponseDto): StockPlanOrderCancelResponse {
        return StockPlanOrderCancelResponse(
            accountNumber = dto.accountNumber ?: ""
        )
    }

    private fun calculateCommissionFees(legDetails: List<LegDetailsDto>): BigDecimal {
        var commissionFees = BigDecimal.ZERO
        legDetails.forEach {
            commissionFees = commissionFees.add(getCommissionFee(it))
        }
        return commissionFees
    }

    private fun getCommissionFee(legDetail: LegDetailsDto): BigDecimal {
        var commissionFees = BigDecimal.ZERO
        commissionFees = commissionFees.add(legDetail.commissionAmount.safeParseBigDecimal() ?: BigDecimal.ZERO)
        commissionFees = commissionFees.add(legDetail.otherFee.safeParseBigDecimal() ?: BigDecimal.ZERO)
        return commissionFees
    }

    private fun getTimeString(time: Long?): String? =
        time?.let {
            String.format(
                DATETIME_FORMAT,
                DateFormattingUtils.formatToShortDate(time),
                DateFormattingUtils.formatTimeWithoutSecondsWithShowingTimezone(time)
            )
        }

    private fun getDateString(time: Long): String = DateFormattingUtils.formatToShortDate(TimeUnit.SECONDS.toMillis(time))
}

/**
 * Work around : When Symbol is empty, Convert display name to ticker symbol
 */
private fun AdvancedOrderDto.resolveSymbol(): String {
    return if (aoCondSymbol.isEmpty()) {
        if (InstrumentType.from(aoCondSecurityType).isOption) {
            getConstructedOsiSymbol(aoCondDisplaySymbol)
        } else {
            aoCondDisplaySymbol
        }
    } else aoCondSymbol
}
