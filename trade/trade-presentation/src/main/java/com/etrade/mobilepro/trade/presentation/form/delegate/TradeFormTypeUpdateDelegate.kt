package com.etrade.mobilepro.trade.presentation.form.delegate

import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.InputHolder
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.SwitchInput
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.presentation.form.delegate.TradeFormFactoryDelegate.OnFormChangeListener
import javax.inject.Provider

internal typealias CollectParams<T> = TradeFormParametersBuilder.(InputHolder, T) -> Unit

internal class TradeFormTypeUpdateDelegate<T>(
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>
) where T : TradeFormInputId, T : WithAdvancedTradeFormInputId {

    fun onFormTypeUpdated(
        collectParams: CollectParams<T>,
        currentFormType: TradeFormType,
        inputId: T,
        listener: OnFormChangeListener,
        ruleListBuilder: RuleListBuilder
    ) {
        ruleListBuilder.apply {
            rule {
                requireInput<SwitchInput>(inputId.advancedOrder).value.observeForever { isAdvancedOrder ->
                    isAdvancedOrder?.let {
                        if (!isAdvancedOrder && currentFormType.advancedOrderType != AdvancedTradeType.None) {
                            val newFormType = TradeFormType(currentFormType.securityType, AdvancedTradeType.None)
                            requireAnyInput(inputId.advancedOrderType).resetValue()
                            onUpdated(inputId, collectParams, currentFormType, newFormType, listener)
                        }
                    }
                }
                inputValue<DropDownOption?>(inputId.advancedOrderType).observeForever { dropdown ->
                    dropdown?.id?.let { advancedOrderType ->
                        val newFormType = getNewFormType(advancedOrderType, inputId, currentFormType.securityType)
                        onUpdated(inputId, collectParams, currentFormType, newFormType, listener)
                    }
                }
            }
        }
    }

    private fun getNewFormType(advancedOrderTypeId: String, inputId: T, securityType: SecurityType): TradeFormType {
        return TradeFormType(
            securityType = securityType,
            advancedOrderType = when (advancedOrderTypeId) {
                inputId.advancedOrderTypeValueId.stopLoss -> AdvancedTradeType.StopLoss
                inputId.advancedOrderTypeValueId.contingent -> AdvancedTradeType.Contingent
                else -> AdvancedTradeType.None
            }
        )
    }

    private fun InputHolder.onUpdated(
        inputId: T,
        collectParams: CollectParams<T>,
        currentFormType: TradeFormType,
        newFormType: TradeFormType,
        listener: OnFormChangeListener
    ) {
        if (currentFormType != newFormType) {
            val formContentBuilder = tradeFormParametersBuilderProvider.get().apply {
                collectParams(this@onUpdated, inputId)
                advancedTradeType = newFormType.advancedOrderType
            }
            val snapshot = formContentBuilder.create(newFormType.securityType)
            listener.onAdvancedOrderType(newFormType, snapshot)
        }
    }
}
