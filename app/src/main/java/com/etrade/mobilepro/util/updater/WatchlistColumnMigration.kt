package com.etrade.mobilepro.util.updater

import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.WatchlistColumn
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnRepo
import java.io.File
import javax.inject.Inject

/**
 * Migrates watchlist columns sort order.
 *
 * @param watchlistColumnRepo a repository that will be used to store columns order
 */
class WatchlistColumnMigration @Inject constructor(private val watchlistColumnRepo: WatchlistColumnRepo) : Migration {

    @SuppressWarnings("MagicNumber")
    private val idMap: Map<Int, WatchlistColumn> = mapOf(
        10 to WatchlistColumn.LastPrice,
        11 to WatchlistColumn.ChangeDollar,
        12 to WatchlistColumn.ChangePercent,
        13 to WatchlistColumn.Volume,
        14 to WatchlistColumn.Bid,
        15 to WatchlistColumn.Ask,
        16 to WatchlistColumn.DayHigh,
        17 to WatchlistColumn.DayLow,
        18 to WatchlistColumn.FiftyTwoWeekHigh,
        19 to WatchlistColumn.FiftyTwoWeekLow,
        20 to WatchlistColumn.PriceEarningRatio,
        21 to WatchlistColumn.EarningsPerShare
    )

    override suspend fun migrate(databaseFile: File) {
        getWatchlistColumns(databaseFile)
            .mapNotNull { idMap[it] }
            .takeIf { it.isNotEmpty() }
            ?.run { watchlistColumnRepo.saveColumns(this) }
    }

    private fun getWatchlistColumns(databaseFile: File): List<Int> = readColumns(databaseFile, "WATCHLISTTBL")
}
