package com.etrade.mobilepro.uiwidgets.actionsheet

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * NOTE: The parent fragment (or activity) must have an ActionSheetClicksViewModel in its
 * ViewModel scope.
 */
class ActionSheetDialogFragment : BottomSheetDialogFragment() {

    private val clicksViewModel: ActionSheetClicksViewModel by viewModels(
        ownerProducer = { parentFragment ?: requireActivity() }
    )

    private val actionItemAdapter: ActionItemAdapter = ActionItemAdapter()
    private val actionSheetStyle: ActionSheetStyle by lazy {
        arguments?.extractActionSheetStyle()
            ?: throw IllegalArgumentException("No actionSheetStyle was provided for ActionSheetDialog")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionSheetStyle.onFragmentCreate(this)
        actionItemAdapter.items = arguments?.getParcelableArray(KEY_ITEMS)
            ?.filterIsInstance<ActionItem>()
            ?.toList()
            .orEmpty()
    }

    @Suppress("RestrictedApi", "VisibleForTests") // https://github.com/material-components/material-components-android/issues/614
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener {
                findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
                    ?.let { BottomSheetBehavior.from(it) }
                    ?.state = BottomSheetBehavior.STATE_EXPANDED
                setOnShowListener(null)
            }
            (this as? BottomSheetDialog)?.behavior?.disableShapeAnimations()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(actionSheetStyle.layoutId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<RecyclerView>(R.id.content_rv).apply {
            actionSheetStyle.setupRecyclerView(this)
            adapter = actionItemAdapter
        }
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        clicksViewModel.onActionSheetCancelled()
    }

    private inner class ActionItemAdapter : RecyclerView.Adapter<ActionItemViewHolder>() {

        var items: List<ActionItem> = emptyList()
            set(value) {
                if (field == value) {
                    return
                }
                field = value
                notifyDataSetChanged()
            }

        override fun getItemCount(): Int = items.size

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActionItemViewHolder = actionSheetStyle.createViewHolder(parent)

        override fun onBindViewHolder(holder: ActionItemViewHolder, position: Int) {
            val item = items[position]
            holder.bindTo(item)
            holder.itemView.setOnClickListener {
                clicksViewModel.onActionSheetClicked(item)
                dismiss()
            }
        }
    }

    companion object {

        private const val TAG_ACTION_SHEET_DIALOG = "ActionSheetDialogFragment"

        private const val KEY_ITEMS = "actionItems"

        fun show(manager: FragmentManager, items: List<ActionItem>, style: ActionSheetStyle = ActionSheetStyle.DefaultBubbles): ActionSheetDialogFragment {
            dismiss(manager)
            return ActionSheetDialogFragment().apply {
                arguments = Bundle().apply {
                    putParcelableArray(KEY_ITEMS, items.toTypedArray())
                    putActionSheetStyle(style)
                }
                show(manager, TAG_ACTION_SHEET_DIALOG)
            }
        }

        fun get(manager: FragmentManager): ActionSheetDialogFragment? = manager.findFragmentByTag(TAG_ACTION_SHEET_DIALOG) as? ActionSheetDialogFragment

        fun dismiss(manager: FragmentManager) {
            get(manager)?.dismiss()
        }
    }
}
