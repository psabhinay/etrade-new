package com.etrade.mobilepro.compose.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.etColors

@Composable
fun ThinDivider(modifier: Modifier = Modifier, isVertical: Boolean = false) {
    val localModifier = if (isVertical) {
        modifier.verticalDivider()
    } else {
        modifier.horizontalDivider()
    }
    Box(
        modifier = localModifier.background(MaterialTheme.etColors.lightGrey)
    )
}

private fun Modifier.verticalDivider() = this.width(Dimens.Size1dp).fillMaxHeight()
private fun Modifier.horizontalDivider() = this.height(Dimens.Size1dp).fillMaxWidth()
