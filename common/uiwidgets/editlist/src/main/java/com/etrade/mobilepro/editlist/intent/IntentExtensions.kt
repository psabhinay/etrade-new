package com.etrade.mobilepro.editlist.intent

import android.content.Intent

internal const val EXTRA_EMPTY_LIST_MESSAGE_RES_ID = "extra.emptyListMessageResId"
internal const val EXTRA_ITEMS_DELETABLE = "extra.itemsDeletable"
internal const val EXTRA_ITEMS_TOGGLEABLE = "extra.itemsToggleable"
internal const val EXTRA_LIST_ACTION_CONFIRMATION_IS_REQUIRED = "extra.listActionConfirmationIsRequired"
internal const val EXTRA_LIST_ACTION_CONFIRMATION_MESSAGE_RES_ID = "extra.listActionConfirmationMessageResId"
internal const val EXTRA_LIST_ACTION_CONFIRMATION_TITLE_RES_ID = "extra.listActionConfirmationTitleResId"
internal const val EXTRA_LIST_ACTION_IS_ENABLED = "extra.listActionIsEnabled"
internal const val EXTRA_LIST_ACTION_IS_VISIBLE = "extra.listActionIsVisible"
internal const val EXTRA_LIST_ACTION_TEXT_RES_ID = "extra.listActionTextResId"
internal const val EXTRA_LIST_FOOTER_MESSAGE = "extra.listFooterMessage"

internal var Intent.emptyListMessageResId: Int
    get() = getIntExtra(EXTRA_EMPTY_LIST_MESSAGE_RES_ID, 0)
    set(value) {
        putExtra(EXTRA_EMPTY_LIST_MESSAGE_RES_ID, value)
    }

var Intent.isDeletableItems: Boolean
    get() = getBooleanExtra(EXTRA_ITEMS_DELETABLE, true)
    set(value) {
        putExtra(EXTRA_ITEMS_DELETABLE, value)
    }

var Intent.isToggleableItems: Boolean
    get() = getBooleanExtra(EXTRA_ITEMS_TOGGLEABLE, false)
    set(value) {
        putExtra(EXTRA_ITEMS_TOGGLEABLE, value)
    }

internal var Intent.listActionTextResId: Int
    get() = getIntExtra(EXTRA_LIST_ACTION_TEXT_RES_ID, 0)
    set(value) {
        putExtra(EXTRA_LIST_ACTION_TEXT_RES_ID, value)
    }

internal var Intent.listActionIsVisible: Boolean
    get() = getBooleanExtra(EXTRA_LIST_ACTION_IS_VISIBLE, false)
    set(value) {
        putExtra(EXTRA_LIST_ACTION_IS_VISIBLE, value)
    }

internal var Intent.listActionIsEnabled: Boolean
    get() = getBooleanExtra(EXTRA_LIST_ACTION_IS_ENABLED, true)
    set(value) {
        putExtra(EXTRA_LIST_ACTION_IS_ENABLED, value)
    }

internal var Intent.listActionConfirmationIsRequired: Boolean
    get() = getBooleanExtra(EXTRA_LIST_ACTION_CONFIRMATION_IS_REQUIRED, false)
    set(value) {
        putExtra(EXTRA_LIST_ACTION_CONFIRMATION_IS_REQUIRED, value)
    }

internal var Intent.listActionConfirmationMessageResId: Int
    get() = getIntExtra(EXTRA_LIST_ACTION_CONFIRMATION_MESSAGE_RES_ID, 0)
    set(value) {
        putExtra(EXTRA_LIST_ACTION_CONFIRMATION_MESSAGE_RES_ID, value)
    }

internal var Intent.listActionConfirmationTitleResId: Int
    get() = getIntExtra(EXTRA_LIST_ACTION_CONFIRMATION_TITLE_RES_ID, 0)
    set(value) {
        putExtra(EXTRA_LIST_ACTION_CONFIRMATION_TITLE_RES_ID, value)
    }

internal var Intent.listFooterMessage: Int
    get() = getIntExtra(EXTRA_LIST_FOOTER_MESSAGE, 0)
    set(value) {
        putExtra(EXTRA_LIST_FOOTER_MESSAGE, value)
    }
