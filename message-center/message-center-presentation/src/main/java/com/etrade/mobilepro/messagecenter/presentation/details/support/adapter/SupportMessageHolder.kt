package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter

import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.api.model.ReferenceBook
import com.etrade.mobilepro.messagecenter.api.model.SupportMessage
import org.threeten.bp.Instant

internal data class SupportMessageHolder(
    val header: Header = Header(),
    val reply: Reply = Reply(),
    val latest: Letter? = null,
    val otherHistory: List<Letter> = emptyList(),
    val initialMessage: Message? = null,
    val backingSupportMessage: SupportMessage? = null
) {
    fun toList(): List<SupportMessageItem> {
        val result = mutableListOf<SupportMessageItem>()
        result.add(header)
        result.add(reply)
        latest?.let {
            result.add(it)
        }
        if (otherHistory.isNotEmpty()) {
            result.add(Separator())
            result.addAll(otherHistory)
        }
        return result.toList()
    }

    fun withNewHeaderStatus(newStatus: String): SupportMessageHolder = this.copy(header = header.copy(status = newStatus))

    fun withNewHistory(
        history: List<SupportMessage.Communication>,
        latestMessageCreatedAt: Instant,
        latestMessageCreatedAtFormatted: String
    ): SupportMessageHolder {
        val newLatest = history.getLatestLetter()?.let { letter ->
            val updatedContent = if (letter.content.createdAt == null) {
                letter.content.copy(
                    createdAt = latestMessageCreatedAt,
                    createdAtFormatted = latestMessageCreatedAtFormatted,
                    caseNo = header.referenceNumber
                )
            } else {
                letter.content.copy(caseNo = header.referenceNumber)
            }
            Letter(content = updatedContent, hasDivider = letter.hasDivider)
        }
        val newOtherHistory = mutableListOf(this.latest)
        newOtherHistory.addAll(this.otherHistory)
        return this.copy(
            latest = newLatest,
            otherHistory = newOtherHistory.filterNotNull()
        )
    }

    companion object {
        fun createFrom(supportMessage: SupportMessage, message: Message, referenceBook: ReferenceBook): SupportMessageHolder {
            val header = Header(
                referenceNumber = message.caseNo,
                status = supportMessage.commonDetails.status,
                accountName = message.accountShortName,
                subject = supportMessage.commonDetails.subject,
                topic = referenceBook.getTopicDescById(supportMessage.commonDetails.topicId),
                replyEnabled = supportMessage.commonDetails.replyEnabled
            )
            val latest = supportMessage.history.getLatestLetter()
            val otherHistory = supportMessage.history.getHistoryExcludingLatest()
            return SupportMessageHolder(
                header,
                Reply(""),
                latest,
                otherHistory,
                message,
                supportMessage
            )
        }
    }
}

private fun List<SupportMessage.Communication>.getLatestLetter(): Letter? = this.firstOrNull()?.let {
    Letter(
        content = it,
        hasDivider = false
    )
}

private fun List<SupportMessage.Communication>.getHistoryExcludingLatest(): List<Letter> {
    val otherHistory = mutableListOf<Letter>()
    this.drop(1).let { previousMessages ->
        val countOfLeft = previousMessages.size
        previousMessages.forEachIndexed { index, item ->
            otherHistory.add(
                Letter(
                    content = item,
                    hasDivider = index < countOfLeft - 1
                )
            )
        }
    }
    return otherHistory
}
