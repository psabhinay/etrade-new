package com.etrade.mobilepro.transfermoney.usecase.confirm

sealed class GetQuickTransferConfirmResult {

    data class Success(
        val confirmationNumber: String? = null,
        val headerMessage: String? = null,
        val message: String? = null,
        val footerMessage: String? = null
    ) : GetQuickTransferConfirmResult()

    data class Failed(val errorDescription: String?) : GetQuickTransferConfirmResult()
}
