package com.etrade.mobilepro.trade.data

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.trade.api.WithPriceOrder

private const val STOP_LOSS_DISCLOSURES_TEXT = "For order types where a stop price is specified (but the order is not a limit order), " +
    "please be aware that there is no guarantee that the order: (i) will be triggered or (ii) if triggered, " +
    "will be executed at or near the trigger price. For additional information about stop loss order types please see:" +
    "<br><br><a href=\"etrade://stoplossdisclosures\">Stop Loss Order Disclosures</a>"

internal fun WithPriceOrder.priceTypeWarnings(warnings: List<ServerMessage>): List<ServerMessage> {
    return if (priceType.showStopLossDisclosure()) {
        listOf(ServerMessage.warning(STOP_LOSS_DISCLOSURES_TEXT, -1)) + warnings
    } else {
        warnings
    }
}
