package com.etrade.mobilepro.alerts.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.common.readstatus.ReadableType
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.util.android.SingleLiveEvent
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class AlertSelectionViewModel @Inject constructor(
    private val readStatusRepo: ReadStatusRepo,
    private val alertsRepo: AlertsRepo,
    private val user: User
) : ViewModel() {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    private val _navigateEvent = SingleLiveEvent<AlertItem.Alert>()
    val navigateEvent: LiveData<AlertItem.Alert> = _navigateEvent

    fun selectAlert(alert: AlertItem.Alert) {
        readStatusRepo.markAsRead(alert.id, ReadableType.ALERT)
        _navigateEvent.value = alert
    }

    fun wasRead(alert: AlertItem.Alert): Boolean = readStatusRepo.wasRead(alert.id)

    fun selectAlertFromNotification(alertMessage: String) {
        viewModelScope.launch {
            alertsRepo.findAlertByMessage(user.getUserId(), alertMessage)
                .onSuccess { alert -> alert?.let { selectAlert(it) } }
                .onFailure { logger.error(it.localizedMessage, it) }
        }
    }
}
