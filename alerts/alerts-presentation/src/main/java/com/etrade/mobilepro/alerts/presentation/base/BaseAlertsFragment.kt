package com.etrade.mobilepro.alerts.presentation.base

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.etrade.mobilepro.alerts.api.model.AlertAction
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.presentation.AlertSelectionViewModel
import com.etrade.mobilepro.alerts.presentation.AlertsSharedViewModel
import com.etrade.mobilepro.alerts.presentation.R
import com.etrade.mobilepro.alerts.presentation.adapter.AlertsAdapter
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.common.readstatus.ReadableType
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.util.android.recyclerviewutil.CustomRecyclerViewItemDivider
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.collection.indicesOfElementsWithType
import com.google.android.material.snackbar.Snackbar

private const val LOAD_MORE_THRESHOLD_VALUE = 7
const val ALERTS_PATH = "/alerts"

abstract class BaseAlertsFragment(
    viewModelFactory: ViewModelProvider.Factory,
    layoutRes: Int,
    private val readStatusRepo: ReadStatusRepo
) : Fragment(layoutRes) {
    protected abstract val alertTab: AlertTab
    protected val sharedViewModel: AlertsSharedViewModel by activityViewModels { viewModelFactory }
    protected abstract val viewModel: BaseAlertsViewModel
    protected abstract val snackbarUtil: SnackBarUtil
    private var itemDecorator: RecyclerView.ItemDecoration? = null
    private val separator: Drawable? by lazy {
        ContextCompat.getDrawable(requireContext(), R.drawable.list_item_divider_light_grey)
    }
    private val alertSelectionViewModel: AlertSelectionViewModel by activityViewModels { viewModelFactory }

    private val adapter = AlertsAdapter(
        thresholdValue = LOAD_MORE_THRESHOLD_VALUE,
        thresholdReachedListener = {
            viewModel.thresholdReached()
        },
        readStatusChecker = {
            alertSelectionViewModel.wasRead(it)
        },
        alertListener = { alert, action ->
            when (action) {
                AlertAction.SELECT -> alertSelectionViewModel.selectAlert(alert)
                AlertAction.DELETE -> viewModel.deleteAlert(alert)
                AlertAction.MARK -> viewModel.markAndCountAlert(alert)
            }
        },
        swipeRevealListener = { alert, action ->
            viewModel.deleteButtonAction(alert, action)
        },
        dragLocker = {
            viewModel.isCellsDraggingLocked()
        }
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doBaseFragmentSetup(provideAlertsRecyclerView())
        doBaseBindings()
        setupFragment()
        bindViewModel(adapter)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        viewModel.hideDeleteButtons()
        return super.onOptionsItemSelected(item)
    }

    override fun onPause() {
        super.onPause()
        viewModel.hideDeleteButtons()
    }

    protected abstract fun provideAlertsRecyclerView(): RecyclerView

    protected abstract fun provideDeletionLiveEvent(): LiveData<String>

    protected open fun setupFragment() {}

    protected abstract fun bindViewModel(adapter: AlertsAdapter)

    private fun doBaseFragmentSetup(recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter
        (recyclerView.itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
    }

    private fun doBaseBindings() {
        sharedViewModel.selectedTab.observeBy(viewLifecycleOwner) {
            if (it == alertTab) {
                viewModel.onBecomeActive()
            } else { // we have only 2 tabs
                viewModel.onBecomeInactive()
            }
        }
        viewModel.refreshViews.observeBy(viewLifecycleOwner) {
            it.getNonConsumedContent()
            adapter.markDirty()
        }
        provideDeletionLiveEvent().observeBy(viewLifecycleOwner) {
            snackbarUtil.snackbar(
                message = getString(R.string.alert_was_removed),
                duration = Snackbar.LENGTH_SHORT
            )
                ?.show()
            viewModel.alertWasDeleted(it)
        }
    }

    fun updateAlertsAdapter(alerts: List<AlertItem>) {
        updateReadStatusRepo(alerts)
        updateItemDecorator(alerts.indicesOfElementsWithType<AlertItem.Divider, AlertItem>())
        adapter.submitAlerts(alerts)
    }

    private fun updateReadStatusRepo(alerts: List<AlertItem>) {
        val alertIds = alerts.filterIsInstance(AlertItem.Alert::class.java)
            .filter { it.status.wasRead }
            .map { it.id }
        readStatusRepo.markAsRead(alertIds, ReadableType.ALERT)
    }

    private fun updateItemDecorator(dividerPositions: List<Int>) {
        separator?.let { separator ->
            itemDecorator?.let { provideAlertsRecyclerView().removeItemDecoration(it) }
            itemDecorator = CustomRecyclerViewItemDivider(
                drawable = separator,
                positions = dividerPositions
            ).also {
                provideAlertsRecyclerView().addItemDecoration(it)
            }
        }
    }

    protected open fun updateForEditableMode(isEditable: Boolean) =
        viewModel.updateAlertsForEditing(isEditable)
}
