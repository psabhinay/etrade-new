package com.etrade.mobilepro.bloombergtv.widget

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.bloombergtv.api.BloombergTvRepository
import com.etrade.mobilepro.bloombergtv.common.BloombergTvPipStatusHelper
import com.etrade.mobilepro.bloombergtv.widget.dto.BloombergWidgetViewDto
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

class BloombergWidgetViewFactory @Inject constructor(
    private val bloombergTvRepository: BloombergTvRepository,
    private val bloombergTvPipStatusHelper: BloombergTvPipStatusHelper
) : DynamicUiViewFactory {
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is BloombergWidgetViewDto) {
            return object : GenericLayout {

                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is BloombergWidgetViewModel) {
                        viewModel.refresh()
                    }
                }

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    BloombergWidgetViewModel(bloombergTvPipStatusHelper, bloombergTvRepository)
                }

                override val viewModelClass = BloombergWidgetViewModel::class.java

                override val uniqueIdentifier: String = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("BloombergViewFactory registered to unknown type")
        }
    }
}
