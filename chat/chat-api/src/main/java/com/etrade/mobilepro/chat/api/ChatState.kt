package com.etrade.mobilepro.chat.api

sealed class ChatState {

    object TerminatedByAgent : ChatState()

    object TerminatedByUser : ChatState()

    data class Error(val message: String?) : ChatState()

    object Loading : ChatState()

    data class MessagesShown(val messages: List<ChatMessage>, val unread: Int = 0) : ChatState()

    data class ChatTransferredMessage(val messages: List<ChatMessage>) : ChatState()

    /** User can be placed in Agent's Queue when :
     * 1. Upon Chat Initiated by user
     * 2. Agent Transfers the session to another Agent, agent can get disconnected without formal transfer.
     */
    data class QueueShown(val estimatedWaitTime: Int, val isAgentDisconnected: Boolean = false) : ChatState()

    data class SendMessageError(val messages: List<ChatMessage>, val message: String) : ChatState()

    // Empty items or Unrecognized events.
    object Unknown : ChatState()
}
