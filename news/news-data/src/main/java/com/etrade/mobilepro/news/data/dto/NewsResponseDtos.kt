package com.etrade.mobilepro.news.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NewsDto(
    @Json(name = "data")
    val data: NewsDataDto?
)

@JsonClass(generateAdapter = true)
data class NewsDataDto(
    @Json(name = "response")
    val newsPayload: NewsPayloadDto?
)

@JsonClass(generateAdapter = true)
data class NewsPayloadDto(
    @Json(name = "news")
    val docsList: DocsListDto?
)

@JsonClass(generateAdapter = true)
data class DocsListDto(
    @Json(name = "doc")
    val docs: List<DocDto>?
)

@JsonClass(generateAdapter = true)
data class DocDto(
    @Json(name = "wsodDoc")
    val textDoc: TextDocDto?
)

@JsonClass(generateAdapter = true)
data class TextDocDto(
    @Json(name = "docId")
    val docId: String?,
    @Json(name = "title")
    val title: String?,
    @Json(name = "timestamp")
    val timestamp: TimestampDto?,
    @Json(name = "url")
    val url: String?,
    @Json(name = "symbols")
    val symbols: String?,
    @Json(name = "content")
    val content: String?,
    @Json(name = "source")
    val source: String?
)

@JsonClass(generateAdapter = true)
class TimestampDto(
    @Json(name = "value")
    val value: String?
)
