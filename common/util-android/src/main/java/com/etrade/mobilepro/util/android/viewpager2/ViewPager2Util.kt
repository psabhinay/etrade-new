package com.etrade.mobilepro.util.android.viewpager2

import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2

/**
 * Returns the internal RecyclerView of a ViewPager2.
 */
val ViewPager2.recyclerView: RecyclerView?
    get() {
        return this[0] as? RecyclerView
    }
