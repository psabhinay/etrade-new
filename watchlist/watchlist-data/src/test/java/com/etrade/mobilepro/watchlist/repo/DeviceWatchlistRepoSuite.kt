package com.etrade.mobilepro.watchlist.repo

import android.content.res.Resources
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.quote.createQuoteResponseAdapter
import com.etrade.mobilepro.quote.dto.QuoteDataContainer
import com.etrade.mobilepro.quote.mapper.MobileQuoteMapper
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.DuplicateSymbolException
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockito.Mockito

private const val DEVICE_WATCHLIST_NAME = "device_watchlist_name"

class DeviceWatchlistRepoSuite {

    private val storage = InMemoryKeyValueStorage()

    private val resources: Resources = mock()

    private val repo: WatchlistRepo = DeviceWatchlistRepo(
        storage,
        Mockito.mock(MobileQuoteRepo::class.java).also {
            whenever(it.loadMobileQuotes(any(), any(), any(), any())).thenReturn(Observable.just(getQuotes()))
        },
        resources.also {
            whenever(it.getString(any())).thenReturn(DEVICE_WATCHLIST_NAME)
        }
    )

    @AfterEach
    fun prepareForTest() {
        storage.clear()
    }

    @Test
    fun `test getting of watchlists`() {
        repo.getUserWatchlists().subscribe(
            Consumer {
                assertEquals(1, it.size)
                assertEquals(Watchlist(DEVICE_WATCHLIST_ID, DEVICE_WATCHLIST_NAME), it[0])
            }
        )
    }

    @Test
    fun `test adding to watchlist`() {
        checkAdding("B,C", "A", "A,B,C")
    }

    @Test
    fun `test adding to empty watchlist`() {
        checkAdding("", "A", "A")
    }

    @Test
    fun `test adding existing ticker to watchlist`() {
        val exception = assertThrows(java.lang.RuntimeException::class.java) {
            checkAdding("AA,AAPL,A,AAA", "A", "AA,AAPL,A,AAA")
        }
        assertTrue(exception.cause is DuplicateSymbolException)
    }

    @Test
    fun `test adding substring ticker to watchlist`() {
        checkAdding("AA,AAA", "A", "A,AA,AAA")
    }

    @Test
    fun `test adding to wrong watchlist`() {
        assertThrows(IllegalArgumentException::class.java) {
            repo.addToWatchList("", "A")
        }
    }

    @Test
    fun `test adding invalid ticker`() {
        assertThrows(IllegalArgumentException::class.java) {
            repo.addToWatchList(DEVICE_WATCHLIST_ID, "")
        }
        assertThrows(IllegalArgumentException::class.java) {
            repo.addToWatchList(DEVICE_WATCHLIST_ID, "\n")
        }
        assertThrows(IllegalArgumentException::class.java) {
            repo.addToWatchList(DEVICE_WATCHLIST_ID, "   ")
        }
        assertThrows(IllegalArgumentException::class.java) {
            repo.addToWatchList(DEVICE_WATCHLIST_ID, "A,B")
        }
    }

    @Test
    fun `test saving watchlist`() {
        repo.saveWatchList(DEVICE_WATCHLIST_ID, listOf(WatchlistEntry("A"), WatchlistEntry("B"), WatchlistEntry("A"), WatchlistEntry("C")), emptyList())
        assertEquals("A,B,C", storage.getStringValue(KEY_DEVICE_WATCHLIST_TICKERS))
    }

    @Test
    fun `test saving empty watchlist`() {
        repo.saveWatchList(DEVICE_WATCHLIST_ID, emptyList(), emptyList())
        assertEquals(mapOf(KEY_DEVICE_WATCHLIST_TICKERS to ""), storage.map)
    }

    @Test
    fun `test saving to wrong watchlist`() {
        assertThrows(IllegalArgumentException::class.java) {
            repo.saveWatchList("", emptyList(), emptyList())
        }
    }

    @Test
    fun `test watchlist creation`() {
        assertThrows(UnsupportedOperationException::class.java) {
            repo.createWatchlist("name")
        }
    }

    @Test
    fun `test watchlist removal`() {
        assertThrows(UnsupportedOperationException::class.java) {
            repo.deleteWatchList("name")
        }
    }

    @Test
    fun `test getting watchlist entries`() {
        checkEntries("SNAP,FB,AAPL")
    }

    @Test
    fun `test getting watchlist entries with less tickers than user needs`() {
        checkEntries("SNAP,FB,AAPL,QQQ", "SNAP,FB,AAPL")
    }

    @Test
    fun `test getting watchlist entries with more tickers than user needs`() {
        checkEntries("SNAP,AAPL")
    }

    private fun checkAdding(initialTickers: String, tickerToAdd: String, expectedTickers: String) {
        storage.putStringValue(KEY_DEVICE_WATCHLIST_TICKERS, initialTickers)
        repo.addToWatchList(DEVICE_WATCHLIST_ID, tickerToAdd).blockingGet()
        assertEquals(expectedTickers, storage.getStringValue(KEY_DEVICE_WATCHLIST_TICKERS))
    }

    private fun checkEntries(initOrder: String, expectedValue: String = initOrder) {
        storage.putStringValue(KEY_DEVICE_WATCHLIST_TICKERS, initOrder)
        val actualOrder = repo.getWatchlistEntries(DEVICE_WATCHLIST_ID).blockingGet().joinToString(",") { it.ticker }
        assertEquals(expectedValue, actualOrder)
    }

    private fun getQuotes(): List<MobileQuote> {
        val mapper = MobileQuoteMapper()
        return getQuotesResponse()
            .data
            ?.quotes
            ?.map { mapper.map(it) } ?: throw RuntimeException("No quotes are provided.")
    }

    private fun getQuotesResponse(): ServerResponseDto<QuoteDataContainer> {
        return MobileQuoteMapper::class.getObjectFromJson(
            resourcePath = "get_mobile_quotes_response.json",
            cls1 = ServerResponseDto::class.java,
            cls2 = QuoteDataContainer::class.java,
            adapters = listOf(createQuoteResponseAdapter())
        )
    }
}

private class InMemoryKeyValueStorage : KeyValueStorage {

    val map: MutableMap<String, Any?> = mutableMapOf()

    override fun getBooleanValue(key: String, defaultValue: Boolean): Boolean = map[key] as Boolean? ?: defaultValue

    override fun getIntValue(key: String, defaultValue: Int): Int = map[key] as Int? ?: defaultValue

    override fun getStringValue(key: String, defaultValue: String?): String? = map[key] as String? ?: defaultValue

    override fun putBooleanValue(key: String, value: Boolean?) {
        map[key] = value
    }

    override fun putIntValue(key: String, value: Int) {
        map[key] = value
    }

    override fun putStringValue(key: String, value: String?) {
        map[key] = value
    }

    fun clear() = map.clear()
}
