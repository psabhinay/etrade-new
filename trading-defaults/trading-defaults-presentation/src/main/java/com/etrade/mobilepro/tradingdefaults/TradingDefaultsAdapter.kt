package com.etrade.mobilepro.tradingdefaults

import com.etrade.mobilepro.tradingdefaults.delegate.TradingDefaultsDividerAdapterDelegate
import com.etrade.mobilepro.tradingdefaults.delegate.TradingDefaultsDividerTitleAdapterDelegate
import com.etrade.mobilepro.tradingdefaults.delegate.TradingDefaultsEditorAdapterDelegate
import com.etrade.mobilepro.tradingdefaults.delegate.TradingDefaultsPickerAdapterDelegate
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class TradingDefaultsAdapter : ListDelegationAdapter<MutableList<TradingDefaultsItem>>() {

    init {
        delegatesManager.addDelegate(TradingDefaultsDividerTitleAdapterDelegate())
        delegatesManager.addDelegate(TradingDefaultsEditorAdapterDelegate())
        delegatesManager.addDelegate(TradingDefaultsPickerAdapterDelegate())
        delegatesManager.addDelegate(TradingDefaultsDividerAdapterDelegate())

        items = mutableListOf()
    }

    fun updateItems(detailItems: List<TradingDefaultsItem>) {
        items.clear()
        items.addAll(detailItems)

        notifyDataSetChanged()
    }
}
