package com.etrade.mobilepro.transfermoney.presentation.transfer

import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount

internal fun selectFromAccount(preSelectedArg: String?, fromAccounts: List<TransferMoneyAccount>): TransferMoneyAccount? {
    return preSelectedArg?.let { preselectId ->
        val candidates = fromAccounts.filter { it.accountId == preselectId }
        if (candidates.size == 1) {
            candidates.first()
        } else {
            return null
        }
    } ?: if (fromAccounts.size == 1) {
        fromAccounts.first()
    } else {
        null
    }
}
