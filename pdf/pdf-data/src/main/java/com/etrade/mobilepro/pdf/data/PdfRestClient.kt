package com.etrade.mobilepro.pdf.data

import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url

interface PdfRestClient {

    /**
     * @param fileUrl Url to download from
     * @see Streaming annotation is so that we avoid Retrofit from trying
     * to move the entire file into memory. Instead of moving the entire file
     * into memory, Retrofit will pass along the bytes right away.
     */
    @Streaming
    @GET
    suspend fun downloadPdfFromUrl(@Url fileUrl: String): ResponseBody
}
