package com.etrade.mobilepro.common.customview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Rect
import android.os.Parcelable
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.view.GestureDetectorCompat
import androidx.core.view.ViewCompat
import androidx.customview.widget.ViewDragHelper
import com.etrade.mobilepro.util.android.extension.pxToDp
import kotlinx.parcelize.Parcelize
import kotlin.math.max

private const val DEFAULT_MIN_FLING_VELOCITY = 300 // dp per second
private const val DEFAULT_MIN_DIST_REQUEST_DISALLOW_PARENT = 1 // dp

@Suppress("TooManyFunctions")
class SwipeRevealLayout : FrameLayout {

    enum class Action {
        START_DRAG, OPEN, CLOSE
    }

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    /**
     * Main view is the view which is shown when the layout is closed.
     */
    private lateinit var mainView: View

    /**
     * Secondary view is the view which is shown when the layout is opened.
     */
    private lateinit var secondaryView: View

    var actionListener: ((Action) -> Unit)? = null
    private val innerActionListener: ((Action) -> Unit)? = {
        when (it) {
            Action.START_DRAG,
            Action.OPEN -> secondaryView.visibility = View.VISIBLE
            Action.CLOSE -> secondaryView.visibility = View.INVISIBLE
        }
        actionListener?.invoke(it)
    }

    /**
     * The rectangle position of the main view when the layout is closed.
     */
    private val rectMainClose = Rect()

    /**
     * The rectangle position of the main view when the layout is opened.
     */
    private val rectMainOpen = Rect()

    /**
     * The rectangle position of the secondary view when the layout is closed.
     */
    private val rectSecClose = Rect()

    /**
     * The rectangle position of the secondary view when the layout is opened.
     */
    private val rectSecOpen = Rect()

    private var isOpenBeforeInit = false
    @Volatile
    private var isScrolling = false
    @Volatile
    private var isOpened = false
    /**
     * @return true if the drag/swipe motion is currently locked.
     */
    @Volatile
    private var _isDragLocked = false

    var isDragLocked: Boolean
        get() = dragLocker?.invoke() ?: _isDragLocked
        set(newValue) { _isDragLocked = newValue }

    var dragLocker: (() -> Boolean)? = null

    private var dragDist = 0f
    private var prevX = -1f

    private lateinit var dragHelper: ViewDragHelper
    private lateinit var gestureDetector: GestureDetectorCompat

    private val mainOpenLeft: Int
        get() = rectMainClose.left - secondaryView.width

    private val mainOpenTop: Int
        get() = rectMainClose.top

    private val secOpenLeft: Int
        get() = rectSecClose.left

    private val secOpenTop: Int
        get() = rectSecClose.top

    private val gestureListener = object : GestureDetector.SimpleOnGestureListener() {
        var hasDisallowed = false

        override fun onDown(e: MotionEvent?): Boolean {
            isScrolling = false
            hasDisallowed = false
            return true
        }

        override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
            isScrolling = true
            return false
        }

        override fun onScroll(e1: MotionEvent?, e2: MotionEvent?, distanceX: Float, distanceY: Float): Boolean {
            isScrolling = true
            if (parent != null) {
                val shouldDisallow: Boolean
                if (!hasDisallowed) {
                    shouldDisallow = distToClosestEdge >= DEFAULT_MIN_DIST_REQUEST_DISALLOW_PARENT
                    if (shouldDisallow) {
                        hasDisallowed = true
                    }
                } else {
                    shouldDisallow = true
                }
                // disallow parent to intercept touch event so that the layout will work
                // properly on RecyclerView or view that handles scroll gesture.
                parent.requestDisallowInterceptTouchEvent(shouldDisallow)
            }
            return false
        }
    }

    private val distToClosestEdge: Int
        get() {
            val pivotLeft = rectMainClose.right - secondaryView.width
            return Math.min(
                mainView.right - pivotLeft,
                rectMainClose.right - mainView.right
            )
        }

    private val halfwayPivotHorizontal: Int
        get() = rectMainClose.right - secondaryView.width / 2

    private val dragHelperCallback = object : ViewDragHelper.Callback() {
        override fun tryCaptureView(child: View, pointerId: Int): Boolean {
            if (isDragLocked) { return false }
            if (!isOpened) {
                innerActionListener?.invoke(Action.START_DRAG)
            }
            dragHelper.captureChildView(mainView, pointerId)
            return false
        }

        override fun clampViewPositionHorizontal(child: View, left: Int, dx: Int): Int = Math.max(
            Math.min(left, rectMainClose.left),
            rectMainClose.left - secondaryView.width
        )

        override fun onViewReleased(releasedChild: View, xvel: Float, yvel: Float) {
            val velRightExceeded = context.pxToDp(xvel.toInt()) >= DEFAULT_MIN_FLING_VELOCITY
            val velLeftExceeded = context.pxToDp(xvel.toInt()) <= -DEFAULT_MIN_FLING_VELOCITY
            val pivotHorizontal = halfwayPivotHorizontal
            if (velRightExceeded) {
                close(animation = true)
            } else if (velLeftExceeded) {
                open(animation = true)
            } else {
                if (mainView.right < pivotHorizontal) {
                    open(animation = true)
                } else {
                    close(animation = true)
                }
            }
        }

        override fun onEdgeDragStarted(edgeFlags: Int, pointerId: Int) {
            super.onEdgeDragStarted(edgeFlags, pointerId)

            if (isDragLocked) {
                return
            }

            if (edgeFlags == ViewDragHelper.EDGE_RIGHT) {
                if (isOpened) {
                    innerActionListener?.invoke(Action.START_DRAG)
                }
                dragHelper.captureChildView(mainView, pointerId)
            }
        }

        override fun onViewPositionChanged(changedView: View, left: Int, top: Int, dx: Int, dy: Int) {
            super.onViewPositionChanged(changedView, left, top, dx, dy)
            ViewCompat.postInvalidateOnAnimation(this@SwipeRevealLayout)
        }

        override fun onViewDragStateChanged(state: Int) {
            when (state) {
                ViewDragHelper.STATE_IDLE -> innerActionListener?.run {
                    invoke(
                        if (isOpened) {
                            Action.OPEN
                        } else {
                            Action.CLOSE
                        }
                    )
                }
            }
        }
    }

    override fun onSaveInstanceState(): Parcelable? = State(
        parentState = super.onSaveInstanceState(),
        secondaryViewVisibility = secondaryView.visibility
    )

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is State) {
            secondaryView.visibility = state.secondaryViewVisibility
            super.onRestoreInstanceState(state.parentState)
        } else {
            super.onRestoreInstanceState(state)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        gestureDetector.onTouchEvent(event)
        dragHelper.processTouchEvent(event)
        return true
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        if (isDragLocked) {
            return super.onInterceptTouchEvent(ev)
        }
        dragHelper.processTouchEvent(ev)
        gestureDetector.onTouchEvent(ev)
        accumulateDragDist(ev)
        val couldBecomeClick = couldBecomeClick(ev)
        val settling = dragHelper.viewDragState == ViewDragHelper.STATE_SETTLING
        val idleAfterScrolled = dragHelper.viewDragState == ViewDragHelper.STATE_IDLE && isScrolling
        // must be placed as the last statement
        prevX = ev.x
        // return true => intercept, cannot trigger onClick event
        return !couldBecomeClick && (settling || idleAfterScrolled)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        // get views
        if (childCount >= 2) {
            secondaryView = getChildAt(0)
            mainView = getChildAt(1)
            secondaryView.visibility = View.INVISIBLE
        } else if (childCount == 1) {
            mainView = getChildAt(0)
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        initRects()
        if (isOpenBeforeInit) {
            open(false)
        } else {
            close(false)
        }
    }

    @Suppress("ComplexMethod")
    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        check(childCount >= 2) { "Layout must have at least two children" }

        var widthMeasureSpec = widthSpec
        var heightMeasureSpec = heightSpec
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        var desiredWidth = 0
        var desiredHeight = 0
        // first find the largest child
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            measureChild(child, widthMeasureSpec, heightMeasureSpec)
            desiredWidth = max(child.measuredWidth, desiredWidth)
            desiredHeight = max(child.measuredHeight, desiredHeight)
        }
        // create new measure spec using the largest child width
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(desiredWidth, widthMode)
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(desiredHeight, heightMode)
        val measuredWidth = MeasureSpec.getSize(widthMeasureSpec)
        val measuredHeight = MeasureSpec.getSize(heightMeasureSpec)
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            val childParams = child.layoutParams
            if (childParams != null) {
                if (childParams.height == ViewGroup.LayoutParams.MATCH_PARENT) {
                    child.minimumHeight = measuredHeight
                }
                if (childParams.width == ViewGroup.LayoutParams.MATCH_PARENT) {
                    child.minimumWidth = measuredWidth
                }
            }
            measureChild(child, widthMeasureSpec, heightMeasureSpec)
            desiredWidth = max(child.measuredWidth, desiredWidth)
            desiredHeight = max(child.measuredHeight, desiredHeight)
        }
        // taking accounts of padding
        desiredWidth += paddingLeft + paddingRight
        desiredHeight += paddingTop + paddingBottom
        // adjust desired width
        if (widthMode == MeasureSpec.EXACTLY) {
            desiredWidth = measuredWidth
        } else {
            if (layoutParams.width == ViewGroup.LayoutParams.MATCH_PARENT) {
                desiredWidth = measuredWidth
            }

            if (widthMode == MeasureSpec.AT_MOST) {
                desiredWidth = if (desiredWidth > measuredWidth) measuredWidth else desiredWidth
            }
        }
        // adjust desired height
        if (heightMode == MeasureSpec.EXACTLY) {
            desiredHeight = measuredHeight
        } else {
            if (layoutParams.height == ViewGroup.LayoutParams.MATCH_PARENT) {
                desiredHeight = measuredHeight
            }

            if (heightMode == MeasureSpec.AT_MOST) {
                desiredHeight = if (desiredHeight > measuredHeight) measuredHeight else desiredHeight
            }
        }
        setMeasuredDimension(desiredWidth, desiredHeight)
    }

    override fun computeScroll() {
        if (dragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this)
        }
    }

    /**
     * Open the panel to show the secondary view.
     */
    fun open(animation: Boolean) {
        isOpenBeforeInit = true
        isOpened = true
        secondaryView.visibility = View.VISIBLE
        if (animation) {
            dragHelper.smoothSlideViewTo(mainView, rectMainOpen.left, rectMainOpen.top)
        } else {
            dragHelper.abort()
            mainView.layout(
                rectMainOpen.left,
                rectMainOpen.top,
                rectMainOpen.right,
                rectMainOpen.bottom
            )
            secondaryView.layout(
                rectSecOpen.left,
                rectSecOpen.top,
                rectSecOpen.right,
                rectSecOpen.bottom
            )
        }
        ViewCompat.postInvalidateOnAnimation(this)
    }

    /**
     * Close the panel to hide the secondary view.
     */
    fun close(animation: Boolean) {
        isOpenBeforeInit = false
        isOpened = false
        if (animation) {
            dragHelper.smoothSlideViewTo(mainView, rectMainClose.left, rectMainClose.top)
        } else {
            dragHelper.abort()
            mainView.layout(
                rectMainClose.left,
                rectMainClose.top,
                rectMainClose.right,
                rectMainClose.bottom
            )
            secondaryView.layout(
                rectSecClose.left,
                rectSecClose.top,
                rectSecClose.right,
                rectSecClose.bottom
            )
            secondaryView.visibility = View.INVISIBLE
        }
        ViewCompat.postInvalidateOnAnimation(this)
    }

    /**
     * @return Set true for lock the swipe.
     */
    fun dragLock(drag: Boolean) {
        this.isDragLocked = drag
    }

    private fun initRects() {
        // close position of main view
        rectMainClose.set(mainView.left, mainView.top, mainView.right, mainView.bottom)
        // close position of secondary view
        rectSecClose.set(secondaryView.left, secondaryView.top, secondaryView.right, secondaryView.bottom)
        // open position of the main view
        rectMainOpen.set(mainOpenLeft, mainOpenTop, mainOpenLeft + mainView.width, mainOpenTop + mainView.height)
        // open position of the secondary view
        rectSecOpen.set(secOpenLeft, secOpenTop, secOpenLeft + secondaryView.width, secOpenTop + secondaryView.height)
    }

    private fun couldBecomeClick(ev: MotionEvent): Boolean = isInMainView(ev) && !shouldInitiateADrag()

    private fun isInMainView(ev: MotionEvent): Boolean {
        val x = ev.x
        val y = ev.y
        val withinVertical = mainView.top <= y && y <= mainView.bottom
        val withinHorizontal = mainView.left <= x && x <= mainView.right
        return withinVertical && withinHorizontal
    }

    private fun shouldInitiateADrag(): Boolean {
        val minDistToInitiateDrag = dragHelper.touchSlop.toFloat()
        return dragDist >= minDistToInitiateDrag
    }

    private fun accumulateDragDist(ev: MotionEvent) {
        val action = ev.action
        if (action == MotionEvent.ACTION_DOWN) {
            dragDist = 0f
            return
        }
        val dragged = Math.abs(ev.x - prevX)
        dragDist += dragged
    }

    private fun init(context: Context) {
        dragHelper = ViewDragHelper.create(this, 1.0f, dragHelperCallback)
        dragHelper.setEdgeTrackingEnabled(ViewDragHelper.EDGE_ALL)
        gestureDetector = GestureDetectorCompat(context, gestureListener)
    }
}

@Parcelize
private class State(
    val parentState: Parcelable?,
    val secondaryViewVisibility: Int
) : Parcelable
