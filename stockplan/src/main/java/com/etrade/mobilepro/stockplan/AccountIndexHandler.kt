package com.etrade.mobilepro.stockplan

interface AccountIndexHandler {
    fun getAccountIndex(): String?
}
