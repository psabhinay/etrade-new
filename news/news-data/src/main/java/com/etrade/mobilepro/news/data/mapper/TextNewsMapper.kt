package com.etrade.mobilepro.news.data.mapper

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.data.dto.TextDocDto
import com.etrade.mobilepro.util.android.textutil.TextUtil.Companion.convertTextWithHmlTagsToSpannable
import com.etrade.mobilepro.util.formatters.safeParseStringToDate
import java.util.Date

private const val DATE_TIME_SOURCE_FORMAT = "%s | %s | %s"
private const val DATE_SOURCE_FORMAT = "%s | %s"

internal class TextNewsMapper {

    fun map(jsonDto: TextDocDto?, omitTime: Boolean = false): NewsItem.Text {
        var newsDate: Date? = safeParseStringToDate(jsonDto?.timestamp?.value)
        val source = jsonDto?.source ?: ""
        var symbols: List<String> = emptyList()
        jsonDto?.symbols?.let {
            if (!it.isBlank()) {
                symbols = it.split(",")
            }
        }
        return NewsItem.Text(
            newsDateTimeSource = parseDateTimeSource(newsDate, source, omitTime),
            title = convertTextWithHmlTagsToSpannable(jsonDto?.title ?: "").toString(),
            url = jsonDto?.url ?: "",
            symbols = symbols,
            content = convertTextWithHmlTagsToSpannable(jsonDto?.content ?: "").toString(),
            docId = jsonDto?.docId ?: ""
        )
    }

    private fun parseDateTimeSource(newsDate: Date?, source: String, omitTime: Boolean): String {
        return if (newsDate != null) {
            val formattedDate = DateFormattingUtils.formatToShortDate(newsDate)
            val formattedTime =
                DateFormattingUtils.formatTimeWithoutSecondsWithShowingTimezone(newsDate.time)
            if (omitTime) {
                String.format(DATE_SOURCE_FORMAT, formattedDate, source)
            } else {
                String.format(DATE_TIME_SOURCE_FORMAT, formattedDate, formattedTime, source)
            }
        } else {
            source
        }
    }
}
