package com.etrade.mobilepro.trade.help

import android.annotation.SuppressLint
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.trade.help.databinding.TradeHelpItemBinding
import com.etrade.mobilepro.util.android.extension.viewBinding

class TradeHelpListAdapter : RecyclerView.Adapter<TradeHelpListAdapter.TradeFieldDefinitionItemViewHolder>() {

    var items = emptyList<TradeHelpItem>()
        @SuppressLint("NotifyDataSetChanged")
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TradeFieldDefinitionItemViewHolder {
        return TradeFieldDefinitionItemViewHolder(parent.viewBinding(TradeHelpItemBinding::inflate))
    }

    override fun onBindViewHolder(holder: TradeFieldDefinitionItemViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class TradeFieldDefinitionItemViewHolder(
        private val itemBinding: TradeHelpItemBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(item: TradeHelpItem) {
            itemBinding.apply {
                itemTitle.text = item.title
                itemDescription.text = item.description
            }
        }
    }
}
