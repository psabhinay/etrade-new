package com.etrade.mobilepro.quotes

import android.content.res.Resources
import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.NavDirections
import com.etrade.mobilepro.QuoteGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.navigation.EtNavController
import com.etrade.mobilepro.quotes.util.SecurityAndOrderTypeSelectionHelper
import com.etrade.mobilepro.trade.api.TradeOptionInfoType
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.viewdelegate.ViewDelegate

val TAG_SELECTOR_SECURITY_TYPE =
    "${TradeSelectionDelegate::class.java.canonicalName}.selector_security_type"
val TAG_SELECTOR_ORDER_TYPE =
    "${TradeSelectionDelegate::class.java.canonicalName}.selector_order_type"

class TradeSelectionDelegate(
    private val helper: SecurityAndOrderTypeSelectionHelper,
    private val manager: FragmentManager,
    private val etNavController: EtNavController,
    val resources: Resources
) : ViewDelegate {

    private lateinit var securityTypeDropDown: DropDownMultiSelector<ActionItem>
    private lateinit var orderTypeDropDown: DropDownMultiSelector<ActionItem>

    override fun observe(owner: LifecycleOwner) {
        helper.apply {
            selectionState.observe(owner, Observer { onSelectionStateChange(it) })
            navigateToTradeSignal.observe(owner, Observer { navigateToTrade(it) })
        }
    }

    fun setup() {
        buildDropDowns()
    }

    private fun buildDropDowns() {
        securityTypeDropDown = buildDropDownSelector(
            tag = TAG_SELECTOR_SECURITY_TYPE,
            title = R.string.security_type,
            itemsData = helper.securityTypeActionItems,
            helpType = TradeOptionInfoType.SECURITY_TYPE_OPTIONS
        )

        orderTypeDropDown = buildDropDownSelector(
            tag = TAG_SELECTOR_ORDER_TYPE,
            title = R.string.order_type,
            itemsData = helper.orderTypeActionItems,
            helpType = TradeOptionInfoType.ORDER_TYPE_OPTIONS
        )
    }

    private fun buildDropDownSelector(
        tag: String,
        @StringRes title: Int,
        itemsData: LiveData<List<DropDownMultiSelector.Selectable<ActionItem>>>,
        helpType: TradeOptionInfoType
    ) = DefaultDropDownMultiSelector(
        supportFragmentManager = manager,
        settings = DropDownMultiSelector.Settings(
            tag = tag,
            multiSelectEnabled = false,
            title = resources.getString(title),
            itemsData = itemsData,
            selectListener = { _, item ->
                helper.selectAction(item.value)
            },
            onCancelListener = {
                helper.cancelActions()
            },
            toolbarActionEnd = buildToolbarActionEnd(helpType)
        )
    )

    private fun buildToolbarActionEnd(helpType: TradeOptionInfoType): ToolbarActionEnd =
        ToolbarActionEnd(
            isIcon = true,
            icon = R.drawable.ic_help,
            contentDescription = resources.getString(com.etrade.mobilepro.trade.optionlegselect.R.string.help),
            onClick = View.OnClickListener {
                etNavController.navigate(
                    QuoteGraphDirections.actionToTradeHelpFragmentInQuoteGraph(
                        helpType,
                        learnOtherPriceTypes = false,
                        skipTitleUpdate = true
                    )
                )
            }
        )

    private fun navigateToTrade(navSignal: ConsumableLiveEvent<NavDirections>?) {
        navSignal?.consume { navDirections ->
            etNavController.navigate(navDirections)
        }
    }

    private fun onSelectionStateChange(state: SecurityAndOrderTypeSelectionHelper.UserSelectionState?) {
        when (state) {
            SecurityAndOrderTypeSelectionHelper.UserSelectionState.IDLE, null -> {
                securityTypeDropDown.close()
                orderTypeDropDown.close()
            }
            SecurityAndOrderTypeSelectionHelper.UserSelectionState.SELECT_SECURITY_TYPE -> {
                helper.loadSecurityType()
                securityTypeDropDown.open()
            }
            SecurityAndOrderTypeSelectionHelper.UserSelectionState.SELECT_ORDER_TYPE -> {
                helper.loadOrderType()
                orderTypeDropDown.open()
            }
        }
    }
}
