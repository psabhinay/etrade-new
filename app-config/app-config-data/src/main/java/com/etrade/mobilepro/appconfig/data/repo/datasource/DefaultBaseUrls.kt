package com.etrade.mobilepro.appconfig.data.repo.datasource

import com.etrade.mobilepro.appconfig.api.BaseUrls

internal val sitBaseUrls = BaseUrls(
    web = "https://us.sit.etrade.com",
    express = "https://express.sit.etrade.com",
    mobileTrade = "https://mobiletrade.sit.etrade.com",
    marketOnDemand = "https://test.etrade.wallst.com/",
    api = "https://api.sit.etrade.com",
    level1StreamerUrl = "https://lightstreamer.sit.etrade.com",
    level2StreamerUrl = "https://l2ls.sit.etrade.com",
    mobileETrade = "https://mobile.sit.etrade.com",
    environmentChange = "https://mobile.sit.etrade.com"
)

internal val uatBaseUrl = BaseUrls(
    web = "https://us.uat.etrade.com", // <MyETUrl>https://us.uat.etrade.com</MyETUrl>
    express = "https://express.uat.etrade.com",
    mobileTrade = "https://mobiletrade.uat.etrade.com", // <TradingUrl>https://mobiletrade.uat.etrade.com</TradingUrl>
    marketOnDemand = "https://test.etrade.wallst.com/", // <MODUrl>https://test.etrade.wallst.com</MODUrl>
    api = "https://api.uat.etrade.com", // <APIUrl>https://api.uat.etrade.com</APIUrl>
    level1StreamerUrl = "https://lightstreamer.uat.etrade.com",
    level2StreamerUrl = "https://l2ls.uat.etrade.com",
    mobileETrade = "https://mobile.uat.etrade.com",
    environmentChange = "https://mobile.uat.etrade.com"
)

internal val prodBaseUrls = BaseUrls(
    web = "https://us.etrade.com",
    eDocWeb = "https://edoc.etrade.com",
    express = "https://express.etrade.com",
    mobileTrade = "https://mobiletrade.etrade.com",
    marketOnDemand = "https://www.etrade.wallst.com/",
    api = "https://api.etrade.com",
    level1StreamerUrl = "https://lightstreamer.etrade.com",
    level2StreamerUrl = "https://l2ls.etrade.com",
    mobileETrade = "https://mobile.etrade.com",
    environmentChange = "https://mobile.etrade.com"
)

internal val m5BaseUrls = BaseUrls(
    web = "https://us1w301m5-lb.etrade.com",
    express = "https://express.sit.etrade.com",
    mobileTrade = "https://mobiletrade1w303m5-lb.etrade.com",
    marketOnDemand = "https://www.etrade.wallst.com/",
    api = "https://api1w303m5-lb.etrade.com",
    level1StreamerUrl = "https://lightstreamer.etrade.com",
    level2StreamerUrl = "https://l2ls.etrade.com",
    mobileETrade = "https://mobile.sit.etrade.com",
    environmentChange = "https://mobile1w403m5-lb.etrade.com"
)

internal val drBaseUrls = BaseUrls(
    web = "https://us-c.etrade.com",
    express = "https://express.sit.etrade.com",
    mobileTrade = "https://mobiletrade-c.etrade.com",
    marketOnDemand = "https://www.etrade.wallst.com/",
    api = "https://api-c.etrade.com",
    level1StreamerUrl = "https://lightstreamer.etrade.com",
    level2StreamerUrl = "https://l2ls.etrade.com",
    mobileETrade = "https://mobile.sit.etrade.com",
    environmentChange = "https://mobile1m7-lb.etrade.com"
)

internal val pltBaseUrls = BaseUrls(
    web = "https://us.sit.etrade.com",
    express = "https://express.sit.etrade.com",
    mobileTrade = "https://mobiletrade.sit.etrade.com",
    marketOnDemand = "https://test.etrade.wallst.com/",
    api = "https://api.sit.etrade.com",
    level1StreamerUrl = "https://lightstreamer.sit.etrade.com",
    level2StreamerUrl = "https://l2ls.sit.etrade.com",
    mobileETrade = "https://mobile.sit.etrade.com",
    environmentChange = "https://pplt16w76m7.etrade.com"
)

internal val mockBaseUrls = BaseUrls(
    web = "https://us.sit.etrade.com",
    express = "https://express.sit.etrade.com",
    mobileTrade = "https://mobiletrade.sit.etrade.com",
    marketOnDemand = "https://test.etrade.wallst.com/",
    api = "https://api.sit.etrade.com",
    level1StreamerUrl = "https://lightstreamer.sit.etrade.com",
    level2StreamerUrl = "https://l2ls.sit.etrade.com",
    mobileETrade = "https://mobile.sit.etrade.com",
    environmentChange = "https://mobile.mock.etrade.com"
)

internal val drExternalBaseUrls = BaseUrls(
    web = "https://us-c.etrade.com",
    express = "https://express-c.etrade.com",
    mobileTrade = "https://mobile-c.etrade.com",
    marketOnDemand = "https://www.etrade.wallst.com",
    api = "https://api-c.etrade.com",
    level1StreamerUrl = "https://lightstreamer.etrade.com",
    level2StreamerUrl = "https://l2ls.etrade.com",
    mobileETrade = "https://mobile-c.etrade.com",
    environmentChange = "https://mobile-c.etrade.com"
)

internal val m3_onlyExternalBaseUrls = BaseUrls(
    web = "https://us-a.etrade.com",
    express = "https://express-a.etrade.com",
    mobileTrade = "https://mobiletrade-a.etrade.com",
    marketOnDemand = "https://www.etrade.wallst.com",
    api = "https://api-a.etrade.com",
    level1StreamerUrl = "https://lightstreamer-a.etrade.com",
    level2StreamerUrl = "https://l2ls-a.etrade.com",
    mobileETrade = "https://mobile-a.etrade.com",
    environmentChange = "https://mobile-a.etrade.com"
)

internal val m5_onlyExternalBaseUrls = BaseUrls(
    web = "https://us-b.etrade.com",
    express = "https://express-b.etrade.com",
    mobileTrade = "https://mobiletrade-b.etrade.com",
    marketOnDemand = "https://www.etrade.wallst.com",
    api = "https://api-b.etrade.com",
    level1StreamerUrl = "https://lightstreamer-b.etrade.com",
    level2StreamerUrl = "https://l2ls-b.etrade.com",
    mobileETrade = "https://mobile-b.etrade.com",
    environmentChange = "https://mobile-b.etrade.com"
)
