package com.etrade.mobilepro.session

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.lang.reflect.Type

class ConverterWithSessionValidationFactory(
    private val converterFactory: Converter.Factory,
    private val sessionTerminationStateProvider: SessionTerminationStateProvider
) : Converter.Factory() {

    @Suppress("DEPRECATION") // SimpleXML is deprecated
    override fun responseBodyConverter(type: Type, annotations: Array<Annotation>, retrofit: Retrofit): Converter<ResponseBody, *>? {
        return converterFactory.responseBodyConverter(type, annotations, retrofit)?.let {
            when (converterFactory) {
                is retrofit2.converter.simplexml.SimpleXmlConverterFactory -> SimpleXmlConverterWithSessionValidation(it, sessionTerminationStateProvider)
                is MoshiConverterFactory -> MoshiConverterWithSessionValidation(it, sessionTerminationStateProvider)
                else -> it
            }
        }
    }

    override fun requestBodyConverter(
        type: Type,
        parameterAnnotations: Array<Annotation>,
        methodAnnotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<*, RequestBody>? {
        return converterFactory.requestBodyConverter(type, parameterAnnotations, methodAnnotations, retrofit)
    }

    override fun stringConverter(type: Type, annotations: Array<Annotation>, retrofit: Retrofit): Converter<*, String>? {
        return converterFactory.stringConverter(type, annotations, retrofit)
    }
}
