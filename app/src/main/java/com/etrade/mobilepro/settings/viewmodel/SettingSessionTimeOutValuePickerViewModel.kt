package com.etrade.mobilepro.settings.viewmodel

import android.content.res.Resources
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.R
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.session.TimeoutOption
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.timeout.setting.service.dto.RequestDetail
import com.etrade.mobilepro.timeout.setting.service.dto.TimeoutSettingRequestDto
import com.etrade.mobilepro.timeout.setting.service.repo.TimeoutSettingRepo
import com.etrade.mobilepro.user.session.manager.SessionTimeoutManager
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

private const val PROFILE_NAME = "WEB_SESSION_TIMEOUT"

class SettingSessionTimeOutValuePickerViewModel(
    override val title: String,
    private val res: Resources,
    override val layoutId: Int,
    private val user: User,
    private val userPreferences: UserPreferences,
    private val timeoutSettingRepo: TimeoutSettingRepo,
    private val UserSessionManager: SessionTimeoutManager
) : SettingValuePickerPayload(title, layoutId, res) {

    override val items: List<String>?
        get() = TimeoutOption.titles(res)

    override val value: String?
        get() = items?.get(position)

    override val contentDescription: String?
        get() = "$title $value"

    override var position: Int = timeout?.let { timeout ->
        TimeoutOption.getIndexByTime(timeout.toLong())
    } ?: 0

    private val timeout: Int?
        get() = user.session?.webSessionTimeout?.takeIf { it > 0 } ?: userPreferences.getTimeOutInSeconds()
            .takeIf { it > 0 }

    override val ghostText: String?
        get() = res.getString(R.string.time_out_option_ghost_text)

    override val onSelected: ((Any) -> Unit)? = { it ->
        (it as? String)?.also { selectedTitle ->
            updateWebTimeout(
                TimeoutOption.values().getOrElse(
                    TimeoutOption.getIndexByTitle(
                        res,
                        selectedTitle
                    )
                ) { TimeoutOption.THIRTY_MINUTES }.timeoutInSeconds
            )
        }
    }

    private fun updateWebTimeout(timeout: Long) {
        viewModelScope.launch {
            timeoutSettingRepo.setTimeout(TimeoutSettingRequestDto(RequestDetail(PROFILE_NAME, timeout.toString()))).fold(
                onSuccess = { settingMessage ->
                    if (settingMessage.isSuccess) {
                        val timeoutInt = timeout.toInt()
                        user.session?.webSessionTimeout = timeoutInt
                        userPreferences.setTimeOutInSeconds(timeoutInt)
                        UserSessionManager.initSessionTimeout(TimeUnit.SECONDS.toMillis(timeout))
                    }
                },
                onFailure = { throwable ->
                    logger.error("Error when update user session timeout", throwable)
                }
            )
        }
    }
}
