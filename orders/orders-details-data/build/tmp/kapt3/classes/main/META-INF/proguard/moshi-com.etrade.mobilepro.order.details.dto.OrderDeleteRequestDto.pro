-if class com.etrade.mobilepro.order.details.dto.OrderDeleteRequestDto
-keepnames class com.etrade.mobilepro.order.details.dto.OrderDeleteRequestDto
-if class com.etrade.mobilepro.order.details.dto.OrderDeleteRequestDto
-keep class com.etrade.mobilepro.order.details.dto.OrderDeleteRequestDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
