package com.etrade.mobilepro.messagecenter.presentation.details.support

import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.Reply
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.SupportMessageHolder
import com.etrade.mobilepro.util.delegate.consumable

internal data class SupportMessageViewState(
    val status: Status = Status.Idle,
    val replyBlockIsVisible: Boolean = false,
    val bottomButtonsStatus: BottomButtonsStatus = BottomButtonsStatus.Hidden,
    val itemsHolder: SupportMessageHolder = SupportMessageHolder(),
    private val _needToRefresh: Boolean = false,
    private val _messageSent: Boolean = false
) {
    val needToRefreshList by consumable(_needToRefresh)
    val items = itemsHolder.toList().filter { replyBlockIsVisible || it !is Reply }
    val messageSent by consumable(_messageSent)
    sealed class Status {
        object Idle : Status()
        object Loading : Status()
        class Error(
            val retry: () -> Unit
        ) : Status()
    }

    sealed class BottomButtonsStatus {
        object Hidden : BottomButtonsStatus()
        object ReplyInvitation : BottomButtonsStatus()
        object SendAndCancel : BottomButtonsStatus()
    }

    fun bottomButtonsAreVisible(): Boolean =
        status == Status.Idle && bottomButtonsStatus != BottomButtonsStatus.Hidden

    fun copyWithoutTouchingRefreshFlag(
        replyBlockIsVisible: Boolean = this.replyBlockIsVisible,
        bottomButtonsStatus: BottomButtonsStatus = this.bottomButtonsStatus
    ): SupportMessageViewState =
        this.copy(
            replyBlockIsVisible = replyBlockIsVisible,
            bottomButtonsStatus = bottomButtonsStatus,
            _needToRefresh = _needToRefresh && needToRefreshList == true
        )

    fun setReplyContent(content: String) {
        itemsHolder.reply.content = content
    }

    fun getReplyContent(): String = itemsHolder.reply.content
}
