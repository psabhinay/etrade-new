package com.etrade.mobilepro.news.base

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.PagingData
import app.cash.turbine.test
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.common.readstatus.ReadableType
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.api.model.NewsSettings
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.api.repo.NewsRepo
import com.etrade.mobilepro.news.data.repo.DefaultNewsRepo
import com.etrade.mobilepro.news.data.rest.NewsApiClient
import com.etrade.mobilepro.news.presentation.base.BaseNewsViewModel
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentLoader
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsFontAdjustHandler
import com.etrade.mobilepro.util.android.recyclerviewutil.SelectionTracker
import com.etrade.mobilepro.watchlist.repo.CombinedWatchlistRepo
import com.etrade.mobilepro.watchlist.repo.DeviceWatchlistRepo
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class BaseNewsViewModelTest {
    @ExperimentalCoroutinesApi
    private val testDispatcher = TestCoroutineDispatcher()

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var sut: BaseNewsViewModel

    @Mock
    private lateinit var contentLoader: TextNewsContentLoader
    @Mock
    private lateinit var statusRepo: ReadStatusRepo
    @Mock
    private lateinit var newsApiClient: NewsApiClient
    @Mock
    private lateinit var fontAdjuster: TextNewsFontAdjustHandler
    @Mock
    private lateinit var selectionTracker: SelectionTracker
    @Mock
    private lateinit var deviceWatchlistRepo: DeviceWatchlistRepo
    @Mock
    private lateinit var combinedWatchlistRepo: CombinedWatchlistRepo

    private lateinit var newsRepo: NewsRepo

    private val dummyFirstItem = NewsItem.Text("datetimesource", "testtitle", "testurl", emptyList(), "testcontent", "testDocIT")
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        MockitoAnnotations.openMocks(this)

        newsRepo = Mockito.spy(
            DefaultNewsRepo(
                apiClient = newsApiClient,
                newsSettings = NewsSettings(),
                deviceWatchlistRepo = deviceWatchlistRepo,
                combinedWatchlistRepo
            )
        )

        sut = object : BaseNewsViewModel(newsRepo, statusRepo, contentLoader, fontAdjuster, selectionTracker) {
            override fun getNewsType(): NewsType = NewsType.MARKET
        }
    }

    @Test
    fun `when state changes from single pane to dual pane and we have first loaded item, this item is selected and loaded`() {
        // given
        sut.isInDualPane = false

        // when
        testDispatcher.runBlockingTest {
            sut.pagingDataFlow.test {
                assertEquals(awaitItem(), PagingData.empty<NewsItem>())
                sut.refresh(true)
                assertNotEquals(awaitItem(), PagingData.empty<NewsItem>())
                cancelAndConsumeRemainingEvents()
            }
            verify(newsRepo).news(eq(NewsType.MARKET), eq(null), eq(null))
        }

        sut.handleFirstLoadedItem(dummyFirstItem)
        sut.isInDualPane = true

        verify(statusRepo, times(1)).markAsRead(dummyFirstItem.docId, ReadableType.NEWS_ARTICLE)
        verify(contentLoader, times(1)).load(dummyFirstItem.docId)
        verify(selectionTracker, times(1)).itemWasSelectedAt(0)
        sut.selectedNewsItem
            .test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it == dummyFirstItem
            }
    }

    @Test
    fun `check refresh with param passed to repo portfolio news`() {
        sut = object : BaseNewsViewModel(newsRepo, statusRepo, contentLoader, fontAdjuster, selectionTracker) {
            override fun getNewsType(): NewsType = NewsType.PORTFOLIO
        }

        testDispatcher.runBlockingTest {
            sut.pagingDataFlow.test {
                assertEquals(awaitItem(), PagingData.empty<NewsItem>())
                sut.refreshWithParams(NewsRequest.Portfolio())
                assertNotEquals(awaitItem(), PagingData.empty<NewsItem>())
                cancelAndConsumeRemainingEvents()
            }
            verify(newsRepo).news(eq(NewsType.PORTFOLIO), eq(NewsRequest.Portfolio()), eq(null))
        }
    }

    @Test
    fun `check refresh with header and param passed to repo company news`() {
        val headerItemSource: Observable<List<NewsItem>> = Observable.just(
            listOf(
                NewsItem.CompanyHeader(
                    quoteWidgetData = null,
                    onSearchFieldTapListener = null,
                    onSetAlertListener = null,
                    onAddToWatchlistListener = null,
                    onTradeListener = null,
                    subsequentFirstItem = null
                )
            )
        )

        sut = object : BaseNewsViewModel(newsRepo, statusRepo, contentLoader, fontAdjuster, selectionTracker) {
            override fun getNewsType(): NewsType = NewsType.COMPANY

            override fun provideHeaderItemsSource(): Observable<List<NewsItem>> {
                return headerItemSource
            }
        }

        testDispatcher.runBlockingTest {
            sut.pagingDataFlow.test {
                assertEquals(awaitItem(), PagingData.empty<NewsItem>())
                sut.refreshWithParams(NewsRequest.Symbol(listOf("MS", "FB", "AAPL")))
                assertNotEquals(awaitItem(), PagingData.empty<NewsItem>())
                cancelAndConsumeRemainingEvents()
            }
            verify(newsRepo).news(eq(NewsType.COMPANY), eq(NewsRequest.Symbol(listOf("MS", "FB", "AAPL"))), eq(headerItemSource))
        }
    }
}
