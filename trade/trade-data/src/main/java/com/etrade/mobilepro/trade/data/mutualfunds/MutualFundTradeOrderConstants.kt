package com.etrade.mobilepro.trade.data.mutualfunds

import com.etrade.mobilepro.trade.data.CommonOrderConstants

// Mutual Fund Sell/Exchange Order Quantity Type Codes
internal const val PLATFORM = CommonOrderConstants.PLATFORM
internal const val QUANTITY_CASH_POSITION = "Cash Position"
internal const val QUANTITY_DOLLARS = "3"
internal const val QUANTITY_ENTIRE_POSITION = "4"
internal const val QUANTITY_MARGIN_POSITION = "Margin position"
internal const val POSITION_QUANTITY_SUBTYPE = "positionQuantity"
internal const val QUANTITY_SHARES = "2"
internal const val QUANTITY_TYPE_BUY = "3"

internal const val CASH_MARGIN_POSITION = "1"
internal const val CASH_POSITION = "2"
internal const val MARGIN_POSITION = "3"

// Mutual Fund Trade Order Action Codes
internal const val BUY_FUNDS_ORDER = "1"
internal const val SELL_FUNDS_ORDER = "2"
internal const val EXCHANGE_FUNDS_ORDER = "5"
internal const val UNKNOWN_FUNDS_ORDER = "-1"
