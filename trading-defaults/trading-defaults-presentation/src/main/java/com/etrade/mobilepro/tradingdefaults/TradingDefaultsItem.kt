package com.etrade.mobilepro.tradingdefaults

import androidx.lifecycle.MutableLiveData

sealed class TradingDefaultsItem {

    data class EditorValue(val hint: String, val value: MutableLiveData<String>) : TradingDefaultsItem()

    data class PickerValue(val hint: String, val value: String, val showValuePicker: () -> Unit) : TradingDefaultsItem()

    data class DividerTitle(val title: String) : TradingDefaultsItem()

    object Divider : TradingDefaultsItem()
}
