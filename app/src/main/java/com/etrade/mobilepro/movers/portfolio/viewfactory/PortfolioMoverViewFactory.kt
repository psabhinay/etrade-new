package com.etrade.mobilepro.movers.portfolio.viewfactory

import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.R
import com.etrade.mobilepro.WithAccountId
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicWidget
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.movers.MoverItemAbsPercentComparator
import com.etrade.mobilepro.movers.Movers
import com.etrade.mobilepro.movers.MoversRouter
import com.etrade.mobilepro.movers.portfolio.GetPortfolioMoversUseCase
import com.etrade.mobilepro.movers.portfolio.PortfolioMoversViewModel
import com.etrade.mobilepro.movers.portfolio.dto.PortfolioMoverViewDto
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.ClickAction
import com.etrade.mobilepro.util.android.CallToAction
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.Collections
import javax.inject.Inject

class PortfolioMoverViewFactory @Inject constructor(
    private val getPortfolioMoversUseCase: GetPortfolioMoversUseCase,
    private val moversRouter: MoversRouter,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController,
    private val resources: Resources,
    private val userViewModel: UserViewModel,
    private val userPreferences: UserPreferences
) : DynamicUiViewFactory {

    private val logger: Logger = LoggerFactory.getLogger(PortfolioMoverViewFactory::class.java)
    private val title = resources.getString(R.string.portfolio_movers_title)
    private var accountId: String? = null
    private var callToAction: CallToAction? = null

    override fun setupValuesFromRequest(screenRequest: ScreenRequest) {
        accountId = (screenRequest as? WithAccountId)?.accountId
        callToAction =
            CallToAction(
                resources.getString(R.string.view_portfolio),
                ClickAction(null, null, resources.getString(R.string.portfolio_deep_link, accountId))
            )
    }

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is PortfolioMoverViewDto) {
            return object : GenericLayout, DynamicWidget {

                override val viewModelClass = Movers::class.java

                override val type = DynamicWidget.Type.MOVERS

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    toPortfolioMoverView(dto)
                }

                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is PortfolioMoversViewModel) {
                        viewModel.getMovers()
                    } else {
                        logger.error("view model is not of type PortfolioMoversViewModel")
                    }
                }

                override val uniqueIdentifier: String
                    get() = "${javaClass.name}_$accountId"
            }
        } else {
            throw DynamicUiViewFactoryException("PortfolioMoverViewFactory registered to unknown type")
        }
    }

    private fun toPortfolioMoverView(dto: PortfolioMoverViewDto): Movers {
        val moverItemsComparator = Collections.reverseOrder(MoverItemAbsPercentComparator())
        val callToAction = dto.ctaList?.let {
            CallToAction(it[0].label, ClickAction(null, null, it[0].clickActionDto.appUrl))
        } ?: callToAction
        return PortfolioMoversViewModel(
            getPortfolioMoversUseCase,
            moversRouter,
            streamingQuoteProvider,
            streamingStatusController,
            title,
            callToAction,
            moverItemsComparator,
            resources,
            userViewModel,
            userPreferences
        )
    }
}
