package com.etrade.mobilepro.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamic.form.fragment.createFormValuesArgument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.squareup.moshi.Moshi
import java.util.Locale
import javax.inject.Inject
import javax.inject.Provider

private const val SYMBOL_VALUE = "symbol"
private const val ORDER_TYPE_VALUE = "orderType"
private const val QUANTITY_VALUE = "quantity"
const val TRADE_PATH = "/tradeStock"

class TradeAppActionRedirectFragment @Inject constructor(
    private val moshi: Moshi,
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    private val userViewModel: UserViewModel
) : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        findNavController().popBackStack(R.id.tradeAppActionRedirectFragment, true)
        if (userViewModel.isAuthenticated) {
            val tradeParameterBuilder = tradeFormParametersBuilderProvider.get()
            val amount = arguments?.get(QUANTITY_VALUE) as? String
            val type = when (arguments?.get(ORDER_TYPE_VALUE) as? String) {
                "buy" -> TradeAction.BUY
                "sell" -> TradeAction.SELL
                else -> null
            }

            val formValues = tradeParameterBuilder.apply {
                // Stock is a default security type for trading, and index cannot be traded.
                symbol = (arguments?.get(SYMBOL_VALUE) as? String)?.uppercase(Locale.ROOT)?.let { Symbol(it, InstrumentType.EQ) }
                action = type
                quantity = amount?.toBigDecimalOrNull()
            }.create(SecurityType.STOCK)
            val params = createFormValuesArgument(moshi, formValues)
            findNavController().navigate(MainGraphDirections.actionLaunchTrade(params))
        } else {
            findNavController().navigate(R.id.accountsHomeFragment)
        }
    }
}
