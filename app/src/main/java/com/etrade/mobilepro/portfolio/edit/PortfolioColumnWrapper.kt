package com.etrade.mobilepro.portfolio.edit

import android.content.res.Resources
import com.etrade.mobilepro.editlist.EditListItem
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn

class PortfolioColumnWrapper(
    val column: PortfolioColumn,
    private val resources: Resources
) : EditListItem {

    override val displayText: CharSequence
        get() = resources.getText(column.reorderTextRes)

    override val isChecked: Boolean = true

    override val contentDescription: String
        get() = if (column.contentDescriptionRes == column.textRes) {
            displayText.toString()
        } else {
            resources.getText(column.contentDescriptionRes).toString()
        }
}
