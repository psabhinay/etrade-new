package com.etrade.mobilepro.watchlist.presentation

import android.content.res.Resources
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.tracking.SCREEN_TITLE_CUSTOMIZE_WATCH_LIST
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.WatchlistColumn
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnRepo
import kotlinx.coroutines.launch
import javax.inject.Inject

class EditWatchlistColumnsViewModel @Inject constructor(
    private val watchlistColumnRepo: WatchlistColumnRepo,
    private val resources: Resources
) : EditListViewModel<WatchlistColumnWrapper>(), ScreenViewModel {

    override val screenName: String? = SCREEN_TITLE_CUSTOMIZE_WATCH_LIST

    override fun onFetchItems(viewState: MutableLiveData<ViewState>) {
        viewModelScope.launch {
            provideItems(watchlistColumnRepo.getColumns().map { WatchlistColumnWrapper(it as WatchlistColumn, resources) })
            viewState.value = ViewState.Idle
        }
    }

    override fun onApplyChanges(
        items: List<WatchlistColumnWrapper>,
        removedItems: List<WatchlistColumnWrapper>,
        viewState: MutableLiveData<ViewState>
    ) {
        viewModelScope.launch {
            watchlistColumnRepo.saveColumns(items.map { it.column })
            viewState.value = ViewState.Success()
        }
    }
}
