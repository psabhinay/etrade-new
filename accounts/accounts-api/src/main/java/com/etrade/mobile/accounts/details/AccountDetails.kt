package com.etrade.mobile.accounts.details

import com.etrade.mobile.accounts.billpay.BillPay
import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount
import java.math.BigDecimal

interface AccountDetails : FundsFlowAccount {
    val accountIdKey: String
    val maskedAccountId: String
    val accountMode: String
    val accountName: String
    val accountShortName: String
    val accountLongName: String
    val mobileDescription: String
    val accountValue: BigDecimal?
    val institutionType: String
    val accountStatus: String
    val restrictionLevel: String
    val closedDate: Long
    val brokerage: Brokerage?
    val billPay: BillPay?

    interface Brokerage {
        val cashAvailableForWithdrawal: BigDecimal?
        val marginAvailableForWithdrawal: BigDecimal?
        val purchasingPower: BigDecimal?
        val totalAvailableForWithdrawal: BigDecimal?
        val marginBalance: BigDecimal?
        val netCashBalance: BigDecimal?
        val cmAccount: Boolean?
        val contributionDetails: List<ContributionDetails>
    }

    interface ContributionDetails {
        val contributionAmount: BigDecimal?
        val taxYear: Int
    }
}

fun AccountDetails.isEquals(second: AccountDetails): Boolean {
    return accountIdKey == second.accountIdKey &&
        maskedAccountId == second.maskedAccountId &&
        accountMode == second.accountMode &&
        accountName == second.accountName &&
        accountShortName == second.accountShortName &&
        accountLongName == second.accountLongName &&
        mobileDescription == second.mobileDescription &&
        accountValue == second.accountValue &&
        institutionType == second.institutionType &&
        accountStatus == second.accountStatus &&
        restrictionLevel == second.restrictionLevel &&
        closedDate == second.closedDate &&
        brokerage.isEquals(second.brokerage) &&
        billPay == second.billPay
}

fun AccountDetails.Brokerage?.isEquals(second: AccountDetails.Brokerage?): Boolean {
    return this?.cashAvailableForWithdrawal == second?.cashAvailableForWithdrawal &&
        this?.marginAvailableForWithdrawal == second?.marginAvailableForWithdrawal &&
        this?.purchasingPower == second?.purchasingPower &&
        this?.totalAvailableForWithdrawal == second?.totalAvailableForWithdrawal &&
        this?.marginBalance == second?.marginBalance &&
        this?.netCashBalance == second?.netCashBalance &&
        this?.cmAccount == second?.cmAccount &&
        this?.contributionDetails.isEquals(second?.contributionDetails)
}

fun Collection<AccountDetails.ContributionDetails>?.isEquals(
    second: Collection<AccountDetails.ContributionDetails>?
): Boolean {
    if (this == null && second == null) return true
    if (this?.size != second?.size) return false

    this?.forEachIndexed { index, item ->
        if (!item.isEquals(second?.elementAt(index))) {
            return false
        }
    }

    return true
}

fun AccountDetails.ContributionDetails?.isEquals(second: AccountDetails.ContributionDetails?): Boolean {
    return this?.contributionAmount == second?.contributionAmount &&
        this?.taxYear == second?.taxYear
}
