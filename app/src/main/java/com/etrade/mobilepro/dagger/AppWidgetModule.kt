package com.etrade.mobilepro.dagger

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.appwidget.AppWidgetHandler
import com.etrade.mobilepro.appwidget.DefaultAppWidgetHandler
import com.etrade.mobilepro.appwidget.DefaultWidgetSettingsRouter
import com.etrade.mobilepro.appwidget.api.WidgetViewStateRepo
import com.etrade.mobilepro.appwidget.api.settings.WidgetId
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettingsRepo
import com.etrade.mobilepro.appwidget.data.WidgetViewStateRepoImpl
import com.etrade.mobilepro.appwidget.data.settings.DefaultWidgetSettingsRepo
import com.etrade.mobilepro.appwidget.settings.WidgetSettingsFragment
import com.etrade.mobilepro.appwidget.settings.WidgetSettingsRouter
import com.etrade.mobilepro.appwidget.settings.WidgetSettingsViewModel
import com.etrade.mobilepro.appwidget.widgetcontainer.AddSymbolToWidgetWatchlistDelegate
import com.etrade.mobilepro.appwidget.widgetcontainer.OpenAccountsScreenDelegate
import com.etrade.mobilepro.appwidget.widgetcontainer.OpenQuoteLookupDelegate
import com.etrade.mobilepro.appwidget.widgetcontainer.OpenWatchlistDelegate
import com.etrade.mobilepro.appwidget.widgetcontainer.OpenWidgetSettingsDelegate
import com.etrade.mobilepro.caching.coroutine.Cache
import com.etrade.mobilepro.caching.coroutine.NO_EXPIRATION
import com.etrade.mobilepro.caching.coroutine.SimpleCache
import com.etrade.mobilepro.common.di.EntryPointActivity
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.home.ACTION_ADD_SYMBOL_TO_WIDGET_WATCHLIST
import com.etrade.mobilepro.home.ACTION_OPEN_ACCOUNTS
import com.etrade.mobilepro.home.ACTION_OPEN_WATCH_LIST
import com.etrade.mobilepro.home.ACTION_OPEN_WIDGET_SETTINGS
import com.etrade.mobilepro.home.EXTRA_WATCHLIST_ID
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.searchingapi.items.EXTRA_QUOTE_TICKER
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.util.JsonSerializer
import com.etrade.mobilepro.util.KeyValueStore
import com.etrade.mobilepro.util.Serializer
import com.etrade.mobilepro.util.sendIntentWith
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
abstract class AppWidgetModule {

    @Binds
    @Singleton
    abstract fun bindAppWidgetInterface(handler: DefaultAppWidgetHandler): AppWidgetHandler

    @Binds
    @Singleton
    abstract fun bindWidgetViewStateRepo(impl: WidgetViewStateRepoImpl): WidgetViewStateRepo

    @Binds
    @Singleton
    abstract fun bindWidgetSettingsRepo(repo: DefaultWidgetSettingsRepo): WidgetSettingsRepo

    @Binds
    @IntoMap
    @FragmentKey(WidgetSettingsFragment::class)
    abstract fun bindWidgetSettingsFragment(fragment: WidgetSettingsFragment): Fragment

    @Binds
    @IntoMap
    @ViewModelKey(WidgetSettingsViewModel::class)
    abstract fun bindWidgetSettingsViewModel(viewModel: WidgetSettingsViewModel): ViewModel

    @Binds
    abstract fun bindWidgetSettingsRouter(router: DefaultWidgetSettingsRouter): WidgetSettingsRouter

    companion object {
        @Provides
        fun provideAddSymbolToWidgetWatchlistDelegate(
            @EntryPointActivity activityClass: Class<out Activity>,
            context: Context
        ) = object : AddSymbolToWidgetWatchlistDelegate {
            override fun invoke() {
                sendIntentWith(context, activityClass) {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    action = ACTION_ADD_SYMBOL_TO_WIDGET_WATCHLIST
                }
            }
        }

        @Provides
        fun provideOpenQuoteLookupDelegate(
            @EntryPointActivity activityClass: Class<out Activity>,
            context: Context
        ) = object : OpenQuoteLookupDelegate {
            override fun invoke(symbol: String, typeCode: String) {
                sendIntentWith(context, activityClass) {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    val ticker = SearchResultItem.Symbol(symbol, InstrumentType.from(typeCode))
                    putExtra(EXTRA_QUOTE_TICKER, ticker.toBundle())
                }
            }
        }

        @Provides
        fun provideOpenWatchlistDelegate(
            @EntryPointActivity activityClass: Class<out Activity>,
            context: Context
        ) = object : OpenWatchlistDelegate {
            override fun invoke(watchlistId: String) {
                sendIntentWith(context, activityClass) {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    action = ACTION_OPEN_WATCH_LIST
                    putExtra(EXTRA_WATCHLIST_ID, watchlistId)
                }
            }
        }

        @Provides
        fun provideOpenAccountsDelegate(
            @EntryPointActivity activityClass: Class<out Activity>,
            context: Context
        ) = object : OpenAccountsScreenDelegate {
            override fun invoke() {
                sendIntentWith(context, activityClass) {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    action = ACTION_OPEN_ACCOUNTS
                }
            }
        }

        @Provides
        fun provideOpenWatchlistSelectionDelegate(
            @EntryPointActivity activityClass: Class<out Activity>,
            context: Context
        ) = object : OpenWidgetSettingsDelegate {
            override fun invoke(widgetId: Int) {
                sendIntentWith(context, activityClass) {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                    action = ACTION_OPEN_WIDGET_SETTINGS
                    putExtra(ACTION_OPEN_WIDGET_SETTINGS, widgetId)
                }
            }
        }

        @Provides
        fun provideWatchlistEntriesCache(
            keyValueStore: KeyValueStore,
            serializer: Serializer<List<WatchlistEntry>>
        ): Cache<List<WatchlistEntry>, String> = SimpleCache(
            expirationTimeInMillis = NO_EXPIRATION,
            serializer = serializer,
            keyValueStore = keyValueStore
        )

        @Provides
        fun provideWatchlistEntriesSerializer(moshi: Moshi): Serializer<List<WatchlistEntry>> =
            JsonSerializer<List<WatchlistEntry>>(moshi).apply { init<List<WatchlistEntry>>() }

        @Provides
        fun provideWidgetIdSerializer(moshi: Moshi): Serializer<WidgetId> =
            JsonSerializer<WidgetId>(moshi).apply { init<WidgetId>() }

        @Provides
        fun provideWidgetSettingsSerializer(moshi: Moshi): Serializer<WidgetSettings> =
            JsonSerializer<WidgetSettings>(moshi).apply { init<WidgetSettings>() }
    }
}
