package com.etrade.mobilepro.trade.mutualfund.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MutualFundFundsRequestDto(
    @Json(name = "value")
    val value: Value
) {
    companion object {
        fun toMutualFundSellRequest(accountId: String, orderAction: String, symbol: String = "", typeCode: String = "MF"): MutualFundFundsRequestDto =
            MutualFundFundsRequestDto(
                Value(
                    accountId = accountId,
                    orderAction = orderAction,
                    legDetails = listOf(LegDetails(symbol = symbol, typeCode = typeCode, transaction = "2"))
                )
            )
    }

    @JsonClass(generateAdapter = true)
    class Value(
        @Json(name = "accountId")
        val accountId: String = "",
        @Json(name = "orderAction")
        val orderAction: String = "",
        @Json(name = "legDetails")
        val legDetails: List<LegDetails>
    )

    @JsonClass(generateAdapter = true)
    class LegDetails(
        @Json(name = "symbol")
        val symbol: String = "",
        @Json(name = "typeCode")
        val typeCode: String = "",
        @Json(name = "transaction")
        val transaction: String = ""
    )
}
