package com.etrade.mobilepro.tradingdefaults.viewholder

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.tradingdefaults.TradingDefaultsItem
import com.etrade.mobilepro.tradingdefaults.databinding.AdapterTradingDefaultsPickerBinding
import com.etrade.mobilepro.util.android.extension.viewBinding

class TradingDefaultsPickerViewHolder(private val binding: AdapterTradingDefaultsPickerBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: TradingDefaultsItem.PickerValue) {
        binding.inputValueView.hint = item.hint
        binding.textInputEditText.apply {
            keyListener = null
            showSoftInputOnFocus = false

            setText(item.value)

            setOnClickListener { item.showValuePicker() }
        }
    }

    companion object {
        fun from(parent: ViewGroup) =
            TradingDefaultsPickerViewHolder(parent.viewBinding(AdapterTradingDefaultsPickerBinding::inflate))
    }
}
