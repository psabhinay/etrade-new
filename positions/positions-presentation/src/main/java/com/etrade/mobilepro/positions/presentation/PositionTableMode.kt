package com.etrade.mobilepro.positions.presentation

enum class PositionTableMode {
    ALL_BROKERAGE_VIEW,
    PORTFOLIO_VIEW
}
