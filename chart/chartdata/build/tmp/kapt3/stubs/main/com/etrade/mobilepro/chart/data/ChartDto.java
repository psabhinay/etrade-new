package com.etrade.mobilepro.chart.data;

import com.etrade.mobilepro.chart.api.ChartPoint;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B)\u0012\u0010\b\u0001\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\u0010\b\u0001\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0007R\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0019\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\t\u00a8\u0006\u000b"}, d2 = {"Lcom/etrade/mobilepro/chart/data/ChartDto;", "", "accounts", "", "Lcom/etrade/mobilepro/chart/data/AccountDataDto;", "indexes", "Lcom/etrade/mobilepro/chart/data/IndexDataDto;", "(Ljava/util/List;Ljava/util/List;)V", "getAccounts", "()Ljava/util/List;", "getIndexes", "chartdata"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ChartDto {
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<com.etrade.mobilepro.chart.data.AccountDataDto> accounts = null;
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<com.etrade.mobilepro.chart.data.IndexDataDto> indexes = null;
    
    public ChartDto(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "accountData")
    java.util.List<com.etrade.mobilepro.chart.data.AccountDataDto> accounts, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "indexData")
    java.util.List<com.etrade.mobilepro.chart.data.IndexDataDto> indexes) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.etrade.mobilepro.chart.data.AccountDataDto> getAccounts() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.etrade.mobilepro.chart.data.IndexDataDto> getIndexes() {
        return null;
    }
}