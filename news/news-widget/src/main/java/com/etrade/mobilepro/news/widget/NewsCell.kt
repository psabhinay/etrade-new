package com.etrade.mobilepro.news.widget

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.etrade.mobilepro.common.compose.theme.EtradeTheme
import com.etrade.mobilepro.common.compose.theme.etColors
import com.etrade.mobilepro.news.api.model.NewsItem

private const val TITLE_MAX_LINES = 3

@Suppress("LongMethod")
@Composable
internal fun NewsCell(
    item: NewsItem.Text,
    wasRead: Boolean,
    modifier: Modifier,
    onSymbolTap: (String) -> Unit
) {
    val mediumSpacing = dimensionResource(R.dimen.spacing_medium)
    val smallSpacing = dimensionResource(R.dimen.spacing_small)
    Row(modifier) {
        Column(
            modifier = Modifier
                .padding(start = mediumSpacing, top = mediumSpacing, bottom = mediumSpacing)
                .weight(1f)
        ) {
            Text(
                item.title,
                style = if (wasRead) {
                    newsReadTitleStyle()
                } else {
                    newsTitleStyle()
                },
                maxLines = TITLE_MAX_LINES,
                overflow = TextOverflow.Ellipsis
            )
            Text(item.newsDateTimeSource, style = newsSourceStyle())
            LazyRow {
                items(item.symbols) { symbol ->
                    Text(
                        symbol,
                        style = if (wasRead) {
                            newsReadSymbolsStyle()
                        } else {
                            newsSymbolsStyle()
                        },
                        modifier = Modifier
                            .clickable { onSymbolTap(symbol) }
                            .padding(end = smallSpacing)
                    )
                }
            }
        }
        Icon(
            painterResource(R.drawable.ic_arrow_right),
            stringResource(R.string.show_more_details),
            tint = colorResource(R.color.purple),
            modifier = Modifier
                .padding(start = smallSpacing, end = mediumSpacing)
                .requiredWidth(smallSpacing)
                .height(dimensionResource(R.dimen.spacing_small12))
                .align(Alignment.CenterVertically)
        )
    }
}

@Composable
internal fun NewsLoadingCell(modifier: Modifier) {
    Row(modifier, horizontalArrangement = Arrangement.Center) {
        CircularProgressIndicator(color = MaterialTheme.etColors.purple)
    }
}

// once all et redesign language is ported to Compose use that and remove these
@Suppress("MagicNumber")
@Composable
private fun newsTitleStyle() = MaterialTheme.typography.subtitle2.copy(
    color = MaterialTheme.etColors.purple,
    fontWeight = FontWeight(500),
    fontSize = 16.sp,
)

@Suppress("MagicNumber")
@Composable
private fun newsReadTitleStyle() = MaterialTheme.typography.subtitle2.copy(
    color = MaterialTheme.etColors.lightBlackOpacity30,
    fontWeight = FontWeight(500),
    fontSize = 16.sp,
)

@Suppress("MagicNumber")
@Composable
private fun newsSourceStyle() = MaterialTheme.typography.subtitle2.copy(
    fontWeight = FontWeight.Normal,
    fontSize = 14.sp
)

@Suppress("MagicNumber")
@Composable
private fun newsSymbolsStyle() = MaterialTheme.typography.subtitle2.copy(
    color = MaterialTheme.etColors.purple,
    fontWeight = FontWeight(500),
    fontSize = 14.sp
)

@Suppress("MagicNumber")
@Composable
private fun newsReadSymbolsStyle() = MaterialTheme.typography.subtitle2.copy(
    color = MaterialTheme.etColors.lightBlackOpacity30,
    fontWeight = FontWeight(500),
    fontSize = 14.sp
)

@Preview
@Composable
private fun NewsCellPreview() {
    EtradeTheme {
        NewsCell(
            item = NewsItem.Text(
                title = "AAPL test title",
                newsDateTimeSource = "12/05/21 | 01:43 AM | Bloomberg",
                url = "https://some url",
                symbols = listOf("AAPL", "FB", "TSLA"),
                content = "Some test content",
                docId = "docId01"
            ),
            modifier = Modifier,
            onSymbolTap = { },
            wasRead = false
        )
    }
}
