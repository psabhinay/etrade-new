package com.etrade.mobilepro.caching

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.threeten.bp.Duration
import org.threeten.bp.Instant

class TimeElapsedCacheControllerTest {

    @Test
    fun `indicates no refresh for item cached for duration under max threshold`() {
        val maxDuration = Duration.ofHours(1)
        val cacheDuration = Duration.ofMinutes(30)

        val now = Instant.now()
        val cachedAt = now.minus(cacheDuration)

        val sut = TimeElapsedCacheController<Any>({ now }, maxDuration) {
            cachedAt
        }

        assertFalse("should not refresh cache", sut.shouldRefresh(Any()))
    }

    @Test
    fun `indicates to refresh for item cached for duration over max threshold`() {
        val maxDuration = Duration.ofHours(1)
        val cacheDuration = Duration.ofHours(1) + Duration.ofMinutes(5)

        val now = Instant.now()
        val cachedAt = now.minus(cacheDuration)

        val sut = TimeElapsedCacheController<Any>({ now }, maxDuration) {
            cachedAt
        }

        assertTrue("should not refresh cache", sut.shouldRefresh(Any()))
    }
}
