package com.etrade.mobilepro.learn.presentation.tutorial

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.etrade.mobilepro.common.OverlayFragmentManagerHolder
import com.etrade.mobilepro.dialog.displayWalkthroughMessage
import com.etrade.mobilepro.dialog.hideWalkthroughMessage
import com.etrade.mobilepro.learn.presentation.R
import com.etrade.mobilepro.learn.presentation.databinding.LearnTutorialFragmentBinding
import com.etrade.mobilepro.learn.presentation.tutorials.ACTION
import com.etrade.mobilepro.learn.presentation.tutorials.ActionsMap.Companion.SYMBOL_TRACKING_DISMISS_DIALOG
import com.etrade.mobilepro.learn.presentation.tutorials.ActionsMap.Companion.TRADE_PLACING_DISMISS_DIALOG
import com.etrade.mobilepro.learn.presentation.tutorials.POSITIVE_ACTION_TEXT
import com.etrade.mobilepro.learn.presentation.tutorials.SUBTITLE_TEXT
import com.etrade.mobilepro.learn.presentation.tutorials.WalkthroughMessageFragment
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import com.etrade.mobilepro.walkthrough.api.Action
import com.etrade.mobilepro.walkthrough.api.SymbolLookupState
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingAction
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingState
import com.etrade.mobilepro.walkthrough.api.TradePlacingAction
import com.etrade.mobilepro.walkthrough.api.TradePlacingState
import com.etrade.mobilepro.walkthrough.api.WalkthroughScenario
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import javax.inject.Inject

private const val MESSAGE_FRAGMENT_TAG = "LearnTutorialFragment.message_fragment"

private const val SYMBOL_LOOK_UP_SCENARIO = "symbol_look_up"
private const val SYMBOL_TRACKING_SCENARIO = "symbol_tracking"
private const val TRADE_PLACING_SCENARIO = "trade_placing"
private const val BUNDLE_CONSUMED_FLAG = "key.bundle_consumed"

interface OpenSymbolLookupNavDelegate {
    fun invoke()
}

interface OpenTradePage {
    operator fun invoke()
}

class LearnTutorialFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    private val walkthroughStatesViewModel: WalkthroughStatesViewModel,
    private val openSymbolLookup: OpenSymbolLookupNavDelegate,
    private val openTradePage: OpenTradePage,
    private val overlayHolder: OverlayFragmentManagerHolder,
    private val fragmentFactory: FragmentFactory
) : Fragment(R.layout.learn_tutorial_fragment) {

    private val binding by viewBinding(LearnTutorialFragmentBinding::bind)
    private val viewModel: LearnTutorialViewModel by viewModels { viewModelFactory }
    private val tutorialAdapter: LearnTutorialAdapter
        get() = binding.tutorialRecyclerview.adapter as LearnTutorialAdapter
    private val args: LearnTutorialFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView(view)
        bindViewModel()
        if (savedInstanceState == null) { startWalkthroughIfNecessary() }
    }

    private fun setupRecyclerView(view: View) {
        binding.tutorialRecyclerview.apply {
            layoutManager = GridLayoutManager(view.context, 2, GridLayoutManager.VERTICAL, false)
            setHasFixedSize(true)
            adapter = LearnTutorialAdapter(view.context, viewModel::onTutorialItemClicked)
        }
    }

    private fun startWalkthroughIfNecessary() {
        if (arguments?.isConsumed == true) { return }
        val walkthroughScenario = args.walkthroughScenario?.map() ?: return

        arguments?.consume()
        viewModel.startWalkthrough(walkthroughScenario)
    }

    @Suppress("LongMethod")
    private fun bindViewModel() {
        viewModel.tutorialItems.observeBy(viewLifecycleOwner) {
            tutorialAdapter.setTutorialItems(it)
        }
        viewModel.walkthroughStates.observeBy(viewLifecycleOwner) {
            when (val state = it.getNonConsumedContent()) {
                is SymbolTrackingState -> { onSymbolTrackingState(state) }
                is SymbolLookupState -> { onSymbolLookupState(state) }
                is TradePlacingState -> { onTradePlacingState(state) }
            }
        }
    }

    private fun onSymbolTrackingState(state: SymbolTrackingState) {
        val symbolTrackingOverlay = ::getSymbolTrackingOverlay
        when (state) {
            SymbolTrackingState.Idle -> { hideOverlay(symbolTrackingOverlay) }
            SymbolTrackingState.DisplayInfoMessage -> {
                showOverlay(symbolTrackingOverlay, SymbolTrackingAction.Canceled)
            }
            SymbolTrackingState.LookupSymbol -> {
                hideOverlay(symbolTrackingOverlay)
                openSymbolLookup.invoke()
            }
        }
    }

    private fun onSymbolLookupState(state: SymbolLookupState) {
        if (state is SymbolLookupState.DisplayLookupScreen) {
            openSymbolLookup.invoke()
        }
    }

    private fun onTradePlacingState(state: TradePlacingState) {
        val tradePlacingOverlay = ::getTradePlacingOverlay
        when (state) {
            TradePlacingState.DisplayInfoMessage -> {
                showOverlay(tradePlacingOverlay, TradePlacingAction.Canceled)
            }
            TradePlacingState.DisplayTradePage -> {
                hideOverlay(tradePlacingOverlay)
                openTradePage()
            }
        }
    }

    private fun generateOverlay(
        @StringRes subTitle: Int,
        @StringRes positiveButtonText: Int,
        dismissAction: String
    ): Fragment {
        return Bundle().apply {
            putString(SUBTITLE_TEXT, resources.getString(subTitle))
            putString(POSITIVE_ACTION_TEXT, resources.getString(positiveButtonText))
            putString(ACTION, dismissAction)
        }.let {
            fragmentFactory.instantiateFragment(WalkthroughMessageFragment::class.java, it)
        }
    }

    private fun getSymbolTrackingOverlay(): Fragment {
        return generateOverlay(
            subTitle = R.string.learn_add_symbol_to_watchlist_dialog_description,
            positiveButtonText = R.string.learn_add_symbol_to_watchlist_dialog_positive_action,
            dismissAction = SYMBOL_TRACKING_DISMISS_DIALOG
        )
    }

    private fun getTradePlacingOverlay(): Fragment {
        return generateOverlay(
            subTitle = R.string.learn_trade_dialog_title,
            positiveButtonText = R.string.got_it,
            dismissAction = TRADE_PLACING_DISMISS_DIALOG
        )
    }

    private fun showOverlay(
        fragmentProvider: () -> Fragment,
        cancelAction: Action
    ) {
        overlayHolder.displayWalkthroughMessage(
            fragmentProvider = fragmentProvider,
            tag = MESSAGE_FRAGMENT_TAG,
            showAsDialogFragment = true,
            onDisplayError = {
                walkthroughStatesViewModel.onAction(cancelAction)
            }
        )
    }

    private fun hideOverlay(fragmentProvider: () -> Fragment) {
        overlayHolder.hideWalkthroughMessage(
            fragmentProvider = fragmentProvider,
            tag = MESSAGE_FRAGMENT_TAG,
            showAsDialogFragment = true
        )
    }

    override fun onDestroyView() {
        binding.tutorialRecyclerview.adapter = null
        super.onDestroyView()
    }
}

private fun String.map(): WalkthroughScenario? {
    return when (this) {
        SYMBOL_LOOK_UP_SCENARIO -> WalkthroughScenario.SYMBOL_LOOKUP
        SYMBOL_TRACKING_SCENARIO -> WalkthroughScenario.SYMBOL_TRACKING
        TRADE_PLACING_SCENARIO -> WalkthroughScenario.TRADE_PLACING
        else -> null
    }
}

private val Bundle.isConsumed: Boolean
    get() = containsKey(BUNDLE_CONSUMED_FLAG)

private fun Bundle.consume() {
    putBoolean(BUNDLE_CONSUMED_FLAG, true)
}
