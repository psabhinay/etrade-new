package com.etrade.mobilepro.util.updater

import android.content.ContentValues
import androidx.core.database.sqlite.transaction
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import io.reactivex.Single
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(application = android.app.Application::class)
class DeviceWatchlistMigrationSuite {

    private val repo = InMemoryWatchlistRepo()
    private val migration: Migration = DeviceWatchlistMigration(repo)

    @Test
    fun `test migration success`() = withDatabaseValues(listOf("QQQ", "AAPL", "SNAP", "FB")) { expected ->
        repo.entries.clear()
        repo.saveWatchlistResult = true
        migration.migrate(databaseFile)
        assertEquals(expected, repo.entries)
    }

    @Test
    fun `test empty watchlist`() = withDatabaseValues(emptyList()) {
        repo.entries.clear()
        repo.saveWatchlistResult = true
        migration.migrate(databaseFile)
        assertTrue(repo.entries.isEmpty())
    }

    @Test
    fun `test repo save failed`() = withDatabaseValues(listOf("QQQ", "AAPL", "SNAP", "FB")) {
        repo.entries.clear()
        repo.saveWatchlistResult = false
        migration.migrate(databaseFile)
        assertTrue(repo.entries.isEmpty())
    }

    private fun withDatabaseValues(entries: List<String>, block: suspend (List<String>) -> Unit) {
        withExistingDatabaseFile({ database ->
            database.transaction {
                execSQL("CREATE TABLE IF NOT EXISTS WATCHLISTENTRIES (EntryId INTEGER PRIMARY KEY AUTOINCREMENT, Symbol TEXT NOT NULL, TypeCode TEXT)")
                entries.forEach { entry ->
                    Assert.assertNotEquals(
                        -1,
                        insert(
                            "WATCHLISTENTRIES", null,
                            ContentValues().apply {
                                put("Symbol", entry)
                            }
                        )
                    )
                }
            }
        }) {
            block(entries)
        }
    }

    private class InMemoryWatchlistRepo : WatchlistRepo {

        val entries: MutableList<String> = mutableListOf()

        var saveWatchlistResult = false

        override fun getUserWatchlists(): Single<List<Watchlist>> {
            throw UnsupportedOperationException()
        }

        override fun getWatchlistEntries(watchlistId: String, isExtendedHours: Boolean): Single<List<WatchlistEntry>> {
            throw UnsupportedOperationException()
        }

        override fun getWatchlistSymbols(watchlistId: String): Single<List<String>> {
            throw UnsupportedOperationException()
        }

        override fun addToWatchList(watchlistId: String, ticker: String): Single<Boolean> {
            throw UnsupportedOperationException()
        }

        override fun saveWatchList(watchlistId: String, entries: List<WatchlistEntry>, removedEntries: List<WatchlistEntry>): Single<Boolean> {
            if (saveWatchlistResult) {
                this.entries.addAll(entries.map { it.ticker })
            }
            return Single.just(saveWatchlistResult)
        }

        override fun createWatchlist(watchlistName: String): Single<Watchlist> {
            throw UnsupportedOperationException()
        }

        override fun deleteWatchList(watchlistId: String): Single<Boolean> {
            throw UnsupportedOperationException()
        }

        override fun getAllWatchlistsEntries(isExtendedHours: Boolean): Single<List<WatchlistEntry>> {
            throw UnsupportedOperationException()
        }
    }
}
