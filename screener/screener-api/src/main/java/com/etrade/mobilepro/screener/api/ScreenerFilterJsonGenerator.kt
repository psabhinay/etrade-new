package com.etrade.mobilepro.screener.api

interface ScreenerFilterJsonGenerator {
    fun generateFiltersJson(
        filtersCollection: Collection<ScreenerFilter>,
        group: ScreenerType.Group
    ): String
}
