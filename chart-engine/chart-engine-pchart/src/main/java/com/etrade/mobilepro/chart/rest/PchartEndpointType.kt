package com.etrade.mobilepro.chart.rest

enum class PchartEndpointType(val typeCode: String) {
    PUBLIC("public"),
    SECURED("secured")
}
