package com.etrade.mobilepro.bloombergtv.presentation

import android.app.NotificationManager
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import android.os.IBinder
import com.etrade.mobilepro.bloombergtv.presentation.common.ACTION_EXIT
import com.etrade.mobilepro.bloombergtv.presentation.common.ACTION_OPEN_IN_APP
import com.etrade.mobilepro.bloombergtv.presentation.common.ACTION_PLAY
import com.etrade.mobilepro.bloombergtv.presentation.common.AUDIO_URL
import com.etrade.mobilepro.bloombergtv.presentation.delegate.CallStateDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.service.AudioFocusServiceDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.service.MediaPlayerServiceDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.service.MediaSessionServiceDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.service.NotificationServiceDelegate

private const val NOTIFICATION_ID = 40010

class BloombergTvAudioService : Service() {

    private val notificationManager get() = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
    private val notificationDelegate = NotificationServiceDelegate()
    private val mediaPlayerDelegate = MediaPlayerServiceDelegate()
    private val callStateDelegate = CallStateDelegate(
        onCallStarted = ::temporarilyPause,
        onCallEnded = ::resume
    )
    private val audioFocusServiceDelegate = AudioFocusServiceDelegate(
        onFocusGain = ::resume,
        onFocusLoss = ::temporarilyPause,
        onFocusLossTransient = ::temporarilyPause,
        onFocusLossCanDuck = { mediaPlayerDelegate.mute() }
    )
    private val mediaSessionServiceDelegate = MediaSessionServiceDelegate(::play, ::pause, ::stop)
    private val becomingNoisyReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            pause()
        }
    }

    private var isPlaybackActive = false

    override fun onCreate() {
        super.onCreate()
        notificationDelegate.init(baseContext)
        callStateDelegate.init(baseContext)
        audioFocusServiceDelegate.init(baseContext)
        mediaSessionServiceDelegate.init(baseContext)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        intent?.getStringExtra(AUDIO_URL)?.let {
            mediaPlayerDelegate.start(
                it,
                hasOngoingCall = { callStateDelegate.hasOngoingCall },
                onCompletion = ::stop,
                onError = ::stop,
            )
        }
        when (intent?.action) {
            ACTION_PLAY ->
                if (mediaPlayerDelegate.isPlaying()) {
                    pause()
                } else {
                    play()
                }
            ACTION_OPEN_IN_APP -> {
                startBloombergTvActivity()
                stop()
            }
            ACTION_EXIT -> stop()
        }
        return START_STICKY
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        stopSelf()
    }

    override fun onDestroy() {
        dismantle()
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? = null

    private fun play() {
        if (audioFocusServiceDelegate.requestFocus()) {
            isPlaybackActive = true
            mediaPlayerDelegate.unmute()
            playOrPause(true)
        }
    }

    private fun resume() {
        if (isPlaybackActive) {
            play()
        }
    }

    private fun pause(resumeWhenPossible: Boolean = false) {
        isPlaybackActive = resumeWhenPossible && isPlaybackActive
        playOrPause(false)
    }

    private fun temporarilyPause() {
        pause(resumeWhenPossible = true)
    }

    private fun stop() {
        stopSelf()
    }

    private fun playOrPause(play: Boolean) {
        if (play) {
            startForeground(NOTIFICATION_ID, notification(play))
            registerBecomingNoisyReceiver()
            mediaPlayerDelegate.play()
        } else {
            notificationManager.notify(NOTIFICATION_ID, notification(play))
            mediaPlayerDelegate.pause()
            unregisterBecomingNoisyReceiver()
        }
        mediaSessionServiceDelegate.setState(play)
    }

    private fun startBloombergTvActivity() {
        with(Intent(this, BloombergTvActivity::class.java)) {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(this)
        }
    }

    private fun dismantle() {
        unregisterBecomingNoisyReceiver()
        notificationManager.cancel(NOTIFICATION_ID)
        callStateDelegate.dismantle()
        mediaPlayerDelegate.dismantlePlayer()
        audioFocusServiceDelegate.dismantle()
        mediaSessionServiceDelegate.release()
    }

    private fun registerBecomingNoisyReceiver() {
        registerReceiver(
            becomingNoisyReceiver,
            IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY)
        )
    }

    private fun unregisterBecomingNoisyReceiver() {
        try {
            unregisterReceiver(becomingNoisyReceiver)
        } catch (e: IllegalArgumentException) {
            // receiver already unregistered
        }
    }

    private fun notification(isPlaying: Boolean) =
        notificationDelegate.buildNotification(mediaSessionServiceDelegate.token, isPlaying)
}
