package com.etrade.mobilepro.mod.auth

import com.etrade.mobilepro.mod.auth.api.AuthTokenProvider
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import retrofit2.Call
import retrofit2.Response

private val authToken = DefaultAuthTokenProvider::class.java.getResource("auth_token.txt").readText()

internal class DefaultAuthTokenProviderTest {

    private val response: Response<ModAuthService.WsodResearchReportUrlResponse> = Response.success(
        ModAuthService.WsodResearchReportUrlResponse(
            reportUrl = DefaultAuthTokenProviderTest::class.java.getResource("successful_wsod_url.txt").readText()
        )
    )

    private val invalidResponseBody: Response<ModAuthService.WsodResearchReportUrlResponse> = Response.success(
        ModAuthService.WsodResearchReportUrlResponse(reportUrl = "bad bad bad")
    )

    @Test
    fun `successfully obtains a mod auth token`() {
        val mockCall = mockResponse(response)

        val modAuthService: ModAuthService = mock {
            on { getWsodResearchReportUrl(any()) } doReturn mockCall
        }

        val sut: AuthTokenProvider = DefaultAuthTokenProvider(modAuthService)

        assertThat(sut.getModAuthToken(), `is`(authToken))
    }

    @Test
    fun `throws an exception for an invalid response url`() {
        val mockCall = mockResponse(invalidResponseBody)

        val modAuthService: ModAuthService = mock {
            on { getWsodResearchReportUrl(any()) } doReturn mockCall
        }

        val sut: AuthTokenProvider = DefaultAuthTokenProvider(modAuthService)

        assertThrows(AuthTokenProvider.ModAuthTokenException::class.java) {
            sut.getModAuthToken()
        }
    }

    private fun <T> mockResponse(response: Response<T>): Call<T> = mock {
        on { execute() } doReturn response
    }
}
