package com.etrade.mobilepro.checkdeposit.deposit.service

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class UploadImageDto(
    @Json(name = "value")
    val value: UploadImageValueDto
)

@JsonClass(generateAdapter = true)
class UploadImageValueDto(
    @Json(name = "imageFace")
    val imageFace: Int,
    @Json(name = "instNo")
    val instNo: String,
    @Json(name = "amount")
    val amount: String,
    @Json(name = "accountNo")
    val accountNo: String,
    @Json(name = "isUniversalApp")
    val isUniversalApp: String = "true",
    @Json(name = "depositImage")
    val depositImage: String
)

@JsonClass(generateAdapter = true)
class UploadImageResponseDto(
    @Json(name = "imageFace")
    val imageFace: String?,
    @Json(name = "Type")
    val type: String?,
    @Json(name = "TokenId")
    val tokenId: String?
) : BaseDataDto()
