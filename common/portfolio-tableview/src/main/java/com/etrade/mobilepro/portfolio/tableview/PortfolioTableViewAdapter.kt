package com.etrade.mobilepro.portfolio.tableview

import android.content.Context
import android.view.ViewGroup
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.tableviewutils.presentation.adapter.SymbolTableViewAdapter
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.DataCellViewHolder
import com.etrade.mobilepro.tableviewutils.presentation.holder.SymbolRowHeaderViewHolder
import com.etrade.mobilepro.tableviewutils.presentation.updateSortViewState
import com.etrade.mobilepro.util.formatLetterRepresentationToNumber
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

open class PortfolioTableViewAdapter(private val context: Context) : SymbolTableViewAdapter(context) {

    var data: PortfolioRecordsTableData = PortfolioRecordsTableData()
        set(value) {
            updateItems(value.columns, value.rows)
            field = value
        }

    var sortState: SortStateInfo =
        SortStateInfo()
        set(value) {
                field = value
                tableView.updateSortViewState(value)
            }

    private var prevSortState: SortStateInfo =
        SortStateInfo()

    override fun getCellItemViewType(row: Int, column: Int): Int = R.layout.table_view_cell_layout

    override fun getRowHeaderItemViewType(position: Int): Int =
        R.layout.table_view_row_header_layout

    override fun onBindCellViewHolder(holder: AbstractViewHolder, cellItemModel: Any, columnPosition: Int, rowPosition: Int) {
        super.onBindCellViewHolder(holder, cellItemModel, columnPosition, rowPosition)
        onBindCellViewHolderDelegate(rowPosition, columnPosition, cellItemModel as Cell, holder)
    }

    private fun onBindCellViewHolderDelegate(
        rowPosition: Int,
        columnPosition: Int,
        cell: Cell,
        holder: AbstractViewHolder
    ) {
        val rowCell = getRowHeaderItem(rowPosition) as? SymbolCell
        val column = getColumnHeaderItem(columnPosition)
        val contentDescriptionWithoutSymbol =
            "${column.displayTextContentDescription}, ${cell.displayText}".formatLetterRepresentationToNumber()

        val resultPrefix = getContentDescriptionPrefix(rowCell)
        val contentDescription = getCellContentDescription(resultPrefix, contentDescriptionWithoutSymbol)

        data.rows.getOrNull(rowPosition)?.let { row ->
            if (!row.isTaxLot) {
                holder.itemView.setBackgroundResource(row.backgroundColor)
            }
        }

        if (holder is DataCellViewHolder) {
            holder.contentDescription = contentDescription
        } else {
            holder.itemView.contentDescription = contentDescription
        }
    }

    open fun getCellContentDescription(
        symbol: String,
        contentDescriptionWithoutSymbol: String,
        rowData: PortfolioRecordsTableRow? = null,
        row: SymbolCell? = null
    ) = "$symbol, $contentDescriptionWithoutSymbol"

    override fun onCreateCellViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        return DataCellViewHolder(
            inflater.inflate(
                viewType,
                parent,
                false
            ),
            null
        )
    }

    override fun onCreateRowHeaderViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val itemView = inflater.inflate(viewType, parent, false)
        return when (viewType) {
            R.layout.table_view_row_header_tax_lot,
            R.layout.table_view_row_header_layout -> SymbolRowHeaderViewHolder(
                itemView
            )
            else -> throw UnsupportedOperationException("Unknown view type: $viewType")
        }
    }

    override fun getAllRowHeaderViewTypes(): List<Int> {
        return listOf(R.layout.table_view_row_header_layout)
    }

    override fun getAllCellViewTypes(): List<Int> {
        return listOf(R.layout.table_view_cell_layout)
    }

    override fun onBindRowHeaderViewHolder(holder: AbstractViewHolder, rowHeaderItemModel: Any, rowPosition: Int) {
        super.onBindRowHeaderViewHolder(holder, rowHeaderItemModel, rowPosition)
        onBindRowHeaderViewHolderDelegate(rowHeaderItemModel, holder, rowPosition)
    }

    open fun onBindRowHeaderViewHolderDelegate(
        rowHeaderItemModel: Any,
        holder: AbstractViewHolder,
        rowPosition: Int
    ) {
        val resultDescription = getContentDescriptionPrefix(rowHeaderItemModel as SymbolCell)

        data.rows.getOrNull(rowPosition)?.backgroundResource?.let {
            holder.itemView.setBackgroundResource(it)
        }
        holder.itemView.contentDescription = resultDescription
    }

    private fun getContentDescriptionPrefix(symbolCell: SymbolCell?): String {
        val symbolDesc = symbolCell?.getSymbolContentDescription() ?: ""
        // symbolCell?.description now is the value of symbolDescription in api response which
        // will never be the same as symbol anymore
        val description = symbolCell?.description ?: ""

        return if (symbolCell?.instrumentType?.isOption == true) {
            // replacing first word (symbol) in content description: e.g. "AAPL March 24, 2021 $150 put" -> "Ay Ay P L March..."
            "$symbolDesc, ${description.substring(description.indexOf(" ") + 1)}"
        } else {
            symbolDesc
        }
    }

    private fun updateItems(columnHeaders: List<PortfolioColumn>, rows: List<PortfolioRecordsTableRow>) {
        val (rowHeaders, cells) = rows.map { it.rowHeader to it.cells }.unzip()
        updateAllItems(columnHeaders.toColumnHeaderCells(), rowHeaders, cells, forceNotifyData = isColumnSortStateChange())
        prevSortState = sortState
    }

    private fun isColumnSortStateChange(): Boolean =
        (prevSortState.sortedColumnPosition != sortState.sortedColumnPosition || prevSortState.sortState != sortState.sortState)

    private fun List<PortfolioColumn>.toColumnHeaderCells(): List<Cell> = map {
        Cell(
            it.name,
            context.getString(it.textRes),
            context.getString(it.contentDescriptionRes)
        )
    }
}
