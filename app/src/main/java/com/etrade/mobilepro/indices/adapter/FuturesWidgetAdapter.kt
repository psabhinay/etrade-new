package com.etrade.mobilepro.indices.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.indices.IndexDataWithColor
import com.etrade.mobilepro.util.android.binding.liveTextColor
import com.etrade.mobilepro.util.android.extension.inflate

class FuturesWidgetAdapter : RecyclerView.Adapter<FuturesWidgetViewHolder>() {
    var futureIndicesItems: List<IndexDataWithColor> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FuturesWidgetViewHolder {
        return FuturesWidgetViewHolder(parent.inflate(R.layout.futures_widget_item))
    }

    override fun getItemCount(): Int = futureIndicesItems.size

    override fun onBindViewHolder(holder: FuturesWidgetViewHolder, position: Int) {
        holder.bind(futureIndicesItems.getOrNull(position))
    }
}

class FuturesWidgetViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    fun bind(item: IndexDataWithColor?) {
        item?.apply {
            view.findViewById<View>(R.id.future_indicator).setBackgroundResource(item.textColorRes)
            view.findViewById<TextView>(R.id.future_ticker).text = item.ticker
            view.findViewById<TextView>(R.id.future_description).text = item.displayName
            view.findViewById<TextView>(R.id.future_last).text = item.last
            view.findViewById<TextView>(R.id.future_change)?.let {
                it.text = item.change
                it.liveTextColor(item.textColorRes)
            }
        }
    }
}
