package com.etrade.mobilepro.positions.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PortfolioPositionLotsResponseDto(
    @Json(name = "PositionLotsResponse")
    val response: PositionLotsResponse
)

@JsonClass(generateAdapter = true)
class PositionLotsResponse(
    @Json(name = "PositionLotsResponse")
    val positionLots: List<PositionLot>
)

@JsonClass(generateAdapter = true)
class PositionLot(
    @Json(name = "positionLotId")
    val positionLotId: String
)
