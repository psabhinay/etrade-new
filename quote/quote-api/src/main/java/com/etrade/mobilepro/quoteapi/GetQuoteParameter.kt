package com.etrade.mobilepro.quoteapi

import com.etrade.mobilepro.searchingapi.items.SearchResultItem

class GetQuoteParameter(
    val symbol: SearchResultItem.Symbol,
    val isExtendedHours: Boolean
)
