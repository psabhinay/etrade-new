package com.etrade.mobilepro.marketscalendar.api.model

data class EventsResponseData<T>(
    val eventsResponseDm: EventsResponseDm<T>?
)

data class EventsResponseDm<T>(
    val eventsDataList: List<T>?
)

data class EventsResponse<T>(
    val eventsResponseData: EventsResponseData<T>?
)
