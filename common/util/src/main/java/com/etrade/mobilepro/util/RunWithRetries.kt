package com.etrade.mobilepro.util

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import kotlinx.coroutines.delay
import java.util.concurrent.TimeUnit
import kotlin.math.min

private const val RETRIES = 3
private const val DELAY_IN_SECONDS = 2L
private const val MAX_AWAIT_TIME = 5L
private const val JITTER_MULTIPLIER = 3

data class RunWithRetriesParam(
    val retries: Int = RETRIES,
    val delay: Long = DELAY_IN_SECONDS,
    val timeUnit: TimeUnit = TimeUnit.SECONDS,
    val maxAwaitTime: Long = timeUnit.convert(MAX_AWAIT_TIME, TimeUnit.MINUTES),
    val jitterMultiplier: Int = JITTER_MULTIPLIER
)

@Suppress("TooGenericExceptionCaught")
suspend fun <T> runWithRetries(
    param: RunWithRetriesParam = RunWithRetriesParam(),
    block: suspend (Int) -> T
): ETResult<T> {
    fun updateSleepTime(delay: Long): Long =
        min(param.maxAwaitTime, (delay..delay * param.jitterMultiplier).random())

    var currentDelay = param.delay
    for (i in 1 until param.retries) {
        try {
            delay(currentDelay)
            return ETResult.success(block.invoke(i))
        } catch (e: Exception) {
            /* retry */
        }
        currentDelay = updateSleepTime(currentDelay)
    }

    return runCatchingET { block.invoke(param.retries) }
}
