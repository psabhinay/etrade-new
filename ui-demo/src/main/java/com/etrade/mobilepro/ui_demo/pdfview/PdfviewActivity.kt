package com.etrade.mobilepro.ui_demo.pdfview

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.ui_demo.databinding.ActivityPdfviewBinding
import com.etrade.mobilepro.uiwidgets.pdfview.loadAndEnableStateSaving
import com.etrade.mobilepro.util.android.binding.viewBinding

class PdfViewActivity : AppCompatActivity() {
    private val binding by viewBinding(ActivityPdfviewBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.pdfView.fromAsset("testpdf3.pdf")
            .fitEachPage(true)
            .loadAndEnableStateSaving(binding.pdfView, "some_id")
    }
}
