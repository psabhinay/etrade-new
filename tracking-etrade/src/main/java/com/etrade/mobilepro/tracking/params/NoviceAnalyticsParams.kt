package com.etrade.mobilepro.tracking.params

import com.etrade.mobilepro.tracking.trackerValue

private const val NOVICE_TUTORIAL = "Novice Tutorial"

enum class Attribute(val text: String) {
    QUOTE_STARTED("Quote Started"),
    WATCHLIST_STARTED("Watch List Started"),
    TRADE_STARTED("Trade Started"),
    QUOTE_COMPLETED("Quote Completed"),
    WATCHLIST_COMPLETED("Watch List Completed"),
    TRADE_COMPLETED("Trade Completed");
}

sealed class NoviceAnalyticsParams(
    val eventType: Attribute
) : AnalyticsParams {

    override val name: String = NOVICE_TUTORIAL

    override val params: Map<String, String>
        get() = Attribute.values().associate {
            it.text to (it == eventType).trackerValue
        }
}

class QuoteStartedNoviceAnalyticParams : NoviceAnalyticsParams(Attribute.QUOTE_STARTED)
class QuoteCompletedNoviceAnalyticParams : NoviceAnalyticsParams(Attribute.QUOTE_COMPLETED)
class WatchlistStartedNoviceAnalyticParams : NoviceAnalyticsParams(Attribute.WATCHLIST_STARTED)
class WatchlistCompletedNoviceAnalyticParams : NoviceAnalyticsParams(Attribute.WATCHLIST_COMPLETED)
class TradeStartedNoviceAnalyticParams : NoviceAnalyticsParams(Attribute.TRADE_STARTED)
class TradeCompletedNoviceAnalyticParams : NoviceAnalyticsParams(Attribute.TRADE_COMPLETED)
