package com.etrade.mobilepro.feature

import com.etrade.mobilepro.feature.FeatureFlagCategories.MORGAN_STANLEY
import com.etrade.mobilepro.feature.FeatureFlagCategories.QUOTES

/**
 * Feature flags available in the E-Trade mobile app.
 *
 * Note that for a feature to be usable in Firebase Remote Config, its key may only contain letters,
 * numbers, and underscores, and cannot start with a number. The feature must also set [remoteConfig]
 * to true.
 *
 * @param [debugConfig] is used to determine whether this feature flag will only appear in debug builds
 */
enum class FeatureFlags(
    override val category: FeatureCategory,
    override val key: String,
    override val title: String,
    override val explanation: String,
    override val defaultValue: Boolean = false,
    val remoteConfig: Boolean,
    val debugConfig: Boolean = false
) : Feature {
    MS_ACCOUNTS(
        category = MORGAN_STANLEY,
        key = "feature_ms_accounts",
        title = "Morgan Stanley Accounts Integration",
        explanation = "Display Morgan Stanley accounts in the E*Trade app.",
        remoteConfig = false
    ),
    QUOTES_REFRESH(
        category = QUOTES,
        key = "feature_quotes_refresh",
        title = "Quotes 2.0",
        explanation = "Takes user to the new quotes experience in E*Trade app.",
        remoteConfig = false,
        debugConfig = true
    );

    companion object {
        fun getRemoteEnabledFeatures(): Set<Feature> = values()
            .filter { it.remoteConfig }
            .toSet()

        fun getAvailableFeatures(isDebugBuild: Boolean): List<Feature> = values().filter {
            if (it.debugConfig) {
                isDebugBuild
            } else {
                true
            }
        }
    }
}

enum class FeatureFlagCategories(
    override val title: String,
    override val explanation: String
) : FeatureCategory {
    MORGAN_STANLEY(
        title = "Morgan Stanley",
        explanation = "Features relating to the Morgan Stanley integration project"
    ),
    QUOTES(
        title = "Quotes 2.0",
        explanation = "Features relating to the Quote refresh project"
    ),
}
