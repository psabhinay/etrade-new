package com.etrade.mobilepro.dynamic.form.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.livedata.combine

interface InputEnabler {

    /**
     * Indicates if the input is enabled. If `true` a user can change the value.
     */
    val isEnabled: LiveData<Boolean>

    /**
     * Allows an input to be enabled.
     *
     * If an input is allowed to be enabled, it may become enabled if all other criteria are met.
     *
     * If an input is not allowed to be enabled, then regardless of the state of rules for the input it will always be disabled.
     *
     * @param allow `true` if an input is allowed to be enabled, `false` otherwise
     */
    fun allowToBeEnabled(allow: Boolean)
}

/**
 * Disables an input.
 */
fun InputEnabler.disableInput() = allowToBeEnabled(false)

class InputEnablerImpl(_isEnabled: LiveData<Boolean>) : InputEnabler {

    private val isAllowed = MutableLiveData(true)

    override val isEnabled: LiveData<Boolean> = _isEnabled.combine(isAllowed) { isEnabled, isAllowed ->
        isAllowed == true && isEnabled == true
    }

    override fun allowToBeEnabled(allow: Boolean) {
        isAllowed.value = allow
    }
}
