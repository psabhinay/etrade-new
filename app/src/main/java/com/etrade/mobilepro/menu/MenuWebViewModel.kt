package com.etrade.mobilepro.menu

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class MenuWebViewModel @AssistedInject constructor(
    tracker: Tracker,
    @Assisted title: String
) : ViewModel() {

    init {
        tracker.screen(title)
    }

    @AssistedFactory
    interface Factory {

        fun create(title: String): MenuWebViewModel
    }
}
