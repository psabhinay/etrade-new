package com.etrade.mobilepro.chartiq.presentation

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

interface ChartIqViewModelFactoryFactory {
    fun create(symbol: SearchResultItem.Symbol, mobileQuoteStateFlow: StateFlow<MobileQuote>?): ViewModelProvider.Factory
}

class ChartIqViewModelFactoryFactoryImpl @Inject constructor(
    private val factory: ChartIqViewModel.Factory
) : ChartIqViewModelFactoryFactory {
    override fun create(
        symbol: SearchResultItem.Symbol,
        mobileQuoteStateFlow: StateFlow<MobileQuote>?
    ): ViewModelProvider.Factory = viewModelFactory { factory.create(symbol, mobileQuoteStateFlow) }
}
