package com.etrade.mobilepro.searchingapi.items

import android.os.Bundle
import android.os.Parcelable
import androidx.annotation.StringRes
import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.SettlementSessionCode
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.getSymbolUnderlier
import com.etrade.mobilepro.instrument.safeUnderlyingSymbolInfo
import kotlinx.parcelize.Parcelize

const val EXTRA_QUOTE_TICKER = "quoteTicker"
const val EXTRA_WIDGET_TAG = "widgetTag"

sealed class SearchResultItem {
    @Parcelize
    data class Symbol(
        val title: String,
        val type: InstrumentType,
        val instrumentId: String = "",
        val description: String = "",
        val cachedAt: Long = 0,
        val queryValue: String = "",
        var expire: String = "",
        var strike: String = "",
        var settlementSessionCd: SettlementSessionCode = SettlementSessionCode.PM,
        var displaySymbol: String = "",
        val underlyerSymbol: String = getSymbolUnderlier(title, type) ?: "",
        val securityType: String = "",
        val underlyingTypeCode: InstrumentType = InstrumentType.EQ
    ) : SearchResultItem(), Parcelable, WithSymbolInfo {

        override val symbol: String
            get() = title
        override val instrumentType: InstrumentType
            get() = type
    }

    object EmptySearchResultItem : SearchResultItem()

    fun isEmpty(): Boolean {
        return this == emptySymbol
    }

    data class SectionTitle(
        val titleResId: Int
    ) : SearchResultItem()

    data class DisclosureText(
        @StringRes val textResource: Int
    ) : SearchResultItem()

    companion object {
        val emptySymbol = Symbol("", InstrumentType.UNKNOWN)
    }
}

fun SearchResultItem.Symbol.toBundle(): Bundle = Bundle().apply {
    putParcelable(EXTRA_QUOTE_TICKER, this@toBundle)
}

fun Instrument.toSearchResultItem(): SearchResultItem.Symbol = SearchResultItem.Symbol(
    title = symbol,
    type = instrumentType,
    underlyerSymbol = safeUnderlyingSymbolInfo.symbol,
    underlyingTypeCode = safeUnderlyingSymbolInfo.instrumentType
)

fun WithSymbolInfo.toSearchResultItem(): SearchResultItem.Symbol = SearchResultItem.Symbol(symbol, instrumentType)

fun SearchResultItem.Symbol.isEtfSymbol(): Boolean = securityType == "ETF" && type.isEquity
