package com.etrade.mobilepro.dropdown

import android.app.Dialog
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog

fun Dialog.setUpBottomSheetDialog() {
    setOnShowListener {
        val bottomSheetDialog = this as BottomSheetDialog
        bottomSheetDialog.findViewById<FrameLayout>(R.id.design_bottom_sheet)?.let { layout ->
            val bottomSheetBehavior = BottomSheetBehavior.from(layout)
            val layoutParams: CoordinatorLayout.LayoutParams = CoordinatorLayout.LayoutParams(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.MATCH_PARENT
            )
            layoutParams.behavior = bottomSheetBehavior
            layout.layoutParams = layoutParams
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }
}
