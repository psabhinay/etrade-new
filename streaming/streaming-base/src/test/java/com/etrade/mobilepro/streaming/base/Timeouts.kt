package com.etrade.mobilepro.streaming.base

internal const val INTERVAL = 10L
internal const val DELAY_TIME = 5 * INTERVAL
