package com.etrade.mobilepro.appwidget.widgetcontainer

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.etrade.mobilepro.appwidget.api.WidgetViewStateRepo
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettingsRepo
import com.etrade.mobilepro.appwidget.indices.getAddSymbolToWatchlistPendingIntent
import com.etrade.mobilepro.appwidget.indices.getLayoutInitializationIntent
import com.etrade.mobilepro.appwidget.indices.getWidgetIndicesRefreshIntent
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.safeLet
import dagger.android.AndroidInjection
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

interface OpenQuoteLookupDelegate {
    fun invoke(symbol: String, typeCode: String)
}

interface OpenWatchlistDelegate {
    fun invoke(watchlistId: String)
}

interface OpenAccountsScreenDelegate {
    fun invoke()
}

interface OpenWatchlistSelectionDelegate {
    fun invoke()
}

interface OpenWidgetSettingsDelegate {
    fun invoke(widgetId: Int)
}

interface AddSymbolToWidgetWatchlistDelegate {
    fun invoke()
}

internal const val ACTION_QUOTE_LOOKUP = "DefaultAppWidgetProvider.action.quote_lookup"
internal const val EXTRA_SYMBOL = "DefaultAppWidgetProvider.extra.symbol"
internal const val EXTRA_INSTRUMENT_TYPE_CODE = "DefaultAppWidgetProvider.extra.instrument_type"
internal const val ACTION_OPEN_WATCHLIST = "DefaultAppWidgetProvider.action.open_watchlist"
internal const val ACTION_OPEN_ACCOUNTS = "DefaultAppWidgetProvider.action.open_accounts"
internal const val EXTRA_WATCHLIST_ID = "DefaultAppWidgetProvider.extra.watchlist_id"
internal const val ACTION_OPEN_WIDGET_SETTINGS = "DefaultAppWidgetProvider.action.open_settings"
internal const val ACTION_ADD_SYMBOL_TO_WATCHLIST = "DefaultAppWidgetProvider.action.add_symbol_to_watchlist"

class DefaultAppWidgetProvider : AppWidgetProvider() {

    @Inject
    lateinit var viewStateRepo: WidgetViewStateRepo

    @Inject
    lateinit var openQuoteLookup: OpenQuoteLookupDelegate

    @Inject
    lateinit var openWatchlist: OpenWatchlistDelegate

    @Inject
    lateinit var openAccounts: OpenAccountsScreenDelegate

    @Inject
    lateinit var openWidgetSettings: OpenWidgetSettingsDelegate

    @Inject
    lateinit var addSymbolToWidgetWatchlistDelegate: AddSymbolToWidgetWatchlistDelegate

    @Inject
    lateinit var widgetSettingsRepo: WidgetSettingsRepo

    override fun onReceive(context: Context?, intent: Intent?) {
        AndroidInjection.inject(this, context)
        super.onReceive(context, intent)
        when (intent?.action) {
            ACTION_QUOTE_LOOKUP -> {
                val extraSymbol = intent.getStringExtra(EXTRA_SYMBOL)
                val extraTypeCode = intent.getStringExtra(EXTRA_INSTRUMENT_TYPE_CODE)
                safeLet(extraSymbol, extraTypeCode) { symbol, typeCode ->
                    openQuoteLookup.invoke(symbol, typeCode)
                }
            }
            ACTION_OPEN_WATCHLIST -> intent.getStringExtra(EXTRA_WATCHLIST_ID)?.let {
                openWatchlist.invoke(it)
            }
            ACTION_OPEN_WIDGET_SETTINGS -> openWidgetSettings.invoke(widgetId = intent.getIntExtra(ACTION_OPEN_WIDGET_SETTINGS, 0))
            ACTION_ADD_SYMBOL_TO_WATCHLIST -> addSymbolToWidgetWatchlistDelegate.invoke()
            ACTION_OPEN_ACCOUNTS -> openAccounts.invoke()
        }
    }

    override fun onEnabled(context: Context?) {
        super.onEnabled(context)
        runBlocking {
            viewStateRepo.setWidgetPlaced(true)
            val viewState = viewStateRepo.getIndicesViewState().copy(watchlistEntryTuples = null)
            viewStateRepo.setIndicesViewState(viewState)
            context?.let {
                scheduleWidgetRefresh(it, widgetSettingsRepo.loadSettings(viewState.widgetId))
            }
        }
    }

    override fun onDisabled(context: Context?) {
        super.onDisabled(context)
        runBlocking {
            widgetSettingsRepo.deleteSettings(viewStateRepo.getIndicesViewState().widgetId)
            viewStateRepo.setWidgetPlaced(false)
            viewStateRepo.setIndicesViewState(null)
        }
        context?.stopService(getWidgetIndicesRefreshIntent(context))
        context?.let(::cancelScheduledUpdates)
    }

    override fun onUpdate(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetIds: IntArray?
    ) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)
        if (context == null) { return }
        if (appWidgetManager == null) { return }
        if (appWidgetIds == null) { return }

        getLayoutInitializationIntent(context, appWidgetIds)
            .also { ContextCompat.startForegroundService(context, it) }
    }

    override fun onAppWidgetOptionsChanged(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetId: Int,
        newOptions: Bundle?
    ) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions)
        if (context == null) { return }
        if (appWidgetManager == null) { return }

        getLayoutInitializationIntent(context, intArrayOf(appWidgetId))
            .also { ContextCompat.startForegroundService(context, it) }
    }

    companion object {

        fun refresh(context: Context) {
            ContextCompat.startForegroundService(context, getWidgetIndicesRefreshIntent(context))
        }

        fun addSymbolToSelectedWatchlist(context: Context, symbol: SearchResultItem.Symbol) {
            ContextCompat.startForegroundService(context, getAddSymbolToWatchlistPendingIntent(context, symbol))
        }

        fun applyWidgetSettings(context: Context) {
            val widgetIds = AppWidgetManager.getInstance(context)
                .getAppWidgetIds(ComponentName(context, DefaultAppWidgetProvider::class.java))

            getLayoutInitializationIntent(context, widgetIds, forceRefresh = true)
                .also { ContextCompat.startForegroundService(context, it) }
        }
    }
}
