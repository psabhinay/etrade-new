package com.etrade.mobilepro.transfermoney.presentation

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.viewmodel.EtNavigationViewModel
import com.etrade.mobilepro.transfermoney.presentation.transfer.EXTERNAL_ACCOUNTS_URL
import com.etrade.mobilepro.transfermoney.presentation.transfer.SCREEN_TITLE_EXTERNAL_ACCOUNTS
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import javax.inject.Inject

@RequireLogin
class TransferMoneyPopupDialogFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    @Web private val baseUrl: String
) : DialogFragment() {

    private val args: TransferMoneyPopupDialogFragmentArgs by navArgs()
    private val etNavigationViewModel: EtNavigationViewModel by activityViewModels { viewModelFactory }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        if (args.errorCode == ERROR_CODE_VERIFY_EXTERNAL_ACCOUNT) {
            verifyExternalAccountDialog()
        } else {
            defaultMessageDialog()
        }

    private fun defaultMessageDialog(): Dialog =
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(args.message)
            .setPositiveButton(R.string.ok) { _, _ -> dismiss() }
            .create()

    private fun verifyExternalAccountDialog(): Dialog =
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(R.string.transfer_verify_external_account_message)
            .setTitle(R.string.transfer_verify_external_account_title)
            .setPositiveButton(R.string.transfer_verify) { _, _ ->
                etNavigationViewModel.navigate(
                    TransferMoneyPopupDialogFragmentDirections
                        .actionOpenExternalAccountsFragment(SCREEN_TITLE_EXTERNAL_ACCOUNTS, "$baseUrl$EXTERNAL_ACCOUNTS_URL")
                )
            }
            .setNegativeButton(R.string.transfer_not_now) { _, _ -> dismiss() }
            .create()
}

private const val ERROR_CODE_VERIFY_EXTERNAL_ACCOUNT = "34006"
