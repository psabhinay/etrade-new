package com.etrade.mobilepro.chartiq.presentation

import androidx.fragment.app.FragmentActivity
import com.etrade.mobilepro.order.details.api.OrderData
import com.etrade.mobilepro.searchingapi.items.SearchResultItem

interface ChartIqRouter {
    fun showLogin(activity: FragmentActivity?, symbol: SearchResultItem.Symbol)
    fun showEditOrderScreen(activity: FragmentActivity?, data: OrderData)
}
