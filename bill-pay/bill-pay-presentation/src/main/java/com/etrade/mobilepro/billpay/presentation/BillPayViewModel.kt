package com.etrade.mobilepro.billpay.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.BillPayStatus
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.SessionId
import com.etrade.mobilepro.billpay.presentation.addpayment.AddPaymentViewModel
import com.etrade.mobilepro.billpay.presentation.paymentlist.PaymentListViewModel
import com.etrade.mobilepro.billpay.usecase.BillPayInitialSetupUseCase
import com.etrade.mobilepro.billpay.usecase.PayeeListUseCase
import com.etrade.mobilepro.common.RestorableViewModelDelegate
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.tracking.SCREEN_TITLE_BILL_PAY
import com.etrade.mobilepro.tracking.SCREEN_TITLE_BILL_PAY_ACTIVITY
import com.etrade.mobilepro.tracking.SCREEN_TITLE_BILL_PAY_MANAGE
import com.etrade.mobilepro.tracking.TabsHostScreenViewModel
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelper
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(BillPayViewModel::class.java)

private val restorableViewModels = arrayOf(
    AddPaymentViewModel::class.java,
    PaymentListViewModel::class.java
)

class BillPayViewModel @Inject constructor(
    private val payeeListUseCase: PayeeListUseCase,
    private val billPayInitialSetupUseCase: BillPayInitialSetupUseCase,
    override val viewPagerTabsTrackerHelper: ViewPagerTabsTrackerHelper,
    private val restorableDelegate: RestorableViewModelDelegate
) : ViewModel(), TabsHostScreenViewModel {

    override val tabNames: Array<String> = arrayOf(SCREEN_TITLE_BILL_PAY, SCREEN_TITLE_BILL_PAY_ACTIVITY, SCREEN_TITLE_BILL_PAY_MANAGE)

    private val _viewState: MutableLiveData<ViewState> = MutableLiveData(ViewState())
    val viewState: LiveData<ViewState> get() = _viewState

    private val _payees: MutableLiveData<List<Payee>> = MutableLiveData(emptyList())
    val payees: LiveData<List<Payee>> get() = _payees

    private val _addedPayee: MutableLiveData<ConsumableLiveEvent<Payee>> = MutableLiveData()
    val addedPayee: LiveData<ConsumableLiveEvent<Payee>> get() = _addedPayee

    val sessionId: String? get() = viewState.value?.billPaySessionId

    init {
        loadBillPayDetails()
    }

    @Suppress("LongMethod")
    fun loadBillPayDetails() {
        _viewState.value = ViewState(isLoading = true)
        viewModelScope.launch {
            billPayInitialSetupUseCase.execute(Unit).fold(
                onSuccess = {
                    _payees.value = it.payees
                    _viewState.updateValue { prevState ->
                        prevState.copy(
                            isInitialSetup = false,
                            isLoading = false,
                            status = ConsumableLiveEvent(it.status),
                            allAccounts = it.allAccounts,
                            validAccounts = it.validAccounts,
                            billPaySessionId = it.sessionId,
                            error = null
                        )
                    }
                },
                onFailure = {
                    logger.error(it.localizedMessage)
                    _viewState.updateValue { prevState ->
                        prevState.copy(
                            isLoading = false,
                            error = Error(it) { loadBillPayDetails() }
                        )
                    }
                }
            )
        }
    }

    fun onPayeeAdded(addedPayee: Payee) {
        val sessionId = viewState.value?.billPaySessionId ?: throw IllegalStateException("BillPaySessionId is null")
        _viewState.updateValue { it.copy(isLoading = true) }
        viewModelScope.launch {
            payeeListUseCase.execute(SessionId(sessionId)).fold(
                onSuccess = {
                    onSuccessReloadPayees(it, addedPayee)
                },
                onFailure = {
                    onErrorReloadPayees(it, addedPayee)
                }
            )
        }
    }

    private fun onSuccessReloadPayees(
        payees: List<Payee>,
        addedPayee: Payee
    ): Boolean {
        return _viewState.updateValue { prevState ->
            val newPayeeInTheList = payees.firstOrNull { payee ->
                payee.name == addedPayee.name && payee.accountNumber == addedPayee.accountNumber &&
                    payee.nickName.toString() == addedPayee.nickName.toString()
            }
            _payees.value = payees
            if (newPayeeInTheList != null && _addedPayee.value?.peekContent() != newPayeeInTheList) {
                _addedPayee.value = ConsumableLiveEvent(newPayeeInTheList)
            }

            prevState.copy(isLoading = false, error = null)
        }
    }

    private fun onErrorReloadPayees(error: Throwable, addedPayee: Payee): Boolean {
        logger.error(error.localizedMessage)
        return _viewState.updateValue { prevState ->
            prevState.copy(
                isLoading = false,
                error = Error(error) { onPayeeAdded(addedPayee) }
            )
        }
    }

    fun refreshPayees(retry: RetryAction? = null, fromEdition: Boolean = false) {
        val sessionId = viewState.value?.billPaySessionId ?: throw IllegalStateException("BillPaySessionId is null")
        _viewState.updateValue { it.copy(isRefreshing = true) }
        viewModelScope.launch {
            payeeListUseCase.execute(SessionId(sessionId)).fold(
                onSuccess = {
                    _payees.postValue(it)
                    _viewState.updateValue { prevState ->
                        prevState.copy(isRefreshing = false, error = null, isEditableOnRefresh = fromEdition)
                    }
                },
                onFailure = {
                    logger.error(it.localizedMessage)
                    _viewState.updateValue { prevState ->
                        prevState.copy(
                            isRefreshing = false,
                            isEditableOnRefresh = fromEdition,
                            error = Error(it, retry)
                        )
                    }
                }
            )
        }
    }

    fun disposeState() {
        restorableViewModels.forEach {
            restorableDelegate.dispose(it)
        }
    }
}

data class ViewState(
    val isInitialSetup: Boolean = false,
    val isLoading: Boolean = false,
    val isRefreshing: Boolean = false,
    val isEditableOnRefresh: Boolean = false,
    val allAccounts: List<AccountDetails> = emptyList(),
    val validAccounts: List<AccountDetails> = emptyList(),
    val billPaySessionId: String? = null,
    val status: ConsumableLiveEvent<BillPayStatus>? = null,
    val error: Error? = null
)

data class Error(
    val error: Throwable? = null,
    val retry: RetryAction? = null
)

typealias RetryAction = () -> Unit
