package com.etrade.mobilepro.instrument

import org.threeten.bp.LocalDate

data class ExpirationDate(
    val year: Int,
    val month: Int,
    val day: Int,
    val settlementSessionCode: SettlementSessionCode
) : Comparable<ExpirationDate?> {

    override fun compareTo(other: ExpirationDate?): Int {
        return toLocalDate().compareTo(other?.toLocalDate())
    }
}

fun ExpirationDate.toLocalDate(): LocalDate = LocalDate.of(year, month, day)
