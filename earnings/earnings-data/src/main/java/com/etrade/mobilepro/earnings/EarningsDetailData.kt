package com.etrade.mobilepro.earnings

import java.math.BigDecimal

data class EarningsResponseResult(
    val earningsDetailData: List<EarningsDetailData>?
)
data class EarningsDetailData(
    val high: BigDecimal?,
    val low: BigDecimal?,
    val estMean: BigDecimal?,
    val quarter: String?,
    val expectedDate: String?,
    val announceDate: String?,
    val announceTime: String?,
    val actualValue: BigDecimal?,
    val surpriseMean: BigDecimal?,
    val fiscalPeriodYear: String?,
    val dateTime: BigDecimal?
)
