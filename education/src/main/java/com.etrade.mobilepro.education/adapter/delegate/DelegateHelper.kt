package com.etrade.mobilepro.education.adapter.delegate

import com.etrade.mobilepro.education.EducationItem

internal object DelegateHelper {
    fun isLastItem(position: Int, items: List<EducationItem>) = position == items.size
    fun isLevelChanged(items: List<EducationItem>, position: Int) = items[position - 1].level != items[position].level
    fun isFirstItem(position: Int) = position == 0
}
