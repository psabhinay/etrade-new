package com.etrade.mobilepro.msaccounts.presentation.aggregate

import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.di.CompleteView
import com.etrade.mobilepro.common.di.Default
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import com.etrade.msaccounts.dto.AggregateValuesDto
import com.etrade.msaccounts.dto.AggregateValuesViewDto
import javax.inject.Inject

class AggregateValuesViewFactory @Inject constructor(
    @CompleteView
    private val completeViewStreamingTextManager: StreamingTextManager,
    @Default
    private val defaultTextManager: StreamingTextManager,
    private val resources: Resources
) : DynamicUiViewFactory {
    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is AggregateValuesViewDto) {
            return AggregateValuesViewLayout(
                dto.data,
                completeViewStreamingTextManager,
                defaultTextManager,
                resources
            )
        } else {
            throw DynamicUiViewFactoryException("AggregateValuesViewFactory registered to unknown type ${dto.javaClass.canonicalName}")
        }
    }
}

class AggregateValuesViewLayout(
    var dto: AggregateValuesDto,
    private val completeViewStreamingTextManager: StreamingTextManager,
    private val defaultTextManager: StreamingTextManager,
    private val resources: Resources
) : GenericLayout {
    override val viewModelFactory: ViewModelProvider.Factory
        get() = viewModelFactory {
            AggregateValuesViewModel(
                dto,
                completeViewStreamingTextManager,
                defaultTextManager,
                resources
            )
        }
    override val viewModelClass: Class<out DynamicViewModel>
        get() = AggregateValuesViewModel::class.java

    override fun refresh(viewModel: DynamicViewModel) {
        if (viewModel is AggregateValuesViewModel) {
            viewModel.refresh(dto)
        }
    }
    override val uniqueIdentifier: String
        get() = javaClass.name
}
