package com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemCircuitBreakerBinding
import com.etrade.mobilepro.debugmenu.presentation.item.CircuitBreakerItem

internal class CircuitBreakerViewHolder(
    private val binding: DebugmenuItemCircuitBreakerBinding,
    listener: (sessionLimitEnabled: Boolean) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    private var item: CircuitBreakerItem? = null

    init {
        binding.circuitBreaker.setOnCheckedChangeListener { _, isChecked ->
            if (item?.enabled != isChecked) {
                listener.invoke(isChecked)
            }
        }
    }

    fun bindTo(item: CircuitBreakerItem) {
        this.item = item
        binding.circuitBreaker.isChecked = item.enabled
    }
}
