package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.CommonOrderConstants
import com.etrade.mobilepro.trade.data.createOptionTradeBody
import com.etrade.mobilepro.trade.data.form.input.DefaultOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.SecurityTypeValueId
import com.etrade.mobilepro.util.ValueCoder

internal class OptionSaveOrderRequestParametersProvider(
    private val tradeLegsCoder: ValueCoder<List<TradeLeg>>,
    private val securityTypeValueId: SecurityTypeValueId,
    private val isTablet: Boolean
) : SaveOrderRequestParametersProvider {

    override val path: String = "savepreparedorder"

    override fun createBody(params: InputValuesSnapshot): InputValuesSnapshot {
        val body = params.toMutableMap()
        body[CommonOrderConstants.PREPARED_ID] = body.remove(DefaultOptionTradeFormInputId.orderId)
        return body.createOptionTradeBody(tradeLegsCoder, securityTypeValueId, isTablet)
    }
}
