package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto
import com.etrade.mobilepro.orders.data.dto.AuditInfoDto
import com.etrade.mobilepro.orders.data.dto.HeaderDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OrderDetailsResponseDto(
    @Json(name = "viewOrderEvents")
    val viewOrderEvents: OrderDetailsDto?,
    @Json(name = "PreparedOrderDetails")
    val preparedOrderDetails: OrderDetailsDto?
) : BaseDataDto()

@JsonClass(generateAdapter = true)
data class OrderDetailsDto(
    @Json(name = "orderDetails")
    val orderDetails: OrderDetailsItemDto,
    @Json(name = "events")
    val events: List<EventDto>,
    @Json(name = "preparedOrderId")
    val preparedOrderId: String?,
    @Json(name = "preparedStatus")
    val preparedStatus: String?
)

@JsonClass(generateAdapter = true)
data class EventDto(
    @Json(name = "header")
    val header: EventHeaderDto,
    @Json(name = "eventLegDetails")
    val eventLegDetails: List<EventLegDetailDto>
)

@JsonClass(generateAdapter = true)
data class EventHeaderDto(
    @Json(name = "eventType")
    val eventType: String,
    @Json(name = "marketSession")
    val marketSession: String,
    @Json(name = "price")
    val price: String,
    @Json(name = "eventTimeEST")
    val eventTimeEST: Long,
    @Json(name = "groupOrderNo")
    val groupOrderNo: String,
    @Json(name = "eventDesc")
    val eventDesc: String
)

@JsonClass(generateAdapter = true)
data class EventLegDetailDto(
    @Json(name = "averageFillPrice") val averageFillPrice: String,
    @Json(name = "commissionAmount") val commissionAmount: String,
    @Json(name = "filledQuantity") val filledQuantity: String,
    @Json(name = "optStrikePrice") val optStrikePrice: String,
    @Json(name = "orderAction") val orderAction: String,
    @Json(name = "otherFee") val otherFee: String,
    @Json(name = "secFee") val secFee: String,
    @Json(name = "iofFee") val iofFee: String,
    @Json(name = "otherFeeCode") val otherFeeCode: String,
    @Json(name = "positionEffect") val positionEffect: String,
    @Json(name = "qtyReserveShow") val qtyReserveShow: String,
    @Json(name = "quantityType") val quantityType: String,
    @Json(name = "quantityValue") val quantityValue: String,
    @Json(name = "typeCode") val typeCode: String,
    @Json(name = "displaySymbol") val displaySymbol: String
)

@JsonClass(generateAdapter = true)
data class OrderDetailsItemDto(
    @Json(name = "groupOrderCount")
    val groupOrderCount: String,
    @Json(name = "accountId")
    val accountId: String,
    @Json(name = "orders")
    val orders: List<OrdersDto>,
    @Json(name = "orderNumber")
    val orderNumber: String
)

@JsonClass(generateAdapter = true)
data class OrdersDto(
    @Json(name = "advancedOrder")
    val advancedOrder: AdvancedOrderDto,
    @Json(name = "auditInfo")
    val auditInfo: AuditInfoDto,
    @Json(name = "header")
    val header: HeaderDto,
    @Json(name = "legDetails")
    val legDetails: List<LegDetailsDto>,
    @Json(name = "egOverride")
    val egOverride: String,
    @Json(name = "egQual")
    val egQual: String
)

@JsonClass(generateAdapter = true)
data class LegDetailsDto(
    @Json(name = "averageFillPrice") val averageFillPrice: String,
    @Json(name = "commissionAmount") val commissionAmount: String,
    @Json(name = "displaySymbol") val displaySymbol: String,
    @Json(name = "filledQuantity") val filledQuantity: String,
    @Json(name = "instrumentName") val instrumentName: String,
    @Json(name = "legNumber") val legNumber: String,
    @Json(name = "optCallPut") val optCallPut: String,
    @Json(name = "optExpireDate") val optExpireDate: String,
    @Json(name = "optExpireYearMonth") val optExpireYearMonth: String,
    @Json(name = "optStrikePrice") val optStrikePrice: String,
    @Json(name = "orderAction") val orderAction: String,
    @Json(name = "otherFee") val otherFee: String,
    @Json(name = "secFee") val secFee: String,
    @Json(name = "iofFee") val iofFee: String,
    @Json(name = "otherFeeCode") val otherFeeCode: String,
    @Json(name = "positionEffect") val positionEffect: String,
    @Json(name = "qtyReserveShow") val qtyReserveShow: String,
    @Json(name = "quantityType") val quantityType: String,
    @Json(name = "quantityValue") val quantityValue: String,
    @Json(name = "typeCode") val typeCode: String,
    @Json(name = "underlyingProductId") val underlyingProductId: String,
    @Json(name = "authMs") val authMs: String,
    @Json(name = "shortSaleCd") val shortSaleCd: String,
    @Json(name = "authMSEST") val authMSEST: String,
    @Json(name = "shortSaleAuthID") val shortSaleAuthID: String,
    @Json(name = "authStatusCd") val authStatusCd: String,
    @Json(name = "originalSymbol") val originalSymbol: String
)
