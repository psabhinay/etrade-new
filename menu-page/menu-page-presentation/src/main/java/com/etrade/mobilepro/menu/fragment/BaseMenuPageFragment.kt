package com.etrade.mobilepro.menu.fragment

import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.toDynamicViewModels
import com.etrade.mobilepro.menu.DividerMenuItem
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.MessagesMenuItem
import com.etrade.mobilepro.menu.TextMenuItem
import com.etrade.mobilepro.menu.fragment.recyclerview.MenuListDelegationAdapter
import com.etrade.mobilepro.menu.viewmodel.MenuPageViewModel
import com.etrade.mobilepro.menupage.presentation.R
import com.etrade.mobilepro.menupage.presentation.databinding.FragmentMenuBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.recyclerviewutil.CustomRecyclerViewItemDivider
import com.etrade.mobilepro.util.collection.indicesOfElementsWithType
import org.slf4j.Logger
import org.slf4j.LoggerFactory

private const val taxLotsDeepLink = "etrade://show/TaxDocs"

abstract class BaseMenuPageFragment : Fragment(R.layout.fragment_menu) {

    protected val binding by viewBinding(FragmentMenuBinding::bind) { onDestroyBinding() }

    protected abstract val menuPageViewModel: MenuPageViewModel

    protected val logger: Logger by lazy { LoggerFactory.getLogger(this.javaClass) }
    private lateinit var menuItemAdapter: MenuListDelegationAdapter
    private var itemDecorator: RecyclerView.ItemDecoration? = null
    private val separator: Drawable? by lazy {
        ContextCompat.getDrawable(requireContext(), R.drawable.list_item_divider_light_grey)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        menuItemAdapter = MenuListDelegationAdapter(
            this::onMenuItemClick,
            viewLifecycleOwner
        ) {
            it.toDynamicViewModels(this) { model ->
                model.clickEvents.observe(viewLifecycleOwner) { event ->
                    when (event) {
                        is CtaEvent.UriEvent -> navigate(event.uri)
                        is CtaEvent.ActionEvent -> navigateAndSaveDirections(Uri.parse(event.url), false)
                        is CtaEvent.NavDirectionsNavigation -> navigate(event.ctaDirections)
                    }
                }
            }
        }

        with(binding.menuItemsRv) {
            layoutManager = LinearLayoutManager(context)
            adapter = menuItemAdapter
        }
    }

    private fun FragmentMenuBinding.onDestroyBinding() {
        menuItemsRv.adapter = null
    }

    protected open fun onItemsUpdated(items: List<MenuItem>) = Unit

    protected fun updateMenuItems(items: List<MenuItem>) {
        menuItemAdapter.updateItems(items)
        updateItemDecorator(excludedPositions = geItemPositionsWithoutDecorators(items))
        onItemsUpdated(items)
    }

    protected abstract fun navigate(directions: NavDirections)

    protected abstract fun navigate(appAction: Uri)

    protected abstract fun navigate(action: () -> Unit)

    protected abstract fun saveMenuDirection(directions: NavDirections?)

    protected abstract fun saveMenuDirection(uri: Uri)

    protected abstract fun saveMenuDirection(action: () -> Unit)

    protected open fun onMenuItemClick(menuItem: TextMenuItem) {
        menuItem.menuActions.forEach { menuAction ->
            when (menuAction) {
                is TextMenuItem.MenuAction.MenuActivityAction -> activity?.let { menuAction.action(it) }
                is TextMenuItem.MenuAction.MenuGenericAction -> menuAction.action()
                is TextMenuItem.MenuAction.MenuNavDirectionsAction -> navigateAndSaveDirections(menuAction.navDirections, menuItem.shouldSaveDirections)
                is TextMenuItem.MenuAction.MenuAppAction -> navigateAndSaveDirections(menuAction.uri, menuItem.shouldSaveDirections)
            }
        }
    }

    private fun navigateAndSaveDirections(directions: NavDirections, shouldSaveDirection: Boolean) {
        if (shouldSaveDirection) {
            saveMenuDirection(directions)
        }
        navigate(directions)
    }

    private fun navigateAndSaveDirections(uri: Uri, shouldSaveDirection: Boolean) {
        if (shouldSaveDirection && uri.toString() != taxLotsDeepLink) {
            saveMenuDirection(uri)
        }
        navigate(uri)
    }

    private fun navigateAndSaveDirections(action: () -> Unit, shouldSaveDirection: Boolean) {
        if (shouldSaveDirection) {
            saveMenuDirection(action)
        }
        navigate(action)
    }

    private fun updateItemDecorator(excludedPositions: List<Int>) {
        separator?.let { separator ->
            itemDecorator?.let { binding.menuItemsRv.removeItemDecoration(it) }
            itemDecorator = CustomRecyclerViewItemDivider(
                drawable = separator,
                positions = excludedPositions,
                countOfLastItemsWithoutDivider = 0
            ).also {
                binding.menuItemsRv.addItemDecoration(it)
            }
        }
    }

    private fun geItemPositionsWithoutDecorators(items: List<MenuItem>): List<Int> {
        val dividerIndices = items.indicesOfElementsWithType<DividerMenuItem, MenuItem>()
        val firstInboxMessageIndex = items.indexOfFirst { it is MessagesMenuItem }
        return dividerIndices + firstInboxMessageIndex
    }
}
