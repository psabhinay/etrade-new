package com.etrade.mobilepro.util.collection

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class RemoveAndReturnFirstIfPredicateTest {

    @Test
    fun `remove and return first even number`() {
        val items = mutableListOf(1, 4, 5, 12, 9, 10, 20)
        val removedItem = items.removeAndReturnFirstIf { it % 2 == 0 }
        assertEquals(4, removedItem)
        assertTrue(items == listOf(1, 5, 12, 9, 10, 20))
    }

    @Test
    fun `return null if item not found`() {
        val items = mutableListOf("aaa", "aa", "bbb")
        val removedItem = items.removeAndReturnFirstIf { it == "ccc" }
        assertNull(removedItem)
        assertEquals(3, items.size)
    }
}
