package com.etrade.mobilepro.tableviewutils.presentation.holder

import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell

interface HasSymbolCell {
    var cell: SymbolCell?
}
