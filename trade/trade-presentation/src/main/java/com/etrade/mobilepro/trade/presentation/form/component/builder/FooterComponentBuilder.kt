package com.etrade.mobilepro.trade.presentation.form.component.builder

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.dynamic.form.api.Component
import com.etrade.mobilepro.dynamic.form.api.ComponentBuilder
import com.etrade.mobilepro.dynamic.form.api.FormDsl
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.InputHolder
import com.etrade.mobilepro.trade.presentation.form.component.FooterComponent
import com.etrade.mobilepro.trade.presentation.form.component.FooterLink
import java.util.LinkedList

internal fun GroupBuilder.footer(init: FooterComponentBuilder.() -> Unit) {
    custom {
        FooterComponentBuilder(this).apply(init).create()
    }
}

@FormDsl
internal class FooterComponentBuilder(
    inputHolder: InputHolder
) : ComponentBuilder(inputHolder) {

    var links: LiveData<List<FooterLink>> = MutableLiveData()

    fun links(init: FooterLinkBuilder.() -> Unit) {
        this.links = MutableLiveData(FooterLinkBuilder(this).apply(init).create())
    }

    override fun create(): Component {
        return FooterComponent(links, isVisible)
    }
}

@FormDsl
internal class FooterLinkBuilder(private val inputHolder: InputHolder) {

    private var links: MutableList<FooterLink> = LinkedList()

    fun link(create: InputHolder.() -> FooterLink) {
        links.add(inputHolder.create())
    }

    fun create(): List<FooterLink> = links.toList()
}
