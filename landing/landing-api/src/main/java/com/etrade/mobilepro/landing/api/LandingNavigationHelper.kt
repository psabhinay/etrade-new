package com.etrade.mobilepro.landing.api

import android.content.Intent
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.navigation.NavDirections

object LandingNavigationHelper {
    private const val SHOULD_HANDLE_LANDING_PAGE_KEY = "should handle landing page key"
    fun createShouldHandleLandingBundle(): Bundle = Bundle().apply { putBoolean(SHOULD_HANDLE_LANDING_PAGE_KEY, true) }

    fun checkIfShouldHandleLanding(bundle: Bundle?): Boolean = bundle?.getBoolean(SHOULD_HANDLE_LANDING_PAGE_KEY, false) ?: false
    fun checkIfShouldHandleLanding(intent: Intent?): Boolean = intent?.getBooleanExtra(SHOULD_HANDLE_LANDING_PAGE_KEY, false) ?: false

    class Directions(@IdRes val id: Int, shouldHandleLanding: Boolean) : NavDirections {
        private val arguments = if (shouldHandleLanding) createShouldHandleLandingBundle() else Bundle()

        override fun getActionId() = id

        override fun getArguments(): Bundle {
            return arguments
        }
    }
}
