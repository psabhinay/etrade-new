package com.etrade.mobilepro.chart.cache

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.chart.util.resolveEndpointType
import com.etrade.mobilepro.chartengine.ChartRequest
import com.etrade.mobilepro.chartengine.SymbolChartData
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.util.AuthenticationStatusProvider
import io.reactivex.Single
import io.realm.Realm
import retrofit2.Retrofit
import javax.inject.Inject

class CachedChartDataSourceDelegate @Inject constructor(
    private val authStatusProvider: AuthenticationStatusProvider,
    @MobileTrade retrofit: Retrofit
) : BaseCachedChartDataSourceDelegate(authStatusProvider, retrofit), SimpleCachedResource.DataSourceDelegate<SymbolChartData, ChartRequest> {

    override fun fetchFromNetwork(id: ChartRequest): Single<SymbolChartData> {
        val requestString = id.getChartRequestString()
        val endpointType = resolveEndpointType(authStatusProvider.isAuthenticated()).typeCode
        return apiClient.getChart(endpointType, requestString).map { response ->
            response.mapToSymbolChartData(id)
        }
    }

    override fun fetchFromDb(id: ChartRequest): Single<SymbolChartData> {
        return Single.fromCallable {
            Realm.getInstance(realmConfig).use { realm ->
                fetchSymbolChartDataFromDb(id, realm)
            }
        }
    }

    override fun putToCache(fetchedResult: SymbolChartData): Single<SymbolChartData> {
        return Single.fromCallable {
            Realm.getInstance(realmConfig).use { autoClosableRealm ->
                autoClosableRealm.executeTransaction { realm ->
                    val chartDataToSave = updateSymbolChartDataToDb(fetchedResult, realm)
                    realm.insertOrUpdate(chartDataToSave)
                }
            }
            fetchedResult
        }
    }
}
