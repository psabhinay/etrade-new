package com.etrade.mobilepro.transactions

interface Transaction {
    val type: String
    val description: String
    val amount: String?
    val formattedAmount: String
    val amountContentDescription: String?
    val date: String

    val amountColorResource: Int
        get() = amount?.toBigDecimalOrNull()?.let {
            if (it.signum() < 0) {
                R.color.red
            } else {
                R.color.green
            }
        } ?: R.color.textColorSecondary

    companion object {

        private const val AMOUNT_SYMBOL_TO_BE_REPLACED = "-"
        private const val AMOUNT_SYMBOL_REPLACE_WITH = " minus "
        private val digitRegex = Regex(""".*\d.*""")

        @Suppress("LongParameterList")
        fun create(type: String, description: String, amount: String?, formattedAmount: String, emptyAmountDescription: String, date: String): Transaction {
            return object : Transaction {
                override val type: String = type
                override val description: String = description
                override val amount: String? = amount
                override val formattedAmount: String = formattedAmount
                override val amountContentDescription: String? = if (containsDigit(amount)) {
                    formattedAmount.replace(AMOUNT_SYMBOL_TO_BE_REPLACED, AMOUNT_SYMBOL_REPLACE_WITH)
                } else {
                    emptyAmountDescription
                }
                override val date: String = date
            }
        }

        private fun containsDigit(amount: String?) = amount?.matches(digitRegex) == true
    }
}
