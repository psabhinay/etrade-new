package com.etrade.mobilepro.watchlist.presentation.compose

import android.annotation.SuppressLint
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.pointer.pointerInput
import com.evrencoskun.tableview.TableView
import kotlinx.coroutines.flow.StateFlow

/**
 * Conditionally applies brightHorizontalGestures.
 * This solution has minor UX flaw mentioned below and probably is worth revisiting.
 *
 * This modifier recomposes in three cases:
 * 1. when @param key: Any? changes
 * 2. when scrollFinished event happens
 * 3. when scrollKey changes (actual value is unimportant)
 *
 * The scrollKey variable changes when we twice failed to scroll in the desired direction (the couldScrollLastTime var is used for this).
 * It means that it is impossible to scroll the tableview and the tab in one uninterrupted gesture.
 * It is a tradeoff required to avoid situation when it is impossible to scroll tab again when we just scrolled it backwards (e.g. news (tab2)->overview(tab1))
 *
 * The scrollCount variable is used to ensure the bridging is applied in case we just scrolled the tab in the opposite direction(backwards)
 * Though we can not scroll to the desired direction in this case we still want to be able to scroll the tableview backwards.
 */
@SuppressLint("UnnecessaryComposedModifier")
fun Modifier.conditionalGestureBridge(key: Any?, scrollsFinishedSignal: StateFlow<Int>, tv: TableView?, direction: Int = 1): Modifier = composed {
    var curOffset by remember { mutableStateOf(Offset.Zero) }
    var scrollKey by remember { mutableStateOf(true) }
    val scrollsFinished by scrollsFinishedSignal.collectAsState()
    var scrollCount by remember { mutableStateOf(scrollsFinished - 1) }
    var couldScrollLastTime by remember { mutableStateOf(true) }

    pointerInput(key1 = key, key2 = "$scrollKey$scrollsFinished") {
        if (tv?.columnHeaderRowCanScrollHorizontally(direction) == true || scrollCount != scrollsFinished) {
            scrollCount = scrollsFinished
            bridgeHorizontalGestures(
                onNewEvent = { it, dir ->
                    tv?.dispatchMotionEventToColumnHeaderRow(it)
                    if (dir == direction) {
                        val canScrollNow = tv?.columnHeaderRowCanScrollHorizontally(dir) ?: false
                        if (!canScrollNow && !couldScrollLastTime) {
                            scrollKey = !scrollKey
                        }
                        couldScrollLastTime = canScrollNow
                    }
                },
                currentOffset = { curOffset },
                onCurrentOffsetUpdate = { curOffset = it }
            )
        }
    }
}
