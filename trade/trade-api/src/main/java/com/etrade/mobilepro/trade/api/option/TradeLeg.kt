package com.etrade.mobilepro.trade.api.option

import com.etrade.mobilepro.instrument.isSymbolLongOption
import com.etrade.mobilepro.orders.api.TransactionType

interface TradeLeg {
    val symbol: String
    val transactionType: TransactionType
    val quantity: Int
    val displaySymbol: String
    val isAMOption: Boolean
}

val TradeLeg.isOption: Boolean
    get() = isSymbolLongOption(symbol)
