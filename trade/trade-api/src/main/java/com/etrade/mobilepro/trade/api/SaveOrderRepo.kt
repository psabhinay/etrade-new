package com.etrade.mobilepro.trade.api

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot

interface SaveOrderRepo {
    suspend fun saveOrder(map: InputValuesSnapshot): ETResult<SaveOrderResponse>
}
