package com.etrade.mobilepro.streaming

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class StreamingSubscriptionControllerImpl @Inject constructor(
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>
) : StreamingSubscriptionController {

    private val disposables = mutableMapOf<Symbol, Disposable>()
    private val compositeDisposable = CompositeDisposable()

    private var symbols: Set<Symbol> = emptySet()
    private var fieldsProvider: (InstrumentType) -> Set<Level1Field> = { emptySet() }

    private var initialSymbolSource: Observable<Set<Symbol>>? = null
    private var symbolsSource: Observable<Set<Symbol>>? = null
    private var onSymbolUpdateCallback: ((symbol: String, instrumentType: InstrumentType, data: Level1Data) -> Unit)? = null
    private var isSubscriptionPaused = true

    private var isInitialized = false

    override fun initSubscription(
        fieldsProvider: (InstrumentType) -> Set<Level1Field>,
        symbolsSource: Observable<Set<Symbol>>,
        onUpdateCallback: (symbol: String, instrumentType: InstrumentType, data: Level1Data) -> Unit
    ) {
        this.fieldsProvider = fieldsProvider
        this.onSymbolUpdateCallback = onUpdateCallback
        this.initialSymbolSource = symbolsSource

        isInitialized = true
    }

    override fun resumeSubscription() {
        if (!isInitialized) return
        if (isSubscriptionPaused) {
            this.symbolsSource = initialSymbolSource?.also {
                it.subscribeBy { updatedSymbols ->
                    if (isSubscriptionPaused) {
                        this.symbols = updatedSymbols
                    } else {
                        updateSubscription(updatedSymbols)
                    }
                }.addTo(compositeDisposable)
            }
            symbols.forEach(::subscribeToSymbol)
            isSubscriptionPaused = false
        }
    }

    override fun pauseSubscription() {
        isSubscriptionPaused = true
        unsubscribeFromAll()
    }

    override fun cancelSubscription() {
        symbols = emptySet()
        symbolsSource = null
        onSymbolUpdateCallback = null
        isInitialized = false
        compositeDisposable.clear()
        pauseSubscription()
    }

    private fun updateSubscription(updatedSymbols: Set<Symbol>) {
        val toSubscribeSymbols = updatedSymbols - symbols
        val toUnsubscribeSymbols = symbols - updatedSymbols
        symbols = updatedSymbols
        toSubscribeSymbols.forEach(::subscribeToSymbol)
        toUnsubscribeSymbols.forEach(::unsubscribeFromSymbol)
    }

    private fun subscribeToSymbol(stockSymbol: Symbol) {
        val symbol = stockSymbol.symbol
        val type = stockSymbol.instrumentType
        streamingQuoteProvider.getQuote(symbol, type, fieldsProvider(type)).subscribeBy { data ->
            onSymbolUpdateCallback?.invoke(symbol, type, data)
        }.also {
            disposables[stockSymbol] = it
        }
    }

    private fun unsubscribeFromSymbol(stockSymbol: WithSymbolInfo) {
        val disposable = disposables.remove(stockSymbol) ?: return
        if (!disposable.isDisposed) {
            disposable.dispose()
        }
    }

    private fun unsubscribeFromAll() {
        val iterator = disposables.iterator()
        while (iterator.hasNext()) {
            val disposable = iterator.next().value

            if (!disposable.isDisposed) {
                disposable.dispose()
            }

            iterator.remove()
        }
    }
}
