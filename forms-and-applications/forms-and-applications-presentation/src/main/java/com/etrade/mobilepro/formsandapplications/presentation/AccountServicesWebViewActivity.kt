package com.etrade.mobilepro.formsandapplications.presentation

import android.content.Context
import android.content.Intent
import androidx.annotation.StringRes
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import org.chromium.customtabsclient.shared.WebviewActivity
import org.chromium.customtabsclient.shared.createWebViewActivityIntent

class AccountServicesWebViewActivity : WebviewActivity() {

    override fun instantiateFragment() {
        fragmentInjectionFactory.instantiateFragment(AccountServicesWebViewFragment::class.java, createFragmentArgs()).apply {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, this)
                .commit()
        }
    }

    companion object {

        /**
         * Creates an intent to start [AccountServicesWebViewActivity].
         *
         * @param context application context
         * @param url an url to open
         * @param titleResId resource ID of a titleResId to display
         *
         * @return the intent to start the activity
         */
        fun intent(context: Context, url: String, @StringRes titleResId: Int = 0): Intent {
            return context.createWebViewActivityIntent(
                activityClass = AccountServicesWebViewActivity::class.java,
                url = url,
                titleResId = titleResId
            )
        }
    }
}
