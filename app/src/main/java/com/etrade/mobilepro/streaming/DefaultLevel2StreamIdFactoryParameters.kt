package com.etrade.mobilepro.streaming

import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2StreamIdFactory
import javax.inject.Inject

class DefaultLevel2StreamIdFactoryParameters @Inject constructor() : Level2StreamIdFactory.Parameters {
    @Suppress("MagicNumber")
    override val bookCount: Int = 5
}
