package com.etrade.mobilepro.checkdeposit.presentation.details

sealed class DepositDetail {

    data class LabelValue(val label: String, val value: String, val contentDescription: String? = null) : DepositDetail()

    data class Image(val url: String, val isFront: Boolean) : DepositDetail()

    data class FooterMessage(val text: String) : DepositDetail()

    object Divider : DepositDetail()
}
