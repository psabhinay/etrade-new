package com.etrade.mobilepro.home.overview.viewfactory

typealias FullTimestampFormatterType = (@JvmSuppressWildcards Long) -> String
