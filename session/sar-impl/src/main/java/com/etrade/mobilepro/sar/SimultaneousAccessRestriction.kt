package com.etrade.mobilepro.sar

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.session.api.SessionEvents

interface SimultaneousAccessRestriction {

    /**
     * sar response events based on which session values are cleared and notified to user.
     */
    val sessionEvent: LiveData<SessionEvents>

    /**
     * Start periodic SAR Check mechanism.
     */
    fun start()

    /**
     * Stop SAR periodic SAR Check mechanism.
     */
    fun stop()
}
