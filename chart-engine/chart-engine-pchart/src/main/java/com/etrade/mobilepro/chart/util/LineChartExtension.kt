package com.etrade.mobilepro.chart.util

import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.chart.model.ChartLineData
import com.etrade.mobilepro.chart.model.ChartLineEntry
import com.etrade.mobilepro.chart.model.ColorResolverImpl
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet

private const val ANIMATION_DURATION_MS = 1000

fun LineChart.updateLineData(lineData: ChartLineData?) {
    if (lineData != null) {
        val colorResolver = ColorResolverImpl(context)
        val chartData = lineData.getLineData(colorResolver)
        configureChart(this, lineData.getConfiguration(), chartData)
        val shouldAnimate = !chartData.firstLineTheSame(data)

        data = chartData

        if (shouldAnimate) {
            animateX(ANIMATION_DURATION_MS)
        }
    } else {
        // We need to clear data when network fails and no cached data we want to show no data available
        // otherwise it shows last selected chart
        data?.clearValues()
        clear()
    }
    notifyDataSetChangedAndInvalidateChart()
}

private fun LineData.firstLineTheSame(other: LineData?): Boolean {
    for (i in this.firstLine.indices) {
        if (!firstLine[i].equalTo(other?.firstLine?.getOrNull(i))) {
            return false
        }
    }
    return true
}

@BindingAdapter("lineData")
fun LineChart.setLineData(lineData: ChartLineData?) {
    updateLineData(lineData)
}

private val LineData.firstLine: List<Entry>
    get() = (dataSets?.firstOrNull() as? LineDataSet)?.values.orEmpty()

fun LineChart.updateLineDataSetWithEntry(newChartLineEntry: ChartLineEntry) {
    data?.getDataSetByLabel(newChartLineEntry.lineDataSetLabel, false)?.let { lineDateSet ->
        if (lineDateSet.entryCount > 0) {
            val currentLastEntry = lineDateSet.getEntryForIndex(lineDateSet.entryCount - 1)
            val isChartUpdated = currentLastEntry?.let {
                updateChartWithLastEntry(it, newChartLineEntry, lineDateSet)
            } ?: false

            if (isChartUpdated) {
                updateYAxisMinAndMax(newChartLineEntry.entryY)
                notifyDataSetChangedAndInvalidateChart()
            }
        }
    }
}

private fun updateChartWithLastEntry(
    currentLastEntry: Entry,
    newChartLineEntry: ChartLineEntry,
    lineDateSet: ILineDataSet
): Boolean {
    return when {
        currentLastEntry.x == newChartLineEntry.entryX && currentLastEntry.y != newChartLineEntry.entryY -> {
            currentLastEntry.y = newChartLineEntry.entryY
            true
        }
        newChartLineEntry.entryX > currentLastEntry.x -> {
            addNewEntryToChart(lineDateSet, newChartLineEntry)
        }
        else -> {
            false
        }
    }
}

private fun addNewEntryToChart(
    lineDateSet: ILineDataSet,
    newChartLineEntry: ChartLineEntry
): Boolean {
    lineDateSet.addEntry(Entry(newChartLineEntry.entryX, newChartLineEntry.entryY))
    return true
}

private fun LineChart.updateYAxisMinAndMax(y: Float) {
    if (axisRight.axisMaximum < y) {
        axisRight.axisMaximum = y
    } else if (axisRight.axisMinimum > y) {
        axisRight.axisMinimum = y
    }
}

private fun LineChart.notifyDataSetChangedAndInvalidateChart() {
    notifyDataSetChanged()
    invalidate()
}
