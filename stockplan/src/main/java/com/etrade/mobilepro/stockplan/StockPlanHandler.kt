package com.etrade.mobilepro.stockplan

import androidx.annotation.WorkerThread
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument

/**
 * [StockPlanHandler] handles the javascript hand off's that are hooked into in
 * [StockPlanJSInterface].
 * Note, this class is annotated with the @WorkerThread annotation because the callbacks
 * arriving from the JS side are happening on a worker thread, and as such need to be handled
 * as such. I.e. if the responses need to be handled directly on the UI thread, then your
 * view class should either have a runOnUiThread block or use an Observer that is observing
 * on the UI thread for these callbacks.
 */
@WorkerThread
interface StockPlanHandler {
    fun tradeButtonClicked(jsonString: String)
    fun updatePageTitle(pageTitle: String)
    fun openNativeQuote(symbol: String)
    fun openPopup(pageTitle: String, textResId: Int)
    fun openURL(url: String, isPdf: Boolean)
    fun navigateToAccount(accountId: String)
    /**
     * @param date : The time in milliseconds
     * @param eventName : What the calendar event is
     */
    fun openNativeCalendar(date: Long, eventName: String)
    fun openOrderDetails(argument: OrderDetailsArgument)
}
