package com.etrade.mobilepro.chat.data.messages.dto

import androidx.annotation.Keep

@Keep
data class ChatGetMessagesRequestDto(val value: ChatDefaultRequestDto)

@Keep
data class ChatPostMessageRequestDto(val value: ChatDefaultRequestDto)

@Keep
data class ChatTerminateRequestDto(val value: ChatDefaultRequestDto)

@Keep
data class ChatDefaultRequestDto(
    val sessionID: String,
    val message: String? = null,
    val metadata: ChatMetadataDto?
)
