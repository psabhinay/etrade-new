package com.etrade.mobilepro.screener.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.presentation.adapter.viewholder.ScreenerSetViewHolder
import com.etrade.mobilepro.screener.presentation.databinding.ItemScreenerSetBinding
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class ScreenerSetAdapterDelegate(
    private val screenerSetSelectedListener: (ScreenerItem.ScreenerSet) -> Unit,
    private val screenerSetClearListener: (ScreenerItem.ScreenerSet) -> Unit
) : AbsListItemAdapterDelegate<ScreenerItem.ScreenerSet, ScreenerItem, ScreenerSetViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): ScreenerSetViewHolder =
        ScreenerSetViewHolder(parent.viewBinding(ItemScreenerSetBinding::inflate), screenerSetSelectedListener, screenerSetClearListener)

    override fun isForViewType(item: ScreenerItem, items: MutableList<ScreenerItem>, position: Int): Boolean =
        item is ScreenerItem.ScreenerSet

    override fun onBindViewHolder(item: ScreenerItem.ScreenerSet, holder: ScreenerSetViewHolder, payloads: MutableList<Any>) =
        holder.bindTo(item)
}
