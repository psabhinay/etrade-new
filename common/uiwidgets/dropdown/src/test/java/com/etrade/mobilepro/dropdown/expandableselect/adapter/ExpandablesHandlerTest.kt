package com.etrade.mobilepro.dropdown.expandableselect.adapter

import com.etrade.mobilepro.dropdown.expandableselect.DropDownExpandableSelector
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ExpandablesHandlerTest {

    @Test
    fun testAutoExpandSuccess() {
        val output = mutableListOf<DropDownExpandableSelector.Selectable<String>>()
        val consumer = object : UpdatesConsumer<String> {
            override fun dispatchUpdates(renderedItems: List<DropDownExpandableSelector.Selectable<String>>) {
                output.clear()
                output.addAll(renderedItems)
            }
        }
        ExpandablesHandler(baseItems = TestData.listWithNestedSelected, consumer = consumer, lastExpandedItem = null) { _, _, _ -> }

        assertEquals(9, output.size)
        assertEquals("l0_0, l0_1, l1_0, l1_1, l2_0, l2_1, l2_2, l1_2, l0_2", output.joinToString { it.value })
        output.forEachIndexed { index, selectable ->
            if (index != 1 && index != 3) {
                assertEquals(false, selectable.isExpanded)
            } else {
                assertEquals(true, selectable.isExpanded)
            }
        }
    }

    @Test
    fun testAutoExpandNoSelected() {
        val output = mutableListOf<DropDownExpandableSelector.Selectable<String>>()
        val consumer = object : UpdatesConsumer<String> {
            override fun dispatchUpdates(renderedItems: List<DropDownExpandableSelector.Selectable<String>>) {
                output.clear()
                output.addAll(renderedItems)
            }
        }
        ExpandablesHandler(baseItems = TestData.listWithNestedNoSelected, consumer = consumer, lastExpandedItem = null) { _, _, _ -> }

        assertEquals(3, output.size)
        assertEquals("l0_0, l0_1, l0_2", output.joinToString { it.value })
        output.forEach { selectable ->
            assertEquals(false, selectable.isExpanded)
        }
    }

    @Test
    fun testAutoExpandTopLevelSelected() {
        val output = mutableListOf<DropDownExpandableSelector.Selectable<String>>()
        val consumer = object : UpdatesConsumer<String> {
            override fun dispatchUpdates(renderedItems: List<DropDownExpandableSelector.Selectable<String>>) {
                output.clear()
                output.addAll(renderedItems)
            }
        }
        ExpandablesHandler(baseItems = TestData.listWithTopLevelSelected, consumer = consumer, lastExpandedItem = null) { _, _, _ -> }

        assertEquals(3, output.size)
        assertEquals("l0_0, l0_1, l0_2", output.joinToString { it.value })
        output.forEach { selectable ->
            assertEquals(false, selectable.isExpanded)
        }
    }

    @Test
    fun testExpandOnClick() {
        val output = mutableListOf<DropDownExpandableSelector.Selectable<String>>()
        val consumer = object : UpdatesConsumer<String> {
            override fun dispatchUpdates(renderedItems: List<DropDownExpandableSelector.Selectable<String>>) {
                output.clear()
                output.addAll(renderedItems)
            }
        }
        val sut = ExpandablesHandler(baseItems = TestData.listWithNestedNoSelected, consumer = consumer, lastExpandedItem = null) { _, _, _ -> }

        sut.itemWasClicked(1, TestData.listWithNestedNoSelected[1])
        assertEquals(6, output.size)
        assertEquals("l0_0, l0_1, l1_0, l1_1, l2_2, l0_2", output.joinToString { it.value })
        output.forEachIndexed { index, selectable ->
            if (index == 1) {
                assertEquals(true, selectable.isExpanded)
            } else {
                assertEquals(false, selectable.isExpanded)
            }
        }
    }

    @Test
    fun testExpandingOneItemCollapsesPrevious() {
        val output = mutableListOf<DropDownExpandableSelector.Selectable<String>>()
        val consumer = object : UpdatesConsumer<String> {
            override fun dispatchUpdates(renderedItems: List<DropDownExpandableSelector.Selectable<String>>) {
                output.clear()
                output.addAll(renderedItems)
            }
        }
        val sut = ExpandablesHandler(baseItems = TestData.listWithTwoNested, consumer = consumer, lastExpandedItem = null) { _, _, _ -> }

        sut.itemWasClicked(1, TestData.listWithTwoNested[1])
        assertEquals(7, output.size)
        assertEquals("l0_0, l0_1, l1_0, l1_1, l2_2, l0_2, l0_3", output.joinToString { it.value })
        output.forEachIndexed { index, selectable ->
            if (index == 1) {
                assertEquals(true, selectable.isExpanded)
            } else {
                assertEquals(false, selectable.isExpanded)
            }
        }

        sut.itemWasClicked(6, output[6])
        assertEquals(7, output.size)
        assertEquals("l0_0, l0_1, l0_2, l0_3, l3_0, l3_1, l3_2", output.joinToString { it.value })
        output.forEachIndexed { index, selectable ->
            if (index == 3) {
                assertEquals(true, selectable.isExpanded)
            } else {
                assertEquals(false, selectable.isExpanded)
            }
        }
    }
}

private object TestData {
    val listWithNestedSelected = listOf(
        DropDownExpandableSelector.Selectable(
            title = "l0_0",
            value = "l0_0",
            selected = false
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_1",
            value = "l0_1",
            selected = false,
            subItems = listOf(
                DropDownExpandableSelector.Selectable(
                    title = "l1_0",
                    value = "l1_0",
                    selected = false
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l1_1",
                    value = "l1_1",
                    selected = false,
                    subItems = listOf(
                        DropDownExpandableSelector.Selectable(
                            title = "l2_0",
                            value = "l2_0",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_1",
                            value = "l2_1",
                            selected = true
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_2",
                            value = "l2_2",
                            selected = false
                        )
                    )
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l1_2",
                    value = "l1_2",
                    selected = false
                )
            )
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_2",
            value = "l0_2",
            selected = false
        )
    )

    val listWithNestedNoSelected = listOf(
        DropDownExpandableSelector.Selectable(
            title = "l0_0",
            value = "l0_0",
            selected = false
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_1",
            value = "l0_1",
            selected = false,
            subItems = listOf(
                DropDownExpandableSelector.Selectable(
                    title = "l1_0",
                    value = "l1_0",
                    selected = false
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l1_1",
                    value = "l1_1",
                    selected = false,
                    subItems = listOf(
                        DropDownExpandableSelector.Selectable(
                            title = "l2_0",
                            value = "l2_0",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_1",
                            value = "l2_1",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_2",
                            value = "l2_2",
                            selected = false
                        )
                    )
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l2_2",
                    value = "l2_2",
                    selected = false
                )
            )
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_2",
            value = "l0_2",
            selected = false
        )
    )

    val listWithTopLevelSelected = listOf(
        DropDownExpandableSelector.Selectable(
            title = "l0_0",
            value = "l0_0",
            selected = false
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_1",
            value = "l0_1",
            selected = false,
            subItems = listOf(
                DropDownExpandableSelector.Selectable(
                    title = "l1_0",
                    value = "l1_0",
                    selected = false
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l1_1",
                    value = "l1_1",
                    selected = false,
                    subItems = listOf(
                        DropDownExpandableSelector.Selectable(
                            title = "l2_0",
                            value = "l2_0",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_1",
                            value = "l2_1",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_2",
                            value = "l2_2",
                            selected = false
                        )
                    )
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l2_2",
                    value = "l2_2",
                    selected = false
                )
            )
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_2",
            value = "l0_2",
            selected = true
        )
    )

    val listWithTwoNested = listOf(
        DropDownExpandableSelector.Selectable(
            title = "l0_0",
            value = "l0_0",
            selected = false
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_1",
            value = "l0_1",
            selected = false,
            subItems = listOf(
                DropDownExpandableSelector.Selectable(
                    title = "l1_0",
                    value = "l1_0",
                    selected = false
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l1_1",
                    value = "l1_1",
                    selected = false,
                    subItems = listOf(
                        DropDownExpandableSelector.Selectable(
                            title = "l2_0",
                            value = "l2_0",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_1",
                            value = "l2_1",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_2",
                            value = "l2_2",
                            selected = false
                        )
                    )
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l2_2",
                    value = "l2_2",
                    selected = false
                )
            )
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_2",
            value = "l0_2",
            selected = false
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_3",
            value = "l0_3",
            selected = false,
            subItems = listOf(
                DropDownExpandableSelector.Selectable(
                    title = "l3_0",
                    value = "l3_0",
                    selected = false
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l3_1",
                    value = "l3_1",
                    selected = false,
                    subItems = listOf(
                        DropDownExpandableSelector.Selectable(
                            title = "l31_0",
                            value = "l31_0",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l31_1",
                            value = "l31_1",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l31_2",
                            value = "l31_2",
                            selected = false
                        )
                    )
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l3_2",
                    value = "l3_2",
                    selected = false
                )
            )
        )
    )
}
