package com.etrade.mobilepro.login.presentation

class BiometricAuthenticationHelper {

    var onBiometricAuthentication: (() -> Unit)? = null
        set(value) {
            field = value
            if (pendingBiometricAuthenticationCallback && value != null) {
                pendingBiometricAuthenticationCallback = false
                value()
            }
        }

    private var pendingBiometricAuthenticationCallback: Boolean = false

    fun invokeBiometricAuthenticationCallback() {
        val callback = onBiometricAuthentication
        if (callback == null) {
            pendingBiometricAuthenticationCallback = true
        } else {
            callback()
        }
    }
}
