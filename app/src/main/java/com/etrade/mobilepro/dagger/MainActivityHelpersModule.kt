package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.home.helper.AlertsCountBadgeHelper
import com.etrade.mobilepro.home.helper.DefaultAlertsCountBadgeHelper
import dagger.Binds
import dagger.Module

@Module
interface MainActivityHelpersModule {
    @Binds
    fun bindAlertCountBadgeHelper(helper: DefaultAlertsCountBadgeHelper): AlertsCountBadgeHelper
}
