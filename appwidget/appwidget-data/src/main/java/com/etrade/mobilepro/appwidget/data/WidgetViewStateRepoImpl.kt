package com.etrade.mobilepro.appwidget.data

import com.etrade.mobilepro.appwidget.api.IndicesViewState
import com.etrade.mobilepro.appwidget.api.WidgetViewStateRepo
import com.etrade.mobilepro.caching.coroutine.Cache
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.util.KeyValueStore
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import javax.inject.Inject

private const val KEY_IS_WIDGET_PLACED = "WidgetViewStateRepoImpl.key.is_widget_placed"
private const val KEY_LATEST_RESOLVED_WATCHLIST_ENTRIES = "WidgetViewStateRepoImpl.key.latest_resolved_watchlist_entries"

class WidgetViewStateRepoImpl @Inject constructor(
    private val keyValueStore: KeyValueStore,
    private val watchlistEntryCache: Cache<List<WatchlistEntry>, String>
) : WidgetViewStateRepo {

    private var state: IndicesViewState? = null
    private val mutex = Mutex()

    override suspend fun getIndicesViewState(): IndicesViewState {
        return mutex.withLock {
            val state = state ?: IndicesViewState()
            state
        }
    }

    override suspend fun setIndicesViewState(viewState: IndicesViewState?) {
        mutex.withLock {
            state = viewState
        }
    }

    override suspend fun isWidgetPlaced(): Boolean {
        return keyValueStore.getBoolean(KEY_IS_WIDGET_PLACED, false)
    }

    override suspend fun setWidgetPlaced(isPlaced: Boolean) {
        keyValueStore.putBoolean(KEY_IS_WIDGET_PLACED, isPlaced)
    }

    override suspend fun getLatestResolvedWatchlistEntries(): ETResult<List<WatchlistEntry>> {
        return watchlistEntryCache.get(KEY_LATEST_RESOLVED_WATCHLIST_ENTRIES)
    }

    override suspend fun setLatestResolvedWatchlistEntries(entries: List<WatchlistEntry>?) {
        if (entries != null) {
            watchlistEntryCache.put(entries, KEY_LATEST_RESOLVED_WATCHLIST_ENTRIES)
        } else {
            watchlistEntryCache.remove(KEY_LATEST_RESOLVED_WATCHLIST_ENTRIES)
        }
    }
}
