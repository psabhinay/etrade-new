package com.etrade.mobilepro.util.domain.data

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class DerivedPositionDataTest {

    @Test
    fun `test deriveDaysGain - Derived value returned when trades have happened today`() {
        listOf(
            Pair(
                listOf(
                    BigDecimal("10.0"), BigDecimal("10"), BigDecimal("0"), BigDecimal("0"),
                    BigDecimal("0"), BigDecimal("0.0"), BigDecimal("0.0")
                ),
                Gain(dollar = BigDecimal("0.0"), percent = BigDecimal("0.0000"))
            ),
            Pair(
                listOf(
                    BigDecimal("18.50"), BigDecimal("5.0"), BigDecimal("4"), BigDecimal("4"),
                    BigDecimal("20.0"), BigDecimal("0.0"), BigDecimal("0.0")
                ),
                Gain(dollar = BigDecimal("-06.00"), percent = BigDecimal("-7.5000"))
            ),
            Pair(
                listOf(
                    BigDecimal("100.0"), BigDecimal("80.0"), BigDecimal("7"), BigDecimal("3"),
                    BigDecimal("90.0"), BigDecimal("1.0"), BigDecimal("2.50")
                ),
                Gain(dollar = BigDecimal("106.50"), percent = BigDecimal("16.9000"))
            ),
            Pair(
                listOf(
                    BigDecimal("100.0"), BigDecimal("80.0"), BigDecimal("7"), BigDecimal("0"),
                    BigDecimal("0.0"), BigDecimal("0.0"), BigDecimal("0.0")
                ),
                Gain(dollar = BigDecimal("140.0"), percent = BigDecimal("25.0000"))
            ),
            Pair(
                listOf(
                    BigDecimal("10.0"), BigDecimal("5.0"), BigDecimal("1"), BigDecimal("1"),
                    BigDecimal("0.0"), BigDecimal("0.0"), BigDecimal("0.0")
                ),
                Gain(dollar = BigDecimal("10.0"), percent = BigDecimal("0.0"))
            )
        ).forEach { testParams ->
            val result = deriveDaysGain(
                streamingLastPriceDecimal = testParams.first[0],
                previousClosePriceDecimal = testParams.first[1],
                quantityDecimal = testParams.first[2],
                todayQuantityDecimal = testParams.first[3],
                todayPricePaidDecimal = testParams.first[4],
                todayCommissionDecimal = testParams.first[5],
                feesDecimal = testParams.first[6]
            )
            assertEquals(testParams.second, result)
        }
    }

    @Test
    fun `test deriveDaysGain - Derived value returned when no trades have happened today`() {
        listOf(
            Pair(
                listOf(BigDecimal("10.0"), BigDecimal("5.0"), BigDecimal("1")),
                Gain(dollar = BigDecimal("5.0"), percent = BigDecimal("100.0000"))
            ),
            Pair(
                listOf(BigDecimal("12.0"), BigDecimal("10.0"), BigDecimal("3")),
                Gain(dollar = BigDecimal("6.0"), percent = BigDecimal("20.0000"))
            ),
            Pair(
                listOf(BigDecimal("90.0"), BigDecimal("100.0"), BigDecimal("1")),
                Gain(dollar = BigDecimal("-10.0"), percent = BigDecimal("-10.0000"))
            ),
            Pair(
                listOf(BigDecimal("90.0"), BigDecimal("120.0"), BigDecimal("10")),
                Gain(dollar = BigDecimal("-300.0"), percent = BigDecimal("-25.0000"))
            )
        ).forEach { testParams ->
            val result = deriveDaysGain(
                streamingLastPriceDecimal = testParams.first[0],
                previousClosePriceDecimal = testParams.first[1],
                quantityDecimal = testParams.first[2]
            )

            assertEquals(testParams.second, result)
        }
    }

    @Test
    fun `test deriveDaysGain - Derived value returned when ENTIRE positions traded today`() {
        listOf(
            Pair(
                listOf(
                    BigDecimal("10.0"), BigDecimal("5.0"), BigDecimal("1"), BigDecimal("1"),
                    BigDecimal("15.0"), BigDecimal("0.0"), BigDecimal("0.0")
                ),
                Gain(dollar = BigDecimal("-5.0"), percent = BigDecimal("-33.3300"))
            ),
            Pair(
                listOf(
                    BigDecimal("10.0"), BigDecimal("5.0"), BigDecimal("-1"), BigDecimal("-1"),
                    BigDecimal("15.0"), BigDecimal("0.0"), BigDecimal("0.0")
                ),
                Gain(dollar = BigDecimal("5.0"), percent = BigDecimal("33.3300"))
            ),
            Pair(
                listOf(
                    BigDecimal("10.0"), BigDecimal("15.0"), BigDecimal("-1"), BigDecimal("-1"),
                    BigDecimal("15.0"), BigDecimal("0.0"), BigDecimal("0.0")
                ),
                Gain(dollar = BigDecimal("5.0"), percent = BigDecimal("33.3300"))
            )
        ).forEach { testParams ->
            val result = deriveDaysGain(
                streamingLastPriceDecimal = testParams.first[0],
                previousClosePriceDecimal = testParams.first[1],
                quantityDecimal = testParams.first[2],
                todayQuantityDecimal = testParams.first[3],
                todayPricePaidDecimal = testParams.first[4],
                todayCommissionDecimal = testParams.first[5],
                feesDecimal = testParams.first[6],
                basisPrice = null
            )
            assertEquals(testParams.second, result)
        }
    }

    /**
     *
     * Sample cost basis calculation:
     * 1 qty @ 15 yesterday
     * 9 qty @ 20 today
     * cost basis = (15*1 + 20*9) / 10 = 19.5
     **/
    @Test
    fun `test deriveDaysGain - Derived value returned when PARTIAL positions traded today`() {
        listOf(
            Pair(
                listOf(
                    BigDecimal("10.0"), BigDecimal("15.0"), BigDecimal("-10"), BigDecimal("-9"),
                    BigDecimal("20.0"), BigDecimal("0.0"), BigDecimal("0.0"),
                    BigDecimal("19.5")
                ),
                Gain(dollar = BigDecimal("95.0"), percent = BigDecimal("48.7200"))
            ),
            Pair(
                listOf(
                    BigDecimal("10.0"), BigDecimal("5.0"), BigDecimal("5"), BigDecimal("1"),
                    BigDecimal("15.0"), BigDecimal("0.0"), BigDecimal("0.0"), BigDecimal("20.0")
                ),
                Gain(dollar = BigDecimal("15.0"), percent = BigDecimal("15.0000"))
            ),
            Pair(
                listOf(
                    BigDecimal("10.0"), BigDecimal("5.0"), BigDecimal("-10"), BigDecimal("-4"),
                    BigDecimal("15.0"), BigDecimal("0.0"), BigDecimal("0.0"), BigDecimal("19.50")
                ),
                Gain(dollar = BigDecimal("-10.0"), percent = BigDecimal("-5.1300"))
            )
        ).forEach { testParams ->
            val result = deriveDaysGain(
                streamingLastPriceDecimal = testParams.first[0],
                previousClosePriceDecimal = testParams.first[1],
                quantityDecimal = testParams.first[2],
                todayQuantityDecimal = testParams.first[3],
                todayPricePaidDecimal = testParams.first[4],
                todayCommissionDecimal = testParams.first[5],
                feesDecimal = testParams.first[6],
                basisPrice = testParams.first[7]
            )
            assertEquals(testParams.second, result)
        }
    }

    @Test
    fun `test deriveMarketValue`() {
        listOf(
            listOf(BigDecimal(5), BigDecimal.TEN, BigDecimal(50))
        ).forEach { testParams ->
            val result = deriveMarketValue(
                price = testParams[0],
                quantity = testParams[1]
            )
            assertEquals(testParams[2], result)
        }
    }
}
