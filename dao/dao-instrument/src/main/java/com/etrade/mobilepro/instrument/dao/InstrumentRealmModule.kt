package com.etrade.mobilepro.instrument.dao

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class InstrumentRealmModule
