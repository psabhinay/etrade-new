package com.etrade.mobilepro.dialog

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.text.method.LinkMovementMethod
import android.text.util.Linkify
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.text.util.LinkifyCompat
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder

fun Context.showDialog(dialog: AppDialog): Dialog {
    return MaterialAlertDialogBuilder(this)
        .setTitle(dialog.title)
        .setMessage(dialog.message)
        .setCancelable(dialog.isCancelable)
        .addAction(dialog.positiveAction, AlertDialog.Builder::setPositiveButton)
        .addAction(dialog.negativeAction, AlertDialog.Builder::setNegativeButton)
        .addAction(dialog.neutralAction, AlertDialog.Builder::setNeutralButton)
        .apply { dialog.cancelAction?.let { setOnCancelListener { it() } } }
        .apply { dialog.dismissAction?.let { setOnDismissListener { it() } } }
        .show()
        .apply {
            if (dialog.linkifyMessage) {
                findViewById<TextView>(android.R.id.message)?.apply {
                    LinkifyCompat.addLinks(this, Linkify.WEB_URLS)
                    movementMethod = LinkMovementMethod.getInstance()
                }
            }
            // https://github.com/material-components/material-components-android/issues/742
            window?.let {
                it.attributes = it.attributes?.also {
                    it.width = WindowManager.LayoutParams.MATCH_PARENT
                    it.height = WindowManager.LayoutParams.WRAP_CONTENT
                }
            }
        }
}

fun Fragment.showDialog(dialog: AppDialog): Dialog? = context?.run { showDialog(dialog) }

private inline fun AlertDialog.Builder.addAction(
    action: AppDialog.Action?,
    setter: AlertDialog.Builder.(CharSequence, DialogInterface.OnClickListener) -> AlertDialog.Builder
): AlertDialog.Builder {

    if (action != null) {
        setter(
            action.label,
            DialogInterface.OnClickListener { _, _ ->
                action.perform?.invoke()
            }
        )
    }
    return this
}
