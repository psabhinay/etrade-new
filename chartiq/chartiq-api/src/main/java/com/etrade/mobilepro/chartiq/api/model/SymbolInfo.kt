package com.etrade.mobilepro.chartiq.api.model

import com.etrade.mobilepro.instrument.InstrumentType

data class SymbolInfo(
    val ticker: String,
    val instrumentType: InstrumentType,
    val lastPrice: String,
    val isExtendedHours: Boolean,
    val isStreaming: Boolean
)
