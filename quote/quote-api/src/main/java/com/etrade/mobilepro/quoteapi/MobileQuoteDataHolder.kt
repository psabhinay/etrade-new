package com.etrade.mobilepro.quoteapi

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface MobileQuoteDataHolder {
    fun refresh()
    val mobileQuoteDataFlow: StateFlow<MobileQuote>
    val quoteWidgetItemFlow: Flow<QuoteWidgetItem>
}
