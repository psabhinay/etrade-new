package com.etrade.mobilepro.trade.presentation.form.approval

import android.content.Context
import com.etrade.mobilepro.dynamic.form.ui.factory.ComponentViewFactory

class ApprovalLinkComponentViewFactory : ComponentViewFactory<ApprovalLinkComponent, ApprovalLinkComponentView> {
    override fun createView(context: Context): ApprovalLinkComponentView =
        ApprovalLinkComponentView(context)
}
