package com.etrade.mobilepro.orders.presentation.section.viewfactory

import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

class MultiLegsOrderSectionViewFactory : OrderDetailSectionViewFactory {
    override fun createSectionView(
        detailsDto: BaseOrderDetailsItem.OrderDetailsItem,
        resources: OrderDetailLabelResources,
        isConditionalOrder: Boolean
    ): List<TableSectionItemLayout> {
        val sections = mutableListOf<TableSectionItemLayout>()
        sections.addAll(getDatesSections(detailsDto, resources, true))
        sections.addAll(getSavedInfo(detailsDto, resources, true))
        sections.addAll(getPriceSections(detailsDto, resources, isConditionalOrder))
        return sections
    }
}
