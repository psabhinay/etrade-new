package com.etrade.mobilepro.screener.api.filter.mutualfund

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class PerformanceFilter(
    val bottom20: Boolean = false,
    val belowMid20: Boolean = false,
    val mid20: Boolean = false,
    val aboveMid20: Boolean = false,
    val top20: Boolean = false,

    val overLast1Month: Boolean = false,
    val overLast3Months: Boolean = false,
    val overLast6Months: Boolean = false,
    val overLastYTD: Boolean = false,
    val overLast1Year: Boolean = false,
    val overLast3Years: Boolean = false,
    val overLast5Years: Boolean = false,
    val overLast10Years: Boolean = false
) : ScreenerFilter {
    fun isPerformanceValuesSelected(): Boolean = bottom20 || belowMid20 ||
        mid20 || aboveMid20 || top20

    override fun isEmpty(): Boolean = !isPerformanceValuesSelected() && !isPeriodSelected()

    fun isPeriodSelected(): Boolean = overLast1Month || overLast3Months || overLast6Months || overLastYTD || overLast1Year || overLast3Years ||
        overLast5Years || overLast10Years

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND
}
