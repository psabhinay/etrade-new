package org.chromium.customtabsclient.shared

import android.net.Uri
import androidx.annotation.DrawableRes

fun internalUrlToUri(title: String? = null, url: String, @DrawableRes upIconResId: Int = 0, externalUrlView: Boolean = false): Uri {
    return Uri.parse("etrade://webview").buildUpon().apply {
        appendQueryParameter("url", url)
        title?.let { appendQueryParameter("title", it) }
        appendQueryParameter("upIcon", upIconResId.toString())
        appendQueryParameter("external", externalUrlView.toString())
    }.build()
}
