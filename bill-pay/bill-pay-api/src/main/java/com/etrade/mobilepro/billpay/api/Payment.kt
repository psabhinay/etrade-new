package com.etrade.mobilepro.billpay.api

import org.threeten.bp.LocalDate
import java.math.BigDecimal

data class Payment(
    val paymentId: String,
    val status: PaymentStatus,
    val amount: BigDecimal?,
    val payeeName: String?,
    val payeeNickname: String?,
    val payeeAccountNumber: String?,
    val deliveryDate: LocalDate?,
    val scheduleDate: LocalDate?,
    val memo: String?,
    val confirmationNumber: String?,
    val accountId: String?
)

fun Payment.getPaymentDate(): LocalDate? {
    return when (status) {
        PaymentStatus.PAID -> deliveryDate
        else -> scheduleDate
    }
}
