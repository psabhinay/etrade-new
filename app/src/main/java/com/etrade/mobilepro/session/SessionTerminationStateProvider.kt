package com.etrade.mobilepro.session

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.session.api.SessionEvents
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

interface SessionTerminationStateProvider {

    val sessionEvent: LiveData<ConsumableLiveEvent<SessionEvents>>

    fun notifySessionTermination()
}

class SessionTerminationStateProviderImpl @Inject constructor() : SessionTerminationStateProvider {

    private val _sessionEvent = MutableLiveData<ConsumableLiveEvent<SessionEvents>>()

    override val sessionEvent: LiveData<ConsumableLiveEvent<SessionEvents>>
        get() = _sessionEvent

    override fun notifySessionTermination() {
        _sessionEvent.postValue(
            ConsumableLiveEvent(SessionEvents.MSG_SESSION_TERMINATED)
        )
    }
}
