package com.etrade.mobilepro.trade.presentation.form.delegate

import android.content.Context
import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.map
import com.etrade.mobilepro.dynamic.form.api.ComponentBuilder
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.FormBuilder
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.InputHolder
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.SelectInputBuilder
import com.etrade.mobilepro.dynamic.form.api.SwitchInput
import com.etrade.mobilepro.dynamic.form.api.form
import com.etrade.mobilepro.dynamic.form.api.inputParameterValue
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.livedata.combine
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.quote.search.history.QuoteSearchHistoryViewModel
import com.etrade.mobilepro.quote.search.presentation.OnSearchResultItemClickListener
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.api.TradeOptionInfoType
import com.etrade.mobilepro.trade.api.option.OptionTradeStrategy
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.approvallevel.ApprovalLevelDataPoint
import com.etrade.mobilepro.trade.dropdown.form.tradeSelect
import com.etrade.mobilepro.trade.dropdown.help.TradeHelpParams
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.input.ContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithTradeLegsTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.trade.optionlegselect.TradeLegBidAskViewModel
import com.etrade.mobilepro.trade.presentation.BR
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.extensions.selectedAccount
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction
import com.etrade.mobilepro.trade.presentation.form.approval.ApprovalLinkComponent
import com.etrade.mobilepro.trade.presentation.form.component.FooterLink
import com.etrade.mobilepro.trade.presentation.form.component.TradeLegsInput
import com.etrade.mobilepro.trade.presentation.form.component.builder.footer
import com.etrade.mobilepro.trade.presentation.form.component.builder.strategyType
import com.etrade.mobilepro.trade.presentation.form.component.builder.tradeLegs
import com.etrade.mobilepro.trade.presentation.form.delegate.TradeFormFactoryDelegate.OnFormChangeListener
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.AccountSectionGroup
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.SymbolInputParams
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.accountSectionGroup
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.collectStopOrLimitOrStopLimit
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.createTermOptions
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.liveForm
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.observeAdvancedOrderForConditionalOrderDisclosure
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.price
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.quoteWidget
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.resetAdvancedOrderTypeOnAdvancedSwitchTurnedOff
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.resetAdvancedOrderTypeOnPriceTypeChanges
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.resetPricesOnSymbolChange
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.resetPricesOnVisibilityChange
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.resetTermAndAllOrNoneOnPriceTypeChange
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.saveAndPreview
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.securityTypeAndSymbol
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.showNotEligibleSymbolDialog
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.stockAndOptionsFooter
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter
import com.etrade.mobilepro.trade.presentation.util.isEligibleForOptionTrade
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.util.ValueCoder
import com.etrade.mobilepro.util.safeLet
import com.etrade.mobilepro.walkthrough.api.State

private const val RECENT_SYMBOLS_MAX_SIZE: Int = 10

internal open class OptionTradeFormFactoryDelegate(
    private val context: Context,
    private val dependencies: FormFactoryDelegateDependencies,
    private val securityType: SecurityType,
    private val inputId: OptionTradeFormInputId,
    private val tradeFormTypeUpdateDelegate: TradeFormTypeUpdateDelegate<OptionTradeFormInputId>,
    private val tradeLegsCoder: ValueCoder<List<TradeLeg>>,
    private val tradingDefaultsPreferences: TradingDefaultsPreferences
) : TradeFormFactoryDelegate {

    open val formType = TradeFormType(securityType)

    @Suppress("LongMethod")
    override fun createForm(
        accounts: List<TradeAccount>,
        accountDelegate: SelectedAccountDelegate,
        parameter: CreateTradeFormParameter,
        updateForm: OnFormChangeListener
    ): Form {

        val resources = context.resources
        return form {
            hiddenInput(this)

            accountSectionGroup(
                resources,
                inputId,
                AccountSectionGroup(
                    accounts,
                    accountDelegate.getCurrentAccountId(securityType), parameter.balanceViewModel
                )
            ) {
                approvalLink(
                    resources,
                    parameter.onActionClickListener,
                    selectedAccount(inputId, accounts)
                )
            }

            verticalGroup {
                securityTypeAndSymbol(
                    inputId,
                    resources,
                    securityType,
                    parameter.onActionClickListener,
                    parameter.walkthroughViewModel.state,
                    inputId.lookupSymbol.extractLookupSymbolInfo(parameter.defaultValues, inputId.symbolCoder)
                )
                quoteWidget(
                    inputId.lookupSymbol,
                    parameter.quoteWidgetViewModel,
                    R.layout.quote_view_base_with_retry
                )
                recentSymbols(
                    inputId,
                    resources,
                    securityType,
                    parameter.quoteSearchHistoryViewModel,
                    parameter.onActionClickListener
                )

                // trade legs section
                strategyType(inputId.strategy, resources)
                tradeLegs(inputId, tradeLegsCoder)
                bidAsk(inputId, parameter.tradeLegBidAskViewModel)
                editTradeLegs(resources, inputId, parameter.onActionClickListener)

                // price type and term section
                optionPrices(
                    inputId,
                    resources,
                    dependencies,
                    securityType,
                    tradingDefaultsPreferences.optionsPriceType
                ) {
                    hideWhenNoSymbol(inputId)
                }
                term(resources, inputId, dependencies.limitOrderTermLabelMap)
                allOrNoneSwitch(inputId, resources, dependencies.allOrNoneValidPriceTypes)
            }

            verticalGroup {
                advancedOrderSwitch(inputId, resources, parameter)
                advancedOrderType(inputId, resources)
                advancedOrderFormInputs(this, parameter)

                // footer
                stockAndOptionsFooter(resources, parameter.onActionClickListener) {
                    hideWhenNoSymbol(inputId)
                }
            }

            rules {
                liveForm(
                    inputId,
                    securityType,
                    updateForm,
                    parameter.conditionalOrderDisclosureViewModel,
                    accountDelegate
                )
                bidAskUpdatesOnTradeLegsChange(inputId, parameter.tradeLegBidAskViewModel)
                resetPricesOnVisibilityChange(inputId)
                resetPricesOnSymbolChange(inputId.lookupSymbol, inputId)
                resetTermAndAllOrNoneOnPriceTypeChange(inputId)
                observeAdvancedOrderForConditionalOrderDisclosure(inputId, parameter.conditionalOrderDisclosureViewModel)
                if (securityType != SecurityType.STOCK_AND_OPTIONS) {
                    advancedOrderRules(this, updateForm)
                }
            }

            val requiredInputs = dependencies.saveAndPreviewActionRequiredInputs + setOf(inputId.tradeLegs, inputId.strategy)
            saveAndPreview(resources, parameter, requiredInputs)
        }
    }

    open fun advancedOrderRules(ruleBuilder: RuleListBuilder, updateForm: OnFormChangeListener) {
        ruleBuilder.apply {
            tradeFormTypeUpdateDelegate.onFormTypeUpdated(
                collectParams,
                formType,
                inputId,
                updateForm,
                ruleBuilder
            )
            resetAdvancedOrderTypeOnAdvancedSwitchTurnedOff(inputId)
            resetAdvancedOrderTypeOnPriceTypeChanges(inputId, inputId)
            resetAdvancedOrderTypeOnTradeLegChanges(inputId)
        }
    }

    open fun hiddenInput(formBuilder: FormBuilder) {
        inputId.apply {
            formBuilder.hidden(orderId)
        }
    }

    open fun advancedOrderFormInputs(
        groupBuilder: GroupBuilder,
        parameter: CreateTradeFormParameter
    ) = Unit
}

private fun GroupBuilder.term(
    resources: Resources,
    inputId: OptionTradeFormInputId,
    limitOrderTermLabelMap: Map<String, String>
) {
    select(inputId.term) {
        title = resources.getString(R.string.trade_hint_term)
        dynamicTermRule(inputId, resources, limitOrderTermLabelMap)
        hideWhenNoSymbol(inputId)
    }
}

private fun SelectInputBuilder.dynamicTermRule(
    inputId: OptionTradeFormInputId,
    resources: Resources,
    limitOrderTermLabelMap: Map<String, String>
) {
    dynamic {
        rule {
            inputValue<DropDownOption>(inputId.priceType).map { option ->
                createTermOptions(resources, inputId, limitOrderTermLabelMap, option)
            }
        }
    }
}

private fun GroupBuilder.approvalLink(
    resources: Resources,
    onActionClickListener: OnActionClickListener,
    selectedAccount: LiveData<TradeAccount?>
) {
    custom {
        ApprovalLinkComponent(
            approvalLevelDataPoint = selectedAccount.map {
                ApprovalLevelDataPoint(
                    resources.getString(
                        R.string.approval_level_label,
                        resources.getString(R.string.approval_level_title_options),
                        it?.approvalLevel
                    ),
                    it?.accountMode ?: AccountMode.UNKNOWN
                )
            },
            onActionClickListener = onActionClickListener
        )
    }
}

private fun GroupBuilder.bidAsk(
    inputId: WithTradeLegsTradeFormInputId,
    bidAskViewModel: TradeLegBidAskViewModel
) {
    layout(R.layout.trade_view_bid_ask) {
        bindings {
            binding(BR.tradeBidAskItem) {
                variable = bidAskViewModel.quoteBidAskItem
            }
        }
        visibleIf {
            requireAnyInput(inputId.tradeLegs).hasValue
        }
    }
}

private fun GroupBuilder.editTradeLegs(
    resources: Resources,
    inputId: WithTradeLegsTradeFormInputId,
    onActionClickListener: OnActionClickListener
) {
    footer {
        links = inputValue<List<TradeLeg>>(inputId.tradeLegs).map {
            listOf(
                FooterLink(
                    resources.getQuantityString(
                        R.plurals.trade_link_edit_legs,
                        it.orEmpty().size
                    )
                ) {
                    onActionClickListener.onActionClick(TradeFormAction.OptionLegSelect(newSearch = false))
                }
            )
        }
        visibleIf {
            with(requireAnyInput(inputId.tradeLegs)) {
                hasValue.combine(isEnabled) { hasValue, isEnabled ->
                    hasValue == true && isEnabled == true
                }
            }
        }
    }
}

private fun GroupBuilder.recentSymbols(
    inputId: OptionTradeFormInputId,
    resources: Resources,
    securityType: SecurityType,
    viewModel: QuoteSearchHistoryViewModel,
    onActionClickListener: OnActionClickListener
) {
    viewModel.fetch(RECENT_SYMBOLS_MAX_SIZE)
    layout(R.layout.trade_view_recent_symbols) {
        bindings {
            binding(BR.viewModel) {
                variable = viewModel
            }
            binding(BR.onSearchResultItemClickListener) {
                variable = object : OnSearchResultItemClickListener {
                    override fun onSearchResultItemClicked(item: SearchResultItem) {
                        if (item is SearchResultItem.Symbol && item.isEligibleForOptionTrade(
                                securityType == SecurityType.STOCK_AND_OPTIONS
                            )
                        ) {
                            onActionClickListener.onActionClick(
                                TradeFormAction.OptionLegSelect(
                                    symbol = item
                                )
                            )
                        } else {
                            onActionClickListener.showNotEligibleSymbolDialog(
                                resources,
                                R.string.trade_error_invalid_option_trade_symbol
                            )
                        }
                    }
                }
            }
        }
        visibleIf {
            requireAnyInput(inputId.lookupSymbol).hasValue.map { !it }
        }
    }
}

@Suppress("LongParameterList", "LongMethod")
private fun GroupBuilder.securityTypeAndSymbol(
    inputId: OptionTradeFormInputId,
    resources: Resources,
    securityType: SecurityType,
    onActionClickListener: OnActionClickListener,
    walkthroughState: LiveData<State>,
    initialSymbol: WithSymbolInfo? = null
) {
    val validTradeTypes = if (securityType == SecurityType.STOCK_AND_OPTIONS) {
        InstrumentType.validStocksAndOptionTradingTypes
    } else {
        InstrumentType.validOptionsTradingTypes
    }

    val params = SymbolInputParams(validTradeTypes, inputId.lookupSymbol, inputId.symbolCoder, onActionClickListener) {
        onActionClickListener.showNotEligibleSymbolDialog(
            resources,
            R.string.trade_error_invalid_option_trade_symbol
        )
    }
    securityTypeAndSymbol(
        resources,
        inputId,
        securityType,
        params,
        { onActionClickListener.onActionClick(TradeFormAction.OptionLegSelect()) },
        walkthroughState,
        initialSymbol
    )
}

@Suppress("LongParameterList", "LongMethod")
private fun GroupBuilder.optionPrices(
    inputId: OptionTradeFormInputId,
    resources: Resources,
    dependencies: FormFactoryDelegateDependencies,
    securityType: SecurityType,
    defaultPriceType: PriceType?,
    init: SelectInputBuilder.() -> Unit = { /* intentionally blank */ }
) {
    horizontalGroup {
        optionPriceType(inputId, resources, securityType, defaultPriceType, init)
        price(inputId.limitPrice, inputId, dependencies.priceTypePriceLabelMap, null, dependencies.isLimitPriceInputVisible)
        price(inputId.stopPrice, inputId, dependencies.priceTypePriceLabelMap, null, dependencies.isStopPriceInputVisible)
    }
    horizontalGroup {
        price(
            inputId.limitPriceForStopLimit,
            inputId,
            null,
            dependencies.limitPriceLabel,
            dependencies.isStopLimitRowVisible
        )
        price(
            inputId.stopPriceForStopLimit,
            inputId,
            null,
            dependencies.stopPriceLabel,
            dependencies.isStopLimitRowVisible
        )
    }
}

@Suppress("LongParameterList", "LongMethod")
private inline fun GroupBuilder.optionPriceType(
    inputId: OptionTradeFormInputId,
    resources: Resources,
    securityType: SecurityType,
    defaultPriceType: PriceType?,
    crossinline init: SelectInputBuilder.() -> Unit
) {
    tradeSelect(inputId.priceType) {
        title = resources.getString(R.string.trade_hint_price_type)
        defaultOptionId = if (securityType == SecurityType.STOCK_AND_OPTIONS) {
            inputId.priceTypeValueId.market
        } else {
            defaultPriceType?.let { inputId.priceTypeValueId.converter.toKey(it) } ?: inputId.priceTypeValueId.market
        }
        helpParams = TradeHelpParams(TradeOptionInfoType.PRICE_TYPE_OPTIONS)
        dynamic {
            rule {
                getPriceTypeOptions(inputId, resources).distinctUntilChanged()
            }
        }
        init()
    }
}

private fun InputHolder.getPriceTypeOptions(
    inputId: OptionTradeFormInputId,
    resources: Resources
): LiveData<List<DropDownOption>> {
    return requireInput<TradeLegsInput>(inputId.tradeLegs).value.map { tradeLegs ->
        val priceTypeOptions = mutableListOf(
            DropDownOption(
                inputId.priceTypeValueId.market,
                resources.getString(R.string.trade_price_type_market)
            )
        )
        tradeLegs?.let {
            when {
                tradeLegs.size == 1 -> {
                    priceTypeOptions.prepareForOnlyOneLeg(inputId.priceTypeValueId, resources)
                }
                tradeLegs.size > 1 -> {
                    priceTypeOptions.prepareForMultipleLegs(inputId.priceTypeValueId, resources)
                }
                else -> {
                    /* No selected trade legs then no price type to add */
                }
            }
        }
        priceTypeOptions.filterPriceTypeOptions(inputId)
    }
}

private fun MutableList<DropDownOption>.filterPriceTypeOptions(inputId: OptionTradeFormInputId): List<DropDownOption> {
    if (inputId is ContingentOptionTradeFormInputId) {
        removeIf { priceType ->
            priceType.id == inputId.priceTypeValueId.trailingStopDollar || priceType.id == inputId.priceTypeValueId.trailingStopPercent
        }
    }
    return this
}

private fun MutableList<DropDownOption>.add(value: String, label: String) {
    add(DropDownOption(value, label))
}

private fun MutableList<DropDownOption>.prepareForMultipleLegs(
    valueId: PriceTypeValueId,
    resources: Resources
) {
    add(valueId.netCredit, resources.getString(R.string.trade_price_type_net_credit))
    add(valueId.netDebit, resources.getString(R.string.trade_price_type_net_debit))
    add(valueId.even, resources.getString(R.string.trade_price_type_even))
}

private fun MutableList<DropDownOption>.prepareForOnlyOneLeg(
    valueId: PriceTypeValueId,
    resources: Resources
) {
    add(valueId.limit, resources.getString(R.string.trade_price_type_limit))
    add(valueId.stop, resources.getString(R.string.trade_price_type_stop))
    add(valueId.stopLimit, resources.getString(R.string.trade_price_type_stop_limit))
    add(valueId.trailingStopDollar, resources.getString(R.string.trade_price_type_trailing_stop))
}

private fun GroupBuilder.allOrNoneSwitch(
    inputId: WithPriceTradeFormInputId,
    resources: Resources,
    validPriceTypes: List<String>
) {
    switch(inputId.allOrNone) {
        title = resources.getString(R.string.trade_hint_all_or_none)
        valueCoder = inputId.allOrNoneValueCoder
        visibleIf {
            inputParameterValue(inputId.priceType).map { priceType ->
                if (priceType == null) {
                    false
                } else {
                    priceType in validPriceTypes
                }
            }
        }
    }
}

private fun ComponentBuilder.hideWhenNoSymbol(inputId: OptionTradeFormInputId) {
    visibleIf {
        requireAnyInput(inputId.lookupSymbol).hasValue
    }
}

@Suppress("LongMethod")
private fun GroupBuilder.advancedOrderSwitch(
    inputId: OptionTradeFormInputId,
    resources: Resources,
    parameter: CreateTradeFormParameter
) {
    switch(inputId.advancedOrder) {
        title = resources.getString(R.string.trade_hint_advanced_order)
        enabledIf {
            MediatorLiveData<Boolean>().apply {
                val onChanged = Observer<Pair<List<TradeLeg>?, String?>> {
                    val isEnabled = isAdvancedOrderEnabled(inputId, it.first, it.second, parameter)
                    if (!isEnabled) {
                        requireInput<SwitchInput>(inputId.advancedOrder).setValue(false)
                    }
                    value = isEnabled
                }
                addSource(
                    inputValue<List<TradeLeg>?>(inputId.tradeLegs).map { tradingLegs ->
                        val priceType = inputValue<DropDownOption?>(inputId.priceType).value?.id
                        tradingLegs to priceType
                    },
                    onChanged
                )
                addSource(
                    inputValue<DropDownOption?>(inputId.priceType).map {
                        val tradingLegs = inputValue<List<TradeLeg>?>(inputId.tradeLegs).value
                        val priceType = it?.id
                        tradingLegs to priceType
                    },
                    onChanged
                )
            }
        }
    }
}

private fun GroupBuilder.advancedOrderType(inputId: OptionTradeFormInputId, resources: Resources) {
    select(inputId.advancedOrderType) {
        title = resources.getString(R.string.trade_hint_order_type)
        dynamic {
            rule {
                MediatorLiveData<List<DropDownOption>>().apply {
                    val onChanged = Observer<Unit> {
                        value = getOrderTypeOptions(inputId, resources)
                    }
                    addSource(
                        inputValue<List<TradeLeg>?>(inputId.tradeLegs).map { Unit },
                        onChanged
                    )
                    addSource(
                        inputValue<DropDownOption?>(inputId.priceType).map { Unit },
                        onChanged
                    )
                }
            }
        }
        visibleIf {
            inputValue<Boolean?>(inputId.advancedOrder).map {
                it == true
            }
        }
    }
}

private fun InputHolder.getOrderTypeOptions(
    inputId: OptionTradeFormInputId,
    resources: Resources
): List<DropDownOption> {
    return mutableListOf<DropDownOption>().apply {
        val priceType = inputValue<DropDownOption?>(inputId.priceType).value?.id
        if (priceType != inputId.priceTypeValueId.trailingStopPercent && priceType != inputId.priceTypeValueId.trailingStopDollar) {
            add(
                DropDownOption(
                    id = inputId.advancedOrderTypeValueId.contingent,
                    name = resources.getString(R.string.trade_advanced_order_type_contingent_title),
                    description = resources.getString(R.string.trade_advanced_order_type_contingent_description)
                )
            )
        }
        val leg = inputValue<List<TradeLeg>?>(inputId.tradeLegs).value?.firstOrNull()
        if (leg?.transactionType == TransactionType.BUY_OPEN_OPT || leg?.transactionType == TransactionType.SELL_OPEN_OPT) {
            add(
                DropDownOption(
                    id = inputId.advancedOrderTypeValueId.stopLoss,
                    name = resources.getString(R.string.trade_advanced_order_type_stop_loss_title),
                    description = resources.getString(R.string.trade_advanced_order_type_stop_loss_description)
                )
            )
        }
    }
}

private fun isAdvancedOrderEnabled(
    inputId: OptionTradeFormInputId,
    legs: List<TradeLeg>?,
    priceType: String?,
    parameter: CreateTradeFormParameter
): Boolean {
    val legsSize = legs?.size ?: 0
    return if (legsSize > 1) {
        false
    } else {
        val tradingLeg = legs?.firstOrNull()
        val isSell = tradingLeg?.transactionType == TransactionType.SELL_CLOSE_OPT
        val isBuyToCover = tradingLeg?.transactionType == TransactionType.BUY_CLOSE_OPT
        val isTrailingStopDollar = priceType == inputId.priceTypeValueId.trailingStopDollar
        val isSellOrBuyToCover = isSell || isBuyToCover
        val isEdit = parameter.tradeOrderType.isEdit()
        return !isEdit && !(isSellOrBuyToCover && isTrailingStopDollar)
    }
}

private fun RuleListBuilder.resetAdvancedOrderTypeOnTradeLegChanges(inputId: OptionTradeFormInputId) {
    rule {
        inputValue<List<TradeLeg>?>(inputId.tradeLegs).observeForever { legs ->
            val advancedOrderTypeInput = getInput<SelectInput>(inputId.advancedOrderType)
            val legsSize = legs?.size ?: 0
            if (legsSize > 1) {
                advancedOrderTypeInput?.resetValue()
            } else {
                val leg = legs?.firstOrNull()
                if (leg?.transactionType == TransactionType.SELL_OPEN_OPT || leg?.transactionType == TransactionType.BUY_CLOSE_OPT) {
                    val isStopLossSelected =
                        advancedOrderTypeInput?.value?.value?.id == inputId.advancedOrderTypeValueId.stopLoss
                    if (isStopLossSelected) {
                        advancedOrderTypeInput?.resetValue()
                    }
                }
            }
        }
    }
}

private fun RuleListBuilder.bidAskUpdatesOnTradeLegsChange(
    inputId: WithTradeLegsTradeFormInputId,
    bidAskViewModel: TradeLegBidAskViewModel
) {
    rule {
        requireInput<TradeLegsInput>(inputId.tradeLegs).value.observeForever {
            if (it != null) {
                bidAskViewModel.subscribeToBidAsk(it)
            } else {
                bidAskViewModel.unsubscribeBidAsk()
            }
        }
    }
}

private val collectParams: CollectParams<OptionTradeFormInputId> = { inputHolder, id ->
    val strategyType = inputHolder.inputValue<StrategyType?>(id.strategy).value
    val tradeLegs = inputHolder.inputValue<List<TradeLeg>?>(id.tradeLegs).value
    safeLet(strategyType, tradeLegs) { type, legs ->
        optionTradeStrategy = object : OptionTradeStrategy {
            override val strategyType: StrategyType = type
            override val optionLegs: List<TradeLeg> = legs
        }
    }

    symbol = inputHolder.inputValue<WithSymbolInfo>(id.lookupSymbol).value
    collectStopOrLimitOrStopLimit(inputHolder, id.limitPrice, id.limitPriceForStopLimit, isLimit = true)
    collectStopOrLimitOrStopLimit(inputHolder, id.stopPrice, id.stopPriceForStopLimit)
    accountId = inputHolder.requireInput<SelectInput>(id.accountId).value.value?.id
    inputHolder.inputValue<DropDownOption>(id.priceType).value?.id?.let { priceType = id.priceTypeValueId.converter.toValue(it) }
    term = inputHolder.inputValue<DropDownOption>(id.term).value?.id?.let { id.termValueId.converter.toValue(it) }
    allOrNone = inputHolder.inputValue<Boolean>(id.allOrNone).value
    advancedOrder = inputHolder.inputValue<Boolean>(id.advancedOrder).value
    inputHolder.inputValue<DropDownOption>(id.advancedOrderType).value?.id?.let {
        advancedTradeType = id.advancedOrderTypeValueId.converter.toValue(it)
    }
}

private fun String.extractLookupSymbolInfo(snapshot: InputValuesSnapshot, symbolCoder: ValueCoder<WithSymbolInfo>): WithSymbolInfo? {
    return snapshot[this]?.let { symbolCoder.decode(it) }
}
