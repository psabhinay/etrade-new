package com.etrade.mobilepro.chart.netassets;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u001a\u0012\u0004\u0012\u00020\u0002\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00030\u0001\u00a8\u0006\u0006"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/GetNetAssetsChartDataEntriesUseCase;", "Lcom/etrade/mobilepro/util/UseCase;", "Lcom/etrade/mobilepro/chart/netassets/GetNetAssetsChartDataEntriesParameter;", "Lcom/etrade/mobilepro/util/Resource;", "", "Lcom/etrade/mobilepro/barchartview/DataEntry;", "netassets_debug"})
public abstract interface GetNetAssetsChartDataEntriesUseCase extends com.etrade.mobilepro.util.UseCase<com.etrade.mobilepro.chart.netassets.GetNetAssetsChartDataEntriesParameter, com.etrade.mobilepro.util.Resource<? extends java.util.List<? extends com.etrade.mobilepro.barchartview.DataEntry>>> {
}