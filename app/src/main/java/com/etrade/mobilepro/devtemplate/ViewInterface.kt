package com.etrade.mobilepro.devtemplate

import androidx.lifecycle.ViewModel

interface ViewInterface<VM> where VM : ViewModel, VM : ViewModelInterface {
    fun onViewStateLoading(loadingViewState: ViewModelInterface.ViewState.Loading)
    fun onViewStateError(errorViewState: ViewModelInterface.ViewState.Error)
    fun onViewStateSuccess(successViewState: ViewModelInterface.ViewState.Success)
}
