package com.etrade.mobilepro.util.android.extension

import android.content.Intent
import android.net.Uri

fun Intent.toDialAction(tel: String? = null): Intent = apply {
    action = Intent.ACTION_DIAL

    tel?.also {
        data = Uri.parse("tel:$it")
    }
}
