package com.etrade.mobilepro.transfermoney.data.repo

import com.etrade.mobilepro.caching.coroutine.DataSource
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.transfermoney.api.Holidays
import com.etrade.mobilepro.transfermoney.api.TransferCalendarHolidays
import com.etrade.mobilepro.transfermoney.data.rest.TransferCalendarResponseDto
import com.etrade.mobilepro.transfermoney.data.rest.TransferCalendarService
import javax.inject.Inject

class HolidaysDataSource @Inject constructor(
    private val service: TransferCalendarService
) : DataSource<Holidays, String> {

    override suspend fun fetch(id: String): ETResult<Holidays> {
        return runCatchingET {
            service.getCalendar()
                .doIfSuccessfulOrThrow(::convertDtoToModel)
        }
    }

    private fun convertDtoToModel(dto: TransferCalendarResponseDto): List<TransferCalendarHolidays> = dto
        .calendar
        .holidays
        .map {
            TransferCalendarHolidays(
                fromInstType = it.fromInstType,
                toInstType = it.toInstType,
                holidays = it.holidays
            )
        }
}
