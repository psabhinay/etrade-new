package com.etrade.mobilepro.balance.api

import java.math.BigDecimal

interface AccountBalance {
    val isPatternDayTrader: Boolean
    val accountValue: BigDecimal?
    val buyingPower: BigDecimal?
    val cashAvailableForInvestment: BigDecimal?
    val totalAvailableForWithdrawal: BigDecimal?
    val availableExcessEquity: BigDecimal?
    val intradayMargin: BigDecimal?
    val intradayNonMargin: BigDecimal?
}
