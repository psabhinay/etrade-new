package com.etrade.mobilepro.mod

import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull

/**
 * This class is responsible for constructing the URL of the MOD service call. This is a work-around required due to limitations
 * in Retrofit handling of URLs and query parameters. [HttpUrl] construction unconditionally escapes characters like "=" that are required for the Base64
 * encoded auth token passed in the URL as a query parameter to MOD.
 */
class MarketOnDemandUrlBuilder(
    private val modBaseUrl: String
) {

    /**
     * Constructs the URL for MOD services, e.g.
     * https://etrade.wallst.com/Research/API/Mobile/CompanyLookup/json?symbol=AAPL&AUTH_TOKEN
     * https://etrade.wallst.com/Research/API/Mobile/ReportSearch/json?symbol=AAAAX&AUTH_TOKEN
     */
    fun constructUrl(pathUrl: String, symbol: String, authToken: String): HttpUrl {
        return modBaseUrl.toHttpUrlOrNull()?.newBuilder()?.apply {
            addPathSegments(pathUrl)
            addQueryParameter("symbol", symbol)
        }?.build()?.toUrl()?.toExternalForm()?.let { urlWithoutToken ->
            "$urlWithoutToken&$authToken".toHttpUrlOrNull()
        } ?: throw IllegalStateException("Market on Demand URL could not be constructed")
    }
}
