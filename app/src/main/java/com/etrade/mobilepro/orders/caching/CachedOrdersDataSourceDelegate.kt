package com.etrade.mobilepro.orders.caching

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.dynamicui.CachedResponseModel
import com.etrade.mobilepro.orders.list.api.OrderParamsLoadSizeAndKey
import com.etrade.mobilepro.orders.list.generateStockPlanOrdersRequestBody
import com.etrade.mobilepro.orders.list.getOrderRequest
import com.etrade.mobilepro.orders.list.rest.GET_ESP_ORDERS_URL
import com.etrade.mobilepro.orders.list.rest.GET_ORDERS_URL
import com.etrade.mobilepro.orders.list.rest.GET_SAVED_ORDERS_URL
import com.etrade.mobilepro.orders.list.rest.OrdersListApiClient
import io.reactivex.Single
import io.realm.Realm
import javax.inject.Inject

class CachedOrdersDataSourceDelegate @Inject constructor(
    private val ordersListApiClient: OrdersListApiClient
) : SimpleCachedResource.DataSourceDelegate<CachedResponseModel, OrderParamsLoadSizeAndKey> {

    override fun fetchFromNetwork(id: OrderParamsLoadSizeAndKey): Single<CachedResponseModel> {
        if (id.orderParams.isStockPlan) {
            val request = generateStockPlanOrdersRequestBody(id.orderParams)
            return ordersListApiClient.getEmployeeStockPlanOrders(request).map {
                createCachedModel(id, it)
            }
        } else {
            val request = getOrderRequest(id.orderParams, id.loadSize, id.key ?: "0")
            val single = if (id.isSaved) {
                ordersListApiClient.getSavedOrders(request)
            } else {
                ordersListApiClient.getOrders(request)
            }

            return single.map {
                createCachedModel(id, it)
            }
        }
    }

    override fun putToCache(fetchedResult: CachedResponseModel): Single<CachedResponseModel> = Single.fromCallable {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { realmTxn ->
                realmTxn.insertOrUpdate(fetchedResult)
            }
        }
        fetchedResult
    }

    override fun fetchFromDb(id: OrderParamsLoadSizeAndKey): Single<CachedResponseModel> = Single.fromCallable {
        Realm.getDefaultInstance().use { realm ->
            val cachedResponse = realm
                .where(CachedResponseModel::class.java)
                .equalTo("id", id.getQueryStringForCache(id.url))
                .findFirst() ?: throw NoSuchElementException()
            realm.copyFromRealm(cachedResponse)
        }
    }

    private fun createCachedModel(id: OrderParamsLoadSizeAndKey, response: String) =
        CachedResponseModel(
            id = id.getQueryStringForCache(id.url),
            retrievedTimestamp = System.currentTimeMillis(),
            response = response
        )

    private val OrderParamsLoadSizeAndKey.url: String
        get() = if (orderParams.isStockPlan) {
            GET_ESP_ORDERS_URL
        } else {
            if (isSaved) {
                GET_SAVED_ORDERS_URL
            } else {
                GET_ORDERS_URL
            }
        }
}
