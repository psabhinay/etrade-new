package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OrderConfirmCancelResponseDto(
    @Json(name = "cancelOrder")
    val cancelOrder: CancelOrderResponse?
) : BaseDataDto()

@JsonClass(generateAdapter = true)
data class CancelOrderResponse(
    @Json(name = "orderId")
    val orderId: String
)
