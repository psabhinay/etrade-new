package com.etrade.mobilepro.menu.builder

import android.net.Uri
import androidx.annotation.DrawableRes
import androidx.navigation.NavDirections
import com.etrade.mobilepro.menu.NewMenuItemValidator
import com.etrade.mobilepro.menu.TextMenuItem
import com.etrade.mobilepro.viewmodel.ActivitySignal

class TextMenuItemBuilder(val newMenuItemValidator: NewMenuItemValidator? = null) {
    var clickable: Boolean? = null
    var contentDescription: String? = null
    var type: TextMenuItem.Type? = null

    @DrawableRes
    var icon: Int? = null
    var id: Int? = null
    var localyticsTag: String? = null
    var menuActions: List<TextMenuItem.MenuAction> = mutableListOf()
    var shouldSaveDirections: Boolean = true
    var subtext: String? = null
    var showNewPill: Boolean = false
    var showRightIcon: Boolean = false
    var text: String? = null

    fun action(action: ActivitySignal) {
        menuActions = menuActions + TextMenuItem.MenuAction.MenuActivityAction(action)
    }

    fun navDirections(navDirections: NavDirections) {
        if (id == null) id = navDirections.actionId
        menuActions = menuActions + TextMenuItem.MenuAction.MenuNavDirectionsAction(navDirections)
    }

    fun appAction(appAction: Uri) {
        menuActions = menuActions + TextMenuItem.MenuAction.MenuAppAction(appAction)
    }

    fun appAction(appAction: String) {
        appAction(Uri.parse(appAction))
    }

    /**
     * Checks whether the menu item with the provided key should show a "New" pill. If so, it
     * adds the new pill and an action to increment the click count.
     *
     * *NOTE* if this method is used, then you must pass a [NewMenuItemValidator] to the
     * [menuPage] builder.
     */
    fun newMenuItemKey(newMenuItemKey: String?) {
        if (newMenuItemValidator?.isNewMenuItem(newMenuItemKey) == true) {
            showNewPill = true
            action { newMenuItemValidator.incrementClickCount(newMenuItemKey) }
        }
    }

    fun build(): TextMenuItem {
        return TextMenuItem(
            clickable = clickable ?: menuActions.isNotEmpty(),
            contentDescription = contentDescription ?: "${text ?: ""} ${subtext ?: ""}",
            icon = icon,
            id = id,
            localyticsTag = localyticsTag,
            shouldSaveDirections = shouldSaveDirections,
            subtext = subtext,
            showNewPill = showNewPill,
            showRightIcon = showRightIcon,
            menuActions = menuActions,
            text = text,
            type = type
        )
    }
}
