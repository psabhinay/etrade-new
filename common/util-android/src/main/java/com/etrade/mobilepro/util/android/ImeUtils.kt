package com.etrade.mobilepro.util.android

import android.app.Activity
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.etrade.mobilepro.util.android.extension.clearFocusSafely
import com.etrade.mobilepro.util.android.extension.hideSoftKeyboard
import com.etrade.mobilepro.util.android.extension.showSoftKeyboard

fun Activity.hideSoftKeyboard() = currentFocus?.hideSoftKeyboard()

fun Activity.showSoftKeyboard() = currentFocus?.showSoftKeyboard()

class ClearFocusOnImeActionDone : TextView.OnEditorActionListener {
    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            v?.apply {
                hideSoftKeyboard()
                clearFocusSafely()
            }
            return true
        }
        return false
    }
}
