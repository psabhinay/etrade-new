package com.etrade.mobilepro.billpay.presentation.managepayee

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.util.formatters.formatPayeeName
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(ManagePayeeViewModel::class.java)

class ManagePayeeViewModel @AssistedInject constructor(
    @Assisted private val payees: List<Payee>
) : ViewModel() {

    private val _viewState = MutableLiveData<ViewState>()
    internal val viewState: LiveData<ViewState> get() = _viewState

    @AssistedFactory
    interface Factory {
        fun create(
            payees: List<Payee>
        ): ManagePayeeViewModel
    }

    init { _viewState.value = ViewState(items = sortPayees(payees)) }

    private fun formatPayee(payee: Payee) = formatPayeeName(payee.name, payee.accountNumber, payee.nickName) ?: payee.name

    private fun sortPayees(payeeList: List<Payee>) =
        payeeList.sortedWith(
            Comparator { first, second ->
                formatPayee(first).compareTo(formatPayee(second), ignoreCase = true)
            }
        )

    internal fun setPayees(payees: List<Payee>) {
        _viewState.updateValue { it.copy(items = sortPayees(payees)) }
    }

    internal data class ViewState(
        val items: List<Payee>? = null
    )
}
