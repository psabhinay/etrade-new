package com.etrade.mobilepro.ui.multitoggle

import android.content.res.Resources
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityNodeInfo
import androidx.core.view.AccessibilityDelegateCompat
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat

internal class ToggleViewAccessibilityDelegate(
    private val resources: Resources,
    private val allowDeselection: Boolean
) : AccessibilityDelegateCompat() {
    override fun onInitializeAccessibilityNodeInfo(
        host: View?,
        info: AccessibilityNodeInfoCompat?
    ) {
        super.onInitializeAccessibilityNodeInfo(host, info)
        (host as? ToggleView)?.let {
            val toggleDescription =
                mapToggleAccessibility(resources, it.toggle.contentDescription)
            host.contentDescription = toggleDescription

            val isSelected = it.toggle.isSelected
            val skipActionTalkBack = isSelected && !allowDeselection
            host.isClickable = !skipActionTalkBack
            if (!skipActionTalkBack) {
                val customAction =
                    AccessibilityNodeInfoCompat.AccessibilityActionCompat(
                        AccessibilityNodeInfo.ACTION_CLICK,
                        mapToggleSelection(resources, isSelected)
                    )
                info?.addAction(customAction)
            }
        }
    }

    override fun sendAccessibilityEvent(host: View, eventType: Int) {
        if (eventType != AccessibilityEvent.TYPE_VIEW_SELECTED) {
            super.sendAccessibilityEvent(host, eventType)
        }
    }

    override fun onRequestSendAccessibilityEvent(
        host: ViewGroup,
        child: View,
        event: AccessibilityEvent
    ): Boolean {
        return if (event.eventType == AccessibilityEvent.TYPE_VIEW_SELECTED) {
            false
        } else {
            super.onRequestSendAccessibilityEvent(host, child, event)
        }
    }
}

private fun mapToggleSelection(res: Resources, isSelected: Boolean?): String {
    val resId: Int = if (isSelected == true) {
        R.string.deactivate
    } else {
        R.string.activate
    }
    return res.getString(resId)
}
