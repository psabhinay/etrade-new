package com.etrade.mobilepro.quote.screen.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemDisclosureBinding
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.util.android.accessibility.bindButtonAccessibility

class DisclosureViewHolder(private val binding: QuoteItemDisclosureBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bindTo(
        item: QuoteItem.QuoteDisclosureItem?,
        disclosureClickListener: (() -> Unit)? = null
    ) {
        item?.let {
            binding.disclosureTitle.bindButtonAccessibility()
            binding.disclosureTitle.setOnClickListener {
                disclosureClickListener?.invoke()
            }
        }
    }
}
