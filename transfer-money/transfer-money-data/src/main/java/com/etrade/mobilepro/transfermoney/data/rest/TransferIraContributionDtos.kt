package com.etrade.mobilepro.transfermoney.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class TransferIraContributionResponseDto(
    @Json(name = "IRAContribInfo")
    val infoDto: IraContribInfoDto?
)

@JsonClass(generateAdapter = true)
class IraContribInfoDto(
    @Json(name = "contribYear")
    val year: Int,
    @Json(name = "accountId")
    val accountId: String?,
    @Json(name = "accountIdKey")
    val accountIdKey: String?,
    @Json(name = "CurrentYearContrib")
    val currentYearContribDto: YearContribDto?,
    @Json(name = "PreviousYearContrib")
    val previousYearContribDto: YearContribDto?,
    @Json(name = "EligibleContribTaxYears")
    val eligibleContribTaxYearsDto: EligibleContribTaxYearsDto?
)

@JsonClass(generateAdapter = true)
class YearContribDto(
    @Json(name = "contributedAmount")
    val amount: String?,
    @Json(name = "contribMaxLimit")
    val maxLimit: String?,
    @Json(name = "contribAmountLeft")
    val amountLeft: String?,
    @Json(name = "taxYear")
    val taxYear: Int,
    @Json(name = "taxDueDate")
    val taxDueDate: Long
)

@JsonClass(generateAdapter = true)
class EligibleContribTaxYearsDto(
    @Json(name = "contribTaxYear1")
    val taxYear1: Int,
    @Json(name = "contribTaxYear2")
    val taxYear2: Int
)
