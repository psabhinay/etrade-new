package com.etrade.mobilepro.login.usecase

import com.etrade.mobilepro.login.api.repo.LoginResult
import com.etrade.mobilepro.util.UseCase

interface LoginResultUseCase : UseCase<LoginResultParameter, Unit>

data class LoginResultParameter(
    val result: LoginResult,
    val request: LoginRequest
)
