package com.etrade.mobilepro.news

import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.news.api.filter.FilterDataSource
import com.etrade.mobilepro.news.api.filter.FilterDataSourceWithForcedUpdate
import com.etrade.mobilepro.news.presentation.filter.portfolio.AccountToPortfolioFilterMapper
import com.etrade.mobilepro.news.presentation.filter.portfolio.PortfolioFilterDataSource
import com.etrade.mobilepro.news.presentation.filter.portfolio.PortfolioFilterItem
import com.etrade.mobilepro.news.presentation.filter.watchlist.WatchlistFilterDataSource
import com.etrade.mobilepro.watchlistapi.Watchlist
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class NewsFilterModule {
    @Binds
    abstract fun bindPortfolioFilterDataSource(impl: PortfolioFilterDataSource): FilterDataSource<PortfolioFilterItem>

    @Binds
    abstract fun bindWatchlistFilterDataSource(impl: WatchlistFilterDataSource): FilterDataSource<Watchlist>

    @Binds
    abstract fun bindWatchlistFilterDataSourceForced(impl: WatchlistFilterDataSource): FilterDataSourceWithForcedUpdate<Watchlist>

    companion object {
        @Provides
        fun bindAccountPresentationMapper(
            impl: AccountToPortfolioFilterMapper
        ): (List<@JvmSuppressWildcards Account>) -> List<@JvmSuppressWildcards PortfolioFilterItem> =
            impl::map
    }
}
