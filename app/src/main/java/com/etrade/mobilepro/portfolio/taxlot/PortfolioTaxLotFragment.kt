package com.etrade.mobilepro.portfolio.taxlot

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.etrade.eo.corelibandroid.createViewModel
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.databinding.FragmentTaxLotListBinding
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.orders.api.OrderAction
import com.etrade.mobilepro.portfolio.taxlot.PortfolioTaxLotRecordsViewModel.ViewEffects.Error
import com.etrade.mobilepro.portfolio.taxlot.PortfolioTaxLotRecordsViewModel.ViewEffects.Loading
import com.etrade.mobilepro.portfolio.taxlot.PortfolioTaxLotRecordsViewModel.ViewEffects.Success
import com.etrade.mobilepro.positions.api.isShort
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.util.android.binding.dataBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.LifecycleObserverViewDelegate
import com.etrade.mobilepro.viewdelegate.ViewDelegate
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@RequireLogin
internal class PortfolioTaxLotFragment @Inject constructor(
    snackbarUtilFactory: SnackbarUtilFactory,
    viewModelFactory: ViewModelProvider.Factory,
    taxLotsViewModelFactory: PortfolioTaxLotRecordsViewModel.Factory
) : Fragment(R.layout.fragment_tax_lot_list) {

    private val lifeCycleViewDelegate: ViewDelegate by lazy {
        LifecycleObserverViewDelegate(listOf(quoteWidgetViewModel))
    }

    private val viewModel: PortfolioTaxLotRecordsViewModel by createViewModel {
        taxLotsViewModelFactory.create(
            accountUuid = args.accountUuid,
            symbol = args.symbol,
            positionId = args.positionId,
            portfolioTableViewType = args.tableViewType,
            positionTableMode = args.tableMode
        )
    }

    private val binding by dataBinding(FragmentTaxLotListBinding::bind)
    private val quoteWidgetViewModel: QuotesWidgetViewModel by viewModels { viewModelFactory }
    private val args: PortfolioTaxLotFragmentArgs by navArgs()
    private var snackBar: Snackbar? = null
    private val viewAdapter: TaxLotTableViewAdapter
        get() = binding.taxLotsTableView.adapter as TaxLotTableViewAdapter
    private val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViewModels()
        binding.taxLotsTableView.adapter = TaxLotTableViewAdapter(requireContext())
        binding.swipeRefreshView.apply {
            applyColorScheme()
            setOnRefreshListener {
                viewModel.refreshTaxLots()
            }
        }

        if (savedInstanceState == null) {
            quoteWidgetViewModel.getQuotes(args.symbol)
        }

        lifeCycleViewDelegate.observe(viewLifecycleOwner)
    }

    override fun onResume() {
        super.onResume()
        viewModel.resumeStreaming()
    }

    override fun onPause() {
        super.onPause()
        viewModel.pauseStreaming()
    }

    @Suppress("LongMethod")
    private fun setUpViewModels() {
        binding.quoteObj = quoteWidgetViewModel
        binding.taxLotsObj = viewModel
        viewModel.viewEffects.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is Error -> handleError()
                    Loading -> handleLoading()
                    Success -> {
                        binding.taxLotsTableView.visibility = View.VISIBLE
                        handleComplete()
                    }
                }
            }
        )

        viewModel.sortDelegate.sortInfo.observe(
            viewLifecycleOwner,
            Observer { viewModel.sortDelegate.sortColumn(it) }
        )

        viewModel.clickEvents.observe(
            viewLifecycleOwner,
            Observer {
                it as CtaEvent.MessageEvent
                val position = it.message.arg1
                val record = viewModel.peekRecord(position) ?: return@Observer
                val orderAction: OrderAction = OrderAction.fromBoolean(record.position.isShort)
                findNavController().navigate(viewModel.getTradeDirections(record, orderAction))
            }
        )
    }

    private fun handleLoading() {
        if (!binding.swipeRefreshView.isRefreshing) {
            binding.loadingIndicator.show()
        }
        snackBar?.dismiss()
    }

    private fun handleError() {
        handleComplete()
        binding.taxLotsTableView.visibility = View.GONE
        snackBar = snackbarUtil.retrySnackbar(dismissOnPause = true) {
            viewModel.refreshTaxLots()
        }
    }

    private fun handleComplete() {
        binding.loadingIndicator.hide()
        snackBar?.dismiss()
        binding.swipeRefreshView.isRefreshing = false
    }
}
