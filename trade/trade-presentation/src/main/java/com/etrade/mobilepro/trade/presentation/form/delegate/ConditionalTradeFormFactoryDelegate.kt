package com.etrade.mobilepro.trade.presentation.form.delegate

import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.api.ExternalInput
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.form
import com.etrade.mobilepro.dynamic.form.api.inputParameterValue
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.displaySymbol
import com.etrade.mobilepro.instrument.isOption
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.form.api.getTransactionType
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ConditionalTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractAction
import com.etrade.mobilepro.trade.form.api.input.extractQuantity
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.TradeConditionalOrderDisclosureViewModel
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.AccountSectionGroup
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.accountSectionGroup
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.liveForm
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.saveAndPreview
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.securityType
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter
import com.etrade.mobilepro.trade.presentation.util.formatLegDetails
import com.etrade.mobilepro.util.ValueCoder
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume

private const val ORDER_NUMBER_1 = 1
private const val ORDER_NUMBER_2 = 2
private const val ORDER_NUMBER_3 = 3

internal class ConditionalTradeFormFactoryDelegate(
    private val resources: Resources,
    private val inputId: ConditionalTradeFormInputId,
    private val conditionalOrderTradeFormInputId: ConditionalOrderTradeFormInputId
) : TradeFormFactoryDelegate {

    private val securityType: SecurityType = SecurityType.CONDITIONAL

    @Suppress("LongMethod")
    override fun createForm(
        accounts: List<TradeAccount>,
        accountDelegate: SelectedAccountDelegate,
        parameter: CreateTradeFormParameter,
        updateForm: TradeFormFactoryDelegate.OnFormChangeListener
    ): Form {
        return form {

            accountSectionGroup(
                resources, inputId,
                AccountSectionGroup(
                    accounts,
                    accountDelegate.getCurrentAccountId(securityType), parameter.balanceViewModel
                )
            )

            val haveInitialOrderOne = parameter.defaultValues[inputId.order1] != null
            val firstFormOpen: MutableLiveData<Boolean> = MutableLiveData(haveInitialOrderOne)

            val dependency1 = OrderInputDependency(ORDER_NUMBER_1, R.string.trade_hint_order_1, inputId.order1, inputId.order2, firstFormOpen)
            val dependency2 = OrderInputDependency(ORDER_NUMBER_2, R.string.trade_hint_order_2, inputId.order2, inputId.order3, firstFormOpen)
            val dependency3 = OrderInputDependency(ORDER_NUMBER_3, R.string.trade_hint_order_3, inputId.order3, firstFormOpen = firstFormOpen)

            verticalGroup {
                securityType(resources, inputId, securityType, parameter.walkthroughViewModel.state)

                val onActionClickListener = parameter.onActionClickListener
                val orderInputValueCoder = createOrderValueCoder()
                val orderInputValueFormatter = createOrderValueFormatter(resources)

                order(resources, dependency1, onActionClickListener, orderInputValueCoder, orderInputValueFormatter)
                order(resources, dependency2, onActionClickListener, orderInputValueCoder, orderInputValueFormatter)
                order(resources, dependency3, onActionClickListener, orderInputValueCoder, orderInputValueFormatter)

                staticText {
                    text = resources.getString(R.string.trade_hint_conditional)
                }
            }

            saveAndPreview(resources, parameter, setOf(inputId.accountId, inputId.order1, inputId.order2))

            rules {
                liveForm(inputId, securityType, updateForm, parameter.conditionalOrderDisclosureViewModel, accountDelegate)
                enableInput(dependency1)
                enableInput(dependency2)
                moveValue(dependency1)
                moveValue(dependency2)
                resetSecurityTypeIfDisclosureRejected(inputId, parameter.conditionalOrderDisclosureViewModel)
                openFirstOrderPageIfPreFilledWhenFormCreated(firstFormOpen)
            }
        }
    }

    private fun RuleListBuilder.openFirstOrderPageIfPreFilledWhenFormCreated(firstFormOpen: LiveData<Boolean>) {
        rule {
            val input = requireInput<ExternalInput<InputValuesSnapshot>>(inputId.order1)
            input.value.observeForever { snapshot ->
                if (firstFormOpen.value == true && snapshot != null) {
                    input.action.invoke()
                }
            }
        }
    }

    private fun GroupBuilder.order(
        resources: Resources,
        dependency: OrderInputDependency,
        onActionClickListener: OnActionClickListener,
        orderItemValueCoder: ValueCoder<InputValuesSnapshot>,
        orderItemValueFormatter: (InputValuesSnapshot) -> CharSequence
    ) {
        val selectInputId = dependency.inputId
        externalSelect(selectInputId, orderItemValueCoder) {
            title = resources.getString(dependency.titleResId)
            showClearAction = true
            valueFormatter = orderItemValueFormatter
            action = {

                val snapshot = inputValue<InputValuesSnapshot>(selectInputId).value
                val accountId = inputParameterValue(inputId.accountId).value as? String
                val action = TradeFormAction.ConditionalOrder(selectInputId, dependency.orderNumber, accountId, snapshot)
                onActionClickListener.onActionClick(action)
                if (dependency.firstFormOpen.value == true) {
                    requireInput<ExternalInput<InputValuesSnapshot>>(selectInputId).setValue(null)
                    dependency.firstFormOpen.value = false
                }
            }
            dependency.nextInputId?.let { nextInputId ->
                doBeforeReset = {
                    requireAnyInput(nextInputId).resetValue()
                }
            }
        }
    }

    private fun createOrderValueFormatter(resources: Resources): (InputValuesSnapshot) -> CharSequence = {
        resources.formatOrder(conditionalOrderTradeFormInputId, it)
    }
}

private fun Resources.formatOrder(inputId: ConditionalOrderTradeFormInputId, snapshot: InputValuesSnapshot): CharSequence {
    val action = inputId.extractAction(snapshot) ?: return ""
    val symbolInfo = inputId.extractSymbolInfo(snapshot)
    val symbol = symbolInfo?.displaySymbol ?: return ""
    return formatLegDetails(action.getTransactionType(symbolInfo.isOption), symbol, inputId.extractQuantity(snapshot))
}

private fun RuleListBuilder.enableInput(dependency: OrderInputDependency) {
    val dependentInputId = dependency.nextInputId ?: return
    rule {
        requireAnyInput(dependency.inputId).hasValue.observeForever {
            requireAnyInput(dependentInputId).allowToBeEnabled(it)
        }
    }
}

private fun RuleListBuilder.resetSecurityTypeIfDisclosureRejected(
    inputId: TradeFormInputId,
    disclosureViewModel: TradeConditionalOrderDisclosureViewModel
) {
    rule {
        disclosureViewModel.resetSecurityType.observeForever(object : Observer<ConsumableLiveEvent<SecurityType>> {
            override fun onChanged(event: ConsumableLiveEvent<SecurityType>?) {
                event?.consume { securityTypeToSet ->
                    disclosureViewModel.resetSecurityType.removeObserver(this)
                    val value = inputId.securityTypeValueId.converter.toKey(securityTypeToSet)
                    requireInput<SelectInput>(inputId.securityType).setParameterValue(value)
                }
            }
        })
    }
}

private fun RuleListBuilder.moveValue(dependency: OrderInputDependency) {
    val originInputId = dependency.nextInputId ?: return
    rule {
        val destInput = requireAnyInput(dependency.inputId)
        val originInput = requireAnyInput(originInputId)

        destInput.hasValue.observeForever { hasValue ->
            if (!hasValue) {
                originInput.value.value?.let { nextValue ->
                    destInput.setValue(nextValue)
                    originInput.clearValue()
                }
            }
        }
    }
}

private fun createOrderValueCoder(): ValueCoder<InputValuesSnapshot> {
    return object : ValueCoder<InputValuesSnapshot> {
        override fun encode(value: InputValuesSnapshot): Any = value

        @Suppress("UNCHECKED_CAST")
        override fun decode(encoded: Any): InputValuesSnapshot? = encoded as? InputValuesSnapshot
    }
}

private class OrderInputDependency(
    val orderNumber: Int,
    @StringRes val titleResId: Int,
    val inputId: String,
    val nextInputId: String? = null,
    val firstFormOpen: MutableLiveData<Boolean>
)
