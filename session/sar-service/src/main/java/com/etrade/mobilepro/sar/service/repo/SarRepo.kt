package com.etrade.mobilepro.sar.service.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.sar.service.SarMessage

interface SarRepo {
    suspend fun checkSar(): ETResult<SarMessage>
}
