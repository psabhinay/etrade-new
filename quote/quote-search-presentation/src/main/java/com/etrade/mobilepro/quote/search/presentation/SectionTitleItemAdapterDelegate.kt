package com.etrade.mobilepro.quote.search.presentation

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.search.presentation.databinding.QuoteItemSearchResultSectionTitleBinding
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class SectionTitleItemAdapterDelegate :
    AbsListItemAdapterDelegate<SearchResultItem.SectionTitle, SearchResultItem, SectionTitleItemAdapterDelegate.SectionTitleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): SectionTitleViewHolder {
        return SectionTitleViewHolder(parent.viewBinding(QuoteItemSearchResultSectionTitleBinding::inflate))
    }

    override fun isForViewType(item: SearchResultItem, items: MutableList<SearchResultItem>, position: Int): Boolean {
        return item is SearchResultItem.SectionTitle
    }

    override fun onBindViewHolder(item: SearchResultItem.SectionTitle, holder: SectionTitleViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }

    class SectionTitleViewHolder(private val binding: QuoteItemSearchResultSectionTitleBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: SearchResultItem.SectionTitle) {
            binding.sectionTitleView.setText(item.titleResId)
        }
    }
}
