package com.etrade.mobilepro.transfermoney.data.rest

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false, name = "QuickTransferResponse")
data class QuickTransferResponseDto(
    @field:Element(name = "isGDCEnabled", required = false)
    var isGDCEnabled: String? = null,
    @field:ElementList(name = "AccountList", required = false)
    var accounts: List<QuickTransferAccountDto>? = null
)

@Root(strict = false, name = "Account")
data class QuickTransferAccountDto(
    @field:Element(name = "DisplayName", required = false)
    var displayName: String? = null,
    @field:Element(name = "AcctId", required = false)
    var accountId: String? = null,
    @field:Element(name = "AcctType", required = false)
    var accountType: String? = null,
    @field:Element(name = "IsFromAcct", required = false)
    var isFromAccount: String? = null,
    @field:Element(name = "IsToAcct", required = false)
    var isToAccount: String? = null
)

@Root(strict = false, name = "QuickTransferActivityResponse")
data class QuickTransferActivityResponseDto(
    @field:ElementList(name = "TransferList", required = false)
    var transfers: List<QuickTransferDetailsDto>? = null
)

@Root(strict = false, name = "Transfer")
data class QuickTransferDetailsDto(
    @field:Element(name = "Status", required = false)
    var status: String? = null,
    @field:Element(name = "Date", required = false)
    var date: String? = null,
    @field:Element(name = "FromAcct", required = false)
    var fromAccountId: String? = null,
    @field:Element(name = "ToAcct", required = false)
    var toAccountId: String? = null,
    @field:Element(name = "FromAcctName", required = false)
    var fromAccount: String? = null,
    @field:Element(name = "ToAcctName", required = false)
    var toAccount: String? = null,
    @field:Element(name = "Amount", required = false)
    var amount: String? = null,
    @field:Element(name = "Frequency", required = false)
    var frequency: String? = null,
    @field:Element(name = "ReferenceNumber", required = false)
    var referenceNumber: String? = null
)
