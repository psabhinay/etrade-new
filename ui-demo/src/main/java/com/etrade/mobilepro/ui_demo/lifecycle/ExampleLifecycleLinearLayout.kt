package com.etrade.mobilepro.ui_demo.lifecycle

import android.content.Context
import android.util.AttributeSet
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.common.customview.LifecycleLinearLayout
import com.etrade.mobilepro.ui_demo.databinding.LayoutLifecycleItemBinding
import com.etrade.mobilepro.util.android.extension.layoutInflater

class ExampleLifecycleLinearLayout(context: Context, attrs: AttributeSet? = null) :
    LifecycleLinearLayout(context, attrs) {

    fun bindItems(items: List<LifecycleItemViewModel>) {
        removeAllViews()
        items.forEach {
            val binding = LayoutLifecycleItemBinding.inflate(layoutInflater, this, true)
            // We extend LifecycleLinearLayout so that we have access to the lifecycle here
            // If you comment out this line you will see that LiveData value changes are not observed.
            binding.lifecycleOwner = this
            binding.item = it
        }
    }
}

@BindingAdapter("items")
fun bindItems(layout: ExampleLifecycleLinearLayout, items: List<LifecycleItemViewModel>?) {
    layout.bindItems(items.orEmpty())
}
