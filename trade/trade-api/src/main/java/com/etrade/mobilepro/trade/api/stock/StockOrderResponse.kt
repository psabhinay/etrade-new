package com.etrade.mobilepro.trade.api.stock

import com.etrade.mobilepro.trade.api.OrderResponse

interface StockOrderResponse : OrderResponse {
    override val orders: List<StockOrder>
}

val StockOrderResponse.yourPrice: String?
    get() = orders.firstOrNull()?.yourPrice
