package com.etrade.mobilepro.billpay.data.repo

import com.etrade.mobilepro.billpay.api.MerchantInfo
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.PayeeAddress
import com.etrade.mobilepro.billpay.api.UsState
import com.etrade.mobilepro.billpay.data.rest.MerchantInfoDto
import com.etrade.mobilepro.billpay.data.rest.PayeeAddressDto
import com.etrade.mobilepro.billpay.data.rest.PayeeDetailDto
import com.etrade.mobilepro.billpay.data.rest.PayeeResponseDto
import com.etrade.mobilepro.billpay.data.rest.StateCodeResponseDto

private const val NULL_IN_STRING = "null"
internal fun PayeeResponseDto.toPayeeList(): List<Payee> {
    return payeesList
        ?.asSequence()
        ?.filter { it.payee.accountNumber != null }
        ?.map { it.payee.toApi() }
        ?.toList() ?: emptyList()
}

internal fun PayeeAddressDto?.toPayeeAddress(): PayeeAddress? = this?.let {
    PayeeAddress(
        address1,
        address2,
        city,
        state,
        zipCode
    )
}

internal fun MerchantInfoDto?.toPayeeMerchantInfo(): MerchantInfo? = this?.let {
    MerchantInfo(
        merchantId,
        merchantZipCode,
        merchantZipCode4
    )
}

fun StateCodeResponseDto.toAddressStateList(): List<UsState> {
    return stateList?.keys?.map {
        UsState(it, stateList[it])
    } ?: emptyList()
}

private fun PayeeDetailDto.toApi() = Payee(
    payeeId = payeeId,
    name = name,
    nickName = nickName?.takeUnless { it == NULL_IN_STRING },
    phoneNumber = phoneNumber,
    accountNumber = requireNotNull(accountNumber),
    leadDays = leadDays ?: -1,
    isAddressAvailable = isAddressAvailable,
    address = address.toPayeeAddress(),
    merchantInfo = merchantInfo.toPayeeMerchantInfo(),
    merchantZipRequired = merchantZipRequired,
    category = category
)
