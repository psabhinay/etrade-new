package com.etrade.mobilepro.news.presentation

import android.view.View
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.news.presentation.customview.NewsWidgetRecyclerView
import com.etrade.mobilepro.news.presentation.dynamic.NewsItemWidgetPayload
import com.etrade.mobilepro.util.android.goneUnless
import com.etrade.mobilepro.util.android.visibleIf

@BindingAdapter("news")
fun NewsWidgetRecyclerView.setNews(newsItems: List<NewsItemWidgetPayload>?) {
    newsItems?.let { setItems(it) }
}

@BindingAdapter("visibleIfLoading")
fun View.visibleIfLoading(viewState: DynamicUiViewState?) {
    visibleIf(viewState is DynamicUiViewState.Loading)
}

@BindingAdapter("showIfError")
fun View.showIfError(viewState: DynamicUiViewState?) {
    goneUnless(viewState is DynamicUiViewState.Error)
}
