package com.etrade.mobilepro.documents.data

import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.documents.StatementDocument
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class StatementDocumentsReferenceDto(
    override val data: List<StatementDocumentDto>
) : BaseReference<List<StatementDocument>>() {
    override fun convertToModel(): List<StatementDocument> = data.map {
        StatementDocument(
            it.documentId,
            it.institutionType,
            DOCUMENTS_DATE_FORMATTER_FOR_UI.format(DOCUMENTS_STATEMENTS_DATE_FORMATTER_FOR_SERVER.parse(it.title)),
            it.postEdgeFolder,
            it.accountUuid
        )
    }
}
