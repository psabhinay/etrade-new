package com.etrade.mobilepro.trade

import android.os.Bundle
import com.etrade.mobilepro.R
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity

class StopLossDisclosuresActivity : SpannableTextOverlayActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        intent.putExtra(EXTRA_TITLE, getString(R.string.stop_loss_disclosures_title))
        intent.putExtra(EXTRA_TEXT, getString(R.string.stop_loss_disclosures_full))
        super.onCreate(savedInstanceState)
    }
}
