package com.etrade.mobilepro.order.details.dto;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000-\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0003\b\u00a3\u0001\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B\u00d5\u0005\u0012\u0010\b\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u001d\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010 \u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010!\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010#\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010%\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010&\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010\'\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010(\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010)\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010*\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010+\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u0012\u0010\b\u0002\u0010,\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003\u00a2\u0006\u0002\u0010-J\u0012\u0010\u0080\u0001\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0081\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0082\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0083\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0084\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0085\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0086\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0087\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0088\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0089\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u008a\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u008b\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u008c\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u008d\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u008e\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u008f\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0090\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0091\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0092\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0093\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0094\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0095\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0096\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0097\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0098\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u0099\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u009a\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u009b\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u009c\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u009d\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u009e\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u009f\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u00a0\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u00a1\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u00a2\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u00a3\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u00a4\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u00a5\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u00a6\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u0012\u0010\u00a7\u0001\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0003J\u00da\u0005\u0010\u00a8\u0001\u001a\u00020\u00002\u0010\b\u0002\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\u0010\b\u0002\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u001d\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010 \u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010!\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010#\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010%\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010&\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010\'\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010(\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010)\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010*\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010+\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00032\u0010\b\u0002\u0010,\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0003H\u00c6\u0001J\u0016\u0010\u00a9\u0001\u001a\u00030\u00aa\u00012\t\u0010\u00ab\u0001\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u000b\u0010\u00ac\u0001\u001a\u00030\u00ad\u0001H\u00d6\u0001J\n\u0010\u00ae\u0001\u001a\u00020\u0006H\u00d6\u0001R&\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R&\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010/\"\u0004\b3\u00101R&\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010/\"\u0004\b5\u00101R&\u0010+\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b6\u0010/\"\u0004\b7\u00101R&\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b8\u0010/\"\u0004\b9\u00101R&\u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010/\"\u0004\b;\u00101R&\u0010#\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b<\u0010/\"\u0004\b=\u00101R&\u0010\u0014\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b>\u0010/\"\u0004\b?\u00101R&\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010/\"\u0004\bA\u00101R&\u0010\u001f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bB\u0010/\"\u0004\bC\u00101R&\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010/\"\u0004\bE\u00101R&\u0010%\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bF\u0010/\"\u0004\bG\u00101R&\u0010\u001d\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bH\u0010/\"\u0004\bI\u00101R&\u0010\'\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bJ\u0010/\"\u0004\bK\u00101R&\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bL\u0010/\"\u0004\bM\u00101R&\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bN\u0010/\"\u0004\bO\u00101R&\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bP\u0010/\"\u0004\bQ\u00101R&\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bR\u0010/\"\u0004\bS\u00101R&\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bT\u0010/\"\u0004\bU\u00101R&\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bV\u0010/\"\u0004\bW\u00101R&\u0010\b\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bX\u0010/\"\u0004\bY\u00101R&\u0010\u0007\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bZ\u0010/\"\u0004\b[\u00101R&\u0010)\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\\\u0010/\"\u0004\b]\u00101R&\u0010(\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b^\u0010/\"\u0004\b_\u00101R&\u0010&\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b`\u0010/\"\u0004\ba\u00101R&\u0010,\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bb\u0010/\"\u0004\bc\u00101R&\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bd\u0010/\"\u0004\be\u00101R&\u0010\u0017\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bf\u0010/\"\u0004\bg\u00101R&\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bh\u0010/\"\u0004\bi\u00101R&\u0010 \u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bj\u0010/\"\u0004\bk\u00101R&\u0010\r\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bl\u0010/\"\u0004\bm\u00101R&\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bn\u0010/\"\u0004\bo\u00101R&\u0010!\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bp\u0010/\"\u0004\bq\u00101R&\u0010\u001e\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\br\u0010/\"\u0004\bs\u00101R&\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bt\u0010/\"\u0004\bu\u00101R&\u0010$\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bv\u0010/\"\u0004\bw\u00101R&\u0010\u001a\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bx\u0010/\"\u0004\by\u00101R&\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bz\u0010/\"\u0004\b{\u00101R&\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b|\u0010/\"\u0004\b}\u00101R&\u0010*\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b~\u0010/\"\u0004\b\u007f\u00101\u00a8\u0006\u00af\u0001"}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/StockPlanOrderDetailsResponseDto;", "", "orderHistoryList", "", "Lcom/etrade/mobilepro/order/details/dto/OrderHistory;", "orderNumber", "", "orderType", "orderStatus", "description", "term", "gtdExpiration", "sharesExercised", "sharesSold", "sharesToExercise", "sharesToSell", "estSharesToSell", "priceType", "limitPrice", "grossProceeds", "estGrossProceeds", "totalPrice", "commission", "secFee", "brokerAssistFee", "disbursementFee", "taxesWithheld", "estNetProceeds", "netProceeds", "exerciseValue", "sharesToKeep", "estSharesToKeep", "sharesIssued", "sharesToIssue", "appreciationSharesValue", "estAppreciationSharesValue", "taxSharesValue", "estTaxSharesValue", "planPrice", "grantType", "planNumber", "planDate", "vestPeriod", "costBasis", "planType", "(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V", "getAppreciationSharesValue", "()Ljava/util/List;", "setAppreciationSharesValue", "(Ljava/util/List;)V", "getBrokerAssistFee", "setBrokerAssistFee", "getCommission", "setCommission", "getCostBasis", "setCostBasis", "getDescription", "setDescription", "getDisbursementFee", "setDisbursementFee", "getEstAppreciationSharesValue", "setEstAppreciationSharesValue", "getEstGrossProceeds", "setEstGrossProceeds", "getEstNetProceeds", "setEstNetProceeds", "getEstSharesToKeep", "setEstSharesToKeep", "getEstSharesToSell", "setEstSharesToSell", "getEstTaxSharesValue", "setEstTaxSharesValue", "getExerciseValue", "setExerciseValue", "getGrantType", "setGrantType", "getGrossProceeds", "setGrossProceeds", "getGtdExpiration", "setGtdExpiration", "getLimitPrice", "setLimitPrice", "getNetProceeds", "setNetProceeds", "getOrderHistoryList", "setOrderHistoryList", "getOrderNumber", "setOrderNumber", "getOrderStatus", "setOrderStatus", "getOrderType", "setOrderType", "getPlanDate", "setPlanDate", "getPlanNumber", "setPlanNumber", "getPlanPrice", "setPlanPrice", "getPlanType", "setPlanType", "getPriceType", "setPriceType", "getSecFee", "setSecFee", "getSharesExercised", "setSharesExercised", "getSharesIssued", "setSharesIssued", "getSharesSold", "setSharesSold", "getSharesToExercise", "setSharesToExercise", "getSharesToIssue", "setSharesToIssue", "getSharesToKeep", "setSharesToKeep", "getSharesToSell", "setSharesToSell", "getTaxSharesValue", "setTaxSharesValue", "getTaxesWithheld", "setTaxesWithheld", "getTerm", "setTerm", "getTotalPrice", "setTotalPrice", "getVestPeriod", "setVestPeriod", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component21", "component22", "component23", "component24", "component25", "component26", "component27", "component28", "component29", "component3", "component30", "component31", "component32", "component33", "component34", "component35", "component36", "component37", "component38", "component39", "component4", "component40", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "", "toString", "orders-details-data"})
@org.simpleframework.xml.Root(name = "OrderDetails", strict = false)
public final class StockPlanOrderDetailsResponseDto {
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(name = "orderhistorylist")
    private java.util.List<com.etrade.mobilepro.order.details.dto.OrderHistory> orderHistoryList;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "ordernumber", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> orderNumber;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "ordertype", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> orderType;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "orderstatus", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> orderStatus;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "Description", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> description;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "Term", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> term;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "gtdexpiration", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> gtdExpiration;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "sharesexercised", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> sharesExercised;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "sharessold", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> sharesSold;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "sharestoexercise", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> sharesToExercise;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "sharestosell", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> sharesToSell;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "estsharestosell", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> estSharesToSell;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "pricetype", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> priceType;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "limitprice", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> limitPrice;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "grossproceeds", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> grossProceeds;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "estgrossproceeds", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> estGrossProceeds;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "totalprice", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> totalPrice;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "commission", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> commission;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "secfee", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> secFee;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "brokerassistfee", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> brokerAssistFee;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "disbursementfee", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> disbursementFee;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "taxeswithheld", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> taxesWithheld;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "estnetproceeds", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> estNetProceeds;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "netproceeds", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> netProceeds;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "exercisevalue", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> exerciseValue;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "sharestokeep", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> sharesToKeep;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "estsharestokeep", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> estSharesToKeep;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "sharesissued", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> sharesIssued;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "sharestoissue", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> sharesToIssue;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "appreciationsharesvalue", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> appreciationSharesValue;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "estappreciationsharesvalue", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> estAppreciationSharesValue;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "taxsharesvalue", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> taxSharesValue;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "esttaxsharesvalue", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> estTaxSharesValue;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "planprice", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> planPrice;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "granttype", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> grantType;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "plannumber", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> planNumber;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "plandate", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> planDate;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "vestperiod", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> vestPeriod;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "costbasis", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> costBasis;
    @org.jetbrains.annotations.Nullable()
    @org.simpleframework.xml.ElementList(entry = "plantype", required = false, inline = true, type = java.lang.String.class)
    private java.util.List<java.lang.String> planType;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.StockPlanOrderDetailsResponseDto copy(@org.jetbrains.annotations.Nullable()
    java.util.List<com.etrade.mobilepro.order.details.dto.OrderHistory> orderHistoryList, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> orderNumber, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> orderType, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> orderStatus, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> description, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> term, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> gtdExpiration, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesExercised, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesSold, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesToExercise, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesToSell, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estSharesToSell, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> priceType, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> limitPrice, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> grossProceeds, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estGrossProceeds, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> totalPrice, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> commission, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> secFee, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> brokerAssistFee, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> disbursementFee, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> taxesWithheld, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estNetProceeds, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> netProceeds, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> exerciseValue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesToKeep, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estSharesToKeep, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesIssued, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesToIssue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> appreciationSharesValue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estAppreciationSharesValue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> taxSharesValue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estTaxSharesValue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> planPrice, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> grantType, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> planNumber, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> planDate, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> vestPeriod, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> costBasis, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> planType) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public StockPlanOrderDetailsResponseDto() {
        super();
    }
    
    public StockPlanOrderDetailsResponseDto(@org.jetbrains.annotations.Nullable()
    java.util.List<com.etrade.mobilepro.order.details.dto.OrderHistory> orderHistoryList, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> orderNumber, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> orderType, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> orderStatus, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> description, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> term, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> gtdExpiration, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesExercised, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesSold, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesToExercise, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesToSell, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estSharesToSell, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> priceType, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> limitPrice, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> grossProceeds, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estGrossProceeds, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> totalPrice, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> commission, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> secFee, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> brokerAssistFee, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> disbursementFee, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> taxesWithheld, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estNetProceeds, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> netProceeds, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> exerciseValue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesToKeep, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estSharesToKeep, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesIssued, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> sharesToIssue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> appreciationSharesValue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estAppreciationSharesValue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> taxSharesValue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> estTaxSharesValue, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> planPrice, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> grantType, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> planNumber, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> planDate, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> vestPeriod, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> costBasis, @org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> planType) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.etrade.mobilepro.order.details.dto.OrderHistory> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.etrade.mobilepro.order.details.dto.OrderHistory> getOrderHistoryList() {
        return null;
    }
    
    public final void setOrderHistoryList(@org.jetbrains.annotations.Nullable()
    java.util.List<com.etrade.mobilepro.order.details.dto.OrderHistory> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getOrderNumber() {
        return null;
    }
    
    public final void setOrderNumber(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getOrderType() {
        return null;
    }
    
    public final void setOrderType(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getOrderStatus() {
        return null;
    }
    
    public final void setOrderStatus(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getDescription() {
        return null;
    }
    
    public final void setDescription(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getTerm() {
        return null;
    }
    
    public final void setTerm(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getGtdExpiration() {
        return null;
    }
    
    public final void setGtdExpiration(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getSharesExercised() {
        return null;
    }
    
    public final void setSharesExercised(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getSharesSold() {
        return null;
    }
    
    public final void setSharesSold(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component10() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getSharesToExercise() {
        return null;
    }
    
    public final void setSharesToExercise(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component11() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getSharesToSell() {
        return null;
    }
    
    public final void setSharesToSell(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component12() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getEstSharesToSell() {
        return null;
    }
    
    public final void setEstSharesToSell(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component13() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getPriceType() {
        return null;
    }
    
    public final void setPriceType(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component14() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getLimitPrice() {
        return null;
    }
    
    public final void setLimitPrice(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component15() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getGrossProceeds() {
        return null;
    }
    
    public final void setGrossProceeds(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component16() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getEstGrossProceeds() {
        return null;
    }
    
    public final void setEstGrossProceeds(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component17() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getTotalPrice() {
        return null;
    }
    
    public final void setTotalPrice(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component18() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getCommission() {
        return null;
    }
    
    public final void setCommission(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component19() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getSecFee() {
        return null;
    }
    
    public final void setSecFee(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component20() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getBrokerAssistFee() {
        return null;
    }
    
    public final void setBrokerAssistFee(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component21() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getDisbursementFee() {
        return null;
    }
    
    public final void setDisbursementFee(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component22() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getTaxesWithheld() {
        return null;
    }
    
    public final void setTaxesWithheld(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component23() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getEstNetProceeds() {
        return null;
    }
    
    public final void setEstNetProceeds(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component24() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getNetProceeds() {
        return null;
    }
    
    public final void setNetProceeds(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component25() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getExerciseValue() {
        return null;
    }
    
    public final void setExerciseValue(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component26() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getSharesToKeep() {
        return null;
    }
    
    public final void setSharesToKeep(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component27() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getEstSharesToKeep() {
        return null;
    }
    
    public final void setEstSharesToKeep(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component28() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getSharesIssued() {
        return null;
    }
    
    public final void setSharesIssued(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component29() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getSharesToIssue() {
        return null;
    }
    
    public final void setSharesToIssue(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component30() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getAppreciationSharesValue() {
        return null;
    }
    
    public final void setAppreciationSharesValue(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component31() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getEstAppreciationSharesValue() {
        return null;
    }
    
    public final void setEstAppreciationSharesValue(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component32() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getTaxSharesValue() {
        return null;
    }
    
    public final void setTaxSharesValue(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component33() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getEstTaxSharesValue() {
        return null;
    }
    
    public final void setEstTaxSharesValue(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component34() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getPlanPrice() {
        return null;
    }
    
    public final void setPlanPrice(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component35() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getGrantType() {
        return null;
    }
    
    public final void setGrantType(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component36() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getPlanNumber() {
        return null;
    }
    
    public final void setPlanNumber(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component37() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getPlanDate() {
        return null;
    }
    
    public final void setPlanDate(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component38() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getVestPeriod() {
        return null;
    }
    
    public final void setVestPeriod(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component39() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getCostBasis() {
        return null;
    }
    
    public final void setCostBasis(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> component40() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<java.lang.String> getPlanType() {
        return null;
    }
    
    public final void setPlanType(@org.jetbrains.annotations.Nullable()
    java.util.List<java.lang.String> p0) {
    }
}