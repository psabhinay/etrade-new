package com.etrade.mobilepro.inboxmessages.dynamicui.databinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.imageloader.loadFromUrl
import com.etrade.mobilepro.imageloader.loadFromUrlAndSetBackgroundColor

@BindingAdapter("fundingPrompt")
internal fun ImageView.setFundingPromptImage(fundingPromptImage: String?) {
    fundingPromptImage?.let { this.loadFromUrlAndSetBackgroundColor(it) }
}

@BindingAdapter("icon")
internal fun ImageView.setIcon(url: String?) {
    url?.let { this.loadFromUrl(it) }
}
