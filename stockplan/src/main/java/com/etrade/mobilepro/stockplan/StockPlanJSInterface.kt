package com.etrade.mobilepro.stockplan

import android.webkit.JavascriptInterface
import com.etrade.mobilepro.order.details.router.StockPlanOrderDetailsArgument
import com.etrade.mobilepro.orders.api.OrderStatus
import org.json.JSONException
import org.json.JSONObject
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit

const val CORP_ID = "corpId"
const val DISCLOSURES = "Disclosures"
const val EXERCISE = "Exercise"
const val PAGE_TITLE = "page"
const val PREVIEW = "Preview"
const val SELL = "Sell"
const val STOCK_PLAN = "My Stock Plan"
const val SYMBOL = "symbol"
const val STOCK_PLAN_JS_INTERFACE_NAME = "StockPlanInterface"
const val JSBRIDGE_GO_BACK = "javascript:jsbridge.backBtnClicked();"
const val CONFIRMATION = "Confirmation"
const val ABOUT_BLANK = "about:blank"
val accountIndexRegex = Regex("[&?]accountIndex=(\\d+)")

const val ET_CORP_ID = 7

/**
 * todo fill in the methods as necessary with subsequent stories
 */
class StockPlanJSInterface(private val stockPlanHandler: StockPlanHandler) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @JavascriptInterface
    fun tradeButtonClicked(jsonString: String) {
        logger.debug("Trade button clicked: $jsonString")
        stockPlanHandler.tradeButtonClicked(jsonString)
    }

    @JavascriptInterface
    fun showOpenOrders(jsonString: String) {
        logger.debug("Show open orders: $jsonString")
    }

    @JavascriptInterface
    fun updatePageTitle(jsonString: String) {
        logger.debug("Update page title: $jsonString")
        try {
            val titleObj = JSONObject(jsonString)
            val title = titleObj.getString(PAGE_TITLE)
            stockPlanHandler.updatePageTitle(title)
        } catch (ex: JSONException) {
            logger.error("Error parsing update page title json", ex)
        }
    }

    @JavascriptInterface
    fun openNativeQuote(jsonString: String) {
        logger.debug("Open native quote: $jsonString")
        try {
            val symbolObj = JSONObject(jsonString)
            val symbol = symbolObj.getString(SYMBOL)
            stockPlanHandler.openNativeQuote(symbol)
        } catch (ex: JSONException) {
            logger.error("Error parsing open native quote json", ex)
        }
    }

    @JavascriptInterface
    fun openPopup(jsonString: String) {
        logger.debug("Open popup: $jsonString")
        try {
            val popUpObj = JSONObject(jsonString)
            when (popUpObj.getString(PAGE_TITLE)) {
                DISCLOSURES -> {
                    var corpId = 0
                    if (popUpObj.has(CORP_ID)) {
                        corpId = popUpObj.getInt(CORP_ID)
                    }
                    val disclosureTextId: Int
                    disclosureTextId = if (corpId == ET_CORP_ID) {
                        R.string.global_disclosure_text_ET_employee
                    } else {
                        R.string.global_disclosure_text
                    }
                    stockPlanHandler.openPopup(DISCLOSURES, disclosureTextId)
                }
            }
        } catch (ex: JSONException) {
            logger.error("Unable to parse jsonString for popup", ex)
        }
    }

    @JavascriptInterface
    fun showAlertDialog(message: String) {
        logger.debug("Show alert dialog: $message")
    }

    @JavascriptInterface
    fun openNativeCalendar(date: String) {
        logger.debug("Open native calendar: $date")
        try {
            val calendarObj = JSONObject(date)
            val calendarDate = TimeUnit.SECONDS.toMillis(calendarObj.getLong("date"))
            val eventName = calendarObj.getString("eventName")
            stockPlanHandler.openNativeCalendar(calendarDate, eventName)
        } catch (ex: JSONException) {
            logger.error("Unable to open native calendar", ex)
        }
    }

    @JavascriptInterface
    fun openPortfolio(jsonString: String) {
        logger.debug("Open portfolio: $jsonString")
        try {
            val accountObj = JSONObject(jsonString)
            val accountId = accountObj.getString("accountId")
            stockPlanHandler.navigateToAccount(accountId)
        } catch (ex: JSONException) {
            logger.error("Unable to open portfolio: $jsonString", ex)
        }
    }

    @JavascriptInterface
    fun updateTotalActions(jsonString: String) {
        logger.debug("Update total actions: $jsonString")
    }

    @JavascriptInterface
    fun updateQuoteTimestamp(jsonString: String) {
        logger.debug("Update quote timestamp: $jsonString")
    }

    @JavascriptInterface
    fun openURL(jsonString: String) {
        logger.debug("Open url: $jsonString")
        try {
            val urlObj = JSONObject(jsonString)
            val isAuthed = urlObj.getBoolean("authenticated")
            val url = urlObj.getString("url")
            if (isAuthed) {
                val isPdf = runCatching { urlObj.getBoolean("ispdf") }.getOrDefault(false)
                stockPlanHandler.openURL(url, isPdf)
            } else {
                stockPlanHandler.openURL(url, false)
            }
        } catch (ex: JSONException) {
            logger.error("Unable to open url: $jsonString", ex)
        }
    }

    @JavascriptInterface
    fun gotoStrummer(jsonString: String) {
        logger.debug("Go to strummer: $jsonString")
    }

    @JavascriptInterface
    fun gotoOrderDetails(jsonString: String) {
        logger.debug("Go to order details: $jsonString")
        runCatching {
            JSONObject(jsonString).let { obj ->
                StockPlanOrderDetailsArgument(
                    orderId = obj.getString("orderId"),
                    orderStatus = OrderStatus.UNKNOWN,
                    symbols = obj.getString("symbol").split(",")
                )
            }
        }.onSuccess {
            stockPlanHandler.openOrderDetails(it)
        }.onFailure {
            logger.error("Unable to open order details: $jsonString", it)
        }
    }
}
