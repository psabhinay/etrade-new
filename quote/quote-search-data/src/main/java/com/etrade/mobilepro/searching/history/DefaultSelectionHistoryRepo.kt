package com.etrade.mobilepro.searching.history

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.SettlementSessionCode
import com.etrade.mobilepro.searching.symbol.cache.SymbolDb
import com.etrade.mobilepro.searchingapi.SelectionHistoryRepo
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import io.realm.Realm
import io.realm.Sort
import javax.inject.Provider

class DefaultSelectionHistoryRepo(
    private val historySize: Long,
    private val historyRealmProvider: Provider<Realm>
) : SelectionHistoryRepo {

    override fun addToHistory(selectedItem: SearchResultItem) {
        when (selectedItem) {
            is SearchResultItem.Symbol -> addSymbolToSelectionHistory(selectedItem)
            else -> return
        }
    }

    override fun symbolSelectionHistory(): List<SearchResultItem.Symbol> {
        return historyRealmProvider.get().use { realm ->
            realm
                .where(SymbolDb::class.java)
                .findAll()
                .sort("cachedAt", Sort.DESCENDING)
                .map { fromDb ->
                    SearchResultItem.Symbol(
                        title = fromDb.title,
                        instrumentId = fromDb.instrumentId,
                        description = fromDb.description,
                        type = InstrumentType.valueOf(fromDb.type),
                        cachedAt = fromDb.cachedAt,
                        queryValue = fromDb.queryValue,
                        expire = fromDb.expire,
                        strike = fromDb.strike,
                        displaySymbol = fromDb.displaySymbol,
                        securityType = fromDb.securityType,
                        settlementSessionCd = SettlementSessionCode.from(fromDb.settlementSessionCd)
                    )
                }
        }
    }

    private fun addSymbolToSelectionHistory(symbol: SearchResultItem.Symbol) {
        val symbolToSave = SymbolDb(
            title = symbol.title,
            instrumentId = symbol.instrumentId,
            description = symbol.description,
            type = symbol.type.typeCode,
            cachedAt = System.currentTimeMillis(),
            queryValue = symbol.queryValue,
            expire = symbol.expire,
            strike = symbol.strike,
            displaySymbol = symbol.displaySymbol,
            securityType = symbol.securityType,
            settlementSessionCd = symbol.settlementSessionCd.code
        )

        historyRealmProvider.get().use { autoClosableRealm ->
            autoClosableRealm.executeTransaction { realm ->
                realm
                    .where(SymbolDb::class.java)
                    .equalTo("title", symbolToSave.title)
                    .findAll()
                    .deleteAllFromRealm()
                val existentList = realm
                    .where(SymbolDb::class.java)
                    .sort("cachedAt", Sort.DESCENDING)
                    .findAll()
                if (existentList.size >= historySize) {
                    existentList.deleteLastFromRealm()
                }
                realm.copyToRealm(symbolToSave)
            }
        }
    }
}
