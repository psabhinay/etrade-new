package com.etrade.mobilepro.orders.data.dto

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OrderDataDto(
    @Json(name = "viewOrdersResponse")
    val ordersResponse: OrdersResponseDto?,
    @Json(name = "GetSavedOrders")
    val savedOrderResponse: OrdersResponseDto?
) : BaseDataDto()

@JsonClass(generateAdapter = true)
data class OrdersResponseDto(
    @Json(name = "count")
    val count: String,
    @Json(name = "begin")
    val begin: String,
    @Json(name = "end")
    val end: String,
    @Json(name = "lastOrderNumber")
    val lastOrderNumber: String,
    @Json(name = "firstOrderNumber")
    val firstOrderNumber: String,
    @Json(name = "orderList")
    val orderList: List<OrderItemDto>
)

@JsonClass(generateAdapter = true)
data class OrderItemDto(
    @Json(name = "groupOrderCount")
    val groupOrderCount: String,
    @Json(name = "accountId")
    val accountId: String,
    @Json(name = "order")
    val order: List<OrderDto>,
    @Json(name = "header")
    val wholeOrderHeader: HeaderDto,
    @Json(name = "orderNumber")
    val orderNumber: String
)

@JsonClass(generateAdapter = true)
data class OrderDto(
    @Json(name = "advancedOrder")
    val advancedOrder: AdvancedOrderDto,
    @Json(name = "auditInfo")
    val auditInfo: AuditInfoDto,
    @Json(name = "header")
    val header: HeaderDto,
    @Json(name = "legDetails")
    val legDetails: List<LegDetailDto>,
    @Json(name = "egOverride")
    val egOverride: String,
    @Json(name = "egQual")
    val egQual: String,
    @Json(name = "extOrderId")
    val extOrderId: String,
    @Json(name = "legCount")
    val legCount: String,
    @Json(name = "timeFinalExecution")
    val timeFinalExecution: String
)
