package com.etrade.mobilepro.appwidget

import androidx.navigation.NavController
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.appwidget.api.settings.WidgetId
import com.etrade.mobilepro.appwidget.settings.WidgetSettingsRouter
import javax.inject.Inject

class DefaultWidgetSettingsRouter @Inject constructor() : WidgetSettingsRouter {
    override fun openWatchlistSelection(navController: NavController, widgetId: WidgetId) {
        navController.navigate(MainGraphDirections.actionToWidgetSettingsWatchlistSelection(widgetId.id))
    }
}
