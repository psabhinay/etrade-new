package com.etrade.mobilepro.inputvalueview.validators

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class LengthInRangeValidatorTest {

    private lateinit var subject: LengthInRangeValidator
    private val validRange = 2..32
    private val invalidRange = IntRange.EMPTY
    private val errorMessage = "Value length is not in range"
    private val validString by lazy { "1".repeat(30) }
    private val blankString by lazy { " ".repeat(30) }
    private val longString by lazy { "1".repeat(1000) }
    private val emptyString = ""
    private val inclusiveString by lazy { "1".repeat(32) }

    @BeforeEach
    fun setup() {
        subject = LengthInRangeValidator(validRange, errorMessage)
    }

    @Test
    fun `should throw in case of invalid range`() {
        val invalidValidator = LengthInRangeValidator(invalidRange, errorMessage)

        assertThrows(IllegalArgumentException::class.java) {
            invalidValidator.validate(validString)
        }
    }

    @Test
    fun `should be valid if value is in the range`() {
        val result = subject.validate(validString)
        assertTrue(result is InputFieldManager.Value.Valid)
        if (result is InputFieldManager.Value.Valid) {
            assertTrue(result.content == validString)
        }
    }

    @Test
    fun `should be valid if length is meet even if its blank`() {
        val result = subject.validate(blankString)
        assertTrue(result is InputFieldManager.Value.Valid)
        if (result is InputFieldManager.Value.Valid) {
            assertTrue(result.content == blankString)
        }
    }

    @Test
    fun `should be invalid if value length is bigger than the range`() {
        val result = subject.validate(longString)
        assertTrue(result is InputFieldManager.Value.Invalid)
        if (result is InputFieldManager.Value.Invalid) {
            assertTrue(result.reason == errorMessage)
        }
    }

    @Test
    fun `should be invalid if value length is smaller than the range`() {
        val result = subject.validate(emptyString)
        assertTrue(result is InputFieldManager.Value.Invalid)
        if (result is InputFieldManager.Value.Invalid) {
            assertTrue(result.reason == errorMessage)
        }
    }

    @Test
    fun `should be valid if the value is in the inclusive range`() {
        val result = subject.validate(inclusiveString)
        assertTrue(result is InputFieldManager.Value.Valid)
        if (result is InputFieldManager.Value.Valid) {
            assertTrue(result.content == inclusiveString)
        }
    }
}
