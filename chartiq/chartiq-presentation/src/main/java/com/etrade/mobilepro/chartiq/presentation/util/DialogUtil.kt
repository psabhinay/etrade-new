package com.etrade.mobilepro.chartiq.presentation.util

import android.content.res.Resources
import androidx.annotation.StringRes
import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.chartiq.presentation.R
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.orders.extensions.android.asSingleMessage
import com.etrade.mobilepro.orders.extensions.android.cancelSuccessDialog

internal fun Resources.simpleDialog(
    @StringRes messageResource: Int,
    dismissDialogAction: () -> Unit
) = simpleDialog(getString(messageResource), dismissDialogAction)

internal fun Resources.simpleDialog(
    text: String,
    dismissDialogAction: () -> Unit
): AppDialog {
    return appDialog {
        titleRes = R.string.chartiq_dialog_default_title
        message = text
        dismissAction = dismissDialogAction
        positiveAction {
            label = getString(R.string.chartiq_dialog_default_action)
            action = dismissDialogAction
        }
    }
}

internal fun Resources.orderDetailsDialog(
    text: String,
    dismissDialogAction: () -> Unit,
    cancelOrderAction: () -> Unit,
    editOrderAction: () -> Unit
): AppDialog {
    return appDialog {
        titleRes = R.string.chartiq_order_dialog_title
        message = text
        linkifyMessage = false
        cancelable = true
        resourcesPositiveAction {
            labelRes = R.string.chartiq_order_dialog_change_button
            action = editOrderAction
        }
        resourcesNegativeAction {
            labelRes = R.string.chartiq_order_dialog_cancel_button
            action = cancelOrderAction
        }
        dismissAction = dismissDialogAction
    }
}

internal fun Resources.orderCancelNotNeededDialog(dismissDialogAction: () -> Unit) = appDialog {
    titleRes = R.string.chartiq_dialog_default_title
    messageRes = R.string.chartiq_order_dialog_cancelled_error
    resourcesPositiveAction {
        labelRes = R.string.chartiq_dialog_default_action
    }
    dismissAction = dismissDialogAction
}

internal fun Resources.orderCancelSubmittedDialog(
    messages: List<ServerMessage>,
    dismissDialogAction: () -> Unit
): AppDialog {
    val text = if (messages.isEmpty()) {
        getString(R.string.orders_dialog_cancel_order_success_message)
    } else {
        messages.asSingleMessage()
    }
    return cancelSuccessDialog(text, dismissDialogAction)
}
