package com.etrade.mobilepro.transfermoney.api

typealias Holidays = List<@JvmSuppressWildcards TransferCalendarHolidays>

data class TransferCalendarHolidays(
    val fromInstType: String,
    val toInstType: String,
    val holidays: List<String>
)
