package com.etrade.mobilepro.news.briefing.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false, name = "GetMovingMarketResponse")
data class BriefingNewsDto constructor(
    @field:Element(name = "MarketUpdate")
    var payloadDto: BriefingNewsPayloadDto? = null
)

@Root(strict = false, name = "MarketUpdate")
data class BriefingNewsPayloadDto constructor(
    @field:Element(name = "StockTickerId")
    var stockTickerId: String? = null,
    @field:Element(name = "DateTime")
    var dateTime: String? = null,
    @field:Element(name = "Title")
    var title: String? = null,
    @field:Element(name = "Article")
    var content: String? = null
)
