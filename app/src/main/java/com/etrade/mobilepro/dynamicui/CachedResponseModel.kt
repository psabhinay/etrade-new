package com.etrade.mobilepro.dynamicui

import com.etrade.mobilepro.caching.ResponseModel
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required

open class CachedResponseModel(
    @PrimaryKey
    @Required
    var id: String = "",
    override var retrievedTimestamp: Long = 0,
    override var response: String? = null
) : RealmObject(), ResponseModel
