package com.etrade.mobilepro.login.usecase

import android.content.res.Resources
import com.etrade.mobilepro.R
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.common.di.PersonalNotifications
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.home.model.MARKET_DATA_AGREEMENT_KEY
import com.etrade.mobilepro.home.model.PUSH_NOTIFICATION_PROMPT_IN_KEY
import com.etrade.mobilepro.home.model.PendingPromptInDialog
import com.etrade.mobilepro.home.model.PromptInDialogsHelper
import com.etrade.mobilepro.landing.api.LandingRouter
import com.etrade.mobilepro.login.api.repo.LoginFailure
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.login.api.repo.LoginSuccess
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.preferences.ResetUserPreferencesUseCase
import com.etrade.mobilepro.pushnotification.api.repo.AlertPushSettingsRepo
import com.etrade.mobilepro.pushnotification.presentation.NotificationOptInDialog
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.BiometricAuthenticationAnalyticsParams
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.usersubscriptionpresentation.MarketDataSubscriptionPromptDialogFragment
import com.etrade.mobilepro.usersubscriptionpresentation.MarketDataSubscriptionPromptDialogFragmentArgs
import com.etrade.mobilepro.userviewmodel.UserViewModel
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(DefaultLoginResultUseCase::class.java)

class DefaultLoginResultUseCase @Inject constructor(
    private val resources: Resources,
    private val userSessionHandler: UserSessionHandler,
    private val userViewModel: UserViewModel,
    private val user: User,
    private val dialogsHelper: PromptInDialogsHelper,
    private val applicationPreferences: ApplicationPreferences,
    private val loginPreferences: LoginPreferencesRepo,
    private val streamingStatusController: StreamingStatusController,
    private val alertPushSettingsRepo: AlertPushSettingsRepo,
    private val landingRouter: LandingRouter,
    private val resetPreferencesUseCase: ResetUserPreferencesUseCase,
    @PersonalNotifications private val readStatusRepo: ReadStatusRepo,
    private val tracker: Tracker,
    private val appInfo: ApplicationInfo
) : LoginResultUseCase {

    override suspend fun execute(parameter: LoginResultParameter) {
        when (val result = parameter.result) {
            is LoginSuccess -> {
                handleNewUserName(parameter.request.username)
                trySaveUserPassword(parameter.request, result.isRsaUser)
                userViewModel.userSessionStateChange(
                    event = SessionStateChange.AUTHENTICATED,
                    // Landing router must have access to user account list before UserViewModel
                    // notifies observers about a user state change.
                    userStateChangePreparation = {
                        if (parameter.request.shouldHandleLanding) {
                            applicationPreferences.homeLandingCount += 1
                            landingRouter.createPendingNavigation()
                        }
                    }
                )
                user.session?.let {
                    applicationPreferences.isAdvancedOrderDisclosureSigned = it.isAdvancedOrderDisclosure
                    applicationPreferences.isConditionalOrderDisclosureSigned = it.isConditionalOrderDisclosure
                }
                userViewModel.updateInternationalState(result.isInternationalUser)
                alertPushSettingsRepo.clearCache()
                createPendingMarketDataAgreementDialog(result)
                createPendingPushNotificationDialog()
                startSession()
                saveLoginVersion()
            }

            is LoginFailure -> {
                userViewModel.userSessionStateChange(SessionStateChange.UNAUTHENTICATED)
            }
        }
    }

    private fun startSession() = userSessionHandler.onLoginSuccess()

    private suspend fun handleNewUserName(username: String) =
        loginPreferences
            .getUserName()
            .onSuccess {
                val differentUser = it.trim() != username.trim()
                clearPreferencesOrMarkPreviousLogin(differentUser)
                clearPersonalNotificationsCache(differentUser)
            }
            .onFailure { clearPreferencesOnException(it) }

    private suspend fun clearPreferencesOrMarkPreviousLogin(differentUser: Boolean) {
        if (differentUser) {
            logger.debug("Different username so resetting user preferences")
            resetPreferencesUseCase.execute(Unit)
        } else {
            loginPreferences.isLoginForSameUser = true
        }
    }

    private fun clearPersonalNotificationsCache(differentUser: Boolean) {
        if (differentUser) {
            logger.debug("Clearing personal notifications cache for different user login")
            readStatusRepo.clearAll()
        }
    }

    private suspend fun clearPreferencesOnException(failure: Throwable) {
        logger.error("Could not get encrypted username got exception", failure)
        resetPreferencesUseCase.execute(Unit)
    }

    private fun createPendingMarketDataAgreementDialog(result: LoginSuccess) {
        dialogsHelper.addPendingDialog(
            key = MARKET_DATA_AGREEMENT_KEY,
            dialog = PendingPromptInDialog(
                showDialogCondition = {
                    !result.isRealTimeUser && streamingStatusController.isStreamingToggleEnabled &&
                        !loginPreferences.isLoginMdaPopupShown
                },
                dialogClass = MarketDataSubscriptionPromptDialogFragment::class.java,
                dialogArgs = MarketDataSubscriptionPromptDialogFragmentArgs(
                    title = resources.getString(R.string.alert),
                    message = resources.getString(R.string.mda_login_message)
                ).toBundle(),
                onDialogShown = { loginPreferences.isLoginMdaPopupShown = true }
            )
        )
    }

    private fun createPendingPushNotificationDialog() {
        dialogsHelper.addPendingDialog(
            key = PUSH_NOTIFICATION_PROMPT_IN_KEY,
            dialog = PendingPromptInDialog(
                showDialogCondition = ::shouldShowPushNotificationDialog,
                dialogClass = NotificationOptInDialog::class.java,
                onDialogShown = { loginPreferences.isPushNotificationPopupShown = true }
            )
        )
    }

    private fun shouldShowPushNotificationDialog() = userShouldSeePushNotificationPrompt() &&
        settingsAreNotEnableAlready()

    private fun userShouldSeePushNotificationPrompt() = loginPreferences.isLoginForSameUser && !loginPreferences.isPushNotificationPopupShown

    private fun settingsAreNotEnableAlready() = !alertPushSettingsRepo.getCurrentSettings().accountAlertsEnabled ||
        !alertPushSettingsRepo.getCurrentSettings().stockAlertsEnabled

    @Suppress("TooGenericExceptionCaught")
    private suspend fun trySaveUserPassword(request: LoginRequest, isRsaUser: Boolean) {
        val password = extractPassword(request.password, isRsaUser)

        try {
            when {
                loginPreferences.isBiometricAuthenticationEnabled -> loginPreferences.updateUserPasswordIfChanged(password, request.authenticator)
                request.shouldOfferBiometric ->
                    loginPreferences.setUserPassword(password, request.authenticator)
                        .onSuccess { tracker.event(BiometricAuthenticationAnalyticsParams(true)) }
            }
        } catch (e: Exception) {
            // expanded this to all exceptions seeing some exceptions while we're saving password
            // no handling is needed if a user cancels the operation.
            logger.error("save password failed", e)
        }
    }

    private fun saveLoginVersion() {
        if (appInfo.appMajorMinorVersion == applicationPreferences.lastLoginMajorMinorVersion) {
            applicationPreferences.versionLoginCount ++
        } else {
            applicationPreferences.lastLoginMajorMinorVersion = appInfo.appMajorMinorVersion
            applicationPreferences.versionLoginCount = 1
        }
    }
}
