package com.etrade.mobilepro.portfolio.drip

import android.os.Bundle
import android.view.View
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.di.Web
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

class DripWebViewFragment @Inject constructor(
    @DeviceType deviceType: String,
    @Web private val baseWebUrl: String
) : WebViewFragment(deviceType) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Min height is set to the Bottom sheet to show the full blank page prior to webview loading,
        // Otherwise the height will be of progress bar alone since webview takes sometime to load.
        view.minimumHeight = resources.getDimensionPixelOffset(R.dimen.drip_webpage_min_height)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun getUrl() = resources.getString(R.string.url_drip, baseWebUrl)
}
