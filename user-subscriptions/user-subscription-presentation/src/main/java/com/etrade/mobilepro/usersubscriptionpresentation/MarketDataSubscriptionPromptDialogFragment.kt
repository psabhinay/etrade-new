package com.etrade.mobilepro.usersubscriptionpresentation

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.common.di.Web
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import javax.inject.Inject

class MarketDataSubscriptionPromptDialogFragment @Inject constructor(
    private val toggleStreaming: SubscriptionStreamingToggle,
    @Web private val baseWebUrl: String
) : DialogFragment() {

    private val args: MarketDataSubscriptionPromptDialogFragmentArgs by navArgs()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return context?.let { ctx ->
            isCancelable = false
            val builder = MaterialAlertDialogBuilder(ctx)
                .setMessage(args.message)
                .setTitle(args.title)
                .setPositiveButton(R.string.yes) { _, _ ->
                    dismiss()
                    requireContext().let {
                        it.startActivity(UserSubscriptionsWebViewActivity.intent(it, baseWebUrl))
                    }
                }
                .setNegativeButton(R.string.no) { _, _ ->
                    toggleStreaming.toggle(false)
                    dismiss()
                }

            builder.create()
        } ?: super.onCreateDialog(savedInstanceState)
    }
}
