package com.etrade.mobilepro.dynamicui.delegate

import com.etrade.mobilepro.dynamicui.DynamicScreenRpcService
import io.realm.Realm
import javax.inject.Inject

class DynamicScreenDataSourceDelegate @Inject constructor(
    dynamicScreenRpcService: DynamicScreenRpcService
) : WithRealmDataSourceDelegate(dynamicScreenRpcService) {

    override fun getRealmInstance(): Realm {
        return Realm.getDefaultInstance()
    }
}
