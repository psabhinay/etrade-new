package com.etrade.mobilepro.bloombergtv.data

import com.etrade.mobilepro.bloombergtv.api.BloombergTvResponse
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.GET

interface BloombergTvService {

    @JsonMoshi
    @GET("/e/t/user/BloombergLive.json")
    fun getBloombergTvResponse(): Single<BloombergTvResponse>
}
