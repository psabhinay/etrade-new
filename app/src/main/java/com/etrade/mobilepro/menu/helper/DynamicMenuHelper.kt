package com.etrade.mobilepro.menu.helper

import com.etrade.mobilepro.menu.viewmodel.MainMenuPageViewModel
import javax.inject.Inject

interface DynamicMenuHelper {
    fun prefetchDynamicMenuItems()
}

class DefaultDynamicMenuHelper @Inject constructor(
    private val mainMenuPageViewModel: MainMenuPageViewModel
) : DynamicMenuHelper {
    override fun prefetchDynamicMenuItems() {
        mainMenuPageViewModel.prefetchDynamicMenuItems()
    }
}
