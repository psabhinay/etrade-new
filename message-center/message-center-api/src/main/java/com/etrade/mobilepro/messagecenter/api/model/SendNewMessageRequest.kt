package com.etrade.mobilepro.messagecenter.api.model

data class SendNewMessageRequest(
    val selAcctHolder: String,
    val accountId: String,
    val subjectCode: String,
    val topicCode: String,
    val message: String,
    val instName: String,
    val instNo: String,
    val isStreamingOn: String,
    val isExtendedHoursOn: String
)
