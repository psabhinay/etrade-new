package com.etrade.mobilepro.trade.data.stock

import com.etrade.eo.rest.retrofit.Xml
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Path

internal interface StockTradingApiClient {

    @Xml
    @FormUrlEncoded
    @POST("/e/t/mobile/{requestType}")
    suspend fun makeStockOrder(
        @Path("requestType")
        requestType: String,
        @FieldMap
        tradingParams: Map<String, String>
    ): StockOrderResponseDto
}
