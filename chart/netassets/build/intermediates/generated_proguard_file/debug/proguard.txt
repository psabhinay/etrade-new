-if class com.etrade.mobilepro.chart.netassets.data.NetAssetsChartResponseDataDto
-keepnames class com.etrade.mobilepro.chart.netassets.data.NetAssetsChartResponseDataDto
-if class com.etrade.mobilepro.chart.netassets.data.NetAssetsChartResponseDataDto
-keep class com.etrade.mobilepro.chart.netassets.data.NetAssetsChartResponseDataDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}

-if class com.etrade.mobilepro.chart.netassets.data.NetAssetsChartResponseDto
-keepnames class com.etrade.mobilepro.chart.netassets.data.NetAssetsChartResponseDto
-if class com.etrade.mobilepro.chart.netassets.data.NetAssetsChartResponseDto
-keep class com.etrade.mobilepro.chart.netassets.data.NetAssetsChartResponseDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}

