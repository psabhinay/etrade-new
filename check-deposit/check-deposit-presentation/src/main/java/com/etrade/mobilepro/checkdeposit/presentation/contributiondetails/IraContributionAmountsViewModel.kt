package com.etrade.mobilepro.checkdeposit.presentation.contributiondetails

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.liveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount
import com.etrade.mobilepro.iraaccount.api.IraContributionDetails
import com.etrade.mobilepro.iraaccount.api.repo.IraContributionDetailRepo
import com.etrade.mobilepro.livedata.combineLatest
import javax.inject.Inject

class IraContributionAmountsViewModel @Inject constructor(
    private val iraContributionDetailRepo: IraContributionDetailRepo
) : ViewModel() {
    private val _iraDetailsRetry = MutableLiveData<Unit>().apply { value = Unit }
    private val _selectedAccount = MutableLiveData<FundsFlowAccount?>()

    val iraContributionDetails = _selectedAccount.distinctUntilChanged().combineLatest(_iraDetailsRetry).switchMap { (selectedAccount, _) ->
        liveData(context = viewModelScope.coroutineContext) {
            emit(ViewState.Loading)
            if (selectedAccount?.isIraAccount == true) {
                val iraDetails = iraContributionDetailRepo.getIraContributionDetails(selectedAccount.accountId, selectedAccount.instNo.toString()).fold(
                    onSuccess = {
                        ViewState.Loaded(it)
                    },
                    onFailure = {
                        ViewState.Failed {
                            _iraDetailsRetry.value = Unit
                        }
                    }
                )
                emit(iraDetails)
            }
        }
    }

    fun setSelectedAccount(selectedAccount: FundsFlowAccount?) {
        _selectedAccount.value = selectedAccount
    }

    sealed class ViewState {
        open val contributionDetails: IraContributionDetails? = null

        object Loading : ViewState()

        data class Loaded(
            override val contributionDetails: IraContributionDetails
        ) : ViewState()

        class Failed(
            val retry: () -> Unit
        ) : ViewState()
    }
}
