-if class com.etrade.mobilepro.chart.data.ChartDto
-keepnames class com.etrade.mobilepro.chart.data.ChartDto
-if class com.etrade.mobilepro.chart.data.ChartDto
-keep class com.etrade.mobilepro.chart.data.ChartDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
