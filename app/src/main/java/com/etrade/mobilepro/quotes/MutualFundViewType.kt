package com.etrade.mobilepro.quotes

enum class MutualFundViewType(val index: Int) {
    SNAPSHOT(0),
    PERFORMANCE(1),
    FEES_EXPENSE(2),
}

val Int.toTabViewType
    get() = MutualFundViewType.values().firstOrNull { it.index == this }
