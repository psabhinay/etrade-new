package com.etrade.mobilepro.chart.cache

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.chart.dto.AggregationChartRequestDto
import com.etrade.mobilepro.chart.dto.AggregationRequestBody
import com.etrade.mobilepro.chart.dto.AggregationRequestEntry
import com.etrade.mobilepro.chartengine.ChartRequest
import com.etrade.mobilepro.chartengine.SymbolChartData
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.util.AuthenticationStatusProvider
import io.reactivex.Single
import io.realm.Realm
import retrofit2.Retrofit
import javax.inject.Inject

private const val AGGREGATION_REQUEST_API_ID = "optchart"
private const val AGGREGATION_REQUEST_HTTP_METHOD = "GET"
private const val CHART_REQUEST_GRAPHS_PATH = "/graphs/%s.txt"
private const val SUCCESSFUL_RESPONSE_CODE = 200

class CachedAggregationChartDataSourceDelegate @Inject constructor(
    authStatusProvider: AuthenticationStatusProvider,
    @MobileTrade retrofit: Retrofit
) : BaseCachedChartDataSourceDelegate(authStatusProvider, retrofit),
    SimpleCachedResource.DataSourceDelegate<List<@JvmSuppressWildcards SymbolChartData>, List<@JvmSuppressWildcards ChartRequest>> {

    override fun fetchFromNetwork(id: List<ChartRequest>): Single<List<SymbolChartData>> {
        return apiClient.getAggregationChart(getEndpointType().typeCode, buildRequestBody(id)).map { response ->
            response.body?.entries?.filter { it.responseCode == SUCCESSFUL_RESPONSE_CODE }
                ?.mapNotNull { entry ->
                    id.find { it.getChartRequestString() == entry.id }?.let { request ->
                        entry.response?.mapToSymbolChartData(request)
                    }
                }
        }
    }

    override fun fetchFromDb(id: List<ChartRequest>): Single<List<SymbolChartData>> {
        return Single.fromCallable {
            Realm.getInstance(realmConfig).use { realm ->
                id.map {
                    fetchSymbolChartDataFromDb(it, realm)
                }
            }
        }
    }

    override fun putToCache(fetchedResult: List<SymbolChartData>): Single<List<SymbolChartData>> {
        return Single.fromCallable {
            Realm.getInstance(realmConfig).use { autoClosableRealm ->
                autoClosableRealm.executeTransaction { realm ->
                    val chartDataToSave = fetchedResult.map {
                        updateSymbolChartDataToDb(it, realm)
                    }
                    realm.copyToRealmOrUpdate(chartDataToSave)
                }
            }
            fetchedResult
        }
    }

    private fun buildRequestBody(requests: List<ChartRequest>) = AggregationChartRequestDto(
        AggregationRequestBody(
            requests.map {
                val chartRequestAsString = it.getChartRequestString()
                AggregationRequestEntry(
                    id = chartRequestAsString,
                    apiId = AGGREGATION_REQUEST_API_ID,
                    url = CHART_REQUEST_GRAPHS_PATH.format(chartRequestAsString),
                    httpMethod = AGGREGATION_REQUEST_HTTP_METHOD
                )
            }
        )
    )
}
