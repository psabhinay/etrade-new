package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.mapper.OrderDetailDescriptionMapper
import com.etrade.mobilepro.order.details.mapper.OrderDetailsMapper
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.testutil.getObjectFromJson
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class StockOrdersResponseDtoTest {

    @Test
    fun `test stock order mapper`() {
        val response = getStockOrderDetailsResponse()
        val orderDetailsDto = response.data?.viewOrderEvents
        val orderDetailsItemDto = orderDetailsDto?.orderDetails
        assertNotNull(orderDetailsItemDto)

        val orders = orderDetailsItemDto?.orders
        assertNotNull(orders)
        assertFalse(orders?.isEmpty() ?: false)

        val firstOrder = orderDetailsItemDto?.orders?.first()
        assertNotNull(firstOrder)
        val orderType = firstOrder?.header?.orderType?.let {
            OrderType.fromTypeCode(it)
        }
        assertNotNull(orderType)
        assertNotEquals(orderType, OrderType.UNKNOWN)
        val legDetailsDto = firstOrder?.legDetails

        assertNotNull(legDetailsDto)
        assertFalse(legDetailsDto?.isEmpty() ?: false)

        if (orderDetailsDto != null) {
            val orderDetailsItems = OrderDetailsMapper(OrderDetailDescriptionMapper()::map).map(orderDetailsDto)
            assertEquals(orderDetailsItems.size, 1)
            assertTrue(orderDetailsItems.first() is BaseOrderDetailsItem.OrderDetailsItem)

            val orderDetailsItem = orderDetailsItems.first() as BaseOrderDetailsItem.OrderDetailsItem
            assertEquals(BigDecimal.valueOf(411.27), orderDetailsItem.estimatedTotalCost)
            assertEquals(BigDecimal.valueOf(6.95), orderDetailsItem.commission)
            assertEquals(OrderStatus.EXECUTED, orderDetailsItem.orderStatus)
            assertEquals(BigDecimal.valueOf(10), orderDetailsItem.priceExecuted)
            assertEquals("2", orderDetailsItem.quantity)
            assertEquals(PriceType.MARKET, orderDetailsItem.priceType)
            assertEquals(1, orderDetailsItem.orderDescriptions.size)

            val orderDescFirst = orderDetailsItem.orderDescriptions.getOrNull(0)
            orderDescFirst?.apply {
                assertEquals("2", displayQuantity)
                assertEquals("2", fillQuantity)
                assertEquals("2", quantity)
                assertEquals(TransactionType.BUY_OPEN, transactionType)
                assertEquals("AAPL", displaySymbol)
                assertEquals("AAPL", originalSymbol)
                assertEquals("", underlyingProductId)
            }
            orderDetailsItem.tradeLegs.apply {
                assertEquals(1, size)
                val firstLeg = first()
                assertEquals("AAPL", firstLeg.symbol)
                assertEquals("AAPL", firstLeg.displaySymbol)
                assertEquals(TransactionType.BUY_OPEN, firstLeg.transactionType)
                assertEquals(2, firstLeg.quantity)
            }
        }
    }

    private fun getStockOrderDetailsResponse(): ServerResponseDto<OrderDetailsResponseDto> =
        StockOrdersResponseDtoTest::class.getObjectFromJson("stock_order_details.json", ServerResponseDto::class.java, OrderDetailsResponseDto::class.java)
}
