package com.etrade.mobilepro.quoteapi

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.livedata.DistinctLiveData
import com.etrade.mobilepro.quoteapi.MobileQuote.Companion.formatTimeStamp
import com.etrade.mobilepro.streaming.api.Level1Data

@Suppress("LongParameterList")
class QuoteWidgetItem(
    override val title: String,
    val symbol: String,
    val instrumentType: InstrumentType,
    val securityType: String,
    initialLastPrice: QuotePriceWidgetItem,
    initialChange: QuoteChangeWidgetItem,
    initialChangeColor: Int,
    initialVolume: String,
    initialAsOfDateTime: String?,
    initialBidPrice: QuotePriceWidgetItem?,
    initialAskPrice: QuotePriceWidgetItem?,
    val showVolume: Boolean,
    val showBidPrice: Boolean,
    val showAskPrice: Boolean,
    private val resources: QuoteTemplateResources,
    val underlyingSymbol: String = symbol,
    val underlyingInstrumentType: InstrumentType = instrumentType,
    val showUnderlyingInfo: Boolean = false,
    override val titleContentDescription: String,
    override val isAffiliatedProduct: Boolean
) : QuoteBidAskItem, QuoteItemInterface, QuoteHeaderViewItem {

    override val lastPrice: LiveData<QuotePriceWidgetItem>
        get() = _lastPrice
    override val change: LiveData<QuoteChangeWidgetItem>
        get() = _change
    override val changeColor: LiveData<Int>
        get() = _changeColor
    override val volume: LiveData<String>
        get() = _volume
    override val asOfDateTime: LiveData<String>
        get() = _asOfDateTime

    override val bidPrice: LiveData<QuotePriceWidgetItem>
        get() = _bidPrice
    override val askPrice: LiveData<QuotePriceWidgetItem>
        get() = _askPrice

    override val contentDescription: LiveData<String?>
        get() = _contentDescription

    private val _lastPrice: MutableLiveData<QuotePriceWidgetItem> = DistinctLiveData(initialLastPrice)
    private val _change: MutableLiveData<QuoteChangeWidgetItem> = DistinctLiveData(initialChange)
    private val _changeColor: MutableLiveData<Int> = DistinctLiveData(initialChangeColor)
    private val _volume: MutableLiveData<String> = DistinctLiveData(initialVolume)
    private val _asOfDateTime: MutableLiveData<String> = DistinctLiveData(initialAsOfDateTime)
    private val _bidPrice: MutableLiveData<QuotePriceWidgetItem> = DistinctLiveData(initialBidPrice)
    private val _askPrice: MutableLiveData<QuotePriceWidgetItem> = DistinctLiveData(initialAskPrice)
    private val _contentDescription: MutableLiveData<String?> = DistinctLiveData()

    init {
        updateContentDescription(resources.priceFormat, resources.sizeFormat, resources.changeFormat, resources.contentDescriptionFormat, _contentDescription)
    }

    val description: LiveData<QuiteWidgetDescription> = MediatorLiveData<QuiteWidgetDescription>().apply {
        @Suppress("LongParameterList")
        fun merge(
            price: QuotePriceWidgetItem?,
            change: QuoteChangeWidgetItem?,
            volume: String?,
            bid: QuotePriceWidgetItem?,
            ask: QuotePriceWidgetItem?,
            asOfDateTime: String?
        ) {
            value = QuiteWidgetDescription(
                title = if (instrumentType.isOption) {
                    title.toNewLineSeparatedDollarAmount()
                } else {
                    title
                },
                titleContentDescription = titleContentDescription,
                price = price,
                change = change,
                volume = volume,
                bid = bid,
                ask = ask,
                asOfDateTime = asOfDateTime,
                showBidPrice = showBidPrice,
                showAskPrice = showAskPrice
            )
        }

        addSource(lastPrice) { merge(it, change.value, volume.value, bidPrice.value, askPrice.value, asOfDateTime.value) }
        addSource(change) { merge(lastPrice.value, it, volume.value, bidPrice.value, askPrice.value, asOfDateTime.value) }
        addSource(volume) { merge(lastPrice.value, change.value, it, bidPrice.value, askPrice.value, asOfDateTime.value) }
        addSource(bidPrice) { merge(lastPrice.value, change.value, volume.value, it, askPrice.value, asOfDateTime.value) }
        addSource(askPrice) { merge(lastPrice.value, change.value, volume.value, bidPrice.value, it, asOfDateTime.value) }
        addSource(asOfDateTime) { merge(lastPrice.value, change.value, volume.value, bidPrice.value, askPrice.value, it) }
    }

    var hasNeutralColorForLast: Boolean = true
    var isSingleItem: Boolean = false

    override fun updateItem(data: Level1Data, isExtendedHours: Boolean) {
        updateLastChangeVolume(data, isExtendedHours)
        updateBidAsk(data)
        updateTimeStamp(data)
    }

    fun updateLastChangeVolume(data: Level1Data, isExtendedHours: Boolean) {
        val useDaysChange = isExtendedHours && (instrumentType != InstrumentType.EQ || isSingleItem)
        _lastPrice.updateWith { data.toLastPrice(it, resources, hasNeutralColorForLast) }
        _change.updateWith { data.toChange(it, useDaysChange) }
        _changeColor.updateWith { data.toChangeColor(it, resources, useDaysChange) }

        data.volume?.let { volume -> _volume.postValue(volume) }
        updateContentDescription(resources.priceFormat, resources.sizeFormat, resources.changeFormat, resources.contentDescriptionFormat, _contentDescription)
    }

    fun updateBidAsk(data: Level1Data) {
        _bidPrice.updateWith { data.toBidPrice(it) }
        _askPrice.updateWith { data.toAskPrice(it) }
    }

    fun updateTimeStamp(data: Level1Data) {
        data.timestampSinceEpoch?.let { timestampSinceEpoch ->
            _asOfDateTime.postValue(formatTimeStamp(timestampSinceEpoch, instrumentType.isMutualFundOrMoneyMarketFund, resources))
        }
    }

    data class QuiteWidgetDescription(
        val title: String?,
        val titleContentDescription: String?,
        val price: QuotePriceWidgetItem?,
        val change: QuoteChangeWidgetItem?,
        val volume: String?,
        val bid: QuotePriceWidgetItem?,
        val ask: QuotePriceWidgetItem?,
        val asOfDateTime: String?,
        val showBidPrice: Boolean,
        val showAskPrice: Boolean
    )
}

// Incorrect accessibility talk-back :
// Inserting new line to avoid treating of "$" as comma
// for.eg Aug 7 '20 $120 read as 20120 (twenty one hundred twenty dollar)
private fun String.toNewLineSeparatedDollarAmount(): String {
    return this.replace(MarketDataFormatter.DOLLAR_SIGN, "\n${MarketDataFormatter.DOLLAR_SIGN}")
}
