package com.etrade.mobilepro.trade.api.option

import com.etrade.mobilepro.backends.api.HasWarnings

interface OptionTradeStrategyResponse : OptionTradeStrategy, HasWarnings
