package com.etrade.mobilepro.editlist

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.editlist.databinding.ItemEditListBinding

abstract class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    /**
     * Let's user to remove an item.
     */
    abstract val removeView: View?

    /**
     * Let's user to drag an item to a new place.
     */
    abstract val dragHandle: View

    /**
     * Let's user to toggle an item
     */
    abstract val toggleView: SwitchCompat?

    abstract val titleView: TextView

    /**
     * Binds this view holder to a [value].
     *
     * @param value a value to bind to
     */
    open fun bindTo(value: EditListItem) {
        titleView.text = value.displayText
        val contentDescription = value.contentDescription
        itemView.contentDescription = value.contentDescription
        removeView?.contentDescription = "${itemView.resources.getString(R.string.remove)} $contentDescription"
        toggleView?.contentDescription = "$contentDescription"
        dragHandle.contentDescription = value.dragHandleContentDescription
        toggleView?.setOnCheckedChangeListener(null)
        toggleView?.isChecked = value.isChecked
    }
}

class ItemEditListViewHolder(val binding: ItemEditListBinding) : ItemViewHolder(binding.root) {

    override val removeView: View? = binding.remove

    override val dragHandle: View = binding.dragHandle

    override val toggleView: SwitchCompat? = binding.toggleAction

    override val titleView: TextView = binding.title
}
