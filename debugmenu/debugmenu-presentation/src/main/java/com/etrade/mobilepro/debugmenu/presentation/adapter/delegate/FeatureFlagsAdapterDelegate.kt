package com.etrade.mobilepro.debugmenu.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder.FeatureFlagsViewHolder
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemFeatureFlagsBinding
import com.etrade.mobilepro.debugmenu.presentation.item.DebugMenuItem
import com.etrade.mobilepro.debugmenu.presentation.item.FeatureFlagsItem
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class FeatureFlagsAdapterDelegate(
    private val listener: () -> Unit
) : AbsListItemAdapterDelegate<FeatureFlagsItem, DebugMenuItem, FeatureFlagsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): FeatureFlagsViewHolder {
        return FeatureFlagsViewHolder(
            parent.viewBinding(DebugmenuItemFeatureFlagsBinding::inflate),
            listener
        )
    }

    override fun isForViewType(
        item: DebugMenuItem,
        items: MutableList<DebugMenuItem>,
        position: Int
    ): Boolean =
        item is FeatureFlagsItem

    override fun onBindViewHolder(
        item: FeatureFlagsItem,
        holder: FeatureFlagsViewHolder,
        payloads: MutableList<Any>
    ) {
        /* no-op */
    }
}
