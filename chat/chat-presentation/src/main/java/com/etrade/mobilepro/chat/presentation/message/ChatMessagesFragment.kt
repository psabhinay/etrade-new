package com.etrade.mobilepro.chat.presentation.message

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri
import android.os.Bundle
import android.os.IBinder
import android.os.Parcelable
import android.provider.Settings
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.api.ChatParamsRequestModel
import com.etrade.mobilepro.chat.api.ChatState
import com.etrade.mobilepro.chat.presentation.ChatRouter
import com.etrade.mobilepro.chat.presentation.R
import com.etrade.mobilepro.chat.presentation.common.ChatConstants
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_ACTIVITY_CLASS
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_PARAMS_REQUEST_BODY
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_REQUEST_CODE_DISCONNECT_DIALOG
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_REQUEST_CODE_TERMINATE_DIALOG
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_SESSION_DATA
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_SESSION_ID
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_START_POINT_MESSAGE
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_TERMINATE_TAG
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.IS_CHAT_LAUNCHED_FROM_MESSAGES_CENTER
import com.etrade.mobilepro.chat.presentation.databinding.ChatFragmentMessagesBinding
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModel.ViewState.ChatTransferredView
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModel.ViewState.Error
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModel.ViewState.Loading
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModel.ViewState.MessagesShown
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModel.ViewState.QueueShown
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModel.ViewState.SendMessageError
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModel.ViewState.StartChatService
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModel.ViewState.Terminated
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModel.ViewState.Undefined
import com.etrade.mobilepro.chat.presentation.message.adapter.ChatMessagesAdapter
import com.etrade.mobilepro.chat.presentation.service.ChatMessagesService
import com.etrade.mobilepro.common.OnNavigateUpListener
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject
import kotlin.math.ceil

private const val TIME_MULTIPLIER = 60

@Suppress("TooManyFunctions")
@RequireLogin
class ChatMessagesFragment @Inject constructor(
    private val chatMessagesViewModelFactory: ChatMessagesViewModelFactory,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val chatRouter: ChatRouter
) : OverlayBaseFragment(), OnNavigateUpListener {

    private val binding by viewBinding(ChatFragmentMessagesBinding::bind)

    override val layoutRes: Int = R.layout.chat_fragment_messages

    private val viewModel: ChatMessagesViewModel by viewModels {
        chatParamsRequestBody = arguments?.getBundle(CHAT_SESSION_DATA)
            ?.getSerializable(CHAT_PARAMS_REQUEST_BODY) as ChatParamsRequestModel?
        chatMessagesViewModelFactory.create(chatParamsRequestBody)
    }
    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private val messagesAdapter by lazy { ChatMessagesAdapter() }
    private val chatStateObserver: Observer<ChatState> = Observer { viewModel.onChatStateChange(it) }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, service: IBinder) {
            val binder = service as ChatMessagesService.MessagesBinder
            chatMessagesService = binder.service
            observeChatState()
        }

        override fun onServiceDisconnected(cName: ComponentName) {
            unsubscribeChatStateChanges()
            chatMessagesService = null
        }
    }

    private val settingsRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {}

    private var snackbar: Snackbar? = null
    private var chatParamsRequestBody: ChatParamsRequestModel? = null
    private var chatMessagesService: ChatMessagesService? = null
    private var isTerminated = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.chat_queue_options_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.minimize_chat) {
            requestOverlayPermissionIfNecessary(goBack = true)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        menu.findItem(R.id.minimize_chat).isVisible = viewModel.viewState.value !is Error
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                CHAT_REQUEST_CODE_DISCONNECT_DIALOG, CHAT_REQUEST_CODE_TERMINATE_DIALOG -> viewModel.terminateByUser()
            }
        }
    }

    override fun onNavigateUp(): Boolean {
        return if (viewModel.viewState.value is Error) {
            false
        } else {
            val dialog = CustomDialogFragment.newInstance(
                title = getString(R.string.chat_end_dialog_title),
                message = getString(R.string.chat_terminate_msg),
                okTitle = getString(R.string.ok),
                cancelTitle = getString(R.string.chat_cancel),
                cancelable = false,
                fragmentId = CHAT_TERMINATE_TAG,
                requestCode = CHAT_REQUEST_CODE_TERMINATE_DIALOG
            )
            dialog.show(parentFragmentManager, CHAT_TERMINATE_TAG)
            true
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupCallbacks()
        setupView()
    }

    override fun onResume() {
        super.onResume()
        isTerminated = false
        chatMessagesService?.removeFloatingButton()
    }

    override fun onPause() {
        super.onPause()
        unsubscribeChatStateChanges()
        if (isTerminated) {
            ChatMessagesService.stop(requireContext())
        } else {
            chatMessagesService?.showFloatingButton()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        chatMessagesService?.run {
            if (ChatMessagesService.isRunning) {
                unbindService(serviceConnection)
            }
        }
    }

    private fun observeChatState() {
        chatMessagesService?.apply {
            removeFloatingButton()
            chatState.observe(viewLifecycleOwner, chatStateObserver)
        }
    }

    private fun setupView() {
        val layoutManager = LinearLayoutManager(requireContext()).apply {
            stackFromEnd = true
            reverseLayout = false
        }

        viewModel.refresh(false)
        binding.chatRecyclerView.layoutManager = layoutManager
        binding.chatRecyclerView.adapter = messagesAdapter
    }

    private fun setupCallbacks() {
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, true) {
            requestOverlayPermissionIfNecessary(goBack = true)
        }
        binding.messageEditText.addTextChangedListener {
            if (!it.isNullOrBlank()) {
                binding.sendMessageButton.visibility = VISIBLE
            } else {
                binding.sendMessageButton.visibility = GONE
            }
        }
        binding.sendMessageButton.setOnClickListener {
            val message = binding.messageEditText.text.toString()
            binding.messageEditText.text = null
            chatMessagesService?.sendMessage(message)
        }
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                handleStatus(it)
            }
        )
    }

    private fun handleQueueShown(state: QueueShown) {
        toggleViewState(isLoading = false, isDisabled = false, showMessages = false)
        state.formatMinutesAndSeconds()
    }

    @SuppressWarnings("ComplexMethod")
    private fun handleStatus(state: ChatMessagesViewModel.ViewState) {
        snackbar?.dismiss()
        when (state) {
            is ChatTransferredView -> handleMessagesShown(state.messages)
            is QueueShown -> handleQueueShown(state)
            is MessagesShown -> handleMessagesShown(state.messages)
            is SendMessageError -> handleSendMessageError(state.messages, state.message)
            is StartChatService -> startChatService(state.sessionId)
            is Loading -> toggleViewState(isLoading = true, isDisabled = false)
            is Terminated -> {
                ChatMessagesService.stop(requireContext())
                isTerminated = true
                goBack(state.navToChatSetup)
            }
            is Error -> {
                handleFailure()
                snackbar = snackBarUtil.retrySnackbar(state.message, dismissOnPause = false) {
                    chatRouter.openChatSetup(findNavController()) // Retry with Chat Setup
                }
            }
            Undefined -> { /* Do Nothing */
            }
        }
    }

    private fun startChatService(sessionId: String? = null) {
        if (chatMessagesService != null) {
            observeChatState()
        } else {
            sessionId?.let { requestOverlayPermissionIfNecessary(goBack = false) }
            toggleViewState(isLoading = true, isDisabled = false, showMessages = false)
            val context = requireContext()
            val bundle = arguments?.getBundle(CHAT_SESSION_DATA)
            val intent = ChatMessagesService.getIntent(context).apply {
                action = ChatConstants.ACTION_START_MESSAGES_SERVICE
                putExtra(CHAT_SESSION_ID, sessionId)
                putExtra(CHAT_ACTIVITY_CLASS, requireActivity().javaClass)
                putExtra(CHAT_PARAMS_REQUEST_BODY, bundle?.getSerializable(CHAT_PARAMS_REQUEST_BODY))
                putExtra(CHAT_START_POINT_MESSAGE, bundle?.getParcelable(CHAT_START_POINT_MESSAGE) as Parcelable?)
                putExtra(IS_CHAT_LAUNCHED_FROM_MESSAGES_CENTER, bundle?.getBoolean(IS_CHAT_LAUNCHED_FROM_MESSAGES_CENTER, false))
            }
            ChatMessagesService.startAndBound(context, intent, serviceConnection)
        }
    }

    private fun handleSendMessageError(messages: List<ChatMessage>, message: String) {
        handleMessagesShown(messages)
        snackbar = snackBarUtil.retrySnackbar(getString(R.string.chat_send_message_failed)) {
            chatMessagesService?.sendMessage(message)
        }
    }

    private fun handleMessagesShown(messages: List<ChatMessage>) {
        toggleViewState(isLoading = false, isDisabled = false, showMessages = true)
        messagesAdapter.submitItems(messages)

        // Scroll to last Message
        binding.chatRecyclerView.scrollToPosition(messagesAdapter.itemCount - 1)
    }

    private fun openSettingsWithOverlayPermission() {
        val intent = Intent(
            Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
            Uri.parse("package:${requireContext().packageName}")
        )
        settingsRequest.launch(intent)
    }

    private fun requestOverlayPermissionIfNecessary(goBack: Boolean) {
        if (!Settings.canDrawOverlays(requireContext())
        ) {
            openSettingsWithOverlayPermission()
        } else if (goBack) {
            goBack()
        }
    }

    private fun goBack(navToChatSetup: Boolean = false) {
        val navController = findNavController()
        if (navToChatSetup) {
            chatRouter.openChatSetup(navController)
        } else {
            val bundle = arguments?.getBundle(CHAT_SESSION_DATA)
            if (bundle?.getBoolean(IS_CHAT_LAUNCHED_FROM_MESSAGES_CENTER, false) == true) {
                chatRouter.openMessageCenter(navController, bundle)
            } else {
                chatRouter.openMenu(navController)
            }
        }
    }

    private fun handleFailure() {
        isTerminated = true
        activity?.invalidateOptionsMenu()
    }

    private fun setChatVisibility(isChatVisible: Boolean = false, isHideAll: Boolean = false) {
        with(binding) {
            when {
                isHideAll -> {
                    chatMessageEditTextLayout.visibility = GONE
                    chatRecyclerView.visibility = GONE
                    sendMessageButton.visibility = GONE
                    chatQueueTitle.visibility = GONE
                    chatQueueDescription.visibility = GONE
                }
                isChatVisible -> {
                    chatMessageEditTextLayout.visibility = VISIBLE
                    chatRecyclerView.visibility = VISIBLE
                    chatQueueTitle.visibility = GONE
                    chatQueueDescription.visibility = GONE
                }
                else -> {
                    chatMessageEditTextLayout.visibility = GONE
                    chatRecyclerView.visibility = GONE
                    sendMessageButton.visibility = GONE
                    chatQueueTitle.visibility = VISIBLE
                    chatQueueDescription.visibility = VISIBLE
                }
            }
        }
    }

    private fun toggleViewState(isLoading: Boolean, isDisabled: Boolean, showMessages: Boolean = false) {
        setChatVisibility(isChatVisible = showMessages, isHideAll = isLoading || isDisabled)
        if (isLoading) {
            binding.chatQueuePb.show()
        } else {
            binding.chatQueuePb.hide()
        }
    }

    private fun unsubscribeChatStateChanges() {
        chatMessagesService?.chatState?.removeObserver { chatStateObserver }
    }

    private fun QueueShown.formatMinutesAndSeconds() {
        if (viewModel.agentDisconnected) {
            resources.getString(R.string.chat_transfer_queue_message)
        } else if (estimatedWaitTimeInSeconds > 0) {
            val estimatedWaitText = if (estimatedWaitTimeInSeconds > TIME_MULTIPLIER) {
                var minutes = ceil((estimatedWaitTimeInSeconds / TIME_MULTIPLIER).toDouble())
                if (minutes > TIME_MULTIPLIER) {
                    val hours = minutes / TIME_MULTIPLIER
                    minutes -= (hours * TIME_MULTIPLIER)

                    "${resources.getQuantityString(R.plurals.chat_hours_wait, hours.toInt(), hours.toInt())} and " +
                        resources.getQuantityString(R.plurals.chat_minutes_wait, minutes.toInt(), minutes.toInt())
                } else {
                    resources.getQuantityString(R.plurals.chat_minutes_wait, minutes.toInt(), minutes.toInt())
                }
            } else {
                resources.getQuantityString(R.plurals.chat_minutes_wait, 1, 1)
            }

            binding.chatQueueTitle.text =
                String.format(getString(R.string.chat_queue_position_message), estimatedWaitText)
        }
    }
}
