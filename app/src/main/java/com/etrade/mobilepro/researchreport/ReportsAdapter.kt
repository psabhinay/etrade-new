package com.etrade.mobilepro.researchreport

import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.QuoteGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.databinding.ItemReportBinding
import com.etrade.mobilepro.mod.api.Report
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemDisclosureBinding
import com.etrade.mobilepro.util.android.binding.resources
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.etrade.mobilepro.util.android.extension.viewBinding

class ReportsAdapter(
    private val disclosuresClickListener: () -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items: List<ReportListItem> = emptyList()
        set(value) {
            dispatchUpdates(field, value)
            field = value
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.item_report -> ReportsViewHolder(parent.viewBinding(ItemReportBinding::inflate))
            R.layout.quote_item_disclosure -> DisclosuresViewHolder(
                parent.viewBinding(
                    QuoteItemDisclosureBinding::inflate
                ),
                disclosuresClickListener
            )
            else -> throw IllegalStateException("Provided view type is not supported")
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        if (item is ReportListItem.ReportItem) {
            (holder as ReportsViewHolder).bindTo(item.report)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is ReportListItem.ReportItem -> R.layout.item_report
            is ReportListItem.DisclosuresItem -> R.layout.quote_item_disclosure
        }
    }
}

class DisclosuresViewHolder(
    binding: QuoteItemDisclosureBinding,
    private val clickListener: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.disclosureTitle.setOnClickListener {
            clickListener.invoke()
        }
    }
}

class ReportsViewHolder(
    private val binding: ItemReportBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(item: Report?) {
        val report = item ?: return
        with(binding) {
            reportSourceTv.text = report.provider
            reportedDateTv.text =
                resources.getString(R.string.report_date_format, report.reportDate)
            reportContainer.setOnClickListener {
                report.reportUrl?.let {
                    itemView.findNavController()
                        .navigate(
                            QuoteGraphDirections.actionQuoteOpenPdf(it, report.provider)
                        )
                }
            }
        }
    }
}
