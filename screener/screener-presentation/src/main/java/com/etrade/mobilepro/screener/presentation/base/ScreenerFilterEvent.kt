package com.etrade.mobilepro.screener.presentation.base

import com.etrade.mobilepro.screener.api.ScreenerFilter

interface ScreenerFilterEvent<F : ScreenerFilter, T : BaseFilterScreenViewModel.ViewState<F>, V> {
    fun update(viewState: T, eventValue: V): T
    fun conditionalUpdate(viewState: T, eventValue: V): Result<T> =
        Result(value = update(viewState, eventValue), shouldTriggerRequest = true)

    data class Result<T>(
        val value: T,
        val shouldTriggerRequest: Boolean
    )
}
