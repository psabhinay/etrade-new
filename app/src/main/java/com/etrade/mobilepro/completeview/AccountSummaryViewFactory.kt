package com.etrade.mobilepro.completeview

import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.etrade.completeview.dto.AccountSummaryViewDto
import com.etrade.mobilepro.R
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.portfolio.model.ExtendedAccountSummaryViewModel
import com.etrade.mobilepro.portfolio.model.PortfolioCtaList
import com.etrade.mobilepro.portfolio.model.PortfolioExtendedSummaryModel
import com.etrade.mobilepro.portfolio.model.createParcelableSectionDto
import com.etrade.mobilepro.portfolio.model.dto.getExtendPortfolioSummaryCta
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import com.etrade.mobilepro.streaming.client.side.calculations.ALL_BROKERAGE_STREAM_ID
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

/**
 * Constructs Account Summary View for individual or All Brokerage Accounts on  Portfolio Screen, and
 * All brokerage Screen respectively.
 */
class AccountSummaryViewFactory @Inject constructor(
    daoProvider: CachedDaoProvider,
    defaultTextManager: StreamingTextManager,
    resources: Resources
) : AbstractAccountSummaryViewFactory(daoProvider, resources) {

    private var _streamType: StreamType = StreamType.PORTFOLIO_VIEW

    override val streamType: StreamType
        get() = _streamType

    override val streamingTextManager: StreamingTextManager = defaultTextManager

    override val accountSummaryLayoutRes: Int = R.layout.view_account_summary

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is AccountSummaryViewDto) {
            return if (dto.data.extraDetailsDtos.isEmpty()) {
                AccountSummaryDelegate(dto.data, dto.ctaList, dto.clickActionDto)
            } else {
                ExtendedAccountSummaryDelegate(dto)
            }
        } else {
            throw DynamicUiViewFactoryException("AccountSummaryViewFactory registered to unknown type")
        }
    }

    override fun setupValuesFromRequest(screenRequest: ScreenRequest) {
        _streamType = when (screenRequest) {
            is ServicePath.AllBrokeragePositions -> StreamType.ALL_BROKERAGE_VIEW
            is ServicePath.CompleteView -> throw IllegalAccessException("Invalid use of Summary view in CompleteView, prefer using Card view")
            else -> StreamType.PORTFOLIO_VIEW
        }
    }

    private inner class ExtendedAccountSummaryDelegate(private val dto: AccountSummaryViewDto) :
        AccountSummaryLayout(dto.data.accountUuid) {

        override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
            val ctaList: PortfolioCtaList = dto.ctaList.getExtendPortfolioSummaryCta()
            ExtendedAccountSummaryViewModel(
                toAccountSummary(
                    dto.data,
                    ctaList.headerViewCtaList,
                    dto.clickActionDto,
                    accountSourceLabel
                ),
                portfolioSummaryModelBuilder = {
                    val account = getAccount(dto.data.accountUuid)
                    val isStreamingAllowed = checkStreamingPermissionByAccountId(getStreamableAccountId(account), account?.isAccountStreamingAllowed)
                    PortfolioExtendedSummaryModel(
                        extraSummarySections = dto.data.createParcelableSectionDto(account?.accountId ?: ALL_BROKERAGE_STREAM_ID),
                        callToActions = ctaList.extendedViewCtaList,
                        title = it?.accountShortName,
                        subTitle = it?.accountId,
                        streamableAccountId = account?.accountId ?: ALL_BROKERAGE_STREAM_ID,
                        isStreamingAllowed = isStreamingAllowed
                    )
                }
            )
        }

        override fun refresh(viewModel: DynamicViewModel) {
            if (viewModel is ExtendedAccountSummaryViewModel) {
                updateViewModelWithDto(dto.data, viewModel.accountSummaryViewModel, dto.ctaList)
                (viewModel.portfolioSummaryModel as PortfolioExtendedSummaryModel).updateSummarySection(
                    dto.data
                )
            }
        }

        override val viewModelClass = ExtendedAccountSummaryViewModel::class.java
    }
}
