package com.etrade.mobilepro.trade.presentation.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.api.TradeRepo
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import kotlinx.coroutines.launch
import javax.inject.Inject

class SharedTradeViewModel @Inject constructor(
    private val tradeRepo: TradeRepo
) : ViewModel() {

    val injectFormInputValuesSignal: LiveData<ConsumableLiveEvent<InputValuesSnapshot>>
        get() = _injectFormInputValues
    val orderTerminatedSignal: LiveData<ConsumableLiveEvent<String>>
        get() = _orderTerminatedSignal
    val optionLegSelectedSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _optionLegSelected

    private val _injectFormInputValues = MutableLiveData<ConsumableLiveEvent<InputValuesSnapshot>>()
    private val _optionLegSelected = MutableLiveData<ConsumableLiveEvent<Unit>>()
    private val _orderTerminatedSignal: MutableLiveData<ConsumableLiveEvent<String>> = MutableLiveData()

    var securityType = SecurityType.STOCK

    fun clearOrder(orderId: String) {
        _orderTerminatedSignal.value = ConsumableLiveEvent(orderId)
        viewModelScope.launch {
            tradeRepo.clearOrder(orderId)
        }
    }

    fun injectFormInputValues(data: InputValuesSnapshot) {
        _injectFormInputValues.value = ConsumableLiveEvent(data)
    }

    fun signalOptionsLegSelected() {
        _optionLegSelected.value = ConsumableLiveEvent(Unit)
    }
}
