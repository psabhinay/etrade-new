package com.etrade.mobilepro.biometric.presentation

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData

interface BiometricAuthenticationDelegate : BiometricAuthenticator {

    fun init(activity: AppCompatActivity)

    val isBiometricPromptCancelled: Boolean
    val isAuthenticationNeeded: Boolean
    val isBiometricPromptShowing: LiveData<Boolean>
}
