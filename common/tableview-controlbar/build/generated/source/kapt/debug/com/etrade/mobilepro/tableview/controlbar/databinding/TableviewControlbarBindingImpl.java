package com.etrade.mobilepro.tableview.controlbar.databinding;
import com.etrade.mobilepro.tableview.controlbar.R;
import com.etrade.mobilepro.tableview.controlbar.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class TableviewControlbarBindingImpl extends TableviewControlbarBinding implements com.etrade.mobilepro.tableview.controlbar.generated.callback.OnClickListener.Listener, com.etrade.mobilepro.tableview.controlbar.generated.callback.OnIndexToggledListener.Listener {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    // variables
    @Nullable
    private final android.view.View.OnClickListener mCallback3;
    @Nullable
    private final android.view.View.OnClickListener mCallback1;
    @Nullable
    private final com.etrade.mobilepro.ui.multitoggle.MultiToggle.OnIndexToggledListener mCallback2;
    // values
    // listeners
    // Inverse Binding Event Handlers

    public TableviewControlbarBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 4, sIncludes, sViewsWithIds));
    }
    private TableviewControlbarBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 3
            , (android.widget.ImageButton) bindings[3]
            , (com.google.android.material.button.MaterialButton) bindings[1]
            , (android.widget.RelativeLayout) bindings[0]
            , (com.etrade.mobilepro.ui.multitoggle.MultiToggle) bindings[2]
            );
        this.actionCustomizeView.setTag(null);
        this.selectStandardOptionView.setTag(null);
        this.selectorContainer.setTag(null);
        this.tabToggle.setTag(null);
        setRootTag(root);
        // listeners
        mCallback3 = new com.etrade.mobilepro.tableview.controlbar.generated.callback.OnClickListener(this, 3);
        mCallback1 = new com.etrade.mobilepro.tableview.controlbar.generated.callback.OnClickListener(this, 1);
        mCallback2 = new com.etrade.mobilepro.tableview.controlbar.generated.callback.OnIndexToggledListener(this, 2);
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x10L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.viewModel == variableId) {
            setViewModel((com.etrade.mobilepro.tableview.controlbar.TableViewControlBarViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setViewModel(@Nullable com.etrade.mobilepro.tableview.controlbar.TableViewControlBarViewModel ViewModel) {
        this.mViewModel = ViewModel;
        synchronized(this) {
            mDirtyFlags |= 0x8L;
        }
        notifyPropertyChanged(BR.viewModel);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeViewModelTableTypeSelectedIndex((androidx.lifecycle.LiveData<java.lang.Integer>) object, fieldId);
            case 1 :
                return onChangeViewModelEnableCustomizeSettings((androidx.lifecycle.LiveData<java.lang.Boolean>) object, fieldId);
            case 2 :
                return onChangeViewModelDropdownSelectedText((androidx.lifecycle.LiveData<java.lang.String>) object, fieldId);
        }
        return false;
    }
    private boolean onChangeViewModelTableTypeSelectedIndex(androidx.lifecycle.LiveData<java.lang.Integer> ViewModelTableTypeSelectedIndex, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelEnableCustomizeSettings(androidx.lifecycle.LiveData<java.lang.Boolean> ViewModelEnableCustomizeSettings, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeViewModelDropdownSelectedText(androidx.lifecycle.LiveData<java.lang.String> ViewModelDropdownSelectedText, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        androidx.lifecycle.LiveData<java.lang.Integer> viewModelTableTypeSelectedIndex = null;
        java.lang.String viewModelDropdownSelectedTextGetValue = null;
        int androidxDatabindingViewDataBindingSafeUnboxViewModelTableTypeSelectedIndex = 0;
        float viewModelEnableCustomizeSettingsFloat10FFloat03F = 0f;
        java.lang.Integer viewModelTableTypeSelectedIndexGetValue = null;
        boolean viewModelEnableTableTypeSelection = false;
        boolean androidxDatabindingViewDataBindingSafeUnboxViewModelEnableCustomizeSettingsGetValue = false;
        androidx.lifecycle.LiveData<java.lang.Boolean> viewModelEnableCustomizeSettings = null;
        java.lang.Boolean viewModelEnableCustomizeSettingsGetValue = null;
        com.etrade.mobilepro.tableview.controlbar.TableViewControlBarViewModel viewModel = mViewModel;
        boolean viewModelEnableDropDown = false;
        androidx.lifecycle.LiveData<java.lang.String> viewModelDropdownSelectedText = null;

        if ((dirtyFlags & 0x1fL) != 0) {


            if ((dirtyFlags & 0x19L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.tableTypeSelectedIndex
                        viewModelTableTypeSelectedIndex = viewModel.getTableTypeSelectedIndex();
                    }
                    updateLiveDataRegistration(0, viewModelTableTypeSelectedIndex);


                    if (viewModelTableTypeSelectedIndex != null) {
                        // read viewModel.tableTypeSelectedIndex.getValue()
                        viewModelTableTypeSelectedIndexGetValue = viewModelTableTypeSelectedIndex.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.tableTypeSelectedIndex.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelTableTypeSelectedIndex = androidx.databinding.ViewDataBinding.safeUnbox(viewModelTableTypeSelectedIndexGetValue);
            }
            if ((dirtyFlags & 0x18L) != 0) {

                    if (viewModel != null) {
                        // read viewModel.enableTableTypeSelection
                        viewModelEnableTableTypeSelection = viewModel.getEnableTableTypeSelection();
                        // read viewModel.enableDropDown
                        viewModelEnableDropDown = viewModel.getEnableDropDown();
                    }
            }
            if ((dirtyFlags & 0x1aL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.enableCustomizeSettings
                        viewModelEnableCustomizeSettings = viewModel.getEnableCustomizeSettings();
                    }
                    updateLiveDataRegistration(1, viewModelEnableCustomizeSettings);


                    if (viewModelEnableCustomizeSettings != null) {
                        // read viewModel.enableCustomizeSettings.getValue()
                        viewModelEnableCustomizeSettingsGetValue = viewModelEnableCustomizeSettings.getValue();
                    }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.enableCustomizeSettings.getValue())
                    androidxDatabindingViewDataBindingSafeUnboxViewModelEnableCustomizeSettingsGetValue = androidx.databinding.ViewDataBinding.safeUnbox(viewModelEnableCustomizeSettingsGetValue);
                if((dirtyFlags & 0x1aL) != 0) {
                    if(androidxDatabindingViewDataBindingSafeUnboxViewModelEnableCustomizeSettingsGetValue) {
                            dirtyFlags |= 0x40L;
                    }
                    else {
                            dirtyFlags |= 0x20L;
                    }
                }


                    // read androidx.databinding.ViewDataBinding.safeUnbox(viewModel.enableCustomizeSettings.getValue()) ? 1.0F : 0.3F
                    viewModelEnableCustomizeSettingsFloat10FFloat03F = ((androidxDatabindingViewDataBindingSafeUnboxViewModelEnableCustomizeSettingsGetValue) ? (1.0F) : (0.3F));
            }
            if ((dirtyFlags & 0x1cL) != 0) {

                    if (viewModel != null) {
                        // read viewModel.dropdownSelectedText
                        viewModelDropdownSelectedText = viewModel.getDropdownSelectedText();
                    }
                    updateLiveDataRegistration(2, viewModelDropdownSelectedText);


                    if (viewModelDropdownSelectedText != null) {
                        // read viewModel.dropdownSelectedText.getValue()
                        viewModelDropdownSelectedTextGetValue = viewModelDropdownSelectedText.getValue();
                    }
            }
        }
        // batch finished
        if ((dirtyFlags & 0x10L) != 0) {
            // api target 1

            this.actionCustomizeView.setOnClickListener(mCallback3);
            this.selectStandardOptionView.setOnClickListener(mCallback1);
            com.etrade.mobilepro.ui.multitoggle.MultiToggleKt.setOnToggleListener(this.tabToggle, mCallback2);
        }
        if ((dirtyFlags & 0x1aL) != 0) {
            // api target 11
            if(getBuildSdkInt() >= 11) {

                this.actionCustomizeView.setAlpha(viewModelEnableCustomizeSettingsFloat10FFloat03F);
            }
            // api target 1

            this.actionCustomizeView.setEnabled(androidxDatabindingViewDataBindingSafeUnboxViewModelEnableCustomizeSettingsGetValue);
        }
        if ((dirtyFlags & 0x18L) != 0) {
            // api target 1

            this.selectStandardOptionView.setEnabled(viewModelEnableDropDown);
            this.tabToggle.setEnabled(viewModelEnableTableTypeSelection);
        }
        if ((dirtyFlags & 0x1cL) != 0) {
            // api target 1

            com.etrade.mobilepro.tableview.controlbar.TableviewControlBarBindingsKt.bindDropdownText(this.selectStandardOptionView, viewModelDropdownSelectedTextGetValue);
        }
        if ((dirtyFlags & 0x19L) != 0) {
            // api target 1

            com.etrade.mobilepro.ui.multitoggle.MultiToggleKt.setToggledIndex(this.tabToggle, androidxDatabindingViewDataBindingSafeUnboxViewModelTableTypeSelectedIndex);
        }
    }
    // Listener Stub Implementations
    // callback impls
    public final void _internalCallbackOnClick(int sourceId , android.view.View callbackArg_0) {
        switch(sourceId) {
            case 3: {
                // localize variables for thread safety
                // viewModel
                com.etrade.mobilepro.tableview.controlbar.TableViewControlBarViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.onCustomizeViewClick();
                }
                break;
            }
            case 1: {
                // localize variables for thread safety
                // viewModel
                com.etrade.mobilepro.tableview.controlbar.TableViewControlBarViewModel viewModel = mViewModel;
                // viewModel != null
                boolean viewModelJavaLangObjectNull = false;



                viewModelJavaLangObjectNull = (viewModel) != (null);
                if (viewModelJavaLangObjectNull) {


                    viewModel.onDropDownClick();
                }
                break;
            }
        }
    }
    public final void _internalCallbackOnIndexToggled(int sourceId , int callbackArg_0) {
        // localize variables for thread safety
        // viewModel
        com.etrade.mobilepro.tableview.controlbar.TableViewControlBarViewModel viewModel = mViewModel;
        // viewModel != null
        boolean viewModelJavaLangObjectNull = false;



        viewModelJavaLangObjectNull = (viewModel) != (null);
        if (viewModelJavaLangObjectNull) {



            viewModel.onTableTypeChange(callbackArg_0);
        }
    }
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): viewModel.tableTypeSelectedIndex
        flag 1 (0x2L): viewModel.enableCustomizeSettings
        flag 2 (0x3L): viewModel.dropdownSelectedText
        flag 3 (0x4L): viewModel
        flag 4 (0x5L): null
        flag 5 (0x6L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.enableCustomizeSettings.getValue()) ? 1.0F : 0.3F
        flag 6 (0x7L): androidx.databinding.ViewDataBinding.safeUnbox(viewModel.enableCustomizeSettings.getValue()) ? 1.0F : 0.3F
    flag mapping end*/
    //end
}