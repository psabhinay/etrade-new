package com.etrade.mobilepro.transfermoney.usecase.preview

import com.etrade.mobilepro.transfermoney.api.TransferRepeat

sealed class GetQuickTransferPreviewResult {

    data class Success(
        val transferFrom: String,
        val transferTo: String,
        val repeat: TransferRepeat,
        val isIraAccount: Boolean,
        val message: String?,
        val amount: String?,
        val scheduleDate: String?,
        val headerMessage: String?,
        val footerMessage: String?
    ) : GetQuickTransferPreviewResult()

    data class Failed(val errorCode: String?, val errorDescription: String?) : GetQuickTransferPreviewResult()
}
