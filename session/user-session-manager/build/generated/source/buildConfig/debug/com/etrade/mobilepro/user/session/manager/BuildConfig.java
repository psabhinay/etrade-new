/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.etrade.mobilepro.user.session.manager;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.etrade.mobilepro.user.session.manager";
  public static final String BUILD_TYPE = "debug";
}
