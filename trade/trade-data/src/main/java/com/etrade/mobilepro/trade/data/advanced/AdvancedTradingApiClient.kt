package com.etrade.mobilepro.trade.data.advanced

import com.etrade.mobilepro.backends.api.ServerMessageException
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.api.AdvancedTradeType.Contingent
import com.etrade.mobilepro.trade.api.OrderPlaceResponse
import com.etrade.mobilepro.trade.api.OrderPreviewResponse
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Path

internal interface AdvancedTradingApiClient {

    @JsonMoshi
    @FormUrlEncoded
    @POST("e/t/mobile/{path}createentry.json")
    suspend fun previewOrder(@Path("path") path: String, @FieldMap tradingParams: Map<String, String>): AdvancedOrderPreviewResponseDto

    @JsonMoshi
    @FormUrlEncoded
    @POST("e/t/mobile/{path}preview.json")
    suspend fun placeOrder(@Path("path") path: String, @FieldMap tradingParams: Map<String, String>): AdvancedOrderPlaceResponseDto

    companion object {
        const val PATH_CONTINGENT = "contingent"
        const val PATH_OTA = "ota"
    }
}

internal suspend fun AdvancedTradingApiClient.preview(
    path: String,
    params: Map<String, String>,
    tradeType: AdvancedTradeType = AdvancedTradeType.None
): ETResult<OrderPreviewResponse> {
    return runCatchingET {
        val response = previewOrder(path, params)
        val errors = response.errors
        if (errors.isEmpty()) {
            when (tradeType) {
                Contingent -> ContingentOrderPreviewResponseWrapper(response)
                else -> AdvancedOrderPreviewResponseWrapper(response)
            }
        } else {
            throw ServerMessageException(errors)
        }
    }
}

internal suspend fun AdvancedTradingApiClient.place(
    path: String,
    params: Map<String, String>,
    tradeType: AdvancedTradeType = AdvancedTradeType.None
): ETResult<OrderPlaceResponse> {
    return runCatchingET {
        val response = placeOrder(path, params)
        val errors = response.errors
        if (errors.isEmpty()) {
            when (tradeType) {
                Contingent -> ContingentOrderPlaceResponseWrapper(response)
                else -> response
            }
        } else {
            throw ServerMessageException(errors)
        }
    }
}
