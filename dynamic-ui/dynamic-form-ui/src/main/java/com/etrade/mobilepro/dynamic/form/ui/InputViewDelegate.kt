package com.etrade.mobilepro.dynamic.form.ui

import android.view.View
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.api.Input

typealias TitleSetter = ((title: CharSequence?) -> Unit)?

class InputViewDelegate(private val view: View, titleSetter: TitleSetter = null) : ComponentView<Input<*>> {

    private val componentViewDelegate: ComponentViewDelegate = ComponentViewDelegate(view)

    private val isEnabledObserver: Observer<Boolean> = Observer {
        view.isEnabled = it
    }

    private val titleObserver: Observer<CharSequence?> = Observer {
        titleSetter?.invoke(it)
    }

    private val contentDescriptionObserver: Observer<CharSequence?> = Observer {
        view.contentDescription = it
    }

    private var input: Input<*>? = null

    override fun render(component: Input<*>, owner: LifecycleOwner) {
        componentViewDelegate.render(component, owner)

        input?.apply {
            isEnabled.removeObserver(isEnabledObserver)
            title.removeObserver(titleObserver)
            contentDescription.removeObserver(contentDescriptionObserver)
        }

        input = component

        component.apply {
            isEnabled.observe(owner, isEnabledObserver)
            title.observe(owner, titleObserver)
            contentDescription.observe(owner, contentDescriptionObserver)
        }
    }
}
