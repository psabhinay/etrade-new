package com.etrade.mobilepro.earnings.repo

import androidx.annotation.IntRange
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.earnings.EarningsDetailData
import com.etrade.mobilepro.earnings.EarningsResponseResult
import com.etrade.mobilepro.earnings.dto.EarningsDetailResponse
import com.etrade.mobilepro.earnings.dto.EarningsResponse
import com.etrade.mobilepro.earnings.dto.createEarningsDetailRequest
import com.etrade.mobilepro.earnings.rest.EarningsDetailsApiService
import com.etrade.mobilepro.earnings.rest.PERIOD_QUARTERLY
import retrofit2.Retrofit
import javax.inject.Inject

private const val FISCAL_QUARTER_LENGTH = 3
private const val INCREMENT_TO_WHOLE = 1

class DefaultEarningsRepoImpl @Inject constructor(
    private val retrofit: Retrofit,
    private val mobileTradeRetrofit: Retrofit,
    private val user: () -> String?
) : EarningsRepo {

    private val earningsDetailsApiService by lazy {
        retrofit.create(EarningsDetailsApiService::class.java)
    }

    private val earningsDetailsApiServiceV2 by lazy {
        mobileTradeRetrofit.create(EarningsDetailsApiService::class.java)
    }

    /**
     *  https://jira.corp.etradegrp.com/browse/ETAND-19863
     *  move this to v2 after API changes.
     */
    override suspend fun loadEarnings(symbol: String, periodType: String, periodCount: Int): ETResult<EarningsResponseResult> {
        return when (periodType) {
            PERIOD_QUARTERLY -> earningsDetailsApiServiceV2.runCatchingET {
                getMarketQuarterlyEarning(createEarningsDetailRequest(symbol, periodType, periodCount))
            }.map(::mapDtoToModel)
            else -> earningsDetailsApiService.runCatchingET {
                getMarketEarning(user.invoke(), symbol, periodType, periodCount)
            }.map(::mapMarketPeriodDtoToModel)
        }
    }

    private fun mapMarketPeriodDtoToModel(earningsQuarterlyResponse: EarningsResponse): EarningsResponseResult {
        return EarningsResponseResult(
            earningsQuarterlyResponse.response?.detail?.firstOrNull()?.period?.map { qtrResponse ->
                EarningsDetailData(
                    high = qtrResponse.high,
                    low = qtrResponse.low,
                    estMean = qtrResponse.estMean,
                    quarter = qtrResponse.quarter,
                    expectedDate = qtrResponse.expectedDate,
                    announceDate = qtrResponse.announceDate,
                    announceTime = qtrResponse.announceTime,
                    actualValue = qtrResponse.actualValue,
                    surpriseMean = qtrResponse.surpriseMean,
                    fiscalPeriodYear = qtrResponse.fiscalPeriodYear,
                    dateTime = qtrResponse.dateTime
                )
            }
        )
    }

    private fun mapDtoToModel(earningsDetailResponse: EarningsDetailResponse): EarningsResponseResult {
        return EarningsResponseResult(
            earningsDetailResponse.response?.detail?.firstOrNull()?.referenceData?.map { qtrResponse ->
                EarningsDetailData(
                    high = qtrResponse.epsHigh,
                    low = qtrResponse.epsLow,
                    estMean = qtrResponse.estMean,
                    quarter = getFiscalQuarter(qtrResponse.fiscalPeriodMonth).toString(),
                    expectedDate = qtrResponse.qtr1ExpectedDate,
                    announceDate = qtrResponse.announceDate,
                    announceTime = qtrResponse.announceTime,
                    actualValue = qtrResponse.actualValue,
                    surpriseMean = qtrResponse.surpriseMean,
                    fiscalPeriodYear = qtrResponse.fiscalPeriod,
                    dateTime = null
                )
            }
        )
    }

    @IntRange(from = 1, to = 4)
    private fun getFiscalQuarter(@IntRange(from = 1, to = 12) fiscalPeriodMonth: Int?): Int? = fiscalPeriodMonth?.let {
        if (it % FISCAL_QUARTER_LENGTH == 0) {
            it / FISCAL_QUARTER_LENGTH
        } else {
            (it / FISCAL_QUARTER_LENGTH) + INCREMENT_TO_WHOLE
        }
    }
}
