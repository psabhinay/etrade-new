package com.etrade.mobilepro.menu.mapper

import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.backends.mgs.MobileScreen
import com.etrade.mobilepro.menu.dto.DataDetail
import com.etrade.mobilepro.menu.dto.MenuDto
import com.etrade.mobilepro.menu.dto.MenuItem
import com.etrade.mobilepro.menu.repo.DynamicMenuItem
import com.etrade.mobilepro.menu.repo.MenuItemType
import com.etrade.mobilepro.util.Resource
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue

internal fun getDynamicMenuResponse(): Resource<BaseMobileResponse> {
    val dynamicMenuItem = createDynamicMenuItemDtos()
    val dynamicMenuResponse = BaseMobileResponse(
        screen = MobileScreen(
            views = dynamicMenuItem,
            references = listOf(),
            meta = null
        )
    )
    return Resource.Success(dynamicMenuResponse)
}

internal fun List<MenuDto>.verifyDynamicMenuItems(expectedDynamicMenuItems: List<DynamicMenuItem>) {
    val initialMenuItems = mutableListOf<MenuItem>()
    forEach { menuDto ->
        initialMenuItems.addAll(menuDto.data.dynamicMenuItems)
        initialMenuItems.add(
            MenuItem(
                label = "",
                menuType = "divider"
            )
        )
    }
    initialMenuItems.removeAt(initialMenuItems.size - 1)
    // the size of expectedDynamicMenuItems = the size of initialDynamicMenuItems + size of dividers
    assertEquals(initialMenuItems.size, expectedDynamicMenuItems.size)

    val zipList = initialMenuItems.zip(expectedDynamicMenuItems)
    zipList.forEach { pair ->
        if (pair.second.menuItemType != MenuItemType.Divider) {
            val initialMenuItem = pair.first
            val expectedMenuItem = pair.second
            assertTrue(MenuItemType.valueOf(initialMenuItem.menuType) == expectedMenuItem.menuItemType)
            assertTrue(initialMenuItem.label == expectedMenuItem.label)
            assertTrue(initialMenuItem.subLabel == expectedMenuItem.subLabel)
            assertTrue(initialMenuItem.icon == expectedMenuItem.icon)
            val isTheSameDeepLink = initialMenuItem.ctaAction?.appUrl == expectedMenuItem.ctaDeepLink ||
                initialMenuItem.ctaAction?.toWebViewDeepLink() == expectedMenuItem.ctaDeepLink ||
                initialMenuItem.ctaAction?.toWebDeepLink() == expectedMenuItem.ctaDeepLink
            assertTrue(isTheSameDeepLink)
        }
    }
}

private fun createDynamicMenuItemDtos(): List<MenuDto> {
    val views = mutableListOf<MenuDto>()
    val type = "menu_section"
    // Alert
    val alertsMenuDetail = DataDetail(
        listOf(
            MenuItem(
                menuType = "MenuItemViewAlertCount",
                label = "Alerts",
                icon = "ic_alerts",
                ctaAction = ClickActionDto(appUrl = "etrade://show/alerts")
            )
        )
    )
    views.add(MenuDto(data = alertsMenuDetail, type = type))
    // Check Deposit
    val checkDepositMenuDetail = DataDetail(
        listOf(
            MenuItem(
                menuType = "MenuItemView",
                label = "Check Deposit",
                icon = "ic_check_deposit",
                ctaAction = ClickActionDto(appUrl = "etrade://show/checkDeposit")
            ),
            MenuItem(
                menuType = "MenuItemView",
                label = "Transfer",
                icon = "ic_transfer",
                ctaAction = ClickActionDto(appUrl = "etrade://menu/transfer")
            )
        )
    )
    views.add(MenuDto(data = checkDepositMenuDetail, type = type))
    // My Profile
    val myProfileMenuDetail = DataDetail(
        listOf(
            MenuItem(
                menuType = "MenuItemView",
                label = "Profile",
                icon = "ic_profile",
                ctaAction = ClickActionDto(appUrl = "etrade://menu/myProfile")
            ),
            MenuItem(
                menuType = "MenuItemView",
                label = "Settings",
                icon = "ic_settings",
                ctaAction = ClickActionDto(appUrl = "etrade://show/settings")
            ),
            MenuItem(
                menuType = "MenuItemView",
                label = "Customer Service",
                icon = "ic_customer_service",
                ctaAction = ClickActionDto(appUrl = "etrade://menu/customerService")
            )
        )
    )
    views.add(MenuDto(data = myProfileMenuDetail, type = type))

    // Quotes
    val quotesMenuDetail = DataDetail(
        listOf(
            MenuItem(
                menuType = "MenuItemView",
                label = "Quotes",
                icon = "ic_quotes",
                ctaAction = ClickActionDto(appUrl = "etrade://show/quotes")
            ),
            MenuItem(
                menuType = "MenuItemView",
                label = "News",
                icon = "ic_news",
                ctaAction = ClickActionDto(appUrl = "etrade://show/news")
            ),
            MenuItem(
                menuType = "MenuItemView",
                label = "Bloomberg TV",
                icon = "ic_bloombergtv",
                ctaAction = ClickActionDto(appUrl = "etrade://show/bloombergTV")
            ),
            MenuItem(
                menuType = "MenuItemView",
                label = "Education Video",
                icon = "ic_education",
                ctaAction = ClickActionDto(appUrl = "etrade://show/educationVideos")
            ),
            MenuItem(
                menuType = "MenuItemView",
                label = "Calendar",
                icon = "ic_calendar",
                ctaAction = ClickActionDto(appUrl = "etrade://show/calendar")
            ),
            MenuItem(
                menuType = "MenuItemView",
                label = "Screeners",
                icon = "ic_screener",
                ctaAction = ClickActionDto(appUrl = "etrade://show/screeners")
            ),
            MenuItem(
                menuType = "MenuItemViewWithArrowAndSubtitle_Bg_Purple",
                label = "Core Portfolios",
                subLabel = "Need help getting started?",
                ctaAction = ClickActionDto()
            ),
            MenuItem(
                menuType = "MenuItemView",
                label = "Knowledge Base",
                icon = "ic_knowledge_base",
                ctaAction = ClickActionDto(webUrl = "/knowledge/investing-basics")
            ),
            MenuItem(
                menuType = "MenuItemView",
                label = "Open New Account",
                icon = "ic_open_account",
                ctaAction = ClickActionDto(appUrl = "etrade://show/openAccount")
            )
        )
    )
    views.add(MenuDto(data = quotesMenuDetail, type = type))
    return views
}
