package com.etrade.mobilepro.login.usecase

import com.etrade.mobilepro.biometric.api.BiometricsProvider
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.util.UseCase
import javax.inject.Inject

interface DisableBiometricLoginIfRequiredUseCase : UseCase<Unit, ETResult<Unit>>

class DisableBiometricLoginIfRequiredUseCaseImpl @Inject constructor(
    private val loginPreferences: LoginPreferencesRepo,
    private val biometricsProvider: BiometricsProvider
) : DisableBiometricLoginIfRequiredUseCase {
    override suspend fun execute(parameter: Unit) = runCatchingET {
        if (loginPreferences.isBiometricAuthenticationEnabled && !biometricsProvider.canAuthenticate().isSuccessful) {
            loginPreferences.disableBiometricAuthentication()
        }
    }
}
