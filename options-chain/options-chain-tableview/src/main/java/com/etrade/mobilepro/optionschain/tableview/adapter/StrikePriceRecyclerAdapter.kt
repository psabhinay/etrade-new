package com.etrade.mobilepro.optionschain.tableview.adapter

import android.content.Context
import android.view.ViewGroup
import com.etrade.mobilepro.optionschain.tableview.databinding.OptionsChainStrikePriceItemBinding
import com.etrade.mobilepro.optionschain.tableview.viewholder.StrikePriceRecyclerViewHolder
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.etrade.mobilepro.util.android.extension.viewBinding
import java.math.BigDecimal

/**
 * Adapter class for Strike Price Recycler
 */
class StrikePriceRecyclerAdapter(
    context: Context
) : AbstractOptionChainAdapter<BigDecimal, StrikePriceRecyclerViewHolder>(context) {

    private var strikePrices: List<BigDecimal> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StrikePriceRecyclerViewHolder {
        return StrikePriceRecyclerViewHolder(parent.viewBinding(OptionsChainStrikePriceItemBinding::inflate))
    }

    override fun getItemCount(): Int = strikePrices.size

    override fun StrikePriceRecyclerViewHolder.bindWithData(data: BigDecimal) {
        bind(data)
    }

    override fun StrikePriceRecyclerViewHolder.bind(position: Int) {
        bindWithData(strikePrices[position])
    }

    fun setData(data: List<BigDecimal>) {
        val oldData = strikePrices
        strikePrices = data
        dispatchUpdates(oldData, strikePrices)
    }
}
