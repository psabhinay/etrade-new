package com.etrade.mobilepro.streaming.lightstreamer.level1

import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.util.domain.data.derivePriceToEarnings
import org.threeten.bp.LocalDate
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

private const val YEAR_DATE_TEMPLATE = "dd MMM yyyy"

object YearDateParser {

    private val timeFormat = SimpleDateFormat(YEAR_DATE_TEMPLATE, Locale.US)
    private val calendar = Calendar.getInstance()

    fun parse(date: String): LocalDate? {
        return runCatching {
            // We have to use here SimpleDateFormat as org.threeten.bp.format.DateTimeFormatter
            // cannot parse month if it is presented in upper case, e.g. "NOV".
            timeFormat.parse(date).let {
                calendar.time = it
                LocalDate.of(
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH) + 1,
                    calendar.get(Calendar.DAY_OF_MONTH)
                )
            }
        }.getOrNull()
    }
}

internal fun Level1Data.calculatePriceToEarnings(fields: Set<Level1Field>): String? =
    if (fields.contains(Level1Field.PRICE_TO_EARNINGS)) {
        derivePriceToEarnings(earningsPerShare, lastPrice)?.toPlainString()
    } else {
        null
    }
