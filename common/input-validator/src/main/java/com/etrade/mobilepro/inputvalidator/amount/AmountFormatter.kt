package com.etrade.mobilepro.inputvalidator.amount

import com.etrade.mobilepro.inputvalueview.InputFieldManager

class AmountFormatter(private val amountFormat: String) : InputFieldManager.Formatter {
    override fun formatRawContent(rawContent: String): String {
        return if (!rawContent.startsWith("$") && rawContent.isNotBlank()) {
            String.format(amountFormat, rawContent)
        } else {
            rawContent
        }
    }
}
