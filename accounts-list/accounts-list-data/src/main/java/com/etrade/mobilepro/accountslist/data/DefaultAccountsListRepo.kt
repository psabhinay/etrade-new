package com.etrade.mobilepro.accountslist.data

import com.etrade.mobile.power.backends.neo.UsServiceException
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.accountslist.api.AccountsListRepo
import com.etrade.mobilepro.accountslist.api.AccountsListResult
import com.etrade.mobilepro.accountslist.data.dto.AccountReferenceDto
import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.mgs.MobileScreen
import com.etrade.mobilepro.common.di.RemoteDynamicScreen
import com.etrade.mobilepro.dynamic.referencehelper.ReferenceHelper
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.MessageType
import com.etrade.mobilepro.inboxmessages.userType
import com.etrade.mobilepro.msaccounts.api.MsUserPreferences
import com.etrade.mobilepro.personalnotifications.data.dto.PersonalNotificationsListViewDto
import com.etrade.mobilepro.personalnotifications.data.getEbmNotification
import com.etrade.mobilepro.personalnotifications.data.getInlineAndBannerNotifications
import com.etrade.mobilepro.personalnotifications.data.mapToPersonalNotificationList
import com.etrade.mobilepro.util.Resource
import com.etrade.msaccounts.dto.UserPreferencesDto
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Custom Accounts List Repo to achieve both mapping the views to personal notifications list and
 * to persist the accounts data in the references. Using the DefaultDynamicScreenRepo
 * would need the dynamic widget creation which is unnecessary for personal notifications.
 */

class DefaultAccountsListRepo @Inject constructor(
    @RemoteDynamicScreen private val screenDataSource: ScreenDataSource,
    private val inboxMessagesRepo: InboxMessagesRepository,
    private val referenceHelper: ReferenceHelper
) : AccountsListRepo {

    @SuppressWarnings("LongMethod", "ComplexMethod")
    override fun getScreen(cacheExpiration: Long?): Observable<Resource<AccountsListResult>> {
        return screenDataSource.getScreen(
            ServicePath.AccountList,
            cacheExpiration = cacheExpiration
        )
            .observeOn(Schedulers.io())
            .map { screenListResource ->
                val screen = screenListResource.data?.screen
                val accounts = screen?.references?.filterIsInstance<AccountReferenceDto>()?.get(0)
                    ?.convertToModel()
                val localyticsNotifications = inboxMessagesRepo.getMessagesBlocking(
                    type = MessageType.DYNAMIC_NOTIFICATIONS,
                    place = null,
                    userType = accounts?.userType()
                ).getOrNull().orEmpty()
                val result = screen.toAccountListResult(localyticsNotifications)
                val errors = screenListResource.data?.extractError() as? ServerError.Known

                when (screenListResource) {
                    is Resource.Failed -> {
                        Resource.Failed(result)
                    }
                    is Resource.Loading, is Resource.Success -> {
                        errors?.let {
                            Resource.Failed(
                                data = result,
                                error = UsServiceException(
                                    errorMessage = errors.messages[0].text,
                                    errorCode = screenListResource.data?.getFirstMessageCode()
                                )
                            )
                        } ?: when (screenListResource) {
                            is Resource.Success -> {
                                screen?.references?.let { references ->
                                    references.forEach(referenceHelper::persist)
                                    Resource.Success(result)
                                } ?: Resource.Failed(result)
                            }
                            else -> screenListResource.map { result }
                        }
                    }
                }
            }
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun MobileScreen?.toAccountListResult(localyticsNotifications: List<InboxMessage>): AccountsListResult {
        return AccountsListResult(
            msUserPreferences = this?.references
                ?.filterIsInstance<UserPreferencesDto>()
                ?.firstOrNull()
                ?.convertToModel()
                ?: MsUserPreferences(),
            personalNotifications = localyticsNotifications.getEbmNotification() + this?.views
                ?.filterIsInstance<PersonalNotificationsListViewDto>()
                ?.firstOrNull()
                ?.mapToPersonalNotificationList()
                .orEmpty() + localyticsNotifications.getInlineAndBannerNotifications()
        )
    }
}
