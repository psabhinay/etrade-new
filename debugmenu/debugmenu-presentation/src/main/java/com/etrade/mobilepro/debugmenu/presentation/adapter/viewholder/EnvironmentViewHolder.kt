package com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.debugmenu.presentation.R
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemEnvironmentBinding
import com.etrade.mobilepro.debugmenu.presentation.item.EnvironmentItem

internal class EnvironmentViewHolder(
    private val binding: DebugmenuItemEnvironmentBinding,
    listener: (EnvironmentItem) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    private var item: EnvironmentItem? = null

    init {
        binding.root.setOnClickListener {
            item?.let(listener)
        }
    }

    fun bindTo(item: EnvironmentItem) {
        this.item = item
        binding.title.text = item.value.asString
        binding.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, if (item.isChecked) { R.drawable.ic_tableview_checkmark_autocolor } else { 0 }, 0)
    }
}
