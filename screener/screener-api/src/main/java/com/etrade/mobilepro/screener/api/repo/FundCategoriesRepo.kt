package com.etrade.mobilepro.screener.api.repo

import com.etrade.mobilepro.screener.api.EtfFundGroup
import com.etrade.mobilepro.screener.api.FundCategory
import com.etrade.mobilepro.screener.api.MutualFundGroup
import com.etrade.mobilepro.util.Resource
import io.reactivex.Flowable

interface FundCategoriesRepo {
    fun loadEtfFundCategories(etfFundGroup: EtfFundGroup): Flowable<Resource<List<FundCategory>>>
    fun loadMutualFundCategories(mutualFundGroup: MutualFundGroup): Flowable<Resource<List<FundCategory>>>
    fun getLoadedEtfFundCategories(etfFundGroup: EtfFundGroup): List<FundCategory>
    fun getLoadedMutualFundCategories(mutualFundGroup: MutualFundGroup): List<FundCategory>
}
