package com.etrade.mobilepro.trade.calculator

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.livedata.DistinctLiveData
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject
import javax.inject.Provider

private const val MARKET_PRICE_TOGGLE_INDEX = 0
private const val LIMIT_PRICE_TOGGLE_INDEX = 1

class TradeCalculatorViewModel @Inject constructor(
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>
) : ViewModel() {

    val applyFormParametersSignal: LiveData<ConsumableLiveEvent<InputValuesSnapshot>>
        get() = _applyFormParametersSignal
    private val _applyFormParametersSignal: MutableLiveData<ConsumableLiveEvent<InputValuesSnapshot>> = MutableLiveData()

    var wasUpdated = false
    var priceType: PriceType = PriceType.MARKET
        private set
    private lateinit var marketPrice: String

    val quantity: LiveData<String>
        get() = _quantity
    private val _quantity = MediatorLiveData<String>()

    val amount: LiveData<String>
        get() = _amount
    private val _amount = DistinctLiveData(CALCULATOR_ZERO)

    val initialAmount: LiveData<String>
        get() = _initialAmount
    private val _initialAmount = MutableLiveData(CALCULATOR_ZERO)

    val price: LiveData<String>
        get() = _price
    private val _price = DistinctLiveData<String>()

    val initialPrice: LiveData<String>
        get() = _initialPrice
    private val _initialPrice = DistinctLiveData<String>()

    val isPriceInputEnabled: LiveData<Boolean>
        get() = _isPriceInputEnabled
    private val _isPriceInputEnabled = MutableLiveData(false)

    val priceTypeSelectedIndex: LiveData<Int>
        get() = _priceTypeSelectedIndex
    private val _priceTypeSelectedIndex = DistinctLiveData<Int>()

    init {
        _quantity.apply {
            addSource(price) { updateCalculations(amount.value, it) }
            addSource(amount) { updateCalculations(it, price.value) }
        }
    }

    fun isValidQuantity() = Transformations.map(quantity) { it != INVALID_DATA && it != CALCULATOR_ZERO }

    private fun convertPriceType(priceType: PriceType) = if (priceType == PriceType.MARKET) {
        MARKET_PRICE_TOGGLE_INDEX
    } else {
        LIMIT_PRICE_TOGGLE_INDEX
    }

    fun refresh(data: Bundle?, marketPrice: String? = null) {
        marketPrice?.let { this.marketPrice = marketPrice }
        if (!wasUpdated) {
            wasUpdated = true
            setData(data)
        } else {
            setMarketPriceToFieldIfNeeded()
        }
    }

    private fun setData(data: Bundle?) {
        if (data == null) {
            this.priceType = PriceType.MARKET
            _priceTypeSelectedIndex.setValue(MARKET_PRICE_TOGGLE_INDEX)
            _isPriceInputEnabled.value = false
            _initialAmount.value = CALCULATOR_ZERO
            _initialPrice.setValue(this.marketPrice)
        } else {
            restorePriceType(data.getSerializable(KEY_PRICE_TYPE) as PriceType?)
            _initialAmount.value = data.getString(KEY_AMOUNT_VALUE)
            if (!setMarketPriceToFieldIfNeeded()) {
                data.getString(KEY_PRICE_VALUE)?.let { _initialPrice.setValue(it) }
            }
        }
    }

    private fun setMarketPriceToFieldIfNeeded() =
        if (priceType == PriceType.MARKET) {
            _initialPrice.setValue(this.marketPrice)
            true
        } else false

    private fun restorePriceType(priceType: PriceType?) {
        priceType?.let { this.priceType = it }
        _priceTypeSelectedIndex.setValue(convertPriceType(this.priceType))
        _isPriceInputEnabled.value = this.priceType == PriceType.LIMIT
    }

    private fun updateCalculations(amount: String?, price: String?) {
        _quantity.value = calculateResult(amount, price)
    }

    fun updatePrice(value: String) {
        _price.setValue(value)
    }

    fun updateAmount(value: String) {
        _amount.setValue(value)
    }

    fun apply(securityType: SecurityType) {
        _applyFormParametersSignal.value = ConsumableLiveEvent(
            tradeFormParametersBuilderProvider.get()
                .apply {
                    quantity = this@TradeCalculatorViewModel.quantity.value?.toBigDecimalOrNull()
                    priceType = this@TradeCalculatorViewModel.priceType
                    limitPrice = price.value?.toValueDigital()?.toBigDecimalOrNull()
                }
                .create(securityType)
        )
    }

    fun setPriceToggleSelected(index: Int) {
        if (index == MARKET_PRICE_TOGGLE_INDEX) {
            priceType = PriceType.MARKET
            _isPriceInputEnabled.value = false
            _initialPrice.setValue(marketPrice)
        } else {
            priceType = PriceType.LIMIT
            _isPriceInputEnabled.value = true
            _initialPrice.setValue(DEFAULT_CALCULATOR_VALUE)
        }

        _initialAmount.value = ""
    }
}
