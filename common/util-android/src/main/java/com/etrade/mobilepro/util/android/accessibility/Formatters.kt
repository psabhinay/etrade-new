package com.etrade.mobilepro.util.android.accessibility

import android.content.res.Resources
import com.etrade.mobilepro.util.android.R

private const val VISIBLE_ACCOUNT_NUMBER_LENGTH = 4

fun getPayeeAccountDescription(
    resources: Resources,
    accountName: String?,
    accountNickname: String?,
    accountNumber: String?
): String? {
    return when {
        accountNickname != null && accountNickname.isNotBlank() -> accountNickname
        accountName != null -> formatAccountNumber(resources, accountName, accountNumber)
        else -> null
    }
}

fun formatAccountNumber(resources: Resources, accountName: String, accountNumber: String?): String {
    return if (accountNumber != null && accountNumber.length >= VISIBLE_ACCOUNT_NUMBER_LENGTH) {
        resources.getString(
            R.string.payee_account_description,
            accountName,
            accountNumber.takeLast(VISIBLE_ACCOUNT_NUMBER_LENGTH).characterByCharacter()
        )
    } else {
        accountName
    }
}
