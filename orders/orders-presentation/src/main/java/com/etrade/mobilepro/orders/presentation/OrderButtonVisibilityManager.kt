package com.etrade.mobilepro.orders.presentation

import com.etrade.mobilepro.orders.api.OrderStatus

interface OrderButtonVisibilityManager {

    val isCancellable: Boolean
    val isEditable: Boolean
    val isCompleted: Boolean
    val isPreviewable: Boolean
    val isDeletable: Boolean
    val isCancelOnly: Boolean

    class Builder internal constructor() {

        var isCancellable: Boolean = false
        var isEditable: Boolean = false
        var isCompleted: Boolean = false
        var isPreviewable: Boolean = false
        var isDeletable: Boolean = false
        var isCancelOnly: Boolean = !(isEditable || isCompleted || isPreviewable || isDeletable)

        internal fun create(): OrderButtonVisibilityManager {
            return object : OrderButtonVisibilityManager {
                override val isCancellable: Boolean = this@Builder.isCancellable
                override val isEditable: Boolean = this@Builder.isEditable
                override val isCompleted: Boolean = this@Builder.isCompleted
                override val isPreviewable: Boolean = this@Builder.isPreviewable
                override val isDeletable: Boolean = this@Builder.isDeletable
                override val isCancelOnly: Boolean = this@Builder.isCancelOnly
            }
        }
    }
}

val OrderButtonVisibilityManager.isOrderSupported: Boolean
    get() = isCancellable || !isCancelOnly

fun orderButtonVisibilityManager(init: OrderButtonVisibilityManager.Builder.() -> Unit = { /* intentionally blank */ }): OrderButtonVisibilityManager {
    return OrderButtonVisibilityManager.Builder().apply(init).create()
}

fun mutualFundOrderButtonVisibilityManager(orderStatus: OrderStatus): OrderButtonVisibilityManager {
    return orderButtonVisibilityManager {
        isCancellable = orderStatus == OrderStatus.OPEN
        isCompleted = orderStatus != OrderStatus.OPEN
        isCancelOnly = orderStatus == OrderStatus.OPEN
    }
}
