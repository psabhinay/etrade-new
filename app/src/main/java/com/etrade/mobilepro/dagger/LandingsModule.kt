package com.etrade.mobilepro.dagger

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.home.model.DefaultPromptInDialogsHelper
import com.etrade.mobilepro.home.model.PromptInDialogsHelper
import com.etrade.mobilepro.landing.DefaultLandingRouter
import com.etrade.mobilepro.landing.api.LandingRouter
import com.etrade.mobilepro.landing.api.repo.LandingPagesDataSource
import com.etrade.mobilepro.landing.api.repo.LandingRepo
import com.etrade.mobilepro.landing.api.repo.UserAccountsDataSource
import com.etrade.mobilepro.landing.presentation.LandingPageSelectionFragment
import com.etrade.mobilepro.landing.presentation.LandingPageSelectionViewModel
import com.etrade.mobilepro.landing.presentation.helper.DefaultLandingPageSelectionShowHideHelper
import com.etrade.mobilepro.landing.presentation.helper.LandingPageSelectionShowHideHelper
import com.etrade.mobilepro.landing.repo.DefaultLandingPagesDataSource
import com.etrade.mobilepro.landing.repo.DefaultLandingRepo
import com.etrade.mobilepro.landing.usecase.DefaultLandingInteractor
import com.etrade.mobilepro.landing.usecase.LandingInteractor
import com.etrade.mobilepro.settings.viewmodel.datasource.DefaultUserAccountsDataSource
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
interface LandingsModule {
    @Binds
    fun bindLandingRepo(repo: DefaultLandingRepo): LandingRepo

    @Binds
    fun bindLandingInteractor(interactor: DefaultLandingInteractor): LandingInteractor

    @Binds
    fun bindUserAccountsDataSource(dataSource: DefaultUserAccountsDataSource): UserAccountsDataSource

    @Binds
    fun bindLandingPagesDataSource(dataSource: DefaultLandingPagesDataSource): LandingPagesDataSource

    @Binds
    @Singleton
    fun bindLandingRouter(router: DefaultLandingRouter): LandingRouter

    @Binds
    fun bindShowHideHelper(helper: DefaultLandingPageSelectionShowHideHelper): LandingPageSelectionShowHideHelper

    @Binds
    @Singleton
    fun bindsPromptInDialogsHelper(helper: DefaultPromptInDialogsHelper): PromptInDialogsHelper

    @Binds
    @IntoMap
    @FragmentKey(LandingPageSelectionFragment::class)
    fun bindLandingSelectionFragment(fragment: LandingPageSelectionFragment): Fragment

    @Binds
    @IntoMap
    @ViewModelKey(LandingPageSelectionViewModel::class)
    fun bindLandingSelectionViewModel(viewModel: LandingPageSelectionViewModel): ViewModel
}
