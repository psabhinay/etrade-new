package com.etrade.mobilepro.dagger

import android.content.res.Resources
import com.etrade.mobilepro.R
import com.etrade.mobilepro.alerts.api.model.UserAlertStringResources
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.alerts.data.repo.DefaultAlertsRepo
import com.etrade.mobilepro.alerts.data.rest.AlertApiClient
import com.etrade.mobilepro.alerts.presentation.base.AlertsDeleter
import com.etrade.mobilepro.alerts.presentation.base.AlertsHolder
import com.etrade.mobilepro.alerts.presentation.base.DefaultAlertsDeleter
import com.etrade.mobilepro.alerts.presentation.base.DefaultAlertsHolder
import com.etrade.mobilepro.alerts.presentation.manage.DefaultUserAlertsHolder
import com.etrade.mobilepro.alerts.presentation.manage.UserAlertsHolder
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.AlertInputValueDependenciesProvider
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.AlertSettingsHolder
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.DefaultAlertInputValueDependenciesProvider
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.DefaultAlertSettingsHolder
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.DefaultUserInputValidator
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.UserInputValidator
import com.etrade.mobilepro.common.di.MobileTrade
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class AlertsModule {
    @Provides
    @Singleton
    internal fun provideAlertApiClient(@MobileTrade retrofit: Retrofit): AlertApiClient = retrofit.create(AlertApiClient::class.java)

    @Provides
    @Singleton
    internal fun provideAlertsRepo(resources: Resources, alertApiClient: AlertApiClient): AlertsRepo = DefaultAlertsRepo(
        alertApiClient,
        UserAlertStringResources(
            targetPrice = resources.getString(R.string.target_price),
            dailyPercent = resources.getString(R.string.daily_percent),
            peTarget = resources.getString(R.string.pe_target),
            dailyVolume = resources.getString(R.string.daily_volume),
            news = resources.getString(R.string.news)
        ),
        AndroidSchedulers.mainThread()
    )

    @Provides
    internal fun provideAlertsHolder(): AlertsHolder =
        DefaultAlertsHolder()

    @Provides
    internal fun provideAlertsDeleter(alertsRepo: AlertsRepo): AlertsDeleter =
        DefaultAlertsDeleter(alertsRepo)

    @Provides
    internal fun provideAlertInputValueDepsProvider(resources: Resources): AlertInputValueDependenciesProvider =
        DefaultAlertInputValueDependenciesProvider(resources)

    @Provides
    internal fun provideInputValuesValidator(): UserInputValidator =
        DefaultUserInputValidator()

    @Provides
    internal fun provideUserAlertsHolder(): UserAlertsHolder =
        DefaultUserAlertsHolder()

    @Provides
    internal fun provideAlertSettingsHolder(): AlertSettingsHolder =
        DefaultAlertSettingsHolder()
}
