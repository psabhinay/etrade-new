package com.etrade.mobilepro.tableviewutils.presentation.cell

import com.etrade.mobilepro.util.color.DataColor

open class DataCell(
    rowId: String,
    displayText: String,
    var value: Any,
    var dataColor: DataColor = DataColor.NEUTRAL,
    displayTextContentDescription: String = displayText
) : Cell(rowId, displayText, displayTextContentDescription) {
    override fun getContent(): Any {
        return value
    }
}
