package com.etrade.mobilepro.iraaccount.data

import com.etrade.mobilepro.iraaccount.api.IraContributionDetails
import com.etrade.mobilepro.iraaccount.data.rest.IraContributionInfoResponseDto
import org.slf4j.LoggerFactory
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.DateTimeParseException
import java.util.Locale

private val logger = LoggerFactory.getLogger(ContributionDetails::class.java)

private val TAX_DUE_DATE_FORMAT by lazy { DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss", Locale.US) }

fun IraContributionInfoResponseDto.IraInfo.fromDto(): IraContributionDetails {
    val availableYears = this.availableYears.orEmpty().filterNotNull()
    return ContributionDetails(
        availableDepositTypes = availableYears + "Rollover",
        shouldShowPreviousYear = availableYears.size > 1,
        currentYearContributionDetails = this.currentYearContrib.fromDto(),
        previousYearContributionDetails = this.previousYearContribution?.fromDto()
    )
}

fun IraContributionInfoResponseDto.IraInfo.ContributionAmount.fromDto(): IraContributionDetails.Details {
    return ContributionDetails.Details(
        taxYear = this.taxYear.toString(),
        yearToDateContribution = this.contributedAmount,
        contributionLimit = this.contribMaxLimit,
        contributionAmountLeft = this.contributionAmountLeft,
        contributionDeadline = parseTaxDueDate(this.taxDueDate)
    )
}

private fun parseTaxDueDate(dueDate: String): LocalDate? = try {
    LocalDate.parse(dueDate, TAX_DUE_DATE_FORMAT)
} catch (e: DateTimeParseException) {
    logger.error("Could not parse tax due date: $dueDate", e)
    null
}
