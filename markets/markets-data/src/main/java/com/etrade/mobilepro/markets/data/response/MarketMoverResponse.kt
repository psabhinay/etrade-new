package com.etrade.mobilepro.markets.data.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MarketMoverResponse(
    @Json(name = "quote.quote")
    val quotes: List<Quotes>? = null,
    @Json(name = "errMsgBuffer")
    val errMsgBuffer: ErrMsgBuffer? = null
)
