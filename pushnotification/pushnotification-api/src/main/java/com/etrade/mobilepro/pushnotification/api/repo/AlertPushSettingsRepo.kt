package com.etrade.mobilepro.pushnotification.api.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.pushnotification.api.AlertPushSettings

interface AlertPushSettingsRepo {
    fun clearCache()
    fun getCurrentSettings(): AlertPushSettings
    suspend fun setAlertPushSettings(settings: AlertPushSettings): ETResult<AlertPushSettings>

    /**
     * Get the registered token that this device is using for smart alerts.
     *
     * @return A [ETResult.success] if there was a previously registered token, otherwise a [ETResult.failure] containing a [NoSuchElementException] if
     * no token is registered for this device or any other [Throwable] if an unknown exception occurs.
     */
    suspend fun getRegisteredToken(): ETResult<String>

    /**
     * Register the token that this device must use for smart alerts.
     *
     * @return [ETResult.success] if the token could be registered, otherwise [ETResult.failure]
     */
    suspend fun registerToken(token: String): ETResult<Unit>

    suspend fun unregisterCurrentToken(): ETResult<Unit>
}
