package com.etrade.mobilepro.quote.mapper

import com.etrade.mobilepro.holdingsapi.PositionHoldingsItem
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.quoteapi.DateStruct
import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.quoteapi.FundQuoteItem
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.QuoteChangeWidgetItem
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.quoteapi.QuoteItemInterface
import com.etrade.mobilepro.quoteapi.QuoteItems
import com.etrade.mobilepro.quoteapi.QuotePriceWidgetItem
import com.etrade.mobilepro.quoteapi.QuoteRangeBarData
import com.etrade.mobilepro.quoteapi.getTimeStamp
import com.etrade.mobilepro.quoteapi.isAffiliatedProduct
import com.etrade.mobilepro.quoteapi.isEtfSymbol
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@Suppress("LargeClass", "TooManyFunctions")
class MobileQuotePageItemMapper @Inject constructor(
    private val quoteWidgetItemMapper: MobileQuoteWidgetItemMapper,
    private val quoteDetailMapper: QuoteDetailMapper,
    private val quoteOptionDetailMapper: QuoteOptionDetailMapper,
    private val fundQuoteDetailItemsMapper: FundQuoteDetailItemsMapper
) {

    @Suppress("LongMethod")
    fun map(quote: MobileQuote, streamingEnabled: Boolean, isShowHoldings: Boolean): List<QuoteItemInterface> {
        val items: MutableList<QuoteItemInterface> = mutableListOf()
        items.add(QuoteItem.HeaderButtons(quote.typeCode))
        if (isShowHoldings) {
            items.add(QuoteItem.QuoteHoldings)
        }
        items.add(quoteWidgetItemMapper.map(quote))
        if (quote.typeCode == InstrumentType.EQ && streamingEnabled) {
            items.add(QuoteItem.QuoteMomentumBar())
        }
        if (quote.typeCode.isOption) {
            items.addAll(getOptionItemsList(quote))
        } else {
            quote.extendedQuoteHrDetail?.let {
                items.add(getClosingPriceQuote(quote))
            } ?: items.add(QuoteItem.ThinGutter)
            val symbol = SearchResultItem.Symbol(title = quote.symbol, type = quote.typeCode)
            items.add(QuoteItem.QuoteChartItem(symbol))
            items.add(QuoteItem.ThickGutter)
            if (quote.typeCode == InstrumentType.EQ || quote.typeCode == InstrumentType.INDX) {
                items.add(getRange(quote))
                if (quote.typeCode == InstrumentType.EQ) {
                    items.add(QuoteItem.ThinGutter)
                    items.add(quoteDetailMapper.map(quote))
                }
            }
            items.add(QuoteItem.ThickGutter)
        }
        return items
    }

    fun mapDualPane(quote: MobileQuote, streamingEnabled: Boolean, isShowHoldings: Boolean): Pair<QuoteItems, QuoteItems> {
        val firstPaneItems: MutableList<QuoteItemInterface> = mutableListOf()
        val secondPaneItems: MutableList<QuoteItemInterface> = mutableListOf()
        val symbol = SearchResultItem.Symbol(title = quote.symbol, type = quote.typeCode)
        firstPaneItems.add(QuoteItem.HeaderButtons(quote.typeCode))
        if (isShowHoldings) {
            firstPaneItems.add(QuoteItem.QuoteHoldings)
        }
        firstPaneItems.add(quoteWidgetItemMapper.map(quote))
        if (quote.typeCode == InstrumentType.EQ && streamingEnabled) {
            firstPaneItems.add(QuoteItem.QuoteMomentumBar())
        }
        firstPaneItems.add(QuoteItem.QuoteChartItem(symbol))
        if (quote.typeCode.isOption) {
            firstPaneItems.addAll(getOptionItemsList(quote))
        } else {
            mapStockDualPane(quote, firstPaneItems, secondPaneItems)
        }
        if (quote.isEtfSymbol()) {
            firstPaneItems.add(QuoteItem.ThickGutter)
            firstPaneItems.add(QuoteItem.QuoteEtfLegalFooterItem)
        }
        return Pair(QuoteItems(firstPaneItems), QuoteItems(secondPaneItems))
    }

    private fun mapStockDualPane(
        quote: MobileQuote,
        firstPaneItems: MutableList<QuoteItemInterface>,
        secondPaneItems: MutableList<QuoteItemInterface>
    ) {
        quote.extendedQuoteHrDetail?.let {
            firstPaneItems.add(getClosingPriceQuote(quote))
        } ?: firstPaneItems.add(QuoteItem.ThinGutter)
        secondPaneItems.add(QuoteItem.QuoteFooterItem)
        if (quote.typeCode == InstrumentType.EQ || quote.typeCode == InstrumentType.INDX) {
            firstPaneItems.add(QuoteItem.ThickGutter)
            firstPaneItems.add(getRange(quote))
            if (quote.typeCode == InstrumentType.EQ) {
                firstPaneItems.add(QuoteItem.ThickGutter)
                firstPaneItems.add(quoteDetailMapper.map(quote))
            }
        }
    }

    private fun getMutualFundArray(quote: MobileQuote): Array<List<QuoteItemInterface>> {
        val headerButtons = QuoteItem.HeaderButtons(quote.typeCode)
        val quoteDetailItem = fundQuoteDetailItemsMapper.getMutualFundQuoteDetail(quote)
        val fundPerformance = fundQuoteDetailItemsMapper.getFundCategoryPerformanceItem(quote)
        val snapShotItems: MutableList<QuoteItemInterface> = getMutualFundSnapShotItems(headerButtons, quoteDetailItem, quote)
        val performanceItems: MutableList<QuoteItemInterface> = getMutualFundPerformanceItems(headerButtons, quoteDetailItem, fundPerformance)
        val feesAndExpenses: MutableList<QuoteItemInterface> = getMutualFundFeesAndExpensesItems(headerButtons, quoteDetailItem, quote)

        return arrayOf(snapShotItems, performanceItems, feesAndExpenses)
    }

    fun getMutualFundItems(quote: MobileQuote): QuoteItems {
        return getMutualFundArray(quote).let {
            QuoteItems(it[0], it[1], it[2])
        }
    }

    private fun getMutualFundFeesAndExpensesItems(
        headerButtons: QuoteItem.HeaderButtons,
        quoteDetailItem: FundQuoteItem.MutualFundDetailItem,
        quote: MobileQuote
    ): MutableList<QuoteItemInterface> {
        val feesAndExpenses: MutableList<QuoteItemInterface> = mutableListOf()
        feesAndExpenses.add(headerButtons)
        feesAndExpenses.add(quoteDetailItem)
        feesAndExpenses.add(QuoteItem.ThickGutter)
        feesAndExpenses.add(fundQuoteDetailItemsMapper.getFeesAndExpensesItem(quote.fundDetails))
        feesAndExpenses.add(FundQuoteItem.LinksItem)
        feesAndExpenses.add(FundQuoteItem.MutualFundDisclosureTextItem)
        return feesAndExpenses
    }

    private fun getMutualFundPerformanceItems(
        headerButtons: QuoteItem.HeaderButtons,
        quoteDetailItem: FundQuoteItem.MutualFundDetailItem,
        fundPerformance: FundQuoteItem.FundPerformanceItem
    ): MutableList<QuoteItemInterface> {
        val performanceItems: MutableList<QuoteItemInterface> = mutableListOf()
        performanceItems.add(headerButtons)
        performanceItems.add(quoteDetailItem)
        performanceItems.add(fundPerformance)
        performanceItems.add(FundQuoteItem.LinksItem)
        performanceItems.add(FundQuoteItem.MutualFundDisclosureTextItem)
        return performanceItems
    }

    fun mapFundDualPane(quote: MobileQuote): Pair<QuoteItems, QuoteItems> {
        val firstPaneItems = getFundDualPaneFirstPanelItems(quote)
        val secondPaneItems = getFundDualPaneSecondPanelItems(quote)
        return Pair(firstPaneItems, QuoteItems(secondPaneItems[0], secondPaneItems[1], secondPaneItems[2]))
    }

    fun mapOptionsDualPane(quote: MobileQuote, isShowHoldings: Boolean): Pair<QuoteItems, QuoteItems> {
        val firstPaneItems = getOptionsDualPaneFirstPanelItems(quote, isShowHoldings)
        val secondPaneItems = getOptionsDualPaneSecondPanelItems(quote)
        return Pair(firstPaneItems, secondPaneItems)
    }

    private fun getOptionsDualPaneFirstPanelItems(quote: MobileQuote, isShowHoldings: Boolean): QuoteItems {
        val firstPaneItems: MutableList<QuoteItemInterface> = mutableListOf()
        val symbol = SearchResultItem.Symbol(title = quote.symbol, type = quote.typeCode)
        firstPaneItems.add(QuoteItem.HeaderButtons(quote.typeCode))
        if (isShowHoldings) {
            firstPaneItems.add(QuoteItem.QuoteHoldings)
        }
        firstPaneItems.add(quoteWidgetItemMapper.map(quote))
        firstPaneItems.add(getOptionClosingQuote(quote))
        firstPaneItems.add(QuoteItem.ThickGutter)
        firstPaneItems.add(QuoteItem.QuoteChainItem(symbol))
        firstPaneItems.add(QuoteItem.ThickGutter)
        firstPaneItems.add(quoteOptionDetailMapper.map(quote))
        firstPaneItems.add(QuoteItem.ThickGutter)
        firstPaneItems.add(QuoteItem.QuoteFooterItem)
        firstPaneItems.add(QuoteItem.QuoteDisclosureItem)
        return QuoteItems(firstPaneItems, firstPaneItems, firstPaneItems)
    }

    private fun getOptionsDualPaneSecondPanelItems(quote: MobileQuote): QuoteItems {
        val secondPaneItems: MutableList<QuoteItemInterface> = mutableListOf()
        val symbol = SearchResultItem.Symbol(title = quote.symbol, type = quote.typeCode)
        secondPaneItems.add(QuoteItem.QuoteChartItem(symbol))
        secondPaneItems.add(QuoteItem.ThickGutter)
        return QuoteItems(secondPaneItems, secondPaneItems, secondPaneItems)
    }

    fun mapIndexDualPane(quote: MobileQuote): Pair<QuoteItems, QuoteItems> {
        val firstPaneItems = getIndexDualPaneFirstPanelItems(quote)
        val secondPaneItems = getIndexDualPaneSecondPanelItems(quote)
        return Pair(firstPaneItems, secondPaneItems)
    }

    private fun getIndexDualPaneFirstPanelItems(quote: MobileQuote): QuoteItems {
        val firstPaneItems: MutableList<QuoteItemInterface> = mutableListOf()
        val symbol = SearchResultItem.Symbol(title = quote.symbol, type = quote.typeCode)
        firstPaneItems.add(QuoteItem.HeaderButtons(quote.typeCode))
        firstPaneItems.add(quoteWidgetItemMapper.map(quote))
        firstPaneItems.add(QuoteItem.ThickGutter)
        firstPaneItems.add(QuoteItem.QuoteChainItem(symbol))
        firstPaneItems.add(QuoteItem.ThickGutter)
        firstPaneItems.add(getRange(quote))
        firstPaneItems.add(QuoteItem.ThinGutter)
        return QuoteItems(firstPaneItems, firstPaneItems, firstPaneItems)
    }

    private fun getIndexDualPaneSecondPanelItems(quote: MobileQuote): QuoteItems {
        val secondPaneItems: MutableList<QuoteItemInterface> = mutableListOf()
        val symbol = SearchResultItem.Symbol(title = quote.symbol, type = quote.typeCode)
        secondPaneItems.add(QuoteItem.QuoteChartItem(symbol))
        secondPaneItems.add(QuoteItem.ThickGutter)
        return QuoteItems(secondPaneItems, secondPaneItems, secondPaneItems)
    }

    private fun getFundDualPaneFirstPanelItems(quote: MobileQuote): QuoteItems {
        val firstPaneItems: MutableList<QuoteItemInterface> = mutableListOf()
        firstPaneItems.add(QuoteItem.HeaderButtons(quote.typeCode))
        val fundClassSection = fundQuoteDetailItemsMapper.getMutualFundClassSection(quote)
        val fundIconSection = fundQuoteDetailItemsMapper.getFundIconSection(quote)
        val quoteDetailItem = fundQuoteDetailItemsMapper.getMutualFundQuoteDetail(quote)
        firstPaneItems.add(quoteDetailItem)
        firstPaneItems.add(QuoteItem.ThickGutter)
        firstPaneItems.add(fundClassSection)
        if (quote.fundDetails?.hasNoTransactionsFee == true || quote.fundDetails?.isAllStarFund == true || quote.isAffiliatedProduct()) {
            firstPaneItems.add(fundIconSection)
        }
        firstPaneItems.add(QuoteItem.ThickGutter)
        firstPaneItems.add(fundQuoteDetailItemsMapper.getQuoteFundFooterItem(quote))
        firstPaneItems.add(FundQuoteItem.LinksItem)
        if (quote.typeCode.isMutualFundFund) {
            firstPaneItems.add(FundQuoteItem.MutualFundDisclosureTextItem)
        } else {
            firstPaneItems.add(FundQuoteItem.MoneyMarketFundDisclosureTextItem)
        }

        return QuoteItems(firstPaneItems, firstPaneItems, firstPaneItems)
    }

    private fun getFundDualPaneSecondPanelItems(quote: MobileQuote): Array<List<QuoteItemInterface>> {
        val snapShotItems = getDualPaneFundSnapShotItems(quote)
        val feesAndExpenses = listOf(fundQuoteDetailItemsMapper.getFeesAndExpensesItem(quote.fundDetails))
        val performanceItems = listOf(fundQuoteDetailItemsMapper.getFundCategoryPerformanceItem(quote))

        return arrayOf(snapShotItems, performanceItems, feesAndExpenses)
    }

    private fun getDualPaneFundSnapShotItems(
        quote: MobileQuote
    ): MutableList<QuoteItemInterface> {
        val snapShotItems: MutableList<QuoteItemInterface> = mutableListOf()
        addFundSnapShotBody(quote, snapShotItems)
        return snapShotItems
    }

    private fun addFundSnapShotBody(
        quote: MobileQuote,
        snapShotItems: MutableList<QuoteItemInterface>
    ) {
        val morningStarDetails = fundQuoteDetailItemsMapper.getMorningStarDetails(quote.fundDetails)
        val summarySection = fundQuoteDetailItemsMapper.getSummarySection(quote.fundDetails)

        if (quote.typeCode.isMutualFundFund) {
            val fundMorningStarRatings = fundQuoteDetailItemsMapper.getMorningStarRatings(quote.fundDetails)
            snapShotItems.add(QuoteItem.QuoteChartItem(SearchResultItem.Symbol(title = quote.symbol, type = quote.typeCode)))
            snapShotItems.add(QuoteItem.ThickGutter)
            snapShotItems.add(fundMorningStarRatings)
            snapShotItems.add(QuoteItem.ThinGutter)
        }

        snapShotItems.add(morningStarDetails)
        snapShotItems.add(QuoteItem.ThinGutter)

        if (quote.typeCode.isMutualFundFund) {
            val quarterlyTrailingReturnsSection = fundQuoteDetailItemsMapper.getQuarterlyReturns(quote.fundDetails)
            snapShotItems.add(quarterlyTrailingReturnsSection)
            snapShotItems.add(QuoteItem.ThinGutter)
        }
        snapShotItems.add(summarySection)
    }

    private fun getMutualFundSnapShotItems(
        headerButtons: QuoteItem.HeaderButtons,
        mutualFundDetailItem: FundQuoteItem.MutualFundDetailItem,
        quote: MobileQuote
    ): MutableList<QuoteItemInterface> {
        val snapShotItems: MutableList<QuoteItemInterface> = mutableListOf()
        val fundClassSection = fundQuoteDetailItemsMapper.getMutualFundClassSection(quote)
        val fundIconSection = fundQuoteDetailItemsMapper.getFundIconSection(quote)
        val quoteFundFooterSection = fundQuoteDetailItemsMapper.getQuoteFundFooterItem(quote)

        snapShotItems.add(headerButtons)
        snapShotItems.add(mutualFundDetailItem)
        snapShotItems.add(QuoteItem.ThickGutter)
        snapShotItems.add(fundClassSection)
        if (quote.fundDetails?.hasNoTransactionsFee == true || quote.fundDetails?.isAllStarFund == true || quote.isAffiliatedProduct()) {
            snapShotItems.add(fundIconSection)
        }
        snapShotItems.add(QuoteItem.ThickGutter)
        addFundSnapShotBody(quote, snapShotItems)
        snapShotItems.add(QuoteItem.ThickGutter)
        snapShotItems.add(quoteFundFooterSection)
        snapShotItems.add(FundQuoteItem.LinksItem)
        snapShotItems.add(FundQuoteItem.MutualFundDisclosureTextItem)
        return snapShotItems
    }

    private fun getMoneyMarketFundArray(quote: MobileQuote): Array<List<QuoteItemInterface>> {
        val headerButtons = QuoteItem.HeaderButtons(quote.typeCode)
        val quoteDetailItem = fundQuoteDetailItemsMapper.getMoneyMarketFundQuoteDetail(quote)
        val fundIconSection = fundQuoteDetailItemsMapper.getFundIconSection(quote)
        val snapShotItems: MutableList<QuoteItemInterface> = getMoneyMarketFundSnapShotItems(quote, headerButtons, quoteDetailItem, fundIconSection)
        val performanceItems: MutableList<QuoteItemInterface> = getMoneyMarketFundPerformanceItems(quote, headerButtons, quoteDetailItem, fundIconSection)

        return arrayOf(snapShotItems, performanceItems)
    }

    fun getMoneyMarketFundItems(quote: MobileQuote): QuoteItems {
        return getMoneyMarketFundArray(quote).let {
            QuoteItems(it[0], it[1])
        }
    }

    private fun getMoneyMarketFundPerformanceItems(
        quote: MobileQuote,
        headerButtons: QuoteItem.HeaderButtons,
        quoteDetailItem: FundQuoteItem.MoneyMarketFundDetail,
        fundIconSection: FundQuoteItem.FundIconSection
    ): MutableList<QuoteItemInterface> {
        val performanceItems: MutableList<QuoteItemInterface> = mutableListOf()
        val fundPerformance = fundQuoteDetailItemsMapper.getFundCategoryPerformanceItem(quote)
        performanceItems.add(headerButtons)
        performanceItems.add(quoteDetailItem)
        if (quote.fundDetails?.hasNoTransactionsFee == true || quote.fundDetails?.isAllStarFund == true) {
            performanceItems.add(QuoteItem.ThinGutter)
            performanceItems.add(fundIconSection)
        }
        performanceItems.add(fundPerformance)
        performanceItems.add(FundQuoteItem.LinksItem)
        performanceItems.add(FundQuoteItem.MoneyMarketFundDisclosureTextItem)
        return performanceItems
    }

    private fun getMoneyMarketFundSnapShotItems(
        quote: MobileQuote,
        headerButtons: QuoteItem.HeaderButtons,
        quoteDetailItem: FundQuoteItem.MoneyMarketFundDetail,
        fundIconSection: FundQuoteItem.FundIconSection
    ): MutableList<QuoteItemInterface> {
        val snapShotItems: MutableList<QuoteItemInterface> = mutableListOf()
        val morningStarSection = fundQuoteDetailItemsMapper.getMorningStarDetails(quote.fundDetails)
        val summarySection = fundQuoteDetailItemsMapper.getSummarySection(quote.fundDetails)
        val quoteFundFooterSection = fundQuoteDetailItemsMapper.getQuoteFundFooterItem(quote)
        snapShotItems.add(headerButtons)
        snapShotItems.add(quoteDetailItem)
        if (quote.fundDetails?.hasNoTransactionsFee == true || quote.fundDetails?.isAllStarFund == true) {
            snapShotItems.add(QuoteItem.ThinGutter)
            snapShotItems.add(fundIconSection)
        }
        snapShotItems.add(QuoteItem.ThickGutter)
        snapShotItems.add(morningStarSection)
        snapShotItems.add(QuoteItem.ThinGutter)
        snapShotItems.add(summarySection)
        snapShotItems.add(QuoteItem.ThickGutter)
        snapShotItems.add(quoteFundFooterSection)
        snapShotItems.add(FundQuoteItem.LinksItem)
        snapShotItems.add(FundQuoteItem.MoneyMarketFundDisclosureTextItem)
        return snapShotItems
    }

    private fun getOptionItemsList(quote: MobileQuote): List<QuoteItemInterface> {
        val items: MutableList<QuoteItemInterface> = mutableListOf()
        val symbol = SearchResultItem.Symbol(title = quote.symbol, type = quote.typeCode)
        items.add(getOptionClosingQuote(quote))
        items.add(QuoteItem.QuoteChartItem(symbol))
        items.add(QuoteItem.ThickGutter)
        items.add(QuoteItem.QuoteChainItem(symbol))
        items.add(QuoteItem.ThickGutter)
        items.add(quoteOptionDetailMapper.map(quote))
        items.add(QuoteItem.ThickGutter)
        return items
    }

    private fun getClosingPriceQuote(quote: MobileQuote): QuoteItem.ClosingPriceItem {
        val closingPriceTitle = quoteWidgetItemMapper.templateResources.closingPrice
        val quotePrice = quote.quotePrice
        val changeVal = quotePrice.change.toDoubleOrNull() ?: 0.0
        val changeColor = getChangeColor(changeVal)
        val change = QuoteChangeWidgetItem(quotePrice.change, quotePrice.percentChange)
        val lastPrice = QuotePriceWidgetItem(
            price = quotePrice.lastPrice,
            priceColor = quoteWidgetItemMapper.templateResources.neutralChangeColorRes,
            exchangeCode = quotePrice.askSize,
            size = quote.tradeExchange
        )
        val volume = quotePrice.volume
        val lastTradeTimeStamp = quote.getTimeStamp(quoteWidgetItemMapper.templateResources, excludeDate = true)
        return QuoteItem.ClosingPriceItem(
            closingPriceTitle,
            quote.underlyingSymbol,
            lastPrice,
            change,
            changeColor,
            volume,
            lastTradeTimeStamp,
            quoteWidgetItemMapper.templateResources,
            isOption = false
        )
    }

    private fun getOptionClosingQuote(quote: MobileQuote): QuoteItem.ClosingPriceItem {
        val changeVal = quote.underLyingChange.toDoubleOrNull() ?: 0.0
        val changeColor = getChangeColor(changeVal)
        val change = QuoteChangeWidgetItem(quote.underLyingChange, quote.underLyingPercentChange)
        val lastPrice = QuotePriceWidgetItem(
            price = quote.underLyingLastPrice,
            priceColor = quoteWidgetItemMapper.templateResources.neutralChangeColorRes,
            exchangeCode = quote.exchangeCode,
            size = quote.tradeExchange
        )
        val closingPriceTitle = getClosingPriceTitle(quote)

        val lastTradeTimeStamp = quote.underLyingLastTradeTime?.let {
            MobileQuote.formatTimeStamp(TimeUnit.SECONDS.toMillis(it), false, quoteWidgetItemMapper.templateResources)
        }

        return QuoteItem.ClosingPriceItem(
            closingPriceTitle,
            quote.underlyingSymbol,
            lastPrice,
            change,
            changeColor,
            quote.underLyingVolume,
            lastTradeTimeStamp,
            quoteWidgetItemMapper.templateResources,
            isOption = true
        )
    }

    private fun getClosingPriceTitle(quote: MobileQuote): String {
        return if (quote.extendedQuoteHrDetail != null) {
            quoteWidgetItemMapper.templateResources.underlyerClosingPrice
        } else {
            quoteWidgetItemMapper.templateResources.underlyerPrice
        }
    }

    private fun getChangeColor(changeVal: Double): Int {
        return if (changeVal >= 0) {
            quoteWidgetItemMapper.templateResources.positiveChangeColorRes
        } else {
            quoteWidgetItemMapper.templateResources.negativeChangeColorRes
        }
    }

    fun getBaseQuoteDetails(news: List<NewsItem.Text>?): List<QuoteItemInterface> {
        val items: MutableList<QuoteItemInterface> = mutableListOf()
        when {
            news == null -> {
                items.add(QuoteItem.NewsLoading)
            }
            news.isEmpty() -> {
                items.add(NewsItem.Empty)
            }
            else -> {
                news.forEach {
                    items.add(it)
                    items.add(QuoteItem.ThinGutter)
                }
                items.add(QuoteItem.QuoteNewsFooterItem)
                items.add(QuoteItem.ThinGutter)
            }
        }
        return items
    }

    fun getFooterDetails(): List<QuoteItemInterface> =
        mutableListOf<QuoteItemInterface>().apply {
            add(QuoteItem.ThickGutter)
            add(QuoteItem.QuoteFooterItem)
        }

    fun getEtfFooterDetails(isInDualPane: Boolean): List<QuoteItemInterface> =
        if (isInDualPane) {
            mutableListOf<QuoteItemInterface>().apply {
                add(QuoteItem.ThickGutter)
                add(QuoteItem.QuoteEtfDisclosureItem)
            }
        } else {
            mutableListOf<QuoteItemInterface>().apply {
                add(QuoteItem.ThickGutter)
                add(QuoteItem.QuoteFooterItem)
                add(QuoteItem.QuoteEtfLegalFooterItem)
                add(QuoteItem.QuoteEtfDisclosureItem)
            }
        }

    fun map(errorMessage: String, noFooter: Boolean): List<QuoteItemInterface> {
        return when (noFooter) {
            false -> listOf(
                QuoteItem.NewsRetry(errorMessage),
                QuoteItem.ThickGutter,
                QuoteItem.QuoteFooterItem,
                QuoteItem.QuoteDisclosureItem
            )
            true -> listOf(QuoteItem.NewsRetry(errorMessage))
        }
    }

    fun map(holdingsItem: PositionHoldingsItem): QuoteItemInterface =
        if (holdingsItem.hasHoldings) QuoteItem.QuoteHoldings else QuoteItem.Empty

    fun map(quote: MobileQuote, option: MobileQuote): MobileQuote {
        with(option) {
            underLyingSymbolDescription = quote.description
            underLyingLastPrice = quote.quotePrice.lastPrice
            underLyingChange = quote.quotePrice.change
            underLyingPercentChange = quote.quotePrice.percentChange
            underLyingVolume = quote.quotePrice.volume
            underLyingLastTradeTime = quote.quotePrice.timeOfLastTrade
            underlyingTypeCode = quote.typeCode
            extendedQuoteHrDetail = quote.extendedQuoteHrDetail
        }
        return option
    }

    private fun getRange(quote: MobileQuote): QuoteItem.QuoteRangeBarItem {
        val dateFormat = quoteWidgetItemMapper.templateResources.dateFormat
        val rangeBarData = QuoteRangeBarData(
            week52High = quote.week52High,
            week52Low = quote.week52Low,
            dayHigh = quote.dayHigh,
            dayLow = quote.dayLow,
            week52HighDate = formatWeek52Date(dateFormat, quote.week52HighDate),
            week52LowDate = formatWeek52Date(dateFormat, quote.week52LowDate)
        )
        return QuoteItem.QuoteRangeBarItem(rangeBarData) { date -> formatWeek52Date(dateFormat, date) }
    }

    companion object {

        private fun formatWeek52Date(dateFormat: String, date: DateStruct) = String.format(dateFormat, date.month, date.day, date.year)
    }
}
