package com.etrade.mobilepro.news.presentation.customview

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.news.presentation.adapter.EmptyNewsItemViewHolder
import com.etrade.mobilepro.news.presentation.adapter.TextNewsItemViewHolder
import com.etrade.mobilepro.news.presentation.databinding.ItemTextNewsBinding
import com.etrade.mobilepro.news.presentation.dynamic.NewsItemWidgetPayload
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.extension.viewBinding

class NewsWidgetRecyclerView : RecyclerView {
    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    init {
        layoutManager = LinearLayoutManager(context)
        adapter = NewsWidgetAdapter()
        this.setupRecyclerViewDivider(R.drawable.thin_divider)
    }

    fun setItems(items: List<NewsItemWidgetPayload>) {
        (adapter as? NewsWidgetAdapter)?.setItems(items)
    }

    private class NewsWidgetAdapter : RecyclerView.Adapter<ViewHolder>() {
        private var items: List<NewsItemWidgetPayload> = emptyList()
        @Suppress("NotifyDataSetChanged")
        fun setItems(items: List<NewsItemWidgetPayload>) {
            this.items = items
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return when (viewType) {
                R.layout.item_text_news -> TextNewsItemViewHolder(
                    parent.viewBinding(
                        ItemTextNewsBinding::inflate
                    ),
                    selectNewsItemListener = null, symbolSelectListener = null
                )
                R.layout.item_empty_news -> EmptyNewsItemViewHolder(parent.inflate(viewType))
                else -> throw IllegalArgumentException("wrong news item for overview screen")
            }
        }

        override fun getItemCount(): Int = items.size

        override fun getItemViewType(position: Int): Int {
            return when (items[position].item) {
                is NewsItem.Empty -> R.layout.item_empty_news
                is NewsItem.Text -> R.layout.item_text_news
                is NewsItem.CompanyHeader -> R.layout.item_company_news_header
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val payload = items[position]
            if (holder is TextNewsItemViewHolder) {
                holder.selectNewsItemListener = { item, pos ->
                    payload.itemSelectListener?.invoke(
                        holder.itemView.findNavController(),
                        item,
                        pos
                    )
                }
                holder.symbolSelectListener = {
                    payload.symbolSelectListener?.invoke(holder.itemView, it)
                }
                holder.bindTo(payload.item, wasRead = payload.wasRead, isSelected = false)
            }
        }
    }
}
