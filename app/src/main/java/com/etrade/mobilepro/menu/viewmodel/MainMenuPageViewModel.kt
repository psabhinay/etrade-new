package com.etrade.mobilepro.menu.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.dummy.DefaultDynamicMainMenu
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.inboxmessages.InboxMessagesDynamicPayload
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.MessagePlace
import com.etrade.mobilepro.inboxmessages.MessageType
import com.etrade.mobilepro.inboxmessages.UserType
import com.etrade.mobilepro.inboxmessages.addInboxMessagesView
import com.etrade.mobilepro.inboxmessages.inboxMessagesDynamicViewLiveData
import com.etrade.mobilepro.inboxmessages.userType
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.menu.MenuDeepLink
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.MessagesMenuItem
import com.etrade.mobilepro.menu.mapper.MainDynamicMenuItemMapper
import com.etrade.mobilepro.menu.repo.DynamicMenuItem
import com.etrade.mobilepro.menu.repo.MenuRepo
import com.etrade.mobilepro.themeselection.NightModePreferences
import com.etrade.mobilepro.tracking.SCREEN_TITLE_CUSTOMER_SERVICE
import com.etrade.mobilepro.tracking.SCREEN_TITLE_MY_PROFILE
import com.etrade.mobilepro.tracking.SCREEN_TITLE_TRANSFER
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import javax.inject.Inject

private const val GENERIC_INBOX_MESSAGE_MENU_ITEM_OFFSET = 1
private const val REFER_A_FRIEND_INBOX_MESSAGE_MENU_ITEM_OFFSET = 0

@SuppressWarnings("LongParameterList")
class MainMenuPageViewModel @Inject constructor(
    context: Context,
    tracker: Tracker,
    private val inboxMessagesRepository: InboxMessagesRepository,
    private val dispatchers: DispatcherProvider,
    private val accountListRepo: AccountListRepo,
    @Web private val baseUrl: String,
    private val defaultDynamicMainMenu: DefaultDynamicMainMenu,
    private val dynamicMenuItemMapper: MainDynamicMenuItemMapper,
    private val imageLoader: ImageLoader,
    private val menuRepo: MenuRepo,
    private val nightModePreferences: NightModePreferences
) : AppBaseMenuPageViewModel(context.resources, tracker) {

    @Suppress("StaticFieldLeak")
    private val applicationContext: Context = context.applicationContext

    private val genericInboxMessagesMenuItem: LiveData<InboxMessagesDynamicPayload> by lazy {
        createInboxMessageLiveDataByMessagePlace(
            userType = accountListRepo.userType(),
            messagePlace = MessagePlace.MENU
        )
    }

    private lateinit var referAFriendInboxMessagesMenuItem: LiveData<InboxMessagesDynamicPayload>

    private val _menuPageItems: MutableLiveData<List<MenuItem>> = MutableLiveData(emptyList())
    override val menuPageItems: LiveData<List<MenuItem>>
        get() = getInboxMessageMenuItems()

    internal val viewState: LiveData<ViewState>
        get() = menuPageItems.map {
            if (it.isEmpty()) {
                ViewState.Loading
            } else {
                ViewState.Success
            }
        }
    private val logger = LoggerFactory.getLogger(MainMenuPageViewModel::class.java)
    private val menuItemDeeplinkAndLocalyticsTagMap = mapOf(
        MenuDeepLink.TRANSFER.appUrl to SCREEN_TITLE_TRANSFER,
        MenuDeepLink.USER_PROFILE.appUrl to SCREEN_TITLE_MY_PROFILE,
        MenuDeepLink.CUSTOMER_SERVICE.appUrl to SCREEN_TITLE_CUSTOMER_SERVICE
    )

    fun prefetchDynamicMenuItems() {
        viewModelScope.launch {
            menuRepo.prefetchDynamicMenuItems()
        }
    }

    fun loadDynamicMenuPageItems() {
        viewModelScope.launch {
            menuRepo.getDynamicMenuItems().fold(
                onSuccess = { dynamicMenuItems ->
                    dynamicMenuItems.setupMenuItemLocalyticsTags()
                    _menuPageItems.value = dynamicMenuItems.mapToMenuPageItems()
                },
                onFailure = {
                    logger.error("Get dynamic menu items failed", it)
                    _menuPageItems.value = getDefaultMenuPageItems()
                }
            )
        }
    }

    fun trackScreen(pageTitle: String) {
        tracker.screen(pageTitle)
    }

    private fun getDefaultMenuPageItems(): List<MenuItem> {
        val dynamicMenuItems = defaultDynamicMainMenu.createDynamicMenuItems()
        dynamicMenuItems.setupMenuItemLocalyticsTags()
        return dynamicMenuItemMapper.mapDynamicMenuItemsToPage(dynamicMenuItems)
    }

    private fun List<DynamicMenuItem>.mapToMenuPageItems(): List<MenuItem> {
        return if (this.isEmpty()) {
            getDefaultMenuPageItems()
        } else {
            dynamicMenuItemMapper.mapDynamicMenuItemsToPage(this)
        }
    }

    private fun List<DynamicMenuItem>.setupMenuItemLocalyticsTags() {
        this.map { dynamicMenuItem ->
            val localyticsTag = menuItemDeeplinkAndLocalyticsTagMap[dynamicMenuItem.ctaDeepLink]
            localyticsTag?.let { tag ->
                dynamicMenuItem.localyticsTag = tag
            }
        }
    }

    private fun createInboxMessageLiveDataByMessagePlace(
        userType: UserType,
        messagePlace: MessagePlace,
        uiMode: Int? = null
    ): LiveData<InboxMessagesDynamicPayload> {
        return inboxMessagesDynamicViewLiveData(
            baseUrl = baseUrl,
            imageLoader = imageLoader,
            type = MessageType.DYNAMIC_NAVIGATION,
            place = messagePlace,
            userType = userType,
            coroutineContext = dispatchers.IO,
            repository = inboxMessagesRepository,
            context = applicationContext,
            uiMode = uiMode
        )
    }

    private fun getInboxMessageMenuItems(): LiveData<List<MenuItem>> {
        referAFriendInboxMessagesMenuItem = createInboxMessageLiveDataByMessagePlace(
            userType = accountListRepo.userType(),
            messagePlace = MessagePlace.MENU_REFER_A_FRIEND,
            uiMode = nightModePreferences.uiMode
        )
        return _menuPageItems.appendInboxMessageMenuItem(
            inboxMessage = genericInboxMessagesMenuItem,
            indexOffset = GENERIC_INBOX_MESSAGE_MENU_ITEM_OFFSET
        ).appendInboxMessageMenuItem(
            inboxMessage = referAFriendInboxMessagesMenuItem,
            indexOffset = REFER_A_FRIEND_INBOX_MESSAGE_MENU_ITEM_OFFSET
        )
    }

    private fun LiveData<List<MenuItem>>.appendInboxMessageMenuItem(
        inboxMessage: LiveData<InboxMessagesDynamicPayload>,
        indexOffset: Int
    ): LiveData<List<MenuItem>> {
        return combineLatest(inboxMessage).map { (items, messages) ->
            if (items.isEmpty()) {
                items
            } else {
                messages.view?.let {
                    items.addInboxMessagesView(
                        position = items.size - indexOffset,
                        view = MessagesMenuItem(it)
                    )
                } ?: items
            }
        }
    }

    private fun AccountListRepo.userType() = getAccountList().userType()

    internal sealed class ViewState {
        object Loading : ViewState()
        object Success : ViewState()
    }
}
