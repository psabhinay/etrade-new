package com.etrade.mobilepro.login.usecase

import com.etrade.mobilepro.cache.CacheClearHandler
import com.etrade.mobilepro.encryption.api.CipherAuthenticator
import com.etrade.mobilepro.login.api.repo.LoginFailure
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.login.api.repo.LoginRepo
import com.etrade.mobilepro.login.api.repo.LoginResult
import com.etrade.mobilepro.login.api.repo.LoginSuccess
import com.etrade.mobilepro.util.UseCase
import javax.inject.Inject

interface LogInUseCase : UseCase<LoginRequest, LoginResult>

class LogInUseCaseImp @Inject constructor(
    private val loginRepo: LoginRepo,
    private val accountListUseCase: AccountListUseCase,
    private val loginResultUseCase: LoginResultUseCase,
    private val loginPreferences: LoginPreferencesRepo,
    private val cacheClearHandler: CacheClearHandler
) : LogInUseCase {

    val isLoginForPreviousUser: Boolean
        get() = loginPreferences.isLoginForSameUser

    override suspend fun execute(parameter: LoginRequest): LoginResult {
        val username = parameter.username
        val password = parameter.password

        return loginRepo.login(username, password).fold(
            onSuccess = { result ->
                when (result) {
                    is LoginSuccess -> {
                        cacheClearHandler.clear()
                        loadAccountList(parameter, result)
                    }
                    else -> result
                }
            },
            onFailure = {
                LoginFailure()
            }
        )
    }

    private suspend fun loadAccountList(request: LoginRequest, loginSuccessResult: LoginResult): LoginResult {
        return if (accountListUseCase.execute(Unit)) {
            loginResultUseCase.execute(
                LoginResultParameter(
                    loginSuccessResult, request
                )
            )
            loginPreferences.setUserName(request.username)
            loginSuccessResult
        } else {
            loginResultUseCase.execute(
                LoginResultParameter(
                    LoginFailure(), request
                )
            )
            LoginFailure()
        }
    }
}

data class LoginRequest(
    val username: String,
    val password: String,
    val shouldHandleLanding: Boolean,
    val shouldOfferBiometric: Boolean,
    val authenticator: CipherAuthenticator
)
