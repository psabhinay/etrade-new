package com.etrade.mobilepro.util

import com.nhaarman.mockitokotlin2.spy
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.jupiter.api.Test

class InvocationCooldownTest {

    var currentTime = System.currentTimeMillis()
    val clock = { currentTime }
    val action: () -> Unit = spy {}

    val cooldown = InvocationCooldown(clock)

    @Test
    fun `invocation cooldown when no cooldown invokes action`() {
        cooldown.invokeIfReady(action)
        verify(action, times(1)).invoke()
        currentTime += 100
        cooldown.invokeIfReady(action)
        verify(action, times(2)).invoke()
    }

    @Test
    fun `invocation cooldown during cooldown does not invoke action`() {
        cooldown.startCooldown(300)
        cooldown.invokeIfReady(action)
        verify(action, times(0)).invoke()

        currentTime += 100
        cooldown.invokeIfReady(action)
        verify(action, times(0)).invoke()

        currentTime += 100
        cooldown.invokeIfReady(action)
        verify(action, times(0)).invoke()

        currentTime += 100
        cooldown.invokeIfReady(action)
        verify(action, times(0)).invoke()
    }

    @Test
    fun `invocation cooldown after cooldown invokes action`() {
        cooldown.startCooldown(150)
        cooldown.invokeIfReady(action)
        verify(action, times(0)).invoke()

        currentTime += 100
        cooldown.invokeIfReady(action)
        verify(action, times(0)).invoke()

        currentTime += 100
        cooldown.invokeIfReady(action)
        verify(action, times(1)).invoke()
    }
}
