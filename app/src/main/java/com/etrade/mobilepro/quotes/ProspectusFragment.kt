package com.etrade.mobilepro.quotes

import android.webkit.WebSettings
import com.etrade.mobilepro.common.di.DeviceType
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

class ProspectusFragment @Inject constructor(@DeviceType deviceType: String) : WebViewFragment(deviceType) {
    override fun applySettings(settings: WebSettings) {
        super.applySettings(settings)
        settings.apply {
            loadWithOverviewMode = true
            useWideViewPort = true
        }
    }
}
