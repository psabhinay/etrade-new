package com.etrade.mobilepro.quotes.util

import android.content.res.Resources
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.dynamic.form.fragment.createFormValuesArgument
import com.etrade.mobilepro.instrument.OptionType
import com.etrade.mobilepro.instrument.SettlementSessionCode
import com.etrade.mobilepro.instrument.getOptionType
import com.etrade.mobilepro.instrument.isOption
import com.etrade.mobilepro.optionschain.presentation.OptionChainActionProvider
import com.etrade.mobilepro.optionschain.presentation.OptionChainType
import com.etrade.mobilepro.optionschain.presentation.OptionsChainConditionalOrderDisclosureViewModel
import com.etrade.mobilepro.optionschain.presentation.toSecurityType
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.quote.screen.presentation.QuoteTabsFragmentDirections
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.START_DESTINATION
import com.etrade.mobilepro.trade.TRADE_OPTION_LEG_SELECT_FRAGMENT
import com.etrade.mobilepro.trade.api.option.OptionTradeStrategy
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.getTradeAction
import com.etrade.mobilepro.trade.optionlegselect.OptionLegSelectFragmentArgs
import com.etrade.mobilepro.trade.optionlegselect.OptionTradeActionItemProvider
import com.etrade.mobilepro.trade.optionlegselect.TradeActionItemProvider
import com.etrade.mobilepro.trade.presentation.util.toFormBundle
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.squareup.moshi.Moshi
import javax.inject.Inject
import javax.inject.Provider

private fun ActionItem.toSelectable(resources: Resources): DropDownMultiSelector.Selectable<ActionItem> =
    DropDownMultiSelector.Selectable(
        title = "",
        titleFormatter = { resources.getString(it.displayNameId) },
        value = this,
        selected = false
    )

class SecurityAndOrderTypeSelectionHelper @Inject constructor(
    val resources: Resources,
    val disclosureViewModel: OptionsChainConditionalOrderDisclosureViewModel,
    private val moshi: Moshi,
    private val optionChainActionProvider: OptionChainActionProvider,
    @OptionTradeActionItemProvider private val optionTradeActionItemProvider: TradeActionItemProvider,
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    private val userViewModel: UserViewModel,
    private val tradingDefaultsPreferences: TradingDefaultsPreferences
) {
    private val _selectionState = MutableLiveData(UserSelectionState.IDLE)
    val selectionState: LiveData<UserSelectionState> get() = _selectionState
    private lateinit var _symbol: SearchResultItem.Symbol
    private var optionChainType: OptionChainType? = null
    private val _navigateToTradeSignal: MutableLiveData<ConsumableLiveEvent<NavDirections>> = MutableLiveData()

    private val _securityTypeActionItems: MutableLiveData<List<DropDownMultiSelector.Selectable<ActionItem>>> = MutableLiveData()
    val securityTypeActionItems: LiveData<List<DropDownMultiSelector.Selectable<ActionItem>>> get() = _securityTypeActionItems

    private val _orderTypeActionItems: MutableLiveData<List<DropDownMultiSelector.Selectable<ActionItem>>> = MutableLiveData()
    val orderTypeActionItems: LiveData<List<DropDownMultiSelector.Selectable<ActionItem>>> get() = _orderTypeActionItems

    fun loadSecurityType() {
        _securityTypeActionItems.value =
            optionChainActionProvider.getActions(
                type = _symbol.type,
                underlyingTypeCode = _symbol.underlyingTypeCode,
                isFromQuotes = true
            ).map { actionItem ->
                actionItem.toSelectable(resources)
            }
    }

    fun loadOrderType() {
        _orderTypeActionItems.value =
            optionTradeActionItemProvider.getActions()
                .map { actionItem ->
                    actionItem.toSelectable(resources)
                }
    }

    val navigateToTradeSignal: LiveData<ConsumableLiveEvent<NavDirections>> get() = _navigateToTradeSignal

    private fun buildSettings(
        @StringRes title: Int,
        tag: String,
        items: List<ActionItem>
    ): DropDownMultiSelector.Settings<ActionItem> {
        return DropDownMultiSelector.Settings(
            tag = tag,
            multiSelectEnabled = false,
            title = resources.getString(title),
            itemsData = getDropDownItems(items),
            selectListener = { _, item ->
                selectAction(item.value)
            },
            onCancelListener = {
                cancelActions()
            }
        )
    }

    private fun getDropDownItems(items: List<ActionItem>): LiveData<List<DropDownMultiSelector.Selectable<ActionItem>>> =
        MutableLiveData(
            items.map { actionItem ->
                actionItem.toSelectable(resources)
            }
        )

    fun updateCurrentSymbol(symbol: SearchResultItem.Symbol) {
        _symbol = symbol
    }

    private fun navigateToTrade() {
        if (_symbol.isOption) {
            _selectionState.value = UserSelectionState.SELECT_SECURITY_TYPE
        } else {
            _navigateToTradeSignal.value = ConsumableLiveEvent(
                QuoteTabsFragmentDirections.actionQuoteLaunchTrade(
                    _symbol.toFormBundle(
                        tradeFormParametersBuilderProvider.get(),
                        moshi,
                        userViewModel.currentSelectedAccount.value?.account?.accountId
                    )
                )
            )
        }
    }

    fun selectAction(item: ActionItem) {
        when (_selectionState.value) {
            UserSelectionState.IDLE, null -> {
                throw IllegalStateException("Invalid state: ${_selectionState.value}")
            }
            UserSelectionState.SELECT_SECURITY_TYPE -> {
                optionChainType = optionChainActionProvider.getOptionChainType(item.id)
                onSecurityTypeSelected()
            }
            UserSelectionState.SELECT_ORDER_TYPE -> {
                onOrderTypeSelected(item)
            }
        }
    }

    private fun onOrderTypeSelected(item: ActionItem) {
        optionChainType?.let {
            val bundle = createFormBundle(
                symbol = _symbol,
                securityType = it.toSecurityType(),
                transactionType = optionTradeActionItemProvider.getTransactionType(item.id)
            )
            val directions: NavDirections = if (it == OptionChainType.TRADE_CONDITIONAL) {
                QuoteTabsFragmentDirections.actionQuoteLaunchTrade(bundle)
            } else {
                object : NavDirections {
                    override fun getArguments() = OptionLegSelectFragmentArgs(bundle).toBundle().apply {
                        putString(START_DESTINATION, TRADE_OPTION_LEG_SELECT_FRAGMENT)
                    }

                    override fun getActionId() = R.id.action_quote_launch_trade
                }
            }
            _navigateToTradeSignal.value = ConsumableLiveEvent(directions)
        }
        cancelActions()
    }

    private fun onSecurityTypeSelected() {
        if (optionChainType == OptionChainType.TRADE_CONDITIONAL) {
            _selectionState.value = UserSelectionState.IDLE
            disclosureViewModel.onConditionalOrderSelected { isSigned ->
                _selectionState.value = if (isSigned) {
                    UserSelectionState.SELECT_ORDER_TYPE
                } else {
                    UserSelectionState.IDLE
                }
            }
        } else {
            _selectionState.value = UserSelectionState.SELECT_ORDER_TYPE
        }
    }

    private fun createFormBundle(
        symbol: SearchResultItem.Symbol,
        securityType: SecurityType,
        transactionType: TransactionType
    ): Bundle {
        val formValues = tradeFormParametersBuilderProvider.get()
            .apply {
                this.symbol = symbol
                accountId = userViewModel.currentSelectedAccount.value?.account?.accountId
                action = transactionType.getTradeAction()
                quantity = tradingDefaultsPreferences.optionsContracts.toBigDecimal()
                term = tradingDefaultsPreferences.optionsTerm
                priceType = tradingDefaultsPreferences.optionsPriceType
                optionTradeStrategy = buildOptionTradeStrategy(
                    symbol,
                    transactionType
                )
            }
            .create(securityType)
        return createFormValuesArgument(moshi, formValues)
    }

    private fun buildOptionTradeStrategy(
        symbol: SearchResultItem.Symbol,
        transactionType: TransactionType
    ) =
        object : OptionTradeStrategy {
            override val strategyType: StrategyType
                get() = when (getOptionType(symbol.underlyerSymbol)) {
                    OptionType.CALL -> StrategyType.CALL
                    OptionType.PUT -> StrategyType.PUT
                    else -> StrategyType.UNKNOWN
                }

            override val optionLegs: List<TradeLeg> =
                listOf(
                    object : TradeLeg {
                        override val symbol: String = symbol.symbol
                        override val transactionType: TransactionType = transactionType
                        override val quantity: Int = tradingDefaultsPreferences.optionsContracts
                        override val displaySymbol: String = symbol.displaySymbol
                        override val isAMOption: Boolean =
                            symbol.settlementSessionCd == SettlementSessionCode.AM
                    }
                )
        }

    fun cancelActions() {
        optionChainType = null
        _selectionState.value = UserSelectionState.IDLE
    }

    fun openTrade() {
        navigateToTrade()
    }

    enum class UserSelectionState {
        IDLE,
        SELECT_SECURITY_TYPE,
        SELECT_ORDER_TYPE,
    }
}
