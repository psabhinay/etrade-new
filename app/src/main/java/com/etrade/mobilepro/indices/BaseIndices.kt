package com.etrade.mobilepro.indices

import androidx.annotation.ColorRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.indices.dto.Action
import com.etrade.mobilepro.indices.dto.BehaviourOptions
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.searchingapi.items.EXTRA_WIDGET_TAG
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.util.android.CallToAction
import kotlinx.coroutines.launch

const val WIDGET_TAG_INDICES = "Indices"

abstract class BaseIndices(
    @ColorRes private val dowJonesColor: Int,
    @ColorRes private val nasdaqColor: Int,
    @ColorRes private val sP500Color: Int,
    val callToAction: CallToAction?,
    val defaultErrorMessage: String,
    private val behaviourOptions: BehaviourOptions = BehaviourOptions()
) : DynamicViewModel(R.layout.indices_table) {

    override val variableId: Int = BR.obj

    private val mutableFutures: MutableLiveData<Futures> = MutableLiveData()
    private val mutableIndices: MutableLiveData<List<IndexDataWithColor>> = MutableLiveData()
    private val mutableFutureIndices: MutableLiveData<List<IndexDataWithColor>> = MutableLiveData()

    private val _viewState = MutableLiveData<DynamicUiViewState>()
    val viewState: LiveData<DynamicUiViewState>
        get() = _viewState

    val futures: LiveData<Futures>
        get() = mutableFutures
    val indices: LiveData<List<IndexDataWithColor>>
        get() = mutableIndices
    val futureIndices: LiveData<List<IndexDataWithColor>>
        get() = mutableFutureIndices

    var isFirstItem: Boolean = false

    override fun onCtaClicked(appUrl: String?) {
        appUrl?.let { ticker ->
            val event = when (behaviourOptions.clickAction) {
                Action.OPEN_DETAILS ->
                    CtaEvent.LaunchQuote(
                        SearchResultItem.Symbol(title = ticker, type = InstrumentType.INDX)
                            .toBundle().also {
                                it.putString(EXTRA_WIDGET_TAG, WIDGET_TAG_INDICES)
                            }
                    )
                Action.OPEN_INDEX_CHART -> CtaEvent.NavDirectionsNavigation(
                    MainGraphDirections.actionShowChartBottomSheet()
                )
                Action.DO_NOTHING -> null // no event
            }
            event?.let(clickEvents::setValue)
        }
    }

    fun getMarketIndices() {
        if (viewState.value != DynamicUiViewState.Success) {
            _viewState.value = DynamicUiViewState.Loading
        }
        viewModelScope.launch {
            fetchData()
                .onSuccess {
                    mutableIndices.value = it.indices
                    mutableFutureIndices.value = it.futureIndices
                    mutableFutures.value = it.futures
                    _viewState.postValue(DynamicUiViewState.Success)
                }
                .onFailure { e ->
                    _viewState.value = DynamicUiViewState.Error(e.message)
                    logger.error("Unable to fetch market indices", e)
                }
        }
    }

    protected abstract suspend fun fetchData(): ETResult<IndicesData>

    protected abstract fun isStreamingActive(): Boolean

    protected fun onIndicesUpdated(indices: List<IndexDataWithColor>) = mutableIndices.postValue(indices)

    @ColorRes
    protected fun getIndicatorColor(index: Int): Int {
        return when (index) {
            0 -> dowJonesColor
            1 -> nasdaqColor
            else -> sP500Color
        }
    }
}

data class IndicesData(val indices: List<IndexDataWithColor>?, val futureIndices: List<IndexDataWithColor>?, val futures: Futures?)

data class Futures(val delayedFuturesTimeStamp: String? = null, val futuresExpiry: String? = null)
