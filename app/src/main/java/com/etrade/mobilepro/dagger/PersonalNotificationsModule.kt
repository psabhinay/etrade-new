package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.common.di.PersonalNotifications
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.personalnotifications.data.PersonalNotificationsReadStatusRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class PersonalNotificationsModule {

    companion object {
        @Provides
        @Singleton
        @PersonalNotifications
        internal fun providePersonalNotificationsReadStatusesRepo(): ReadStatusRepo =
            PersonalNotificationsReadStatusRepo()
    }
}
