package com.etrade.mobilepro.cache

interface CacheClearHandler {
    fun clear()
}
