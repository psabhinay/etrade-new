package com.etrade.mobilepro.chartengine

import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

/**
 * Represents one line in mp android chart business layer
 */
data class SymbolChartData(
    val queryValue: String = "",
    val displayData: List<ChartOHLCData>,
    val yesterdaysClose: Double,
    val cachedAt: Long,
    val shouldCachedTimeInSeconds: Long
)

data class ChartOHLCData(
    /**
     * date from server is returning in "MM/dd/yyyy HH:mm"
     * we don't currently use open, high, low, and volume for line charts.
     */
    val date: String,
    val open: Double,
    val high: Double,
    val low: Double,
    val close: Double,
    val volume: Long
) : TimedChartPoint {
    override val x: LocalDateTime = LocalDateTime.parse(date, responseDateTimeFormatter)
    override val y: Double = close
}

private val responseDateTimeFormatter by lazy { DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm") }
