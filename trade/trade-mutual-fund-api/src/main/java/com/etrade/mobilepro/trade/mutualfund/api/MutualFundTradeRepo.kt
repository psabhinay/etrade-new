package com.etrade.mobilepro.trade.mutualfund.api

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.instrument.WithSymbolInfo

interface MutualFundTradeRepo {
    suspend fun getMutualFundSellEntry(accountId: String): ETResult<List<MutualFundEntry>>

    suspend fun getMutualFundExchangeSellEntry(accountId: String): ETResult<List<MutualFundEntry>>

    suspend fun getMutualFundExchangeBuyEntry(accountId: String, symbol: WithSymbolInfo): ETResult<MutualFundResult>
}
