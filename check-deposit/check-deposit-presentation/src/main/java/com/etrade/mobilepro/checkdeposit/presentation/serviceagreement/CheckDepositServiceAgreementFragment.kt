package com.etrade.mobilepro.checkdeposit.presentation.serviceagreement

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.checkdeposit.presentation.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

@RequireLogin
class CheckDepositServiceAgreementFragment @Inject constructor(
    private val viewModelProvider: ViewModelProvider.Factory,
    @DeviceType deviceType: String,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : WebViewFragment(deviceType) {

    private val viewModel: CheckDepositServiceAgreementViewModel by viewModels { viewModelProvider }
    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        activity?.apply {
            toolbarActionEndView?.apply {
                setToolbarActionTextAndContentDescription(R.string.agree)
                setOnClickListener {
                    viewModel.acceptServiceAgreement()
                }
                visibility = View.VISIBLE
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is CheckDepositServiceAgreementViewModel.ViewState.Loading -> loading = true
                    is CheckDepositServiceAgreementViewModel.ViewState.Error -> {
                        loading = false
                        snackBarUtil.retrySnackbar { viewModel.acceptServiceAgreement() }?.apply { show() }
                    }
                    is CheckDepositServiceAgreementViewModel.ViewState.Success -> {
                        loading = false

                        findNavController().navigate(CheckDepositServiceAgreementFragmentDirections.openCheckDepositLanding(true))
                    }
                }
            }
        )
    }
}
