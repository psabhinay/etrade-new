package com.etrade.mobilepro.quote.screen.presentation.ui

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.common.compose.theme.EtradeTheme
import com.etrade.mobilepro.compose.components.LiveDataObserve
import com.etrade.mobilepro.etcompose.tabhorizontalpager.CombinedTabHorizontalPager
import com.etrade.mobilepro.etcompose.tabhorizontalpager.PagerItem
import com.etrade.mobilepro.etcompose.tabhorizontalpager.rememberPagerState
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import kotlinx.coroutines.launch

@Composable
fun MainContent(
    pagerItems: List<PagerItem>,
    onTabChangedListener: ((Int) -> Unit)? = null,
    pageScrollLiveData: LiveData<ConsumableLiveEvent<Int>>
) {
    EtradeTheme {
        Scaffold { innerPadding ->
            val pagerState = rememberPagerState(0)
            val coroutineScope = rememberCoroutineScope()
            LiveDataObserve(pageScrollLiveData) {
                it.consume { index ->
                    coroutineScope.launch {
                        pagerState.scrollToPage(index)
                    }
                }
            }
            CombinedTabHorizontalPager(
                items = pagerItems,
                modifier = Modifier.padding(innerPadding),
                pagerState = pagerState,
                onTabChangedListener = onTabChangedListener,
                animateScroll = false
            )
        }
    }
}
