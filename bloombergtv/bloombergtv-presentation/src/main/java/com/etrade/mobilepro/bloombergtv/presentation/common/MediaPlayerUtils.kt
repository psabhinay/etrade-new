package com.etrade.mobilepro.bloombergtv.presentation.common

import android.media.MediaPlayer
import com.etrade.mobilepro.common.result.runCatchingET

val MediaPlayer?.position: Int
    get() = runCatching { this?.currentPosition }.getOrNull() ?: 0

val MediaPlayer?.playing: Boolean
    get() = runCatching { this?.isPlaying }.getOrNull() ?: false

fun MediaPlayer?.safeCall(condition: Boolean, action: MediaPlayer.() -> Unit) =
    this?.takeIf { condition }?.action()

fun MediaPlayer?.mute() = setVolume(0f)

fun MediaPlayer?.unmute() = setVolume(1f)

fun MediaPlayer?.setVolume(level: Float) =
    runCatchingET { this?.setVolume(level, level) }
