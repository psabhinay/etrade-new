package com.etrade.mobilepro.quote.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.dto.FundQuoteResponseDto
import com.etrade.mobilepro.quote.dto.QuoteDto
import com.etrade.mobilepro.quote.dto.QuoteStockDto
import com.etrade.mobilepro.quote.mapper.MobileQuoteMapper
import com.etrade.mobilepro.quote.mapper.MobileQuoteWidgetItemMapper
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesRepo
import com.etrade.mobilepro.quoteapi.QuoteChangeWidgetItem
import com.etrade.mobilepro.quoteapi.QuotePriceWidgetItem
import com.etrade.mobilepro.quoteapi.QuoteTemplateResources
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.quoteapi.usecase.TradeIsExtendedHoursOnUseCase
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.lightstreamer.level1.Level1Column
import com.etrade.mobilepro.streaming.lightstreamer.level1.LightstreamerStockData
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

private const val GREEN = 0xFF00FF00.toInt()
private const val RED = 0xFFFF0000.toInt()

class QuotesWidgetViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private lateinit var sut: QuotesWidgetViewModel

    private var quoteMapper = MobileQuoteWidgetItemMapper(
        QuoteTemplateResources(
            valueMissing = "--",
            titleFormat = "%1\$s - %2\$s",
            asOfDateTimeFormat = "As of %1\$s",
            nasdaqAsOfDateTimeFormat = "NASDAQ as of %1\$s",
            positiveChangeColorRes = GREEN,
            negativeChangeColorRes = RED,
            neutralChangeColorRes = 0,
            dateFormat = "%1s/%2s/%3s",
            changeFormat = "%s (%2s%%)",
            peFormat = "%.1fx",
            closingPrice = "Closing Price",
            underlyerPrice = "Underlyer Price",
            underlyerClosingPrice = "Underlyer Closing Price",
            fundClassTitle = "Class",
            fundDescriptionFormat = "Category: %1\$s\\n\\n%2\$s",
            fundDescriptionNotFound = "Fund description not found",
            fundPerformanceCategoryFormat = "Morningstar %1\$s",
            priceFormat = "price is %1s",
            sizeFormat = "size is %1s",
            contentDescriptionFormat = "Symbol %1s, %2s, Change %3s, Volume %4s, %5s"
        )
    )

    private val quoteService: MobileQuoteRepo = mockQuoteService()

    private val quoteFundsTradeService: MutualFundsTradeQuotesRepo = mock()

    private val mockIsExtendedHoursOnUseCase: IsExtendedHoursOnUseCase = mock {
        onBlocking { execute(anyOrNull()) }.thenReturn(false)
    }

    private val mockTradeIsExtendedHoursOnUseCase: TradeIsExtendedHoursOnUseCase = mock {
        onBlocking { execute(anyOrNull()) }.thenReturn(false)
    }

    private val mockStreamingStatusController: StreamingStatusController = mock {
        on { this.isStreamingToggleEnabled }.doReturn(true)
    }

    private fun mockQuoteService(): MobileQuoteRepo {
        return mock {
            on {
                loadMobileQuote("AAPL", requireEarningsDate = false, extendedHours = false, requireExchangeCode = false)
            } doReturn
                Observable.just(MobileQuoteMapper().map(getMockQuotes()))
            on {
                loadMobileQuote("AAAAX", requireEarningsDate = false, extendedHours = false, requireExchangeCode = false)
            } doReturn
                Observable.just(MobileQuoteMapper().map(getMockMFQuotes()))
            on {
                loadMobileQuote("OZSC", requireEarningsDate = false, extendedHours = false, requireExchangeCode = false)
            } doReturn
                Observable.just(MobileQuoteMapper().map(getMockPennyStockQuotes()))
        }
    }

    @Test
    fun `loading indicator status change upon creation`() {
        sut = QuotesWidgetViewModel(
            quoteService = quoteService,
            quoteMapper = quoteMapper,
            mutualFundsTradeQuotesRepo = quoteFundsTradeService,
            isExtendedHoursOnUseCase = mockIsExtendedHoursOnUseCase,
            streamingStatusController = mockStreamingStatusController,
            streamingQuoteProvider = createProvider(),
            tradeIsExtendedHoursOnUseCase = mockTradeIsExtendedHoursOnUseCase,
            marketTradeStatusDelegate = mock()
        )

        val testObserver = sut.loadingIndicator.test().assertValue(true)

        // when
        sut.getQuotes("AAPL")

        testObserver.assertValue(false)
    }

    @Test
    fun `returns error when quote call is not successful`() {
        val mockErrorObserver = mock<Observer<ConsumableLiveEvent<String>?>>()
        val mockDynamicService = mock<MobileQuoteRepo> {
            on {
                loadMobileQuote("AAPL", false, false, false)
            } doReturn Observable.error(RuntimeException("Bad things"))
        }

        // initialize sut
        sut = QuotesWidgetViewModel(
            quoteService = mockDynamicService,
            quoteMapper = quoteMapper,
            mutualFundsTradeQuotesRepo = quoteFundsTradeService,
            isExtendedHoursOnUseCase = mockIsExtendedHoursOnUseCase,
            streamingStatusController = mockStreamingStatusController,
            streamingQuoteProvider = createProvider(),
            tradeIsExtendedHoursOnUseCase = mockTradeIsExtendedHoursOnUseCase,
            marketTradeStatusDelegate = mock()
        )
        sut.errorMessage.observeForever(mockErrorObserver)

        // when
        runBlocking {
            sut.getQuotes("AAPL")

            // verify behaviour
            inOrder(mockErrorObserver) {
                verify(mockErrorObserver).onChanged(eq(ConsumableLiveEvent("Bad things")))
            }
        }
    }

    @Test
    fun `quote updates on data loaded`() {
        sut = QuotesWidgetViewModel(
            quoteService = quoteService,
            quoteMapper = quoteMapper,
            mutualFundsTradeQuotesRepo = quoteFundsTradeService,
            isExtendedHoursOnUseCase = mockIsExtendedHoursOnUseCase,
            streamingStatusController = mockStreamingStatusController,
            streamingQuoteProvider = createProvider(),
            tradeIsExtendedHoursOnUseCase = mockTradeIsExtendedHoursOnUseCase,
            marketTradeStatusDelegate = mock()
        )

        sut.getQuotes("AAPL")
        sut.quote.test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it?.volume?.value == "10.0"
            }
    }

    @Test
    fun `quote check penny stock values`() {
        sut = QuotesWidgetViewModel(
            quoteService = quoteService,
            quoteMapper = quoteMapper,
            mutualFundsTradeQuotesRepo = quoteFundsTradeService,
            isExtendedHoursOnUseCase = mockIsExtendedHoursOnUseCase,
            streamingStatusController = mockStreamingStatusController,
            streamingQuoteProvider = createProvider(),
            tradeIsExtendedHoursOnUseCase = mockTradeIsExtendedHoursOnUseCase,
            marketTradeStatusDelegate = mock()
        )

        sut.getQuotes("OZSC")
        sut.quote.test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it?.lastPrice?.value == QuotePriceWidgetItem("0.009", 0, "", "0")
                it?.bidPrice?.value == QuotePriceWidgetItem("0.0089", 0, "", "0")
                it?.askPrice?.value == QuotePriceWidgetItem("0.0095", 0, "", "0")
                // This value will get formatted by @BindingAdapter("formatChange")
                it?.change?.value == QuoteChangeWidgetItem("0.000400", "4.65")
            }
    }

    private fun getMockQuotes(): QuoteDto {
        return this::class.getObjectFromJson<QuoteStockDto>("/responses/quotes.json")
    }

    private fun getMockPennyStockQuotes(): QuoteDto {
        return this::class.getObjectFromJson<QuoteStockDto>("/responses/quotes_penny_stock.json")
    }

    private fun getMockMFQuotes(): FundQuoteResponseDto {
        return this::class.getObjectFromJson("/responses/quotes_mutual_funds.json")
    }

    private fun createProvider(): StreamingQuoteProvider<Level1Field, Level1Data, Any> {
        val mockData: Observable<Level1Data> = Observable.just(mockData)
        return mock {
            on { it.getQuote(eq("AAPL"), eq(InstrumentType.EQ), anyOrNull(), anyOrNull()) }.thenReturn(mockData)
        }
    }

    private val mockData = LightstreamerStockData(
        mock {
            on(it.getNewValue(Level1Column.ACCUM_VOLUME.column)).thenReturn("10.0")
        },
        emptySet(),
        InstrumentType.EQ
    )
}
