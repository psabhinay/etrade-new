package com.etrade.mobilepro.bank;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\u0018\u0000 \u00032\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0006"}, d2 = {"Lcom/etrade/mobilepro/bank/BankHomeFragmentDirections;", "", "()V", "Companion", "OpenDatePicker", "ToCheckDeposit", "bank-dynamic_debug"})
public final class BankHomeFragmentDirections {
    @org.jetbrains.annotations.NotNull()
    public static final com.etrade.mobilepro.bank.BankHomeFragmentDirections.Companion Companion = null;
    
    private BankHomeFragmentDirections() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0082\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0006H\u00c6\u0003J\'\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00062\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\b\u0010\u0013\u001a\u00020\u0014H\u0016J\b\u0010\u0015\u001a\u00020\u0016H\u0016J\t\u0010\u0017\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\t\u00a8\u0006\u001a"}, d2 = {"Lcom/etrade/mobilepro/bank/BankHomeFragmentDirections$OpenDatePicker;", "Landroidx/navigation/NavDirections;", "startDate", "Lorg/threeten/bp/LocalDate;", "endDate", "isStart", "", "(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Z)V", "getEndDate", "()Lorg/threeten/bp/LocalDate;", "()Z", "getStartDate", "component1", "component2", "component3", "copy", "equals", "other", "", "getActionId", "", "getArguments", "Landroid/os/Bundle;", "hashCode", "toString", "", "bank-dynamic_debug"})
    static final class OpenDatePicker implements androidx.navigation.NavDirections {
        @org.jetbrains.annotations.NotNull()
        private final org.threeten.bp.LocalDate startDate = null;
        @org.jetbrains.annotations.NotNull()
        private final org.threeten.bp.LocalDate endDate = null;
        private final boolean isStart = false;
        
        @org.jetbrains.annotations.NotNull()
        public final com.etrade.mobilepro.bank.BankHomeFragmentDirections.OpenDatePicker copy(@org.jetbrains.annotations.NotNull()
        org.threeten.bp.LocalDate startDate, @org.jetbrains.annotations.NotNull()
        org.threeten.bp.LocalDate endDate, boolean isStart) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public OpenDatePicker(@org.jetbrains.annotations.NotNull()
        org.threeten.bp.LocalDate startDate, @org.jetbrains.annotations.NotNull()
        org.threeten.bp.LocalDate endDate, boolean isStart) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final org.threeten.bp.LocalDate component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final org.threeten.bp.LocalDate getStartDate() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final org.threeten.bp.LocalDate component2() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final org.threeten.bp.LocalDate getEndDate() {
            return null;
        }
        
        public final boolean component3() {
            return false;
        }
        
        public final boolean isStart() {
            return false;
        }
        
        @java.lang.Override()
        public int getActionId() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @kotlin.Suppress(names = {"CAST_NEVER_SUCCEEDS"})
        @java.lang.Override()
        public android.os.Bundle getArguments() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0082\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u00d6\u0003J\b\u0010\r\u001a\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\t\u0010\u0011\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0013"}, d2 = {"Lcom/etrade/mobilepro/bank/BankHomeFragmentDirections$ToCheckDeposit;", "Landroidx/navigation/NavDirections;", "bankAccountId", "", "(Ljava/lang/String;)V", "getBankAccountId", "()Ljava/lang/String;", "component1", "copy", "equals", "", "other", "", "getActionId", "", "getArguments", "Landroid/os/Bundle;", "hashCode", "toString", "bank-dynamic_debug"})
    static final class ToCheckDeposit implements androidx.navigation.NavDirections {
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String bankAccountId = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.etrade.mobilepro.bank.BankHomeFragmentDirections.ToCheckDeposit copy(@org.jetbrains.annotations.NotNull()
        java.lang.String bankAccountId) {
            return null;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object other) {
            return false;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        public ToCheckDeposit(@org.jetbrains.annotations.NotNull()
        java.lang.String bankAccountId) {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String component1() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getBankAccountId() {
            return null;
        }
        
        @java.lang.Override()
        public int getActionId() {
            return 0;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public android.os.Bundle getArguments() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\f\u00a8\u0006\r"}, d2 = {"Lcom/etrade/mobilepro/bank/BankHomeFragmentDirections$Companion;", "", "()V", "openDatePicker", "Landroidx/navigation/NavDirections;", "startDate", "Lorg/threeten/bp/LocalDate;", "endDate", "isStart", "", "toCheckDeposit", "bankAccountId", "", "bank-dynamic_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections openDatePicker(@org.jetbrains.annotations.NotNull()
        org.threeten.bp.LocalDate startDate, @org.jetbrains.annotations.NotNull()
        org.threeten.bp.LocalDate endDate, boolean isStart) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.navigation.NavDirections toCheckDeposit(@org.jetbrains.annotations.NotNull()
        java.lang.String bankAccountId) {
            return null;
        }
    }
}