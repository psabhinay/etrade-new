package com.etrade.mobilepro.feedback.api

import com.etrade.mobile.power.backends.neo.BaseUsData
import com.etrade.mobile.power.backends.neo.BaseUsResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FeedbackResponse(
    @Json(name = "data")
    override val data: FeedbackResponseData?
) : BaseUsResponse()

@JsonClass(generateAdapter = true)
data class FeedbackResponseData(
    @Json(name = "sfdcReponse")
    val sfdcReponse: SfdcReponse? // yes, typo in response body
) : BaseUsData()

@JsonClass(generateAdapter = true)
data class SfdcReponse(
    @Json(name = "errorCode")
    val errorCode: Long,
    @Json(name = "errorMessage")
    val errorMessage: String,
    @Json(name = "returnField")
    val returnField: String
)

object SdfcReponceConstants {
    const val messageSuccess = "Success"
    const val codeSuccess = 0L
}

fun FeedbackResponse.isSubmitSuccessful(): Boolean {
    if (this.isSuccess == true) {
        return false
    }

    if (data == null) {
        return false
    }

    val responseData = data

    if (responseData.sfdcReponse == null) {
        return false
    }

    val sdfcReponce = responseData.sfdcReponse

    if (sdfcReponce.errorMessage != SdfcReponceConstants.messageSuccess) {
        return false
    }

    if (sdfcReponce.errorCode != SdfcReponceConstants.codeSuccess) {
        return false
    }

    return true
}
