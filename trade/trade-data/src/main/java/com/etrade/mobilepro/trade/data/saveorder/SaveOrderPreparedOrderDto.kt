package com.etrade.mobilepro.trade.data.saveorder

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class SaveOrderPreparedOrderDto(
    @Json(name = "accountId")
    val accountId: String?,
    @Json(name = "preparedId")
    val preparedId: String?
)
