package com.etrade.mobilepro.dagger

import android.content.res.Resources
import com.etrade.completeview.dto.AccountSummaryCardViewDto
import com.etrade.completeview.dto.AccountSummaryViewDto
import com.etrade.completeview.dto.NetSummaryViewDto
import com.etrade.mobilepro.balance.data.repo.AccountBalancesRepo
import com.etrade.mobilepro.balance.section.BalanceSectionMapper
import com.etrade.mobilepro.balance.section.dto.BalanceDetailsViewDto
import com.etrade.mobilepro.balance.section.viewfactory.BalanceDetailsViewFactory
import com.etrade.mobilepro.bank.actions.BankActionsViewDto
import com.etrade.mobilepro.bank.actions.BankActionsViewFactory
import com.etrade.mobilepro.bank.transactions.BankTransactionViewDto
import com.etrade.mobilepro.bank.transactions.BankTransactionViewFactory
import com.etrade.mobilepro.biometric.api.BiometricsProvider
import com.etrade.mobilepro.bloombergtv.api.BloombergTvRepository
import com.etrade.mobilepro.bloombergtv.common.BloombergTvPipStatusHelper
import com.etrade.mobilepro.bloombergtv.widget.BloombergWidgetViewFactory
import com.etrade.mobilepro.bloombergtv.widget.dto.BloombergWidgetViewDto
import com.etrade.mobilepro.chart.data.ChartParams
import com.etrade.mobilepro.chart.data.GetChartLineDataUseCase
import com.etrade.mobilepro.chart.linechart.dto.MarketChartViewDto
import com.etrade.mobilepro.chart.linechart.viewfactory.MarketOverviewChartViewFactory
import com.etrade.mobilepro.common.TimeStampReporter
import com.etrade.mobilepro.common.di.CompleteView
import com.etrade.mobilepro.common.di.CompleteViewDynamicScreen
import com.etrade.mobilepro.common.di.Default
import com.etrade.mobilepro.common.di.DowJonesColor
import com.etrade.mobilepro.common.di.LocalDynamicScreen
import com.etrade.mobilepro.common.di.NasdaqColor
import com.etrade.mobilepro.common.di.RemoteDynamicScreen
import com.etrade.mobilepro.common.di.SP500Color
import com.etrade.mobilepro.common.di.ViewFactory
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.completeview.AccountSummaryCardViewFactory
import com.etrade.mobilepro.completeview.AccountSummaryViewFactory
import com.etrade.mobilepro.completeview.NetSummaryViewFactory
import com.etrade.mobilepro.completeview.edit.CompleteViewRepo
import com.etrade.mobilepro.completeview.repo.CompleteViewDynamicScreenRepo
import com.etrade.mobilepro.dummy.CompleteViewStreamingTextManager
import com.etrade.mobilepro.dummy.DefaultStreamingTextManager
import com.etrade.mobilepro.dynamic.referencehelper.ReferenceHelper
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.dynamicui.DefaultDynamicScreenRepo
import com.etrade.mobilepro.dynamicui.DynamicDividerViewDto
import com.etrade.mobilepro.dynamicui.DynamicDividerViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.RealmReferenceHelper
import com.etrade.mobilepro.dynamicui.ViewAllAccountsViewDto
import com.etrade.mobilepro.dynamicui.ViewAllAccountsViewFactory
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.datasource.LocalDynamicScreenDataSource
import com.etrade.mobilepro.dynamicui.datasource.RemoteDynamicScreenDataSource
import com.etrade.mobilepro.dynamicui.delegate.DynamicScreenDataSourceDelegate
import com.etrade.mobilepro.dynamicui.delegate.WithRealmDataSourceDelegate
import com.etrade.mobilepro.home.overview.dto.OverviewFooterViewDto
import com.etrade.mobilepro.home.overview.edit.OverviewWidgetRepo
import com.etrade.mobilepro.home.overview.viewfactory.OverviewFooterViewFactory
import com.etrade.mobilepro.inboxmessages.dynamicui.api.DynamicScreenContentUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.bannerwidget.BannerWidgetViewDto
import com.etrade.mobilepro.inboxmessages.dynamicui.bannerwidget.BannerWidgetViewFactory
import com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt.FundingPromptViewDto
import com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt.FundingPromptViewFactory
import com.etrade.mobilepro.indices.dto.MarketIndicesViewDto
import com.etrade.mobilepro.indices.viewfactory.MarketIndicesViewFactory
import com.etrade.mobilepro.login.data.dto.LoginViewDto
import com.etrade.mobilepro.login.viewfactory.OverviewLoginViewFactory
import com.etrade.mobilepro.markets.broader.BroaderMarketIndicesViewFactory
import com.etrade.mobilepro.markets.broader.dto.BroaderMarketIndicesViewDto
import com.etrade.mobilepro.markets.earnings.GetTodayEarningsUseCase
import com.etrade.mobilepro.markets.earnings.TodayEarningsViewDto
import com.etrade.mobilepro.markets.earnings.TodayEarningsViewFactory
import com.etrade.mobilepro.markets.footer.MarketsOverviewFooterViewDto
import com.etrade.mobilepro.markets.footer.MarketsOverviewFooterViewFactory
import com.etrade.mobilepro.markets.futures.widget.FutureIndicesLoader
import com.etrade.mobilepro.markets.futures.widget.MarketsFuturesViewDto
import com.etrade.mobilepro.markets.futures.widget.MarketsFuturesWidgetViewFactory
import com.etrade.mobilepro.movers.MoversRouter
import com.etrade.mobilepro.movers.markets.GetMarketMoversUseCase
import com.etrade.mobilepro.movers.markets.dto.MarketMoverViewDto
import com.etrade.mobilepro.movers.markets.viewfactory.MarketMoverViewFactory
import com.etrade.mobilepro.movers.portfolio.GetPortfolioMoversUseCase
import com.etrade.mobilepro.movers.portfolio.dto.PortfolioMoverViewDto
import com.etrade.mobilepro.movers.portfolio.viewfactory.PortfolioMoverViewFactory
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsEnrollmentViewFactory
import com.etrade.mobilepro.msaccounts.presentation.MsAccountsSummaryViewFactory
import com.etrade.mobilepro.msaccounts.presentation.aggregate.AggregateValuesViewFactory
import com.etrade.mobilepro.news.api.dto.NewsWidgetBriefingDto
import com.etrade.mobilepro.news.api.dto.NewsWidgetPortfolioDto
import com.etrade.mobilepro.news.api.dto.NewsWidgetTodayDto
import com.etrade.mobilepro.news.api.repo.WidgetNewsRepo
import com.etrade.mobilepro.news.briefing.api.BriefingNewsRepo
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.news.presentation.dynamic.NewsWidgetBriefingViewFactory
import com.etrade.mobilepro.news.presentation.dynamic.NewsWidgetViewFactory
import com.etrade.mobilepro.portfolio.PositionListViewFactory
import com.etrade.mobilepro.portfolio.PositionTypeSelectorViewDto
import com.etrade.mobilepro.portfolio.model.PositionTypeSelectorViewFactory
import com.etrade.mobilepro.positions.data.dto.PositionListViewDto
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.settings.SettingsViewFactory
import com.etrade.mobilepro.settings.dto.SettingsViewDto
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import com.etrade.mobilepro.themeselection.NightModePreferences
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.msaccounts.dto.AggregateValuesViewDto
import com.etrade.msaccounts.dto.MsAccountsEnrollmentViewDto
import com.etrade.msaccounts.dto.MsAccountsSummaryViewDto
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap
import org.chromium.customtabsclient.shared.WebViewHelper
import javax.inject.Named
import javax.inject.Provider
import javax.inject.Singleton

@Module(
    includes = [
        ApplicationModule::class,
        ConcurrencyModule::class,
        QuoteModule::class
    ]
)
abstract class DynamicUiModule {

    /* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(AccountSummaryCardViewDto::class)
    abstract fun bindAccountSummaryCardViewFactory(accountSummaryCardViewFactory: AccountSummaryCardViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(AccountSummaryViewDto::class)
    abstract fun bindAccountSummaryViewFactory(accountSummaryViewFactory: AccountSummaryViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(PositionTypeSelectorViewDto::class)
    abstract fun bindPositionTypeSelectorViewFactory(positionTypeSelectorViewFactory: PositionTypeSelectorViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(BankTransactionViewDto::class)
    abstract fun bindBankTransactionViewFactory(factory: BankTransactionViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(DynamicDividerViewDto::class)
    abstract fun bindDynamicDividerViewFactory(dynamicDividerViewFactory: DynamicDividerViewFactory): DynamicUiViewFactory

    @Binds
    @Default
    abstract fun bindDynamicScreenDataSourceDelegate(
        dynamicScreenDataSourceDelegate: DynamicScreenDataSourceDelegate
    ): WithRealmDataSourceDelegate

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(LoginViewDto::class)
    abstract fun bindLoginViewFactory(overviewLoginViewFactory: OverviewLoginViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(PositionListViewDto::class)
    abstract fun bindPortfolioRecordsViewFactory(positionListViewFactory: PositionListViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(MarketIndicesViewDto::class)
    abstract fun bindMarketIndicesViewFactory(marketIndicesViewFactory: MarketIndicesViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(MsAccountsEnrollmentViewDto::class)
    abstract fun bindMsAccountsEnrollmentViewFactory(factory: MsAccountsEnrollmentViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(MsAccountsSummaryViewDto::class)
    abstract fun bindMsAccountsSummaryViewFactory(factory: MsAccountsSummaryViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(AggregateValuesViewDto::class)
    abstract fun bindAggregateValuesViewFactory(factory: AggregateValuesViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(BroaderMarketIndicesViewDto::class)
    abstract fun provideBroaderMarketIndicesViewFactory(broaderMarketIndicesViewFactory: BroaderMarketIndicesViewFactory): DynamicUiViewFactory

    @Binds
    @Singleton
    abstract fun bindReferenceHelper(referenceHelper: RealmReferenceHelper): ReferenceHelper

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(FundingPromptViewDto::class)
    abstract fun bindFundingPromptViewFactory(factory: FundingPromptViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(SettingsViewDto::class)
    abstract fun bindSettingsViewFactory(factory: SettingsViewFactory): DynamicUiViewFactory

    @Binds
    @Singleton
    @Default
    abstract fun bindStreamingTextManager(manager: DefaultStreamingTextManager): StreamingTextManager

    @Binds
    @Singleton
    @CompleteView
    abstract fun bindCompleteViewStreamingTextManager(manager: CompleteViewStreamingTextManager): StreamingTextManager

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(ViewAllAccountsViewDto::class)
    abstract fun bindViewAllAccountsDto(factory: ViewAllAccountsViewFactory): DynamicUiViewFactory

    @Binds
    @IntoMap
    @ViewFactory
    @ClassKey(BannerWidgetViewDto::class)
    abstract fun bindBannerWidgetViewFactory(factoryWidget: BannerWidgetViewFactory): DynamicUiViewFactory

    companion object {

        @Provides
        @CompleteViewDynamicScreen
        @Suppress("LongParameterList")
        fun provideCompleteViewDynamicScreenRepo(
            @RemoteDynamicScreen screenDataSource: ScreenDataSource,
            @ViewFactory objectFactories: Map<Class<*>, @JvmSuppressWildcards DynamicUiViewFactory>,
            referenceHelper: ReferenceHelper,
            dynamicContentUpdater: DynamicScreenContentUpdater,
            @CompleteViewRepo widgetsRepo: OverviewWidgetRepo,
            user: User,
            userPreferences: UserPreferences
        ): DynamicScreenRepo {
            return CompleteViewDynamicScreenRepo(
                screenDataSource,
                objectFactories,
                referenceHelper,
                dynamicContentUpdater,
                widgetsRepo,
                user,
                userPreferences
            )
        }

        @Provides
        @RemoteDynamicScreen
        fun provideDynamicScreenRepo(
            @RemoteDynamicScreen screenDataSource: ScreenDataSource,
            @ViewFactory objectFactories: Map<Class<*>, @JvmSuppressWildcards DynamicUiViewFactory>,
            referenceHelper: ReferenceHelper,
            dynamicContentUpdater: DynamicScreenContentUpdater
        ): DynamicScreenRepo {
            return DefaultDynamicScreenRepo(screenDataSource, objectFactories, referenceHelper, dynamicContentUpdater)
        }

        @Provides
        @LocalDynamicScreen
        fun provideDynamicStaticScreenRepo(
            @LocalDynamicScreen screenDataSource: ScreenDataSource,
            @ViewFactory objectFactories: Map<Class<*>, @JvmSuppressWildcards DynamicUiViewFactory>,
            referenceHelper: ReferenceHelper,
            dynamicContentUpdater: DynamicScreenContentUpdater
        ): DynamicScreenRepo {
            return DefaultDynamicScreenRepo(screenDataSource, objectFactories, referenceHelper, dynamicContentUpdater)
        }

        @Provides
        @RemoteDynamicScreen
        fun provideDynamicScreenDataSource(
            moshi: Moshi,
            @Default
            delegate: WithRealmDataSourceDelegate
        ): ScreenDataSource = RemoteDynamicScreenDataSource(moshi, delegate)

        @Provides
        @LocalDynamicScreen
        @Suppress("LongParameterList")
        fun provideStaticScreenDataSource(
            @Web webBaseUrl: String,
            res: Resources,
            biometricsProvider: BiometricsProvider,
            nightModePreferences: NightModePreferences,
            user: User
        ): ScreenDataSource {
            return LocalDynamicScreenDataSource(webBaseUrl, res, biometricsProvider, nightModePreferences, user)
        }

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(BalanceDetailsViewDto::class)
        fun provideBalanceDetailsViewFactory(
            balancesRepo: AccountBalancesRepo,
            sectionMapper: BalanceSectionMapper
        ): DynamicUiViewFactory = BalanceDetailsViewFactory(balancesRepo, sectionMapper)

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(NetSummaryViewDto::class)
        fun provideNetSummaryViewFactory(
            @CompleteView
            completeViewStreamingTextManager: StreamingTextManager,
            @Default
            streamingTextManager: StreamingTextManager
        ): DynamicUiViewFactory = NetSummaryViewFactory(completeViewStreamingTextManager, streamingTextManager)

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(PortfolioMoverViewDto::class)
        @Suppress("LongParameterList")
        fun providePortfolioMoversViewFactory(
            getPortfolioMoversUseCase: GetPortfolioMoversUseCase,
            moversRouter: MoversRouter,
            streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
            streamingStatusController: StreamingStatusController,
            res: Resources,
            userViewModel: UserViewModel,
            userPreferences: UserPreferences
        ): DynamicUiViewFactory =
            PortfolioMoverViewFactory(
                getPortfolioMoversUseCase,
                moversRouter,
                streamingQuoteProvider,
                streamingStatusController,
                res,
                userViewModel,
                userPreferences
            )

        @Suppress("LongParameterList")
        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(MarketMoverViewDto::class)
        fun provideMarketMoversViewFactory(
            getMarketMoversUseCase: GetMarketMoversUseCase,
            moversRouter: MoversRouter,
            streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
            streamingStatusController: StreamingStatusController,
            res: Resources
        ): DynamicUiViewFactory =
            MarketMoverViewFactory(
                getMarketMoversUseCase,
                moversRouter,
                streamingQuoteProvider,
                streamingStatusController,
                res
            )

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(BankActionsViewDto::class)
        fun provideBankActionsViewFactory(): DynamicUiViewFactory = BankActionsViewFactory()

        @Provides
        fun provideMarkets(@DowJonesColor dowJonesColor: Int, @NasdaqColor nasdaqColor: Int, @SP500Color sp500Color: Int): List<ChartParams> {
            return listOf(
                ChartParams("DJIND", "DOW", dowJonesColor),
                ChartParams("COMP.IDX", "NASDAQ", nasdaqColor),
                ChartParams("SPX", "S&P 500", sp500Color)
            )
        }

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(MarketChartViewDto::class)
        fun provideMarketOverviewChartViewFactory(
            @Named("Symbol") getChartLineDataUseCase: GetChartLineDataUseCase,
            markets: List<ChartParams>,
            streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
            streamingStatusController: StreamingStatusController
        ): DynamicUiViewFactory = MarketOverviewChartViewFactory(getChartLineDataUseCase, markets, streamingQuoteProvider, streamingStatusController)

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(OverviewFooterViewDto::class)
        fun provideTimeStampViewFactory(
            webViewHelper: WebViewHelper,
            userViewModel: Provider<UserViewModel>,
            timeStampReporter: TimeStampReporter
        ): DynamicUiViewFactory = OverviewFooterViewFactory(webViewHelper, userViewModel, timeStampReporter)

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(NewsWidgetPortfolioDto::class)
        @Suppress("LongParameterList")
        fun providePortfolioNewsWidgetViewFactory(
            widgetNewsRepo: WidgetNewsRepo,
            statusRepo: ReadStatusRepo,
            userViewModelProvider: Provider<UserViewModel>,
            newsRouter: NewsRouter,
            res: Resources,
            userPreferences: UserPreferences
        ): DynamicUiViewFactory {
            val selectedAccountProvider = {
                userViewModelProvider.get().accountList.firstOrNull {
                    it.account?.accountUuid == (
                        userPreferences.pfNewsWidgetAccountUuid
                            ?: userViewModelProvider.get().defaultAccountUuid
                        )
                }?.account
            }
            val userHasMoreThanOneBrokerageAccount = {
                userViewModelProvider.get().hasMoreThanOneBrokerageAccount
            }
            val userHasMoreThanOneAccount = {
                userViewModelProvider.get().hasMoreThanOneAccount
            }
            return NewsWidgetViewFactory(
                widgetNewsRepo,
                statusRepo,
                selectedAccountProvider,
                userHasMoreThanOneBrokerageAccount,
                userHasMoreThanOneAccount,
                newsRouter,
                res,
                userPreferences
            )
        }

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(TodayEarningsViewDto::class)
        fun provideTodayEarningsViewFactory(
            getTodayEarningsUseCase: GetTodayEarningsUseCase,
            tracker: Tracker
        ): DynamicUiViewFactory =
            TodayEarningsViewFactory(getTodayEarningsUseCase, tracker)

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(NewsWidgetBriefingDto::class)
        fun provideBriefingNewsWidgetViewFactory(
            widgetNewsRepo: BriefingNewsRepo,
            statusesRepo: ReadStatusRepo,
            newsRouter: NewsRouter
        ): DynamicUiViewFactory =
            NewsWidgetBriefingViewFactory(widgetNewsRepo, statusesRepo, newsRouter)

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(MarketsFuturesViewDto::class)
        fun provideMarketsFuturesWidgetViewFactory(
            futureIndicesLoader: FutureIndicesLoader
        ): DynamicUiViewFactory = MarketsFuturesWidgetViewFactory(futureIndicesLoader)

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(MarketsOverviewFooterViewDto::class)
        fun provideMarketsOverviewFooterViewFactory(
            timeStampReporter: TimeStampReporter,
            userViewModelProvider: Provider<UserViewModel>
        ): DynamicUiViewFactory = MarketsOverviewFooterViewFactory(timeStampReporter, userViewModelProvider)

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(BloombergWidgetViewDto::class)
        fun provideBloombergWidgetViewFactory(
            bloombergTvRepository: BloombergTvRepository,
            bloombergTvPipStatusHelper: BloombergTvPipStatusHelper
        ): DynamicUiViewFactory =
            BloombergWidgetViewFactory(bloombergTvRepository, bloombergTvPipStatusHelper)

        @Provides
        @IntoMap
        @ViewFactory
        @ClassKey(NewsWidgetTodayDto::class)
        @Suppress("LongParameterList")
        fun provideTodaysNewsWidgetViewFactory(
            widgetNewsRepo: WidgetNewsRepo,
            statusRepo: ReadStatusRepo,
            userViewModelProvider: Provider<UserViewModel>,
            newsRouter: NewsRouter,
            res: Resources,
            userPreferences: UserPreferences
        ): DynamicUiViewFactory {
            val selectedAccountProvider = {
                userViewModelProvider.get().accountList.firstOrNull {
                    it.account?.accountUuid == (
                        userPreferences.pfNewsWidgetAccountUuid
                            ?: userViewModelProvider.get().defaultAccountUuid
                        )
                }?.account
            }
            val userHasMoreThanOneBrokerageAccount = {
                userViewModelProvider.get().hasMoreThanOneBrokerageAccount
            }
            val userHasMoreThanOneAccount = {
                userViewModelProvider.get().hasMoreThanOneAccount
            }
            return NewsWidgetViewFactory(
                widgetNewsRepo,
                statusRepo,
                selectedAccountProvider,
                userHasMoreThanOneBrokerageAccount,
                userHasMoreThanOneAccount,
                newsRouter,
                res,
                userPreferences
            )
        }
    }
}
