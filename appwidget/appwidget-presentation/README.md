# App widget presentation

## Notes

### Note-1: Empty spaces in layouts for RemoteViews

RemoteViews can not use View or Space for empty spaces.

### Note-2: View visibility using RemoteAdapter

Once a view is set in a `RemoteAdapter`, its visibility is managed by the `AppWidgetManager` system class and we can not toggle its visibility by simply
using `RemoteViews#setViewVisibility`. In order to gain control over its visibility, we have to explicitly detach the view from the `RemoteAdapter` by attaching
another one, that is why we have to use some "Fake views" to re-gain visibility control over our actual views.

### Note-3: ListView and GridView flicker when updating a collection containing a single item

There is this strange issue in RemoteViews causing UI flickering when the list contains only one item and we try to update/invalidate its content.

https://stackoverflow.com/questions/22846350/flickering-issues-in-home-widget-list-view-if-there-is-only-1-item

In order to fix it, we need to cache the `RemoteViews` created for the first position and provide it as a loading view 
in RemoteViewsService#RemoteViewsFactory#getLoadingView, once again, this issue is not present when a list has 2 or more items.

### Note-4: RemoteAdapter cannot handle view updates faster than 100 ms

From my dev testing, I noticed that there is an ugly flickering issue when trying to update the widget too fast, specifically with streaming updates that happen
faster than 100 ms. Also, I noticed that certain Rx operators like debounce or sample will help a little bit but causing a significant delay of the streaming data,
in other words, the app was buffering old data or only streaming data for the most active symbols, like a race condition. The solution was to implement a backpressure
strategy to handle items one by one every 100 ms by dropping values that do not fit on that time window, dropping items is not an issue for the streaming feature
because the user is only interested in the latest quote data values.

### Note-5: RemoteAdapter item which changes its text size is sometimes not honoring parent's wrap_content property

Problem: For some reason, RemoteViews commands for building `ListView`'s items that dynamically changes its text size are not always making its parent `View`
to correctly resize itself based on its `wrap_content`; This is causing the remote `ListView` not showing its full content but partially showing it using its
scroll feature when there is indeed room available. This behavior was tested on emulators/devices running Android 6 to 10 with different screen densities, and 
the issue was partially present on all of them.

Solution: By adding a small delay when constructing the `RemoteViews` commands, the remote `ListView` properly shows its content based on its wrap_content property 
and (for this scenario) its `TextView` children that changed their text size. The assumption is that with that delay the remote view is aware that it has to
invalidate the list content using the text size values of the list content. PLEASE NOTE that this solution is actually a workaround. Nevertheless, such fix 
successfully addressed the issue on all the devices and scenarios where the issue was present in dev testing.

### Note-6: Service has to be marked as "foreground" as soon as possible due to framework requirements.
That's why it is setting up its foreground execution right after dependencies are injected.
If the service is not marked as foreground within the next 5 seconds after it was launched, the system will crash the app.

Quote from Android documentation

```
After the system has created the service, the app has five seconds to call the service's startForeground() method to show
the new service's user-visible notification. If the app does not call startForeground() within the time limit,
the system stops the service and declares the app to be ANR.
```

https://developer.android.com/about/versions/oreo/background

### Note-7: Workaround for fixing device specific (Samsung Galaxy) and Android OS specific (9 pie) firebase crash report.

Calling setupForegroundExecution on the onStartCommand function as well as workaround for custom battery and performance managers.
Such managers can have custom background restrictions which are out of the scope of the standard android framework. For instance, Galaxy 8 and Galaxy Note 8
devices running Android 9 that activate its custom battery saver mode, remove the foreground mark from our service while allowing it to continue to receive new commands.
If the service is not longer recognized as a foreground service and it receives new commands, the system will silently crash the application.