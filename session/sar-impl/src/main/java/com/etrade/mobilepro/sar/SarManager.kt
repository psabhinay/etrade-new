package com.etrade.mobilepro.sar

import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.common.di.ApplicationScope
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.sar.service.DEFAULT_CHECK_INTERVAL_MILLIS
import com.etrade.mobilepro.sar.service.SarMessage
import com.etrade.mobilepro.sar.service.repo.SarRepo
import com.etrade.mobilepro.session.api.SessionEvents
import com.etrade.mobilepro.session.api.SessionEvents.MSG_NO_SESSION_TIMEOUT
import com.etrade.mobilepro.session.api.SessionEvents.MSG_SESSION_SAR_TERMINATED
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.util.getSessionCookieStringOrEmpty
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import okhttp3.CookieJar
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Singleton

internal const val LOGGED_OFF = "SMSESSION=LOGGEDOFF"

@Singleton
class SarManager @Inject constructor(
    @ApplicationScope val appScope: CoroutineScope,
    private val sarClientRepo: SarRepo,
    private val isUserLoggedIn: UserAuthenticationState,
    private val cookieJar: CookieJar,
    @MobileTrade private val mobileBaseUrl: String
) : SimultaneousAccessRestriction {

    private val logger = LoggerFactory.getLogger(this::class.java)

    private val _sessionEvent: MutableLiveData<SessionEvents> = MutableLiveData()

    override val sessionEvent: LiveData<SessionEvents> = _sessionEvent

    private val sarCheckHandler = Handler(Looper.getMainLooper())

    /**
     *  Denotes the SAR Check state indicating started or stopped.
     */
    private var isStarted: Boolean = false

    private val sarCheckRunnable = Runnable {
        if (isUserLoggedIn()) {
            checkSar()
        }
    }

    private fun checkSar() {
        appScope.launch {
            val message = if (cookieJar.getSessionCookieStringOrEmpty(mobileBaseUrl) == LOGGED_OFF) {
                SarMessage(DEFAULT_CHECK_INTERVAL_MILLIS, "", terminateSession = true, isCookieError = true)
            } else {
                sarClientRepo.checkSar().fold(
                    onSuccess = { sarMessage -> sarMessage },
                    onFailure = { throwable ->
                        logger.error(throwable.message, throwable)
                        SarMessage(DEFAULT_CHECK_INTERVAL_MILLIS, throwable.message)
                    }
                )
            }

            if (isStarted) {
                onSarMessageReceived(message)
            } else {
                logger.debug("Ignoring invalid sar message")
            }
        }
    }

    private fun onSarMessageReceived(sarMsg: SarMessage) {

        if (sarMsg.terminateSession) {
            _sessionEvent.postValue(
                if (sarMsg.isCookieError) {
                    SessionEvents.MSG_SESSION_TERMINATED
                } else {
                    MSG_SESSION_SAR_TERMINATED
                }
            )
        } else {
            sarCheckHandler.postAtTime(sarCheckRunnable, SystemClock.uptimeMillis().plus(sarMsg.nextCheckInterval))
        }

        logger.debug("Sar Check : Terminate session: ${sarMsg.terminateSession} : ${sarMsg.errorMsg ?: ""}")
    }

    override fun start() {
        if (!isStarted) {
            isStarted = true
            sarCheckRunnable.run()
        }
    }

    override fun stop() {
        isStarted = false
        _sessionEvent.postValue(MSG_NO_SESSION_TIMEOUT)
        sarCheckHandler.removeCallbacks(sarCheckRunnable)
    }
}
