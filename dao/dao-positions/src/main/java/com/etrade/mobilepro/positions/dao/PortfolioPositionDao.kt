package com.etrade.mobilepro.positions.dao

import androidx.lifecycle.LiveData
import io.reactivex.Observable

/**
 * Data access object for portfolio positions.
 */
interface PortfolioPositionDao {

    /**
     * Gets list of portfolio positions for specified [positionIds]. A positions is wrapped with [LiveData] object.
     *
     * @param positionIds ordered list of positions IDs, the result will be in the same order
     *
     * @return list of portfolio positions
     */
    fun getPositions(positionIds: List<String>): LiveData<List<PositionRealmObject>>

    fun getPositionsWithSymbol(symbol: String): Observable<List<PositionRealmObject>>

    fun getPositionsWithSymbolNonAsync(symbol: String): List<PositionRealmObject>

    fun getAccountPositionsAsObservable(accountId: String): Observable<List<PositionRealmObject>>

    fun getAccountPositions(accountId: String): List<PositionRealmObject>

    fun getAccountPositionsWithUuid(accountUuid: String): List<PositionRealmObject>

    /**
     * this method does not copy positions, it will only update positions currently in realm
     */
    fun updatePositions(positions: List<PositionRealmObject>)

    /**
     * removes positions that are not in [validPositionIds]
     *
     * @param accountUuid the unique account id for this account
     * @param validPositionIds the current valid position ids
     */
    fun deleteInvalidPositions(accountUuid: String, validPositionIds: List<String>)
}
