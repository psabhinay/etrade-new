package com.etrade.mobilepro.education.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.education.databinding.AdapterEducationOptionsRiskDisclosureBinding

class EducationOptionsRiskDisclosureViewHolder(private val binding: AdapterEducationOptionsRiskDisclosureBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(
        optionsRiskDisclosureListener: View.OnClickListener,
        disclosureListener: View.OnClickListener
    ) {
        binding.optionsRiskDisclosure.setOnClickListener(optionsRiskDisclosureListener)
        binding.educationDisclosure.setOnClickListener(disclosureListener)
    }
}
