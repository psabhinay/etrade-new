package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.common.di.DynamicMenuScreen
import com.etrade.mobilepro.common.di.DynamicMenuScreenDataSourceDelegate
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.dynamicui.DynamicScreenRpcService
import com.etrade.mobilepro.dynamicui.datasource.RemoteDynamicScreenDataSource
import com.etrade.mobilepro.dynamicui.delegate.WithRealmDataSourceDelegate
import com.etrade.mobilepro.menu.DefaultNewMenuItemValidator
import com.etrade.mobilepro.menu.DynamicMenuDataSourceDelegate
import com.etrade.mobilepro.menu.NewMenuItem
import com.etrade.mobilepro.menu.NewMenuItemValidator
import com.etrade.mobilepro.menu.NewMenuItems
import com.etrade.mobilepro.menu.helper.DefaultDynamicMenuHelper
import com.etrade.mobilepro.menu.helper.DynamicMenuHelper
import com.etrade.mobilepro.menu.repo.DefaultMenuRepo
import com.etrade.mobilepro.menu.repo.MenuRepo
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
interface MenuPageModule {

    @Binds
    fun bindDynamicMenuHelper(repo: DefaultDynamicMenuHelper): DynamicMenuHelper

    @Binds
    fun bindDynamicMenuItemsListRepo(repo: DefaultMenuRepo): MenuRepo

    @Binds
    fun bindNewMenuItemValidator(validator: DefaultNewMenuItemValidator): NewMenuItemValidator

    companion object {
        @Provides
        @DynamicMenuScreenDataSourceDelegate
        fun bindDynamicMenuDataSourceDelegate(service: DynamicScreenRpcService):
            WithRealmDataSourceDelegate = DynamicMenuDataSourceDelegate(service)

        @Provides
        @DynamicMenuScreen
        fun providesDynamicMenuDataSource(
            moshi: Moshi,
            @DynamicMenuScreenDataSourceDelegate
            delegate: WithRealmDataSourceDelegate
        ): ScreenDataSource = RemoteDynamicScreenDataSource(moshi, delegate)

        @Provides
        fun provideNewMenuItems(): List<NewMenuItem> = NewMenuItems.getNewMenuItems()
    }
}
