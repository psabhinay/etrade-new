package com.etrade.mobilepro.positions.data.dto

import com.etrade.mobilepro.util.SortOrder
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class AllBrokeragePositionsRequestDto(
    @Json(name = "all")
    val all: AllBrokeragePositionsDto
)

/**
 * All brokerage positions parameters.
 *
 * As per: https://confluence.corp.etradegrp.com/display/MI/API+-+All+Brokerage+Positions
 */
@JsonClass(generateAdapter = true)
class AllBrokeragePositionsDto(

    /**
     * Token to get the next page.
     */
    @Json(name = "continuationToken")
    val continuationToken: String? = null,

    /**
     * Portfolio 2.0 response, flag introduced to support backward compatibility.
     */
    @Json(name = "enableNewExperience")
    val enableNewExperience: Boolean = true,

    /**
     * Number of records per page.
     */
    @Json(name = "pageRecCount")
    val pageRecCount: String? = null,

    /**
     * Sort column.
     */
    @Json(name = "sortColumn")
    val sortColumn: String? = PortfolioRecordColumn.SYMBOL.code,

    /**
     * Order of sorting.
     */
    @Json(name = "sortOrder")
    val sortOrder: String? = SortOrder.ASCENDING.code
)
