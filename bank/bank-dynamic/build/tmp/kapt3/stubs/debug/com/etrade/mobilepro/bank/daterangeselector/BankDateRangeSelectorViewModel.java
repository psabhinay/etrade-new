package com.etrade.mobilepro.bank.daterangeselector;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\bJ\u0006\u0010\u0019\u001a\u00020\u001aJ\u0006\u0010\u001b\u001a\u00020\u001aR\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\n0\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\n0\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\fR\u0014\u0010\u0015\u001a\u00020\u0016X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018\u00a8\u0006\u001c"}, d2 = {"Lcom/etrade/mobilepro/bank/daterangeselector/BankDateRangeSelectorViewModel;", "Lcom/etrade/mobilepro/dynamicui/api/DynamicViewModel;", "resources", "Landroid/content/res/Resources;", "startDate", "Landroidx/lifecycle/LiveData;", "Lorg/threeten/bp/LocalDate;", "endDate", "(Landroid/content/res/Resources;Landroidx/lifecycle/LiveData;Landroidx/lifecycle/LiveData;)V", "endDateContentDescription", "", "getEndDateContentDescription", "()Landroidx/lifecycle/LiveData;", "formattedEndDate", "getFormattedEndDate", "formattedStartDate", "getFormattedStartDate", "getResources", "()Landroid/content/res/Resources;", "startDateContentDescription", "getStartDateContentDescription", "variableId", "", "getVariableId", "()I", "onEndDateClicked", "", "onStartDateClicked", "bank-dynamic_debug"})
public final class BankDateRangeSelectorViewModel extends com.etrade.mobilepro.dynamicui.api.DynamicViewModel {
    @org.jetbrains.annotations.NotNull()
    private final android.content.res.Resources resources = null;
    private final androidx.lifecycle.LiveData<org.threeten.bp.LocalDate> startDate = null;
    private final androidx.lifecycle.LiveData<org.threeten.bp.LocalDate> endDate = null;
    private final int variableId = 0;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.String> formattedStartDate = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.String> formattedEndDate = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.String> startDateContentDescription = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.lang.String> endDateContentDescription = null;
    
    public BankDateRangeSelectorViewModel(@org.jetbrains.annotations.NotNull()
    android.content.res.Resources resources, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<org.threeten.bp.LocalDate> startDate, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<org.threeten.bp.LocalDate> endDate) {
        super(0);
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.res.Resources getResources() {
        return null;
    }
    
    @java.lang.Override()
    public int getVariableId() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getFormattedStartDate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getFormattedEndDate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getStartDateContentDescription() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getEndDateContentDescription() {
        return null;
    }
    
    public final void onStartDateClicked() {
    }
    
    public final void onEndDateClicked() {
    }
}