package com.etrade.mobilepro.valuepicker

interface ValuePickerWidgetItem {
    val title: String
    val value: String?
    val contentDescription: String?
    var onDropDownSelectedListener: ((title: String, description: String) -> Unit)?
    fun showValuePickerDropDown()
}
