package com.etrade.mobilepro.arr.api

import com.etrade.mobilepro.mod.api.Report
import io.reactivex.Single

interface AnalystResearchReportRepo {

    fun getAnalystReports(symbol: String): Single<List<Report>>
}
