package com.etrade.mobilepro.chat.api.message.models

data class ChatForMobileModel(val data: MobileChatDataModel?, val success: Boolean?)

data class MobileChatDataModel(val chatInfoModel: ChatInfoModel?, val messages: String?)

data class ChatInfoModel(val sessionID: String?)
