package com.etrade.mobilepro.login.data

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.login.api.repo.LogoutRepo
import com.etrade.mobilepro.login.data.restclient.LogoutService
import retrofit2.Retrofit
import javax.inject.Inject

class DefaultLogoutRepo @Inject constructor(retrofit: Retrofit) : LogoutRepo {

    private val logoutService: LogoutService = retrofit.create(LogoutService::class.java)

    override suspend fun logout(): ETResult<Unit> {
        return runCatchingET { logoutService.logout() }.mapCatching {
            if (it.result?.code != 0) {
                error(it.result?.fault?.error?.description ?: "unknown server error")
            }
        }
    }
}
