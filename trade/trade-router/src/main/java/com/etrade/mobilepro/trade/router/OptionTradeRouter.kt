package com.etrade.mobilepro.trade.router

import androidx.annotation.IdRes
import androidx.navigation.NavController
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.searchingapi.items.SearchResultItem

interface OptionTradeRouter {

    @get:IdRes
    val tradeFragmentId: Int

    fun openOptionDisclosures(navController: NavController)

    fun openOptionLegSelect(
        navController: NavController,
        inputValues: InputValuesSnapshot,
        symbol: SearchResultItem.Symbol? = null,
        transactionType: TransactionType? = TransactionType.UNKNOWN
    )

    fun openTradeFromLegSelect(navController: NavController, formMap: InputValuesSnapshot)
}
