package com.etrade.mobilepro.trade.presentation.extensions

import androidx.lifecycle.map
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId

internal fun GroupBuilder.selectedAccount(inputId: TradeFormInputId, accounts: List<TradeAccount>) =
    (requireInput<SelectInput>(inputId.accountId)).value.map { option ->
        accounts.find { account -> account.id == option?.id }
            ?: accounts.firstOrNull()
    }
