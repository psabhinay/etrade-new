package com.etrade.mobilepro.news.presentation.detail.helper

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

class DefaultTextNewsFontAdjustHandler @Inject constructor() : TextNewsFontAdjustHandler {
    private val mutableIsLargeFontSize = MutableLiveData<Boolean>()

    init {
        mutableIsLargeFontSize.value = false
    }

    override val isLargeFontSize: LiveData<Boolean> = mutableIsLargeFontSize

    override fun toggleFontSize() {
        mutableIsLargeFontSize.value = !(mutableIsLargeFontSize.value ?: false)
    }
}
