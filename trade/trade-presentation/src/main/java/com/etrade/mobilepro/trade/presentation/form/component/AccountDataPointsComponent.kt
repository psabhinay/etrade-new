package com.etrade.mobilepro.trade.presentation.form.component

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.balance.viewmodel.AccountBalanceViewModel
import com.etrade.mobilepro.dynamic.form.api.Component
import com.etrade.mobilepro.livedata.combine
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.presentation.AccountDataPoint
import com.etrade.mobilepro.trade.presentation.extensions.toDataPoints

internal class AccountDataPointsComponent(
    resources: Resources,
    selectedAccount: LiveData<TradeAccount?>,
    private val balanceViewModel: AccountBalanceViewModel
) : Component {

    override val isVisible: LiveData<Boolean> = MutableLiveData(true)

    val dataPoints: LiveData<List<AccountDataPoint>> = selectedAccount.combine(balanceViewModel.accountBalance) { account, balance ->
        if (account != null && balance != null) {
            balance.toDataPoints(resources, account.accountMode)
        } else {
            emptyList()
        }
    }

    val loadingIndicator: LiveData<Boolean> = balanceViewModel.blockingLoadingIndicator

    val needsRetry: LiveData<Boolean> = balanceViewModel.accountBalanceFailed

    private var currentAccount: TradeAccount? = null

    init {
        selectedAccount.observeForever { account ->
            currentAccount = account
            account?.let {
                balanceViewModel.fetch(account.id)
            }
        }
    }

    fun refresh() = balanceViewModel.refresh()
}
