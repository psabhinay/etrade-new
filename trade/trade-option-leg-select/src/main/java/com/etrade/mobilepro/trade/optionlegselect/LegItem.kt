package com.etrade.mobilepro.trade.optionlegselect

interface LegItem {
    val quantity: String
    val displayName: String
}

data class OptionLegItem(
    override val quantity: String,
    override val displayName: String,
    val isDeletePending: Boolean
) : LegItem

internal class StockLegItem(
    override val quantity: String,
    override val displayName: String,
    val orderType: String
) : LegItem
