package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.util.MutualConverter

internal object StockPriceTypeValueId : PriceTypeValueId {
    override val even: String = ""
    override val limit: String = "lmt"
    override val market: String = "mkt"
    override val netCredit: String = ""
    override val netDebit: String = ""
    override val stop: String = "stp"
    override val stopLimit: String = "stl"
    override val trailingStopDollar: String = "tsc"
    override val trailingStopPercent: String = "tsp"
    override val converter: MutualConverter<String, PriceType> by lazy { createConverter() }
}
