package com.etrade.mobilepro.chat.api.setup.repo

sealed class ChatSetupResult {

    object Success : ChatSetupResult()

    data class NonOperatingHour(
        val isHoliday: Boolean = false
    ) : ChatSetupResult()

    data class Failure(
        val errorMessage: String? = null
    ) : ChatSetupResult()
}
