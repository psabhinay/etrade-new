package com.etrade.mobilepro.trade.data.util

private const val ACCOUNT_ID_LAST_DIGITS = 4

internal fun String.maskAccountId(): String = "XXXX-${takeLast(ACCOUNT_ID_LAST_DIGITS)}"
