package com.etrade.mobilepro.chart.performance.data

import com.etrade.mobilepro.chart.data.ChartRequestDto
import com.etrade.mobilepro.chart.data.ChartRequestValueDto
import com.etrade.mobilepro.chart.data.toChartLine
import com.etrade.mobilepro.chart.performance.model.PerformanceChartData
import com.etrade.mobilepro.chartengine.TimeFrame
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import retrofit2.Retrofit
import javax.inject.Inject

class DefaultPerformanceChartRepo @Inject constructor(
    @MobileTrade retrofit: Retrofit
) : PerformanceChartRepo {

    private val chartApiClient: PerformanceChartApiClient = retrofit.create(PerformanceChartApiClient::class.java)

    override fun getPerformanceChart(accounts: List<String>, indexes: List<String>, period: TimeFrame): Observable<Resource<PerformanceChartData>> {
        return chartApiClient.getPerformanceChart(createRequest(accounts, indexes, period.displayName))
            .map { it.data?.toPerformanceChartData()?.let { chartData -> Resource.Success(chartData) } ?: Resource.Failed<PerformanceChartData>() }
    }

    private fun createRequest(accounts: List<String>, indexes: List<String>, period: String): ChartRequestDto {
        return ChartRequestDto(ChartRequestValueDto(accounts, indexes, period))
    }

    private fun PerformanceChartResponseDataDto.toPerformanceChartData(): PerformanceChartData {
        return with(performance) {
            PerformanceChartData(
                accounts?.map { it.toChartLine() }.orEmpty(),
                indexes?.map { it.toChartLine() }.orEmpty()
            )
        }
    }
}
