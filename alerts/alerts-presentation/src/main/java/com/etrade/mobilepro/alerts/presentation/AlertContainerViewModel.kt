package com.etrade.mobilepro.alerts.presentation

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.alerts.presentation.account.AccountAlertsViewModel
import com.etrade.mobilepro.alerts.presentation.market.MarketAlertsViewModel
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.common.readstatus.ReadableType
import com.etrade.mobilepro.tracking.SCREEN_TITLE_ALERTS_ACCOUNT
import com.etrade.mobilepro.tracking.SCREEN_TITLE_ALERTS_MARKET
import com.etrade.mobilepro.tracking.TabsHostScreenViewModel
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelper
import javax.inject.Inject

class AlertContainerViewModel @Inject constructor(
    private val readStatusRepo: ReadStatusRepo,
    override val viewPagerTabsTrackerHelper: ViewPagerTabsTrackerHelper
) : ViewModel(), TabsHostScreenViewModel {

    override val tabNames: Array<String> = arrayOf(SCREEN_TITLE_ALERTS_MARKET, SCREEN_TITLE_ALERTS_ACCOUNT)

    var marketAlertsViewModel: MarketAlertsViewModel? = null
    var accountAlertsViewModel: AccountAlertsViewModel? = null

    override fun onCleared() {
        marketAlertsViewModel?.reset()
        accountAlertsViewModel?.reset()
        readStatusRepo.clearExpiredReadableEntities(type = ReadableType.ALERT)
        super.onCleared()
    }
}
