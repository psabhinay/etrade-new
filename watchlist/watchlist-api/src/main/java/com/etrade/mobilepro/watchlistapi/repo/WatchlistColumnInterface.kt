package com.etrade.mobilepro.watchlistapi.repo

interface WatchlistColumnInterface {
    val textRes: Int
    val reorderTextRes: Int
    val contentDescriptionRes: Int
}
