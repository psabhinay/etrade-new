package com.etrade.mobilepro.documents

import com.etrade.mobilepro.util.Resource
import kotlinx.coroutines.flow.Flow

interface DocumentsRepository {

    fun getStatementDocuments(request: StatementDocumentsRequest): Flow<Resource<List<StatementDocument>>>

    fun getTaxDocuments(request: TaxDocumentsRequest): Flow<Resource<List<TaxDocument>>>
}
