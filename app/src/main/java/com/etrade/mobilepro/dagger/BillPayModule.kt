package com.etrade.mobilepro.dagger

import android.content.res.Resources
import com.etrade.mobilepro.billpay.api.BillPayRepo
import com.etrade.mobilepro.billpay.api.PaymentStatus
import com.etrade.mobilepro.billpay.data.repo.BillPayRepoImp
import com.etrade.mobilepro.billpay.data.rest.BillPayApiClient
import com.etrade.mobilepro.billpay.presentation.getLabel
import com.etrade.mobilepro.billpay.presentation.paymentlist.PaymentStatusFilterItem
import com.etrade.mobilepro.billpay.usecase.BillPayInitialSetupUseCase
import com.etrade.mobilepro.billpay.usecase.BillPayInitialSetupUseCaseImpl
import com.etrade.mobilepro.billpay.usecase.FilterBillPayAccountsUseCase
import com.etrade.mobilepro.billpay.usecase.FilterBillPayAccountsUseCaseImp
import com.etrade.mobilepro.billpay.usecase.GenerateBillPayStatusUseCase
import com.etrade.mobilepro.billpay.usecase.GenerateBillPayStatusUseCaseImp
import com.etrade.mobilepro.billpay.usecase.PayeeListUseCase
import com.etrade.mobilepro.billpay.usecase.PayeeListUseCaseImpl
import com.etrade.mobilepro.billpay.usecase.PaymentListUseCase
import com.etrade.mobilepro.billpay.usecase.PaymentListUseCaseImpl
import com.etrade.mobilepro.common.di.Web
import dagger.Binds
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
abstract class BillPayModule {

    @Binds
    abstract fun bindBillPayRepo(repo: BillPayRepoImp): BillPayRepo

    @Binds
    abstract fun bindFilterBillPayAccountsUseCase(useCase: FilterBillPayAccountsUseCaseImp): FilterBillPayAccountsUseCase

    @Binds
    abstract fun bindGenerateBillPayStatusUseCase(useCase: GenerateBillPayStatusUseCaseImp): GenerateBillPayStatusUseCase

    @Binds
    abstract fun bindPaymentListUseCase(useCase: PaymentListUseCaseImpl): PaymentListUseCase

    @Binds
    abstract fun bindPayeeListUseCase(useCase: PayeeListUseCaseImpl): PayeeListUseCase

    @Binds
    abstract fun bindBillPayUseCase(useCase: BillPayInitialSetupUseCaseImpl): BillPayInitialSetupUseCase

    companion object {
        @Provides
        @Singleton
        fun provideBiometricManager(@Web retrofit: Retrofit): BillPayApiClient {
            return retrofit.create(BillPayApiClient::class.java)
        }

        @Provides
        @Singleton
        internal fun getPaymentListStatusFilterMap(resources: Resources): Map<PaymentStatus, PaymentStatusFilterItem> {
            return mapOf(
                PaymentStatus.ALL to PaymentStatusFilterItem(resources.getString(PaymentStatus.ALL.getLabel()), PaymentStatus.ALL),
                PaymentStatus.SCHEDULED to PaymentStatusFilterItem(resources.getString(PaymentStatus.SCHEDULED.getLabel()), PaymentStatus.SCHEDULED),
                PaymentStatus.PAID to PaymentStatusFilterItem(resources.getString(PaymentStatus.PAID.getLabel()), PaymentStatus.PAID),
                PaymentStatus.CANCELLED to PaymentStatusFilterItem(resources.getString(PaymentStatus.CANCELLED.getLabel()), PaymentStatus.CANCELLED),
                PaymentStatus.FAILED to PaymentStatusFilterItem(resources.getString(PaymentStatus.FAILED.getLabel()), PaymentStatus.FAILED)
            )
        }
    }
}
