package com.etrade.mobilepro.pushnotification.presentation

import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.android.AndroidInjection
import javax.inject.Inject

class EtMessagingService : FirebaseMessagingService() {

    @Inject
    lateinit var services: Array<MessagingService>

    override fun onCreate() {
        super.onCreate()

        AndroidInjection.inject(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        services.forEach { it.onDestroy() }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)

        services.forEach { it.onNewToken(token) }
    }

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)

        services.forEach { it.onMessageReceived(message) }
    }
}
