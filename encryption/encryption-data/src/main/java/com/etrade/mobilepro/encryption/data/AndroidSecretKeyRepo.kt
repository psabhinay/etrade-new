package com.etrade.mobilepro.encryption.data

import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import com.etrade.mobilepro.encryption.api.SecretKeyMaterial
import com.etrade.mobilepro.encryption.api.SecretKeyRepo
import com.etrade.mobilepro.encryption.api.SymmetricAlgorithmSpec
import com.etrade.mobilepro.util.delegate.ResettableLazy
import java.security.KeyStore
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey
import javax.inject.Inject

internal const val KEY_STORE = "AndroidKeyStore"

private const val KEY_ALIAS_COMMON = "ETradeCommon"
private const val KEY_ALIAS_SECURE = "ETradeSecure"

class AndroidSecretKeyRepo @Inject constructor(override val algorithmSpec: SymmetricAlgorithmSpec) : SecretKeyRepo {

    private val keyStore: KeyStore = KeyStore.getInstance(KEY_STORE).apply { load(null) }

    private val secureKeyLazy = ResettableLazy {
        val secretKey = getKey(algorithmSpec, KEY_ALIAS_SECURE) {
            setUserAuthenticationRequired(true)
            setInvalidatedByBiometricEnrollment(true)
        }
        SecretKeyMaterial(secretKey, true)
    }

    override val commonKey: SecretKeyMaterial by lazy {
        SecretKeyMaterial(getKey(algorithmSpec, KEY_ALIAS_COMMON), false)
    }

    override val secureKey: SecretKeyMaterial by secureKeyLazy

    override fun recreateSecretKey() {
        keyStore.deleteEntry(KEY_ALIAS_SECURE)
        secureKeyLazy.reset()
    }

    private fun getKey(
        algorithmSpec: SymmetricAlgorithmSpec,
        alias: String,
        init: KeyGenParameterSpec.Builder.() -> Unit = { /* intentionally blank */ }
    ): SecretKey {
        return keyStore.getKey(alias, null) as? SecretKey ?: createKey(algorithmSpec, alias, init)
    }

    private fun createKey(algorithmSpec: SymmetricAlgorithmSpec, alias: String, init: KeyGenParameterSpec.Builder.() -> Unit): SecretKey {
        return KeyGenerator.getInstance(algorithmSpec.algorithm, KEY_STORE)
            .apply { init(createKeyGenParameterSpec(alias, algorithmSpec, init)) }
            .generateKey()
    }

    private fun createKeyGenParameterSpec(
        alias: String,
        algorithmSpec: SymmetricAlgorithmSpec,
        init: KeyGenParameterSpec.Builder.() -> Unit
    ): KeyGenParameterSpec {
        return KeyGenParameterSpec.Builder(alias, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
            .setBlockModes(algorithmSpec.blockMode)
            .setEncryptionPaddings(algorithmSpec.encryptionPadding)
            .setRandomizedEncryptionRequired(false)
            .apply(init)
            .build()
    }
}
