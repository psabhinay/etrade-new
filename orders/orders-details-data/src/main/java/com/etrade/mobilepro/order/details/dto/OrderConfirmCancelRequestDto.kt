package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.trade.data.common.LegDetailsCommonDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OrderConfirmCancelRequestDto(
    @Json(name = "value")
    val value: OrderConfirmCancelRequest
)
@JsonClass(generateAdapter = true)
data class OrderConfirmCancelRequest(
    @Json(name = "previewId")
    val previewId: String = "",
    @Json(name = "underlier")
    val underlier: String = "",
    @Json(name = "strategyType")
    val strategyType: String = "1",
    @Json(name = "requestType")
    val requestType: String = "ORDER_REQTYPE_UNSPECIFIED",
    @Json(name = "originalOrderNo")
    val originalOrderNo: String = "",
    @Json(name = "overrideRestrictedCd")
    val overrideRestrictedCd: String = "",
    @Json(name = "priceType")
    val priceType: String = "",
    @Json(name = "allOrNone")
    val allOrNone: String = "0",
    @Json(name = "accountId")
    val accountId: String = "",
    @Json(name = "orderType")
    val orderType: String = "3",
    @Json(name = "orderTerm")
    val orderTerm: String = "",
    @Json(name = "acctDesc")
    val acctDesc: String = "",
    @Json(name = "stopPrice")
    val stopPrice: String = "",
    @Json(name = "optionLevelCd")
    val optionLevelCd: String = "0",
    @Json(name = "limitPrice")
    val limitPrice: String = "",
    @Json(name = "orderNumber")
    val orderNumber: String = "",
    @Json(name = "destMktCode")
    val destMktCode: String = "",
    @Json(name = "origSysCodeReq")
    val origSysCodeReq: String = "57",
    @Json(name = "orderSource")
    val orderSource: String = "android",
    @Json(name = "dayTradingFlag")
    val dayTradingFlag: String = "true",
    @Json(name = "dayTradingVer")
    val dayTradingVer: String = "1",
    @Json(name = "overnightIndicatorFlag")
    val overnightIndicatorFlag: String = "0",
    @Json(name = "clearancecode")
    val clearanceCode: String = "",
    @Json(name = "legRequestList")
    val legRequestList: List<LegDetailsCommonDto>

)
