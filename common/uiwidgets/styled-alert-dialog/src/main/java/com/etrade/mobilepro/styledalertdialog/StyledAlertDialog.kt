package com.etrade.mobilepro.styledalertdialog

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.graphics.Typeface
import android.view.LayoutInflater
import android.widget.TextView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.lang.ref.WeakReference

class StyledAlertDialog(context: Context) {
    private val weakContext = WeakReference(context)

    @SuppressWarnings("LongMethod")
    @SuppressLint("InflateParams")
    fun showAlert(
        title: String?,
        message: String?,
        positiveAnswerAction: (() -> Unit)? = null,
        negativeAnswerAction: (() -> Unit)? = null,
        dismissAction: (() -> Unit)? = null
    ) {
        weakContext.get()?.let {
            val titleView = LayoutInflater.from(it).inflate(R.layout.alert_dialog_title, null) as TextView
            titleView.text = title
            val dialog = MaterialAlertDialogBuilder(it)
                .setCustomTitle(titleView)
                .setMessage(message)
                .setPositiveButton(R.string.yes) { dialog, _ ->
                    positiveAnswerAction?.invoke()
                    dialog.dismiss()
                }
                .setNegativeButton(R.string.no) { dialog, _ ->
                    negativeAnswerAction?.invoke()
                    dialog.dismiss()
                }
                .setOnDismissListener {
                    dismissAction?.invoke()
                }
                .create()
            dialog.setOnShowListener {
                val btnPositive = dialog.getButton(DialogInterface.BUTTON_POSITIVE)
                btnPositive.setTypeface(btnPositive.typeface, Typeface.BOLD)
                btnPositive.isAllCaps = false
                val btnNegative = dialog.getButton(DialogInterface.BUTTON_NEGATIVE)
                btnNegative.setTypeface(btnNegative.typeface, Typeface.BOLD)
                btnNegative.isAllCaps = false
            }
            dialog.show()
        }
    }

    @SuppressLint("InflateParams")
    fun showAlert(
        title: String?,
        message: String?,
        isCancelable: Boolean,
        positiveAnswerAction: (() -> Unit)? = null
    ) {
        weakContext.get()?.let {
            val titleView = LayoutInflater.from(it).inflate(R.layout.alert_dialog_title, null)
            (titleView as? TextView)?.text = title
            val dialog = MaterialAlertDialogBuilder(it)
                .setCustomTitle(titleView)
                .setMessage(message)
                .setCancelable(isCancelable)
                .setPositiveButton(R.string.ok) { dialog, _ ->
                    positiveAnswerAction?.invoke()
                    dialog.dismiss()
                }
                .create()
            dialog.setOnShowListener {
                val btn = dialog.getButton(DialogInterface.BUTTON_POSITIVE)
                btn.setTypeface(btn.typeface, Typeface.BOLD)
            }
            dialog.show()
        }
    }
}
