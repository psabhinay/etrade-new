package com.etrade.mobilepro.searching.symbol.cache

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import org.threeten.bp.Duration

class CachedSymbolsSearchController(private val expiresIn: Duration) : SimpleCachedResource.Controller<List<SearchResultItem.Symbol>> {
    override fun shouldRefresh(itemFromCache: List<SearchResultItem.Symbol>): Boolean {
        return itemFromCache.isEmpty() || hasExpired(itemFromCache)
    }

    private fun hasExpired(items: List<SearchResultItem.Symbol>): Boolean {
        return items.find {
            System.currentTimeMillis() - it.cachedAt >= expiresIn.toMillis()
        } != null
    }
}
