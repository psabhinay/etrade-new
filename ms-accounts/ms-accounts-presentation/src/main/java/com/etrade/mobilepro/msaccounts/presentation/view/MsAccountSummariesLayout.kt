package com.etrade.mobilepro.msaccounts.presentation.view

import android.content.Context
import android.util.AttributeSet
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.common.customview.LifecycleLinearLayout
import com.etrade.mobilepro.msaccounts.presentation.R
import com.etrade.mobilepro.msaccounts.presentation.databinding.MsAccountSummaryBinding
import com.etrade.mobilepro.msaccounts.presentation.model.MsAccountSummary
import com.etrade.mobilepro.util.android.extension.layoutInflater

class MsAccountSummariesLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : LifecycleLinearLayout(context, attrs) {

    fun bindMsAccountSummaries(sections: List<MsAccountSummary>) {
        removeAllViews()
        sections.forEach {
            if (childCount > 0) {
                layoutInflater.inflate(R.layout.ms_account_divider, this, true)
            }
            val binding = MsAccountSummaryBinding.inflate(layoutInflater, this, true)
            binding.lifecycleOwner = this
            binding.msAccount = it
        }
    }
}

@BindingAdapter("msAccounts")
fun bindMsAccountSummaries(
    sectionsLayout: MsAccountSummariesLayout,
    sections: List<MsAccountSummary>?
) {
    sectionsLayout.bindMsAccountSummaries(sections.orEmpty())
}
