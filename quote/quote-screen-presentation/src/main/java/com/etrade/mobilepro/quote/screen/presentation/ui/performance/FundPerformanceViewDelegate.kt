package com.etrade.mobilepro.quote.screen.presentation.ui.performance

import androidx.compose.runtime.Composable
import androidx.compose.ui.viewinterop.AndroidViewBinding
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.etrade.eo.corelibandroid.createViewModel
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.quote.screen.presentation.R
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteFundPerformanceWidgetBinding
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class FundPerformanceViewDelegate @AssistedInject constructor(
    @Assisted private val fragment: Fragment,
    @Assisted private val symbol: SearchResultItem.Symbol,
    assistedModelFactory: FundPerformanceViewModel.Factory
) {
    private lateinit var binding: QuoteFundPerformanceWidgetBinding
    private val viewModel = fragment.createViewModel { assistedModelFactory.create(symbol) }.value

    @AssistedFactory
    interface Factory {
        fun create(
            fragment: Fragment,
            symbol: SearchResultItem.Symbol
        ): FundPerformanceViewDelegate
    }
    fun onViewCreated() {
        fragment.lifecycle.addObserver(viewModel)
    }

    @Composable
    fun FundPerformanceQuoteViewCompose() {
        AndroidViewBinding(QuoteFundPerformanceWidgetBinding::inflate) {
            if (!::binding.isInitialized || binding != this) {
                binding = this
                binding.lifecycleOwner = fragment.viewLifecycleOwner
                quoteItem = viewModel
                fragment.bindFundViewModel()
                fragment.bindViews()
                this@FundPerformanceViewDelegate.viewModel.getMobileQuote(symbol)
            }
        }
    }

    private fun Fragment.bindViews() {
        binding.disclosureView.disclosureTitle.setOnClickListener { setupDisclosures() }
        binding.disclosureView.definitionsTitle.setOnClickListener { setUpDefinitionsDialog() }
    }

    private fun Fragment.bindFundViewModel() {
        viewModel.disclosureClickListener = {
            findNavController().navigate(R.id.mfQuoteDisclosureFragment)
        }
    }

    private fun setupDisclosures() {
        fragment.apply {
            startActivity(
                SpannableTextOverlayActivity.intent(
                    requireContext(),
                    getString(R.string.disclosures),
                    getString(R.string.mutual_fund_disclosure),
                    R.drawable.ic_close
                )
            )
        }
    }

    private fun setUpDefinitionsDialog() {
        fragment.apply {
            startActivity(
                SpannableTextOverlayActivity.intent(
                    requireContext(),
                    getString(R.string.definitions),
                    getString(R.string.mutual_fund_definitions),
                    R.drawable.ic_close
                )
            )
        }
    }
}
