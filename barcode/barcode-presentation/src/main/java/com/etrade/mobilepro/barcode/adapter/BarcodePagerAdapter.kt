package com.etrade.mobilepro.barcode.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class BarcodePagerAdapter(
    private val fragment: Fragment,
    private val barcodeTabs: List<BarcodeTab>
) : FragmentStateAdapter(fragment) {

    fun getPageTitle(position: Int): String {
        return fragment.getString(barcodeTabs[position].title)
    }

    override fun getItemCount() = barcodeTabs.size

    override fun createFragment(position: Int) = barcodeTabs[position].fragmentClass.let { clazz ->
        fragment.childFragmentManager.fragmentFactory
            .instantiate(clazz.classLoader!!, clazz.name)
    }
}
