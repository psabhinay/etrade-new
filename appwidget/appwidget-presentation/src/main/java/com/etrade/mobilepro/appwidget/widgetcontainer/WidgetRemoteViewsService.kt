package com.etrade.mobilepro.appwidget.widgetcontainer

import android.content.Intent
import android.widget.RemoteViewsService
import com.etrade.mobilepro.appwidget.api.WidgetViewStateRepo
import com.etrade.mobilepro.appwidget.common.getAdapterTypeOrThrow
import com.etrade.mobilepro.appwidget.common.getIndexChangeColorPicker
import com.etrade.mobilepro.appwidget.common.getLayoutTypeOrThrow
import com.etrade.mobilepro.appwidget.indices.IndicesRemoteViewsAdapter
import com.etrade.mobilepro.appwidget.watchlist.WatchlistRemoteViewsAdapter
import com.etrade.mobilepro.util.color.DataColorResolver
import dagger.android.AndroidInjection
import javax.inject.Inject

class WidgetRemoteViewsService : RemoteViewsService() {

    @Inject
    lateinit var viewStateRepo: WidgetViewStateRepo

    @Inject
    lateinit var dataColorResolver: DataColorResolver

    override fun onCreate() {
        super.onCreate()
        AndroidInjection.inject(this)
    }

    override fun onGetViewFactory(intent: Intent?): RemoteViewsFactory {

        return when (requireNotNull(intent).getAdapterTypeOrThrow()) {
            AdapterType.Indices -> IndicesRemoteViewsAdapter(
                context = applicationContext,
                changeColorPicker = getIndexChangeColorPicker(),
                repo = viewStateRepo
            )
            AdapterType.Watchlist -> WatchlistRemoteViewsAdapter(
                context = applicationContext,
                repo = viewStateRepo,
                dataColorResolver = dataColorResolver,
                layout = intent.getLayoutTypeOrThrow()
            )
        }
    }
}
