package com.etrade.mobilepro.encryption.data

import com.etrade.mobilepro.encryption.api.Base64Processor
import com.etrade.mobilepro.encryption.api.CipherAuthenticator
import com.etrade.mobilepro.encryption.api.EncryptionProvider
import com.etrade.mobilepro.encryption.api.SecretKeyMaterial
import com.etrade.mobilepro.encryption.api.SecretKeyRepo
import com.etrade.mobilepro.encryption.api.transformation
import com.etrade.mobilepro.util.secure.nextByteArray
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.security.MessageDigest
import java.security.SecureRandom
import java.util.Random
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.GCMParameterSpec
import javax.inject.Inject

private const val AUTHENTICATION_TAG_LENGTH = 128
private const val IV_LENGTH = 12

private const val CIPHERTEXT_DELIMITER = '.'

class DefaultEncryptionProvider @Inject constructor(
    private val secretKeyRepo: SecretKeyRepo,
    private val base64Processor: Base64Processor
) : EncryptionProvider {

    private val cipher: Cipher = Cipher.getInstance(secretKeyRepo.algorithmSpec.transformation)
    private val digest: MessageDigest = MessageDigest.getInstance("SHA-256")
    private val random: Random = SecureRandom()

    private val cipherMutex: Mutex = Mutex()

    override fun isSecureEncryptionSupported(): Boolean = secretKeyRepo.commonKey.secretKey.isInsideSecureHardware

    override suspend fun encrypt(plaintext: String, keyMaterial: SecretKeyMaterial, authenticate: CipherAuthenticator): String {
        return cipherMutex.withLock {
            val iv = random.nextByteArray(IV_LENGTH)
            val ciphertext = performOperation(Cipher.ENCRYPT_MODE, iv, plaintext.toByteArray(), keyMaterial, authenticate)
            base64Processor.encode(iv) + CIPHERTEXT_DELIMITER + base64Processor.encode(ciphertext)
        }
    }

    override suspend fun decrypt(encrypted: String, keyMaterial: SecretKeyMaterial, authenticate: CipherAuthenticator): String {
        return cipherMutex.withLock {
            val (iv, ciphertext) = encrypted.split(CIPHERTEXT_DELIMITER)
                .map { base64Processor.decode(it) }
            String(performOperation(Cipher.DECRYPT_MODE, iv, ciphertext, keyMaterial, authenticate))
        }
    }

    private suspend fun performOperation(
        mode: Int,
        iv: ByteArray,
        input: ByteArray,
        keyMaterial: SecretKeyMaterial,
        authenticate: CipherAuthenticator
    ): ByteArray {
        return cipher.run {
            init(mode, keyMaterial.secretKey, iv)
            takeIf { keyMaterial.isUserAuthenticationRequired }
                ?.let { authenticate(it) }
                ?: this
        }.doFinal(input)
    }

    override suspend fun hash(value: String): String {
        return synchronized(digest) {
            digest.run {
                reset()
                base64Processor.encode(digest(value.toByteArray()))
            }
        }
    }

    private fun Cipher.init(mode: Int, secretKey: SecretKey, iv: ByteArray) {
        init(mode, secretKey, GCMParameterSpec(AUTHENTICATION_TAG_LENGTH, iv))
    }
}
