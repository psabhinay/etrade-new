package com.etrade.mobilepro.positions.api

interface TaxLotPosition : Position {
    val taxLotId: String
    val acquiredDateMs: Long
}
