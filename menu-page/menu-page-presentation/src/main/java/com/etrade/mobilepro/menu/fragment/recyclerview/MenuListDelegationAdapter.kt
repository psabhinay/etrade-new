package com.etrade.mobilepro.menu.fragment.recyclerview

import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.dynamicui.api.DynamicLayout
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.TextMenuItem
import com.etrade.mobilepro.menu.fragment.recyclerview.delegate.DividerMenuItemDelegate
import com.etrade.mobilepro.menu.fragment.recyclerview.delegate.MessagesMenuItemDelegate
import com.etrade.mobilepro.menu.fragment.recyclerview.delegate.TextMenuItemDelegate
import com.etrade.mobilepro.menupage.presentation.R
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class MenuListDelegationAdapter(
    onMenuItemClickListener: (TextMenuItem) -> Unit,
    lifecycleOwner: LifecycleOwner,
    toDynamicViewModels: (List<GenericLayout>) -> List<DynamicLayout>
) : ListDelegationAdapter<MutableList<MenuItem>>() {

    init {
        delegatesManager.addDelegate(DividerMenuItemDelegate())
        delegatesManager.addDelegate(TextMenuItemDelegate(R.layout.view_menu_text, TextMenuItem.Type.Text, onMenuItemClickListener))
        delegatesManager.addDelegate(TextMenuItemDelegate(R.layout.view_menu_with_arrow, TextMenuItem.Type.TextWithArrow, onMenuItemClickListener))
        delegatesManager.addDelegate(
            TextMenuItemDelegate(
                R.layout.view_menu_with_arrow_and_subtitle,
                TextMenuItem.Type.TextWithArrowAndSubtitle,
                onMenuItemClickListener
            )
        )
        delegatesManager.addDelegate(TextMenuItemDelegate(R.layout.view_menu_with_count, TextMenuItem.Type.Count, onMenuItemClickListener))
        delegatesManager.addDelegate(TextMenuItemDelegate(R.layout.view_menu_highlight_with_subtitle, TextMenuItem.Type.Highlight, onMenuItemClickListener))
        delegatesManager.addDelegate(MessagesMenuItemDelegate(lifecycleOwner, toDynamicViewModels))

        items = mutableListOf()
    }

    fun updateItems(items: List<MenuItem>) {
        this.items.clear()
        this.items.addAll(items)

        notifyDataSetChanged()
    }
}
