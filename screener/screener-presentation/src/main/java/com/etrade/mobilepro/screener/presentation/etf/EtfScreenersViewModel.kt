package com.etrade.mobilepro.screener.presentation.etf

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.datasource.ScreenerSetsDataSource
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Etf
import com.etrade.mobilepro.screener.presentation.base.BaseScreenersViewModel
import com.etrade.mobilepro.tracking.Tracker
import javax.inject.Inject

class EtfScreenersViewModel @Inject constructor(
    screenerSetsDataSource: ScreenerSetsDataSource,
    @Etf screenerInteractor: ScreenerInteractor,
    tracker: Tracker
) : BaseScreenersViewModel(
    screenerGroup = ScreenerType.Group.ETF,
    screenerSetsDataSource = screenerSetsDataSource,
    screenerInteractor = screenerInteractor,
    tracker = tracker
)
