package com.etrade.mobilepro.settings

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.R
import com.etrade.mobilepro.biometric.api.BiometricsProvider
import com.etrade.mobilepro.biometric.presentation.enabler.BiometricEnablerError
import com.etrade.mobilepro.biometric.presentation.enabler.BiometricEnablerFragment
import com.etrade.mobilepro.common.ALERT_TERMS_AND_CONDITIONS_URL
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.getOverlayIntentForUrlAction
import com.etrade.mobilepro.common.setupToolbarWithUpButton
import com.etrade.mobilepro.databinding.FragmentSettingsBinding
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.dropdown.DropDownSelector
import com.etrade.mobilepro.dynamicui.GenericRecyclerAdapter
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.toDynamicViewModels
import com.etrade.mobilepro.livedata.observeIn
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.settings.viewmodel.SettingValuePickerPayload
import com.etrade.mobilepro.settings.viewmodel.SettingsViewModel
import com.etrade.mobilepro.settings.viewmodel.feature.HasAction
import com.etrade.mobilepro.settings.viewmodel.feature.HasActionsMap
import com.etrade.mobilepro.settings.viewmodel.feature.HasDropDown
import com.etrade.mobilepro.settings.viewmodel.feature.ViewModelAction
import com.etrade.mobilepro.settings.viewmodel.feature.ViewModelActionId
import com.etrade.mobilepro.settings.viewmodel.feature.ViewModelActionResult
import com.etrade.mobilepro.settings.viewmodel.feature.ViewModelActionResultType
import com.etrade.mobilepro.settings.viewmodel.feature.sendActionResult
import com.etrade.mobilepro.streaming.StreamingStatusNotifierDelegate
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.usersubscriptionpresentation.UserSubscriptionsWebViewActivity
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

const val OVERLAY = "overlay"

private const val BIOMETRIC_ENABLER_FRAGMENT_TAG = "BIOMETRIC_ENABLER_FRAGMENT_TAG"
private val POPUP_DIALOG_TAG = "${SettingsFragment::class.java.canonicalName}_popup"

class SettingsFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val userViewModel: UserViewModel,
    private val fragmentInjectionFactory: FragmentFactory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val streamingStatusController: StreamingStatusController,
    private val streamingStatusNotifierDelegate: StreamingStatusNotifierDelegate,
    @Web private val baseWebUrl: String,
    private val biometricsProvider: BiometricsProvider,
) : Fragment(R.layout.fragment_settings) {

    private val binding by viewBinding(FragmentSettingsBinding::bind)

    private val settingsViewModel: SettingsViewModel by viewModels { viewModelFactory }
    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private val viewAdapter: GenericRecyclerAdapter
        get() = binding.settingsRecyclerView.adapter as GenericRecyclerAdapter
    private val dropDownsMap = mutableMapOf<String, DropDownSelector>()
    private val hasActionsMap = HasActionsMap()

    private val clickObserver: Observer<CtaEvent> by lazy {
        Observer<CtaEvent> { ctaEvent ->
            when (ctaEvent) {
                is CtaEvent.ActionEvent -> {
                    val url = ctaEvent.url

                    if (url.contains(OVERLAY)) {
                        activity?.let {
                            startActivity(getOverlayIntentForUrlAction(url, ctaEvent.title, it))
                        }
                    } else {
                        findNavController().navigate(Uri.parse(url))
                    }
                }
                is CtaEvent.UriEvent -> {
                    findNavController().navigate(ctaEvent.uri)
                }
                is CtaEvent.NavDirectionsNavigation -> {
                    findNavController().navigate(ctaEvent.ctaDirections)
                }
            }
        }
    }

    private val webViewRequest = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        streamingStatusNotifierDelegate.init(this, requireActivity())
        setupFragment()
        bindViewModel()
    }

    override fun onStart() {
        super.onStart()
        (activity as? AppCompatActivity)?.setupToolbarWithUpButton(
            titleResId = R.string.settings,
            upButtonResId = R.drawable.ic_close
        )
    }

    override fun onResume() {
        super.onResume()
        checkIfBiometricLoginToggleShouldBeEnabled()
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            ViewModelActionId.getActionByRequestCode(bundle.customDialogRequestCode)
                ?.let { takeActionOnPopupAndActivityResult(it, ViewModelActionResultType.Success) }
        }
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_CANCELLED) { _, bundle ->
            ViewModelActionId.getActionByRequestCode(bundle.customDialogRequestCode)
                ?.let { takeActionOnPopupAndActivityResult(it, ViewModelActionResultType.Cancel) }
        }
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_TEXT_AREA_TAPPED) { _, _ ->
            startActivity(
                getOverlayIntentForUrlAction(
                    ALERT_TERMS_AND_CONDITIONS_URL,
                    getString(R.string.alert_terms_and_conditions),
                    requireActivity()
                )
            )
        }
    }

    @SuppressWarnings("ComplexMethod")
    private fun takeActionOnPopupAndActivityResult(
        viewModelActionId: ViewModelActionId,
        viewModelActionResultType: ViewModelActionResultType
    ) {
        when (viewModelActionId) {
            ViewModelActionId.SHOW_ACCOUNT_ALERT_POPUP,
            ViewModelActionId.SHOW_STOCK_ALERT_POPUP,
            ViewModelActionId.BIOMETRIC_RESULT -> {
                sendToggleEnablingResult(viewModelActionId, viewModelActionResultType)
            }
            ViewModelActionId.MARKET_DATA_AGREEMENT_RESULT -> {
                if (streamingStatusController.isStreamingToggleEnabled) {
                    sendToggleEnablingResult(viewModelActionId, ViewModelActionResultType.Success)
                } else {
                    sendToggleEnablingResult(viewModelActionId, ViewModelActionResultType.Cancel)
                }
            }
            ViewModelActionId.START_ENABLING_BIOMETRIC -> startBiometricEnablingProcess()
            ViewModelActionId.START_MARKET_DATA_AGREEMENT -> {
                when (viewModelActionResultType) {
                    is ViewModelActionResultType.Success -> startMarketDataAgreementWebView()
                    else -> sendToggleEnablingResult(viewModelActionId, viewModelActionResultType)
                }
            }
            else -> { /* do nothing */
            }
        }
    }

    private fun sendToggleEnablingResult(
        viewModelActionId: ViewModelActionId,
        viewModelActionResultType: ViewModelActionResultType
    ) {
        hasActionsMap.sendActionResult(
            viewModelActionId,
            ViewModelActionResult.ToggleEnablingResult(
                viewModelActionResultType,
                ViewModelActionResultType.Success == viewModelActionResultType
            )
        )
    }

    private fun startMarketDataAgreementWebView() {
        webViewRequest.launch(UserSubscriptionsWebViewActivity.intent(requireContext(), baseWebUrl))
    }

    override fun onDestroyView() {
        dropDownsMap.clear()
        super.onDestroyView()
    }

    private fun setupFragment() {
        binding.settingsRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = GenericRecyclerAdapter(this@SettingsFragment, emptyList())
        }

        activity?.supportFragmentManager?.findFragmentByTag(BIOMETRIC_ENABLER_FRAGMENT_TAG)?.let {
            (it as BiometricEnablerFragment).bindBiometricCallbacks()
        }
    }

    private fun bindViewModel() {
        settingsViewModel.getViewState().observeIn(this) {
            when (it) {
                is SettingsViewModel.ViewState.Loading -> {
                    binding.loadingIndicator.show()
                }
                is SettingsViewModel.ViewState.Error -> {
                    binding.loadingIndicator.hide()
                }
                is SettingsViewModel.ViewState.Success -> {
                    onSuccessViewState(it)
                }
            }
        }
        userViewModel.userSessionState.observeIn(this) {
            settingsViewModel.loadSettings(it.peekContent())
        }
    }

    private fun onSuccessViewState(it: SettingsViewModel.ViewState.Success) {
        viewAdapter.values = it.result.toDynamicViewModels(this) { model ->
            model.clickEvents.observe(viewLifecycleOwner, clickObserver)
            if (model is HasDropDown) {
                setupHasDropDownViewModel(model)
            }
            if (model is HasAction) {
                setupHasAction(model)
                model.actionSet.forEach {
                    hasActionsMap[it] = model
                }
            }
            when (model) {
                is SettingValuePickerPayload -> {
                    // Workaround to fix case when we open drop down from recycler view item
                    // and then do orientation event and this item now not present in the screen
                    // it's not possible to handle drop down event after orientation changes in case if the recycler view item was destroyed
                    model.initValuePickerDropDown(childFragmentManager)
                }
            }
        }
        binding.loadingIndicator.hide()
    }

    private fun startBiometricEnablingProcess() {
        val fragment = initBiometricEnablerFragment()
        fragment.bindBiometricCallbacks()
        fragment.startEnablingProcess()
    }

    private fun initBiometricEnablerFragment(): BiometricEnablerFragment {
        val fragment = fragmentInjectionFactory.instantiateFragment(
            BiometricEnablerFragment::class.java
        ) as BiometricEnablerFragment
        activity?.supportFragmentManager?.beginTransaction()?.add(fragment, BIOMETRIC_ENABLER_FRAGMENT_TAG)?.commitNow()
        return fragment
    }

    private fun BiometricEnablerFragment.bindBiometricCallbacks() {
        onAuthenticationSuccess = {
            takeActionOnPopupAndActivityResult(ViewModelActionId.BIOMETRIC_RESULT, ViewModelActionResultType.Success)
        }
        onAuthenticationFail = {
            takeActionOnPopupAndActivityResult(ViewModelActionId.BIOMETRIC_RESULT, ViewModelActionResultType.Error(""))
            if (it == BiometricEnablerError.BIOMETRIC_INVALID_PASSWORD) {
                userViewModel.userSessionStateChange(SessionStateChange.UNAUTHENTICATED)
            }
        }
        onAuthenticationCancel = {
            takeActionOnPopupAndActivityResult(ViewModelActionId.BIOMETRIC_RESULT, ViewModelActionResultType.Cancel)
        }
    }

    private fun setupHasAction(hasActionViewModel: HasAction) {
        hasActionViewModel.actionSignal.observeIn(this) { actionSignal ->
            actionSignal.getNonConsumedContent()?.let {
                takeActionOnViewModelActionSignalReceived(it)
            }
        }
    }

    private fun takeActionOnViewModelActionSignalReceived(viewModelAction: ViewModelAction) {
        when (viewModelAction) {
            is ViewModelAction.Popup -> showPopUp(viewModelAction)
            is ViewModelAction.EnableBiometric -> startBiometricEnablingProcess()
            is ViewModelAction.ErrorSnackBar -> snackbarUtil.errorSnackbar(getString(R.string.error_message_general))
        }
    }

    private fun showPopUp(viewModelAction: ViewModelAction.Popup) {
        var message = viewModelAction.message
        val clickableMessageRanges = if (viewModelAction.showAlertTermsAndConditions) {
            val alertsAndConditionsPart = getString(R.string.alert_terms_and_conditions)
            message = "${message}\n\n$alertsAndConditionsPart"
            listOf(
                Pair(first = viewModelAction.message.length, second = message.length)
            )
        } else {
            emptyList()
        }

        val cancelTitle = if (viewModelAction.showCancelButton) {
            viewModelAction.cancelTitle ?: getString(R.string.no)
        } else {
            null
        }
        val dialog = CustomDialogFragment.newInstance(
            title = viewModelAction.title,
            message = message,
            okTitle = viewModelAction.okTitle ?: getString(R.string.yes),
            cancelTitle = cancelTitle,
            cancelable = false,
            clickableMessageRanges = clickableMessageRanges,
            requestCode = viewModelAction.viewModelActionId.requestCode
        )
        dialog.show(parentFragmentManager, POPUP_DIALOG_TAG)
    }

    private fun setupHasDropDownViewModel(viewModel: HasDropDown) {
        dropDownsMap[viewModel.dropDownTag] = viewModel.createDropDownSelector(parentFragmentManager)
        viewModel.viewState.observeIn(this) { viewState ->
            viewState.openDropDownSelectorWithTag?.consume { selectorTag ->
                dropDownsMap[selectorTag]?.open()
            }
        }
    }

    private fun checkIfBiometricLoginToggleShouldBeEnabled() {
        if (!biometricsProvider.canAuthenticate().isSuccessful) {
            sendToggleEnablingResult(
                ViewModelActionId.BIOMETRIC_RESULT,
                ViewModelActionResultType.Cancel
            )
        }
    }
}
