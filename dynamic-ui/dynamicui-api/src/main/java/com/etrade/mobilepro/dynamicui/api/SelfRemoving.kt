package com.etrade.mobilepro.dynamicui.api

import io.reactivex.Observable

interface SelfRemoving {
    /**
     * Represents signal, that this widget instance wants to be removed from the recycler view
     * Should return on MainThread
     */
    fun selfRemovalRequest(): Observable<Boolean>
}
