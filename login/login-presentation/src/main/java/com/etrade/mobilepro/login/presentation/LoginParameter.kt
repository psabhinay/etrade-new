package com.etrade.mobilepro.login.presentation

import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticator

class LoginParameter(
    val authenticator: BiometricCipherAuthenticator,
    val isUserInitiated: Boolean = BiometricAuthenticator.USER_INITIATED_BY_DEFAULT,
    val shouldHandleLanding: Boolean = false
)
