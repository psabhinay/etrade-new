package com.etrade.mobilepro.tradingdefaults.viewholder

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.tradingdefaults.R
import com.etrade.mobilepro.tradingdefaults.TradingDefaultsItem
import com.etrade.mobilepro.tradingdefaults.databinding.AdapterTradingDefaultsDividerTitleBinding
import com.etrade.mobilepro.util.android.extension.viewBinding

class TradingDefaultsDividerTitleViewHolder(private val binding: AdapterTradingDefaultsDividerTitleBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: TradingDefaultsItem.DividerTitle) {
        binding.title.text = item.title
        binding.title.contentDescription =
            binding.root.context.getString(R.string.label_header_description, item.title)
    }

    companion object {
        fun from(parent: ViewGroup) = TradingDefaultsDividerTitleViewHolder(
            parent.viewBinding(AdapterTradingDefaultsDividerTitleBinding::inflate)
        )
    }
}
