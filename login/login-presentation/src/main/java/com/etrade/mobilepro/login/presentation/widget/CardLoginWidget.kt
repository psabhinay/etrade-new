package com.etrade.mobilepro.login.presentation.widget

import android.content.Context
import android.text.Editable
import android.text.Selection
import android.text.SpanWatcher
import android.text.Spannable
import android.text.Spanned
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.Checkable
import android.widget.EditText
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.doOnPreDraw
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.etrade.mobilepro.login.presentation.LoginViewModel
import com.etrade.mobilepro.login.presentation.NO_SELECTION
import com.etrade.mobilepro.login.presentation.R
import com.etrade.mobilepro.login.widget.LoginResultDialogFragment
import com.google.android.material.internal.CheckableImageButton
import com.google.android.material.textfield.TextInputLayout.END_ICON_PASSWORD_TOGGLE

class CardLoginWidget : LoginWidget {

    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    private var activity: AppCompatActivity = (context as AppCompatActivity)
    private val textChangeListeners = mutableListOf<TextWatcher>()

    @Suppress("EmptyFunctionBlock")
    private val usernameSelectionWatcher =
        object : SpanWatcher {
            override fun onSpanAdded(text: Spannable?, what: Any?, start: Int, end: Int) {}
            override fun onSpanRemoved(text: Spannable?, what: Any?, start: Int, end: Int) {}
            override fun onSpanChanged(text: Spannable?, what: Any?, ostart: Int, oend: Int, nstart: Int, nend: Int) {
                doOnPreDraw {
                    loginViewModel?.apply {
                        when (what) {
                            Selection.SELECTION_START -> usernameSelectionStart = nstart
                            Selection.SELECTION_END -> usernameSelectionEnd = nend
                        }
                    }
                }
            }
        }

    @Suppress("EmptyFunctionBlock")
    private val passwordSelectionWatcher =
        object : SpanWatcher {
            override fun onSpanAdded(text: Spannable?, what: Any?, start: Int, end: Int) {}
            override fun onSpanRemoved(text: Spannable?, what: Any?, start: Int, end: Int) {}
            override fun onSpanChanged(text: Spannable?, what: Any?, ostart: Int, oend: Int, nstart: Int, nend: Int) {
                doOnPreDraw {
                    loginViewModel?.apply {
                        when (what) {
                            Selection.SELECTION_START -> passwordSelectionStart = nstart
                            Selection.SELECTION_END -> passwordSelectionEnd = nend
                        }
                    }
                }
            }
        }

    override fun onFinishInflate() {
        super.onFinishInflate()
        setupCardView()
    }

    private fun setupCardView() {
        binding.loginEtradeLogo.visibility = View.GONE
        binding.signUpButton.visibility = View.GONE
    }

    override fun initLoginScreen(savedUserName: String?, isRememberUserChecked: Boolean) {
        binding.loginRememberUserCheckbox.isChecked = isRememberUserChecked

        if (isRememberUserChecked && savedUserName?.isNotEmpty() == true) {
            binding.loginUserName.setText(savedUserName)
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        val lifecycleOwner = context as? LifecycleOwner ?: throw IllegalArgumentException("view component is not a lifecycle Owner implementation")
        activity = lifecycleOwner as AppCompatActivity
        attachTo(lifecycleOwner)

        restoreViewSelection()
        LoginResultDialogFragment.restore(activity.supportFragmentManager)?.onAttached()

        binding.loginUserName.text?.apply { updateSpan(usernameSelectionWatcher, length) }
        binding.loginPassword.text?.apply { updateSpan(passwordSelectionWatcher, length) }
    }

    @Suppress("LongMethod")
    fun bindViewModel(viewModel: LoginViewModel?) {
        viewModel?.apply {
            loginViewModel = this
            setupView(
                activity = activity,
                biometricDelegate = biometricAuthenticationDelegate,
                shouldHandleLanding = true
            )

            // This approach is required to persist in memory username and password EditText's values
            // because the way dynamic-ui works does not trigger onSaveInstanceState nor onRestoreInstanceState callbacks
            // in GenericRecyclerAdapter's child views.
            with(binding) {

                setupLoginField(
                    loginUserName,
                    retainedUsername,
                    { _, event ->
                        // if retain username selection isn't postponed here, it can be called before
                        // focus switches to the tapped view
                        when (event.action) {
                            MotionEvent.ACTION_UP,
                            MotionEvent.ACTION_CANCEL -> doOnPreDraw { retainUsernameSelection() }
                        }
                        false
                    }
                ) { str -> retainedUsername = str }

                setupLoginField(
                    loginPassword,
                    retainedPassword,
                    { _, event ->
                        when (event.action) {
                            MotionEvent.ACTION_UP,
                            MotionEvent.ACTION_CANCEL -> doOnPreDraw { retainPasswordSelection() }
                        }
                        false
                    }
                ) { str -> retainedPassword = str }

                setupLoginField(
                    loginSecurityCode,
                    retainedSecurityCode
                ) { str -> retainedSecurityCode = str }

                if (loginPasswordLayout.endIconMode == END_ICON_PASSWORD_TOGGLE) {
                    updateForPasswordTransformation(hidePasswordState)

                    loginPasswordLayout.setEndIconOnClickListener {
                        hidePasswordState = !hidePasswordState
                        retainPasswordSelection()
                        updateForPasswordTransformation(hidePasswordState)
                        restoreViewSelection()
                    }
                }
            }
        }
    }

    private fun setupLoginField(
        editText: EditText,
        initialValue: String,
        onTouch: ((View, MotionEvent) -> Boolean)? = null,
        onAfterTextChange: (String) -> (Unit)
    ) {
        editText.apply {
            if (initialValue.isNotEmpty()) {
                setText(initialValue)
            }
            onTouch?.let { setOnTouchListener(it) }

            textChangeListeners.add(
                addTextChangedListener { editable ->
                    editable?.let { onAfterTextChange(text.toString()) }
                }
            )
        }
    }

    private fun updateForPasswordTransformation(isTransformed: Boolean) {
        binding.loginPassword.transformationMethod =
            if (isTransformed) {
                PasswordTransformationMethod.getInstance()
            } else { null }
        (
            binding.loginPasswordLayout
                .findViewById<CheckableImageButton>(R.id.text_input_end_icon) as Checkable
            ).isChecked = !isTransformed
    }

    private fun retainPasswordSelection() =
        loginViewModel?.apply {
            with(binding) {
                if (loginPassword.hasFocus()) {
                    passwordSelectionStart = loginPassword.selectionStart
                    passwordSelectionEnd = loginPassword.selectionEnd
                    clearUsernameSelection()
                }
            }
        }

    private fun retainUsernameSelection() =
        loginViewModel?.apply {
            with(binding) {
                if (loginUserName.hasFocus()) {
                    usernameSelectionStart = loginUserName.selectionStart
                    usernameSelectionEnd = loginUserName.selectionEnd
                    clearPasswordSelection()
                }
            }
        }

    private fun restoreViewSelection() {
        loginViewModel?.apply {
            with(binding) {
                when {
                    passwordSelectionStart != NO_SELECTION -> {
                        loginPassword.requestFocus()
                        loginPassword.setSelection(passwordSelectionStart, passwordSelectionEnd)
                    }
                    usernameSelectionStart != NO_SELECTION -> {
                        loginUserName.requestFocus()
                        loginUserName.setSelection(usernameSelectionStart, usernameSelectionEnd)
                    }
                    else -> initViewFocus()
                }
            }
        }
    }

    private fun initViewFocus() =
        if (binding.loginUserName.text.isNullOrBlank()) {
            binding.loginUserName.requestFocus()
        } else {
            binding.loginPassword.requestFocus()
        }

    @CallSuper
    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        textChangeListeners.apply {
            forEach {
                binding.loginUserName.removeTextChangedListener(it)
                binding.loginPassword.removeTextChangedListener(it)
            }
            clear()
        }
    }

    /*
        These lifecycle methods help to clear preferences after process death
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun onResumed() = loginViewModel?.setHidePasswordEnabled(true)

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun onPaused() = loginViewModel?.setHidePasswordEnabled(false)

    private fun Editable.updateSpan(watcher: SpanWatcher, endIndex: Int) {
        removeSpan(watcher)
        setSpan(
            watcher,
            0,
            endIndex.takeUnless { it == -1 } ?: 0,
            Spanned.SPAN_INCLUSIVE_INCLUSIVE
        )
    }
}
