package com.etrade.mobilepro.screener.data.json.mutualfund

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_LIKE
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.FeesFilter
import javax.inject.Inject

private const val FEES_FIELD = "Fees"

class FeesFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val feesFilter = screenerFilter as FeesFilter
        val arguments = mutableListOf<Argument>()

        if (feesFilter.isNotEmpty()) {
            arguments.add(
                Argument(getFeesClauses(feesFilter), FEES_FIELD)
            )
        }

        return PartialScreenerParams(arguments)
    }

    private fun getFeesClauses(feesFilter: FeesFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (feesFilter.noLoadTransaction) {
            clauses.add(noLoadTransactionClause)
            clauses.add(waivedClause)
        }
        if (feesFilter.transactionFee) {
            clauses.add(transactionFeeClause)
        }
        if (feesFilter.frontEndLoad) {
            clauses.add(frontEndLoadClause)
        }
        if (feesFilter.deferredLoad) {
            clauses.add(deferredLoadClause)
        }

        return clauses
    }

    private val noLoadTransactionClause =
        Clause(OPERATOR_LIKE, listOf("No Load No Transaction Fee"))
    private val transactionFeeClause =
        Clause(OPERATOR_LIKE, listOf("Transaction Fee"))
    private val deferredLoadClause =
        Clause(OPERATOR_LIKE, listOf("Deferred"))
    private val frontEndLoadClause =
        Clause(OPERATOR_LIKE, listOf("Initial"))
    private val waivedClause =
        Clause(OPERATOR_LIKE, listOf("Waived"))
}
