package com.etrade.mobilepro.markets.data.response

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false, name = "Output")
data class FutureQuote(
    @field:Element(name = "Symbol")
    var symbol: String? = null,
    @field:Element(name = "LastTradePrice")
    var price: String? = null,
    @field:Element(name = "PercentChange")
    var percentchange: String? = null,
    @field:Element(name = "Change")
    var change: String? = null,
    @field:Element(required = false, name = "formatedLastTradeDate")
    var lastTradeDate: String? = null,
    @field:Element(required = false, name = "formatedLastTradeTime")
    var lastTradeTime: String? = null,
    @field:Element(required = false, name = "ExpirationDate")
    var expireDate: String? = null
)
