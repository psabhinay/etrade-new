package com.etrade.mobilepro.personalnotifications.fragment

import com.etrade.mobilepro.common.di.DeviceType
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

internal const val VERIFY_EXTERNAL_ACCOUNT_URL = "/etx/mm/movemoney/manage-accounts"

class VerifyExternalAccountWebViewFragment @Inject constructor(
    @DeviceType deviceType: String
) : WebViewFragment(deviceType)
