package com.etrade.mobilepro.trade.presentation.usecase

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.inputValuesSnapshot
import com.etrade.mobilepro.trade.api.SaveOrderRepo
import com.etrade.mobilepro.trade.api.SaveOrderResponse
import com.etrade.mobilepro.util.UseCase
import javax.inject.Inject

interface SaveOrderUseCase : UseCase<Form, ETResult<SaveOrderResponse>>

class SaveOrderUseCaseImpl @Inject constructor(
    private val saveOrderRepo: SaveOrderRepo
) : SaveOrderUseCase {

    override suspend fun execute(parameter: Form): ETResult<SaveOrderResponse> {
        return saveOrderRepo.saveOrder(parameter.inputValuesSnapshot)
    }
}
