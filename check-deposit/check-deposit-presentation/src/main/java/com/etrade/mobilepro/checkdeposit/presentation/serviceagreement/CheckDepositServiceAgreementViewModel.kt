package com.etrade.mobilepro.checkdeposit.presentation.serviceagreement

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.checkdeposit.usecases.AcceptServiceAgreementUseCase
import com.etrade.mobilepro.util.invoke
import kotlinx.coroutines.launch
import javax.inject.Inject

class CheckDepositServiceAgreementViewModel @Inject constructor(
    private val acceptServiceAgreementUseCase: AcceptServiceAgreementUseCase
) : ViewModel() {

    private val _viewState: MutableLiveData<ViewState> = MutableLiveData()
    val viewState: LiveData<ViewState>
        get() = _viewState

    fun acceptServiceAgreement() {
        _viewState.value = ViewState.Loading

        viewModelScope.launch {
            acceptServiceAgreementUseCase().fold(
                onSuccess = {
                    // captureStatus ignored as in existing app
                    _viewState.value = ViewState.Success
                },
                onFailure = {
                    _viewState.value = ViewState.Error
                }
            )
        }
    }

    sealed class ViewState {

        object Loading : ViewState()
        object Error : ViewState()
        object Success : ViewState()
    }
}
