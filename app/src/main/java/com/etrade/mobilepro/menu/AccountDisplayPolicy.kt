package com.etrade.mobilepro.menu

enum class AccountDisplayPolicy {
    Default,
    ExcludeRetirementAccounts,
    IncludeRetirementAccountsOnly
}
