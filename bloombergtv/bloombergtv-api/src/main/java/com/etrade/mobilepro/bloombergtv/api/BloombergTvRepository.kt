package com.etrade.mobilepro.bloombergtv.api

import io.reactivex.Single

interface BloombergTvRepository {
    fun getBloombergtvResponse(): Single<BloombergTvResponse>
}
