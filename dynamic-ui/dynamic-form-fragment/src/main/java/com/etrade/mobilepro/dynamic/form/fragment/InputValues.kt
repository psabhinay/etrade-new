package com.etrade.mobilepro.dynamic.form.fragment

import android.os.Bundle
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.util.json.toJsonString
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types

private const val KEY_FORM_VALUES = "serializedFormValues"

fun createFormValuesArgument(moshi: Moshi, snapshot: InputValuesSnapshot): Bundle {
    return Bundle().apply {
        putString(KEY_FORM_VALUES, moshi.toJsonString(snapshot))
    }
}

fun restoreInputValuesSnapshot(moshi: Moshi, bundle: Bundle): InputValuesSnapshot {
    return bundle.getString(KEY_FORM_VALUES)?.let {
        val type = Types.newParameterizedType(Map::class.java, String::class.java, Any::class.java)
        moshi.adapter<InputValuesSnapshot>(type).fromJson(it)
    }.orEmpty()
}
