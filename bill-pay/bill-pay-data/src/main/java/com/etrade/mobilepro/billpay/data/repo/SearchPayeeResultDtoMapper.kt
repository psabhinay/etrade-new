package com.etrade.mobilepro.billpay.data.repo

import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.billpay.api.SearchPayeeResult
import com.etrade.mobilepro.billpay.api.SearchPayeeResultItem
import com.etrade.mobilepro.billpay.data.rest.searchpayee.SearchPayeeResponseBodyDto

fun ServerResponseDto<SearchPayeeResponseBodyDto>.toSearchResult(): SearchPayeeResult =
    when (extractError()) {
        ServerError.NoError -> {
            val searchResult = data?.payeeList?.map {
                SearchPayeeResultItem(
                    payeeId = it.payeeId,
                    name = it.name,
                    category = it.category,
                    merchantId = it.merchantId ?: 0,
                    merchantZipRequired = it.merchantZipRequired ?: false,
                    exactMatch = it.exactMatch ?: false
                )
            } ?: emptyList()

            if (searchResult.isEmpty()) {
                SearchPayeeResult.NoResults
            } else {
                SearchPayeeResult.Success(searchResult)
            }
        }
        ServerError.NullResponse -> {
            SearchPayeeResult.NoResults
        }
        is ServerError.Known -> {
            SearchPayeeResult.Error
        }
    }
