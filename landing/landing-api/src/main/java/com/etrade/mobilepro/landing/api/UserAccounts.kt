package com.etrade.mobilepro.landing.api

data class AccountBasicInfo(
    val uuid: String,
    val title: String,
    val accountId: String?
)

data class UserAccounts(
    val allAccounts: List<AccountBasicInfo>,
    val brokerageAccounts: List<AccountBasicInfo>,
    val bankAccounts: List<AccountBasicInfo>
) {
    val total: Int
        get() = allAccounts.size
    fun hasMoreThanOneAccount(): Boolean = total > 1

    fun getTitleOfAccount(uuid: String): String? = allAccounts.find { it.uuid == uuid }?.title
}
