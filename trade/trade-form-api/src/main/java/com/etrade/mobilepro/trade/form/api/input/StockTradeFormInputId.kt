package com.etrade.mobilepro.trade.form.api.input

interface StockTradeFormInputId : BaseTradeFormInputId, WithAdvancedTradeFormInputId, WithActionTradeFormInputId, WithQuantity, WithWalkthroughInputId {
    val positionLotId: String
    val originalQuantity: String
}
