package com.etrade.mobilepro.util.android.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.content.res.Resources
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.ref.WeakReference
import javax.inject.Inject

interface OrientationLock : LifecycleObserver {
    /**
     * @param fragment The fragment which orientation must be locked
     * @param lockedOrientation Desired locked orientation, must be one value from [ActivityInfo] (e.g. [ActivityInfo.SCREEN_ORIENTATION_PORTRAIT])
     * @param applyIfTablet false = Do not lock screen orientation on Tablet
     */
    fun init(fragment: Fragment, lockedOrientation: Int, applyIfTablet: Boolean = false)
}

class OrientationLockViewModel @Inject constructor() : ViewModel() {
    internal var initialOrientation: Int? = null
    internal var applyIfTablet: Boolean = false
}

class OrientationLockImpl @Inject constructor(
    private val resources: Resources,
    private val viewModelFactory: ViewModelProvider.Factory,
    private val isTablet: () -> Boolean
) : OrientationLock {

    private lateinit var fragmentRef: WeakReference<Fragment>
    private var lockedOrientation: Int = ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT
    private var shouldRestoreOrientation: Boolean = true

    private val isPortraitOrientation: Boolean
        get() = resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT
    private val viewModel: OrientationLockViewModel? by lazy {
        fragmentRef.get()
            ?.activityViewModels<OrientationLockViewModel> { viewModelFactory }
            ?.value
    }

    override fun init(fragment: Fragment, lockedOrientation: Int, applyIfTablet: Boolean) {
        if (::fragmentRef.isInitialized && fragmentRef.get() === fragment) {
            return
        }
        this.lockedOrientation = lockedOrientation
        fragmentRef = WeakReference(fragment)
        fragment.lifecycle.addObserver(this)
        viewModel?.applyIfTablet = applyIfTablet
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        withActivity { activity ->
            if (viewModel?.initialOrientation == null) {
                viewModel?.initialOrientation = activity.requestedOrientation
            }
            if (shouldApplyLock()) {
                activity.requestedOrientation = lockedOrientation
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    @SuppressLint("SourceLockedOrientationActivity")
    fun onResume() {
        withActivity {
            /*
            * To prevent infinity loop with onResume => onPause => onResume => onPause calls due orientation changes
            * check if orientation changed
            */
            if (shouldApplyLock() && !isPortraitOrientation) {
                shouldRestoreOrientation = false
                it.requestedOrientation = lockedOrientation
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPause() {
        withActivity {
            it.requestedOrientation = viewModel?.initialOrientation ?: ActivityInfo.SCREEN_ORIENTATION_FULL_USER
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        fragmentRef.clear()
    }

    private fun withActivity(block: (activity: Activity) -> Unit) {
        fragmentRef.get()?.activity?.apply { block(this) }
    }

    private fun shouldApplyLock(): Boolean = if (isTablet()) { viewModel?.applyIfTablet ?: false } else { true }
}
