package com.etrade.mobilepro.barcode

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.etrade.mobilepro.barcode.adapter.BarcodePagerAdapter
import com.etrade.mobilepro.barcode.adapter.BarcodeTab
import com.etrade.mobilepro.barcode.databinding.FragmentBarcodeBinding
import com.etrade.mobilepro.barcode.viewmodel.BarcodeSharedViewModel
import com.etrade.mobilepro.barcode.viewmodel.BarcodeViewModel
import com.etrade.mobilepro.common.Navigation
import com.etrade.mobilepro.common.OnFragmentBackPressListener
import com.etrade.mobilepro.common.OnNavigateUpListener
import com.etrade.mobilepro.common.SearchViewHandler
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.common.setupToolbar
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.tracking.initTabsTracking
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.afterStarted
import com.google.android.material.tabs.TabLayoutMediator
import javax.inject.Inject

class BarcodeFragment @Inject constructor(
    val navigation: MainNavigation,
    private val viewModelFactory: ViewModelProvider.Factory,
    private val searchViewHandler: SearchViewHandler
) : Fragment(R.layout.fragment_barcode), OnFragmentBackPressListener, OnNavigateUpListener {

    private val binding by viewBinding(FragmentBarcodeBinding::bind)
    private val barcodeViewModel: BarcodeViewModel by viewModels { viewModelFactory }
    private val barcodeSharedViewModel: BarcodeSharedViewModel by activityViewModels { viewModelFactory }
    private val barcodeFragments = listOf(BarcodeTab.Scan, BarcodeTab.Results)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toggleSearchView(false)
        initViews()
        barcodeSharedViewModel.quoteSymbolLookupFound.observe(viewLifecycleOwner) {
            it.getNonConsumedContent()?.let {
                afterStarted {
                    binding.viewpager.setCurrentItem(barcodeFragments.indexOf(BarcodeTab.Results), false)
                    navigation.navigateToQuoteDetails(activity, SearchResultItem.Symbol(it, InstrumentType.EQ).toBundle())
                }
            }
        }
    }

    override fun onNavigateUp(): Boolean {
        toggleSearchView(true)
        return false
    }

    override fun onBackPressed(): Boolean {
        toggleSearchView(true)
        return false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        searchViewHandler.toggleLookupViewPager(true, activity)
        setHasOptionsMenu(false)
    }

    override fun onResume() {
        super.onResume()
        searchViewHandler.toggleLookupViewPager(false, activity)
    }

    private fun initViews() {
        activity?.let {
            setupToolbar(
                title = getString(R.string.barcode_title),
                navigation = Navigation(
                    R.drawable.ic_arrow_back,
                    {
                        toggleSearchView(true)
                        parentFragmentManager.popBackStack()
                    },
                    getString(R.string.label_back)
                )
            )
        }

        val adapter = BarcodePagerAdapter(this, barcodeFragments)
        binding.viewpager.adapter = adapter
        TabLayoutMediator(binding.tabLayout, binding.viewpager) { tab, position ->
            tab.text = adapter.getPageTitle(position)
        }.attach()

        binding.viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                barcodeSharedViewModel.resetSwipeToDeleteActions()
            }
        })

        binding.viewpager.initTabsTracking(barcodeViewModel)
    }

    private fun toggleSearchView(shouldBeAttached: Boolean) {
        searchViewHandler.toggleSearchView(shouldBeAttached, activity)
    }
}
