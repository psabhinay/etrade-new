package com.etrade.mobilepro.barcode.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.barcode.R
import com.etrade.mobilepro.barcode.api.BarcodeItem
import com.etrade.mobilepro.barcode.databinding.AdapterBarcodeResultsBinding
import com.etrade.mobilepro.common.customview.SwipeRevealLayout
import org.threeten.bp.Instant
import org.threeten.bp.format.DateTimeFormatter

class BarcodeResultViewHolder(
    private val binding: AdapterBarcodeResultsBinding,
    private val clickListener: View.OnClickListener,
    private val deleteItemClickListener: View.OnClickListener,
    private val swipeActionListener: (BarcodeItem, Int, SwipeRevealLayout.Action) -> Unit,
    private val upcStringTemplate: String,
    private val dateFormat: DateTimeFormatter
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: BarcodeItem) {
        binding.apply {
            barcodeItemContainer.tag = item
            barcodeItemContainer.setOnClickListener(clickListener)

            val upcText = upcStringTemplate.format(item.upc)
            upc.text = upcText
            upc.contentDescription = upcText.replace("UPC", "U P C")
            company.text =
                binding.root.context.getString(R.string.title_subtitle_format, item.symbol, item.company)
            product.text = item.product
            date.text = dateFormat.format(Instant.ofEpochMilli(item.timeInMilliseconds))

            deleteBtn.tag = item
            deleteBtn.setOnClickListener(deleteItemClickListener)

            swipeReveal.actionListener =
                { swipeActionListener(item, bindingAdapterPosition, it) }

            if (item.isDeleteButtonVisible) {
                swipeReveal.open(animation = false)
            } else {
                swipeReveal.close(animation = false)
            }
        }
    }
}
