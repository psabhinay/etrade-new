package com.etrade.mobilepro.screener.presentation.mutualfund.portfolio

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.mutualfund.PortfolioFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.FragmentMutualFundPortfolioBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class MutualFundPortfolioFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<PortfolioFilter, MutualFundPortfolioViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {

    private val binding by viewBinding(FragmentMutualFundPortfolioBinding::bind)
    override val layoutRes: Int = R.layout.fragment_mutual_fund_portfolio
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND
    override val viewModel by viewModels<MutualFundPortfolioViewModel> { viewModelFactory }
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<PortfolioFilter, MutualFundPortfolioViewModel.ViewState, Boolean>>
        get() = mapOf(
            binding.lessThanOneHundredM to TotalNetAsset.LESS_THAN_100M,
            binding.oneHundredToFiveHundreds to TotalNetAsset.ONE_HUNDRED_M_TO_500M,
            binding.fiveHundredsMToOneB to TotalNetAsset.FIVE_HUNDRED_M_TO_1B,
            binding.oneBToTwoB to TotalNetAsset.ONE_B_TO_2B,
            binding.greaterThanTwoB to TotalNetAsset.GREATER_THAN_2B,
            binding.lessThanThreePercent to DistributionYield.LESS_THAN_3,
            binding.threeToFivePercent to DistributionYield.THREE_TO_5,
            binding.greaterThanFivePercent to DistributionYield.GREATER_THAN_5
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                it.filter.apply {
                    binding.apply {
                        lessThanOneHundredM.isChecked = netAssetsLessThan100
                        oneHundredToFiveHundreds.isChecked = netAssets100to500
                        fiveHundredsMToOneB.isChecked = netAssets500to1
                        oneBToTwoB.isChecked = netAssets1to2
                        greaterThanTwoB.isChecked = netAssetsGreaterThan2
                        lessThanThreePercent.isChecked = distributionYieldLessThan3
                        threeToFivePercent.isChecked = distributionYield3to5
                        greaterThanFivePercent.isChecked = distributionYieldGreaterThan5
                    }
                }
            }
        )
    }
}
