package com.etrade.mobilepro.util.android.binding

import android.app.Activity
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.viewbinding.ViewBinding
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

inline fun <T : ViewBinding> Activity.viewBinding(crossinline factory: (LayoutInflater) -> T) =
    lazy(LazyThreadSafetyMode.NONE) {
        factory(layoutInflater)
    }

/**
 * A property delegate that initialize the ViewBinding and observes Fragment's viewLifecycle.
 * On the view lifecycle ON_DESTROY_EVENT, the backing property is cleared. Note that the lifecycle
 * event is called before onDestroyEvent, so any binding cleanup should occur in the [cleanup] lambda.
 */
fun <T : ViewBinding> Fragment.viewBinding(
    factory: (View) -> T,
    cleanup: T.() -> Unit = {}
): ReadOnlyProperty<Fragment, T> =
    object : ReadOnlyProperty<Fragment, T>, LifecycleObserver {

        // A backing property to hold our value
        private var binding: T? = null

        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            // Clear out backing property just before onDestroyView
            binding?.cleanup()
            binding = null
            viewLifecycleOwner.lifecycle.removeObserver(this)
        }

        override fun getValue(thisRef: Fragment, property: KProperty<*>): T =
            // Return the backing property if it's set, otherwise initialize
            binding ?: factory(requireView()).also {
                if (viewLifecycleOwner.lifecycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)) {
                    binding = it
                    viewLifecycleOwner.lifecycle.addObserver(this)
                }
            }
    }

inline val ViewBinding.resources: Resources
    get() = root.resources
