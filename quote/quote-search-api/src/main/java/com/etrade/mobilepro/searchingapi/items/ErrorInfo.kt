package com.etrade.mobilepro.searchingapi.items

data class ErrorInfo(
    var errorCode: Int? = null,
    var errorType: Int? = null,
    var errorDescription: String? = null
)
