package com.etrade.mobilepro.trade.data.stock

import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.action
import com.etrade.mobilepro.trade.api.quantity
import com.etrade.mobilepro.trade.api.stock.StockOrder
import com.etrade.mobilepro.trade.api.stock.StockOrderPreviewResponse
import com.etrade.mobilepro.trade.api.symbols
import com.etrade.mobilepro.trade.api.ticker
import com.etrade.mobilepro.trade.data.DAY_TRADING_FLAG
import com.etrade.mobilepro.trade.data.DAY_TRADING_VER
import com.etrade.mobilepro.trade.data.TradeOrderArguments
import com.etrade.mobilepro.trade.data.form.input.DefaultStockTradeFormInputId
import com.etrade.mobilepro.trade.data.util.mockApi
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
internal class NewStockOrderTradeDelegateTest {

    private val inputId: StockTradeFormInputId = DefaultStockTradeFormInputId

    private val fakeApiClient: StockTradingApiClient = mock {
        runBlocking {
            whenever(it.makeStockOrder(any(), any())).thenReturn(mock())
        }
    }

    private val mockUser: User = mock {
        whenever(it.getUserId()).thenReturn(12345)
    }

    @Test
    fun `test default stock new preview market order builder`() {
        val sut = createDelegate()
        val inputMap = mapOf(
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.allOrNone to "0",
            inputId.stopPrice to "",
            inputId.limitPrice to "",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to "day"
        )

        runBlockingTest {
            sut.previewOrder(inputMap, MarketDestination.AUTO)
        }

        val expectedMap = mapOf(
            StockOrderConstants.PLATFORM to "android",
            StockOrderConstants.USER_ID to "12345",
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.allOrNone to "0",
            inputId.quantity to "1",
            StockOrderConstants.LOT_SUPPORT to "false",
            inputId.stopPrice to "",
            inputId.limitPrice to "",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to "day",
            StockOrderConstants.MARKET_SESSION to "1",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )
        runBlockingTest {
            verify(fakeApiClient).makeStockOrder("PreviewNewStockOrder", expectedMap)
        }
    }

    @Test
    fun `test stock new preview market order builder`() {
        val sut = createDelegate(true)
        val inputMap = mapOf(
            inputId.accountId to "12345678",
            inputId.symbol to "FB",
            inputId.action to inputId.actionValueId.sell,
            inputId.quantity to "2.22",
            inputId.allOrNone to "1",
            inputId.stopPrice to "",
            inputId.limitPrice to "",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to "day"
        )

        runBlockingTest {
            sut.previewOrder(inputMap, MarketDestination.NSDQ)
        }

        val expectedMap = mapOf(
            StockOrderConstants.PLATFORM to "tablet",
            StockOrderConstants.USER_ID to "12345",
            inputId.accountId to "12345678",
            inputId.symbol to "FB",
            inputId.action to inputId.actionValueId.sell,
            inputId.allOrNone to "1",
            inputId.quantity to "2.22",
            StockOrderConstants.LOT_SUPPORT to "false",
            inputId.stopPrice to "",
            inputId.limitPrice to "",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay,
            StockOrderConstants.MARKET_SESSION to "1",
            StockOrderConstants.DESTINATION_MARKET_CODE to "NSDQ",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )
        runBlockingTest {
            verify(fakeApiClient).makeStockOrder("PreviewNewStockOrder", expectedMap)
        }
    }

    @Test
    fun `test stock new place market order and overnight order`() {
        // place new stock order test
        val sut = createDelegate()
        val inputMap = mapOf(
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.allOrNone to "0",
            inputId.stopPrice to "",
            inputId.limitPrice to "",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay
        )

        val orders = listOf(
            mock<StockOrder> {
                whenever(it.yourPrice).thenReturn("Market")
            }
        )

        runBlockingTest {
            sut.placeOrder(
                TradeOrderArguments(inputMap, MarketDestination.ARCA),
                TradeOrder(
                    "", "",
                    mock<StockOrderPreviewResponse> {
                        whenever(it.orders).thenReturn(orders)
                    }
                )
            )
        }

        val expectedMap = mutableMapOf(
            StockOrderConstants.PLATFORM to "android",
            StockOrderConstants.USER_ID to "12345",
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.allOrNone to "0",
            inputId.quantity to "1",
            StockOrderConstants.LOT_SUPPORT to "false",
            inputId.stopPrice to "",
            inputId.limitPrice to "",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay,
            StockOrderConstants.PRICE to "Market",
            StockOrderConstants.MARKET_SESSION to "1",
            StockOrderConstants.DESTINATION_MARKET_CODE to "ARCA"
        )

        runBlockingTest {
            verify(fakeApiClient).makeStockOrder("PlaceNewStockOrder", expectedMap)
        }

        // Overnight indicator order test
        runBlockingTest {
            sut.placeOrder(
                TradeOrderArguments(inputMap, MarketDestination.ARCA),
                TradeOrder(
                    "", "",
                    mock<StockOrderPreviewResponse> {
                        whenever(it.orders).thenReturn(orders)
                        whenever(it.isOvernightTrading).thenReturn(true)
                    }
                )
            )
        }
        expectedMap[StockOrderConstants.OVERNIGHT_INDICATOR_FLAG] = "1"
        expectedMap.remove(StockOrderConstants.DESTINATION_MARKET_CODE)

        runBlockingTest {
            verify(fakeApiClient).makeStockOrder("PlaceNewStockOrder", expectedMap)
        }
    }

    @Test
    fun `test market session for extended hour terms`() {
        val sut = createDelegate()
        val inputMap = mutableMapOf(
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.allOrNone to "0",
            inputId.stopPrice to "",
            inputId.limitPrice to "",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.extendedHoursDay
        )

        val expectedMap = mutableMapOf(
            StockOrderConstants.PLATFORM to "android",
            StockOrderConstants.USER_ID to "12345",
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.allOrNone to "0",
            inputId.quantity to "1",
            StockOrderConstants.LOT_SUPPORT to "false",
            inputId.stopPrice to "",
            inputId.limitPrice to "",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodForDay,
            StockOrderConstants.MARKET_SESSION to "2",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        runBlockingTest {
            sut.previewOrder(inputMap, MarketDestination.AUTO)
        }

        runBlockingTest {
            verify(fakeApiClient).makeStockOrder("PreviewNewStockOrder", expectedMap)
        }

        // Test immediate or cancel
        inputMap[inputId.term] = inputId.termValueId.extendedHoursImmediateOrCancel
        expectedMap[inputId.term] = inputId.termValueId.immediateOrCancel

        runBlockingTest {
            sut.previewOrder(inputMap, MarketDestination.AUTO)
        }

        runBlockingTest {
            verify(fakeApiClient).makeStockOrder("PreviewNewStockOrder", expectedMap)
        }
    }

    @Test
    fun `test stop and limit order`() {
        val sut = createDelegate()
        val inputMap = mutableMapOf(
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.allOrNone to "0",
            inputId.stopPriceForStopLimit to "150",
            inputId.limitPriceForStopLimit to "150",
            inputId.priceType to inputId.priceTypeValueId.stopLimit,
            inputId.term to inputId.termValueId.goodUntilCancel
        )

        val expectedMap = mutableMapOf(
            StockOrderConstants.PLATFORM to "android",
            StockOrderConstants.USER_ID to "12345",
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.allOrNone to "0",
            inputId.quantity to "1",
            StockOrderConstants.LOT_SUPPORT to "false",
            inputId.stopPrice to "150",
            inputId.limitPrice to "150",
            inputId.priceType to inputId.priceTypeValueId.stopLimit,
            inputId.term to inputId.termValueId.goodUntilCancel,
            StockOrderConstants.MARKET_SESSION to "1",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        runBlockingTest {
            sut.previewOrder(inputMap, MarketDestination.AUTO)
        }

        runBlockingTest {
            verify(fakeApiClient).makeStockOrder("PreviewNewStockOrder", expectedMap)
        }
    }

    @Test
    fun `test trailing stop dollar and percent order`() {
        val sut = createDelegate()
        val inputMap = mutableMapOf(
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.allOrNone to "0",
            inputId.stopPrice to "150",
            inputId.priceType to inputId.priceTypeValueId.trailingStopDollar,
            inputId.term to inputId.termValueId.goodUntilCancel
        )

        val expectedMap = mutableMapOf(
            StockOrderConstants.PLATFORM to "android",
            StockOrderConstants.USER_ID to "12345",
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.allOrNone to "0",
            inputId.quantity to "1",
            StockOrderConstants.LOT_SUPPORT to "false",
            StockOrderConstants.OFFSET_VALUE to "150",
            StockOrderConstants.OFFSET_TYPE to inputId.priceTypeValueId.trailingStopDollar,
            inputId.priceType to inputId.priceTypeValueId.trailingStopDollar,
            inputId.term to inputId.termValueId.goodUntilCancel,
            StockOrderConstants.MARKET_SESSION to "1",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        runBlockingTest {
            sut.previewOrder(inputMap, MarketDestination.AUTO)
        }

        runBlockingTest {
            verify(fakeApiClient).makeStockOrder("PreviewNewStockOrder", expectedMap)
        }

        val priceTypeValueId = inputId.priceTypeValueId
        inputMap[inputId.priceType] = priceTypeValueId.trailingStopPercent
        inputMap.remove(inputId.stopPrice)
        inputMap[inputId.stopPrice] = "150"
        expectedMap[StockOrderConstants.OFFSET_TYPE] = priceTypeValueId.trailingStopPercent
        expectedMap[inputId.priceType] = priceTypeValueId.trailingStopPercent

        runBlockingTest {
            sut.previewOrder(inputMap, MarketDestination.AUTO)
        }

        runBlockingTest {
            verify(fakeApiClient).makeStockOrder("PreviewNewStockOrder", expectedMap)
        }
    }

    @Test
    fun `test tax lot support`() {
        val sut = createDelegate()
        val inputMap = mutableMapOf(
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "5",
            inputId.allOrNone to "0",
            inputId.stopPrice to "150",
            inputId.priceType to inputId.priceTypeValueId.trailingStopDollar,
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.positionLotId to "1122334455"
        )

        val expectedMap = mutableMapOf(
            StockOrderConstants.PLATFORM to "android",
            StockOrderConstants.USER_ID to "12345",
            inputId.accountId to "12345678",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.allOrNone to "0",
            inputId.quantity to "5",
            inputId.positionLotId to "1122334455",
            StockOrderConstants.LOT_SUPPORT to "true",
            StockOrderConstants.LOT_QUANTITY to "5",
            StockOrderConstants.OFFSET_VALUE to "150",
            StockOrderConstants.OFFSET_TYPE to inputId.priceTypeValueId.trailingStopDollar,
            inputId.priceType to inputId.priceTypeValueId.trailingStopDollar,
            inputId.term to inputId.termValueId.goodUntilCancel,
            StockOrderConstants.MARKET_SESSION to "1",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        runBlockingTest {
            sut.previewOrder(inputMap, MarketDestination.AUTO)
        }

        runBlockingTest {
            verify(fakeApiClient).makeStockOrder("PreviewNewStockOrder", expectedMap)
        }
    }

    private fun createDelegate(isTablet: Boolean = false): NewStockOrderTradeDelegate {
        return NewStockOrderTradeDelegate(fakeApiClient, inputId, isTablet, mock(), mockUser)
    }

    @Test
    fun `compare input and output for the preview stock order call using a mock client`() {
        val sut = NewStockOrderTradeDelegate(mockApi(), inputId, false, mock(), mockUser)
        val inputMap = mutableMapOf(
            inputId.accountId to "12341276",
            inputId.symbol to "AAPL",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "5",
            inputId.allOrNone to "0",
            inputId.stopPrice to "150",
            inputId.priceType to inputId.priceTypeValueId.trailingStopDollar,
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.positionLotId to "1122334455"
        )

        runBlocking {
            val result = sut.previewOrder(inputMap, MarketDestination.AUTO)
            result.getOrNull()?.orders?.forEach {
                assertEquals(it.account, "XXXX-1276")
                assertEquals(it.ticker, "AAPL")
                assertEquals(it.symbols.toString(), "[AAPL]")
                assertEquals(it.action, TransactionType.BUY_OPEN)
                assertEquals(it.quantity.toString(), "5")
                it.legs.forEach { leg ->
                    assertEquals(leg.symbol, "AAPL")
                    assertEquals(leg.action, TransactionType.BUY_OPEN)
                    assertEquals(leg.quantity.toString(), "5")
                }
            } ?: Assert.fail("invalid response")
        }
    }
}
