package com.etrade.mobilepro.instrument.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.dao.realm.BaseRealmDao
import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.dao.InstrumentDao
import com.etrade.mobilepro.instrument.dao.InstrumentRealmObject
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.OptionData
import com.etrade.mobilepro.streaming.api.StockData
import io.realm.Realm
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Realm implementation of [InstrumentDao].
 *
 * @param realm instance of Realm
 */
class RealmInstrumentDao(realm: Realm) : BaseRealmDao<InstrumentRealmObject>(realm, InstrumentRealmObject::class.java), InstrumentDao {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun getInstrumentsAsLiveData(symbols: Collection<String>): LiveData<List<Instrument>> =
        getAllIn("symbol", symbols.toTypedArray(), true).map { it }

    override fun getInstruments(symbols: Collection<String>): List<InstrumentRealmObject> = getAllInAsRealmResults("symbol", symbols.toTypedArray()).let {
        it.realm.copyFromRealm(it)
    }

    override fun getInstrumentsNonAsync(symbols: Collection<String>): List<Instrument> {
        return Realm.getInstance(realm.configuration).use {
            it.copyFromRealm(it.where(classObj).`in`("symbol", symbols.toTypedArray()).findAll())
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun updateInstruments(instruments: List<Instrument>) {
        saveAllAsync(instruments as List<InstrumentRealmObject>)
            .onFailure {
                logger.error("Failed to save instruments", it)
            }
    }
}

@Suppress("LongMethod")
fun InstrumentRealmObject.updateWithLevel1Data(data: Level1Data): InstrumentRealmObject {
    var daysChange: String = this.daysChangeVal
    var daysChangePercent: String = this.daysChangePer
    var marketCap: String = this.marketCapRaw
    var delta: String = this.deltaRaw
    var gamma: String = this.gammaRaw
    var impliedVolatilityPercent: String = this.impliedVolatilityPct
    var theta: String = this.thetaRaw
    var vega: String = this.vegaRaw

    if (data is StockData) {
        daysChange = data.dayChange ?: this.daysChangeVal
        daysChangePercent = data.dayChangePercent ?: this.daysChangePer
        marketCap = data.marketCap ?: this.marketCapRaw
    }

    if (data is OptionData) {
        delta = data.delta ?: this.deltaRaw
        gamma = data.gamma ?: this.gammaRaw
        impliedVolatilityPercent = data.impliedVolatility ?: this.impliedVolatilityPct
        theta = data.theta ?: this.thetaRaw
        vega = data.vega ?: this.vegaRaw
    }

    return copy(
        askRaw = data.ask ?: this.askRaw,
        bidRaw = data.bid ?: this.bidRaw,
        daysChangeVal = daysChange,
        daysChangePer = daysChangePercent,
        eps = data.earningsPerShare ?: this.eps,
        lastPriceRaw = data.lastPrice ?: this.lastPriceRaw,
        tickIndicatorRaw = data.tickIndicator?.toString() ?: "",
        pe = data.priceToEarnings ?: this.pe,
        volumeRaw = data.volume ?: this.volumeRaw,
        week52High = data.yearHigh ?: this.week52High,
        week52Low = data.yearLow ?: this.week52Low,
        marketCapRaw = marketCap,
        deltaRaw = delta,
        gammaRaw = gamma,
        impliedVolatilityPct = impliedVolatilityPercent,
        thetaRaw = theta,
        vegaRaw = vega,
        previousClose = data.previousClosePrice.orEmpty()
    )
}

fun InstrumentDao.getInstrumentsAssociated(symbols: Collection<String>): LiveData<Map<String, Instrument>> {
    return getInstrumentsAsLiveData(symbols).map { instruments -> instruments.associateBy { it.symbol } }
}
