package com.etrade.mobilepro.trade.form.api.input.shared

interface WithTrailingStopInputId {
    val offsetValue: String
}
