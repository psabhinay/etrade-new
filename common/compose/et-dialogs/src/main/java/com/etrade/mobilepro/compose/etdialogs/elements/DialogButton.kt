package com.etrade.mobilepro.compose.etdialogs.elements

import androidx.compose.ui.text.AnnotatedString

data class DialogButton<T>(
    val title: AnnotatedString,
    val isEnabled: (T) -> Boolean = { true },
    val onClick: () -> Unit = { }
)
