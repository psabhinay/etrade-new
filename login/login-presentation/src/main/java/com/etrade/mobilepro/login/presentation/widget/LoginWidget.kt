package com.etrade.mobilepro.login.presentation.widget

import android.app.Dialog
import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.View
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import androidx.annotation.CallSuper
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticationDelegate
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticator
import com.etrade.mobilepro.biometric.presentation.buildPasswordChangedDialog
import com.etrade.mobilepro.biometric.presentation.buildRsaConfirmDialog
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.showDialog
import com.etrade.mobilepro.livedata.combine
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.login.api.repo.LoginResultCode
import com.etrade.mobilepro.login.presentation.AuthenticationFailedException
import com.etrade.mobilepro.login.presentation.BiometricAuthenticationHelperHolder
import com.etrade.mobilepro.login.presentation.BiometricCipherAuthenticator
import com.etrade.mobilepro.login.presentation.LoginParameter
import com.etrade.mobilepro.login.presentation.LoginViewModel
import com.etrade.mobilepro.login.presentation.R
import com.etrade.mobilepro.login.presentation.SecurityIdProvider
import com.etrade.mobilepro.login.presentation.cta.LoginCtaAction
import com.etrade.mobilepro.login.presentation.cta.LoginCtaAction.CALL_ETRADE_SUPPORT
import com.etrade.mobilepro.login.presentation.cta.LoginCtaAction.CHECK_BOX_REMEMBER_USERNAME
import com.etrade.mobilepro.login.presentation.cta.LoginCtaAction.CHECK_BOX_USE_CODE
import com.etrade.mobilepro.login.presentation.cta.LoginCtaAction.FORGOT_PASSWORD
import com.etrade.mobilepro.login.presentation.cta.LoginCtaAction.FORGOT_USERNAME
import com.etrade.mobilepro.login.presentation.cta.LoginCtaAction.PRIVACY_POLICY
import com.etrade.mobilepro.login.presentation.cta.LoginCtaAction.SIGN_UP
import com.etrade.mobilepro.login.presentation.cta.LoginCtaAction.VIEW_EULA
import com.etrade.mobilepro.login.presentation.cta.LoginCtaItem
import com.etrade.mobilepro.login.presentation.databinding.ViewLoginWidgetBinding
import com.etrade.mobilepro.login.widget.LoginErrorCtaAction
import com.etrade.mobilepro.login.widget.LoginErrorCtaAction.CALL_ETRADE
import com.etrade.mobilepro.login.widget.LoginErrorCtaAction.DISMISS_ERROR_DIALOG
import com.etrade.mobilepro.login.widget.LoginErrorCtaAction.DISMISS_WARNING_DIALOG
import com.etrade.mobilepro.login.widget.LoginErrorCtaAction.RECOVER_USERNAME
import com.etrade.mobilepro.login.widget.LoginErrorCtaAction.RESET_PASSWORD
import com.etrade.mobilepro.login.widget.LoginResultDialog
import com.etrade.mobilepro.login.widget.LoginResultDialogFragment
import com.etrade.mobilepro.login.widget.buildPasswordUpdateDialog
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.autofillManager
import com.etrade.mobilepro.util.android.extension.clearFocusSafely
import com.etrade.mobilepro.util.android.extension.hideSoftKeyboard
import com.etrade.mobilepro.util.android.extension.inflater
import com.etrade.mobilepro.util.android.goneUnless
import com.etrade.mobilepro.util.android.textutil.TextUtil
import com.etrade.mobilepro.util.android.textutil.updateTextPaintForUrlSpans
import com.google.android.material.textfield.TextInputLayout
import com.google.android.material.textfield.TextInputLayout.END_ICON_PASSWORD_TOGGLE
import kotlinx.coroutines.launch
import javax.crypto.Cipher

private const val DISABLED_ALPHA: Float = 0.5f
private const val ENABLED_ALPHA: Float = 1f

@Suppress("LargeClass", "TooManyFunctions")
abstract class LoginWidget : ConstraintLayout, LifecycleObserver {

    val binding = ViewLoginWidgetBinding.inflate(inflater, this, true)

    init {
        isFocusableInTouchMode = true
        isFocusable = true

        setupInputChangeListeners()

        // https://stackoverflow.com/questions/57211658/android-talkback-incorrect-announcement-on-textinputlayout
        binding.loginPasswordLayout.setTextInputAccessibilityDelegate(null)
        autofillManager?.requestAutofill(this)
    }

    var loginViewModel: LoginViewModel? = null

    open var shouldFinishActivity: Boolean = false

    private val securityIdProvider: SecurityIdProvider = {
        rsaConfirmDialogBuilder.show(activity.supportFragmentManager)
    }

    private var biometricDelegate: BiometricAuthenticationDelegate? = null

    private lateinit var activity: AppCompatActivity

    private var lifecycleOwner: LifecycleOwner? = null

    private var shouldHandleLanding: Boolean = false

    private val isSecurityCodeEnabled: Boolean
        get() = binding.loginUseCodeCheckbox.isChecked

    private val onLoginSuccess: (warning: String?) -> Unit = { warning ->
        showLoginResultDialog(warning)
    }

    private val onLoginFailure: (errorMessage: String?, errorCode: LoginResultCode) -> Unit = { errorMsg, loginResultCode ->
        if (loginResultCode == LoginResultCode.INVALID_STORED_PASSWORD) {
            passwordUpdateDialogBuilder.show(activity.supportFragmentManager)
        } else {
            showLoginResultDialog(errorMsg ?: context.getString(R.string.error_message_general), loginResultCode)
        }
    }

    private val cipherAuthenticator: BiometricCipherAuthenticator = { cipher, promptType, isUserInitiated ->
        requireNotNull(biometricDelegate).apply {
            setupCallbacks(cipher)
            authenticate(cipher, promptType, isUserInitiated)
        }
    }

    private val passwordListener: (String) -> Unit = { login(it) }

    private val showListener: () -> Unit = {
        loginViewModel?.shouldShowBiometricAuthentication = false
    }

    private val dismissListener: () -> Unit = {
        loginViewModel?.shouldShowBiometricAuthentication = true
    }

    private val rsaConfirmPositiveListener: (String) -> Unit = ::continueWith

    private val passwordChangedDialogBuilder = buildPasswordChangedDialog(passwordListener, dismissListener, showListener)

    private val passwordChangedDialogListener: () -> Unit = {
        rsaConfirmDialogBuilder.dismiss(activity.supportFragmentManager)
        loginViewModel?.apply {
            shouldShowBiometricAuthentication = false
            cancelSecurityId()
        }

        passwordChangedDialogBuilder.show(activity.supportFragmentManager)
    }

    private val noLongerSecurityIdUserListener: () -> Unit = {
        continueWith(securityId = "")
        rsaConfirmDialogBuilder.dismiss(activity.supportFragmentManager)
    }

    private val rsaCancelListener: () -> Unit = {
        loginViewModel?.cancelSecurityId()
    }

    private val rsaConfirmDialogBuilder = buildRsaConfirmDialog(
        positiveListener = rsaConfirmPositiveListener,
        negativeListener = rsaCancelListener,
        actionButton1Listener = passwordChangedDialogListener,
        actionButton2Listener = noLongerSecurityIdUserListener,
        dismissListener = dismissListener,
        showListener = showListener
    )

    private val passwordUpdateDialogBuilder = buildPasswordUpdateDialog(passwordListener)

    private var currentDialog: Dialog? = null

    constructor(ctx: Context) : this(ctx, null)
    constructor(ctx: Context, attributeSet: AttributeSet?) : this(ctx, attributeSet, 0)
    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle)

    abstract fun initLoginScreen(savedUserName: String?, isRememberUserChecked: Boolean)

    @CallSuper
    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        hideSoftKeyboard()

        activity.let {
            if (it is BiometricAuthenticationHelperHolder) {
                it.biometricAuthenticationHelper.onBiometricAuthentication = null
            }
        }
        lifecycleOwner?.let { lifecycleOwner ->
            lifecycleOwner.lifecycle.removeObserver(this)
            loginViewModel?.run {
                dialog.removeObservers(lifecycleOwner)
                loginViewState.removeObservers(lifecycleOwner)
                emptyUserName.removeObservers(lifecycleOwner)
                emptyPassword.removeObservers(lifecycleOwner)
                emptySecurityCode.removeObservers(lifecycleOwner)
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        renderFingerprintIcon()
        // Reattach the biometric delegate to the current activity
        biometricDelegate?.init(this.activity)
    }

    private fun setupInputChangeListeners() {
        binding.loginUserNameLayout.clearErrorOnTextChange()
        binding.loginPasswordLayout.clearErrorOnTextChange()
        binding.loginSecurityCodeLayout.clearErrorOnTextChange()
    }

    @Suppress("LongMethod")
    fun setupView(activity: AppCompatActivity, biometricDelegate: BiometricAuthenticationDelegate, shouldHandleLanding: Boolean) {
        this.activity = activity
        this.shouldHandleLanding = shouldHandleLanding
        passwordChangedDialogBuilder.restore(activity.supportFragmentManager)
        rsaConfirmDialogBuilder.restore(activity.supportFragmentManager)
        passwordUpdateDialogBuilder.restore(activity.supportFragmentManager)
        setupBiometricAuthenticationDelegate(biometricDelegate)
        loginViewModel?.run {
            setupFieldsEnabler(biometricDelegate.isBiometricPromptShowing, loginViewState)
            viewModelScope.launch {
                val userNameResult = loginViewProvider.getUserName()
                initLoginScreen(userNameResult.getOrNull(), loginViewProvider.isRememberUserNameEnabled())
                val isUseCodeEnable = loginViewProvider.isUseSecurityCodeEnabled()
                binding.loginUseCodeCheckbox.isChecked = isUseCodeEnable
                updateSecurityCodeSection(isUseCodeEnable)
            }

            setupLoginBtnListener()
            setupClickListeners(this)
        }
        passwordUpdateDialogBuilder.restore(activity.supportFragmentManager)

        // set here instead of layout file for complete standard behavior
        binding.loginPasswordLayout.endIconMode = END_ICON_PASSWORD_TOGGLE

        if (activity is BiometricAuthenticationHelperHolder) {
            activity.biometricAuthenticationHelper.onBiometricAuthentication = {
                login(isUserInitiated = false)
            }
        }
        binding.loginEulaNote.text = TextUtil
            .convertTextWithHmlTagsToSpannable(resources.getString(R.string.eula_acceptance_note))
            .updateTextPaintForUrlSpans(drawUnderlined = false)
    }

    private fun updateSecurityCodeSection(isUseCodeEnable: Boolean) {
        binding.loginSecurityCodeLayout.goneUnless(isUseCodeEnable)
        if (isUseCodeEnable) {
            binding.loginPassword.imeOptions = EditorInfo.IME_ACTION_NEXT
            binding.loginSecurityCode.imeOptions = EditorInfo.IME_ACTION_DONE
        } else {
            hideSoftKeyboard()
            binding.loginSecurityCode.imeOptions = EditorInfo.IME_ACTION_NEXT
            binding.loginPassword.imeOptions = EditorInfo.IME_ACTION_DONE
        }
    }

    private fun setupFieldsEnabler(
        biometricPromptLiveData: LiveData<Boolean>,
        loginViewState: LiveData<ConsumableLiveEvent<LoginViewModel.ViewState>>
    ) = lifecycleOwner?.let {
        biometricPromptLiveData.combine(loginViewState) { biometricPrompt, loginState ->
            if (loginState != null) {
                loginState.peekContent() is LoginViewModel.ViewState.Error
            } else {
                biometricPrompt == false
            }
        }
            .distinctUntilChanged()
            .observeBy(it, ::changeInputFieldsState)
    }

    private fun setupBiometricAuthenticationDelegate(biometricHelper: BiometricAuthenticationDelegate) {
        this.biometricDelegate = biometricHelper
        biometricHelper.init(this.activity)
    }

    fun authenticateUsingBiometrics() {
        if (loginViewModel?.isBiometricLoginEnabled == true) {
            login()
        }
    }

    private fun setupClickListeners(viewModel: LoginViewModel) {
        binding.loginRememberUserCheckbox.setOnCheckedChangeListener { _, isChecked ->
            viewModel.onCtaAction(CHECK_BOX_REMEMBER_USERNAME, isChecked)
        }
        binding.loginUseCodeCheckbox.setOnCheckedChangeListener { _, isChecked ->
            updateSecurityCodeSection(isSecurityCodeEnabled)
            viewModel.onCtaAction(CHECK_BOX_USE_CODE, isChecked)
        }
        binding.biometricSignIn.setOnClickListener {
            authenticateUsingBiometrics()
        }

        binding.signUpButton.setOnClickListener {
            viewModel.onCtaAction(SIGN_UP)
        }

        binding.loginEulaNote.setOnClickListener {
            viewModel.onCtaAction(VIEW_EULA)
        }

        binding.forgotUserId.setOnClickListener {
            viewModel.onCtaAction(FORGOT_USERNAME)
        }
        binding.forgotPassword.setOnClickListener {
            viewModel.onCtaAction(FORGOT_PASSWORD)
        }
        binding.privacyPolicy.setOnClickListener {
            viewModel.onCtaAction(PRIVACY_POLICY)
        }
    }

    private fun setupLoginBtnListener() {
        binding.loginButton.setOnClickListener { performLoginAction() }
        binding.loginPassword.setOnEditorActionListener { _, actionId, event: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_NEXT || event?.action == KeyEvent.ACTION_DOWN
            ) {
                binding.loginSecurityCode.requestFocus()
            } else if (actionId == EditorInfo.IME_ACTION_DONE) {
                performLoginAction()
            }
            return@setOnEditorActionListener false
        }
        binding.loginSecurityCode.setOnEditorActionListener { _, actionId, event: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_DONE || event?.action == KeyEvent.ACTION_DOWN
            ) {
                performLoginAction()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    private fun performLoginAction() {
        val userNameText = binding.loginUserName.text.toString()
        val passwordText = binding.loginPassword.text.toString()
        val securityCode = binding.loginSecurityCode.text.toString()

        loginViewModel?.login(
            username = userNameText,
            password = passwordText,
            securityCode = securityCode,
            isSecurityCodeEnabled = isSecurityCodeEnabled,
            loginParameter = LoginParameter(cipherAuthenticator, shouldHandleLanding = shouldHandleLanding)
        )
    }

    fun attachTo(lifecycleOwner: LifecycleOwner) {
        lifecycleOwner.lifecycle.addObserver(this)
        loginViewModel?.initViewState()
        observeLiveData(lifecycleOwner)
    }

    @Suppress("LongMethod")
    private fun observeLiveData(lifecycleOwner: LifecycleOwner) {
        this.lifecycleOwner = lifecycleOwner
        loginViewModel?.let { loginViewModel ->
            loginViewModel.dialog.observe(
                lifecycleOwner,
                {
                    onDialog(it?.peekContent())
                }
            )
            loginViewModel.loginViewState.run {
                bindLoginServiceChanges(this)
            }

            loginViewModel.emptyUserName.observe(
                lifecycleOwner,
                {
                    if (it == false) {
                        binding.loginUserNameLayout.showError(R.string.login_missing_username)
                    }
                }
            )

            loginViewModel.emptyPassword.observe(
                lifecycleOwner,
                {
                    if (it == false) {
                        binding.loginPasswordLayout.showError(R.string.login_missing_password)
                    }
                }
            )

            loginViewModel.emptySecurityCode.observeBy(lifecycleOwner) { isEmpty ->
                if (isEmpty) {
                    binding.loginSecurityCodeLayout.showError(R.string.login_missing_code)
                }
            }
        }
    }

    private fun hideProgress() {
        binding.loginProgressBar.visibility = View.GONE
        binding.loginButton.isEnabled = true
        binding.loginButton.text = context.getString(R.string.button_text_logon)
    }

    private fun showProgress() {
        activity.currentFocus?.run {
            hideSoftKeyboard()
            clearFocusSafely()
        }

        binding.loginProgressBar.visibility = View.VISIBLE
        binding.loginButton.isEnabled = false
        binding.loginButton.text = ""
    }

    private fun changeInputFieldsState(isEnabled: Boolean) {
        val alpha = if (isEnabled) {
            ENABLED_ALPHA
        } else {
            DISABLED_ALPHA
        }
        binding.loginUserName.isEnabled = isEnabled
        binding.loginUserName.alpha = alpha
        binding.loginPassword.isEnabled = isEnabled
        binding.loginPassword.alpha = alpha
        binding.loginSecurityCode.isEnabled = isEnabled
        binding.loginSecurityCode.alpha = alpha
        binding.loginUseCodeCheckbox.isEnabled = isEnabled
        binding.loginUseCodeCheckbox.alpha = alpha
        binding.loginRememberUserCheckbox.isEnabled = isEnabled
        binding.loginRememberUserCheckbox.alpha = alpha
    }

    @Suppress("LongMethod")
    private fun bindLoginServiceChanges(
        viewState: LiveData<ConsumableLiveEvent<LoginViewModel.ViewState>>
    ) {
        lifecycleOwner?.let {
            viewState.observe(
                it,
                { event ->
                    when (val content = event.getNonConsumedContent(false)) {
                        is LoginViewModel.ViewState.Loading -> {
                            showProgress()
                        }
                        is LoginViewModel.ViewState.Error -> {
                            clearCredentialsFields()
                            hideProgress()
                            renderFingerprintIcon()
                            onLoginFailure(
                                content.message ?: context.resources.getString(R.string.login_error_message_general),
                                content.loginErrorCode
                            )
                            event.consume()
                        }
                        is LoginViewModel.ViewState.Success -> {
                            hideProgress()
                            autofillManager?.commit()
                            onLoginSuccess(content.message)
                            event.consume()
                        }
                        else -> {
                            hideProgress()
                            event.consume()
                        }
                    }
                }
            )
        }
    }

    private fun clearCredentialsFields() {
        binding.loginPassword.text = null
        binding.loginSecurityCode.text = null
        loginViewModel?.clearPasswordSelection()
    }

    private fun showLoginResultDialog(errorMsg: String?, errorCode: LoginResultCode = LoginResultCode.WARNING) {
        errorMsg?.let {
            LoginResultDialogFragment.show(activity.supportFragmentManager, it, errorCode)?.onAttached()
        } ?: finishActivity() // finish Login Activity if no dialog to be presented
    }

    private fun finishActivity() {
        if (shouldFinishActivity) {
            activity.finish()
        }
    }

    fun LoginResultDialog.onAttached() {
        this.onButtonClickListener = { ctaAction ->
            bindToErrorAction(ctaAction) {
                LoginResultDialogFragment.dismiss(activity.supportFragmentManager)
            }
        }
    }

    private fun bindToErrorAction(it: LoginErrorCtaAction, defaultAction: (() -> Unit)) {
        when (it) {
            CALL_ETRADE -> loginViewModel?.onCtaAction(CALL_ETRADE_SUPPORT)
            RECOVER_USERNAME -> loginViewModel?.onCtaAction(FORGOT_USERNAME)
            RESET_PASSWORD -> loginViewModel?.onCtaAction(FORGOT_PASSWORD)
            DISMISS_ERROR_DIALOG -> defaultAction.invoke()
            DISMISS_WARNING_DIALOG -> {
                defaultAction.invoke()
                finishActivity()
            }
        }
    }

    private fun continueWith(securityId: String) {
        loginViewModel?.resumeLoginWith(securityId)
    }

    private fun login(userSecret: String? = null, isUserInitiated: Boolean = BiometricAuthenticator.USER_INITIATED_BY_DEFAULT) {
        loginViewModel?.login(
            password = userSecret,
            loginParameter = LoginParameter(
                authenticator = cipherAuthenticator,
                isUserInitiated = isUserInitiated,
                shouldHandleLanding = shouldHandleLanding
            ),
            securityIdProvider = securityIdProvider
        )
    }

    private fun onDialog(dialog: AppDialog?) {
        currentDialog?.dismiss()
        currentDialog = dialog?.let { context.showDialog(it) }
    }

    private fun BiometricAuthenticator.setupCallbacks(cipher: Cipher) {
        fun clearCallbacks() {
            onAuthenticationCancel = null
            onAuthenticationFail = null
            onAuthenticationSuccess = null
        }

        onAuthenticationCancel = {
            clearCallbacks()
            loginViewModel?.onCipherAuthenticationCancelled()
        }
        onAuthenticationFail = {
            clearCallbacks()
            loginViewModel?.onCipherAuthenticationFailed(AuthenticationFailedException())
        }
        onAuthenticationSuccess = {
            clearCallbacks()
            loginViewModel?.onCipherAuthenticated(cipher)
        }
    }

    private fun renderFingerprintIcon() {
        val isBiometricLoginEnabled = loginViewModel?.isBiometricLoginEnabled ?: false
        if (isBiometricLoginEnabled) {
            binding.biometricSignIn.visibility = VISIBLE
        } else {
            binding.biometricSignIn.visibility = GONE
        }
    }
}

private fun shakeAnimation(view: View) {
    val shakeAnimation = AnimationUtils.loadAnimation(view.context, R.anim.shake)
    view.startAnimation(shakeAnimation)
}

private fun LoginViewModel.onCtaAction(action: LoginCtaAction, isChecked: Boolean? = null) = onCtaAction(LoginCtaItem(action, isChecked))

private fun TextInputLayout.clearErrorOnTextChange() {
    val editText = editText ?: return
    editText.doOnTextChanged { _, _, before, count ->
        if (editText.isFocused && before == 0 && count > 0) {
            error = null
        }
    }
}

private fun TextInputLayout.showError(@StringRes textRes: Int) {
    error = context.getString(textRes)
    shakeAnimation(this)
}
