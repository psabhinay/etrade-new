package com.etrade.mobilepro.portfolio.extendedsummary

import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer.BottomSheetNavContainer
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import javax.inject.Inject

@RequireLogin
internal class ExtendedPortfolioSummaryBottomSheetFragment @Inject constructor(
    connectionNotifier: ConnectionNotifier,
    userSessionHandler: UserSessionHandler
) : BottomSheetNavContainer(connectionNotifier, userSessionHandler) {

    override fun createNavHost(): NavHostFragment {
        return NavHostFragment.create(
            R.navigation.portfolio_summary_graph,
            arguments
        )
    }

    override fun createDestinationChangeListener(): NavController.OnDestinationChangedListener {
        return NavController.OnDestinationChangedListener { _, destination, _ ->
            titleText = if (destination.label == null) {
                context?.resources?.getString(R.string.portfolio_details_more_info) ?: ""
            } else {
                destination.label.toString()
            }

            subTitleText = ""
        }
    }

    override fun onTitleChange(newTitle: String) {
        titleText = newTitle
    }
}
