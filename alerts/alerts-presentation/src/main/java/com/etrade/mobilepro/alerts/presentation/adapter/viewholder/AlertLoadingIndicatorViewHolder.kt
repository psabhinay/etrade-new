package com.etrade.mobilepro.alerts.presentation.adapter.viewholder

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.alerts.presentation.databinding.ItemNetworkStateLoadingBinding
import com.etrade.mobilepro.util.android.extension.viewBinding

class AlertLoadingIndicatorViewHolder(binding: ItemNetworkStateLoadingBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bindTo() {
        // nop
    }

    companion object {
        fun from(parent: ViewGroup) = AlertLoadingIndicatorViewHolder(parent.viewBinding(ItemNetworkStateLoadingBinding::inflate))
    }
}
