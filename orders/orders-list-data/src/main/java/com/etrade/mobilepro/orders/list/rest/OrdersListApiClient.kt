package com.etrade.mobilepro.orders.list.rest

import com.etrade.eo.rest.retrofit.Scalar
import com.etrade.mobilepro.orders.list.dto.OrderListRequestDto
import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

const val GET_ORDERS_URL = "/app/vieworders/getorders.json"
const val GET_SAVED_ORDERS_URL = "/app/preparedorder/getsavedorders.json"
const val GET_ESP_ORDERS_URL = "/e/t/mobile/espvieworders"

interface OrdersListApiClient {

    @Scalar
    @POST(GET_ORDERS_URL)
    fun getOrders(@Body @JsonMoshi requestBody: OrderListRequestDto): Single<String>

    @Scalar
    @POST(GET_SAVED_ORDERS_URL)
    fun getSavedOrders(@Body @JsonMoshi requestBody: OrderListRequestDto): Single<String>

    @Scalar
    @POST(GET_ESP_ORDERS_URL)
    fun getEmployeeStockPlanOrders(
        @Body request: RequestBody
    ): Single<String>
}
