package com.etrade.mobilepro.trade.api

interface OrderPlaceResponse : OrderResponse {
    override val orders: List<ConfirmedOrder>
}
