package com.etrade.mobilepro.chartiq.presentation

import android.annotation.SuppressLint
import android.app.Activity
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.runtime.Composable
import androidx.compose.ui.viewinterop.AndroidViewBinding
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.chartiq.data.JAVASCRIPT_INTERFACE_NAME
import com.etrade.mobilepro.chartiq.presentation.databinding.ChartiqFragmentChartiqBinding
import com.etrade.mobilepro.chartiq.presentation.util.userAgentDeviceType
import com.etrade.mobilepro.dialog.showDialog
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quote.screen.api.TabVisibilityListener
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.searchingapi.EXTRA_SELECTED_SYMBOL
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.goneIf
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.BlockingLoadingIndicatorViewDelegate
import com.google.android.material.snackbar.Snackbar
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import org.slf4j.LoggerFactory

class ChartIqDelegate @AssistedInject constructor(
    @Assisted private val fragment: Fragment,
    private val applicationInfo: ApplicationInfo,
    private val searchIntentFactory: SearchIntentFactory,
    private val router: ChartIqRouter,
    assistedModelFactory: ChartIqViewModelFactoryFactory,
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    @Assisted private val symbol: SearchResultItem.Symbol,
    @Assisted private val mobileQuoteStateFlow: StateFlow<MobileQuote>?
) : TabVisibilityListener {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    private val webViewReference: WebView
        get() = fragment.view?.findViewById(R.id.webView) ?: binding.webView

    private val viewModel: ChartIqViewModel by fragment.viewModels {
        assistedModelFactory.create(
            symbol,
            mobileQuoteStateFlow
        )
    }
    private val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { fragment.viewLifecycleOwner },
            { fragment.view }
        )
    }
    private var snackBar: Snackbar? = null
    private val quoteWidgetViewModel by fragment.viewModels<QuotesWidgetViewModel> { viewModelFactory }

    private lateinit var binding: ChartiqFragmentChartiqBinding

    private val blockingLoadingIndicatorDelegate by lazy {
        BlockingLoadingIndicatorViewDelegate(
            viewModel,
            fragment.childFragmentManager
        )
    }

    private val symbolLookupRequest =
        fragment.registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                (result.data?.extras?.get(EXTRA_SELECTED_SYMBOL) as? SearchResultItem.Symbol)?.let {
                    viewModel.addSymbolToCompare(it)
                }
            }
        }

    private val chartIqStateObserver = Observer<ChartIqState> { handleChartIqStateUpdate(it) }

    @AssistedFactory
    interface Factory {
        fun create(
            fragment: Fragment,
            symbol: SearchResultItem.Symbol,
            mobileQuoteStateFlow: StateFlow<MobileQuote>?
        ): ChartIqDelegate
    }

    fun onViewCreated() {
        blockingLoadingIndicatorDelegate.observe(fragment.viewLifecycleOwner)
        fragment.lifecycle.addObserver(viewModel)
    }

    @SuppressWarnings("LongMethod")
    private fun Fragment.bindChartIqViewModel() {
        viewModel.dialog.observe(
            viewLifecycleOwner,
            { dialog ->
                dialog?.let { showDialog(it) }
            }
        )
        viewModel.showSymbolLookup.observe(
            viewLifecycleOwner,
            { event ->
                event.consume { params -> lookupSymbol(params) }
            }
        )
        viewModel.chartSettingsState.observe(
            viewLifecycleOwner,
            { state ->
                handleSettingsState(state)
            }
        )
        viewModel.errorMessage.observe(
            viewLifecycleOwner,
            { event ->
                event.consume { showGenericError(it) }
            }
        )
        viewModel.showLogin.observe(
            viewLifecycleOwner,
            { event ->
                event.consume { router.showLogin(activity, symbol) }
            }
        )
        viewModel.editOrder.observe(
            viewLifecycleOwner,
            { event ->
                event.consume { router.showEditOrderScreen(activity, it) }
            }
        )
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.chartIqCommands.collect { event ->
                event.consume { command ->
                    if (command.isNotBlank()) {
                        webViewReference.evaluateJavascript(command, null)
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.chartIqStreamingUpdateCommands.collect { event ->
                event.consume { command ->
                    if (command.isNotBlank()) {
                        webViewReference.evaluateJavascript(command, null)
                    }
                }
            }
        }
    }

    @Composable
    fun ChartIqCompose() {
        AndroidViewBinding(ChartiqFragmentChartiqBinding::inflate) {
            if (!::binding.isInitialized || binding != this) {
                binding = this
                binding.lifecycleOwner = fragment.viewLifecycleOwner
                obj = quoteWidgetViewModel
                fragment.bindQuoteWidgetViewModel()
                fragment.bindChartIqViewModel()
                placeRetainedWebViewIfNecessary()
            }
        }
    }

    private fun setUpWebViewClient() {
        webViewReference.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?) =
                false

            override fun onPageFinished(view: WebView?, url: String?) {
                view?.clearHistory()
            }
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Suppress("DEPRECATION")
    private fun setUpWebView() {
        setUpWebViewClient()
        webViewReference.settings.apply {
            javaScriptEnabled = true
            builtInZoomControls = true
            domStorageEnabled = true
            databaseEnabled = true
            userAgentString = applicationInfo.userAgentDeviceType
            allowUniversalAccessFromFileURLs = true
            allowContentAccess = false
            allowFileAccess = false
        }
        webViewReference.addJavascriptInterface(viewModel.chartIqJavascriptBridge, JAVASCRIPT_INTERFACE_NAME)
        viewModel.onJavascriptInterfaceAdded()
    }

    private fun showGenericError(message: ErrorMessage) {
        val retryAction = message.retryAction ?: return
        snackBar =
            snackbarUtil.retrySnackbar(message = message.message, actionOnRetry = retryAction)
                ?.apply { show() }
    }

    private fun lookupSymbol(params: StartSearchParams) {
        fragment.activity?.let {
            val intent = searchIntentFactory.createIntent(it, params, true)
            symbolLookupRequest.launch(intent)
        }
    }

    private fun handleChartIqStateUpdate(state: ChartIqState) {
        when (state) {
            is ChartIqState.None -> setUpWebView()
            is ChartIqState.Initializing -> webViewReference.loadUrl("file:///android_asset/chartiq.html")
            is ChartIqState.Reload -> webViewReference.reload()
            is ChartIqState.Ready -> logger.debug("ChartIQ is ready")
            is ChartIqState.Error -> handleError()
        }
    }

    private fun handleSettingsState(state: ChartIqViewModel.ChartSettingsState) {
        val settingsOpen = state == ChartIqViewModel.ChartSettingsState.OPEN
        binding.quoteWidgetLayout?.root?.goneIf(settingsOpen)
    }

    private fun Fragment.bindQuoteWidgetViewModel() {
        val progressBar: ContentLoadingProgressBar? =
            binding.root.findViewById(R.id.quote_widget_loading_indicator)
        quoteWidgetViewModel.loadingIndicator.observe(
            viewLifecycleOwner,
            {
                snackBar?.dismiss()
                if (it) {
                    progressBar?.show()
                } else {
                    progressBar?.hide()
                }
            }
        )
        quoteWidgetViewModel.errorMessage.observe(
            viewLifecycleOwner,
            {
                progressBar?.hide()
                handleError()
            }
        )
    }

    private fun handleError() {
        if (snackBar?.isShown != true) {
            snackBar = snackbarUtil.retrySnackbar {
                val chartIqState = viewModel.chartIqState.value
                if (chartIqState is ChartIqState.Error) {
                    chartIqState.retryAction()
                }
                quoteWidgetViewModel.errorMessage.value?.consume {
                    quoteWidgetViewModel.getQuotes(symbol.title)
                }
            }
            snackBar?.show()
        }
    }

    fun retainWebView() {
        if (::binding.isInitialized && viewModel.webViewInstance == null) {
            val webView = binding.webView

            viewModel.webViewInstance = webView
            binding.chartIqRootLayout.removeView(webView)
        }
    }

    fun placeRetainedWebViewIfNecessary() {
        if (::binding.isInitialized) {
            val webViewContainer = binding.chartIqRootLayout
            val retainedWebView = viewModel.webViewInstance ?: return

            if (retainedWebView === binding.webView && binding.webView.parent == null) {
                webViewContainer.addView(retainedWebView)
                return
            }

            if (retainedWebView.parent === webViewContainer) {
                return
            }

            if (retainedWebView.parent != null) {
                (retainedWebView.parent as? ViewGroup)?.removeView(retainedWebView)
            }

            webViewContainer.run {
                binding.webView.let {
                    it.destroy()
                    removeView(it)
                }
                addView(retainedWebView)
            }
        }
    }

    fun onBackPressed(onBack: () -> Unit) {
        if (viewModel.isSettingsOpen()) {
            viewModel.closeSettings()
        } else {
            onBack()
        }
    }

    override fun onVisibilityChange(isVisible: Boolean) {
        if (isVisible) {
            placeRetainedWebViewIfNecessary()
            viewModel.chartIqState.observe(
                fragment.viewLifecycleOwner,
                chartIqStateObserver
            )
        } else {
            snackBar?.dismiss()
        }
    }
}
