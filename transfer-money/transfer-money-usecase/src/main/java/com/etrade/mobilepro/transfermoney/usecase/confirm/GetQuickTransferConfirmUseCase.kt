package com.etrade.mobilepro.transfermoney.usecase.confirm

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.transfermoney.api.repo.TransferMoneyRepo
import com.etrade.mobilepro.util.UseCase
import javax.inject.Inject

interface QuickTransferConfirmUseCase : UseCase<GetQuickTransferConfirmRequest, ETResult<GetQuickTransferConfirmResult>>

class QuickTransferConfirmUseCaseImpl @Inject constructor(
    private val repo: TransferMoneyRepo
) : QuickTransferConfirmUseCase {

    override suspend fun execute(parameter: GetQuickTransferConfirmRequest): ETResult<GetQuickTransferConfirmResult> = runCatchingET {
        repo
            .getConfirm(parameter.toRepositoryConfirmRequest())
            .getOrThrow()
            .let {
                if (it.errorDescription != null) {
                    GetQuickTransferConfirmResult.Failed(it.errorDescription)
                } else {
                    GetQuickTransferConfirmResult.Success(
                        it.confirmationNumber?.trim(),
                        it.headerMessage?.trim(),
                        it.message?.trim(),
                        it.footerMessage?.trim()
                    )
                }
            }
    }
}
