package com.etrade.mobilepro.chat.presentation.message.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.presentation.databinding.ChatItemAgentIsTypingBinding
import com.etrade.mobilepro.chat.presentation.message.viewHolder.ChatAgentTypingViewHolder
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class ChatAgentTypingAdapterDelegate : AbsListItemAdapterDelegate<ChatMessage.AgentTyping, ChatMessage, ChatAgentTypingViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup) =
        ChatAgentTypingViewHolder(parent.viewBinding(ChatItemAgentIsTypingBinding::inflate))

    override fun isForViewType(item: ChatMessage, items: MutableList<ChatMessage>, position: Int) = item is ChatMessage.AgentTyping

    override fun onBindViewHolder(item: ChatMessage.AgentTyping, holder: ChatAgentTypingViewHolder, payloads: MutableList<Any>) {
        /* do nothing */
    }
}
