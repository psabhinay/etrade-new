package com.etrade.mobilepro.trade.data.form.input

import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.data.form.DefaultSymbolInputValueCoder
import com.etrade.mobilepro.trade.data.form.value.DefaultConditionValueId
import com.etrade.mobilepro.trade.data.form.value.DefaultQualifierValueId
import com.etrade.mobilepro.trade.data.form.value.DefaultSecurityTypeValueId
import com.etrade.mobilepro.trade.form.api.input.ContingentOrderInputId
import com.etrade.mobilepro.trade.form.api.value.ConditionValueId
import com.etrade.mobilepro.trade.form.api.value.QualifierValueId
import com.etrade.mobilepro.trade.form.api.value.SecurityTypeValueId
import com.etrade.mobilepro.util.ValueCoder

object DefaultContingentOrderInputId : ContingentOrderInputId {
    override val symbol: String = "sym1"
    override val symbolCoder: ValueCoder<WithSymbolInfo> = DefaultSymbolInputValueCoder
    override val accountId: String = ""
    override val securityType: String = "aocondsecuritytype0"
    override val securityTypeValueId: SecurityTypeValueId = DefaultSecurityTypeValueId
    override val contingentSymbol: String = "aocondsymbol0"
    override val qualifier: String = "aofollowprice0"
    override val qualifierValueId: QualifierValueId = DefaultQualifierValueId
    override val condition: String = "offsettype0"
    override val conditionValueId: ConditionValueId = DefaultConditionValueId
    override val conditionValue: String = "offsetvalue0"
}
