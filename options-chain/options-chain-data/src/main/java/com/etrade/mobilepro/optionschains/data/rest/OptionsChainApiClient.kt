package com.etrade.mobilepro.optionschains.data.rest

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.optionschains.data.dto.OptionsChainDto
import com.etrade.mobilepro.optionschains.data.dto.OptionsChainRequestDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Options Chain API client.
 */
interface OptionsChainApiClient {

    /**
     * Post method for getting full options chains with quotes response
     */
    @JsonMoshi
    @POST("app/product/fetchFullOptionChainsWithQuotes/{symbol}.json")
    suspend fun getFullOptionsChainsWithQuotes(
        @Body @JsonMoshi requestBody: OptionsChainRequestDto,
        @Path("symbol") symbol: String
    ): ServerResponseDto<OptionsChainDto>
}
