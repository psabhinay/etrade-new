package com.etrade.mobilepro.appwidget.indices

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.appwidget.api.IndicesViewState
import com.etrade.mobilepro.appwidget.api.WidgetViewStateRepo
import com.etrade.mobilepro.appwidget.api.settings.WidgetId
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettingsRepo
import com.etrade.mobilepro.appwidget.common.copyWith
import com.etrade.mobilepro.appwidget.common.getEntries
import com.etrade.mobilepro.appwidget.common.toTuples
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.getOrDefault
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.marketsapi.Index
import com.etrade.mobilepro.marketsapi.MoverRepo
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdateListener
import com.etrade.mobilepro.user.session.manager.SessionState
import com.etrade.mobilepro.user.session.manager.SessionStateChangeListener
import com.etrade.mobilepro.watchlist.usecase.GetWatchlistEntriesUseCase
import com.etrade.mobilepro.watchlist.usecase.ResolveTimestampUseCase
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.WatchListDeleted
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.WatchlistEvent
import com.etrade.mobilepro.watchlistapi.WatchlistEventListener
import com.etrade.mobilepro.watchlistapi.WatchlistEventNotifier
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.async
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.rx2.await
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.Collections
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val MAX_INDICES_TO_DISPLAY = 3
private const val QUOTE_EMISSION_DELAY_IN_MILLIS = 100L
private const val QUOTE_TIME_OUT_IN_SECONDS = 3L

private val logger: Logger = LoggerFactory.getLogger(IndicesViewModel::class.java)

@Suppress("LargeClass")
class IndicesViewModel @Inject constructor(
    private val moverRepo: MoverRepo,
    private val viewStateRepo: WidgetViewStateRepo,
    private val widgetSettingsRepo: WidgetSettingsRepo,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController,
    private val sessionState: SessionState,
    private val getWatchlistEntries: GetWatchlistEntriesUseCase,
    private val resolveTimestamp: ResolveTimestampUseCase,
    private val watchlistEventNotifier: WatchlistEventNotifier,
    private val isAuthenticated: UserAuthenticationState,
    private val watchlistRepo: WatchlistRepo
) : StreamingStatusUpdateListener, SessionStateChangeListener, WatchlistEventListener {
    val viewState: LiveData<IndicesViewState> by lazy { _viewState }
    val emissionsCompletedSignal: LiveData<Unit> by lazy { _emissionsCompletedSignal }
    var isStreaming: Boolean = false
    val isWidgetPlaced: Boolean get() = runBlocking { viewStateRepo.isWidgetPlaced() }

    private val _viewState = MutableLiveData(runBlocking { viewStateRepo.getIndicesViewState() })
    private val _emissionsCompletedSignal = MutableLiveData<Unit>()
    private val recentQuoteData: MutableMap<String, Level1Data> = Collections.synchronizedMap(mutableMapOf())

    private val isLoading: Boolean
        get() {
            return _viewState.value?.isLoading == true
        }
    private val isNotStreaming: Boolean get() = !isStreaming
    private val isStreamingEnabled: Boolean get() = streamingStatusController.isStreamingToggleEnabled

    private val mainScope = MainScope()
    private var disposableStreaming: Disposable? = null

    init {
        streamingStatusController.subscribeForStreamingStatusUpdates(this)
        sessionState.addListener(this)
        watchlistEventNotifier.addListener(this)
    }

    fun dispose() {
        streamingStatusController.unsubscribeFromStreamingStatusUpdates(this)
        sessionState.removeListener(this)
        watchlistEventNotifier.removeListener(this)
        mainScope.cancel()
        unsubscribeFromIndicesUpdates()
        notifyEmissionsCompleted()
    }

    fun onDataRequested() {
        if (!isLoading) {
            if (isStreaming) {
                unsubscribeFromIndicesUpdates()
            }
            loadData()
        }
    }

    fun addSymbolToWatchlist(symbol: SearchResultItem.Symbol) {
        mainScope.launch {
            runCatching {
                if (isStreaming) {
                    unsubscribeFromIndicesUpdates()
                }
                updateState { it.copy(isLoading = true) }
                watchlistRepo.addToWatchList(
                    watchlistId = widgetSettingsRepo.loadSettings(getWidgetId()).watchListId,
                    ticker = symbol.title
                ).await()
            }.onSuccess {
                loadData()
            }.onFailure { e ->
                logger.error("Could not add symbol ${symbol.title} to selected watchlist", e)
                if (isStreamingEnabled) {
                    requestStreamingIfEnabled()
                } else {
                    updateState { it.copy(isLoading = false) }
                }
            }
        }
    }

    private suspend fun updateState(block: suspend (IndicesViewState) -> IndicesViewState) {
        val newState = block(viewStateRepo.getIndicesViewState())
        viewStateRepo.setIndicesViewState(newState)
        _viewState.value = newState
    }

    private fun notifyEmissionsCompleted() {
        _emissionsCompletedSignal.value = Unit
    }

    private fun loadData() {
        mainScope.launch {
            updateState {
                it.copy(isLoading = true, isError = false)
            }
            val deferredIndices = async { fetchIndices() }
            val deferredEntries = async { resolveWatchlistEntries() }
            val indicesResult = deferredIndices.await()
            val entriesResult = deferredEntries.await()

            if (indicesResult.isSuccess && entriesResult.isSuccess) {
                val entries = entriesResult.getOrThrow()
                val timestamp = resolveTimestamp.execute(entries).getOrNull()
                    ?: indicesResult.getOrNull()?.firstOrNull()?.timeStamp.orEmpty()
                updateState {
                    it.copy(
                        isLoading = isStreamingEnabled,
                        indices = indicesResult.getOrThrow(),
                        watchlistEntryTuples = entries.toTuples(),
                        formattedTimestamp = timestamp
                    )
                }
            } else {
                indicesResult.exceptionOrNull()?.let { logger.error("Could not fetch market indices", it) }
                entriesResult.exceptionOrNull()?.let { logger.error("Could not get watchlist entries", it) }
                updateState { it.copy(isError = true, isLoading = false) }
            }

            requestStreamingIfEnabled()
        }
    }

    private suspend fun requestStreamingIfEnabled() {
        if (isStreamingEnabled) {
            if (isNotStreaming) {
                onStreamingRequested()
            }
        } else {
            notifyEmissionsCompleted()
        }
    }

    private suspend fun resolveWatchlistEntries(): ETResult<List<WatchlistEntry>> {
        return runCatchingET {
            val watchlistId = widgetSettingsRepo.loadSettings(getWidgetId()).watchListId
                .takeIf { id ->
                    !isAuthenticated() || watchlistRepo.getUserWatchlists().await().any { it.id == id }
                }
                ?: DEVICE_WATCHLIST_ID
            val watchlistEntries = when {
                isAuthenticated() || watchlistId == DEVICE_WATCHLIST_ID -> {
                    getWatchlistEntries.execute(watchlistId).getOrThrow()
                }
                else -> viewStateRepo.getLatestResolvedWatchlistEntries().getOrThrow()
            }

            watchlistId to watchlistEntries
        }.map { (id, entries) ->
            updateState { it.copy(watchlistId = id) }
            val widgetId = getWidgetId()
            val settings = widgetSettingsRepo.loadSettings(widgetId)
            widgetSettingsRepo.saveSettings(widgetId, settings.copy(watchListId = id))
            viewStateRepo.setLatestResolvedWatchlistEntries(entries)
            entries
        }
    }

    private suspend fun fetchIndices(): ETResult<List<Index>> {
        return moverRepo.getMarketIndices()
            .map { it.take(MAX_INDICES_TO_DISPLAY) }
    }

    private suspend fun getIndicesWithFallback(): List<Index> {
        return _viewState.value?.indices
            ?: viewStateRepo.getIndicesViewState().indices
            ?: fetchIndices()
                .onFailure { logger.error("Could not fetch market indices", it) }
                .getOrDefault(emptyList())
    }

    private suspend fun onStreamingRequested() {
        isStreaming = true
        val indices = getIndicesWithFallback()
            .also {
                updateState { viewState -> viewState.copy(indices = it) }
            }
        val entries = resolveWatchlistEntries()
            .onSuccess { watchlistEntries ->
                updateState { viewState -> viewState.copy(watchlistEntryTuples = watchlistEntries.toTuples()) }
            }
            .onFailure { logger.error("Could not load watchlist entries", it) }
            .getOrDefault(emptyList())

        if (entries.isEmpty()) {
            val timestamp = indices.firstOrNull()?.timeStamp.orEmpty()
            updateState { it.copy(formattedTimestamp = timestamp) }
        }
        if (indices.isNotEmpty()) {
            subscribeToQuoteUpdates(getStreamingTicketDataFlowable(indices, entries))
        } else {
            updateState { it.copy(isLoading = false, isError = true) }
            notifyEmissionsCompleted()
        }
    }

    private fun subscribeToQuoteUpdates(streamingFlowable: Flowable<TickerData>) {
        disposableStreaming = streamingFlowable
            .subscribeBy(
                onNext = { tickerData ->
                    Thread.sleep(QUOTE_EMISSION_DELAY_IN_MILLIS)
                    when (tickerData.origin) {
                        TickerOrigin.Index -> updateIndexAndNotify(tickerData)
                        TickerOrigin.WatchlistEntry -> updateEntryAndNotify(tickerData)
                    }
                },
                onComplete = {
                    runBlocking { updateState { it.copy(isLoading = false) } }
                    notifyEmissionsCompleted()
                },
                onError = { exception ->
                    logger.error("Market indices streaming error", exception)
                    runBlocking { updateState { it.copy(isLoading = false, isError = true) } }
                    notifyEmissionsCompleted()
                }
            )
    }

    private fun getStreamingTicketDataFlowable(indices: List<Index>, entries: List<WatchlistEntry>): Flowable<TickerData> = Observable
        .merge(
            indices.toQuoteStreams(streamingQuoteProvider, recentQuoteData) +
                entries.toQuoteStreams(streamingQuoteProvider, recentQuoteData) +
                getQuoteTimeoutStream()
        )
        .toFlowable(BackpressureStrategy.DROP)
        .observeOn(Schedulers.io(), false, indices.size + entries.size)
        .filter { it is TickerData } // Readme.md#Note-4
        .map { it as TickerData }

    private fun getQuoteTimeoutStream(): Observable<QuoteTimeOut> = Observable
        .just(QuoteTimeOut)
        .delay(QUOTE_TIME_OUT_IN_SECONDS, TimeUnit.SECONDS)
        .doOnNext { mainScope.launch { updateState { it.copy(isLoading = false) } } }

    private fun unsubscribeFromIndicesUpdates() {
        isStreaming = false
        disposableStreaming?.dispose()
        disposableStreaming = null
    }

    private fun updateIndexAndNotify(tickerData: TickerData) = mainScope.launch {
        val viewState = viewStateRepo.getIndicesViewState()
        val indices = viewState.indices ?: return@launch
        val indexPosition = indices.indexOfFirst { it.ticker == tickerData.ticker }
        val updatedIndex = indices.getOrNull(indexPosition)?.copyWith(tickerData.data)

        if (updatedIndex != null) {
            indices.toMutableList()
                .apply { set(indexPosition, updatedIndex) }
                .let { updatedIndices ->
                    val timestamp = if (viewState.watchlistEntryTuples.isNullOrEmpty()) {
                        tickerData.data.getFormattedTimestamp()
                    } else {
                        viewState.formattedTimestamp
                    }
                    updateState {
                        it.copy(
                            indices = updatedIndices,
                            formattedTimestamp = timestamp,
                            isLoading = false,
                            isError = false
                        )
                    }
                }
        }
    }

    private fun updateEntryAndNotify(tickerData: TickerData) = mainScope.launch {
        val viewState = viewStateRepo.getIndicesViewState()
        val watchlistEntryTuples = viewState.watchlistEntryTuples ?: return@launch
        val watchlistEntries = watchlistEntryTuples.getEntries()
        val entryPosition = watchlistEntries.indexOfFirst { it.ticker == tickerData.ticker }
        val updatedEntry = watchlistEntries.getOrNull(entryPosition)
            ?.copyWith(tickerData.data)
            ?: return@launch

        watchlistEntryTuples
            .toMutableList()
            .apply { set(entryPosition, updatedEntry to tickerData.data) }
            .let { updatedWatchlistData ->
                val timestamp = tickerData.data.getFormattedTimestamp()
                    ?: resolveTimestamp.execute(updatedWatchlistData.getEntries()).getOrNull()
                updateState {
                    it.copy(
                        watchlistEntryTuples = updatedWatchlistData,
                        formattedTimestamp = timestamp,
                        isLoading = false,
                        isError = false
                    )
                }
            }
    }

    override fun invoke(status: StreamingStatusUpdate) {
        mainScope.launch {
            if (status is StreamingStatusUpdate.StreamingToggleUpdate) {
                if (status.enabled && isNotStreaming) {
                    onDataRequested()
                } else {
                    unsubscribeFromIndicesUpdates()
                    notifyEmissionsCompleted()
                }
            }
        }
    }

    override fun onSessionStateChange(state: SessionStateChange) {
        if (state is SessionStateChange.UNAUTHENTICATED && isStreaming) {
            notifyEmissionsCompleted()
        }
    }

    override fun onWatchlistEvent(event: WatchlistEvent) {
        if (event is WatchListDeleted && event.watchlistId != viewState.value?.watchlistId) {
            return
        }
        if (isStreaming) {
            unsubscribeFromIndicesUpdates()
        }
        onDataRequested()
    }

    private suspend fun getWidgetId(): WidgetId = viewStateRepo.getIndicesViewState().widgetId
}
