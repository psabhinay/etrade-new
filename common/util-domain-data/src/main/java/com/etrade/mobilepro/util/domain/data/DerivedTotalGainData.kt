package com.etrade.mobilepro.util.domain.data

import java.math.BigDecimal
import java.math.RoundingMode

/**
 * All total gain calculation function have been implemented based on the following doc:
 * https://confluence.corp.etradegrp.com/display/COREBT/PBS+Backlog+-+Details?preview=/149900777/165805765/Portfolios%20-%20Fields%20Mapping%20and%20Calculations.docx#PBSBacklog-Details-Details-Details-Positions-Calculations&Mappings
 */

// Total Gain = [(Current price – price    paid) * qty] – [commission + fees]
fun deriveTotalGain(
    currentPrice: BigDecimal,
    pricePaid: BigDecimal,
    quantity: BigDecimal,
    commission: BigDecimal,
    fees: BigDecimal
): BigDecimal {
    return currentPrice
        .subtract(pricePaid)
        .multiply(quantity)
        .subtract(commission)
        .subtract(fees)
}

// Total_gain_pct = 100 * (Total gain $ / Total cost)
fun deriveTotalGainPercentage(
    totalGain: BigDecimal,
    totalCost: BigDecimal
): BigDecimal? {
    return if (totalCost.compareTo(BigDecimal.ZERO) == 0) {
        null
    } else {
        totalGain
            .divide(totalCost, DIVIDE_SCALE, RoundingMode.HALF_UP)
            .times(BigDecimal(PERCENT_MULTIPLIER))
    }
}

// Total cost = price paid * abs(qty)  + commission + fees]
fun deriveCommonTotalCost(
    pricePaid: BigDecimal,
    quantity: BigDecimal,
    commission: BigDecimal,
    fees: BigDecimal
): BigDecimal = quantity
    .abs()
    .multiply(pricePaid)
    .add(commission)
    .add(fees)

// Total cost = (price paid *opt mult * abs(quantity)) + commission + fees]
fun deriveOptionTotalCost(
    pricePaid: BigDecimal,
    quantity: BigDecimal,
    commission: BigDecimal,
    fees: BigDecimal,
    optionMultiplier: BigDecimal? = BigDecimal.ONE
): BigDecimal = quantity
    .abs()
    .multiply(pricePaid)
    .multiply(optionMultiplier)
    .add(commission)
    .add(fees)

// Total Cost = ((Price Paid  * abs(quantity))/100) + commission + fees]
fun deriveBondTotalCost(
    pricePaid: BigDecimal,
    quantity: BigDecimal,
    commission: BigDecimal,
    fees: BigDecimal
): BigDecimal = quantity
    .abs()
    .multiply(pricePaid)
    .divide(BigDecimal(PERCENT_MULTIPLIER), DIVIDE_SCALE, RoundingMode.HALF_UP)
    .add(commission)
    .add(fees)

// Account Total Gain = 100 * (total gain $ / total price paid)
fun deriveAccountTotalGainPercentage(totalGain: BigDecimal, totalPricePaid: BigDecimal): BigDecimal? {
    return if (totalPricePaid.compareTo(BigDecimal.ZERO) == 0) {
        null
    } else {
        totalGain
            .divide(totalPricePaid, DIVIDE_SCALE, RoundingMode.HALF_UP)
            .multiply(BigDecimal(PERCENT_MULTIPLIER))
    }
}
