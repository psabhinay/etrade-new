package com.etrade.mobilepro.msaccounts.presentation.model

data class MsAccountFootnote(
    val symbol: String,
    val text: String,
    val description: String
)
