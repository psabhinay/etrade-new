package com.etrade.mobilepro.screener.presentation.stock.fundamentals

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.stock.FundamentalsFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Stock
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class FundamentalsViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @Stock screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<FundamentalsFilter, FundamentalsViewModel.ViewState>(
    aggregator,
    ScreenerType.Stock.Fundamentals,
    screenerInteractor
) {
    override fun provideInitialState(initialFilter: FundamentalsFilter): ViewState = ViewState(filter = initialFilter)

    data class ViewState(
        override val filter: FundamentalsFilter
    ) : BaseFilterScreenViewModel.ViewState<FundamentalsFilter>
}
