package com.etrade.mobilepro.trade.presentation.util

import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.backends.api.LEVERAGED_ETF_AGREEMENT_ERROR_CODE
import com.etrade.mobilepro.backends.api.MRB_FLAGGED_SYMBOL_ERROR_CODE
import com.etrade.mobilepro.backends.api.OTC_DISCLOSURE_ERROR_CODE
import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.backends.api.ServerMessageException
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.appDialog
import com.etrade.mobilepro.presentation.handleServerMessages

class TradeExceptionViewModelDelegate(
    private val resources: Resources,
    private val baseUrl: String,
    private val dialogLiveData: MutableLiveData<AppDialog?>,
    private val navigationMap: Map<String, () -> Unit>,
    private val onOptionApprovalUpgrade: (accountId: String) -> Unit = { /* intentionally blank */ }
) {

    private val errorCodesToIntercept: Map<String, Int> by lazy {
        mutableMapOf(
            "2027" to R.string.trade_error_options_approval_level_1,
            "3002" to R.string.trade_error_options_approval_level_2_or_3,
            "3008" to R.string.trade_error_options_approval_level_3,
            "3015" to R.string.trade_error_options_approval_level_3
        )
    }

    fun handleTradeOrderException(exception: Throwable, accountId: String? = null): Boolean {
        return when (exception) {
            is ServerMessageException -> {
                val approvalDialogs = mutableListOf<AppDialog>()
                val messages = accountId?.let {
                    exception.messages.filter { message ->
                        val errorMessage = errorCodesToIntercept[message.code]
                        errorMessage?.let { messageRes ->
                            approvalDialogs.add(createOptionsApprovalDialog(accountId, messageRes, approvalDialogs))
                        }
                        errorMessage == null
                    }
                } ?: exception.messages
                handleServerMessages(resources, baseUrl, messages.mapDisclosureErrors(), dialogLiveData, navigationMap) {
                    // the first dialog will show the next and so on if there are any
                    dialogLiveData.value = approvalDialogs.takeIf { it.isNotEmpty() }?.removeAt(0)
                }
                true
            }
            else -> {
                false
            }
        }
    }

    private fun Collection<ServerMessage>.mapDisclosureErrors(): Collection<ServerMessage> {
        return this.map {
            when (it.code) {
                OTC_DISCLOSURE_ERROR_CODE.toString() -> ServerMessage(
                    ServerMessage.Type.DISCLOSURE_ERROR,
                    resources.getString(R.string.trade_otc_agreement_warning_message),
                    it.priority,
                    it.code
                )
                LEVERAGED_ETF_AGREEMENT_ERROR_CODE.toString() -> ServerMessage(
                    ServerMessage.Type.DISCLOSURE_ERROR,
                    resources.getString(R.string.trade_leveraged_etf_warning_message),
                    it.priority,
                    it.code
                )
                MRB_FLAGGED_SYMBOL_ERROR_CODE.toString() -> ServerMessage(
                    ServerMessage.Type.DISCLOSURE_ERROR,
                    it.text,
                    it.priority,
                    it.code
                )
                else -> it
            }
        }
    }

    private fun createOptionsApprovalDialog(
        accountId: String,
        @StringRes message: Int,
        dialogs: MutableList<AppDialog>
    ): AppDialog {
        return resources.appDialog {
            val onDialogDismissed = {
                dialogLiveData.value = if (dialogs.isEmpty()) {
                    null
                } else {
                    dialogs.removeAt(0)
                }
            }
            messageRes = message
            resourcesPositiveAction {
                labelRes = R.string.trade_label_upgrade_account
                action = {
                    onDialogDismissed()
                    onOptionApprovalUpgrade(accountId)
                }
            }
            resourcesNegativeAction {
                labelRes = R.string.trade_label_not_now
                action = onDialogDismissed
            }
        }
    }
}
