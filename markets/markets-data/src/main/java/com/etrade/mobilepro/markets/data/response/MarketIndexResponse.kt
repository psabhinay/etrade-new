package com.etrade.mobilepro.markets.data.response

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false, name = "GetMarketIndicesResponse")
data class MarketIndexResponse constructor(
    @field:ElementList(name = "QuoteList")
    var quotes: ArrayList<Quote>? = null
)
