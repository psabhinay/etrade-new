package com.etrade.mobilepro.billpay.presentation.common

import android.os.Parcel
import com.etrade.mobilepro.billpay.api.MerchantInfo
import kotlinx.android.parcel.Parceler

object PayeeMerchantInfoParceler : Parceler<MerchantInfo> {

    override fun create(parcel: Parcel): MerchantInfo = parcel.run {
        MerchantInfo(
            id = readInt(),
            merchantZip = readString(),
            merchantZip4 = readString()
        )
    }

    override fun MerchantInfo.write(parcel: Parcel, flags: Int) = parcel.run {
        writeInt(id ?: 0)
        writeString(merchantZip)
        writeString(merchantZip4)
    }
}
