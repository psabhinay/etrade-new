package com.etrade.mobilepro.trade.presentation.form.delegate

import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.dynamic.form.api.inputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.data.form.input.DefaultStopLossStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormType
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class StopLossStockTradeFormFactoryDelegateTest {

    private val tradeFormType = TradeFormType(SecurityType.STOCK, AdvancedTradeType.StopLoss)
    private val sut: TradeFormFactoryDelegate? = mockDelegates.getFormDelegate(tradeFormType)

    private val form = sut?.createForm(
        mockTradeAccounts,
        mockSelectedAccountDelegate("31459185"),
        mockParameter,
        object : TradeFormFactoryDelegate.OnFormChangeListener {
            override fun onSecurityType(securityType: SecurityType) =
                Unit

            override fun onAdvancedOrderType(
                formType: TradeFormType,
                snapshot: InputValuesSnapshot
            ) = Unit
        }
    )

    @Test
    fun `check StopLossStockTradeFormFactoryDelegate is configured for StopLoss trade type`() {
        assertTrue(mockDelegates.getFormDelegate(tradeFormType) is StopLossStockTradeFormFactoryDelegate)
    }

    @Test
    fun `StopLossStockTradeFormFactoryDelegate creates form successfully`() {
        assertNotNull(form)

        // Top level sections : Account, Stock form, Advanced order
        assertTrue(form?.groups?.size == VERTICAL_GROUP_SECTIONS)
    }

    @Test
    // Buttons : Preview, Save
    fun `StopLossStockTrade Form check if button state is valid`() {
        assertValidActionButtons(form?.actions)
    }

    @Test
    fun `StopLossStockTrade Form mandatory view components`() {
        val formIds = form?.inputValuesSnapshot?.keys ?: emptySet()
        DefaultStopLossStockTradeFormInputId.run {
            assertTrue(formIds.contains(accountId))
            assertTrue(formIds.contains(action))
            assertTrue(formIds.contains(priceType))
            assertTrue(formIds.contains(quantity))
            assertTrue(formIds.contains(symbol))
            assertTrue(formIds.contains(term))
            assertTrue(formIds.contains(advancedOrder))
            assertTrue(formIds.contains(stopLossInputId.priceType))
            assertTrue(formIds.contains(stopLossInputId.term))
        }
    }

    @Test
    fun `check Advanced Order Stop Loss Stock form content for injected user input`() {
        val testStockInput = with(DefaultStopLossStockTradeFormInputId) {
            mapOf(
                securityType to securityTypeValueId.stock,
                accountId to "31459185",
                symbol to "FB:EQ",
                quantity to "1",
                priceType to priceTypeValueId.limit,
                limitPrice to "0.45",
                term to termValueId.goodForDay,
                allOrNone to "0",
                stopLossInputId.quantity to "1",
                stopLossInputId.term to stopLossInputId.termValueId.goodForDay,
                walkthroughFormCompleted to "completed"
            )
        }

        form?.resetValues()
        form?.injectValuesFrom(testStockInput)
        assertFormValues(testStockInput, form)
    }
}
