package com.etrade.mobilepro.chat.presentation.setup

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.chat.api.setup.models.ChatProductItemModel
import com.etrade.mobilepro.chat.api.setup.models.customerInfo.ChatCustomerAccountModel
import com.etrade.mobilepro.chat.presentation.ChatRouter
import com.etrade.mobilepro.chat.presentation.R
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_PARAMS_REQUEST_BODY
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_SESSION_DATA
import com.etrade.mobilepro.chat.presentation.databinding.ChatFragmentSetupBinding
import com.etrade.mobilepro.chat.presentation.databinding.ChatSetupInputFormBinding
import com.etrade.mobilepro.chat.presentation.service.ChatMessagesService
import com.etrade.mobilepro.chat.presentation.setup.ChatSetupViewModel.ViewState.Status.Error
import com.etrade.mobilepro.chat.presentation.setup.ChatSetupViewModel.ViewState.Status.GoToChatMessages
import com.etrade.mobilepro.chat.presentation.setup.ChatSetupViewModel.ViewState.Status.Idle
import com.etrade.mobilepro.chat.presentation.setup.ChatSetupViewModel.ViewState.Status.Loading
import com.etrade.mobilepro.chat.presentation.setup.ChatSetupViewModel.ViewState.Status.Unavailable
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.styledalertdialog.StyledAlertDialog
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.startDialerActivity
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.formatters.formatAccountName
import javax.inject.Inject

private val ACCOUNT_TAG = "${ChatSetupFragment::class.java.canonicalName}_account_selector"
private val SUBJECT_TAG = "${ChatSetupFragment::class.java.canonicalName}_subject_selector"
private val NAME_TAG = "${ChatSetupFragment::class.java.canonicalName}_name_selector"

private const val REQUEST_CODE_SHOW_CALL_US_DIALOG = 5

@RequireLogin
class ChatSetupFragment @Inject constructor(
    viewModelProvider: ViewModelProvider.Factory,
    private val chatRouter: ChatRouter,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : OverlayBaseFragment() {

    private val binding by viewBinding(ChatFragmentSetupBinding::bind)
    private val inputForm by viewBinding(ChatSetupInputFormBinding::bind)

    private val viewModel by viewModels<ChatSetupViewModel> { viewModelProvider }
    private lateinit var accountSelector: DropDownMultiSelector<ChatCustomerAccountModel>
    private lateinit var subjectSelector: DropDownMultiSelector<ChatProductItemModel>
    private lateinit var nameSelector: DropDownMultiSelector<String>

    private val snackBarUtil: SnackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    override val layoutRes: Int = R.layout.chat_fragment_setup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.chat_setup_options_menu, menu)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (ChatMessagesService.isRunning) {
            chatRouter.openChatMessages(findNavController(), requireArguments().getBundle(CHAT_SESSION_DATA))
            return
        }
        bindViewModel()

        setupSelectors()
        setupListeners()

        viewModel.refresh(forced = false)
    }

    private fun handleSelections(state: ChatSetupViewModel.ViewState) {
        state.selectedAccount?.let {
            inputForm.account.setText(formatAccountName(it.instDescription, it.accNum))
        }
        inputForm.subject.setText(state.selectedSubject?.lookupName)
        inputForm.name.setText(state.selectedName)
    }

    private fun handleEnabledState(state: ChatSetupViewModel.ViewState) {
        inputForm.subjectLayout.isEnabled = state.isSubjectEnabled()
        inputForm.accountLayout.isEnabled = state.isAccountEnabled()
        inputForm.nameLayout.isEnabled = state.isNameEnabled()
        binding.chat.isEnabled = state.isChatButtonEnabled()
    }

    private fun setupListeners() {
        inputForm.account.setOnClickListener {
            accountSelector.open()
        }
        inputForm.name.setOnClickListener {
            nameSelector.open()
        }
        inputForm.subject.setOnClickListener {
            subjectSelector.open()
        }
        binding.chat.setOnClickListener {
            viewModel.onChatButtonClick()
        }
        with(inputForm.chatAboutText) {
            doAfterTextChanged {
                val text = it?.toString()
                hint = if (text.isNullOrEmpty()) {
                    getString(R.string.chat_setup_input_topic_hint)
                } else {
                    null
                }
                viewModel.messageTextChanged(text)
            }
        }
    }

    private fun navigateToChatMessagesFragment() {
        val bundle = arguments?.getBundle(CHAT_SESSION_DATA) ?: Bundle()
        chatRouter.openChatMessages(
            findNavController(),
            bundle.apply { putSerializable(CHAT_PARAMS_REQUEST_BODY, viewModel.chatParamsRequest) }
        )
    }

    @Suppress("LongMethod") // no way to shorten
    private fun setupSelectors() {
        accountSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = ACCOUNT_TAG,
                multiSelectEnabled = false,
                title = resources.getString(R.string.title_account),
                itemsData = viewModel.accountsData,
                selectListener = { _, selected ->
                    viewModel.accountSelected(selected.value)
                }
            )
        )
        subjectSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = SUBJECT_TAG,
                multiSelectEnabled = false,
                title = resources.getString(R.string.chat_setup_subject),
                itemsData = viewModel.subjectsData,
                selectListener = { _, selected ->
                    viewModel.subjectSelected(selected.value)
                }
            )
        )
        nameSelector = DefaultDropDownMultiSelector(
            supportFragmentManager = parentFragmentManager,
            settings = DropDownMultiSelector.Settings(
                tag = NAME_TAG,
                multiSelectEnabled = false,
                title = resources.getString(R.string.chat_name),
                itemsData = viewModel.nameData,
                selectListener = { _, selected ->
                    viewModel.nameSelected(selected.value)
                }
            )
        )
    }

    private fun handleStatus(state: ChatSetupViewModel.ViewState) {
        state.status.popup?.let {
            displayChatUnavailableDialog(getChatUnavailableMessage(it))
        }
        when (state.status) {
            is Idle -> toggleViewState(isLoading = false)
            Loading -> toggleViewState(isLoading = true, isDisabled = false)
            is Error, is Unavailable -> {
                toggleViewState(isLoading = false, isDisabled = true)
            }
            is GoToChatMessages -> {
                toggleViewState(isLoading = false, isDisabled = true)
                navigateToChatMessagesFragment()
            }
        }
    }

    private fun getChatUnavailableMessage(popup: ChatSetupViewModel.ViewState.Status.Popup): String =
        if (popup == ChatSetupViewModel.ViewState.Status.Popup.HOLIDAY) {
            requireContext().getString(R.string.chat_holiday_message)
        } else {
            requireContext().getString(R.string.chat_unavailable_message)
        }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                handleStatus(it)
                handleSelections(it)
                handleEnabledState(it)
            }
        )
    }

    private fun displayChatUnavailableDialog(message: String) {
        context?.let {
            StyledAlertDialog(it).showAlert(
                title = getString(R.string.chat_unavailable_title),
                message = message,
                isCancelable = false,
                positiveAnswerAction = {
                    activity?.onBackPressed()
                }
            )
        }
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                REQUEST_CODE_SHOW_CALL_US_DIALOG -> {
                    chatRouter.popChat(activity)
                    context?.startDialerActivity(getString(R.string.support_phone_number)) { snackBarUtil }
                }
            }
        }
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_CANCELLED) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                REQUEST_CODE_SHOW_CALL_US_DIALOG -> {
                    chatRouter.popChat(activity)
                }
            }
        }
    }

    private fun toggleViewState(isLoading: Boolean, isDisabled: Boolean = false) {
        binding.scrollView.visibility = if (isLoading || isDisabled) {
            View.GONE
        } else {
            View.VISIBLE
        }
        if (isLoading) {
            binding.chatSetupPb.show()
        } else {
            binding.chatSetupPb.hide()
        }
    }
}
