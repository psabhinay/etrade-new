package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input

import android.content.Context
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.inputvalueview.InputFieldManager.Value
import io.michaelrocks.libphonenumber.android.NumberParseException
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import java.util.Locale

open class PhoneValidator(
    private val locale: Locale,
    private val invalidMessage: String,
    private val context: Context
) : InputFieldManager.Validator {

    private val util by lazy { PhoneNumberUtil.createInstance(context) }

    override fun validate(value: String): Value {
        return try {
            val phoneNumber = util.parse(value, locale.country)
            if (util.isValidNumber(phoneNumber)) {
                Value.Valid(value)
            } else {
                Value.Invalid(value, invalidMessage)
            }
        } catch (e: NumberParseException) {
            Value.Invalid(value, invalidMessage)
        }
    }
}
