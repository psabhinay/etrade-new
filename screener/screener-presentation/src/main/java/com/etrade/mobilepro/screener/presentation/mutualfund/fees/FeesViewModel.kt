package com.etrade.mobilepro.screener.presentation.mutualfund.fees

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.mutualfund.FeesFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.MutualFund
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class FeesViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @MutualFund screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<FeesFilter, FeesViewModel.ViewState>(aggregator, ScreenerType.MutualFund.Fees, screenerInteractor) {
    override fun provideInitialState(initialFilter: FeesFilter): ViewState = ViewState(filter = initialFilter)

    data class ViewState(
        override val filter: FeesFilter
    ) : BaseFilterScreenViewModel.ViewState<FeesFilter>
}
