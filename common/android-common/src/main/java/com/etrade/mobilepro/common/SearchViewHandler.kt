package com.etrade.mobilepro.common

import androidx.fragment.app.FragmentActivity

interface SearchViewHandler {
    fun toggleSearchView(shouldBeAttached: Boolean, activity: FragmentActivity?)

    fun toggleLookupViewPager(shouldShowPager: Boolean, activity: FragmentActivity?)
}
