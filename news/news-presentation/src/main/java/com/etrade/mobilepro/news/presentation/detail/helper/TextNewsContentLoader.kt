package com.etrade.mobilepro.news.presentation.detail.helper

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.util.NetworkState

interface TextNewsContentLoader {
    val networkState: LiveData<NetworkState>
    val content: LiveData<NewsItem.Text>
    fun load(contentId: String)
    fun load(item: NewsItem.Text)
    fun loadEmpty()
    fun retry()
    fun unsubscribe()
}
