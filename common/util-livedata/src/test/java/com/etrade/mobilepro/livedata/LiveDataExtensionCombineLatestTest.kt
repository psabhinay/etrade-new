package com.etrade.mobilepro.livedata

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.test
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class LiveDataExtensionCombineLatestTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun combineLatestTest() {
        val first = MutableLiveData<Int>()

        val second = MutableLiveData<Boolean>()

        val result = first.combineLatest(second)

        assertTrue(result.value == null)

        first.value = 1

        result
            .test()
            .assertNoValue()

        second.value = true

        result
            .test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it.first == 1 && it.second
            }

        first.value = 2

        result
            .test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it.first == 2 && it.second
            }

        second.value = false

        result
            .test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it.first == 2 && !it.second
            }

        first.value = null

        result
            .test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it == null
            }

        first.value = 3
        second.value = null

        result
            .test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it == null
            }

        second.value = true

        result
            .test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it.first == 3 && it.second
            }
    }
}
