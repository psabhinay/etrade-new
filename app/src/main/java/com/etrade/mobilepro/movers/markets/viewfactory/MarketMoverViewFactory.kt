package com.etrade.mobilepro.movers.markets.viewfactory

import android.content.res.Resources
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicWidget
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.movers.Movers
import com.etrade.mobilepro.movers.MoversRouter
import com.etrade.mobilepro.movers.markets.GetMarketMoversUseCase
import com.etrade.mobilepro.movers.markets.MarketMoversViewModel
import com.etrade.mobilepro.movers.markets.dto.MarketMoverViewDto
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.util.ClickAction
import com.etrade.mobilepro.util.android.CallToAction
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class MarketMoverViewFactory @Inject constructor(
    private val getMarketMoversUseCase: GetMarketMoversUseCase,
    private val moversRouter: MoversRouter,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController,
    private val resources: Resources
) : DynamicUiViewFactory {

    private val logger: Logger = LoggerFactory.getLogger(MarketMoverViewFactory::class.java)
    private val title = resources.getString(R.string.market_movers_title)
    private val callToAction =
        CallToAction(
            resources.getString(R.string.view_all),
            ClickAction(null, null, resources.getString(R.string.market_mover_deep_link))
        )

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is MarketMoverViewDto) {
            return object : GenericLayout, DynamicWidget {

                override val viewModelClass = Movers::class.java

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    toMarketMoversView(dto)
                }

                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is MarketMoversViewModel) {
                        viewModel.getMovers()
                    } else {
                        logger.error("view model is not of type MarketMoversViewModel")
                    }
                }

                override val type = DynamicWidget.Type.MOVERS

                override val uniqueIdentifier: String = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("MarketMoverViewFactory registered to unknown type")
        }
    }

    private fun toMarketMoversView(dto: MarketMoverViewDto): Movers {
        return MarketMoversViewModel(
            getMarketMoversUseCase,
            moversRouter,
            streamingQuoteProvider,
            streamingStatusController,
            title,
            dto.ctaList?.let {
                CallToAction(it[0].label, ClickAction(null, null, it[0].clickActionDto.appUrl))
            } ?: callToAction,
            resources
        )
    }
}
