package com.etrade.mobilepro.portfoliosnapshot

import com.etrade.mobile.power.backends.neo.BaseUsData
import com.etrade.mobile.power.backends.neo.BaseUsResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class NeoPortfolioSnapshotResponse(
    @Json(name = "data")
    override val data: PortfolioSnapshotResponseData
) : BaseUsResponse()

@JsonClass(generateAdapter = true)
class PortfolioSnapshotResponseData(
    @Json(name = "PortfolioResponse")
    val portfolioData: PortfolioResponse? = null
) : BaseUsData()

@JsonClass(generateAdapter = true)
class PortfolioResponse(
    @Json(name = "positionsList")
    val portfolioSnapshotPosition: List<PortfolioSnapshotPosition>
)

@JsonClass(generateAdapter = true)
data class PortfolioSnapshotPosition(
    @Json(name = "positionId")
    val positionId: String,
    @Json(name = "symbol")
    val symbol: String,
    @Json(name = "currentPrice")
    val currentPrice: String,
    @Json(name = "gainLossValue")
    val gainLossValue: String,
    @Json(name = "gainLossPct")
    val gainLossPct: String,
    @Json(name = "marketValue")
    val marketValue: String,
    @Json(name = "optionUnderlier")
    val optionUnderlier: PortfolioOptionsUnderlier? = null,
    @Json(name = "securityType")
    val securityType: String
)

@JsonClass(generateAdapter = true)
data class PortfolioOptionsUnderlier(
    @Json(name = "optionSymbol")
    val optionSymbol: String,
    @Json(name = "optionType")
    val optionType: String,
    @Json(name = "strikePrice")
    val strikePrice: String,
    @Json(name = "expiryDate")
    val expiryDate: String
)
