package com.etrade.mobilepro.dynamicui

import com.etrade.mobile.power.backends.neo.UsServiceException
import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.mgs.BaseMeta
import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.MobileScreen
import com.etrade.mobilepro.dynamic.referencehelper.ReferenceHelper
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.inboxmessages.dynamicui.api.DynamicScreenContentUpdater
import com.etrade.mobilepro.inboxmessages.dynamicui.data.extension.toInboxMessagesWidgetPlace
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.slf4j.Logger
import org.slf4j.LoggerFactory

open class DefaultDynamicScreenRepo(
    private val screenDataSource: ScreenDataSource,
    private val objectFactories: Map<Class<*>, @JvmSuppressWildcards DynamicUiViewFactory>,
    private val referenceHelper: ReferenceHelper,
    private val dynamicContentUpdater: DynamicScreenContentUpdater
) : DynamicScreenRepo {

    protected val logger: Logger by lazy { LoggerFactory.getLogger(javaClass) }

    /**
     *  Transforms original views if required, below are the possible transformations
     *  1. View Orders Rearrange
     *  2. Remove a View
     *  3. Add or insert a view created locally
     *
     *  Refer: CompleteView Screen
     *  */
    open fun screenTransformation(param: MobileScreen): Observable<MobileScreen> = Observable.just(param)

    override fun getScreen(request: ScreenRequest, showCachedData: Boolean, cacheExpiration: Long?): Observable<Resource<List<GenericLayout>>> {
        return screenDataSource.getScreen(request, showCachedData, cacheExpiration)
            .observeOn(Schedulers.io())
            .switchMap { screenListResource ->
                screenTransformation(screenListResource.data?.screen ?: MobileScreen())
                    .map { screen -> mapScreenResult(request, screenListResource, screen) }
            }
            .observeOn(AndroidSchedulers.mainThread())
    }

    @SuppressWarnings("LongMethod")
    private fun mapScreenResult(request: ScreenRequest, screenListResource: Resource<BaseMobileResponse>, screen: MobileScreen): Resource<List<GenericLayout>> {
        val updatedContent = dynamicContentUpdater.updateWithInboxMessages(
            place = request.toInboxMessagesWidgetPlace(),
            input = screen
        )
        val mappedViews = createViews(request, updatedContent, screen.meta)

        val errors = screenListResource.data?.extractError() as? ServerError.Known

        return when (screenListResource) {
            is Resource.Failed -> {
                Resource.Failed(emptyList())
            }
            is Resource.Loading, is Resource.Success -> {
                errors?.let {
                    Resource.Failed(
                        data = mappedViews,
                        error = UsServiceException(
                            errorMessage = errors.messages[0].text,
                            errorCode = screenListResource.data?.getFirstMessageCode()
                        )
                    )
                } ?: when (screenListResource) {
                    is Resource.Success -> {
                        screen.references?.let { references ->
                            references.forEach(referenceHelper::persist)
                            Resource.Success(mappedViews)
                        } ?: Resource.Failed(emptyList())
                    }
                    is Resource.Loading, is Resource.Failed -> screenListResource.map { mappedViews }
                }
            }
        }
    }

    private fun createViews(request: ScreenRequest, views: List<BaseScreenView?>, meta: List<BaseMeta>?) = views.mapNotNull { viewDto ->
        if (viewDto == null) {
            return@mapNotNull null
        }

        val mapper = objectFactories[viewDto::class.java]
        if (mapper == null) {
            logger.warn("Could not find mapper for view data transfer type '${viewDto.type}'")
            null
        } else {
            mapper.setupValuesFromRequest(request)
            mapper.createView(viewDto, meta.orEmpty())
        }
    }
}
