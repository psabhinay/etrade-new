package com.etrade.mobilepro.appwidget.watchlist

import android.content.Context
import androidx.annotation.WorkerThread
import com.etrade.mobilepro.appwidget.api.WatchlistEntryTuple
import com.etrade.mobilepro.appwidget.api.WidgetViewStateRepo
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.common.BaseRemoteViewsAdapter
import com.etrade.mobilepro.util.color.DataColorResolver
import kotlinx.coroutines.runBlocking

@WorkerThread
internal class WatchlistRemoteViewsAdapter(
    context: Context,
    val repo: WidgetViewStateRepo,
    dataColorResolver: DataColorResolver,
    layout: WidgetSettings.Layout
) : BaseRemoteViewsAdapter<WatchlistEntryTuple>(context) {

    override val remoteViewsFactory = WatchlistRemoteViewsFactory(context, dataColorResolver, layout)

    override fun getUpdatedItems(): List<WatchlistEntryTuple> = runBlocking {
        repo.getIndicesViewState().watchlistEntryTuples ?: emptyList()
    }
}
