package com.etrade.mobilepro.dropdown

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.common.Navigation
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.common.getNavigationButtonDescription
import com.etrade.mobilepro.common.setToolbarActionEndIconAndContentDescription
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.setupToolbar
import com.etrade.mobilepro.util.android.accessibility.OnFirstAccessibilityFocusRunner
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

const val BOTTOM_SHEET_DIALOG_SELECTED_ITEM_KEY = "BottomSheetDialogSelectedItemKey"
const val BOTTOM_SHEET_DIALOG_ITEMS_KEY = "BottomSheetDialogItemsKey"
const val BOTTOM_SHEET_DIALOG_TITLE_KEY = "BottomSheetDialogTitleKey"
const val BOTTOM_SHEET_DIALOG_GHOST_TEXT_KEY = "BottomSheetDialogGhostTextKey"
const val BOTTOM_SHEET_DIALOG_DIVIDER_POSITION_KEY = "BottomSheetDividerPositionKey"

class OverlayBottomSheetDialogFragment : BottomSheetDialogFragment() {
    var selectedPosition: Int? = null
        set(value) {
            selectedPosition?.let { viewAdapter?.notifyItemChanged(it) }
            value?.let { viewAdapter?.notifyItemChanged(it) }
            field = value
        }
    var dividerPosition: Int? = null
    var title: String = ""
    var ghostText: String? = null
    internal var items: List<DropDownItem> = emptyList()
    var listener: ((Int) -> Unit)? = null
    var dismissAction: (() -> Unit)? = null
    var toolbarActionEnd: ToolbarActionEnd? = null

    private var viewAdapter: SelectorViewAdapter? = null

    override fun getTheme() = R.style.BottomSheetDialogTheme

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val requiredTag = requireNotNull(tag)
        if (handledWhenDirty(this, requiredTag)) {
            return
        }

        listener.let {
            if (it == null) {
                listener = listeners[requiredTag]
            } else {
                listeners[requiredTag] = it
            }
        }

        toolbarActionEnd.let {
            if (it == null) {
                toolbarActionEnd = toolbarActionEndMap[requiredTag]
            } else {
                toolbarActionEndMap[requiredTag] = it
            }
        }
        restoreStateFromBundle(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inflatedView = inflater.inflate(R.layout.overlay_bottomsheet, container, false)
        viewAdapter = SelectorViewAdapter(items) { position ->
            dismiss()
            listener?.invoke(position)
        }
        viewAdapter?.selectedPosition = selectedPosition
        viewAdapter?.dividerPosition = dividerPosition
        viewAdapter?.ghostText = ghostText
        val viewManager = LinearLayoutManager(context)
        inflatedView.findViewById<RecyclerView>(R.id.overlayRecyclerView).apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
        return inflatedView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val nav = Navigation(
            icon = R.drawable.ic_close,
            onClick = { dismiss() },
            contentDescription = null
        )
        setupToolbar(
            title = title,
            navigation = nav,
            runOnFirstFocus = { toolbar, _, title, _ ->
                OnFirstAccessibilityFocusRunner(toolbar) {
                    title?.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED)
                    toolbar.navigationContentDescription = context?.getNavigationButtonDescription(nav.icon)
                }
            },
            actionEnd = toolbarActionEnd
        )
        dialog?.window?.attributes?.windowAnimations = R.style.DialogAnimation
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setUpBottomSheetDialog()
        return dialog
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(BOTTOM_SHEET_DIALOG_TITLE_KEY, title)
        outState.putParcelableArrayList(BOTTOM_SHEET_DIALOG_ITEMS_KEY, ArrayList(items))

        selectedPosition?.let { outState.putInt((BOTTOM_SHEET_DIALOG_SELECTED_ITEM_KEY), it) }

        dividerPosition?.let {
            outState.putInt(BOTTOM_SHEET_DIALOG_DIVIDER_POSITION_KEY, it)
        }
        ghostText?.also {
            outState.putString(BOTTOM_SHEET_DIALOG_GHOST_TEXT_KEY, it)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        dismissAction?.invoke()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!requireActivity().isChangingConfigurations) {
            val requiredTag = requireNotNull(tag)
            listeners.remove(requiredTag)
            toolbarActionEndMap.remove(requiredTag)
            if (!isRemoving) {
                markDirty(requiredTag)
            }
        }
        dismissAction = null
    }

    private fun restoreStateFromBundle(savedInstanceState: Bundle?) = savedInstanceState?.let { bundle ->
        bundle.getParcelableArrayList<DropDownItem>(BOTTOM_SHEET_DIALOG_ITEMS_KEY)?.let { items = it }
        title = bundle.getString(BOTTOM_SHEET_DIALOG_TITLE_KEY, "")
        selectedPosition = bundle.getInt(BOTTOM_SHEET_DIALOG_SELECTED_ITEM_KEY, -1)

        if (bundle.containsKey(BOTTOM_SHEET_DIALOG_DIVIDER_POSITION_KEY)) {
            dividerPosition = bundle.getInt(BOTTOM_SHEET_DIALOG_DIVIDER_POSITION_KEY)
        }

        if (bundle.containsKey(BOTTOM_SHEET_DIALOG_GHOST_TEXT_KEY)) {
            ghostText = bundle.getString(BOTTOM_SHEET_DIALOG_GHOST_TEXT_KEY)
        }
    }

    /**
     * Sets custom tool bar end action to be displayed with [toolbarActionEnd]
     * @param toolbarActionEnd tool bar action end
     */
    internal fun setToolBarActionEndText(toolbarActionEnd: ToolbarActionEnd) {
        this.toolbarActionEnd = toolbarActionEnd

        // check if view is created
        view?.apply {
            if (toolbarActionEnd.isIcon) {
                findViewById<ImageView>(R.id.toolbarActionEndIcon).apply {
                    setToolbarActionEndIconAndContentDescription(toolbarActionEnd.icon, contentDescription)
                    setOnClickListener(toolbarActionEnd.onClick)
                    visibility = View.VISIBLE
                }
            } else {
                findViewById<TextView>(R.id.toolbarActionEnd).apply {
                    setToolbarActionTextAndContentDescription(toolbarActionEnd.text)
                    setOnClickListener(toolbarActionEnd.onClick)
                    visibility = View.VISIBLE
                }
            }
        }
    }

    private companion object {

        val listeners: MutableMap<String, (Int) -> Unit> = mutableMapOf()

        var toolbarActionEndMap: MutableMap<String, ToolbarActionEnd> = mutableMapOf()

        private val dirtyFragments: MutableSet<String> = mutableSetOf()

        fun markDirty(tag: String) {
            dirtyFragments += tag
        }

        fun handledWhenDirty(fragment: DialogFragment, tag: String): Boolean {
            return dirtyFragments.remove(tag).also { dirty ->
                if (dirty) {
                    fragment.dismiss()
                }
            }
        }
    }
}
