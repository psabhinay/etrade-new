package com.etrade.mobilepro.screener.data.json

import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.Industry
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.Sector
import com.etrade.mobilepro.screener.api.filter.stock.FundamentalsFilter
import com.etrade.mobilepro.screener.api.filter.stock.MarketSegmentFilter
import com.etrade.mobilepro.screener.data.json.stock.FundamentalsFilterModelGenerator
import com.etrade.mobilepro.screener.data.json.stock.MarketSegmentFilterModelGenerator
import com.squareup.moshi.Moshi
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import javax.inject.Provider

@Disabled("This should be fixed on ETAND-8190")
internal class FilterJsonGeneratorsTest {
    private val readedExpectedJson = DefaultScreenerFilterJsonGenerator::class.java.getResource("expectedJson.json")?.readText()?.replace("\n", "")

    private val fundamentalsFilter = FundamentalsFilter(
        peRatioOverTrailing12Months = true,
        peRatio15to25x = true
    )

    private val marketSegmentFilter = MarketSegmentFilter(
        microCap = true,
        sector = Sector("", "", isAllItemsElement = true),
        industry = Industry("", "", isAllItemsElement = true)
    )

    private val generators: Map<Class<*>, Provider<FilterModelGenerator>> = mapOf(
        FundamentalsFilter::class.java to Provider<FilterModelGenerator> { FundamentalsFilterModelGenerator() },
        MarketSegmentFilter::class.java to Provider<FilterModelGenerator> { MarketSegmentFilterModelGenerator() }
    )

    @Test
    fun testFundamentalsFilterJsonGenerating() {
        Assertions.assertEquals(
            readedExpectedJson,
            DefaultScreenerFilterJsonGenerator(Moshi.Builder().build(), generators, DefaultRequestToResultParameterMapper()).generateFiltersJson(listOf(fundamentalsFilter, marketSegmentFilter), ScreenerType.Group.STOCK)
        )
    }
}
