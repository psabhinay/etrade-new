package com.etrade.mobilepro.portfolio

import com.etrade.mobilepro.instrument.Symbol

interface PortfolioTableViewScrollListener {

    fun onLastVisibleItemPositionChanged(position: Int)

    fun onVisibleItemsChanged(visibleSymbols: Set<Symbol>)
}
