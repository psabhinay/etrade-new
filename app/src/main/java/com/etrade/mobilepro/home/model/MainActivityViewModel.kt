package com.etrade.mobilepro.home.model

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.appsflyer.AppsFlyerLib
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.appwidget.AppWidgetHandler
import com.etrade.mobilepro.chat.presentation.service.ChatMessagesService
import com.etrade.mobilepro.common.navigation.NavRouter
import com.etrade.mobilepro.common.navigation.NavRouterSignal
import com.etrade.mobilepro.common.navigation.NavRouterSignalViewModel
import com.etrade.mobilepro.common.navigation.navigate
import com.etrade.mobilepro.common.toolbarView
import com.etrade.mobilepro.common.viewmodel.pendingNavDirections
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.eventtrackerapi.AppsFlyerRepo
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.navigation.pendingNavUri
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.pushnotification.usecase.ManageSmartAlertSubscriptionUseCase
import com.etrade.mobilepro.quotelookup.getSearchIntentAndOptions
import com.etrade.mobilepro.searchingapi.SearchAnimationParams
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.tier.CustomerTierRepo
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.TrackLogInParams
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.DeviceRootChecker
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.viewmodel.ActivitySignal
import com.etrade.mobilepro.viewmodel.ActivitySignalViewModel
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingState
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@Suppress("LongParameterList")
class MainActivityViewModel @Inject constructor(
    context: Context,
    private val tracker: Tracker,
    private val user: User,
    private val streamingStatusController: StreamingStatusController,
    private val appWidgetHandler: AppWidgetHandler,
    private val customerTierRepo: CustomerTierRepo,
    private val accountListRepo: AccountListRepo,
    private val manageAlertSubscription: ManageSmartAlertSubscriptionUseCase,
    private val userSessionHandler: UserSessionHandler,
    private val walkthroughStatesViewModel: WalkthroughStatesViewModel,
    private val appPreferences: ApplicationPreferences,
    private val appsFlyerRepo: AppsFlyerRepo,
    private val deviceRootChecker: DeviceRootChecker,
    private val userViewModel: UserViewModel
) : ViewModel(), ActivitySignalViewModel, NavRouterSignalViewModel {

    override val activitySignal: LiveData<ConsumableLiveEvent<ActivitySignal>>
        get() = _activitySignal

    override val navRouterSignal: LiveData<ConsumableLiveEvent<NavRouterSignal>>
        get() = _navRouterSignal

    @Suppress("StaticFieldLeak")
    private val applicationContext: Context = context.applicationContext

    private val _activitySignal: MutableLiveData<ConsumableLiveEvent<ActivitySignal>> = MutableLiveData()
    private val _navRouterSignal: MutableLiveData<ConsumableLiveEvent<NavRouterSignal>> = MediatorLiveData<ConsumableLiveEvent<NavRouterSignal>>().apply {
        addSource(walkthroughStatesViewModel.state) {
            when (it) {
                SymbolTrackingState.DisplayWatchlistPage -> navigate(MainGraphDirections.actionHomeToWatchlist())
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        walkthroughStatesViewModel.dispose()
    }

    fun openSearchActivity(
        query: SearchQuery,
        sharedViewId: Int = R.id.menu_quote_lookup,
        simpleSymbolSearch: Boolean,
        animated: Boolean = true,
        quoteLookupLocation: String? = null
    ) {
        _activitySignal.value = { activity: Activity ->
            val params = StartSearchParams(
                query = query,
                stockQuoteSearchOnly = simpleSymbolSearch,
                animationParams = SearchAnimationParams(sharedViewId, animated),
                quoteLookupLocation = quoteLookupLocation
            )
            val (intent, option) = activity.toolbarView.getSearchIntentAndOptions(activity, params)
            activity.startActivity(intent, option?.toBundle())
        }.consumable()
    }

    fun userSessionStateChange(event: SessionStateChange?) {
        when (event) {
            SessionStateChange.AUTHENTICATED -> {
                viewModelScope.launch {
                    manageAlertSubscription.execute()

                    val userId = user.session?.aatId ?: return@launch
                    val isInternationalUser = user.session?.isInternationalUser ?: return@launch

                    tracker.logIn(userId, getLogInParams(userId, isInternationalUser))

                    handleAppsFlyerTracking(isInternationalUser, userId)
                }
                appWidgetHandler.refresh()
                postLoginNavigation()
            }
            SessionStateChange.UNAUTHENTICATED -> {
                viewModelScope.launch {
                    userSessionHandler.logout()
                }
                ChatMessagesService.stop(applicationContext)
                postLogoutNavigation()
            }
        }
    }

    private fun handleAppsFlyerTracking(isInternationalUser: Boolean, userId: String) {
        if (appPreferences.shouldTrackWithAppsFlyer) {
            val appsFlyer = AppsFlyerLib.getInstance()

            if (isInternationalUser) {
                viewModelScope.launch(Dispatchers.IO) {
                    appsFlyerRepo.sendAppsFlyerEraseRequest(
                        // this is potentially blocking
                        AdvertisingIdClient.getAdvertisingIdInfo(applicationContext).id
                    )
                }
                appPreferences.shouldTrackWithAppsFlyer = false
                appsFlyer.stopTracking(true, applicationContext)
            } else {
                appsFlyer.setCustomerUserId(userId)
                appsFlyer.trackEvent(applicationContext, "login", mapOf("id" to userId))
            }
        }
    }

    private fun postLoginNavigation() {
        _navRouterSignal.value = { router: NavRouter ->
            if (!router.hasDestinationInBackStack(R.id.quoteBottomSheetFragment) && pendingNavDirections == null && pendingNavUri == null) {
                router.navigateToLandingPage()
            } else {
                pendingNavUri?.let {
                    router.onLoggedIn()
                }

                pendingNavDirections?.let {
                    router.onLoggedIn()
                } ?: Unit
            }
        }.consumable()
    }

    private fun postLogoutNavigation() {
        if (userViewModel.previousSessionState == SessionStateChange.AUTHENTICATED) {
            _activitySignal.value = { activity: Activity ->
                // This forces an activity to finish, marking it as "finishing". isFinishing() method is being used in LiveData observers to avoid consumption
                // of a signal or an event by an instance of finishing activity. This helps to overcome the problem when for some period of time there may be
                // two instances of MainActivity with active observers on the same LiveData objects.
                activity.finish()

                val intent = Intent(activity, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NO_ANIMATION)
                activity.startActivity(intent)
            }.consumable()
        }
    }

    private suspend fun getLogInParams(userId: String, isInternationalUser: Boolean): TrackLogInParams {
        val accounts = accountListRepo.getAccountList()

        return TrackLogInParams(
            international = isInternationalUser,
            accountFunded = isAccountFunded(accounts),
            accountTypes = getAccountTypes(accounts),
            isStreamingEnabled = streamingStatusController.isStreamingToggleEnabled,
            serviceTier = customerTierRepo.getTier().getOrNull()?.asString,
            isRootedDevice = deviceRootChecker.isRooted(),
            customerId = userId
        )
    }

    private fun getAccountTypes(accounts: List<Account>): String? {
        return accounts
            .asSequence()
            .map { it.accountType.name }
            .distinct()
            .sorted()
            .joinToString(separator = ", ")
    }

    private fun isAccountFunded(accounts: List<Account>): Boolean {
        return accounts.firstOrNull { it.funded } != null
    }
}
