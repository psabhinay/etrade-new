package com.etrade.mobilepro.ui_demo.featureflags

import com.etrade.mobilepro.feature.Feature
import com.etrade.mobilepro.feature.FeatureCategory
import com.etrade.mobilepro.feature.qualifiers.RemoteEnabledFeatures
import com.etrade.mobilepro.ui_demo.featureflags.DemoFeatureFlagCategories.NON_REMOTE
import com.etrade.mobilepro.ui_demo.featureflags.DemoFeatureFlagCategories.REMOTE
import dagger.Provides

enum class DemoFeatureFlags(
    override val category: FeatureCategory,
    override val key: String,
    override val title: String,
    override val explanation: String,
    override val defaultValue: Boolean = false,
) : Feature {
    NON_REMOTE_1(
        category = NON_REMOTE,
        key = "feature_local_first",
        title = "Local Feature 1",
        explanation = "First local feature."
    ),
    REMOTE_1(
        category = REMOTE,
        key = "feature_remote_first",
        title = "Remote Feature 1",
        explanation ="First remote feature."
    ),
    NON_REMOTE_2(
        category = NON_REMOTE,
        key = "feature_local_second",
        title = "Local Feature 2",
        explanation ="Second local feature."
    ),
    DEMO4(
        category = REMOTE,
        key = "feature_remote_second",
        title = "Remote Feature 2",
        explanation ="Second remote feature."
    );

    companion object {
        fun getRemoteEnabledFeatures(): Set<Feature> = values()
            .filter { it.category == REMOTE }
            .toSet()
    }
}

enum class DemoFeatureFlagCategories(
    override val title: String,
    override val explanation: String
) : FeatureCategory {
    NON_REMOTE(
        title = "Non-Remote",
        explanation = "Features that cannot be set by remote config"
    ),
    REMOTE(
        title = "Remote",
        explanation = "Features that can be set by remote config"
    )
}
