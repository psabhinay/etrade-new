package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.util.MutualConverter

object CommonPriceTypeValueId : PriceTypeValueId {
    override val even: String = PriceType.EVEN.typeCode
    override val limit: String = PriceType.LIMIT.typeCode
    override val market: String = PriceType.MARKET.typeCode
    override val netCredit: String = PriceType.NET_CREDIT.typeCode
    override val netDebit: String = PriceType.NET_DEBIT.typeCode
    override val stop: String = PriceType.STOP.typeCode
    override val stopLimit: String = PriceType.STOP_LIMIT.typeCode
    override val trailingStopDollar: String = PriceType.TRAILING_STOP_DOLLAR.typeCode
    override val trailingStopPercent: String = PriceType.TRAILING_STOP_PERCENT.typeCode
    override val converter: MutualConverter<String, PriceType> by lazy { createConverter() }
}
