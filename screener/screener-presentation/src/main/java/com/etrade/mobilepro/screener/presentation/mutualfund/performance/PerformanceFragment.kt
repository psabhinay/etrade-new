package com.etrade.mobilepro.screener.presentation.mutualfund.performance

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.mutualfund.PerformanceFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.FragmentPerformanceBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class PerformanceFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<PerformanceFilter, PerformanceViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {

    private val binding by viewBinding(FragmentPerformanceBinding::bind)
    override val viewModel by viewModels<PerformanceViewModel> { viewModelFactory }
    override val layoutRes: Int = R.layout.fragment_performance
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<PerformanceFilter, PerformanceViewModel.ViewState, Boolean>>
        get() = mapOf(
            binding.bottomTwentyPercent to Category.BOTTOM_20,
            binding.belowMidTwentyPercent to Category.BELOW_MID_20,
            binding.midTwentyPercent to Category.MID_20,
            binding.aboveMidTwentyPercent to Category.ABOVE_MID_20,
            binding.topTwentyPercent to Category.TOP_20,
            binding.oneMonth to Period.ONE_MONTH,
            binding.threeMonths to Period.THREE_MONTHS,
            binding.sixMonths to Period.SIX_MONTHS,
            binding.ytd to Period.YTD,
            binding.oneYear to Period.ONE_YEAR,
            binding.threeYears to Period.THREE_YEARS,
            binding.fiveYears to Period.FIVE_YEARS,
            binding.tenYears to Period.TEN_YEARS
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                updateCategory(viewState = it)
                updatePeriod(viewState = it)
            }
        )
    }

    private fun updateCategory(viewState: PerformanceViewModel.ViewState) = viewState.filter.apply {
        binding.apply {
            bottomTwentyPercent.isChecked = bottom20
            belowMidTwentyPercent.isChecked = belowMid20
            midTwentyPercent.isChecked = mid20
            aboveMidTwentyPercent.isChecked = aboveMid20
            topTwentyPercent.isChecked = top20
        }
    }

    private fun updatePeriod(viewState: PerformanceViewModel.ViewState) = viewState.filter.apply {
        binding.apply {
            oneMonth.isChecked = overLast1Month
            threeMonths.isChecked = overLast3Months
            sixMonths.isChecked = overLast6Months
            ytd.isChecked = overLastYTD
            oneYear.isChecked = overLast1Year
            threeYears.isChecked = overLast3Years
            fiveYears.isChecked = overLast5Years
            tenYears.isChecked = overLast10Years
        }
    }
}
