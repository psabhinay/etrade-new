package com.etrade.mobilepro.transfermoney.presentation.confirmation

data class PreviewConfirmation(
    val amount: String,
    val fromAccount: String,
    val toAccount: String,
    val scheduleDate: String
)
