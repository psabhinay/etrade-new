package com.etrade.mobilepro.carettablayout

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.core.view.children
import com.google.android.material.tabs.TabLayout
import kotlin.math.min

/**
 * A [TabLayout] which ensures that its child views have a fixed min width based on
 * its parent's width even when it is in [TabLayout.MODE_SCROLLABLE] mode.
 */
private const val MAX_TABS_PER_SCREEN = 4.5f

internal class FullWidthTabLayout : TabLayout {

    var fitsScreenWidth = true
        private set

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val screenWidth = context.resources.displayMetrics.widthPixels
        val tabLayout = getChildAt(0) as? ViewGroup
        val childCount = tabLayout?.childCount ?: 0

        if (childCount > 0) {
            val tabsPerScreen = min(childCount * 1f, MAX_TABS_PER_SCREEN)
            val tabMinWidth = screenWidth / tabsPerScreen

            tabLayout?.children?.forEach {
                it.minimumWidth = tabMinWidth.toInt()
            }
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val totalWidth = tabLayout?.children?.sumOf { it.measuredWidth } ?: 0
        fitsScreenWidth = totalWidth <= screenWidth
    }
}
