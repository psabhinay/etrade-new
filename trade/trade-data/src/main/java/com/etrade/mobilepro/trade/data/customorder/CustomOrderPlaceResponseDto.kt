package com.etrade.mobilepro.trade.data.customorder

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class CustomOrderPlaceResponseDto(
    @Json(name = "customOrderPlace")
    override val order: CustomOrderDto?
) : CustomOrderResponse()
