package com.etrade.mobilepro.home

import android.content.res.Resources
import android.os.Bundle
import android.view.MenuItem
import androidx.annotation.IdRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.FloatingWindow
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.ui.NavigationUI
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RestorableViewModelDelegate
import com.etrade.mobilepro.common.isMenuButton
import com.etrade.mobilepro.common.navigation.NavRouterSignal
import com.etrade.mobilepro.common.navigation.NavRouterSignalViewModel
import com.etrade.mobilepro.common.navigation.navigate
import com.etrade.mobilepro.menu.TextMenuItem
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.walkthrough.api.TradePlacingAction
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import java.util.Stack
import javax.inject.Inject

class BottomNavigationViewModel @Inject constructor(
    private val userViewModel: UserViewModel,
    private val restorableDelegate: RestorableViewModelDelegate,
    private val resources: Resources,
    private val walkthroughViewModel: WalkthroughStatesViewModel,
    private val appPreferences: ApplicationPreferences
) : ViewModel(), NavRouterSignalViewModel {

    override val navRouterSignal: LiveData<ConsumableLiveEvent<NavRouterSignal>>
        get() = _navRouterSignal

    val accountsItemSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _accountsItemSignal
    val checkedItem: LiveData<Int?>
        get() = _checkedItem
    val isVisible: LiveData<Boolean>
        get() = _isVisible

    val savedMenuDirections: Stack<TextMenuItem.MenuAction> by lazy {
        Stack<TextMenuItem.MenuAction>()
    }

    /**
     * Collection of navigate ids in menu fragment.
     */
    private val menuFragmentSet = setOf(
        R.id.action_menu_login_required_web_view,
        R.id.newsFragment,
        R.id.alertsFragment,
        R.id.bill_pay_nav_graph,
        R.id.check_deposit_graph,
        R.id.learn_graph,
        R.id.learnContainerFragment,
        R.id.learTutorialFragment,
        R.id.transfer_money_nav_graph
    )

    private val _accountsItemSignal: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()
    private val _checkedItem: MutableLiveData<Int?> = MutableLiveData()
    private val _isVisible: MutableLiveData<Boolean> = MutableLiveData()
    private val _navRouterSignal: MutableLiveData<ConsumableLiveEvent<NavRouterSignal>> = MutableLiveData()

    private var previousDestination: NavDestination? = null

    init {
        restorableDelegate.onBottomNavigationItemSelected(R.id.accountsHomeFragment)
    }

    fun selectItem(currentItemId: Int, item: MenuItem, navController: NavController): Boolean {
        val itemId = item.itemId
        restorableDelegate.onBottomNavigationItemSelected(itemId)
        walkthroughViewModel.onAction(TradePlacingAction.Canceled)

        if (currentItemId == R.id.accountsHomeFragment) {
            handleAccountsMenuItemLeaving(navController)
        }

        if (restoreAccountTabHostFragment(itemId)) {
            return true
        }

        return if (itemId != R.id.action_launch_trade || userViewModel.isAuthenticated) {
            val result = NavigationUI.onNavDestinationSelected(item, navController)

            if (itemId.isMenuButton(resources)) {
                restoreMenuPage()
            }

            result
        } else {
            _navRouterSignal.navigate(MainGraphDirections.actionLaunchTrade())
            false
        }
    }

    private fun handleAccountsMenuItemLeaving(navController: NavController) {
        if (navController.currentDestination?.id == R.id.accountTabHostFragment) {
            userViewModel.currentSelectedAccount.value?.account?.accountId?.let {
                appPreferences.lastVisitedAccountsTabAccountId = it
            }
        } else {
            appPreferences.lastVisitedAccountsTabAccountId = ""
        }
    }

    /**
     * Checks if last page on "Accounts" bottom menu was [AccountTabHostFragment] and restores its state.
     *
     * @return `true` if [AccountTabHostFragment] navigation state has been restored successfully, and `false` otherwise.
     */
    private fun restoreAccountTabHostFragment(destinationId: Int): Boolean {
        if (destinationId == R.id.accountsHomeFragment && userViewModel.isAuthenticated) {
            val accountId = appPreferences.lastVisitedAccountsTabAccountId
            val tabIndex = appPreferences.lastVisitedAccountsTabIndex

            if (accountId.isNotEmpty() && tabIndex != -1) {
                _navRouterSignal.navigate(
                    MainGraphDirections.actionToSelectedAccountTabIndexOrTitle(
                        accountId = accountId,
                        tabIndex = tabIndex
                    )
                )
                return true
            }
        }

        return false
    }

    private fun restoreMenuPage() {
        savedMenuDirections.forEach {
            when (it) {
                is TextMenuItem.MenuAction.MenuNavDirectionsAction -> {
                    _navRouterSignal.navigate(it.navDirections)
                }
                is TextMenuItem.MenuAction.MenuAppAction -> {
                    _navRouterSignal.navigate(it.uri)
                }
                is TextMenuItem.MenuAction.MenuGenericAction -> {
                    _navRouterSignal.navigate(it.action)
                }
            }
        }
    }

    fun reselectItem(item: MenuItem) {
        when (item.itemId) {
            R.id.accountsHomeFragment -> _accountsItemSignal.value = Unit.consumable()
            R.id.menuGraph -> _navRouterSignal.navigate(MainGraphDirections.actionPopMenuFragmentStackToMenuFragment())
        }
    }

    fun updateDestination(destination: NavDestination, arguments: Bundle?) {
        previousDestination = (destination.takeIf { it !is FloatingWindow } ?: previousDestination)?.also {
            updateCheckedItem(it.id)
            updateVisibility(it, arguments)
        }
    }

    private fun updateCheckedItem(@IdRes destinationId: Int) {
        _checkedItem.value = when {
            menuFragmentSet.contains(destinationId) -> R.id.menuGraph
            destinationId == R.id.overviewFragment || destinationsWithAccountNameTitle.contains(destinationId) -> R.id.accountsHomeFragment
            destinationId == R.id.action_launch_trade || destinationId == R.id.tradeFragment -> R.id.action_launch_trade
            else -> null
        }
    }

    private fun updateVisibility(destination: NavDestination, arguments: Bundle?) {
        if (destination is FloatingWindow) {
            return
        }
        _isVisible.value = destinationsWithBottomBar.contains(destination.id) && !(arguments?.getBoolean(KEY_HIDE_BOTTOM_BAR, false) ?: false)
    }
}
