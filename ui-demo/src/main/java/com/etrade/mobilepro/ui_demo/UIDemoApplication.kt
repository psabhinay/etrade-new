package com.etrade.mobilepro.ui_demo

import android.app.Application
import com.etrade.mobilepro.common.di.ApplicationScope
import com.etrade.mobilepro.feature.firebase.FirebaseFeatureFlagProvider
import com.etrade.mobilepro.ui_demo.dagger.ApplicationComponent
import com.etrade.mobilepro.ui_demo.dagger.ApplicationModule
import com.etrade.mobilepro.ui_demo.dagger.DaggerApplicationComponent
import com.localytics.androidx.AnalyticsListenerAdapter
import com.localytics.androidx.Localytics
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class UIDemoApplication : Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    @ApplicationScope
    lateinit var appScope: CoroutineScope

    @Inject
    lateinit var firebaseFeatureFlagProvider: FirebaseFeatureFlagProvider

    lateinit var binder: ApplicationComponent

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate() {
        super.onCreate()
        initDagger()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        initFirebase()
        Localytics.autoIntegrate(this)
        Localytics.setAnalyticsListener(object : AnalyticsListenerAdapter() {
            override fun localyticsSessionDidOpen(isFirst: Boolean, isUpgrade: Boolean, isResume: Boolean) {
                if (isFirst && !isResume) {
                    val localFile = File("/android_asset/onboarding/index.html")
                    Localytics.forceInAppMessage(localFile)
                }
            }
        })
    }

    private fun initDagger() {
        binder = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()

        binder.inject(this)
    }

    private fun initFirebase() {
        appScope.launch {
            firebaseFeatureFlagProvider.initializeRemoteConfig()
        }
    }
}
