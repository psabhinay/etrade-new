package com.etrade.mobilepro.session.api

import com.etrade.eo.rest.authentication.Authenticator
import java.util.concurrent.CopyOnWriteArrayList
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class User @Inject constructor() : Authenticator, UserConsumerKeyInterface {

    private val realtimeQuotesStatusChangeListeners = CopyOnWriteArrayList<RealtimeQuotesStatusUpdateListener>()

    @Volatile
    var session: Session? = null

    override val authenticationToken: String?
        get() = session?.token

    val defaultAccountId: String?
        get() = session?.defaultAccountId?.replace("-", "")

    val principleId: String?
        get() = session?.principalId

    val token: String?
        get() = session?.token

    fun getUserId(): Long? = session?.userId

    override fun getConsumerKey(): String? = session?.consumerKey

    fun isLoggedIn(): Boolean = session != null

    var isRealtimeUser: Boolean
        get() = session?.isRealTimeQuotes ?: false
        set(value) {
            session?.let {
                if (it.isRealTimeQuotes != value) {
                    it.isRealTimeQuotes = value
                    realtimeQuotesStatusChangeListeners.forEach { listener -> listener() }
                }
            }
        }

    val isMultiAccountUser: Boolean
        get() = session?.let {
            it.numberOfAccounts > 1
        } ?: false

    val isExtendedHoursAccepted: Boolean
        get() = session?.isExtendedHoursAccepted ?: false

    fun subscribeForRealtimeQuotesStatusUpdates(listener: RealtimeQuotesStatusUpdateListener) {
        realtimeQuotesStatusChangeListeners.add(listener)
    }

    fun unsubscribeFromRealtimeQuotesStatusUpdates(listener: RealtimeQuotesStatusUpdateListener) {
        realtimeQuotesStatusChangeListeners.remove(listener)
    }

    /**
     * @return return streamer url if it is not empty. Otherwise, return default streamer url.
     * */
    fun getValidStreamerUrl(
        streamerUrl: String?,
        defaultStreamerUrl: String
    ): String {
        return streamerUrl?.takeIf {
            it.isNotEmpty()
        } ?: defaultStreamerUrl
    }
}

typealias RealtimeQuotesStatusUpdateListener = () -> Unit

typealias UserAuthenticationState = () -> Boolean

interface UserConsumerKeyInterface {
    fun getConsumerKey(): String?
}
