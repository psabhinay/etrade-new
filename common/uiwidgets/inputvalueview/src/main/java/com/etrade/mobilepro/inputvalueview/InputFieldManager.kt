package com.etrade.mobilepro.inputvalueview

import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.inputvalueview.validators.ComposedValidator
import com.etrade.mobilepro.util.android.extension.updateValue
import com.google.android.material.textfield.TextInputLayout

/**
 * This class is responsible for additional logic applied to EditText
 * This additional logic is represented by following interfaces:
 *  Formatter - formats raw content(i.e. inputted by a user)
 *  Validator - validates value
 *  Customizer - changes input filters of EditText on some events (see Event enum below)
 * This class instance should be attached to specific EditText, typically in onActivityCreated(..) callback
 * This class relies on external dependencies(see Dependencies) to apply customized value logic
 This class relies on external means of saving its state(see rawContentData parameter of setupView(..) method)
 *
 * Currently this class is not intended to be used outside of this module, excepting its inner entities definitions.
 * Custom views of this module delegate their method calls to this class
 */
class InputFieldManager(
    private val lastCompleteValue: MutableLiveData<String>
) {

    /**
     * This enum represents events which happens with EditText
     * ON_INIT - event happens when setupView(..) method sets Customizer instance. On this event client can preconfigure
     *      EditText
     * ON_FOCUS_GAINED / ON_FOCUS_LOST - these events happen when EditText receives and looses focus
     */
    enum class Event {
        ON_INIT,
        ON_FOCUS_GAINED,
        ON_FOCUS_LOST
    }

    /**
     * This determines, in what way raw content of EditText will be formatted on ON_FOCUS_LOST event or by demand
     */
    interface Formatter {
        /**
         * @param rawContent - raw content of EditText(that one which is stored in external LiveData). This can and can not be the same
         * as the current content of EditText
         */
        fun formatRawContent(rawContent: String): String
    }

    /**
     * Determines validation logic.
     */
    interface Validator {
        /**
         * @param value - raw content of EditText(that one which is stored in external LiveData). This can and can not be the same
         * as the current content of EditText
         */
        fun validate(value: String): Value

        operator fun plus(other: Validator): ComposedValidator = ComposedValidator(this, other)
    }

    /**
     * Determines, which list of InputFilters should be applied on different events
     */
    interface Customizer {
        /**
         * @param event - Event, which has just happened
         */
        fun updateInputFilters(event: Event): List<InputFilter>
    }

    data class Dependencies(
        val formatter: Formatter,
        val validator: Validator,
        val customizer: Customizer? = null
    )

    sealed class Value {
        class Valid(val content: String) : Value()
        class Invalid(val content: String, val reason: String) : Value()
    }

    private var editText: EditText? = null
    private var textInputLayout: TextInputLayout? = null
    private lateinit var validator: Validator
    private lateinit var formatter: Formatter
    private var customizer: Customizer? = null
    private lateinit var rawContentData: MutableLiveData<String>
    private lateinit var validatedData: MutableLiveData<Value>
    private var rawContent: String
        get() = rawContentData.value ?: ""
        set(newValue) {
            rawContentData.value = newValue
        }

    internal var rawTextChangeListener: ((String) -> Unit)? = null

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            rawContent = s?.toString() ?: ""
            validatedData.value = validator.validate(rawContent)
            rawTextChangeListener?.invoke(rawContent)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    }

    private val focusListener = View.OnFocusChangeListener { _, hasFocus ->
        if (hasFocus) {
            applyInputFilters(this.customizer?.updateInputFilters(Event.ON_FOCUS_GAINED))
            textInputLayout?.error = null
            editText?.editableText?.clear()
            editText?.editableText?.append(rawContent)
            editText?.addTextChangedListener(textWatcher)
        } else {
            editText?.removeTextChangedListener(textWatcher)
            applyInputFilters(this.customizer?.updateInputFilters(Event.ON_FOCUS_LOST))
            lastCompleteValue.value = rawContent
            formatContents()
        }
    }

    internal fun updateTextValueWithoutNotification(value: String) {
        editText?.removeTextChangedListener(textWatcher)
        editText?.text?.updateValue(value)
        rawContent = value
        editText?.addTextChangedListener(textWatcher)
    }

    @SuppressWarnings("LongParameterList")
    internal fun setupView(
        editText: EditText?,
        textInputLayout: TextInputLayout?,
        dependencies: Dependencies,
        rawContentData: MutableLiveData<String>,
        validatedData: MutableLiveData<Value>
    ) {
        this.formatter = dependencies.formatter
        this.validator = dependencies.validator
        this.validatedData = validatedData
        this.textInputLayout = textInputLayout
        this.editText = editText
        this.rawContentData = rawContentData

        editText?.text?.let {
            if (it.isNotBlank() && rawContentData.value == null) {
                rawContentData.value = it.toString()
            }
        }

        rawContentData.value.let {
            if (!it.isNullOrBlank()) {
                formatContents()
            }
        }

        this.customizer = dependencies.customizer
        applyInputFilters(this.customizer?.updateInputFilters(Event.ON_INIT))
        this.editText?.onFocusChangeListener = focusListener
    }

    internal fun formatContents() {
        val validated = validator.validate(rawContent)
        validatedData.value = validated

        when (validated) {
            is Value.Valid -> {
                val formattedContent = formatter.formatRawContent(validated.content)
                editText?.editableText?.apply {
                    clear()
                    append(formattedContent)
                }
            }
            is Value.Invalid -> {
                textInputLayout?.error = validated.reason
            }
        }
    }

    internal fun getValue(): Value = validator.validate(rawContent)

    private fun applyInputFilters(filters: List<InputFilter>?) {
        filters?.forEach {
            editText?.updateFilter(it)
        }
    }
}

private fun EditText.updateFilter(filter: InputFilter) {
    filters = if (filters.isNullOrEmpty()) {
        arrayOf(filter)
    } else {
        filters
            .toMutableList()
            .apply {
                removeAll { it.javaClass == filter.javaClass }
                add(filter)
            }
            .toTypedArray()
    }
}
