package com.etrade.mobilepro.backends.api

sealed class ServerError {
    object NullResponse : ServerError()
    object NoError : ServerError()
    data class Known(
        val messages: List<ServerMessage>
    ) : ServerError()

    fun extractMessage(): String? = (this as? Known)?.messages?.getOrNull(0)?.text

    fun extractServerMessage(): ServerMessage? = (this as? Known)?.messages?.getOrNull(0)
}
