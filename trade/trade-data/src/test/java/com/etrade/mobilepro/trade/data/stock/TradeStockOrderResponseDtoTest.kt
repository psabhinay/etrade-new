package com.etrade.mobilepro.trade.data.stock

import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.testutil.XmlDeserializer
import com.etrade.mobilepro.trade.api.action
import com.etrade.mobilepro.trade.api.quantity
import com.etrade.mobilepro.trade.api.ticker
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class TradeStockOrderResponseDtoTest {
    @Test
    fun `test stock market order new preview response`() {
        val previewResponse = StockOrderPreviewResponseWrapper(readFromFile("preview_stock_order_response.xml"))
        val order = previewResponse.orders.first()
        assertEquals(false, order.allOrNone)
        assertEquals(TransactionType.BUY_OPEN, order.action)
        assertEquals(PriceType.MARKET, order.priceType)
        assertEquals(OrderTerm.GOOD_FOR_DAY, order.term)
        assertEquals(false, order.allOrNone)
        assertEquals("AAPL", order.ticker)
        assertEquals("Market", order.yourPrice)
        assertEquals(BigDecimal("0.00"), order.fee)
        assertEquals(BigDecimal("1621.50"), order.cost)
        assertEquals(BigDecimal(5), order.quantity)
    }

    @Test
    fun `test stock market order new place response`() {
        val placeResponse = StockOrderPlaceResponseWrapper(readFromFile("place_stock_order_response.xml"))
        val order = placeResponse.orders.first()
        assertEquals("2752", order.confirmationOrderId)
        assertEquals(false, order.allOrNone)
        assertEquals(TransactionType.BUY_OPEN, order.action)
        assertEquals(PriceType.MARKET, order.priceType)
        assertEquals(OrderTerm.GOOD_FOR_DAY, order.term)
        assertEquals(false, order.allOrNone)
        assertEquals("AAPL", order.ticker)
        assertEquals(BigDecimal("0.00"), order.fee)
        assertEquals(BigDecimal("1621.85"), order.cost)
        assertEquals(BigDecimal(5), order.quantity)
        assertEquals("02/14/2020 01:31:34 PM ET", order.placedDate)
    }

    @Test
    fun `test multiple warning messages`() {
        val previewResponse = readFromFile("sell_preview_stock_order_response.xml")
        assertEquals(2, previewResponse.warnings.size)
    }

    @Test
    fun `test overnight indicator warning`() {
        val previewResponse = StockOrderPreviewResponseWrapper(readFromFile("preview_stock_order_overnight_response.xml"))
        assertEquals(2, previewResponse.warnings.size)
        assertEquals(0, previewResponse.warnings.first().priority)
        assertTrue(previewResponse.isOvernightTrading)
    }

    @Test
    fun `test xml parse of login error`() {
        val errorResponse = readFromFile("session_expired_error.xml")
        assertEquals(1, errorResponse.getErrors().size)
        assertTrue(errorResponse.warnings.isEmpty())
        assertNull(errorResponse.order)
    }

    @Test
    fun `test xml parse of extended hours disclosure error`() {
        val errorResponse = readFromFile("preview_stock_order_extended_hours_disclosure.xml")
        assertEquals(1, errorResponse.getErrors().size)
        assertNotNull(errorResponse.order)
    }

    private fun readFromFile(path: String): StockOrderResponseDto = XmlDeserializer.getObjectFromXml(path, StockOrderResponseDto::class.java)
}
