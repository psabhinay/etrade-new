package com.etrade.mobilepro.compose.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.dimensionResource

@Composable
fun ThickDivider() {
    Box(
        modifier = Modifier
            .height(dimensionResource(R.dimen.spacing_medium))
            .fillMaxWidth()
            .background(colorResource(R.color.light_grey))
    )
}
