package com.etrade.mobilepro.portfolio

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.OptionType
import com.etrade.mobilepro.instrument.getOptionType
import com.etrade.mobilepro.instrument.safeUnderlyingSymbolInfo
import com.etrade.mobilepro.orders.api.OFFSET_CLOSED
import com.etrade.mobilepro.orders.api.OFFSET_OPEN
import com.etrade.mobilepro.orders.api.OrderAction
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.positions.api.isShort
import com.etrade.mobilepro.trade.api.option.OptionTradeStrategy
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.getTradeAction
import com.etrade.mobilepro.trade.form.api.toSecurityType
import java.math.BigDecimal
import javax.inject.Provider

fun getTradeFormParams(
    portfolioRecord: PortfolioRecord,
    accountId: String?,
    orderAction: OrderAction,
    tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    defaultQuantity: BigDecimal
): InputValuesSnapshot {
    val recInstrument = portfolioRecord.instrument
    val instrumentType = recInstrument.instrumentType

    return tradeFormParametersBuilderProvider.get()
        .apply {
            symbol = recInstrument.safeUnderlyingSymbolInfo
            this.accountId = accountId

            if (portfolioRecord.isTaxLot) {
                positionLotId = portfolioRecord.recordId
            }

            setActionAndQuantity(
                portfolioRecord,
                instrumentType,
                orderAction,
                defaultQuantity
            )

            if (instrumentType.isOption) {
                addLegToTradeParams(portfolioRecord, orderAction, defaultQuantity)
            }
        }
        .create(instrumentType.toSecurityType())
}

private fun TradeFormParametersBuilder.addLegToTradeParams(
    record: PortfolioRecord,
    orderAction: OrderAction,
    defaultQuantity: BigDecimal
) {
    val tradeLeg = createLegFromPortfolioRecord(record, orderAction, defaultQuantity)
    optionTradeStrategy = createOptionStrategyObject(listOf(tradeLeg))
}

internal fun TradeFormParametersBuilder.addLegsToTradeParams(records: List<PortfolioRecord>, defaultQuantity: BigDecimal) {
    val optionLegsForTrade: MutableList<TradeLeg> = mutableListOf()
    for (record in records) {
        val calculatedOrderAction = OrderAction.fromBoolean(record.position.isShort)
        optionLegsForTrade += createLegFromPortfolioRecord(record, calculatedOrderAction, defaultQuantity)
    }
    optionTradeStrategy = createOptionStrategyObject(optionLegsForTrade)
}

private fun createLegFromPortfolioRecord(
    record: PortfolioRecord,
    orderAction: OrderAction,
    defaultQuantity: BigDecimal
): TradeLeg {
    val transactionType = getTransactionType(record.position, InstrumentType.OPTN, orderAction)
    val retainPositionsQuantity =
        transactionType == TransactionType.SELL_CLOSE_OPT || transactionType == TransactionType.BUY_CLOSE_OPT
    val quantity =
        record.position.getTradeQuantity(retainPositionsQuantity, defaultQuantity)
    return object : TradeLeg {
        override val symbol: String = record.instrument.symbol
        override val transactionType: TransactionType = transactionType
        override val quantity: Int = quantity?.toInt() ?: 1
        override val displaySymbol: String = record.instrument.displaySymbol
        override val isAMOption: Boolean =
            false // TODO: Determine whether it's AM or PM expiring option from portfolio screen
    }
}

private fun createOptionStrategyObject(optionLegsForTrade: List<TradeLeg>): OptionTradeStrategy {
    return object : OptionTradeStrategy {
        override val strategyType: StrategyType =
            if (optionLegsForTrade.size == 1) {
                when (getOptionType(optionLegsForTrade[0].symbol)) {
                    OptionType.CALL -> StrategyType.CALL
                    OptionType.PUT -> StrategyType.PUT
                    else -> StrategyType.UNKNOWN
                }
            } else {
                // Incomplete strategy
                StrategyType.UNKNOWN
            }
        override val optionLegs: List<TradeLeg> = optionLegsForTrade
    }
}

internal fun TradeFormParametersBuilder.setActionAndQuantity(
    record: PortfolioRecord,
    instrumentType: InstrumentType,
    orderAction: OrderAction,
    defaultQuantity: BigDecimal
) {
    val isExchange = orderAction == OrderAction.EXCHANGE_ACTION

    action = if (isExchange) {
        TradeAction.EXCHANGE
    } else {
        getTransactionType(record.position, instrumentType, orderAction).getTradeAction()
    }

    quantity = record.position.getTradeQuantity(
        record.isTaxLot || isExchange || action == TradeAction.BUY_TO_COVER || action == TradeAction.SELL,
        defaultQuantity
    )
}

internal fun Position.getTradeQuantity(
    retainPositionsQuantity: Boolean,
    defaultQuantity: BigDecimal
) =
    if (retainPositionsQuantity) {
        displayQuantity
    } else {
        defaultQuantity
    }?.abs()

internal fun getTransactionType(
    position: Position,
    instrumentType: InstrumentType,
    orderAction: OrderAction
): TransactionType {
    return TransactionType.from(
        orderAction,
        if (orderAction.isBuy.xor(position.isShort)) OFFSET_OPEN else OFFSET_CLOSED,
        instrumentType.isOption
    )
}
