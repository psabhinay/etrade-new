package com.etrade.mobilepro.util

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

// Not thread safe
fun <T> mutableLazy(onValueChanged: ((T) -> Unit)? = null, initializer: () -> T) = MutableLazyDelegate(lazy(initializer), onValueChanged)

class MutableLazyDelegate<T>(
    private val lazy: Lazy<T>,
    private val onValueSet: ((T) -> Unit)?
) : ReadWriteProperty<Any, T> {
    private var value: T? = null
    override fun getValue(thisRef: Any, property: KProperty<*>): T = value ?: lazy.getValue(thisRef, property)

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        this.value = value
        onValueSet?.invoke(value)
    }
}
