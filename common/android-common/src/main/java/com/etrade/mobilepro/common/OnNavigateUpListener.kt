package com.etrade.mobilepro.common

interface OnNavigateUpListener {

    fun onNavigateUp(): Boolean
}
