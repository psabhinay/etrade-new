package com.etrade.mobilepro.dynamicui.dto.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class AccountOverviewRequestDto(
    @Json(name = "accountOverview")
    val accountOverview: AccountOverviewPayLoad
)

@JsonClass(generateAdapter = true)
class AccountOverviewPayLoad(
    @Json(name = "accountUuid")
    val accountUuid: String?,
    @Json(name = "widgets")
    val widgetList: List<String>
)
