package com.etrade.mobilepro.screener.data.result.column

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.stock.EarningsDividendsFilter
import com.etrade.mobilepro.screener.api.filter.stock.FundamentalsFilter
import com.etrade.mobilepro.screener.api.filter.stock.MarketSegmentFilter
import com.etrade.mobilepro.screener.api.filter.stock.OpinionsFilter
import com.etrade.mobilepro.screener.api.filter.stock.PriceVolumeFilter
import com.etrade.mobilepro.screener.api.filter.stock.TechnicalsFilter
import com.etrade.mobilepro.screener.data.result.ResultColumn
import com.etrade.mobilepro.screener.data.result.ResultColumnsResolver
import javax.inject.Inject

private val commonForAllStockResults = setOf(
    ResultColumn.LastPrice,
    ResultColumn.ChangeDollar,
    ResultColumn.ChangePercent,
    ResultColumn.Volume
)

class StockResultColumnsResolver @Inject constructor() : ResultColumnsResolver {

    override fun getColumns(filter: ScreenerFilter): Set<ResultColumn> = commonForAllStockResults +
        when (filter) {
            is MarketSegmentFilter -> getSpecificColumns(filter)
            is PriceVolumeFilter -> getSpecificColumns(filter)
            is OpinionsFilter -> emptySet()
            is FundamentalsFilter -> getSpecificColumns(filter)
            is EarningsDividendsFilter -> getSpecificColumns(filter)
            is TechnicalsFilter -> getSpecificColumns(filter)
            else -> throw IllegalArgumentException("$filter is wrong for stock result columns resolver")
        }

    private fun getSpecificColumns(filter: MarketSegmentFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.isCapSelected()) {
            result.add(ResultColumn.MarketCap)
        }
        return result
    }

    private fun getSpecificColumns(filter: PriceVolumeFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.isPricePerformanceSpValuesSelected()) {
            if (filter.pricePerformanceSpOver4Weeks) {
                result.add(ResultColumn.Price4WeeksSP500)
            }
            if (filter.pricePerformanceSpOver13Weeks) {
                result.add(ResultColumn.Price13WeeksSP500)
            }
            if (filter.pricePerformanceSpOver52Weeks) {
                result.add(ResultColumn.Price52WeeksSP500)
            }
        }
        if (filter.isPricePerformanceIndustryValuesSelected()) {
            if (filter.pricePerformanceIndustryOver4Weeks) {
                result.add(ResultColumn.Price4WeeksIndustry)
            }
            if (filter.pricePerformanceIndustryOver13Weeks) {
                result.add(ResultColumn.Price13WeeksIndustry)
            }
            if (filter.pricePerformanceIndustryOver52Weeks) {
                result.add(ResultColumn.Price52WeeksIndustry)
            }
        }
        return result
    }

    private fun getSpecificColumns(filter: FundamentalsFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.peRatioButtonsAreChecked()) {
            if (filter.peRatioOverTrailing12Months) {
                result.add(ResultColumn.PeRatioTrailing12)
            }
            if (filter.peRatioOverCurrentFiscalYear) {
                result.add(ResultColumn.PeRatioCurrentYear)
            }
            if (filter.peRatioOverNextFiscalYear) {
                result.add(ResultColumn.PeRatioNextYear)
            }
        }
        if (filter.isPriceBookRatioSelected()) {
            result.add(ResultColumn.PriceToBook)
        }
        return result
    }

    @Suppress("ComplexMethod", "LongMethod")
    private fun getSpecificColumns(filter: EarningsDividendsFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.isEpsGrowthAnnualValuesSelected()) {
            if (filter.epsGrowthAnnualForCurrentFiscalYear) {
                result.add(ResultColumn.EpsGrowthAnnualCurrent)
            }
            if (filter.epsGrowthAnnualForNextFiscalYear) {
                result.add(ResultColumn.EpsGrowthAnnualNext)
            }
        }
        if (filter.isRevenueGrowthAnnualValuesSelected()) {
            if (filter.revenueGrowthAnnualForCurrentFiscalYear) {
                result.add(ResultColumn.RevenueGrowthAnnualCurrent)
            }
            if (filter.revenueGrowthAnnualForNextFiscalYear) {
                result.add(ResultColumn.RevenueGrowthAnnualNext)
            }
        }
        if (filter.isEpsGrowthQuarterlyValuesSelected()) {
            result.add(ResultColumn.EpsGrowthQuarterly)
        }
        if (filter.isRevenueGrowthQuarterlyValuesSelected()) {
            result.add(ResultColumn.RevenueGrowthQuarterly)
        }
        if (filter.isDividendYieldValuesSelected()) {
            result.add(ResultColumn.DividendYield)
        }
        return result
    }

    private fun getSpecificColumns(filter: TechnicalsFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (!filter.stock52WeekHighNotAvailable) {
            result.add(ResultColumn.PriceChange52WeeksHigh)
        }
        if (!filter.stock52WeekLowNotAvailable) {
            result.add(ResultColumn.PriceChange52WeeksLow)
        }
        return result
    }
}
