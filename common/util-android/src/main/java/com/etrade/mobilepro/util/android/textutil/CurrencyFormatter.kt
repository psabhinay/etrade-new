package com.etrade.mobilepro.util.android.textutil

import java.math.BigDecimal
import java.text.DecimalFormat
import java.util.Locale

internal fun formatToCurrency(text: String, decimalDigits: Int): String {
    val currencyFormatter = (DecimalFormat.getCurrencyInstance(Locale.US) as DecimalFormat)
        .apply { minimumFractionDigits = decimalDigits }

    return text
        .keepDigitsOnly()
        .takeIf { it.isNotEmpty() }
        ?.let { addPaddingIfNecessary(it, decimalDigits) }
        ?.let(::StringBuilder)
        ?.let { placeDecimal(it, decimalDigits) }
        ?.toString()
        ?.let { BigDecimal(it) }
        ?.let(currencyFormatter::format)
        ?: text
}

private fun String.keepDigitsOnly() = replace("[^\\d]".toRegex(), "")

private fun addPaddingIfNecessary(text: String, padding: Int): String = if (text.length <= padding) {
    String.format("%${padding}s", text).replace(' ', '0')
} else {
    text
}

private fun placeDecimal(builder: StringBuilder, decimalDigits: Int): StringBuilder = builder.insert(builder.length - decimalDigits, '.')
