package com.etrade.mobilepro.accountvalues.completeview

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.account.dao.AccountDao
import com.etrade.mobilepro.accountvalues.COMPLETE_VIEW_SUMMARY_STREAM_ID
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito

class CompleteViewStreamingAccountValuesProviderTest {

    @Test
    fun `accountValues should return an observable with account values`() {

        // Given
        val mockObservable = mock<Observable<CalculationResult>>()
        val mockHelper = mock<SubscriptionManagerHelper<CalculationResult>> {
            on { subscribeTo(any(), any(), any()) } doReturn mockObservable
        }
        val sut = CompleteViewStreamingAccountValuesProvider(createMockDaoProvider(), mockHelper, mock())

        // When
        val result = sut.accountValues(COMPLETE_VIEW_SUMMARY_STREAM_ID, true)

        // Then
        assertEquals(result, mockObservable)
    }
}

private fun mockAccountDaoProvider(accountDao: AccountDao) = mock<AccountDaoProvider> {
    on { getAccountDao() } doReturn (accountDao)
}

private fun mockAccountDao(accounts: LiveData<List<Account>>) = mock<AccountDao> {
    on { getAllAccounts() } doReturn (accounts)
}

internal fun createMockDaoProvider(mockAccountsLiveData: LiveData<List<Account>>? = null): AccountDaoProvider {
    return mockAccountsLiveData?.let { mockAccountDaoProvider(mockAccountDao(it)) }
        ?: Mockito.mock(AccountDaoProvider::class.java)
}
