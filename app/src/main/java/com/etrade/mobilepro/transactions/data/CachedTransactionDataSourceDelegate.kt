package com.etrade.mobilepro.transactions.data

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.transactions.ErrorInfo
import com.etrade.mobilepro.transactions.TransactionDetailInfo
import com.etrade.mobilepro.transactions.TransactionsResponseInfo
import com.etrade.mobilepro.transactions.TransactionsServiceProvider
import com.etrade.mobilepro.transactions.api.CachedTransactionDetail
import com.etrade.mobilepro.transactions.api.CachedTransactions
import com.etrade.mobilepro.transactions.api.TransactionsRequest
import com.etrade.mobilepro.transactions.api.TransactionsResponse
import io.reactivex.Single
import io.realm.Realm
import io.realm.RealmList
import javax.inject.Inject

class CachedTransactionDataSourceDelegate @Inject constructor(
    private val transactionsServiceProvider: TransactionsServiceProvider
) : SimpleCachedResource.DataSourceDelegate<TransactionsResponse, TransactionsRequest> {

    override fun fetchFromNetwork(id: TransactionsRequest): Single<TransactionsResponse> {
        return transactionsServiceProvider.getTransactions(
            id.userId,
            id.accountId,
            id.institutionType,
            id.fromDate,
            id.toDate
        ).map {
            TransactionsResponse(
                queryValue = id.getCacheId(),
                transactionsResponseInfo = it,
                fetchedAt = System.currentTimeMillis()
            )
        }
    }

    override fun fetchFromDb(id: TransactionsRequest): Single<TransactionsResponse> {
        return Single.fromCallable {
            Realm.getDefaultInstance().use { realm ->
                val cachedTransactions =
                    realm.where(CachedTransactions::class.java)
                        .equalTo("queryValue", id.getCacheId())
                        .findFirst()?.also { realm.copyFromRealm(it) } ?: throw NoSuchElementException()
                TransactionsResponse(
                    cachedTransactions.queryValue,
                    object : TransactionsResponseInfo {
                        override val errors: List<ErrorInfo> = emptyList()
                        override val transactions: List<TransactionDetailInfo> = cachedTransactions.transactions.map {
                            it.toTransactionDetail()
                        }
                    },
                    cachedTransactions.savedAt
                )
            }
        }
    }

    override fun putToCache(fetchedResult: TransactionsResponse): Single<TransactionsResponse> {
        return Single.fromCallable {
            if (fetchedResult.transactionsResponseInfo.errors.isEmpty()) {
                Realm.getDefaultInstance().use { realm ->
                    realm.executeTransaction { realmTx ->
                        findOrCreateCachedTransactions(realm, fetchedResult).run {
                            savedAt = fetchedResult.fetchedAt
                            val realmList = RealmList<CachedTransactionDetail>()
                            realmList.addAll(
                                fetchedResult.transactionsResponseInfo.transactions.map {
                                    it.toCachedTransactionDetail(realm)
                                }
                            )
                            transactions = realmList
                            realmTx.insertOrUpdate(this)
                        }
                    }
                }
            }
            fetchedResult
        }
    }

    private fun findOrCreateCachedTransactions(
        realm: Realm,
        response: TransactionsResponse
    ) = realm.where(CachedTransactions::class.java)
        .equalTo("queryValue", response.queryValue).findFirst() ?: realm.createObject(CachedTransactions::class.java, response.queryValue)

    private fun TransactionDetailInfo.toCachedTransactionDetail(realm: Realm): CachedTransactionDetail {
        val cachedTransactionDetail = realm.createObject(CachedTransactionDetail::class.java)
        cachedTransactionDetail.date = date
        cachedTransactionDetail.type = type ?: ""
        cachedTransactionDetail.description = description ?: ""
        cachedTransactionDetail.amount = amount ?: ""
        return cachedTransactionDetail
    }

    private fun CachedTransactionDetail.toTransactionDetail(): TransactionDetailInfo =
        object : TransactionDetailInfo {
            override val amount: String? = this@toTransactionDetail.amount
            override val date: String = this@toTransactionDetail.date
            override val description: String? = this@toTransactionDetail.description
            override val type: String? = this@toTransactionDetail.type
        }
}
