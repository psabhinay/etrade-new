package com.etrade.mobilepro.dynamic.form.viewmodel

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.inputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.util.AppMessage
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.invoke
import com.etrade.mobilepro.viewmodel.AppMessageViewModel
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

abstract class FormViewModel(
    protected val resources: Resources,
    private val createFormUseCase: CreateFormUseCase
) : ViewModel(), AppMessageViewModel, OnActionClickListener {

    override val appMessageSignal: LiveData<AppMessage?>
        get() = _appMessage

    val loadingIndicator: LiveData<Boolean>
        get() = _loadingIndicator

    val peekFormValueSnapshot: InputValuesSnapshot
        get() = form.value?.inputValuesSnapshot.orEmpty()

    protected val isFormLoading: Boolean
        get() = _loadingIndicator.value == true

    private val _form: MutableLiveData<LiveData<Form>> = MutableLiveData()
    private val _loadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)
    private val _appMessage: MutableLiveData<AppMessage?> = MutableLiveData()

    val form: LiveData<Form> = _form.switchMap { it }

    private val logger = LoggerFactory.getLogger(javaClass)

    private var formInitialValues: InputValuesSnapshot? = null
    private var inputsDisabled: List<String> = emptyList()

    protected open val disableIfNoChange: Boolean = false

    final override fun dismissAppMessage() {
        _appMessage.value = null
    }

    fun fetch(initialValues: InputValuesSnapshot? = null, defaultValues: InputValuesSnapshot? = null, inputsDisabled: List<String>? = null) {
        if (form.value == null) {
            inputsDisabled?.let { this.inputsDisabled = it }
            refresh(initialValues.orEmpty(), defaultValues.orEmpty())
        }
    }

    fun refresh(initialValues: InputValuesSnapshot = peekFormValueSnapshot, defaultValues: InputValuesSnapshot = emptyMap()) {
        if (isFormLoading) {
            return
        }

        formInitialValues = initialValues

        viewModelScope.launch {
            _loadingIndicator.value = true
            createFormUseCase(createParameter(initialValues, defaultValues, disableIfNoChange, this@FormViewModel))
                .onSuccess { form ->
                    val formValue = form.value
                    formInitialValues?.let { formValue?.injectValuesFrom(addDefaultValuesIfNeeded(formValue, it)) }
                    formValue?.disabledFields(inputsDisabled)
                    formInitialValues = null
                    _form.value = form
                }
                .onFailure {
                    handleException(it) {
                        refresh(initialValues)
                    }
                }
            _loadingIndicator.value = false
        }
    }

    fun injectValues(values: InputValuesSnapshot) {
        if (isFormLoading) {
            formInitialValues = formInitialValues.orEmpty() + values
        }
        form.value?.injectValuesFrom(values)
    }

    fun resetFormValues() {
        formInitialValues = null
        form.value?.resetValues()
        form.value?.let { injectValues(addDefaultValuesIfNeeded(it, emptyMap())) }
    }

    protected abstract fun addDefaultValuesIfNeeded(
        form: Form,
        initialValues: InputValuesSnapshot
    ): InputValuesSnapshot

    protected abstract fun createParameter(
        initialValues: InputValuesSnapshot,
        defaultValues: InputValuesSnapshot,
        disableIfNoChange: Boolean,
        onActionClickListener: OnActionClickListener
    ): CreateFormUseCaseParameter

    protected fun handleException(t: Throwable, retryAction: (() -> Unit)? = null) {
        logger.error(t.message, t)
        errorMessage(
            ErrorMessage(resources.getString(R.string.error_message_general), retryAction)
        )
    }

    protected fun errorMessage(message: String) {
        errorMessage(ErrorMessage(message))
    }

    protected fun errorMessage(message: ErrorMessage) {
        _appMessage.value = message
    }
}
