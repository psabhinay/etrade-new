package com.etrade.mobilepro.pushnotification.presentation

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import androidx.appcompat.widget.LinearLayoutCompat.LayoutParams
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.pushnotification.presentation.databinding.PushnotificationFragmentOptInDialogBinding
import com.etrade.mobilepro.pushnotification.presentation.viewmodel.NotificationOptInViewModel
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import javax.inject.Inject

class NotificationOptInDialog @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory
) : DialogFragment() {

    private lateinit var binding: PushnotificationFragmentOptInDialogBinding
    private val viewModel: NotificationOptInViewModel by activityViewModels { viewModelFactory }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        isCancelable = false
        return MaterialAlertDialogBuilder(requireContext(), R.style.ThemeOverlay_Etrade_AlertDialog).apply {
            setTitle(R.string.stay_in_the_know)
            setMessage(R.string.push_notification_opt_in_message)
            setView(createCustomView(requireContext()))
            setPositiveButton(R.string.ok) { _, _ ->
                viewModel.setAlerts(
                    binding.stockAlertsCheckbox.isChecked,
                    binding.accountAlertsCheckbox.isChecked
                )
            }
        }.create().apply {
            setOnShowListener {
                // Make checkboxes view scrollable
                findViewById<FrameLayout>(R.id.customPanel)?.let {
                    it.layoutParams = LayoutParams(MATCH_PARENT, 0, 1f)
                }
            }
        }
    }

    private fun createCustomView(context: Context): View {
        binding = PushnotificationFragmentOptInDialogBinding.inflate(LayoutInflater.from(context))
        binding.let {
            it.stockAlertsCheckbox.isChecked = true
            it.accountAlertsCheckbox.isChecked = true
            it.alertsTermsAndConditions.setOnClickListener {
                showAlertTermsAndConditions()
            }
        }
        return binding.root
    }

    private fun showAlertTermsAndConditions() {
        activity?.let {
            startActivity(
                SpannableTextOverlayActivity.intent(
                    context = it,
                    title = getString(R.string.alert_terms_and_conditions),
                    text = getString(R.string.pushnotification_alert_terms_and_conditions_content),
                    negative = R.drawable.ic_arrow_back
                )
            )
        }
    }
}
