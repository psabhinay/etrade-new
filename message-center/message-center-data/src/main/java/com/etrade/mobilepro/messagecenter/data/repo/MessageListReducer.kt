package com.etrade.mobilepro.messagecenter.data.repo

import com.etrade.mobilepro.messagecenter.api.model.Message
import javax.inject.Inject

interface MessageListReducer {
    fun reduceMessageList(messages: List<Message>): List<Message>
}

class DefaultMessageListReducer @Inject constructor() : MessageListReducer {
    override fun reduceMessageList(messages: List<Message>): List<Message> =
        messages
            .fold(mutableListOf<Message>()) { accumulated, new ->
                val toReplace = accumulated.indexOfFirst { it.caseNo == new.caseNo }
                if (toReplace >= 0) {
                    replaceIfNewerOrSame(accumulated, toReplace, new)
                } else {
                    accumulated.add(new)
                }
                accumulated
            }
            .sortedByDescending { it.createdAt }
            .toList()

    private fun replaceIfNewerOrSame(list: MutableList<Message>, index: Int, candidate: Message) {
        if (list[index].createdAt?.isBefore(candidate.createdAt) == true || list[index].createdAt?.compareTo(candidate.createdAt) == 0) {
            list[index] = candidate
        }
    }
}

internal fun List<Message>?.reduceUsing(reducer: MessageListReducer): List<Message> = this?.let { reducer.reduceMessageList(it) } ?: emptyList()
