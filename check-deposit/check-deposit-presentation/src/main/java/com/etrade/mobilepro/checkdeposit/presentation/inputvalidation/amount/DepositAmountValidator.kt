package com.etrade.mobilepro.checkdeposit.presentation.inputvalidation.amount

import com.etrade.mobilepro.checkdeposit.presentation.R
import com.etrade.mobilepro.inputvalueview.InputFieldManager

private val validCurrency by lazy { """^0*([,0-9]*)?(\.\d*)?""".toRegex() }

internal class DepositAmountValidator(
    private val getString: (Int) -> String
) : InputFieldManager.Validator {
    override fun validate(value: String): InputFieldManager.Value {
        if (value.isBlank()) { return InputFieldManager.Value.Invalid(value, getString(R.string.check_deposit_amount_validator_invalid_currency_amount)) }

        return try {
            val sanitizedInput = value.removePrefix("$").replace(",", "")
            checkNotNull(validCurrency.matchEntire(sanitizedInput), { getString(R.string.check_deposit_amount_validator_invalid_currency_amount) })
                .let(::validate)
        } catch (e: IllegalStateException) {
            InputFieldManager.Value.Invalid(value, e.message ?: getString(R.string.check_deposit_amount_validator_invalid_currency_amount))
        }
    }

    private fun validate(match: MatchResult): InputFieldManager.Value {
        val (dollars, cents) = match.destructured
        val formattedDollars = dollars.trim().ifEmpty { "0" }
        val formattedCents = cents.replace(".", "").trim().ifEmpty { "00" }.let {
            when {
                it.length > 2 -> it.take(2)
                it.length == 1 -> it + "0"
                else -> it
            }
        }

        val formattedValue = "$formattedDollars.$formattedCents"
        return InputFieldManager.Value.Valid(formattedValue)
    }
}
