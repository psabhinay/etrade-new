package com.etrade.mobilepro.tableviewutils.presentation.holder

import android.view.View
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.TextView
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.toBondSymbolWithSubTitle
import com.etrade.mobilepro.instrument.toOptionsSymbolWithSubtitle
import com.etrade.mobilepro.tableviewutils.presentation.R
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

open class SymbolRowHeaderViewHolder(
    itemView: View,
    private val displaySymbolDescription: Boolean = false
) : AbstractViewHolder(itemView), HasSymbolCell {

    private val rowHeaderTextView: TextView = itemView.findViewById<View>(R.id.txtRowHeader) as TextView
    private val rowSubHeaderTextView: TextView? = itemView.findViewById<View>(R.id.txtRowSubHeader) as? TextView

    override var cell: SymbolCell? = null
        set(value) {
            field = value

            value?.let {
                when {
                    it.instrumentType == InstrumentType.BOND -> {
                        rowSubHeaderTextView?.text = it.description?.toBondSymbolWithSubTitle() ?: it.displayText
                        rowHeaderTextView.text = it.description
                    }
                    it.instrumentType == InstrumentType.INDX -> {
                        rowSubHeaderTextView?.text = it.displayText
                        rowHeaderTextView.text = it.symbol
                    }
                    it.instrumentType.isOption -> {
                        val titles = it.description?.toOptionsSymbolWithSubtitle() ?: Pair(it.displayText, null)
                        rowHeaderTextView.text = titles.first
                        rowSubHeaderTextView?.text = titles.second
                    }
                    else -> {
                        rowHeaderTextView.text = it.displayText

                        if (displaySymbolDescription && it.description?.isNotEmpty() == true) {
                            rowSubHeaderTextView?.text = it.description
                        } else {
                            rowSubHeaderTextView?.text = ""
                        }
                    }
                }
            }
        }

    init {
        itemView.setAccessibilityDelegate(object : View.AccessibilityDelegate() {
            override fun onInitializeAccessibilityNodeInfo(host: View?, info: AccessibilityNodeInfo?) {
                super.onInitializeAccessibilityNodeInfo(host, info)
                info?.addAction(AccessibilityNodeInfo.AccessibilityAction(AccessibilityNodeInfoCompat.ACTION_CLICK, ""))
                info?.removeAction(AccessibilityNodeInfo.AccessibilityAction.ACTION_LONG_CLICK)
                info?.isLongClickable = false
            }
        })
    }
}
