package com.etrade.mobilepro.trade.mutualfund.presentation

enum class MutualFundTradeType {
    SELL,
    EXCHANGE_SELL,
    EXCHANGE_BUY
}
