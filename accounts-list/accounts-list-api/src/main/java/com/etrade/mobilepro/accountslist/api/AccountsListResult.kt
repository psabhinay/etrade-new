package com.etrade.mobilepro.accountslist.api

import com.etrade.mobilepro.msaccounts.api.MsUserPreferences
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification

data class AccountsListResult(
    val msUserPreferences: MsUserPreferences,
    val personalNotifications: List<PersonalNotification>
)
