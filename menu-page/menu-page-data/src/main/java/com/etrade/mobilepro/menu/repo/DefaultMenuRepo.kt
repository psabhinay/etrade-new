package com.etrade.mobilepro.menu.repo

import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.common.di.DynamicMenuScreen
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.menu.mapper.mapToDynamicMenuItemListResult
import kotlinx.coroutines.rx2.awaitLast
import org.slf4j.LoggerFactory
import java.lang.Long.MAX_VALUE
import javax.inject.Inject

private const val MENU_CACHE_EXPIRATION_TIME = MAX_VALUE

class DefaultMenuRepo @Inject constructor(
    @DynamicMenuScreen private val screenDataSource: ScreenDataSource,
    @Web private val baseUrl: String
) : MenuRepo {

    private val logger = LoggerFactory.getLogger(DefaultMenuRepo::class.java)

    override suspend fun prefetchDynamicMenuItems() {
        runCatching {
            screenDataSource.getScreen(
                request = ServicePath.DynamicMenu,
                showCachedData = true,
                cacheExpiration = null
            ).awaitLast()
        }.onFailure {
            logger.error("Prefetch dynamic menu items failed", it)
        }
    }

    override suspend fun getDynamicMenuItems(): ETResult<List<DynamicMenuItem>> {
        return runCatchingET {
            screenDataSource.getScreen(
                request = ServicePath.DynamicMenu,
                showCachedData = true,
                cacheExpiration = MENU_CACHE_EXPIRATION_TIME
            ).awaitLast()
        }.mapToDynamicMenuItemListResult(baseUrl)
    }
}
