/*
 * Copyright (C) 2017 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.etrade.mobilepro.util

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.io.IOException
import java.lang.reflect.Type

/**
 * A JsonAdapter to handle the JsonDataException of expected BEGIN_ARRAY but got BEGIN_OBJECT
 * when parsing for a List.
 *
 * now when we define something like
 * class TestResponse {
 *     List<TestObject> testList;
 *   }
 *
 *   class TestObject {
 *     String value
 *   }
 *
 * we will parse { "testList": { "value": "dummy" } } as a list of size one instead of crashing.
 *
 * similar functionality to https://github.com/square/moshi/blob/master/examples/src/main/java/com/squareup/moshi/recipes/DefaultOnDataMismatchAdapter.java
 **/

class ObjectArrayMismatchAdapter<T> private constructor(private val delegate: JsonAdapter<List<T?>>, private val singleItemDelegate: JsonAdapter<T>) :
    JsonAdapter<List<T?>>() {
    @Throws(IOException::class)
    override fun fromJson(reader: JsonReader): List<T?>? {
        // Use a peeked reader to leave the reader in a known state even if there's an exception.
        val peeked = reader.peekJson()
        val result = try {
            // Attempt to decode to the target type List<T?>? with the peeked reader.
            delegate.fromJson(peeked)
        } catch (e: JsonDataException) {
            // if we fail, we try to use single item adapter to generate a list of size one.
            listOf(singleItemDelegate.fromJson(peeked))
        } finally {
            peeked.close()
        }
        // Skip the value back on the reader, no matter the state of the peeked reader.
        reader.skipValue()
        return result
    }

    @Throws(IOException::class)
    override fun toJson(writer: JsonWriter, value: List<T?>?) {
        delegate.toJson(writer, value)
    }

    companion object {

        fun <T> newFactory(type: Class<T>): Factory {
            return object : Factory {
                override fun create(
                    requestedType: Type,
                    annotations: Set<Annotation>,
                    moshi: Moshi
                ): JsonAdapter<*>? {
                    val parameterizedType = Types.newParameterizedType(List::class.java, type)
                    if (parameterizedType != requestedType) return null
                    val delegate = moshi.nextAdapter<List<T?>>(this, parameterizedType, annotations)
                    val singleItemDelegate = moshi.nextAdapter<T>(this, type, annotations)
                    return ObjectArrayMismatchAdapter(delegate, singleItemDelegate)
                }
            }
        }
    }
}
