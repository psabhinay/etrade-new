package com.etrade.mobilepro.billpay.data.rest.searchpayee

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SearchPayeeRequestBodyDto(
    @Json(name = "value")
    val content: SearchRequestDto
)

@JsonClass(generateAdapter = true)
data class SearchRequestDto(
    @Json(name = "payeeName")
    val payeeName: String,
    @Json(name = "sessionId")
    val sessionId: String
)
