package com.etrade.mobilepro.watchlist

import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.etrade.eo.corelibandroid.createViewModel
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.dropdown.DropDownManager
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingAction
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.etrade.mobilepro.watchlist.delegates.AddToWatchListDelegate
import com.etrade.mobilepro.watchlist.overview.viewmodel.AddSymbolToWatchlistEvent
import com.etrade.mobilepro.watchlist.overview.viewmodel.ViewEffects
import com.etrade.mobilepro.watchlist.overview.viewmodel.ViewState
import com.etrade.mobilepro.watchlist.overview.viewmodel.WatchlistOverviewViewModel
import com.etrade.mobilepro.watchlist.overview.viewmodel.watchlistDisplay
import com.etrade.mobilepro.watchlist.presentation.R
import com.etrade.mobilepro.watchlist.presentation.WL_DROPDOWN_TAG
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.google.android.material.snackbar.Snackbar
import java.lang.ref.WeakReference
import javax.inject.Inject

class AddToWatchListDelegateImpl @Inject constructor(
    private val watchlistOverviewViewModelFactory: WatchlistOverviewViewModel.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val userViewModel: UserViewModel,
    private val walkthroughStatesViewModel: WalkthroughStatesViewModel
) : AddToWatchListDelegate {
    private lateinit var watchListOverviewViewModel: WatchlistOverviewViewModel
    private lateinit var fragmentRef: WeakReference<Fragment>
    private var snackBarUtil: SnackBarUtil? = null
    private var loadingIndicator: ContentLoadingProgressBar? = null
    private var symbol: SearchResultItem.Symbol? = null
    private var dropDownManager: DropDownManager<Watchlist>? = null

    private var showDropDownAfterFetch: Boolean = false

    private var _newSymbolAddedToWatchList = MutableLiveData<ConsumableLiveEvent<Unit>>()
    override val newSymbolAddedToWatchList: LiveData<ConsumableLiveEvent<Unit>> = _newSymbolAddedToWatchList

    override fun init(fragment: Fragment, loadingIndicator: ContentLoadingProgressBar?) {
        fragmentRef = WeakReference(fragment)
        watchListOverviewViewModel = fragment.createViewModel { watchlistOverviewViewModelFactory.create(watchlistId = null) }.value
        this.loadingIndicator = loadingIndicator
        bindViewModel()
    }

    override fun showWatchList() {
        if (userViewModel.isAuthenticated) {
            if (dropDownManager == null && watchListOverviewViewModel.viewState.value != ViewState.FetchWatchlist.Loading) {
                showDropDownAfterFetch = true
                getWatchList()
            } else {
                dropDownManager?.openDropDown()
            }
        } else {
            addSelectedSymbolToWatchlist()
        }
    }

    override fun setSymbol(symbol: SearchResultItem.Symbol) {
        this.symbol = symbol
    }

    override fun clearUp() {
        this.loadingIndicator = null
        this.dropDownManager = null
    }

    override fun getWatchList() {
        watchListOverviewViewModel.getUserWatchlists()
    }

    private fun bindViewModel() {
        fragmentRef.reference { fragment ->
            snackBarUtil = snackbarUtilFactory.createSnackbarUtil({ fragment.viewLifecycleOwner }, { fragment.view })

            watchListOverviewViewModel.viewEffects.observe(
                fragment.viewLifecycleOwner,
                Observer {
                    handleViewEffects(it, fragment)
                }
            )

            watchListOverviewViewModel.addSymbolToWatchListEvent.observe(
                fragment.viewLifecycleOwner,
                Observer {
                    handleAddSymbolToWatchlistEvent(it)
                }
            )

            watchListOverviewViewModel.viewState.observe(
                fragment.viewLifecycleOwner,
                Observer { viewState ->
                    when (viewState) {
                        is ViewState.FetchWatchlist -> handleFetchWatchlistState(viewState)
                    }
                }
            )
        }
    }

    private fun handleViewEffects(viewEffects: ViewEffects?, fragment: Fragment) {
        when (viewEffects) {
            is ViewEffects.SnackBarMessage -> {
                snackBarUtil?.snackbar(viewEffects.message, Snackbar.LENGTH_LONG)?.show()
            }
            is ViewEffects.GenericError -> {
                snackBarUtil?.snackbar(
                    fragment.getString(R.string.error_message_general),
                    Snackbar.LENGTH_LONG
                )?.show()
            }
            is ViewEffects.AddedToWatchlist -> {
                showAddedToWatchlistSnackBar(viewEffects.symbol)
                _newSymbolAddedToWatchList.value = ConsumableLiveEvent(Unit)
            }
        }
    }

    private fun handleFetchWatchlistState(state: ViewState.FetchWatchlist) {
        when (state) {
            is ViewState.FetchWatchlist.Loading -> {
                loadingIndicator?.show()
                dropDownManager?.closeDropDown()
            }
            is ViewState.FetchWatchlist.Success -> {
                loadingIndicator?.hide()

                if (state.listOfWatchlist.isNotEmpty()) {
                    if (userViewModel.isAuthenticated) {
                        initDropDown(state.listOfWatchlist)
                    }
                }
            }
            is ViewState.FetchWatchlist.Error -> {
                loadingIndicator?.hide()
                dropDownManager?.closeDropDown()
            }
        }
    }

    private fun initDropDown(listOfWatchlists: List<Watchlist>) {
        fragmentRef.reference { fragment ->
            dropDownManager = BottomSheetSelector<Watchlist>(fragment.childFragmentManager, fragment.resources).apply {
                init(WL_DROPDOWN_TAG, fragment.getString(R.string.watchlist_title), listOfWatchlists)
                setListener { _, selectedWatchList ->
                    watchListOverviewViewModel.getWatchlistEntries(selectedWatchList)
                    addSelectedSymbolToWatchlist()
                }
            }
            if (showDropDownAfterFetch) {
                dropDownManager?.openDropDown()
                showDropDownAfterFetch = false
            }
        }
    }

    private fun addSelectedSymbolToWatchlist() {
        symbol?.also {
            watchListOverviewViewModel.addSymbolToWatchList(it)
        }
    }

    private fun handleAddSymbolToWatchlistEvent(event: AddSymbolToWatchlistEvent) {
        when (event) {
            is AddSymbolToWatchlistEvent.Loading -> {
                loadingIndicator?.show()
            }
            is AddSymbolToWatchlistEvent.Success -> {
                loadingIndicator?.hide()
                walkthroughStatesViewModel.onAction(SymbolTrackingAction.SymbolAddedToWatchlist)
            }
            is AddSymbolToWatchlistEvent.Error -> {
                loadingIndicator?.hide()
            }
        }
    }

    private fun <T> WeakReference<Fragment>.reference(block: (fragment: Fragment) -> T) {
        get()?.let { block.invoke(it) }
    }

    private fun showAddedToWatchlistSnackBar(symbol: SearchResultItem.Symbol) =
        fragmentRef.reference { fragment ->
            snackBarUtil?.snackbar(
                fragment.resources.getString(R.string.watchlist_symbol_added_message, symbol.watchlistDisplay),
                Snackbar.LENGTH_SHORT
            )?.show()
        }
}
