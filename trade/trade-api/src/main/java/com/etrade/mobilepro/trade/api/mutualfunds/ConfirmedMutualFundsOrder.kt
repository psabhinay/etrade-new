package com.etrade.mobilepro.trade.api.mutualfunds

import com.etrade.mobilepro.trade.api.ConfirmedOrder

interface ConfirmedMutualFundsOrder : MutualFundsOrder, ConfirmedOrder
