package com.etrade.mobilepro.usersession.api

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

interface UserSessionStateProvider {
    val userSessionState: LiveData<ConsumableLiveEvent<SessionStateChange>>
    fun observeLoginState(viewLifecycleOwner: LifecycleOwner, loginSignalHandler: LoginSignalHandler? = null, block: () -> Unit)
}
