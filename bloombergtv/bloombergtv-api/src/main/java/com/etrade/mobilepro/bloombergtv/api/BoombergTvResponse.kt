package com.etrade.mobilepro.bloombergtv.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BloombergTvResponse(
    @Json(name = "BloombergLiveData")
    val bloombergLiveData: BloombergLiveData,
    @Json(name = "_emptyscopedata")
    val emptyscopedata: Any?,
    @Json(name = "errMsgBuffer")
    val errMsgBuffer: ErrMsgBuffer
)

@JsonClass(generateAdapter = true)
data class BloombergLiveData(
    @Json(name = "navUserType")
    val navUserType: String = "",
    @Json(name = "base_url")
    val baseURL: String = "",
    @Json(name = "AuthToken")
    val authToken: String = "",
    @Json(name = "hasError")
    val hasError: String = "",
    @Json(name = "time_interval")
    val timeInterval: String = "",
    @Json(name = "url")
    val url: String = "",
    @Json(name = "errorDesc")
    val errorDescription: String = ""
)

@JsonClass(generateAdapter = true)
data class ErrMsgBuffer(
    @Json(name = "errMsg")
    val errMsg: String
)
