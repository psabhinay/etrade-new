package com.etrade.mobilepro.chart

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.chartengine.ChartRepo
import com.etrade.mobilepro.chartengine.ChartRequest
import com.etrade.mobilepro.chartengine.SymbolChartData
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import javax.inject.Inject

class DefaultChartRepo @Inject constructor(
    private val aggregationCacheController: SimpleCachedResource.Controller<List<SymbolChartData>>,
    private val aggregationCacheDataSourceDelegate: SimpleCachedResource.DataSourceDelegate<List<SymbolChartData>, List<ChartRequest>>,
    private val cacheController: SimpleCachedResource.Controller<SymbolChartData>,
    private val cacheDataSourceDelegate: SimpleCachedResource.DataSourceDelegate<SymbolChartData, ChartRequest>
) : ChartRepo {

    override fun getChart(chartRequest: ChartRequest): Observable<Resource<SymbolChartData>> =
        SimpleCachedResource(chartRequest, cacheDataSourceDelegate, cacheController).asObservable()

    override fun getAggregatedCharts(chartRequests: List<ChartRequest>): Observable<Resource<List<SymbolChartData>>> =
        SimpleCachedResource(chartRequests, aggregationCacheDataSourceDelegate, aggregationCacheController).asObservable()
}
