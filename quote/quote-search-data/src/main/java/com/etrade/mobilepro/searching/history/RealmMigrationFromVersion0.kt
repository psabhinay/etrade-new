package com.etrade.mobilepro.searching.history

import com.etrade.mobilepro.searching.symbol.cache.SymbolDb
import io.realm.DynamicRealm
import javax.inject.Inject

/**
 * Migration by this class is add the new field {@code settlementSessionCd} on table {@code SymbolDb}
 */
class RealmMigrationFromVersion0 @Inject constructor() : RealMigrationDelegate {
    override val oldVersion: Long = 0

    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        if (oldVersion == this.oldVersion) {
            realm.schema.get(SymbolDb::class.java.simpleName)
                ?.addField(SymbolDb::settlementSessionCd.name, Int::class.java)
        }
    }
}
