package com.etrade.mobilepro.watchlist.news

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.LiveData
import androidx.paging.compose.collectAsLazyPagingItems
import com.etrade.mobilepro.compose.components.EtRetrySnackbar
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.movers.MoverGridData
import com.etrade.mobilepro.movers.widget.MoversWidgetItem
import com.etrade.mobilepro.movers.widget.MoversWidgetViewModel
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.presentation.watchlist.WatchlistNewsViewModel
import com.etrade.mobilepro.news.widget.NewsWidgetItem
import com.etrade.mobilepro.news.widget.hasNoNews
import com.etrade.mobilepro.util.NetworkState
import com.etrade.mobilepro.watchlist.news.filter.WatchlistFilter
import com.etrade.mobilepro.watchlist.news.filter.WatchlistFilterViewModel
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import kotlinx.coroutines.ExperimentalCoroutinesApi

@Suppress("LongMethod", "LongParameterList")
@OptIn(ExperimentalCoroutinesApi::class)
@Composable
fun WatchlistNews(
    moversVM: MoversWidgetViewModel,
    newsVM: WatchlistNewsViewModel,
    filterVM: WatchlistFilterViewModel,
    filterTapListener: LiveData<() -> Unit>,
    onNewsTap: (NewsItem.Text) -> Unit,
    onSymbolTap: (String) -> Unit,
    onDisclosuresTap: () -> Unit,
    onRefresh: () -> Unit
) {
    val data: MoverGridData by moversVM.moverItemsFlow.collectAsState()
    val moversState = moversVM.viewState.observeAsState()
    val newsItems = newsVM.pagingDataFlow.collectAsLazyPagingItems()
    val currentWatchlist = filterVM.filterDataSource.selectedItem.observeAsState()
    val filterLoadingState = filterVM.filterDataSource.networkState.observeAsState()
    val onFilterTap = filterTapListener.observeAsState {}
    val moversTitle = stringResource(R.string.movers_widget_title)
    val ctx = LocalContext.current
    Column {
        if (filterLoadingState.value !is NetworkState.Failed) {
            WatchlistFilter(
                currentWatchlist.value?.name.orEmpty(),
                filterLoadingState.value == NetworkState.Loading,
                Modifier.fillMaxWidth(),
                onFilterTap.value
            )
        }
        SwipeRefresh(rememberSwipeRefreshState(false), onRefresh) {
            when (filterLoadingState.value) {
                NetworkState.Loading,
                NetworkState.Success,
                null -> LazyColumn(
                    modifier = Modifier.fillMaxSize(),
                    state = newsVM.newsListStateRetainer.rememberLazyListState()
                ) {
                    MoversWidgetItem(
                        title = moversTitle,
                        data = data,
                        modifier = Modifier.fillMaxWidth(),
                        state = moversState.value ?: DynamicUiViewState.Success,
                        onItemTap = { moversVM.onItemTapped(ctx, it) },
                        onRetry = { moversVM.getMovers() }
                    )
                    NewsWidgetItem(
                        itemsList = newsItems,
                        onNewsTap = { item, index ->
                            newsVM.newsItemSelected(item, index)
                            onNewsTap(item)
                        },
                        onSymbolTap = onSymbolTap,
                        onDisclosuresTap = onDisclosuresTap,
                        wasRead = newsVM::wasRead
                    )
                }
                is NetworkState.Failed -> Column {
                    Spacer(modifier = Modifier.fillMaxSize().weight(1f))
                    EtRetrySnackbar(action = onRefresh)
                }
            }
        }
    }
    newsVM.newsListStateRetainer.RestoreScrollStateWhen(
        data = newsItems,
        condition = remember { { lazyNewsItems -> lazyNewsItems.hasNoNews().not() } }
    )
}
