package com.etrade.mobilepro.markets.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.BaseApiTest
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

// TODO : Add Unit tests
internal class MarketsMoversApiTest : BaseApiTest() {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun `markets dollar top gainer api success call test`() {
    }

    @Test
    fun `markets dollar top loser api success call test`() {
    }

    @Test
    fun `markets pct top gainer api success call test`() {
    }

    @Test
    fun `markets pct top loser api success call test`() {
    }

    @Test
    fun `markets most active api success call test`() {
    }

    @Test
    fun `markets dollar top gainer network error call test`() {
    }

    @Test
    fun `markets dollar top gainer network wrong response call test`() {
    }
}
