package com.etrade.mobilepro.menu.viewmodel

import android.content.Context
import android.content.res.Resources
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.R
import com.etrade.mobilepro.R.string.menu_url_upload_documents
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dao.AccountListRepo
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.MessagePlace
import com.etrade.mobilepro.inboxmessages.MessageType
import com.etrade.mobilepro.inboxmessages.addOrRemoveInboxMessagesView
import com.etrade.mobilepro.inboxmessages.inboxMessagesDynamicViewLiveData
import com.etrade.mobilepro.inboxmessages.userType
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.menu.CustomerServiceMenuFragmentDirections
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menu.MessagesMenuItem
import com.etrade.mobilepro.menu.builder.menuPage
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.tracking.SCREEN_TITLE_CALL_US
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import com.etrade.mobilepro.util.android.extension.startDialerActivity
import javax.inject.Inject

class CustomerServiceMenuPageViewModel @Inject constructor(
    resources: Resources,
    tracker: Tracker,
    private val isLoggedIn: UserAuthenticationState,
    @Web private val baseUrl: String,
    imageLoader: ImageLoader,
    inboxMessagesRepository: InboxMessagesRepository,
    dispatchers: DispatcherProvider,
    accountListRepo: AccountListRepo,
    context: Context
) : AppBaseMenuPageViewModel(resources, tracker) {

    private val inboxMessages by lazy {
        inboxMessagesDynamicViewLiveData(
            baseUrl = baseUrl,
            imageLoader = imageLoader,
            type = MessageType.DYNAMIC_NAVIGATION,
            place = MessagePlace.CUSTOMER_SERVICE,
            userType = accountListRepo.getAccountList().userType(),
            coroutineContext = dispatchers.IO,
            repository = inboxMessagesRepository,
            context = context
        )
    }

    private val _menuPageItems: LiveData<List<MenuItem>> by lazy { MutableLiveData(getMenuPageItems()) }
    override val menuPageItems: LiveData<List<MenuItem>>
        get() = _menuPageItems.combineLatest(inboxMessages).map { (items, messages) ->
            if (items.isEmpty()) {
                items
            } else {
                items.addOrRemoveInboxMessagesView(
                    position = 0,
                    view = messages.view?.let { MessagesMenuItem(it) }
                )
            }
        }

    val showCallUsError: LiveData<ConsumableLiveEvent<Boolean>>
        get() = _showCallUsError
    private val _showCallUsError = MutableLiveData<ConsumableLiveEvent<Boolean>>()

    @SuppressWarnings("LongMethod")
    private fun getMenuPageItems(): List<MenuItem> {
        return menuPage {
            // Message Center
            requireLogin(isLoggedIn) {
                textWithArrowItem {
                    navDirections(CustomerServiceMenuFragmentDirections.actionCustomerServiceMenuToMessageCenter())
                    text(R.string.menu_message_center_title)
                }
            }

            // Call Us
            textWithArrowItem(R.id.customerServiceMenuCallUs) {
                text(R.string.menu_call_us_title)
                action { activity ->
                    activity.startDialerActivity(resources.getString(R.string.support_phone_number)) {
                        _showCallUsError.value = true.consumable()
                    }
                    trackScreen(SCREEN_TITLE_CALL_US)
                }
            }

            requireLogin(isLoggedIn) {
                // Live Chat
                textWithArrowItem {
                    navDirections(CustomerServiceMenuFragmentDirections.actionCustomerServiceMenuToChat())
                    text(R.string.menu_live_chat_title)
                }
                // Upload Documents
                textWithArrowItem(R.id.customerServiceMenuUploadDocuments) {
                    text(R.string.menu_upload_documents_title)
                    accountWebNav(url(menu_url_upload_documents))
                }
            }

            // Leave Feedback
            textWithArrowItem {
                navDirections(CustomerServiceMenuFragmentDirections.actionCustomerServiceMenuToFeedback())
                text(R.string.menu_leave_feedback_title)
            }
        }
    }

    private fun url(@StringRes id: Int) = resources.getString(id, baseUrl)
}
