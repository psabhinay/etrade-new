package com.etrade.mobilepro.trade.presentation.form.delegate

import android.content.Context
import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.dynamic.form.api.FormBuilder
import com.etrade.mobilepro.dynamic.form.api.GroupBuilder
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.form.api.SecurityType.OPTION
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder.StopLossFormInput
import com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder.StopLossFormParams
import com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder.hiddenAllOrNoneRule
import com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder.setHiddenInputValues
import com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder.stopLossFormHiddenInputAndLegDisplayText
import com.etrade.mobilepro.trade.presentation.form.delegate.advancedorder.stopLossOrderForm
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.resetPricesOnVisibilityChange
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.util.ValueCoder

internal class StopLossOptionTradeFormFactoryDelegate(
    private val context: Context,
    private val inputId: StopLossOptionTradeFormInputId,
    private val priceTypeDependenciesProvider: FormFactoryDelegateDependencies,
    tradeLegsCoder: ValueCoder<List<TradeLeg>>,
    tradeFormTypeUpdateDelegate: TradeFormTypeUpdateDelegate<OptionTradeFormInputId>,
    tradingDefaultsPreferences: TradingDefaultsPreferences
) : OptionTradeFormFactoryDelegate(
    context = context,
    dependencies = priceTypeDependenciesProvider,
    inputId = inputId,
    securityType = OPTION,
    tradeFormTypeUpdateDelegate = tradeFormTypeUpdateDelegate,
    tradeLegsCoder = tradeLegsCoder,
    tradingDefaultsPreferences = tradingDefaultsPreferences
) {

    override val formType: TradeFormType = TradeFormType(OPTION, AdvancedTradeType.StopLoss)

    // needed to construct request params
    override fun hiddenInput(formBuilder: FormBuilder) {
        super.hiddenInput(formBuilder)
        formBuilder.hidden(inputId.action)
        formBuilder.hidden(inputId.symbol)
        formBuilder.hidden(inputId.quantity)

        inputId.stopLossInputId.apply {
            formBuilder.hidden(action)
            formBuilder.hidden(quantity)
            formBuilder.hidden(symbol)
            formBuilder.hidden(allOrNone)
        }
    }

    override fun advancedOrderFormInputs(groupBuilder: GroupBuilder, parameter: CreateTradeFormParameter) {
        groupBuilder.stopLossOrderForm(
            advancedFormInputId = inputId,
            params = StopLossFormParams(
                dependencies = priceTypeDependenciesProvider,
                resources = context.resources,
                parameter = parameter,
                primaryInputId = inputId,
                securityType = OPTION,
                stopLossInputId = inputId.stopLossInputId
            )
        ) {
            setOptionsHiddenInputAndLegDisplayText(context.resources, inputId)
        }
    }

    override fun advancedOrderRules(ruleBuilder: RuleListBuilder, updateForm: TradeFormFactoryDelegate.OnFormChangeListener) {
        super.advancedOrderRules(ruleBuilder, updateForm)
        ruleBuilder.apply {
            resetPricesOnVisibilityChange(inputId.stopLossInputId, includeLimitPrice = false)
            hiddenAllOrNoneRule(inputId, inputId.stopLossInputId)
        }
    }

    private fun GroupBuilder.setOptionsHiddenInputAndLegDisplayText(resources: Resources, inputId: StopLossOptionTradeFormInputId) {

        val tradeLeg = getTradeLegLiveData(inputId)

        dynamicText {
            title = resources.getString(R.string.order)
            displayValue {
                tradeLeg.map {
                    val qty: String? = it?.quantity?.toString()
                    val symbol = object : WithSymbolInfo {
                        override val instrumentType: InstrumentType = InstrumentType.OPTN
                        override val symbol: String = it?.symbol ?: ""
                    }

                    val action = it.toPrimaryStopLossAction(inputId.actionValueId)
                    setHiddenInputValues(inputId, action, StopLossFormInput(inputId.quantity, qty), symbol)

                    val transactionParams = inputId.stopLossInputId.actionValueId.toOptionsStopLossAction(it?.transactionType)
                    stopLossFormHiddenInputAndLegDisplayText(inputId.stopLossInputId, qty, resources, symbol, transactionParams)
                }
            }

            visibleIf {
                inputValue<List<TradeLeg>>(inputId.tradeLegs).map {
                    it?.size == 1
                }
            }
        }
    }

    private fun GroupBuilder.getTradeLegLiveData(primaryInputId: OptionTradeFormInputId): LiveData<TradeLeg?> {

        return inputValue<List<TradeLeg>>(primaryInputId.tradeLegs).map { it?.firstOrNull() }
    }

    // OPTIONS: Exit Strategy : Stop Loss is opposite type of primary action
    // Buy open to Sell close
    // Sell open -> Buy close
    private fun ActionValueId.toOptionsStopLossAction(action: TransactionType?): StopLossFormInput<TransactionType> = when (action) {
        TransactionType.BUY_OPEN_OPT -> StopLossFormInput(sell, TransactionType.SELL_CLOSE_OPT)
        TransactionType.SELL_OPEN_OPT -> StopLossFormInput(buyToCover, TransactionType.BUY_CLOSE_OPT)
        else -> StopLossFormInput(sell, TransactionType.SELL_CLOSE_OPT)
    }

    private fun TradeLeg?.toPrimaryStopLossAction(actionValueId: ActionValueId): String = when (this?.transactionType) {
        TransactionType.BUY_OPEN_OPT -> actionValueId.buy
        TransactionType.SELL_OPEN_OPT -> actionValueId.sellShort
        else -> actionValueId.buy
    }
}
