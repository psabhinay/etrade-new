package com.etrade.mobilepro.login.widget

import android.text.InputType
import com.etrade.mobilepro.edittextdialog.EditTextDialogFragment

private const val PASSWORD_UPDATE_DIALOG_TAG = "PASSWORD_UPDATE_DIALOG_TAG"

fun buildPasswordUpdateDialog(positiveListener: ((String) -> Unit)): EditTextDialogFragment.Builder {
    return EditTextDialogFragment.Builder(PASSWORD_UPDATE_DIALOG_TAG).apply {
        hint(R.string.password)
        title(R.string.update_your_password)
        message(R.string.you_password_has_changed)
        positiveButton(listener = positiveListener)
        setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
    }
}
