package com.etrade.mobilepro.quoteapi

import com.etrade.mobilepro.instrument.InstrumentType

data class FundClassInfo(
    val symbol: String? = null,
    val exchangeCode: String? = null,
    val typeCode: InstrumentType,
    val fundClass: String? = null
) {
    override fun toString(): String {
        return fundClass ?: ""
    }
}
