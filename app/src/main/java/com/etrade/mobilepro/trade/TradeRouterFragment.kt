package com.etrade.mobilepro.trade

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navOptions
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.home.TRADE_PATH
import com.etrade.mobilepro.util.android.logAppAction
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

const val START_DESTINATION = "startDestination"

const val TRADE_FRAGMENT = "tradeFragment"
const val TRADE_ORDER_DETAILS_FRAGMENT = "tradeOrderDetailsFragment"
const val TRADE_OPTION_LEG_SELECT_FRAGMENT = "tradeOptionLegSelectFragment"

private val logger: Logger = LoggerFactory.getLogger(TradeRouterFragment::class.java)

@RequireLogin
class TradeRouterFragment @Inject constructor() : Fragment() {

    private val args: TradeRouterFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        logAppAction(activity?.intent, TRADE_PATH)
        when (args.startDestination) {
            TRADE_FRAGMENT -> R.id.action_tradeRouterFragment_to_tradeFragment
            TRADE_ORDER_DETAILS_FRAGMENT -> R.id.action_tradeRouterFragment_to_tradeOrderDetailsFragment
            TRADE_OPTION_LEG_SELECT_FRAGMENT -> R.id.action_tradeRouterFragment_to_optionLegSelectFragment
            else -> return
        }.let { routeToStartDestination(it) }
    }

    private fun routeToStartDestination(destinationId: Int) {
        try {
            findNavController().navigate(
                destinationId,
                arguments,
                navOptions {
                    popUpTo(R.id.trade_navigation_graph) {
                        inclusive = true
                    }
                }
            )
        } catch (ex: IllegalArgumentException) {
            logger.error("failed to find destination", ex)
        }
    }
}
