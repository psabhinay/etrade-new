package com.etrade.mobilepro.trade.api

interface WithHardToBorrowOrder {
    val estDailyBorrowInterest: String?
    val borrowRate: String?
}
