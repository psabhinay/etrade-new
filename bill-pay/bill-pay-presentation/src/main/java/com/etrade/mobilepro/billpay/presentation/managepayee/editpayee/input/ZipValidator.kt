package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.inputvalueview.InputFieldManager.Value

internal class ZipValidator(
    private val zeroErrorDesc: String
) : InputFieldManager.Validator {
    override fun validate(value: String): Value {
        return if (value == "00000") {
            Value.Invalid(value, zeroErrorDesc)
        } else {
            Value.Valid(value)
        }
    }
}
