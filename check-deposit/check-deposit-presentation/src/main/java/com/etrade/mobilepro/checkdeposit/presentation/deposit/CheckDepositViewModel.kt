package com.etrade.mobilepro.checkdeposit.presentation.deposit

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.checkdeposit.presentation.R
import com.etrade.mobilepro.checkdeposit.presentation.deposit.CheckCaptureViewState.CheckCapture
import com.etrade.mobilepro.checkdeposit.usecases.api.CHECK_BACK
import com.etrade.mobilepro.checkdeposit.usecases.api.CHECK_FRONT
import com.etrade.mobilepro.checkdeposit.usecases.api.DepositImageUseCase
import com.etrade.mobilepro.checkdeposit.usecases.api.GetCheckDepositAccountsUseCase
import com.etrade.mobilepro.checkdeposit.usecases.api.UploadCheckImage
import com.etrade.mobilepro.checkdeposit.usecases.api.UploadCheckImageUseCase
import com.etrade.mobilepro.checkdeposit.usecases.deposit.CheckDepositResult
import com.etrade.mobilepro.checkdeposit.usecases.deposit.DepositImageRequest
import com.etrade.mobilepro.checkdeposit.usecases.validation.ValidateDepositBusinessRulesRequest
import com.etrade.mobilepro.checkdeposit.usecases.validation.ValidateDepositBusinessRulesUseCase
import com.etrade.mobilepro.checkdeposit.usecases.validation.ValidationResult
import com.etrade.mobilepro.common.Restorable
import com.etrade.mobilepro.common.RestorableViewModelDelegate
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.livedata.concatCombine
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.tracking.SCREEN_TITLE_CHECK_DEPOSIT
import com.etrade.mobilepro.tracking.SCREEN_TITLE_CHECK_DEPOSIT_ACTIVITY
import com.etrade.mobilepro.tracking.TabsHostScreenViewModel
import com.etrade.mobilepro.tracking.ViewPagerTabsTrackerHelper
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.invoke
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(CheckDepositViewModel::class.java)
private const val DEFAULT_WORK_FLOW_ID = ""

const val CACHE_PREFIX = "CheckDep_"

@SuppressWarnings("LargeClass") // Complicated UI with many states...
class CheckDepositViewModel @Inject constructor(
    private val loadEligibleAccountsUseCase: GetCheckDepositAccountsUseCase,
    private val uploadCheckImageUseCase: UploadCheckImageUseCase,
    private val validateDepositBusinessRulesUseCase: ValidateDepositBusinessRulesUseCase,
    private val depositImageUseCase: DepositImageUseCase,
    private val resources: Resources,
    override val viewPagerTabsTrackerHelper: ViewPagerTabsTrackerHelper,
    private val restorableDelegate: RestorableViewModelDelegate
) : ViewModel(), TabsHostScreenViewModel {

    override val tabNames: Array<String> = arrayOf(SCREEN_TITLE_CHECK_DEPOSIT, SCREEN_TITLE_CHECK_DEPOSIT_ACTIVITY)

    private var checkImageSaved = false

    val rawDepositAmount = MutableLiveData<String?>()
    private val validatedDepositAmount = MutableLiveData<InputFieldManager.Value?>()
    private val depositAmount = validatedDepositAmount.map {
        when (it) {
            is InputFieldManager.Value.Valid -> { it.content }
            else -> null
        }
    }

    private val _restoreCheckDepositAmountSignal: MutableLiveData<ConsumableLiveEvent<String>> = MutableLiveData()
    val restoreCheckDepositAmountSignal: MutableLiveData<ConsumableLiveEvent<String>>
        get() = _restoreCheckDepositAmountSignal

    private val _termsSigned: MutableLiveData<Boolean> = MutableLiveData()
    val termsSigned: LiveData<Boolean>
        get() = _termsSigned

    private val _selectedAccount = MutableLiveData<FundsFlowAccount?>()
    val selectedAccount: LiveData<FundsFlowAccount?>
        get() = _selectedAccount

    private val _depositDetailsViewState = MediatorLiveData<DepositDetailsViewState>().apply {
        addSource(_selectedAccount.distinctUntilChanged()) { selectedAccount ->
            workFlowId = DEFAULT_WORK_FLOW_ID

            val depositAmount = (restorableDelegate.getRestoredState(this@CheckDepositViewModel::class.java) as? ViewState)?.depositDetails?.depositAmount
            value = DepositDetailsViewState.validate(depositAmount, selectedAccount, null)
        }

        addSource(depositAmount) { amount ->
            value = DepositDetailsViewState.validate(amount, _selectedAccount.value, value?.iraDepositType)
        }
    }
    val depositDetailsViewState: LiveData<DepositDetailsViewState>
        get() = _depositDetailsViewState

    private val _depositBusinessRulesValidation: LiveData<ValidateDepositBusinessRulesViewState?> = _depositDetailsViewState.distinctUntilChanged().switchMap {
        liveData(context = viewModelScope.coroutineContext) {
            if (it is DepositDetailsViewState.ReadyForCapture) {
                validateDepositBusinessRulesUseCase(
                    ValidateDepositBusinessRulesRequest(
                        instNo = it.selectedAccount.instNo.toString(),
                        accountNo = it.selectedAccount.accountId,
                        depositAmount = it.depositAmount,
                        iraDepositType = it.iraDepositType
                    )
                ).fold(
                    onSuccess = { result ->
                        emit(
                            when (result) {
                                is ValidationResult.Valid -> ValidateDepositBusinessRulesViewState.Valid
                                is ValidationResult.Invalid -> ValidateDepositBusinessRulesViewState.Invalid(
                                    result.message ?: resources.getString(R.string.error_message_general)
                                )
                            }
                        )
                    },
                    onFailure = { emit(ValidateDepositBusinessRulesViewState.Error(resources.getString(R.string.error_message_general))) }
                )
            } else {
                emit(null)
            }
        }
    }

    private val _depositEligibleAccounts = MutableLiveData<List<FundsFlowAccount>>()
    private val _checkCaptureViewState = MutableLiveData<CheckCaptureViewState>()
    private val _viewState = MutableLiveData<ViewState>()
    private val _depositImageViewState = MediatorLiveData<DepositImageViewState>().apply {
        addSource(_depositDetailsViewState.distinctUntilChanged()) {
            value = null
        }
        addSource(_checkCaptureViewState.distinctUntilChanged()) {
            value = null
        }
    }

    val viewState = MediatorLiveData<ViewState>().apply {
        @SuppressWarnings("ComplexMethod")
        fun merge(
            depositImageState: DepositImageViewState?,
            validationState: ValidateDepositBusinessRulesViewState?,
            detailsState: DepositDetailsViewState?,
            checkCaptureState: CheckCaptureViewState?
        ) {
            value = when (detailsState) {
                is DepositDetailsViewState.InProgress -> ViewState.DetailsNeeded(detailsState, validationState)
                is DepositDetailsViewState.ReadyForCapture -> getViewStateWhenReadyForCapture(
                    checkCaptureState,
                    detailsState,
                    validationState,
                    depositImageState
                )
                else -> ViewState.DetailsNeeded()
            }
        }

        value = restorableDelegate.getRestoredState(this@CheckDepositViewModel::class.java) as? ViewState ?: ViewState.Loading

        addSource(_viewState) { value = it }

        addSource(_depositDetailsViewState) {
            if (it?.depositAmount == null) {
                rawDepositAmount.value = null
            }

            merge(_depositImageViewState.value, _depositBusinessRulesValidation.value, it, _checkCaptureViewState.value)
        }

        addSource(_checkCaptureViewState) {
            merge(_depositImageViewState.value, _depositBusinessRulesValidation.value, _depositDetailsViewState.value, it)
        }

        addSource(_depositBusinessRulesValidation) {
            when (it) {
                is ValidateDepositBusinessRulesViewState.Valid -> merge(
                    _depositImageViewState.value,
                    it,
                    _depositDetailsViewState.value,
                    _checkCaptureViewState.value
                )
                is ValidateDepositBusinessRulesViewState.Invalid, is ValidateDepositBusinessRulesViewState.Error ->
                    _depositDetailsViewState.value =
                        DepositDetailsViewState.InProgress(
                            selectedAccount = _depositDetailsViewState.value?.selectedAccount,
                            iraDepositType = _depositDetailsViewState.value?.iraDepositType
                        )
            }
        }

        addSource(_depositImageViewState) {
            if (it is DepositImageViewState.Invalid) {
                _checkCaptureViewState.value = when {
                    it.frontImageFieldToFlag -> CheckCaptureViewState.InProgress(
                        CheckCapture.Failure(),
                        _checkCaptureViewState.value?.checkBack
                    )
                    it.rearImageFieldToFlag -> CheckCaptureViewState.InProgress(
                        _checkCaptureViewState.value?.checkFront,
                        CheckCapture.Failure()
                    )
                    else -> CheckCaptureViewState.InProgress()
                }
            } else {
                merge(it, _depositBusinessRulesValidation.value, _depositDetailsViewState.value, _checkCaptureViewState.value)
            }
        }
    }

    init {
        restorableDelegate.init(this::class.java, viewState, ::restoreState)
        _viewState.value = restorableDelegate.getRestoredState(this::class.java) as? ViewState ?: ViewState.Loading
    }

    private fun getViewStateWhenReadyForCapture(
        checkCaptureState: CheckCaptureViewState?,
        detailsState: DepositDetailsViewState.ReadyForCapture,
        validationState: ValidateDepositBusinessRulesViewState?,
        depositImageState: DepositImageViewState?
    ): ViewState {
        return when {
            checkCaptureState is CheckCaptureViewState.InProgress -> ViewState.CaptureInProgress(detailsState, checkCaptureState, depositImageState)
            checkCaptureState is CheckCaptureViewState.Completed && validationState != null -> getViewStateWhenCaptureCompleted(
                depositImageState,
                detailsState,
                checkCaptureState
            )
            else -> ViewState.CaptureInProgress(
                detailsState,
                CheckCaptureViewState.InProgress(checkCaptureState?.checkFront, checkCaptureState?.checkBack)
            )
        }
    }

    private fun getViewStateWhenCaptureCompleted(
        depositImageState: DepositImageViewState?,
        detailsState: DepositDetailsViewState.ReadyForCapture,
        checkCaptureState: CheckCaptureViewState.Completed
    ): ViewState {
        return when (depositImageState) {
            is DepositImageViewState.Loading -> ViewState.Loading
            is DepositImageViewState.Valid -> ViewState.DepositSubmitted(depositImageState)
            is DepositImageViewState.Error, is DepositImageViewState.Invalid -> ViewState.ReadyForSubmission(detailsState, checkCaptureState, depositImageState)
            else -> ViewState.ReadyForSubmission(detailsState, checkCaptureState)
        }
    }

    val accountsDropdownSelectables: LiveData<List<DropDownMultiSelector.Selectable<FundsFlowAccount>>> =
        _depositEligibleAccounts.concatCombine(_depositDetailsViewState) { accounts, state ->
            accounts.map { account ->
                DropDownMultiSelector.Selectable(
                    title = account.displayString(),
                    value = account,
                    selected = state.selectedAccount?.accountId == account.accountId
                )
            }
        }

    val accounts: LiveData<List<FundsFlowAccount>>
        get() = _depositEligibleAccounts

    private var workFlowId: String = DEFAULT_WORK_FLOW_ID

    fun selectAccount(account: FundsFlowAccount?) {
        if (_selectedAccount.value?.accountId != account?.accountId) {
            _depositDetailsViewState.updateValue {
                DepositDetailsViewState.validate(
                    depositAmount = null,
                    selectedAccount = it.selectedAccount,
                    selectedContributionType = it.iraDepositType
                )
            }
            validatedDepositAmount.value = null

            if (!checkImageSaved) {
                _checkCaptureViewState.value = CheckCaptureViewState.InProgress()
            }
        }

        _selectedAccount.value = account
    }

    fun selectIraContributionType(contributionType: String?) {
        _depositDetailsViewState.updateValue {
            DepositDetailsViewState.validate(
                depositAmount = it.depositAmount,
                selectedAccount = it.selectedAccount,
                selectedContributionType = contributionType
            )
        }
    }

    fun grabAccounts(bankAccountId: String?) {
        if (_depositEligibleAccounts.value != null) { return }
        _viewState.value = ViewState.Loading
        _depositDetailsViewState.value = _depositDetailsViewState.value?.invalidate()
        viewModelScope.launch {
            loadEligibleAccountsUseCase().fold(
                onSuccess = {
                    _termsSigned.value = it.termsSigned
                    _depositEligibleAccounts.value = it.eligibleAccounts

                    val bankAccount = it.eligibleAccounts.find { account -> account.accountId == bankAccountId }
                        ?: it.eligibleAccounts.firstOrNull()
                    if (_selectedAccount.value == null) { selectAccount(bankAccount) }
                },
                onFailure = {
                    _viewState.value = ViewState.AccountFetchFailed
                }
            )
        }
    }

    @Suppress("LongMethod")
    fun captureCheckImage(@UploadCheckImage.CheckSide checkSide: Int, imageBytes: ByteArray) {
        viewModelScope.launch {
            val depositDetails = viewState.value?.depositDetails
            if (depositDetails is DepositDetailsViewState.ReadyForCapture) {
                _checkCaptureViewState.apply {
                    value = (value ?: CheckCaptureViewState.InProgress()).updateCheckSide(checkSide, CheckCapture.Uploading)
                }

                val uploadRequest = depositDetails.selectedAccount.toImageUploadRequest(
                    amount = depositDetails.depositAmount,
                    checkSide = checkSide,
                    checkImage = imageBytes
                )
                uploadCheckImageUseCase(uploadRequest).fold(
                    onSuccess = {
                        _checkCaptureViewState.apply {
                            value = (value ?: CheckCaptureViewState.InProgress()).updateCheckSide(checkSide, CheckCapture.Success(it))
                        }
                    },
                    onFailure = {
                        _checkCaptureViewState.apply {
                            value = (value ?: CheckCaptureViewState.InProgress()).updateCheckSide(
                                checkSide,
                                CheckCapture.Failure(
                                    errorMessage = resources.getString(R.string.upload_failed)
                                )
                            )
                        }
                    }
                )
            }
        }
    }

    @SuppressWarnings("LongMethod")
    fun submitDeposit() {
        viewModelScope.launch {
            val viewState = viewState.value

            if (viewState is ViewState.ReadyForSubmission) {
                _depositImageViewState.value = DepositImageViewState.Loading

                val detailsState = viewState.depositDetails
                val captureState = viewState.checkCapture

                depositImageUseCase(
                    DepositImageRequest(
                        amount = detailsState.depositAmount,
                        accountNo = detailsState.selectedAccount.accountId,
                        instNo = detailsState.selectedAccount.instNo.toString(),
                        workflowId = workFlowId,
                        frontImageToken = captureState.checkFront.result.tokenId,
                        rearImageToken = captureState.checkBack.result.tokenId,
                        iraDepositType = detailsState.iraDepositType
                    )
                ).fold(
                    onSuccess = { result ->
                        when (result) {
                            is CheckDepositResult.Valid -> {
                                _depositImageViewState.value = DepositImageViewState.Valid(result)
                            }
                            is CheckDepositResult.Invalid -> {
                                _depositImageViewState.value = DepositImageViewState.Invalid(
                                    message = result.message ?: resources.getString(R.string.error_message_general),
                                    frontImageFieldToFlag = result.frontImageFieldToFlag,
                                    rearImageFieldToFlag = result.rearImageFieldToFlag,
                                    amountFieldToFlag = result.amountFieldToFlag
                                )
                            }
                        }
                    },
                    onFailure = {
                        logger.error("Check deposit failed", it)
                        _depositImageViewState.value =
                            DepositImageViewState.Error(resources.getString(R.string.error_message_general))
                    }
                )
            }
        }
    }

    private val _checkScanClicks = MutableLiveData<ConsumableLiveEvent<String>>()
    val checkScanClicks: LiveData<ConsumableLiveEvent<String>> get() = _checkScanClicks

    fun onCheckScanClicked(docType: String) {
        _checkScanClicks.value = ConsumableLiveEvent(docType)
    }

    fun resetDetails() {
        workFlowId = DEFAULT_WORK_FLOW_ID
        _depositDetailsViewState.value =
            DepositDetailsViewState.InProgress(selectedAccount = _selectedAccount.value)
        _checkCaptureViewState.value = CheckCaptureViewState.InProgress()
    }

    fun onAmountEntered(amount: BigDecimal) {
        if (amount > BigDecimal.ZERO) {
            validatedDepositAmount.value = InputFieldManager.Value.Valid(amount.toPlainString())
        }
    }

    fun disposeState() {
        restorableDelegate.dispose(this::class.java)
    }

    private fun restoreState(restoredState: Restorable) {
        (restoredState as? ViewState)?.apply {
            _viewState.value = this

            checkCapture?.checkFront?.restoreCheckImage(CHECK_FRONT)
            checkCapture?.checkBack?.restoreCheckImage(CHECK_BACK)

            depositDetails?.selectedAccount?.let {
                selectAccount(it)
                checkImageSaved = false
            }

            depositDetails?.depositAmount?.let {
                _restoreCheckDepositAmountSignal.value = ConsumableLiveEvent(it)
            }
        }
    }

    private fun CheckCapture.restoreCheckImage(checkSide: Int) {
        (this as? CheckCapture.Success)?.let {
            checkImageSaved = true

            _checkCaptureViewState.apply {
                value = (value ?: CheckCaptureViewState.InProgress()).updateCheckSide(checkSide, it)
            }
        }
    }
}
