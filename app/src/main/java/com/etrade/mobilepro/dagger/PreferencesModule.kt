package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.preferences.ResetUserPreferencesUseCase
import com.etrade.mobilepro.preferences.ResetUserPreferencesUseCaseImp
import com.etrade.mobilepro.themeselection.NightModePreferences
import com.etrade.mobilepro.themeselection.NightModePreferencesImpl
import dagger.Binds
import dagger.Module

@Module
abstract class PreferencesModule {
    @Binds
    abstract fun bindResetUserPreferencesUseCase(useCase: ResetUserPreferencesUseCaseImp): ResetUserPreferencesUseCase

    @Binds
    abstract fun bindNightModePreferences(nightModePreferencesImpl: NightModePreferencesImpl): NightModePreferences
}
