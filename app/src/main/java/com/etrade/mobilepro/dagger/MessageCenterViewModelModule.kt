package com.etrade.mobilepro.dagger

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.messagecenter.presentation.details.service.ServiceMessageDetailsViewModel
import com.etrade.mobilepro.messagecenter.presentation.details.support.SupportMessageDetailsViewModel
import com.etrade.mobilepro.messagecenter.presentation.messagelist.MessageCenterListViewModel
import com.etrade.mobilepro.messagecenter.presentation.newmessage.NewMessageViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface MessageCenterViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MessageCenterListViewModel::class)
    fun bindMessageCenterListViewModel(viewModel: MessageCenterListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ServiceMessageDetailsViewModel::class)
    fun bindServiceMessageDetailViewModel(viewModel: ServiceMessageDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SupportMessageDetailsViewModel::class)
    fun bindSupportMessageDetailsViewModel(viewModel: SupportMessageDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewMessageViewModel::class)
    fun bindNewMessageViewModel(viewModel: NewMessageViewModel): ViewModel
}
