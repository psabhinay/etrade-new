package com.etrade.mobilepro.quote.presentation

import android.content.Context
import android.widget.TextView
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.quoteapi.QuoteChangeWidgetItem
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(application = android.app.Application::class)
class BindingAdaptersQuoteWidgetTextViewTest {

    private var sutTextView: TextView? = null

    private val context = getApplicationContext<Context>()

    @Before
    fun setUp() {
        sutTextView = TextView(context)
    }

    @Test
    fun testQuotePrice() {
        Assert.assertNotNull(sutTextView)

        sutTextView?.let {
            // when
            it.setQuotePrice("67.78567")
            // then
            Assert.assertEquals("67.79", it.text.toString())
        }

        sutTextView?.let {
            // when
            it.setQuotePrice("0.78567")
            // then
            Assert.assertEquals("0.7857", it.text.toString())
        }

        sutTextView?.let {
            // when
            it.setQuotePrice("0.78000")
            // then
            Assert.assertEquals("0.78", it.text.toString())
        }

        sutTextView?.let {
            // when
            it.setQuotePrice("0.38800")
            // then
            Assert.assertEquals("0.388", it.text.toString())
        }
    }

    @Test
    fun testQuoteVolume() {
        Assert.assertNotNull(sutTextView)

        sutTextView?.let {
            // when
            it.setQuoteVolume("123456789")
            // then
            Assert.assertEquals(it.text.toString(), "123.46M")

            // when
            it.setQuoteVolume("123456")
            // then
            Assert.assertEquals(it.text.toString(), "123.46K")
        }
    }

    @Test
    fun testQuoteChange() {
        Assert.assertNotNull(sutTextView)

        sutTextView?.let {
            it.setQuoteChange(QuoteChangeWidgetItem("0.290000", "1.00000"))
            Assert.assertEquals("0.29 (1.00%)", it.text.toString())
        }

        sutTextView?.let {
            it.setQuoteChange(QuoteChangeWidgetItem("0.31300", "-11.12320"))
            Assert.assertEquals("0.313 (-11.12%)", it.text.toString())
        }

        sutTextView?.let {
            it.setQuoteChange(QuoteChangeWidgetItem("0.11156", "-20.2360"))
            Assert.assertEquals("0.1116 (-20.24%)", it.text.toString())
        }
    }
}
