package com.etrade.mobilepro.inboxmessages.dynamicui.data.lowcash

import com.etrade.mobile.accounts.dto.AccountDto
import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.inboxmessages.InboxMessage
import com.etrade.mobilepro.inboxmessages.dynamicui.api.lowcash.LowCashPrompt
import com.etrade.mobilepro.inboxmessages.dynamicui.data.findAccountDto
import com.etrade.mobilepro.inboxmessages.dynamicui.data.findCorrespondingInboxMessage

internal fun createLowCashPrompt(
    factory: LowCashPromptFactory,
    view: BaseScreenView,
    accounts: List<AccountDto>,
    messages: ETResult<List<InboxMessage>>,
    isSystemInDarkMode: Boolean
): LowCashPrompt? {
    val account = findAccountDto(view, accounts) ?: return null
    val promptCode = account.promptsForFunding ?: return null
    val message = findCorrespondingInboxMessage(messages, promptCode, isSystemInDarkMode) ?: return null
    return factory.create(message)
}
