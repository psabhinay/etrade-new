package com.etrade.mobilepro.util.android.textutil

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.util.Linkify
import android.view.View
import android.widget.TextView
import androidx.core.text.util.LinkifyCompat

private const val SPACE = " "

fun TextView.setTextAndLinkify(text: String) {
    this.text = TextUtil.convertTextWithHmlTagsToSpannable(text)
    LinkifyCompat.addLinks(this, Linkify.ALL)
    (this.text as? Spannable)?.updateTextPaintForUrlSpans(drawUnderlined = false)
}

/**
 * Create a [Spannable] containing one or more clickable links.
 *
 * @param links a [List] of links (text + on click action)
 * @param separator to be used between links
 * @return a [Spannable] object
 */
fun linksSpannable(
    links: List<Pair<String, (View) -> Unit>>,
    separator: String = SPACE
): Spannable =
    links.foldIndexed(SpannableStringBuilder()) { index, spannable, (linkText, linkAction) ->
        if (index > 0) {
            val separatorString =
                if (separator != SPACE) {
                    " $separator "
                } else {
                    SPACE
                }
            spannable.append(separatorString)
        }
        val startPosition = spannable.length
        spannable.append(linkText)
        val span = linkSpan(onClick = linkAction)
        spannable.set(span, startPosition, linkText.length)
        spannable
    }

/**
 * Set a [Spannable] as contents of the [TextView].
 *
 * @param text [Spannable] to be used
 */
fun TextView.set(text: Spannable) {
    movementMethod = LinkMovementMethod.getInstance()
    setText(text, TextView.BufferType.SPANNABLE)
}

/**
 * Append a colored text fragment to [SpannableStringBuilder].
 *
 * @param text [String]
 * @param color RGB color [Int]
 */
fun SpannableStringBuilder.append(text: String, color: Int): SpannableStringBuilder =
    append(text, ForegroundColorSpan(color), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

private fun linkSpan(
    isUnderlined: Boolean = false,
    onClick: (widget: View) -> Unit,
): ClickableSpan = object : ClickableSpan() {
    override fun onClick(widget: View) {
        onClick(widget)
    }
    override fun updateDrawState(ds: TextPaint) {
        ds.isUnderlineText = isUnderlined
    }
}

private fun Spannable.set(span: ClickableSpan, position: Int, length: Int) {
    setSpan(span, position, position + length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
}
