package com.etrade.mobilepro.util

private const val MAJOR_VER_MULTIPLIER = 10000
private const val MINOR_VER_MULTIPLIER = 100
private const val VERSION_SEGMENT_SIZE = 3

val String.versionCodeInt: Int
    get() = split(".")
        .take(VERSION_SEGMENT_SIZE)
        .mapIndexedNotNull { index, s ->
            when (index) {
                0 -> s.toIntOrNull()?.times(MAJOR_VER_MULTIPLIER)
                1 -> s.toIntOrNull()?.times(MINOR_VER_MULTIPLIER)
                else -> s.toIntOrNull()
            }
        }.sum()
