package com.etrade.mobilepro.screener.data.result

import com.etrade.mobilepro.screener.api.result.PrescreenedQuote

interface ResultColumnTitleProvider {
    fun getTitle(column: ResultColumn, items: Collection<PrescreenedQuote> = emptyList()): String
}
