package com.etrade.mobilepro.dagger

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.screener.presentation.ScreenersContainerViewModel
import com.etrade.mobilepro.screener.presentation.etf.EtfScreenersViewModel
import com.etrade.mobilepro.screener.presentation.etf.fundprofile.EtfFundProfileViewModel
import com.etrade.mobilepro.screener.presentation.etf.performance.EtfPerformanceViewModel
import com.etrade.mobilepro.screener.presentation.etf.portfolio.EtfPortfolioViewModel
import com.etrade.mobilepro.screener.presentation.etf.result.EtfResultsViewModel
import com.etrade.mobilepro.screener.presentation.etf.risk.EtfRiskViewModel
import com.etrade.mobilepro.screener.presentation.etf.technicals.EtfTechnicalsViewModel
import com.etrade.mobilepro.screener.presentation.mutualfund.MutualFundScreenersViewModel
import com.etrade.mobilepro.screener.presentation.mutualfund.expenses.ExpensesViewModel
import com.etrade.mobilepro.screener.presentation.mutualfund.fees.FeesViewModel
import com.etrade.mobilepro.screener.presentation.mutualfund.fundprofile.FundProfileViewModel
import com.etrade.mobilepro.screener.presentation.mutualfund.performance.PerformanceViewModel
import com.etrade.mobilepro.screener.presentation.mutualfund.portfolio.MutualFundPortfolioViewModel
import com.etrade.mobilepro.screener.presentation.mutualfund.result.MutualFundResultsViewModel
import com.etrade.mobilepro.screener.presentation.mutualfund.risk.RiskViewModel
import com.etrade.mobilepro.screener.presentation.stock.StockScreenersViewModel
import com.etrade.mobilepro.screener.presentation.stock.earningsanddividends.EarningsAndDividendsViewModel
import com.etrade.mobilepro.screener.presentation.stock.fundamentals.FundamentalsViewModel
import com.etrade.mobilepro.screener.presentation.stock.marketsegment.MarketSegmentViewModel
import com.etrade.mobilepro.screener.presentation.stock.opinions.OpinionsViewModel
import com.etrade.mobilepro.screener.presentation.stock.pricevolume.PriceVolumeViewModel
import com.etrade.mobilepro.screener.presentation.stock.results.StockResultsViewModel
import com.etrade.mobilepro.screener.presentation.stock.technicals.TechnicalsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ScreenersViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ScreenersContainerViewModel::class)
    abstract fun bindScreenersContainerViewModel(viewModel: ScreenersContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MutualFundScreenersViewModel::class)
    abstract fun bindMutualFundScreenersViewModel(viewModel: MutualFundScreenersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StockScreenersViewModel::class)
    abstract fun bindStockScreenersViewModel(viewModel: StockScreenersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EtfScreenersViewModel::class)
    abstract fun bindEtfScreenersViewModel(viewModel: EtfScreenersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TechnicalsViewModel::class)
    abstract fun bindTechnicalsViewModel(viewModel: TechnicalsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MarketSegmentViewModel::class)
    abstract fun bindMarketSegmentViewModel(viewModel: MarketSegmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FundamentalsViewModel::class)
    abstract fun bindFundamentalsViewModel(viewModel: FundamentalsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EtfPortfolioViewModel::class)
    abstract fun bindEtfPortfolioViewModel(viewModel: EtfPortfolioViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MutualFundPortfolioViewModel::class)
    abstract fun bindPortfolioViewModel(viewModel: MutualFundPortfolioViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FundProfileViewModel::class)
    abstract fun bindFundProfileViewModel(viewModel: FundProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EtfFundProfileViewModel::class)
    abstract fun bindEtfFundProfileViewModel(viewModel: EtfFundProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EtfPerformanceViewModel::class)
    abstract fun bindEtfPerformanceViewModel(viewModel: EtfPerformanceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EtfTechnicalsViewModel::class)
    abstract fun bindEtfTechnicalsViewModel(viewModel: EtfTechnicalsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EtfRiskViewModel::class)
    abstract fun bindEtfRiskViewModel(viewModel: EtfRiskViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RiskViewModel::class)
    abstract fun bindRiskViewModel(viewModel: RiskViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExpensesViewModel::class)
    abstract fun bindExpensesViewModel(viewModel: ExpensesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PriceVolumeViewModel::class)
    abstract fun bindPriceVolumeViewModel(viewModel: PriceVolumeViewModel): ViewModel

    @Suppress("FunctionMaxLength")
    @Binds
    @IntoMap
    @ViewModelKey(EarningsAndDividendsViewModel::class)
    abstract fun bindEarningsAndDividendsViewModel(viewModel: EarningsAndDividendsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OpinionsViewModel::class)
    abstract fun bindOpinionsViewModel(viewModel: OpinionsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PerformanceViewModel::class)
    abstract fun bindPerformanceViewModel(viewModel: PerformanceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FeesViewModel::class)
    abstract fun bindFeesViewModel(viewModel: FeesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StockResultsViewModel::class)
    abstract fun bindStockResultsViewModel(viewModel: StockResultsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MutualFundResultsViewModel::class)
    abstract fun bindMfResultsViewModel(viewModel: MutualFundResultsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EtfResultsViewModel::class)
    abstract fun bindEtfResultsViewModel(viewModel: EtfResultsViewModel): ViewModel
}
