package com.etrade.mobilepro.masked.input.value.view

internal fun StringBuilder.handleOnTextAdded(input: CharSequence, mask: String) {
    clear()
    append(
        input.filterIndexed { index, char ->
            char != mask[index]
        }
    )
}

internal fun StringBuilder.handleOnTextRemoved(
    startPosition: Int,
    removedLength: Int,
    mask: String,
    maskPlaceholderChar: Char
) {
    val rawValueStart = mask.substring(0, startPosition + 1)
        .count { it == maskPlaceholderChar } - 1

    if (rawValueStart >= 0) {
        val numberOfRawCharsToRemove =
            mask.substring(startPosition - removedLength + 1, startPosition + 1)
                .count { it == maskPlaceholderChar }
        val rawValueEnd = if (numberOfRawCharsToRemove == 0) {
            rawValueStart + 1
        } else {
            rawValueStart + numberOfRawCharsToRemove
        }

        delete(rawValueStart, rawValueEnd)
    }
}
