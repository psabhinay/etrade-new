package com.etrade.mobilepro.webview.bridge

interface OpenQuoteHandler {
    fun openQuote(symbol: String)
}
