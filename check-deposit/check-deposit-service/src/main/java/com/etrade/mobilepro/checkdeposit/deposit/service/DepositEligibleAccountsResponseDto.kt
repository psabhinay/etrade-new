package com.etrade.mobilepro.checkdeposit.deposit.service

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigInteger

@JsonClass(generateAdapter = true)
class DepositEligibleAccountsResponseDto(
    @Json(name = "DepositEligibleAccounts")
    val depositEligibleAccountsDto: DepositEligibleAccountsDto?
) : BaseDataDto()

@JsonClass(generateAdapter = true)
class DepositEligibleAccountsDto(
    @Json(name = "isSignedForTerms")
    val signedForTerms: Boolean?,
    @Json(name = "accountList")
    val accountList: List<DepositAccountDto>
)

@JsonClass(generateAdapter = true)
class DepositAccountDto(
    @Json(name = "accountId")
    val accountId: String,
    @Json(name = "instNo")
    val instNo: BigInteger,
    @Json(name = "accountType")
    val accountType: String,
    @Json(name = "accountCategory")
    val accountCategory: String,
    @Json(name = "nickName")
    val nickName: String?
)
