package com.etrade.mobilepro.messagecenter.presentation.common

import android.os.Parcelable
import com.etrade.mobilepro.messagecenter.api.model.Message
import kotlinx.parcelize.Parcelize
import org.threeten.bp.Instant

@Parcelize
class MessageParcel(
    val id: String,
    val accountId: String,
    val accountShortName: String,
    val descId: String,
    val subjectId: String,
    val caseTypeId: String,
    val caseTypeDesc: String,
    val caseNo: String,
    val isUnread: Boolean,
    val status: String,
    val createdAt: Long?,
    val createdAtFormatted: String,
    val readAt: Long?
) : Parcelable {
    fun toDomain(): Message =
        Message(
            id,
            accountId,
            accountShortName,
            descId,
            subjectId,
            caseTypeId,
            caseTypeDesc,
            caseNo,
            isUnread,
            status,
            createdAt?.let { Instant.ofEpochMilli(it) },
            createdAtFormatted,
            readAt?.let { Instant.ofEpochMilli(it) }
        )
}

internal fun Message.parcelize(): MessageParcel =
    MessageParcel(
        id,
        accountId,
        accountShortName,
        descId,
        subjectId,
        caseTypeId,
        caseTypeDesc,
        caseNo,
        isUnread,
        status,
        createdAt?.toEpochMilli(),
        createdAtFormatted,
        readAt?.toEpochMilli()
    )
