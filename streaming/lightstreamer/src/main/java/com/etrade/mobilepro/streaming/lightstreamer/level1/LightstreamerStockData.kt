package com.etrade.mobilepro.streaming.lightstreamer.level1

import com.etrade.eo.streaming.UpdateInfo
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.util.domain.data.deriveDayChange
import com.etrade.mobilepro.util.domain.data.deriveDayChangePercent
import com.etrade.mobilepro.util.isBigDecimalZero
import org.threeten.bp.LocalDate
import java.math.BigDecimal

private const val DO_NOT_MULTIPLY_PRIMARY_EXCHANGE = "u"
private val hundred: BigDecimal = BigDecimal("100")

/**
 * Level 1 quotes data for stocks.
 */
class LightstreamerStockData constructor(
    updateInfo: UpdateInfo,
    fields: Set<Level1Field>,
    instrumentType: InstrumentType
) : StockData {

    private val shouldModify: Boolean = updateInfo.getValue(Level1Column.PRIMARY_EXCHANGE) != DO_NOT_MULTIPLY_PRIMARY_EXCHANGE

    override val ask: String? = updateInfo.getValue(Level1Column.ASK)
    override val askExchange: String? = updateInfo.getValue(Level1Column.ASK_EXCHANGE)
    override val askSize: String? = updateInfo.getValue(Level1Column.ASK_SIZE).toModifiedSize(shouldModify)
    override val beta: String? = updateInfo.getValue(Level1Column.BETA)
    override val bid: String? = updateInfo.getValue(Level1Column.BID)
    override val bidExchange: String? = updateInfo.getValue(Level1Column.BID_EXCHANGE)
    override val bidSize: String? = updateInfo.getValue(Level1Column.BID_SIZE).toModifiedSize(shouldModify)
    override val change: String?
    override val changePercent: String?
    override val closingMark: String? = updateInfo.getValue(Level1Column.CLOSING_MARK)
    override val dayChange: String?
    override val dayChangePercent: String?
    override val dayHigh: String? = updateInfo.getValue(Level1Column.HIGH)
    override val dayLow: String? = updateInfo.getValue(Level1Column.LOW)
    override val dividend: String? = updateInfo.getValue(Level1Column.DIVIDEND_RATE)
    override val dividendYield: String? = updateInfo.getValue(Level1Column.DIVIDEND_YIELD)?.plus("%")
    override val earningsPerShare: String? = updateInfo.getValue(Level1Column.EPS)
    override val lastPrice: String?
    override val lastPriceExchange: String?
    override val lastVolume: String?
    override val mark: String? = updateInfo.getValue(Level1Column.MARK)
    override val marketCap: String? = updateInfo.getValue(Level1Column.MARKET_CAP)
    override val tradeStatus: String? = updateInfo.getValue(Level1Column.TRADE_STATUS)
    override val tradeHaltReason: String? = updateInfo.getValue(Level1Column.TRADE_HALT_REASON)
    override val openPrice: String? = updateInfo.getValue(Level1Column.OPEN)
    override val previousClosePrice: String? = updateInfo.getValue(Level1Column.PREVIOUS_CLOSE)
    override val priceToEarnings: String?
    override val timestampSinceEpoch: Long?
    override val volume: String? = updateInfo.getValue(Level1Column.ACCUM_VOLUME)
    override val volumeTenDayAverage: String? = updateInfo.getValue(Level1Column.VOLUME_TEN_DAY_AVERAGE)
    override val tickIndicator: Short?
    override val yearHigh: String? = updateInfo.getValue(Level1Column.YEAR_HIGH)
    override val yearHighDate: LocalDate? = updateInfo.getYearDate(Level1Column.YEAR_HIGH_DATE)
    override val yearLow: String? = updateInfo.getValue(Level1Column.YEAR_LOW)
    override val yearLowDate: LocalDate? = updateInfo.getYearDate(Level1Column.YEAR_LOW_DATE)

    init {
        val tickIndicatorColumnDependency: Level1Column

        when (val status = updateInfo.getValue(Level1Column.MARKET_STATUS)) {
            "PET", "POT" -> {
                tickIndicatorColumnDependency = Level1Column.EXT_HOURS_LAST
                val closingPrice = updateInfo.getValue(Level1Column.LAST)
                lastPrice = updateInfo.getValue(Level1Column.EXT_HOURS_LAST)?.takeIf {
                    !it.isBigDecimalZero()
                }
                change = updateInfo.getValue(Level1Column.EXT_HOURS_LAST_CHANGE)
                changePercent = updateInfo.getValue(Level1Column.EXT_HOURS_LAST_CHANGE_PCT)
                val nonExtendedHoursChange = updateInfo.getValue(Level1Column.LAST_CHANGE)
                dayChange = deriveDayChange(change, nonExtendedHoursChange)
                dayChangePercent = deriveDayChangePercent(dayChange, closingPrice, nonExtendedHoursChange)
                lastPriceExchange = updateInfo.getValue(Level1Column.EXT_HOURS_EXCHANGE_CODE)
                lastVolume = updateInfo.getValue(Level1Column.EXT_HOURS_VOL)
                timestampSinceEpoch = updateInfo.getTimeValue()
            }
            else -> {
                val nonExtendedHoursChange = updateInfo.getValue(Level1Column.LAST_CHANGE)
                val nonExtendedHoursChangePercent = updateInfo.getValue(Level1Column.LAST_CHANGE_PCT)
                if (status == "CLO") {
                    val extendedHourLast = updateInfo.getValue(Level1Column.EXT_HOURS_LAST)
                    val closingPrice = updateInfo.getValue(Level1Column.LAST)
                    if (!(extendedHourLast?.isBigDecimalZero() == true || closingPrice?.isBigDecimalZero() == true)) {
                        if (extendedHourLast != null && !instrumentType.isIndex) {
                            tickIndicatorColumnDependency = Level1Column.EXT_HOURS_LAST
                            lastPrice = extendedHourLast
                            lastPriceExchange = updateInfo.getValue(Level1Column.EXT_HOURS_EXCHANGE_CODE)
                            change = updateInfo.getValue(Level1Column.EXT_HOURS_LAST_CHANGE)
                            changePercent = updateInfo.getValue(Level1Column.EXT_HOURS_LAST_CHANGE_PCT)
                            dayChange = deriveDayChange(change, nonExtendedHoursChange)
                            dayChangePercent = deriveDayChangePercent(dayChange, closingPrice, nonExtendedHoursChange)
                        } else {
                            // we might have symbols that don't have extended hours trading
                            tickIndicatorColumnDependency = Level1Column.LAST
                            lastPrice = updateInfo.getValue(Level1Column.LAST)
                            lastPriceExchange = updateInfo.getValue(Level1Column.EXCHANGE_CODE)
                            change = nonExtendedHoursChange
                            changePercent = nonExtendedHoursChangePercent
                            dayChange = nonExtendedHoursChange
                            dayChangePercent = nonExtendedHoursChangePercent
                        }
                        lastVolume = updateInfo.getValue(Level1Column.EXT_HOURS_VOL) ?: updateInfo.getValue(Level1Column.LAST_VOLUME)
                        // we want to update if instrument is index else we don't want to update timestamp when we get a closed market update
                        timestampSinceEpoch = if (instrumentType.isIndex) {
                            updateInfo.getMarketHrsTimeValue() ?: System.currentTimeMillis()
                        } else {
                            null
                        }
                    } else {
                        // skipping updates if last is detected as 0
                        lastPrice = null
                        change = null
                        changePercent = null
                        dayChange = null
                        dayChangePercent = null
                        lastPriceExchange = null
                        lastVolume = null
                        timestampSinceEpoch = null
                        tickIndicatorColumnDependency = Level1Column.LAST
                    }
                } else {
                    tickIndicatorColumnDependency = Level1Column.LAST
                    lastPrice = updateInfo.getValue(Level1Column.LAST)?.takeIf { !it.isBigDecimalZero() }
                    lastPriceExchange = updateInfo.getValue(Level1Column.EXCHANGE_CODE)
                    change = nonExtendedHoursChange
                    changePercent = nonExtendedHoursChangePercent
                    dayChange = nonExtendedHoursChange
                    dayChangePercent = nonExtendedHoursChangePercent
                    lastVolume = updateInfo.getValue(Level1Column.LAST_VOLUME)
                    timestampSinceEpoch = updateInfo.getMarketHrsTimeValue()
                }
            }
        }

        priceToEarnings = calculatePriceToEarnings(fields)
        tickIndicator = updateInfo.getTickIndicator(tickIndicatorColumnDependency)
    }

    /** Etrade mobile client displays size with multiplying factor of 100*/
    private fun String?.toModifiedSize(shouldModify: Boolean): String? {
        return if (shouldModify) {
            if (this.isNullOrEmpty()) {
                null
            } else {
                BigDecimal(this).multiply(hundred).toPlainString()
            }
        } else {
            this
        }
    }
}
