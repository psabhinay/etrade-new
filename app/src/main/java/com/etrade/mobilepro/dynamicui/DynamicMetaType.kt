package com.etrade.mobilepro.dynamicui

import com.etrade.mobilepro.backends.mgs.BaseMeta
import com.etrade.mobilepro.backends.mgs.PaginationMeta

enum class DynamicMetaType(
    override val dtoName: String,
    override val dtoClass: Class<out BaseMeta>
) : DynamicEnum<BaseMeta> {

    PAGINATION("pagination", PaginationMeta::class.java)
}
