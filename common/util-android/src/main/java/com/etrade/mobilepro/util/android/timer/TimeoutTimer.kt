package com.etrade.mobilepro.util.android.timer

import android.os.CountDownTimer
import org.threeten.bp.Duration
import javax.inject.Inject

interface TimeoutTimer {
    fun start(timeout: Duration, actionOnStop: (() -> Unit)?)
    fun stop()
    fun isActive(): Boolean
}

class DefaultTimeoutTimer @Inject constructor() : TimeoutTimer {
    private var countDownTimer: CountDownTimer? = null
    private var actionOnStop: (() -> Unit)? = null

    override fun start(timeout: Duration, actionOnStop: (() -> Unit)?) {
        stop()
        this.actionOnStop = actionOnStop
        countDownTimer = createTimer(timeout)
        countDownTimer?.start()
    }

    override fun stop() {
        actionOnStop?.invoke()
        actionOnStop = null
        countDownTimer?.cancel()
        countDownTimer = null
    }

    override fun isActive(): Boolean = countDownTimer != null

    private fun createTimer(timeout: Duration): CountDownTimer = object : CountDownTimer(timeout.toMillis(), timeout.toMillis()) {
        override fun onFinish() {
            stop()
        }

        override fun onTick(millisUntilFinished: Long) {
            // nop
        }
    }
}
