package com.etrade.mobilepro.menu.fragment.recyclerview.delegate

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.menu.DividerMenuItem
import com.etrade.mobilepro.menu.MenuItem
import com.etrade.mobilepro.menupage.presentation.R
import com.etrade.mobilepro.util.android.extension.inflate
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class DividerMenuItemDelegate : AbsListItemAdapterDelegate<DividerMenuItem, MenuItem, RecyclerView.ViewHolder>() {

    override fun isForViewType(item: MenuItem, items: MutableList<MenuItem>, position: Int): Boolean {
        return item is DividerMenuItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return object : RecyclerView.ViewHolder(parent.inflate(R.layout.view_menu_divider)) {}
    }

    override fun onBindViewHolder(item: DividerMenuItem, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        // np
    }
}
