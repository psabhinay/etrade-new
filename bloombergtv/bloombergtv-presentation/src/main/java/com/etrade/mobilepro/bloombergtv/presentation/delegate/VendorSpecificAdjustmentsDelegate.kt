package com.etrade.mobilepro.bloombergtv.presentation.delegate

import android.os.Build
import android.os.Handler
import android.os.Looper
import android.view.View
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvActivity

private const val SAMSUNG = "Samsung"
private const val PLAYER_INITIALIZATION_DELAY_MS = 500L

internal class VendorSpecificAdjustmentsDelegate : BloombergTvActivityDelegate {
    private val isSamsung = Build.MANUFACTURER.equals(SAMSUNG, ignoreCase = true)
    private lateinit var activity: BloombergTvActivity
    private val handler = Handler(Looper.getMainLooper())

    override fun init(activity: BloombergTvActivity) {
        this.activity = activity
    }

    fun handleVendorSpecifics() {
        if (isSamsung) {
            activity.binding.loadingPanel.visibility = View.VISIBLE
            handler.postDelayed(
                {
                    activity.binding.loadingPanel.visibility = View.GONE
                    activity.binding.loadingIndicator.visibility = View.GONE
                },
                PLAYER_INITIALIZATION_DELAY_MS
            )
        } else {
            activity.binding.loadingIndicator.visibility = View.GONE
        }
    }

    fun dismantle() {
        handler.removeCallbacksAndMessages(null)
    }
}
