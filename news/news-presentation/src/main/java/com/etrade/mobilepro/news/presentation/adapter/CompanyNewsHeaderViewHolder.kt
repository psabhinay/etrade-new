package com.etrade.mobilepro.news.presentation.adapter

import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.presentation.databinding.ItemCompanyNewsHeaderBinding

class CompanyNewsHeaderViewHolder(
    private val binding: ItemCompanyNewsHeaderBinding
) : RecyclerView.ViewHolder(binding.root) {

    @Suppress("LongMethod")
    fun bindTo(item: NewsItem?, showSearchHeader: Boolean = true) {
        if (item is NewsItem.CompanyHeader) {
            with(binding) {
                lifecycleOwner?.let { lifeCycleOwner ->
                    item.quoteWidgetData?.observe(
                        lifeCycleOwner,
                        Observer {
                            binding.quoteItem = it
                        }
                    )
                }

                searchHeaderTv.setOnClickListener {
                    item.onSearchFieldTapListener?.invoke()
                }
                setAlertBtn.setOnClickListener {
                    item.onSetAlertListener?.invoke()
                }
                addWatchListBtn.setOnClickListener {
                    item.onAddToWatchlistListener?.invoke()
                }
                tradeBtn.setOnClickListener {
                    item.onTradeListener?.invoke()
                }
                if (!showSearchHeader) {
                    searchHeaderTv.visibility = View.GONE
                    searchGutter.visibility = View.GONE
                }
            }
        }
    }
}
