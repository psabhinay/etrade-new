package com.etrade.mobilepro.screener.data.result.column

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfFundProfileFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfPerformanceFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfPortfolioFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfRiskFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfTechnicalsFilter
import com.etrade.mobilepro.screener.data.result.ResultColumn
import com.etrade.mobilepro.screener.data.result.ResultColumnsResolver
import javax.inject.Inject

private val commonForAllEtfResults = setOf(
    ResultColumn.EtfDescription,
    ResultColumn.LastPrice,
    ResultColumn.ChangeDollar,
    ResultColumn.ChangePercent,
    ResultColumn.FundCategory,
    ResultColumn.Volume
)

class EtfResultColumnsResolver @Inject constructor() : ResultColumnsResolver {
    override fun getColumns(filter: ScreenerFilter): Set<ResultColumn> = commonForAllEtfResults +
        when (filter) {
            is EtfFundProfileFilter -> getSpecificColumns(filter)
            is EtfPerformanceFilter -> getSpecificColumns(filter)
            is EtfRiskFilter -> getSpecificColumns(filter)
            is EtfPortfolioFilter -> getSpecificColumns(filter)
            is EtfTechnicalsFilter -> getSpecificColumns(filter)
            else -> throw IllegalArgumentException("$filter is wrong for etf result columns resolver")
        }

    private fun getSpecificColumns(filter: EtfFundProfileFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.isExpenseRatioValueSelected()) {
            result.add(ResultColumn.ExpenseRatio)
        }
        if (filter.morningStarRating.isNotEmpty()) {
            result.add(ResultColumn.MorningStarRating)
        }
        return result
    }

    private fun getSpecificColumns(filter: EtfPerformanceFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (!filter.isNavReturnEmpty() || !filter.isWithinCategoryEmpty()) {
            result.add(ResultColumn.MarketReturnCategory1Month)
            result.add(ResultColumn.MarketReturnCategory1Year)
            result.add(ResultColumn.MarketReturnCategory5Years)
            result.add(ResultColumn.MarketReturnCategory10Years)
            result.add(ResultColumn.NavReturnCategory1Year)
            result.add(ResultColumn.NavReturnCategory5Years)
            result.add(ResultColumn.NavReturnCategory10Years)
            result.add(ResultColumn.NavReturnCategorySinceInception)
        }
        return result
    }

    private fun getSpecificColumns(filter: EtfRiskFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.isNotEmpty()) {
            result.add(ResultColumn.StandardDeviation)
        }
        return result
    }

    private fun getSpecificColumns(filter: EtfPortfolioFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (!filter.isNetAssetEmpty()) {
            result.add(ResultColumn.TotalAssets)
        }
        if (!filter.isYieldEmpty()) {
            result.add(ResultColumn.Yield)
        }
        return result
    }

    private fun getSpecificColumns(filter: EtfTechnicalsFilter): Set<ResultColumn> {
        val result = mutableSetOf<ResultColumn>()
        if (filter.isEtfTechnicalsValuesSelected()) {
            result.add(ResultColumn.Volume)
            result.add(ResultColumn.Volume30DaysAvg)
        }
        return result
    }
}
