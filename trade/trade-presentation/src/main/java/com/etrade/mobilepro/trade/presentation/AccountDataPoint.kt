package com.etrade.mobilepro.trade.presentation

internal class AccountDataPoint(
    val label: String,
    val value: String
)
