package com.etrade.mobilepro.accountvalues

import com.etrade.eo.accountvalues.api.AccountValuesVO
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Observable
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class DefaultStreamingAccountValuesProviderTest {

    @Test
    fun `accountValues should return an observable with account values`() {

        // Given
        val mockObservable = mock<Observable<AccountValuesVO>>()
        val mockHelper = mock<SubscriptionManagerHelper<AccountValuesVO>> {
            on { subscribeTo(any(), any(), any()) } doReturn mockObservable
        }
        val sut = DefaultStreamingAccountValuesProvider(mockHelper)

        // When
        val result = sut.accountValues("accId", true)

        // Then
        assertEquals(result, mockObservable)
    }
}
