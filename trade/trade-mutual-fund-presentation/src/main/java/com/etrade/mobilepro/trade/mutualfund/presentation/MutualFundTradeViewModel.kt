package com.etrade.mobilepro.trade.mutualfund.presentation

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.backends.api.ServerResponseException
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.mutualfund.api.MutualFundEntry
import com.etrade.mobilepro.trade.mutualfund.api.MutualFundTradeRepo
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(MutualFundTradeViewModel::class.java)
private const val INVALID_FUNDS_FAMILY = "100048"
private const val NO_MUTUAL_FUND_CODE = "100002"
private const val NO_RESULT_CODE = "9004"

class MutualFundTradeViewModel @Inject constructor(
    private val resources: Resources,
    private val mutualFundTradeRepo: MutualFundTradeRepo
) : ViewModel() {

    val actionLoadingIndicator: LiveData<Boolean>
        get() = _actionLoadingIndicator
    val errorMessage: LiveData<ConsumableLiveEvent<ErrorMessage>>
        get() = _errorMessage
    val mutualFundSellEntry: LiveData<List<MutualFundEntry>>
        get() = _mutualFundSellEntry
    val mutualFundBuyEntry: LiveData<List<MutualFundEntry>>
        get() = _mutualFundBuyEntry
    val mutualFundErrorMessage: LiveData<String>
        get() = _mutualFundErrorMessage

    private val _errorMessage: MutableLiveData<ConsumableLiveEvent<ErrorMessage>> = MutableLiveData()
    private val _actionLoadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)
    private val _mutualFundSellEntry: MutableLiveData<List<MutualFundEntry>> = MutableLiveData()
    private val _mutualFundBuyEntry: MutableLiveData<List<MutualFundEntry>> = MutableLiveData()
    private val _mutualFundErrorMessage: MutableLiveData<String> = MutableLiveData()

    private var selectedAccountId: String? = null
    private var selectedSymbol: String? = null

    fun start(
        accountId: String,
        type: MutualFundTradeType,
        symbol: String?,
        typeCode: String?
    ) {
        when (type) {
            MutualFundTradeType.SELL -> {
                getMutualFundSellEntries(accountId)
            }
            MutualFundTradeType.EXCHANGE_SELL -> {
                getMutualFundExchangeSellEntries(accountId)
            }
            MutualFundTradeType.EXCHANGE_BUY -> {
                symbol?.let { it ->
                    typeCode?.let { typeCode -> getMutualFundExchangeBuyEntries(accountId, it, typeCode) }
                }
            }
        }
    }

    fun getMutualFundSellEntries(accountId: String) {
        selectedAccountId = accountId
        _actionLoadingIndicator.value = true
        viewModelScope.launch {
            mutualFundTradeRepo.getMutualFundSellEntry(accountId).fold(
                onSuccess = {
                    _mutualFundSellEntry.value = it
                },
                onFailure = { handleMutualFundException(it) }
            )
            _actionLoadingIndicator.value = false
        }
    }

    private fun getMutualFundExchangeBuyEntries(accountId: String, symbol: String, typeCode: String) {
        selectedAccountId = accountId
        selectedSymbol = symbol
        _actionLoadingIndicator.value = true
        viewModelScope.launch {
            mutualFundTradeRepo.getMutualFundExchangeBuyEntry(
                accountId,
                object : WithSymbolInfo {
                    override val symbol: String = symbol
                    override val instrumentType: InstrumentType = InstrumentType.from(typeCode)
                }
            ).fold(
                onSuccess = {
                    _mutualFundBuyEntry.value = it.entryList
                },
                onFailure = { handleMutualFundException(it) }
            )
            _actionLoadingIndicator.value = false
        }
    }

    private fun getMutualFundExchangeSellEntries(accountId: String) {
        selectedAccountId = accountId
        _actionLoadingIndicator.value = true
        viewModelScope.launch {
            mutualFundTradeRepo.getMutualFundExchangeSellEntry(accountId).fold(
                onSuccess = {
                    _mutualFundSellEntry.value = it
                },
                onFailure = { handleMutualFundException(it) }
            )
            _actionLoadingIndicator.value = false
        }
    }

    private fun handleMutualFundException(throwable: Throwable) {
        logger.error(throwable.message)
        val exception = throwable as? ServerResponseException
        exception?.error?.extractServerMessage()?.also {
            when (it.code) {
                NO_MUTUAL_FUND_CODE ->
                    _mutualFundErrorMessage.value =
                        resources.getString(R.string.trade_warning_no_mutual_fund_in_account)
                NO_RESULT_CODE ->
                    _mutualFundErrorMessage.value =
                        String.format(resources.getString(R.string.trade_warning_no_result_for_fund, selectedSymbol))
                INVALID_FUNDS_FAMILY -> _mutualFundErrorMessage.value = it.text
                else -> errorMessage(resources.getString(R.string.error_message_general))
            }
        } ?: errorMessage(resources.getString(R.string.error_message_general))
    }

    private fun errorMessage(message: String) {
        errorMessage(ErrorMessage(message))
    }

    private fun errorMessage(message: ErrorMessage) {
        _errorMessage.value = ConsumableLiveEvent(message)
    }
}
