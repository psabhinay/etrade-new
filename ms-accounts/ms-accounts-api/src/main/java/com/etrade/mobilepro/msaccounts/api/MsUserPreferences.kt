package com.etrade.mobilepro.msaccounts.api

import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.msaccounts.api.MsUserPreferences.Companion.ENROLLMENT_CTA_LABEL
import com.etrade.mobilepro.msaccounts.api.MsUserPreferences.Companion.SETTINGS_CTA_LABEL
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MsUserPreferences(
    val loginUuid: String? = null,
    val isMsEligible: Boolean = false,
    val isUserEnrolled: Boolean = false,
    val ctaList: List<CallToActionDto> = emptyList()
) {
    companion object {
        const val ENROLLMENT_CTA_LABEL = "MS Enrollment"
        const val SETTINGS_CTA_LABEL = "MS Settings"
    }
}

fun MsUserPreferences.getEnrollmentCta(): CallToActionDto? =
    ctaList.firstOrNull { it.label == ENROLLMENT_CTA_LABEL }

fun MsUserPreferences.getSettingsCta(): CallToActionDto? =
    ctaList.firstOrNull { it.label == SETTINGS_CTA_LABEL }
