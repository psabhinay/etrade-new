package com.etrade.mobilepro.quote.mapper

import androidx.lifecycle.Transformations
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.companyoverview.CompanyOverviewProvider
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quoteapi.DateStruct
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.quoteapi.QuoteTemplateResources
import com.etrade.mobilepro.util.market.time.NEW_YORK_ZONE_ID
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.threeten.bp.ZonedDateTime
import javax.inject.Inject

class QuoteDetailMapper @Inject constructor(
    private val templateResources: QuoteTemplateResources,
    private val companyOverviewProvider: CompanyOverviewProvider
) {

    private val logger: Logger = LoggerFactory.getLogger(QuoteDetailMapper::class.java)

    fun map(quote: MobileQuote): QuoteItem.QuoteDetailItem {
        val openPrice = quote.openPrice
        val prevClosePrice = quote.previousClose
        val averageVolume = quote.averageVolume
        val pe = String.format(templateResources.peFormat, quote.pe.toDoubleOrNull())
        val eps = quote.eps
        val formattedNextEarningsDate = formatDate(quote.nextEarningDate)
        val marketCap = quote.marketCap
        val sharesOutstanding = quote.sharesOutstanding
        val beta = quote.beta
        val dividendYield = MarketDataFormatter.formatMarketPercData(quote.dividendYield.toBigDecimalOrNull())
        val dividend = quote.declaredDividend
        val formattedExDividendDate = formatDate(quote.exDividendDate)
        val formattedDividendPayableDate = formatDate(quote.dividentPayDate)
        val sector = Transformations.map(companyOverviewProvider.createCompanySectorLiveData(quote.symbol)) { resource ->
            resource?.data
        }
        val isThirtyDayYieldVisible = quote.subSecType == InstrumentType.ETF.typeCode

        return QuoteItem.QuoteDetailItem(
            symbol = quote.symbol,
            initialOpenPrice = openPrice,
            initialPrevClosePrice = prevClosePrice,
            initialAverageVolume = averageVolume,
            initialPriceToEarnings = pe,
            initialEarningsPerShare = eps,
            nextEarningsDate = formattedNextEarningsDate,
            initialMarketCap = marketCap,
            sharesOutstanding = sharesOutstanding,
            initialBeta = beta,
            thirtyDayYield = quote.thirtyDaySecYield,
            isThirtyDayYieldVisible = isThirtyDayYieldVisible,
            initialDividendYield = dividendYield,
            initialDividend = dividend,
            exDividendDate = formattedExDividendDate,
            dividendPayableDate = formattedDividendPayableDate,
            exchangeCode = quote.exchangeCode,
            sector = sector,
            nextEarningsDateRaw = quote.nextEarningDate.safeParseToMillis()
        )
    }

    private fun formatDate(date: DateStruct): String {
        return String.format(
            templateResources.dateFormat,
            validateDateParameter(date.month),
            validateDateParameter(date.day),
            validateDateParameter(date.year)
        )
    }

    private fun validateDateParameter(num: String): String =
        if (num.isBlank() || num == "0") {
            templateResources.valueMissing
        } else num

    // MagicNumber: represented by hour value, self-explanatory.
    // TooGenericExceptionCaught: Catches exceptions for both DateTimeException and NumberFormatException.
    @Suppress("MagicNumber", "TooGenericExceptionCaught")
    private fun DateStruct.safeParseToMillis(): Long? {
        return try {
            ZonedDateTime.of(year.toInt(), month.toInt(), day.toInt(), 12, 0, 0, 0, NEW_YORK_ZONE_ID).toInstant().toEpochMilli()
        } catch (e: RuntimeException) {
            logger.warn("Wrong date: $year-$month-$day", e)
            null
        }
    }
}
