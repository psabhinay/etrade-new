package com.etrade.mobilepro.encryption.api

/**
 * A repo which provides secret keys.
 */
interface SecretKeyRepo {

    /**
     * Provides an algorithm that can be used with the keys to encrypt / decrypt data.
     */
    val algorithmSpec: SymmetricAlgorithmSpec

    /**
     * Common key that can be used to encrypt all kind of data. This key doesn't require an authorization from a user to be used.
     */
    val commonKey: SecretKeyMaterial

    /**
     * Unlike [commonKey], secure key requires a user to authorize its usage.
     */
    val secureKey: SecretKeyMaterial

    /**
     * Method to call when the secret key has been invalidated and can no longer be used. Will reset the [secureKey] lazy delegate and delete the secure key
     * from key store
     */
    fun recreateSecretKey()
}
