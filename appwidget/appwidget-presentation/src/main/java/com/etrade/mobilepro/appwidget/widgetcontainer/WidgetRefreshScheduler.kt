package com.etrade.mobilepro.appwidget.widgetcontainer

import android.app.AlarmManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Context
import android.os.SystemClock
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.indices.getLayoutInitializationIntent

internal fun scheduleWidgetRefresh(context: Context, settings: WidgetSettings) {
    val alarmManager = context.getSystemService(Context.ALARM_SERVICE)
        as? AlarmManager ?: return
    cancelScheduledUpdates(context)
    if (settings.refreshRate == WidgetSettings.RefreshRate.MANUAL) { return }
    alarmManager.setRepeating(
        AlarmManager.ELAPSED_REALTIME,
        SystemClock.elapsedRealtime() + settings.refreshRate.inMillis,
        settings.refreshRate.inMillis,
        getPendingIntent(context)
    )
}

internal fun cancelScheduledUpdates(context: Context) {
    val alarmManager = context.getSystemService(Context.ALARM_SERVICE)
        as? AlarmManager ?: return
    alarmManager.cancel(getPendingIntent(context))
}

private fun getPendingIntent(context: Context): PendingIntent {
    val widgetIds = AppWidgetManager.getInstance(context)
        .getAppWidgetIds(ComponentName(context, DefaultAppWidgetProvider::class.java))

    return getLayoutInitializationIntent(context, widgetIds, forceRefresh = true)
        .run { PendingIntent.getForegroundService(context, 0, this, PendingIntent.FLAG_IMMUTABLE) }
}
