package com.etrade.mobilepro.trade.presentation.form.delegate.shared

import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.RuleListBuilder
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.api.SwitchInput
import com.etrade.mobilepro.dynamic.form.api.inputValue
import com.etrade.mobilepro.dynamic.form.api.requireAnyInput
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.presentation.TradeConditionalOrderDisclosureViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume

internal fun RuleListBuilder.observeAdvancedOrderForConditionalOrderDisclosure(
    inputId: WithAdvancedTradeFormInputId,
    disclosureViewModel: TradeConditionalOrderDisclosureViewModel
) {
    rule {
        requireInput<SwitchInput>(inputId.advancedOrder).value.observeForever {
            val isOn = it ?: false
            if (isOn) {
                disclosureViewModel.onAdvancedOrderSwitchedOn()
                disclosureViewModel.resetAdvancedOrderSwitch.observeForever(object : Observer<ConsumableLiveEvent<Boolean>> {
                    override fun onChanged(event: ConsumableLiveEvent<Boolean>?) {
                        event?.consume { needReset ->
                            disclosureViewModel.resetAdvancedOrderSwitch.removeObserver(this)
                            if (needReset) {
                                requireInput<SwitchInput>(inputId.advancedOrder).setValue(false)
                            }
                        }
                    }
                })
            }
        }
    }
}

internal fun RuleListBuilder.resetAdvancedOrderTypeOnPriceTypeChanges(
    advancedInputId: WithAdvancedTradeFormInputId,
    priceTypeInputId: WithPriceTradeFormInputId
) {
    rule {
        inputValue<DropDownOption?>(priceTypeInputId.priceType).observeForever {
            val valueId = priceTypeInputId.priceTypeValueId
            if (it?.id == valueId.trailingStopDollar || it?.id == valueId.trailingStopPercent) {
                val advancedOrderTypeInput = getInput<SelectInput>(advancedInputId.advancedOrderType)
                val isContingentSelected = advancedOrderTypeInput?.value?.value?.id == advancedInputId.advancedOrderTypeValueId.contingent
                if (isContingentSelected) {
                    advancedOrderTypeInput?.resetValue()
                }
            }
        }
    }
}

internal fun RuleListBuilder.resetAdvancedOrderTypeOnAdvancedSwitchTurnedOff(inputId: WithAdvancedTradeFormInputId) {
    rule {
        requireInput<SwitchInput>(inputId.advancedOrder).value.observeForever {
            requireAnyInput(inputId.advancedOrderType).resetValue()
        }
    }
}
