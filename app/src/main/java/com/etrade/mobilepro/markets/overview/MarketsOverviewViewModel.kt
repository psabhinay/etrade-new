package com.etrade.mobilepro.markets.overview

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.common.di.LocalDynamicScreen
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.home.overview.edit.OverviewWidgetRepo
import com.etrade.mobilepro.markets.edit.MarketsOverview
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.MarketsBloombergTvEventAnalyticsParams
import com.etrade.mobilepro.tracking.params.MarketsCustomizeEventAnalyticsParams
import com.etrade.mobilepro.tracking.params.MarketsIndicesTapEventAnalyticsParams
import com.etrade.mobilepro.util.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.rx2.asFlow
import javax.inject.Inject

class MarketsOverviewViewModel @Inject constructor(
    @LocalDynamicScreen
    private val dynamicScreenRepo: DynamicScreenRepo,
    private val tracker: Tracker,
    @MarketsOverview private val widgetsRepo: OverviewWidgetRepo
) : ViewModel() {

    private val viewStates = MutableLiveData<ViewState>()
    private var job: Job? = null
    val lifecycleObserverSet = mutableSetOf<LifecycleObserver>()
    var shouldRefresh: Boolean = true

    @ExperimentalCoroutinesApi
    fun loadMarketOverview(userSessionState: SessionStateChange) {
        job?.cancel()
        job = viewModelScope.launch {
            viewStates.value = ViewState.Loading
            chooseScreenDataSource(userSessionState)
                .catch {
                    viewStates.postValue(ViewState.Error)
                }
                .collect { response ->
                    when (response) {
                        is Resource.Success -> {
                            viewStates.postValue(ViewState.Success(response.data ?: emptyList()))
                        }
                        is Resource.Failed -> {
                            viewStates.postValue(ViewState.Error)
                        }
                    }
                }
        }
    }

    fun getViewState(): LiveData<ViewState> = viewStates

    @ExperimentalCoroutinesApi
    private suspend fun chooseScreenDataSource(userSessionState: SessionStateChange): Flow<Resource<List<GenericLayout>>> {
        return when (userSessionState) {
            SessionStateChange.AUTHENTICATED -> {
                val widgetList = widgetsRepo.getWidgets()
                    .filter { it.isChecked }
                    .map { it.widgetName }
                dynamicScreenRepo.getScreen(ServicePath.AuthenticatedMarketOverview(widgetList))
            }
            SessionStateChange.UNAUTHENTICATED -> {
                dynamicScreenRepo.getScreen(ServicePath.MarketOverview)
            }
        }.asFlow()
    }

    fun trackBloombergTv() {
        tracker.event(MarketsBloombergTvEventAnalyticsParams())
    }

    fun trackCustomize() {
        tracker.event(MarketsCustomizeEventAnalyticsParams())
    }

    fun trackIndices() {
        tracker.event(MarketsIndicesTapEventAnalyticsParams())
    }

    sealed class ViewState {

        object Loading : ViewState()

        object Error : ViewState()

        data class Success(val result: List<GenericLayout>) : ViewState()
    }
}
