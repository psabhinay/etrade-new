package com.etrade.mobilepro.common.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.viewpager2.widget.ViewPager2

class FragmentPageListener(
    private val parentFragment: Fragment,
    private val onFragmentSelected: ((Int) -> Unit)?
) : ViewPager2.OnPageChangeCallback() {
    private var selectedPosition: Int? = null

    val selectedFragment: Fragment?
        get() = selectedPosition?. let { parentFragment.childFragmentManager.getViewPagerFragment(it) }

    override fun onPageSelected(position: Int) {
        super.onPageSelected(position)
        selectedPosition = position
        onFragmentSelected?.invoke(position)
    }
}

fun FragmentManager.getViewPagerFragment(position: Int): Fragment? =
    findFragmentByTag("f$position")

fun ViewPager2.setupFragmentPageListener(
    parentFragment: Fragment,
    onFragmentSelected: ((Int) -> Unit)? = null
): FragmentPageListener {
    val listener = FragmentPageListener(parentFragment, onFragmentSelected)
    registerOnPageChangeCallback(listener)

    parentFragment.viewLifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {
        @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        fun onDestroy() {
            unregisterOnPageChangeCallback(listener)
            parentFragment.viewLifecycleOwner.lifecycle.removeObserver(this)
        }
    })
    return listener
}
