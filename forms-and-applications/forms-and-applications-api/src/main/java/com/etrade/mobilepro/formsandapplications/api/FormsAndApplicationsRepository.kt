package com.etrade.mobilepro.formsandapplications.api

import com.etrade.mobilepro.common.result.ETResult

/**
 * Repository provides access to forms and applications data.
 */
interface FormsAndApplicationsRepository {

    /**
     * @return list of forms and application references
     */
    suspend fun getFormsAndApplications(): ETResult<List<FormsAndApplicationsReference>>
}
