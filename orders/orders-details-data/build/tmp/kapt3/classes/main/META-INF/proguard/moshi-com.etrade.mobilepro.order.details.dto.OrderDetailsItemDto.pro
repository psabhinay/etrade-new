-if class com.etrade.mobilepro.order.details.dto.OrderDetailsItemDto
-keepnames class com.etrade.mobilepro.order.details.dto.OrderDetailsItemDto
-if class com.etrade.mobilepro.order.details.dto.OrderDetailsItemDto
-keep class com.etrade.mobilepro.order.details.dto.OrderDetailsItemDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
