package com.etrade.mobilepro.navigation

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.navOptions
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.home.COMPLETE_VIEW
import com.etrade.mobilepro.home.FRAGMENT
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.orders.OrderBottomSheetFragmentDirections
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.portfolio.extendedsummary.ExtendedPortfolioSummaryBottomSheetFragmentDirections
import com.etrade.mobilepro.portfolio.model.PortfolioExtendedSummaryModel
import com.etrade.mobilepro.portfolio.taxlot.PortfolioTaxLotBottomSheetFragmentDirections
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.quotelookup.QuoteLookupActivity
import com.etrade.mobilepro.quotes.QUOTE_BOTTOM_SHEET_TAG
import com.etrade.mobilepro.quotes.QuoteBottomSheetFragment
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import org.slf4j.LoggerFactory
import javax.inject.Inject

internal class MainNavigationImpl @Inject constructor(private val userViewModel: UserViewModel) :
    MainNavigation {

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    override fun navigateToCompleteView(activity: FragmentActivity) {
        logger.debug("navigateToCompleteView from $activity")
        if (activity is MainActivity) {
            findNavController(activity, R.id.nav_host_fragment).navigate(
                MainGraphDirections.actionHomeToCompleteview(),
                navOptions {
                    popUpTo(R.id.completeviewFragment) { inclusive = true }
                }
            )
        } else {
            activity.startActivity(
                Intent(activity, MainActivity::class.java).apply {
                    putExtra(FRAGMENT, COMPLETE_VIEW)
                    flags = (Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                }
            )
        }
    }

    override fun navigateToQuoteDetails(activity: FragmentActivity?, bundle: Bundle) {
        if (activity is QuoteLookupActivity) {
            activity.showBottomSheet(bundle)
        } else {
            activity?.findNavController(R.id.nav_host_fragment)?.let { navController ->
                navigateToQuoteDetails(navController, bundle)
            }
        }
    }

    override fun navigateToQuoteDetails(navController: NavController, bundle: Bundle) {
        if (navController.currentDestination?.id == R.id.quoteBottomSheetFragment) {
            navController.popBackStack()
        }
        navController.navigate(R.id.quoteBottomSheetFragment, bundle)
    }

    override fun launchBottomSheetOrders(
        activity: FragmentActivity?,
        accountId: String,
        oderStatus: OrderStatus
    ) {
        activity?.findNavController(R.id.nav_host_fragment)?.navigate(
            OrderBottomSheetFragmentDirections.actionGlobalOrdersBottomSheetFragment(
                accountId, oderStatus
            )
        )
    }

    override fun launchExtendedPortfolioBottomSheet(activity: FragmentActivity?, info: Any) {
        activity?.findNavController(R.id.nav_host_fragment)?.navigate(
            ExtendedPortfolioSummaryBottomSheetFragmentDirections.actionPortfolioExtendedSummaryBottomSheetFragment(
                info as PortfolioExtendedSummaryModel
            )
        )
    }

    override fun launchBottomSheetPortfolioTaxLot(
        activity: FragmentActivity?,
        positionId: String,
        accountUuid: String,
        symbol: String,
        portfolioTableViewType: PortfolioTableViewType,
        positionTableMode: PositionTableMode
    ) {
        activity?.findNavController(R.id.nav_host_fragment)?.navigate(
            PortfolioTaxLotBottomSheetFragmentDirections.actionPortfolioTaxLotSummaryBottomSheetFragment(
                positionId,
                accountUuid,
                symbol,
                portfolioTableViewType,
                positionTableMode
            )
        )
    }

    override fun navigateToAccount(navController: NavController, accountId: String) {
        userViewModel.updateSelectedAccountByAccountId(accountId)
        val direction =
            MainGraphDirections.actionToSelectedAccountTabIndexOrTitle(tabIndex = 0, accountId = accountId)
        navController.navigate(direction)
    }

    override fun launchQuoteDetailsBottomSheet(activity: FragmentActivity?, bundle: Bundle) {
        activity?.supportFragmentManager?.let {
            if (it.findFragmentByTag(QUOTE_BOTTOM_SHEET_TAG) == null) {
                val fragment = it.fragmentFactory.instantiateFragment(
                    QuoteBottomSheetFragment::class.java,
                    bundle
                )
                (fragment as? QuoteBottomSheetFragment)?.show(
                    activity.supportFragmentManager,
                    QUOTE_BOTTOM_SHEET_TAG
                )
            }
        }
    }

    override fun openPdf(navController: NavController, url: String) {
        navController.navigate(
            MainGraphDirections.actionOpenPdfActivity(
                url = url
            )
        )
    }
}
