package com.etrade.mobilepro.screener.api.datasource

import com.etrade.mobilepro.screener.api.Industry
import com.etrade.mobilepro.screener.api.IndustryType

interface IndustryTypeDataSource {
    fun getAllTypesItem(): IndustryType
    fun getIndustryTypes(): List<IndustryType>
    fun getAllIndustriesItem(): Industry
    fun getIndustriesOfType(type: IndustryType): List<Industry>
}
