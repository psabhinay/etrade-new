package com.etrade.mobilepro.screener.api.filter.stock

import com.etrade.mobilepro.screener.api.Industry
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.Sector

data class MarketSegmentFilter(
    val sector: Sector,
    val industry: Industry,
    val microCap: Boolean = false,
    val smallCap: Boolean = false,
    val midCap: Boolean = false,
    val largeCap: Boolean = false,
    val megaCap: Boolean = false,

    // auxiliary, doest not take part in requests
    val selectedSectorPosition: Int = 0,
    val selectedIndustryPosition: Int = 0
) : ScreenerFilter {
    fun isCapSelected(): Boolean = microCap || smallCap || midCap || largeCap || megaCap

    override fun isEmpty(): Boolean = sector.isAllItemsElement && industry.isAllItemsElement && !isCapSelected()

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.STOCK
}
