package com.etrade.mobilepro.quotelookup

class SearchException(cause: String? = null) : RuntimeException(cause)
