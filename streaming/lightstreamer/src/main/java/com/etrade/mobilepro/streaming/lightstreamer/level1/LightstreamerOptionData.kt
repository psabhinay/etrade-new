package com.etrade.mobilepro.streaming.lightstreamer.level1

import com.etrade.eo.streaming.UpdateInfo
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.OptionData
import org.threeten.bp.LocalDate

/**
 * Level 1 quotes data for options.
 */
class LightstreamerOptionData constructor(
    updateInfo: UpdateInfo,
    fields: Set<Level1Field>
) : OptionData {

    // region Level1Data
    override val ask: String? = updateInfo.getValue(Level1Column.ASK)
    override val askExchange: String? = updateInfo.getValue(Level1Column.ASK_EXCHANGE)
    override val askSize: String? = updateInfo.getValue(Level1Column.ASK_SIZE)
    override val bid: String? = updateInfo.getValue(Level1Column.BID)
    override val bidExchange: String? = updateInfo.getValue(Level1Column.BID_EXCHANGE)
    override val bidSize: String? = updateInfo.getValue(Level1Column.BID_SIZE)
    override val change: String? = updateInfo.getValue(Level1Column.LAST_CHANGE)
    override val changePercent: String? = updateInfo.getValue(Level1Column.LAST_CHANGE_PCT)
    override val closingMark: String? = updateInfo.getValue(Level1Column.CLOSING_MARK)
    override val dayHigh: String? = updateInfo.getValue(Level1Column.HIGH)
    override val dayLow: String? = updateInfo.getValue(Level1Column.LOW)
    override val earningsPerShare: String? = updateInfo.getValue(Level1Column.EPS)
    override val lastPrice: String? = updateInfo.getValue(Level1Column.LAST)
    override val lastPriceExchange: String? = updateInfo.getValue(Level1Column.EXCHANGE_CODE)
    override val lastVolume: String? = updateInfo.getValue(Level1Column.LAST_VOLUME)
    override val mark: String? = updateInfo.getValue(Level1Column.MARK)
    override val openPrice: String? = updateInfo.getValue(Level1Column.OPEN)
    override val previousClosePrice: String? = updateInfo.getValue(Level1Column.PREVIOUS_CLOSE)
    override val priceToEarnings: String?
    override val tickIndicator: Short? = updateInfo.getTickIndicator(Level1Column.LAST)
    override val timestampSinceEpoch: Long? = updateInfo.getTimeValue()
    override val volume: String? = updateInfo.getValue(Level1Column.ACCUM_VOLUME)
    override val yearHigh: String? = updateInfo.getValue(Level1Column.YEAR_HIGH)
    override val yearHighDate: LocalDate? = updateInfo.getYearDate(Level1Column.YEAR_HIGH_DATE)
    override val yearLow: String? = updateInfo.getValue(Level1Column.YEAR_LOW)
    override val yearLowDate: LocalDate? = updateInfo.getYearDate(Level1Column.YEAR_LOW_DATE)
    // endregion

    // region OptionData
    override val openInterest: String? = updateInfo.getValue(Level1Column.OPEN_INTEREST)
    override val intrinsicValue: String? = updateInfo.getValue(Level1Column.INTRINSIC_VALUE)
    override val delta: String? = updateInfo.getValue(Level1Column.DELTA)
    override val gamma: String? = updateInfo.getValue(Level1Column.GAMMA)
    override val theta: String? = updateInfo.getValue(Level1Column.THETA)
    override val vega: String? = updateInfo.getValue(Level1Column.VEGA)
    override val rho: String? = updateInfo.getValue(Level1Column.RHO)
    override val impliedVolatility: String? = updateInfo.getValue(Level1Column.IMPLIED_VOLATILITY)
    // endregion

    init {
        priceToEarnings = calculatePriceToEarnings(fields) // Do not join with assignment
    }
}
