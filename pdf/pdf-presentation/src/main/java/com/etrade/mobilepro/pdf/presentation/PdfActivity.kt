package com.etrade.mobilepro.pdf.presentation

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentFactory
import androidx.navigation.navArgs
import com.etrade.mobilepro.baseactivity.ActivityNetworkConnectionDelegate
import com.etrade.mobilepro.baseactivity.ActivitySessionDelegate
import com.etrade.mobilepro.common.setToolbarIcon
import com.etrade.mobilepro.pdf.presentation.databinding.PdfActivityBinding
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class PdfActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var fragmentInjectionFactory: FragmentFactory

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var activityNetworkConenectionDelegate: ActivityNetworkConnectionDelegate

    @Inject
    lateinit var activitySessionDelegate: ActivitySessionDelegate

    private val args: PdfActivityArgs by navArgs()

    private lateinit var binding: PdfActivityBinding

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        supportFragmentManager.fragmentFactory = fragmentInjectionFactory
        super.onCreate(savedInstanceState)

        binding = PdfActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupToolbar()

        if (savedInstanceState == null) {
            instantiateFragment()
        }

        activityNetworkConenectionDelegate.initNetworkConnectivityBanner(this)
        activitySessionDelegate.observeTimeoutEvent(this)
    }

    private fun setupToolbar() {
        binding.toolbar.setToolbarIcon(false, isWhite = true)
        binding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun instantiateFragment() {
        fragmentInjectionFactory.instantiateFragment(PdfFragment::class.java, args.toBundle()).apply {
            supportFragmentManager.beginTransaction()
                .add(R.id.pdf_fragment_container, this)
                .commit()
        }
    }
}
