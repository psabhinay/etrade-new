package com.etrade.mobilepro.checkdeposit.usecases.api

interface ImageUploadResult {
    /**
     * The token to reference the check image when deposit is submitted
     */
    val tokenId: String
}
