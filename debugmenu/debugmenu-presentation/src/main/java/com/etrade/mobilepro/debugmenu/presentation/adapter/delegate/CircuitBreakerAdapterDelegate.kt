package com.etrade.mobilepro.debugmenu.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder.CircuitBreakerViewHolder
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemCircuitBreakerBinding
import com.etrade.mobilepro.debugmenu.presentation.item.CircuitBreakerItem
import com.etrade.mobilepro.debugmenu.presentation.item.DebugMenuItem
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class CircuitBreakerAdapterDelegate(
    private val listener: (circuitBreakerEnabled: Boolean) -> Unit
) : AbsListItemAdapterDelegate<CircuitBreakerItem, DebugMenuItem, CircuitBreakerViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): CircuitBreakerViewHolder {
        return CircuitBreakerViewHolder(parent.viewBinding(DebugmenuItemCircuitBreakerBinding::inflate), listener)
    }

    override fun isForViewType(item: DebugMenuItem, items: MutableList<DebugMenuItem>, position: Int): Boolean =
        item is CircuitBreakerItem

    override fun onBindViewHolder(item: CircuitBreakerItem, holder: CircuitBreakerViewHolder, payloads: MutableList<Any>) =
        holder.bindTo(item)
}
