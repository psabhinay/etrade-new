package com.etrade.mobilepro.searching.validation

import android.annotation.SuppressLint
import com.etrade.mobilepro.searchingapi.SearchQueryValidator
import com.etrade.mobilepro.searchingapi.items.SearchQuery

class DefaultSearchQueryValidator : SearchQueryValidator {
    private val allowedCapChars = 'A'..'Z'
    private val allowedChars = 'a'..'z'
    private val digits = '0'..'9'
    private val specialChars = setOf('.', ' ')

    @SuppressLint("DefaultLocale")
    override fun makeValidOrEmpty(query: SearchQuery): SearchQuery {
        val inputQueryValue = query.value.trim()
        val outputQueryValue = StringBuilder()
        inputQueryValue.forEach { char ->
            val isLetter = char in allowedChars || char in allowedCapChars
            if (isLetter || char in digits || char in specialChars) {
                outputQueryValue.append(char)
            }
        }
        return SearchQuery(type = query.type, value = outputQueryValue.toString().uppercase())
    }
}
