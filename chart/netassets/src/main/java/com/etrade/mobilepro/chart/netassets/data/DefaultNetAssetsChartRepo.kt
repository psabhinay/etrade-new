package com.etrade.mobilepro.chart.netassets.data

import com.etrade.mobilepro.chart.data.ChartRequestDto
import com.etrade.mobilepro.chart.data.ChartRequestValueDto
import com.etrade.mobilepro.chart.data.toChartLine
import com.etrade.mobilepro.chart.netassets.model.NetAssetsChartData
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.execute
import retrofit2.Retrofit
import javax.inject.Inject

class DefaultNetAssetsChartRepo @Inject constructor(
    @MobileTrade retrofit: Retrofit
) : NetAssetsChartRepo {

    private val chartApiClient: NetAssetsChartApiClient = retrofit.create(NetAssetsChartApiClient::class.java)

    override suspend fun getNetAssetsChart(accounts: List<String>): Resource<NetAssetsChartData> {
        return execute({ chartApiClient.getNetAssetsChart(createRequest(accounts)) }) { body ->
            body.data
                ?.overview
                ?.accounts
                ?.map { it.toChartLine() }
                ?.let { NetAssetsChartData(it) }
        }
    }

    private fun createRequest(accounts: List<String>): ChartRequestDto = ChartRequestDto(ChartRequestValueDto(accounts, emptyList(), "6M"))
}
