package com.etrade.mobilepro.apiguard

import okhttp3.Interceptor
import okhttp3.Response

/**
 * An interceptor of API calls, that gathers required telemetry for API guard. Use it only for those API calls, that require filtering of unwanted traffic.
 */
class ApiGuardInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = ApiGuard.transform(chain.request())
        val response = chain.proceed(request)
        ApiGuard.parse(response)
        return response
    }
}
