package com.etrade.mobilepro.alerts.presentation.setalert

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.alerts.api.model.AlertValueType
import com.etrade.mobilepro.alerts.api.model.NewsAlertValueType
import com.etrade.mobilepro.alerts.api.model.SetAlertResponse
import com.etrade.mobilepro.alerts.api.model.SetNewsAlertResponse
import com.etrade.mobilepro.alerts.api.repo.AlertsRepo
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.AlertInputValueDependenciesProvider
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.AlertSettingsHolder
import com.etrade.mobilepro.alerts.presentation.setalert.setvalue.UserInputValidator
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.quote.mapper.MobileQuoteWidgetItemMapper
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem
import com.etrade.mobilepro.searchingapi.SearchInteractor
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.tracking.SCREEN_TITLE_SET_ALERTS
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.SetMarketAlertAnalyticsParams
import com.etrade.mobilepro.tracking.params.SetNewsAlertAnalyticsParams
import com.etrade.mobilepro.util.android.SingleLiveEvent
import com.etrade.mobilepro.util.delegate.consumable
import com.etrade.mobilepro.util.safeParseBigDecimal
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(SetAlertViewModel::class.java)
class SetAlertViewModel @Inject constructor(
    val inputValueDepsProvider: AlertInputValueDependenciesProvider,
    private val settingsHolder: AlertSettingsHolder,
    private val commonValidator: UserInputValidator,
    private val mobileQuoteRepo: MobileQuoteRepo,
    private val quoteMapper: MobileQuoteWidgetItemMapper,
    private val alertsRepo: AlertsRepo,
    private val user: User,
    searchInteractor: SearchInteractor,
    private val tracker: Tracker
) : ViewModel(), ScreenViewModel {
    private val compositeDisposable = CompositeDisposable()
    // we need another one CompositeDisposable for retries, as we can run into situation, when while some quote fetch request is in ongoing state,
    // we select next symbol and then we need to cancel the ongoing.
    // We can not chain network fetching directly to searchInteractor due to possible onErrors, so this is the way.
    private val retriesDisposable = CompositeDisposable()
    private val _viewState = MutableLiveData<ViewState>().apply { value = ViewState.InitialSearchInProgress }
    private val _widgetViewState = MutableLiveData<WidgetViewState>().apply { value = WidgetViewState.Success(quoteWidgetItem = null, pe = "") }
    val viewState: LiveData<ViewState>
        get() = _viewState
    val widgetViewState: LiveData<WidgetViewState>
        get() = _widgetViewState
    private val _needOpenQuoteLookup = SingleLiveEvent<Boolean>()
    private val _needResetViews = SingleLiveEvent<Boolean>()
    private val _currentSymbol = MutableLiveData<SearchResultItem.Symbol?>()
    private val _needLeaveBlankPage = SingleLiveEvent<Boolean>().apply { value = false }
    val needResetViews: LiveData<Boolean> = _needResetViews
    val needOpenQuoteLookup: LiveData<Boolean> = _needOpenQuoteLookup
    val currentSymbol: LiveData<SearchResultItem.Symbol?> = _currentSymbol
    val needLeaveBlankPage: LiveData<Boolean> = _needLeaveBlankPage

    init {
        compositeDisposable.add(
            searchInteractor
                .searchResultItemUntilChangedAsObservable(searchType = SearchType.SYMBOL)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = {
                        when (it) {
                            is SearchResultItem.Symbol -> {
                                settingsHolder.resetContents()
                                _needResetViews.value = true
                                setSymbol(it)
                            }
                            SearchResultItem.EmptySearchResultItem -> {
                                _needLeaveBlankPage.value = true
                            }
                        }
                    },
                    onError = {
                        logger.debug(it.localizedMessage)
                    }
                )
        )
    }

    override val screenName: String? = SCREEN_TITLE_SET_ALERTS

    fun setSymbol(symbol: SearchResultItem.Symbol) {
        if (currentSymbol.value != symbol) {
            _currentSymbol.value = symbol
            fetchQuote()
        }
    }

    fun setAlertButtonState(): LiveData<Pair<Boolean, ViewState>> =
        commonValidator.userInputIsValid(settingsHolder.inputValues, settingsHolder.switchedValues, inputValueDepsProvider).combineLatest(viewState)

    fun inputValueHolder(type: AlertValueType): MutableLiveData<String> = settingsHolder.inputValueHolder(type)

    fun switchValueHolder(type: NewsAlertValueType): MutableLiveData<Boolean> = settingsHolder.switchValueHolder(type)

    fun openQuoteLookup() {
        _needOpenQuoteLookup.value = true
        _viewState.value = ViewState.InitialSearchInProgress
    }

    fun retryAlert() = setAlert()
    fun retryWidget() = fetchQuote()

    @Suppress("LongMethod")
    fun setAlert() {
        val symbol = currentSymbol.value?.title ?: return
        val token = user.token ?: return
        val customerId = user.session?.aatId ?: return

        _viewState.value = ViewState.Loading
        val setAlertValues = settingsHolder.inputValues
            .mapValues { it.value.value ?: "" }
            .filterValues { it.isNotBlank() } // only fields with values are needed
        val setNewsAlertValues = settingsHolder.switchedValues
            .mapValues { it.value.value ?: false }
            .filterValues { it } // only turned On values are needed
        // we can show only one retry snack bar, thus we have to do requests sequentially, and retry them together
        compositeDisposable.add(
            Single
                .just(Pair<SetAlertResponse?, SetNewsAlertResponse?>(null, null))
                .flatMap { pair ->
                    if (setAlertValues.values.isNotEmpty()) {
                        return@flatMap alertsRepo
                            .setAlert(token, symbol, setAlertValues)
                            .map {
                                Pair<SetAlertResponse?, SetNewsAlertResponse?>(it, null)
                            }
                    } else {
                        return@flatMap Single.just(pair)
                    }
                }
                .flatMap { pair ->
                    if (setNewsAlertValues.isNotEmpty()) {
                        return@flatMap alertsRepo
                            .setNewsAlert(token, symbol, setNewsAlertValues)
                            .map { setNewsAlertResponse ->
                                Pair<SetAlertResponse?, SetNewsAlertResponse?>(pair.first, setNewsAlertResponse)
                            }
                    } else {
                        return@flatMap Single.just(Pair<SetAlertResponse?, SetNewsAlertResponse?>(pair.first, null))
                    }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onSuccess = {
                        if (it.second?.hasError == true) {
                            _viewState.value = ViewState.Error.AlertRelated(it.second?.errorMessage)
                        } else {
                            _viewState.value = ViewState.Success(it.first, it.second)
                        }

                        if (it.first?.hasError == false) {
                            tracker.event(SetMarketAlertAnalyticsParams(symbol, customerId, setAlertValues))
                        }

                        if (it.second?.hasError == false) {
                            tracker.event(SetNewsAlertAnalyticsParams(symbol, customerId, setNewsAlertValues))
                        }
                    },
                    onError = {
                        _viewState.value = ViewState.Error.NetworkRelated
                    }
                )
        )
    }

    fun resetCurrentSymbol() {
        settingsHolder.resetContents()
        _currentSymbol.value = null
        _widgetViewState.value = WidgetViewState.Success(quoteWidgetItem = null, pe = "")
    }

    override fun onCleared() {
        retriesDisposable.clear()
        compositeDisposable.clear()
        super.onCleared()
    }

    private fun fetchQuote() {
        _widgetViewState.value = WidgetViewState.Loading
        val symbol = currentSymbol.value ?: return
        retriesDisposable.clear()
        retriesDisposable.add(
            mobileQuoteRepo
                .loadMobileQuote(symbol.title)
                .map {
                    val pe = MarketDataFormatter.formatNumberWithZeroToTwoDecimals(it.pe.safeParseBigDecimal())
                    SymbolData(quoteMapper.map(it), it.typeCode.isEquity || it.typeCode.isIndex, pe)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                    onNext = {
                        if (it.isAllowed) {
                            _widgetViewState.value = WidgetViewState.Success(it.quoteItem, it.pe)
                        } else {
                            _widgetViewState.value = WidgetViewState.Error.Type
                        }
                    },
                    onError = {
                        _widgetViewState.value = WidgetViewState.Error.Loading
                        logger.debug(it.localizedMessage)
                    }
                )
        )
    }

    sealed class ViewState {
        object Loading : ViewState()

        object InitialSearchInProgress : ViewState()

        data class Success(
            val setAlertResponse: SetAlertResponse?,
            val setNewsAlertResponse: SetNewsAlertResponse?
        ) : ViewState()

        sealed class Error : ViewState() {
            class AlertRelated(message: String?) : Error() {
                val consumableErrorMessage by consumable(message)
            }
            object NetworkRelated : Error()
        }
    }

    sealed class WidgetViewState {
        object Loading : WidgetViewState()
        data class Success(
            val quoteWidgetItem: QuoteWidgetItem?,
            val pe: String
        ) : WidgetViewState()

        sealed class Error : WidgetViewState() {
            object Loading : Error()
            object Type : Error()
        }
    }

    private class SymbolData(
        val quoteItem: QuoteWidgetItem,
        val isAllowed: Boolean,
        val pe: String
    )
}
