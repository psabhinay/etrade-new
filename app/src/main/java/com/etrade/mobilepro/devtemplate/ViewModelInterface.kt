package com.etrade.mobilepro.devtemplate

import com.etrade.mobilepro.dynamicui.api.DynamicViewModel

interface ViewModelInterface {
    fun fetchDataFromServer()
    sealed class ViewState {
        object Loading : ViewState()
        data class Error(val message: String?) : ViewState()
        data class Success(val results: MutableList<DynamicViewModel>?) : ViewState()
    }
}
