package com.etrade.mobilepro.orders.viewmodel

import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobilepro.order.details.router.GeneralOrderDetailsArgument
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.order.details.router.StockPlanOrderDetailsArgument
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.extensions.resolveQuantityIfPartial
import com.etrade.mobilepro.orders.list.api.DefaultOrderItem
import com.etrade.mobilepro.orders.list.api.OrderItem
import com.etrade.mobilepro.orders.list.api.OrderParams
import com.etrade.mobilepro.orders.list.api.StockPlanOrderItem
import com.etrade.mobilepro.stockplan.AccountIndexHandler
import com.etrade.mobilepro.stockplan.AccountTypeHandler
import org.threeten.bp.LocalDate
import javax.inject.Inject

class OrdersHelper @Inject constructor(
    private val accountTypeHandler: AccountTypeHandler,
    private val accountIndexHandler: AccountIndexHandler
) {
    internal val isEmployeeStockPlan get() = accountTypeHandler.getAccountType() == AccountType.ESP
    internal var selectedOrderItem: OrderItem? = null

    internal fun buildOrdersParams(
        accountId: String,
        status: OrderStatus,
        monthsBack: Long? = null,
        forceRefresh: Boolean
    ): OrderParams {
        val toDate: LocalDate = LocalDate.now()
        val fromDate: LocalDate? = monthsBack?.let { toDate.minusMonths(it) }

        return OrderParams(
            accountId = accountId,
            fromDate = fromDate,
            toDate = toDate,
            orderStatus = status,
            isStockPlan = isEmployeeStockPlan,
            accountIndex = accountIndexHandler.getAccountIndex(),
            forceRefresh = forceRefresh
        )
    }

    internal fun buildOrdersParams(
        accountId: String,
        status: OrderStatus,
        fromDate: LocalDate,
        toDate: LocalDate,
        forceRefresh: Boolean = false
    ) = OrderParams(
        accountId = accountId,
        fromDate = fromDate,
        toDate = toDate,
        orderStatus = status,
        isStockPlan = isEmployeeStockPlan,
        accountIndex = accountIndexHandler.getAccountIndex(),
        forceRefresh = forceRefresh
    )

    internal fun buildOrderDetailArgument(accountId: String, orderId: String? = null, snackBarMessage: String? = null): OrderDetailsArgument? {
        return when (val item = selectedOrderItem) {
            is DefaultOrderItem -> GeneralOrderDetailsArgument(
                orderId = orderId ?: item.orderNumber,
                orderStatus = item.orderStatus,
                symbols = item.symbols,
                accountId = accountId,
                instrumentType = item.instrumentType,
                orderType = item.orderType,
                originalQuantity = item.orderDescriptions.first().fillQuantityValue.resolveQuantityIfPartial(),
                advancedOrderSymbol = item.advancedOrderSymbol,
                advancedOrderDisplaySymbol = item.advancedOrderDisplaySymbol,
                snackBarMessage = snackBarMessage
            )
            is StockPlanOrderItem -> StockPlanOrderDetailsArgument(
                orderId = item.orderNumber,
                orderStatus = item.orderStatus,
                symbols = item.symbols
            )
            else -> null
        }
    }
}
