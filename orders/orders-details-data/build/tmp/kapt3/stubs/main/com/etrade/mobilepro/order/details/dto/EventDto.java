package com.etrade.mobilepro.order.details.dto;

import com.etrade.mobilepro.backends.neo.BaseDataDto;
import com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto;
import com.etrade.mobilepro.orders.data.dto.AuditInfoDto;
import com.etrade.mobilepro.orders.data.dto.HeaderDto;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0087\b\u0018\u00002\u00020\u0001B\u001f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0001\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\u000e\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u0016"}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/EventDto;", "", "header", "Lcom/etrade/mobilepro/order/details/dto/EventHeaderDto;", "eventLegDetails", "", "Lcom/etrade/mobilepro/order/details/dto/EventLegDetailDto;", "(Lcom/etrade/mobilepro/order/details/dto/EventHeaderDto;Ljava/util/List;)V", "getEventLegDetails", "()Ljava/util/List;", "getHeader", "()Lcom/etrade/mobilepro/order/details/dto/EventHeaderDto;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "orders-details-data"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class EventDto {
    @org.jetbrains.annotations.NotNull()
    private final com.etrade.mobilepro.order.details.dto.EventHeaderDto header = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.etrade.mobilepro.order.details.dto.EventLegDetailDto> eventLegDetails = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.EventDto copy(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "header")
    com.etrade.mobilepro.order.details.dto.EventHeaderDto header, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "eventLegDetails")
    java.util.List<com.etrade.mobilepro.order.details.dto.EventLegDetailDto> eventLegDetails) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public EventDto(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "header")
    com.etrade.mobilepro.order.details.dto.EventHeaderDto header, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "eventLegDetails")
    java.util.List<com.etrade.mobilepro.order.details.dto.EventLegDetailDto> eventLegDetails) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.EventHeaderDto component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.EventHeaderDto getHeader() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.order.details.dto.EventLegDetailDto> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.order.details.dto.EventLegDetailDto> getEventLegDetails() {
        return null;
    }
}