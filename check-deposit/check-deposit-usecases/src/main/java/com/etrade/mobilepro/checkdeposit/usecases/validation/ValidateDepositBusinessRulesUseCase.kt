package com.etrade.mobilepro.checkdeposit.usecases.validation

import com.etrade.mobilepro.checkdeposit.deposit.service.RemoteCheckDepositService
import com.etrade.mobilepro.checkdeposit.validate.ValidateDepositBusinessRulesDto
import com.etrade.mobilepro.checkdeposit.validate.ValidateDepositBusinessRulesValueDto
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.util.UseCase

interface ValidateDepositBusinessRulesUseCase : UseCase<ValidateDepositBusinessRulesRequest, ETResult<ValidationResult>>

class ValidateDepositBusinessRulesUseCaseImpl(
    private val service: RemoteCheckDepositService
) : ValidateDepositBusinessRulesUseCase {

    override suspend fun execute(parameter: ValidateDepositBusinessRulesRequest): ETResult<ValidationResult> = runCatchingET {
        service.validateDepositBusinessRules(
            ValidateDepositBusinessRulesDto(
                ValidateDepositBusinessRulesValueDto(
                    instNo = parameter.instNo,
                    accountNo = parameter.accountNo,
                    depositAmount = parameter.depositAmount,
                    iraDepositType = parameter.iraDepositType
                )
            )
        ).doSuccessful {
            if (it.hasNoMessages()) {
                ValidationResult.Valid
            } else {
                ValidationResult.Invalid(it.getFirstMessageText(), it.getFirstMessageCode())
            }
        }
    }
}
