package com.etrade.mobilepro.watchlist.mapper

import com.etrade.mobilepro.watchlist.dto.WatchlistDto
import com.etrade.mobilepro.watchlistapi.Watchlist

internal class WatchlistDtoMapper {

    /**
     * Maps [jsonDto] to a [Watchlist] model.
     *
     * @param jsonDto JSON representation of the model
     *
     * @return a watchlist
     */
    fun map(jsonDto: WatchlistDto): Watchlist {
        return Watchlist(
            id = jsonDto.watchListId,
            name = jsonDto.watchListName,
            uuid = jsonDto.watchListUuid
        )
    }
}
