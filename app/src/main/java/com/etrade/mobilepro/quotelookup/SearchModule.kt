package com.etrade.mobilepro.quotelookup

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.core.app.ActivityOptionsCompat
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.toolbarView
import com.etrade.mobilepro.positions.api.PortfolioEntryRepo
import com.etrade.mobilepro.positions.data.repo.DefaultPortfolioEntryRepo
import com.etrade.mobilepro.positions.data.rest.PortfolioApiClient
import com.etrade.mobilepro.quotelookup.interaction.DefaultSearchInteractor
import com.etrade.mobilepro.searching.dto.SymbolLookupResponseDto
import com.etrade.mobilepro.searching.history.DefaultSelectionHistoryRepo
import com.etrade.mobilepro.searching.history.HistoryMigrationModule
import com.etrade.mobilepro.searching.history.HistoryRealmProvider
import com.etrade.mobilepro.searching.mapper.SymbolLookupMapper
import com.etrade.mobilepro.searching.rest.SymbolSearchApiClient
import com.etrade.mobilepro.searching.symbol.DefaultSymbolLookupRepo
import com.etrade.mobilepro.searching.symbol.DefaultSymbolSearchRepo
import com.etrade.mobilepro.searching.symbol.cache.CachedSymbolsSearchController
import com.etrade.mobilepro.searching.symbol.cache.CachedSymbolsSearchDataDelegate
import com.etrade.mobilepro.searching.validation.DefaultSearchQueryValidator
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.searchingapi.SearchInteractor
import com.etrade.mobilepro.searchingapi.SearchQueryValidator
import com.etrade.mobilepro.searchingapi.SelectionHistoryRepo
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.SymbolLookupRepo
import com.etrade.mobilepro.searchingapi.SymbolSearchRepo
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SymbolLookUpResult
import dagger.Module
import dagger.Provides
import org.threeten.bp.Duration
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [HistoryMigrationModule::class])
class SearchModule {

    @Provides
    @Singleton
    fun provideSearchIntentFactory(): SearchIntentFactory {
        return object : SearchIntentFactory {
            override fun createIntent(context: Context, params: StartSearchParams, reorderToFront: Boolean): Intent {
                return context.createSearchIntent(params, reorderToFront)
            }

            override fun createSearchIntentAndOptions(
                activity: Activity,
                params: StartSearchParams
            ): Pair<Intent, ActivityOptionsCompat?> {
                return activity.toolbarView.getSearchIntentAndOptions(activity, params)
            }
        }
    }

    @Provides
    @Singleton
    internal fun provideSymbolSearchRepo(
        cacheController: SimpleCachedResource.Controller<List<SearchResultItem.Symbol>>,
        dataDelegate: SimpleCachedResource.DataSourceDelegate<List<SearchResultItem.Symbol>, SearchQuery>,
        queryValidator: SearchQueryValidator
    ): SymbolSearchRepo {
        return DefaultSymbolSearchRepo(cacheController, dataDelegate, queryValidator)
    }

    @Provides
    @Singleton
    internal fun provideSelectionHistoryRepo(historyRealmProvider: HistoryRealmProvider): SelectionHistoryRepo =
        DefaultSelectionHistoryRepo(
            historySize = 10,
            historyRealmProvider = historyRealmProvider
        )

    @Provides
    @Singleton
    internal fun provideSearchInteractor(): SearchInteractor = DefaultSearchInteractor()

    @Provides
    internal fun provideSymbolSearchCacheController(): SimpleCachedResource.Controller<List<SearchResultItem.Symbol>> =
        CachedSymbolsSearchController(expiresIn = Duration.ofDays(1))

    @Provides
    @Singleton
    internal fun provideSymbolSearchApiClient(@MobileTrade retrofit: Retrofit): SymbolSearchApiClient = retrofit.create(SymbolSearchApiClient::class.java)

    @Provides
    internal fun provideSymbolSearchDataDelegate(apiClient: SymbolSearchApiClient): SimpleCachedResource.DataSourceDelegate<List<SearchResultItem.Symbol>, SearchQuery> =
        CachedSymbolsSearchDataDelegate(apiClient)

    @Provides
    internal fun provideSymbolLookupMapper(): (SymbolLookupResponseDto, SearchQuery) -> SymbolLookUpResult = SymbolLookupMapper()::map

    @Provides
    @Singleton
    internal fun provideSymbolLookupRepo(
        apiClient: SymbolSearchApiClient,
        mapper: (@JvmSuppressWildcards SymbolLookupResponseDto, @JvmSuppressWildcards SearchQuery) -> @JvmSuppressWildcards SymbolLookUpResult
    ): SymbolLookupRepo = DefaultSymbolLookupRepo(apiClient, mapper)

    @Provides
    internal fun provideSearchQueryValidator(): SearchQueryValidator = DefaultSearchQueryValidator()

    @Provides
    @Singleton
    internal fun providePortfolioApiClient(@MobileTrade retrofit: Retrofit): PortfolioApiClient = retrofit.create(PortfolioApiClient::class.java)

    @Provides
    internal fun providePortfolioEntryRepo(apiClient: PortfolioApiClient): PortfolioEntryRepo = DefaultPortfolioEntryRepo(apiClient)
}
