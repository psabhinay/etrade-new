package com.etrade.mobilepro.util

import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.comparesEqualTo
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class SafeBigDecimalParseKtTest {

    @Test
    fun `safe big decimal parsing of strings with sign indicators`() {
        assertThat("+1.00".safeParseBigDecimal(), comparesEqualTo(BigDecimal("1.0")))
        assertThat("-1.00".safeParseBigDecimal(), comparesEqualTo(BigDecimal("-1.0")))
    }

    @Test
    fun `safe big decimal parsing of strings with commas`() {
        assertThat("+1,000,000.42".safeParseBigDecimal(), comparesEqualTo(BigDecimal("1000000.42")))
        assertThat("-1,000,000.42".safeParseBigDecimal(), comparesEqualTo(BigDecimal("-1000000.42")))
    }

    @Test
    fun `safe big decimal parsing of string with currency sign`() {
        assertThat("\$12,238.25".safeParseBigDecimal(), comparesEqualTo(BigDecimal("12238.25")))
        assertThat("-\$2,871.64".safeParseBigDecimal(), comparesEqualTo(BigDecimal("-2871.64")))
    }

    @Test
    fun `safe big decimal parsing of string with percent sign`() {
        assertThat("2.6849%".safeParseBigDecimal(), comparesEqualTo(BigDecimal("2.6849")))
        assertThat("-1.3433%".safeParseBigDecimal(), comparesEqualTo(BigDecimal("-1.3433")))
    }

    @Test
    fun `safe big decimal parsing of string with multiplication`() {
        assertThat("719.45K".safeParseBigDecimal(), comparesEqualTo(BigDecimal("719450")))
        assertThat("24.31M".safeParseBigDecimal(), comparesEqualTo(BigDecimal("24310000")))
        assertThat("30.79B".safeParseBigDecimal(), comparesEqualTo(BigDecimal("30790000000")))
        assertThat("12.31T".safeParseBigDecimal(), comparesEqualTo(BigDecimal("12310000000000")))
    }

    @Test
    fun `safe big decimal parsing of strings with exponential expressions`() {
        assertThat("5.11E-4".safeParseBigDecimal(), comparesEqualTo(BigDecimal("0.000511")))
        assertThat("2.12E9".safeParseBigDecimal(), comparesEqualTo(BigDecimal("2120000000")))
    }

    @Test
    fun `safe big decimal parsing of string with wrong data`() {
        assertThat("30.garbage-crap79K".safeParseBigDecimal(), nullValue())
    }
}
