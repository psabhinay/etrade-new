package com.etrade.mobilepro.watchlistapi

class WatchlistCRUDCallbacks(
    val currentWatchlists: () -> List<Watchlist>?,
    val onStartCreating: () -> Unit,
    val onWatchlistCreated: (newWatchlist: Watchlist, updatedList: List<Watchlist>) -> Unit,
    val onWatchlistDeleted: (updatedList: List<Watchlist>) -> Unit,
    val onError: () -> Unit,
)
