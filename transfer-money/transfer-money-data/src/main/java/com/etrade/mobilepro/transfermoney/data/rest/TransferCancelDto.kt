package com.etrade.mobilepro.transfermoney.data.rest

import com.etrade.mobilepro.backends.af.ResultDto
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "QuickTransferDeleteResponse", strict = false)
class TransferCancelDto(
    @field:Element(name = "Result", required = false)
    var resultDto: ResultDto? = null
)
