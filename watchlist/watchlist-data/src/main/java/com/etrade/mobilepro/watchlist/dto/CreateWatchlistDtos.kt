package com.etrade.mobilepro.watchlist.dto

import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CreateWatchlistResponseDto(
    override val data: CreateWatchlistResponseDataDto
) : BaseReference<Watchlist>() {
    override fun convertToModel() =
        Watchlist(
            id = data.watchListId,
            name = data.watchListName,
            uuid = data.watchListUuid,
        )
}

@JsonClass(generateAdapter = true)
data class CreateWatchlistResponseDataDto(
    var watchListId: String,
    var watchListName: String,
    var watchListUuid: String,
)

@JsonClass(generateAdapter = true)
data class CreateWatchlistRequestDto(
    val create: CreateWatchlistRequestDataDto,
)

@JsonClass(generateAdapter = true)
data class CreateWatchlistRequestDataDto(
    val watchListName: String?,
)
