package com.etrade.mobilepro.eventtracker

import android.content.Context
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.etrade.mobilepro.eventtrackerapi.EventTrackerInterface
import com.etrade.mobilepro.eventtrackerdata.R
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(AppsFlyerHelper::class.java)

class AppsFlyerHelper : EventTrackerInterface {

    override fun setupTracker(shouldStartTracking: Boolean, context: Context) {
        // Setting up AppsFlyer. See: https://support.appsflyer.com/hc/en-us/articles/207032126#Integration
        val conversionDataListener = object : AppsFlyerConversionListener {
            override fun onConversionDataSuccess(data: MutableMap<String, Any>?) {
                // unused
            }

            override fun onConversionDataFail(error: String?) {
                logger.error("error onConversionDataFail :  $error")
            }

            override fun onAppOpenAttribution(data: MutableMap<String, String>?) {
                // unused
            }

            override fun onAttributionFailure(error: String?) {
                logger.error("error onAttributionFailure :  $error")
            }
        }

        AppsFlyerLib.getInstance().init(context.getString(R.string.apps_flyer_dev_key), conversionDataListener, context)
        if (shouldStartTracking) {
            AppsFlyerLib.getInstance().startTracking(context)
        }
    }
}
