package com.etrade.mobilepro.trade.mutualfund.api

import com.etrade.mobilepro.instrument.InstrumentType

class MutualFundEntry(
    val securityName: String,
    val symbol: String,
    val instrumentType: InstrumentType
) {
    override fun toString(): String = symbol
}
