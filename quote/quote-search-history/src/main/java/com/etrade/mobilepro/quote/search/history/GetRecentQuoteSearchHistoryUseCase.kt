package com.etrade.mobilepro.quote.search.history

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.searchingapi.SelectionHistoryRepo
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.UseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface GetRecentQuoteSearchHistoryUseCase : UseCase<Int?, ETResult<List<SearchResultItem.Symbol>>>

class GetRecentQuoteSearchHistoryUseCaseImpl @Inject constructor(
    private val historyRepo: SelectionHistoryRepo
) : GetRecentQuoteSearchHistoryUseCase {

    override suspend fun execute(parameter: Int?): ETResult<List<SearchResultItem.Symbol>> {
        return withContext(Dispatchers.IO) {
            ETResult.success(
                historyRepo.symbolSelectionHistory()
                    .sortedByDescending { it.cachedAt }
                    .run {
                        if (parameter != null) {
                            take(parameter)
                        } else {
                            this
                        }
                    }
            )
        }
    }
}
