package com.etrade.mobilepro.util.android

interface DeviceRootChecker {

    fun isRooted(): Boolean
}
