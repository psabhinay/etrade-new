package com.etrade.mobilepro.livedata

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.test
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class OutsourcedLiveDataTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun `check setting of new source`() {
        val source = createSource()
        val outsourced = createOutsourced()
        outsourced.source = source

        val observer = outsourced.test()

        (1..3).forEach {
            source.value = it
            observer.assertValue(it.toString())
        }

        observer.assertHistorySize(3)
    }

    @Test
    fun `check changing of source`() {
        val source1 = createSource()
        val outsourced = createOutsourced()
        outsourced.source = source1

        val observer = outsourced.test()

        source1.value = 0
        observer.assertValue(0.toString())

        val source2 = createSource()
        outsourced.source = source2

        (1..3).forEach {
            source1.value = it * 10
            source2.value = it
            observer.assertValue(it.toString())
        }

        observer.assertHistorySize(4)
    }

    @Test
    fun `check nulling of source`() {
        val source = createSource()
        val outsourced = createOutsourced()
        outsourced.source = source

        val observer = outsourced.test()

        source.value = 0
        observer.assertValue(0.toString())

        outsourced.source = null

        (1..3).forEach {
            source.value = it
            observer.assertValue(0.toString())
        }

        observer.assertHistorySize(1)
    }

    private fun createSource(): MutableLiveData<Int> = MutableLiveData()

    private fun createOutsourced(): OutsourcedLiveData<Int, String> {
        return OutsourcedLiveData { origin, liveData ->
            liveData.value = origin.toString()
        }
    }
}
