package com.etrade.mobilepro.landing.usecase

import com.etrade.mobilepro.landing.api.LandingPage
import com.etrade.mobilepro.landing.api.repo.UserAccountsDataSource
import javax.inject.Inject

interface LandingInteractor {
    fun getEnabledState(page: LandingPage): Boolean
}

class DefaultLandingInteractor @Inject constructor(
    private val userAccountsDataSource: UserAccountsDataSource
) : LandingInteractor {
    private val userAccounts by lazy { userAccountsDataSource.getUserAccounts() }

    override fun getEnabledState(page: LandingPage): Boolean = when (page) {
        is LandingPage.AllBrokerage -> userAccounts.brokerageAccounts.size > 1
        is LandingPage.Portfolio -> userAccounts.brokerageAccounts.isNotEmpty()
        is LandingPage.AllAccounts,
        is LandingPage.Watchlist -> true
        is LandingPage.BottomInfoBox -> false
    }
}
