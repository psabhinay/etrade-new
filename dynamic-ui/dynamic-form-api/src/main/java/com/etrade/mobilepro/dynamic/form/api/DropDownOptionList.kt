package com.etrade.mobilepro.dynamic.form.api

class DropDownOptionList(
    val dropDownOptions: List<DropDownOption>,
    val selectedOptionIndex: Int
) {
    val selectedOption: DropDownOption?
        get() = dropDownOptions.getOrNull(selectedOptionIndex)
}
