package com.etrade.mobilepro.compose.components

import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.Styles

@Composable
fun RetryButton(modifier: Modifier = Modifier, enabled: Boolean = true, onClick: () -> Unit) {
    Button(onClick = onClick, enabled = enabled, modifier = modifier, elevation = ButtonDefaults.elevation(0.dp, 0.dp, 0.dp)) {
        Text(
            text = stringResource(R.string.retry),
            style = Styles.Medium16sp,
            modifier = Modifier.padding(horizontal = Dimens.Size8dp, vertical = Dimens.Size4dp)
        )
    }
}
