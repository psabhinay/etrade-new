package com.etrade.mobilepro.order.details.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "cancelorderresponse", strict = true)
data class StockPlanOrderCancelResponseDto(
    @field:Element(name = "pricetype", required = false)
    var priceType: String? = null,
    @field:Element(name = "emailAddress", required = false)
    var emailAddress: String? = null,
    @field:Element(name = "orderTime", required = false)
    var orderTime: String? = null,
    @field:Element(name = "companyName", required = false)
    var companyName: String? = null,
    @field:Element(name = "sharesToExercise", required = false)
    var sharesToExercise: String? = null,
    @field:Element(name = "tickerName", required = false)
    var tickerName: String? = null,
    @field:Element(name = "sharesToHold", required = false)
    var sharesToHold: String? = null,
    @field:Element(name = "transactionType", required = false)
    var transactionType: String? = null,
    @field:Element(name = "is_etca", required = false)
    var isEtca: String? = null,
    @field:Element(name = "accountNumber", required = false)
    var accountNumber: String? = null,
    @field:Element(name = "term", required = false)
    var term: String? = null,
    @field:Element(name = "isAON", required = false)
    var isAON: String? = null,
    @field:Element(name = "orderNumber", required = false)
    var orderNumber: String? = null,
    @field:Element(name = "sharesToSell", required = false)
    var sharesToSell: String? = null,
    @field:Element(name = "limitPrice", required = false)
    var limitPrice: String? = null,
    @field:Element(name = "tradeDesiredExpiration", required = false)
    var tradeDesiredExpiration: String? = null,
    @field:Element(name = "iscustomgtc", required = false)
    var iscustomGtc: String? = null,
    @field:Element(name = "emvPending", required = false)
    var emvPending: String? = null,
    @field:Element(name = "hasEtslAcct", required = false)
    var hasEtslAcct: String? = null,
    @field:Element(name = "account_number", required = false)
    var accountTitle: String? = null,
    @field:Element(name = "minorTitle", required = false)
    var minorTitle: String? = null,
    @field:Element(name = "etsl_acct", required = false)
    var etsl_acct: String? = null,
    @field:Element(name = "majorTitle", required = false)
    var majorTitle: String? = null,
    @field:Element(name = "ticker_symbol", required = false)
    var ticker_symbol: String? = null
)
