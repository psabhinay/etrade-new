package com.etrade.mobilepro.trade.data.stock

import com.etrade.mobilepro.backends.af.ErrorDto
import com.etrade.mobilepro.backends.af.ErrorType
import com.etrade.mobilepro.backends.af.ResultDto
import com.etrade.mobilepro.backends.api.EXTENDED_HOURS_DISCLOSURE_ERROR_CODE
import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.orders.api.MarketSession
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.OrderLeg
import com.etrade.mobilepro.trade.api.stock.StockOrder
import com.etrade.mobilepro.trade.api.stock.StockOrderResponse
import com.etrade.mobilepro.trade.data.form.value.StockOrderTermValueId
import com.etrade.mobilepro.trade.data.form.value.StockPriceTypeValueId
import com.etrade.mobilepro.util.safeParseBigDecimal
import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root
import java.math.BigDecimal

internal const val DAY_TRADING_WARNING_CODE = 3041
internal const val PARTIAL_FILL_ERROR_CODE = 1086
internal const val EDIT_MARKET_ORDER_ERROR_CODE = 2064
internal const val CYOTA_CHALLENGE_ERROR_CODE = 10000

private const val CONFIRMATION_ERROR_MESSAGE =
    "We have not received an order confirmation. You may be experiencing connectivity issues. " +
        "Please check your Open/Executed Orders before placing another order."

private const val CYOTA_CHALLENGE_MESSAGE =
    "Until we confirm your identity, you will have limited access to your account(s).\n\n" +
        "To help us safeguard your online account and other personal information," +
        "please call us immediately at 1-800-387-2331 for assistance."

private const val EXTENDED_HOURS_DISCLOSURE_MESSAGE =
    "Since you're placing a trade during extended hours, you need to sign a disclosure first."

@Root(strict = false)
internal class StockOrderResponseDto(
    @field:Element(name = "Result", required = false) var result: ResultDto? = null,
    @field:ElementList(name = "Order", required = false, inline = true) var order: List<OrderDto>? = null
) : StockOrderResponse {
    override val orders: List<StockOrder>
        get() = order?.map { it }.orEmpty()
    override val warnings: List<ServerMessage>
        get() = result?.fault?.errors?.warnings().orEmpty()
}

@Root(name = "Order", strict = false)
internal data class OrderDto constructor(
    @field:Element(name = "UserId", required = false) var userId: String = "",
    @field:Element(name = "AccountId", required = false) var accountId: String = "",
    @field:Element(name = "Ticker", required = false) override var symbol: String = "",
    @field:Element(name = "MktSession", required = false) var marketSessionRaw: String = "",
    @field:Element(name = "Action", required = false) var actionRaw: String = "",
    @field:Element(name = "Quantity", required = false) var quantityRaw: String = "",
    @field:Element(name = "AllOrNone", required = false) var allOrNoneRaw: String = "",
    @field:Element(name = "PriceType", required = false) var priceTypeRaw: String = "",
    @field:Element(name = "LimitPrice", required = false) var limitPriceRaw: String? = null,
    @field:Element(name = "StopPrice", required = false) var stopPriceRaw: String? = null,
    @field:Element(name = "OffSetType", required = false) var offsetType: String = "",
    @field:Element(name = "OffSetValue", required = false) var offsetValue: String? = null,
    @field:Element(name = "Term", required = false) var termRaw: String = "",
    @field:Element(name = "OrderId", required = false) var orderId: String? = null,
    @field:Element(name = "YourPrice", required = false) override var yourPrice: String = "",
    @field:Element(name = "Fee", required = false) var feeRaw: String = "",
    @field:Element(name = "Cost", required = false) var costRaw: String = "",
    @field:Element(name = "BorrowRate", required = false) override var borrowRate: String? = null,
    @field:Element(name = "EstDailyBorrowInterest", required = false) override var estDailyBorrowInterest: String? = null,
    @field:Element(name = "Date", required = false) var date: String = "0",
    @field:Element(name = "OTCTier", required = false) var otcTierRaw: String? = null
) : StockOrder, OrderLeg {

    override val account: String
        get() = accountId

    override val legs: List<OrderLeg>
        get() = listOf(this)

    override val priceType: PriceType
        get() = StockPriceTypeValueId.converter.toValue(priceTypeRaw.trim()) ?: PriceType.UNKNOWN

    override val limitPrice: BigDecimal?
        get() = limitPriceRaw?.safeParseBigDecimal()

    override val stopPrice: BigDecimal?
        get() = stopPriceRaw?.safeParseBigDecimal()

    override val term: OrderTerm
        get() = when (termRaw.trim()) {
            StockOrderTermValueId.goodForDay -> {
                if (marketSession == MarketSession.MARKET_AFTERHOUR) {
                    OrderTerm.EXTENDED_HOUR_DAY
                } else {
                    OrderTerm.GOOD_FOR_DAY
                }
            }
            StockOrderTermValueId.immediateOrCancel -> {
                if (marketSession == MarketSession.MARKET_AFTERHOUR) {
                    OrderTerm.EXTENDED_HOUR_IOC
                } else {
                    OrderTerm.IMMEDIATE_OR_CANCEL
                }
            }
            StockOrderTermValueId.goodUntilCancel -> OrderTerm.GOOD_UNTIL_CANCEL
            StockOrderTermValueId.fillOrKill -> OrderTerm.FILL_OR_KILL
            else -> OrderTerm.UNKNOWN
        }

    override val allOrNone: Boolean
        get() = allOrNoneRaw == "1"

    override val fee: BigDecimal?
        get() = feeRaw.safeParseBigDecimal()

    override val cost: BigDecimal?
        get() = costRaw.safeParseBigDecimal()

    override val action: TransactionType
        get() = when (actionRaw.trim()) {
            "Buy", "EH Buy" -> TransactionType.BUY_OPEN
            "Sell", "EH Sell" -> TransactionType.SELL_CLOSE
            "Sell Short", "EH Sell Short" -> TransactionType.SELL_OPEN
            "Buy To Cover", "EH Buy To Cover" -> TransactionType.BUY_CLOSE
            else -> TransactionType.UNKNOWN
        }
    override val quantity: BigDecimal?
        get() = quantityRaw.safeParseBigDecimal()

    override val otcTier: String?
        get() = otcTierRaw

    private val marketSession: MarketSession
        get() = MarketSession.getMarketSession(marketSessionRaw.toIntOrNull())
}

internal fun StockOrderResponseDto.getErrors(isPlaceOrder: Boolean = false) =
    result?.fault?.errors?.errors(isPlaceOrder = isPlaceOrder).orEmpty()

private fun List<ErrorDto>.warnings(): List<ServerMessage> = errors(ServerMessage.Type.WARN)

internal fun List<ErrorDto>.errors(errorType: ServerMessage.Type = ServerMessage.Type.ERROR, isPlaceOrder: Boolean = false): List<ServerMessage> {
    return asSequence()
        .filter { it.errorType.toTradeErrorType() == errorType }
        .map {
            val errorMessage = if (isPlaceOrder && errorType == ServerMessage.Type.ERROR) {
                when (it.errorCode) {
                    CYOTA_CHALLENGE_ERROR_CODE -> CYOTA_CHALLENGE_MESSAGE
                    EDIT_MARKET_ORDER_ERROR_CODE, PARTIAL_FILL_ERROR_CODE -> it.errorDescription
                    EXTENDED_HOURS_DISCLOSURE_ERROR_CODE -> EXTENDED_HOURS_DISCLOSURE_MESSAGE
                    else -> CONFIRMATION_ERROR_MESSAGE
                }
            } else {
                it.errorDescription
            }

            if (it.errorCode == EXTENDED_HOURS_DISCLOSURE_ERROR_CODE) {
                ServerMessage(ServerMessage.Type.DISCLOSURE_ERROR, errorMessage, it.messagePriority(), it.errorCode.toString())
            } else {
                ServerMessage(errorType, errorMessage, it.messagePriority(), it.errorCode.toString())
            }
        }.distinct().toList()
}

private fun ErrorDto.messagePriority(): Int {
    return if (errorCode == DAY_TRADING_WARNING_CODE) {
        0
    } else {
        1
    }
}

private fun ErrorType.toTradeErrorType(): ServerMessage.Type {
    return when (this) {
        ErrorType.ERROR, ErrorType.EXCEPTION, ErrorType.UNKNOWN -> ServerMessage.Type.ERROR
        ErrorType.WARNING -> ServerMessage.Type.WARN
    }
}
