package com.etrade.mobilepro.landing.api.repo

import com.etrade.mobilepro.landing.api.UserAccounts

interface UserAccountsDataSource {
    fun getUserAccounts(): UserAccounts
}
