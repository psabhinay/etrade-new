package com.etrade.mobilepro.billpay.presentation.addpayment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.presentation.common.isValidAmount
import com.etrade.mobilepro.billpay.presentation.common.toParcelable
import com.etrade.mobilepro.billpay.presentation.paymentpreview.PaymentPreview
import com.etrade.mobilepro.common.Restorable
import com.etrade.mobilepro.common.RestorableViewModelDelegate
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import org.threeten.bp.LocalDate
import java.math.BigDecimal

class AddPaymentViewModel @AssistedInject constructor(
    @Assisted private val validAccountList: List<AccountDetails>,
    @Assisted private val payeeList: List<Payee>,
    @Assisted("originAccount") private val originAccount: String?,
    @Assisted("destinationPayeeId") private val destinationPayeeId: String?,
    private val restorableDelegate: RestorableViewModelDelegate
) : ViewModel() {

    private val _restoreTransferAmountSignal: MutableLiveData<ConsumableLiveEvent<BigDecimal>> = MutableLiveData()
    val restoreTransferAmountSignal: MutableLiveData<ConsumableLiveEvent<BigDecimal>>
        get() = _restoreTransferAmountSignal

    internal val isPayeeAccountExternal: Boolean = true // All payee accounts are treated as external accounts
    internal val isSelectedAccountExternal: Boolean
        get() = requireNotNull(viewState.value?.selectedAccount?.isExternalAccount) { "Selected account must not be null" }

    private val _viewState: MediatorLiveData<ViewState> = MediatorLiveData()
    internal val viewState: LiveData<ViewState> get() = _viewState

    @AssistedFactory
    @SuppressWarnings("LongParameterList")
    interface Factory {
        fun create(
            validAccountList: List<AccountDetails>,
            payeeList: List<Payee>,
            @Assisted("originAccount") originAccount: String?,
            @Assisted("destinationPayeeId") destinationPayeeId: String?
        ): AddPaymentViewModel
    }

    init {
        restorableDelegate.init(this::class.java, viewState, ::restoreState)

        val defaultViewState = ViewState(
            payees = payeeList,
            validAccounts = validAccountList,
            selectedAccount = validAccountList.firstOrNull {
                it.accountId == originAccount
            }
        )

        destinationPayeeId?.let { preSelectedPayeeId ->
            _viewState.value = defaultViewState.copy(
                selectedPayee = payeeList.firstOrNull {
                    it.payeeId == preSelectedPayeeId
                }
            )
        } ?: run {
            _viewState.value = restorableDelegate.getRestoredState(this::class.java) as? ViewState ?: defaultViewState
        }
    }

    internal fun canCollectData(): Boolean {
        return viewState.value?.run {
            selectedAccount != null &&
                selectedPayee != null &&
                selectedScheduleDate != null &&
                amount?.isValidAmount() ?: false
        } ?: false
    }

    internal fun collectData(): PaymentPreview? = viewState.value?.run {
        PaymentPreview(
            originAccount = requireNotNull(selectedAccount),
            destinationAccount = requireNotNull(selectedPayee).toParcelable(),
            scheduleDate = requireNotNull(selectedScheduleDate),
            amount = requireNotNull(amount),
            memo = memo
        )
    }

    internal fun onPayeeSelected(payee: Payee) {
        _viewState.updateValue { it.copy(selectedPayee = payee) }
    }

    internal fun onAccountSelected(account: AccountDetails) {
        _viewState.updateValue { it.copy(selectedAccount = account) }
    }

    internal fun onScheduleDateSelected(date: LocalDate, page: Int) {
        _viewState.updateValue { it.copy(selectedScheduleDate = date, selectedCalendarPage = page) }
    }

    internal fun onAmountEntered(amount: BigDecimal) {
        _viewState.updateValue { it.copy(amount = amount) }
    }

    internal fun setMemo(memo: String?) {
        _viewState.updateValue {
            it.copy(
                memo = if (memo.isNullOrEmpty()) {
                    null
                } else {
                    memo
                }
            )
        }
    }

    internal fun setPayees(payees: List<Payee>) {
        val payeesNotFound = if (payees.isEmpty()) {
            ConsumableLiveEvent(Unit)
        } else {
            null
        }
        val selectedPayee = _viewState.value?.selectedPayee
        val updatedPayee = payees.firstOrNull {
            it.payeeId == selectedPayee?.payeeId
        }
        _viewState.updateValue { it.copy(payees = payees, payeesNotFound = payeesNotFound, selectedPayee = updatedPayee) }
    }

    internal fun reset() {
        _viewState.updateValue {
            ViewState(
                validAccounts = it.validAccounts,
                payees = it.payees
            )
        }
    }

    private fun restoreState(restoredState: Restorable) {
        if (restoredState is ViewState) {
            restoredState.apply {
                _viewState.value = this

                selectedAccount?.let { onAccountSelected(it) }
                selectedPayee?.let { onPayeeSelected(it) }
                amount?.let {
                    onAmountEntered(it)
                    _restoreTransferAmountSignal.value = ConsumableLiveEvent(it)
                }
                selectedScheduleDate?.let { onScheduleDateSelected(it, selectedCalendarPage ?: 0) }
                setMemo(memo)
            }
        }
    }
}

internal data class ViewState(
    val isLoading: Boolean = false,
    val validAccounts: List<AccountDetails> = emptyList(),
    val payees: List<Payee> = emptyList(),
    val payeesNotFound: ConsumableLiveEvent<Unit>? = null,
    val selectedPayee: Payee? = null,
    val selectedAccount: AccountDetails? = null,
    val selectedScheduleDate: LocalDate? = null,
    val selectedCalendarPage: Int? = null,
    val amount: BigDecimal? = null,
    val memo: String? = null,
    val error: ConsumableLiveEvent<Throwable>? = null
) : Restorable
