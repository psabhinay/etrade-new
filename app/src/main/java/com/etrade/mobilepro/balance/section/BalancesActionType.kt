package com.etrade.mobilepro.balance.section

enum class BalancesActionType {
    MORE_INFO_FED_CALL,
    MORE_INFO_MAINTENANCE_CALL
}
