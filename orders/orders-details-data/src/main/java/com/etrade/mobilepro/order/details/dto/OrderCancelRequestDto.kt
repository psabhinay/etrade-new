package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.orders.api.OrderType
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class OrderCancelRequestDto(
    @Json(name = "accountId")
    val accountId: String,
    @Json(name = "orderNo")
    val orderNumber: String = "",
    @Json(name = "orderType")
    val orderType: String = OrderType.UNKNOWN.typeCode,
    @Json(name = "platform")
    val platform: String = ""
)
