package com.etrade.mobilepro.login.session

import android.content.Context
import android.content.Intent
import android.webkit.CookieManager
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import com.etrade.eo.marketschedule.api.MarketScheduleProvider
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvAudioService
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvService
import com.etrade.mobilepro.cache.CacheClearHandler
import com.etrade.mobilepro.marketschedule.MarketScheduleProviderHolder
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.session.api.SessionEvents
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.streaming.StreamingSessionDelegateHelper
import com.etrade.mobilepro.tracking.SCREEN_TITLE_SESSION_TIME_OUT
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen
import com.etrade.mobilepro.user.session.manager.SessionState
import com.etrade.mobilepro.user.session.manager.SessionStateChangeListener
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.extension.isServiceRunning
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Provider

private val logger = LoggerFactory.getLogger(UserSessionHandlerImpl::class.java)

@MainThread
@Suppress("LongParameterList")
class UserSessionHandlerImpl @Inject constructor(
    private val applicationPreferences: ApplicationPreferences,
    private val user: User,
    private val logoutUseCase: LogoutUseCase,
    private val marketScheduleProvider: Provider<MarketScheduleProvider>,
    private val marketScheduleProviderHolder: MarketScheduleProviderHolder,
    private val context: Context,
    private val userViewModel: UserViewModel,
    private val sessionManagerDelegate: SessionManagerDelegate,
    private val streamingSessionDelegateHelper: StreamingSessionDelegateHelper,
    private val cacheClearHandler: CacheClearHandler,
    private val tracker: Tracker
) : UserSessionHandler, SessionState {

    private val listeners = mutableSetOf<SessionStateChangeListener>()
    override val sessionTerminateEvent: LiveData<SessionEvents> = sessionManagerDelegate.sessionTerminateEvent

    override val isUserLoggedIn: UserAuthenticationState
        get() = {
            user.isLoggedIn()
        }

    init {
        sessionTerminateEvent.observeForever { event ->
            if (event != null) {
                when (event) {
                    SessionEvents.MSG_SESSION_TERMINATED -> {
                        logger.debug("Received timeout MSG_SESSION_TERMINATED")
                        userViewModel.userSessionStateChange(SessionStateChange.UNAUTHENTICATED)
                        trackSessionTimeOut()
                    }
                    SessionEvents.MSG_SESSION_SAR_TERMINATED -> {
                        logger.debug("Received MSG_SESSION_SAR_TERMINATED")
                        userViewModel.userSessionStateChange(SessionStateChange.UNAUTHENTICATED, shouldShowSarLogoutDialog = true)
                    }
                    SessionEvents.MSG_NO_SESSION_TIMEOUT -> {
                        logger.debug("MSG_NO_SESSION_TIMEOUT")
                    }
                    SessionEvents.MSG_SESSION_TIMEOUT_ALARM_CANCELLED -> {
                        logger.debug("MSG_SESSION_TIMEOUT_ALARM_CANCELLED")
                    }
                }
            }
        }
    }

    override suspend fun logout() {
        if (isUserLoggedIn()) {
            sendLogoutRequest()
        }
        // Extra call just to make sure cookies are all removed
        CookieManager.getInstance().apply {
            removeAllCookies { /* do nothing */ }
            flush()
        }
        terminateSession()
        cacheClearHandler.clear()
        stopBackgroundServices()
        listeners.forEach {
            it.onSessionStateChange(SessionStateChange.UNAUTHENTICATED)
        }
    }

    override fun addListener(listener: SessionStateChangeListener) {
        listeners.add(listener)
    }

    override fun removeListener(listener: SessionStateChangeListener) {
        listeners.remove(listener)
    }

    override fun unAuthenticateSession() {
        if (isUserLoggedIn()) {
            userViewModel.userSessionStateChange(SessionStateChange.UNAUTHENTICATED)
        }
    }

    override fun onLoginSuccess() {
        user.session?.let {
            applicationPreferences.isAdvancedOrderDisclosureSigned = it.isAdvancedOrderDisclosure
            applicationPreferences.isConditionalOrderDisclosureSigned = it.isConditionalOrderDisclosure
            streamingSessionDelegateHelper.startStreamingSession()
            marketScheduleProviderHolder.marketScheduleProvider = marketScheduleProvider.get().apply {
                start()
                setTime(it.currentServerTime, it.timeZoneOffset)
            }
            sessionManagerDelegate.start(user.session?.webSessionTimeout?.toLong())
        }
        listeners.forEach {
            it.onSessionStateChange(SessionStateChange.AUTHENTICATED)
        }
    }

    /**
     * Terminate session clears user session info,
     * And terminates SAR validation and Market scheduler
     * also gets rid of watchlist sort state
     */
    private fun terminateSession() {
        sessionManagerDelegate.stop()
        streamingSessionDelegateHelper.stopStreamingSession()
        marketScheduleProviderHolder.marketScheduleProvider?.stop()
        marketScheduleProviderHolder.marketScheduleProvider = null
        user.session = null
    }

    private suspend fun sendLogoutRequest() {
        logoutUseCase.execute(Unit)
    }

    private fun stopBackgroundServices() {
        if (context.isServiceRunning(BloombergTvAudioService::class.java.name)) {
            context.stopService(Intent(context, BloombergTvAudioService::class.java))
        }

        if (context.isServiceRunning(BloombergTvService::class.java.name)) {
            context.stopService(Intent(context, BloombergTvService::class.java))
        }
    }

    private fun trackSessionTimeOut() {
        tracker.screen(SCREEN_TITLE_SESSION_TIME_OUT)
    }
}
