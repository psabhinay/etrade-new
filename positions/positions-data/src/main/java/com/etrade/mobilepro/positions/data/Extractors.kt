package com.etrade.mobilepro.positions.data

import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.data.dto.PortfolioRecordColumn
import com.etrade.mobilepro.util.formatters.parseShortDateFromString

val baseValueExtractors: Map<PortfolioRecordColumn, (PortfolioRecord) -> Comparable<*>?> by lazy {
    mapOf<PortfolioRecordColumn, (PortfolioRecord) -> Comparable<*>?>(
        PortfolioRecordColumn.ASK to { item -> item.instrument.ask },
        PortfolioRecordColumn.BASE_SYMBOL_PRICE to { item -> item.position.baseSymbolAndPrice },
        PortfolioRecordColumn.BID to { item -> item.instrument.bid },
        PortfolioRecordColumn.DAYS_EXPIRATION to { item -> item.instrument.daysToExpire },
        PortfolioRecordColumn.DAYS_GAIN to { item -> item.position.daysGain },
        PortfolioRecordColumn.DAYS_GAIN_PCT to { item -> item.position.daysGainPercentage },
        PortfolioRecordColumn.DELTA to { item -> item.instrument.delta },
        PortfolioRecordColumn.EPS to { item -> item.instrument.earningsPerShare },
        PortfolioRecordColumn.GAMMA to { item -> item.instrument.gamma },
        PortfolioRecordColumn.IV_PCT to { item -> item.instrument.impliedVolatilityPercentage },
        PortfolioRecordColumn.LAST_TRADE to { item -> item.instrument.lastPrice },
        PortfolioRecordColumn.MARK_TO_MARKET to { item -> item.position.mark },
        PortfolioRecordColumn.MARKET_CAP to { item -> item.instrument.marketCap },
        PortfolioRecordColumn.MARKET_VALUE to { item -> item.position.marketValue },
        PortfolioRecordColumn.PE_RATIO to { item -> item.instrument.priceEarningRation },
        PortfolioRecordColumn.PREV_CLOSE to { item -> item.position.todayClose },
        PortfolioRecordColumn.PRICE_CHANGE to { item -> item.instrument.daysChange },
        PortfolioRecordColumn.PRICE_CHANGE_PCT to { item -> item.instrument.daysChangePercentage },
        PortfolioRecordColumn.PRICE_PAID to { item -> item.position.pricePaid },
        PortfolioRecordColumn.QUANTITY to { item -> item.position.displayQuantity },
        PortfolioRecordColumn.SYMBOL to { item -> item.toAdjustedDisplayName() },
        PortfolioRecordColumn.THETA to { item -> item.instrument.theta },
        PortfolioRecordColumn.TOTAL_GAIN to { item -> item.position.totalGain },
        PortfolioRecordColumn.TOTAL_GAIN_PCT to { item -> item.position.totalGainPercentage },
        PortfolioRecordColumn.VEGA to { item -> item.instrument.vega },
        PortfolioRecordColumn.VOLUME to { item -> item.instrument.volume },
        PortfolioRecordColumn.WEEK_52_HIGH to { item -> item.instrument.fiftyTwoWeekHigh },
        PortfolioRecordColumn.WEEK_52_LOW to { item -> item.instrument.fiftyTwoWeekLow }
    )
}

// Tax lot has acquired date as display name, so convert the date string to Long for accurate comparison
private fun PortfolioRecord.toAdjustedDisplayName(): Comparable<*> =
    if (isTaxLot) {
        displayName.epochDay
    } else {
        displayName
    }

val String.epochDay
    get() = parseShortDateFromString(this)?.toEpochDay() ?: 0L
