package com.etrade.mobilepro.portfolio.data

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.tableviewutils.api.ColumnRepo
import com.etrade.mobilepro.tableviewutils.presentation.data.BaseColumnRepo
import javax.inject.Inject

/**
 * This repository handles everything related to columns of a portfolio table's entries in standard view.
 */
interface StandardViewColumnRepo : ColumnRepo<PortfolioColumn> {
    fun getDefaultOrder(): List<PortfolioColumn>
}

private const val COLUMN_LAST_PRICE = "LastPrice"
private const val COLUMN_DAYS_GAIN_DOLLAR = "DaysGainDollar"
private const val COLUMN_TOTAL_GAIN_DOLLAR = "TotalGainDollar"
private const val COLUMN_DAYS_GAIN_PERCENT = "ColumnDaysGainPercent"
private const val COLUMN_TOTAL_GAIN_PERCENT = "TotalGainPercent"
private const val COLUMN_BID = "Bid"
private const val COLUMN_ASK = "Ask"
private const val COLUMN_QUANTITY = "Quantity"
private const val COLUMN_PRICE_PAID = "PricePaid"
private const val COLUMN_MARKET_VALUE = "MarketValue"
private const val COLUMN_CHANGE_PERCENT = "ChangePercent"
private const val COLUMN_CHANGE_DOLLAR = "ChangeDollar"
private const val COLUMN_TODAYS_CLOSE = "TodaysClose"
private const val COLUMN_VOLUME = "Volume"
private const val COLUMN_MARKET_CAP = "MarketCap"
private const val COLUMN_PRICE_EARNING_RATIO = "PriceEarningRatio"
private const val COLUMN_EARNINGS_PER_SHARE = "EarningsPerShare"
private const val COLUMN_FIFTY_TWO_WEEK_HIGH = "52WeekHigh"
private const val COLUMN_FIFTY_TWO_WEEK_LOW = "52WeekLow"

class StandardViewColumnRepoImpl @Inject constructor(
    override val storageKey: String,
    storage: KeyValueStorage
) : BaseColumnRepo<PortfolioColumn>(storage), StandardViewColumnRepo {

    override val allColumns: Map<String, PortfolioColumn> = mapOf(
        COLUMN_LAST_PRICE to PortfolioColumn.LAST_PRICE,
        COLUMN_DAYS_GAIN_DOLLAR to PortfolioColumn.DAYS_GAIN_DOLLAR,
        COLUMN_TOTAL_GAIN_DOLLAR to PortfolioColumn.TOTAL_GAIN_DOLLAR,
        COLUMN_DAYS_GAIN_PERCENT to PortfolioColumn.DAYS_GAIN_PERCENT,
        COLUMN_TOTAL_GAIN_PERCENT to PortfolioColumn.TOTAL_GAIN_PERCENT,
        COLUMN_BID to PortfolioColumn.BID,
        COLUMN_ASK to PortfolioColumn.ASK,
        COLUMN_QUANTITY to PortfolioColumn.QUANTITY,
        COLUMN_PRICE_PAID to PortfolioColumn.PRICE_PAID,
        COLUMN_MARKET_VALUE to PortfolioColumn.MARKET_VALUE,
        COLUMN_CHANGE_PERCENT to PortfolioColumn.CHANGE_PERCENT,
        COLUMN_CHANGE_DOLLAR to PortfolioColumn.CHANGE_DOLLAR,
        COLUMN_TODAYS_CLOSE to PortfolioColumn.TODAYS_CLOSE,
        COLUMN_VOLUME to PortfolioColumn.VOLUME,
        COLUMN_MARKET_CAP to PortfolioColumn.MARKET_CAP,
        COLUMN_PRICE_EARNING_RATIO to PortfolioColumn.PRICE_EARNING_RATIO,
        COLUMN_EARNINGS_PER_SHARE to PortfolioColumn.EARNINGS_PER_SHARE,
        COLUMN_FIFTY_TWO_WEEK_HIGH to PortfolioColumn.FIFTY_TWO_WEEK_HIGH,
        COLUMN_FIFTY_TWO_WEEK_LOW to PortfolioColumn.FIFTY_TWO_WEEK_LOW
    )

    override val defaultColumnsOrder: List<String> = listOf(
        COLUMN_LAST_PRICE,
        COLUMN_DAYS_GAIN_DOLLAR,
        COLUMN_TOTAL_GAIN_DOLLAR,
        COLUMN_DAYS_GAIN_PERCENT,
        COLUMN_TOTAL_GAIN_PERCENT,
        COLUMN_BID,
        COLUMN_ASK,
        COLUMN_QUANTITY,
        COLUMN_PRICE_PAID,
        COLUMN_MARKET_VALUE,
        COLUMN_CHANGE_DOLLAR,
        COLUMN_CHANGE_PERCENT,
        COLUMN_TODAYS_CLOSE,
        COLUMN_VOLUME,
        COLUMN_MARKET_CAP,
        COLUMN_PRICE_EARNING_RATIO,
        COLUMN_EARNINGS_PER_SHARE,
        COLUMN_FIFTY_TWO_WEEK_HIGH,
        COLUMN_FIFTY_TWO_WEEK_LOW
    )

    override fun getDefaultOrder(): List<PortfolioColumn> =
        defaultColumnsOrder.mapNotNull { allColumns[it] }
}
