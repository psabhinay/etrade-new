package com.etrade.mobilepro.portfolio

import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.common.di.RemoteDynamicScreen
import com.etrade.mobilepro.dynamic.referencehelper.ReferenceHelper
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.portfolio.model.PositionId
import com.etrade.mobilepro.portfolio.model.TaxLotReferenceListDto
import com.etrade.mobilepro.positions.api.Position
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.UseCase
import kotlinx.coroutines.rx2.awaitLast
import javax.inject.Inject

/**
 * Gets portfolio tax lots for specified account ID.
 */
interface GetPortfolioTaxLotsUseCase :
    UseCase<GetPortfolioTaxLotsParameter, Resource<List<Position>>>

/**
 * Implementation of [GetPortfolioTaxLotsUseCase].
 *
 * @param screenDataSource data source for screens
 */
class GetPortfolioTaxLotsUseCaseImpl @Inject constructor(
    @RemoteDynamicScreen private val screenDataSource: ScreenDataSource,
    private val referenceHelper: ReferenceHelper
) : GetPortfolioTaxLotsUseCase {

    override suspend fun execute(parameter: GetPortfolioTaxLotsParameter): Resource<List<Position>> {
        return com.etrade.mobilepro.util.execute({
            screenDataSource.getScreen(
                ServicePath.TaxLots(
                    parameter.accountUuid,
                    parameter.positionId.value
                )
            ).awaitLast()
        }) { mapResource(it) }
    }

    private fun mapResource(resource: Resource<BaseMobileResponse>): List<Position> {
        val positions = mutableListOf<Position>()
        resource.data?.screen?.run {
            references?.forEach {
                if (it is TaxLotReferenceListDto) {
                    positions.addAll(it.data)
                }
                referenceHelper::persist
            }
        }
        return positions
    }
}

/**
 * Parameter for [GetPortfolioTaxLotsUseCase].
 *
 * @param accountUuid account UUID
 * @param positionId portfolio position to get tax lots for
 */
class GetPortfolioTaxLotsParameter(
    val accountUuid: String,
    val positionId: PositionId
)
