package com.etrade.mobilepro.screener.data.datasource

import android.content.res.Resources
import com.etrade.mobilepro.screener.api.Industry
import com.etrade.mobilepro.screener.api.Sector
import com.etrade.mobilepro.screener.api.datasource.SectorDataSource
import com.etrade.mobilepro.screener.data.R
import javax.inject.Inject

@Suppress("LargeClass")
class DefaultSectorDataSource @Inject constructor(private val resources: Resources) : SectorDataSource {
    private val allIndustries by lazy {
        Industry(
            id = "",
            title = resources.getString(R.string.all_industries),
            isAllItemsElement = true
        )
    }

    private val allSectors by lazy {
        Sector(
            id = "00",
            title = resources.getString(R.string.all_sectors),
            isAllItemsElement = true
        )
    }

    private val sectorsData by lazy {
        listOf(
            allSectors,
            Sector(
                id = "51",
                title = resources.getString(R.string.sector_basic_materials),
                value = listOf(
                    allIndustries,
                    Industry(
                        id = "511010",
                        title = resources.getString(R.string.industry_chemicals)
                    ),
                    Industry(
                        id = "512020",
                        title = resources.getString(R.string.industry_construction_materials)
                    ),
                    Industry(
                        id = "513020",
                        title = resources.getString(R.string.industry_containers_and_packaging)
                    ),
                    Industry(
                        id = "512010",
                        title = resources.getString(R.string.industry_metal_and_mining)
                    ),
                    Industry(
                        id = "513010",
                        title = resources.getString(R.string.industry_paper_and_forest)
                    )
                )
            ),
            Sector(
                id = "53",
                title = resources.getString(R.string.sector_consumer_cyclical),
                value = listOf(
                    allIndustries,
                    Industry(
                        id = "531010",
                        title = resources.getString(R.string.industry_automobiles_and_auto_parts)
                    ),
                    Industry(
                        id = "534020",
                        title = resources.getString(R.string.industry_diversified_retail)
                    ),
                    Industry(
                        id = "532030",
                        title = resources.getString(R.string.industry_homebuilding_and_construction)
                    ),
                    Industry(
                        id = "533010",
                        title = resources.getString(R.string.industry_hotels_and_entertainment_services)
                    ),
                    Industry(
                        id = "532040",
                        title = resources.getString(R.string.industry_household_goods)
                    ),
                    Industry(
                        id = "532050",
                        title = resources.getString(R.string.industry_leisure_products)
                    ),
                    Industry(
                        id = "533020",
                        title = resources.getString(R.string.industry_media_and_publishing)
                    ),
                    Industry(
                        id = "534030",
                        title = resources.getString(R.string.industry_other_specalty_retailers)
                    ),
                    Industry(
                        id = "532020",
                        title = resources.getString(R.string.industry_textiles_and_apparel)
                    )
                )
            ),
            Sector(
                id = "54",
                title = resources.getString(R.string.sector_consumer_non_cyclical),
                value = listOf(
                    allIndustries,
                    Industry(
                        id = "541010",
                        title = resources.getString(R.string.industry_beverages)
                    ),
                    Industry(
                        id = "543010",
                        title = resources.getString(R.string.industry_food_and_drug_retailing)
                    ),
                    Industry(
                        id = "541020",
                        title = resources.getString(R.string.industry_food_and_tobacco)
                    ),
                    Industry(
                        id = "542010",
                        title = resources.getString(R.string.industry_personal_household_services)
                    )
                )
            ),
            Sector(
                id = "50",
                title = resources.getString(R.string.sector_energy),
                value = listOf(
                    allIndustries,
                    Industry(
                        id = "501010",
                        title = resources.getString(R.string.industry_coal)
                    ),
                    Industry(
                        id = "501020",
                        title = resources.getString(R.string.industry_oil_and_gas)
                    ),
                    Industry(
                        id = "501030",
                        title = resources.getString(R.string.industry_oil_gas_equipment)
                    ),
                    Industry(
                        id = "502010",
                        title = resources.getString(R.string.industry_renewable_energy)
                    ),
                    Industry(
                        id = "503010",
                        title = resources.getString(R.string.industry_uranium)
                    )
                )
            ),
            Sector(
                id = "55",
                title = resources.getString(R.string.sector_financial),
                value = listOf(
                    allIndustries,
                    Industry(
                        id = "551010",
                        title = resources.getString(R.string.industry_banking_services)
                    ),
                    Industry(
                        id = "555010",
                        title = resources.getString(R.string.industry_collective_investments)
                    ),
                    Industry(
                        id = "556010",
                        title = resources.getString(R.string.industry_holding_companies)
                    ),
                    Industry(
                        id = "553010",
                        title = resources.getString(R.string.industry_insurance)
                    ),
                    Industry(
                        id = "551020",
                        title = resources.getString(R.string.industry_investment_banking_and_services)
                    ),
                    Industry(
                        id = "554020",
                        title = resources.getString(R.string.industry_real_estate_operations)
                    ),
                    Industry(
                        id = "554030",
                        title = resources.getString(R.string.industry_residential_commercial_reit)
                    )
                )
            ),
            Sector(
                id = "56",
                title = resources.getString(R.string.sector_healthcare),
                value = listOf(
                    allIndustries,
                    Industry(
                        id = "562020",
                        title = resources.getString(R.string.industry_biotech_and_research)
                    ),
                    Industry(
                        id = "561010",
                        title = resources.getString(R.string.industry_healthcare_equipment)
                    ),
                    Industry(
                        id = "561020",
                        title = resources.getString(R.string.industry_healthcare_providers)
                    ),
                    Industry(
                        id = "562010",
                        title = resources.getString(R.string.industry_pharma)
                    )
                )
            ),
            Sector(
                id = "52",
                title = resources.getString(R.string.sector_industrials),
                value = listOf(
                    allIndustries,
                    Industry(
                        id = "521010",
                        title = resources.getString(R.string.industry_aerospace_and_defense)
                    ),
                    Industry(
                        id = "522010",
                        title = resources.getString(R.string.industry_construction_and_engineering)
                    ),
                    Industry(
                        id = "522020",
                        title = resources.getString(R.string.industry_diversified_industrial_goods)
                    ),
                    Industry(
                        id = "524050",
                        title = resources.getString(R.string.industry_feight_and_logistics)
                    ),
                    Industry(
                        id = "523010",
                        title = resources.getString(R.string.industry_industrial_conglomerates)
                    ),
                    Industry(
                        id = "521020",
                        title = resources.getString(R.string.industry_machinery_equipment_components)
                    ),
                    Industry(
                        id = "524060",
                        title = resources.getString(R.string.industry_passenger_transportation)
                    ),
                    Industry(
                        id = "522030",
                        title = resources.getString(R.string.industry_professional_and_commercial_services)
                    ),
                    Industry(
                        id = "524070",
                        title = resources.getString(R.string.industry_transport_infrastructure)
                    )
                )
            ),
            Sector(
                id = "57",
                title = resources.getString(R.string.sector_technology),
                value = listOf(
                    allIndustries,
                    Industry(
                        id = "571020",
                        title = resources.getString(R.string.industry_communication_and_networking)
                    ),
                    Industry(
                        id = "571060",
                        title = resources.getString(R.string.industry_computer_phones)
                    ),
                    Industry(
                        id = "571040",
                        title = resources.getString(R.string.industry_electronic_equipment)
                    ),
                    Industry(
                        id = "571050",
                        title = resources.getString(R.string.industry_office_equipment)
                    ),
                    Industry(
                        id = "571010",
                        title = resources.getString(R.string.industry_semiconductor)
                    ),
                    Industry(
                        id = "572010",
                        title = resources.getString(R.string.industry_software)
                    )
                )
            ),
            Sector(
                id = "58",
                title = resources.getString(R.string.sector_telecom_services),
                value = listOf(
                    allIndustries,
                    Industry(
                        id = "581010",
                        title = resources.getString(R.string.industry_telecom_services)
                    )
                )
            ),
            Sector(
                id = "59",
                title = resources.getString(R.string.sector_utilities),
                value = listOf(
                    allIndustries,
                    Industry(
                        id = "591010",
                        title = resources.getString(R.string.industry_electrical_utilities_and_ipp)
                    ),
                    Industry(
                        id = "591040",
                        title = resources.getString(R.string.industry_multiline_utilities)
                    ),
                    Industry(
                        id = "591020",
                        title = resources.getString(R.string.industry_natural_gas_utilities)
                    ),
                    Industry(
                        id = "591030",
                        title = resources.getString(R.string.industry_water_and_related)
                    )
                )
            )
        )
    }

    override fun getAllSectorsItem(): Sector = allSectors

    override fun getAllIndustriesItem(): Industry = allIndustries

    override fun getSectors(): List<Sector> = sectorsData

    override fun getIndustriesBySectorId(sectorId: String): List<Industry> =
        sectorsData.find { it.id == sectorId }?.value ?: emptyList()
}
