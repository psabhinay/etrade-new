package com.etrade.mobilepro.orders.viewmodel

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.order.details.presentation.GeneralOrder
import com.etrade.mobilepro.order.details.presentation.MutualFundOrder
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel
import com.etrade.mobilepro.order.details.presentation.PreviewMutualFundOrder
import com.etrade.mobilepro.order.details.presentation.PreviewOrder
import com.etrade.mobilepro.order.details.presentation.StockPlanOrder
import com.etrade.mobilepro.order.details.router.GeneralMutualFundOrderDetailsArgument
import com.etrade.mobilepro.order.details.router.GeneralOrderDetailsArgument
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.order.details.router.PreviewMutualFundOrderDetailsArgument
import com.etrade.mobilepro.order.details.router.PreviewOrderDetailsArgument
import com.etrade.mobilepro.order.details.router.StockPlanOrderDetailsArgument
import javax.inject.Inject
import kotlin.reflect.KClass

class OrderDetailsViewDelegateMap @Inject constructor(
    @GeneralOrder
    private val generalOrderDetailsDelegate: OrderDetailsViewModel.OrderDetailsDelegate,
    @MutualFundOrder mutualFundOrderDetailsDelegate: OrderDetailsViewModel.OrderDetailsDelegate,
    @PreviewMutualFundOrder previewMutualFundOrderDetailsDelegate: OrderDetailsViewModel.OrderDetailsDelegate,
    @PreviewOrder previewOrderDetailsDelegate: OrderDetailsViewModel.OrderDetailsDelegate,
    @StockPlanOrder stockPlanOrderDetailsDelegate: OrderDetailsViewModel.OrderDetailsDelegate
) {

    class ViewModelKey(val value: KClass<out ViewModel>)

    private val delegateMap: Map<KClass<out OrderDetailsArgument>, OrderDetailsViewModel.OrderDetailsDelegate> = mapOf(
        GeneralOrderDetailsArgument::class to generalOrderDetailsDelegate,
        GeneralMutualFundOrderDetailsArgument::class to mutualFundOrderDetailsDelegate,
        PreviewMutualFundOrderDetailsArgument::class to previewMutualFundOrderDetailsDelegate,
        PreviewOrderDetailsArgument::class to previewOrderDetailsDelegate,
        StockPlanOrderDetailsArgument::class to stockPlanOrderDetailsDelegate
    )

    fun getDelegate(argument: OrderDetailsArgument): OrderDetailsViewModel.OrderDetailsDelegate {
        val key = if (argument is GeneralOrderDetailsArgument && argument.instrumentType.isMutualFundOrMoneyMarketFund) {
            GeneralMutualFundOrderDetailsArgument(
                orderId = argument.orderId,
                orderStatus = argument.orderStatus,
                symbols = argument.symbols
            )
        } else {
            argument
        }
        return delegateMap.getOrDefault(key::class, generalOrderDetailsDelegate)
    }
}
