package com.etrade.mobilepro.news.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.common.readstatus.ReadableType
import javax.inject.Inject

class NewsContainerViewModel @Inject constructor(
    private val readStatusRepo: ReadStatusRepo
) : ViewModel() {

    val clearLiveData: LiveData<Unit>
        get() = _clearLiveData

    private val _clearLiveData: MutableLiveData<Unit> = MutableLiveData()

    override fun onCleared() {
        readStatusRepo.clearExpiredReadableEntities(type = ReadableType.NEWS_ARTICLE)
        _clearLiveData.value = Unit
        super.onCleared()
    }
}
