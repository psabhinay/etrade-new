package com.etrade.mobilepro.portfolio.model

import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.positions.dao.PositionRealmObject
import com.etrade.mobilepro.positions.data.mapper.BOOLEAN_STRING_VALUE_TRUE
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PositionsReferenceDto(
    @Json(name = "data")
    override val data: List<PositionReferenceDto>
) : BaseReference<List<PositionRealmObject>>() {

    override fun convertToModel(): List<PositionRealmObject> = data.map {
        PositionRealmObject(
            positionId = it.positionId,
            accountUuid = it.accountUuid,
            accountId = it.accountId,
            pricePaidRaw = it.pricePaid,
            basisPrice = it.basisPrice,
            symbol = it.symbol,
            commissionRaw = it.commission,
            quantityRaw = it.quantity,
            displayQuantityRaw = it.displayQuantity,
            feesRaw = it.fees,
            todayQuantity = it.todayQuantity,
            todayPricePaid = it.todayPricePaid,
            todayCommissions = it.todayCommissions,
            daysGainValue = it.daysGainValue,
            daysGainPercentageRaw = it.daysGainPercentage,
            totalGainValue = it.totalGainValue,
            totalGainPercentageRaw = it.totalGainPercentage,
            todaysClose = it.todaysClose,
            daysQuantity = it.daysQuantity,
            daysPurchase = it.daysPurchase,
            hasLots = it.hasLots,
            inTheMoneyFlag = it.inTheMoneyFlag == BOOLEAN_STRING_VALUE_TRUE,
            optionUnderlier = it.optionUnderlier,
            strikePrice = it.strikePrice,
            markToMarket = it.markToMarket,
            marketValueRaw = it.marketValue,
            baseSymbolPrice = it.baseSymbolPrice
        )
    }
}
