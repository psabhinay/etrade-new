package com.etrade.mobilepro.dagger

import com.etrade.eo.streaming.SubscriptionManager
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.common.di.Level1Streamer
import com.etrade.mobilepro.dagger.qualifiers.AllBrokerage
import com.etrade.mobilepro.dagger.qualifiers.Portfolio
import com.etrade.mobilepro.movers.DefaultMoversRouter
import com.etrade.mobilepro.movers.MoversRouter
import com.etrade.mobilepro.portfolio.GetPortfolioNextPageUseCase
import com.etrade.mobilepro.portfolio.GetPortfolioNextPageUseCaseImpl
import com.etrade.mobilepro.portfolio.GetPortfolioTaxLotsUseCase
import com.etrade.mobilepro.portfolio.GetPortfolioTaxLotsUseCaseImpl
import com.etrade.mobilepro.portfolio.PortfolioSettingsRepository
import com.etrade.mobilepro.portfolio.PortfolioSettingsRepositoryImpl
import com.etrade.mobilepro.portfolio.action.PortfolioRecordActionProvider
import com.etrade.mobilepro.portfolio.action.PortfolioRecordActionProviderImpl
import com.etrade.mobilepro.portfolio.data.ListViewColumnRepo
import com.etrade.mobilepro.portfolio.data.ListViewColumnRepoImpl
import com.etrade.mobilepro.portfolio.data.OptionsViewColumnRepo
import com.etrade.mobilepro.portfolio.data.OptionsViewColumnRepoImpl
import com.etrade.mobilepro.portfolio.data.PortfolioColumnRepo
import com.etrade.mobilepro.portfolio.data.PortfolioColumnRepoImpl
import com.etrade.mobilepro.portfolio.data.StandardViewColumnRepo
import com.etrade.mobilepro.portfolio.data.StandardViewColumnRepoImpl
import com.etrade.mobilepro.portfolio.data.TileViewColumnRepo
import com.etrade.mobilepro.portfolio.data.TileViewColumnRepoImpl
import com.etrade.mobilepro.portfolio.delegate.PositionSortDelegateFactory
import com.etrade.mobilepro.portfolio.lightstreamer.AccountPortfolioDataProvider
import com.etrade.mobilepro.portfolio.lightstreamer.AccountPortfolioDataProviderImpl
import com.etrade.mobilepro.portfolio.lightstreamer.AccountPortfolioStreamIdFactory
import com.etrade.mobilepro.portfolio.lightstreamer.MdpAccountPortfolioStreamIdFactory
import com.etrade.mobilepro.portfolio.lightstreamer.PortfolioUpdateEvent
import com.etrade.mobilepro.portfolio.lightstreamer.createPortfolioUpdateEvent
import com.etrade.mobilepro.portfolio.taxlot.TaxLotSortDelegateFactory
import com.etrade.mobilepro.streaming.base.DefaultSubscriptionManagerHelper
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
interface PortfolioModule {

    /* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */

    @Binds
    fun bindAccountPortfolioDataProvider(provider: AccountPortfolioDataProviderImpl): AccountPortfolioDataProvider

    @Binds
    fun bindAccountPortfolioStreamIdFactory(streamIdFactory: MdpAccountPortfolioStreamIdFactory): AccountPortfolioStreamIdFactory

    @Binds
    @Singleton
    fun bindGetPortfolioNextPageUseCase(getPortfolioNextPageUseCase: GetPortfolioNextPageUseCaseImpl): GetPortfolioNextPageUseCase

    @Binds
    @Singleton
    fun bindGetPortfolioTaxLotsUseCase(getPortfolioTaxLotsUseCase: GetPortfolioTaxLotsUseCaseImpl): GetPortfolioTaxLotsUseCase

    @Binds
    @Singleton
    fun bindListViewColumnRepo(listViewColumnRepo: ListViewColumnRepoImpl): ListViewColumnRepo

    @Binds
    fun bindMoversRouter(router: DefaultMoversRouter): MoversRouter

    @Binds
    @Singleton
    fun bindOptionsViewColumnRepo(optionsViewColumnRepo: OptionsViewColumnRepoImpl): OptionsViewColumnRepo

    @Binds
    @Singleton
    fun bindPortfolioColumnRepo(portfolioColumnRepo: PortfolioColumnRepoImpl): PortfolioColumnRepo

    @Binds
    fun bindTaxLotSortDelegateFactory(factory: TaxLotSortDelegateFactory): PositionSortDelegateFactory

    @Binds
    @Singleton
    fun bindPortfolioRecordActionProvider(portfolioRecordActionProvider: PortfolioRecordActionProviderImpl): PortfolioRecordActionProvider

    @Binds
    @Singleton
    fun bindPortfolioSettingsRepository(portfolioSettingsRepository: PortfolioSettingsRepositoryImpl): PortfolioSettingsRepository

    @Binds
    @Singleton
    fun bindTileViewColumnRepo(listViewColumnRepo: TileViewColumnRepoImpl): TileViewColumnRepo

    companion object {

        @Provides
        @Singleton
        @AllBrokerage
        fun provideAllBrokerageStandardViewColumnRepo(storage: KeyValueStorage): StandardViewColumnRepo {
            return StandardViewColumnRepoImpl("allBrokerageStandardViewTableColumns", storage)
        }

        @Provides
        @Singleton
        @Portfolio
        fun providePortfolioStandardViewColumnRepo(storage: KeyValueStorage): StandardViewColumnRepo {
            return StandardViewColumnRepoImpl("portfolioStandardViewTableColumns", storage)
        }

        @Provides
        @Singleton
        fun provideSubscriptionManagerHelper(@Level1Streamer subscriptionManager: SubscriptionManager): SubscriptionManagerHelper<PortfolioUpdateEvent> {
            return DefaultSubscriptionManagerHelper(
                subscriptionManager,
                useSnapshot = false,
                updateInfoParser = createPortfolioUpdateEvent
            )
        }
    }
}
