package com.etrade.mobilepro.sar.service.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SarResponseDto(
    @Json(name = "terminateSession")
    val terminateSession: Boolean,
    @Json(name = "nextCheckMillis")
    val nextCheckMillis: Long? = null,
    @Json(name = "terminateReason")
    val terminateReason: String? = null
)
