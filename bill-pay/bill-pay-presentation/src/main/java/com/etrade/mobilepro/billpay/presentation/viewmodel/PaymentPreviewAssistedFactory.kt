package com.etrade.mobilepro.billpay.presentation.viewmodel

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.billpay.presentation.paymentpreview.PaymentPreview
import com.etrade.mobilepro.billpay.presentation.paymentpreview.PaymentPreviewViewModel
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

interface PaymentPreviewAssistedFactory {
    fun create(
        sessionId: String,
        paymentPreview: PaymentPreview
    ): ViewModelProvider.Factory
}

class PaymentPreviewAssistedFactoryImpl @Inject constructor(
    private val factory: PaymentPreviewViewModel.Factory
) : PaymentPreviewAssistedFactory {

    override fun create(sessionId: String, paymentPreview: PaymentPreview): ViewModelProvider.Factory =
        viewModelFactory { factory.create(sessionId, paymentPreview) }
}
