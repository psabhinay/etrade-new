package com.etrade.mobilepro.bank.transactions;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u0013H\u0016R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\u000b\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\f\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/etrade/mobilepro/bank/transactions/BankTransactionViewFactory;", "Lcom/etrade/mobilepro/dynamicui/DynamicUiViewFactory;", "resources", "Landroid/content/res/Resources;", "(Landroid/content/res/Resources;)V", "emptyDate", "", "getEmptyDate", "()Ljava/lang/String;", "emptyDate$delegate", "Lkotlin/Lazy;", "emptyValue", "getEmptyValue", "emptyValue$delegate", "createView", "Lcom/etrade/mobilepro/dynamicui/api/GenericLayout;", "dto", "", "meta", "", "bank-dynamic_debug"})
public final class BankTransactionViewFactory implements com.etrade.mobilepro.dynamicui.DynamicUiViewFactory {
    private final android.content.res.Resources resources = null;
    private final kotlin.Lazy emptyValue$delegate = null;
    private final kotlin.Lazy emptyDate$delegate = null;
    
    @javax.inject.Inject()
    public BankTransactionViewFactory(@org.jetbrains.annotations.NotNull()
    android.content.res.Resources resources) {
        super();
    }
    
    private final java.lang.String getEmptyValue() {
        return null;
    }
    
    private final java.lang.String getEmptyDate() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.etrade.mobilepro.dynamicui.api.GenericLayout createView(@org.jetbrains.annotations.NotNull()
    java.lang.Object dto, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.lang.Object> meta) {
        return null;
    }
    
    public void setupValuesFromRequest(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.dynamicui.api.ScreenRequest screenRequest) {
    }
}