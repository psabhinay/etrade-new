package com.etrade.mobilepro.appwidget.settings

import com.etrade.mobilepro.appwidget.R
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.watchlist.selection.WatchlistRow

internal fun WidgetSettings.RefreshRate.asStringRes(): Int = when (this) {
    WidgetSettings.RefreshRate.MINUTES_15 -> R.string.appwidget_refresh_rate_15_min
    WidgetSettings.RefreshRate.MINUTES_30 -> R.string.appwidget_refresh_rate_30_min
    WidgetSettings.RefreshRate.HOURS_1 -> R.string.appwidget_refresh_rate_1_hour
    WidgetSettings.RefreshRate.HOURS_2 -> R.string.appwidget_refresh_rate_2_hours
    WidgetSettings.RefreshRate.MANUAL -> R.string.appwidget_refresh_rate_manual
}

internal fun WidgetSettings.Layout.asStringRes(): Int = when (this) {
    WidgetSettings.Layout.TILES -> R.string.appwidget_layout_tiles
    WidgetSettings.Layout.LIST -> R.string.appwidget_layout_list
}

internal fun WidgetSettings.getSelectedWatchlistName(watchlistRows: List<WatchlistRow>?): String? =
    watchlistRows?.find { it.watchlist.id == watchListId }?.watchlist?.name
