package com.etrade.mobilepro.feature.firebase

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import app.cash.turbine.test
import com.etrade.mobilepro.feature.BaseFeature
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigValue
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations.openMocks
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class FirebaseFeatureFlagProviderTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()

    private lateinit var sut: FirebaseFeatureFlagProvider

    private val testDispatcher = TestCoroutineDispatcher()

    private val feature1 = BaseFeature("feature1", true)
    private val feature2Allowed = BaseFeature("feature2", false)
    private val feature3Allowed = BaseFeature("feature3", true)

    private val remoteFeatures = setOf(feature2Allowed, feature3Allowed)

    @Mock
    lateinit var remoteConfig: FirebaseRemoteConfig

    @Mock
    lateinit var trueConfigValue: FirebaseRemoteConfigValue

    @Mock
    lateinit var falseConfigValue: FirebaseRemoteConfigValue

    @Before
    fun setup() {
        openMocks(this)
        whenever(trueConfigValue.asBoolean()).thenReturn(true)
        whenever(falseConfigValue.asBoolean()).thenReturn(false)
        sut = FirebaseFeatureFlagProvider(
            remoteFeatures,
            testDispatcher,
            remoteConfig,
            context.resources,
            true,
        )
    }

    @After
    fun teardown() {
        testDispatcher.cancelChildren()
    }

    @Test
    fun isFeatureEnabled_inAllowedAndConfigured_returnsConfiguredValue() {
        testDispatcher.runBlockingTest {
            sut.isFeatureEnabled(feature2Allowed).test {
                assertThat(awaitItem(), equalTo(false))
                sut.allValues.value = mapOf(feature2Allowed.key to trueConfigValue)
                assertThat(awaitItem(), equalTo(true))
                expectNoEvents()
            }
        }
    }

    @Test
    fun isFeatureEnabled_onlyInAllowed_returnsDefault() {
        testDispatcher.runBlockingTest {
            sut.isFeatureEnabled(feature2Allowed).test {
                assertThat(awaitItem(), equalTo(false))
                expectNoEvents()
            }
            sut.isFeatureEnabled(feature3Allowed).test {
                assertThat(awaitItem(), equalTo(true))
                expectNoEvents()
            }
        }
    }

    @Test
    fun isFeatureEnabled_onlyConfigured_returnsDefault() {
        testDispatcher.runBlockingTest {
            sut.allValues.value = mapOf(feature1.key to falseConfigValue)
            sut.isFeatureEnabled(feature1).test {
                assertThat(awaitItem(), equalTo(true))
                awaitComplete()
            }
        }
    }

    @Test
    fun isFeatureEnabled_invalidRemoteValue_returnsDefault() {
        testDispatcher.runBlockingTest {
            sut.allValues.value = mapOf(feature2Allowed.key to trueConfigValue)
            sut.isFeatureEnabled(feature2Allowed).test {
                assertThat(awaitItem(), equalTo(true))
                whenever(trueConfigValue.asBoolean()).thenThrow(IllegalArgumentException())
                sut.allValues.value = mapOf(feature2Allowed.key to trueConfigValue, feature3Allowed.key to falseConfigValue)
                assertThat(awaitItem(), equalTo(false))
                expectNoEvents()
            }
        }
    }

    @Test
    fun hasFeature_onlyInAllowed_returnsFalse() {
        testDispatcher.runBlockingTest {
            sut.hasFeature(feature2Allowed).test {
                assertThat(awaitItem(), equalTo(false))
                expectNoEvents()
            }
        }
    }

    @Test
    fun hasFeature_inAllowedAndConfigured_returnsTrue() {
        testDispatcher.runBlockingTest {
            sut.hasFeature(feature2Allowed).test {
                assertThat(awaitItem(), equalTo(false))
                expectNoEvents()
                sut.allValues.value = mapOf(feature2Allowed.key to falseConfigValue)
                assertThat(awaitItem(), equalTo(true))
                expectNoEvents()
            }
        }
    }

    @Test
    fun hasFeature_configuredAndNotAllowed_returnsFalse() {
        testDispatcher.runBlockingTest {
            sut.hasFeature(feature1).test {
                assertThat(awaitItem(), equalTo(false))
                sut.allValues.value = mapOf(feature1.key to falseConfigValue)
                awaitComplete()
            }
        }
    }
}
