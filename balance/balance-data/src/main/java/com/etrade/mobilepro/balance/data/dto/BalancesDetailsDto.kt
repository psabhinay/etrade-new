package com.etrade.mobilepro.balance.data.dto

import com.etrade.mobilepro.balance.api.AccountBalance
import com.etrade.mobilepro.util.safeParseBigDecimal
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root
import java.math.BigDecimal

@Root(strict = false, name = "BAL_REAL_TIME")
data class BalancesDetailsDto constructor(
    @field:Element(name = "NET_ACCT_VAL", required = false)
    var netAccountVal: String? = null,
    @field:Element(name = "MKT_SEC_LONG_VAL", required = false)
    var marketSecLongVal: String? = null,
    @field:Element(name = "MARGIN_CASH_VAL", required = false)
    var marginCashVal: String? = null,
    @field:Element(name = "DT_MARGIN_CASH_VAL", required = false)
    var dtMarginCashVal: String? = null,
    @field:Element(name = "MARGIN_EQ_PER", required = false)
    var marginEqPer: String? = null,
    @field:Element(name = "MINEQ_CALL_VAL", required = false)
    var minEqCallVal: String? = null,
    @field:Element(name = "INV_CASH_VAL", required = false)
    var invCashVal: String? = null,
    @field:Element(name = "TOT_WITHDRAW_CASH_VAL", required = false)
    var totalWithdrawCashval: String? = null,
    @field:Element(name = "MARGIN_SEC_VAL", required = false)
    var marginSecVal: String? = null,
    @field:Element(name = "DT_MARGIN_SEC_VAL", required = false)
    var dtMarginSecVal: String? = null,
    @field:Element(name = "DT_CASH_SEC_VAL", required = false)
    var dtCashSecVal: String? = null,
    @field:Element(name = "DT_WITHHELD_CASH_VAL", required = false)
    var dtWithheldCashVal: String? = null,
    @field:Element(name = "WITHHELD_CASH_VAL", required = false)
    var withheldCashVal: String? = null,
    @field:Element(name = "CASH_SEC_VAL", required = false)
    var cashsecVal: String? = null,
    @field:Element(name = "HOLDING_CASH_VAL", required = false)
    var holdingCashVal: String? = null,
    @field:Element(name = "WITHDRAW_MARGIN_VAL", required = false)
    var withdrawMarginVal: String? = null,
    @field:Element(name = "WITHDRAW_CASH_VAL", required = false)
    var withdrawCashVal: String? = null,
    @field:Element(name = "UNINV_CASH_BAL", required = false)
    var uninvCashbal: String? = null,
    @field:Element(name = "DT_WITHHELD_MARGIN_VAL", required = false)
    var dtwithheldMarginVal: String? = null,
    @field:Element(name = "DAY_TRD_CNT", required = false)
    var dtCount: String? = null,
    @field:Element(name = "DAY_TRD_ALLOWED_CNT", required = false)
    var dtAllowedCt: String? = null,
    @field:Element(name = "WITHHELD_MARGIN_VAL", required = false)
    var withheldMarginVal: String? = null,
    @field:Element(name = "MARGIN_BAL_VAL", required = false)
    var marginbalVal: String? = null,
    @field:Element(name = "NET_CASH_MAR_BAL", required = false)
    var netCashmarbal: String? = null,
    @field:Element(name = "SHORT_RES_VAL", required = false)
    var shortresVal: String? = null,
    @field:Element(name = "HOUSE_CALL_VAL", required = false)
    var housecallVal: String? = null,
    @field:Element(name = "FED_CALL_VAL", required = false)
    var fedcallVal: String? = null,
    @field:Element(name = "SWEEP_CASH_HOLDING", required = false)
    var sweepCashholding: String? = null,
    @field:Element(name = "MARGIN_EQ_VAL", required = false)
    var margineqVal: String? = null,
    @field:Element(name = "MKT_SEC_SHORT_VAL", required = false)
    var mktSecshortVal: String? = null,
    @field:Element(name = "MKT_SEC_VAL", required = false)
    var mktSecVal: String? = null,
    @field:Element(name = "MARGIN_DEBT_VAL", required = false)
    var margindebtVal: String? = null,
    @field:Element(name = "HAS_SALES_PROCEED", required = false)
    var hassalesproceed: String? = null,
    @field:Element(name = "UNSTLMT_POS1_VAL", required = false)
    var unstlmtpoS1VAL: String? = null,
    @field:Element(name = "UNSTLMT_POS3_VAL", required = false)
    var unstlmtpoS3VAL: String? = null,
    @field:Element(name = "UNSTLMT_POS2_VAL", required = false)
    var unstlmtpoS2VAL: String? = null,
    @field:Element(name = "FUNDS_WH_PWR_VAL", required = false)
    var fundswhpwrVal: String? = null,
    @field:Element(name = "DTCALL", required = false)
    var dtcall: String? = null,
    @field:Element(name = "FUNDS_WH_WDL_VAL", required = false)
    var fundsWhWithdrawVal: String? = null,
    @field:Element(name = "CASH_INV_VAL", required = false)
    var cashinvVal: String? = null,
    @field:Element(name = "UNSETTLED_CASH_FOR_INV", required = false)
    var unsettledCashforinv: String? = null,
    @field:Element(name = "SETTLED_CASH_FOR_INV", required = false)
    var settledCashforinv: String? = null,
    @field:Element(name = "CASH_CALL_VAL", required = false)
    var cashcallVal: String? = null,
    @field:Element(name = "INF_OPTN_LVL", required = false)
    var infOptnLevel: String? = null,
    @field:Element(name = "INF_MARGIN_LVL", required = false)
    var infMarginLevel: String? = null,
    @field:Element(name = "COMP_RVW_CD", required = false)
    var comprvwcd: String? = null,
    @field:Element(name = "MARGIN_LVL", required = false)
    var marginLevel: String? = null,
    @field:Element(name = "DT_STATUS", required = false)
    var dtStatus: String? = null,
    @field:Element(name = "AVL_EX_EQ", required = false)
    var avlexeq: String? = null,
    @field:Element(name = "TOT_MAR_REQ", required = false)
    var totMarreq: String? = null,
    @field:Element(name = "EXCESS_EQUITY", required = false)
    var excessEquity: String? = null,
    @field:Element(name = "OPEN_ORDER_RESERVE", required = false)
    var openOrderReserve: String? = null,
    @field:Element(name = "FUNDS_ON_HLD", required = false)
    var fundsOnHold: String? = null
) : AccountBalance {

    override val isPatternDayTrader: Boolean
        get() = dtStatus?.isNotBlank() == true
    override val accountValue: BigDecimal?
        get() = netAccountVal?.safeParseBigDecimal()
    override val buyingPower: BigDecimal?
        get() = marginCashVal?.safeParseBigDecimal()
    override val cashAvailableForInvestment: BigDecimal?
        get() = settledCashforinv?.safeParseBigDecimal()?.let {
            (unsettledCashforinv?.safeParseBigDecimal() ?: BigDecimal.ZERO) + it
        } ?: unsettledCashforinv?.safeParseBigDecimal()
    override val totalAvailableForWithdrawal: BigDecimal?
        get() = totalWithdrawCashval?.safeParseBigDecimal()
    override val availableExcessEquity: BigDecimal?
        get() = avlexeq?.safeParseBigDecimal()
    override val intradayMargin: BigDecimal?
        get() = dtMarginCashVal?.safeParseBigDecimal()
    override val intradayNonMargin: BigDecimal?
        get() = dtCashSecVal?.safeParseBigDecimal()
}
