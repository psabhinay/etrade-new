package com.etrade.mobilepro.transfermoney.presentation.transfer.source

import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount

interface TransferSourceHolder {
    val accounts: List<TransferMoneyAccount>
    val selectedFrom: TransferMoneyAccount?
    val selectedTo: TransferMoneyAccount?

    fun copyAsHolder(
        accounts: List<TransferMoneyAccount> = this.accounts,
        selectedFrom: TransferMoneyAccount? = this.selectedFrom,
        selectedTo: TransferMoneyAccount? = this.selectedTo
    ): TransferSourceHolder {
        return object : TransferSourceHolder {
            override val accounts: List<TransferMoneyAccount> = accounts
            override val selectedFrom: TransferMoneyAccount? = selectedFrom
            override val selectedTo: TransferMoneyAccount? = selectedTo
        }
    }
}
