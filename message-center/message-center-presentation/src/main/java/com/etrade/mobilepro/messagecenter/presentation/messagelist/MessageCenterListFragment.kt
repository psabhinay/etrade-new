package com.etrade.mobilepro.messagecenter.presentation.messagelist

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.messagecenter.presentation.R
import com.etrade.mobilepro.messagecenter.presentation.common.MessageParcel
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterFragmentMessageListBinding
import com.etrade.mobilepro.messagecenter.presentation.messagelist.adapter.MessageListAdapter
import com.etrade.mobilepro.messagecenter.presentation.messagelist.router.MessageDetailsRouter
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

const val MESSAGE_CENTER_PATH = "/messagecenter"
internal const val IS_CHAT_LAUNCHED_FROM_MESSAGE_CENTER = "isChatLaunchedFromMessagesCenter"
internal const val MESSAGE = "message"

@RequireLogin
class MessageCenterListFragment @Inject constructor(
    viewModelProvider: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val messageDetailsRouter: MessageDetailsRouter
) : Fragment(R.layout.message_center_fragment_message_list) {

    private val binding by viewBinding(MessageCenterFragmentMessageListBinding::bind) { onDestroyBinding() }

    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private val viewModel by navGraphViewModels<MessageCenterListViewModel>(R.id.message_center_nav_graph) { viewModelProvider }
    private var snackbar: Snackbar? = null
    private val adapter by lazy {
        MessageListAdapter {
            findNavController().navigate(messageDetailsRouter.getMessageDetailRoute(it))
        }
    }
    private val adapterDataObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            super.onItemRangeInserted(positionStart, itemCount)
            if (positionStart == 0) {
                binding.recyclerView.smoothScrollToPosition(0)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val message = arguments?.getParcelable(MESSAGE) as MessageParcel?
        message?.let {
            findNavController().navigate(messageDetailsRouter.getMessageDetailRoute(it.toDomain()))
            requireArguments().clear()
            return
        }
        setupFragment()
        bindViewModel()
        viewModel.refresh(forced = false)
    }

    private fun MessageCenterFragmentMessageListBinding.onDestroyBinding() {
        adapter.tryUnregisterObserver(adapterDataObserver)
        recyclerView.adapter = null
    }

    private fun setupFragment() {
        with(binding.recyclerView) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = this@MessageCenterListFragment.adapter
            setupRecyclerViewDivider(
                drawableResId = R.drawable.thin_divider,
                countOfLastItemsWithoutDivider = 0
            )
        }
        binding.swipeRefresh.apply {
            applyColorScheme()
            setOnRefreshListener {
                viewModel.refresh(forced = true)
                adapter.tryRegisterObserver(adapterDataObserver)
            }
        }
        binding.chatWithUs.setOnClickListener {
            navigateToChat()
        }
        binding.newMessage.setOnClickListener {
            findNavController().navigate(MessageCenterListFragmentDirections.actionMessageCenterListToNewMessage())
        }
    }

    private fun navigateToChat() {
        val navController = findNavController()
        if (navController.currentDestination?.id == R.id.messageCenterListFragment) {
            navController.navigate(
                MessageCenterListFragmentDirections
                    .actionMessageCenterListToChatSetup(bundleOf(IS_CHAT_LAUNCHED_FROM_MESSAGE_CENTER to true))
            )
        }
    }

    private fun bindViewModel() {
        viewModel.viewState.observeBy(viewLifecycleOwner) {
            handleLoadingState(it.loading)
            handleErrorState(it.error)
            handleSuccessState(it)
        }
        viewModel.snackBarMessage.observeBy(viewLifecycleOwner) {
            it.getNonConsumedContent()?.let { message -> snackBarUtil.snackbar(message, Snackbar.LENGTH_SHORT) }
        }
        viewModel.scrollMessagesToTop.observeBy(viewLifecycleOwner) {
            it.getNonConsumedContent()?.let { adapter.tryRegisterObserver(adapterDataObserver) }
        }
        viewModel.newMessageButtonVisible.observeBy(viewLifecycleOwner) {
            binding.newMessage.isVisible = it
            binding.buttonLayout.visibility = View.VISIBLE
        }
    }

    private fun handleLoadingState(loading: MessageCenterListViewModel.ViewState.Loading) =
        when (loading) {
            MessageCenterListViewModel.ViewState.Loading.NONE -> {
                binding.loadingPb.hide()
                binding.swipeRefresh.isRefreshing = false
                binding.swipeRefresh.visibility = View.VISIBLE
            }
            MessageCenterListViewModel.ViewState.Loading.INITIAL -> {
                binding.loadingPb.show()
                binding.swipeRefresh.isRefreshing = false
                binding.swipeRefresh.visibility = View.GONE
            }
            MessageCenterListViewModel.ViewState.Loading.REFRESH -> {
                binding.loadingPb.hide()
                binding.swipeRefresh.isRefreshing = true
                binding.swipeRefresh.visibility = View.VISIBLE
            }
        }

    private fun handleErrorState(error: MessageCenterListViewModel.ViewState.Error) =
        when (error) {
            MessageCenterListViewModel.ViewState.Error.None -> snackbar?.dismiss()
            is MessageCenterListViewModel.ViewState.Error.Loading -> {
                snackbar = snackBarUtil.retrySnackbar { error.retry.invoke() }
            }
        }

    private fun handleSuccessState(state: MessageCenterListViewModel.ViewState) {
        if (state.items.isEmpty()) {
            binding.recyclerView.visibility = View.GONE
            binding.noMessages.visibility = if (state.error == MessageCenterListViewModel.ViewState.Error.None) {
                View.VISIBLE
            } else {
                View.GONE
            }
        } else {
            binding.recyclerView.visibility = View.VISIBLE
            binding.noMessages.visibility = View.GONE
        }
        adapter.submitItems(state.items)
        adapter.tryUnregisterObserver(adapterDataObserver)
    }
}
