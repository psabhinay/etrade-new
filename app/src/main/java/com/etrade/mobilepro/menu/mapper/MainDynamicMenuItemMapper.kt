package com.etrade.mobilepro.menu.mapper

import android.content.Context
import android.content.res.Resources
import com.etrade.mobilepro.R
import com.etrade.mobilepro.home.model.AlertsCountViewModel
import com.etrade.mobilepro.menu.NewMenuItemValidator
import com.etrade.mobilepro.menu.builder.MenuPageBuilder
import com.etrade.mobilepro.menu.builder.TextMenuItemBuilder
import com.etrade.mobilepro.menu.repo.DynamicMenuItem
import com.etrade.mobilepro.menu.repo.MenuItemType
import com.etrade.mobilepro.userviewmodel.UserViewModel
import javax.inject.Inject

class MainDynamicMenuItemMapper @Inject constructor(
    context: Context,
    newMenuItemValidator: NewMenuItemValidator,
    private val alertsCountViewModel: AlertsCountViewModel,
    private val userViewModel: UserViewModel
) : BaseDynamicMenuItemMapper(context, newMenuItemValidator) {

    private fun MenuPageBuilder.alertItem(dynamicMenuItem: DynamicMenuItem) {
        count {
            dynamicSetup(dynamicMenuItem)
            alertCount(context.resources, userViewModel.isAuthenticated, alertsCountViewModel.viewState.value?.count ?: 0)
        }
    }

    override fun mapDynamicItem(dynamicMenuItem: DynamicMenuItem, builder: MenuPageBuilder) {
        when (dynamicMenuItem.menuItemType) {
            MenuItemType.MenuItemViewAlertCount -> builder.alertItem(dynamicMenuItem)
            else -> super.mapDynamicItem(dynamicMenuItem, builder)
        }
    }
}

private fun TextMenuItemBuilder.alertCount(resources: Resources, isAuthenticated: Boolean, count: Int) {
    if (isAuthenticated) {
        subtext = count.toString()
        showRightIcon = count > 0
        contentDescription = resources.getString(R.string.alerts_content_description_unread_alerts, text, subtext ?: "0", text)
    } else {
        subtext = "0"
        showRightIcon = false
        contentDescription = text
    }
}
