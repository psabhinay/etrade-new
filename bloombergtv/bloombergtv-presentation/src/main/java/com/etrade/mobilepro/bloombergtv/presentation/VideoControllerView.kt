package com.etrade.mobilepro.bloombergtv.presentation

import android.content.Context
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.view.Gravity
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import androidx.core.view.isVisible
import com.etrade.mobilepro.bloombergtv.presentation.databinding.VideoControllerBinding
import com.etrade.mobilepro.bloombergtv.presentation.delegate.MediaPlayerDelegate
import com.etrade.mobilepro.bloombergtv.presentation.delegate.VideoControllerDelegate
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import java.util.Formatter
import java.util.Locale

private const val DEFAULT_TIMEOUT_MS = 3000L
private const val DEFAULT_REFRESH_MS = 1000L
private const val TIME_LABEL_FORMAT_WITH_HOURS = "%d:%02d:%02d"
private const val TIME_LABEL_FORMAT = "%02d:%02d"

@Suppress("ViewConstructor") // it is created only manually
internal class VideoControllerView(
    context: Context,
    private val delegate: VideoControllerDelegate,
    private val coroutineScope: CoroutineScope
) : FrameLayout(context) {

    internal var onHide: (() -> Unit)? = null
    internal var onShow: (() -> Unit)? = null

    private val binding: VideoControllerBinding = VideoControllerBinding.inflate(
        context.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
    )
    private var mediaPlayerDelegate: MediaPlayerDelegate? = null
    private var hideJob: Job? = null
    private var updateUIJob: Job? = null
    private val ignoredKeyCodes = setOf(
        KeyEvent.KEYCODE_VOLUME_DOWN,
        KeyEvent.KEYCODE_VOLUME_UP,
        KeyEvent.KEYCODE_VOLUME_MUTE
    )

    /**
     * Set the view that acts as the anchor for the control view.
     * This can for example be a VideoView, or your Activity's main view.
     *
     * @param view The view to which to anchor the controller when it is visible.
     * @param mediaPlayerDelegate Interface to the media player
     */
    internal fun attachTo(view: ViewGroup, mediaPlayerDelegate: MediaPlayerDelegate) {
        this.mediaPlayerDelegate = mediaPlayerDelegate
        updatePausePlay()
        binding.pause.setOnClickListener {
            doPauseResume()
            show()
        }
        binding.mediaControllerPictureInPicture.setOnClickListener {
            delegate.onPictureInPictureClick()
        }
        binding.mediaControllerAudioOnlyMode.setOnClickListener {
            delegate.onAudioOnlyModeClick()
        }
        view.addView(binding.root, LayoutParams(MATCH_PARENT, MATCH_PARENT, Gravity.BOTTOM))
    }

    /**
     * Show the view on screen. It will go away
     * automatically after 'timeout' milliseconds of inactivity.
     */
    internal fun show() {
        if (!binding.root.isVisible) {
            binding.root.isVisible = true
            binding.pause.requestLayout()
        }
        updateUIJob?.cancel()
        updateUIJob = coroutineScope.launch {
            while (isActive) {
                mediaPlayerDelegate?.let {
                    binding.timeCurrent.text = stringForTime(it.currentPosition)
                }
                delay(DEFAULT_REFRESH_MS)
            }
        }
        updatePausePlay()
        hideJob?.cancel()
        hideJob = coroutineScope.launch {
            delay(DEFAULT_TIMEOUT_MS)
            hide()
        }
        onShow?.invoke()
    }

    internal fun enablePausePlayButton(enabled: Boolean) {
        binding.pause.isEnabled = enabled
    }

    override fun performClick(): Boolean {
        show()
        return super.performClick()
    }

    override fun onTrackballEvent(ev: MotionEvent): Boolean {
        show()
        return false
    }

    override fun dispatchKeyEvent(event: KeyEvent): Boolean {
        val mediaPlayerControl = mediaPlayerDelegate ?: return true
        val keyCode = event.keyCode
        val uniqueDown = event.repeatCount == 0 && event.action == KeyEvent.ACTION_DOWN
        if (ignoredKeyCodes.contains(keyCode)) {
            return super.dispatchKeyEvent(event)
        }
        return when (keyCode) {
            KeyEvent.KEYCODE_HEADSETHOOK,
            KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE,
            KeyEvent.KEYCODE_SPACE -> {
                if (uniqueDown) {
                    doPauseResume()
                    show()
                    binding.pause.requestFocus()
                }
                true
            }
            KeyEvent.KEYCODE_MEDIA_PLAY -> {
                if (uniqueDown && !mediaPlayerControl.isPlaying) {
                    mediaPlayerControl.start()
                    updatePausePlay()
                    show()
                }
                true
            }
            KeyEvent.KEYCODE_MEDIA_STOP,
            KeyEvent.KEYCODE_MEDIA_PAUSE -> {
                if (uniqueDown && mediaPlayerControl.isPlaying) {
                    mediaPlayerControl.pause()
                    updatePausePlay()
                    show()
                }
                true
            }
            KeyEvent.KEYCODE_BACK,
            KeyEvent.KEYCODE_MENU -> {
                if (uniqueDown) {
                    hide()
                }
                true
            }
            else -> {
                show()
                super.dispatchKeyEvent(event)
            }
        }
    }

    override fun setEnabled(enabled: Boolean) {
        binding.pause.isEnabled = enabled
        super.setEnabled(enabled)
    }

    internal fun hide() {
        updateUIJob?.cancel()
        binding.root.isVisible = false
        onHide?.invoke()
    }

    private fun stringForTime(timeMs: Int): String {
        val totalSeconds = timeMs / 1000
        val seconds = totalSeconds % 60
        val minutes = totalSeconds / 60 % 60
        val hours = totalSeconds / 3600
        val formatter = Formatter(Locale.getDefault())
        return if (hours > 0) {
            formatter.format(TIME_LABEL_FORMAT_WITH_HOURS, hours, minutes, seconds).toString()
        } else {
            formatter.format(TIME_LABEL_FORMAT, minutes, seconds).toString()
        }
    }

    private fun doPauseResume() = mediaPlayerDelegate?.run {
        delegate.isPlaying = if (isPlaying) {
            pause()
            false
        } else {
            start()
            true
        }
        updatePausePlay()
    }

    private fun updatePausePlay() = mediaPlayerDelegate?.run {
        val imageRes = if (isPlaying) {
            android.R.drawable.ic_media_pause
        } else {
            android.R.drawable.ic_media_play
        }
        binding.pause.setImageResource(imageRes)
    }
}
