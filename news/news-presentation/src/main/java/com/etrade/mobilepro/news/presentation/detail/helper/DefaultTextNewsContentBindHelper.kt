package com.etrade.mobilepro.news.presentation.detail.helper

import android.widget.TextView
import androidx.core.widget.ContentLoadingProgressBar
import androidx.core.widget.TextViewCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.news.presentation.customview.TextNewsCellView
import com.etrade.mobilepro.util.NetworkState
import javax.inject.Inject

class DefaultTextNewsContentBindHelper @Inject constructor() : TextNewsContentBindHelper {

    override fun bindTextNewsContent(
        contentLoader: TextNewsContentLoader,
        viewLifecycleOwner: LifecycleOwner,
        contentLoadingProgressBar: ContentLoadingProgressBar?,
        contentTextView: TextView?,
        newsCell: TextNewsCellView?,
        actionOnRetry: () -> Unit
    ) {
        if (contentLoadingProgressBar == null || contentTextView == null) {
            return
        }
        contentLoader.networkState.observe(
            viewLifecycleOwner,
            Observer { networkState ->
                if (networkState == NetworkState.Loading) {
                    contentLoadingProgressBar.show()
                } else {
                    contentLoadingProgressBar.hide()
                }
                if (networkState is NetworkState.Failed) {
                    actionOnRetry.invoke()
                }
            }
        )
        contentLoader.content.observe(
            viewLifecycleOwner,
            Observer { newsTextItem ->
                contentTextView.text = if (newsTextItem.content.isNotBlank() && newsTextItem.content != "null") {
                    newsTextItem.content
                } else {
                    "" // TODO Maybe some placeholder
                }
                newsCell?.bindTo(newsTextItem)
            }
        )
    }

    override fun bindTextNewsFontAdjusting(
        fontAdjustHandler: TextNewsFontAdjustHandler,
        viewLifecycleOwner: LifecycleOwner,
        contentTextView: TextView?,
        newsCell: TextNewsCellView?
    ) {
        if (contentTextView == null) {
            return
        }
        fontAdjustHandler.isLargeFontSize.observe(
            viewLifecycleOwner,
            Observer { isLargeFont ->
                val contentStyleResId = if (isLargeFont) {
                    R.style.NewsDetailContentTextStyle_Larger
                } else {
                    R.style.NewsDetailContentTextStyle
                }
                TextViewCompat.setTextAppearance(contentTextView, contentStyleResId)
                newsCell?.toggleLargeFont(isLargeFont)
            }
        )
    }
}
