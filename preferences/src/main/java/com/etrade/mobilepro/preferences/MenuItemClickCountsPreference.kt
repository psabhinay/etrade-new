package com.etrade.mobilepro.preferences

interface MenuItemClickCountsPreference {
    operator fun get(menuItemKey: String): Int
    operator fun set(menuItemKey: String, count: Int)
}
