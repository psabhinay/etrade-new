package com.etrade.mobilepro.util.android.animutil

import android.transition.Transition
import android.transition.TransitionSet
import androidx.annotation.IdRes

/**
 * Utility methods for working with transitions
 */
object TransitionUtils {

    fun findTransition(
        set: TransitionSet,
        clazz: Class<out Transition>,
        @IdRes targetId: Int
    ): Transition? {
        for (i in 0 until set.transitionCount) {
            val transition = set.getTransitionAt(i)
            if (transition.javaClass == clazz) {
                if (transition.targetIds.contains(targetId)) {
                    return transition
                }
            }
            if (transition is TransitionSet) {
                val child = findTransition(transition, clazz, targetId)
                if (child != null) return child
            }
        }
        return null
    }
}
