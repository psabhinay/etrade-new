package com.etrade.mobilepro.movers

import android.content.res.Resources
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.viewModelScope
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate.StreamingToggleUpdate
import com.etrade.mobilepro.util.android.CallToAction
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.TimestampEvent
import com.etrade.mobilepro.util.android.TimestampEventProvider
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.formatCurrencyAmount
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.formatters.formatMarketPercentDataCustom
import com.etrade.mobilepro.util.removePercent
import com.etrade.mobilepro.util.safeParseBigDecimal
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.util.Collections

@Suppress("LongParameterList")
abstract class Movers(
    layoutId: Int,
    val title: String,
    val showActionView: Boolean,
    var callToAction: CallToAction? = null,
    private val moversRouter: MoversRouter,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController,
    private val itemsComparator: Comparator<MoverItem>? = null,
    private val shouldDisplayOptionsData: Boolean,
    protected val resources: Resources,
    private val streamSnapshotsToFlow: Boolean = false
) : DynamicViewModel(layoutId), LifecycleObserver, TimestampEventProvider {
    private val streamingCompositeDisposable = CompositeDisposable()

    private val noQuotesWarning = resources.getString(R.string.portfolio_movers_no_positions)

    private val symbolSubscriptionFields: Set<Level1Field> = setOf(
        Level1Field.CHANGE,
        Level1Field.CHANGE_PERCENT,
        Level1Field.DAY_CHANGE,
        Level1Field.DAY_CHANGE_PERCENT,
        Level1Field.LAST_PRICE,
        Level1Field.TICK_INDICATOR,
        Level1Field.TIMESTAMP_SINCE_EPOCH
    )

    private val streamingStatusUpdateListener: (StreamingStatusUpdate) -> Unit = { status ->
        if (status is StreamingToggleUpdate) {
            streamingCompositeDisposable.clear()
            if (status.enabled) {
                subscribeToSymbolUpdates()
            }
        }
    }

    init {
        streamingStatusController.subscribeForStreamingStatusUpdates(streamingStatusUpdateListener)
    }

    open val subtitle: LiveData<String?> = MutableLiveData()

    open val showAccountSelection: Boolean = false
    private val _moverItemsFlow = MutableStateFlow(MoverGridData())
    val moverItems = MutableLiveData<MutableList<MoverItem>>().apply {
        if (streamSnapshotsToFlow) {
            observeForever {
                val snapshot = mutableListOf<MoverItem>().apply { addAll(it.map { it.copy() }) }
                _moverItemsFlow.value = MoverGridData(snapshot)
            }
        }
    }

    val moverItemsFlow = _moverItemsFlow.asStateFlow()

    override val timestampEvents = MutableLiveData<ConsumableLiveEvent<TimestampEvent>>()

    private val _viewState = MutableLiveData<DynamicUiViewState>()
    val viewState: LiveData<DynamicUiViewState>
        get() = _viewState

    override fun onCleared() {
        super.onCleared()
        streamingCompositeDisposable.clear()
        streamingStatusController.unsubscribeFromStreamingStatusUpdates(
            streamingStatusUpdateListener
        )
    }

    override fun onCtaClicked(appUrl: String?) {
        appUrl?.let {
            clickEvents.value = moversRouter.getTabDest(it)
        }
    }

    /**
     * Custom failure handler
     * @return true if the failure was handled
     */
    open fun handleGetMoversFailure(e: Throwable): Boolean = false

    /**
     * Allows children access and update viewState livedata
     */
    fun updateViewState(action: MutableLiveData<DynamicUiViewState>.() -> Unit) {
        action(_viewState)
    }

    protected open val ignoreEmptyFetchedMoversWarning = false

    fun getMovers() {
        _viewState.value = DynamicUiViewState.Loading
        viewModelScope.launch {
            fetchData()
                .onSuccess {
                    if (it.isEmpty() && !ignoreEmptyFetchedMoversWarning) {
                        _viewState.postValue(DynamicUiViewState.Warning(noQuotesWarning))
                    } else {
                        onMoversFetched(it)
                        _viewState.postValue(DynamicUiViewState.Success)
                    }
                }
                .onFailure { e ->
                    if (!handleGetMoversFailure(e)) {
                        _viewState.value = DynamicUiViewState.Error(e.message)
                    }
                    logger.error("Unable to fetch market indices", e)
                }
        }
    }

    protected abstract suspend fun fetchData(): ETResult<List<MoverItem>>

    open fun openAccountSelection() {
        logger.debug("Show account selection dropdown")
    }

    private fun onMoversFetched(movers: List<MoverItem>) {
        val list = Collections.synchronizedList(movers)
        itemsComparator?.let {
            list.sortWith(it)
        }
        moverItems.postValue(list)
        if (streamingStatusController.isStreamingToggleEnabled) {
            subscribeToSymbolUpdates(list)
        }
    }

    @Suppress("LongMethod", "LongParameterList")
    protected fun createMoverItem(
        symbol: String,
        displaySymbol: String,
        price: String,
        change: String?,
        decimalPercentChange: BigDecimal?,
        optionExpiryDate: String? = null,
        optionsStrikePricePlusType: String? = null
    ): MoverItem {
        val decimalChange = change?.safeParseBigDecimal()
        val isPositiveColor = decimalChange?.signum()?.let { it >= 0 } ?: false
        val formattedPercentChange: String =
            MarketDataFormatter.formatMarketPercentDataCustom(decimalPercentChange)
        val formattedChange: String = MarketDataFormatter.formatMarketDataCustom(decimalChange)
        val formattedPrice: String =
            MarketDataFormatter.formatMarketDataCustom(price.toBigDecimalOrNull())
        return MoverItem(
            ticker = symbol,
            displaySymbol = displaySymbol,
            price = formattedPrice,
            priceDescription = resources.getString(
                R.string.label_last_price,
                formattedPrice.formatCurrencyAmount()
            ),
            gain = formattedChange,
            gainPercentage = formattedPercentChange,
            gainDescription = resources.getString(
                R.string.label_days_change_dollars_and_percent,
                formattedChange.formatCurrencyAmount(),
                formattedPercentChange.formatCurrencyAmount().removePercent()
            ),
            decimalPercentChange = decimalPercentChange,
            isPositiveGain = isPositiveColor,
            shouldDisplayOptionsData = shouldDisplayOptionsData,
            optionsExpiryDate = optionExpiryDate,
            optionsStrikePricePlusType = optionsStrikePricePlusType
        ) { view ->
            val instrumentType = if (optionExpiryDate != null) {
                InstrumentType.OPTN
            } else {
                InstrumentType.EQ
            }
            moversRouter.navigateToQuoteDetails(view, symbol, instrumentType)
        }
    }

    private fun subscribeToSymbolUpdates(movers: MutableList<MoverItem>? = moverItems.value) {
        val items = movers ?: return
        streamingCompositeDisposable.clear()
        items.forEach { moverItem ->
            val instrumentType =
                if (moverItem.optionsExpiryDate == null) InstrumentType.UNKNOWN else InstrumentType.OPTN
            streamingCompositeDisposable.add(
                streamingQuoteProvider.getQuote(
                    moverItem.ticker,
                    instrumentType,
                    symbolSubscriptionFields
                )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy { data ->
                        updateTimeStamp(data)
                        updateWithData(items, moverItem, data)
                    }
            )
        }
    }

    private fun updateTimeStamp(data: Level1Data) {
        data.timestampSinceEpoch?.let {
            logger.debug("timestampSinceEpoch: $it")
            timestampEvents.postValue(TimestampEvent(it).consumable())
        }
    }

    private fun updateWithData(
        items: MutableList<MoverItem>,
        moverItem: MoverItem,
        data: Level1Data
    ) {
        data.lastPrice?.let { price ->
            val (change, percentChange) = if (data is StockData) {
                Pair(data.dayChange, data.dayChangePercent)
            } else {
                Pair(data.change, data.changePercent)
            }
            val updatedItem = createMoverItem(
                symbol = moverItem.ticker,
                displaySymbol = moverItem.displaySymbol,
                price = price,
                change = change,
                decimalPercentChange = percentChange?.toBigDecimalOrNull(),
                optionExpiryDate = moverItem.optionsExpiryDate,
                optionsStrikePricePlusType = moverItem.optionsStrikePricePlusType
            )

            val indexOfItemToReplace = items.indexOfFirst { it.ticker == moverItem.ticker }
            if (indexOfItemToReplace >= 0 && indexOfItemToReplace < items.size) {
                items[indexOfItemToReplace] = updatedItem
            }
            itemsComparator?.let {
                items.sortWith(it)
            }
            moverItems.value = items
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onLifeCyclePause() {
        /** We want to unsubscribe from streaming */
        streamingCompositeDisposable.clear()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onLifeCycleResume() {
        /** We need to subscribe to streaming here because gets unsubscribe onPause lifecycle */
        if (streamingStatusController.isStreamingToggleEnabled) {
            subscribeToSymbolUpdates()
        } else {
            streamingCompositeDisposable.clear()
        }
    }

    protected fun resetOnAccountSelection() {
        // unsubscribe from streaming and clear the values on different account selection
        streamingCompositeDisposable.clear()
        moverItems.value = mutableListOf()
    }
}
