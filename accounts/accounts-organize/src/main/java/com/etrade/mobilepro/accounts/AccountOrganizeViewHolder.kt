package com.etrade.mobilepro.accounts

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import com.etrade.mobilepro.accounts.databinding.AccountsItemAdapterBinding
import com.etrade.mobilepro.editlist.EditListItem
import com.etrade.mobilepro.editlist.ItemViewHolder
import com.etrade.mobilepro.util.android.extension.inflater

class AccountOrganizeViewHolder(val binding: AccountsItemAdapterBinding) :
    ItemViewHolder(binding.root) {

    constructor(parent: ViewGroup) : this(AccountsItemAdapterBinding.inflate(parent.inflater, parent, false))

    override val removeView: View? = null

    override val dragHandle: View = binding.dragHandle

    override val toggleView: SwitchCompat? = null

    override val titleView: TextView = binding.title

    override fun bindTo(value: EditListItem) {
        binding.title.text = value.displayText
    }
}
