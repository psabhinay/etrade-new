package com.etrade.mobilepro.portfolio.lightstreamer

import javax.inject.Inject

private const val LS_MDP = "MDP"
private const val LS_TRADE_EVENT = "TRADE_EVENT"

/**
 * Stream id factory for user portfolio based on Market Data Push Queue Adapter:
 * https://confluence.corp.etradegrp.com/display/TDTS/Market+Data+Push+Queue+Adapter+-+Realtime+Events
 */
class MdpAccountPortfolioStreamIdFactory @Inject constructor() : AccountPortfolioStreamIdFactory {

    override fun getStreamIds(accountId: String): Set<String> {
        return setOf(
            "$LS_MDP.$accountId:$LS_TRADE_EVENT"
        )
    }
}
