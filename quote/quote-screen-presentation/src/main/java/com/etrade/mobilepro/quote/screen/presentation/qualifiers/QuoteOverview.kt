package com.etrade.mobilepro.quote.screen.presentation.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class QuoteOverview

@Qualifier
annotation class QuoteViewModelClass
