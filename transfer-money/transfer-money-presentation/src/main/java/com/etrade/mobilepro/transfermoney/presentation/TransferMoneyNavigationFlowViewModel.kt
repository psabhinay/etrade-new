package com.etrade.mobilepro.transfermoney.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.delegate.consumable

class TransferMoneyNavigationFlowViewModel : ViewModel() {

    private val _switchToTransferActivity = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val switchToTransferActivity: LiveData<ConsumableLiveEvent<Unit>>
        get() = _switchToTransferActivity

    private val _showNewActivityDetails = MutableLiveData<ConsumableLiveEvent<String>>()
    val showNewActivityDetails: LiveData<ConsumableLiveEvent<String>>
        get() = _showNewActivityDetails

    var referenceNumber: String? by consumable(null)

    fun switchToTransferActivity() {
        _switchToTransferActivity.value = ConsumableLiveEvent(Unit)
    }

    fun showNewActivityDetails(referenceNumber: String) {
        _showNewActivityDetails.value = ConsumableLiveEvent(referenceNumber)
    }
}
