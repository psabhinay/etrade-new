package com.etrade.mobilepro.transfermoney.usecase.confirm

import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.etrade.mobilepro.transfermoney.api.TransferRepeat
import com.etrade.mobilepro.transfermoney.api.repo.ConfirmRequest
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import org.threeten.bp.LocalDate

data class GetQuickTransferConfirmRequest(
    val userId: String,
    val scheduleDate: LocalDate,
    val fromAccount: TransferMoneyAccount,
    val toAccount: TransferMoneyAccount,
    val amount: String,
    val frequency: TransferRepeat,
    val platform: String,
    val iRAContribYear: String?
) {

    fun toRepositoryConfirmRequest(): ConfirmRequest = ConfirmRequest(
        userId,
        scheduleDate.format(createAmericanSimpleDateFormatter()),
        fromAccount.accountId,
        toAccount.accountId,
        amount,
        frequency,
        platform,
        iRAContribYear
    )
}
