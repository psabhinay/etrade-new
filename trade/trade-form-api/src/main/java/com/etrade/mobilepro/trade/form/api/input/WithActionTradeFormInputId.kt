package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.form.api.TradeAction
import com.etrade.mobilepro.trade.form.api.value.ActionValueId

interface WithActionTradeFormInputId : TradeFormInputId {
    val action: String
    val actionValueId: ActionValueId
}

fun WithActionTradeFormInputId.extractAction(map: InputValuesSnapshot): TradeAction? {
    return (map[action] as? String)?.let {
        actionValueId.converter.toValue(it)
    }
}

fun WithActionTradeFormInputId.extractTransactionType(map: InputValuesSnapshot, isOption: Boolean): TransactionType {
    val typeCode = map[action] as? String ?: ""
    return TransactionType.fromTypeCode(typeCode.toIntOrNull(), isOption)
}
