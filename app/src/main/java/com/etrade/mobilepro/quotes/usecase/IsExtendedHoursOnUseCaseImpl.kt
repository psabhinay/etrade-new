package com.etrade.mobilepro.quotes.usecase

import com.etrade.mobilepro.marketschedule.MarketScheduleProviderHolder
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import javax.inject.Inject

class IsExtendedHoursOnUseCaseImpl @Inject constructor(
    private val marketScheduleProviderHolder: MarketScheduleProviderHolder,
    private val streamingStatusController: StreamingStatusController
) : IsExtendedHoursOnUseCase {

    override suspend fun execute(parameter: Unit): Boolean =
        marketScheduleProviderHolder.isExtendedHourEligiblePeriod && streamingStatusController.isStreamingToggleEnabled
}
