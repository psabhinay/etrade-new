package com.etrade.mobilepro.news.data.repo

import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.api.repo.WidgetNewsRepo
import com.etrade.mobilepro.news.data.dto.NewsDto
import com.etrade.mobilepro.news.data.mapper.TextNewsMapper
import com.etrade.mobilepro.news.data.rest.NewsApiClient
import io.reactivex.Single

class DefaultWidgetNewsRepo(private val apiClient: NewsApiClient) : WidgetNewsRepo {
    private val textNewsMapper = TextNewsMapper()

    @Suppress("LongMethod")
    override fun getWidgetNews(request: NewsRequest, amount: Int): Single<List<NewsItem>> {
        val newsSource = when (request) {
            is NewsRequest.Portfolio ->
                apiClient
                    .getPortfolioNews(
                        request.resource,
                        startIndex = 0,
                        maxCount = amount,
                        stockPlan = request.includeStockPlan
                    )
                    .chainToList()
            is NewsRequest.Symbol -> apiClient.getSymbolNews(
                request.joinSymbols(),
                startIndex = 0,
                maxCount = amount
            )
                .chainToList()
            is NewsRequest.Market -> apiClient.getMarketNews(0, amount)
                .chainToList(omitTime = false) // https://jira.corp.etradegrp.com/browse/ETAND-16214
            else -> throw UnsupportedOperationException()
        }
        return newsSource.replaceWithEmptyItem()
    }

    private fun Single<List<NewsItem>>.replaceWithEmptyItem(): Single<List<NewsItem>> =
        map {
            if (it.isEmpty()) {
                listOf(NewsItem.Empty)
            } else {
                it
            }
        }

    private fun Single<NewsDto>.chainToList(omitTime: Boolean = true): Single<List<NewsItem>> {
        return this
            .toObservable()
            .map { newsDto -> newsDto.data?.newsPayload?.docsList?.docs ?: emptyList() }
            .map<List<NewsItem>> { docDto ->
                docDto.map {
                    textNewsMapper.map(it.textDoc, omitTime = omitTime)
                }
            }
            .firstOrError()
    }
}
