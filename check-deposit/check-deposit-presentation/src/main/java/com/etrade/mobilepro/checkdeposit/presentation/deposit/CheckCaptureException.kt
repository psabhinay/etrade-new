package com.etrade.mobilepro.checkdeposit.presentation.deposit

import android.content.Intent

internal class CheckCaptureException(message: String) : Exception(message) {
    constructor(message: String, data: Intent) : this(addDebugInfo(message, misnapResult = data.miSnapResultCode(), mibiData = data.mibiData()))
    constructor(message: String, misnapResult: String? = null, mibiData: String? = null) : this(addDebugInfo(message, misnapResult, mibiData))
}

private fun addDebugInfo(
    message: String,
    misnapResult: String? = null,
    mibiData: String? = null
) = "$message :: Result code: $misnapResult. MIBI data: $mibiData"
