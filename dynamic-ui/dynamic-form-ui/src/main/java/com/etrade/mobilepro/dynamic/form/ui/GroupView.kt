package com.etrade.mobilepro.dynamic.form.ui

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.dynamic.form.api.Component
import com.etrade.mobilepro.dynamic.form.api.Group
import com.etrade.mobilepro.dynamic.form.api.HorizontalGroup
import com.etrade.mobilepro.dynamic.form.api.VerticalGroup
import com.etrade.mobilepro.dynamic.form.ui.factory.ComponentViewFactoryProvider

internal class GroupView : LinearLayout, ComponentView<Group> {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    internal lateinit var viewFactoryProvider: ComponentViewFactoryProvider

    private val componentViewDelegate: ComponentViewDelegate = ComponentViewDelegate(this)

    init {
        isFocusable = false
        importantForAccessibility = IMPORTANT_FOR_ACCESSIBILITY_NO
    }

    override fun render(component: Group, owner: LifecycleOwner) {
        componentViewDelegate.render(component, owner)
        orientation = when (component) {
            is HorizontalGroup -> HORIZONTAL
            is VerticalGroup -> VERTICAL
        }

        component.components.forEach {
            addComponent(it, owner)
        }
    }

    private fun addComponent(component: Component, owner: LifecycleOwner) {
        if (component is Group) {
            addGroup(component, owner)
        } else {
            val factory = requireNotNull(viewFactoryProvider.viewFactories[component::class]) {
                "Component ${component::class} is not supported."
            }
            addComponentView(factory.createView(context), component, owner)
        }
    }

    private fun addGroup(group: Group, owner: LifecycleOwner) {
        addView(
            GroupView(context)
                .also { it.viewFactoryProvider = viewFactoryProvider }
                .apply { render(group, owner) }
        )
    }

    private fun <C : Component, V : ComponentView<C>> addComponentView(componentView: V, component: C, owner: LifecycleOwner) {
        if (componentView is View) {
            val params = if (orientation == VERTICAL) {
                LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            } else {
                LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT).apply {
                    weight = 1f
                }
            }
            addView(componentView, params)
        }
        componentView.render(component, owner)
    }
}
