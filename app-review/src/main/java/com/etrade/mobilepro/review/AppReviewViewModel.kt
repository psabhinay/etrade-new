package com.etrade.mobilepro.review

import android.app.Activity
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.google.android.play.core.ktx.launchReview
import com.google.android.play.core.ktx.requestReview
import com.google.android.play.core.review.ReviewInfo
import com.google.android.play.core.review.ReviewManagerFactory
import com.localytics.androidx.Localytics
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class AppReviewViewModel @Inject constructor(
    context: Context
) : ViewModel() {

    private val logger: Logger = LoggerFactory.getLogger(AppReviewViewModel::class.java)
    private val reviewManager = ReviewManagerFactory.create(context)

    private var reviewInfo: ReviewInfo? = null
    private val _showAppReview: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()
    val showAppReview: LiveData<ConsumableLiveEvent<Unit>>
        get() = _showAppReview

    init {
        Localytics.setMessagingListener(AppReviewLocalyticsMessagingListener(::requestAppReview))
    }

    private fun requestAppReview() {
        viewModelScope.launch {
            runCatching { reviewManager.requestReview() }
                .onSuccess {
                    reviewInfo = it
                    _showAppReview.postValue(ConsumableLiveEvent(Unit))
                }
                .onFailure {
                    logger.error("There was some problem with ReviewManager.requestReview", it)
                }
        }
    }

    fun launchReview(activity: Activity) {
        viewModelScope.launch {
            runCatching { reviewManager.launchReview(activity, requireNotNull(reviewInfo)) }
                .onSuccess {
                    // The flow has finished. The API does not indicate whether the user
                    // reviewed or not, or even whether the review dialog was shown. Thus, no
                    // matter the result, we continue our app flow.
                }
                .onFailure {
                    logger.error("There was some problem with ReviewManager.launchReview", it)
                }
        }
    }
}
