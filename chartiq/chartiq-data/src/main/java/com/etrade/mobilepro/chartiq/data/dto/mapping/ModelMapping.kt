package com.etrade.mobilepro.chartiq.data.dto.mapping

import com.etrade.mobilepro.chartiq.api.model.Account
import com.etrade.mobilepro.chartiq.api.model.OrderDetails
import com.etrade.mobilepro.chartiq.api.model.SymbolInfo
import com.etrade.mobilepro.chartiq.data.AlertDialogAction
import com.etrade.mobilepro.chartiq.data.AlertDialogContent
import com.etrade.mobilepro.chartiq.data.dto.AccountDto
import com.etrade.mobilepro.chartiq.data.dto.AlertActionDto
import com.etrade.mobilepro.chartiq.data.dto.AlertDialogContentDto
import com.etrade.mobilepro.chartiq.data.dto.MarketDataDto
import com.etrade.mobilepro.chartiq.data.dto.PriceDto
import com.etrade.mobilepro.chartiq.data.dto.SymbolDto
import com.etrade.mobilepro.chartiq.data.dto.SymbolInfoDto
import com.etrade.mobilepro.chartiq.data.dto.Toggle
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.OrderAction
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.orders.data.dto.LegDetailDto
import com.etrade.mobilepro.orders.data.dto.OrderDto
import com.etrade.mobilepro.orders.data.dto.OrderItemDto
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.util.safeParseBigDecimal

internal fun Account.toDto() = AccountDto(
    id = id,
    displayName = displayName
)

private const val MS_IN_SECOND = 1000L
internal fun Level1Data.toMarketData(ticker: String?) = lastPrice?.let {
    MarketDataDto(
        price = it.toDoubleOrNull() ?: 0.0,
        volume = lastVolume?.toLongOrNull() ?: 0L,
        timestamp = (timestampSinceEpoch ?: 0L) / MS_IN_SECOND,
        ticker = ticker
    )
}

private fun Boolean.toToggle() = if (this) {
    Toggle.ON
} else {
    Toggle.OFF
}

internal fun SymbolInfo.toDto() = SymbolInfoDto(
    ticker = ticker,
    securityType = instrumentType.typeCode,
    isExtendedHours = isExtendedHours.toToggle(),
    isStreaming = isStreaming.toToggle(),
    price = PriceDto(lastPrice)
)

internal fun WithSymbolInfo.toDto() = SymbolDto(
    symbol = symbol,
    securityType = instrumentType.typeCode
)

internal fun AlertActionDto.toAlertAction() = AlertDialogAction(
    title = title,
    callback = callback
)

internal fun AlertDialogContentDto.toAlertDialogContent() = AlertDialogContent(
    alertId = alert.id,
    title = alert.title,
    message = alert.message,
    positiveButton = actions.getOrNull(0)?.toAlertAction(),
    negativeButton = actions.getOrNull(1)?.toAlertAction(),
    neutralButton = actions.getOrNull(2)?.toAlertAction()
)

internal fun OrderItemDto.toOrderDetails(): OrderDetails {
    val order = order.first()
    val legDetails = order.legDetails.first()
    val orderAction = OrderAction.fromString(legDetails.orderAction)
    val transactionType = TransactionType.from(orderAction, legDetails.positionEffect, false)
    val priceType = getPriceType(order, legDetails)
    val stopPrice = when (priceType) {
        PriceType.TRAILING_STOP_DOLLAR,
        PriceType.TRAILING_STOP_PERCENT,
        PriceType.TRAILING_STOP_LIMIT -> order.advancedOrder.offsetValue
        else -> wholeOrderHeader.stopPrice
    }.safeParseBigDecimal()

    return OrderDetails(
        accountId = accountId,
        orderNumber = orderNumber,
        symbol = legDetails.displaySymbol,
        instrumentType = InstrumentType.from(legDetails.typeCode),
        transactionType = transactionType,
        priceType = priceType,
        sharesAmount = legDetails.quantityValue.safeParseBigDecimal(),
        limitPrice = wholeOrderHeader.limitPrice.safeParseBigDecimal(),
        stopPrice = stopPrice
    )
}

private fun OrderItemDto.getPriceType(order: OrderDto, legDetails: LegDetailDto): PriceType {
    val advancedOrderType = AdvancedOrderType.fromTypeCode(order.advancedOrder.aoType)
    val typeCode = wholeOrderHeader.orderTrigger
    return if (advancedOrderType == AdvancedOrderType.AO_TRAIL_STOP) {
        PriceType.getPriceTypeForAdvancedOrderType(
            advancedOrderType,
            order.advancedOrder.offsetType,
            legDetails.orderAction,
            typeCode,
        )
    } else {
        PriceType.mapPriceType(typeCode)
    }
}
