package com.etrade.mobilepro.pushnotification.data.rest

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "SetAndroidProfileItemResponse", strict = false)
class AlertPushSettingsResponseDto {
    @field:Element(name = "Result", required = false)
    var result: String? = null
}
