package com.etrade.mobilepro.dynamicui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.dynamicui.api.AttachStateAware
import com.etrade.mobilepro.dynamicui.api.PositionAware
import com.etrade.mobilepro.dynamicui.api.SelfRemoving
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import org.slf4j.LoggerFactory

abstract class BaseRecyclerAdapter(
    private val lifecycleOwner: LifecycleOwner
) : RecyclerView.Adapter<BaseRecyclerAdapter.GenericDataBindingViewHolder>() {

    private val compositeDisposable = CompositeDisposable()
    protected val logger = LoggerFactory.getLogger(this::class.java)

    fun disposeAll() {
        compositeDisposable.clear()
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GenericDataBindingViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater, viewType, parent, false
        )
        binding.lifecycleOwner = lifecycleOwner
        return GenericDataBindingViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: GenericDataBindingViewHolder,
        position: Int
    ) {
        val obj = getObjForPosition(position)
        holder.bind(getVariableIdForPosition(position), obj)
    }

    override fun getItemViewType(position: Int): Int {
        return getLayoutIdForPosition(position)
    }

    override fun onViewAttachedToWindow(holder: GenericDataBindingViewHolder) {
        super.onViewAttachedToWindow(holder)
        (holder.obj as? AttachStateAware)?.onAttached(holder)
    }

    override fun onViewDetachedFromWindow(holder: GenericDataBindingViewHolder) {
        super.onViewDetachedFromWindow(holder)
        (holder.obj as? AttachStateAware)?.onDetached(holder)
    }

    protected abstract fun getObjForPosition(position: Int): Any?

    protected abstract fun getLayoutIdForPosition(position: Int): Int

    protected abstract fun getVariableIdForPosition(position: Int): Int

    protected abstract fun removeItemAt(position: Int)

    inner class GenericDataBindingViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        internal var obj: Any? = null

        fun bind(variableId: Int, obj: Any?) {
            this.obj = obj
            binding.setVariable(variableId, obj)
            if (obj is PositionAware) {
                obj.currentPosition = bindingAdapterPosition
            }
            binding.executePendingBindings()
            if (obj is SelfRemoving) {
                addSelfRemovalWidgetSubscription(obj.selfRemovalRequest())
            }
        }

        private fun addSelfRemovalWidgetSubscription(signal: Observable<Boolean>) {
            compositeDisposable.add(
                signal
                    .subscribeBy(
                        onNext = { needsRemoving ->
                            if (needsRemoving) {
                                removeItemAt(bindingAdapterPosition)
                            }
                        },
                        onError = {
                            logger.debug(it.message)
                        }
                    )
            )
        }
    }
}
