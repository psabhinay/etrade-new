package com.etrade.mobilepro.markets.data.response

import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false, name = "GetFutureQuotes")
data class MarketFutureResponse(
    @field:ElementList(name = "Output", inline = true, required = false)
    var quotes: ArrayList<FutureQuote>? = null
)
