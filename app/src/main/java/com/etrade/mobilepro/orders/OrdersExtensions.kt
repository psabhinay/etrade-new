package com.etrade.mobilepro.orders

import android.content.res.Resources
import com.etrade.mobilepro.R
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.extensions.android.getLabel

fun AdvancedOrderType.getLabel(orderType: OrderType, resources: Resources): String {
    return when {
        AdvancedOrderType.bracketedOrderTypes.contains(this) -> resources.getString(R.string.bracketed)
        AdvancedOrderType.contingentOrderTypes.contains(this) -> resources.getString(R.string.contingent)
        OrderType.conditionalOrderTypes.contains(orderType) -> resources.getString(orderType.getLabel())
        else -> resources.getString(R.string.unknown)
    }
}
