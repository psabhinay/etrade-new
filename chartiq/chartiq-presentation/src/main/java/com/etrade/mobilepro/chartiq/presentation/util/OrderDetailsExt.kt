package com.etrade.mobilepro.chartiq.presentation.util

import android.content.res.Resources
import com.etrade.mobilepro.chartiq.api.model.OrderDetails
import com.etrade.mobilepro.chartiq.presentation.R
import com.etrade.mobilepro.orders.api.PriceType

internal fun OrderDetails.getPriceMessage(resources: Resources) = when (priceType) {
    PriceType.LIMIT -> resources.getString(R.string.chartiq_order_dialog_price_template, limitPrice)
    PriceType.STOP -> resources.getString(R.string.chartiq_order_dialog_price_template, stopPrice)
    PriceType.STOP_LIMIT -> resources.getString(R.string.chartiq_order_dialog_price_pair_template, stopPrice, limitPrice)
    PriceType.TRAILING_STOP_DOLLAR -> resources.getString(R.string.chartiq_order_dialog_price_template, stopPrice)
    PriceType.TRAILING_STOP_PERCENT -> resources.getString(R.string.chartiq_order_dialog_price_percent_template, stopPrice)
    else -> ""
}.orEmpty()
