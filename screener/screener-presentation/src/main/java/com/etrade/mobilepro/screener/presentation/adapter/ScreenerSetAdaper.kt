package com.etrade.mobilepro.screener.presentation.adapter

import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.presentation.adapter.delegate.ScreenerDisclosuresAdapterDelegate
import com.etrade.mobilepro.screener.presentation.adapter.delegate.ScreenerFooterAdaperDelegate
import com.etrade.mobilepro.screener.presentation.adapter.delegate.ScreenerSetAdapterDelegate
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class ScreenerSetAdaper(
    screenerSetSelectedListener: (ScreenerItem.ScreenerSet) -> Unit,
    screenerSetClearListener: (ScreenerItem.ScreenerSet) -> Unit
) : ListDelegationAdapter<MutableList<ScreenerItem>>() {
    init {
        delegatesManager.addDelegate(ScreenerFooterAdaperDelegate())
        delegatesManager.addDelegate(ScreenerSetAdapterDelegate(screenerSetSelectedListener, screenerSetClearListener))
        delegatesManager.addDelegate(ScreenerDisclosuresAdapterDelegate())
        items = mutableListOf()
    }

    fun submitItems(newItems: List<ScreenerItem>) {
        dispatchUpdates(items, newItems)
        items.clear()
        items.addAll(newItems)
    }
}
