package com.etrade.mobilepro.util.updater

import com.etrade.mobilepro.portfolio.PortfolioPositionsDisplayMode
import com.etrade.mobilepro.portfolio.data.PortfolioColumnRepo
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import java.io.File
import javax.inject.Inject

/**
 * Migrates portfolio column's sort order.
 *
 * @param portfolioColumnRepo a repository that will be used to store columns order
 */
class PortfolioColumnMigration @Inject constructor(private val portfolioColumnRepo: PortfolioColumnRepo) : Migration {

    @SuppressWarnings("MagicNumber")
    private val idMap: Map<Int, PortfolioColumn> = mapOf(
        10 to PortfolioColumn.LAST_PRICE,
        11 to PortfolioColumn.DAYS_GAIN_DOLLAR,
        12 to PortfolioColumn.DAYS_GAIN_PERCENT,
        13 to PortfolioColumn.TOTAL_GAIN_DOLLAR,
        14 to PortfolioColumn.TOTAL_GAIN_PERCENT,
        15 to PortfolioColumn.MARK,
        16 to PortfolioColumn.BID,
        17 to PortfolioColumn.ASK,
        18 to PortfolioColumn.QUANTITY,
        19 to PortfolioColumn.PRICE_PAID,
        20 to PortfolioColumn.MARKET_VALUE,
        21 to PortfolioColumn.CHANGE_PERCENT,
        22 to PortfolioColumn.CHANGE_DOLLAR,
        23 to PortfolioColumn.TODAYS_CLOSE,
        24 to PortfolioColumn.VOLUME,
        25 to PortfolioColumn.MARKET_CAP,
        26 to PortfolioColumn.PRICE_EARNING_RATIO,
        27 to PortfolioColumn.EARNINGS_PER_SHARE,
        28 to PortfolioColumn.FIFTY_TWO_WEEK_HIGH,
        29 to PortfolioColumn.FIFTY_TWO_WEEK_LOW,
        30 to PortfolioColumn.IMPLIED_VOLATILITY_PERCENT,
        31 to PortfolioColumn.DELTA,
        32 to PortfolioColumn.GAMMA,
        33 to PortfolioColumn.VEGA,
        34 to PortfolioColumn.THETA,
        36 to PortfolioColumn.DAYS_TO_EXPIRE,
        37 to PortfolioColumn.BASE_SYMBOL_AND_PRICE
    )

    override suspend fun migrate(databaseFile: File) {
        migrate(databaseFile, "PORTFOLIOTBL", PortfolioTableViewType.STANDARD_VIEW, PortfolioPositionsDisplayMode.TABLE)
        migrate(databaseFile, "PORTFOLIOOPTTBL", PortfolioTableViewType.OPTIONS_VIEW, PortfolioPositionsDisplayMode.TABLE)
        migrate(databaseFile, "PORTFOLIOLISTTBL", PortfolioTableViewType.STANDARD_VIEW, PortfolioPositionsDisplayMode.LIST)
        migrate(databaseFile, "PORTFOLIOTILETBL", PortfolioTableViewType.STANDARD_VIEW, PortfolioPositionsDisplayMode.TILE)
    }

    private suspend fun migrate(databaseFile: File, tableName: String, type: PortfolioTableViewType, mode: PortfolioPositionsDisplayMode) {
        readColumns(databaseFile, tableName)
            .mapNotNull { idMap[it] }
            .takeIf { it.isNotEmpty() }
            ?.run { portfolioColumnRepo.savePortfolioColumns(PositionTableMode.PORTFOLIO_VIEW, type, this, mode) }
    }
}
