package com.etrade.mobilepro.calendar.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.calendar.databinding.AdapterWeekDayTitleBinding

class WeekDayTitleViewHolder(private val binding: AdapterWeekDayTitleBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(title: String) {
        itemView.contentDescription = title
        binding.title.text = title.first().toString()
    }
}
