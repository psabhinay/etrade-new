package com.etrade.mobilepro.pushnotification.usecase

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.pushnotification.api.repo.AlertPushSettingsRepo
import javax.inject.Inject

/**
 * Use case which depending on business rules can decide if the device must be subscribed to or unsubscribed from smart alerts.
 */
interface ManageSmartAlertSubscriptionUseCase {
    suspend fun execute(): ETResult<Unit>
}

class ManageSmartAlertSubscriptionUseCaseImpl @Inject constructor(
    private val smartAlertSubscription: SmartAlertSubscriptionUseCase,
    private val alertRepo: AlertPushSettingsRepo
) : ManageSmartAlertSubscriptionUseCase {

    override suspend fun execute(): ETResult<Unit> {
        val isSubscriptionRequired = alertRepo.getCurrentSettings().let { it.accountAlertsEnabled || it.stockAlertsEnabled }

        return if (isSubscriptionRequired) {
            smartAlertSubscription.subscribe()
        } else {
            smartAlertSubscription.unsubscribe()
        }
    }
}
