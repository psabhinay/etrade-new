package com.etrade.mobilepro.home

import android.content.res.Resources
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.navigation.FloatingWindow
import androidx.navigation.NavDestination
import com.etrade.mobilepro.R
import com.etrade.mobilepro.accounts.util.android.view.AccountViewType
import com.etrade.mobilepro.accounts.util.android.view.AccountsSpinnerModel
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dropdown.viewmodel.DropDownModel
import com.etrade.mobilepro.dropdown.viewmodel.DropDownViewModel
import com.etrade.mobilepro.livedata.filterNotNull
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

private const val TITLE = "title"

class ToolbarViewModel @Inject constructor(
    private val resources: Resources,
    private val userViewModel: UserViewModel,
    private val cachedDaoProvider: CachedDaoProvider
) : ViewModel(), DropDownViewModel<AccountsSpinnerModel> {

    val displayDropDownArrow: LiveData<Boolean>
        get() = _displayDropDownArrow
    val isVisible: LiveData<Boolean>
        get() = _isVisible
    val isSearchVisible: LiveData<Boolean>
        get() = _isSearchVisible
    val toolbarContentDescription: LiveData<CharSequence>
        get() = _toolbarContentDescription
    val toolbarTitle: LiveData<CharSequence>
        get() = _toolbarTitle
    val resetToolbar: LiveData<ConsumableLiveEvent<Unit>>
        get() = _resetToolbar

    val isTitleNavigationEnabled: Boolean
        get() = titleNavigationEnabled

    val dropDownEnabled: LiveData<Boolean?>
        get() = _dropDownEnabled

    val isToolBarLocked: Boolean
        get() = _toolBarLock

    /**
     * Collection of all destinations which should display an account selector.
     */
    private val destinationsWithAccountSelector = destinationsWithAccountNameTitle - R.id.completeviewFragment

    /**
     * Collection of destinations that do not want to update title on destination change. Ones that have toolbar logic of their own.
     */
    private val destinationsDoNotUpdateTitle = setOf(R.id.completeviewFragment)

    /**
     * Destinations without quote lookup menu item.
     */
    private val destinationsWithQuoteLookup = setOf(
        R.id.accountTabHostFragment,
        R.id.allBrokerageAccountsFragment,
        R.id.completeviewFragment,
        R.id.marketFragment,
        R.id.overviewFragment,
        R.id.bankFragment,
        R.id.watchlistContainerFragment
    )

    /**
     * Destinations where the toolbar should not be modified
     */
    private val destinationsWithLockToolBar = setOf(
        R.id.watchlistContainerFragment
    )

    /**
     * Destinations where the toolbar should not be visible, they will implement a custom toolbar
     */
    private val destinationsWithNoToolbar = setOf<Int>()

    private val _displayDropDownArrow: MutableLiveData<Boolean> = MutableLiveData()
    private val _isSearchVisible: MutableLiveData<Boolean> = MutableLiveData()
    private val _toolbarContentDescription: MutableLiveData<CharSequence> = MutableLiveData()
    private val _toolbarTitle: MutableLiveData<CharSequence> = MutableLiveData()
    private val _resetToolbar: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()
    private val _dropDownEnabled: MediatorLiveData<Boolean?> = MediatorLiveData()
    private var _toolBarLock: Boolean = false
    private val _isVisible: MutableLiveData<Boolean> = MutableLiveData()

    private var titleNavigationEnabled: Boolean = true
    private var previousDestination: NavDestination? = null

    init {
        cachedDaoProvider.open()
        _dropDownEnabled.addSource(userViewModel.userSessionState) {
            if (isToolBarLocked) {
                _dropDownEnabled.value = null
            } else {
                _dropDownEnabled.value = isDropDownEnabled(userViewModel.accountList)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        cachedDaoProvider.close()
    }

    fun resetToolbar() {
        _resetToolbar.value = ConsumableLiveEvent(Unit)
    }

    fun manualToolbar(displayDropDownArrow: Boolean, title: CharSequence?, titleContentDescription: String? = null) {
        _displayDropDownArrow.value = displayDropDownArrow
        title?.let {
            _toolbarTitle.value = it
            _toolbarContentDescription.value = titleContentDescription ?: it
        }
    }

    fun updateDestination(destination: NavDestination, arguments: Bundle?) {
        previousDestination = (destination.takeIf { it !is FloatingWindow } ?: previousDestination)?.also {
            updateSelectedAccount(destination, it, arguments)
            updateMenu(it.id)
            updateToolbar(it, arguments)
        }
    }

    private fun updateSelectedAccount(destination: NavDestination, previousDestination: NavDestination, arguments: Bundle?) {
        if (previousDestination.id == R.id.allBrokerageAccountsFragment && destination is FloatingWindow) {
            return
        }
        arguments?.getString("accountUuid")?.let { accountUuid ->
            userViewModel.updateSelectedAccount(accountUuid)
        }
    }

    @Suppress("LongMethod")
    private fun updateToolbar(destination: NavDestination, arguments: Bundle?) {
        val destinationId = destination.id
        titleNavigationEnabled = destinationsWithAccountNameTitle.contains(destinationId)

        _isVisible.value = !destinationsWithNoToolbar.contains(destinationId)

        if (titleNavigationEnabled) {
            _displayDropDownArrow.value = destinationsWithAccountSelector.contains(destination.id) &&
                userViewModel.isAuthenticated &&
                userViewModel.hasMoreThanOneAccount

            _dropDownEnabled.value = _displayDropDownArrow.value

            onAllowedDestination(destinationId) {
                userViewModel.currentSelectedAccount.value?.let { account ->
                    _toolbarTitle.value = account.accountDisplayName
                    _toolbarContentDescription.value = if (userViewModel.hasMoreThanOneAccount) {
                        resources.getString(
                            R.string.label_dropdown_description,
                            account.contentDescription
                        )
                    } else {
                        resources.getString(
                            R.string.label_header_description,
                            account.contentDescription
                        )
                    }
                }
            }
        } else {
            onAllowedDestination(destinationId) {
                _displayDropDownArrow.value = false

                val title = destination.label ?: arguments?.getString(TITLE)

                title?.let {
                    _toolbarTitle.value = it
                    _toolbarContentDescription.value =
                        resources.getString(R.string.label_header_description, it)
                }
            }
        }
    }

    private fun updateMenu(destinationId: Int) {
        _isSearchVisible.value = destinationsWithQuoteLookup.contains(destinationId)
    }

    override val dropDown: LiveData<DropDownModel<AccountsSpinnerModel>>
        get() = userViewModel.currentSelectedAccount.map { selectedAccount ->
            getAccountsSpinnerModel(userViewModel.accountList, selectedAccount)
        }.filterNotNull()

    private fun getAccountsSpinnerModel(
        accounts: List<AccountsSpinnerModel>?,
        selectedAccount: AccountsSpinnerModel?
    ): DropDownModel<AccountsSpinnerModel>? {
        accounts ?: return null

        if (isDropDownEnabled(accounts)) {
            return DropDownModel(
                tag = "ACCOUNTS",
                title = resources.getString(R.string.title_accounts),
                items = accounts,
                initialSelectedPosition = getIndexOfSelectedAccount(accounts, selectedAccount),
                dividerPosition = accounts.dividerPosition()
            )
        }
        return null
    }

    override fun selectDropDownItem(item: AccountsSpinnerModel) {
        if (userViewModel.currentSelectedAccount.value != item) {
            userViewModel.navigateToSelectedAccountTabByTitle(item.accountViewType, item.account)
            userViewModel.updateSelectedAccount(item)
        }
    }

    private fun getIndexOfSelectedAccount(accounts: List<AccountsSpinnerModel>, selectedAccount: AccountsSpinnerModel?): Int {
        if (selectedAccount?.accountViewType == AccountViewType.ALL_BROKERAGE_ACCOUNT_VIEW) {
            return 1
        }
        for ((index, spinnerItem) in accounts.withIndex()) {
            if (spinnerItem.account?.accountUuid == selectedAccount?.account?.accountUuid) {
                return index
            }
        }
        return 0
    }

    private fun onAllowedDestination(destinationId: Int, updater: () -> Unit) {
        if (!destinationsDoNotUpdateTitle.contains(destinationId)) {
            updater.invoke()
        }
    }

    fun destinationNeedsToBeLocked(destination: NavDestination) {
        _toolBarLock = destination.id in destinationsWithLockToolBar || destination is FloatingWindow
    }

    private fun isDropDownEnabled(accounts: List<AccountsSpinnerModel>) =
        accounts.size > 1 && userViewModel.isAuthenticated
}
