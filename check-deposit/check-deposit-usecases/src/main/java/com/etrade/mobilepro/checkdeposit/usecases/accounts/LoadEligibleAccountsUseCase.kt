package com.etrade.mobilepro.checkdeposit.usecases.accounts

import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.checkdeposit.deposit.service.DepositEligibleAccountsResponseDto
import com.etrade.mobilepro.checkdeposit.deposit.service.RemoteCheckDepositService
import com.etrade.mobilepro.checkdeposit.usecases.api.EligibleAccounts
import com.etrade.mobilepro.checkdeposit.usecases.api.GetCheckDepositAccountsUseCase
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET

class LoadEligibleAccountsUseCase constructor(
    private val checkDepositService: RemoteCheckDepositService
) : GetCheckDepositAccountsUseCase {

    private val noEligibleAccountsMessageCodes: Set<String> = setOf(
        "RESTR_REASON_USER_ACCT_RESTR_STATUS"
    )

    override suspend fun execute(parameter: Unit): ETResult<EligibleAccounts> = runCatchingET {
        val response = checkDepositService.getEligibleAccounts()

        val error = response.extractError()
        if (error is ServerError.Known && error.messages.all { noEligibleAccountsMessageCodes.contains(it.code) }) {
            EligibleAccounts(
                termsSigned = false,
                eligibleAccounts = emptyList()
            )
        } else {
            response.doIfSuccessfulOrThrow {
                it.toEligibleAccounts()
            }
        }
    }

    private fun DepositEligibleAccountsResponseDto.toEligibleAccounts(): EligibleAccounts {
        val termsSigned = depositEligibleAccountsDto?.signedForTerms ?: false
        val accounts = depositEligibleAccountsDto?.accountList?.map { accountDto ->
            DepositAccount(
                accountId = accountDto.accountId,
                nickname = accountDto.nickName,
                instNo = accountDto.instNo,
                accountType = accountDto.accountType,
                accountCategory = accountDto.accountCategory
            )
        }
        return EligibleAccounts(
            termsSigned = termsSigned,
            eligibleAccounts = accounts.orEmpty()
        )
    }
}
