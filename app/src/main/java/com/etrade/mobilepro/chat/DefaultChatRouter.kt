package com.etrade.mobilepro.chat

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.annotation.IdRes
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.etrade.mobilepro.R
import com.etrade.mobilepro.chat.presentation.ChatRouter
import com.etrade.mobilepro.chat.presentation.common.ChatConstants.CHAT_START_POINT_MESSAGE
import com.etrade.mobilepro.chat.presentation.setup.ChatSetupFragmentDirections
import com.etrade.mobilepro.home.MainActivity
import com.etrade.mobilepro.messagecenter.presentation.common.MessageParcel
import javax.inject.Inject

class DefaultChatRouter @Inject constructor() : ChatRouter {
    private val handler = Handler(Looper.getMainLooper())

    override fun openMenu(navController: NavController) {
        popFragmentIfExist(navController, R.id.menuFragment)
        navController.navigate(R.id.action_pop_menu_fragment_stack_to_menu_fragment)
    }

    override fun openChatMessages(navController: NavController, chatSessionData: Bundle?) {
        navController.navigate(ChatSetupFragmentDirections.actionChatSetupToChatMessages(chatSessionData))
    }

    override fun openChatSetup(navController: NavController) {
        navController.navigate(R.id.action_chat_setup)
    }

    override fun popChat(activity: Activity?) {
        if (activity is MainActivity) {
            activity.findNavController(R.id.nav_host_fragment).run {
                popBackStack()
            }
        }
    }

    override fun openMessageCenter(navController: NavController, bundle: Bundle) {
        val message: MessageParcel? = bundle.getParcelable(CHAT_START_POINT_MESSAGE)
        if (!popFragmentIfExist(navController, R.id.messageCenterListFragment)) {
            navController.navigate(
                R.id.action_home_to_message_center_list,
                Bundle().apply { putParcelable(CHAT_START_POINT_MESSAGE, message) }
            )
        }
    }

    private fun popFragmentIfExist(navController: NavController, @IdRes destination: Int): Boolean =
        handler.post { navController.popBackStack(destination, false) }
}
