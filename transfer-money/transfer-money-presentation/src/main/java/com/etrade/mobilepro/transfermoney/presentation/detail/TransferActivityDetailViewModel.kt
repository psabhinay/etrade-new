package com.etrade.mobilepro.transfermoney.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.livedata.postUpdateValue
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.transfermoney.api.TransferMoneyActivity
import com.etrade.mobilepro.transfermoney.api.repo.TransferMoneyRepo
import com.etrade.mobilepro.transfermoney.presentation.common.isRecurring
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val RESULT_CODE_OK = "0"

class TransferActivityDetailViewModel @Inject constructor(
    private val user: User,
    private val transferMoneyRepo: TransferMoneyRepo,
    private val dispatchers: DispatcherProvider
) : ViewModel() {
    private val _viewState = MutableLiveData<ViewState>().apply { value = ViewState(isLoading = true) }
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    internal fun initialize(item: TransferMoneyActivity) {
        _viewState.value = ViewState(item = item)
    }

    internal fun cancelTransfer(onlyCurrent: Boolean) {
        _viewState.value?.item?.let { item ->
            viewModelScope.launch(dispatchers.IO) {
                _viewState.postUpdateValue { it.copy(isLoading = true, hasError = false) }
                transferMoneyRepo
                    .deleteTransfer(user.getUserId().toString(), item.referenceNumber, deleteSeries = !onlyCurrent)
                    .fold(
                        onSuccess = {
                            this@TransferActivityDetailViewModel.handleSuccessCancellation(it, onlyCurrent)
                        },
                        onFailure = {
                            _viewState.postUpdateValue {
                                it.copy(
                                    isLoading = false,
                                    hasError = true,
                                    retry = { cancelTransfer(onlyCurrent) }
                                )
                            }
                        }
                    )
            }
        }
    }

    internal fun hasRecurringItem(): Boolean = viewState.value?.item?.isRecurring() == true

    private fun handleSuccessCancellation(resultCode: String, onlyCurrent: Boolean) =
        if (resultCode == RESULT_CODE_OK) {
            _viewState.postUpdateValue {
                it.copy(
                    isLoading = false,
                    itemsDeleted = if (onlyCurrent) { Amount.ONE } else { Amount.SERIES },
                    hasError = false
                )
            }
        } else {
            _viewState.postUpdateValue {
                it.copy(
                    isLoading = false,
                    hasError = true,
                    retry = { cancelTransfer(onlyCurrent) }
                )
            }
        }

    internal data class ViewState(
        val item: TransferMoneyActivity? = null,
        val itemsDeleted: Amount = Amount.NONE,
        val isLoading: Boolean = false,
        val hasError: Boolean = false,
        val retry: (() -> Unit)? = null
    )

    internal enum class Amount {
        NONE, ONE, SERIES
    }
}
