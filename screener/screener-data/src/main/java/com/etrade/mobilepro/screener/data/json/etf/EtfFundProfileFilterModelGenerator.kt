package com.etrade.mobilepro.screener.data.json.etf

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_EQUALS
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.OPERATOR_LIKE
import com.etrade.mobilepro.screener.api.OPERATOR_NOT_EQUAL_TO
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfFundProfileFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val MORNINGSTAR_FIELD = "MorningstarRatingSinceIncep"

private const val FAMILY_FIELD = "FamilyName"
private const val CATEGORIES_FIELD = "FundCategory"
private const val GROUP_FIELD = "SuperCategory"

private const val EXPENSE_RATIO_FIELD = "ExpenseRatio"
private const val LEVERAGED_FIELD = "ETFIsLeveraged"
private const val EXCHANGE_TRADED_NOTES_FIELD = "IsETN"
private const val STARS_ENABLED_FIELD = "AllStarListFlag"
private const val OPTIONS_AVAILABLE_FIELD = "OffersOptions"

class EtfFundProfileFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    @Suppress("LongMethod", "ComplexMethod") // not complex method, there is no nesting conditions
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val etfFundProfileFilter = screenerFilter as EtfFundProfileFilter
        val arguments = mutableListOf<Argument>()

        if (!etfFundProfileFilter.fundFamily.isAllItemsElement) {
            arguments.add(Argument(listOf(getLikeClause(listOf(etfFundProfileFilter.fundFamily.id))), FAMILY_FIELD))
        }
        if (etfFundProfileFilter.fundCategories.isNotEmpty()) {
            val values = if (etfFundProfileFilter.fundCategories.find { it.isAllItemsElement } != null) {
                listOf("%")
            } else {
                etfFundProfileFilter.fundCategories.map { it.id }
            }

            arguments.add(Argument(listOf(getLikeClause(values)), CATEGORIES_FIELD, listOf()))
        }
        if (!etfFundProfileFilter.etfFundGroup.isAllItemsElement) {
            arguments.add(Argument(listOf(getLikeClause(listOf(etfFundProfileFilter.etfFundGroup.id))), GROUP_FIELD))
        } else {
            arguments.add(Argument(listOf(getLikeClause(listOf("%"))), GROUP_FIELD))
        }
        if (etfFundProfileFilter.morningStarRating.isNotEmpty()) {
            arguments.add(Argument(getStarClauses(etfFundProfileFilter), MORNINGSTAR_FIELD))
        }
        if (etfFundProfileFilter.isExpenseRatioValueSelected()) {
            arguments.add(Argument(getExpenseRatioValuesClauses(etfFundProfileFilter), EXPENSE_RATIO_FIELD))
        }
        if (etfFundProfileFilter.leveragedEtfsExclude) {
            arguments.add(Argument(listOf(excludeClause), LEVERAGED_FIELD))
        }
        if (etfFundProfileFilter.leveragedEtfsShowOnly) {
            arguments.add(Argument(listOf(includeClause), LEVERAGED_FIELD))
        }
        if (etfFundProfileFilter.exchangeTradedNotesExclude) {
            arguments.add(Argument(listOf(excludeClause), EXCHANGE_TRADED_NOTES_FIELD))
        }
        if (etfFundProfileFilter.exchangeTradedNotesShowOnly) {
            arguments.add(Argument(listOf(includeClause), EXCHANGE_TRADED_NOTES_FIELD))
        }
        if (etfFundProfileFilter.allStarsFunds) {
            arguments.add(Argument(listOf(includeClause), STARS_ENABLED_FIELD))
        }
        if (etfFundProfileFilter.optionsAvailable) {
            arguments.add(Argument(listOf(includeClause), OPTIONS_AVAILABLE_FIELD))
        }

        return PartialScreenerParams(arguments)
    }

    private fun getExpenseRatioValuesClauses(etfFundProfileFilter: EtfFundProfileFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (etfFundProfileFilter.bottom20) {
            clauses.addAll(bottom20Clauses)
        }
        if (etfFundProfileFilter.belowMid20) {
            clauses.addAll(belowMid20Clauses)
        }
        if (etfFundProfileFilter.mid20) {
            clauses.addAll(mid20Clauses)
        }
        if (etfFundProfileFilter.aboveMid20) {
            clauses.addAll(aboveMid20Clauses)
        }
        if (etfFundProfileFilter.top20) {
            clauses.addAll(top20Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private val bottom20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("21"))
    )
    private val belowMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("21")),
        Clause(OPERATOR_LESS_THAN, listOf("41"))
    )
    private val mid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("41")),
        Clause(OPERATOR_LESS_THAN, listOf("61"))
    )
    private val aboveMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("61")),
        Clause(OPERATOR_LESS_THAN, listOf("81"))
    )
    private val top20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("81")),
        Clause(OPERATOR_LESS_THAN, listOf("101"))
    )

    private fun getStarClauses(etfFundProfileFilter: EtfFundProfileFilter): List<Clause> {
        return etfFundProfileFilter.morningStarRating.map { getStarClause(it) }
    }

    private fun getLikeClause(values: List<String>) = Clause(OPERATOR_LIKE, values)
    private fun getStarClause(value: Int) = Clause(OPERATOR_LIKE, listOf(value.toString()))

    private val excludeClause = Clause(OPERATOR_NOT_EQUAL_TO, listOf("1"))
    private val includeClause = Clause(OPERATOR_EQUALS, listOf("1"))
}
