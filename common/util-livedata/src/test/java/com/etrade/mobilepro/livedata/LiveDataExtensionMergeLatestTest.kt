package com.etrade.mobilepro.livedata

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.test
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class LiveDataExtensionMergeLatestTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun mergeLatestTest() {
        val first = MutableLiveData<String?>()

        val second = MutableLiveData<String?>()

        val result = first.mergeLatest(second)

        assertTrue(result.value == null)

        first.value = null
        second.value = null

        result
            .test()
            .assertHasValue()
            .assertValue {
                it.first == null && it.second == null
            }

        first.value = "sample"

        result
            .test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it.first == "sample" && it.second == null
            }

        first.value = null
        second.value = "sample"

        result
            .test()
            .awaitValue()
            .assertHasValue()
            .assertValue {
                it.first == null && it.second == "sample"
            }
    }
}
