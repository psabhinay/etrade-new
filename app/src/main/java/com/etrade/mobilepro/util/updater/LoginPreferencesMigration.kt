package com.etrade.mobilepro.util.updater

import com.etrade.eo.userprefrences.api.BuildConfig
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.encryption.api.CipherAuthenticator
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.util.updater.legacy.LegacyEncryptionProvider
import javax.inject.Inject

class LoginPreferencesMigration @Inject constructor(
    private val legacyEncryptionProvider: LegacyEncryptionProvider,
    private val loginPreferencesRepo: LoginPreferencesRepo
) {

    private val isBiometricAuthenticationEnabledKey = "fingerprintEnableSwitchStatus"
    private val isRsaUserKey = "U_PWD_RSA"
    private val passwordKey = "encryptedPassword"
    private val usernameKey = "encryptedUsername"

    suspend fun migrate(keyValueStorage: KeyValueStorage, authenticator: CipherAuthenticator) {
        if (loginPreferencesRepo.version == BuildConfig.VERSION_CODE) {
            return
        }

        loginPreferencesRepo.apply {
            version = BuildConfig.VERSION_CODE
            isRsaUser = keyValueStorage.getBooleanValue(isRsaUserKey)
            isPushNotificationPopupShown = true
        }

        migrateUsername(keyValueStorage, authenticator)
        legacyEncryptionProvider.removeCredentials()
        keyValueStorage.clear()
    }

    private suspend fun migrateUsername(keyValueStorage: KeyValueStorage, authenticator: CipherAuthenticator) {
        val username = keyValueStorage.getStringValue(usernameKey, null)
            ?.let { legacyEncryptionProvider.decrypt(it) }
            ?: return

        loginPreferencesRepo.setUserName(username)
            .onSuccess { migratePassword(keyValueStorage, authenticator) }
    }

    private suspend fun migratePassword(keyValueStorage: KeyValueStorage, authenticator: CipherAuthenticator) {
        if (keyValueStorage.getBooleanValue(isBiometricAuthenticationEnabledKey)) {
            val password = keyValueStorage.getStringValue(passwordKey, null)
                ?.let { legacyEncryptionProvider.decrypt(it) } ?: return

            loginPreferencesRepo.setUserPassword(password, authenticator)
        }
    }

    private fun KeyValueStorage.clear() {
        putBooleanValue(isBiometricAuthenticationEnabledKey, false)
        putBooleanValue(isRsaUserKey, false)
        putStringValue(passwordKey, null)
        putStringValue(usernameKey, null)
    }
}
