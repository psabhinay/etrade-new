package com.etrade.mobilepro.feature.presentation.viewmodel

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

interface AsyncActionViewModel {

    val isExecuting: StateFlow<Boolean>

    val title: String

    fun execute()
}

internal class DefaultAsyncActionViewModel(
    override val title: String,
    val scope: CoroutineScope,
    val action: suspend () -> ETResult<Unit>
) : AsyncActionViewModel {

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    override val isExecuting = MutableStateFlow(false)

    private var job: Job? = null

    override fun execute() {
        job?.cancel()
        job = scope.launch {
            isExecuting.value = true
            action()
                .onFailure { t ->
                    logger.error("$title action failed", t)
                    isExecuting.value = false
                }
                .onSuccess { isExecuting.value = false }
        }
    }
}
