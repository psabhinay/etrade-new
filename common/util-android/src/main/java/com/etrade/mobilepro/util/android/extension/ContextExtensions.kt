package com.etrade.mobilepro.util.android.extension

import android.app.Activity
import android.app.ActivityManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.content.pm.ApplicationInfo
import android.content.res.Configuration
import android.net.Uri
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.autofill.AutofillManager
import androidx.annotation.DimenRes
import androidx.core.content.PermissionChecker
import androidx.core.content.getSystemService

fun Context.pxToDp(px: Int): Int {
    val metrics = resources.displayMetrics
    return (px / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
}

private const val HALF = 0.5f
fun Context.dpToPx(dp: Int): Int {
    val metrics = resources.displayMetrics
    return (dp * metrics.density + HALF).toInt()
}

fun Context.spToPx(dp: Float): Float {
    val metrics = resources.displayMetrics
    return (dp * metrics.scaledDensity + HALF)
}

fun Context.selfPermissionGranted(permission: String): Boolean {
    return PermissionChecker.checkSelfPermission(this, permission) == PermissionChecker.PERMISSION_GRANTED
}

fun Context.openApplicationSettings() {
    val intent = Intent()
        .apply {
            action = ACTION_APPLICATION_DETAILS_SETTINGS
            data = Uri.fromParts("package", packageName, null)
        }

    startActivity(intent)
}

// annotated as deprecated, but "For backwards compatibility,
// it will still return the caller's own services."
@Suppress("DEPRECATION")
fun Context.isServiceRunning(serviceName: String): Boolean {
    val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
        if (serviceName == service.service.className) {
            return true
        }
    }

    return false
}

tailrec fun Context.getActivity(): Activity? = this as? Activity
    ?: (this as? ContextWrapper)?.baseContext?.getActivity()

fun Context.isApplicationDebuggable(): Boolean =
    (this.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE) != 0

fun Context.startDialerActivity(phoneNumber: String, onError: () -> Unit) {
    val dialAction = Intent().toDialAction(phoneNumber).addFlags(FLAG_ACTIVITY_NEW_TASK)
    if (handlerExists(dialAction)) {
        try {
            startActivity(dialAction)
        } catch (e: ActivityNotFoundException) {
            onError()
        }
    } else {
        // Product/design will provide a more specific error message.
        onError()
    }
}

fun Context.handlerExists(intent: Intent): Boolean {
    return intent.resolveActivity(packageManager) != null
}

inline val Context.autofillManager: AutofillManager?
    get() = getSystemService()

fun Context.getFloat(@DimenRes resId: Int): Float {
    val typedValue = TypedValue()
    resources.getValue(resId, typedValue, true)
    return typedValue.float
}

fun Context.isInDarkMode(): Boolean =
    this.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
