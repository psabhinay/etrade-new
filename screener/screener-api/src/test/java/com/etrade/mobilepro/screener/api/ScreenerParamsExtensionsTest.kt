package com.etrade.mobilepro.screener.api

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ScreenerParamsExtensionsTest {
    private val initialClauses = mutableListOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("1")),
        Clause(OPERATOR_LESS_THAN, listOf("300")),
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("300")),
        Clause(OPERATOR_LESS_THAN, listOf("2000"))
    )

    private val expectedClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("1")),
        Clause(OPERATOR_LESS_THAN, listOf("2000"))
    )

    @Test
    fun testFundamentalsFilterJsonGenerating() {
        Assertions.assertEquals(
            expectedClauses,
            initialClauses.mergeFollowingClauses()
        )
    }
}
