package com.etrade.mobilepro.orders.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.list.api.OrderItem
import com.etrade.mobilepro.orders.list.api.OrdersRepo
import com.etrade.mobilepro.orders.list.paging.OrderListPagingSource
import com.etrade.mobilepro.orders.list.paging.OrdersPagingSettings
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class OrdersListViewModel @Inject constructor(
    private val cachedOrdersRepo: OrdersRepo,
    private val helper: OrdersHelper
) : ViewModel() {

    var ordersFlow: Flow<PagingData<OrderItem>>? = null
        private set

    fun fetchOrders(
        accountId: String,
        status: OrderStatus,
        forceRefresh: Boolean
    ): Flow<PagingData<OrderItem>> {
        val params = helper.buildOrdersParams(accountId, status, forceRefresh = forceRefresh)
        val pagingSettings = OrdersPagingSettings.getOrdersPageSetting(status)
        val pagedListConfig = PagingConfig(
            enablePlaceholders = false,
            initialLoadSize = pagingSettings.initialLoadCount,
            prefetchDistance = pagingSettings.prefetchDistance,
            pageSize = pagingSettings.pageSize
        )
        val ordersFlow = Pager(pagedListConfig) {
            OrderListPagingSource(
                cachedOrdersRepo,
                params
            )
        }.flow.cachedIn(viewModelScope)

        this.ordersFlow = ordersFlow
        return ordersFlow
    }

    fun setSelectedOrderItem(item: OrderItem) {
        helper.selectedOrderItem = item
    }

    fun getOrderDetailArgument(accountId: String): OrderDetailsArgument? =
        helper.buildOrderDetailArgument(accountId)
}
