package com.etrade.mobilepro.order.details.api

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.common.result.ETResult
import io.reactivex.Observable
import io.reactivex.Single

interface OrderDetailsRepo {
    fun getOrderDetails(requestParams: OrderDetailsRequestParams): Observable<List<BaseOrderDetailsItem>>
    fun getSavedOrderDetails(requestParams: OrderDetailsRequestParams): Observable<List<BaseOrderDetailsItem>>
    suspend fun getStockPlanOrderDetails(orderNumber: String, symbol: String): ETResult<BaseOrderDetailsItem.StockPlanOrderDetailsItem>
    fun cancelOrder(requestParams: OrderCancelRequest): Single<List<ServerMessage>>
    fun cancelGroupOrder(requestParams: OrderCancelRequest): Single<List<ServerMessage>>
    suspend fun cancelStockPlanOrder(orderNumber: String): ETResult<StockPlanOrderCancelResponse>
    suspend fun deleteOrder(accountId: String, orderId: String): ETResult<Unit>
}
