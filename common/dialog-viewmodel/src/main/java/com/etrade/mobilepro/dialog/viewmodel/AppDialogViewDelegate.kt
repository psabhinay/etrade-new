package com.etrade.mobilepro.dialog.viewmodel

import android.app.Dialog
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.decorateActions
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.viewdelegate.ViewDelegate

class AppDialogViewDelegate(
    viewModel: AppDialogViewModel,
    private val showDialog: (AppDialog) -> Dialog?
) : ViewDelegate {

    private var currentDialog: Dialog? = null
    private val consumableDialog: LiveData<ConsumableLiveEvent<AppDialog?>?> =
        viewModel.dialog

    override fun observe(owner: LifecycleOwner) {
        consumableDialog.observe(
            owner,
            { signal ->
                if (signal != null && signal.isConsumed.not()) {
                    signal.peekContent().let {
                        val newDialog = it?.decorateActions { signal.consume() }

                        onAppDialog(newDialog)
                    }
                }
            }
        )
    }

    private fun onAppDialog(dialog: AppDialog?) {
        currentDialog?.dismiss()
        currentDialog = dialog?.let { showDialog(it) }
    }
}
