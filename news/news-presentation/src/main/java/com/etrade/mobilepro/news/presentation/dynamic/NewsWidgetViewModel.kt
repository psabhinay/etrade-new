package com.etrade.mobilepro.news.presentation.dynamic

import android.content.res.Resources
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.navigation.NavController
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.dynamicui.api.Diffable
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.SelfRemoving
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.api.repo.WidgetNewsRepo
import com.etrade.mobilepro.news.presentation.BR
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.getStockPlanSymbolFromTitle
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

private const val MAX_NEWS_PER_WIDGET = 3

@Suppress("LongParameterList")
class NewsWidgetViewModel(
    private val widgetNewsRepo: WidgetNewsRepo,
    private val statusRepo: ReadStatusRepo,
    val showAccountSelection: Boolean,
    private val hasMoreThanOneAccount: Boolean,
    selectedAccountProvider: () -> Account?,
    private val router: NewsRouter,
    private val newsType: NewsType,
    private val userPreferences: UserPreferences,
    resources: Resources,
    override val layoutId: Int = R.layout.widget_news
) : DynamicViewModel(layoutId), SelfRemoving, Diffable {

    override val variableId: Int = BR.obj

    private val compositeDisposable = CompositeDisposable()
    private val selfRemovingSignal = BehaviorSubject.createDefault(false).toSerialized()
    private val _viewState = MutableLiveData<DynamicUiViewState>()
    private val _items = MutableLiveData<List<NewsItemWidgetPayload>>()
    private var selectedAccount = selectedAccountProvider()
    val newsItems: LiveData<List<NewsItemWidgetPayload>>
        get() = _items
    val viewState: LiveData<DynamicUiViewState>
        get() = _viewState
    val portfolioNewsAvailable: LiveData<Boolean> = newsItems.combineLatest(viewState).map { (items, viewState) ->
        viewState is DynamicUiViewState.Success && !(items.size == 1 && items[0].item is NewsItem.Empty)
    }
    val noPortfolioNews: LiveData<Boolean> = newsItems.combineLatest(viewState).map { (items, viewState) ->
        viewState is DynamicUiViewState.Success && (items.size == 1 && items[0].item is NewsItem.Empty)
    }

    val widgetTitle = when (newsType) {
        NewsType.PORTFOLIO -> resources.getString(R.string.portfolio_news_widget_title)
        else -> resources.getString(R.string.today_news_widget_title)
    }

    private val _openAccountSelection: MutableLiveData<ConsumableLiveEvent<String?>> = MutableLiveData()
    val openAccountSelection: LiveData<ConsumableLiveEvent<String?>> = _openAccountSelection

    val widgetSubTitle = MutableLiveData(
        when (newsType) {
            NewsType.PORTFOLIO -> selectedAccount?.accountShortName
            else -> ""
        }
    )

    fun fetchNews() {
        _viewState.postValue(DynamicUiViewState.Loading)
        val source = getNewsSource()
        compositeDisposable.add(
            source
                .map { list ->
                    list.map { item ->
                        val wasRead = if (item is NewsItem.Text) {
                            statusRepo.wasRead(item.docId)
                        } else {
                            false
                        }
                        NewsItemWidgetPayload(item, wasRead, getItemSelectListener(), getSymbolSelectListener())
                    }
                }
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                    onSuccess = {
                        _viewState.postValue(DynamicUiViewState.Success)
                        _items.postValue(it)
                    },
                    onError = {
                        _viewState.postValue(DynamicUiViewState.Error(it.message))
                        selfRemovingSignal.onNext(true)
                    }
                )
        )
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    fun viewAllNewsClicked(view: View) {
        if (newsType == NewsType.PORTFOLIO) {
            selectedAccount?.accountId?.let {
                router.navigateToAccountNewsPage(view, it)
            }
        } else {
            router.navigateToMarketNews(view)
        }
    }

    override fun selfRemovalRequest(): Observable<Boolean> = selfRemovingSignal.observeOn(AndroidSchedulers.mainThread())

    override fun isSameItem(other: Any): Boolean =
        other.javaClass == javaClass && newsType == (other as NewsWidgetViewModel).newsType

    override fun hasSameContents(other: Any): Boolean = false

    private fun getSymbolSelectListener(): (View, String) -> Unit {
        return { view, s ->
            router.navigateToQuoteDetails(view, s, InstrumentType.EQ)
        }
    }

    private fun getItemSelectListener(): (NavController?, NewsItem.Text, Int) -> Unit {
        return { navController, item, _ ->
            _items.value?.let {
                _items.value = it.markAsRead(item)
            }
            router.navigateToTextNewsDetails(navController, item.docId)
        }
    }

    private fun provideUuidBasedSource(account: Account?): Single<List<NewsItem>>? {
        if (account == null) {
            return null
        }
        val shortName = account.accountShortName
        val symbol = getStockPlanSymbolFromTitle(shortName)
        if (symbol.isBlank()) {
            return null
        }
        return widgetNewsRepo.getWidgetNews(NewsRequest.Symbol(listOf(symbol)), MAX_NEWS_PER_WIDGET)
    }

    private fun provideUserIdBasedSource(account: Account?): Single<List<NewsItem>> {
        return widgetNewsRepo.getWidgetNews(
            NewsRequest.Portfolio(
                resource = account?.encAccountId ?: "",
                includeStockPlan = false
            ),
            MAX_NEWS_PER_WIDGET
        )
    }

    private fun getNewsSource() =
        if (newsType == NewsType.PORTFOLIO) {
            provideUuidBasedSource(selectedAccount) ?: provideUserIdBasedSource(selectedAccount)
        } else {
            widgetNewsRepo.getWidgetNews(NewsRequest.Market, MAX_NEWS_PER_WIDGET)
        }

    fun openAccountSelection() {
        _openAccountSelection.value = ConsumableLiveEvent(selectedAccount?.accountUuid)
    }

    fun onAccountSelection(account: Account) {
        if (account.accountUuid != selectedAccount?.accountUuid) {
            widgetSubTitle.value = account.accountShortName
            userPreferences.pfNewsWidgetAccountUuid = account.accountUuid
            selectedAccount = account
            fetchNews()
        }
    }
}
