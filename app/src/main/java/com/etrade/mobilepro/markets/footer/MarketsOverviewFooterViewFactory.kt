package com.etrade.mobilepro.markets.footer

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.TimeStampReporter
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicWidget
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject
import javax.inject.Provider

class MarketsOverviewFooterViewFactory @Inject constructor(
    private val timeStampReporter: TimeStampReporter,
    private val userViewModel: Provider<UserViewModel>
) : DynamicUiViewFactory {
    private fun isCustomizeViewVisible(): Boolean = with(userViewModel.get()) {
        isAuthenticated && !accountList.isNullOrEmpty()
    }
    override fun createView(dto: Any, meta: List<Any>): GenericLayout = if (dto is MarketsOverviewFooterViewDto) {
        object : GenericLayout, DynamicWidget {
            override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                MarketsOverviewFooterViewModel(timeStampReporter, isCustomizeViewVisible())
            }
            override val viewModelClass: Class<out DynamicViewModel> = MarketsOverviewFooterViewModel::class.java
            override val type: DynamicWidget.Type = DynamicWidget.Type.DEFAULT

            override fun refresh(viewModel: DynamicViewModel) {
                (viewModel as? MarketsOverviewFooterViewModel)?.refresh()
            }
            override val uniqueIdentifier: String = javaClass.name
        }
    } else {
        throw DynamicUiViewFactoryException("MarketsOverviewFooterViewFactory registered to unknown type")
    }
}
