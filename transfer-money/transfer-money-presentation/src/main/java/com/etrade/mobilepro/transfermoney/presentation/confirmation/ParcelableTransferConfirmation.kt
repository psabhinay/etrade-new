package com.etrade.mobilepro.transfermoney.presentation.confirmation

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ParcelableTransferConfirmation(
    val amount: String,
    val fromAccount: String,
    val toAccount: String,
    val scheduleDate: String,
    val confirmationNumber: String,
    val message: String
) : Parcelable
