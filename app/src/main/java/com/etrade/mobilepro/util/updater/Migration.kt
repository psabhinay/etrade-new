package com.etrade.mobilepro.util.updater

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import java.io.File

/**
 * Single migration procedure.
 */
interface Migration {

    /**
     * Migrates data from [databaseFile] to the app.
     *
     * @param databaseFile database file
     */
    suspend fun migrate(databaseFile: File)

    /**
     * Reads rows from a table.
     *
     * @param databaseFile location of a database
     * @param tableName name of a table
     * @param columns list of columns to read
     * @param read reads a cursor
     *
     * @return list of rows
     */
    fun <T> readTableRows(databaseFile: File, tableName: String, columns: List<String>, read: (Cursor) -> List<T>): List<T> {
        return try {
            SQLiteDatabase.openDatabase(databaseFile.path, null, SQLiteDatabase.OPEN_READONLY).use { db ->
                db.query(true, tableName, columns.toTypedArray(), null, null, null, null, null, null).use { cursor ->
                    read(cursor)
                }
            }
        } catch (e: SQLiteException) {
            emptyList()
        }
    }
}
