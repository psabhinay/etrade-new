package com.etrade.mobilepro.newaccount

import android.webkit.JavascriptInterface
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.etrade.mobilepro.eventtrackerapi.AppsFlyerRepo
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

internal const val OLA_EVENT_INTERFACE_NAME = "app"

class CompleteOlaEventJSInterface(
    private val appsFlyerRepo: AppsFlyerRepo,
    private val viewLifecycleOwner: LifecycleOwner,
    private val moshi: Moshi
) {

    private val logger = LoggerFactory.getLogger(CompleteOlaEventJSInterface::class.java)

    @JavascriptInterface
    fun recordEvent(eventName: String?, eventValue: String?) {
        logger.debug("Record event: $eventName, event value: $eventValue")
        eventName?.let {
            val paramsMap: Map<String, String> = if (eventValue != null) {
                val type = Types.newParameterizedType(MutableMap::class.java, String::class.java, String::class.java)
                val adapter: JsonAdapter<Map<String, String>> = moshi.adapter(type)
                adapter.fromJson(eventValue) ?: emptyMap()
            } else {
                emptyMap()
            }

            viewLifecycleOwner.lifecycleScope.launch { appsFlyerRepo.trackEvent(it, paramsMap) }
        }
    }
}
