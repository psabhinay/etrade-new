package com.etrade.mobilepro.debugmenu.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder.ForceCrashViewHolder
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemForcecrashBinding
import com.etrade.mobilepro.debugmenu.presentation.item.DebugMenuItem
import com.etrade.mobilepro.debugmenu.presentation.item.ForceCrashItem
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class ForceCrashAdapterDelegate : AbsListItemAdapterDelegate<ForceCrashItem, DebugMenuItem, ForceCrashViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): ForceCrashViewHolder {
        return ForceCrashViewHolder(parent.viewBinding(DebugmenuItemForcecrashBinding::inflate))
    }

    override fun isForViewType(item: DebugMenuItem, items: MutableList<DebugMenuItem>, position: Int): Boolean =
        item is ForceCrashItem

    override fun onBindViewHolder(item: ForceCrashItem, holder: ForceCrashViewHolder, payloads: MutableList<Any>) {
        // nop
    }
}
