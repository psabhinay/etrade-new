package com.etrade.mobilepro.positions.data.dto

import com.etrade.mobilepro.util.SortOrder
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class IndividualPositionsRequestDto(
    @Json(name = "individual")
    val individual: IndividualPositions
)

/**
 * Individual positions parameters.
 *
 * As per: https://confluence.corp.etradegrp.com/display/MI/API+-+Positions
 */
@JsonClass(generateAdapter = true)
class IndividualPositions(

    /**
     * Account UUID.
     */
    @Json(name = "accountUuid")
    val accountUuid: String,

    /**
     * Remove fetch limit
     */
    @Json(name = "allRecords")
    val allRecords: Boolean = true,

    /**
     * Token to get the next page.
     */
    @Json(name = "continuationToken")
    val continuationToken: String? = null,

    /**
     * Portfolio 2.0 response, flag introduced to support backward compatibility.
     */
    @Json(name = "enableNewExperience")
    val enableNewExperience: Boolean = true,

    /**
     * Order Count flag to included Saved / Open orders in the response.
     */
    @Json(name = "orderCount")
    val orderCount: Boolean = true,

    /**
     * Number of records per page.
     */
    @Json(name = "pageRecCount")
    val pageRecCount: String? = null,

    /**
     * Sort column.
     */
    @Json(name = "sortColumn")
    val sortColumn: String? = PortfolioRecordColumn.SYMBOL.code,

    /**
     * Order of sorting.
     */
    @Json(name = "sortOrder")
    val sortOrder: String? = SortOrder.ASCENDING.code
)
