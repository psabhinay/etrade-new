package com.etrade.mobilepro.chat.data.repo

import com.etrade.mobilepro.chat.api.message.models.ChatForMobileModel
import com.etrade.mobilepro.chat.api.message.models.ChatRetrievedMessageModel
import com.etrade.mobilepro.chat.api.message.repo.ChatMessagesRepo
import com.etrade.mobilepro.chat.data.messages.repo.ChatMessagesRepoImpl
import com.etrade.mobilepro.chat.data.messages.repo.ChatRequestBodyManager
import com.etrade.mobilepro.chat.data.messages.service.ChatMessagesApiService
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.RequestBody
import org.junit.Assert
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
internal class ChatMessagesRepoTest {

    private val fakeApiClient: ChatMessagesApiService = mock()
    private val fakeChatRequestBodyManager: ChatRequestBodyManager = mock()
    private val fakeBody: RequestBody = mock()

    private lateinit var chatSetupRepo: ChatMessagesRepo

    @Before
    fun setup() {
        chatSetupRepo = ChatMessagesRepoImpl(
            apiService = fakeApiClient,
            chatRequestBodyManager = fakeChatRequestBodyManager
        )
    }

    @Test
    fun testGetMessages() {
        runBlockingTest {
            whenever(fakeChatRequestBodyManager.createRetrievedMessageBody(chatTestMessagesData, -1)).thenReturn(fakeBody)
            whenever(fakeApiClient.retrieveMessages(fakeBody)).thenReturn(chatTestRetrievedMessageDto)

            val result: ChatRetrievedMessageModel = chatSetupRepo.getMessages(chatTestMessagesData).getOrThrow()
            Assert.assertEquals(TEST_SUCCESS, result.success)
            Assert.assertEquals(TEST_RETRIEVED_MESSAGE, result.data.items?.first()?.message?.text)
        }
    }

    @Test
    fun testGetMobileChatSuccess() {
        runBlockingTest {
            whenever(fakeChatRequestBodyManager.createMobileChatBody(chatTestRequestParams)).thenReturn(fakeBody)
            whenever(fakeApiClient.getChatForMobile(fakeBody)).thenReturn(chatTestForMobileDto)

            val result = chatSetupRepo.getMobileChat(chatTestRequestParams).getOrThrow()
            verifyResult(result)
        }
    }

    @Test
    fun testPostMessage() {
        runBlockingTest {
            whenever(fakeChatRequestBodyManager.createPostMessageBody(chatTestPostMessageData)).thenReturn(fakeBody)
            whenever(fakeApiClient.postChat(fakeBody)).thenReturn(chatTestForMobileDto)

            val result = chatSetupRepo.postMessage(chatTestPostMessageData).getOrThrow()
            verifyResult(result)
        }
    }

    @Test
    fun testTerminateChat() {
        runBlockingTest {
            whenever(fakeChatRequestBodyManager.createTerminateChatBody(chatTestTerminateData)).thenReturn(fakeBody)
            whenever(fakeApiClient.terminateChat(fakeBody)).thenReturn(chatTestForMobileDto)

            val result = chatSetupRepo.terminateChat(chatTestTerminateData).getOrThrow()
            verifyResult(result)
        }
    }

    private fun verifyResult(result: ChatForMobileModel) {
        val data = result.data
        Assert.assertEquals(TEST_SUCCESS, result.success)
        Assert.assertEquals(TEST_MESSAGES, data?.messages)
        Assert.assertEquals(TEST_SESSION_ID, data?.chatInfoModel?.sessionID)
    }
}
