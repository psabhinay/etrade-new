package com.etrade.mobilepro.common

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

interface OverlayFragmentManager {
    @Suppress("LongParameterList")
    fun manageFragment(
        visible: Boolean,
        fragmentProvider: () -> Fragment,
        showDialogs: Boolean = false,
        tag: String? = null,
        showAsDialogFragment: Boolean = false,
        fragmentManager: FragmentManager? = null
    )
}

interface OverlayFragmentManagerHolder {
    var overlayManager: OverlayFragmentManager?
}
