package com.etrade.mobilepro.tooltip.viewmodel

import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.tooltip.AppTooltip
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.viewdelegate.ViewDelegate

class AppTooltipViewDelegate(
    private val viewModel: AppTooltipViewModel,
    private val showTooltip: (ConsumableLiveEvent<AppTooltip>) -> Unit
) : ViewDelegate {

    override fun observe(owner: LifecycleOwner) {
        viewModel.tooltip.observe(owner, ::onAppDialog)
    }

    private fun onAppDialog(tooltip: ConsumableLiveEvent<AppTooltip>) {
        showTooltip(tooltip)
    }
}
