package com.etrade.mobilepro.trade.api.advanced

import com.etrade.mobilepro.trade.api.ConfirmedOrder

interface ConfirmedAdvancedOrder : AdvancedOrder, ConfirmedOrder
