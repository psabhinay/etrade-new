package com.etrade.mobilepro.appwidget.api.settings

@JvmInline
value class WidgetId(val id: Int)
