package com.etrade.mobilepro.positions.data.dto

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PortfolioTaxLotsViewDto(
    @Json(name = "data")
    override val data: PortfolioTaxLotsDataDto?,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?
) : BaseScreenView()

@JsonClass(generateAdapter = true)
class PortfolioTaxLotsDataDto(
    @Json(name = "position_lots")
    val taxLots: List<PortfolioTaxLotDto>
)

@JsonClass(generateAdapter = true)
class PortfolioTaxLotDto(
    @Json(name = "lot_id")
    val taxLotId: String,
    @Json(name = "position_id")
    val positionId: String
)
