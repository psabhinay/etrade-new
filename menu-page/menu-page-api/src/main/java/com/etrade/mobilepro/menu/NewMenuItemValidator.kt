package com.etrade.mobilepro.menu

interface NewMenuItemValidator {
    fun isNewMenuItem(menuItemKey: String?): Boolean
    fun incrementClickCount(menuItemKey: String?)
}
