package com.etrade.mobilepro.screener.presentation.mutualfund.fees

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.mutualfund.FeesFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.FragmentFeesBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class FeesFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<FeesFilter, FeesViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {

    private val binding by viewBinding(FragmentFeesBinding::bind)
    override val layoutRes: Int = R.layout.fragment_fees
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND
    override val viewModel by viewModels<FeesViewModel> { viewModelFactory }
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<FeesFilter, FeesViewModel.ViewState, Boolean>>
        get() = mapOf(
            binding.noLoadTrans to NoLoad.NO_LOAD_TRANSACTTION,
            binding.transactionFee to NoLoad.TRANSACTION_FEE,
            binding.frontEndLoad to Load.FRONT_END,
            binding.deferredLoad to Load.DEFERRED
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                it.filter.apply {
                    binding.noLoadTrans.isChecked = noLoadTransaction
                    binding.transactionFee.isChecked = transactionFee
                    binding.frontEndLoad.isChecked = frontEndLoad
                    binding.deferredLoad.isChecked = deferredLoad
                }
            }
        )
    }
}
