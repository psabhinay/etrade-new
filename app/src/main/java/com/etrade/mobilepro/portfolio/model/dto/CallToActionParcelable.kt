package com.etrade.mobilepro.portfolio.model.dto

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CallToActionParcelable(
    val label: String,
    val action: ClickActionParcelable,
    val isEnabled: Boolean = true
) : Parcelable
