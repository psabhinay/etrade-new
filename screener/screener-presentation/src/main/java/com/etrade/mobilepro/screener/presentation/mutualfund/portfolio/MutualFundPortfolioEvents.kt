package com.etrade.mobilepro.screener.presentation.mutualfund.portfolio

import com.etrade.mobilepro.screener.api.filter.mutualfund.PortfolioFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class TotalNetAsset : ScreenerFilterEvent<PortfolioFilter, MutualFundPortfolioViewModel.ViewState, Boolean> {
    LESS_THAN_100M {
        override fun update(viewState: MutualFundPortfolioViewModel.ViewState, eventValue: Boolean): MutualFundPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(netAssetsLessThan100 = eventValue))
    },
    ONE_HUNDRED_M_TO_500M {
        override fun update(viewState: MutualFundPortfolioViewModel.ViewState, eventValue: Boolean): MutualFundPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(netAssets100to500 = eventValue))
    },
    FIVE_HUNDRED_M_TO_1B {
        override fun update(viewState: MutualFundPortfolioViewModel.ViewState, eventValue: Boolean): MutualFundPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(netAssets500to1 = eventValue))
    },
    ONE_B_TO_2B {
        override fun update(viewState: MutualFundPortfolioViewModel.ViewState, eventValue: Boolean): MutualFundPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(netAssets1to2 = eventValue))
    },
    GREATER_THAN_2B {
        override fun update(viewState: MutualFundPortfolioViewModel.ViewState, eventValue: Boolean): MutualFundPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(netAssetsGreaterThan2 = eventValue))
    }
}

internal enum class DistributionYield : ScreenerFilterEvent<PortfolioFilter, MutualFundPortfolioViewModel.ViewState, Boolean> {
    LESS_THAN_3 {
        override fun update(viewState: MutualFundPortfolioViewModel.ViewState, eventValue: Boolean): MutualFundPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(distributionYieldLessThan3 = eventValue))
    },
    THREE_TO_5 {
        override fun update(viewState: MutualFundPortfolioViewModel.ViewState, eventValue: Boolean): MutualFundPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(distributionYield3to5 = eventValue))
    },
    GREATER_THAN_5 {
        override fun update(viewState: MutualFundPortfolioViewModel.ViewState, eventValue: Boolean): MutualFundPortfolioViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(distributionYieldGreaterThan5 = eventValue))
    }
}
