package com.etrade.mobilepro.util.android.permissions

import android.content.Context
import androidx.activity.ComponentActivity
import androidx.activity.result.contract.ActivityResultContracts.RequestMultiplePermissions
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import androidx.fragment.app.Fragment

class PermissionsChecker internal constructor(
    private val fragment: Fragment? = null,
    private val activity: ComponentActivity? = null,
    private val singleListener: SinglePermissionCheckerListener? = null,
    private var multipleListener: MultiplePermissionsCheckerListener? = null,
    private val permissions: List<String>
) {

    private val permissionRequest = fragment?.registerForActivityResult(
        RequestMultiplePermissions(),
        this::onRequestPermissionsResult
    ) ?: activity?.registerForActivityResult(
        RequestMultiplePermissions(),
        this::onRequestPermissionsResult
    )

    private fun onRequestPermissionsResult(permissionResults: Map<String, Boolean>) {
        when {
            permissionResults.size == 1 ->
                singleListener?.handleResult(
                    permissions[0],
                    permissionResults[permissions[0]] ?: false
                )
            permissionResults.size > 1 ->
                multipleListener?.handleResult(permissionResults)
            else -> {
                /**
                 * It is possible that the permissions request interaction
                 * with the user is interrupted. In this case you will receive empty permissions
                 * and results arrays which should be treated as a cancellation.
                 */
                handleEmptyPermissionsResult()
            }
        }
    }

    fun check() {
        val context: Context? = fragment?.context ?: activity
        val permissionsGranted: Boolean = context
            ?.let { ctx ->
                permissions.all {
                    PermissionChecker.checkSelfPermission(ctx, it) ==
                        PermissionChecker.PERMISSION_GRANTED
                }
            }
            ?: false

        /**
         * Calling this API for permissions already granted to your app would show UI
         * to the user to decided whether the app can still hold these permissions.
         **/
        if (permissionsGranted) {
            onRequestPermissionsResult(permissions.map { it to true }.toMap())
        } else {
            permissionRequest?.launch(permissions.toTypedArray())
        }
    }

    private fun shouldShowRationale(permission: String): Boolean {
        return run { activity ?: fragment?.activity }
            ?.let { ActivityCompat.shouldShowRequestPermissionRationale(it, permission) }
            ?: false
    }

    private fun SinglePermissionCheckerListener.handleResult(permission: String, granted: Boolean) {
        if (granted) {
            onPermissionGranted(PermissionGrantedResponse(permission))
        } else {
            if (shouldShowRationale(permission)) {
                onPermissionRationaleShouldBeShown(permission)
            } else {
                onPermissionDenied(PermissionDeniedResponse(permission, true))
            }
        }
    }

    private fun MultiplePermissionsCheckerListener.handleResult(
        permissionResults: Map<String, Boolean>
    ) {
        val permissionsGranted: MutableList<PermissionGrantedResponse> = mutableListOf()
        val permissionsDenied: MutableList<PermissionDeniedResponse> = mutableListOf()

        permissionResults.forEach { (permission, granted) ->
            if (granted) {
                permissionsGranted.add(PermissionGrantedResponse(permission))
            } else {
                permissionsDenied.add(permission, !shouldShowRationale(permission))
            }

            onMultiplePermissionsResult(
                MultiplePermissionsResponse(permissionsGranted, permissionsDenied)
            )
        }
    }

    private fun MutableList<PermissionDeniedResponse>.add(permission: String, permanent: Boolean) =
        add(PermissionDeniedResponse(permission, permanent))

    private fun handleEmptyPermissionsResult() {
        singleListener?.handleResult(permissions[0], false)
        multipleListener?.handleResult(permissions.map { it to false }.toMap())
    }

    companion object {

        fun with(fragment: Fragment): FragmentPermissionsChecker {
            return FragmentPermissionsChecker(fragment)
        }

        fun with(activity: ComponentActivity): ActivityPermissionsChecker {
            return ActivityPermissionsChecker(activity)
        }
    }
}
