package com.etrade.mobilepro.checkdeposit.presentation.activity

import com.etrade.mobilepro.checkdeposit.activity.service.DepositActivityItemDto
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class CheckDepositActivityAdapter(
    depositActivityItemListener: ((DepositActivityItemDto, String) -> Unit)?
) : ListDelegationAdapter<MutableList<DepositActivityResultItem>>() {
    init {
        delegatesManager.addDelegate(CheckDepositActivityAdapterDelegate(depositActivityItemListener))
        items = mutableListOf()
    }

    fun addItems(depositActivityItems: List<DepositActivityResultItem>) {
        dispatchUpdates(items, depositActivityItems, DepositActivityItemDiffCallback(items, depositActivityItems))
        items.clear()
        items.addAll(depositActivityItems)
    }
}
