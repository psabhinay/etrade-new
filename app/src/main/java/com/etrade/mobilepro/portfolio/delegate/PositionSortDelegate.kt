package com.etrade.mobilepro.portfolio.delegate

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.portfolio.TableViewSortListener
import com.etrade.mobilepro.portfolio.model.SortInfo
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo

interface PositionSortDelegate : TableViewSortListener {
    /**
     * Sorting information that is being updated whenever refresh is required to update columns.
     */
    val sortInfo: LiveData<SortInfo>

    val columns: LiveData<List<PortfolioColumn>>

    val sortViewState: LiveData<SortStateInfo>

    val positionTableMode: PositionTableMode

    fun sortColumn(newState: SortInfo)

    fun updateColumns()

    fun onCleared()
}
