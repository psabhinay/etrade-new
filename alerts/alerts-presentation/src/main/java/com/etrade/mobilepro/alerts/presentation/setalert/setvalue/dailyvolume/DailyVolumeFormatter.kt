package com.etrade.mobilepro.alerts.presentation.setalert.setvalue.dailyvolume

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import java.math.BigDecimal

internal class DailyVolumeFormatter : InputFieldManager.Formatter {
    override fun formatRawContent(rawContent: String): String {
        val amount = rawContent.toDoubleOrNull() ?: return rawContent
        return MarketDataFormatter.formatVolumeTwoDecimals(BigDecimal.valueOf(amount))
    }
}
