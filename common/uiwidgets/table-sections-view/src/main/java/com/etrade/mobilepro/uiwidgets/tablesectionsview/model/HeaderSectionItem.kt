package com.etrade.mobilepro.uiwidgets.tablesectionsview.model

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.R
import com.etrade.mobilepro.uiwidgets.tablesectionsview.databinding.ItemSectionRowTitleBinding
import com.etrade.mobilepro.util.android.extension.viewBinding

class HeaderSectionItem(val title: String) : TableSectionItemLayout {
    override fun getRowView(linearParams: LinearLayout.LayoutParams, parent: ViewGroup): View =
        parent.viewBinding(ItemSectionRowTitleBinding::inflate).root.apply {
            layoutParams = linearParams
            text = title
            contentDescription = resources.getString(R.string.label_header_description, title)
        }
}
