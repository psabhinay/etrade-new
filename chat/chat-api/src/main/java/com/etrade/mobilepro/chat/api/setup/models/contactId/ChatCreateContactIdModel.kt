package com.etrade.mobilepro.chat.api.setup.models.contactId

import com.etrade.mobilepro.chat.api.ChatMetadataModel

data class ChatCreateContactIdModel(val data: ChatGetContactIdMessagesModel? = null, val metadata: ChatMetadataModel? = null)
