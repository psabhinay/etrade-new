package com.etrade.mobilepro.debugmenu.data.repo

import com.etrade.mobilepro.appconfig.api.AppEnvironment
import com.etrade.mobilepro.appconfig.api.repo.datasource.BaseUrlsDataSource
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.debugmenu.api.EnvChange
import com.etrade.mobilepro.debugmenu.api.repo.DebugMenuRepo
import com.etrade.mobilepro.debugmenu.data.mapper.mapToBaseUrls
import com.etrade.mobilepro.debugmenu.data.rest.DebugMenuApiClient
import com.etrade.mobilepro.debugmenu.data.rest.EnvDto
import okhttp3.Credentials
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject
import javax.xml.stream.XMLStreamException

private const val UNAUTH_CODE = 401

class DefaultDebugMenuRepo @Inject constructor(
    private val retrofitBuilder: Retrofit.Builder,
    private val baseUrlDataSource: BaseUrlsDataSource,
) : DebugMenuRepo {
    private val apiClientDR by lazy { createApiClient(AppEnvironment.DR) }
    private val apiClientDR_EXTERNAL by lazy { createApiClient(AppEnvironment.DR_EXTERNAL) }
    private val apiClientM5 by lazy { createApiClient(AppEnvironment.M5) }
    private val apiClientMOCK by lazy { createApiClient(AppEnvironment.MOCK) }
    private val apiClientPLT by lazy { createApiClient(AppEnvironment.PLT) }
    private val apiClientPROD by lazy { createApiClient(AppEnvironment.PROD) }
    private val apiClientProdM3 by lazy { createApiClient(AppEnvironment.PROD_M3_ONLY) }
    private val apiClientProdM5 by lazy { createApiClient(AppEnvironment.PROD_M5_ONLY) }
    private val apiClientSIT by lazy { createApiClient(AppEnvironment.SIT) }
    private val apiClientUAT by lazy { createApiClient(AppEnvironment.UAT) }
    override suspend fun requestEnvChange(appEnvironment: AppEnvironment, login: String, password: String): ETResult<EnvChange> = runCatchingET {
        val credentials = Credentials.basic(login, password)
        val response = getEnvChangeResponse(appEnvironment, credentials)
        when {
            response.isSuccessful -> EnvChange.Success(urls = response.body().mapToBaseUrls())
            response.code() == UNAUTH_CODE -> EnvChange.AuthFailed
            else -> EnvChange.ServerError(code = response.code())
        }
    }.run {
        if (isFailure && this.exceptionOrNull()?.cause is XMLStreamException) {
            ETResult.success(EnvChange.ServerError())
        } else {
            this
        }
    }

    private suspend fun getEnvChangeResponse(
        appEnvironment: AppEnvironment,
        credentials: String
    ): Response<EnvDto> = when (appEnvironment) {
        AppEnvironment.DR -> apiClientDR
        AppEnvironment.DR_EXTERNAL -> apiClientDR_EXTERNAL
        AppEnvironment.M5 -> apiClientM5
        AppEnvironment.MOCK -> apiClientMOCK
        AppEnvironment.PLT -> apiClientPLT
        AppEnvironment.PROD -> apiClientPROD
        AppEnvironment.PROD_M3_ONLY -> apiClientProdM3
        AppEnvironment.PROD_M5_ONLY -> apiClientProdM5
        AppEnvironment.SIT -> apiClientSIT
        AppEnvironment.UAT -> apiClientUAT
    }.requestAppEnvironmentEndpoints(credentials)

    private fun createApiClient(appEnvironment: AppEnvironment): DebugMenuApiClient = if (appEnvironment == AppEnvironment.PROD) {
        object : DebugMenuApiClient {
            override suspend fun requestAppEnvironmentEndpoints(credentials: String): Response<EnvDto> = Response.success(EnvDto())
        }
    } else {
        retrofitBuilder
            .baseUrl(baseUrlDataSource.getBaseUrls(appEnvironment).environmentChange)
            .build()
            .create(DebugMenuApiClient::class.java)
    }
}
