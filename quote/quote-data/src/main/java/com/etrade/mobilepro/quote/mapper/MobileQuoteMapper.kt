package com.etrade.mobilepro.quote.mapper

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.dto.FundQuoteResponseDto
import com.etrade.mobilepro.quote.dto.QuoteDto
import com.etrade.mobilepro.quote.dto.QuoteResponse
import com.etrade.mobilepro.quoteapi.DateStruct
import com.etrade.mobilepro.quoteapi.FeeDetail
import com.etrade.mobilepro.quoteapi.FundClassInfo
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MutualFundDetails
import com.etrade.mobilepro.quoteapi.MutualFundPerformance
import com.etrade.mobilepro.quoteapi.QuotePrice
import com.etrade.mobilepro.quoteapi.QuoteSpecialEvents
import javax.inject.Inject

class MobileQuoteMapper @Inject constructor() {
    // For now it is 1 to 1 mapper, maybe later some types transformations will be added here
    fun map(jsonDto: QuoteResponse): MobileQuote {
        return when (jsonDto) {
            is QuoteDto -> {
                mapQuoteDto(jsonDto)
            }
            is FundQuoteResponseDto -> {
                mapFundDto(jsonDto)
            }
            else -> MobileQuote()
        }
    }

    private fun mapQuoteDto(jsonDto: QuoteDto): MobileQuote {
        val specialEvents = QuoteSpecialEvents(
            nextEarningData = jsonDto.specialEvents?.nextEarningData ?: false,
            dividendPayDate = jsonDto.specialEvents?.dividendPayDate ?: false,
            exDividendDate = jsonDto.specialEvents?.exDividendDate ?: false,
            splitRatio = jsonDto.specialEvents?.splitRatio ?: false,
            tradeHalt = jsonDto.specialEvents?.tradeHalt ?: false,
            daysToExpiration = jsonDto.specialEvents?.daysToExpiration ?: false,
            qtyFactorSplitFrom = jsonDto.specialEvents?.qtyFactorSplitFrom ?: 0,
            qtyFactorSplitTo = jsonDto.specialEvents?.qtyFactorSplitTo ?: 0

        )
        val week52HighDate = DateStruct(
            day = jsonDto.week52HighDate?.day ?: "",
            month = jsonDto.week52HighDate?.month ?: "",
            year = jsonDto.week52HighDate?.year ?: ""
        )
        val week52LowDate = DateStruct(
            day = jsonDto.week52LowDate?.day ?: "",
            month = jsonDto.week52LowDate?.month ?: "",
            year = jsonDto.week52LowDate?.year ?: ""
        )
        val dividendPayDate = DateStruct(
            day = jsonDto.dividentPayDate?.day ?: "",
            month = jsonDto.dividentPayDate?.month ?: "",
            year = jsonDto.dividentPayDate?.year ?: ""
        )
        val exDividendDate = DateStruct(
            day = jsonDto.exDividendDate?.day ?: "",
            month = jsonDto.exDividendDate?.month ?: "",
            year = jsonDto.exDividendDate?.year ?: ""
        )
        val nextEarningDate = DateStruct(
            day = jsonDto.nextEarningDate?.day ?: "",
            month = jsonDto.nextEarningDate?.month ?: "",
            year = jsonDto.nextEarningDate?.year ?: ""
        )
        val extendedQuote = jsonDto.extendedQuoteHrDetail?.let { extendedHourQuote ->
            QuotePrice(
                lastPrice = extendedHourQuote.lastPrice ?: "",
                change = extendedHourQuote.change ?: "",
                percentChange = extendedHourQuote.percentChange ?: "",
                bid = extendedHourQuote.bid ?: "",
                bidSize = extendedHourQuote.bidSize ?: "",
                ask = extendedHourQuote.ask ?: "",
                askSize = extendedHourQuote.askSize ?: "",
                volume = extendedHourQuote.volume ?: "",
                timeOfLastTrade = extendedHourQuote.timeOfLastTrade ?: 0
            )
        }
        val quotePrice =
            QuotePrice(
                lastPrice = jsonDto.lastPrice ?: "",
                lastTradeSize = jsonDto.tradeSize ?: "",
                change = jsonDto.change ?: "",
                percentChange = jsonDto.percentChange ?: "",
                bid = jsonDto.bid ?: "",
                bidSize = jsonDto.bidSize ?: "",
                ask = jsonDto.ask ?: "",
                askSize = jsonDto.askSize ?: "",
                volume = jsonDto.volume ?: "",
                timeOfLastTrade = jsonDto.timestampOfLastTrade ?: 0
            )

        return MobileQuote(
            symbol = jsonDto.symbol ?: "",
            description = jsonDto.description ?: "",
            exchangeCode = jsonDto.exchangeCode,
            exchangeName = jsonDto.exchangeName ?: "",
            typeCode = InstrumentType.from(jsonDto.typeCode),
            quotePrice = quotePrice,
            openPrice = jsonDto.openPrice ?: "",
            previousClose = jsonDto.previousClose ?: "",
            dayHigh = jsonDto.dayHigh ?: "",
            dayLow = jsonDto.dayLow ?: "",
            delayedOrRtq = jsonDto.delayedOrRtq ?: 0,
            timeZone = jsonDto.timeZone ?: "",
            lastTradeTime = jsonDto.lastTradeTime ?: "",
            specialEvents = specialEvents,
            week52Low = jsonDto.week52Low ?: "",
            week52High = jsonDto.week52High ?: "",
            week52HighDate = week52HighDate,
            week52LowDate = week52LowDate,
            marketCap = jsonDto.marketCap ?: "",
            sharesOutstanding = jsonDto.sharesOutstanding ?: "",
            averageVolume = jsonDto.averageVolume ?: "",
            pe = jsonDto.pe ?: "",
            eps = jsonDto.eps ?: "",
            dividendYield = jsonDto.dividendYield ?: "",
            declaredDividend = jsonDto.declaredDividend ?: "",
            dividentPayDate = dividendPayDate,
            beta = jsonDto.beta ?: "",
            thirtyDaySecYield = jsonDto.secYield,
            exDividendDate = exDividendDate,
            subSecType = jsonDto.subSecType ?: "",
            hasMiniOptions = jsonDto.hasMiniOptions ?: false,
            msRating = jsonDto.msRating ?: "",
            fundCategory = jsonDto.fundCategory ?: "",
            showDrip = jsonDto.showDrip ?: 0,
            commissionFree = jsonDto.commissionFree ?: false,
            isOptionable = jsonDto.isOptionable ?: false,
            cusip = jsonDto.cusip ?: "",
            overnightTrading = jsonDto.overnightTrading ?: 0,
            oneYYield = jsonDto.oneYYield ?: "",
            bidExchange = jsonDto.bidExchange ?: "",
            askExchange = jsonDto.askExchange ?: "",
            tradeExchange = jsonDto.tradeExchange ?: "",
            tradeSize = jsonDto.tradeSize ?: "",
            allStar = jsonDto.allStar ?: false,
            exchangeTradeNotes = jsonDto.exchangeTradeNotes ?: false,
            leveragedFund = jsonDto.leveragedFund ?: false,
            inverseFund = jsonDto.inverseFund ?: false,
            commodity = jsonDto.commodity ?: false,
            overnightExchange = jsonDto.overnightExchange ?: false,
            dst = jsonDto.dst ?: false,
            nextEarningDate = nextEarningDate,
            extendedQuoteHrDetail = extendedQuote,
            availability = jsonDto.availability ?: "",
            ahFlagHasNumericData = checkAhFlagHasNumericData(jsonDto.ahFlag),
            underlyingSymbol = jsonDto.underlyingSymbol ?: "",
            rho = jsonDto.rho ?: "",
            vega = jsonDto.vega ?: "",
            theta = jsonDto.theta ?: "",
            gamma = jsonDto.gamma ?: "",
            deliverableContract = jsonDto.deliverableContract ?: "",
            daysToExpire = jsonDto.daysToExpire ?: "",
            intrinsicValue = jsonDto.intrinsicValue ?: "",
            timePremium = jsonDto.timePremium ?: "",
            openInterest = jsonDto.openInterest ?: "",
            delta = jsonDto.delta ?: "",
            impliedVolatility = jsonDto.iv ?: "",
            underLyingSymbolDescription = jsonDto.underLyingSymbolDescription ?: "",
            underLyingExchangeCode = jsonDto.underLyingExchangeCode ?: "",
            underLyingExchangeName = jsonDto.underLyingExchangeName ?: "",
            underLyingLastPrice = jsonDto.underLyingLastPrice ?: "",
            underLyingChange = jsonDto.underLyingChange ?: "",
            underLyingPercentChange = jsonDto.underLyingPercentChange ?: "",
            underLyingVolume = jsonDto.underLyingVolume ?: "",
            underLyingLastTradeTime = null,
            affiliatedStatusCode = jsonDto.affiliatedStatusCd ?: ""
        )
    }

    private fun mapFundDto(jsonDto: FundQuoteResponseDto): MobileQuote {
        val fundPerformance = jsonDto.fundCategoryPerformance?.let {
            MutualFundPerformance(
                fundCategoryName = it.fundCategoryName,
                nonAdjMonthlyTrailingReturnYtd = it.nonAdjMonthlyTrailingReturnYtd,
                nonAdjMonthlyTrailingReturn1y = it.nonAdjMonthlyTrailingReturn1y,
                nonAdjMonthlyTrailingReturn5y = it.nonAdjMonthlyTrailingReturn5y,
                nonAdjMonthlyTrailingReturn10y = it.nonAdjMonthlyTrailingReturn10y,
                adjMonthlyTrailingReturnYtd = it.adjMonthlyTrailingReturnYtd,
                adjMonthlyTrailingReturn1y = it.adjMonthlyTrailingReturn1y,
                adjMonthlyTrailingReturn5y = it.adjMonthlyTrailingReturn5y,
                adjMonthlyTrailingReturn10y = it.adjMonthlyTrailingReturn10y,
                nonAdjQuarterlyTrailingReturnYtd = it.nonAdjQuarterlyTrailingReturnYtd,
                nonAdjQuarterlyTrailingReturn1y = it.nonAdjQuarterlyTrailingReturn1y,
                nonAdjQuarterlyTrailingReturn5y = it.nonAdjQuarterlyTrailingReturn5y,
                nonAdjQuarterlyTrailingReturn10y = it.nonAdjQuarterlyTrailingReturn10y,
                adjQuarterlyTrailingReturnYtd = it.adjQuarterlyTrailingReturnYtd,
                adjQuarterlyTrailingReturn1y = it.adjQuarterlyTrailingReturn1y,
                adjQuarterlyTrailingReturn5y = it.adjQuarterlyTrailingReturn5y,
                adjQuarterlyTrailingReturn10y = it.adjQuarterlyTrailingReturn10y,
                monthlyTrailingReturnYtd = jsonDto.monthlyTrailingReturnYtd,
                monthlyTrailingReturn1y = jsonDto.monthlyTrailingReturn1y,
                monthlyTrailingReturn5y = jsonDto.monthlyTrailingReturn5y,
                monthlyTrailingReturn10y = jsonDto.monthlyTrailingReturn10y,
                quarterlyTrailingReturnYtd = jsonDto.quarterlyTrailingReturnYtd,
                quarterlyTrailingReturn1y = jsonDto.quarterlyTrailingReturn1y,
                quarterlyTrailingReturn5y = jsonDto.quarterlyTrailingReturn5y,
                quarterlyTrailingReturn10y = jsonDto.quarterlyTrailingReturn10y,
                monthlyLoadAdjTrailingReturnYtd = jsonDto.monthlyLoadAdjTrailingReturnYtd,
                monthlyLoadAdjTrailingReturn1y = jsonDto.monthlyLoadAdjTrailingReturn1y,
                monthlyLoadAdjTrailingReturn5y = jsonDto.monthlyLoadAdjTrailingReturn5y,
                monthlyLoadAdjTrailingReturn10y = jsonDto.monthlyLoadAdjTrailingReturn10y,
                quarterlyLoadAdjTrailingReturnYtd = jsonDto.quarterlyLoadAdjTrailingReturnYtd,
                quarterlyLoadAdjTrailingReturn1y = jsonDto.quarterlyLoadAdjTrailingReturn1y,
                quarterlyLoadAdjTrailingReturn5y = jsonDto.quarterlyLoadAdjTrailingReturn5y,
                quarterlyLoadAdjTrailingReturn10y = jsonDto.quarterlyLoadAdjTrailingReturn10y,
                inceptionDate = jsonDto.inceptionDate,
                performanceAsOfDate = jsonDto.performanceAsOfDate,
                quarteryPerformanceAsOfDate = jsonDto.qtrlyPerformanceAsOfDate,
                monthlyLoadAdjTrailingReturnSinceInception = jsonDto.monthlyLoadAdjTrailingReturnSinceInception,
                monthlyTrailingReturnSinceInception = jsonDto.monthlyTrailingReturnSinceInception,
                quarterlyLoadAdjTrailingReturnSinceInception = jsonDto.quarterlyLoadAdjTrailingReturnSinceInception,
                quarterlyTrailingReturnSinceInception = jsonDto.quarterlyTrailingReturnSinceInception
            )
        }

        val quotePrice =
            QuotePrice(
                lastPrice = jsonDto.netAssetValue ?: "",
                change = jsonDto.change ?: "",
                percentChange = jsonDto.percentChange ?: "",
                timeOfLastTrade = jsonDto.timeOfLastTradeInMillis?.toLongOrNull()
            )

        val mutualFundDetails =
            MutualFundDetails(
                netAssetValue = jsonDto.netAssetValue,
                publicOfferPrice = jsonDto.publicOfferPrice,
                previousClose = jsonDto.previousClose,
                familyName = jsonDto.familyName,
                timeOfLastTradeInMillis = jsonDto.timeOfLastTradeInMillis?.toLongOrNull(),
                fundClassInfoList = jsonDto.fundClassInfo?.map {
                    FundClassInfo(
                        it.symbol,
                        it.exchangeCode,
                        // service response always returns "" for fund class typeCode, we have to get it from the jsonDto
                        InstrumentType.from(jsonDto.typeCode),
                        it.fundClass
                    )
                },
                hasNoTransactionsFee = when (jsonDto.pricingParameterCode) {
                    "N" -> true
                    "F" -> {
                        jsonDto.frontLoadcd == "2"
                    }
                    else -> false
                },
                isAllStarFund = jsonDto.allStar ?: false,
                morningStarAsOfDate = jsonDto.morningStarAsOfDate,
                morningStarCategory = jsonDto.morningStarCategory,
                morningStarRating = jsonDto.morningStarRating,
                numFundsInOverallRating = jsonDto.numFundsInOverallRating,
                threeYearMorningStarRating = jsonDto.threeYMsRating,
                numFundsInMsRatingFor3y = jsonDto.numFundsInMsRatingFor3y,
                fiveYearMorningStarRating = jsonDto.fiveYMsRating,
                numFundsInMsRatingFor5y = jsonDto.numFundsInMsRatingFor5y,
                tenYearMorningStarRating = jsonDto.tenYMsRating,
                numFundsInMsRatingFor10y = jsonDto.numFundsInMsRatingFor10y,
                availability = jsonDto.availability,
                qtrlyPerformanceAsOfDate = jsonDto.qtrlyPerformanceAsOfDate,
                quarterlyTrailingReturnYtd = jsonDto.quarterlyTrailingReturnYtd,
                quarterlyTrailingReturn1y = jsonDto.quarterlyTrailingReturn1y,
                quarterlyTrailingReturn5y = jsonDto.quarterlyTrailingReturn5y,
                quarterlyTrailingReturn10y = jsonDto.quarterlyTrailingReturn10y,
                netExpenseRatio = jsonDto.netExpenseRatio,
                grossExpenseRatio = jsonDto.grossExpenseRatio,
                totalNetAssets = jsonDto.totalNetAssets,
                inceptionDate = jsonDto.inceptionDate,
                thirtyDaySecYield = jsonDto.secYield,
                oneYearSecYield = jsonDto.oneYYield,
                initialInvestment = jsonDto.initialInvestment,
                subsequentInvestment = jsonDto.subsequentInvestment,
                salesCharge = jsonDto.salesCharge,
                fundRedemptionFee = jsonDto.earlyRedemptionFee,
                etradeTransactionFee = jsonDto.transactionFee,
                mutualFundPerformance = fundPerformance,
                fundDescription = jsonDto.fundDescription,
                additionalFeeDetails = getAdditionalFeeDetails(jsonDto),
                etradeEarlyRedemptionFee = jsonDto.etradeEarlyRedemptionFee,
                initialIra = jsonDto.initialIra,
                subsequentIra = jsonDto.subsequentIra,
                managementFee = jsonDto.managementFee,
                administrationFee = jsonDto.administrationFee,
                twelveBOneFee = jsonDto.actual12B1Fee,
                sevenDayCurrentYield = jsonDto.sevenDayCurrentYield,
                annualTotalReturn = jsonDto.annualTotalReturn,
                weightedAvgMaturity = jsonDto.weightedAvgMaturity
            )

        return MobileQuote(
            symbol = jsonDto.symbol ?: "",
            description = jsonDto.symbolDescription ?: "",
            exchangeCode = jsonDto.exchangeCode ?: "",
            exchangeName = jsonDto.exchangeName ?: "",
            typeCode = InstrumentType.from(jsonDto.typeCode),
            lastTradeTime = jsonDto.timeOfLastTradeInMillis ?: "",
            quotePrice = quotePrice,
            delayedOrRtq = jsonDto.delayedOrRtq ?: 0,
            timeZone = jsonDto.timeZone ?: "",
            cusip = jsonDto.cusip ?: "",
            oneYYield = jsonDto.oneYYield ?: "",
            allStar = jsonDto.allStar ?: false,
            availability = jsonDto.availability ?: "",
            fundDetails = mutualFundDetails,
            mutualFundPerformance = fundPerformance,
            affiliatedStatusCode = jsonDto.affiliatedStatusCd
        )
    }

    private fun getAdditionalFeeDetails(jsonDto: FundQuoteResponseDto): Pair<Boolean?, List<FeeDetail>> {
        jsonDto.frontEndSalesCharges?.let { feesList ->
            return Pair(
                true,
                feesList.map {
                    FeeDetail(
                        it.percent ?: "",
                        it.range ?: ""
                    )
                }
            )
        }
        jsonDto.deferredSalesCharges?.let { feesList ->
            return Pair(
                true,
                feesList.map {
                    FeeDetail(
                        it.percent ?: "",
                        it.range ?: ""
                    )
                }
            )
        }
        return Pair(null, emptyList())
    }

    private fun checkAhFlagHasNumericData(flag: Any?): Boolean = flag is Double
}
