package com.etrade.mobilepro.transfermoney.api.repo

interface TransferStockAccountsDataSource {
    suspend fun getStockAccountIds(): Set<String>
}
