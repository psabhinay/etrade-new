package com.etrade.mobilepro.baseactivity

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity

interface ActivitySessionDelegate {
    fun observeTimeoutEvent(activity: AppCompatActivity)
    fun onUserInteraction(activity: Activity)
}
