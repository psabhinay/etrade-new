package com.etrade.mobilepro.login.session

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.etrade.mobilepro.BuildConfig
import com.etrade.mobilepro.appconfig.api.Timeouts
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.sar.SimultaneousAccessRestriction
import com.etrade.mobilepro.session.SessionTerminationStateProvider
import com.etrade.mobilepro.session.api.SessionEvents
import com.etrade.mobilepro.session.ping.PingSessionDelegate
import com.etrade.mobilepro.user.session.manager.SessionTimeoutManager
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SessionManagerDelegate @Inject constructor(
    private val appConfigRepo: AppConfigRepo,
    private val sarDelegate: SimultaneousAccessRestriction,
    private val pingSessionDelegate: PingSessionDelegate,
    private val sessionTimeoutManager: SessionTimeoutManager,
    serviceUnAuthorisedState: SessionTerminationStateProvider
) {
    fun start(deviceTimeoutInterval: Long?) {
        sarDelegate.start()
        pingSessionDelegate.start()
        sessionTimeoutManager.initSessionTimeout(getTimeIntervalInMills(deviceTimeoutInterval))
    }

    fun stop() {
        pingSessionDelegate.stop()
        sarDelegate.stop()
        sessionTimeoutManager.cancelSessionTimeout()
    }

    val sessionTerminateEvent: LiveData<SessionEvents>
        get() = _sessionTerminateEvent

    private val _sessionTerminateEvent: MediatorLiveData<SessionEvents> = MediatorLiveData()

    private val logger = LoggerFactory.getLogger(SessionManagerDelegate::class.java)

    init {
        _sessionTerminateEvent.addSource(sarDelegate.sessionEvent) {
            logger.debug("received sar ${it.code}")
            _sessionTerminateEvent.setValue(it)
        }
        _sessionTerminateEvent.addSource(sessionTimeoutManager.sessionEvent) {
            logger.debug("received timeout sessionTerminateEvent ${it.code}")
            _sessionTerminateEvent.setValue(it)
        }
        _sessionTerminateEvent.addSource(pingSessionDelegate.sessionEvent) {
            logger.debug("current session  expired :  ${it.code}")
            _sessionTerminateEvent.setValue(it)
        }
        _sessionTerminateEvent.addSource(serviceUnAuthorisedState.sessionEvent) {
            val event = it.getNonConsumedContent() ?: SessionEvents.MSG_NO_SESSION_TIMEOUT
            logger.debug("current session  expired :  ${event.code}")
            _sessionTerminateEvent.setValue(event)
        }
    }

    private fun getTimeIntervalInMills(deviceTimeoutInterval: Long?): Long = appConfigRepo.getAppConfig().run {
        if (!timeouts.limitSessionTo30Sec || !BuildConfig.DEBUG) {
            TimeUnit.SECONDS.toMillis(deviceTimeoutInterval?.takeIf { it >= Timeouts.DEFAULT_SESSION_TIMEOUT_SEC } ?: Timeouts.DEFAULT_SESSION_TIMEOUT_SEC)
        } else {
            TimeUnit.SECONDS.toMillis(Timeouts.DEBUG_SESSION_TIMEOUT_SEC)
        }
    }
}
