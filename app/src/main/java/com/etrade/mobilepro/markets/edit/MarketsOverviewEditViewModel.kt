package com.etrade.mobilepro.markets.edit

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.home.overview.edit.OverviewWidget
import com.etrade.mobilepro.home.overview.edit.OverviewWidgetRepo
import com.etrade.mobilepro.tracking.SCREEN_TITLE_OVERVIEW_CUSTOMIZE
import com.etrade.mobilepro.tracking.ScreenViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class MarketsOverviewEditViewModel @Inject constructor(
    @MarketsOverview private val widgetsRepo: OverviewWidgetRepo
) : EditListViewModel<OverviewWidget>(), ScreenViewModel {

    override val screenName: String = SCREEN_TITLE_OVERVIEW_CUSTOMIZE

    override fun createFrom(item: OverviewWidget, value: Boolean): OverviewWidget {
        return OverviewWidget(item.widgetName, item.displayText, value)
    }

    override fun onFetchItems(viewState: MutableLiveData<ViewState>) {
        viewModelScope.launch {
            provideItems(widgetsRepo.getWidgets())

            viewState.value = ViewState.Idle
        }
    }

    override fun onApplyChanges(
        items: List<OverviewWidget>,
        removedItems: List<OverviewWidget>,
        viewState: MutableLiveData<ViewState>
    ) {
        viewModelScope.launch {
            widgetsRepo.saveWidgets(items)

            viewState.value = ViewState.Success()
        }
    }
}
