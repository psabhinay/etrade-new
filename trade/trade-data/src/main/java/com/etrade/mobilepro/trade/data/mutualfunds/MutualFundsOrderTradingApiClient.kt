package com.etrade.mobilepro.trade.data.mutualfunds

import com.etrade.eo.rest.retrofit.Scalar
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.trade.data.mutualfunds.dto.MutualFundsTradeOrderResponseDto
import com.etrade.mobilepro.trade.data.mutualfunds.dto.MutualFundsTradeRequestValueDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Path

internal const val BUY_PREVIEW_REQUEST = "mobilemfbuypreview"
internal const val SELL_PREVIEW_REQUEST = "mobilemfsellpreview"
internal const val EXCHANGE_PREVIEW_REQUEST = "mobilemfexchangepreview"

internal const val BUY_PLACE_REQUEST = "mobilemfbuyplace"
internal const val SELL_PLACE_REQUEST = "mobilemfsellplace"
internal const val EXCHANGE_PLACE_REQUEST = "mobilemfexchangeplace"

internal interface MutualFundsOrderTradingApiClient {
    @JsonMoshi
    @POST("/app/mftrading/{requestType}.json")
    @Headers("Content-Type: application/json")
    suspend fun makeFundsPreviewOrder(
        @Path("requestType")
        requestType: String,
        @Scalar @Body body: String
    ): ServerResponseDto<MutualFundsTradeOrderResponseDto>

    @JsonMoshi
    @POST("/app/mftrading/{requestType}.json")
    suspend fun makeFundsPlaceOrder(
        @Path("requestType")
        requestType: String,
        @JsonMoshi @Body body: MutualFundsTradeRequestValueDto
    ): ServerResponseDto<MutualFundsTradeOrderResponseDto>
}
