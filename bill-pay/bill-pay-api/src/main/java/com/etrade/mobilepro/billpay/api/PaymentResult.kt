package com.etrade.mobilepro.billpay.api

import org.threeten.bp.LocalDate

sealed class PaymentResult {
    data class Success(
        val paymentId: String,
        val confirmationNumber: String,
        val deliveryDate: LocalDate
    ) : PaymentResult()
    sealed class Error : PaymentResult() {
        object DuplicateEntry : Error()
        object Generic : Error()
    }
}
