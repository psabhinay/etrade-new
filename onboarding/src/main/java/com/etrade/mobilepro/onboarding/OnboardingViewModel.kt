package com.etrade.mobilepro.onboarding

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.common.di.IsTablet
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.tracking.SCREEN_TITLE_OPEN_AN_ACCOUNT
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.AnalyticsParams
import com.etrade.mobilepro.tracking.params.OnboardingAnalyticsParams
import com.etrade.mobilepro.tracking.screen
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.timer.TimeoutTimer
import com.localytics.androidx.InAppCampaign
import com.localytics.androidx.InAppConfiguration
import com.localytics.androidx.Localytics
import com.localytics.androidx.MessagingListenerV2Adapter
import org.threeten.bp.Duration
import javax.inject.Inject

private const val LOCALYTICS_ON_BOARDING_EVENT = "Onboarding Start"
private const val ON_BOARDING_TIMEOUT_SECONDS = 10L

class OnboardingViewModel @Inject constructor(
    @IsTablet private val isTablet: Boolean,
    private val tracker: Tracker,
    private val applicationPreferences: ApplicationPreferences,
    private val timeoutTimer: TimeoutTimer
) : ViewModel() {

    private val imageAssetPrefix: String = if (isTablet) {
        "tablet"
    } else {
        "phone"
    }

    private val _onboardingPages = MutableLiveData<List<OnboardingPage>>()
    val onboardingPages: LiveData<List<OnboardingPage>>
        get() = _onboardingPages

    private val _onboardingViewedPages: MutableList<Int> by lazy { mutableListOf(1) }

    private val _dimissInAppMessageSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()
    val dismissInAppMessageSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _dimissInAppMessageSignal

    private var builtInOnBoarding = false

    private val inAppMessagingListener = object : MessagingListenerV2Adapter() {
        override fun localyticsShouldShowInAppMessage(campaign: InAppCampaign): Boolean {
            if (campaign.eventName == LOCALYTICS_ON_BOARDING_EVENT) {
                return if (shouldShowLocalyticsOnboarding(campaign, applicationPreferences)) {
                    true
                } else {
                    builtInOnBoarding = true
                    timeoutTimer.stop()
                    false
                }
            }

            return super.localyticsShouldShowInAppMessage(campaign)
        }

        override fun localyticsWillDisplayInAppMessage(campaign: InAppCampaign, configuration: InAppConfiguration): InAppConfiguration {
            if (campaign.eventName == LOCALYTICS_ON_BOARDING_EVENT) {
                applicationPreferences.isLocalyticsOnboardingShow = true
                tracker.event(OnboardingAnalyticsParams(_onboardingViewedPages, OnboardingAnalyticsParams.Action.LocalyticsOnboardingFlow))
            }

            return super.localyticsWillDisplayInAppMessage(campaign, configuration)
        }

        override fun localyticsDidDismissInAppMessage() {
            _dimissInAppMessageSignal.value = ConsumableLiveEvent(Unit)
            super.localyticsDidDismissInAppMessage()
        }
    }

    @Suppress("MagicNumber")
    fun createOnboardingPages() {
        _onboardingPages.value = listOf(
            OnboardingPage(
                title = R.string.onboarding_1_page_title,
                subtitle = R.string.onboarding_1_page_subtitle,
                image = getOnboardingImagePathForPage(1)
            ),
            OnboardingPage(subtitle = R.string.onboarding_2_page_subtitle, image = getOnboardingImagePathForPage(2)),
            OnboardingPage(subtitle = R.string.onboarding_3_page_subtitle, image = getOnboardingImagePathForPage(3)),
            OnboardingPage(
                subtitle = R.string.onboarding_4_page_subtitle,
                subtitleColor = R.color.light_black,
                image = getOnboardingImagePathForPage(4)
            )
        )
    }

    fun onboardingPageViewed(page: Int) {
        _onboardingViewedPages.add(page + 1)
    }

    fun trackOnboardingOpenAccount() {
        tracker.event(OnboardingAnalyticsParams(_onboardingViewedPages, OnboardingAnalyticsParams.Action.OpenAccount))
        tracker.screen(SCREEN_TITLE_OPEN_AN_ACCOUNT)
    }

    fun trackOnboardingLogin() {
        tracker.event(OnboardingAnalyticsParams(_onboardingViewedPages, OnboardingAnalyticsParams.Action.Login))
    }

    fun trackOnboardingDismiss() {
        tracker.event(object : AnalyticsParams {
            override val name: String
                get() = "Onboarding Start Dismiss"
            override val params: Map<String, String>
                get() = emptyMap()
        })
    }

    private fun getOnboardingImagePathForPage(page: Int): String = "file:///android_asset/${imageAssetPrefix}_onboarding_$page.png"

    fun showLocalyticsInAppMessage() {
        Localytics.setMessagingListener(inAppMessagingListener)
        if (!applicationPreferences.isLocalyticsOnboardingShow) {
            Localytics.triggerInAppMessage(LOCALYTICS_ON_BOARDING_EVENT)
            startLocalyticsOnBoardingTimer()
        } else {
            _dimissInAppMessageSignal.value = ConsumableLiveEvent(Unit)
        }
    }

    private fun startLocalyticsOnBoardingTimer() {
        timeoutTimer.start(Duration.ofSeconds(ON_BOARDING_TIMEOUT_SECONDS)) {
            if (!applicationPreferences.isLocalyticsOnboardingShow) {
                showBuiltInOnBoarding()
            }
        }
    }

    private fun showBuiltInOnBoarding() {
        createOnboardingPages()
        tracker.event(
            OnboardingAnalyticsParams(
                _onboardingViewedPages,
                if (builtInOnBoarding) {
                    OnboardingAnalyticsParams.Action.BuiltInOnboardingFlow
                } else {
                    OnboardingAnalyticsParams.Action.BackUpBuiltInOnboardingFlow
                }
            )
        )
    }
}
