package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.localytics.DefaultLocalyticsHelper
import com.etrade.mobilepro.localytics.LocalyticsHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalyticsModule {

    @Provides
    @Singleton
    fun provideLocalyticsHelper(): LocalyticsHelper = DefaultLocalyticsHelper()
}
