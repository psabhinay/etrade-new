package com.etrade.mobilepro.transfermoney.presentation.activity

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.disposeOnDestinationChanged
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.transfermoney.api.TransferMoneyActivity
import com.etrade.mobilepro.transfermoney.presentation.R
import com.etrade.mobilepro.transfermoney.presentation.TransferMoneyContainerFragmentDirections
import com.etrade.mobilepro.transfermoney.presentation.TransferMoneyNavigationFlowViewModel
import com.etrade.mobilepro.transfermoney.presentation.adapter.activity.TransferActivityAdapter
import com.etrade.mobilepro.transfermoney.presentation.common.parcelize
import com.etrade.mobilepro.transfermoney.presentation.databinding.TransferFragmentActivityBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@RequireLogin
class TransferActivityFragment @Inject constructor(
    private val snackbarUtilFactory: SnackbarUtilFactory,
    viewModelFactory: ViewModelProvider.Factory
) : Fragment(R.layout.transfer_fragment_activity) {

    private val binding by viewBinding(TransferFragmentActivityBinding::bind)
    private val viewModel by navGraphViewModels<TransferActivityViewModel>(R.id.transfer_money_nav_graph) { viewModelFactory }
    private val navigationFlowViewModel: TransferMoneyNavigationFlowViewModel by navGraphViewModels(R.id.transfer_money_nav_graph)
    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    // Use parent viewLifecycleOwner to show snackbar in ViewPager
    private val parentSnackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ requireParentFragment().viewLifecycleOwner }, { view }) }
    private val transferActivityAdapter by lazy { TransferActivityAdapter { openDetails(it) } }

    private val disposeListener = disposeOnDestinationChanged { viewModel.disposeState() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
    }

    override fun onResume() {
        super.onResume()
        findNavController().addOnDestinationChangedListener(disposeListener)
    }

    override fun onPause() {
        super.onPause()
        findNavController().removeOnDestinationChangedListener(disposeListener)
    }

    private fun setupFragment() {
        binding.apply {
            swipeRefresh.apply {
                applyColorScheme()
                setOnRefreshListener {
                    viewModel.refresh()
                }
                isRefreshing = false
            }
            recyclerView.apply {
                layoutManager = LinearLayoutManager(requireContext())
                setupRecyclerViewDivider(R.drawable.thin_divider)
                adapter = transferActivityAdapter
            }
        }
    }

    private fun bindViewModel() {
        bindViewState()
        bindTransferCancelledSignal()
        bindShowNewActivityDetails()
    }

    private fun bindViewState() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                binding.noContent.visibility = if (it is TransferActivityViewModel.ViewState.Success && it.items.isEmpty()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }
                handleViewState(it)
            }
        )
    }

    private fun handleViewState(it: TransferActivityViewModel.ViewState?) {
        binding.apply {
            when (it) {
                TransferActivityViewModel.ViewState.Loading.Initial -> {
                    progressBar.show()
                    swipeRefresh.isRefreshing = false
                }
                TransferActivityViewModel.ViewState.Loading.Refreshing -> {
                    progressBar.hide()
                    swipeRefresh.isRefreshing = true
                }
                is TransferActivityViewModel.ViewState.Success -> {
                    progressBar.hide()
                    swipeRefresh.isRefreshing = false
                    transferActivityAdapter.items = it.items
                    bindShowActivityDetails(it.items)
                }
                is TransferActivityViewModel.ViewState.Error -> {
                    progressBar.hide()
                    swipeRefresh.isRefreshing = false
                    snackBarUtil.retrySnackbar { it.retry.invoke() }
                }
            }
        }
    }

    private fun bindTransferCancelledSignal() {
        viewModel.transferCancelledSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.let { onlyOneWasCancelled ->
                    val stringRes = if (onlyOneWasCancelled) {
                        R.string.transfer_cancelled_desc
                    } else {
                        R.string.transfer_series_cancelled_desc
                    }
                    parentSnackBarUtil.snackbar(getString(stringRes), duration = Snackbar.LENGTH_SHORT)
                    viewModel.refresh()
                }
            }
        )
    }

    private fun bindShowNewActivityDetails() {
        navigationFlowViewModel.showNewActivityDetails.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.run {
                    viewModel.refresh()
                    navigationFlowViewModel.referenceNumber = it.peekContent()
                }
            }
        )
    }

    private fun bindShowActivityDetails(items: List<TransferMoneyActivity>) {
        navigationFlowViewModel.referenceNumber?.also { referenceNumber ->
            items.firstOrNull { it.referenceNumber == referenceNumber }
                ?.also { openDetails(it) }
        }
    }

    private fun openDetails(it: TransferMoneyActivity) {
        findNavController().navigate(TransferMoneyContainerFragmentDirections.actionContainerToTransferDetails(it.parcelize()))
    }
}
