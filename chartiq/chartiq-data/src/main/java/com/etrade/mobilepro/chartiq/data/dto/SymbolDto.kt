package com.etrade.mobilepro.chartiq.data.dto

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class SymbolDto(

    @Json(name = "symbol")
    override val symbol: String,

    @Json(name = "securityType")
    val securityType: String
) : WithSymbolInfo {

    override val instrumentType: InstrumentType = InstrumentType.from(securityType)
}
