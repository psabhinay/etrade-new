package com.etrade.mobilepro.trade.api

import java.math.BigDecimal

interface WithCostOrder {
    val fee: BigDecimal?
    val cost: BigDecimal?
}
