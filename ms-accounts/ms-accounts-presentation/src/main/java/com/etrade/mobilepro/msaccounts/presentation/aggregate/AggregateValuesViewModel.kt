package com.etrade.mobilepro.msaccounts.presentation.aggregate

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.etrade.completeview.dto.AccountSummarySectionDto
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.accountvalues.COMPLETE_VIEW_SUMMARY_STREAM_ID
import com.etrade.mobilepro.completeview.getStreamingText
import com.etrade.mobilepro.completeview.model.AccountSummarySection
import com.etrade.mobilepro.completeview.toAccountSummarySection
import com.etrade.mobilepro.completeview.toStreamingValueDto
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.Diffable
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.msaccounts.presentation.BR
import com.etrade.mobilepro.msaccounts.presentation.R
import com.etrade.mobilepro.streaming.api.StreamingTextManager
import com.etrade.mobilepro.util.safeParseBigDecimal
import com.etrade.msaccounts.dto.AggregateValuesDto
import java.math.BigDecimal

private const val STREAM_ID_NET_ASSETS = "NET_ASSETS"

class AggregateValuesViewModel(
    dto: AggregateValuesDto,
    private val completeViewTextManager: StreamingTextManager,
    private val defaultTextManager: StreamingTextManager,
    private val resources: Resources
) : DynamicViewModel(R.layout.view_aggregate_values), Diffable {

    override val variableId: Int = BR.obj

    private val _aggregateValues = MutableLiveData<List<AccountSummarySection>>()
    val aggregateValues: LiveData<List<AccountSummarySection>>
        get() = _aggregateValues

    init {
        initAggregateValue(dto)
    }

    // Assumes that there will be only one AggregateValuesViewModel in a list
    override fun isSameItem(other: Any) =
        other.javaClass == javaClass

    override fun hasSameContents(other: Any) = false

    fun onInfoClicked() {
        clickEvents.postValue(
            CtaEvent.InfoDialogEvent(
                resources.getString(R.string.ms_accounts_aggregate_values_info_title),
                resources.getString(R.string.ms_accounts_aggregate_values_info_content)
            )
        )
    }

    fun refresh(dto: AggregateValuesDto) {
        initAggregateValue(dto)
    }

    private fun initAggregateValue(dto: AggregateValuesDto) {
        _aggregateValues.value = dto.createAccountSummarySections()
    }

    private fun AggregateValuesDto.createAccountSummarySections(): List<AccountSummarySection> {
        val streamingTextManager = if (accountId == null) {
            completeViewTextManager
        } else {
            defaultTextManager
        }

        return listOfNotNull(
            msAccountsTotalAssets?.createCombinedAssetsAccountSummarySection(
                netAssets,
                accountId,
                streamingTextManager
            ),
            msAccountsTotalLiabilities?.copy(title = resources.getString(R.string.ms_accounts_aggregate_values_liabilities_value_label))
                ?.toAccountSummarySection(streamingTextManager, null, false)
        )
    }

    private fun AccountSummarySectionDto.buildCombinedAssetsSectionDto(netAssets: String): AccountSummarySectionDto {
        return copy(
            title = resources.getString(R.string.ms_accounts_aggregate_values_combined_assets_label),
            value = value.copy(
                initial = MarketDataFormatter.formatMoneyValueWithCurrency(
                    calculateCombinedAssets(netAssets, value.initial)
                )
            )
        )
    }

    private fun AccountSummarySectionDto.createCombinedAssetsAccountSummarySection(
        netAssets: String,
        accountId: String?,
        streamingTextManager: StreamingTextManager
    ) = AccountSummarySection(
        resources.getString(R.string.ms_accounts_aggregate_values_combined_assets_label),
        getStreamingText(
            streamingTextManager,
            copy(
                value = value.copy(
                    initial = netAssets,
                    streamId = STREAM_ID_NET_ASSETS
                )
            ).toStreamingValueDto(
                accountId ?: COMPLETE_VIEW_SUMMARY_STREAM_ID
            ),
            isAccountStreamingAllowed = true
        ).map {
            it.copy(
                text = MarketDataFormatter.formatMoneyValueWithCurrency(
                    calculateCombinedAssets(it.text, value.initial)
                )
            )
        }
    )

    private fun calculateCombinedAssets(
        netAssets: String?,
        msAccountsTotalAssets: String?
    ): BigDecimal? {
        val etAssets = netAssets?.safeParseBigDecimal()
        val msAssets = msAccountsTotalAssets?.safeParseBigDecimal()

        etAssets ?: msAssets ?: return null

        return (etAssets ?: BigDecimal.ZERO) + (msAssets ?: BigDecimal.ZERO)
    }
}
