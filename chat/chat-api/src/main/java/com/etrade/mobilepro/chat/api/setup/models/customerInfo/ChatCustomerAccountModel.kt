package com.etrade.mobilepro.chat.api.setup.models.customerInfo

data class ChatCustomerAccountModel(
    val accNum: String,
    val accountType: String? = null,
    val accounts: List<ChatAccountModel> = emptyList(),
    val instDescription: String,
    val instNo: String? = "",
    val instName: String? = null,
    val accountHolderCount: String? = null,
    val isOH: Boolean = false
)

data class ChatAccountModel(
    val firstname: String? = null,
    val lastname: String? = null,
    val cid: String? = null,
    val tier: String? = null
)
