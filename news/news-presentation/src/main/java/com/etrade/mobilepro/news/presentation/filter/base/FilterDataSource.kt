package com.etrade.mobilepro.news.presentation.filter.base

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.util.NetworkState

interface FilterDataSource<T> {
    val networkState: LiveData<NetworkState>
    val items: LiveData<List<T>>
    val selectedItem: LiveData<T>
    fun refresh()
    fun getSelectedItemIndex(): Int
    fun setSelectedItem(item: T)
    fun findAndSelectItemByAccountId(accountId: String, isStockPlan: Boolean? = null)
    fun clear()
}
