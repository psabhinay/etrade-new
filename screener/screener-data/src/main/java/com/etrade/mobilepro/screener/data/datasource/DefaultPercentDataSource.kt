package com.etrade.mobilepro.screener.data.datasource

import android.content.res.Resources
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.Percent
import com.etrade.mobilepro.screener.api.datasource.PercentDataSource
import com.etrade.mobilepro.screener.data.R
import javax.inject.Inject

class DefaultPercentDataSource @Inject constructor(private val resources: Resources) : PercentDataSource {
    private val emptyPercentItem by lazy {
        Percent(
            id = "",
            title = resources.getString(R.string.percent_invested_hint),
            isAllItemsElement = true,
            clauses = emptyList()
        )
    }
    private val percents by lazy {
        listOf(
            Percent(
                id = resources.getString(R.string.percent_less_than_10),
                title = resources.getString(R.string.percent_less_than_10),
                clauses = listOf(
                    Clause(
                        operator = OPERATOR_GREATER_THAN,
                        values = listOf("0")
                    ),
                    Clause(
                        operator = OPERATOR_LESS_THAN,
                        values = listOf("10")
                    )
                )
            ),
            Percent(
                id = resources.getString(R.string.percent_less_than_20),
                title = resources.getString(R.string.percent_less_than_20),
                clauses = listOf(
                    Clause(
                        operator = OPERATOR_GREATER_THAN,
                        values = listOf("0")
                    ),
                    Clause(
                        operator = OPERATOR_LESS_THAN,
                        values = listOf("20")
                    )
                )
            ),
            Percent(
                id = resources.getString(R.string.percent_less_than_30),
                title = resources.getString(R.string.percent_less_than_30),
                clauses = listOf(
                    Clause(
                        operator = OPERATOR_GREATER_THAN,
                        values = listOf("0")
                    ),
                    Clause(
                        operator = OPERATOR_LESS_THAN,
                        values = listOf("30")
                    )
                )
            ),
            Percent(
                id = resources.getString(R.string.percent_less_than_40),
                title = resources.getString(R.string.percent_less_than_40),
                clauses = listOf(
                    Clause(
                        operator = OPERATOR_GREATER_THAN,
                        values = listOf("0")
                    ),
                    Clause(
                        operator = OPERATOR_LESS_THAN,
                        values = listOf("40")
                    )
                )
            ),
            Percent(
                id = resources.getString(R.string.percent_less_than_50),
                title = resources.getString(R.string.percent_less_than_50),
                clauses = listOf(
                    Clause(
                        operator = OPERATOR_GREATER_THAN,
                        values = listOf("0")
                    ),
                    Clause(
                        operator = OPERATOR_LESS_THAN,
                        values = listOf("50")
                    )
                )
            ),
            Percent(
                id = resources.getString(R.string.percent_more_than_50),
                title = resources.getString(R.string.percent_more_than_50),
                clauses = listOf(
                    Clause(
                        operator = OPERATOR_GREATER_THAN,
                        values = listOf("50")
                    ),
                    Clause(
                        operator = OPERATOR_LESS_THAN,
                        values = listOf("101")
                    )
                )
            ),
            Percent(
                id = resources.getString(R.string.percent_more_than_60),
                title = resources.getString(R.string.percent_more_than_60),
                clauses = listOf(
                    Clause(
                        operator = OPERATOR_GREATER_THAN,
                        values = listOf("60")
                    ),
                    Clause(
                        operator = OPERATOR_LESS_THAN,
                        values = listOf("101")
                    )
                )
            ),
            Percent(
                id = resources.getString(R.string.percent_more_than_70),
                title = resources.getString(R.string.percent_more_than_70),
                clauses = listOf(
                    Clause(
                        operator = OPERATOR_GREATER_THAN,
                        values = listOf("70")
                    ),
                    Clause(
                        operator = OPERATOR_LESS_THAN,
                        values = listOf("101")
                    )
                )
            ),
            Percent(
                id = resources.getString(R.string.percent_more_than_80),
                title = resources.getString(R.string.percent_more_than_80),
                clauses = listOf(
                    Clause(
                        operator = OPERATOR_GREATER_THAN,
                        values = listOf("80")
                    ),
                    Clause(
                        operator = OPERATOR_LESS_THAN,
                        values = listOf("101")
                    )
                )
            ),
            Percent(
                id = resources.getString(R.string.percent_more_than_90),
                title = resources.getString(R.string.percent_more_than_90),
                clauses = listOf(
                    Clause(
                        operator = OPERATOR_GREATER_THAN,
                        values = listOf("90")
                    ),
                    Clause(
                        operator = OPERATOR_LESS_THAN,
                        values = listOf("101")
                    )
                )
            )
        )
    }

    override fun getEmptyPercent(): Percent = emptyPercentItem

    override fun getExposurePercentList(): List<Percent> = percents
}
