package com.etrade.mobilepro.watchlist.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class WatchlistMessage(

    @Json(name = "code")
    val code: String?,

    @Json(name = "type")
    val type: String?,

    @Json(name = "subType")
    val subType: String?,

    @Json(name = "priority")
    val priority: Int?,

    @Json(name = "text")
    val text: String?
)
