package com.etrade.mobilepro.screener.api.datasource

import com.etrade.mobilepro.screener.api.MovingAverage

interface MovingAveragesDataSource {
    fun getEmptyItem(): MovingAverage
    fun getMovingAverages(): List<MovingAverage>
}
