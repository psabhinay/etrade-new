package com.etrade.mobilepro.util.android

/**
 * Used as a wrapper for data exposed via a LiveData data to provide.
 *  1. One time consumable content
 *  2. Explicit access to the content even if its consumed earlier.
 * Also, overcomes the limitation of SingleLiveEvent - restricted to one observer
 */
data class ConsumableLiveEvent<out T>(private val content: T) {

    val isConsumed: Boolean
        get() = _isConsumed
    private var _isConsumed = false

    /**
     * Returns the content and prevents its use again.
     *
     * @param consume: The condition to know if we should consume the consumable or not
     */
    fun getNonConsumedContent(consume: Boolean = true): T? {
        return if (_isConsumed) {
            null
        } else {
            if (consume) {
                _isConsumed = true
            }
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}

/**
 * Consumes an event.
 *
 * @param block The consumer of an event's payload.
 */
fun <T> ConsumableLiveEvent<T>.consume(block: ((T) -> Unit)? = null) {
    getNonConsumedContent()?.run { block?.invoke(this) }
}

/**
 * Consumes an event if a given [condition] is `true`. If [condition] is `false` the event will not be consumed.
 *
 * @param condition The condition to meet for a consumption.
 * @param block The consumer of an event's payload.
 */
fun <T> ConsumableLiveEvent<T>.consumeIf(condition: Boolean, block: (T) -> Unit) {
    if (condition) {
        consume(block)
    }
}

/**
 * Consumes an event if a given [condition] is `false`. If [condition] is `true` the event will not be consumed.
 *
 * @param condition The condition to meet for a consumption.
 * @param block The consumer of an event's payload.
 */
fun <T> ConsumableLiveEvent<T>.consumeIfNot(condition: Boolean, block: (T) -> Unit) = consumeIf(!condition, block)

fun <T> T.consumable(): ConsumableLiveEvent<T> = ConsumableLiveEvent(this)
