package com.etrade.mobilepro.optionschain.tableview

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.horizontallistpicker.HorizontalListPicker
import com.etrade.mobilepro.optionschain.tableview.adapter.OptionsListAdapter
import com.etrade.mobilepro.optionschain.tableview.view.OptionsChainTableView
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.uiwidgets.groupscrollrecyclerview.VerticalGroupScrollRecyclerView.OnScrollStoppedListener
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory

abstract class OptionChainTableViewFragment(
    layoutRes: Int,
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    @IdRes private val optionsChainDatePickerId: Int,
    @IdRes private val optionsChainSwipeId: Int,
    @IdRes private val optionsChainTableId: Int
) : Fragment(layoutRes), OptionItemClickListener {

    protected val optionChainTableViewModel: OptionChainTableViewModel by viewModels { viewModelFactory }
    protected val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }) { view } }

    protected val errorObserver: Observer<ConsumableLiveEvent<String>> = Observer {
        it.getNonConsumedContent()?.let { message -> snackBarUtil.errorSnackbar(message) }
    }

    private val handler = Handler(Looper.getMainLooper())

    private var optionTableView: OptionsChainTableView? = null
    private var datePicker: HorizontalListPicker? = null
    private var optionsChainSwipeRefreshView: SwipeRefreshLayout? = null

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewProperties(view)
        setupViewModels()
        setUpViewAdapters()
        setupDatePicker()
        setupSwipeRefresh()
    }

    protected open fun onOptionChainPageLoad() = Unit
    protected open fun onOptionChainPageRefresh() = Unit

    protected fun loadPage(symbol: String) {
        onOptionChainPageLoad()
        optionChainTableViewModel.loadOptionsChain(symbol)
    }

    private fun setupViewProperties(view: View) {
        optionTableView = view.findViewById(optionsChainTableId)
        datePicker = view.findViewById(optionsChainDatePickerId)
        optionsChainSwipeRefreshView = view.findViewById(optionsChainSwipeId)
    }

    private fun setupSwipeRefresh() {
        optionsChainSwipeRefreshView?.apply {
            applyColorScheme()
            setOnRefreshListener {
                refreshPage()
            }
        }
    }

    private fun setupDatePicker() {
        datePicker?.apply {
            selector = BottomSheetSelector(childFragmentManager, resources)
            selectorTitle = getString(R.string.options_chain_expiration_date_selector)
            onSelectedItemPositionChangeListener = object : HorizontalListPicker.OnSelectedItemPositionChangeListener {
                override fun onNewSelectedItemPosition(position: Int) {
                    optionChainTableViewModel.updateSelectedExpirationDatePosition(position)
                    refreshPage()
                }
            }
        }
    }

    private fun refreshPage() {
        onOptionChainPageRefresh()
        optionChainTableViewModel.refresh()
    }

    private fun setUpViewAdapters() {
        val callAdapter = OptionsListAdapter(viewLifecycleOwner, R.layout.options_chain_item_row_call, requireContext())
        callAdapter.optionClickListener = this
        val putAdapter = OptionsListAdapter(viewLifecycleOwner, R.layout.options_chain_item_row_put, requireContext())
        putAdapter.optionClickListener = this
        optionTableView?.setAdapters(callAdapter, putAdapter)
    }

    private fun setupViewModels() {
        optionChainTableViewModel.apply {
            expirationDates.observe(viewLifecycleOwner, { datePicker?.items = it })
            selectedExpirationDatePosition.observe(
                viewLifecycleOwner,
                {
                    datePicker?.selectedItemPosition = it
                    datePicker?.setSelectorContentDescription(
                        "${getString(R.string.label_dropdown_description, getString(R.string.options_chain_expiration_date))}, ${datePicker?.items?.get(it)}"
                    )
                }
            )
            loadingIndicator.observe(viewLifecycleOwner, { optionsChainSwipeRefreshView?.isRefreshing = it })
            errorMessage.observe(viewLifecycleOwner, errorObserver)

            optionPairs.observe(viewLifecycleOwner, { handleOptionPairs(it) })
        }
    }

    private fun handleOptionPairs(it: List<SelectedOptionsPair>) {
        optionTableView?.apply {
            setOptions(it)
            shiftTableForInTheMoneyVisibility(it.totalInTheMoneyCount)
            this@OptionChainTableViewFragment.handler.post {
                animateFanIn()
                optionChainTableViewModel.updateVisiblePositions(getCurrentVisibleRange(), true)
                setOnScrollStoppedListener(object : OnScrollStoppedListener {
                    override fun onScrollStopped(recyclerView: RecyclerView) {
                        optionChainTableViewModel.updateVisiblePositions(getCurrentVisibleRange())
                    }
                })
            }
        }
    }

    private val List<SelectedOptionsPair>.totalInTheMoneyCount
        get() = count { pair -> pair.call.option.isInTheMoney }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacksAndMessages(null)
    }

    override fun onPause() {
        super.onPause()
        optionChainTableViewModel.stopStreaming()
    }

    override fun onResume() {
        super.onResume()
        optionChainTableViewModel.resumeStreaming()
    }
}
