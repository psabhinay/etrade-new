package com.etrade.mobilepro.screener.presentation.base

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.databinding.FragmentResultsBinding
import com.etrade.mobilepro.screener.presentation.tableview.SymbolWithExtraHeightTableViewAdapter
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.tableviewutils.presentation.getVisibleSymbols
import com.etrade.mobilepro.tableviewutils.presentation.updateSortViewState
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.android.textutil.TextUtil

abstract class BaseScreenerResultFragment constructor(
    snackbarUtilFactory: SnackbarUtilFactory,
    private val router: MainNavigation
) : OverlayBaseFragment() {

    private val binding by viewBinding(FragmentResultsBinding::bind)
    override val layoutRes: Int = R.layout.fragment_results

    protected abstract val viewModel: BaseScreenerResultViewModel

    protected open val disclosureText: String? = null

    protected abstract val resultsDisclosureClickListener: () -> Unit

    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    private val onTableViewScrollListener = object : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            binding.resultsTable.getVisibleSymbols().let { visibleItems ->
                viewModel.notifyVisibleItemsChanged(visibleItems)
            }
        }
    }

    private var pendingAction: (() -> Unit)? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
        if (savedInstanceState == null) {
            viewModel.refresh()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.resumeStreaming()
    }

    override fun onPause() {
        super.onPause()
        viewModel.pauseStreaming()
    }

    private fun setupFragment() {
        binding.apply {
            resultsTable.apply {
                initTableViewParams(
                    tableViewListener = ScreenerResultTableViewListener(
                        viewModel
                    ) { symbol, instrumentType ->
                        router.navigateToQuoteDetails(activity, SearchResultItem.Symbol(symbol, instrumentType).toBundle())
                    },
                    adapterProvider = {
                        SymbolWithExtraHeightTableViewAdapter(requireActivity())
                    }
                )
                rowHeaderRecyclerView.addOnScrollListener(onTableViewScrollListener)
                isSortable = true
            }

            disclosureText?.let {
                disclosureContent.text = TextUtil.convertTextWithHmlTagsToSpannable(it)
                disclosureContent.visibility = View.VISIBLE
            }

            disclosuresBtn.setOnClickListener {
                resultsDisclosureClickListener.invoke()
            }
        }
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    BaseScreenerResultViewModel.ViewState.Loading -> {
                        handleLoadingViewState()
                    }
                    is BaseScreenerResultViewModel.ViewState.Success -> {
                        handleSuccessViewState(it)
                    }
                    BaseScreenerResultViewModel.ViewState.Error -> {
                        handleErrorViewState()
                    }
                }
            }
        )

        viewModel.sortState.observe(
            viewLifecycleOwner,
            Observer { handleSortState(it) }
        )
    }

    private fun handleLoadingViewState() {
        binding.apply {
            loadingPb.show()
            noItemsTv.visibility = View.GONE
            resultsScrollContainer.visibility = View.GONE
        }
    }

    private fun handleSuccessViewState(viewState: BaseScreenerResultViewModel.ViewState.Success) {
        binding.apply {
            loadingPb.hide()
            if (viewState.dataSet.isEmpty) {
                noItemsTv.visibility = View.VISIBLE
                resultsScrollContainer.visibility = View.GONE
            } else {
                noItemsTv.visibility = View.GONE
                resultsScrollContainer.visibility = View.VISIBLE
                resultsTable.applyData(
                    columnHeaderList = viewState.dataSet.columnHeaders,
                    rowHeaders = viewState.dataSet.rowHeaders,
                    cellItems = viewState.dataSet.cellItems
                )
                pendingAction?.invoke()
            }
        }
    }

    private fun handleErrorViewState() {
        binding.apply {
            loadingPb.hide()
            noItemsTv.visibility = View.GONE
            resultsScrollContainer.visibility = View.GONE
            snackbarUtil.retrySnackbar {
                viewModel.refresh()
            }
        }
    }

    private fun handleSortState(sortInfo: SortStateInfo) {
        binding.resultsTable.apply {
            if (adapter == null) {
                pendingAction = {
                    pendingAction = null
                    updateSortViewState(sortInfo)
                }
            } else {
                updateSortViewState(sortInfo)
            }
        }
    }
}
