package com.etrade.mobilepro.instrument

import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.GregorianCalendar
import java.util.Locale

private const val MIN_OPTION_SYMBOL_LENGTH = 20
private const val OPTION_CALL_STRING = "Call"
private const val OPTION_PUT_STRING = "Put"

private val gMonNumMonths = mapOf(
    "Jan" to "01",
    "Feb" to "02",
    "Mar" to "03",
    "Apr" to "04",
    "May" to "05",
    "Jun" to "06",
    "Jul" to "07",
    "Aug" to "08",
    "Sep" to "09",
    "Oct" to "10",
    "Nov" to "11",
    "Dec" to "12"
)

fun getSymbolUnderlier(symbol: String, type: InstrumentType): String? {
    return if (type.isOption) {
        symbol.split("--").firstOrNull()?.let { underlyingSymbol ->
            checkAndRectifyIndexUnderlyingSymbol(underlyingSymbol)
        }
    } else {
        symbol
    }
}

fun checkAndRectifyIndexUnderlyingSymbol(underlyingSymbol: String) = when (underlyingSymbol.uppercase(Locale.ROOT)) {
    "SPXW" -> "SPX"
    "VIXW" -> "VIX"
    "RUTW" -> "RUT"
    "NDXP" -> "NDX"
    else -> underlyingSymbol
}

fun isSymbolLongOption(symbol: String): Boolean = symbol.length > MIN_OPTION_SYMBOL_LENGTH

fun formatToShortSymbol(symbol: String, default: String? = null): String = if (isSymbolLongOption(symbol)) {
    symbol.substringBefore('-')
} else {
    default ?: symbol
}

/**
 * @param osiKey QQQ---091121C00044000
 * @return "QQQ NOV 21 '09 $44.00 CALL"
 */
@SuppressWarnings("MagicNumber", "LongMethod", "TooGenericExceptionCaught")
fun getDisplaySymbolFromOsiKey(osiKey: String): String? {
    try {
        var symbolPart = osiKey.substring(0, 6)
        symbolPart = symbolPart.replace("0".toRegex(), "").replace("-".toRegex(), "")

        // Date part
        val date = getExpirationDateFromOsiKey(osiKey)

        val calendar = GregorianCalendar.getInstance()
        calendar.set(Calendar.YEAR, date.year)
        // In Calendar class Jan=0, Feb=1
        calendar.set(Calendar.MONTH, date.month - 1)
        calendar.set(Calendar.DAY_OF_MONTH, date.day)

        val datePart = SimpleDateFormat("MMM dd ''yy", Locale.US).format(
            calendar
                .time
        )

        // Call/Put part
        val callPutPart = getCallPutPartFromOsiKey(osiKey)

        // Strike price part
        val strike = osiKey.substring(13).replace("-".toRegex(), "0")
        // We now have strike equal to something like "00085000" which would
        // mean $85.00.
        val strikeDouble = Integer.parseInt(strike) / 1000.0
        var strikePart = NumberFormat.getCurrencyInstance(Locale.US).format(
            strikeDouble
        )
        strikePart = strikePart.replace(".00", "") // ommit .00 to follow same description format from server side.
        return symbolPart.plus(" ").plus(datePart).plus(" ").plus(strikePart).plus(" ").plus(callPutPart)
    } catch (e: Exception) {
        return null
    }
}

// Suppose the symbol is GOU MA from a saved watchlist
// then from displaysymbol "GOU Jan 16 '10 $110 Put" construct the symbol
// like GOU---100116P00110000
// sometimes wrong input from currentprice display symbol comes up like this
// DLY AM JAN-10 $11 CALL
// correct input is "DLY Jan 16 '10 $11 Call"
@SuppressWarnings("LongMethod", "TooGenericExceptionCaught", "TooGenericExceptionThrown", "MagicNumber")
fun getConstructedOsiSymbol(displaySymbol: String): String {
    // If it is a regular stock symbol return the same symbol do not process anything.
    if (!displaySymbol.contains("$")) {
        return displaySymbol
    }

    val osiSymbol: String
    val symbol: String
    val month: String
    var year: String
    val date: String
    var callputIndicator: String
    var strike: String
    val expDate: String
    var strikeLeft: String
    var strikeRight: String

    try {
        val tokens = displaySymbol.split(" ")
        symbol = tokens[0]
        month = tokens[1]
        date = tokens[2]
        year = tokens[3]
        strike = tokens[4]
        callputIndicator = tokens[5]
        callputIndicator = when {
            callputIndicator.equals(OPTION_CALL_STRING, ignoreCase = true) -> "C"
            callputIndicator.equals(OPTION_PUT_STRING, ignoreCase = true) -> "P"
            else -> throw RuntimeException()
        }
        year = year.substring(year.indexOf("'") + 1, year.length)
        callputIndicator = callputIndicator.substring(0)
        strike = strike.substring(strike.indexOf("$") + 1, strike.length)

        // TD#7457 Some Options do not load in Excel Manager, some symbols from server doesn't contain strike with . and decimal "AJL Apr 17 '10 $240 Put".
        // example condition added to address the same.
        when {
            strike.contains(".") -> {
                strikeLeft = strike.substring(0, strike.indexOf("."))
                strikeRight = strike.substring(strike.indexOf(".") + 1, strike.length)
            }
            else -> {
                strikeLeft = strike
                strikeRight = "000"
            }
        }
        expDate = year.plus(gMonNumMonths[month]).plus(date)
        strikeLeft = padLeft(strikeLeft)
        strikeRight = padRight(strikeRight)
        strike = strikeLeft + strikeRight
        osiSymbol = (
            fillSymbolWithDash(symbol) + expDate +
                callputIndicator + strike
            )
    } catch (e: Exception) {
        // incase of any exception just return the displaysymbol so that excel watchlist doesn't get affected due to some symbols having issue.
        return displaySymbol
    }
    return osiSymbol
}

/**
 * Gets an option type from a symbol.
 *
 * @param symbol The symbol to get an option type from.
 *
 * @return `OptionType` for the [symbol] or `null` if the [symbol] is not an option.
 */
@SuppressWarnings("MagicNumber")
fun getOptionType(symbol: String): OptionType? {
    return symbol.takeIf { isSymbolLongOption(it) }
        ?.let {
            when (symbol[12]) {
                'C' -> OptionType.CALL
                'P' -> OptionType.PUT
                else -> OptionType.UNKNOWN
            }
        }
}

@SuppressWarnings("MagicNumber")
fun getExpirationDateFromOsiKey(osiKey: String, isAMOption: Boolean = false): ExpirationDate {
    val year = 2000 + Integer.parseInt(osiKey.substring(6, 8))
    val month = Integer.parseInt(osiKey.substring(8, 10))
    val day = Integer.parseInt(osiKey.substring(10, 12))
    return ExpirationDate(year, month, day, if (isAMOption) SettlementSessionCode.AM else SettlementSessionCode.PM)
}

@Suppress("MagicNumber")
fun getStrikePrice(symbol: String): BigDecimal? {
    return symbol.takeIf { isSymbolLongOption(symbol) }
        ?.substring(13)
        ?.let { BigDecimal(it).divide(BigDecimal.valueOf(1000)) }
}

internal fun getCallPutPartFromOsiKey(osiKey: String): String? {
    return when (getOptionType(osiKey)) {
        OptionType.CALL -> OPTION_CALL_STRING
        OptionType.PUT -> OPTION_PUT_STRING
        else -> null
    }
}

/**
 * Fills the remaining spaces of symbol with Dash(-). In the entire 21
 * character long symbol (GOU----100116P00110000), symbol reserves 6 spaces
 * so if the actual symbol length is less that 6, rest space needs to be
 * filled with dash. Ex:GOU---.
 */
@SuppressWarnings("MagicNumber")
private fun fillSymbolWithDash(symbol: String): String {
    return symbol.padEnd(6, '-')
}

private fun padLeft(s: String): String {
    val formatter = DecimalFormat("00000")
    val replacedString = s.replace(",", "").toDoubleOrNull()
    return replacedString?.let { formatter.format(it) } ?: throw IllegalArgumentException()
}

private fun padRight(strike: String): String {
    return when (strike.length) {
        1 -> strike.plus("00")
        2 -> strike.plus("0")
        else -> strike
    }
}
