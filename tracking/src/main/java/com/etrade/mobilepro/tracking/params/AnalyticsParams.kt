package com.etrade.mobilepro.tracking.params

interface AnalyticsParams {

    val name: String
    val params: Map<String, String>
}
