package com.etrade.mobilepro.balance.section

import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.balance.data.repo.AccountBalancesRepo
import com.etrade.mobilepro.balance.data.repo.BalanceRequest
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.CtaEvent.RetryEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.home.overview.viewmodel.OVERVIEW_CACHE_EXPIRATION_TIME
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.util.Resource
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy

private const val BALANCE_CACHE_EXPIRATION_EXPIRATION = OVERVIEW_CACHE_EXPIRATION_TIME

class BalancesDetailsViewModel(
    private val balancesRepo: AccountBalancesRepo,
    private val accountId: String,
    private val sectionMapper: BalanceSectionMapper,
    private val userId: String
) : DynamicViewModel(layoutId = R.layout.balances_account_details) {

    override val variableId: Int = BR.obj

    private var compositeDisposable = CompositeDisposable()

    val loadingIndicator: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = true
    }

    private val onMoreInfoClickListener: (BalancesActionType) -> Unit = { actionType ->
        val (titleId, messageId) = actionType.resolveTextResources()
        clickEvents.value = CtaEvent.NavDirectionsNavigation(
            MoreInfoPopupFragmentDirections.actionShowMoreInfoPopup(titleId, messageId)
        )
    }

    val sectionItems = MutableLiveData<List<TableSectionItemLayout>>()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun fetch(useCache: Boolean = true) {
        compositeDisposable.clear()

        compositeDisposable.add(
            balancesRepo.getBalances(BalanceRequest(accountId, userId), BALANCE_CACHE_EXPIRATION_EXPIRATION.takeIf { useCache })
                .subscribeBy(
                    onError = { onError(it) },
                    onNext = {
                        when (it) {
                            is Resource.Failed -> onError(it.error)

                            is Resource.Success -> {
                                it.data?.let { accountBalance ->
                                    val data = sectionMapper.map(accountBalance.balances, onMoreInfoClickListener)
                                    sectionItems.postValue(data)
                                }
                                loadingIndicator.postValue(false)
                            }
                        }
                    }
                )
        )
    }

    fun refresh() {
        fetch(false)
    }

    // Retry balances service call
    override fun onCtaClicked(appUrl: String?) {
        loadingIndicator.postValue(true)
        refresh()
    }

    private fun onError(throwable: Throwable?) {
        logger.error("unable to load account balances", throwable)
        clickEvents.postValue(RetryEvent)
        loadingIndicator.postValue(false)
    }

    private fun BalancesActionType.resolveTextResources() =
        when (this) {
            BalancesActionType.MORE_INFO_FED_CALL ->
                R.string.fed_call_more_info_popup_title to R.string.fed_call_more_info_popup_message
            BalancesActionType.MORE_INFO_MAINTENANCE_CALL ->
                R.string.maintenance_call_more_info_popup_title to R.string.maintenance_call_more_info_popup_message
        }
}
