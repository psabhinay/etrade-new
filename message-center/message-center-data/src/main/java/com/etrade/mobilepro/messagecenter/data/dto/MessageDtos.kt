package com.etrade.mobilepro.messagecenter.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "SecureMsgInboxResponse", strict = false)
class MessageListResponseDto(
    @field:ElementList(name = "MsgList")
    var messages: List<MessageDto?>? = null
)

@Root(strict = false, name = "Message")
class MessageDto(
    @field:Element(name = "MsgId", required = false)
    var msgId: String? = null,
    @field:Element(name = "DescId", required = false)
    var descId: String? = null,
    @field:Element(name = "UnreadFlag", required = false)
    var unreadFlag: String? = null,
    @field:Element(name = "CasetypeId", required = false)
    var casetypeId: String? = null,
    @field:Element(name = "NotifyEmail", required = false)
    var notifyEmail: String? = null,
    @field:Element(name = "ChatRefId", required = false)
    var chatRefId: String? = null,
    @field:Element(name = "StartTime", required = false)
    var startTime: String? = null,
    @field:Element(name = "WorkgroupId", required = false)
    var workgroupId: String? = null,
    @field:Element(name = "ReadTs", required = false)
    var readTs: String? = null,
    @field:Element(name = "CaseNo", required = false)
    var caseNo: String? = null,
    @field:Element(name = "FmtAccountId", required = false)
    var fmtAccountId: String? = null,
    @field:Element(name = "IsDtlAvail", required = false)
    var isDtlAvail: String? = null,
    @field:Element(name = "WebLogin", required = false)
    var webLogin: String? = null,
    @field:Element(name = "CasetypeDesc", required = false)
    var casetypeDesc: String? = null,
    @field:Element(name = "MsgDB", required = false)
    var msgDb: String? = null,
    @field:Element(name = "Status", required = false)
    var status: String? = null,
    @field:Element(name = "AccountId", required = false)
    var accountId: String? = null,
    @field:Element(name = "State", required = false)
    var state: String? = null,
    @field:Element(name = "SubjId", required = false)
    var subjId: String? = null,
    @field:Element(name = "EndTime", required = false)
    var endTime: String? = null,
    @field:Element(name = "NotifyMethod", required = false)
    var notifyMethod: String? = null,
    @field:Element(name = "CreationTs", required = false)
    var creationTs: String? = null
)
