package com.etrade.mobilepro.documents.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class TaxDocumentDto(
    @Json(name = "subDocType")
    val title: String,
    @Json(name = "reportDate")
    val reportDate: String,
    @Json(name = "amended")
    val amended: Boolean,
    @Json(name = "documentId")
    val documentId: String,
    @Json(name = "institutionType")
    val institutionType: String,
    @Json(name = "postEdgeFolder")
    val postEdgeFolder: String,
    @Json(name = "accountUuid")
    val accountUuid: String,
    @Json(name = "year")
    val year: String
)
