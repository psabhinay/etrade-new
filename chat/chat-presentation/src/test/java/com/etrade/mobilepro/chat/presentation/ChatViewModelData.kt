package com.etrade.mobilepro.chat.presentation

import com.etrade.mobilepro.chat.api.ChatGeolocationModel
import com.etrade.mobilepro.chat.api.ChatMetadataModel
import com.etrade.mobilepro.chat.api.ChatParamsDataModel
import com.etrade.mobilepro.chat.api.ChatParamsRequestModel
import com.etrade.mobilepro.chat.api.message.models.ChatForMobileModel
import com.etrade.mobilepro.chat.api.message.models.ChatInfoModel
import com.etrade.mobilepro.chat.api.message.models.MobileChatDataModel

internal const val TEST_ID = "98908983"
internal const val TEST_CHAT_CONTACT_ID = "12345678"
internal const val TEST_FIRST_NAME = "TEST_FIRST_NAME"
internal const val TEST_LAST_NAME = "TEST_LAST_NAME"
internal const val TEST_USER_NAME = "$TEST_FIRST_NAME $TEST_LAST_NAME"
internal const val TEST_ACC_NUM = "TEST_ACC_NUM"
internal const val TEST_ACC_DESCRIPTION = "TEST_ACC_DESCRIPTION"
internal const val TEST_ERROR_MESSAGE = "testErrorMessage"
internal const val TEST_GEOLOCATION_CITY = "New York"
internal const val TEST_GEOLOCATION_COUNTRY_CODE = "US"
internal const val TEST_BUTTON_ID = "TEST_BUTTON_ID"
internal const val TEST_SESSION_KEY = "TEST_SESSION_KEY"
internal const val TEST_MESSAGES = "TEST_MESSAGES"
internal const val TEST_SESSION_ID = "TEST_SESSION_ID"

internal val chatTestGeolocationModel
    get() = ChatGeolocationModel(countryCode = TEST_GEOLOCATION_COUNTRY_CODE, city = TEST_GEOLOCATION_CITY)

internal val chatTestMetadataModel
    get() = ChatMetadataModel(buttonId = TEST_BUTTON_ID, sessionKey = TEST_SESSION_KEY)

internal val chatTestRequestParams
    get() = ChatParamsRequestModel(
        ChatParamsDataModel(
            firstName = TEST_FIRST_NAME,
            lastName = TEST_LAST_NAME,
            productID = TEST_ID,
            contactID = TEST_CHAT_CONTACT_ID,
            accountNo = TEST_ACC_NUM,
            isOH = "false",
            isBusinessHrs = "true",
            isStockPlan = "false",
            metadata = chatTestMetadataModel,
            chatGeoLoc = chatTestGeolocationModel
        )
    )

internal val chatTestForMobileModel
    get() = ChatForMobileModel(
        data = MobileChatDataModel(
            chatInfoModel = ChatInfoModel(TEST_SESSION_ID),
            messages = TEST_MESSAGES
        ),
        success = true
    )
