package com.etrade.mobilepro.ui_demo.dagger

import com.etrade.mobilepro.ui_demo.UIDemoApplication
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityModule::class,
        ApplicationModule::class,
        FeatureFlagsModule::class,
        FragmentModule::class,
        ViewModelModule::class,
    ]
)
interface ApplicationComponent {

    fun inject(application: UIDemoApplication)
}
