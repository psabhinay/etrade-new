package com.etrade.mobilepro.login.widget

interface LoginResultDialog {

    var onButtonClickListener: ((LoginErrorCtaAction) -> Unit)?
}
