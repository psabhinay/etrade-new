package com.etrade.mobilepro.bank

import android.content.Context
import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.etrade.eo.core.util.forEachNotNull
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.bank.actions.BankActionsViewModel
import com.etrade.mobilepro.bank.daterangeselector.BankDateRangeSelector
import com.etrade.mobilepro.bank.transactions.BankTransactionDataDto
import com.etrade.mobilepro.bank.transactions.BankTransactionGenericLayout
import com.etrade.mobilepro.common.di.RemoteDynamicScreen
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.MessageType
import com.etrade.mobilepro.inboxmessages.addOrRemoveInboxMessagesView
import com.etrade.mobilepro.inboxmessages.dynamicui.api.InboxMessagesWidgetPlace
import com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt.rearrangeFundingPromptsToTheirAccountSummaries
import com.etrade.mobilepro.inboxmessages.inboxMessagesDynamicViewLiveData
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.livedata.concatCombine
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.tracking.SCREEN_TITLE_OVERVIEW
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.transactions.InstitutionType
import com.etrade.mobilepro.transactions.TransactionDetailInfo
import com.etrade.mobilepro.transactions.TransactionsResponseInfo
import com.etrade.mobilepro.transactions.TransactionsServiceProvider
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import com.etrade.mobilepro.util.safeParseBigDecimal
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import org.slf4j.LoggerFactory
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

private const val PREVIOUS_DAYS = 60L
private const val INVALID_PARAM = 20044

private typealias DateRange = ConsumableLiveEvent<Pair<LocalDate, LocalDate>>

class BankHomeViewModel @Inject constructor(
    @Web private val baseUrl: String,
    @RemoteDynamicScreen
    private val cachedDynamicScreenRepo: DynamicScreenRepo,
    imageLoader: ImageLoader,
    private val transactionsServiceProvider: TransactionsServiceProvider,
    private val resources: Resources,
    private val user: User,
    private val darkModeChecker: DarkModeChecker,
    inboxMessagesRepository: InboxMessagesRepository,
    dispatchers: DispatcherProvider,
    context: Context
) : ViewModel(), ScreenViewModel {
    override val screenName: String = SCREEN_TITLE_OVERVIEW

    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val emptyValue: String by lazy { resources.getString(R.string.empty_default) }

    private val emptyDate: String by lazy { resources.getString(R.string.empty_date_default) }

    private val _viewStates = MutableLiveData<ViewState>(ViewState.Loading)
    val viewState: LiveData<ViewState>
        get() = _viewStates

    private val compositeDisposable = CompositeDisposable()

    private var accountUuId: String? = null
    private var accountId: String? = null

    private val _startDate = MutableLiveData<LocalDate>().apply {
        value = LocalDate.now().minusDays(PREVIOUS_DAYS)
    }
    private val _endDate = MutableLiveData<LocalDate>().apply {
        value = LocalDate.now()
    }

    private val bankDateRangeSelector: BankDateRangeSelector by lazy { BankDateRangeSelector(resources, _startDate, _endDate) }

    private val inboxMessages = inboxMessagesDynamicViewLiveData(
        baseUrl = baseUrl,
        imageLoader = imageLoader,
        type = MessageType.DASHBOARD_INFORMATION,
        coroutineContext = dispatchers.IO,
        repository = inboxMessagesRepository,
        context = context
    )

    private val _dynamicWidgets = MutableLiveData<List<GenericLayout>>()

    private val _transactionsWidgets = MutableLiveData<List<GenericLayout>>().apply { value = emptyList() }

    val dateRangeSelection: LiveData<DateRange> = _startDate.combineLatest(_endDate).map { ConsumableLiveEvent(it) }

    val widgets: LiveData<List<GenericLayout>> = _dynamicWidgets
        .combineLatest(_transactionsWidgets)
        .map { getWidgetsWithDateRangeSelector(it.first, it.second) }.concatCombine(inboxMessages) { views, inboxMessagesPayload ->
            views.addOrRemoveInboxMessagesView(view = inboxMessagesPayload.view)
        }

    private fun getBankAccountDynamicScreen(accountUuId: String, accountId: String) {
        _viewStates.value = ViewState.Loading
        compositeDisposable.clear()
        val screen = cachedDynamicScreenRepo.getScreen(
            ServicePath.BankAccountOverView(
                accountUuid = accountUuId,
                accountId = accountId,
                startDate = _startDate.formatDate(),
                endDate = _endDate.formatDate()
            )
        )
        val disposable = subscribeToScreenChanges(screen)
        compositeDisposable.add(disposable)
    }

    private fun subscribeToScreenChanges(screen: Observable<Resource<List<GenericLayout>>>): Disposable {
        return screen.subscribeBy(
            onNext = { response ->
                val screens = response.data ?: emptyList()

                when (response) {
                    is Resource.Success -> {
                        _viewStates.postValue(ViewState.Success(uiMode = darkModeChecker.uiMode))
                        _dynamicWidgets.postValue(screens.rearrangeFundingPromptsToTheirAccountSummaries(InboxMessagesWidgetPlace.OVERVIEW))
                    }
                    is Resource.Loading -> _viewStates.postValue(ViewState.Loading)
                    is Resource.Failed -> _viewStates.postValue(ViewState.Error)
                }
            },
            onError = { onError("Could not load bank home screen", it) }
        )
    }

    fun fetchScreen(accountId: String, accountUuId: String, forceRefresh: Boolean = false) {
        if (this.accountUuId != accountUuId || forceRefresh) {
            getBankAccountDynamicScreen(accountUuId, accountId)
        }
        this.accountUuId = accountUuId
        this.accountId = accountId
    }

    fun updateStartDate(date: LocalDate) {
        _startDate.value = date
    }

    fun updateEndDate(date: LocalDate) {
        _endDate.value = date
    }

    // TODO: Remove this Implementation when dynamic service supports transaction.
    fun getTransactions(accountId: Long, startDate: LocalDate, endDate: LocalDate) {
        val userId = user.getUserId()
        if (userId == null) {
            onError("Invalid User ID")
            return
        }

        compositeDisposable.add(
            transactionsServiceProvider.getTransactions(userId, accountId, InstitutionType.TELEBANK, startDate, endDate).subscribeBy(
                onSuccess = ::onSuccess,
                onError = { onError("TransactionsViewModel", it) }
            )
        )
    }

    private fun onSuccess(response: TransactionsResponseInfo) {
        if (response.errors.isNotEmpty()) {
            response.errors.forEachNotNull { errorInfo ->
                when (errorInfo.errorCode) {
                    INVALID_PARAM -> return@forEachNotNull
                    else -> {
                        onError("Service call failed: ${errorInfo.errorDescription}")
                        return@forEachNotNull
                    }
                }
            }
        } else {
            _transactionsWidgets.postValue(response.transactions.map { toBankTransactionsView(it) })
        }
    }

    private fun onError(errorMsg: String, throwable: Throwable? = null) {
        throwable?.run { logger.error(errorMsg, this) } ?: logger.error(errorMsg)
        _viewStates.postValue(ViewState.Error)
    }

    private fun toBankTransactionsView(it: TransactionDetailInfo): BankTransactionGenericLayout {
        return BankTransactionGenericLayout(
            BankTransactionDataDto(
                transactionType = "${it.type}",
                transactionAmount = it.amount?.safeParseBigDecimal(),
                transactionDescription = "${it.description}",
                transactionDate = it.date
            ),
            emptyValue, resources.getString(R.string.accessibility_empty_amount_description), emptyDate
        )
    }

    private fun getWidgetsWithDateRangeSelector(dynamicScreens: List<GenericLayout>, transactions: List<GenericLayout>): List<GenericLayout> {
        return mutableListOf<GenericLayout>().apply {
            addAll(dynamicScreens)
            // TODO: Remove this line when dynamic service supports Bank account cta
            add(createPlaceHolderBankCtaView())
            add(bankDateRangeSelector)
            // TODO: Remove explicitly loading transactions once dynamic service supports Bank account transactions
            addAll(transactions)
        }
    }

    private fun createPlaceHolderBankCtaView(): GenericLayout {
        return object : GenericLayout {
            override val viewModelFactory = viewModelFactory {
                BankActionsViewModel(accountId)
            }
            override val viewModelClass = BankActionsViewModel::class.java

            override val uniqueIdentifier: String
                get() = "${javaClass.name}_$accountId"
        }
    }

    sealed class ViewState {
        object Loading : ViewState()

        object Error : ViewState()

        data class Success(val uiMode: Int) : ViewState()
    }
}

private val LOCALDATE_DISPLAYED_DATE_FORMAT: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")

private fun LiveData<LocalDate>.formatDate(): String = this.value?.format(LOCALDATE_DISPLAYED_DATE_FORMAT) ?: ""
