package com.etrade.mobilepro.portfolio

import android.view.View
import androidx.core.view.setPadding
import androidx.core.widget.ContentLoadingProgressBar
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.binding.bindRowWidthPercentage
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.portfolio.model.PageLoadingState
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableData
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableRow
import com.etrade.mobilepro.portfolio.tableview.PortfolioTableViewAdapter
import com.etrade.mobilepro.portfolio.taxlot.TaxLotTableViewAdapter
import com.etrade.mobilepro.tableviewutils.presentation.RestrictedTableView
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.tableviewutils.presentation.getVisibleSymbols
import com.etrade.mobilepro.util.android.extension.getFloat
import com.etrade.mobilepro.util.android.goneIf
import com.evrencoskun.tableview.ITableView
import com.evrencoskun.tableview.TableView

@BindingAdapter("pageLoadingState")
fun bindPageLoadingState(view: ContentLoadingProgressBar, pageLoadingState: PageLoadingState?) {
    if (pageLoadingState == PageLoadingState.Loading) {
        view.show()
    } else {
        view.hide()
    }
}

@BindingAdapter("pageLoadingState")
fun bindPageLoadingState(view: TableView, pageLoadingState: PageLoadingState?) {
    view.isIncompleteTableContent = pageLoadingState is PageLoadingState.Loading ||
        if (pageLoadingState is PageLoadingState.Idle) {
            pageLoadingState.hasNextPage
        } else {
            false
        }
}

@BindingAdapter(
    requireAll = false,
    value = ["portfolioTableData", "portfolioTableViewScrollListener", "portfolioPositionsDisplayMode", "portfolioSortViewState"]
)
fun RestrictedTableView.bindPortfolioTableData(
    data: PortfolioRecordsTableData?,
    listener: PortfolioTableViewScrollListener?,
    mode: PortfolioPositionsDisplayMode?,
    sortState: SortStateInfo?
) {
    val viewAdapter = adapter
    if (listener == null || mode == null && viewAdapter == null) {
        return
    }
    setSortStateForPortfolioTableViewAdapter(sortState)
    setupTableView(mode, data, listener)
}

private fun RestrictedTableView.setupTableView(
    mode: PortfolioPositionsDisplayMode?,
    data: PortfolioRecordsTableData?,
    listener: PortfolioTableViewScrollListener
) {
    when (mode) {
        PortfolioPositionsDisplayMode.TABLE -> {
            ensureTableAdapter(data)
            setTableModeListeners(listener)
        }
        PortfolioPositionsDisplayMode.LIST -> {
            ensureListAdapter(data)
            setNonTableModeListeners(listener, PortfolioPositionsDisplayMode.LIST)
        }
        PortfolioPositionsDisplayMode.TILE -> {
            ensureTileAdapter(data)
            setNonTableModeListeners(listener, PortfolioPositionsDisplayMode.TILE)
        }
    }
    (tableViewListener as? PortfolioTableViewListener)?.displayMode = mode
}

private fun RestrictedTableView.setSortStateForPortfolioTableViewAdapter(
    sortState: SortStateInfo?
) {
    val ensuredAdapter = adapter
    if (ensuredAdapter is PortfolioTableViewAdapter) {
        ensuredAdapter.sortState = sortState ?: SortStateInfo()
    }
}

@BindingAdapter(value = ["taxLotTableData", "taxLotSortState"])
fun RestrictedTableView.bindTaxLotTableData(data: PortfolioRecordsTableData?, sortState: SortStateInfo?) {
    data?.let {
        (adapter as TaxLotTableViewAdapter).data = it
    }
    setSortStateForPortfolioTableViewAdapter(sortState)
}

private fun TableView.removeAllListeners() {
    rowHeaderRecyclerView.removeListeners()
    cellRecyclerView.removeListeners()
}

private fun RecyclerView.removeListeners() {
    (tag as? Pair<*, *>)?.let {
        removeOnLayoutChangeListener(it.first as View.OnLayoutChangeListener)
        removeOnScrollListener(it.second as RecyclerView.OnScrollListener)
    }
}

private fun TableView.setTableModeListeners(listener: PortfolioTableViewScrollListener) {
    removeAllListeners()
    val onLayoutChangedListener = createOnLayoutChangeListener {
        listener.onVisibleItemsChanged(getVisibleSymbols())
    }
    val onScrollListener = createOnScrollListener(listener, PortfolioPositionsDisplayMode.TABLE)

    rowHeaderRecyclerView.applyListeners(onLayoutChangedListener, onScrollListener)
}

private fun TableView.setNonTableModeListeners(
    listener: PortfolioTableViewScrollListener,
    displayMode: PortfolioPositionsDisplayMode
) {
    removeAllListeners()
    val onLayoutChangedListener = createOnLayoutChangeListener {
        listener.onVisibleItemsChanged(getVisibleListSymbols(displayMode))
    }
    val onScrollListener = createOnScrollListener(listener, displayMode)

    cellRecyclerView.applyListeners(onLayoutChangedListener, onScrollListener)
}

private fun RecyclerView.applyListeners(
    onLayoutChangedListener: View.OnLayoutChangeListener,
    onScrollListener: RecyclerView.OnScrollListener
) {
    addOnLayoutChangeListener(onLayoutChangedListener)
    addOnScrollListener(onScrollListener)
    // Save this listeners to tag so that we can remove them properly after the view has been recycled.
    tag = onLayoutChangedListener to onScrollListener
}

private fun createOnLayoutChangeListener(block: () -> Unit) =
    View.OnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
        block.invoke()
    }

private fun ITableView.getVisibleListSymbols(mode: PortfolioPositionsDisplayMode): Set<Symbol> {
    val rowLayoutManager =
        cellRecyclerView.layoutManager as? LinearLayoutManager ?: return emptySet()
    val firstVisiblePosition = rowLayoutManager.findFirstVisibleItemPosition()
    val lastVisiblePosition = rowLayoutManager.findLastVisibleItemPosition()

    val rowAdapter = adapter.cellRecyclerViewAdapter

    if (rowAdapter == null ||
        firstVisiblePosition == RecyclerView.NO_POSITION ||
        lastVisiblePosition == RecyclerView.NO_POSITION
    ) {
        return emptySet()
    }

    val itemsList = rowAdapter.items.subList(firstVisiblePosition, lastVisiblePosition + 1)
    return if (mode == PortfolioPositionsDisplayMode.TILE) {
        itemsList.flatten()
    } else {
        itemsList.map { it.first() }
    }.map {
        val row = it as PortfolioRecordsTableRow
        Symbol(row.rowHeader.symbol, row.rowHeader.instrumentType)
    }.toSet()
}

private fun TableView.createOnScrollListener(
    listener: PortfolioTableViewScrollListener,
    mode: PortfolioPositionsDisplayMode
) =
    object : RecyclerView.OnScrollListener() {

        private var previousFirstVisibleItemPosition: Int = RecyclerView.NO_POSITION
        private var previousLastVisibleItemPosition: Int = RecyclerView.NO_POSITION

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            val layoutManager = recyclerView.layoutManager as LinearLayoutManager
            val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
            val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()

            if (previousLastVisibleItemPosition != lastVisibleItemPosition) {
                previousLastVisibleItemPosition = lastVisibleItemPosition
                listener.onLastVisibleItemPositionChanged(lastVisibleItemPosition)

                if (previousFirstVisibleItemPosition != firstVisibleItemPosition) {
                    previousFirstVisibleItemPosition = firstVisibleItemPosition
                    val visibleSymbols = when (mode) {
                        PortfolioPositionsDisplayMode.TABLE -> getVisibleSymbols(
                            firstVisibleItemPosition,
                            lastVisibleItemPosition
                        )
                        else -> getVisibleListSymbols(mode)
                    }

                    listener.onVisibleItemsChanged(visibleSymbols)
                }
            }
        }
    }

@BindingAdapter(value = ["portfolioTableGoneIf", "positionsExpanded"])
fun bindPortfolioTableGoneIf(view: TableView, gone: Boolean, positionsExpanded: Boolean?) {
    view.apply {
        val calculatedGone = positionsExpanded == false || gone
        cornerContainerView.goneIf(calculatedGone)
        rowHeaderRecyclerView.goneIf(calculatedGone)
        cellRecyclerView.goneIf(calculatedGone)
        columnHeaderRecyclerView.goneIf(calculatedGone)
    }
}

@BindingAdapter("isTableSortable")
fun bindIsTableSortable(view: TableView, isSortable: Boolean?) {
    view.isSortable = isSortable ?: false
}

private fun RestrictedTableView.ensureTableAdapter(data: PortfolioRecordsTableData? = null) {
    val tableAdapter: PortfolioTableViewAdapter = if (adapter is PortfolioTableViewAdapter) {
        adapter as PortfolioTableViewAdapter
    } else {
        applyTableViewParams()
        val newAdapter = PortfolioTableViewAdapter(context)
        adapter = newAdapter
        newAdapter
    }
    data?.let { tableAdapter.data = it }
}

private fun RestrictedTableView.ensureListAdapter(data: PortfolioRecordsTableData? = null) {
    val listAdapter: PortfolioListViewAdapter = if (adapter is PortfolioListViewAdapter) {
        adapter as PortfolioListViewAdapter
    } else {
        applyListViewParams()
        val newAdapter = PortfolioListViewAdapter(context)
        adapter = newAdapter
        newAdapter
    }
    data?.let { listAdapter.data = it }
}

private fun RestrictedTableView.ensureTileAdapter(data: PortfolioRecordsTableData? = null) {
    val tileAdapter: PortfolioTileViewAdapter = if (adapter is PortfolioTileViewAdapter) {
        adapter as PortfolioTileViewAdapter
    } else {
        applyTileViewParams()
        val newAdapter = PortfolioTileViewAdapter(context)
        adapter = newAdapter
        newAdapter
    }
    data?.let { tileAdapter.data = it }
}

private fun RestrictedTableView.applyTileViewParams() {
    isShowHorizontalSeparators = false
    isShowVerticalSeparators = false
    rowHeaderWidth = 0
    columnHeaderHeight = 0
    setPadding(resources.getDimensionPixelSize(R.dimen.spacing_small12))
}

private fun RestrictedTableView.applyTableViewParams() {
    isShowHorizontalSeparators = false
    isShowVerticalSeparators = true
    bindRowWidthPercentage(this, context.getFloat(R.dimen.default_tableview_row_header_width))
    columnHeaderHeight =
        context.resources.getDimensionPixelOffset(R.dimen.default_column_header_height)
    setPadding(0)
}

private fun RestrictedTableView.applyListViewParams() {
    isShowHorizontalSeparators = true
    isShowVerticalSeparators = true
    rowHeaderWidth = 0
    columnHeaderHeight = 0
    setPadding(0)
}
