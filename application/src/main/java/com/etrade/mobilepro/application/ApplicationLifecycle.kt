package com.etrade.mobilepro.application

import android.app.Activity

interface ApplicationLifecycle {
    fun onUserInteraction(activity: Activity)
}
