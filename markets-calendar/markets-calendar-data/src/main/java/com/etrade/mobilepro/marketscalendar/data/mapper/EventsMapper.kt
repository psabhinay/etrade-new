package com.etrade.mobilepro.marketscalendar.data.mapper

import com.etrade.mobilepro.marketscalendar.api.model.EventsEarnings
import com.etrade.mobilepro.marketscalendar.api.model.EventsEconomic
import com.etrade.mobilepro.marketscalendar.api.model.EventsRequest
import com.etrade.mobilepro.marketscalendar.api.model.EventsResponse
import com.etrade.mobilepro.marketscalendar.api.model.EventsResponseData
import com.etrade.mobilepro.marketscalendar.api.model.EventsResponseDm
import com.etrade.mobilepro.marketscalendar.api.model.EventsSummaryResponse
import com.etrade.mobilepro.marketscalendar.data.dto.ALL_SYMBOLS_EVENTS
import com.etrade.mobilepro.marketscalendar.data.dto.EventsEarningsData
import com.etrade.mobilepro.marketscalendar.data.dto.EventsEconomicData
import com.etrade.mobilepro.marketscalendar.data.dto.EventsRequestBody
import com.etrade.mobilepro.marketscalendar.data.dto.EventsResponseDto
import com.etrade.mobilepro.marketscalendar.data.dto.EventsSummaryResponseDto
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalTime

fun EventsResponseDto<EventsEarningsData>.mapToEventsEarningsResponse(): EventsResponse<EventsEarnings> {
    return EventsResponse(
        eventsResponseData = EventsResponseData(
            eventsResponseDm = EventsResponseDm(
                eventsDataList = this.dataDto?.eventsResponseDm?.eventsDataList?.map {
                    EventsEarnings(
                        symbol = it.symbol,
                        epsestMean = it.epsestMean,
                        epsactualValue = it.epsactualValue,
                        dateTime = it.dateTime,
                        epsannounceDate = it.epsannounceDate,
                        epsannounceTime = it.epsannounceTime
                    )
                }
            )
        )
    )
}

fun EventsResponseDto<EventsEconomicData>.mapToEventsEconomicResponse(): EventsResponse<EventsEconomic> {
    return EventsResponse(
        eventsResponseData = EventsResponseData(
            eventsResponseDm = EventsResponseDm(
                eventsDataList = this.dataDto?.eventsResponseDm?.eventsDataList?.map {
                    EventsEconomic(
                        ecEcondetName = it.ecEcondetName,
                        ecEcondetConcensus = it.ecEcondetConcensus,
                        ecEcondetActual = it.ecEcondetActual,
                        ecSeStartdate = it.ecSeStartdate,
                        ecEconDesc = it.ecEconDesc,
                        ecEconDefinition = it.ecEconDefinition,
                        ecEconName = it.ecEconName
                    )
                }
            )
        )
    )
}

internal fun EventsRequest.mapToEventsRequestBody(): EventsRequestBody {
    val startDate = eventsRequestValue.eventsRequestDateRange.startDate
    val endDate = eventsRequestValue.eventsRequestDateRange.endDate
    return EventsRequestBody(
        EventsRequestBody.Value(
            dateRangeSet = listOf(
                EventsRequestBody.DateRange(
                    getDateForEventsRequestBody(startDate, LocalTime.MIN.hour, LocalTime.MIN.minute, LocalTime.MIN.second),
                    getDateForEventsRequestBody(endDate, LocalTime.MAX.hour, LocalTime.MAX.minute, LocalTime.MAX.second)
                )
            ),
            symbolSet = listOf(ALL_SYMBOLS_EVENTS),
            eventFilterEnable = eventsRequestValue.eventFilterEnable,
            eventTypeSet = eventsRequestValue.eventTypeSet
        )
    )
}

internal fun EventsSummaryResponseDto.mapToEventsSummaryResponse(): EventsSummaryResponse {
    val everySummaryData = this.data?.eventsSummaryResponseDm?.eventsSummaryDataList?.map {
        EventsSummaryResponse.EventsSummary(it.dateTime)
    }
    return EventsSummaryResponse(
        eventsSummaryResponseData = EventsSummaryResponse.EventsSummaryResponseData(
            eventsSummaryResponseDm = EventsSummaryResponse.EventsSummaryResponseDm(eventsSummaryList = everySummaryData)
        )
    )
}

private fun getDateForEventsRequestBody(
    date: LocalDate,
    hourOfDay: Int,
    minute: Int,
    second: Int
): EventsRequestBody.Date = EventsRequestBody.Date(
    year = date.year,
    month = date.monthValue,
    dayOfMonth = date.dayOfMonth,
    hourOfDay = hourOfDay,
    minute = minute,
    second = second
)
