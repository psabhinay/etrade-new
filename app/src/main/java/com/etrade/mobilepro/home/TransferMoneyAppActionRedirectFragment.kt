package com.etrade.mobilepro.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.userviewmodel.UserViewModel
import javax.inject.Inject

const val KEY_FROM_ACCOUNT = "fromAccount"
const val KEY_AMOUNT = "amount"

@RequireLogin
class TransferMoneyAppActionRedirectFragment @Inject constructor(
    private val userViewModel: UserViewModel
) : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        findNavController().popBackStack(R.id.transferMoneyAppActionRedirectFragment, true)
        if (userViewModel.isAuthenticated) {
            val fromAccount = arguments?.get(KEY_FROM_ACCOUNT) as? String
            val amount = arguments?.get(KEY_AMOUNT) as? String
            findNavController().navigate(MainGraphDirections.actionLaunchTransferMoney(fromAccount, amount))
        } else {
            findNavController().navigate(R.id.accountsHomeFragment)
        }
    }
}
