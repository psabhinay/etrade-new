package com.etrade.mobilepro.transfermoney.presentation.transfer

import com.etrade.mobilepro.transfermoney.presentation.preview.ParcelableGetQuickTransferPreviewRequest

sealed class TransferMoneyPreviewButtonState {

    data class Enabled(val request: ParcelableGetQuickTransferPreviewRequest) : TransferMoneyPreviewButtonState()

    object Disabled : TransferMoneyPreviewButtonState()
}
