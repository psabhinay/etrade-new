package com.etrade.mobilepro.screener.presentation.mutualfund.result

import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.screener.api.ScreenerDisclosure
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseScreenerResultFragment
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class MutualFundResultsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    router: MainNavigation,
    private val screenersRouter: ScreenersRouter
) : BaseScreenerResultFragment(snackbarUtilFactory, router) {
    override val viewModel by viewModels<MutualFundResultsViewModel> { viewModelFactory }

    override val disclosureText: String? get() = getString(R.string.disclosure_mutual_fund_result_inline)

    override val resultsDisclosureClickListener: () -> Unit = {
        screenersRouter.actionResultsToDisclosure(findNavController(), ScreenerDisclosure.MUTUAL_FUND_RESULTS)
    }
}
