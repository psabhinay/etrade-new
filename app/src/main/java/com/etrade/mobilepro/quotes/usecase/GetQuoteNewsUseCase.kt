package com.etrade.mobilepro.quotes.usecase

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.api.repo.NewsRepo
import com.etrade.mobilepro.quoteapi.GetQuoteParameter
import com.etrade.mobilepro.util.UseCase
import kotlinx.coroutines.rx2.awaitLast
import javax.inject.Inject

interface GetQuoteNewsUseCase : UseCase<GetQuoteParameter, ETResult<List<NewsItem.Text>>>

class GetQuoteNewsUseCaseImpl @Inject constructor(
    private val repo: NewsRepo
) : GetQuoteNewsUseCase {

    override suspend fun execute(parameter: GetQuoteParameter): ETResult<List<NewsItem.Text>> = runCatchingET {
        with(parameter.symbol) {
            when {
                type.isMutualFundOrMoneyMarketFund -> emptyList()
                type.isIndex -> repo.marketNews().awaitLast()
                else -> repo.symbolNews(NewsRequest.Symbol(listOf(underlyerSymbol))).awaitLast()
            }
        }
    }
}
