package com.etrade.mobilepro.tracking.params

private const val EVENT_MARKETS_INDICES = "Markets 2.0 Indices"
private const val EVENT_MARKERS_BROADER_INDICES = "Markets 2.0 Broader Indices"
private const val EVENT_MARKETS_BLOOMBERG_TV = "Markets 2.0 Bloomberg TV"
private const val EVENT_MARKETS_TODAY_TOP_EARNINGS = "Markets 2.0 Today's Top Earnings"
private const val EVENT_MARKETS_CUSTOMIZE = "Markets 2.0 Customize"

sealed class MarketsTapEventAnalyticsParams(
    override val name: String
) : AnalyticsParams {

    override val params: Map<String, String> = emptyMap()
}

class MarketsIndicesTapEventAnalyticsParams : MarketsTapEventAnalyticsParams(EVENT_MARKETS_INDICES)
class MarketsBroaderIndicesTapEventAnalyticsParams : MarketsTapEventAnalyticsParams(EVENT_MARKERS_BROADER_INDICES)
class MarketsBloombergTvEventAnalyticsParams : MarketsTapEventAnalyticsParams(EVENT_MARKETS_BLOOMBERG_TV)
class MarketsTodayTopEarningsEventAnalyticsParams : MarketsTapEventAnalyticsParams(EVENT_MARKETS_TODAY_TOP_EARNINGS)
class MarketsCustomizeEventAnalyticsParams : MarketsTapEventAnalyticsParams(EVENT_MARKETS_CUSTOMIZE)
