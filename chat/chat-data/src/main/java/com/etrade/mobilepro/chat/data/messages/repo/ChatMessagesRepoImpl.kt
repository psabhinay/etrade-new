package com.etrade.mobilepro.chat.data.messages.repo

import com.etrade.mobilepro.chat.api.ChatParamsRequestModel
import com.etrade.mobilepro.chat.api.message.models.ChatForMobileModel
import com.etrade.mobilepro.chat.api.message.models.ChatRequestModel
import com.etrade.mobilepro.chat.api.message.models.ChatRetrievedMessageModel
import com.etrade.mobilepro.chat.api.message.repo.ChatMessagesRepo
import com.etrade.mobilepro.chat.data.common.toChatForMobileModel
import com.etrade.mobilepro.chat.data.common.toRetrievedMessageModel
import com.etrade.mobilepro.chat.data.messages.service.ChatMessagesApiService
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import javax.inject.Inject

class ChatMessagesRepoImpl @Inject constructor(
    private val apiService: ChatMessagesApiService,
    private val chatRequestBodyManager: ChatRequestBodyManager
) : ChatMessagesRepo {

    private var ackMessageSequenceCount: Int = -1

    override suspend fun getMessages(chatRequestModel: ChatRequestModel.GetMessages): ETResult<ChatRetrievedMessageModel> =
        runCatchingET {
            val requestBody = chatRequestBodyManager.createRetrievedMessageBody(chatRequestModel, ackMessageSequenceCount)
            apiService.retrieveMessages(requestBody).toRetrievedMessageModel().also {
                ackMessageSequenceCount = it.sequence ?: ackMessageSequenceCount
            }
        }

    override suspend fun getMobileChat(chatRequestModel: ChatParamsRequestModel): ETResult<ChatForMobileModel> =
        runCatchingET {
            val body = chatRequestBodyManager.createMobileChatBody(chatRequestModel)
            apiService.getChatForMobile(body).toChatForMobileModel()
        }

    override suspend fun postMessage(chatRequestModel: ChatRequestModel.PostMessage): ETResult<ChatForMobileModel> =
        runCatchingET {
            val body = chatRequestBodyManager.createPostMessageBody(chatRequestModel)
            apiService.postChat(body).toChatForMobileModel()
        }

    override suspend fun terminateChat(chatRequestModel: ChatRequestModel.TerminateChat): ETResult<ChatForMobileModel> =
        runCatchingET {
            ackMessageSequenceCount = -1
            val body = chatRequestBodyManager.createTerminateChatBody(chatRequestModel)
            apiService.terminateChat(body).toChatForMobileModel()
        }
}
