package com.etrade.mobilepro.msaccounts.presentation

import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel

class MsAccountsErrorViewModel : DynamicViewModel(R.layout.ms_accounts_error) {

    override val variableId: Int = BR.obj

    fun onRetryClicked() {
        logger.debug("onRetryClicked")
        clickEvents.postValue(CtaEvent.RetryEvent)
    }
}
