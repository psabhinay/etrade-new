package com.etrade.mobilepro.streaming.lightstreamer

/**
 * Maps a field to required set of fields.
 */
interface LightstreamerFieldMapper<T> {

    /**
     * Maps a [field] to corresponding set of fields.
     *
     * @param field The field to map.
     * @param isOption The flag indicating if fields are required for an option symbol.
     *
     * @return The set of fields that are required to represent the [field].
     */
    fun map(field: T, isOption: Boolean): Set<String>
}

/**
 * Maps a set [fields] to corresponding set of fields.
 *
 * @param fields The set of fields to map.
 * @param isOption The flag indicating if fields are required for an option symbol.
 *
 * @return The set of fields that are required to represent the [fields].
 */
fun <T> LightstreamerFieldMapper<T>.map(fields: Set<T>, isOption: Boolean): Set<String> = fields.flatMap { map(it, isOption) }.toSet()
