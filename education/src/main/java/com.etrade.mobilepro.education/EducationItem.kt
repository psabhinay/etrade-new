package com.etrade.mobilepro.education

private const val LEVEL_BASIC = 0
private const val LEVEL_INTERMEDIATE = 1
private const val LEVEL_ADVANCED = 2

private const val LEVEL_TEXT_BASIC = "Basic"
private const val LEVEL_TEXT_INTERMEDIATE = "Intermediate"
private const val LEVEL_TEXT_ADVANCED = "Advanced"

private const val INDEX_TITLE = 2
private const val INDEX_TOPIC = 0
private const val INDEX_AUTHOR = 3
private const val INDEX_LENGTH = 4
private const val INDEX_DESCRIPTION = 5
private const val INDEX_LEVEL = 1
private const val INDEX_VIDEO_ID = 6

private const val QUOTE = "\""

class EducationItem : Comparable<EducationItem> {

    lateinit var title: String
    lateinit var topic: String
    lateinit var author: String
    lateinit var length: String
    lateinit var description: String
    lateinit var level: String
    lateinit var videoId: String

    fun map(line: String, regex: Regex): EducationItem {
        val values: List<String> = line.split(regex)

        title = values[INDEX_TITLE]
        topic = values[INDEX_TOPIC]
        author = removeSurroundingQuotes(values[INDEX_AUTHOR])
        length = formatVideoLength(values[INDEX_LENGTH])
        description = removeSurroundingQuotes(values[INDEX_DESCRIPTION])
        level = values[INDEX_LEVEL]
        videoId = values[INDEX_VIDEO_ID]

        return this
    }

    private fun removeSurroundingQuotes(value: String): String {
        return if (value.startsWith(QUOTE) && value.endsWith(QUOTE)) {
            value.drop(1).dropLast(1)
        } else {
            value
        }
    }

    private fun formatVideoLength(value: String): String = " - $value"

    private val levelOrder: Int by lazy {
        when (level) {
            LEVEL_TEXT_BASIC -> LEVEL_BASIC
            LEVEL_TEXT_INTERMEDIATE -> LEVEL_INTERMEDIATE
            LEVEL_TEXT_ADVANCED -> LEVEL_ADVANCED

            else -> throw IllegalArgumentException("Unknown level \"$level\" for education item")
        }
    }

    override fun compareTo(other: EducationItem): Int = levelOrder - other.levelOrder
}
