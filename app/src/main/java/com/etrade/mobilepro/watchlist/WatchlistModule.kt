package com.etrade.mobilepro.watchlist

import android.content.res.Resources
import com.etrade.mobilepro.R
import com.etrade.mobilepro.appwidget.AppWidgetHandler
import com.etrade.mobilepro.appwidget.api.WidgetViewStateRepo
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.executeBlocking
import com.etrade.mobilepro.watchlist.delegates.AddToWatchListDelegate
import com.etrade.mobilepro.watchlist.presentation.WatchlistColumnRepoImpl
import com.etrade.mobilepro.watchlist.repo.CombinedWatchlistRepo
import com.etrade.mobilepro.watchlist.repo.DefaultWatchlistRepo
import com.etrade.mobilepro.watchlist.repo.DeviceWatchlistRepo
import com.etrade.mobilepro.watchlist.repo.SetHasAddedToWatchlistUseCase
import com.etrade.mobilepro.watchlist.rest.WatchlistApiClient
import com.etrade.mobilepro.watchlist.usecase.DefaultWatchlistNullStateUseCase
import com.etrade.mobilepro.watchlist.usecase.GetWatchlistEntriesUseCase
import com.etrade.mobilepro.watchlist.usecase.GetWatchlistEntriesUseCaseImpl
import com.etrade.mobilepro.watchlist.usecase.ResolveMutualFundTimestampUseCase
import com.etrade.mobilepro.watchlist.usecase.ResolveMutualFundTimestampUseCaseImpl
import com.etrade.mobilepro.watchlist.usecase.ResolveTimestampUseCase
import com.etrade.mobilepro.watchlist.usecase.ResolveTimestampUseCaseImpl
import com.etrade.mobilepro.watchlist.usecase.WatchlistNullStateUseCase
import com.etrade.mobilepro.watchlistapi.WatchlistEvent
import com.etrade.mobilepro.watchlistapi.WatchlistEventListener
import com.etrade.mobilepro.watchlistapi.WatchlistEventNotifier
import com.etrade.mobilepro.watchlistapi.qualifiers.DefaultWatchlistRepository
import com.etrade.mobilepro.watchlistapi.qualifiers.DeviceWatchlistRepository
import com.etrade.mobilepro.watchlistapi.qualifiers.HasAddedToWatchlist
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnRepo
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import dagger.Binds
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.runBlocking
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
abstract class WatchlistModule {

    @Binds
    abstract fun provideAddWatchListDelegate(impl: AddToWatchListDelegateImpl): AddToWatchListDelegate

    @Binds
    abstract fun bindWatchlistNullStateUseCase(impl: DefaultWatchlistNullStateUseCase): WatchlistNullStateUseCase

    @Binds
    @Singleton
    abstract fun bindResolveTimestampUseCase(impl: ResolveTimestampUseCaseImpl): ResolveTimestampUseCase

    @Binds
    abstract fun bindSetHasAddedToWatchlistUseCase(impl: DefaultSetHasAddedToWatchlistUseCase): SetHasAddedToWatchlistUseCase

    @Binds
    @Singleton
    abstract fun bindWatchlistColumnRepo(impl: WatchlistColumnRepoImpl): WatchlistColumnRepo

    @Binds
    @Singleton
    @DefaultWatchlistRepository
    abstract fun bindDefaultWatchlistRepo(impl: DefaultWatchlistRepo): WatchlistRepo

    @Binds
    @Singleton
    @DeviceWatchlistRepository
    abstract fun bindDeviceWatchlistRepo(impl: DeviceWatchlistRepo): WatchlistRepo

    companion object {

        @Provides
        @Singleton
        internal fun provideGetWatchlistEntriesUseCase(
            watchlistRepo: WatchlistRepo,
            isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase
        ): GetWatchlistEntriesUseCase = GetWatchlistEntriesUseCaseImpl(watchlistRepo) { isExtendedHoursOnUseCase.executeBlocking() }

        @Provides
        @Singleton
        internal fun provideResolveMutualFundTimestampUseCase(
            resources: Resources
        ): ResolveMutualFundTimestampUseCase = ResolveMutualFundTimestampUseCaseImpl { resources.getString(R.string.mf_time_stamp, it) }

        @Provides
        @Singleton
        internal fun provideWatchlistApiClient(@MobileTrade retrofit: Retrofit): WatchlistApiClient = retrofit.create(WatchlistApiClient::class.java)

        @Suppress("LongParameterList")
        @Provides
        @Singleton
        internal fun provideWatchlistEventNotifier(
            appWidgetHandler: AppWidgetHandler,
            widgetViewStateRepo: WidgetViewStateRepo,
            watchlistRepo: CombinedWatchlistRepo
        ): WatchlistEventNotifier {
            val watchlistEventListener = object : WatchlistEventListener {
                override fun onWatchlistEvent(event: WatchlistEvent) {
                    runBlocking {
                        if (widgetViewStateRepo.isWidgetPlaced()) { appWidgetHandler.refresh() }
                    }
                }
            }
            return WatchlistEventNotifierImpl(watchlistRepo)
                .also { it.addListener(watchlistEventListener) }
        }

        @Provides
        @Singleton
        internal fun provideCombinedWatchlistRepo(
            @DeviceWatchlistRepository deviceWatchlistRepo: WatchlistRepo,
            @DefaultWatchlistRepository defaultWatchlistRepo: WatchlistRepo,
            userViewModel: UserViewModel,
            hasAddedToWatchlistUseCase: SetHasAddedToWatchlistUseCase
        ): CombinedWatchlistRepo =
            CombinedWatchlistRepo(deviceWatchlistRepo, defaultWatchlistRepo, hasAddedToWatchlistUseCase) { userViewModel.isAuthenticated }

        @Provides
        @Singleton
        internal fun provideWatchlistRepo(
            watchlistEventNotifier: WatchlistEventNotifier
        ): WatchlistRepo {
            // Always expecting an instance of [WatchlistEventNotifierImpl] which implements [WatchlistRepo]
            return watchlistEventNotifier as WatchlistRepo
        }

        @Provides
        @HasAddedToWatchlist
        fun provideHasAddedToWatchlist(userPreferences: UserPreferences): Boolean {
            return userPreferences.getHasAddedToWatchlist()
        }
    }
}
