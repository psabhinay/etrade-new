package com.etrade.mobilepro.dropdown.multiselect

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.lifecycle.Lifecycle.State.STARTED
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenStateAtLeast
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.common.Navigation
import com.etrade.mobilepro.common.getNavigationButtonDescription
import com.etrade.mobilepro.common.getToolbarTitleContentDescription
import com.etrade.mobilepro.common.overlayToolbarTitleView
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.setupToolbar
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.dropdown.R
import com.etrade.mobilepro.dropdown.databinding.OverlayBottomsheetBinding
import com.etrade.mobilepro.dropdown.multiselect.adapter.DropDownAdapter
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.accessibility.OnFirstAccessibilityFocusRunner
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference

class OverlayDropDownFragment : BottomSheetDialogFragment() {

    private val binding by viewBinding(OverlayBottomsheetBinding::bind)

    private var snackbarUtilFactory: SnackbarUtilFactory? = null
    private var isMultiSelect = false
    private var pendingAction: (() -> Unit)? = null
    private var onCancelAction: (() -> Unit)? = null

    override fun getTheme() = R.style.BottomSheetDialogTheme

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.overlay_bottomsheet, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupToolbar(
            navigation = Navigation(
                icon = R.drawable.ic_close,
                onClick = {
                    onCancelAction?.invoke()
                    dismiss()
                },
                contentDescription = getString(R.string.close)
            )
        )

        pendingAction?.invoke()
        pendingAction = null
        dialog?.window?.attributes?.windowAnimations = R.style.DialogAnimation
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener {
            val bottomSheetDialog = dialog as BottomSheetDialog
            val bottomSheet = bottomSheetDialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)!!
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            val layoutParams: CoordinatorLayout.LayoutParams = CoordinatorLayout.LayoutParams(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.MATCH_PARENT
            )
            layoutParams.behavior = bottomSheetBehavior
            bottomSheet.layoutParams = layoutParams
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        return dialog
    }

    override fun onCancel(dialog: DialogInterface) {
        onCancelAction?.invoke()
        super.onCancel(dialog)
    }

    @Suppress("LongMethod")
    internal fun <T> applySettings(settings: DropDownMultiSelector.Settings<T>, fallBackValues: Set<T>?) {
        pendingAction = {
            this.onCancelAction = {
                settings.onCancelListener?.invoke(fallBackValues)
            }
            overlayToolbarTitleView?.apply {
                text = settings.title
                contentDescription =
                    getToolbarTitleContentDescription(settings.title, requireContext())
            }
            this.snackbarUtilFactory = settings.snackbarUtilFactory
            handleProvidedSelectionFlow(settings)
            val adapter = DropDownAdapter(provideInnerListener(settings))
            adapter.ghostText = settings.ghostText
            adapter.actionItemSettings = settings.actionItemSettings
            binding.overlayRecyclerView.layoutManager = LinearLayoutManager(requireContext())
            binding.overlayRecyclerView.adapter = adapter
            settings.itemsData.removeObservers(viewLifecycleOwner)
            settings.itemsData.observeBy(viewLifecycleOwner) {
                adapter.submitItems(it)
            }
            handleMultiSelectCase(settings)
            handleActionButton(settings)
            handleLoadingState(settings)
            handleExplicitToolbarEnd(settings)
        }

        lifecycleScope.launch {
            lifecycle.whenStateAtLeast(STARTED) { pendingAction?.invoke() }
        }
    }

    @Suppress("ComplexMethod") // Detekt is considering the if/else blocks as new lines. Method logic is simple and does not make sense to split
    private fun toggleState(isProcessing: Boolean, includeActionButton: Boolean) {
        toolbarActionEndView?.visibility = if (isProcessing) { View.GONE } else if (isMultiSelect) { View.VISIBLE } else { View.GONE }
        binding.overlayRecyclerView.visibility = if (isProcessing) { View.GONE } else { View.VISIBLE }
        binding.loadingIndicator.visibility = if (isProcessing) { View.VISIBLE } else { View.GONE }
        if (includeActionButton) {
            binding.actionButton.visibility = if (isProcessing) { View.GONE } else { View.VISIBLE }
        }
    }

    private fun <T> provideInnerListener(settings: DropDownMultiSelector.Settings<T>): (Int, DropDownMultiSelector.Selectable<T>) -> Unit =
        if (settings.selectionFlow == null) {
            { pos, item ->
                settings.selectListener.invoke(pos, item)
                if (!settings.multiSelectEnabled && item.enabled) {
                    this.dismiss()
                }
            }
        } else {
            { pos, item ->
                settings.selectionFlow.willSelectListener.invoke(pos, item)
            }
        }

    @Suppress("LongMethod") // nothing to split / extract, short enough
    private fun <T> handleProvidedSelectionFlow(settings: DropDownMultiSelector.Settings<T>) {
        settings.selectionFlow?.let { selectionFlow ->
            selectionFlow.stageData.removeObservers(viewLifecycleOwner)
            selectionFlow.stageData.observeBy(viewLifecycleOwner) {
                val pos = it.data?.first
                val item = it.data?.second
                val hasActionButton = settings.actionButtonSettings != null
                toggleState(isProcessing = it is Resource.Loading, includeActionButton = hasActionButton)
                if (pos != null && item != null) {
                    when (it) {
                        is Resource.Loading -> {
                            // nop
                        }
                        is Resource.Success -> {
                            settings.selectListener.invoke(pos, item)
                            if (!settings.multiSelectEnabled && item.enabled) {
                                this.dismiss()
                            }
                            selectionFlow.didSelectListener.invoke(pos, item)
                        }
                        is Resource.Failed -> {
                            selectionFlow.onError?.invoke(pos, item)
                            selectionFlow.onRetry?.let { onRetryClosure ->
                                snackbarUtilFactory?.createSnackbarUtil(
                                    { viewLifecycleOwner },
                                    { view }
                                )?.retrySnackbar {
                                    onRetryClosure.invoke(pos, item)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun <T> handleMultiSelectCase(settings: DropDownMultiSelector.Settings<T>) {
        if (settings.multiSelectEnabled) {
            isMultiSelect = true
            toolbarActionEndView?.apply {
                setToolbarActionTextAndContentDescription(
                    settings.saveButtonText ?: getString(R.string.save)
                )
                setOnClickListener {
                    settings.eventListener?.invoke(
                        DropDownMultiSelector.Settings.Event.SAVE,
                        WeakReference(this@OverlayDropDownFragment)
                    )
                }
                visibility = View.VISIBLE
            }
            binding.clearAll.visibility = View.VISIBLE
            binding.clearAllDivider.visibility = View.VISIBLE
            binding.clearAll.setOnClickListener {
                settings.eventListener?.invoke(DropDownMultiSelector.Settings.Event.CLEAR_ALL, WeakReference(this))
            }
        }
    }

    private fun <T> handleActionButton(settings: DropDownMultiSelector.Settings<T>) {
        settings.actionButtonSettings?.let { buttonSettings ->
            with(binding.actionButton) {
                visibility = View.VISIBLE
                text = buttonSettings.buttonTitle
                setOnClickListener {
                    buttonSettings.actionButtonListener.invoke()
                }
            }
        }
    }

    private fun <T> handleLoadingState(settings: DropDownMultiSelector.Settings<T>) {
        settings.loadingState?.let { state ->
            state.observeBy(viewLifecycleOwner) {

                val hasActionButton = settings.actionButtonSettings != null
                toggleState(isProcessing = it is Resource.Loading, includeActionButton = hasActionButton)
                if (it is Resource.Failed) {
                    if (it.retry == null) {
                        snackbarUtilFactory?.createSnackbarUtil(
                            { viewLifecycleOwner },
                            { view }
                        )?.errorSnackbar()
                    } else {
                        snackbarUtilFactory?.createSnackbarUtil(
                            { viewLifecycleOwner },
                            { view }
                        )?.retrySnackbar { it.retry?.invoke() }
                    }
                }
            }
        }
    }

    private fun <T> handleExplicitToolbarEnd(settings: DropDownMultiSelector.Settings<T>) {
        settings.toolbarActionEnd?.let {
            val nav = Navigation(
                icon = R.drawable.ic_close,
                onClick = {
                    onCancelAction?.invoke()
                    dismiss()
                },
                contentDescription = getString(R.string.close)
            )
            setupToolbar(
                title = settings.title,
                navigation = nav,
                runOnFirstFocus = { toolbar, _, title, _ ->
                    OnFirstAccessibilityFocusRunner(toolbar) {
                        title?.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED)
                        toolbar.navigationContentDescription = context?.getNavigationButtonDescription(nav.icon)
                    }
                },
                actionEnd = it
            )
        }
    }
}
