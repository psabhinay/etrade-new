package com.etrade.mobilepro.chart.netassets;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/GetNetAssetsChartDataEntriesParameter;", "", "accountId", "", "(Ljava/lang/String;)V", "getAccountId", "()Ljava/lang/String;", "netassets_debug"})
public final class GetNetAssetsChartDataEntriesParameter {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String accountId = null;
    
    public GetNetAssetsChartDataEntriesParameter(@org.jetbrains.annotations.Nullable()
    java.lang.String accountId) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccountId() {
        return null;
    }
}