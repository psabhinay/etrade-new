package com.etrade.mobilepro.bloombergtv.presentation.delegate

import android.media.AudioAttributes
import android.media.MediaPlayer
import android.view.SurfaceHolder
import com.etrade.mobilepro.bloombergtv.common.ViewState
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvActivity
import com.etrade.mobilepro.bloombergtv.presentation.BloombergTvViewModel
import com.etrade.mobilepro.bloombergtv.presentation.common.mute
import com.etrade.mobilepro.bloombergtv.presentation.common.playing
import com.etrade.mobilepro.bloombergtv.presentation.common.position
import com.etrade.mobilepro.bloombergtv.presentation.common.safeCall
import com.etrade.mobilepro.bloombergtv.presentation.common.unmute
import com.etrade.mobilepro.bloombergtv.presentation.delegate.service.AudioFocusServiceDelegate

internal class MediaPlayerDelegate(
    private val viewModel: BloombergTvViewModel,
    private val videoControllerDelegate: VideoControllerDelegate,
) : BloombergTvActivityDelegate {

    val currentPosition: Int get() = player.position

    val isPlaying: Boolean get() = player.playing

    private lateinit var activity: BloombergTvActivity

    private var player: MediaPlayer? = null

    private var isPrepared = false

    private val audioFocusServiceDelegate = AudioFocusServiceDelegate(
        onFocusGain = ::start,
        onFocusLoss = ::pause,
        onFocusLossTransient = ::pause,
        onFocusLossCanDuck = player::mute,
    )

    fun start() {
        if (audioFocusServiceDelegate.requestFocus()) {
            player.unmute()
            player.safeCall(isPrepared) { start() }
        }
    }

    fun pause() {
        player.safeCall(isPrepared) { pause() }
    }

    @Suppress("ClickableViewAccessibility")
    override fun init(activity: BloombergTvActivity) {
        this.activity = activity
        audioFocusServiceDelegate.init(activity)
        videoControllerDelegate.init(activity)
        videoControllerDelegate.attachTo(activity.binding.bloombergtvContainer, this)
        activity.binding.root.setOnTouchListener { _, _ ->
            videoControllerDelegate.show()
            false
        }
        activity.binding.videoSurface.holder.addCallback(
            onSurfaceCreated { player?.setDisplay(this) }
        )
        viewModel.viewState.observe(activity) {
            if (it is ViewState.Success) {
                prepareWithUrl()
            }
        }
    }

    fun enablePausePlayButton(enabled: Boolean) =
        videoControllerDelegate.enablePausePlayButton(enabled)

    internal fun preparePlayer() {
        videoControllerDelegate.hide()
        player = createPlayer()
        prepareWithUrl()
    }

    internal fun releasePlayer() {
        audioFocusServiceDelegate.dismantle()
        isPrepared = false
        enablePausePlayButton(false)
        player?.release()
        player = null
    }

    private fun createPlayer() =
        MediaPlayer().apply {
            setScreenOnWhilePlaying(true)
            setOnPreparedListener {
                isPrepared = true
                enablePausePlayButton(true)
                player?.start()
                if (!viewModel.shouldBePlaying) {
                    player?.pause()
                }
                videoControllerDelegate.show()
            }
            setOnErrorListener { _, what, extra ->
                releasePlayer()
                viewModel.onError("mediaplayer error $what with $extra")
                true
            }
            setAudioAttributes(
                AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .build()
            )
        }

    private fun prepareWithUrl() {
        runCatching {
            viewModel.videoUrl?.let {
                player?.setDataSource(it)
                player?.prepareAsync()
            }
        }
    }

    private fun onSurfaceCreated(action: SurfaceHolder.() -> Unit): SurfaceHolder.Callback = object : SurfaceHolder.Callback {
        override fun surfaceCreated(holder: SurfaceHolder) = holder.action()
        override fun surfaceDestroyed(holder: SurfaceHolder) = Unit
        override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) = Unit
    }
}
