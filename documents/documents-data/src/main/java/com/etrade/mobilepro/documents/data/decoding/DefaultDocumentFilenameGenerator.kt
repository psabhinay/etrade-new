package com.etrade.mobilepro.documents.data.decoding

import com.etrade.mobilepro.documents.Document
import com.etrade.mobilepro.documents.StatementDocument
import com.etrade.mobilepro.documents.TaxDocument
import javax.inject.Inject

private const val ACCOUNT_STATEMENT = "account_statement_"
private const val TAX_DOC = "tax_document_"
private const val UNKNOWN_TYPE = "file_"
class DefaultDocumentFilenameGenerator @Inject constructor() : DocumentFilenameGenerator {
    override fun generateFilename(document: Document): String {
        val docType = when (document) {
            is StatementDocument -> ACCOUNT_STATEMENT
            is TaxDocument -> TAX_DOC
            else -> UNKNOWN_TYPE
        }
        val dateInfo = if (document is TaxDocument) {
            "${document.title}_${document.year}".replace("/", "_")
        } else {
            document.title.replace("/", "_")
        }
        return "$docType$dateInfo"
    }
}
