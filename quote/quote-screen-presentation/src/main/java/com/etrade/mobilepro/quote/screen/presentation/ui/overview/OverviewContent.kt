package com.etrade.mobilepro.quote.screen.presentation.ui.overview

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.etrade.mobilepro.common.compose.theme.EtradeTheme
import com.etrade.mobilepro.etcompose.tabhorizontalpager.PagerContent

@Composable
fun overviewPagerContent(): PagerContent = {
    OverviewContent()
}

@Composable
fun OverviewContent() {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        Column {
            // TODO: ETAND-18590
            Text("TODO: ETAND-18590")
        }
    }
}

@Preview
@Composable
fun PreviewOverviewContent() {
    EtradeTheme {
        OverviewContent()
    }
}
