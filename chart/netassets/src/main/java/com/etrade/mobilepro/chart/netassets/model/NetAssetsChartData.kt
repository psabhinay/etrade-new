package com.etrade.mobilepro.chart.netassets.model

import com.etrade.mobilepro.chart.api.ChartLine

class NetAssetsChartData(val accounts: List<ChartLine>)
