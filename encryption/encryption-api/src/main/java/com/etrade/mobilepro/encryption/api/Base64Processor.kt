package com.etrade.mobilepro.encryption.api

interface Base64Processor {
    fun encode(input: ByteArray): String
    fun decode(input: String): ByteArray
}
