package com.etrade.mobilepro.accountslist.data.dto

import com.etrade.mobile.accounts.dto.AccountDto
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.backends.mgs.BaseReference
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class AccountReferenceDto(
    override val data: List<AccountDto>
) : BaseReference<List<Account>>() {

    @Suppress("LongMethod") // just mappings
    override fun convertToModel() = data.map {
        Account(
            accountUuid = it.accountUuid,
            accountId = it.accountId,
            accountMode = it.accountMode,
            accountDescription = it.accountDescription,
            accountShortName = it.accountShortName,
            accountLongName = it.accountLongName,
            _accountType = it.accountType.name,
            _institutionType = it.institutionType.name,
            restrictionLevel = it.restrictionLevel,
            cashAvailableForWithdrawal = it.cashAvailableForWithdrawal,
            marginAccountFlag = it.marginAccountFlag ?: false,
            marginAvailableForWithdrawal = it.marginAvailableForWithdrawal,
            purchasingPower = it.purchasingPower,
            totalAvailableForWithdrawal = it.totalAvailableForWithdrawal,
            funded = it.funded ?: false,
            ledgerAccountValue = it.ledgerAccountValue,
            optionLevel = it.optionLevel,
            accountValue = it.accountValue,
            accountIndex = it.accountIndex,
            availableBalance = it.availableBalance,
            daysGain = it.daysGain,
            encAccountId = it.encAccountId,
            isIra = it.isIra,
            isAccountStreamingAllowed = it.streamingRestrictions?.isAccountStreamingAllowed ?: false,
            fundingPrompt = it.promptsForFunding,
            _managedAccountType = it.managedAccountType?.name ?: ""
        )
    }
}
