package com.etrade.mobilepro.login.api.repo

enum class LoginPreferencesError {
    GENERIC_ERROR,
    NO_VALID_BIOMETRIC,
    SECRET_KEY_INVALIDATED,
    LOGIN_CANCELED,
}
