package com.etrade.mobilepro.util.android.footnotespan

import android.graphics.Paint
import android.text.SpannableStringBuilder
import androidx.core.text.buildSpannedString
import androidx.core.text.inSpans

class FootnoteSectionBuilder(private val symbolSuffix: String) {

    private val footnotes: MutableList<Pair<String, SpannableStringBuilder.() -> Unit>> = mutableListOf()

    fun footnote(symbol: String, builderAction: SpannableStringBuilder.() -> Unit) {
        footnotes.add(Pair(symbol, builderAction))
    }

    fun build(measurePaint: Paint): CharSequence {
        val footnoteContext = FootnoteSectionContext(symbolSuffix)

        val content = buildSpannedString {
            footnotes.forEachIndexed { index, (symbol, builderAction) ->
                if (index > 0) {
                    append("\n")
                }
                inSpans(footnoteContext.addFootnote(symbol), builderAction)
            }
        }
        footnoteContext.updateMargin(measurePaint)
        return content
    }

    fun isEmpty() = footnotes.isEmpty()
}

fun footnoteSection(
    symbolSuffix: String = ". ",
    setup: FootnoteSectionBuilder.() -> Unit
): FootnoteSectionBuilder {
    val builder = FootnoteSectionBuilder(symbolSuffix)
    builder.setup()
    return builder
}
