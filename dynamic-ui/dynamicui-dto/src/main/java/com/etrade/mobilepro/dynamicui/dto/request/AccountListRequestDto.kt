package com.etrade.mobilepro.dynamicui.dto.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class AccountListRequestDto(
    @Json(name = "accountList")
    val accountList: String = "",
    @Json(name = "displayPersonalNotifications")
    val displayPersonalNotifications: Boolean = true,
    @Json(name = "enableDualAccountVisibility")
    val enableDualAccountVisibility: Boolean = true
)
