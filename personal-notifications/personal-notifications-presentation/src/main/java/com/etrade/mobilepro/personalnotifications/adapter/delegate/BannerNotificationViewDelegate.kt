package com.etrade.mobilepro.personalnotifications.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.personalnotifications.adapter.holder.BannerNotificationViewHolder
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.Localytics
import com.etrade.mobilepro.personalnotifications.databinding.ViewBannerNotificationItemBinding
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class BannerNotificationViewDelegate(
    val imageLoader: ImageLoader,
    val clickListener: ((PersonalNotification) -> Unit)
) : AbsListItemAdapterDelegate<Localytics.Banner, PersonalNotification, BannerNotificationViewHolder>() {
    override fun isForViewType(
        item: PersonalNotification,
        items: MutableList<PersonalNotification>,
        position: Int
    ): Boolean {
        return item is Localytics.Banner
    }

    override fun onCreateViewHolder(parent: ViewGroup): BannerNotificationViewHolder {
        return BannerNotificationViewHolder(
            parent.viewBinding(ViewBannerNotificationItemBinding::inflate),
            clickListener
        )
    }

    override fun onBindViewHolder(
        item: Localytics.Banner,
        holder: BannerNotificationViewHolder,
        payloads: MutableList<Any>
    ) {
        holder.bind(item, imageLoader)
    }
}
