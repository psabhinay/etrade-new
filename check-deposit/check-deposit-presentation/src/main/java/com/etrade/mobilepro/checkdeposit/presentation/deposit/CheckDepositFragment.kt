package com.etrade.mobilepro.checkdeposit.presentation.deposit

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.checkdeposit.button.CheckCaptureButton
import com.etrade.mobilepro.checkdeposit.presentation.CheckDepositLandingFragmentDirections
import com.etrade.mobilepro.checkdeposit.presentation.CheckDepositNavigationFlowViewModel
import com.etrade.mobilepro.checkdeposit.presentation.R
import com.etrade.mobilepro.checkdeposit.presentation.contributiondetails.IraContributionAmountsViewModel
import com.etrade.mobilepro.checkdeposit.presentation.databinding.FragmentCheckDepositBinding
import com.etrade.mobilepro.checkdeposit.usecases.api.CHECK_BACK
import com.etrade.mobilepro.checkdeposit.usecases.api.CHECK_FRONT
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.di.CheckDepositServiceAgreement
import com.etrade.mobilepro.common.disposeOnDestinationChanged
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.dropdown.multiselect.DefaultDropDownMultiSelector
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount
import com.etrade.mobilepro.livedata.concatCombine
import com.etrade.mobilepro.util.android.ClearFocusOnImeActionDone
import com.etrade.mobilepro.util.android.accessibility.bindButtonAccessibility
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.clearFocusSafely
import com.etrade.mobilepro.util.android.extension.hideSoftKeyboard
import com.etrade.mobilepro.util.android.extension.isApplicationDebuggable
import com.etrade.mobilepro.util.android.extension.openApplicationSettings
import com.etrade.mobilepro.util.android.permissions.PermissionDeniedResponse
import com.etrade.mobilepro.util.android.permissions.PermissionGrantedResponse
import com.etrade.mobilepro.util.android.permissions.PermissionsChecker
import com.etrade.mobilepro.util.android.permissions.SinglePermissionCheckerListener
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.util.android.textutil.CurrencyTextWatcher
import com.etrade.mobilepro.util.android.textutil.linksSpannable
import com.etrade.mobilepro.util.android.textutil.set
import com.google.android.material.snackbar.Snackbar
import com.miteksystems.misnap.misnapworkflow_UX2.MiSnapWorkflowActivity_UX2
import com.miteksystems.misnap.params.CameraApi
import com.miteksystems.misnap.params.MiSnapApi
import com.miteksystems.misnap.params.ScienceApi
import org.chromium.customtabsclient.shared.internalUrlToUri
import org.json.JSONObject
import org.slf4j.LoggerFactory
import javax.inject.Inject

private const val ACCOUNT_TAG = "Check deposit account selector tag"
private const val DEPOSIT_TYPE_TAG = "Check deposit ira deposit type selector tag"

private const val URL_FAQ = "file:///android_asset/faq/check-deposit-faq.html"
private const val URL_MAILING_INSTRUCTIONS = "file:///android_asset/mailing-instructions/mailing-instructions.html"

private const val REQUEST_CODE_SHOW_APP_SETTINGS = 255
private const val REQUEST_CODE_CAMERA_PERMISSION = 256

private val logger = LoggerFactory.getLogger(CheckDepositFragment::class.java)

@SuppressWarnings("LargeClass", "TooManyFunctions") // Complicated UI with many states...
@RequireLogin
class CheckDepositFragment @Inject constructor(
    private val viewModelProvider: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    @CheckDepositServiceAgreement private val checkDepositServiceAgreementUrl: String
) : Fragment(R.layout.fragment_check_deposit) {

    private val binding by viewBinding(FragmentCheckDepositBinding::bind) { onDestroyBinding() }

    private val viewModel: CheckDepositViewModel by navGraphViewModels(R.id.check_deposit_graph) { viewModelProvider }
    private val iraContributionAmountsViewModel: IraContributionAmountsViewModel by navGraphViewModels(
        R.id.check_deposit_graph
    ) { viewModelProvider }
    private val navigationFlowViewModel: CheckDepositNavigationFlowViewModel by navGraphViewModels(R.id.check_deposit_graph)
    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    private lateinit var accountSelector: DropDownMultiSelector<FundsFlowAccount>
    private lateinit var depositTypeSelector: DropDownMultiSelector<String>

    private val permissionsChecker = buildPermissionsChecker()

    private val disposeListener = disposeOnDestinationChanged { viewModel.disposeState() }

    private var snackbar: Snackbar? = null
    private var loading: Boolean = true
        set(value) {
            field = value
            binding.checkDepositInputGroup.visibility = if (value) {
                View.GONE
            } else {
                View.VISIBLE
            }
        }

    private var checkButtonsEnabled = false
        set(value) {
            field = value
            with(binding) {
                checkFrontButton.isEnabled = true
                checkBackButton.isEnabled = true

                if (value) {
                    checkFrontButton.setOnClickListener {
                        viewModel.onCheckScanClicked(MiSnapApi.PARAMETER_DOCTYPE_CHECK_FRONT)
                    }
                    checkBackButton.setOnClickListener {
                        viewModel.onCheckScanClicked(MiSnapApi.PARAMETER_DOCTYPE_CHECK_BACK)
                    }
                } else {
                    val pleaseCompleteDetailsOnClickListener = View.OnClickListener {
                        snackBarUtil.snackbar(
                            getString(R.string.please_complete_details),
                            duration = Snackbar.LENGTH_SHORT
                        )
                    }
                    checkFrontButton.setOnClickListener(pleaseCompleteDetailsOnClickListener)
                    checkBackButton.setOnClickListener(pleaseCompleteDetailsOnClickListener)
                }
            }
        }

    private var footerLinksEnabled = true

    private val miSnapRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            /*
            When a user clicks a capture button, it is disabled to prevent accidental multiple taps.
            When we the activity finishes and this fragment resumes, we need to determine whether to re-enable them.
            */
            updateView()
            result.data?.let { handleMiSnapResult(result.resultCode, it) }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        setupDepositPage()
    }

    override fun onResume() {
        super.onResume()
        updateView()
        footerLinksEnabled = true
        findNavController().addOnDestinationChangedListener(disposeListener)
    }

    override fun onPause() {
        super.onPause()
        findNavController().removeOnDestinationChangedListener(disposeListener)
    }

    private fun FragmentCheckDepositBinding.onDestroyBinding() {
        depositAmount.hideSoftKeyboard()
    }

    private fun setupDepositPage() {
        bindViewState()
        bindAccountsDropdown()
        bindIraDetails()
        setupAccountSelector()
        setupDepositTypeSelector()
        setupContributionDetailsButton()
        setupAmountInput()
        setupCheckScanClicks()
        checkButtonsEnabled = false

        binding.submitButton.setOnClickListener {
            viewModel.submitDeposit()
        }

        viewModel.restoreCheckDepositAmountSignal.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { amount ->
                    amount.toDoubleOrNull()?.let { amountDouble ->
                        val formattedAmount = MarketDataFormatter.formatCurrencyValue(amountDouble)
                        binding.depositAmountInput.setText(formattedAmount)
                    }
                }
            }
        )
    }

    private fun setupContributionDetailsButton() {
        binding.iraContributionDetailsButton.setOnClickListener {
            findNavController().navigate(R.id.openIraContributionAmounts)
        }
    }

    private fun setupAmountInput() {
        CurrencyTextWatcher.noLimit(editText = binding.depositAmountInput) {
            viewModel.onAmountEntered(it)
        }
        binding.depositAmountInput.setOnEditorActionListener(ClearFocusOnImeActionDone())
        setupFooterLinks()
    }

    private fun setupCheckScanClicks() {
        viewModel.checkScanClicks.observe(
            viewLifecycleOwner,
            Observer { event ->
                event.consume { permissionsChecker.check() }
            }
        )
    }
    private fun setupAccountSelector() {
        accountSelector = DefaultDropDownMultiSelector(
            parentFragmentManager,
            DropDownMultiSelector.Settings(
                tag = ACCOUNT_TAG,
                multiSelectEnabled = false,
                title = getString(R.string.title_account),
                itemsData = viewModel.accountsDropdownSelectables,
                selectListener = { _, selectable ->
                    viewModel.selectAccount(selectable.value)
                }
            )
        )

        with(binding.depositAccountSelectorInput) {
            keyListener = null
            showSoftInputOnFocus = false
            setOnClickListener {
                binding.depositAmount.clearFocusSafely()
                accountSelector.open()
            }
            bindButtonAccessibility(resources.getString(R.string.deposit_account_selected))
        }
    }

    private fun setupDepositTypeSelector() {
        val settings = DropDownMultiSelector.Settings(
            tag = DEPOSIT_TYPE_TAG,
            multiSelectEnabled = false,
            title = getString(R.string.title_deposit_type),
            itemsData = iraContributionAmountsViewModel.iraContributionDetails.concatCombine(viewModel.depositDetailsViewState) { iraDetails, state ->
                iraDetails.contributionDetails?.availableDepositTypes?.map { depositType ->
                    DropDownMultiSelector.Selectable(
                        title = depositType,
                        value = depositType,
                        selected = state.iraDepositType == depositType
                    )
                } ?: emptyList()
            },
            selectListener = { _, selectable ->
                viewModel.selectIraContributionType(selectable.value)
            }
        )
        depositTypeSelector = DefaultDropDownMultiSelector(parentFragmentManager, settings)
        with(binding.iraDepositTypeSelectorInput) {
            keyListener = null
            showSoftInputOnFocus = false
            setOnClickListener {
                binding.iraDepositTypeSelector.clearFocusSafely()
                depositTypeSelector.open()
            }
            bindButtonAccessibility(resources.getString(R.string.deposit_type_selected))
        }
    }

    private fun bindAccountsDropdown() {
        viewModel.accountsDropdownSelectables.observe(
            viewLifecycleOwner,
            Observer { dropdownAccounts ->
                with(binding) {
                    if (dropdownAccounts.isNullOrEmpty()) {
                        depositAccountSelectorInput.setText(R.string.no_available_accounts)
                        depositAccountSelector.isEnabled = false
                        depositAmount.isEnabled = false
                    } else {
                        depositAccountSelector.isEnabled = true
                        depositAmount.isEnabled = true
                    }
                }
            }
        )
    }

    private fun updateCaptureButtons(front: CheckCaptureViewState.CheckCapture?, back: CheckCaptureViewState.CheckCapture?) {
        fun selectState(state: CheckCaptureViewState.CheckCapture?) = when (state) {
            is CheckCaptureViewState.CheckCapture.Success -> {
                CheckCaptureButton.State.SUCCESS
            }
            is CheckCaptureViewState.CheckCapture.Failure -> {
                state.consumerErrorMessage?.let {
                    snackBarUtil.errorSnackbar(it)
                }
                CheckCaptureButton.State.FAILURE
            }
            is CheckCaptureViewState.CheckCapture.Uploading -> {
                CheckCaptureButton.State.LOADING
            }
            null -> CheckCaptureButton.State.INITIAL
        }

        binding.checkFrontButton.state = selectState(front)
        binding.checkBackButton.state = selectState(back)
    }

    @SuppressWarnings("LongMethod", "ComplexMethod")
    private fun bindViewState() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer { viewState ->
                updateView(viewState)
            }
        )
    }

    private fun bindIraDetails() {
        iraContributionAmountsViewModel.iraContributionDetails.observe(
            viewLifecycleOwner,
            Observer {
                binding.iraDepositTypeSelector.isEnabled = when (it) {
                    is IraContributionAmountsViewModel.ViewState.Loading -> {
                        updateIraDepositType(loading = true)
                        false
                    }
                    is IraContributionAmountsViewModel.ViewState.Loaded -> {
                        updateIraDepositType(loading = false)
                        true
                    }
                    is IraContributionAmountsViewModel.ViewState.Failed -> {
                        updateIraDepositType(loading = false)
                        snackBarUtil.retrySnackbar(getString(R.string.could_not_load_ira_details)) { it.retry() }
                        false
                    }
                }
            }
        )
        viewModel.selectedAccount.observe(
            viewLifecycleOwner,
            Observer {
                iraContributionAmountsViewModel.setSelectedAccount(it)
            }
        )
    }

    private fun updateIraDepositType(loading: Boolean) {
        binding.iraDepositTypeSelector.endIconDrawable?.let { icon ->
            icon.setVisible(loading, loading)
            (icon as? Animatable)?.apply {
                if (loading) {
                    start()
                } else {
                    stop()
                }
            }
        }
    }

    private fun updateView() = viewModel.viewState.value?.let { updateView(it) }

    @SuppressWarnings("LongMethod")
    private fun updateView(viewState: ViewState) {
        logger.debug("View state: $viewState")
        return when (viewState) {
            is ViewState.Loading -> loading = true
            is ViewState.AccountFetchFailed -> loading = false
            is ViewState.DetailsNeeded -> {
                loading = false
                checkButtonsEnabled = false
                binding.submitButton.isEnabled = false
                updateDepositDetails(viewState.depositDetails)
                updateCaptureButtons(
                    viewState.checkCapture?.checkFront,
                    viewState.checkCapture?.checkBack
                )
                handleValidationState(viewState.validationState)
            }
            is ViewState.CaptureInProgress -> {
                loading = false
                checkButtonsEnabled = true
                binding.submitButton.isEnabled = false
                updateDepositDetails(viewState.depositDetails)
                updateCaptureButtons(
                    front = viewState.checkCapture.checkFront,
                    back = viewState.checkCapture.checkBack
                )
                handleDepositSubmitState(viewState.depositImageState)
            }
            is ViewState.ReadyForSubmission -> {
                loading = false
                checkButtonsEnabled = true
                binding.submitButton.isEnabled = true
                updateDepositDetails(viewState.depositDetails)
                updateCaptureButtons(
                    front = viewState.checkCapture.checkFront,
                    back = viewState.checkCapture.checkBack
                )
                handleDepositSubmitState(viewState.depositImageState)
            }
            is ViewState.DepositSubmitted -> {
                handleDepositSubmitState(viewState.depositImageState)
            }
        }
    }

    private fun updateDepositDetails(detailsViewState: DepositDetailsViewState) {
        detailsViewState.selectedAccount?.let { selectedAccount ->
            with(binding.depositAccountSelectorInput) {
                setText(selectedAccount.displayString())
                bindButtonAccessibility(
                    resources.getString(R.string.deposit_account_selected) + selectedAccount.displayString()
                )
            }
            binding.iraInputsGroup.visibility = if (selectedAccount.isIraAccount) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }
        detailsViewState.depositAmount?.let { amount ->
            viewModel.rawDepositAmount.value = amount
        } ?: binding.depositAmountInput.text?.clear()
        with(binding.iraDepositTypeSelectorInput) {
            detailsViewState.iraDepositType?.let { depositType ->
                setText(depositType)
                bindButtonAccessibility(
                    resources.getString(R.string.deposit_type_selected) +
                        depositType
                )
            } ?: text?.clear()
        }
    }

    private fun startMiSnapWorkflow(docType: String) {
        val misnapParams = JSONObject().apply {
            put(MiSnapApi.MiSnapDocumentType, docType)
            if (docType == MiSnapApi.PARAMETER_DOCTYPE_CHECK_FRONT) {
                put(ScienceApi.MiSnapGeoRegion, ScienceApi.GEO_REGION_US)
            }
            val allowScreenshots = if (requireContext().isApplicationDebuggable()) { 1 } else { 0 }
            put(CameraApi.MiSnapAllowScreenshots, allowScreenshots)
            put(CameraApi.MiSnapFocusMode, CameraApi.PARAMETER_FOCUS_MODE_HYBRID)
        }

        val intentMiSnap = Intent(context, MiSnapWorkflowActivity_UX2::class.java)
        intentMiSnap.putExtra(MiSnapApi.JOB_SETTINGS, misnapParams.toString())
        miSnapRequest.launch(intentMiSnap)
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            when (bundle.customDialogRequestCode) {
                REQUEST_CODE_SHOW_APP_SETTINGS -> context?.openApplicationSettings()
                REQUEST_CODE_CAMERA_PERMISSION -> permissionsChecker.check()
            }
        }
    }

    private fun handleMiSnapResult(resultCode: Int, data: Intent) {
        try {
            val misnapResult = data.miSnapResultCode()
            when (resultCode) {
                Activity.RESULT_OK -> {
                    handleCapture(misnapResult, data)
                }
                Activity.RESULT_CANCELED -> {
                    val misnapResultCode = data.miSnapResultCode()
                    if (misnapResultCode == MiSnapApi.RESULT_CANCELED) {
                        logger.debug("Check capture cancelled by user.")
                    } else {
                        throw CheckCaptureException("Check capture cancelled", data)
                    }
                }
            }
        } catch (e: CheckCaptureException) {
            logger.error("Check capture failed", e)
            snackbar = snackBarUtil.errorSnackbar(e.message)
        }
    }

    private fun handleCapture(misnapResult: String?, data: Intent) {
        logger.debug("Check deposit capture. MiSnap result code: $misnapResult, Data: $data")
        when (misnapResult) {
            MiSnapApi.RESULT_SUCCESS_VIDEO, MiSnapApi.RESULT_SUCCESS_STILL -> {
                handleCheckImage(data)
            }
            else -> {
                throw CheckCaptureException("Unknown successful result code", misnapResult = misnapResult)
            }
        }
    }

    private fun handleCheckImage(data: Intent) {
        val mibiDataJson = data.mibiData()
        data.getStringExtra(MiSnapApi.RESULT_MIBI_DATA)?.let {
            val mibiDataObject = JSONObject(it)
            data.getByteArrayExtra(MiSnapApi.RESULT_PICTURE_DATA)?.let { bytes ->
                when (mibiDataObject.getString("Document")) {
                    MiSnapApi.PARAMETER_DOCTYPE_CHECK_FRONT -> {
                        viewModel.captureCheckImage(CHECK_FRONT, bytes)
                    }
                    MiSnapApi.PARAMETER_DOCTYPE_CHECK_BACK -> {
                        viewModel.captureCheckImage(CHECK_BACK, bytes)
                    }
                    else -> {
                        throw CheckCaptureException("Unknown document. Expecting front or back of check.", mibiData = mibiDataJson)
                    }
                }
            }
        }
    }

    private fun showCameraPermissionRationaleDialog(@StringRes actionLabel: Int, requestCode: Int) {
        val fragment = CustomDialogFragment
            .newInstance(
                message = getString(R.string.camera_permission_rationale_message),
                okTitle = getString(actionLabel),
                cancelTitle = getString(R.string.cancel),
                requestCode = requestCode
            )
        parentFragmentManager.also { fragment.show(it, "check_deposit_camera_permission_dialog") }
    }

    private fun setupFooterLinks() {
        val links = links(
            R.string.mobile_deposit_service_agreement to checkDepositServiceAgreementUrl,
            R.string.faq to URL_FAQ,
            R.string.mailing_instructions to URL_MAILING_INSTRUCTIONS
        )
        val separator = getString(R.string.dash)
        binding.checkDepositLinks.set(linksSpannable(links, separator))
    }

    private fun links(vararg links: Pair<Int, String>): List<Pair<String, (View) -> Unit>> =
        links.map { (resId, url) ->
            val text = getString(resId)
            val onCLick: (View) -> Unit = {
                if (footerLinksEnabled) {
                    openLink(text, url)
                }
                footerLinksEnabled = false
            }
            text to onCLick
        }

    private fun openLink(title: String, url: String) {
        findNavController().navigate(internalUrlToUri(title, url, R.drawable.ic_close, true))
    }

    private fun handleValidationState(validationState: ValidateDepositBusinessRulesViewState?) {
        when (validationState) {
            is ValidateDepositBusinessRulesViewState.Error -> {
                validationState.consumerMessage?.also { message ->
                    snackBarUtil.errorSnackbar(message)
                }
            }
            is ValidateDepositBusinessRulesViewState.Invalid -> {
                validationState.consumerMessage?.also { message ->
                    val navController = findNavController()
                    if (navController.currentDestination?.id == R.id.checkDepositLanding) {
                        navController.navigate(CheckDepositLandingFragmentDirections.openPopup(message))
                    }
                }
            }
        }
    }

    private fun handleDepositSubmitState(state: DepositImageViewState?) {
        when (state) {
            is DepositImageViewState.Error -> state.consumerMessage?.also { snackBarUtil.errorSnackbar(it) }
            is DepositImageViewState.Invalid -> state.consumerMessage?.also {
                findNavController().navigate(CheckDepositLandingFragmentDirections.openPopup(it))
            }
            is DepositImageViewState.Valid -> {
                navigationFlowViewModel.refreshDepositActivity()
                findNavController()
                    .takeIf { it.currentDestination?.id == R.id.checkDepositLanding }
                    ?.run {
                        CheckDepositLandingFragmentDirections.openDepositConfirmation(
                            amount = MarketDataFormatter.formatCurrencyValue(state.depositResults.amount),
                            confirmationNumber = state.depositResults.workflowId,
                            account = state.depositResults.accountNumber
                        ).let(::navigate)
                    }
            }
        }
    }

    private val String.docTypeButton: View?
        get() = when (this) {
            MiSnapApi.PARAMETER_DOCTYPE_CHECK_FRONT -> binding.checkFrontButton
            MiSnapApi.PARAMETER_DOCTYPE_CHECK_BACK -> binding.checkBackButton
            else -> null
        }

    private fun buildPermissionsChecker() = PermissionsChecker
        .with(this)
        .withPermission(Manifest.permission.CAMERA)
        .withListener(object : SinglePermissionCheckerListener {
            override fun onPermissionGranted(response: PermissionGrantedResponse) {
                viewModel.checkScanClicks.value?.peekContent()?.let { docType ->
                    docType.docTypeButton?.isEnabled = false
                    startMiSnapWorkflow(docType)
                }
            }

            override fun onPermissionDenied(response: PermissionDeniedResponse) {
                if (response.permanentlyDenied) {
                    showCameraPermissionRationaleDialog(R.string.settings, REQUEST_CODE_SHOW_APP_SETTINGS)
                }
            }

            override fun onPermissionRationaleShouldBeShown(permission: String) {
                showCameraPermissionRationaleDialog(R.string.ok, REQUEST_CODE_CAMERA_PERMISSION)
            }
        })
}
