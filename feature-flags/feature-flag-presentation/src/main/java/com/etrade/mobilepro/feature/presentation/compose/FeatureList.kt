package com.etrade.mobilepro.feature.presentation.compose

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Divider
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.etrade.mobilepro.feature.presentation.viewmodel.FeatureFlagsViewModel

@Composable
internal fun FeatureList(viewModel: FeatureFlagsViewModel, modifier: Modifier = Modifier) =
    Surface(modifier = modifier) {
        Box(modifier = Modifier.fillMaxSize()) {
            val scrollState = rememberLazyListState()
            LazyColumn(state = scrollState) {
                viewModel.actionItemModels.forEachIndexed { index, actionModel ->
                    if (index > 0) item { Divider() }
                    item { AsyncActionButtonItem(actionModel) }
                }
                val groupedFeatures = viewModel.featureFlagItemModels.groupBy { it.category }
                groupedFeatures.keys.forEach { category ->
                    category?.let { item { FeatureCategoryHeader(it) } }
                    groupedFeatures[category]?.forEachIndexed { index, featureModel ->
                        if (index > 0) item { Divider() }
                        item { FeatureItem(featureModel) }
                    }
                }
            }
        }
    }

@Preview
@Composable
private fun FeatureListPreview() = FeatureList(listViewModel())
