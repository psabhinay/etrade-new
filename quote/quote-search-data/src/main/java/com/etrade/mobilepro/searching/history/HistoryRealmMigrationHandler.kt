package com.etrade.mobilepro.searching.history

import io.realm.DynamicRealm
import io.realm.RealmMigration
import javax.inject.Inject

class HistoryRealmMigrationHandler @Inject constructor(
    private val migrations: Set<@JvmSuppressWildcards RealMigrationDelegate>
) : RealmMigration {

    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        var incOldVersion = oldVersion
        migrations.sortedBy { it.oldVersion }
            .filter { it.oldVersion >= oldVersion }
            .forEach { migration ->
                migration.migrate(realm, incOldVersion++, newVersion)
            }
    }
}
