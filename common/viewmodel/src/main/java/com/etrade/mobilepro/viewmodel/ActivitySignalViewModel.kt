package com.etrade.mobilepro.viewmodel

import android.app.Activity
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.util.android.ConsumableLiveEvent

typealias ActivitySignal = (Activity) -> Unit

interface ActivitySignalViewModel {
    val activitySignal: LiveData<ConsumableLiveEvent<ActivitySignal>>
}
