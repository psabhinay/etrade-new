package com.etrade.mobilepro.accounts.data.repo

import com.etrade.mobile.accounts.details.AccountsDetailedList
import com.etrade.mobile.accounts.details.AccountsDetailsRepo
import com.etrade.mobilepro.accounts.data.mapper.mapToApiModel
import com.etrade.mobilepro.accounts.data.rest.AccountsApiClient
import javax.inject.Inject

class DefaultAccountsDetailsRepo @Inject constructor(
    private val apiClient: AccountsApiClient
) : AccountsDetailsRepo {
    override suspend fun getAccountsDetailedList(consumerKey: String): AccountsDetailedList =
        apiClient
            .getAccountsDetails(consumerKey = consumerKey)
            .holder
            ?.mapToApiModel() ?: throw IllegalStateException("Got wrong Accounts Details response for consumerKey $consumerKey")
}
