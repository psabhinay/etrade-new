package com.etrade.mobilepro.instrument

interface WithSymbolInfo : WithInstrumentType, WithSymbol

val WithSymbolInfo.underlier: WithSymbolInfo
    get() {
        return if (instrumentType.isOption) {
            Symbol(getSymbolUnderlier(symbol, instrumentType) ?: symbol, InstrumentType.UNKNOWN)
        } else {
            this
        }
    }
