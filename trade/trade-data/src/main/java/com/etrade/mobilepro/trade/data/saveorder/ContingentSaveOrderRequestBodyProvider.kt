package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.trade.data.fixLimitStopPriceParam
import com.etrade.mobilepro.trade.data.stock.resolvePrices
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithContingentOrderInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithQuantity
import com.etrade.mobilepro.trade.form.api.input.WithTradeLegsTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractPriceType

internal class ContingentSaveOrderRequestBodyProvider<T>(
    private val inputId: T
) : BaseAdvancedSaveOrderRequestBodyProvider()
        where T : WithActionTradeFormInputId, T : WithContingentOrderInputId, T : WithPriceTradeFormInputId, T : WithQuantity {
    override fun createBody(params: InputValuesSnapshot): InputValuesSnapshot {
        val body: MutableMap<String, Any?> = mutableMapOf()
        params.toMutableMap().apply {
            resolvePrices(this)
            body["orderGroupType"] = OrderType.CONTINGENT.typeCode
            body["accountId"] = this[inputId.accountId]
            fixLimitStopPriceParam(inputId, inputId.extractPriceType(this))
            val tradeLegs = this[(inputId as? WithTradeLegsTradeFormInputId)?.tradeLegs] as? String
            val tradeLegSymbol = tradeLegs?.getJsonValue("symbol")?.let { inputId.symbolCoder.decode(it) }
            val orderDto = createOrder(this, inputId, tradeLegSymbol).apply {
                val symbolInfo = inputId.contingentOrderInputId.extractSymbolInfo(params)
                contingentSymbol = symbolInfo?.symbol
                contingentProductType = symbolInfo?.getProductType()
                contingentQualifier = params[inputId.contingentOrderInputId.qualifier].toString()
                contingentCondition = params[inputId.contingentOrderInputId.condition].toString()
                contingentValue = params[inputId.contingentOrderInputId.conditionValue].toString()
            }
            body["orderSvcReqList"] = listOf(orderDto)
        }
        return mapOf("value" to body)
    }
}

private fun String.getJsonValue(key: String) = substringAfter("$key\":\"").substringBefore("\"")
