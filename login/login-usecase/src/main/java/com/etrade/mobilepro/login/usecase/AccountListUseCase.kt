package com.etrade.mobilepro.login.usecase

import com.etrade.mobilepro.util.UseCase

interface AccountListUseCase : UseCase<Unit, Boolean>
