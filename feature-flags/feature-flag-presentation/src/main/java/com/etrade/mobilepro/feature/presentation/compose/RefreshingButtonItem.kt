package com.etrade.mobilepro.feature.presentation.compose

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
internal fun RefreshingButtonItem(
    onClick: () -> Unit,
    text: String,
    refreshing: Boolean,
    modifier: Modifier = Modifier
) {
    Box(
        modifier = modifier
            .background(MaterialTheme.colors.background)
            .fillMaxWidth()
    ) {
        Button(
            onClick = onClick,
            enabled = !refreshing,
            modifier = Modifier
                .padding(8.dp)
                .fillMaxWidth()
        ) {
            Box(contentAlignment = Alignment.Center) {
                CircularProgressIndicator(Modifier.alpha(if (refreshing) 1f else 0f))
                Text(
                    text,
                    color = MaterialTheme.colors.onPrimary
                )
            }
        }
    }
}

@Composable
private fun RefreshingButtonItemPreview(refreshing: Boolean) {
    RefreshingButtonItem(
        onClick = {},
        text = "${if (!refreshing) "Not " else ""}Refreshing Button",
        refreshing = refreshing
    )
}

@Preview
@Composable
private fun RefreshingButtonItemIsRefreshing() = RefreshingButtonItemPreview(refreshing = true)

@Preview
@Composable
private fun RefreshingButtonItemNotRefreshing() = RefreshingButtonItemPreview(refreshing = false)
