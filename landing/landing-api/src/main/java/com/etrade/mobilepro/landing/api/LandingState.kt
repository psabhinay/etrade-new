package com.etrade.mobilepro.landing.api

data class LandingState(
    val selected: LandingPage?,
    val pages: List<LandingPage>,
    val userAccounts: UserAccounts
)
