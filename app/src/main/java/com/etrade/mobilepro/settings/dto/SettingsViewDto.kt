package com.etrade.mobilepro.settings.dto

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class SettingsViewDto(
    @Json(name = "data")
    override val data: SettingDto,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?,
    @Json(name = "type")
    override val type: String
) : BaseScreenView() {
    val viewType: SettingsViewType = SettingsViewType.from(type)
}

@JsonClass(generateAdapter = true)
data class SettingDto(
    @Json(name = "title")
    val title: String,
    @Json(name = "value")
    var value: String? = null,
    @Json(name = "secondary_title")
    val secondaryTitle: String? = null
)

enum class SettingsViewType(val value: String) {
    SETTINGS_TEXT_VERSION("settings_text_version"),
    SETTINGS_ACTION_END_USER_LICENSE_AGREEMENT("settings_action_end_user_license_agreement"),
    SETTINGS_ACTION_PRIVACY_POLICY("settings_action_privacy_policy"),
    SETTINGS_ACTION_DISCLOSURES("settings_action_disclosures"),
    SETTINGS_ACTION_TRADING_DEFAULTS("settings_action_trading_defaults"),
    SETTINGS_PICKER_DEFAULT_HOME_PAGE("settings_picker_default_home_page"),
    SETTINGS_PICKER_SESSION_TIME_OUT("settings_picker_session_time_out"),
    SETTINGS_TOGGLE_BIOMETRIC_AUTH("settings_toggle_biometric_auth"),
    SETTINGS_TOGGLE_REMEMBER_USER_ID("settings_toggle_remember_user_id"),
    SETTINGS_TOGGLE_STREAMING("settings_toggle_streaming"),
    SETTINGS_TOGGLE_STOCK_ALERTS("settings_toggle_stock_alerts"),
    SETTINGS_TOGGLE_ACCOUNT_ALERTS("settings_toggle_account_alerts"),
    SETTINGS_ACTION_THEME_SELECTION("settings_action_theme_selection"),
    UNKNOWN("unknown");

    companion object {
        fun from(type: String?) = type?.let {
            values().firstOrNull { type == it.value }
        } ?: UNKNOWN
    }
}
