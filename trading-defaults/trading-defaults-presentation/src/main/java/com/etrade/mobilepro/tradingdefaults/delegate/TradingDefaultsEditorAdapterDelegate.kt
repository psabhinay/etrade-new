package com.etrade.mobilepro.tradingdefaults.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.tradingdefaults.TradingDefaultsItem
import com.etrade.mobilepro.tradingdefaults.viewholder.TradingDefaultsEditorViewHolder
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class TradingDefaultsEditorAdapterDelegate :
    AbsListItemAdapterDelegate<TradingDefaultsItem.EditorValue, TradingDefaultsItem, TradingDefaultsEditorViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): TradingDefaultsEditorViewHolder {
        return TradingDefaultsEditorViewHolder.from(parent)
    }

    override fun isForViewType(item: TradingDefaultsItem, items: MutableList<TradingDefaultsItem>, position: Int): Boolean =
        item is TradingDefaultsItem.EditorValue

    override fun onBindViewHolder(item: TradingDefaultsItem.EditorValue, holder: TradingDefaultsEditorViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }
}
