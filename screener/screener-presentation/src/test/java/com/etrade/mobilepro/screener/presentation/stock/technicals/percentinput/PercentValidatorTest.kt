package com.etrade.mobilepro.screener.presentation.stock.technicals.percentinput

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

private const val ERROR_DESC = "invalid value"
internal class PercentValidatorTest {
    private lateinit var sut: PercentValidator

    @BeforeEach
    fun setup() {
        sut = PercentValidator(ERROR_DESC)
    }

    @ParameterizedTest
    @ValueSource(strings = ["0", "1", "60", "100", "999"])
    fun testSuccess(input: String) {
        val result = sut.validate(input)
        assertTrue(result is InputFieldManager.Value.Valid && result.content == input)
    }

    @ParameterizedTest
    @ValueSource(strings = ["", "1000", "a", "10%"])
    fun testFail(input: String) {
        val result = sut.validate(input)
        assertTrue(result is InputFieldManager.Value.Invalid && result.content == input && result.reason == ERROR_DESC)
    }
}
