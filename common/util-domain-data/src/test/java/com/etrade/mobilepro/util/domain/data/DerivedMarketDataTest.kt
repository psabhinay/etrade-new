package com.etrade.mobilepro.util.domain.data

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class DerivedMarketDataTest {

    @Test
    fun `test derivePriceToEarnings function`() {
        listOf(
            PriceToEarningsTestData("11.89", "308.95", BigDecimal("25.98")),
            PriceToEarningsTestData("-11.89", "308.95", BigDecimal.ZERO),
            PriceToEarningsTestData("5.0", "0", BigDecimal.ZERO.setScale(2)),
            PriceToEarningsTestData("5.0", "0.0", BigDecimal.ZERO.setScale(2)),
            PriceToEarningsTestData("11.89", "-308.95", BigDecimal("-25.98")),
            PriceToEarningsTestData("0", "10.0", BigDecimal.ZERO),
            PriceToEarningsTestData("0.0", "10.0", BigDecimal.ZERO),
            PriceToEarningsTestData(null, null, null),
            PriceToEarningsTestData(null, "5.0", null),
            PriceToEarningsTestData("5.0", null, null),
            PriceToEarningsTestData("invalid", "5.0", null),
            PriceToEarningsTestData("5.0", "invalid", null)
        ).forEachIndexed { index, testParams ->
            println("test $index, Params: earningsPerShare = ${testParams.earningsPerShare}, price = ${testParams.price}")
            val result = derivePriceToEarnings(testParams.earningsPerShare, testParams.price)
            assertEquals(testParams.expectedResult, result)
        }
    }

    @Test
    fun `test deriveInitialPrice - Derived value returns when lastPrice and change are not null and valid values`() {
        val result = deriveInitialPrice(lastPrice = "10.0", change = "5.0")
        assertEquals("5.0", result)
    }

    @Test
    fun `test deriveInitialPrice - Null returns if any lastPrice or change is null or invalid value`() {
        listOf(
            Pair(null, null),
            Pair("5.0", null),
            Pair(null, "5.0"),
            Pair("invalid", "5.0"),
            Pair("5.0", "invalid")
        ).forEach { testParams ->
            val result = deriveInitialPrice(lastPrice = testParams.first, change = testParams.second)
            assertNull(result)
        }
    }

    @Test
    fun `test deriveChange - Derived value returns when initialPrice and lastPrice are not null and valid values`() {
        listOf(
            Triple("10.0", "5.0", "-5.0"),
            Triple("5.0", "10.0", "5.0")
        ).forEach { testParams ->
            val result = deriveChange(initialPrice = testParams.first, lastPrice = testParams.second)
            assertEquals(testParams.third, result)
        }
    }

    @Test
    fun `test deriveChange - Null returns if any initialPrice or lastPrice is null or invalid value`() {
        listOf(
            Pair(null, null),
            Pair("5.0", null),
            Pair(null, "5.0"),
            Pair("invalid", "5.0"),
            Pair("5.0", "invalid")
        ).forEach { testParams ->
            val result = deriveChange(initialPrice = testParams.first, lastPrice = testParams.second)
            assertNull(result)
        }
    }

    @Test
    fun `test deriveChangePercent - Derived value returns when initialPrice and change are not null and valid values`() {
        listOf(
            Triple("10.0", "5.0", "50.0000"),
            Triple("10.0", "-5.0", "-50.0000")
        ).forEach { testParams ->
            val result = deriveChangePercent(initialPrice = testParams.first, change = testParams.second)
            assertEquals(testParams.third, result)
        }
    }

    @Test
    fun `test deriveChangePercent - Null returns if any initialPrice or change is null or invalid value`() {
        listOf(
            Pair(null, null),
            Pair("5.0", null),
            Pair(null, "5.0"),
            Pair("invalid", "5.0"),
            Pair("5.0", "invalid")
        ).forEach { testParams ->
            val result = deriveChangePercent(initialPrice = testParams.first, change = testParams.second)
            assertNull(result)
        }
    }

    @Test
    fun `test deriveDayChange - Derived value returns when extendedHourLast and closingPrice and nonExtendedHoursChange are not null and valid values`() {
        listOf(
            Pair(Triple("15.0", "10.0", "15.0"), "20.0"),
            Pair(Triple("10.0", "15.0", "15.0"), "10.0")
        ).forEach { testParams ->
            val result = deriveDayChange(
                extendedHourLast = testParams.first.first,
                closingPrice = testParams.first.second,
                nonExtendedHoursChange = testParams.first.third
            )
            assertEquals(testParams.second, result)
        }
    }

    @Test
    fun `test deriveDayChange - Null returns if any extendedHourLast or closingPrice or nonExtendedHoursChange is null or invalid value`() {
        listOf(
            Triple(null, "5.0", "5.0"),
            Triple("5.0", null, "5.0"),
            Triple("5.0", "5.0", null),
            Triple("invalid", "5.0", "5.0"),
            Triple("5.0", "invalid", "5.0"),
            Triple("5.0", "5.0", "invalid")
        ).forEach { testParams ->
            val result = deriveDayChange(
                extendedHourLast = testParams.first,
                closingPrice = testParams.second,
                nonExtendedHoursChange = testParams.third
            )
            assertNull(result)
        }
    }

    @Test
    fun `test deriveDayChange - Derived value returns when extendedHourChange and nonExtendedHoursChange are not null and valid values`() {
        listOf(
            Triple("5.0", "10.0", "15.0"),
            Triple("0.0", "5.0", "5.0"),
            Triple("5.0", "0.0", "5.0"),
            Triple("-5.0", "10.0", "5.0"),
            Triple("5.0", "-10.0", "-5.0"),
            Triple("-5.0", "-10.0", "-15.0")
        ).forEach { testParams ->
            val result = deriveDayChange(extendedHourChange = testParams.first, nonExtendedHoursChange = testParams.second)
            assertEquals(testParams.third, result)
        }
    }

    @Test
    fun `test deriveDayChange - Null returns if any extendedHourChange or nonExtendedHoursChange is null or invalid value`() {
        listOf(
            Pair(null, null),
            Pair("5.0", null),
            Pair(null, "5.0"),
            Pair("invalid", "5.0"),
            Pair("5.0", "invalid")
        ).forEach { testParams ->
            val result = deriveDayChange(extendedHourChange = testParams.first, nonExtendedHoursChange = testParams.second)
            assertNull(result)
        }
    }

    @Test
    fun `test deriveDayChangePercent - Derived value returns when closingPrice and nonExtendedHoursChange and dayChange are not null and valid values`() {
        listOf(
            Pair(Triple("15.0", "5.0", "5.0"), "50.0000"),
            Pair(Triple("15.0", "5.0", "10.0"), "100.0000"),
            Pair(Triple("10.0", "0.0", "5.0"), "50.0000"),
            Pair(Triple("10.0", "5.0", "0.0"), "0.0000"),
            Pair(Triple("5.0", "-5.0", "-5.0"), "-50.0000"),
            Pair(Triple("5.0", "-5.0", "-10.0"), "-100.0000"),
            Pair(Triple("10.0", "0.0", "-5.0"), "-50.0000"),
            Pair(Triple("5.0", "-5.0", "0.0"), "0.0000"),
            Pair(Triple("5.0", "5.0", "10.0"), "0.0")
        ).forEach { testParams ->
            val result = deriveDayChangePercent(
                closingPrice = testParams.first.first,
                nonExtendedHoursChange = testParams.first.second,
                dayChange = testParams.first.third
            )
            assertEquals(testParams.second, result)
        }
    }

    @Test
    fun `test deriveDayChangePercent - Null returns if any closingPrice or nonExtendedHoursChange or dayChange is null or invalid value`() {
        listOf(
            Triple(null, "5.0", "5.0"),
            Triple("5.0", null, "5.0"),
            Triple("5.0", "5.0", null),
            Triple("invalid", "5.0", "5.0"),
            Triple("5.0", "invalid", "5.0"),
            Triple("5.0", "4.0", "invalid")
        ).forEach { testParams ->
            val result = deriveDayChangePercent(
                closingPrice = testParams.first,
                nonExtendedHoursChange = testParams.second,
                dayChange = testParams.third
            )
            assertNull(result)
        }
    }

    private data class PriceToEarningsTestData(
        val earningsPerShare: String?,
        val price: String?,
        val expectedResult: BigDecimal?
    )
}
