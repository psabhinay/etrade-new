package com.etrade.mobilepro.quote.presentation

enum class TradeQuoteType {
    BUY,
    SELL,
    EXCHANGE_BUY
}
