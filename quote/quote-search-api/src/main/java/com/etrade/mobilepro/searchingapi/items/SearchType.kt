package com.etrade.mobilepro.searchingapi.items

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

enum class SearchType {
    SYMBOL,
    CALL_OPTION,
    PUT_OPTION
}

@Parcelize
data class SearchQuery(
    val type: SearchType,
    val value: String = "",
    val description: String = "",
    var expiryYear: String = "",
    var expiryMonth: String = "",
    var expiryDay: String = ""
) : Parcelable {
    fun isEmpty() = value.isEmpty()
    fun isNotEmpty() = value.isNotEmpty()
}
