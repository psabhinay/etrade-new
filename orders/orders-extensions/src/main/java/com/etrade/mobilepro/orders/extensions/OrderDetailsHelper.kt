package com.etrade.mobilepro.orders.extensions

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.MutableInputValuesSnapshot
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.OptionType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.getOptionType
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem.AdvancedOrderItem
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem.OrderDetailsItem
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.trade.api.option.OptionTradeStrategy
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormInputIdMap
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.getTradeAction
import com.etrade.mobilepro.trade.form.api.input.ContingentOrderInputId
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithTradeLegsTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractAllOrNone

fun MutableInputValuesSnapshot.addContingentOrderValues(inputId: ContingentOrderInputId, item: AdvancedOrderItem, symbol: String?) {
    val safeSymbol = if (item.symbol.isEmpty()) {
        symbol.orEmpty()
    } else {
        item.symbol
    }
    val optionType = getOptionType(safeSymbol) ?: OptionType.UNKNOWN
    this[inputId.securityType] = if (optionType == OptionType.UNKNOWN) {
        inputId.securityTypeValueId.stock
    } else {
        inputId.securityTypeValueId.option
    }
    val symbolInfo = inputId.symbolCoder.encode(object : WithSymbolInfo {
        override val instrumentType: InstrumentType = if (optionType == OptionType.UNKNOWN) {
            InstrumentType.EQ
        } else {
            InstrumentType.OPTN
        }
        override val symbol: String = safeSymbol
    })

    this[inputId.symbol] = symbolInfo
    this[inputId.contingentSymbol] = symbolInfo
    this[inputId.qualifier] = item.qualifier.followPrice
    this[inputId.condition] = item.condition.offsetType
    this[inputId.conditionValue] = item.advancedValue
}

fun TradeFormParametersBuilder.applyOrderDetailValues(detailsItem: OrderDetailsItem, strategyType: StrategyType? = null) {
    priceType = detailsItem.priceType
    val advancedItem = detailsItem.advancedItem
    limitPrice = advancedItem?.price ?: detailsItem.limitPrice
    stopPrice = advancedItem?.price ?: detailsItem.stopPrice
    offsetValue = advancedItem?.offsetValue
    term = detailsItem.term
    quantity = detailsItem.quantity.resolveQuantityIfPartial().toBigDecimal()
    action = detailsItem.orderAction.getTradeAction()
    allOrNone = detailsItem.execInstruction.allOrNoneFlag
    strategyType?.let { type ->
        optionTradeStrategy = object : OptionTradeStrategy {
            override val strategyType = type
            override val optionLegs = detailsItem.tradeLegs
        }
    }
}

fun String.resolveQuantityIfPartial(): String {
    val divisor = '/'
    return if (contains(divisor)) {
        val first = substringBefore(divisor).toBigDecimal()
        val second = substringAfter(divisor).toBigDecimal()
        second.subtract(first).toString()
    } else {
        this
    }
}

fun TradeFormInputIdMap.getDisabledInputFields(
    isOpenOrder: Boolean,
    securityType: SecurityType,
    formParams: InputValuesSnapshot
): Array<String> {
    val tradeFormInputId = getInputId(TradeFormType(securityType))
    return tradeFormInputId?.run {
        listOfNotNull(
            accountId,
            this.securityType,
            (this as? WithSymbolTradeFormInputId)?.symbol,
            resolveDisabledActionInputField(isOpenOrder),
            (this as? WithTradeLegsTradeFormInputId)?.tradeLegs,
            (this as? OptionTradeFormInputId)?.lookupSymbol,
            resolveDisabledAllOrNoneInputField(isOpenOrder, formParams)
        ).toTypedArray()
    } ?: emptyArray()
}

private fun TradeFormInputId.resolveDisabledActionInputField(isOpenOrder: Boolean): String? {
    return if ((this is WithActionTradeFormInputId) && isOpenOrder) {
        action
    } else {
        null
    }
}

private fun TradeFormInputId.resolveDisabledAllOrNoneInputField(isOpenOrder: Boolean, formParams: InputValuesSnapshot): String? {
    if (this !is WithPriceTradeFormInputId || !isOpenOrder) { return null }

    val isAllOrNoneEnabled = extractAllOrNone(formParams) ?: false

    // Only orders that originally enabled allOrNone field can edit such field
    return if (isAllOrNoneEnabled) {
        null
    } else {
        allOrNone
    }
}
