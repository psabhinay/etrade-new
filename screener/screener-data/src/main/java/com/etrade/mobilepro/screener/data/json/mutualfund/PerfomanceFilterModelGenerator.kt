package com.etrade.mobilepro.screener.data.json.mutualfund

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.mutualfund.PerformanceFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val OVER_1M_FIELD = "1Month_TR_Ptile"
private const val OVER_3M_FIELD = "3Month_TR_Ptile"
private const val OVER_6M_FIELD = "6Month_TR_Ptile"
private const val OVER_YTD_FIELD = "YTD_TR_Ptile"
private const val OVER_1Y_FIELD = "1Year_TR_Ptile"
private const val OVER_3Y_FIELD = "3Year_TR_Ptile"
private const val OVER_5Y_FIELD = "5Year_TR_Ptile"
private const val OVER_10Y_FIELD = "10Year_TR_Ptile"

class PerformanceFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    @Suppress("LongMethod", "ComplexMethod") // not complex method, there is no nesting conditions
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val performanceFilter = screenerFilter as PerformanceFilter
        val arguments = mutableListOf<Argument>()

        if (!performanceFilter.isPerformanceValuesSelected()) {
            return PartialScreenerParams(arguments)
        }

        val clauses = getPerfomanceValuesClauses(performanceFilter)

        if (performanceFilter.overLast1Month) {
            arguments.add(Argument(clauses, OVER_1M_FIELD))
        }
        if (performanceFilter.overLast3Months) {
            arguments.add(Argument(clauses, OVER_3M_FIELD))
        }
        if (performanceFilter.overLast6Months) {
            arguments.add(Argument(clauses, OVER_6M_FIELD))
        }
        if (performanceFilter.overLastYTD) {
            arguments.add(Argument(clauses, OVER_YTD_FIELD))
        }
        if (performanceFilter.overLast1Year) {
            arguments.add(Argument(clauses, OVER_1Y_FIELD))
        }
        if (performanceFilter.overLast3Years) {
            arguments.add(Argument(clauses, OVER_3Y_FIELD))
        }
        if (performanceFilter.overLast5Years) {
            arguments.add(Argument(clauses, OVER_5Y_FIELD))
        }
        if (performanceFilter.overLast10Years) {
            arguments.add(Argument(clauses, OVER_10Y_FIELD))
        }

        return PartialScreenerParams(arguments)
    }

    private fun getPerfomanceValuesClauses(performanceFilter: PerformanceFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (performanceFilter.bottom20) {
            clauses.addAll(bottom20Clauses)
        }
        if (performanceFilter.belowMid20) {
            clauses.addAll(belowMid20Clauses)
        }
        if (performanceFilter.mid20) {
            clauses.addAll(mid20Clauses)
        }
        if (performanceFilter.aboveMid20) {
            clauses.addAll(aboveMid20Clauses)
        }
        if (performanceFilter.top20) {
            clauses.addAll(top20Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private val bottom20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("21"))
    )
    private val belowMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("21")),
        Clause(OPERATOR_LESS_THAN, listOf("41"))
    )
    private val mid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("41")),
        Clause(OPERATOR_LESS_THAN, listOf("61"))
    )
    private val aboveMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("61")),
        Clause(OPERATOR_LESS_THAN, listOf("81"))
    )
    private val top20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("81")),
        Clause(OPERATOR_LESS_THAN, listOf("101"))
    )
}
