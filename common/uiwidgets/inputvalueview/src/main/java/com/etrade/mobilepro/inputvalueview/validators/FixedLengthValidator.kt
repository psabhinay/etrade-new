package com.etrade.mobilepro.inputvalueview.validators

import com.etrade.mobilepro.inputvalueview.InputFieldManager

open class FixedLengthValidator(
    private val requiredLength: Int,
    private val validationError: String
) : InputFieldManager.Validator {
    override fun validate(value: String): InputFieldManager.Value {
        return if (value.length == requiredLength) {
            InputFieldManager.Value.Valid(value)
        } else {
            InputFieldManager.Value.Invalid(value, validationError)
        }
    }
}
