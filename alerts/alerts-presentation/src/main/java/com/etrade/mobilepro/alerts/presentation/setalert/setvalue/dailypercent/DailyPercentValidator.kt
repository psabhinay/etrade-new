package com.etrade.mobilepro.alerts.presentation.setalert.setvalue.dailypercent

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import java.util.regex.Pattern

// based on https://regexr.com/2rios
private val pattern: Pattern by lazy { Pattern.compile("""-?[\d]{1,6}([.|,]\d{1,5})?""") }
private val noLeadingDigitPattern: Pattern by lazy { Pattern.compile("""-?[.|,][\d]{1,5}""") }

internal class DailyPercentValidator(private val genericValidationErrorDesc: String) : InputFieldManager.Validator {
    override fun validate(value: String): InputFieldManager.Value {
        if (value.isEmpty()) {
            return InputFieldManager.Value.Valid(value)
        }
        if (noLeadingDigitPattern.matcher(value).matches()) {
            return InputFieldManager.Value.Valid(value)
        }
        return if (pattern.matcher(value).matches()) {
            InputFieldManager.Value.Valid(value)
        } else {
            InputFieldManager.Value.Invalid(value, genericValidationErrorDesc)
        }
    }
}
