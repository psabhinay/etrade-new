package com.etrade.mobilepro.messagecenter.data.mapper

import com.etrade.mobilepro.messagecenter.data.dto.ReferenceBookResponseDto
import com.etrade.mobilepro.testutil.XmlDeserializer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ReferenceBookDtoMapperTest {
    @Test
    fun testMapper() {
        val input = XmlDeserializer.getObjectFromXml("reference_book_response.xml", ReferenceBookResponseDto::class.java)
        val output = input.toReferenceBook(emptyMap())

        assertEquals(6, output.accounts.size)
        assertEquals(14, output.subjects.size)
        assertEquals(8, output.subjects[0].topics.size)
    }
}
