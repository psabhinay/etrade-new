package com.etrade.mobilepro.trade.data.stoploss

import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.trade.data.advanced.AdvancedTradingApiClient
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.shared.WithStopLossInputId
import com.etrade.mobilepro.trade.form.api.input.shared.WithTrailingStopInputId
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest

@ExperimentalCoroutinesApi
internal abstract class AbstractStopLossOrderTradeDelegateTest<T> where T : TradeFormInputId,
                                                                        T : WithStopLossInputId,
                                                                        T : WithAdvancedTradeFormInputId,
                                                                        T : WithSymbolTradeFormInputId,
                                                                        T : WithPriceTradeFormInputId,
                                                                        T : WithTrailingStopInputId {

    private val fakeClient: AdvancedTradingApiClient = mock {
        runBlocking {
            whenever(it.previewOrder(any(), any())).thenReturn(mock())
            whenever(it.placeOrder(any(), any())).thenReturn(mock())
        }
    }

    abstract val inputId: T

    protected fun comparePreviewInputAndOutput(input: Map<String, String>, output: Map<String, String>) {
        val delegate = StopLossOrderTradeDelegate(fakeClient, inputId)
        runBlockingTest {
            delegate.previewOrder(input, MarketDestination.AUTO)
        }
        runBlockingTest {
            verify(fakeClient).previewOrder(AdvancedTradingApiClient.PATH_OTA, output)
        }
    }
}
