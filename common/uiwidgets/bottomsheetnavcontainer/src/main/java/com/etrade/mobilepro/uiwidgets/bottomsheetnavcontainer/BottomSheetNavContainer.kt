package com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.networkUnavailableView
import com.etrade.mobilepro.common.setToolbarIcon
import com.etrade.mobilepro.session.api.SessionEvents
import com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer.databinding.FragmentBottomSheetContainerBinding
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.pxToDp
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

private val FRAG_TAG = "${BottomSheetNavContainer::class.java}.bottomNavFragment"

/**
 * Modal bottom sheet that contains a nav controller and inside navigation.
 */
abstract class BottomSheetNavContainer constructor(
    private val connectionNotifier: ConnectionNotifier,
    private val userSessionHandler: UserSessionHandler
) : BottomSheetDialogFragment() {
    private val binding by viewBinding(FragmentBottomSheetContainerBinding::bind)

    protected val hostNavFragment: NavHostFragment?
        get() = childFragmentManager.findFragmentByTag(FRAG_TAG) as? NavHostFragment

    private val isLastPageInStack: Boolean
        get() = hostNavFragment?.childFragmentManager?.backStackEntryCount == 0

    private val hostNavFragmentView: View?
        get() = hostNavFragment?.view

    open val bottomSheetMargin by lazy {
        requireContext().pxToDp(resources.getDimension(R.dimen.spacing_huge).toInt())
    }

    var titleText: String = ""
        set(value) {
            binding.toolbarTitle.text = value
            field = value

            updateToolbarDescription()
        }

    var titleTextContentDescription: String? = null
        set(value) {
            field = value
            updateToolbarDescription()
        }

    var subTitleText: String = ""
        set(value) {
            binding.toolbarSubTitle.text = value
            binding.toolbarSubTitle.isVisible = value.isNotEmpty()
            field = value

            updateToolbarDescription()
        }

    var toolBarDividerVisible: Boolean
        set(value) {
            binding.toolBarDivider.isVisible = value
        }
        get() = binding.toolBarDivider.isVisible

    private fun updateToolbarDescription() {
        binding.toolBarContainer.contentDescription = getString(R.string.label_header_description, "${titleTextContentDescription ?: titleText} $subTitleText")
    }

    private val keyboardListener: KeyboardListener by lazy {
        KeyboardListener(requireActivity()) { shown ->
            if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
                hostNavFragmentView?.let { keyboardListener.scrollViewForKeyboardSize(it, shown) }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_bottom_sheet_container, container, false)
    }

    override fun onPause() {
        super.onPause()
        unsubscribeKeyboardVisibilityChanges()
    }

    override fun onResume() {
        super.onResume()
        subscribeKeyboardVisibilityChanges()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initNetworkConnectivityBanner()
        setHostFragment(hostNavFragment ?: createNavHost())

        childFragmentManager.registerFragmentLifecycleCallbacks(
            object : FragmentManager.FragmentLifecycleCallbacks() {
                override fun onFragmentViewCreated(fm: FragmentManager, f: Fragment, v: View, savedInstanceState: Bundle?) {
                    super.onFragmentViewCreated(fm, f, v, savedInstanceState)

                    if (f.tag == FRAG_TAG) {
                        hostNavFragment?.childFragmentManager?.addOnBackStackChangedListener {
                            setupToolbar()
                        }
                        hostNavFragment?.navController?.addOnDestinationChangedListener(
                            createDestinationChangeListener()
                        )
                    }
                }
            },
            false
        )

        userSessionHandler.sessionTerminateEvent.observe(
            this@BottomSheetNavContainer.viewLifecycleOwner,
            {
                when (it) {
                    SessionEvents.MSG_SESSION_SAR_TERMINATED,
                    SessionEvents.MSG_SESSION_TERMINATED -> {
                        dismissAllowingStateLoss()
                    }
                    else -> { /* Not handle */
                    }
                }
            }
        )
    }

    private fun setHostFragment(host: NavHostFragment) {
        childFragmentManager.beginTransaction()
            .replace(R.id.bottom_sheet_nav_host, host, FRAG_TAG)
            .setPrimaryNavigationFragment(host)
            .commit()
    }

    override fun onStart() {
        super.onStart()
        setupToolbar()
    }

    protected abstract fun createNavHost(): NavHostFragment

    protected abstract fun createDestinationChangeListener(): NavController.OnDestinationChangedListener

    protected abstract fun onTitleChange(newTitle: String)

    private fun setupToolbar() {
        binding.toolBar.setToolbarIcon(isLastPageInStack, isWhite = true)
        binding.toolBar.setNavigationOnClickListener { onNavigationBackPressed(isLastPageInStack) }
    }

    protected open fun onNavigationBackPressed(isLastPageInStack: Boolean) {
        dismissOrPopStack(isLastPageInStack)
    }

    protected open fun onBackPressed(isLastPageInStack: Boolean) {
        dismissOrPopStack(isLastPageInStack)
    }

    private fun dismissOrPopStack(isLastPageInStack: Boolean) {
        if (isLastPageInStack) {
            dismiss()
        } else {
            hostNavFragment?.findNavController()?.popBackStack()
        }
    }

    override fun getTheme(): Int = R.style.BottomSheetDialogTheme

    private fun initNetworkConnectivityBanner() {
        val connectionCallback = object : ConnectionNotifier.ConnectionNotification {
            override fun onNetworkAvailable() {
                activity?.runOnUiThread {
                    networkUnavailableView?.visibility = View.GONE
                }
            }

            override fun onNetworkLost() {
                activity?.runOnUiThread {
                    networkUnavailableView?.visibility = View.VISIBLE
                }
            }
        }

        connectionNotifier.createNotifier(connectionCallback)?.let {
            lifecycle.addObserver(it)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)

        dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        setDialogLayoutParams(dialog)
        return dialog
    }

    private fun setDialogLayoutParams(dialog: Dialog) {
        val layoutParams: CoordinatorLayout.LayoutParams = CoordinatorLayout.LayoutParams(
            CoordinatorLayout.LayoutParams.MATCH_PARENT,
            getRootLayoutHeight()
        )

        layoutParams.topMargin = bottomSheetMargin

        dialog.setOnShowListener { dialogInterface ->
            setUpBottomSheet(
                dialogInterface,
                layoutParams,
                bottomSheetMargin
            )
        }
        dialog.setOnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action != KeyEvent.ACTION_DOWN) {
                onBackPressed(isLastPageInStack)
                true
            } else {
                false
            }
        }
    }

    open fun getRootLayoutHeight() = CoordinatorLayout.LayoutParams.MATCH_PARENT

    private fun unsubscribeKeyboardVisibilityChanges() {
        hostNavFragmentView?.let { keyboardListener.scrollViewForKeyboardSize(it, false) }
        activity?.getViewTreeObserver()?.removeOnGlobalLayoutListener(keyboardListener)
    }

    private fun subscribeKeyboardVisibilityChanges() {
        hostNavFragmentView?.post { activity?.getViewTreeObserver()?.addOnGlobalLayoutListener(keyboardListener) }
    }

    private fun setUpBottomSheet(
        dialogInterface: DialogInterface?,
        params: CoordinatorLayout.LayoutParams,
        bottomSheetMargin: Int
    ) {
        val bottomSheetDialog = dialogInterface as BottomSheetDialog
        val bottomSheet = bottomSheetDialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)!!
        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)

        params.behavior = bottomSheetBehavior
        bottomSheet.layoutParams = params
        bottomSheet.setPadding(0, 0, 0, bottomSheetMargin)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }
}
