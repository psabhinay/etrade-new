package com.etrade.mobilepro.chat.presentation.message

import android.animation.ValueAnimator
import android.animation.ValueAnimator.AnimatorUpdateListener
import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.LinearLayout
import com.etrade.mobilepro.chat.presentation.R
import java.util.ArrayList

class ChatDotsIndicator : LinearLayout {
    private lateinit var dots: MutableList<View>
    private var animation: ValueAnimator? = null
    private var isAttached = false
    private var jumpHalfTime = JUMP_DURATION / 2
    // Cached Calculations
    private val dotsStartTime = IntArray(DOTS_COUNT)
    private val dotsJumpUpEndTime = IntArray(DOTS_COUNT)
    private val dotsJumpDownEndTime = IntArray(DOTS_COUNT)

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        orientation = HORIZONTAL
        gravity = Gravity.BOTTOM
        calculateCachedValues()
        initializeDots(context)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        // We allow the height to save space for the jump height
        setMeasuredDimension(
            measuredWidth,
            measuredHeight + resources.getDimensionPixelSize(R.dimen.chat_dots_jump_height)
        )
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        isAttached = true
        createAnimation()
        if (visibility == View.VISIBLE) {
            animation?.start()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        isAttached = false
        animation?.end()
    }

    override fun setVisibility(visibility: Int) {
        super.setVisibility(visibility)
        when (visibility) {
            View.VISIBLE -> {
                createAnimation()
                startAnimationIfAttached()
            }
            View.INVISIBLE, View.GONE -> animation?.end()
        }
    }

    private fun createDotView(context: Context) =
        ImageView(context).apply { setImageResource(R.drawable.chat_dots_indicator) }

    private fun startAnimationIfAttached() {
        if (isAttached && !animation!!.isRunning) {
            animation!!.start()
        }
    }

    @Suppress("LongMethod")
    private fun createAnimation() {
        if (animation != null) return // We already have an animation
        calculateCachedValues()
        initializeDots(context)
        animation = ValueAnimator.ofInt(0, LOOP_DURATION).apply {
            addUpdateListener(
                AnimatorUpdateListener { valueAnimator ->
                    val animationValue = valueAnimator.animatedValue as Int
                    if (animationValue < LOOP_START_DELAY) { // Do nothing
                        return@AnimatorUpdateListener
                    }
                    for (i in 0 until dots.size) {
                        val dotStartTime = dotsStartTime[i]
                        val animationFactor = when {
                            animationValue < dotStartTime -> 0f // No animation is needed for this dot yet
                            animationValue < dotsJumpUpEndTime[i] -> { // Animate jump up
                                (animationValue - dotStartTime).toFloat() / jumpHalfTime
                            }
                            animationValue < dotsJumpDownEndTime[i] -> { // Animate jump down
                                1 - (animationValue - dotStartTime - jumpHalfTime).toFloat() / jumpHalfTime
                            }
                            else -> 0f // Dot finished animation for this loop
                        }
                        dots[i].translationY = -1 * resources.getDimensionPixelSize(R.dimen.chat_dots_jump_height) * animationFactor
                    }
                }
            )
            duration = LOOP_DURATION.toLong()
            repeatCount = Animation.INFINITE
        }
    }

    private fun calculateCachedValues() {
        // The offset is the time delay between dots start animation
        val startOffset = (LOOP_DURATION - (JUMP_DURATION + LOOP_START_DELAY)) / (DOTS_COUNT - 1)
        for (i in 0 until DOTS_COUNT) {
            val startTime = LOOP_START_DELAY + startOffset * i
            dotsStartTime[i] = startTime
            dotsJumpUpEndTime[i] = startTime + jumpHalfTime
            dotsJumpDownEndTime[i] = startTime + JUMP_DURATION
        }
    }

    private fun initializeDots(context: Context) {
        removeAllViews()
        // Create the dots
        dots = ArrayList(DOTS_COUNT)
        val params = LayoutParams(
            resources.getDimensionPixelSize(R.dimen.chat_dots_space_default),
            resources.getDimensionPixelSize(R.dimen.chat_dots_size_default)
        )
        for (i in 0 until DOTS_COUNT) { // Add dot
            val dotView = createDotView(context)
            addView(dotView, params)
            dots.add(dotView)
            // Add space
            if (i < DOTS_COUNT - 1) {
                addView(View(context), params)
            }
        }
    }

    companion object {
        const val DOTS_COUNT = 3
        const val LOOP_DURATION = 1000
        const val LOOP_START_DELAY = 400
        const val JUMP_DURATION = 300
    }
}
