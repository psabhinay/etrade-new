package com.etrade.mobilepro.orders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.databinding.FragmentButtonSheetOrdersBinding
import com.etrade.mobilepro.orders.adapter.OrdersPagingDataAdapter
import com.etrade.mobilepro.orders.list.api.OrderItem
import com.etrade.mobilepro.orders.sharedviewmodel.OrderSharedViewModel
import com.etrade.mobilepro.orders.viewmodel.OrdersListViewModel
import com.etrade.mobilepro.swiperefresh.applyColorScheme
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val logger: Logger = LoggerFactory.getLogger(OrdersListFragment::class.java)

@RequireLogin
class OrdersListFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.fragment_button_sheet_orders) {

    private val binding by viewBinding(FragmentButtonSheetOrdersBinding::bind)

    private val args: OrderBottomSheetFragmentArgs by navArgs()
    private val listViewModel: OrdersListViewModel by viewModels { viewModelFactory }
    private val sharedViewModel: OrderSharedViewModel by activityViewModels { viewModelFactory }
    private val snackbarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }
    private var snackBar: Snackbar? = null
    private val ordersPagingDataAdapter = OrdersPagingDataAdapter(
        selectItemListener = { item, _ -> navigateToDetail(item) },
        selectionChecker = { false }
    )

    private val loadStateListener: (CombinedLoadStates) -> Unit = {
        binding.textEmptyState.isVisible = it.append.endOfPaginationReached && ordersPagingDataAdapter.itemCount < 1
    }

    private var currentPagingJob: Job? = null

    private fun navigateToDetail(item: OrderItem) {
        listViewModel.setSelectedOrderItem(item)
        navigateToOrderDetails()
    }

    private fun navigateToOrderDetails() {
        args.accountId?.let {
            listViewModel.getOrderDetailArgument(it)?.let {
                val directions = OrdersListFragmentDirections.actionOrdersPageToOrderDetailsFragment(it)
                findNavController().navigate(directions)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_button_sheet_orders, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupPullToRefresh()
        setupViewModels()
        setupOrdersRecyclerView()

        if (savedInstanceState == null) {
            refreshList()
        }
    }

    private fun setupPullToRefresh() {
        binding.swipeRefresh.apply {
            applyColorScheme()
            setOnRefreshListener { refreshList(forceRefresh = true) }
        }
    }

    @Suppress("LongMethod")
    private fun setupViewModels() {
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            ordersPagingDataAdapter.loadStateFlow.collectLatest {
                handleRefreshState(it.refresh)

                handleNetworkState(it.append)
            }
        }

        sharedViewModel.isRefreshListRequested.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { requested ->
                    if (requested) {
                        refreshList(forceRefresh = true)
                    }
                }
            }
        )

        listViewModel.ordersFlow?.let { flow ->
            currentPagingJob?.cancel()
            currentPagingJob = viewLifecycleOwner.lifecycleScope.launchWhenResumed {
                flow.collectLatest {
                    ordersPagingDataAdapter.submitData(it)
                }
            }
        }
    }

    private fun handleNetworkState(state: LoadState) {
        when (state) {
            is LoadState.Loading -> {
                ordersPagingDataAdapter.isLoading = true
            }

            is LoadState.NotLoading -> {
                ordersPagingDataAdapter.isLoading = false
            }

            is LoadState.Error -> {
                ordersPagingDataAdapter.isLoading = false
                logger.error("Order list network failure", state.error)
                snackBar = snackbarUtil.retrySnackbar(
                    actionOnRetry = {
                        ordersPagingDataAdapter.retry()
                    }
                )
            }
        }
    }

    private fun handleRefreshState(state: LoadState) {
        when (state) {
            is LoadState.Loading -> {
                if (!binding.swipeRefresh.isRefreshing) {
                    binding.loadingIndicator.show()
                }
                snackBar?.dismiss()
            }

            is LoadState.NotLoading -> {
                binding.loadingIndicator.hide()
                binding.swipeRefresh.isRefreshing = false
                snackBar?.dismiss()
            }

            is LoadState.Error -> {
                binding.loadingIndicator.hide()
                binding.swipeRefresh.isRefreshing = false
                logger.error("Order list refresh network failure", state.error)
                snackBar = snackbarUtil.retrySnackbar(
                    actionOnRetry = {
                        ordersPagingDataAdapter.retry()
                    }
                )
            }
        }
    }

    private fun refreshList(forceRefresh: Boolean = false) {
        args.accountId?.let {
            binding.textEmptyState.isVisible = false
            currentPagingJob?.cancel()
            currentPagingJob = viewLifecycleOwner.lifecycleScope.launchWhenResumed {
                listViewModel.fetchOrders(it, args.orderType, forceRefresh).collectLatest { pagingData ->
                    ordersPagingDataAdapter.submitData(pagingData)
                }
            }
        }
    }

    private fun setupOrdersRecyclerView() {
        with(binding.openOrdersRecyclerView) {
            adapter = ordersPagingDataAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

        ordersPagingDataAdapter.removeLoadStateListener(loadStateListener)
        ordersPagingDataAdapter.addLoadStateListener(loadStateListener)
    }
}
