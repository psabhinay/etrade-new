package com.etrade.mobilepro.chat.api

sealed class ChatMessage {

    object Loading : ChatMessage()
    object AgentTyping : ChatMessage()
    data class User(val message: String) : ChatMessage()
    data class Agent(val message: String) : ChatMessage()
    object TransferQueue : ChatMessage()
}
