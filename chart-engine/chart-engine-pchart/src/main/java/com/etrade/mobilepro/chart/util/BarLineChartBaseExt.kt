package com.etrade.mobilepro.chart.util

import com.github.mikephil.charting.charts.BarLineChartBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.renderer.XAxisRenderer
import com.github.mikephil.charting.utils.Transformer
import com.github.mikephil.charting.utils.ViewPortHandler

/**
 * Solution was found at https://github.com/PhilJay/MPAndroidChart/pull/2692
 * to draw labels at specific positions.
 * Feature has yet to be merged to MPAndroidChart
**/
fun BarLineChartBase<*>.setSpecificPositionLabels(labels: Set<Float>) {
    setXAxisRenderer(
        SpecificPositionLabelsXAxisRenderer(
            viewPortHandler,
            xAxis,
            getTransformer(axisLeft.axisDependency),
            labels.toFloatArray()
        )
    )
}

class SpecificPositionLabelsXAxisRenderer(
    viewPortHandler: ViewPortHandler,
    xAxis: XAxis,
    trans: Transformer,
    val specificLabelPositions: FloatArray
) : XAxisRenderer(viewPortHandler, xAxis, trans) {

    override fun computeAxisValues(min: Float, max: Float) {

        mAxis.mEntryCount = specificLabelPositions.size
        mAxis.mEntries = specificLabelPositions
        mAxis.setCenterAxisLabels(false)

        computeSize()
    }
}
