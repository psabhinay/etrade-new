package com.etrade.mobilepro.screener.presentation.common

import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell

data class TableViewPayload(
    val columnHeaderList: List<Cell>,
    val rowHeaders: List<SymbolCell>,
    val cellItems: List<List<ResultDataCell>>
)
