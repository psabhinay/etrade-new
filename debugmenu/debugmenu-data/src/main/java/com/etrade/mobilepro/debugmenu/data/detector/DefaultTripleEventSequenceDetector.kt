package com.etrade.mobilepro.debugmenu.data.detector

import com.etrade.mobilepro.debugmenu.api.TripleEventSequenceDetector
import com.etrade.mobilepro.util.android.timer.TimeoutTimer
import org.threeten.bp.Duration
import javax.inject.Inject

private const val TRIPLE_EVENTS = 3

class DefaultTripleEventSequenceDetector @Inject constructor(
    private val timer: TimeoutTimer
) : TripleEventSequenceDetector {

    private var listener: (() -> Unit)? = null
    private var events = 0
    private var timeout: Duration = Duration.ofSeconds(1)

    override fun setup(timeout: Duration, listener: () -> Unit) {
        this.timeout = timeout
        this.listener = listener
    }

    override fun onEvent() {
        if (!timer.isActive()) {
            events = 0
            timer.start(timeout) {
                if (events >= TRIPLE_EVENTS) {
                    listener?.invoke()
                }
            }
        }
        events++
    }
}
