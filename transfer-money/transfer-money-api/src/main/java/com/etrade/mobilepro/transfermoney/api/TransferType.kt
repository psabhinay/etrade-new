package com.etrade.mobilepro.transfermoney.api

enum class TransferType(val transferTypeResponse: String) {
    TRANSFER_MONEY("TRANSFER_MONEY, "),
    WIRE_TRANSFER("WIRE_TRANSFER, "),
    ACCOUNT_TRANSFER("ACCOUNT_TRANSFER, "),
    CHECK("CHECK, "),
    ROLLOVER_ACCOUNT("ROLLOVER_ACCOUNT"),
    UNKNOWN("INVALID_TRANSFER_TYPE");

    companion object {
        /**
         * Creates a TransferType from Transfer Activity responses
         */
        fun fromTransferTypeResponse(transferType: String?): TransferType =
            transferType?.let { type ->
                values().firstOrNull { it.transferTypeResponse == type } ?: UNKNOWN
            } ?: UNKNOWN
    }
}
