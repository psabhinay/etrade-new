package com.etrade.mobilepro.backends.af

private const val EXCEPTION_CODE = 1001

enum class ErrorType(val value: Int) {
    /**
     * An error has occurred where the request or operation could not be completed successfully.
     */
    ERROR(1),
    /**
     * A warning that is returned with the successful completion of a request or operation.
     */
    WARNING(2),
    /**
     * An exception has occurred.
     */
    EXCEPTION(EXCEPTION_CODE),

    /**
     * An unknown type
     */
    UNKNOWN(0);

    companion object {
        fun fromCode(code: Int): ErrorType {
            return values().find { it.value == code } ?: UNKNOWN
        }
    }
}
