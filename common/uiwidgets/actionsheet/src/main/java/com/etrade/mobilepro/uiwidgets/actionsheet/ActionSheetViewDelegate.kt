package com.etrade.mobilepro.uiwidgets.actionsheet

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.viewdelegate.ViewDelegate

class ActionSheetViewDelegate(
    private val viewModel: ActionSheetViewModel,
    private val clicksViewModel: ActionSheetClicksViewModel,
    private val manager: FragmentManager,
    private val style: ActionSheetStyle = ActionSheetStyle.DefaultBubbles
) : ViewDelegate {

    override fun observe(owner: LifecycleOwner) {
        viewModel.actions.observe(owner) { onActions(it) }

        clicksViewModel.actionSheetClicks.observe(owner) {
            it.consume(viewModel::selectAction)
        }

        clicksViewModel.actionSheetCancelled.observe(owner) {
            it.consume { viewModel.cancelActions() }
        }
    }

    fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            dismiss()
        }
    }

    private fun onActions(items: List<ActionItem>) {
        if (items.isEmpty()) {
            dismiss()
        } else {
            ActionSheetDialogFragment.show(manager, items, style)
        }
    }

    private fun dismiss() = ActionSheetDialogFragment.dismiss(manager)
}
