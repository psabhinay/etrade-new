package com.etrade.mobilepro.trade.presentation.util

import android.content.res.Resources
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.orders.extensions.android.getLabel
import java.lang.StringBuilder

fun Resources.formatLegDetails(transactionType: TransactionType, symbol: String, quantity: String? = null): CharSequence {
    val displayTransactionType = getString(transactionType.getLabel())
    val builder = StringBuilder()
    if (displayTransactionType.isNotBlank() && displayTransactionType.isNotEmpty()) {
        builder.append("$displayTransactionType ")
    }
    if (!quantity.isNullOrEmpty() && quantity.isNotBlank()) {
        builder.append("$quantity ")
    }
    builder.append(symbol)
    return builder.toString()
}
