package com.etrade.mobilepro.orders.list.api

class OrderItemsAndKey(
    val orderItems: List<OrderItem> = emptyList(),
    val key: String? = null,
    val isLoading: Boolean = false,
    val errorThrowable: Throwable? = null
)
