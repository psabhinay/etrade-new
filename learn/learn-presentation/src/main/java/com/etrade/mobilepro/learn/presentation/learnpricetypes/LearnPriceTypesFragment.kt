package com.etrade.mobilepro.learn.presentation.learnpricetypes

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.toolbarView
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.learn.presentation.R
import com.etrade.mobilepro.learn.presentation.databinding.LearnPriceTypesFragmentBinding
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.trade.api.TradeOptionInfoType
import com.etrade.mobilepro.trade.presentation.util.SharedTradeViewModel
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.walkthrough.api.TradePlacingAction
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import javax.inject.Inject

class LearnPriceTypesFragment @Inject constructor(
    private val walkthroughViewModel: WalkthroughStatesViewModel,
    viewModelFactory: ViewModelProvider.Factory,
    private val tradeRouter: TradeRouter
) : Fragment(R.layout.learn_price_types_fragment) {

    private val sharedTradeViewModel: SharedTradeViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }
    private val viewModel: LearnPriceTypesViewModel by viewModels { viewModelFactory }
    private val binding by viewBinding(LearnPriceTypesFragmentBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        walkthroughViewModel.onAction(TradePlacingAction.PriceTypesDisplayed)
        setupClickListeners()
        bindViewModel()
    }

    private fun setupClickListeners() {
        with(binding.tradeNowTradeLater) {
            tradeNowButton.setOnClickListener {
                onDefaultPriceType()
                viewModel.marketOrLimitButtonPress(PriceType.MARKET, sharedTradeViewModel.securityType)
            }
            tradeLaterButton.setOnClickListener {
                viewModel.marketOrLimitButtonPress(PriceType.LIMIT, sharedTradeViewModel.securityType)
            }
            cancelIcon.setOnClickListener {
                onDefaultPriceType()
                activity?.onBackPressed()
            }
            otherPriceTypes.setOnClickListener {
                tradeRouter.openHelp(
                    findNavController(),
                    TradeOptionInfoType.PRICE_TYPE, true
                )
            }
        }
        binding.root.onBackKeyPressed()
    }

    private fun bindViewModel() {
        viewModel.applyFormParametersSignal.observe(
            viewLifecycleOwner,
            Observer {
                onApplyFormParametersSignal(it)
            }
        )
    }

    private fun onApplyFormParametersSignal(event: ConsumableLiveEvent<InputValuesSnapshot>) {
        event.consume {
            sharedTradeViewModel.injectFormInputValues(it)
            findNavController().popBackStack()
        }
    }

    override fun onPause() {
        super.onPause()
        activity?.toolbarView?.visibility = View.VISIBLE
    }

    override fun onResume() {
        super.onResume()
        activity?.toolbarView?.visibility = View.GONE
    }

    private fun View.onBackKeyPressed() {
        isFocusableInTouchMode = true
        requestFocus()
        setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                onDefaultPriceType()
            }
            false
        }
    }

    private fun onDefaultPriceType() {
        walkthroughViewModel.onAction(TradePlacingAction.PriceTypeEntered)
    }
}
