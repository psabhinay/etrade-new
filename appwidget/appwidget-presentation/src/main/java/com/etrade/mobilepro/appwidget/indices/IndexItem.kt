package com.etrade.mobilepro.appwidget.indices

import com.etrade.mobilepro.marketsapi.Index

internal data class IndexItem(
    val index: Index,
    val position: Int,
    val formattedIndexChange: String
)
