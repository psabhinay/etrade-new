package com.etrade.mobilepro.quote.screen.presentation.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteLayoutErrorRetryBinding
import com.etrade.mobilepro.quoteapi.QuoteItem

class NewsRetryViewHolder(private val binding: QuoteLayoutErrorRetryBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bindTo(item: QuoteItem.NewsRetry?, retryClickListener: (() -> Unit)?) {
        item?.let {
            binding.retryButton.setOnClickListener {
                retryClickListener?.invoke()
            }
            binding.errorText.text = item.errorMessage
        }
    }
}
