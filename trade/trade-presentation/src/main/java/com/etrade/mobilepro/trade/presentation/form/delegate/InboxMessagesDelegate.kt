package com.etrade.mobilepro.trade.presentation.form.delegate

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.inboxmessages.InboxMessagesStaticPayload
import com.etrade.mobilepro.inboxmessages.InboxMessagesView
import com.etrade.mobilepro.inboxmessages.MessageType
import com.etrade.mobilepro.inboxmessages.fetchInboxMessagesStaticViewLiveData
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.util.android.coroutine.DispatcherProvider
import javax.inject.Inject

class InboxMessagesDelegate @Inject constructor(
    inboxMessagesRepo: InboxMessagesRepository,
    dispatchers: DispatcherProvider,
) {
    val inboxMessages = fetchInboxMessagesStaticViewLiveData(
        type = MessageType.TRADE_INFORMATION,
        place = null,
        coroutineContext = dispatchers.IO,
        repository = inboxMessagesRepo
    )

    private val _showBanner = MutableLiveData<Boolean>()
    val showBanner: LiveData<Boolean> = _showBanner

    fun observeFormSymbolChanges(symbol: LiveData<String?>) {
        symbol.combineLatest(inboxMessages).map { it.hasHaltedSymbol() }.observeForever {
            _showBanner.value = it ?: false
        }
    }

    fun toggleTradeHaltInboxMessage(show: Boolean, view: InboxMessagesView) {
        val messages = inboxMessages.value?.messages ?: emptyList()
        val targetVisibility = if (show) { View.VISIBLE } else { View.GONE }
        view.apply {
            setMessages(messages)
            visibility = targetVisibility
        }
    }
}

internal fun Pair<String?, InboxMessagesStaticPayload>.hasHaltedSymbol(): Boolean {
    return this.second.messages?.any { message ->
        message.type == MessageType.TRADE_INFORMATION && message.symbol == this.first
    } ?: false
}
