package com.etrade.mobilepro.fundingprompt

import com.etrade.mobile.accounts.dto.AccountDto
import com.etrade.mobile.accounts.dto.InstitutionType
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt.FundingPromptNavHelper
import com.etrade.mobilepro.navigation.appendUuidQueryParam
import com.etrade.mobilepro.navigation.getNavDirections
import javax.inject.Inject

class DefaultFundingPromptNavHelper @Inject constructor() : FundingPromptNavHelper {

    override fun getNavigationDirections(appUrl: String, accountDto: AccountDto?): CtaEvent? {
        return getNavDirections(
            appendUuidQueryParam(appUrl, accountDto?.accountUuid),
            accountDto?.institutionType == InstitutionType.TELEBANK
        )
    }
}
