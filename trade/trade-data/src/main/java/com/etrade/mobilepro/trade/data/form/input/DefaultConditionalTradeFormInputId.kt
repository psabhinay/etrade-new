package com.etrade.mobilepro.trade.data.form.input

import com.etrade.mobilepro.trade.form.api.input.ConditionalTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId

object DefaultConditionalTradeFormInputId : ConditionalTradeFormInputId, TradeFormInputId by DefaultTradeFormInputId {
    override val accountId: String = "accountid"
    override val order1: String = "0"
    override val order2: String = "1"
    override val order3: String = "2"
}
