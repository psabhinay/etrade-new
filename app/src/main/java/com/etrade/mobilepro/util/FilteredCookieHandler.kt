package com.etrade.mobilepro.util

import org.slf4j.LoggerFactory
import java.net.CookieHandler
import java.net.URI

private val logger = LoggerFactory.getLogger(FilteredCookieHandler::class.java)
private const val LOGGED_OFF = "SMSESSION=LOGGEDOFF"
private const val SET_COOKIE = "Set-Cookie"

class FilteredCookieHandler(private val delegate: CookieHandler) : CookieHandler() {

    /*
     * The history behind this.
     *
     * Trade flow consists of two steps: previewing and placing an order. For stock trade: when we call preview, backend asks the app to set specific cookies to
     * distinct one order from another. When we place the order those cookies should be present in a place request, otherwise it will fail.
     *
     * There are cases when the may be another call(s) to another endpoints between preview and place calls of the trade flow. Those calls may want to use and
     * set their own cookies and override the ones that were set by the preview call.
     *
     * In the observed scenario we have calls to get a quote's details which by itself doesn't require any cookies to be executed successfully but overrides the
     * cookies set by the preview call. FilteredCookieHandler suppress cookie updating by the quote's details call.
     *
     * 02/24/2021 update: removed getMobileQuote from forbiddenUri list due to seeing problems with SMSESSION cookie, now allowing this call to set SMSESSION
     * cookie. Will need to disallow it only on preview order screen when preview order screen makes quote call again.
     */

    private val forbiddenUriPaths: Set<String> = setOf(
        "/e/t/mobile/GetMarketIndice",
        "/e/t/mobile/GetMovingMarket",
        "/e/t/mobile/GetMostActives",
        "/e/t/mobile/getfuturequotes"
    )

    override fun get(uri: URI?, requestHeaders: MutableMap<String, MutableList<String>>?): Map<String, MutableList<String>> {
        requireNotNull(uri)

        return delegate.get(uri, requestHeaders)
    }

    override fun put(uri: URI?, responseHeaders: MutableMap<String, MutableList<String>>?) {
        requireNotNull(uri)

        logger.debug("uri: $uri")
        responseHeaders?.forEach {
            logger.debug("response header: ${it.key}, value: ${it.value}")
        }

        responseHeaders?.get(SET_COOKIE)?.let {
            it.removeIf { cookie -> cookie.contains(LOGGED_OFF) }
        }

        if (uri.isAllowed) {
            delegate.put(uri, responseHeaders)
        }
    }

    private val URI.isAllowed: Boolean
        get() = forbiddenUriPaths.none { path.startsWith(it) }
}
