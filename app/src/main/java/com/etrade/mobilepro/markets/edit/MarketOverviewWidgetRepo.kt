package com.etrade.mobilepro.markets.edit

import android.content.res.Resources
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.R
import com.etrade.mobilepro.dynamicui.DynamicViewType
import com.etrade.mobilepro.home.overview.edit.BaseOverviewWidgetRepo
import com.etrade.mobilepro.home.overview.edit.OverviewWidget
import com.etrade.mobilepro.session.api.User
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject
import javax.inject.Qualifier

private const val ENABLED_WIDGETS_STORAGE_KEY = "ENABLED_MARKET_WIDGETS_STORAGE_KEY"
private const val SAVED_WIDGETS_STORAGE_KEY = "SAVED_MARKET_WIDGETS_STORAGE_KEY"

private const val ENABLED_WIDGETS_INTERNATIONAL_USER_STORAGE_KEY = "ENABLED_WIDGETS_INTERNATIONAL_USER_STORAGE_KEY"
private const val SAVED_WIDGETS_INTERNATIONAL_USER_STORAGE_KEY = "SAVED_WIDGETS_INTERNATIONAL_USER_STORAGE_KEY"

@Qualifier
annotation class MarketsOverview

class DefaultMarketsOverviewWidgetRepo @Inject constructor(
    storage: KeyValueStorage,
    private val resources: Resources,
    private val user: User
) : BaseOverviewWidgetRepo(storage) {

    override val allWidgets: Flow<Map<String, OverviewWidget>>
        get() = flowOf(
            if (user.session?.isInternationalUser == true) {
                mapOf(
                    DynamicViewType.MARKET_MOVERS.dtoName to OverviewWidget(DynamicViewType.MARKET_MOVERS.dtoName, resources.getString(R.string.market_movers)),
                    DynamicViewType.EARNINGS_TODAY.dtoName to OverviewWidget(
                        DynamicViewType.EARNINGS_TODAY.dtoName,
                        resources.getString(R.string.today_top_earnings_customize_view_title)
                    ),
                    DynamicViewType.MARKET_FUTURES.dtoName to OverviewWidget(DynamicViewType.MARKET_FUTURES.dtoName, resources.getString(R.string.futures)),
                    DynamicViewType.NEWS_TODAY.dtoName to OverviewWidget(DynamicViewType.NEWS_TODAY.dtoName, resources.getString(R.string.news)),
                    DynamicViewType.BROADER_INDICES.dtoName to OverviewWidget(
                        DynamicViewType.BROADER_INDICES.dtoName,
                        resources.getString(R.string.broader_market_indices_title)
                    )
                )
            } else {
                mapOf(
                    DynamicViewType.BLOOMBERG_TV.dtoName to OverviewWidget(DynamicViewType.BLOOMBERG_TV.dtoName, resources.getString(R.string.bloombergtv)),
                    DynamicViewType.MARKET_MOVERS.dtoName to OverviewWidget(DynamicViewType.MARKET_MOVERS.dtoName, resources.getString(R.string.market_movers)),
                    DynamicViewType.EARNINGS_TODAY.dtoName to OverviewWidget(
                        DynamicViewType.EARNINGS_TODAY.dtoName,
                        resources.getString(R.string.today_top_earnings_customize_view_title)
                    ),
                    DynamicViewType.MARKET_FUTURES.dtoName to OverviewWidget(DynamicViewType.MARKET_FUTURES.dtoName, resources.getString(R.string.futures)),
                    DynamicViewType.NEWS_TODAY.dtoName to OverviewWidget(DynamicViewType.NEWS_TODAY.dtoName, resources.getString(R.string.news)),
                    DynamicViewType.BROADER_INDICES.dtoName to OverviewWidget(
                        DynamicViewType.BROADER_INDICES.dtoName,
                        resources.getString(R.string.broader_market_indices_title)
                    )
                )
            }
        )

    override val defaultWidgetsOrder: List<String>
        get() =
            if (user.session?.isInternationalUser == true) {
                listOf(
                    DynamicViewType.MARKET_MOVERS.dtoName,
                    DynamicViewType.EARNINGS_TODAY.dtoName,
                    DynamicViewType.MARKET_FUTURES.dtoName,
                    DynamicViewType.NEWS_TODAY.dtoName,
                    DynamicViewType.BROADER_INDICES.dtoName
                )
            } else {
                listOf(
                    DynamicViewType.BLOOMBERG_TV.dtoName,
                    DynamicViewType.MARKET_MOVERS.dtoName,
                    DynamicViewType.EARNINGS_TODAY.dtoName,
                    DynamicViewType.MARKET_FUTURES.dtoName,
                    DynamicViewType.NEWS_TODAY.dtoName,
                    DynamicViewType.BROADER_INDICES.dtoName
                )
            }

    override val enabledWidgetsStorageKey: String
        get() = if (user.session?.isInternationalUser == true) {
            ENABLED_WIDGETS_INTERNATIONAL_USER_STORAGE_KEY
        } else {
            ENABLED_WIDGETS_STORAGE_KEY
        }

    override val savedWidgetsStorageKey: String
        get() = if (user.session?.isInternationalUser == true) {
            SAVED_WIDGETS_INTERNATIONAL_USER_STORAGE_KEY
        } else {
            SAVED_WIDGETS_STORAGE_KEY
        }
}
