package com.etrade.mobilepro.chat.presentation.message.viewHolder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.presentation.databinding.ChatItemMessageUserBinding

class ChatMessageUserViewHolder(private val binding: ChatItemMessageUserBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: ChatMessage.User) {
        binding.userMessageText.text = item.message
    }
}
