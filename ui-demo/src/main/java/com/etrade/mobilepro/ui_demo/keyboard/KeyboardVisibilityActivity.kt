package com.etrade.mobilepro.ui_demo.keyboard

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.etrade.mobilepro.ui_demo.databinding.ActivityKeyboardVisibilityBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.view.buildKeyboardVisibilityMonitor

class KeyboardVisibilityActivity: AppCompatActivity() {

    private val binding by viewBinding(ActivityKeyboardVisibilityBinding::inflate)

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        buildKeyboardVisibilityMonitor {
            targetView = binding.tvText
            lifecycleOwner = this@KeyboardVisibilityActivity
            listener = { isKeyboardVisible ->
                val visibilityText = if (isKeyboardVisible) { "Visible" } else { "Not visible" }
                binding.tvText.text = "Keyboard is: $visibilityText"
            }
        }?.startMonitoring()
    }
}
