package com.etrade.mobilepro.appwidget.data.settings

import com.etrade.mobilepro.appwidget.api.settings.WidgetId
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettings
import com.etrade.mobilepro.appwidget.api.settings.WidgetSettingsRepo
import com.etrade.mobilepro.util.KeyValueStore
import com.etrade.mobilepro.util.Serializer
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import javax.inject.Inject

private const val KEY_SELECTED_WATCH_LIST_ID = "WidgetViewStateRepoImpl.key.selected_watchlist_id"

// should be singleton (or refactored to provide mutexes)
class DefaultWidgetSettingsRepo @Inject constructor(
    private val keyValueStorage: KeyValueStore,
    private val settingsSerializer: Serializer<WidgetSettings>,
    private val widgetIdSerializer: Serializer<WidgetId>
) : WidgetSettingsRepo {
    private val mutex = Mutex()
    override suspend fun loadSettings(id: WidgetId): WidgetSettings = mutex.withLock { loadSettingsBlocking(id) }
    override suspend fun saveSettings(id: WidgetId, settings: WidgetSettings) = mutex.withLock { saveSettingsBlocking(id, settings) }
    override suspend fun deleteSettings(id: WidgetId) = mutex.withLock { deleteSettingsBlocking(id) }

    override fun loadSettingsBlocking(id: WidgetId): WidgetSettings {
        val settings = runCatching {
            val key = widgetIdSerializer.serialize(id)
            val stored = keyValueStorage.getString(key)
            stored?.let { settingsSerializer.deserialize(it) }
        }.getOrNull() ?: WidgetSettings(watchListId = DEVICE_WATCHLIST_ID)
        return settings.copy(watchListId = getSelectedWatchlistId())
    }

    override fun saveSettingsBlocking(id: WidgetId, settings: WidgetSettings): Boolean {
        val result = runCatching {
            val key = widgetIdSerializer.serialize(id)
            val value = settingsSerializer.serialize(settings)
            keyValueStorage.putString(key, value)
        }.isSuccess
        if (result) {
            setSelectedWatchlistId(settings.watchListId)
        }
        return result
    }

    override fun deleteSettingsBlocking(id: WidgetId): Boolean {
        setSelectedWatchlistId(DEVICE_WATCHLIST_ID)
        return saveSettingsBlocking(id, WidgetSettings(watchListId = DEVICE_WATCHLIST_ID))
    }

    // these two methods are left here because of backward compatibility reasons
    private fun getSelectedWatchlistId(): String = keyValueStorage.getString(KEY_SELECTED_WATCH_LIST_ID, null) ?: DEVICE_WATCHLIST_ID

    private fun setSelectedWatchlistId(id: String) {
        keyValueStorage.putString(KEY_SELECTED_WATCH_LIST_ID, id)
    }
}
