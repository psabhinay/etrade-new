package com.etrade.mobilepro.dynamic.form.ui.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.dynamic.form.ui.SelectInputView

internal object SelectInputViewFactory : ComponentViewFactory<SelectInput, SelectInputView> {
    override fun createView(context: Context): SelectInputView = SelectInputView(context)
}
