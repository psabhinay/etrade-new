package com.etrade.mobilepro.documents.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.documents.R
import com.etrade.mobilepro.documents.StatefulDocument
import com.etrade.mobilepro.documents.databinding.DocumentsItemAdapterBinding

internal class DocumentViewHolder(
    private val binding: DocumentsItemAdapterBinding,
    listener: (StatefulDocument) -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    private var item: StatefulDocument? = null

    init {
        binding.root.setOnClickListener {
            item?.let(listener)
        }
        binding.retryBtn.setOnClickListener {
            item?.let {
                if (it.status is StatefulDocument.Status.Failed) {
                    listener.invoke(it)
                }
            }
        }
    }

    fun bind(item: StatefulDocument, previousItem: StatefulDocument?) {
        this.item = item
        binding.title.text = item.document.title

        bindItemStatus(item)
        bindItemHeader(item, previousItem)
    }

    private fun bindItemStatus(item: StatefulDocument) {
        when (item.status) {
            StatefulDocument.Status.Loading -> {
                binding.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                binding.progressBar.visibility = View.VISIBLE
                binding.retryBtn.visibility = View.GONE
            }
            StatefulDocument.Status.Available -> {
                binding.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_right, 0)
                binding.progressBar.visibility = View.GONE
                binding.retryBtn.visibility = View.GONE
            }
            StatefulDocument.Status.Unavailable -> {
                binding.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_right, 0)
                binding.retryBtn.visibility = View.GONE
                binding.progressBar.visibility = View.GONE
            }
            is StatefulDocument.Status.Failed -> {
                binding.title.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
                binding.retryBtn.visibility = View.VISIBLE
                binding.progressBar.visibility = View.GONE
            }
        }
    }

    private fun bindItemHeader(item: StatefulDocument, previousItem: StatefulDocument?) {
        if (item.document.headerTitle == previousItem?.document?.headerTitle) {
            binding.headerGroup.visibility = View.GONE
        } else {
            binding.headerTitle.text = item.document.headerTitle
            binding.headerGroup.visibility = View.VISIBLE
        }
    }
}
