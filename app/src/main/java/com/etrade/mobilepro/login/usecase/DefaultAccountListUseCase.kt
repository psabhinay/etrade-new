package com.etrade.mobilepro.login.usecase

import com.etrade.mobilepro.accountslist.api.AccountsListRepo
import com.etrade.mobilepro.common.di.PersonalNotifications
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.msaccounts.api.MsPreferencesRepo
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.Resource
import kotlinx.coroutines.rx2.awaitLast
import org.slf4j.LoggerFactory
import javax.inject.Inject

class DefaultAccountListUseCase @Inject constructor(
    private val accountsListRepo: AccountsListRepo,
    private val msPreferencesRepo: MsPreferencesRepo,
    @PersonalNotifications private val statusRepo: ReadStatusRepo,
    private val userViewModel: UserViewModel
) : AccountListUseCase {
    private val logger = LoggerFactory.getLogger(DefaultAccountListUseCase::class.java)

    override suspend fun execute(parameter: Unit): Boolean = isLoadingAccountsSuccessful()

    private suspend fun isLoadingAccountsSuccessful(): Boolean {
        val result = accountsListRepo.getScreen()
            .doOnNext {
                if (it is Resource.Failed) {
                    logger.error("Accounts retrieval failed", it.error)
                }
            }
            .runCatching { awaitLast() }
            .onSuccess {
                it.data?.apply {
                    msPreferencesRepo.updateMSUserPreferences(msUserPreferences)
                    userViewModel.hasNewOrUnreadPersonalNotifications =
                        personalNotifications.any { notification ->
                            statusRepo.wasRead(notification.identifier).not()
                        }
                }
            }

        return result.isSuccess && result.getOrThrow() !is Resource.Failed
    }
}
