package com.etrade.mobilepro.documents

enum class DocumentType(val deepLinkPath: String? = null) {
    STATEMENTS,
    TAX_DOCS("/TaxDocs")
}
