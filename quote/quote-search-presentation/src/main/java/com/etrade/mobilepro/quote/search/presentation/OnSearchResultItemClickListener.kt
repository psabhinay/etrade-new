package com.etrade.mobilepro.quote.search.presentation

import com.etrade.mobilepro.searchingapi.items.SearchResultItem

interface OnSearchResultItemClickListener {
    fun onSearchResultItemClicked(item: SearchResultItem)
}
