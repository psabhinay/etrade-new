package com.etrade.mobilepro.login.dagger

import com.etrade.mobilepro.login.api.repo.LoginDataSource
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.login.data.DefaultLoginDataSource
import com.etrade.mobilepro.login.data.LoginPreferencesRepoImp
import com.etrade.mobilepro.login.presentation.DefaultLoginCtaViewProvider
import com.etrade.mobilepro.login.presentation.cta.LoginViewCtaListener
import com.etrade.mobilepro.login.presentation.cta.LoginViewProvider
import com.etrade.mobilepro.login.presentation.router.LoginAuxRouter
import com.etrade.mobilepro.login.router.DefaultLoginAuxRouter
import com.etrade.mobilepro.login.usecase.AccountListUseCase
import com.etrade.mobilepro.login.usecase.DefaultAccountListUseCase
import com.etrade.mobilepro.login.usecase.DefaultLoginResultUseCase
import com.etrade.mobilepro.login.usecase.DisableBiometricLoginIfRequiredUseCase
import com.etrade.mobilepro.login.usecase.DisableBiometricLoginIfRequiredUseCaseImpl
import com.etrade.mobilepro.login.usecase.LogInUseCase
import com.etrade.mobilepro.login.usecase.LogInUseCaseImp
import com.etrade.mobilepro.login.usecase.LoginResultUseCase
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class LoginBinderModule {

    @Binds
    internal abstract fun DisableBiometricLoginIfRequiredUseCase(
        useCaseImpl: DisableBiometricLoginIfRequiredUseCaseImpl
    ): DisableBiometricLoginIfRequiredUseCase

    @Binds
    internal abstract fun bindLoginDataSource(defaultLoginDataSource: DefaultLoginDataSource): LoginDataSource

    @Binds
    internal abstract fun bindLoginResultUseCase(loginUseCase: DefaultLoginResultUseCase): LoginResultUseCase

    @Binds
    internal abstract fun bindAccountListUseCase(accountListCase: DefaultAccountListUseCase): AccountListUseCase

    @Binds
    internal abstract fun bindLogInUseCase(logInUseCase: LogInUseCaseImp): LogInUseCase

    @Binds
    @Singleton
    internal abstract fun bindLoginPreferences(userPreferences: LoginPreferencesRepoImp): LoginPreferencesRepo

    @Binds
    internal abstract fun bindLoginViewCtaListener(loginCtaViewListener: DefaultLoginCtaViewProvider): LoginViewCtaListener

    @Binds
    internal abstract fun bindLoginViewHelper(loginViewHelper: DefaultLoginCtaViewProvider): LoginViewProvider

    @Binds
    internal abstract fun bindLoginAuxRouter(router: DefaultLoginAuxRouter): LoginAuxRouter
}
