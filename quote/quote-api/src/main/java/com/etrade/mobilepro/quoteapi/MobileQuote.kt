package com.etrade.mobilepro.quoteapi

import com.etrade.eo.core.util.DateFormattingUtils
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.PricingParameterCode
import com.etrade.mobilepro.quoteapi.MobileQuote.Companion.formatTimeStamp
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import java.util.concurrent.TimeUnit

data class MobileQuote(
    val symbol: String = "",
    val description: String = "",
    val exchangeCode: String? = null,
    val exchangeName: String = "",
    val typeCode: InstrumentType = InstrumentType.UNKNOWN,
    val openPrice: String = "",
    val previousClose: String = "",
    val dayHigh: String = "",
    val dayLow: String = "",
    val delayedOrRtq: Int = 0,
    val timeZone: String = "",
    val lastTradeTime: String = "",
    val specialEvents: QuoteSpecialEvents = QuoteSpecialEvents(),
    val week52Low: String = "",
    val week52High: String = "",
    val week52LowDate: DateStruct = DateStruct(),
    val week52HighDate: DateStruct = DateStruct(),
    val marketCap: String = "",
    val sharesOutstanding: String = "",
    val averageVolume: String = "",
    val pe: String = "",
    val eps: String = "",
    val dividendYield: String = "",
    val declaredDividend: String = "",
    val dividentPayDate: DateStruct = DateStruct(),
    val beta: String = "",
    val thirtyDaySecYield: String? = null,
    val exDividendDate: DateStruct = DateStruct(),
    val subSecType: String = "",
    val hasMiniOptions: Boolean = false,
    val msRating: String = "",
    val fundCategory: String = "",
    val showDrip: Int = 0,
    val commissionFree: Boolean = false,
    val isOptionable: Boolean = false,
    val cusip: String = "",
    val overnightTrading: Int = 0,
    val oneYYield: String = "",
    val bidExchange: String = "",
    val askExchange: String = "",
    val tradeExchange: String = "",
    val tradeSize: String = "",
    val allStar: Boolean = false,
    val exchangeTradeNotes: Boolean = false,
    val leveragedFund: Boolean = false,
    val inverseFund: Boolean = false,
    val commodity: Boolean = false,
    val overnightExchange: Boolean = false,
    val dst: Boolean = false,
    val nextEarningDate: DateStruct = DateStruct(),
    val quotePrice: QuotePrice = QuotePrice(),
    var extendedQuoteHrDetail: QuotePrice? = null,
    val ahFlagHasNumericData: Boolean = false,
    val availability: String? = null,
    val underlyingSymbol: String = "",
    var searchSymbol: String = "",
    var searchSymbolDescription: String = "",
    val rho: String = "",
    val vega: String = "",
    val theta: String = "",
    val gamma: String = "",
    val deliverableContract: String = "",
    val daysToExpire: String = "",
    val intrinsicValue: String = "",
    val timePremium: String = "",
    val openInterest: String = "",
    var quoteDescription: String = "",
    val delta: String = "",
    val impliedVolatility: String = "",
    var underLyingSymbolDescription: String = "",
    var underLyingExchangeCode: String = "",
    var underLyingExchangeName: String = "",
    var underLyingLastPrice: String = "",
    var underLyingChange: String = "",
    var underLyingPercentChange: String = "",
    var underLyingVolume: String = "",
    var underLyingLastTradeTime: Long? = null,
    var underlyingTypeCode: InstrumentType = InstrumentType.UNKNOWN,
    var fundDetails: MutualFundDetails? = null,
    val mutualFundPerformance: MutualFundPerformance? = null,
    val affiliatedStatusCode: String? = null
) {
    fun addQueryData(symbol: SearchResultItem.Symbol) {
        searchSymbol = if (typeCode.isOption) {
            this.underlyingSymbol
        } else {
            this.symbol
        }

        searchSymbolDescription = symbol.description.ifEmpty {
            if (typeCode.isOption) {
                this.underLyingSymbolDescription
            } else {
                this.description
            }
        }
        quoteDescription = if (typeCode.isOption) {
            underLyingSymbolDescription
        } else {
            description
        }
    }

    companion object {
        fun formatTimeStamp(
            timeInMillis: Long,
            isMutualFund: Boolean,
            templateResources: QuoteTemplateResources,
            excludeDate: Boolean = false
        ): String {
            val formattedDate = if (isMutualFund) {
                DateFormattingUtils.formatToShortDate(timeInMillis)
            } else if (excludeDate) {
                DateFormattingUtils.formatTimeWithoutSecondsWithShowingTimezone(timeInMillis)
            } else {
                DateFormattingUtils.formatFullShortTimeDateWithSlashes(timeInMillis)
            }

            return String.format(
                if (isMutualFund) {
                    templateResources.nasdaqAsOfDateTimeFormat
                } else {
                    templateResources.asOfDateTimeFormat
                },
                formattedDate
            )
        }
    }
}

data class QuoteSpecialEvents(
    val nextEarningData: Boolean = false,
    val dividendPayDate: Boolean = false,
    val exDividendDate: Boolean = false,
    val splitRatio: Boolean = false,
    val tradeHalt: Boolean = false,
    val daysToExpiration: Boolean = false,
    val qtyFactorSplitFrom: Int = 0,
    val qtyFactorSplitTo: Int = 0
)

data class DateStruct(
    val day: String = "",
    val month: String = "",
    val year: String = ""
)

data class QuotePrice(
    val lastPrice: String = "",
    val lastTradeSize: String = "",
    val change: String = "",
    val percentChange: String = "",
    val bid: String = "",
    val bidSize: String = "",
    val ask: String = "",
    val askSize: String = "",
    val volume: String = "",
    val timeOfLastTrade: Long? = null
)

data class MutualFundDetails(
    val netAssetValue: String?,
    val publicOfferPrice: String?,
    val previousClose: String?,
    val familyName: String?,
    val timeOfLastTradeInMillis: Long?,
    val fundClassInfoList: List<FundClassInfo>?,
    val hasNoTransactionsFee: Boolean,
    val isAllStarFund: Boolean,
    val morningStarAsOfDate: String?,
    val morningStarCategory: String?,
    val morningStarRating: Int?,
    val numFundsInOverallRating: Int?,
    val threeYearMorningStarRating: Int?,
    val numFundsInMsRatingFor3y: Int?,
    val fiveYearMorningStarRating: Int?,
    val numFundsInMsRatingFor5y: Int?,
    val tenYearMorningStarRating: Int?,
    val numFundsInMsRatingFor10y: Int?,
    val availability: String?,
    val qtrlyPerformanceAsOfDate: String?,
    val quarterlyTrailingReturnYtd: String?,
    val quarterlyTrailingReturn1y: String?,
    val quarterlyTrailingReturn5y: String?,
    val quarterlyTrailingReturn10y: String?,
    val netExpenseRatio: String?,
    val grossExpenseRatio: String?,
    val totalNetAssets: String?,
    val inceptionDate: String?,
    val thirtyDaySecYield: String?,
    val oneYearSecYield: String?,
    val initialInvestment: String?,
    val subsequentInvestment: String?,
    val initialIra: String?,
    val subsequentIra: String?,
    val salesCharge: String?,
    val fundRedemptionFee: String?,
    val etradeTransactionFee: String?,
    val mutualFundPerformance: MutualFundPerformance?,
    val fundDescription: String?,
    val etradeEarlyRedemptionFee: String?,
    val additionalFeeDetails: Pair<Boolean?, List<FeeDetail>>,
    val managementFee: String?,
    val administrationFee: String?,
    val twelveBOneFee: String?,
    val sevenDayCurrentYield: String?,
    val annualTotalReturn: String?,
    val weightedAvgMaturity: String?
)

data class MutualFundPerformance(
    val fundCategoryName: String?,
    val nonAdjMonthlyTrailingReturnYtd: String?,
    val nonAdjMonthlyTrailingReturn1y: String?,
    val nonAdjMonthlyTrailingReturn5y: String?,
    val nonAdjMonthlyTrailingReturn10y: String?,
    val adjMonthlyTrailingReturnYtd: String?,
    val adjMonthlyTrailingReturn1y: String?,
    val adjMonthlyTrailingReturn5y: String?,
    val adjMonthlyTrailingReturn10y: String?,
    val nonAdjQuarterlyTrailingReturnYtd: String?,
    val nonAdjQuarterlyTrailingReturn1y: String?,
    val nonAdjQuarterlyTrailingReturn5y: String?,
    val nonAdjQuarterlyTrailingReturn10y: String?,
    val adjQuarterlyTrailingReturnYtd: String?,
    val adjQuarterlyTrailingReturn1y: String?,
    val adjQuarterlyTrailingReturn5y: String?,
    val adjQuarterlyTrailingReturn10y: String?,
    val monthlyTrailingReturnYtd: String?,
    val monthlyTrailingReturn1y: String?,
    val monthlyTrailingReturn5y: String?,
    val monthlyTrailingReturn10y: String?,
    val quarterlyTrailingReturnYtd: String?,
    val quarterlyTrailingReturn1y: String?,
    val quarterlyTrailingReturn5y: String?,
    val quarterlyTrailingReturn10y: String?,
    val monthlyLoadAdjTrailingReturnYtd: String?,
    val monthlyLoadAdjTrailingReturn1y: String?,
    val monthlyLoadAdjTrailingReturn5y: String?,
    val monthlyLoadAdjTrailingReturn10y: String?,
    val quarterlyLoadAdjTrailingReturnYtd: String?,
    val quarterlyLoadAdjTrailingReturn1y: String?,
    val quarterlyLoadAdjTrailingReturn5y: String?,
    val quarterlyLoadAdjTrailingReturn10y: String?,
    val inceptionDate: String?,
    val performanceAsOfDate: String?,
    val quarteryPerformanceAsOfDate: String?,
    val monthlyLoadAdjTrailingReturnSinceInception: String?,
    val monthlyTrailingReturnSinceInception: String?,
    val quarterlyLoadAdjTrailingReturnSinceInception: String?,
    val quarterlyTrailingReturnSinceInception: String?
)

data class MutualFundTradeDetails(
    val productFundDetail: ProductFundDetail?,
    val frontLoadText: String?,
    val timeOfLastTradeInMillis: Long?,
    val initialInvestment: String?,
    val subsequentInvestment: String?,
    val initialIra: String?,
    val subsequentIra: String?,
    val salesCharge: String?,
    val fundRedemptionFee: String?,
    val transactionFee: String?,
    val fundDescription: String?,
    val etradeEarlyRedemptionFee: String?,

    val netAssetValue: String?,
    val change: String?,
    val changePercent: String?,
    val annualTotalReturn: String?,
    val sevenDayCurrentYield: String?,
    val weightedAvgMaturity: String?,

    val hasEarlyRedemptionFee: Boolean,
    val isFrontEndFeesFund: Boolean,
    val isSaleChargedFund: Boolean,
    val deferredSalesCharges: List<FundsFee>?,
    val frontEndSalesCharges: List<FundsFee>?,
    val positionDetails: List<MutualFundPosition>,
    val affiliatedStatusCode: String?,

    val isFundOwned: Boolean
) {
    val isNoLoadNoTransFeeFund: Boolean
        get() = !isFrontEndFeesFund && !isSaleChargedFund && (productFundDetail?.typeCode == InstrumentType.MF) && "None" == transactionFee

    val isLoadWaived: Boolean = productFundDetail?.let {
        it.isFrontLoadWaived && PricingParameterCode.FRONT_END_LOAD == it.pricingParameterCd
    } ?: false
}

data class MutualFundPosition(
    val marketValue: String,
    val quantity: String,
    val type: MutualPositionType
)

data class ProductFundDetail(
    val fundName: String?,
    val pricingParameterCd: PricingParameterCode,
    val cusip: String?,
    val streetStatus: String?,
    val ordCutOffTime: String?,
    val isFrontLoadWaived: Boolean,
    val typeCode: InstrumentType,
    val symbol: String
)

fun MobileQuote.getTimeStamp(
    templateResources: QuoteTemplateResources,
    excludeDate: Boolean = false
): String? {
    return timeOfLastTradeUnixSeconds()?.let {
        formatTimeStamp(TimeUnit.SECONDS.toMillis(it), typeCode.isMutualFundOrMoneyMarketFund, templateResources, excludeDate)
    }
}

fun MobileQuote.getTimeStamp(isExtendedHours: Boolean): Long? =
    if (isExtendedHours) {
        timeOfLastTradeUnixSeconds()
    } else {
        quotePrice.timeOfLastTrade
    }

fun MobileQuote.timeOfLastTradeUnixSeconds(): Long? =
    extendedQuoteHrDetail?.takeIf { !typeCode.isOption }?.run { timeOfLastTrade } ?: quotePrice.timeOfLastTrade

fun MobileQuote.isEtfSymbol(): Boolean = typeCode == InstrumentType.EQ && subSecType == "ETF"

fun MobileQuote.isAffiliatedProduct() = affiliatedStatusCode == "Y"

fun MutualFundTradeDetails.isAffiliatedProduct() = affiliatedStatusCode == "Y"
