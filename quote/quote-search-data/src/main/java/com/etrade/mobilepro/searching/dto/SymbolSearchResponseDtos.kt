package com.etrade.mobilepro.searching.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SymbolSearchDto(
    @Json(name = "data")
    val data: SymbolSearchResponseDto?
)

@JsonClass(generateAdapter = true)
data class SymbolSearchResponseDto(
    @Json(name = "response")
    val payload: List<SymbolDto>?
)

@JsonClass(generateAdapter = true)
data class SymbolDto(
    @Json(name = "symbol")
    val name: String?,
    @Json(name = "instrumentId")
    val instrumentId: String?,
    @Json(name = "description")
    val description: String?,
    @Json(name = "type")
    val type: String?,
    @Json(name = "secSubType")
    val securityType: String?
)
