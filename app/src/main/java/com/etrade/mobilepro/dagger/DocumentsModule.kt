package com.etrade.mobilepro.dagger

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.documents.DefaultDocumentArgumentsHelper
import com.etrade.mobilepro.documents.DefaultDocumentsRouter
import com.etrade.mobilepro.documents.DocumentStorage
import com.etrade.mobilepro.documents.DocumentsFragment
import com.etrade.mobilepro.documents.DocumentsRepository
import com.etrade.mobilepro.documents.data.DefaultDocumentStorage
import com.etrade.mobilepro.documents.data.DefaultDocumentsRepository
import com.etrade.mobilepro.documents.data.decoding.DefaultDocumentDecoder
import com.etrade.mobilepro.documents.data.decoding.DefaultDocumentFilenameGenerator
import com.etrade.mobilepro.documents.data.decoding.DocumentDecoder
import com.etrade.mobilepro.documents.data.decoding.DocumentFilenameGenerator
import com.etrade.mobilepro.documents.details.DocumentDetailsFragment
import com.etrade.mobilepro.documents.details.DocumentDetailsViewModel
import com.etrade.mobilepro.documents.router.DocumentArgumentsHelper
import com.etrade.mobilepro.documents.router.DocumentsRouter
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
interface DocumentsModule {
    @Binds
    fun bindDocumentRouter(router: DefaultDocumentsRouter): DocumentsRouter

    @Binds
    fun bindDocumentsArgumentHelper(helper: DefaultDocumentArgumentsHelper): DocumentArgumentsHelper

    @Binds
    @Singleton
    fun bindDocumentStorage(storage: DefaultDocumentStorage): DocumentStorage

    @Binds
    fun bindDocumentsRepository(repository: DefaultDocumentsRepository): DocumentsRepository

    @Binds
    fun bindDocumentFilenameGenerator(generator: DefaultDocumentFilenameGenerator): DocumentFilenameGenerator

    @Binds
    @IntoMap
    @FragmentKey(DocumentsFragment::class)
    fun bindDocumentsFragment(fragment: DocumentsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(DocumentDetailsFragment::class)
    fun bindDocumentDetailsFragment(fragment: DocumentDetailsFragment): Fragment

    @Binds
    @IntoMap
    @ViewModelKey(DocumentDetailsViewModel::class)
    fun bindDocumentDetailsViewModel(viewModel: DocumentDetailsViewModel): ViewModel

    companion object {
        @Provides
        fun provideDocumentDecoder(context: Context): DocumentDecoder = DefaultDocumentDecoder(cacheDir = "${context.filesDir}/docs")
    }
}
