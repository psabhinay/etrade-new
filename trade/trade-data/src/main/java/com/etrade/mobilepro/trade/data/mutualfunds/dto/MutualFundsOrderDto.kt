package com.etrade.mobilepro.trade.data.mutualfunds.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
data class BuyingPower(
    @Json(name = "currentBp")
    val currentBp: BigDecimal?,
    @Json(name = "currentOor")
    val currentOor: BigDecimal?,
    @Json(name = "currentNetBp")
    val currentNetBp: BigDecimal?,
    @Json(name = "currentOrderImpact")
    val currentOrderImpact: BigDecimal?,
    @Json(name = "netBp")
    val netBp: BigDecimal?
)

@JsonClass(generateAdapter = true)
data class BuyingPowerDetails(
    @Json(name = "nonMarginable")
    val nonMarginable: BuyingPower? = null,
    @Json(name = "marginable")
    val marginable: BuyingPower? = null
)

@JsonClass(generateAdapter = true)
data class BuyingPowerSummary(
    @Json(name = "cashBpDetails")
    val cashBpDetails: BuyingPowerDetails? = null,

    @Json(name = "marginBpDetails")
    val marginBpDetails: BuyingPowerDetails? = null,

    @Json(name = "lmiPdtBpDetails")
    val lmiPdtBpDetails: BuyingPowerDetails? = null,

    @Json(name = "lmiBpDetails")
    val lmiBpDetails: BuyingPowerDetails? = null,

    @Json(name = "dtBpDetails")
    val dtBpDetails: BuyingPowerDetails? = null
)

@JsonClass(generateAdapter = true)
data class FundOrderId(
    @Json(name = "id")
    val id: String = "",
    @Json(name = "cashMargin")
    val cashMargin: String = ""
)

@JsonClass(generateAdapter = true)
data class LegDetails(
    @Json(name = "symbol")
    val symbolRaw: String? = "",

    @Json(name = "transaction")
    val transaction: String? = "1",

    @Json(name = "typeCode")
    val typeCode: String? = "",

    @Json(name = "quantity")
    val quantityRaw: MutualFundQuantity? = null,

    @Json(name = "quantityType")
    val quantityTypeRaw: String? = null
)

@JsonClass(generateAdapter = true)
data class MutualFundQuantity(
    @Json(name = "cash")
    val cash: String? = "",
    @Json(name = "margin")
    val margin: String? = ""
)
