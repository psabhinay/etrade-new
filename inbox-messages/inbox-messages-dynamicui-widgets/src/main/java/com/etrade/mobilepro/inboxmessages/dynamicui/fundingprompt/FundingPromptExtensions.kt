package com.etrade.mobilepro.inboxmessages.dynamicui.fundingprompt

import com.etrade.mobilepro.completeview.AccountSummaryLayout
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.inboxmessages.dynamicui.api.InboxMessagesWidgetPlace
import com.etrade.mobilepro.inboxmessages.dynamicui.api.WithAccountDtoInfo

fun List<GenericLayout>.rearrangeFundingPromptsToTheirAccountSummaries(place: InboxMessagesWidgetPlace?): List<GenericLayout> {
    val fundingPrompts = this.filter { it.viewModelClass == FundingPromptViewModel::class.java }
    if (fundingPrompts.isEmpty()) {
        return this
    }
    return when (place) {
        InboxMessagesWidgetPlace.COMPLETE_VIEW -> {
            val resultingList = mutableListOf<GenericLayout>()
            this.filterNot { it.viewModelClass == FundingPromptViewModel::class.java }
                .map { layout ->
                    resultingList.add(
                        layout.getMatchingFundingPromptLayout(fundingPrompts)
                    )
                }
            resultingList
        }
        InboxMessagesWidgetPlace.OVERVIEW -> {
            toMutableList().apply {
                removeIf { it is AccountSummaryLayout }
            }
        }
        else -> this
    }
}

private fun GenericLayout.getMatchingFundingPromptLayout(fundingPrompts: List<GenericLayout>) =
    if (this is AccountSummaryLayout) {
        fundingPrompts.find {
            (it as? WithAccountDtoInfo)?.account?.accountUuid == uuid
        }?.also { fundingLayout ->
            if (fundingLayout is FundingPromptViewLayout) {
                fundingLayout.accountSourceLabel = accountSourceLabel
            }
        } ?: this
    } else {
        this
    }

fun List<GenericLayout>.filterPortfolioViews(): List<GenericLayout> {
    val fundingPrompts = this.filter { it.viewModelClass == FundingPromptViewModel::class.java }
    return if (fundingPrompts.isNotEmpty()) {
        fundingPrompts
    } else {
        this
    }
}
