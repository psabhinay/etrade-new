package com.etrade.mobilepro.order.details.mapper

import com.etrade.mobilepro.order.details.api.OrderDetailDescription
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.util.safeParseBigDecimal

class OrderDetailDescriptionMapper {

    private val String.safeIntValue get() = safeParseBigDecimal()?.toInt() ?: 0

    fun map(description: OrderDetailDescription, hasPartialExecution: Boolean) = object : TradeLeg {
        override val symbol: String = description.originalSymbol
        override val transactionType: TransactionType = description.transactionType
        override val quantity: Int = description.resolveQuantity(hasPartialExecution)
        override val displaySymbol: String = description.displaySymbol
        override val isAMOption: Boolean = false // TODO: In scope of ETAND-15762
    }

    private fun OrderDetailDescription.resolveQuantity(hasPartialExecution: Boolean): Int {
        return if (hasPartialExecution) {
            quantity.safeIntValue - fillQuantity.safeIntValue
        } else {
            quantity.safeIntValue
        }
    }
}
