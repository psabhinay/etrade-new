package com.etrade.mobilepro.news.presentation.base

import android.view.View
import androidx.annotation.CallSuper
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.news.api.filter.FilterDataSource
import com.etrade.mobilepro.news.presentation.NewsSharedViewModel
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.news.presentation.adapter.NewsAdapter
import com.etrade.mobilepro.news.presentation.databinding.FragmentFilterableNewsBinding
import com.etrade.mobilepro.util.NetworkState
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.installTouchBasedClickListener

abstract class BaseFilterableNewsFragment<T, F : FilterDataSource<T>, VM : BaseNewsViewModel> :
    BaseNewsFragment<VM>() {

    protected val binding by viewBinding(FragmentFilterableNewsBinding::bind)

    protected abstract val filterDropDownTitle: Int

    protected open val dividerPosition: Int? = null

    final override val layoutRes: Int = R.layout.fragment_filterable_news

    final override fun getSwipeRefreshLayout(): SwipeRefreshLayout = binding.swipeRefresh

    final override fun getNewsRecyclerView(): RecyclerView = binding.newsRv

    final override fun getNewsDetailContainer(): View? = binding.newsDetailContainer?.root

    fun hideFilterView() {
        binding.newsFilter.newsFilter.visibility = View.GONE
        binding.newsGutter.visibility = View.GONE
    }

    @CallSuper
    override fun bindViewModels(
        viewModel: VM,
        sharedViewModel: NewsSharedViewModel,
        adapter: NewsAdapter
    ) {
        with(getFilterDataSource(sharedViewModel)) {
            initFilterDropDown(this)

            networkState.observe(
                viewLifecycleOwner,
                Observer { networkState ->
                    with(binding.newsFilter.filterLoadingProgressBar) {
                        when (networkState) {
                            is NetworkState.Loading -> show()
                            else -> hide()
                        }
                    }
                }
            )

            selectedItem.observe(
                viewLifecycleOwner,
                Observer { selectedItem: T? ->
                    binding.newsFilter.filterValue.text = getSelectedItemTitle(selectedItem)
                    applyFilter(selectedItem)
                }
            )
        }
    }

    protected abstract fun applyFilter(selectedItem: T?)

    protected abstract fun getFilterDataSource(sharedViewModel: NewsSharedViewModel): FilterDataSource<T>

    protected abstract fun getSelectedItemTitle(selectedItem: T?): CharSequence

    protected open fun onSingleFilterValueClicked() = Unit

    private fun initFilterDropDown(filterDataSource: FilterDataSource<T>) {
        val dropDownManager = BottomSheetSelector<T>(parentFragmentManager, resources)
        filterDataSource.items.observe(
            viewLifecycleOwner,
            Observer { filterItems ->
                if (filterItems.size > 1) {
                    dropDownManager.init(
                        tag = this@BaseFilterableNewsFragment::class.java.name,
                        title = getString(filterDropDownTitle),
                        items = filterItems,
                        initialSelectedPosition = filterDataSource.getSelectedItemIndex(),
                        dividerPosition = dividerPosition
                    )
                    dropDownManager.setListener { _, selected ->
                        filterDataSource.setSelectedItem(selected)
                    }
                    binding.newsFilter.root.installTouchBasedClickListener {
                        dropDownManager.updateInitialSelectedPosition(filterDataSource.getSelectedItemIndex())
                        dropDownManager.openDropDown()
                    }
                } else {
                    binding.newsFilter.root.installTouchBasedClickListener {
                        onSingleFilterValueClicked()
                    }
                }
            }
        )
    }
}
