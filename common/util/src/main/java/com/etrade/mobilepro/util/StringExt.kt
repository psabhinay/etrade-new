package com.etrade.mobilepro.util

import java.math.BigDecimal
import java.net.URLEncoder
import java.time.DateTimeException
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.Locale

fun String.toSentence(locale: Locale = Locale.getDefault()): String {
    return trim()
        .lowercase(locale)
        .split(' ')
        .joinToString(" ") {
            it.capitalize(locale)
        }
}

fun String?.toNullIfBlank(): String? = if (this.isNullOrBlank()) {
    null
} else {
    this
}

fun String?.isQuoteSizeValid(): Boolean {
    val value = this?.safeParseBigDecimal()?.stripTrailingZeros()
    return null != value && value > BigDecimal.ZERO
}

fun String?.formatDash(): String? {
    return this?.replace("-", "dash")
}

fun String.formatCheckableButtonSymbols(): String {
    val stringBuilder = StringBuilder()
    val array = toCharArray()

    array.forEachIndexed { index, char ->
        stringBuilder.append(
            when {
                char == '<' -> " less than"
                char == '>' -> " greater than"
                (char == '-' || char == '–') && array.shouldFormatTo(index) -> " to"
                char == '=' && array.shouldFormatEqual(index) -> " or equal"
                else -> array.letterToNumberString(index)
            }
        )
    }
    return stringBuilder.toString().formatYTD().formatAverage()
}

fun String.formatLetterRepresentationToNumber(): String {
    val array = toCharArray()
    val stringBuilder = StringBuilder()
    for (i in array.indices) {
        stringBuilder.append(array.letterToNumberString(i))
    }
    return stringBuilder.toString()
}

fun CharSequence.splitOnFirstSpace(): Pair<String, String?> =
    with(split(" ".toRegex(), 2)) {
        val first = get(0)
        val second = if (size > 1) { get(1) } else { null }
        first to second
    }

/**
 * Convert the date format used for options into a shortened digit-only format
 * E.g. Aug 20 '21 -> 08/20/21
 */
fun String.toShortDate(): String =
    try {
        val originalFormatter = DateTimeFormatter.ofPattern("MMM dd ''uu")
        val dateTime = LocalDate.parse(this, originalFormatter)
        val shortFormatter = DateTimeFormatter.ofPattern("MM/dd/uu")
        dateTime.format(shortFormatter)
    } catch (e: DateTimeException) {
        this
    }

@Suppress("MagicNumber")
private fun CharArray.shouldFormatEqual(index: Int): Boolean {
    val symbol = getCharFromIndex(index, -2)
    return symbol == '<' || symbol == '>'
}

private fun CharArray.shouldFormatTo(index: Int) = size > index + 1 && get(index + 1).isWhitespace()

private fun String.formatYTD() = replaceFirst("YTD", "Y T D")

private fun String.formatAverage() = replaceFirst("Avg", " average")

private fun CharArray.getCharFromIndex(index: Int, steps: Int) =
    if (index + steps in 0 until size) {
        get(index + steps)
    } else {
        null
    }

private fun CharArray.letterToNumberString(index: Int): String {
    val isPreviousCharDigit = getCharFromIndex(index, -1)?.isDigit() == true
    val current = get(index)
    if (isPreviousCharDigit) {
        when (current) {
            'K' -> return " Thousand"
            'M' -> return " Million"
            'B' -> return " Billion"
            'T' -> return " Trillion"
        }
    }
    return current.toString()
}

/*
 * This is duplicated from the Kotlin stdlib `String.capitalize` extension function because it is marked as experimental,
 * and the entire call stack would have to be annotated.
 */
private fun String.capitalize(locale: Locale): String {
    if (isNotEmpty()) {
        val firstChar = this[0]
        if (firstChar.isLowerCase()) {
            return buildString {
                val titleChar = firstChar.titlecaseChar()
                if (titleChar != firstChar.uppercaseChar()) {
                    append(titleChar)
                } else {
                    append(this@capitalize.substring(0, 1).uppercase(locale))
                }
                append(this@capitalize.substring(1))
            }
        }
    }
    return this
}

fun String.encode(): String {
    return URLEncoder.encode(this, "utf-8")
}

/*
1/1/1 -> 01/01/01
1/09/20 -> 01/09/20
01/9/20 -> 01/09/20
etc.
 */
fun String.addLeadingZerosToDate(separator: String = "/"): String =
    this
        .split(separator)
        .joinToString(separator) {
            if (it.length == 1) {
                "0$it"
            } else {
                it
            }
        }

fun String.isPositiveNumber() =
    matches("^\\d*(\\.?\\d+)?$".toRegex()) && !matches("^0*(\\.?0+)?$".toRegex())

fun String.isNegativeNumber() =
    matches("^-\\d*(\\.?\\d+)?$".toRegex())
