package com.etrade.mobilepro.trade.api.option

import com.etrade.mobilepro.orders.api.StrategyType

interface OptionTradeStrategy {
    val strategyType: StrategyType
    val optionLegs: List<TradeLeg>
}
