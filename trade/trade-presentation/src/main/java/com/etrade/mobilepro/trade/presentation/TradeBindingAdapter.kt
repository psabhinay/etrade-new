package com.etrade.mobilepro.trade.presentation

import android.content.res.Resources
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quoteapi.FundsTradeSummaryItem
import com.etrade.mobilepro.trade.presentation.util.toMutualFundsQuoteSummaryItems
import com.etrade.mobilepro.uiwidgets.tablesectionsview.SectionLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection

@BindingAdapter("fundsTradeSections")
internal fun SectionLayout.setFundsTradeSections(summaryItem: FundsTradeSummaryItem?) {
    summaryItem?.let {
        setContent(addQuoteHeaderSection(resources, it) + toMutualFundsQuoteSummaryItems(resources, it))
    }
}

private fun addQuoteHeaderSection(res: Resources, fundsTradeSummaryItem: FundsTradeSummaryItem): List<TableSectionItemLayout> = with(fundsTradeSummaryItem) {
    if (instrumentType == InstrumentType.MMF) {
        listOfNotNull(
            createSummarySection(res.getString(R.string.trade_label_seven_day_yield), sevenDayCurrentYield),
            createSummarySection(res.getString(R.string.trade_label_annual_return), annualAvgReturn),
            createSummarySection(res.getString(R.string.trade_label_weighted_avg_maturity), weightedAvgMaturity)
        )
    } else {
        listOfNotNull(
            createSummarySection(res.getString(R.string.trade_label_net_asset_value), netAssetValue),
            createSummarySection(res.getString(R.string.trade_label_change), change)
        )
    }
}
