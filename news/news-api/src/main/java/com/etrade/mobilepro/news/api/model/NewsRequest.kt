package com.etrade.mobilepro.news.api.model

sealed class NewsRequest {
    data class Portfolio(
        val resource: String = "all",
        val includeStockPlan: Boolean = true
    ) : NewsRequest()

    data class Symbol(
        val resource: List<String>
    ) : NewsRequest() {
        fun joinSymbols() = resource.joinToString(",")
    }

    data class Watchlist(
        val watchlistId: String
    ) : NewsRequest()

    object Market : NewsRequest()
}
