package com.etrade.mobilepro.news.briefing.data.mapper

import android.app.Application
import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.briefing.data.dto.BriefingNewsDto
import com.etrade.mobilepro.news.briefing.data.dto.BriefingNewsPayloadDto
import com.etrade.mobilepro.testutil.XmlDeserializer
import com.etrade.mobilepro.util.android.textutil.TextUtil
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@RunWith(AndroidJUnit4::class)
@Config(application = Application::class, sdk = [Build.VERSION_CODES.P])
class BriefingNewsMapperTest {
    private lateinit var sut: BriefingNewsMapper

    @Before
    fun setUp() {
        sut = BriefingNewsMapper {
            it
        }
    }

    @Test
    fun `response with no payload should result in empty list`() {
        val input = BriefingNewsDto(payloadDto = null)

        val output = sut.map(input)

        assertTrue(output.isEmpty())
    }

    @Test
    fun `null fields in response result in empty strings and lists`() {
        val input = BriefingNewsDto(
            payloadDto = BriefingNewsPayloadDto(
                null,
                null,
                null,
                null
            )
        )

        val output = sut.map(input)

        assertTrue("Has one element", output.size == 1)
        assertTrue("the only elem is NewsItem.Text", output[0] is NewsItem.Text)
        val casted = output[0] as NewsItem.Text
        assertTrue(casted.docId.isEmpty())
        assertTrue(casted.newsDateTimeSource.isEmpty())
        assertTrue(casted.title.isEmpty())
        assertTrue(casted.content.isEmpty())
        assertTrue(casted.url.isEmpty())
        assertTrue(casted.symbols.isEmpty())
    }

    @Test
    fun `success case, maps 1 to 1`() {
        val input = BriefingNewsDto(
            payloadDto = BriefingNewsPayloadDto(
                stockTickerId = "testvalue1",
                dateTime = "testvalue2",
                title = "testvalue3",
                content = "testvalue4"
            )
        )

        val output = sut.map(input)

        assertTrue("Has one element", output.size == 1)
        assertTrue("the only elem is NewsItem.Text", output[0] is NewsItem.Text)
        val casted = output[0] as NewsItem.Text
        assertTrue(casted.docId == "testvalue1testvalue2")
        assertTrue(casted.newsDateTimeSource == "")
        assertTrue(casted.title == "testvalue3")
        assertTrue(casted.content == "testvalue4")
    }

    @Test
    fun `try catch test for wrong date format from crash log, no exceptions are thrown`() {
        val input = BriefingNewsDto(
            payloadDto = BriefingNewsPayloadDto(
                stockTickerId = "A",
                dateTime = "05-30-19 08:35:31 ET",
                title = "title",
                content = "content"
            )
        )

        val output = sut.map(input)
        assertEquals("Has one element", 1, output.size)
        assertTrue("the only elem is NewsItem.Text", output[0] is NewsItem.Text)
        val casted = output[0] as NewsItem.Text
        assertEquals("date was parsed", "05/30/2019 | 08:35 AM EDT", casted.newsDateTimeSource)
    }

    @Test
    fun `test briefing news mapper`() {
        val briefingNewsMapper = BriefingNewsMapper {
            TextUtil.convertTextWithHmlTagsToSpannable(it).toString()
        }
        val input = XmlDeserializer.getObjectFromXml("moving_market_response.xml", BriefingNewsDto::class.java)
        val expectedNewsItems = briefingNewsMapper.map(input)
        assertEquals(1, expectedNewsItems.size)

        val expectedNewsItemText = expectedNewsItems[0] as NewsItem.Text
        assertEquals("S&P 500 to 3800", expectedNewsItemText.title)
        assertEquals(0, expectedNewsItemText.symbols.size)
    }
}
