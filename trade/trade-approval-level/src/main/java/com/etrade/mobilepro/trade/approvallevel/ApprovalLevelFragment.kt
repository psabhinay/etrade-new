package com.etrade.mobilepro.trade.approvallevel

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.etrade.eo.corelibandroid.createViewModel
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.dialog.showDialog
import com.etrade.mobilepro.dialog.viewmodel.AppDialogViewDelegate
import com.etrade.mobilepro.trade.accountupgrade.util.getApprovalLevelUpgradeUrl
import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.trade.approvallevel.databinding.ApprovalLevelFragmentBinding
import com.etrade.mobilepro.trade.approvallevel.router.ApprovalLevelRouter
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.startDialerActivity
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.ViewDelegate
import org.chromium.customtabsclient.shared.WebviewActivity
import javax.inject.Inject

private const val KEY_ACCOUNT_MODE = "accountMode"

@RequireLogin
class ApprovalLevelFragment @Inject constructor(
    approvalLevelFactory: ApprovalLevelViewModel.Factory,
    @Web private val webBaseUrl: String,
    private val approvalLevelRouter: ApprovalLevelRouter,
    private val snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.approval_level_fragment) {

    private val binding by viewBinding(ApprovalLevelFragmentBinding::bind)
    private val accountMode: AccountMode
        get() = requireNotNull(requireArguments().get(KEY_ACCOUNT_MODE)) as AccountMode

    private val viewModel: ApprovalLevelViewModel by createViewModel { approvalLevelFactory.create(accountMode) }

    private val appDialogViewDelegate: ViewDelegate by lazy { AppDialogViewDelegate(viewModel) { showDialog(it) } }

    private val snackBarUtil: SnackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ viewLifecycleOwner }, { view }) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            accountModeText.setText(accountMode.getApprovalLevelAccountDescription())
            importantNote.movementMethod = LinkMovementMethod.getInstance()
            importantNote.setText(R.string.approval_level_important_note)
            applyForAnOptionUpgrade.setOnClickListener {
                requireContext().startActivity(
                    WebviewActivity.intent(
                        requireContext(),
                        webBaseUrl.plus(getApprovalLevelUpgradeUrl(accountMode == AccountMode.IRA)),
                        R.string.approval_level_title_options
                    )
                )
            }
            sendSecureMessage.setOnClickListener {
                activity?.let { approvalLevelRouter.navigateToNewMessage(findNavController()) }
            }
            callCustomerService.setOnClickListener {
                viewModel.showCallUsConfirmDialog(
                    {
                        requireContext().startDialerActivity(resources.getString(R.string.support_phone_number)) { snackBarUtil }
                    },
                    { /* Intentionally blank. */ }
                )
            }
        }
        bindViewModel()
    }

    private fun bindViewModel() {
        appDialogViewDelegate.observe(viewLifecycleOwner)
    }

    private fun AccountMode.getApprovalLevelAccountDescription(): Int = when (this) {
        AccountMode.CASH -> R.string.approval_level_cash_account
        AccountMode.IRA -> R.string.approval_level_ira_account
        else -> R.string.approval_level_margin_account
    }
}
