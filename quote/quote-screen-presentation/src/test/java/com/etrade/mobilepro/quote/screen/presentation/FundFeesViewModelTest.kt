package com.etrade.mobilepro.quote.screen.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.quote.dto.FundQuoteResponseDto
import com.etrade.mobilepro.quote.mapper.DefaultFundQuoteDetailItemsMapper
import com.etrade.mobilepro.quote.mapper.MobileQuoteMapper
import com.etrade.mobilepro.quote.screen.presentation.fees.FundFeesViewViewModel
import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.quoteapi.FundQuoteItem
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.QuoteTemplateResources
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuoteUseCase
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito

class FundFeesViewModelTest {
    @ExperimentalCoroutinesApi
    private val testCoroutineDispatcher = TestCoroutineDispatcher()
    private val mockMobileQuoteStateFlow = MutableStateFlow(MobileQuote())

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val mockGetMobileQuoteUSeCase = mock<GetMobileQuoteUseCase>()
    private val mockFundQuoteDetailItemsMapper = Mockito.mock(FundQuoteDetailItemsMapper::class.java)

    private lateinit var sut: FundFeesViewViewModel
    private lateinit var mobileQuote: MobileQuote
    private lateinit var mapperResources: QuoteTemplateResources
    private lateinit var defaultFundQuoteDetailItemsMapper: DefaultFundQuoteDetailItemsMapper
    private lateinit var expenseItem: FundQuoteItem.FeesAndExpensesItem

    private fun getMockMFQuotes(): FundQuoteResponseDto {
        return this::class.getObjectFromJson("/responses/quotes_mutual_funds.json")
    }

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(testCoroutineDispatcher)
        mapperResources = getQuoteResources()
        defaultFundQuoteDetailItemsMapper = DefaultFundQuoteDetailItemsMapper(mapperResources)
        mobileQuote = MobileQuoteMapper().map(getMockMFQuotes())
        expenseItem = defaultFundQuoteDetailItemsMapper.getFeesAndExpensesItem(mobileQuote.fundDetails)
        whenever(mockFundQuoteDetailItemsMapper.getFeesAndExpensesItem(mobileQuote.fundDetails)).thenReturn(expenseItem)
    }

    @ExperimentalCoroutinesApi
    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `loadFeesData with quote livedata received is not null`() {
        runBlocking(testCoroutineDispatcher) {
            mockMobileQuoteStateFlow.value = mobileQuote
            sut = FundFeesViewViewModel(
                mock {
                    whenever(it.getString(any())).thenReturn("Value")
                },
                mockGetMobileQuoteUSeCase,
                mockIsUseExtendedHoursOn(),
                mockFundQuoteDetailItemsMapper,
                symbol,
                mockMobileQuoteStateFlow
            )
        }

        sut.configureFeeData()

        sut.feesAndExpenseLiveData.observeForever {
            assertNotNull(it.data?.data)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `fetch quotes using API on success response`() {
        runBlocking(testCoroutineDispatcher) {
            whenever(mockGetMobileQuoteUSeCase.execute(any())).thenReturn(ETResult.success(mobileQuote))
            sut = FundFeesViewViewModel(
                mock {
                    whenever(it.getString(any())).thenReturn("Value")
                },
                mockGetMobileQuoteUSeCase,
                mockIsUseExtendedHoursOn(),
                mockFundQuoteDetailItemsMapper,
                symbol,
                null
            )
        }

        sut.configureFeeData()

        sut.feesAndExpenseLiveData.observeForever {
            assertNotNull(it.data?.data)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `fetch quotes using API on failure response`() {
        runBlocking(testCoroutineDispatcher) {
            whenever(mockGetMobileQuoteUSeCase.execute(any())).thenReturn(ETResult.failure(RuntimeException("Something went wrong!")))
            sut = FundFeesViewViewModel(
                mock {
                    whenever(it.getString(any())).thenReturn("Value")
                },
                mockGetMobileQuoteUSeCase,
                mockIsUseExtendedHoursOn(),
                mockFundQuoteDetailItemsMapper,
                symbol,
                null
            )
        }

        sut.configureFeeData()

        sut.feesAndExpenseLiveData.observeForever {
            assertNull(it.data?.data)
        }
    }
}
