package com.etrade.mobilepro.chat.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.api.ChatState
import com.etrade.mobilepro.chat.api.message.repo.ChatMessagesRepo
import com.etrade.mobilepro.chat.presentation.TEST_ERROR_MESSAGE
import com.etrade.mobilepro.chat.presentation.TEST_SESSION_ID
import com.etrade.mobilepro.chat.presentation.chatTestForMobileModel
import com.etrade.mobilepro.chat.presentation.chatTestRequestParams
import com.etrade.mobilepro.chat.presentation.message.ChatMessagesViewModel
import com.etrade.mobilepro.common.result.ETResult
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
class ChatMessagesViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    private val testCoroutineDispatcher = TestCoroutineDispatcher()
    private val chatMessagesRepo: ChatMessagesRepo = mock()
    private lateinit var sut: ChatMessagesViewModel

    @Before
    fun setup() {
        Dispatchers.setMain(testCoroutineDispatcher)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun testChatMessagesRefreshService() {
        sut = ChatMessagesViewModel(
            chatMessagesRepo,
            null
        )
        sut.refresh(false)

        sut.viewState.test()
            .awaitValue()
            .assertValue { it is ChatMessagesViewModel.ViewState.StartChatService }
            .assertValue { (it as ChatMessagesViewModel.ViewState.StartChatService).sessionId == null }
    }

    @Test
    fun testChatMessagesInitiateChat() {
        sut = ChatMessagesViewModel(
            chatMessagesRepo,
            chatTestRequestParams
        )
        runBlocking(testCoroutineDispatcher) {
            whenever(chatMessagesRepo.getMobileChat(chatTestRequestParams)).thenReturn(
                ETResult.success(
                    chatTestForMobileModel
                )
            )
        }

        sut.refresh(false)

        sut.viewState.test()
            .awaitValue()
            .assertValue { it is ChatMessagesViewModel.ViewState.StartChatService }
            .assertValue { (it as ChatMessagesViewModel.ViewState.StartChatService).sessionId == TEST_SESSION_ID }
    }

    @Test
    fun testChatTerminateByUser() {
        sut = ChatMessagesViewModel(
            chatMessagesRepo,
            chatTestRequestParams
        )
        sut.terminateByUser()

        sut.viewState.test()
            .awaitValue()
            .assertValue { it is ChatMessagesViewModel.ViewState.Terminated }
    }

    @Test
    fun testChatHandleMessageShownState() {
        sut = ChatMessagesViewModel(
            chatMessagesRepo,
            chatTestRequestParams
        )
        val testMessages: List<ChatMessage> = mock()
        sut.onChatStateChange(ChatState.MessagesShown(testMessages))
        sut.viewState.test()
            .awaitValue()
            .assertValue { it is ChatMessagesViewModel.ViewState.MessagesShown }
            .assertValue { (it as ChatMessagesViewModel.ViewState.MessagesShown).messages == testMessages }
    }

    @Test
    fun testChatHandleQueueShownState() {
        sut = ChatMessagesViewModel(
            chatMessagesRepo,
            chatTestRequestParams
        )
        val estimatedWaitTime = 1234
        sut.onChatStateChange(ChatState.QueueShown(estimatedWaitTime))
        sut.viewState.test()
            .awaitValue()
            .assertValue { it is ChatMessagesViewModel.ViewState.QueueShown }
            .assertValue { (it as ChatMessagesViewModel.ViewState.QueueShown).estimatedWaitTimeInSeconds == estimatedWaitTime }
    }

    @Test
    fun testChatHandleErrorState() {
        sut = ChatMessagesViewModel(
            chatMessagesRepo,
            chatTestRequestParams
        )
        sut.onChatStateChange(ChatState.Error(""))
        sut.viewState.test()
            .awaitValue()
            .assertValue { it is ChatMessagesViewModel.ViewState.Error }
    }

    @Test
    fun testChatHandleLoadingState() {
        sut = ChatMessagesViewModel(
            chatMessagesRepo,
            chatTestRequestParams
        )
        val testMessages: List<ChatMessage> = mock()
        sut.onChatStateChange(ChatState.SendMessageError(testMessages, TEST_ERROR_MESSAGE))

        sut.viewState.test()
            .awaitValue()
            .assertValue { it is ChatMessagesViewModel.ViewState.SendMessageError }
            .assertValue { (it as ChatMessagesViewModel.ViewState.SendMessageError).message == TEST_ERROR_MESSAGE }
            .assertValue { (it as ChatMessagesViewModel.ViewState.SendMessageError).messages == testMessages }
    }
}
