package com.etrade.mobilepro.tableviewutils.presentation.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.tableviewutils.presentation.R
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.DataCellViewHolder
import com.etrade.mobilepro.tableviewutils.presentation.holder.SymbolRowHeaderViewHolder
import com.etrade.mobilepro.util.android.goneUnless
import com.etrade.mobilepro.util.formatLetterRepresentationToNumber
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

open class SymbolTableViewAdapter(
    context: Context,
    private val displaySymbolDescription: Boolean = false
) : TableViewAdapter(context) {

    override fun onCreateRowHeaderViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val layout = inflater.inflate(R.layout.table_view_row_header_layout, parent, false)
        return SymbolRowHeaderViewHolder(layout, displaySymbolDescription)
    }

    override fun bindRowHeaderViewHolder(
        holder: AbstractViewHolder,
        rowHeaderItemModel: Any,
        rowPosition: Int
    ) {
        val rowHeader = rowHeaderItemModel as SymbolCell
        // Get the holder to update row header item text
        val rowHeaderViewHolder = holder as SymbolRowHeaderViewHolder
        val shouldDisplayDescription = displaySymbolDescription && rowHeader.description?.isNotEmpty() == true

        holder.itemView.findViewById<View>(R.id.txtRowSubHeader)
            ?.goneUnless(rowHeader.instrumentType.hasSubTitle() || shouldDisplayDescription)

        rowHeaderViewHolder.cell = rowHeader

        holder.itemView.contentDescription = when {
            rowHeader.instrumentType.isOption -> rowHeader.description
            shouldDisplayDescription -> "${rowHeader.getSymbolContentDescription()} ${rowHeader.description}"
            else -> rowHeader.getSymbolContentDescription()
        }
    }

    override fun onBindCellViewHolder(holder: AbstractViewHolder, cellItemModel: Any, columnPosition: Int, rowPosition: Int) {
        super.onBindCellViewHolder(holder, cellItemModel, columnPosition, rowPosition)

        val row = getRowHeaderItem(rowPosition) as? SymbolCell
        val column = getColumnHeaderItem(columnPosition)
        val cell = cellItemModel as Cell

        val contentDescriptionWithoutSymbol = "${column.displayTextContentDescription}, ${cell.displayText}".formatLetterRepresentationToNumber()

        val contentDescription = if (row != null) {
            "${row.getSymbolContentDescription()}, $contentDescriptionWithoutSymbol"
        } else {
            contentDescriptionWithoutSymbol
        }

        if (holder is DataCellViewHolder) {
            holder.contentDescription = contentDescription
        } else {
            holder.itemView.contentDescription = contentDescription
        }
    }
}

private fun InstrumentType.hasSubTitle(): Boolean = this == InstrumentType.BOND || this.isOption
