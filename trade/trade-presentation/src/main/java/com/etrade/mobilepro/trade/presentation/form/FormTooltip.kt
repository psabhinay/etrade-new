package com.etrade.mobilepro.trade.presentation.form

import com.etrade.mobilepro.tooltip.AppTooltip
import com.etrade.mobilepro.tooltip.TooltipGravity

class FormTooltip(
    val targetInputId: String,
    message: String,
    shouldConsumeDismissEvent: Boolean = true,
    onExplicitDismiss: (() -> Unit)? = null,
    onImplicitDismiss: (() -> Unit)? = null
) : AppTooltip(message, TooltipGravity.BOTTOM, shouldConsumeDismissEvent, onExplicitDismiss, onImplicitDismiss)

class FormActionTooltip(
    val targetActionTitle: String,
    message: String,
    onExplicitDismiss: (() -> Unit)? = null,
    onImplicitDismiss: (() -> Unit)? = null
) : AppTooltip(message, TooltipGravity.TOP, true, onExplicitDismiss, onImplicitDismiss)
