package com.etrade.mobilepro.alerts.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false, name = "SetAlerts")
class SetNewsAlertResponseXmlDto(
    @field:Element(name = "haserror", required = false)
    var hasError: String? = null,
    @field:Element(name = "errormessage", required = false)
    var errorMessage: String? = null,
    @field:Element(name = "hasalerts", required = false)
    var hasAlerts: String? = null,
    @field:ElementList(name = "AlertsDetails", required = false)
    var alerts: List<SetNewsAlertDetailXmlDto?>? = null
)

@Root(strict = false, name = "Alert")
class SetNewsAlertDetailXmlDto(
    @field:Element(name = "altDelete", required = false)
    var delete: String? = null,
    @field:Element(name = "lastedited", required = false)
    var lastEdited: String? = null,
    @field:Element(name = "altsymbol", required = false)
    var symbol: String? = null,
    @field:Element(name = "deleteinfo", required = false)
    var deleteInfo: String? = null,
    @field:Element(name = "altcontent", required = false)
    var content: String? = null,
    @field:Element(name = "altedit", required = false)
    var edit: String? = null,
    @field:Element(name = "altiplstatus", required = false)
    var inPlayStatus: String? = null,
    @field:Element(name = "altratstatus", required = false)
    var upDownStatus: String? = null,
    @field:Element(name = "alternstatus", required = false)
    var earningsStatus: String? = null,
    @field:Element(name = "altsplstatus", required = false)
    var splitNoticeStatus: String? = null
)
