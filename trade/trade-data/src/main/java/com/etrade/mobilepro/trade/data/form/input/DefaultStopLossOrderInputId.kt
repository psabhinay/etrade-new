package com.etrade.mobilepro.trade.data.form.input

import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.data.form.CommonAllOrNoneValueCoder
import com.etrade.mobilepro.trade.data.form.DefaultSymbolInputValueCoder
import com.etrade.mobilepro.trade.data.form.value.AdvancedActionValueId
import com.etrade.mobilepro.trade.data.form.value.CommonAdvancedPriceTypeValueId
import com.etrade.mobilepro.trade.data.form.value.CommonOrderTermValueId
import com.etrade.mobilepro.trade.data.form.value.ConditionalOrderOptionsActionValueId
import com.etrade.mobilepro.trade.data.form.value.DefaultSecurityTypeValueId
import com.etrade.mobilepro.trade.form.api.input.shared.StopLossInputId
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.trade.form.api.value.SecurityTypeValueId
import com.etrade.mobilepro.util.ValueCoder

object DefaultStopLossOrderInputId : StopLossInputId {
    override val accountId: String = ""
    override val securityType: String = "securitytype1"
    override val securityTypeValueId: SecurityTypeValueId = DefaultSecurityTypeValueId
    override val quantity: String = "quantity1"
    override val symbol: String = "symbol1"
    override val symbolCoder: ValueCoder<WithSymbolInfo> = DefaultSymbolInputValueCoder
    override val action: String = "transaction1"
    override val actionValueId: ActionValueId = AdvancedActionValueId
    override val allOrNone: String = "aon1"
    override val allOrNoneValueCoder: ValueCoder<Boolean> = CommonAllOrNoneValueCoder
    override val limitPrice: String = "price1"
    override val limitPriceForStopLimit: String = "limitPriceForStopLimit1"
    override val offsetValue: String = "offsetvalue1"
    override val priceType: String = "pricetype1"
    override val priceTypeValueId: PriceTypeValueId = CommonAdvancedPriceTypeValueId
    override val stopPrice: String = "stopprice1"
    override val stopPriceForStopLimit: String = "stopPriceForStopLimit1"
    override val term: String = "orderterm1"
    override val termValueId: OrderTermValueId = CommonOrderTermValueId
}

object DefaultStopLossOptionOrderInputId : StopLossInputId by DefaultStopLossOrderInputId {
    override val actionValueId: ActionValueId = ConditionalOrderOptionsActionValueId
}
