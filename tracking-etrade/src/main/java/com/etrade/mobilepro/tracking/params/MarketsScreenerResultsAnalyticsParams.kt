package com.etrade.mobilepro.tracking.params

private const val EVENT_MARKETS_SCREENER_RESULTS = "Markets 2.0 Screener Results"
private const val ATTRIBUTE_MARKETS_SECURITY_TYPE = "Security Type"
private const val SECURITY_TYPE_STOCK = "stock"
private const val SECURITY_TYPE_MUTUAL_FUND = "mutual fund"
private const val SECURITY_TYPE_ETF = "ETF"

sealed class MarketsScreenerResultsAnalyticsParams(securityType: String) : AnalyticsParams {

    override val name: String = EVENT_MARKETS_SCREENER_RESULTS

    override val params: Map<String, String> = mapOf(
        ATTRIBUTE_MARKETS_SECURITY_TYPE to securityType
    )
}

class StockMarketsScreenerResultsAnalyticsParams : MarketsScreenerResultsAnalyticsParams(SECURITY_TYPE_STOCK)
class MutualFundMarketsScreenerResultsAnalyticsParams : MarketsScreenerResultsAnalyticsParams(SECURITY_TYPE_MUTUAL_FUND)
class EtfMarketsScreenerResultsAnalyticsParams : MarketsScreenerResultsAnalyticsParams(SECURITY_TYPE_ETF)
