package com.etrade.mobilepro.util

fun <K, V> Map<K, V?>.filterNotNullValues(): Map<K, V> {
    val nonNullMap = mutableMapOf<K, V>()
    forEach { entry ->
        entry.value?.let {
            nonNullMap[entry.key] = it
        }
    }
    return nonNullMap
}

fun <K, V> Map<K, V>.inverse(): Map<V, K> = entries.associate { it.value to it.key }
