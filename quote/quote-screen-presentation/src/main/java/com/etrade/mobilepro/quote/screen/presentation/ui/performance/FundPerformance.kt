package com.etrade.mobilepro.quote.screen.presentation.ui.performance

data class FundPerformance(
    val symbol: String = "",
    val symbolAdjust: String? = null,
    val fundCategory: String = "",
    val monthlyYTD: String = "--",
    val monthly1Year: String = "--",
    val monthly5year: String = "--",
    val monthly10year: String = "--",
    val categoryMonthlyYTD: String = "--",
    val categoryMonthly1Year: String = "--",
    val categoryMonthly5year: String = "--",
    val categoryMonthly10year: String = "--",
    val quarterlyYTD: String = "--",
    val quarterly1Year: String = "--",
    val quarterly5year: String = "--",
    val quarterly10year: String = "--",
    val categoryQuarterlyYTD: String = "--",
    val categoryQuarterly1Year: String = "--",
    val categoryQuarterly5year: String = "--",
    val categoryQuarterly10year: String = "--",
    val inceptionDate: String = "--",
    val performanceAsOfDate: String = "--",
    val quarterlyPerformanceAsOfDate: String = "--",
    val monthlyReturnsSinceInception: String = "--",
    val quarterlyReturnsSinceInception: String = "--"
)
