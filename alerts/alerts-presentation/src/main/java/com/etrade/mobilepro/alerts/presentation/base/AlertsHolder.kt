package com.etrade.mobilepro.alerts.presentation.base

import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.api.model.AlertItem.Divider
import java.util.concurrent.TimeUnit
import javax.annotation.concurrent.NotThreadSafe

private const val STARTING_EVENT_ID = "0"

interface AlertsHolder {
    fun add(newChunk: List<AlertItem>)
    fun insertAccordingToOccurrenceDate(alert: AlertItem.Alert)
    fun setHeadersVisibility(headerItems: List<AlertItem>, isVisible: Boolean)
    fun delete(alertId: String)
    fun deleteAll(headerItems: List<AlertItem>)
    fun resetIfUserIsChanged(userId: Long, headerItems: List<AlertItem>)
    fun alerts(addBottomLoadingIndicator: Boolean = false): List<AlertItem>
    fun lastAlertInfo(goingToRefresh: Boolean): AlertPaginationInfo
    fun isEmpty(headerItems: List<AlertItem>): Boolean
    fun toggleDeleteButtonVisibility(alert: AlertItem.Alert, isVisible: Boolean)
    fun hideAllDeleteButtons()

    fun setEditable(isEditable: Boolean): Boolean
    fun toggleMarkedStatus(alert: AlertItem.Alert)
}

data class AlertPaginationInfo(
    val id: String,
    val occurredAt: Long
)

@NotThreadSafe
class DefaultAlertsHolder : AlertsHolder {

    private val alerts = mutableListOf<AlertItem>()
    private var currentUserId: Long? = null

    override fun add(newChunk: List<AlertItem>) {
        if (alerts.size == 1 && alerts[0] is Divider && newChunk.isEmpty()) {
            alerts.clear()
        }
        alerts.addAll(newChunk)
    }

    override fun insertAccordingToOccurrenceDate(alert: AlertItem.Alert) {
        val index = alerts.indexOfFirst {
            it is AlertItem.Alert && it.occurredAt <= alert.occurredAt
        }
        if (index >= 0 && index < alerts.size) {
            alerts.add(index, alert)
        }
    }

    override fun setHeadersVisibility(headerItems: List<AlertItem>, isVisible: Boolean) {
        headerItems.forEachIndexed { index, _ ->
            if (alerts.size > 0 && alerts[index] is Divider) {
                (alerts[index] as Divider).show = isVisible
            }
        }
    }

    override fun delete(alertId: String) {
        alerts.firstOrNull { it is AlertItem.Alert && it.id == alertId }?.let {
            alerts.remove(it)
        }
    }

    override fun deleteAll(headerItems: List<AlertItem>) {
        alerts.clear()
        alerts.addAll(headerItems)
    }

    override fun resetIfUserIsChanged(userId: Long, headerItems: List<AlertItem>) {
        if (currentUserId != userId) {
            currentUserId = userId
            deleteAll(headerItems)
        }
    }

    override fun alerts(addBottomLoadingIndicator: Boolean): List<AlertItem> {
        return if (addBottomLoadingIndicator) {
            mutableListOf<AlertItem>()
                .apply {
                    addAll(alerts)
                }
                .apply {
                    add(AlertItem.LoadingIndicator)
                }
        } else {
            alerts.toList()
        }
    }

    override fun lastAlertInfo(goingToRefresh: Boolean): AlertPaginationInfo =
        if (goingToRefresh) {
            AlertPaginationInfo(
                id = STARTING_EVENT_ID,
                occurredAt = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())
            )
        } else {
            AlertPaginationInfo(
                id = lastAlert()?.id ?: STARTING_EVENT_ID,
                occurredAt = lastAlert()?.occurredAt ?: TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())
            )
        }

    override fun isEmpty(headerItems: List<AlertItem>): Boolean = alerts.isEmpty() || alerts == headerItems

    override fun toggleDeleteButtonVisibility(alert: AlertItem.Alert, isVisible: Boolean) {
        if (isVisible) {
            hideAllDeleteButtons()
        }
        val newOpenedIndex = alerts.indexOf(alert)
        if (newOpenedIndex >= 0) {
            alerts[newOpenedIndex] = alert.copy(isDeleteButtonVisible = isVisible)
        }
    }

    override fun hideAllDeleteButtons() {
        val currentlyOpenedIndex = alerts.indexOfFirst {
            it is AlertItem.Alert && it.isDeleteButtonVisible
        }
        if (currentlyOpenedIndex >= 0) {
            val currentlyOpened = alerts[currentlyOpenedIndex] as AlertItem.Alert
            alerts[currentlyOpenedIndex] = currentlyOpened.copy(isDeleteButtonVisible = false)
        }
    }

    /**
     * @param isEditable True if the alerts should be editable, false otherwise.
     * @return True if the collection changed, false otherwise.
     */
    override fun setEditable(isEditable: Boolean): Boolean {
        return if (lastAlert()?.status?.isEditable == !isEditable) {
            alerts
                .filterIsInstance<AlertItem.Alert>()
                .forEach {
                    it.status.isEditable = isEditable
                    if (!isEditable) it.status.isMarked = false
                }
            true
        } else { false }
    }

    override fun toggleMarkedStatus(alert: AlertItem.Alert) {
        alert.status.isMarked = !alert.status.isMarked
    }

    @Suppress("SafeCast")
    private fun lastAlert(): AlertItem.Alert? {
        if (alerts.isEmpty()) {
            return null
        }
        val last = alerts.last() // we have no footers
        return if (last is AlertItem.Alert) {
            last
        } else {
            null
        }
    }
}
