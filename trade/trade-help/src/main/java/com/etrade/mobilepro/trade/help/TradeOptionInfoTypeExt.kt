package com.etrade.mobilepro.trade.help

import androidx.annotation.StringRes
import com.etrade.mobilepro.trade.api.TradeOptionInfoType

@StringRes
fun TradeOptionInfoType.getTitleId(learnOtherPriceTypes: Boolean = false): Int = when (this) {
    TradeOptionInfoType.SECURITY_TYPE,
    TradeOptionInfoType.SECURITY_TYPE_OPTIONS -> R.string.security_type
    TradeOptionInfoType.PRICE_TYPE,
    TradeOptionInfoType.PRICE_TYPE_OPTIONS -> if (learnOtherPriceTypes) {
        R.string.trade_other_price_types
    } else {
        R.string.price_type
    }
    TradeOptionInfoType.ORDER_TYPE,
    TradeOptionInfoType.ORDER_TYPE_OPTIONS,
    TradeOptionInfoType.ORDER_TYPE_MF -> R.string.order_type
}
