package com.etrade.mobilepro.user.session.manager

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.session.api.SessionEvents
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.session.api.UserAuthenticationState

interface UserSessionHandler : SessionStateUnAuthenticate {

    val sessionTerminateEvent: LiveData<SessionEvents>

    val isUserLoggedIn: UserAuthenticationState

    fun onLoginSuccess()

    suspend fun logout()
}

interface SessionStateChangeListener {
    fun onSessionStateChange(state: SessionStateChange)
}

interface SessionState {
    fun addListener(listener: SessionStateChangeListener)
    fun removeListener(listener: SessionStateChangeListener)
}

/**
 * Interface to set the Global Auth State to UnAuthenticated state,
 * thereby initiating logout and post logout cleanup.
 */
interface SessionStateUnAuthenticate {

    fun unAuthenticateSession()
}
