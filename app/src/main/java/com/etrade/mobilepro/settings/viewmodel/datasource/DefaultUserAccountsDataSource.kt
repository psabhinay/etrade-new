package com.etrade.mobilepro.settings.viewmodel.datasource

import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobilepro.accounts.util.android.view.AccountsSpinnerModel
import com.etrade.mobilepro.landing.api.AccountBasicInfo
import com.etrade.mobilepro.landing.api.UserAccounts
import com.etrade.mobilepro.landing.api.repo.UserAccountsDataSource
import com.etrade.mobilepro.userviewmodel.UserViewModel
import javax.inject.Inject

class DefaultUserAccountsDataSource @Inject constructor(
    private val userViewModel: UserViewModel
) : UserAccountsDataSource {
    override fun getUserAccounts(): UserAccounts {
        val accounts = userViewModel.accountList
        return UserAccounts(
            allAccounts = accounts.getSubListAndMap { it.account != null },
            brokerageAccounts = accounts.getSubListAndMap { it.account?.accountType == AccountType.Brokerage },
            bankAccounts = accounts.getSubListAndMap { it.account?.accountType == AccountType.Bank }
        )
    }

    private fun List<AccountsSpinnerModel>.getSubListAndMap(comparator: (AccountsSpinnerModel) -> Boolean): List<AccountBasicInfo> =
        this.filter(comparator).map(::mapToBasicInfo)

    private fun mapToBasicInfo(model: AccountsSpinnerModel): AccountBasicInfo =
        AccountBasicInfo(
            uuid = requireNotNull(model.account?.accountUuid),
            title = model.accountDisplayName,
            accountId = model.account?.accountId,
        )
}
