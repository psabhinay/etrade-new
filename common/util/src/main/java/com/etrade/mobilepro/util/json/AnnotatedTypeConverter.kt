package com.etrade.mobilepro.util.json

import com.etrade.eo.rest.retrofit.Json
import com.etrade.eo.rest.retrofit.Scalar
import com.etrade.eo.rest.retrofit.Xml
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

/**
 * Converts types to JSON or XML, depending on how they're annotated in the Retrofit method.
 *
 * @see Json
 * @see Xml
 * @see Scalar
 * @see JsonMoshi
 */
class AnnotatedTypeConverter
constructor(
    private val jsonFactory: Converter.Factory,
    private val xmlFactory: Converter.Factory,
    private val scalarFactory: Converter.Factory,
    private val jsonMoshiFactory: Converter.Factory
) : Converter.Factory() {

    override fun responseBodyConverter(type: Type, annotations: Array<Annotation>, retrofit: Retrofit): Converter<ResponseBody, *>? {

        for (annotation in annotations) {
            if (annotation is Json) {
                return jsonFactory.responseBodyConverter(type, annotations, retrofit)
            }
            if (annotation is Xml) {
                return xmlFactory.responseBodyConverter(type, annotations, retrofit)
            }
            if (annotation is Scalar) {
                return scalarFactory.responseBodyConverter(type, annotations, retrofit)
            }
            if (annotation is JsonMoshi) {
                return jsonMoshiFactory.responseBodyConverter(type, annotations, retrofit)
            }
        }
        return null
    }

    override fun requestBodyConverter(
        type: Type,
        parameterAnnotations: Array<Annotation>,
        methodAnnotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<*, RequestBody>? {

        for (annotation in parameterAnnotations) {
            if (annotation is Json) {
                return jsonFactory.requestBodyConverter(
                    type, parameterAnnotations, methodAnnotations,
                    retrofit
                )
            }
            if (annotation is Xml) {
                return xmlFactory.requestBodyConverter(
                    type, parameterAnnotations, methodAnnotations,
                    retrofit
                )
            }
            if (annotation is Scalar) {
                return scalarFactory.requestBodyConverter(
                    type, parameterAnnotations, methodAnnotations,
                    retrofit
                )
            }
            if (annotation is JsonMoshi) {
                return jsonMoshiFactory.requestBodyConverter(
                    type, parameterAnnotations, methodAnnotations,
                    retrofit
                )
            }
        }
        return null
    }
}
