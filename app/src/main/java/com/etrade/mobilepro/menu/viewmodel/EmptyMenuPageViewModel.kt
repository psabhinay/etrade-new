package com.etrade.mobilepro.menu.viewmodel

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.menu.MenuItem
import javax.inject.Inject

class EmptyMenuPageViewModel @Inject constructor(resources: Resources) : MenuPageViewModel(resources) {
    private val _menuPageItems = MutableLiveData<List<MenuItem>>(emptyList())
    override val menuPageItems: LiveData<List<MenuItem>>
        get() = _menuPageItems
}
