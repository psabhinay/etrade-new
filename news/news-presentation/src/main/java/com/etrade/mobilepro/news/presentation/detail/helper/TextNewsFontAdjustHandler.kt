package com.etrade.mobilepro.news.presentation.detail.helper

import androidx.lifecycle.LiveData

interface TextNewsFontAdjustHandler {
    val isLargeFontSize: LiveData<Boolean>
    fun toggleFontSize()
}
