package com.etrade.mobilepro.movers.compose

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import com.etrade.mobilepro.movers.MoverGridData
import com.etrade.mobilepro.movers.MoverItem
import com.etrade.mobilepro.movers.R
import java.math.BigDecimal
import java.util.UUID
import kotlin.random.Random

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun MoverGrid(
    data: MoverGridData,
    columns: Int = 3,
    contentPadding: PaddingValues = PaddingValues(dimensionResource(R.dimen.spacing_medium)),
    onTap: ((MoverItem) -> Unit)? = null
) {
    LazyVerticalGrid(
        cells = GridCells.Fixed(columns),
        contentPadding = contentPadding
    ) {
        items(data.items) { item ->
            MoverCell(item, modifier = Modifier.clickable { onTap?.invoke(item) })
        }
    }
}

@Composable
@Preview
@Suppress("LongMethod", "MagicNumber")
private fun PreviewGrid() {
    fun generateMoverItems(amount: Int): List<MoverItem> = (0..amount).map {
        fun createRandomValueWithFraction() = "${Random.nextInt(from = -100, until = 100)}.${
        Random.nextInt(
            from = -100,
            until = 100
        )
        }"

        val ticker = UUID.randomUUID().toString().takeLast(4).uppercase()
        val hasOptionsInfo = Random.nextBoolean()
        MoverItem(
            ticker = ticker,
            displaySymbol = ticker,
            price = createRandomValueWithFraction(),
            isPositiveGain = Random.nextBoolean(),
            gain = createRandomValueWithFraction(),
            gainPercentage = createRandomValueWithFraction(),
            decimalPercentChange = BigDecimal.valueOf(
                Random.nextDouble(
                    from = -100.0,
                    until = 100.0
                )
            ),
            optionsExpiryDate = if (hasOptionsInfo) {
                "07/09/21"
            } else {
                null
            },
            optionsStrikePricePlusType = if (hasOptionsInfo) {
                "$999 C"
            } else {
                null
            },
            itemClickHandler = { println("$it tapped") },
            shouldDisplayOptionsData = true
        )
    }

    val items = generateMoverItems(7)
    MoverGrid(MoverGridData(items)) // Wrap with theme
}
