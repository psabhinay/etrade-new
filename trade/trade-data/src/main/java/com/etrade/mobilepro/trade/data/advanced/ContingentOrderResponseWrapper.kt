package com.etrade.mobilepro.trade.data.advanced

import com.etrade.mobilepro.orders.api.ContingentCondition
import com.etrade.mobilepro.orders.api.ContingentQualifier
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderPlaceResponse
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderPreviewResponse
import com.etrade.mobilepro.trade.api.advanced.ConfirmedAdvancedOrder
import com.etrade.mobilepro.trade.api.advanced.ContingentOrderDetails
import com.etrade.mobilepro.trade.api.advanced.ContingentOrderResponse

internal class ContingentOrderPreviewResponseWrapper(
    responseDto: AdvancedOrderPreviewResponseDto
) : ContingentOrderResponse, AdvancedOrderPreviewResponse by responseDto {
    override val previewId: String? = responseDto.previewId
    override val contingentOrderDetails: List<ContingentOrderDetails> = createContingentOrders(responseDto)
}

internal class ContingentOrderPlaceResponseWrapper(
    responseDto: AdvancedOrderPlaceResponseDto
) : ContingentOrderResponse, AdvancedOrderPlaceResponse by responseDto {
    override val contingentOrderDetails = createContingentOrders(responseDto)
    override val orders: List<ConfirmedAdvancedOrder> = responseDto.orders
}

internal fun createContingentOrders(responseDto: BaseAdvancedOrderResponseDto): List<ContingentOrderDetails> = responseDto.advancedOrders?.map {
    object : ContingentOrderDetails {
        override val condition = ContingentCondition.fromOffsetType(it.offsetType)
        override val conditionValue = it.offsetValue
        override val qualifier = ContingentQualifier.fromFollowPrice(it.followPrice)
        override val symbol = it.symbol.orEmpty()
        override val displaySymbol: String = it.displaySymbol.orEmpty()
    }
}.orEmpty()
