package com.etrade.mobilepro.marketscalendar.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class EventsEconomicData(
    @Json(name = "ecEcondetName")
    val ecEcondetName: String?,
    @Json(name = "ecEcondetConcensus")
    val ecEcondetConcensus: String?,
    @Json(name = "ecEcondetActual")
    val ecEcondetActual: String?,
    @Json(name = "ecSeStartdate")
    val ecSeStartdate: String?,
    @Json(name = "ecEconDesc")
    val ecEconDesc: String?,
    @Json(name = "ecEconDefinition")
    val ecEconDefinition: String?,
    @Json(name = "ecEconName")
    val ecEconName: String?
)
