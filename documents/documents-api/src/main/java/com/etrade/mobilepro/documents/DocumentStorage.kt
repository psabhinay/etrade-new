package com.etrade.mobilepro.documents

import com.etrade.mobilepro.util.Resource
import kotlinx.coroutines.flow.Flow

interface DocumentStorage {
    suspend fun getDocumentData(document: Document): Flow<Resource<Document>>
    fun getCachedDocument(documentId: String): Document?
    fun getCachedDocuments(documents: List<Document>): List<Document>
    fun clearAllCachedDocuments()
}
