package com.etrade.mobilepro.imageloader.glide

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.imageloader.loadFromUrlAndSetBackgroundColor
import com.etrade.mobilepro.session.api.UserConsumerKeyInterface
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import java.io.InputStream

class GlideImageLoader(context: Context, client: OkHttpClient, userConsumerKey: UserConsumerKeyInterface) : ImageLoader {

    init {
        val glideOkHttpClient = OkHttpUrlLoader.Factory(
            client.newBuilder()
                .apply {
                    interceptors().add(0, DataTokenInterceptor(userConsumerKey))
                }
                .build()
        )

        Glide.get(context).registry.replace(GlideUrl::class.java, InputStream::class.java, glideOkHttpClient)
    }

    override fun show(url: String, target: ImageView, onResourceReady: (() -> Unit)?) {
        Glide.with(target).load(url).listener(object : RequestListener<Drawable> {
            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean = false

            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: Target<Drawable>?,
                dataSource: DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                onResourceReady?.invoke()

                return false
            }
        }).into(target)
    }

    override fun showAndSetBackground(url: String, target: ImageView, color: Int?) {
        target.loadFromUrlAndSetBackgroundColor(url, color)
    }
}

private class DataTokenInterceptor(private val userConsumerKey: UserConsumerKeyInterface) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder()

        userConsumerKey.getConsumerKey()?.also {
            request.addHeader("DataToken", it)
        }

        return chain.proceed(request.build())
    }
}
