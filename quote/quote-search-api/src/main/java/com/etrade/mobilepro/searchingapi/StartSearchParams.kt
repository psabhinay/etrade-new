package com.etrade.mobilepro.searchingapi

import com.etrade.mobilepro.searchingapi.items.SearchQuery

class StartSearchParams(
    val query: SearchQuery,
    val stockQuoteSearchOnly: Boolean,
    val finishOnSearch: Boolean = false,
    val animationParams: SearchAnimationParams? = null,
    val showOwnedSymbols: Boolean = false,
    val selectedAccountId: String? = null,
    val quoteLookupLocation: String? = null
)

class SearchAnimationParams(
    val sharedViewId: Int,
    val animated: Boolean
)
