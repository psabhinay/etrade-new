package com.etrade.mobilepro.home.overview.edit

import android.content.Context
import android.content.Intent
import com.etrade.mobilepro.editlist.EditListActivity
import com.etrade.mobilepro.editlist.EditListViewModel
import com.etrade.mobilepro.editlist.intent.BaseIntentBuilder
import com.etrade.mobilepro.editlist.intent.ListActionBuilder
import com.etrade.mobilepro.editlist.intent.ListEditDslMarker

class EditOverviewActivity : EditListActivity<OverviewWidget>() {

    override val viewModelClass: Class<out EditListViewModel<OverviewWidget>>
        get() = OverViewEditViewModel::class.java

    companion object {

        fun intent(context: Context, init: IntentBuilder.() -> Unit): Intent = IntentBuilder(context).apply(init).intent
    }

    @ListEditDslMarker
    class IntentBuilder(context: Context) : BaseIntentBuilder(Intent(context, EditOverviewActivity::class.java)) {

        init {
            listAction {
                isVisible = false
            }
        }

        private fun listAction(init: ListActionBuilder.() -> Unit) = ListActionBuilder(intent).init()
    }
}
