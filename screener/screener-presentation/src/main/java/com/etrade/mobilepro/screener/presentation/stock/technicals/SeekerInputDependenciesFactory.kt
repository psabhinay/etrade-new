package com.etrade.mobilepro.screener.presentation.stock.technicals

import android.content.res.Resources
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.stock.technicals.percentinput.PercentCustomizer
import com.etrade.mobilepro.screener.presentation.stock.technicals.percentinput.PercentFormatter
import com.etrade.mobilepro.screener.presentation.stock.technicals.percentinput.PercentValidator
import javax.inject.Inject

interface SeekerInputDependenciesFactory {
    fun getPercentInputDependencies(): InputFieldManager.Dependencies
}

class DefaultSeekerInputDependenciesFactory @Inject constructor(private val resources: Resources) : SeekerInputDependenciesFactory {
    override fun getPercentInputDependencies(): InputFieldManager.Dependencies =
        InputFieldManager.Dependencies(
            formatter = PercentFormatter(resources.getString(R.string.percent_format)),
            validator = PercentValidator(resources.getString(R.string.percent_validation_error_desc)),
            customizer = PercentCustomizer()
        )
}
