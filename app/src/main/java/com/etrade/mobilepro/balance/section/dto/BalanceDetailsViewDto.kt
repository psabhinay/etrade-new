package com.etrade.mobilepro.balance.section.dto

import com.etrade.mobilepro.backends.mgs.BaseScreenView
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class BalanceDetailsViewDto(
    @Json(name = "data")
    override val data: Any,
    @Json(name = "accountId")
    val accountId: String,
    @Json(name = "uerId")
    val userId: String,
    @Json(name = "cta")
    override val ctaList: List<CallToActionDto>?
) : BaseScreenView()
