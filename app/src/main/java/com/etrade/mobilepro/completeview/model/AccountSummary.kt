package com.etrade.mobilepro.completeview.model

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.inboxmessages.dynamicui.api.lowcash.LowCashPrompt
import com.etrade.mobilepro.streaming.api.StreamingText
import com.etrade.mobilepro.util.ClickAction
import com.etrade.mobilepro.util.android.CallToAction

interface AccountSummary {
    val uuid: String?
    val name: String?
    val detailLabel: LiveData<String>
    val detailValue: LiveData<StreamingText>
    val additionalSections: List<AccountSummarySection>
    val showName: Boolean
    val showFdicLogo: Boolean
    val callToAction: LiveData<List<CallToAction>?>
    val clickAction: ClickAction?
    val accountSourceLabel: String?

    val callToActionListSize: Int
        get() = callToAction.value?.size ?: 0
    val lowCashPrompt: LowCashPrompt?

    fun onCtaClicked(clickAction: ClickAction?) = Unit
    fun onAugmentationClicked() = Unit
    fun onInfoClicked() = Unit
}
