package com.etrade.mobilepro.util.domain.data

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

class TimeStampUtilTest {

    private val testData = listOf(5, 3, 7, 2, 4)
    private val formatter: (Long) -> String = { "test result is $it" }

    @Test
    fun `test resolveTimeStamp`() {
        val result = testData.resolveTimeStamp(
            transform = { toLong() },
            timeStampFormatter = formatter
        )

        assertEquals("test result is 7", result)
    }

    @Test
    fun `test resolveTimeStamp on empty data set`() {
        val testData = emptyList<Int>()
        val result = testData.resolveTimeStamp(
            transform = { toLong() },
            timeStampFormatter = formatter
        )

        assertNull(result)
    }

    @Test
    fun `test resolveTimeStamp om empty data set after transformation operator`() {
        val result = testData.resolveTimeStamp(
            transform = { null },
            timeStampFormatter = formatter
        )

        assertNull(result)
    }
}
