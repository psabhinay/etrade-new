package com.etrade.mobilepro.order.details.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(name = "OrderDetails", strict = false)
data class StockPlanOrderDetailsResponseDto(

    @field:ElementList(name = "orderhistorylist")
    var orderHistoryList: List<OrderHistory>? = null,

    // NOTE: The use of elementList for singular values is due to the server occasionally returning a response in which there are
    // duplicate tags/entries for each value. This confuses SimpleXML and causes it to fail in parsing unless the element is mapped to a list
    @field:ElementList(entry = "ordernumber", required = false, inline = true, type = String::class)
    var orderNumber: List<String>? = null,

    @field:ElementList(entry = "ordertype", required = false, inline = true, type = String::class)
    var orderType: List<String>? = null,

    @field:ElementList(entry = "orderstatus", required = false, inline = true, type = String::class)
    var orderStatus: List<String>? = null,

    @field:ElementList(entry = "Description", required = false, inline = true, type = String::class)
    var description: List<String>? = null,

    @field:ElementList(entry = "Term", required = false, inline = true, type = String::class)
    var term: List<String>? = null,

    @field:ElementList(entry = "gtdexpiration", required = false, inline = true, type = String::class)
    var gtdExpiration: List<String>? = null,

    @field:ElementList(entry = "sharesexercised", required = false, inline = true, type = String::class)
    var sharesExercised: List<String>? = null,

    @field:ElementList(entry = "sharessold", required = false, inline = true, type = String::class)
    var sharesSold: List<String>? = null,

    @field:ElementList(entry = "sharestoexercise", required = false, inline = true, type = String::class)
    var sharesToExercise: List<String>? = null,

    @field:ElementList(entry = "sharestosell", required = false, inline = true, type = String::class)
    var sharesToSell: List<String>? = null,

    @field:ElementList(entry = "estsharestosell", required = false, inline = true, type = String::class)
    var estSharesToSell: List<String>? = null,

    @field:ElementList(entry = "pricetype", required = false, inline = true, type = String::class)
    var priceType: List<String>? = null,

    @field:ElementList(entry = "limitprice", required = false, inline = true, type = String::class)
    var limitPrice: List<String>? = null,

    @field:ElementList(entry = "grossproceeds", required = false, inline = true, type = String::class)
    var grossProceeds: List<String>? = null,

    @field:ElementList(entry = "estgrossproceeds", required = false, inline = true, type = String::class)
    var estGrossProceeds: List<String>? = null,

    @field:ElementList(entry = "totalprice", required = false, inline = true, type = String::class)
    var totalPrice: List<String>? = null,

    @field:ElementList(entry = "commission", required = false, inline = true, type = String::class)
    var commission: List<String>? = null,

    @field:ElementList(entry = "secfee", required = false, inline = true, type = String::class)
    var secFee: List<String>? = null,

    @field:ElementList(entry = "brokerassistfee", required = false, inline = true, type = String::class)
    var brokerAssistFee: List<String>? = null,

    @field:ElementList(entry = "disbursementfee", required = false, inline = true, type = String::class)
    var disbursementFee: List<String>? = null,

    @field:ElementList(entry = "taxeswithheld", required = false, inline = true, type = String::class)
    var taxesWithheld: List<String>? = null,

    @field:ElementList(entry = "estnetproceeds", required = false, inline = true, type = String::class)
    var estNetProceeds: List<String>? = null,

    @field:ElementList(entry = "netproceeds", required = false, inline = true, type = String::class)
    var netProceeds: List<String>? = null,

    @field:ElementList(entry = "exercisevalue", required = false, inline = true, type = String::class)
    var exerciseValue: List<String>? = null,

    @field:ElementList(entry = "sharestokeep", required = false, inline = true, type = String::class)
    var sharesToKeep: List<String>? = null,

    @field:ElementList(entry = "estsharestokeep", required = false, inline = true, type = String::class)
    var estSharesToKeep: List<String>? = null,

    @field:ElementList(entry = "sharesissued", required = false, inline = true, type = String::class)
    var sharesIssued: List<String>? = null,

    @field:ElementList(entry = "sharestoissue", required = false, inline = true, type = String::class)
    var sharesToIssue: List<String>? = null,

    @field:ElementList(entry = "appreciationsharesvalue", required = false, inline = true, type = String::class)
    var appreciationSharesValue: List<String>? = null,

    @field:ElementList(entry = "estappreciationsharesvalue", required = false, inline = true, type = String::class)
    var estAppreciationSharesValue: List<String>? = null,

    @field:ElementList(entry = "taxsharesvalue", required = false, inline = true, type = String::class)
    var taxSharesValue: List<String>? = null,

    @field:ElementList(entry = "esttaxsharesvalue", required = false, inline = true, type = String::class)
    var estTaxSharesValue: List<String>? = null,

    @field:ElementList(entry = "planprice", required = false, inline = true, type = String::class)
    var planPrice: List<String>? = null,

    @field:ElementList(entry = "granttype", required = false, inline = true, type = String::class)
    var grantType: List<String>? = null,

    @field:ElementList(entry = "plannumber", required = false, inline = true, type = String::class)
    var planNumber: List<String>? = null,

    @field:ElementList(entry = "plandate", required = false, inline = true, type = String::class)
    var planDate: List<String>? = null,

    @field:ElementList(entry = "vestperiod", required = false, inline = true, type = String::class)
    var vestPeriod: List<String>? = null,

    @field:ElementList(entry = "costbasis", required = false, inline = true, type = String::class)
    var costBasis: List<String>? = null,

    @field:ElementList(entry = "plantype", required = false, inline = true, type = String::class)
    var planType: List<String>? = null
)

@Root(name = "orderhistory", strict = false)
data class OrderHistory(

    @field:Element(name = "price", required = false)
    var price: String? = null,

    @field:Element(name = "transaction", required = false)
    var transaction: String? = null,

    @field:Element(name = "sellquantity", required = false)
    var sellQuantity: String? = null,

    @field:Element(name = "date", required = false)
    var date: String? = null,

    @field:Element(name = "exercisequantity", required = false)
    var exerciseQuantity: String? = null
)
