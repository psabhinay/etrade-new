package com.etrade.mobilepro.trade.api.option

import com.etrade.mobilepro.trade.api.OrderResponse

interface OptionOrderResponse : OrderResponse {
    override val orders: List<OptionOrder>
}
