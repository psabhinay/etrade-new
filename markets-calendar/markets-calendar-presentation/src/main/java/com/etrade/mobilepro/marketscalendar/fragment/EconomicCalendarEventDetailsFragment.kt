package com.etrade.mobilepro.marketscalendar.fragment

import android.view.LayoutInflater
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.marketscalendar.MarketCalendarRouter
import com.etrade.mobilepro.marketscalendar.adapter.delegate.EconomicEventDetailsAdapterDelegate
import com.etrade.mobilepro.marketscalendar.viewmodel.EventDetails
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import javax.inject.Inject

@RequireLogin
class EconomicCalendarEventDetailsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    router: MarketCalendarRouter
) : CalendarEventDetailsFragment(
    viewModelFactory
) {

    override val item: EventDetails? by lazy { router.getEconomicEventDetailsItem(arguments) }

    override fun createAdapterDelegate(layoutInflater: LayoutInflater, noValue: String): AdapterDelegate<MutableList<EventDetails>> {
        return EconomicEventDetailsAdapterDelegate(noValue)
    }
}
