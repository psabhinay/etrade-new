package com.etrade.mobilepro.messagecenter.presentation.details.service

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.api.model.ServiceMessage
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterRepo
import com.etrade.mobilepro.util.delegate.consumable
import kotlinx.coroutines.launch
import javax.inject.Inject

class ServiceMessageDetailsViewModel @Inject constructor(
    private val repo: MessageCenterRepo
) : ViewModel() {
    private val _viewState = MutableLiveData<ViewState>()
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    internal fun refresh(message: Message, forced: Boolean) {
        val current = _viewState.value
        if (current == null || forced) {
            _viewState.value = current?.toLoading() ?: ViewState(loading = ViewState.Loading.INITIAL)
            viewModelScope.launch {
                repo
                    .getServiceMessageDetails(message)
                    .fold(
                        onSuccess = { serviceMessageDetails ->
                            _viewState.updateValue { it.toSuccess(serviceMessageDetails) }
                        },
                        onFailure = {
                            _viewState.updateValue { it.toFailed { refresh(message, forced = true) } }
                        }
                    )
            }
        }
    }

    internal data class ViewState(
        val loading: Loading = Loading.NONE,
        val error: Error = Error.None,
        val item: ServiceMessage = ServiceMessage(),
        private val needToRefreshFlag: Boolean = false
    ) {
        val needToRefreshList by consumable(needToRefreshFlag)
        enum class Loading {
            NONE, INITIAL
        }

        sealed class Error {
            object None : Error()
            class Loading(
                val retry: () -> Unit
            ) : Error()
        }

        fun toLoading(): ViewState = this.copy(loading = Loading.INITIAL, error = Error.None)
        fun toFailed(retry: () -> Unit): ViewState = this.copy(loading = Loading.NONE, error = Error.Loading(retry))
        fun toSuccess(item: ServiceMessage): ViewState =
            this.copy(loading = Loading.NONE, error = Error.None, item = item, needToRefreshFlag = false)
    }
}
