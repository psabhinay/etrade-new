package com.etrade.mobilepro.tableviewutils.presentation.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.doOnLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import com.etrade.mobilepro.instrument.underlier
import com.etrade.mobilepro.tableviewutils.presentation.R
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.DataCell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.ColumnHeaderViewHolder
import com.etrade.mobilepro.tableviewutils.presentation.holder.DataCellViewHolder
import com.etrade.mobilepro.tableviewutils.presentation.holder.HasDataCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.RowHeaderViewHolder
import com.etrade.mobilepro.tableviewutils.presentation.util.ColumnHeaderMeasuredSize
import com.etrade.mobilepro.tableviewutils.presentation.util.ColumnRowSizeCache
import com.etrade.mobilepro.tableviewutils.presentation.util.ViewMeasuredSize
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import com.etrade.mobilepro.util.android.extension.inflate
import com.evrencoskun.tableview.ITableView
import com.evrencoskun.tableview.TableView
import com.evrencoskun.tableview.adapter.AbstractTableAdapter
import com.evrencoskun.tableview.adapter.recyclerview.AbstractRecyclerViewAdapter
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

// Fix for ETAND-18096, on some devises size measurement of [TextView] with the same text value might have different results.
private const val ROUNDING_CORRECTION_PIXEL = 1

/**
 * This is a sample of custom TableView Adapter.
 */
@Suppress("TooManyFunctions", "LargeClass")
open class TableViewAdapter(context: Context) : AbstractTableAdapter<Cell, Cell, DataCell>(context) {

    var columnRowSizeCache: ColumnRowSizeCache
        private set

    protected val defaultHeightOfRows: Int
    private val defaultHeaderHeight: Int
    protected val inflater: LayoutInflater = LayoutInflater.from(context)

    private val columnHeaderViewHolderSamples = mutableMapOf<Int, AbstractViewHolder>()
    private val rowHeaderViewHolderSamples = mutableMapOf<Int, AbstractViewHolder>()
    private val cellViewHolderSamples = mutableMapOf<Int, AbstractViewHolder>()
    private var cornerViewMeasuredSize: ViewMeasuredSize? = null

    init {
        val defaultColumnWidth = context.resources.getDimensionPixelSize(R.dimen.default_column_with)
        defaultHeightOfRows = context.resources.getDimensionPixelSize(R.dimen.spacing_xxxlarge)
        defaultHeaderHeight = context.resources.getDimensionPixelSize(R.dimen.table_view_header_height)
        columnRowSizeCache = ColumnRowSizeCache(
            defaultColumnWidth,
            defaultHeightOfRows,
            defaultHeaderHeight,
            ::measureCellWidth,
            ::measureHeaderWidth,
            ::measureHeaderHeight,
            ::measureRowHeaderHeight,
            ::measureCornerViewHeight
        )
    }

    override fun setTableView(tableView: TableView?) {
        tableView?.doOnLayout { view ->
            columnRowSizeCache.adjustColumnWidthsToMatchParentWidth(getParentWidth(view as TableView)).takeIf { it }?.let {
                notifyDataSetChanged()
            }
        }
        super.setTableView(tableView)
    }

    /**
     * This is where you create your custom Cell ViewHolder. This method is called when Cell
     * RecyclerView of the TableView needs a new RecyclerView.ViewHolder of the given type to
     * represent an item.
     *
     * @param viewType : This value comes from "getCellItemViewType" method to support different
     * type of viewHolder as a Cell item.
     * @see .getCellItemViewType
     */
    override fun onCreateCellViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        val layout: View = inflater.inflate(R.layout.table_view_cell_layout, parent, false)
        // Create a Cell ViewHolder
        return DataCellViewHolder(layout, null)
    }

    /**
     * That is where you set Cell View Model displayText to your custom Cell ViewHolder. This method is
     * Called by Cell RecyclerView of the TableView to display the displayText at the specified position.
     * This method gives you everything you need about a cell item.
     *
     * @param holder : This is one of your cell ViewHolders that was created on
     * ```onCreateCellViewHolder``` method. In this example we have created
     * "DataCellViewHolder" holder.
     * @param cellItemModel  : This is the cell view model located on this X and Y position. In this
     * example, the model class is "Cell".
     * @param columnPosition : This is the X (Column) position of the cell item.
     * @param rowPosition    : This is the Y (Row) position of the cell item.
     * @see .onCreateCellViewHolder
     */
    override fun onBindCellViewHolder(holder: AbstractViewHolder, cellItemModel: Any, columnPosition: Int, rowPosition: Int) {
        val cell = cellItemModel as DataCell
        val params = holder.itemView.layoutParams
        params.width = columnRowSizeCache[columnPosition]
        rowHeaderRecyclerViewAdapter?.items?.getOrNull(rowPosition)?.let {
            params.height = columnRowSizeCache.getRowHeight(it as Cell)
        }
        bindCellViewHolder(holder, cell)
    }

    private fun bindCellViewHolder(holder: AbstractViewHolder, cell: DataCell) {
        (holder as HasDataCell).cell = cell
    }

    /**
     * This is where you create your custom Column Header ViewHolder. This method is called when
     * Column Header RecyclerView of the TableView needs a new RecyclerView.ViewHolder of the given
     * type to represent an item.
     *
     * @param viewType : This value comes from "getColumnHeaderItemViewType" method to support
     * different type of viewHolder as a Column Header item.
     * @see .getColumnHeaderItemViewType
     */
    override fun onCreateColumnHeaderViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        // TODO: check
        // Log.e(LOG_TAG, " onCreateColumnHeaderViewHolder has been called");
        // Get Column Header xml Layout
        val layout = inflater.inflate(R.layout.table_view_column_header_layout, parent, false)
        // Create a ColumnHeader ViewHolder
        return ColumnHeaderViewHolder(layout)
    }

    /**
     * That is where you set Column Header View Model displayText to your custom Column Header ViewHolder.
     * This method is Called by ColumnHeader RecyclerView of the TableView to display the displayText at
     * the specified position. This method gives you everything you need about a column header
     * item.
     *
     * @param holder : This is one of your column header ViewHolders that was created
     * on ```onCreateColumnHeaderViewHolder``` method. In this example
     * we have created "ColumnHeaderViewHolder" holder.
     * @param columnHeaderItemModel : This is the column header view model located on this X
     * position. In this example, the model class is "ColumnHeader".
     * @param columnPosition        : This is the X (Column) position of the column header item.
     * @see .onCreateColumnHeaderViewHolder
     */
    override fun onBindColumnHeaderViewHolder(holder: AbstractViewHolder, columnHeaderItemModel: Any, columnPosition: Int) {
        holder as ColumnHeaderViewHolder
        val columnHeader = columnHeaderItemModel as Cell
        val columnWidth = columnRowSizeCache[columnPosition]
        val params = holder.itemView.layoutParams
        params.width = columnWidth
        params.height = columnRowSizeCache.columnHeaderHeight

        val measuredWidths: MeasuredHeaderWidth = measuredHeaderSizes[columnHeader.displayText] ?: return
        measuredWidths.run {
            holder.columnHeaderTextview.width = textViewWidth + ROUNDING_CORRECTION_PIXEL
        }
        bindColumnHeaderViewHolder(holder, columnHeader)
    }

    private fun bindColumnHeaderViewHolder(holder: AbstractViewHolder, columnHeaderModel: Cell) {
        val columnHeaderViewHolder = holder as ColumnHeaderViewHolder
        columnHeaderViewHolder.setColumnHeader(columnHeaderModel)
    }

    /**
     * This is where you create your custom Row Header ViewHolder. This method is called when
     * Row Header RecyclerView of the TableView needs a new RecyclerView.ViewHolder of the given
     * type to represent an item.
     *
     * @param viewType : This value comes from "getRowHeaderItemViewType" method to support
     * different type of viewHolder as a row Header item.
     * @see .getRowHeaderItemViewType
     */
    override fun onCreateRowHeaderViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        // Get Row Header xml Layout
        val layout = inflater.inflate(R.layout.table_view_row_header_layout, parent, false)
        return RowHeaderViewHolder(layout)
    }

    /**
     * That is where you set Row Header View Model displayText to your custom Row Header ViewHolder. This
     * method is Called by RowHeader RecyclerView of the TableView to display the displayText at the
     * specified position. This method gives you everything you need about a row header item.
     *
     * @param holder : This is one of your row header ViewHolders that was created on
     * ```onCreateRowHeaderViewHolder``` method. In this example we have
     * created "RowHeaderViewHolder" holder.
     * @param rowHeaderItemModel : This is the row header view model located on this Y position. In
     * this example, the model class is "RowHeader".
     * @param rowPosition        : This is the Y (row) position of the row header item.
     * @see .onCreateRowHeaderViewHolder
     */
    override fun onBindRowHeaderViewHolder(
        holder: AbstractViewHolder,
        rowHeaderItemModel: Any,
        rowPosition: Int
    ) {
        holder.itemView.layoutParams.height = columnRowSizeCache.getRowHeight(rowHeaderItemModel as Cell)
        bindRowHeaderViewHolder(holder, rowHeaderItemModel, rowPosition)
    }

    protected open fun bindRowHeaderViewHolder(holder: AbstractViewHolder, rowHeaderItemModel: Any, rowPosition: Int) {
        val rowHeader = rowHeaderItemModel as Cell

        // Get the holder to update row header item text
        val rowHeaderViewHolder = holder as RowHeaderViewHolder
        rowHeaderViewHolder.rowHeaderTextView.text = rowHeader.displayText
    }

    override fun onCreateCornerView(parent: ViewGroup): View {
        val cornerLayout: View = parent.inflate(R.layout.table_view_corner_header_layout)
        cornerLayout.apply {
            val cornerContainer = findViewById<View>(R.id.columnHeaderContainer)
            val cornerTextView = findViewById<TextView>(R.id.cornerHeaderTextview)
            val cornerImageView = findViewById<ImageView>(R.id.cornerHeaderSortButton)
            cornerContainer.contentDescription = "${cornerTextView.contentDescription} ${cornerImageView.contentDescription}"
        }
        return cornerLayout
    }

    override fun getColumnHeaderItemViewType(position: Int): Int {
        // The unique ID for this type of column header item
        // If you have different items for Cell View by X (Column) position,
        // then you should fill this method to be able create different
        // type of DataCellViewHolder on "onCreateCellViewHolder"
        return 0
    }

    override fun getRowHeaderItemViewType(position: Int): Int {
        // The unique ID for this type of row header item
        // If you have different items for Row Header View by Y (Row) position,
        // then you should fill this method to be able create different
        // type of RowHeaderViewHolder on "onCreateRowHeaderViewHolder"
        return 0
    }

    override fun getCellItemViewType(row: Int, column: Int): Int {
        return 0
    }

    /**
     * Get type with a cell item, needs to be implemented if getAllCellViewTypes is not implemented.
     * Also needs to be implemented if internally getCellItemViewType(row: Int, column: Int) calls getCellItem
     * since we're not setting items before getting types now.
     */
    open fun getCellItemViewType(cellItem: Cell?, row: Int, column: Int): Int {
        return 0
    }

    // Helper methods to make calculation take less time by knowing all view types beforehand
    open fun getAllCellViewTypes(): List<Int> {
        return listOf(0)
    }

    open fun getAllRowHeaderViewTypes(): List<Int> {
        return listOf(0)
    }

    protected open fun getCellWidth(column: Int, headerCell: Cell): Int? = null

    @SuppressLint("InflateParams")
    override fun setAllItems(columnHeaderItems: List<Cell>, rowHeaderItems: List<Cell>, cellItems: List<List<DataCell>>) {
        val rowOfLargestItems = cellItems.getOrNull(0)?.toMutableList()
        cellItems.forEach { row ->
            row.forEachIndexed { columnIndex, rowItem ->
                if (rowItem.displayText.length > (rowOfLargestItems?.getOrNull(columnIndex)?.displayText?.length ?: 0)) {
                    rowOfLargestItems?.set(columnIndex, rowItem)
                }
            }
        }
        initViewHolderSamples(columnHeaderItems)
        columnRowSizeCache.measure(
            getParentWidth(tableView as? TableView),
            columnHeaderItems,
            rowHeaderItems,
            rowOfLargestItems?.let { listOf(it) } ?: emptyList(),
            ::getCellWidth
        )
        tableView?.columnHeaderHeight = columnRowSizeCache.columnHeaderHeight
        super.setAllItems(columnHeaderItems, rowHeaderItems, cellItems)
    }

    fun updateAllItems(columnHeaders: List<Cell>, rowHeaders: List<Cell>, cells: List<List<DataCell>>, forceNotifyData: Boolean = false) {
        val oldColumnHeaders = columnHeaderRecyclerViewAdapter.items
        val oldRowHeaders = rowHeaderRecyclerViewAdapter.items

        if (columnHeaders != oldColumnHeaders || rowHeaders.size != oldRowHeaders.size) {
            setAllItems(columnHeaders, rowHeaders, cells)
        } else {
            if (oldRowHeaders != rowHeaders) {
                setRowHeaderItems(rowHeaders)
            }

            if (columnHeaders != oldColumnHeaders) {
                setColumnHeaderItems(columnHeaders)
            }
            cellRecyclerViewAdapter.setItems(cells, forceNotifyData)
            updateVisibleRows(cells, rowHeaders, oldRowHeaders)
        }
    }

    protected fun SymbolCell.getSymbolContentDescription() = underlier.symbol.characterByCharacter()

    @Suppress("UNCHECKED_CAST")
    private fun updateVisibleRows(cellItems: List<List<DataCell>>, rowHeaders: List<Cell>, oldRowHeaders: List<Any>) {
        tableView.cellLayoutManager.visibleCellRowRecyclerViews.forEach { rowRecyclerView ->
            val holderPosition = tableView.cellRecyclerView.getChildAdapterPosition(rowRecyclerView)

            if (holderPosition != NO_POSITION) {
                val updatedRowItems = cellItems[holderPosition]
                val rowAdapter = rowRecyclerView.adapter as AbstractRecyclerViewAdapter<DataCell>
                val oldRowItems: List<DataCell> = rowAdapter.items

                if (rowHeaders[holderPosition] != oldRowHeaders[holderPosition]) {
                    rowAdapter.items = updatedRowItems
                } else if (updatedRowItems != oldRowItems) {
                    val result: DiffUtil.DiffResult = DiffUtil.calculateDiff(
                        CellItemsDiffUtilCallback(oldRowItems, updatedRowItems)
                    )
                    rowAdapter.setItems(updatedRowItems, false)
                    result.dispatchUpdatesTo(rowAdapter)
                }
            }
        }
    }

    private fun initViewHolderSamples(columnHeaderItems: List<Cell>) {
        tableView?.let { view ->
            columnHeaderItems.forEachIndexed { index, _ ->
                val viewType = getColumnHeaderItemViewType(index)
                view.createViewHolderSample(CellRecyclerViewType.COLUMN, viewType)
            }
            getAllRowHeaderViewTypes().forEach { viewType ->
                view.createViewHolderSample(CellRecyclerViewType.ROW, viewType)
            }
            getAllCellViewTypes().forEach { viewType ->
                view.createViewHolderSample(CellRecyclerViewType.CELL, viewType)
            }
            if (cornerViewMeasuredSize == null) {
                cornerViewMeasuredSize = onCreateCornerView(view.columnHeaderRecyclerView).measureViewSize(
                    widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(view.rowHeaderWidth, View.MeasureSpec.EXACTLY)
                )
            }
        }
    }

    fun updateColumnWidthCache(columnRowSizeCache: ColumnRowSizeCache) {
        this.columnRowSizeCache.widths = columnRowSizeCache.widths
        notifyDataSetChanged()
    }

    private fun getCellViewTypeCatching(item: Cell, rowIndex: Int, columnIndex: Int): Int {
        return runCatching {
            getCellItemViewType(rowIndex, columnIndex)
        }.getOrNull() ?: getCellItemViewType(item, rowIndex, columnIndex)
    }

    private fun measureCellWidth(cell: DataCell, row: Int, column: Int): Int {
        return cellViewHolderSamples[getCellViewTypeCatching(cell, row, column)]?.let { holder ->
            holder.measureHolder(
                { bindCellViewHolder(holder, cell) },
                { itemView.measureViewSize().width }
            )
        } ?: 0
    }

    private val measuredHeaderSizes = mutableMapOf<String, MeasuredHeaderWidth>()

    private fun measureHeaderWidth(cell: Cell, position: Int): Int {
        return columnHeaderViewHolderSamples[getColumnHeaderItemViewType(position)]?.let { holder ->
            holder as ColumnHeaderViewHolder
            // bind and measure text
            val headerMeasuredSize = holder.bindAndMeasureColumnHeaderWidths(cell)

            val measuredValues = MeasuredHeaderWidth(
                holderWidth = headerMeasuredSize.containerWidth,
                holderHeight = headerMeasuredSize.containerHeight,
                textViewWidth = headerMeasuredSize.titleViewWidth,
            )
            measuredHeaderSizes[cell.displayText] = measuredValues
            // we want to calculate column width based on the shortest header text
            measuredValues.holderWidth
        } ?: 0
    }

    private fun measureHeaderHeight(cell: Cell): Int {
        return measuredHeaderSizes[cell.displayText]?.holderHeight ?: defaultHeaderHeight
    }

    private fun measureCornerViewHeight() = cornerViewMeasuredSize?.height ?: defaultHeaderHeight

    private fun measureRowHeaderHeight(cell: Cell, position: Int): Int {
        return rowHeaderViewHolderSamples[getRowHeaderItemViewType(position)]?.let { holder ->
            holder.measureHolder(
                { bindRowHeaderViewHolder(holder, cell, position) },
                {
                    val widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(tableView.rowHeaderWidth, View.MeasureSpec.EXACTLY)
                    itemView.measureViewSize(widthMeasureSpec = widthMeasureSpec).height
                }
            )
        } ?: 0
    }

    private enum class CellRecyclerViewType {
        COLUMN,
        ROW,
        CELL
    }

    @Suppress("ComplexMethod")
    private fun ITableView.createViewHolderSample(cellRecyclerViewType: CellRecyclerViewType, viewType: Int) {
        when (cellRecyclerViewType) {
            CellRecyclerViewType.COLUMN -> {
                columnHeaderViewHolderSamples[viewType] ?: onCreateColumnHeaderViewHolder(columnHeaderRecyclerView, viewType).also {
                    columnHeaderViewHolderSamples[viewType] = it
                }
            }
            CellRecyclerViewType.ROW -> {
                rowHeaderViewHolderSamples[viewType] ?: onCreateRowHeaderViewHolder(rowHeaderRecyclerView, viewType).also {
                    rowHeaderViewHolderSamples[viewType] = it
                }
            }
            CellRecyclerViewType.CELL -> {
                cellViewHolderSamples[viewType] ?: onCreateCellViewHolder(cellRecyclerView, viewType).also {
                    cellViewHolderSamples[viewType] = it
                }
            }
        }
    }

    private fun ColumnHeaderViewHolder.bindAndMeasureColumnHeaderWidths(cell: Cell) = measureHolder(
        {
            bindColumnHeaderViewHolder(this, cell)

            // if table view is sortable we want to measure column holder width with visible sort icon
            if (tableView?.isSortable == true) {
                itemView.findViewById<View>(R.id.columnHeaderSortButton).visibility = View.VISIBLE
            }
        },
        {
            val containerSize = itemView.measureViewSize()
            ColumnHeaderMeasuredSize(
                containerSize.width,
                containerSize.height,
                columnHeaderTextview.measureViewSize().width
            )
        }
    )

    private fun <R> AbstractViewHolder.measureHolder(bindHolder: () -> Unit, measure: AbstractViewHolder.() -> R): R {
        bindHolder()
        return measure()
    }

    private fun View.measureViewSize(
        widthMeasureSpec: Int = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
        heightMeasureSpec: Int = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
    ): ViewMeasuredSize {
        measure(
            widthMeasureSpec,
            heightMeasureSpec
        )
        return ViewMeasuredSize(measuredWidth, measuredHeight)
    }

    private class CellItemsDiffUtilCallback(
        val oldCellItems: List<DataCell>?,
        val newCellItems: List<DataCell>?
    ) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = true

        override fun getOldListSize(): Int = oldCellItems?.size ?: 0

        override fun getNewListSize(): Int = newCellItems?.size ?: 0

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldCellItems?.get(oldItemPosition) == newCellItems?.get(newItemPosition)
        }
    }

    private fun getParentWidth(parent: TableView?): Int = parent?.let { view ->
        view.width - view.rowHeaderWidth
    } ?: 0

    private data class MeasuredHeaderWidth(
        val holderWidth: Int,
        val holderHeight: Int,
        val textViewWidth: Int
    )
}
