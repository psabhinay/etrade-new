package com.etrade.mobilepro.devtemplate

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.markets.MarketsGenericLayout
import com.etrade.mobilepro.markets.OverviewItem
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.data.response.MarketFutureResponse
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class DevTemplateViewModel @Inject constructor(private val marketsService: MoverService) : ViewModel(), ViewModelInterface {
    private val _viewStates = MutableLiveData<ViewModelInterface.ViewState>()
    val viewState: LiveData<ViewModelInterface.ViewState> = _viewStates
    private val compositeDisposable = CompositeDisposable()
    private val logger: Logger = LoggerFactory.getLogger(DevTemplateViewModel::class.java)

    override fun fetchDataFromServer() {
        _viewStates.value = ViewModelInterface.ViewState.Loading
        val single: Single<MarketFutureResponse> = marketsService.getMarketFutures()
        compositeDisposable.add(
            single.subscribeBy(
                onSuccess = { marketFutureResponse ->
                    val listOfQuotes = mutableListOf<DynamicViewModel>()
                    marketFutureResponse.quotes?.mapNotNull {
                        listOfQuotes.add(MarketsGenericLayout.Quote(OverviewItem(it.symbol!!, it.symbol!!, it.price!!, it.change!!, it.percentchange!!)))
                    }
                    _viewStates.postValue(ViewModelInterface.ViewState.Success(listOfQuotes))
                },
                onError = { throwable ->
                    logger.error("Markets futures fetch failed", throwable)
                    _viewStates.postValue(ViewModelInterface.ViewState.Error(""))
                }
            )
        )
    }

    init {
        _viewStates.value = ViewModelInterface.ViewState.Loading
        fetchDataFromServer()
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}
