package com.etrade.mobilepro.util.delegate

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * Returns a property delegate for a read/write property that calls a specified callback function when changed. Setting of [initialValue] is not considered as
 * the change of a value, but if the same value provided again, [onChange] will be triggered.
 *
 * @param initialValue initial value
 * @param onChange will be called anytime a value is set
 */
inline fun <reified T> suppressInitial(initialValue: T, crossinline onChange: () -> Unit): ReadWriteProperty<Any?, T> {
    return object : SuppressedInitialValueProperty<T>(initialValue) {
        override fun onValueChanged() = onChange()
    }
}

/**
 * Suppresses the callbacks for initial values. All subsequent sets to this property will trigger [onValueChanged] method if values are not equal or if the
 * value is provided for the first time (not initial).
 *
 * @param currentValue provides initial value
 */
abstract class SuppressedInitialValueProperty<T>(private var currentValue: T) : ReadWriteProperty<Any?, T> {

    private var actualValueIsSet: Boolean = false

    override fun getValue(thisRef: Any?, property: KProperty<*>): T = currentValue

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        if (actualValueIsSet && currentValue == value) { return }
        currentValue = value
        actualValueIsSet = true
        onValueChanged()
    }

    /**
     * Triggered whenever value is changed or the first value is provided.
     */
    protected abstract fun onValueChanged()
}
