package com.etrade.mobilepro.billpay.data.rest

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PaymentListResponseDto(
    @Json(name = "payments")
    val payments: List<PaymentListEntryDto>
) : BaseDataDto()

@JsonClass(generateAdapter = true)
class PaymentListEntryDto(
    @Json(name = "paymentDetails")
    val paymentDetails: PaymentDetailsEntryDto,

    @Json(name = "payeeDetails")
    val payeeDetails: PayeeDetailsEntryDto
)

@JsonClass(generateAdapter = true)
class PaymentDetailsEntryDto(
    @Json(name = "paymentId")
    val paymentId: String,

    @Json(name = "memo")
    val memo: String?,

    @Json(name = "confirmationNumber")
    val confirmationNumber: String?,

    @Json(name = "status")
    val status: String?,

    @Json(name = "amount")
    val amount: String?,

    @Json(name = "accountId")
    val accountId: String?,

    @Json(name = "scheduleDate")
    val scheduleDate: String?,

    @Json(name = "deliveryDate")
    val deliveryDate: String?
)

@JsonClass(generateAdapter = true)
class PayeeDetailsEntryDto(
    @Json(name = "payeeId")
    val payeeId: String,

    @Json(name = "name")
    val name: String?,

    @Json(name = "nickName")
    val nickName: String?,

    @Json(name = "leadDays")
    val leadDays: String?,

    @Json(name = "isAddressAvailable")
    val isAddressAvailable: Boolean?,

    @Json(name = "eBillActivationStatus")
    val eBillActivationStatus: String?,

    @Json(name = "merchantId")
    val merchantId: String?,

    @Json(name = "merchantZipRequired")
    val merchantZipRequired: Boolean?,

    @Json(name = "accountNumber")
    val accountNumber: String?,

    @Json(name = "category")
    val category: String?,

    @Json(name = "ebillId")
    val ebillId: String?,

    @Json(name = "address")
    val address: PayeeAddressEntryDto?
)

@JsonClass(generateAdapter = true)
class PayeeAddressEntryDto(
    @Json(name = "address1")
    val address1: String?,

    @Json(name = "address2")
    val address2: String?,

    @Json(name = "city")
    val city: String?,

    @Json(name = "state")
    val state: String?,

    @Json(name = "zipCode")
    val zipCode: String?
)
