package com.etrade.mobilepro.iraaccount.data

import com.etrade.mobilepro.iraaccount.api.IraContributionDetails
import org.threeten.bp.LocalDate

internal data class ContributionDetails(
    override val availableDepositTypes: List<String>,
    override val shouldShowPreviousYear: Boolean,
    override val currentYearContributionDetails: IraContributionDetails.Details,
    override val previousYearContributionDetails: IraContributionDetails.Details?
) : IraContributionDetails {

    data class Details(
        override val taxYear: String,
        override val yearToDateContribution: String,
        override val contributionLimit: String,
        override val contributionAmountLeft: String,
        override val contributionDeadline: LocalDate?
    ) : IraContributionDetails.Details
}
