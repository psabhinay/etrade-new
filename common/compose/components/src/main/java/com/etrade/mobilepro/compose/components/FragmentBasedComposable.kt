package com.etrade.mobilepro.compose.components

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.util.Xml
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import org.slf4j.LoggerFactory
import javax.inject.Inject

interface FragmentBasedComposable {
    /**
     * @param fragmentManager Host for regular Fragment content.
     * @param isLazilyHoisted Flag to indicate if the content is gonna be lazily hoisted by compose components like
     * [LazyRow] or [HorizontalPager].
     */
    @Composable
    fun Content(
        fragmentManager: FragmentManager,
        isLazilyHoisted: Boolean,
        params: Params
    )

    /**
     * @param layoutResId Layout that must comply with the following:
     *
     * - Root element is [FragmentContainerView].
     * - [FragmentContainerView] statically references a fragment.
     * - [FragmentContainerView] has an android id.
     *
     * @param fragmentContainerViewId Must be the same android id defined in [layoutResId] for
     * [FragmentContainerView] because the later uses it as id for its fragment.
     */
    class Params(
        val layoutResId: Int,
        val fragmentContainerViewId: Int
    )
}

class FragmentBasedComposableImpl @Inject constructor() : FragmentBasedComposable {

    private val logger = LoggerFactory.getLogger(FragmentBasedComposableImpl::class.java)

    @Composable
    @Suppress("TooGenericExceptionCaught")
    override fun Content(
        fragmentManager: FragmentManager,
        isLazilyHoisted: Boolean,
        params: FragmentBasedComposable.Params
    ) {
        AndroidView(
            modifier = Modifier.fillMaxSize(),
            factory = { context ->
                if (isLazilyHoisted) {
                    attemptViewRestoration(fragmentManager, params.fragmentContainerViewId)
                }
                try {
                    getWrappedQuoteFragment(
                        context = context,
                        fragmentManager = fragmentManager,
                        layoutResId = params.layoutResId,
                        fragmentId = params.fragmentContainerViewId
                    )!!
                } catch (ex: Exception) {
                    logger.error(
                        "Couldn't generate wrapped fragment via reflection",
                        ex
                    )
                    View(context)
                }
            }
        )
    }

    /**
     * Lazily switching layouts in compose (e.x. HorizontalPager) doesn't trigger view restoration in Fragments.
     * This function attempts to mimic view restoration for that scenario.
     */
    private fun attemptViewRestoration(
        fragmentManager: FragmentManager,
        @IdRes
        fragmentId: Int
    ) {
        val quoteFragment = fragmentManager.findFragmentById(fragmentId)

        if (quoteFragment != null) {
            with(fragmentManager) {

                beginTransaction()
                    .detach(quoteFragment)
                    .commitNowAllowingStateLoss()

                beginTransaction()
                    .attach(quoteFragment)
                    .commitNowAllowingStateLoss()
            }
        }
    }

    /**
     * Get a fragment instance wrapped in a [FragmentContainerView] which has access to androidx library protected
     * APIs (e.x. [FragmentManager.onContainerAvailable]) useful for dynamically placing fragments ina view container.
     *
     * Note: [FragmentContainerView] doesn't have the ability to pass arguments to its target fragment. If the target fragment
     * requires arguments ([Bundle]), a solution using dynamic DI or a Service Locator needs to be implemented.
     */
    private fun getWrappedQuoteFragment(
        context: Context,
        fragmentManager: FragmentManager,
        @LayoutRes layoutResId: Int,
        @IdRes fragmentId: Int
    ): View? {
        return instantiateFragmentContainerView(
            context = context,
            attr = getRootAttributeSet(context, layoutResId),
            fragmentManager = fragmentManager
        )?.also {
            markFragmentAsRestored(
                fragmentManager = fragmentManager,
                fragmentId = fragmentId
            )
        }
    }

    /**
     * Get a [FragmentContainerView] instance using reflection to access protected constructor
     * FragmentContainerView(context: Context,attrs: AttributeSet, fm: FragmentManager) so we can specify
     * a [FragmentManager] (supportFragmentManager, parentFragmentManager or childFragmentManager) that better
     * suits a wrapped fragment and its container.
     *
     * By default, [FragmentContainerView] is in charge of resolving a [FragmentManager] but it fallbacks to
     * [AppCompatActivity.getSupportFragmentManager] for certain cases (e.x. target Fragment in a [RecyclerView]).
     */
    private fun instantiateFragmentContainerView(
        context: Context,
        attr: AttributeSet,
        fragmentManager: FragmentManager
    ): FragmentContainerView? {

        return FragmentContainerView::class.java.getDeclaredConstructor(
            Context::class.java,
            AttributeSet::class.java,
            FragmentManager::class.java
        ).run {
            isAccessible = true
            newInstance(context, attr, fragmentManager)
        }
    }

    private fun getRootAttributeSet(context: Context, xmlResId: Int): AttributeSet {
        return context.resources
            .getXml(xmlResId)
            .apply {
                next()
                nextTag()
            }.let(Xml::asAttributeSet)
    }

    /**
     * Prevent a crash where a fragment hoisted by [FragmentContainerView] is not correctly restored by [FragmentStateManager]
     * due to the fragment not being recreated when navigating back from another nav destination.
     */
    private fun markFragmentAsRestored(
        fragmentManager: FragmentManager,
        @IdRes fragmentId: Int
    ) {
        val fragment = fragmentManager.findFragmentById(fragmentId)

        if (fragment != null) {
            val field = fragment::class.java.superclass.getDeclaredField("mRestored")
            field.isAccessible = true
            field.set(fragment, true)
        }
    }
}
