package com.etrade.mobilepro.tableviewutils.presentation

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.evrencoskun.tableview.ITableView
import com.evrencoskun.tableview.adapter.AbstractTableAdapter
import com.evrencoskun.tableview.adapter.recyclerview.RowHeaderRecyclerViewAdapter
import com.evrencoskun.tableview.sort.SortState
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert
import org.junit.Test

class TableViewUtilTest {

    private val mockCells: List<SymbolCell> = listOf(
        mockSymbol("AAPL", InstrumentType.EQ),
        mockSymbol("AAPL--200501C00275000", InstrumentType.OPTNC),
        mockSymbol("AAAAX", InstrumentType.MF),
        mockSymbol("AAAAX", InstrumentType.MF),
        mockSymbol("AAAAX", InstrumentType.MF),
        mockSymbol("A", InstrumentType.EQ)
    ).mapIndexed { index, symbol ->
        SymbolCell(
            "id$index",
            "text",
            symbol.symbol,
            symbol.instrumentType,
            null
        )
    }

    private val rowHeaderRecyclerViewAdapter: RowHeaderRecyclerViewAdapter<Any> = mock {
        whenever(it.items).thenReturn(mockCells)
    }

    private val adapter: AbstractTableAdapter<Any, Any, Any> = mock {
        whenever(it.rowHeaderRecyclerViewAdapter).thenReturn(rowHeaderRecyclerViewAdapter)
    }

    private val tableView: ITableView = mock {
        whenever(it.adapter).thenReturn(adapter)
    }

    @Test
    fun `check visible symbols when there is no first visible position`() {
        checkVisibleSymbols(
            lastVisiblePosition = Int.MAX_VALUE,
            expected = emptySet()
        )
    }

    @Test
    fun `check visible symbols when there is no last visible position`() {
        checkVisibleSymbols(
            firstVisiblePosition = Int.MAX_VALUE,
            expected = emptySet()
        )
    }

    @Test
    fun `check visible symbols when there are no visible positions`() {
        checkVisibleSymbols(
            expected = emptySet()
        )
    }

    @Test
    fun `check visible symbols when arguments for all positions`() {
        checkVisibleSymbols(
            firstVisiblePosition = 0,
            lastVisiblePosition = mockCells.lastIndex,
            expected = setOf(
                Symbol("A", InstrumentType.EQ),
                Symbol("AAAAX", InstrumentType.MF),
                Symbol("AAPL", InstrumentType.EQ),
                Symbol("AAPL--200501C00275000", InstrumentType.OPTNC)
            )
        )
    }

    @Test
    fun `check visible symbols for portion of positions`() {
        checkVisibleSymbols(
            firstVisiblePosition = 1,
            lastVisiblePosition = 4,
            expected = setOf(
                Symbol("AAAAX", InstrumentType.MF),
                Symbol("AAPL--200501C00275000", InstrumentType.OPTNC)
            )
        )
    }

    @Test
    fun `check preference key format`() {
        val expectedPreference = "ASCENDING/LAST"
        val columnList = listOf("LAST", "CHANGE", "VOLUME")
        val sortStateInfo =
            SortStateInfo(SortState.ASCENDING, 0)
        val sortedColumnList = listOf("CHANGE", "LAST", "VOLUME")
        val expectedChangedSortStateInfo =
            SortStateInfo(SortState.ASCENDING, 1)

        Assert.assertEquals(expectedPreference, sortStateInfo.toPreferenceString(columnList))
        Assert.assertEquals(expectedChangedSortStateInfo, expectedPreference.toSortStateInfo(sortedColumnList))
    }

    private fun checkVisibleSymbols(
        firstVisiblePosition: Int = RecyclerView.NO_POSITION,
        lastVisiblePosition: Int = RecyclerView.NO_POSITION,
        expected: Set<WithSymbolInfo>
    ) = Assert.assertEquals(expected, tableView.getVisibleSymbols(firstVisiblePosition, lastVisiblePosition))

    private fun mockSymbol(symbol: String, instrumentType: InstrumentType): WithSymbolInfo {
        return object : WithSymbolInfo {
            override val symbol: String = symbol
            override val instrumentType: InstrumentType = instrumentType
        }
    }
}
