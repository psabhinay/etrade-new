package com.etrade.mobilepro.checkdeposit.presentation.activity

import androidx.recyclerview.widget.DiffUtil

class DepositActivityItemDiffCallback(
    private val oldItems: List<DepositActivityResultItem>,
    private val newItems: List<DepositActivityResultItem>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition]
        val newItem = newItems[newItemPosition]
        return oldItem.depositActivityItem.requestId == newItem.depositActivityItem.requestId
    }

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }
}
