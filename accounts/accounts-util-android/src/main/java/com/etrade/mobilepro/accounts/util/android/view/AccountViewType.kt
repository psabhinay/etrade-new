package com.etrade.mobilepro.accounts.util.android.view

/**
 * Enum Class for the accountViewType of Account used to display appropriate Account Views.
 */

enum class AccountViewType {
    UNAUTHENTICATED_VIEW,
    SINGLE_BROKERAGE_ACCOUNT_VIEW,
    MANAGED_ACCOUNT_VIEW,
    ESP_ACCOUNT_VIEW,
    BANK_ACCOUNT_VIEW,
    COMPLETE_MULTIPLE_ACCOUNT_VIEW,
    ALL_BROKERAGE_ACCOUNT_VIEW
}
