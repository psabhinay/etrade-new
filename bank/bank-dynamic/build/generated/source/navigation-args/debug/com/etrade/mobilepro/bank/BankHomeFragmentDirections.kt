package com.etrade.mobilepro.bank

import android.os.Bundle
import android.os.Parcelable
import androidx.navigation.NavDirections
import java.io.Serializable
import java.lang.UnsupportedOperationException
import kotlin.Boolean
import kotlin.Int
import kotlin.String
import kotlin.Suppress
import org.threeten.bp.LocalDate

public class BankHomeFragmentDirections private constructor() {
  private data class OpenDatePicker(
    public val startDate: LocalDate,
    public val endDate: LocalDate,
    public val isStart: Boolean
  ) : NavDirections {
    public override fun getActionId(): Int = R.id.openDatePicker

    @Suppress("CAST_NEVER_SUCCEEDS")
    public override fun getArguments(): Bundle {
      val result = Bundle()
      if (Parcelable::class.java.isAssignableFrom(LocalDate::class.java)) {
        result.putParcelable("startDate", this.startDate as Parcelable)
      } else if (Serializable::class.java.isAssignableFrom(LocalDate::class.java)) {
        result.putSerializable("startDate", this.startDate as Serializable)
      } else {
        throw UnsupportedOperationException(LocalDate::class.java.name +
            " must implement Parcelable or Serializable or must be an Enum.")
      }
      if (Parcelable::class.java.isAssignableFrom(LocalDate::class.java)) {
        result.putParcelable("endDate", this.endDate as Parcelable)
      } else if (Serializable::class.java.isAssignableFrom(LocalDate::class.java)) {
        result.putSerializable("endDate", this.endDate as Serializable)
      } else {
        throw UnsupportedOperationException(LocalDate::class.java.name +
            " must implement Parcelable or Serializable or must be an Enum.")
      }
      result.putBoolean("isStart", this.isStart)
      return result
    }
  }

  private data class ToCheckDeposit(
    public val bankAccountId: String
  ) : NavDirections {
    public override fun getActionId(): Int = R.id.toCheckDeposit

    public override fun getArguments(): Bundle {
      val result = Bundle()
      result.putString("bankAccountId", this.bankAccountId)
      return result
    }
  }

  public companion object {
    public fun openDatePicker(
      startDate: LocalDate,
      endDate: LocalDate,
      isStart: Boolean
    ): NavDirections = OpenDatePicker(startDate, endDate, isStart)

    public fun toCheckDeposit(bankAccountId: String): NavDirections = ToCheckDeposit(bankAccountId)
  }
}
