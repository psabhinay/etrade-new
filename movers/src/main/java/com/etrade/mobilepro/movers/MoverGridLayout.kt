package com.etrade.mobilepro.movers

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.core.view.children
import androidx.gridlayout.widget.GridLayout
import com.etrade.mobilepro.movers.databinding.MoversItemBinding
import com.etrade.mobilepro.util.android.accessibility.processAccessibilityStreamingEvent
import com.etrade.mobilepro.util.android.extension.layoutInflater

private const val MIN_ROW = 1
private const val MAX_ROW = 2
private const val MAX_COLUMN = 3
private const val DEFAULT_GRID_WEIGHT = 1.0f

@SuppressLint("ResourceType")
open class MoverGridLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
) : GridLayout(context, attrs, defStyleAttr) {

    private val isMoverViewsCreated: Boolean
        get() = childCount > 0

    private fun setGridLayoutRowColumn(gridLayout: GridLayout, size: Int) {
        if (size != 0) {
            gridLayout.columnCount = MAX_COLUMN
            if (size > MAX_COLUMN) {
                gridLayout.rowCount = MAX_ROW
            } else {
                gridLayout.rowCount = MIN_ROW
            }
        }
    }

    fun setData(moverItems: List<MoverItem>) {
        if (moverItems.isNotEmpty()) {
            if (isMoverViewsCreated) {
                updateMoverViews(moverItems)
            } else {
                initMoverViews(moverItems)
            }
        } else {
            removeAllViews()
        }
    }

    private fun initMoverViews(moverItems: List<MoverItem>) {
        setGridLayoutRowColumn(this, moverItems.size)
        var index = 0
        for (row in 0 until rowCount) {
            val rowSpec = spec(row, FILL, DEFAULT_GRID_WEIGHT)
            for (column in 0 until columnCount) {
                val columnSpec = spec(column, FILL, DEFAULT_GRID_WEIGHT)
                val gridLayoutParams = LayoutParams()
                gridLayoutParams.rowSpec = rowSpec
                gridLayoutParams.columnSpec = columnSpec
                gridLayoutParams.width = 0

                MoversItemBinding.inflate(layoutInflater, this, false).apply {
                    root.moverBinding = this
                    addView(root, gridLayoutParams)

                    moverItem = moverItems.getOrNull(index)
                }
                index++
            }
        }
    }

    private fun updateMoverViews(moverItems: List<MoverItem>) {
        children.forEachIndexed { index, view ->
            val moverItem = moverItems.getOrNull(index)
            moverItem?.let {
                view.processAccessibilityStreamingEvent(
                    "${it.displaySymbol} ${it.priceDescription} ${it.gainDescription}"
                )
            }
            view.moverBinding?.moverItem = moverItem
        }
    }

    private var View.moverBinding: MoversItemBinding?
        get() = getTag(R.id.movers_item_binding) as? MoversItemBinding
        set(value) = setTag(R.id.movers_item_binding, value)
}
