package com.etrade.mobilepro.newaccount

class AppsFlyerOLAEvent(val name: String, val relativePath: String)

internal val analyticsOLAEvents = listOf(
    // classic
    AppsFlyerOLAEvent("Brokerage OLA Start", "/oaa/rtao/accounts/individual"),
    AppsFlyerOLAEvent("Joint OLA Start", "/oaa/rtao/accounts/joint"),
    AppsFlyerOLAEvent("Custodial OLA Start", "/oaa/rtao/accounts/custodial"),
    AppsFlyerOLAEvent("Traditional IRA OLA Start", "/oaa/rtao/accounts/traditionalira"),
    AppsFlyerOLAEvent("Roth IRA OLA Start", "/oaa/rtao/accounts/rothira"),
    AppsFlyerOLAEvent("Rollover IRA OLA Start", "/oaa/rtao/accounts/rolloverira"),
    AppsFlyerOLAEvent("SEP IRA Start", "/oaa/rtao/accounts/sepira"),
    AppsFlyerOLAEvent("Core OLA Start", "/etx/wm/core-portfolios/rtq/1"),
    AppsFlyerOLAEvent("Ideal Brokerage OLA Start", "/etx/rtao/account-types/brokerage/individual"),
    AppsFlyerOLAEvent("Ideal Joint OLA Start", "/etx/rtao/account-types/brokerage/joint"),
    AppsFlyerOLAEvent("Ideal Custodial OLA Start", "/etx/rtao/account-types/brokerage/custodial"),
    AppsFlyerOLAEvent("Ideal Traditional IRA OLA Start", "/etx/rtao/account-types/retirement/traditional-ira"),
    AppsFlyerOLAEvent("Ideal Rollover IRA OLA Start", "/etx/rtao/account-types/retirement/rollover-ira"),
    AppsFlyerOLAEvent("Ideal Roth IRA OLA Start", "/etx/rtao/account-types/retirement/roth-ira"),
    AppsFlyerOLAEvent("Ideal SEP IRA Start", "/etx/rtao/account-types/retirement/sep-ira"),
    AppsFlyerOLAEvent("Premium Savings OLA Start", "/etx/rtao/bank/accounts/premium-savings-account?tb=5000"),
    AppsFlyerOLAEvent("Max-Rate Checking OLA Start", "/etx/rtao/bank/accounts/max-rate-checking?tb=5000"),
    AppsFlyerOLAEvent("E*TRADE Checking OLA Start", "/etx/rtao/bank/accounts/etrade-checking?tb=5000"),
    AppsFlyerOLAEvent("Ideal Brokerage OLA Start", "/etx/rtao/account-category"),
    AppsFlyerOLAEvent("Brokerage OLA Start", "/oaa/rtao/accounts"),

    // Multi-Account OLA events
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-category"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/brokerage"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/brokerage/individual"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/brokerage/joint"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/brokerage/custodial"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-category/retirement"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/retirement"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/retirement/traditional"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/retirement/roth"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/retirement/sep"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/retirement/rollover"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-category/bank"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank/individual"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank/joint"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank/individual/etrade-checking"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank/individual/max-rate-checking"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank/individual/premium-savings-account"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank/joint/etrade-checking"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank/joint/max-rate-checking"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank/joint/premium-savings-account"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank/etrade-checking"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank/max-rate-checking"),
    AppsFlyerOLAEvent("MA OLA Start", "/etx/rtao/ma/account-types/bank/premium-savings-account")
)
