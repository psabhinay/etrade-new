package com.etrade.mobilepro.tableviewutils.presentation.util

private const val SECOND_LINE_MIN_LENGTH = 2

internal fun replaceMiddleSpaceWithNewLineChar(text: String): String {
    if (text.isEmpty()) return text

    val middleIndex = text.length / 2
    val chars = text.toCharArray()
    val isAcceptableLineBreak: (Int) -> Boolean = { index ->
        // we add 1 to index as index zero-base value but we want to calculate length
        chars.size - (index + 1) >= SECOND_LINE_MIN_LENGTH &&
            chars[index].isWhitespace()
    }
    val replace: (Int) -> String = { index ->
        chars[index] = '\n'
        String(chars)
    }

    if (isAcceptableLineBreak(middleIndex)) {
        return replace(middleIndex)
    }

    for (step in 0..middleIndex) {
        val leftIndex = middleIndex - step
        if (isAcceptableLineBreak(leftIndex)) {
            return replace(leftIndex)
        }

        val rightIndex = middleIndex + step
        if (rightIndex < chars.size && isAcceptableLineBreak(rightIndex)) {
            return replace(rightIndex)
        }
    }

    return text
}
