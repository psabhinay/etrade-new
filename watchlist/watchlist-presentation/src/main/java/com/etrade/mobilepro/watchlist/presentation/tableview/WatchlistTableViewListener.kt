package com.etrade.mobilepro.watchlist.presentation.tableview

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.holder.SymbolRowHeaderViewHolder
import com.etrade.mobilepro.tableviewutils.presentation.sorting.SortableTableViewListener
import com.etrade.mobilepro.tableviewutils.presentation.sorting.TableDataSortable

class WatchlistTableViewListener(
    sortable: TableDataSortable,
    private val listener: ((SymbolCell) -> Unit)? = null
) : SortableTableViewListener(sortable) {

    override fun onRowHeaderClicked(rowHeaderView: RecyclerView.ViewHolder, row: Int) {
        listener?.let {
            val cell: SymbolCell? = (rowHeaderView as SymbolRowHeaderViewHolder).cell
            cell?.let { symbolCell ->
                it.invoke(symbolCell)
            }
        }
    }
}
