package com.etrade.mobilepro.util.android.extension

import android.widget.TextView
import androidx.annotation.StringRes
import com.etrade.mobilepro.util.formatAccessibilityDescription

fun TextView.updateTextAndDescription(text: String?, @StringRes contentDescriptionTemplateId: Int) {
    this.text = text
    this.contentDescription = context.getString(contentDescriptionTemplateId, text.formatAccessibilityDescription())
}
