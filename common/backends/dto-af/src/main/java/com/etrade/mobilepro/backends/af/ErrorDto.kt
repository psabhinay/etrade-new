package com.etrade.mobilepro.backends.af

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "Error", strict = false)
data class ErrorDto(
    @field:Element(name = "ErrorCode", required = false)
    var errorCode: Int = 0,
    @field:Element(name = "ErrorType", required = false)
    var errorTypeInt: Int = 0,
    @field:Element(name = "ErrorDescription", required = false)
    var errorDescription: String = ""
) {
    val errorType: ErrorType
        get() = ErrorType.fromCode(errorTypeInt)
}
