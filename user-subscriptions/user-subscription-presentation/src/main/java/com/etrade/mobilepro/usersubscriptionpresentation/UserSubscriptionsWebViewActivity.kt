package com.etrade.mobilepro.usersubscriptionpresentation

import android.content.Context
import android.content.Intent
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import org.chromium.customtabsclient.shared.WebviewActivity
import org.chromium.customtabsclient.shared.createWebViewActivityIntent

class UserSubscriptionsWebViewActivity : WebviewActivity() {

    override fun instantiateFragment() {
        fragmentInjectionFactory.instantiateFragment(UserSubscriptionsWebViewFragment::class.java, createFragmentArgs()).apply {
            supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container, this)
                .commit()
        }
    }

    companion object {
        private const val SUBSCRIPTION_URL = "/etx/hw/subscriptioncenter#/subscription"

        /**
         * Creates an intent to start [UserSubscriptionsWebViewActivity].
         *
         * @param context application context
         * @param baseUrl the base baseUrl
         *
         * @return the intent to start the activity
         */
        fun intent(context: Context, baseUrl: String): Intent {
            return context.createWebViewActivityIntent(
                activityClass = UserSubscriptionsWebViewActivity::class.java,
                url = "$baseUrl$SUBSCRIPTION_URL",
                titleResId = R.string.manage_subscriptions,
                upIconResId = R.drawable.ic_close
            )
        }
    }
}
