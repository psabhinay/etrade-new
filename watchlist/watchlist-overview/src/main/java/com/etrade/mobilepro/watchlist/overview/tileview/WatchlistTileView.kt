package com.etrade.mobilepro.watchlist.overview.tileview

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.compose.components.ETLazyVerticalGrid
import com.etrade.mobilepro.movers.MoverItem
import com.etrade.mobilepro.movers.compose.MoverCell

private const val COLUMN_COUNT = 3

@ExperimentalFoundationApi
@Composable
fun WatchlistTileView(
    entryList: List<MoverItem> = emptyList(),
    onItemTap: ((MoverItem, Int) -> Unit) = { _, _ -> },
    footer: (LazyListScope.() -> Unit) = {},
) {
    ETLazyVerticalGrid(
        cells = GridCells.Fixed(COLUMN_COUNT),
        contentPadding = PaddingValues(Dimens.Size16dp),
        footer = footer
    ) {
        entryList.forEachIndexed { index, moverItem ->
            item {
                MoverCell(moverItem, modifier = Modifier.clickable { onItemTap.invoke(moverItem, index) })
            }
        }
    }
}
