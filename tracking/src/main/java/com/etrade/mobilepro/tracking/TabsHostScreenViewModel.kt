package com.etrade.mobilepro.tracking

interface TabsHostScreenViewModel {

    val tabNames: Array<String>

    val viewPagerTabsTrackerHelper: ViewPagerTabsTrackerHelper

    fun trackTab(position: Int) {
        viewPagerTabsTrackerHelper.trackTab(position, tabNames[position])
    }
}
