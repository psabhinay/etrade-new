package com.etrade.mobilepro.dynamic.form.api.util

import android.content.res.Resources
import com.etrade.mobilepro.dynamic.form.api.DropDownOption

fun Int.toDropDownOption(resources: Resources): DropDownOption {
    return DropDownOption(
        id = toString(),
        name = resources.getString(this)
    )
}
