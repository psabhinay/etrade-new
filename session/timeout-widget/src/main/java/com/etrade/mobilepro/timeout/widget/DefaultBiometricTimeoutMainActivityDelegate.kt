package com.etrade.mobilepro.timeout.widget

import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticationDelegate
import com.etrade.mobilepro.login.presentation.BiometricAuthenticationHelperHolder
import com.etrade.mobilepro.login.widget.LoginResultDialogFragment
import com.etrade.mobilepro.timeout.state.TimeoutState
import com.etrade.mobilepro.util.reference
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.ref.WeakReference
import javax.inject.Inject

class DefaultBiometricTimeoutMainActivityDelegate @Inject constructor(
    private val timeoutState: TimeoutState,
    private val biometricDelegate: BiometricAuthenticationDelegate,
    private val viewModelFactory: ViewModelProvider.Factory
) : BiometricTimeoutMainActivityDelegate, LifecycleObserver {

    private val logger: Logger = LoggerFactory.getLogger(DefaultBiometricTimeoutMainActivityDelegate::class.java)

    private val isLoginResultDialogShown: Boolean
        get() {
            return activityRef.get()?.let {
                LoginResultDialogFragment.restore(it.supportFragmentManager) != null
            } ?: false
        }

    private lateinit var activityRef: WeakReference<AppCompatActivity>

    private val viewModel: BiometricTimeoutMainActivityDelegateViewModel
        get() = activityRef.get()!!.viewModels<BiometricTimeoutMainActivityDelegateViewModel> { viewModelFactory }.value

    private val sharedSessionTerminatedViewModel: SharedSessionTerminatedViewModel
        get() = activityRef.get()!!.viewModels<SharedSessionTerminatedViewModel> { viewModelFactory }.value

    private val callbacks: () -> Unit = {
        timeoutState.shouldShowSarLogoutDialog = false
        timeoutState.isShowingSessionDialog = false
        authenticate()
    }

    override fun init(activity: AppCompatActivity) {
        activityRef = WeakReference(activity)
        activity.lifecycle.addObserver(this)
        biometricDelegate.init(activity)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        activityRef.reference {
            SessionDialogFragment.restore(it.supportFragmentManager, callbacks)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        logger.debug("shouldShowSarLogoutDialog: ${timeoutState.shouldShowSarLogoutDialog}")
        if (timeoutState.shouldShowSarLogoutDialog) {
            showSessionTerminateDialog()
            timeoutState.shouldShowSarLogoutDialog = false
        }
        if (biometricDelegate.isAuthenticationNeeded) {
            authenticate()
        }
    }

    override fun showSessionTerminateDialog() {
        require(activityRef.get() != null)
        sharedSessionTerminatedViewModel.showSessionTerminatedPage(callbacks)
        timeoutState.isShowingSessionDialog = true
    }

    override fun authenticate() {
        if (!isLoginResultDialogShown &&
            viewModel.isBiometricLoginAvailable &&
            viewModel.shouldShowBiometricAuthentication &&
            !biometricDelegate.isBiometricPromptCancelled
        ) {
            activityRef.reference {
                if (it is BiometricAuthenticationHelperHolder) {
                    it.biometricAuthenticationHelper.invokeBiometricAuthenticationCallback()
                }
            }
        }
    }
}
