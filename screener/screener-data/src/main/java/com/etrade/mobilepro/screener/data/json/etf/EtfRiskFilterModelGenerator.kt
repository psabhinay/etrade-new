package com.etrade.mobilepro.screener.data.json.etf

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfRiskFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val RISK_VALUES_FIELD = "StandardDev"

class EtfRiskFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val etfRiskFilter = screenerFilter as EtfRiskFilter
        val arguments = mutableListOf<Argument>()

        if (etfRiskFilter.isNotEmpty()) {
            arguments.add(
                Argument(getRiskValuesClauses(etfRiskFilter), RISK_VALUES_FIELD)
            )
        }

        return PartialScreenerParams(arguments)
    }

    private fun getRiskValuesClauses(etfRiskFilter: EtfRiskFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (etfRiskFilter.bottom20) {
            clauses.addAll(bottom20Clauses)
        }
        if (etfRiskFilter.belowMid20) {
            clauses.addAll(belowMid20Clauses)
        }
        if (etfRiskFilter.mid20) {
            clauses.addAll(mid20Clauses)
        }
        if (etfRiskFilter.aboveMid20) {
            clauses.addAll(aboveMid20Clauses)
        }
        if (etfRiskFilter.top20) {
            clauses.addAll(top20Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private val bottom20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("21"))
    )
    private val belowMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("21")),
        Clause(OPERATOR_LESS_THAN, listOf("41"))
    )
    private val mid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("41")),
        Clause(OPERATOR_LESS_THAN, listOf("61"))
    )
    private val aboveMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("61")),
        Clause(OPERATOR_LESS_THAN, listOf("81"))
    )
    private val top20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("81")),
        Clause(OPERATOR_LESS_THAN, listOf("101"))
    )
}
