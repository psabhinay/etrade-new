package com.etrade.mobilepro.feature.presentation.viewmodel

import com.etrade.mobilepro.feature.Feature
import kotlinx.coroutines.flow.Flow

class FeatureFlagItemViewModel(
    private val enabledFactory: () -> Flow<Boolean>,
    private val providerStatusFactory: () -> Flow<String>,
    val feature: Feature,
    val isSettable: Boolean,
    val remoteConfig: Boolean,
    val setEnabled: suspend (Boolean) -> Unit
) : Feature by feature {
    val isEnabled: Flow<Boolean> by lazy { enabledFactory() }
    val providerStatus: Flow<String> by lazy { providerStatusFactory() }
}
