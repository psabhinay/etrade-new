package com.etrade.mobilepro.marketscalendar.api.repo

import com.etrade.mobilepro.marketscalendar.api.model.EventsEarnings
import com.etrade.mobilepro.marketscalendar.api.model.EventsEconomic
import com.etrade.mobilepro.marketscalendar.api.model.EventsRequest
import com.etrade.mobilepro.marketscalendar.api.model.EventsResponse
import com.etrade.mobilepro.marketscalendar.api.model.EventsSummaryResponse
import io.reactivex.Single

interface MarketsCalendarRepo {

    fun getEventsEarningsRx(request: EventsRequest): Single<EventsResponse<EventsEarnings>>

    fun getEventsEconomic(request: EventsRequest): Single<EventsResponse<EventsEconomic>>

    fun getEventsSummary(request: EventsRequest): Single<EventsSummaryResponse>

    suspend fun getEventsEarnings(request: EventsRequest): EventsResponse<EventsEarnings>
}
