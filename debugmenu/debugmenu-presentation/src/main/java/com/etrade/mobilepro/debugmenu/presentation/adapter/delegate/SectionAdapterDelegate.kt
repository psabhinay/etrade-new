package com.etrade.mobilepro.debugmenu.presentation.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.debugmenu.presentation.adapter.viewholder.SectionViewHolder
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuItemSectionBinding
import com.etrade.mobilepro.debugmenu.presentation.item.DebugMenuItem
import com.etrade.mobilepro.debugmenu.presentation.item.SectionItem
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class SectionAdapterDelegate : AbsListItemAdapterDelegate<SectionItem, DebugMenuItem, SectionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): SectionViewHolder {
        return SectionViewHolder(parent.viewBinding(DebugmenuItemSectionBinding::inflate))
    }

    override fun isForViewType(item: DebugMenuItem, items: MutableList<DebugMenuItem>, position: Int): Boolean =
        item is SectionItem

    override fun onBindViewHolder(item: SectionItem, holder: SectionViewHolder, payloads: MutableList<Any>) =
        holder.bindTo(item)
}
