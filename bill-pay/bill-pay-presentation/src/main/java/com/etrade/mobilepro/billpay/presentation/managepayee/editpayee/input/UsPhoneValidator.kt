package com.etrade.mobilepro.billpay.presentation.managepayee.editpayee.input

import android.content.Context
import java.util.Locale

class UsPhoneValidator(
    invalidMessage: String,
    context: Context
) : PhoneValidator(
    Locale.US,
    invalidMessage,
    context
)
