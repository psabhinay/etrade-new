package com.etrade.mobilepro.markets.futures

import android.view.View
import androidx.core.widget.ContentLoadingProgressBar
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.R
import com.etrade.mobilepro.databinding.FragmentMarketsFuturesBinding
import com.etrade.mobilepro.markets.common.MarketsBaseFragment
import com.etrade.mobilepro.markets.common.MarketsBaseViewModel
import com.etrade.mobilepro.markets.common.MarketsTableViewListener
import com.etrade.mobilepro.swiperefresh.MultiSwipeRefreshLayout
import com.etrade.mobilepro.tableviewutils.presentation.RestrictedTableView
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

internal class MarketsFuturesFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : MarketsBaseFragment<MarketsFuturesViewModel>(viewModelFactory, snackbarUtilFactory, R.layout.fragment_markets_futures) {

    private val binding by viewBinding(FragmentMarketsFuturesBinding::bind) { onDestroyBinding() }

    override val marketsRowHeaderWidth = R.dimen.markets_default_row_header_width

    override val table: RestrictedTableView
        get() = binding.tableScroll.tableView
    override val loading: ContentLoadingProgressBar
        get() = binding.loading.loadingIndicator
    override val emptyQuotesText: View
        get() = binding.emptyQuotes.emptyQuotesText
    override val swipeRefresh: MultiSwipeRefreshLayout
        get() = binding.swipeRefresh

    override val viewModelClassType = MarketsFuturesViewModel::class.java

    override fun handleSuccess(success: MarketsBaseViewModel.ViewState.Success) {
        super.handleSuccess(success)
        val timeStampText = getTimeStampText(R.string.futures_contract, success.timeStamp)
        binding.tableScroll.footerTimeStamp.text = timeStampText
        table.tableViewListener = MarketsTableViewListener()
    }
}
