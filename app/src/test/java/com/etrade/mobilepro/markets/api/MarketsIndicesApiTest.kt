package com.etrade.mobilepro.markets.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.BaseApiTest
import com.etrade.mobilepro.markets.data.response.MarketIndexResponse
import com.etrade.mobilepro.testutil.XmlDeserializer
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

internal class MarketsIndicesApiTest : BaseApiTest() {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun `markets indices api success call test`() {
        val marketIndexResponse: MarketIndexResponse = XmlDeserializer.getObjectFromXml("markets_indices.xml", MarketIndexResponse::class.java)

        assertEquals(10, marketIndexResponse.quotes?.size)
        assertEquals("DOW", marketIndexResponse.quotes?.get(0)?.symbolDesc)
    }
}
