package com.etrade.mobilepro.onboarding

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.Point
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import kotlin.math.roundToInt

private const val SEVENTY_PERCENT = 0.7
private const val NINETY_PERCENT = 0.9

class OnboardingBackgroundView : View {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val paint: Paint by lazy {
        Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = ContextCompat.getColor(context, R.color.purple)
            style = Paint.Style.FILL_AND_STROKE
            isAntiAlias = true
        }
    }
    private val triangleWidth: Int by lazy { (width * SEVENTY_PERCENT).roundToInt() }
    private val triangleHeight: Int by lazy { (height * NINETY_PERCENT).roundToInt() }

    override fun draw(canvas: Canvas?) {
        super.draw(canvas)

        canvas?.let { drawTriangle(triangleWidth, triangleHeight, paint, it) }
    }

    private fun drawTriangle(width: Int, height: Int, paint: Paint, canvas: Canvas) {
        val p1 = Point(0, 0)
        val p2 = Point(0, height)
        val p3 = Point(width, 0)
        val path = Path()

        path.fillType = Path.FillType.EVEN_ODD
        path.moveTo(p1.x.toFloat(), p1.y.toFloat())
        path.lineTo(p2.x.toFloat(), p2.y.toFloat())
        path.lineTo(p3.x.toFloat(), p3.y.toFloat())
        path.close()

        canvas.drawPath(path, paint)
    }
}
