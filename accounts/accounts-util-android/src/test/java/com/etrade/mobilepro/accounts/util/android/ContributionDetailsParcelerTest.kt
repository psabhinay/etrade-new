package com.etrade.mobilepro.accounts.util.android

import android.os.Parcel
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobile.accounts.details.isEquals
import org.junit.Test
import org.junit.runner.RunWith
import java.math.BigDecimal

@RunWith(AndroidJUnit4::class)
class ContributionDetailsParcelerTest {

    @Test
    fun `data A can be serialized and deserialized`() {
        testData(dataA)
    }

    @Test
    fun `data B can be serialized and deserialized`() {
        testData(dataB)
    }

    private fun testData(data: AccountDetails.ContributionDetails) {
        val parcel = Parcel.obtain()
        ContributionDetailsParceler.run { data.write(parcel, 0) }
        parcel.setDataPosition(0)
        val result = ContributionDetailsParceler.create(parcel)
        assert(data.isEquals(result))
    }

    private val dataA = object : AccountDetails.ContributionDetails {
        override val contributionAmount: BigDecimal? = BigDecimal(133)
        override val taxYear: Int = 99
    }

    private val dataB = object : AccountDetails.ContributionDetails {
        override val contributionAmount: BigDecimal? = null
        override val taxYear: Int = 133
    }
}
