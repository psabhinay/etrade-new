package com.etrade.mobilepro.login.widget

enum class LoginErrorCtaAction {
    CALL_ETRADE,
    DISMISS_ERROR_DIALOG,
    DISMISS_WARNING_DIALOG,
    RECOVER_USERNAME,
    RESET_PASSWORD
}
