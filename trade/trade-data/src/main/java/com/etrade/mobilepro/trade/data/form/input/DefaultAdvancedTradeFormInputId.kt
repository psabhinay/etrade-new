package com.etrade.mobilepro.trade.data.form.input

import com.etrade.mobilepro.trade.data.form.value.DefaultAdvancedOrderTypeValueId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.AdvancedOrderTypeValueId

object DefaultAdvancedTradeFormInputId : WithAdvancedTradeFormInputId {
    override val advancedOrder: String = "advancedOrder"
    override val advancedOrderType: String = "advancedOrderType"
    override val advancedOrderTypeValueId: AdvancedOrderTypeValueId = DefaultAdvancedOrderTypeValueId
}
