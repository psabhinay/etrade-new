package com.etrade.mobilepro.portfolio.model

import android.os.Parcelable
import androidx.lifecycle.MutableLiveData
import com.etrade.completeview.dto.AccountSummaryDto
import com.etrade.mobilepro.R
import com.etrade.mobilepro.backends.mgs.MOVEMENT_TYPE_NEUTRAL
import com.etrade.mobilepro.dummy.createAccountBalanceStreamId
import com.etrade.mobilepro.dummy.createLightStreamerStreamId
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.portfolio.model.dto.CallToActionParcelable
import com.etrade.mobilepro.portfolio.model.dto.ParcelablePortfolioSummaryItemDto
import com.etrade.mobilepro.portfolio.model.dto.toPortfolioSummarySections
import com.etrade.mobilepro.util.android.SingleLiveEvent
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
class PortfolioExtendedSummaryModel(
    val title: String?,
    val subTitle: String?,
    val streamableAccountId: String,
    val isStreamingAllowed: Boolean,
    override var extraSummarySections: List<ParcelablePortfolioSummaryItemDto>,
    override val callToActions: List<CallToActionParcelable>
) : Parcelable, ExtendedPortfolioSummary {

    @IgnoredOnParcel
    override val accountTitle: String = title ?: ""

    @IgnoredOnParcel
    override val accountNumber: String = subTitle?.let { "Account Number: $it" } ?: ""

    @IgnoredOnParcel
    val clickEvents: MutableLiveData<CtaEvent> by lazy { SingleLiveEvent<CtaEvent>() }

    override fun getIconResource(index: Int): Int {
        if (index >= callToActions.size) return 0
        val label = callToActions[index].label
        return when {
            label.contains("performance", true) -> R.drawable.ic_tableview_chart
            label.contains("news", true) -> R.drawable.ic_news
            label.contains("drip", true) -> R.drawable.ic_tableview_drip
            else -> 0
        }
    }

    override fun onCtaClicked(index: Int) {
        val uri = callToActions[index].action.appUrl ?: return
        clickEvents.value = CtaEvent.UriEvent(uri)
    }

    fun updateSummarySection(summaryDto: AccountSummaryDto) {
        extraSummarySections = summaryDto.createParcelableSectionDto(streamableAccountId)
    }
}

internal fun AccountSummaryDto.createParcelableSectionDto(streamableAccountId: String): List<ParcelablePortfolioSummaryItemDto> {
    val mutableList = mutableListOf<ParcelablePortfolioSummaryItemDto>()
    val streamId = createAccountBalanceStreamId(streamableAccountId)

    mutableList.add(ParcelablePortfolioSummaryItemDto(detailLabel, detailValue, streamId, MOVEMENT_TYPE_NEUTRAL))
    additionalSectionDtos.forEach {
        mutableList.add(
            ParcelablePortfolioSummaryItemDto(
                title = it.title,
                initialValue = it.value.initial,
                streamId = createLightStreamerStreamId(it.value.streamId, streamableAccountId),
                movementType = it.value.movementType
            )
        )
    }
    mutableList.addAll(extraDetailsDtos.toPortfolioSummarySections())
    return mutableList.toList()
}
