package com.etrade.mobilepro.chartengine

import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable

/**
 * The implementations of this interface is to fetch SymbolChartData from network or cache with a ChartRequest
 */

interface ChartRepo {

    fun getChart(chartRequest: ChartRequest): Observable<Resource<SymbolChartData>>

    fun getAggregatedCharts(chartRequests: List<ChartRequest>): Observable<Resource<List<SymbolChartData>>>
}
