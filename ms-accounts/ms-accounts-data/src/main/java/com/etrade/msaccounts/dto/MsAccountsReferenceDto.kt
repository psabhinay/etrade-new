package com.etrade.msaccounts.dto

import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.msaccounts.dao.MsAccount
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MsAccountsReferenceDto(
    override val data: List<MsAccountDto>
) : BaseReference<List<MsAccount>>() {

    @Suppress("LongMethod") // just mappings
    override fun convertToModel() = data.map {
        MsAccount(
            accountUuid = it.accountUuid,
            accountDisplayName = it.accountDisplayName,
            accountDisplayNumber = it.accountDisplayNumber,
            balance = it.balance,
            balanceType = it.balanceType,
            instType = it.instType,
            keyAccountId = it.keyAccountId,
            platform = it.platform
        )
    }
}
