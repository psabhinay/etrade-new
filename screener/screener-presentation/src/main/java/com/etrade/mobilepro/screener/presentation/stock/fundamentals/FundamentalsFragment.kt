package com.etrade.mobilepro.screener.presentation.stock.fundamentals

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.stock.FundamentalsFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentFundamentalsPeRatioBinding
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentFundamentalsPriceBookRatioBinding
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class FundamentalsFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<FundamentalsFilter, FundamentalsViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {

    private val peRatio by viewBinding(PartialFragmentFundamentalsPeRatioBinding::bind)
    private val priceBookRatio by viewBinding(PartialFragmentFundamentalsPriceBookRatioBinding::bind)
    override val layoutRes: Int = R.layout.fragment_fundamentals
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.STOCK
    override val viewModel by viewModels<FundamentalsViewModel> { viewModelFactory }
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<FundamentalsFilter, FundamentalsViewModel.ViewState, Boolean>>
        get() = mapOf(
            peRatio.peZeroFifteenX to PeRatio.ZERO_15X,
            peRatio.peFifteenTwentyFiveX to PeRatio.FIFTEEN_25X,
            peRatio.peTwentyFiveFiftyX to PeRatio.TWENTY_FIVE_50X,
            peRatio.peFiftyOneHundredX to PeRatio.FIFTY_100X,
            peRatio.peBelowAvg to PeRatio.BELOW_AVG,
            peRatio.peAboveAvg to PeRatio.ABOVE_AVG,
            peRatio.trailingTwelveMonths to PeRatioPeriod.TRAILING_12_MONTHS,
            peRatio.currentFiscalYear to PeRatioPeriod.CURRENT_FISCAL_YEAR,
            peRatio.nextFiscalYear to PeRatioPeriod.NEXT_FISCAL_YEAR,
            priceBookRatio.zeroOneX to PriceBookRatio.ZERO_1X,
            priceBookRatio.oneTwoX to PriceBookRatio.ONE_2X,
            priceBookRatio.twoThreeX to PriceBookRatio.TWO_3X,
            priceBookRatio.moreThanThreeX to PriceBookRatio.GREATER_THAN_3X
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                updatePeRatio(viewState = it)
                updatePriceBookRatio(viewState = it)
            }
        )
    }

    private fun updatePeRatio(viewState: FundamentalsViewModel.ViewState) {
        viewState.filter.apply {
            peRatio.apply {
                peZeroFifteenX.isChecked = peRatio0to15x
                peFifteenTwentyFiveX.isChecked = peRatio15to25x
                peTwentyFiveFiftyX.isChecked = peRatio25to50x
                peFiftyOneHundredX.isChecked = peRatio50to100x
                peBelowAvg.isChecked = peRatioBelowIndustryAvg
                peAboveAvg.isChecked = peRatioAboveIndustryAvg
                trailingTwelveMonths.isChecked = peRatioOverTrailing12Months
                currentFiscalYear.isChecked = peRatioOverCurrentFiscalYear
                nextFiscalYear.isChecked = peRatioOverNextFiscalYear
            }
        }
    }

    private fun updatePriceBookRatio(viewState: FundamentalsViewModel.ViewState) {
        viewState.filter.apply {
            priceBookRatio.apply {
                zeroOneX.isChecked = priceBookRatio0to1x
                oneTwoX.isChecked = priceBookRatio1to2x
                twoThreeX.isChecked = priceBookRatio2to3x
                moreThanThreeX.isChecked = priceBookRatioGreaterThan3x
            }
        }
    }
}
