package com.etrade.mobilepro.streaming

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.etrade.eo.streaming.LSClient
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate.StreamingSubscriptionUpdate
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdateListener
import com.etrade.mobilepro.util.safeLet
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.lang.ref.WeakReference
import java.util.concurrent.atomic.AtomicBoolean

interface StreamingSessionDelegate {

    fun init(activity: AppCompatActivity)

    fun startStreamingSession()

    fun stopStreamingSession()
}

class StreamingSessionDelegateImpl(
    private val user: User,
    private val lsClient: LSClient,
    private val streamerAdapterSet: String,
    private val streamerUrlMediator: () -> String,
    private val streamingStatusController: StreamingStatusController
) : StreamingSessionDelegate, LifecycleObserver {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)
    private lateinit var activityRef: WeakReference<AppCompatActivity>
    private val isStreamingSessionStarted = AtomicBoolean(false)

    private val streamingStatusUpdateListener: StreamingStatusUpdateListener = { status ->
        if (status is StreamingSubscriptionUpdate) {
            stopStreamingSession()
            if (status.isRealTime) {
                startStreamingSession()
            }
        }
    }

    override fun init(activity: AppCompatActivity) {
        activityRef = WeakReference(activity)
        activity.lifecycle.addObserver(this)
    }

    override fun startStreamingSession() {
        safeLet(user.principleId, user.token) { principleId, token ->
            if (user.isRealtimeUser && !isStreamingSessionStarted.getAndSet(true)) {
                logger.debug("startStreamingSession")
                lsClient.start(
                    serverUrl = streamerUrlMediator.invoke(),
                    adapterSet = streamerAdapterSet,
                    userName = principleId,
                    passwordToken = token
                )
            }
        }
    }

    override fun stopStreamingSession() {
        if (isStreamingSessionStarted.getAndSet(false)) {
            logger.debug("stopStreamingSession")
            lsClient.stop()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        logger.debug("Got ON_START lifecycle event so will try to subscribeForStreamingStatusUpdates")
        streamingStatusController.subscribeForStreamingStatusUpdates(streamingStatusUpdateListener)
        if (user.isRealtimeUser && !isStreamingSessionStarted.get()) {
            startStreamingSession()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        logger.debug("Got ON_STOP lifecycle event so will try to unsubscribeFromStreamingStatusUpdates")
        streamingStatusController.unsubscribeFromStreamingStatusUpdates(streamingStatusUpdateListener)
    }
}
