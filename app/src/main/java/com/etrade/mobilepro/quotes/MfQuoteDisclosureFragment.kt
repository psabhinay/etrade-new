package com.etrade.mobilepro.quotes

import com.etrade.mobilepro.R
import com.etrade.mobilepro.overlay.BottomSheetSimpleTextFragment

class MfQuoteDisclosureFragment : BottomSheetSimpleTextFragment() {
    override val text: CharSequence
        get() = getText(R.string.mf_quote_detail_disclosure_body)
}
