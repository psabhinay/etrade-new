package com.etrade.mobilepro.util.android

import android.content.Intent
import com.google.firebase.appindexing.Action
import com.google.firebase.appindexing.FirebaseUserActions
import com.google.firebase.appindexing.builders.AssistActionBuilder

const val ACTION_TOKEN_EXTRA = "actions.fulfillment.extra.ACTION_TOKEN"

fun logAppAction(intent: Intent?, desiredPath: String) {
    intent?.let {
        val path = it.data?.path
        val token = it.getStringExtra(ACTION_TOKEN_EXTRA)
        if (token != null) {
            logAppAction(token, path == desiredPath)
        }
        it.removeExtra(ACTION_TOKEN_EXTRA)
    }
}

fun logAppAction(actionToken: String, isSuccess: Boolean) {
    val action = AssistActionBuilder()
        .setActionToken(actionToken)
        .setActionStatus(if (isSuccess) Action.Builder.STATUS_TYPE_COMPLETED else Action.Builder.STATUS_TYPE_FAILED)
        .build()
    FirebaseUserActions.getInstance().end(action)
}
