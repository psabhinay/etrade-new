package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.dagger.factory.ViewModelInjectionTracker
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen
import javax.inject.Inject

class DefaultViewModelInjectionTracker @Inject constructor(
    private val tracker: Tracker
) : ViewModelInjectionTracker {
    override fun trackScreenIfApplicable(viewModel: Any) {
        viewModel.let { it as? ScreenViewModel }?.screenName?.let {
            tracker.screen(it)
        }
    }
}
