package com.etrade.mobilepro.dynamic.form.ui

import android.content.Context
import android.util.AttributeSet
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.map
import com.etrade.mobilepro.dropdown.DropDownItemConverter
import com.etrade.mobilepro.dropdown.DropDownManager
import com.etrade.mobilepro.dropdown.SimpleDropDownItem
import com.etrade.mobilepro.dynamic.form.api.ClickInterceptor
import com.etrade.mobilepro.dynamic.form.api.DropDownOption
import com.etrade.mobilepro.dynamic.form.api.DropDownOptionList
import com.etrade.mobilepro.dynamic.form.api.SelectInput
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.livedata.filter
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.valuepicker.ValuePickerWidget

abstract class AbstractSelectInputView<in T : SelectInput> : ValuePickerWidget, ComponentView<T>, WithInputId {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val inputViewDelegate: InputViewDelegate = InputViewDelegate(this, ::setTitle)

    private val selectedOptionObserver: Observer<CharSequence?> = Observer { value = it }

    private val optionListObserver: Observer<DropDownOptionList> = Observer { list ->
        dropDownOptions = list.dropDownOptions
        selectedOptionIndex = list.selectedOptionIndex
    }

    private val clickRequestObserver = Observer<ConsumableLiveEvent<Unit>> {
        it.consume { showDropDown() }
    }

    private val interceptableClickObserver = Observer<Boolean> { shouldInterceptClick ->
        if (shouldInterceptClick) {
            input?.notifyClickIntercepted()
        } else {
            showDropDown()
        }
    }

    private var input: T? = null
    override val inputId: String?
        get() { return input?.id }
    protected var dropDownOptions: List<DropDownOption> = emptyList()
    protected var selectedOptionIndex: Int = -1
    protected var bottomSheetSelectorTitle = ""
    private val clickSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()
    private var interceptableClickSignal: LiveData<Boolean>? = null

    init {
        setOnClickListener { clickSignal.value = Unit.consumable() }
    }

    override fun render(component: T, owner: LifecycleOwner) {
        inputViewDelegate.render(component, owner)

        input?.apply { ->
            displayValue.removeObserver(selectedOptionObserver)
            dropDown.optionList.removeObserver(optionListObserver)
            clickRequestSignal.removeObserver(clickRequestObserver)
            interceptableClickSignal?.removeObserver(interceptableClickObserver)
        }

        input = component

        component.apply {
            displayValue.observe(owner, selectedOptionObserver)
            dropDown.optionList.observe(owner, optionListObserver)
            clickRequestSignal.observe(owner, clickRequestObserver)
            interceptableClickSignal = getInterceptableClickSignal()
                .apply { observe(owner, interceptableClickObserver) }
        }
    }

    protected abstract fun createDropDownManager(itemConverter: DropDownItemConverter<DropDownOption>, input: T): DropDownManager<DropDownOption>

    private fun setTitle(title: CharSequence?) {
        hint = title
        bottomSheetSelectorTitle = title?.toString() ?: ""
    }

    private fun showDropDown() {
        input?.let { input ->
            val itemConverter: DropDownItemConverter<DropDownOption> = {
                SimpleDropDownItem(
                    title = it.name,
                    description = it.description,
                    isSingleLineDescription = false,
                    contentDescription = it.contentDescription
                )
            }
            createDropDownManager(itemConverter, input).apply {
                setListener { _, item -> input.setValue(item) }
                openDropDown()
            }
        }
    }

    private fun ClickInterceptor.getInterceptableClickSignal(): LiveData<Boolean> {
        return clickSignal.combineLatest(shouldInterceptClick)
            .filter { (click, _) -> !click.isConsumed }
            .map { (click, shouldInterceptClick) ->
                click.consume()
                shouldInterceptClick
            }
    }
}
