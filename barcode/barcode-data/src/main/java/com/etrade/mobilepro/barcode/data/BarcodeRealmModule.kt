package com.etrade.mobilepro.barcode.data

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class BarcodeRealmModule
