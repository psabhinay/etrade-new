package com.etrade.mobilepro.markets.data.response

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(strict = false, name = "Output")
data class Quote constructor(
    @field:Element(required = false, name = "Symbol")
    var symbol: String? = null,
    @field:Element(required = false, name = "ProductType")
    var productType: String? = null,
    @field:Element(required = false, name = "SymbolDesc")
    var symbolDesc: String? = null,
    @field:Element(name = "Price")
    var price: String? = null,
    @field:Element(name = "TimeStamp")
    var timestamp: String? = null,
    @field:Element(name = "Change")
    var change: String? = null,
    @field:Element(name = "PercentChange")
    var percentChange: String? = null,
    @field:Element(name = "Volume")
    var volume: String? = null
)
