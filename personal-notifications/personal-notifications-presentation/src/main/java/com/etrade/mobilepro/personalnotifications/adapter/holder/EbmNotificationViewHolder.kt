package com.etrade.mobilepro.personalnotifications.adapter.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.inboxmessages.databinding.InboxMessageDashboardInformationViewBinding
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification
import com.etrade.mobilepro.personalnotifications.api.PersonalNotification.Localytics

class EbmNotificationViewHolder(
    private val binding: InboxMessageDashboardInformationViewBinding,
    private val clickListener: ((PersonalNotification) -> Unit),
    private val itemDismissListener: ((Localytics) -> Unit)
) : RecyclerView.ViewHolder(binding.root) {
    private lateinit var item: Localytics
    private val onClick: ((View) -> Unit) = { clickListener.invoke(item) }

    fun bind(item: Localytics.EmergencyBroadcast) {
        this.item = item
        binding.title.apply {
            text = item.message.title
            setOnClickListener(onClick)
        }
        binding.subtitle.apply {
            text = item.message.subtitle
            setOnClickListener(onClick)
        }
        binding.logo.setOnClickListener(onClick)
        binding.close.setOnClickListener { itemDismissListener.invoke(item) }
    }
}
