package com.etrade.mobilepro.dynamicui.api

/**
 * Request a screen from a server.
 */
interface ScreenRequest {

    /**
     * Url to fetch a screen.
     */
    val url: String

    /**
     * Content key to cache and retrieve data.
     */
    val contentKey: String

    /**
     * Creates request body if a screen service needs it.
     *
     * @return request body or `null` if it is not needed
     */
    fun createRequestBody(): Any?
}
