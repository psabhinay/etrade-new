package com.etrade.mobilepro.uiwidgets.tablesectionsview

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

open class SectionLayout constructor(
    context: Context,
    attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {
    init {
        orientation = VERTICAL
    }

    fun setContent(data: List<TableSectionItemLayout>) {
        // clear previous section views if any present in the view group especially when user refreshes.
        removeAllViews()

        val linearParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)

        data.forEach {
            addView(it.getRowView(linearParams, this as ViewGroup))
        }
    }
}

@BindingAdapter("section")
fun setTableSection(view: SectionLayout, data: List<TableSectionItemLayout>?) {
    data?.let {
        view.setContent(data)
    }
}
