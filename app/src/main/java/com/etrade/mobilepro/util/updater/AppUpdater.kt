package com.etrade.mobilepro.util.updater

/**
 * Performs necessary operations to update app's data to meet new version's requirements.
 */
interface AppUpdater {

    /**
     * Performs all update operations required for this version of the app.
     *
     * Avoid accessing the data before this operation completes or during its execution.
     */
    fun update()
}
