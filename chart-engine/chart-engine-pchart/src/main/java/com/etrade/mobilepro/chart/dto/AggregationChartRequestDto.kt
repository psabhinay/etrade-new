package com.etrade.mobilepro.chart.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AggregationChartRequestDto(
    @Json(name = "ApiAggregatorRequest")
    val body: AggregationRequestBody
)

@JsonClass(generateAdapter = true)
data class AggregationRequestBody(
    @Json(name = "Api")
    val entries: List<AggregationRequestEntry>
)

@JsonClass(generateAdapter = true)
data class AggregationRequestEntry(
    @Json(name = "id")
    val id: String,
    @Json(name = "api_id")
    val apiId: String,
    @Json(name = "url")
    val url: String,
    @Json(name = "httpMethod")
    val httpMethod: String
)
