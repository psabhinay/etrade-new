package com.etrade.mobilepro.dynamicui

import com.etrade.mobilepro.accountslist.data.dto.AccountReferenceDto
import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.documents.data.StatementDocumentsReferenceDto
import com.etrade.mobilepro.documents.data.TaxDocumentsReferenceDto
import com.etrade.mobilepro.instrument.model.InstrumentReferenceListDto
import com.etrade.mobilepro.portfolio.model.PositionsReferenceDto
import com.etrade.mobilepro.portfolio.model.TaxLotReferenceListDto
import com.etrade.mobilepro.watchlist.dto.CreateWatchlistResponseDto
import com.etrade.msaccounts.dto.MsAccountsReferenceDto
import com.etrade.msaccounts.dto.UserPreferencesDto

enum class DynamicReferenceType(
    override val dtoName: String,
    override val dtoClass: Class<out BaseReference<*>>
) : DynamicEnum<BaseReference<*>> {

    ACCOUNT_STATEMENTS_LIST("account_statements_list", StatementDocumentsReferenceDto::class.java),
    ACCOUNT_TAX_DOCUMENTS("account_tax_documents", TaxDocumentsReferenceDto::class.java),
    ACCOUNTS("accounts", AccountReferenceDto::class.java),
    INSTRUMENTS("instruments", InstrumentReferenceListDto::class.java),
    MS_ACCOUNTS("ms_accounts", MsAccountsReferenceDto::class.java),
    POSITIONS("positions", PositionsReferenceDto::class.java),
    TAX_LOTS("taxlots", TaxLotReferenceListDto::class.java),
    USER_PREFERENCES("user_preferences", UserPreferencesDto::class.java),
    WATCHLIST_CREATE("watchList_Create", CreateWatchlistResponseDto::class.java)
}
