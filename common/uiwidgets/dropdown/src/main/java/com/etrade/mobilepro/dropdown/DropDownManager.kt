package com.etrade.mobilepro.dropdown

import android.view.View
import com.etrade.mobilepro.common.ToolbarActionEnd

/**
 * The implementations of this interface are for managing a dropdown view
 */
interface DropDownManager<T> {

    /**
     * A view behind this manager.
     */
    val view: View?

    /**
     * Currently we have a DialogFragment implementation of this interface.
     * Please call init with tag on re-creation before setListener to restore the listener to the correct DialogFragment.
     *
     * @param tag a string identifier
     * @param title title of the drop down
     * @param items a list of items to display
     * @param initialSelectedPosition the position where you want to be selected,
     * can be null/omitted if you don't want any item shown as selected
     * @param dividerPosition the position where you want a thick divider rather than a thin line
     * can be null/omitted if you don't want any thick dividers
     */
    @Suppress("LongParameterList")
    fun init(
        tag: String,
        title: String = "",
        items: List<T>,
        initialSelectedPosition: Int? = null,
        dividerPosition: Int? = null,
        toolbarActionEnd: ToolbarActionEnd? = null
    )

    fun updateInitialSelectedPosition(initialSelectedPosition: Int?)

    fun setListener(listener: (position: Int, item: T) -> Unit)

    fun openDropDown()

    fun closeDropDown()

    /**
     * Please call setToolBarActionEnd method after init method
     * to restore the tool bar action end text listener to the correct DialogFragment.
     */
    fun setToolBarActionEnd(toolbarActionEnd: ToolbarActionEnd)

    /**
     * Check if drop down is open for a specific tag.
     *
     * @param tag a string identifier
     *
     * @return `true` if a drop down is open, `false` otherwise
     *
     * @see init
     */
    fun isOpen(tag: String): Boolean
}
