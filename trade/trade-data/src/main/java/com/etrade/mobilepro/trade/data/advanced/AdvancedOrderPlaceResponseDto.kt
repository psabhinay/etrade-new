package com.etrade.mobilepro.trade.data.advanced

import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderPlaceResponse
import com.etrade.mobilepro.trade.api.advanced.ConfirmedAdvancedOrder
import com.etrade.mobilepro.trade.data.util.maskAccountId
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal class AdvancedOrderPlaceResponseDto : BaseAdvancedOrderResponseDto(), AdvancedOrderPlaceResponse {

    @delegate:Transient
    override val orders: List<ConfirmedAdvancedOrder> by lazy {
        createOrders { confirmedOrder }
    }

    override fun getAccount(accountId: String): String = accountId.maskAccountId()
}
