package com.etrade.mobilepro.landing

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavDirections
import androidx.navigation.NavOptions
import com.etrade.mobilepro.MainGraphDirections
import com.etrade.mobilepro.R
import com.etrade.mobilepro.accounts.util.android.view.AccountViewType
import com.etrade.mobilepro.landing.api.LandingPage
import com.etrade.mobilepro.landing.api.LandingPendingNavigation
import com.etrade.mobilepro.landing.api.LandingRouter
import com.etrade.mobilepro.landing.api.repo.LandingRepo
import com.etrade.mobilepro.userviewmodel.UserViewModel
import javax.inject.Inject

private const val ALL_BROKERAGE_NAV_THRESHOLD = 3
private const val DEFAULT_NAV_THRESHOLD = 2

class DefaultLandingRouter @Inject constructor(
    private val landingRepo: LandingRepo,
    private val userViewModel: UserViewModel
) : LandingRouter, NavController.OnDestinationChangedListener {

    private var pendingNavigation: LandingPendingNavigation? = null
    private var pendingNavDestinationHitCounter: Int = 0
    override fun createPendingNavigation() {
        val landingPage = landingRepo.getLandingState().selected
        pendingNavigation = LandingPendingNavigation(
            actualFragmentId = getActualFragmentId(landingPage),
            navDirections = getNavDirections(landingPage),
            navOptions = getNavOptions(landingPage),
            navPreparation = getNavPreparation(landingPage)
        )
        pendingNavDestinationHitCounter = 0
    }

    private val navThreshold: Int
        get() = if (landingRepo.getLandingState().selected is LandingPage.AllBrokerage) {
            ALL_BROKERAGE_NAV_THRESHOLD
        } else {
            DEFAULT_NAV_THRESHOLD
        }

    override fun getPendingNavigation(): LandingPendingNavigation? = pendingNavigation

    override fun onDestinationChanged(controller: NavController, destination: NavDestination, arguments: Bundle?) {
        pendingNavigation?.let { pending ->
            if (pending.actualFragmentId != R.id.accountTabHostFragment) {
                handleLeavingAllBrokerageAndWatchlist(destination, pending, controller)
            } else {
                handleLeavingOverviewAndPortfolio()
            }
        }
    }

    private fun handleLeavingAllBrokerageAndWatchlist(
        destination: NavDestination,
        pending: LandingPendingNavigation,
        controller: NavController
    ) {
        pendingNavDestinationHitCounter++
        if (pendingNavDestinationHitCounter == navThreshold) {
            userViewModel.accountList.find { it.accountViewType == AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW }?.let {
                userViewModel.updateSelectedAccount(it)
            }
        }
        if (destination.id == R.id.overviewFragment || (destination.id == pending.actualFragmentId && pendingNavDestinationHitCounter > 1)) {
            controller.navigate(R.id.accountsHomeFragment)
            pendingNavigation = null
            pendingNavDestinationHitCounter = 0
        }
    }

    private fun handleLeavingOverviewAndPortfolio() {
        pendingNavDestinationHitCounter++
        if (pendingNavDestinationHitCounter == navThreshold) {
            userViewModel.accountList.find { it.accountViewType == AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW }?.let {
                userViewModel.updateSelectedAccount(it)
            }
        }
        if (pendingNavDestinationHitCounter > navThreshold) {
            pendingNavigation = null
            pendingNavDestinationHitCounter = 0
        }
    }

    private fun getNavPreparation(page: LandingPage?): (() -> Unit)? = when (page) {
        is LandingPage.AllAccounts -> null
        is LandingPage.AllBrokerage -> {
            {
                userViewModel.accountList.find { it.accountViewType == AccountViewType.ALL_BROKERAGE_ACCOUNT_VIEW }?.let {
                    userViewModel.updateSelectedAccount(it)
                }
            }
        }
        is LandingPage.Portfolio -> {
            {
                userViewModel.accountList.find { it.account?.accountUuid == page.uuid }?.let {
                    userViewModel.updateSelectedAccount(it)
                }
            }
        }
        is LandingPage.Watchlist -> null
        is LandingPage.BottomInfoBox -> null
        null -> null
    }

    private fun getNavDirections(page: LandingPage?): NavDirections = when (page) {
        is LandingPage.AllAccounts -> MainGraphDirections.actionToSelectedAccountTabIndexOrTitle()
        is LandingPage.AllBrokerage -> MainGraphDirections.actionHomeToAllBrokerage()
        is LandingPage.Portfolio -> MainGraphDirections.actionToSelectedAccountTabIndexOrTitle(accountId = page.accountId)
        is LandingPage.Watchlist -> MainGraphDirections.actionHomeToWatchlist()
        else -> MainGraphDirections.actionToSelectedAccountTabIndexOrTitle()
    }

    private fun getNavOptions(page: LandingPage?): NavOptions? = when (page) {
        is LandingPage.AllAccounts -> null
        is LandingPage.AllBrokerage ->
            NavOptions
                .Builder()
                .apply {
                    setPopUpTo(R.id.accountTabHostFragment, false)
                }
                .build()
        is LandingPage.Portfolio -> null
        is LandingPage.Watchlist -> null
        is LandingPage.BottomInfoBox -> null
        null -> null
    }

    private fun getActualFragmentId(landingPage: LandingPage?): Int? = when (landingPage) {
        is LandingPage.AllAccounts -> null // It is R.id.completeviewFragment but we do not need to handle this case.
        is LandingPage.AllBrokerage -> R.id.allBrokerageAccountsFragment
        is LandingPage.Portfolio -> R.id.accountTabHostFragment
        is LandingPage.Watchlist -> R.id.overviewFragment
        else -> null
    }
}
