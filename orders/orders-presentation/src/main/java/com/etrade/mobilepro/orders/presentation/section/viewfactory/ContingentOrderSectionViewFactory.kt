package com.etrade.mobilepro.orders.presentation.section.viewfactory

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.safeParseBigDecimal

class ContingentOrderSectionViewFactory : AdvancedOrderDetailSectionViewFactory {
    override fun createSectionView(
        advancedOrderItem: BaseOrderDetailsItem.AdvancedOrderItem,
        resources: OrderDetailLabelResources
    ): List<TableSectionItemLayout> {
        val sections = mutableListOf<TableSectionItemLayout>()
        val stockResources = resources.defaultResources.resource
        sections.add(createSummarySection(resources.stock.qualifier, stockResources.getString(advancedOrderItem.qualifier.getLabel())))
        sections.add(createSummarySection(resources.stock.condition, stockResources.getString(advancedOrderItem.condition.getLabel())))
        sections.add(
            createSummarySection(
                resources.stock.advancedValue,
                MarketDataFormatter.formatMoneyValueWithCurrency(advancedOrderItem.advancedValue.safeParseBigDecimal())
            )
        )
        return sections
    }
}
