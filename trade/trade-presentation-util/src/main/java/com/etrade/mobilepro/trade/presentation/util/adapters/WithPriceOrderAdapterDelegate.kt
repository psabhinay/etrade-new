package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.WithPriceOrder
import com.etrade.mobilepro.trade.presentation.util.R
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.util.safeParseBigDecimal

internal class WithPriceOrderAdapterDelegate(private val resources: Resources) : OrderAdapterDelegate {

    override fun createOrderSectionItems(order: Order): List<TableSectionItemLayout> {
        require(order is WithPriceOrder)
        return listOfNotNull(
            sectionItem(resources, R.string.trade_label_price_type, order.priceType.getLabel())
        ) + createPriceSectionItems(order)
    }

    private fun createPriceSectionItems(response: WithPriceOrder): List<TableSectionItemLayout> {
        return with(response) {
            val mutableList = mutableListOf<TableSectionItemLayout?>()
            when (priceType) {
                PriceType.HIDDEN_STOP -> mutableList.add(yourPriceSection)
                PriceType.LIMIT, PriceType.NET_CREDIT, PriceType.NET_DEBIT -> {
                    mutableList.add(limitPriceSection)
                }
                PriceType.STOP -> {
                    mutableList.add(stopPriceSection)
                }
                PriceType.STOP_LIMIT -> {
                    mutableList.add(stopPriceSection)
                    mutableList.add(limitPriceSection)
                }
                PriceType.TRAILING_STOP_DOLLAR, PriceType.TRAILING_STOP_PERCENT -> mutableList.add(yourPriceSection)
                else -> { // no need to add any items
                }
            }
            mutableList.add(getAllOrNoneItem(allOrNone))
            mutableList.add(sectionItem(resources, R.string.trade_label_term, term.getLabel()))
            mutableList.filterNotNull()
        }
    }

    private fun getAllOrNoneItem(allOrNone: Boolean): TableSectionItemLayout? {
        return takeIf { allOrNone }?.let {
            sectionItem(
                resources = resources,
                labelRes = R.string.trade_label_all_or_none,
                valueRes = R.string.yes
            )
        }
    }

    private val WithPriceOrder.limitPriceSection: TableSectionItemLayout?
        get() = sectionItem(resources, R.string.trade_label_limit_price, MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(limitPrice))

    private val WithPriceOrder.stopPriceSection: TableSectionItemLayout?
        get() = sectionItem(resources, R.string.trade_label_stop_price, MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(stopPrice))

    private val WithPriceOrder.yourPriceSection: TableSectionItemLayout?
        get() = sectionItem(
            resources, R.string.trade_label_price,
            MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(yourPrice?.safeParseBigDecimal())
        )
}
