package com.etrade.mobilepro.completeview.model

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.completeview.StreamingNetSummaryValues
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.livedata.SingleSourceLiveDataDelegate
import com.etrade.mobilepro.streaming.api.StreamingText

data class NetSummaryViewModel(
    val initialDayGain: LiveData<StreamingText>,
    val initialNetAssets: LiveData<StreamingText>
) : DynamicViewModel(R.layout.item_net_summary) {

    private val _dayGain = SingleSourceLiveDataDelegate(initialDayGain)
    private val _netAssets = SingleSourceLiveDataDelegate(initialNetAssets)

    val dayGain: LiveData<StreamingText>
        get() = _dayGain.value
    val netAssets: LiveData<StreamingText>
        get() = _netAssets.value

    override val variableId: Int = BR.obj

    fun updateSummaryData(updatedData: StreamingNetSummaryValues) {
        _dayGain.updateSource(updatedData.dayGain)
        _netAssets.updateSource(updatedData.netAssets)
    }
}
