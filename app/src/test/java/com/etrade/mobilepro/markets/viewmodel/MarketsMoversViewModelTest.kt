package com.etrade.mobilepro.markets.viewmodel

import androidx.lifecycle.Observer
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.markets.common.MarketsBaseViewModel
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.data.response.MarketMostActiveResponse
import com.etrade.mobilepro.markets.movers.MarketsMoversViewModel
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.QuotePrice
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.stub.streaming.TestStreamingSubscriptionController
import com.etrade.mobilepro.testutil.XmlDeserializer
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Observable
import io.reactivex.Single
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Test
import org.mockito.ArgumentMatchers.anyBoolean
import org.mockito.ArgumentMatchers.anyString

@ExperimentalCoroutinesApi
internal class MarketsMoversViewModelTest : MarketsBaseViewModelTest() {

    private val stubStreamingController = TestStreamingSubscriptionController()
    private val mockMobileQuoteRepo: MobileQuoteRepo = mock {
        on {
            loadMobileQuotes(anyString(), eq(true), eq(true), eq(false))
        } doReturn Observable.just(emptyList())
    }

    @Test
    fun `fetches market most active upon creation`() {
        val mockDynamicService = mockMoverService()

        MarketsMoversViewModel(
            mockDataColorResolver,
            mockDynamicService,
            stubStreamingController,
            mockStreamingStatusProvider(false),
            mock(),
            mock()
        )

        verify(mockDynamicService).getMostActives("NSDQ")
    }

    @Test
    fun `returns error view state when market most active call is not successful`() {
        val mockObserver = mock<Observer<MarketsBaseViewModel.ViewState>>()
        val mockMoverService = mock<MoverService> {
            on { getMostActives("NSDQ") } doReturn Single.error(RuntimeException("Something went wrong"))
        }

        runBlockingTest {
            val vm = MarketsMoversViewModel(
                mockDataColorResolver,
                mockMoverService,
                stubStreamingController,
                mockStreamingStatusProvider(false),
                mock(),
                mock()
            )
            vm.viewState.observeForever(mockObserver)
            verify(mockObserver).onChanged(eq(MarketsBaseViewModel.ViewState.Error()))
        }
    }

    @Test
    fun `returns failure view state when the response contains an empty list of quotes`() {
        val mockObserver = mock<Observer<MarketsBaseViewModel.ViewState>>()
        val marketMoverResponse = MarketMostActiveResponse(emptyList())
        val mockMoverService = mockMoverService(marketMoverResponse)
        runBlockingTest {
            val vm = MarketsMoversViewModel(
                mockDataColorResolver,
                mockMoverService,
                stubStreamingController,
                mockStreamingStatusProvider(false),
                mock(),
                mock()
            )
            vm.viewState.observeForever(mockObserver)
            verify(mockObserver).onChanged(eq(MarketsBaseViewModel.ViewState.Empty))
        }
    }

    @Test
    fun `returns view state with market most active on success`() {
        val mockObserver = mock<Observer<MarketsBaseViewModel.ViewState>>()

        val moversResponse = XmlDeserializer.getObjectFromXml("markets_most_active.xml", MarketMostActiveResponse::class.java)
        val mockMoverService = mockMoverService(moversResponse)
        val mockMobileQuoteRepo: MobileQuoteRepo = mock {
            on {
                loadMobileQuotes(anyString(), anyBoolean(), anyBoolean(), anyBoolean())
            } doReturn Observable.just(
                moversResponse.quotes!!.map {
                    MobileQuote(
                        symbol = it.symbol!!,
                        quotePrice = QuotePrice(lastPrice = it.price!!, change = it.change!!, percentChange = it.percentChange!!)
                    )
                }
            )
        }

        runBlockingTest {
            val vm = MarketsMoversViewModel(
                mockDataColorResolver,
                mockMoverService,
                stubStreamingController,
                mockStreamingStatusProvider(false),
                mockIsUseExtendedHoursOn(),
                mockMobileQuoteRepo
            )
            vm.viewState.observeForever(mockObserver)

            val result = vm.viewState.value as MarketsBaseViewModel.ViewState.Success
            Assert.assertEquals(10, result.rowHeaders.size)
            Assert.assertEquals("VALE", result.rowHeaders[0].displayText)
            Assert.assertEquals(10, result.cellItems.size)
            Assert.assertEquals("13.66", result.cellItems[0][0].value.toString())
        }
    }

    @Test
    fun `updates view state upon symbol update event from streamer`() {
        val mockObserver = mock<Observer<MarketsBaseViewModel.ViewState>>()
        val moversResponse = XmlDeserializer.getObjectFromXml("markets_most_active.xml", MarketMostActiveResponse::class.java)
        val mockMoverService = mockMoverService(moversResponse)
        val updatedLastPrice = "14.77"
        val mockLevel1Data = mock<Level1Data> {
            on { lastPrice } doReturn updatedLastPrice
            on { timestampSinceEpoch } doReturn 1578595602009L
        }
        val mockMobileQuoteRepo: MobileQuoteRepo = mock {
            on {
                loadMobileQuotes(anyString(), anyBoolean(), anyBoolean(), anyBoolean())
            } doReturn Observable.just(
                moversResponse.quotes!!.map {
                    MobileQuote(
                        symbol = it.symbol!!,
                        quotePrice = QuotePrice(lastPrice = it.price!!, change = it.change!!, percentChange = it.percentChange!!)
                    )
                }
            )
        }

        runBlockingTest {
            val vm = MarketsMoversViewModel(
                mockDataColorResolver,
                mockMoverService,
                stubStreamingController,
                mockStreamingStatusProvider(true),
                mockIsUseExtendedHoursOn(),
                mockMobileQuoteRepo
            )
            vm.viewState.observeForever(mockObserver)
            stubStreamingController.emitStreamData("VALE", InstrumentType.UNKNOWN, mockLevel1Data)

            val result = vm.viewState.value as MarketsBaseViewModel.ViewState.Success
            Assert.assertEquals("VALE", result.rowHeaders[0].displayText)
            Assert.assertEquals(10, result.cellItems.size)
            Assert.assertEquals(updatedLastPrice, result.cellItems[0][0].value.toString())
            Assert.assertEquals("January 09, 2020 01:46 PM EST", result.timeStamp)
        }
    }

    @Test
    fun `verify extended hours fetch streaming off and market scheduler returns extended hours`() {
        val moversResponse = XmlDeserializer.getObjectFromXml("markets_most_active.xml", MarketMostActiveResponse::class.java)
        val mockMoverService = mockMoverService(moversResponse)

        runBlockingTest {
            MarketsMoversViewModel(
                mockDataColorResolver,
                mockMoverService,
                stubStreamingController,
                mockStreamingStatusProvider(false),
                mockIsUseExtendedHoursOn(),
                mockMobileQuoteRepo
            )

            verify(mockMobileQuoteRepo).loadMobileQuotes("VALE,GE,PCG,BAC,NOK,F,PFE,T,ENLK,ENLC", extendedHours = true)
        }
    }

    private fun mockMoverService(marketMoverResponse: MarketMostActiveResponse = MarketMostActiveResponse()) = mock<MoverService> {
        on { getMostActives("NSDQ") } doReturn Single.just(marketMoverResponse)
    }

    private fun mockStreamingStatusProvider(isStreamingEnabled: Boolean) = mock<StreamingStatusController> {
        on { this.isStreamingToggleEnabled } doReturn isStreamingEnabled
    }

    private fun mockIsUseExtendedHoursOn() = mock<IsExtendedHoursOnUseCase> {
        onBlocking { execute(anyOrNull()) }.thenReturn(true)
    }
}
