package com.etrade.mobilepro.portfolio.data

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.instrument.Instrument
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.portfolio.model.SimpleInstrument
import com.etrade.mobilepro.portfolio.model.SimplePortfolioRecord
import com.etrade.mobilepro.portfolio.model.SimplePosition
import com.etrade.mobilepro.positions.api.PortfolioRecord
import com.etrade.mobilepro.positions.api.Position
import java.util.UUID

internal fun merge(
    recordId: String?,
    position: Position?,
    instrument: Instrument?,
    baseSymbolAndPrice: String? = null,
    isTaxLot: Boolean = false
): PortfolioRecord {
    val p = position?.let { SimplePosition(it, baseSymbolAndPrice) } ?: createPosition(recordId, instrument)
    val i = instrument?.let { SimpleInstrument(it) } ?: createInstrument(position)

    return SimplePortfolioRecord(
        recordId = recordId ?: p.positionId,
        displayName = position?.symbol ?: instrument?.symbol ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE,
        isTaxLot = isTaxLot,
        position = p,
        instrument = i
    )
}

private fun createPosition(recordId: String?, instrument: Instrument?): Position {
    return SimplePosition(
        positionId = recordId ?: UUID.randomUUID().toString(),
        symbol = instrument?.symbol ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE
    )
}

private fun createInstrument(position: Position?): Instrument {
    val symbol = position?.symbol
    return SimpleInstrument(
        symbol = symbol ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE,
        symbolDescription = symbol ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE,
        displaySymbol = symbol ?: MarketDataFormatter.EMPTY_FORMATTED_VALUE,
        instrumentType = InstrumentType.UNKNOWN
    )
}
