package com.etrade.mobilepro.checkdeposit.presentation.details.holder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.checkdeposit.presentation.R
import com.etrade.mobilepro.checkdeposit.presentation.databinding.AdapterDepositDetailImageBinding
import com.etrade.mobilepro.checkdeposit.presentation.details.DepositDetail
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.util.android.binding.resources

class DepositDetailImageViewHolder(
    private val imageLoader: ImageLoader,
    private val binding: AdapterDepositDetailImageBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: DepositDetail.Image) {
        with(binding) {
            imageLoader.show(item.url, binding.image) {
                image.visibility = View.VISIBLE
            }
            if (item.isFront) {
                root.contentDescription = resources.getString(R.string.check_image_front)
            } else {
                root.contentDescription = resources.getString(R.string.check_image_back)
            }
        }
    }
}
