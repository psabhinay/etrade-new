package com.etrade.mobilepro.msaccounts.presentation.view

import android.content.Context
import android.util.AttributeSet
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.common.customview.LifecycleLinearLayout
import com.etrade.mobilepro.msaccounts.presentation.databinding.MsAccountFootnoteBinding
import com.etrade.mobilepro.msaccounts.presentation.model.MsAccountFootnote
import com.etrade.mobilepro.util.android.extension.layoutInflater
import com.etrade.mobilepro.util.android.footnotespan.FootnoteSectionContext
import com.etrade.mobilepro.util.android.footnotespan.buildFootnote

class MsAccountFootnotesLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : LifecycleLinearLayout(context, attrs) {

    init {
        orientation = VERTICAL
    }

    fun bindMsAccountFootnotes(footnotes: List<MsAccountFootnote>) {
        removeAllViews()
        val footnotesContext = FootnoteSectionContext()
        footnotes.forEach {
            val binding = MsAccountFootnoteBinding.inflate(layoutInflater, this, true)
            binding.root.text = footnotesContext.buildFootnote(binding.root, it.symbol, it.text)
            binding.root.contentDescription = it.description
        }
    }
}

@BindingAdapter("footnotes")
fun bindMsAccountFootnotes(
    sectionsLayout: MsAccountFootnotesLayout,
    footnotes: List<MsAccountFootnote>?
) {
    sectionsLayout.bindMsAccountFootnotes(footnotes.orEmpty())
}
