package com.etrade.mobilepro.order.details.dto

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.mapper.OrderDetailDescriptionMapper
import com.etrade.mobilepro.order.details.mapper.OrderDetailsMapper
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.orders.api.MarketSession
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.api.OrigSysCode
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.testutil.getObjectFromJson
import com.etrade.mobilepro.trade.api.option.TradeLeg
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.math.BigDecimal
import java.util.Locale

internal class OrderDetailsMapperTest {

    private fun getOrderDetailsResponse(): ServerResponseDto<OrderDetailsResponseDto> =
        OrderDetailsMapperTest::class.getObjectFromJson(
            "order_details.json",
            ServerResponseDto::class.java,
            OrderDetailsResponseDto::class.java
        )

    @Test
    fun `test orders mapper results`() {
        val response = getOrderDetailsResponse()
        val orderDetailsDto = response.data?.viewOrderEvents

        val orderDetailList = orderDetailsDto?.let {
            OrderDetailsMapper(OrderDetailDescriptionMapper()::map).map(it)
        }.orEmpty()

        assert(!orderDetailList.isNullOrEmpty())

        orderDetailList.forEach {
            assert(it is BaseOrderDetailsItem.OrderDetailsItem)
            (it as BaseOrderDetailsItem.OrderDetailsItem).assert()
        }
    }

    private fun BaseOrderDetailsItem.OrderDetailsItem.assert() {
        assert(executedDate.epochDay >= orderPlacedDate.epochDay)
        assert(!isAdvancedOrder)
        assertEquals(account, "84716016")
        assert(advancedItem == null)
        assert(!goodTillDateFlag)
        assert(!hasPartialExecution)
        assert(!hasDependentFlag)
        assert(!hasMultipleLegs)
        assert(!hiddenFlag)
        assertEquals(orderStatus, OrderStatus.EXECUTED)
        assertEquals(orderType, OrderType.EQUITY)
        assertEquals(priceType, PriceType.MARKET)
        assertEquals(quantity, "1")
        assertEquals(orderNumber, "19")
        assertEquals(term, OrderTerm.GOOD_FOR_DAY)
        assertEquals(priceExecuted, BigDecimal.TEN)
        assertEquals(commission, BigDecimal.ZERO)
        assertEquals(estimatedCommission, BigDecimal.ZERO)
        assertEquals(estimatedTotalCost?.toPlainString(), "174.46")
        assertEquals(orderSavedDate, "05/27/2021")
        assertEquals(gtdExpirationData, "05/27/2021")
        assertEquals(marketSession, MarketSession.MARKET_REGULAR)
        assertEquals(marketDestination, MarketDestination.UNKNOWN)
        assertEquals(origSysCode, OrigSysCode.ORIG_SYS_CODE_ANDR)
        assertEquals(tradeLegs.size, 1)

        tradeLegs.assert()
    }

    private fun List<TradeLeg>.assert() {
        forEach { leg ->
            assertEquals(leg.displaySymbol, "MRNA")
            assertEquals(leg.quantity, 1)
            assertEquals(leg.symbol, "MRNA")
            assertEquals(leg.transactionType, TransactionType.BUY_OPEN)
        }
    }

    private val DISPLAYED_DATE_FORMAT: DateTimeFormatter =
        DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US)

    private val String?.epochDay
        get() = this?.let {
            LocalDate.parse(split(" ").first(), DISPLAYED_DATE_FORMAT).toEpochDay()
        } ?: 0L
}
