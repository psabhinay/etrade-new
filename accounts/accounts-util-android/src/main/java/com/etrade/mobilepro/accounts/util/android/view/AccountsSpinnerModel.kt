package com.etrade.mobilepro.accounts.util.android.view

import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobile.accounts.dto.InstitutionType
import com.etrade.mobile.accounts.dto.ManagedAccountType
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.util.WithContentDescription

/**
 * Account Dropdown Data model
 */
class AccountsSpinnerModel(val accountDisplayName: String, val accountViewType: AccountViewType, val account: Account? = null) :
    WithContentDescription {

    override val contentDescription: String = account?.run {
        accountDisplayName.replace("-", "dash")
    } ?: accountDisplayName

    override fun toString(): String {
        return accountDisplayName
    }
}

fun Account.getAccountViewType(): AccountViewType {
    return when {
        institutionType == InstitutionType.TELEBANK -> AccountViewType.BANK_ACCOUNT_VIEW
        institutionType == InstitutionType.OLINK -> AccountViewType.ESP_ACCOUNT_VIEW
        accountType == AccountType.Managed -> when (managedAccountType) {
            ManagedAccountType.FixedIncomePortfolios -> AccountViewType.SINGLE_BROKERAGE_ACCOUNT_VIEW
            ManagedAccountType.BlendPortfolios,
            ManagedAccountType.BlendPortfoliosFlex,
            ManagedAccountType.Unknown -> AccountViewType.MANAGED_ACCOUNT_VIEW
        }
        else -> AccountViewType.SINGLE_BROKERAGE_ACCOUNT_VIEW
    }
}
