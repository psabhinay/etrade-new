package com.etrade.mobilepro.dynamicui.api

interface Diffable {
    fun hasSameContents(other: Any): Boolean
    fun isSameItem(other: Any): Boolean
}
