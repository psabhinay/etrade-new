package com.etrade.mobilepro.earnings.viewModel

import android.content.res.Resources
import com.etrade.eo.core.util.isZero
import com.etrade.mobilepro.earnings.EarningsDetailData
import com.etrade.mobilepro.earnings.R
import com.etrade.mobilepro.util.android.parseSafeLocalDate
import com.etrade.mobilepro.util.android.parseSafeLocalTime
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import com.etrade.mobilepro.util.formatters.createAmericanSimpleTimeFormatter
import com.etrade.mobilepro.util.market.time.NEW_YORK_ZONE_ID
import org.threeten.bp.DayOfWeek
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.DateTimeFormatter.ISO_LOCAL_TIME
import java.math.BigDecimal
import java.text.DecimalFormat
import kotlin.math.absoluteValue

private val DATE_FORMATTER: DateTimeFormatter = DateTimeFormatter.BASIC_ISO_DATE
private const val WEEKS_TO_ADD = 12L
private const val NUMBER_DAYS_FOR_ANNOUNCEMENT = 3L
private const val NUMBER_DAYS_FOR_RELEASE = 14L
private val NEXT_EARNINGS_DATE_FORMATTER by lazy { createAmericanSimpleDateFormatter() }
private val NEXT_EARNINGS_TIME_FORMATTER by lazy { createAmericanSimpleTimeFormatter() }
private val VALUE_TEMPLATE: DecimalFormat = DecimalFormat("#.##")
private val WEEK_DAYS_SIZE = DayOfWeek.values().size

enum class MarketEarningsType {
    EARNINGS, EARNINGS_RELEASE, EARNINGS_ANNOUNCEMENT
}

data class MarketEarnings(
    val type: MarketEarningsType,
    val title: String,
    val message: String,
    val high: String? = null,
    val mean: String? = null,
    val low: String? = null,
    val dateTimeForCalendar: LocalDateTime? = null
)

private data class NextEarningDate(val date: LocalDateTime?, val isNextEarningsDateEstimated: Boolean)

@SuppressWarnings("ComplexMethod")
private fun getNextEarningsDate(currentDateTime: LocalDateTime, earningsDetailData: EarningsDetailData?): NextEarningDate {
    if (earningsDetailData == null) {
        return NextEarningDate(null, false)
    }

    val announceDate: LocalDate? = earningsDetailData.announceDate?.let { parseAnnounceDate(it) }
    val expectedDate: LocalDate? = earningsDetailData.expectedDate?.let { parseExpectedDate(it) }
    val announceTime: LocalTime? = earningsDetailData.announceTime?.let { parseAnnounceTime(it) }

    return if (isTwoDayPast(currentDateTime, expectedDate) || (announceDate == null && expectedDate == null)) {
        NextEarningDate(null, false)
    } else if (expectedDate == null || expectedDate.isBefore(currentDateTime.toLocalDate())) {
        NextEarningDate(findNextEarningsDate(announceDate, expectedDate, announceTime), announceDate != null)
    } else {
        NextEarningDate(LocalDateTime.of(expectedDate, announceTime ?: LocalTime.MIN), false)
    }
}

private fun findNextEarningsDate(announceDate: LocalDate?, expectedDate: LocalDate?, announceTime: LocalTime?): LocalDateTime? {
    return if (announceDate != null) {
        var date: LocalDate? = announceDate

        if (expectedDate != null) {
            date = if (announceDate > expectedDate) announceDate else expectedDate
        }

        // Estimate 3 months forward from this date as the next earnings date (12 * weeks to the day to avoid a Saturday / Sunday date)
        date = date?.plusWeeks(WEEKS_TO_ADD)

        appendTimeToNextEarningsDate(announceTime, date)
    } else {
        LocalDateTime.of(expectedDate, LocalTime.MIN)
    }
}

private fun appendTimeToNextEarningsDate(announceTime: LocalTime?, date: LocalDate?): LocalDateTime? {
    return if (announceTime != null) {
        LocalDateTime.of(date, announceTime)
    } else {
        LocalDateTime.of(date, LocalTime.MIN)
    }
}

private fun parseExpectedDate(expectedDate: String): LocalDate? = expectedDate.parseSafeLocalDate(DATE_FORMATTER)

private fun parseAnnounceDate(announceDate: String): LocalDate? = announceDate.parseSafeLocalDate(DATE_FORMATTER)

private fun parseAnnounceTime(announceTime: String): LocalTime? = announceTime.parseSafeLocalTime(ISO_LOCAL_TIME)

private fun isTwoDayPast(currentDateTime: LocalDateTime, expectedDate: LocalDate?): Boolean {
    return expectedDate?.let {
        Duration.between(it.atStartOfDay(), currentDateTime).toDays() in 1..2
    } ?: false
}

private fun getNumberOfBusinessDays(startDateTime: LocalDateTime, endDateTime: LocalDateTime): Long {
    var startW = startDateTime.dayOfWeek
    var endW = endDateTime.dayOfWeek

    if (endDateTime.isBefore(startDateTime)) {
        startW = endDateTime.dayOfWeek
        endW = startDateTime.dayOfWeek
    }

    val days = Duration.between(startDateTime, endDateTime).toDays().absoluteValue
    val daysWithoutWeekends = days - 2 * ((days + startW.value) / WEEK_DAYS_SIZE)

    // adjust for starting and ending on a Sunday:
    return daysWithoutWeekends + (if (startW == DayOfWeek.SUNDAY) 1 else 0).toLong() + (if (endW == DayOfWeek.SUNDAY) 1 else 0).toLong()
}

private fun getMarketEarningsType(currentDate: LocalDateTime, lastDate: LocalDateTime?, nextDate: LocalDateTime?): MarketEarningsType {
    return if (lastDate != null && nextDate != null) {
        var lastEarningsDateDifference = Duration.between(currentDate, lastDate).toDays().absoluteValue

        if (lastEarningsDateDifference in (NUMBER_DAYS_FOR_ANNOUNCEMENT + 1) until WEEK_DAYS_SIZE) {
            val numberOfBusinessDays = getNumberOfBusinessDays(currentDate, lastDate).absoluteValue

            if (numberOfBusinessDays in 0..NUMBER_DAYS_FOR_ANNOUNCEMENT) {
                lastEarningsDateDifference = numberOfBusinessDays
            }
        }

        if (lastEarningsDateDifference in 0..NUMBER_DAYS_FOR_ANNOUNCEMENT) {
            MarketEarningsType.EARNINGS_ANNOUNCEMENT
        } else {
            val nextEarningsDateDifference = Duration.between(nextDate, currentDate).toDays().absoluteValue

            if (nextEarningsDateDifference in 0..NUMBER_DAYS_FOR_RELEASE) {
                MarketEarningsType.EARNINGS_RELEASE
            } else {
                MarketEarningsType.EARNINGS
            }
        }
    } else {
        MarketEarningsType.EARNINGS
    }
}

@Suppress("LongMethod") // noting to extract
fun createMarketEarnings(
    earningsDetailData: List<EarningsDetailData>,
    resources: Resources,
    currentDateTime: LocalDateTime = LocalDateTime.now()
): MarketEarnings {
    val emptyDate: String = resources.getString(R.string.empty_date_default)
    val size: Int = earningsDetailData.size

    if (size < 2) {
        return MarketEarnings(
            type = MarketEarningsType.EARNINGS,
            title = resources.getString(R.string.earnings),
            message = resources.getString(R.string.earnings_date).format(emptyDate)
        )
    }

    // Get the middle element, middle element is the last item in the historic data set assuming the response has equal number of future and historical periods
    var detailData: EarningsDetailData? = earningsDetailData.getOrNull(size / 2 - 1)

    val (nextEarningsDate: LocalDateTime?, isNextEarningsDateEstimated: Boolean) = getNextEarningsDate(currentDateTime, detailData)
    val lastEarningsDate: LocalDateTime? = detailData?.dateTime?.let { LocalDateTime.ofInstant(Instant.ofEpochMilli(it.longValueExact()), NEW_YORK_ZONE_ID) }
    val announcedQuarter: String? = detailData?.quarter
    val epsActualValue: BigDecimal? = detailData?.actualValue
    val epsActualMeanEstimate: BigDecimal? = detailData?.surpriseMean

    detailData = earningsDetailData.getOrNull(size / 2)

    val nextQuarter: String? = detailData?.quarter
    val epsEstimateHigh: BigDecimal? = detailData?.high
    val epsEstimateMean: BigDecimal? = detailData?.estMean
    val epsEstimateLow: BigDecimal? = detailData?.low

    val emptyValue: String = resources.getString(R.string.empty_default)

    return when (getMarketEarningsType(currentDateTime, lastEarningsDate, nextEarningsDate)) {
        MarketEarningsType.EARNINGS -> {
            createEarningsType(resources, nextEarningsDate, isNextEarningsDateEstimated, emptyDate)
        }
        MarketEarningsType.EARNINGS_RELEASE -> {
            createEarningsReleaseType(
                resources,
                epsEstimateHigh,
                emptyValue,
                epsEstimateMean,
                epsEstimateLow,
                nextEarningsDate,
                nextQuarter,
                emptyDate
            )
        }
        MarketEarningsType.EARNINGS_ANNOUNCEMENT -> {
            createEarningsAnnouncementType(
                resources,
                announcedQuarter,
                emptyValue,
                epsActualValue,
                lastEarningsDate,
                emptyDate,
                epsActualMeanEstimate
            )
        }
    }
}

@Suppress("LongMethod", "LongParameterList")
private fun createEarningsAnnouncementType(
    resources: Resources,
    announcedQuarter: String?,
    emptyValue: String,
    epsActualValue: BigDecimal?,
    lastEarningsDate: LocalDateTime?,
    emptyDate: String,
    epsActualMeanEstimate: BigDecimal?
): MarketEarnings {
    val actual: BigDecimal = epsActualValue ?: BigDecimal.ZERO
    val mean: BigDecimal = epsActualMeanEstimate ?: BigDecimal.ZERO
    val value: BigDecimal = actual - mean
    val valueLabel: String = when {
        value.isZero() -> resources.getString(R.string.matches_analyst)
        actual < mean -> resources.getString(R.string.missing_analyst)
        actual >= mean -> resources.getString(R.string.beating_analyst)
        else -> emptyValue
    }

    return MarketEarnings(
        type = MarketEarningsType.EARNINGS_ANNOUNCEMENT,
        title = resources.getString(R.string.earnings_announcement),
        message = resources.getString(R.string.announcement_text).format(
            announcedQuarter ?: emptyValue,
            epsActualValue ?: emptyValue,
            lastEarningsDate?.format(NEXT_EARNINGS_DATE_FORMATTER) ?: emptyDate,
            valueLabel
        ).let {
            if (value.isZero()) {
                it
            } else {
                "$it ${resources.getString(R.string.announcement_text_by).format(VALUE_TEMPLATE.format(value.abs()))}"
            }
        }
    )
}

@Suppress("LongParameterList")
private fun createEarningsReleaseType(
    resources: Resources,
    epsEstimateHigh: BigDecimal?,
    emptyValue: String,
    epsEstimateMean: BigDecimal?,
    epsEstimateLow: BigDecimal?,
    nextEarningsDate: LocalDateTime?,
    nextQuarter: String?,
    emptyDate: String
): MarketEarnings {
    return MarketEarnings(
        type = MarketEarningsType.EARNINGS_RELEASE,
        title = resources.getString(R.string.earnings_release),
        high = epsEstimateHigh?.toString() ?: emptyValue,
        mean = epsEstimateMean?.toString() ?: emptyValue,
        low = epsEstimateLow?.toString() ?: emptyValue,
        message = resources.getString(R.string.earnings_to_be_released_on).format(
            nextQuarter ?: emptyValue,
            nextEarningsDate?.format(NEXT_EARNINGS_DATE_FORMATTER) ?: emptyDate,
            nextEarningsDate?.format(NEXT_EARNINGS_TIME_FORMATTER) ?: "--/--"
        ),
        dateTimeForCalendar = nextEarningsDate
    )
}

private fun createEarningsType(
    resources: Resources,
    nextEarningsDate: LocalDateTime?,
    isNextEarningsDateEstimated: Boolean,
    emptyDate: String
): MarketEarnings {
    return MarketEarnings(
        type = MarketEarningsType.EARNINGS,
        title = resources.getString(R.string.earnings),
        message = resources.getString(R.string.earnings_date).format(
            nextEarningsDate?.format(NEXT_EARNINGS_DATE_FORMATTER) ?: emptyDate
        ).let {
            if (isNextEarningsDateEstimated) {
                "$it ${resources.getString(R.string.next_earnings_date_est)}"
            } else {
                it
            }
        },
        dateTimeForCalendar = nextEarningsDate
    )
}
