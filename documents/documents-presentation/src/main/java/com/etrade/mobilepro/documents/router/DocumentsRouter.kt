package com.etrade.mobilepro.documents.router

import androidx.navigation.NavController
import com.etrade.mobilepro.documents.Document
import org.threeten.bp.LocalDate

interface DocumentsRouter {
    fun navigateToDetails(navController: NavController, item: Document)

    fun openTaxYearSelector(navController: NavController, minYear: Int, maxYear: Int, value: Int)

    fun openDateRangeSelector(navController: NavController, date: LocalDate, valueKey: String, minDate: LocalDate?, maxDate: LocalDate?)
}
