package com.etrade.mobilepro.menu

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.home.BottomNavigationViewModel
import com.etrade.mobilepro.home.model.AppActionViewModel
import com.etrade.mobilepro.menu.fragment.BaseMenuPageFragment
import com.etrade.mobilepro.menu.viewmodel.MainMenuPageViewModel
import com.etrade.mobilepro.menu.viewmodel.MenuPageViewModel
import com.etrade.mobilepro.navigation.EtNavController
import com.etrade.mobilepro.userviewmodel.UserViewModel

const val MENU_PAGE_LEVEL = 2

/**
 * Generic menu page fragment. To a new page:
 * 1) Define a navigation destination with a new navigation id
 * 2) Create a new subclass of [MenuPageViewModel] (see, for example, [MainMenuPageViewModel])
 * 3) Override the [menuPageViewModel] property and provide your new view model with `by viewModels`
 */
abstract class AppBaseMenuFragment(
    protected val viewModelFactory: ViewModelProvider.Factory,
    protected val userViewModel: UserViewModel
) : BaseMenuPageFragment() {

    private val appActionViewModel: AppActionViewModel by activityViewModels { viewModelFactory }
    protected val etNavController: EtNavController by lazy {
        EtNavController(
            navController = findNavController(),
            userViewModel = userViewModel,
            appActionViewModel = appActionViewModel
        )
    }

    protected val bottomNavigationViewModel: BottomNavigationViewModel by activityViewModels { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        menuPageViewModel.menuPageItems.observe(viewLifecycleOwner, Observer(this::updateMenuItems))
    }

    override fun navigate(directions: NavDirections) {
        etNavController.navigate(directions)
    }

    override fun navigate(appAction: Uri) {
        etNavController.navigateFromAppAction(appAction)
    }

    override fun navigate(action: () -> Unit) {
        action()
    }

    override fun saveMenuDirection(directions: NavDirections?) {
        if (directions != null) {
            bottomNavigationViewModel.savedMenuDirections.add(TextMenuItem.MenuAction.MenuNavDirectionsAction(directions))
        } else {
            bottomNavigationViewModel.savedMenuDirections.clear()
        }
    }

    override fun saveMenuDirection(uri: Uri) {
        bottomNavigationViewModel.savedMenuDirections.add(TextMenuItem.MenuAction.MenuAppAction(uri))
    }

    override fun saveMenuDirection(action: () -> Unit) {
        bottomNavigationViewModel.savedMenuDirections.add(TextMenuItem.MenuAction.MenuGenericAction(action))
    }

    /**
     * If there's more than 1 nav directions saved in the stack, it means user just pressed back button and we're
     * returning to a previous menu level
     */
    fun popNav() {
        bottomNavigationViewModel.savedMenuDirections.apply {
            if (this.size >= MENU_PAGE_LEVEL) {
                this.pop()
            }
        }
    }
}
