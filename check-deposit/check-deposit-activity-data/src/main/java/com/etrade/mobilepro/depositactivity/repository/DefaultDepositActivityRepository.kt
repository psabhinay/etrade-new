package com.etrade.mobilepro.depositactivity.repository

import com.etrade.mobilepro.checkdeposit.activity.DepositActivityRepository
import com.etrade.mobilepro.checkdeposit.activity.service.DepositActivityItemDto
import com.etrade.mobilepro.checkdeposit.activity.service.DepositActivityService
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET

private const val HTTP_SUCCESS = 200
private const val HTTP_SUCCESS_EMPTY_BODY = 204

class DefaultDepositActivityRepository(
    private val depositActivityService: DepositActivityService,
    private val getConsumerKey: () -> String?
) : DepositActivityRepository {
    override suspend fun loadDepositActivityItems(): ETResult<List<DepositActivityItemDto>> {
        return runCatchingET {
            val response = depositActivityService.getDepositActivity(getConsumerKey.invoke())
            when {
                response.code() == HTTP_SUCCESS -> response.body()?.checkDepositActivityResponse?.depositActivity ?: emptyList()
                response.code() == HTTP_SUCCESS_EMPTY_BODY -> emptyList()
                else -> throw UnsupportedOperationException("unexpected result")
            }
        }
    }
}
