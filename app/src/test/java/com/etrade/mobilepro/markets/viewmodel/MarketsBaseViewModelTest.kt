package com.etrade.mobilepro.markets.viewmodel

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.markets.common.MarketsBaseViewModel
import com.etrade.mobilepro.util.color.DataColor
import com.etrade.mobilepro.util.color.DataColorResolver
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
internal abstract class MarketsBaseViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    protected val mockResources: Resources = mock()
    protected val mockObserver = mock<Observer<MarketsBaseViewModel.ViewState>>()
    protected val mockDataColorResolver = mock<DataColorResolver> {
        on { resolveFilledDataColor(anyOrNull()) } doReturn DataColor.FILLED_POSITIVE
        on { resolveDataColor(anyOrNull()) } doReturn DataColor.NEGATIVE
    }
    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(testCoroutineDispatcher)
    }

    @After
    fun cleanUp() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }
}
