package com.etrade.mobilepro.portfolio.delegate

import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode

interface PositionSortDelegateFactory {
    fun create(
        portfolioTableViewType: PortfolioTableViewType,
        positionTableMode: PositionTableMode
    ): PositionSortDelegate
}
