package com.etrade.mobilepro.chart.netassets;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0017B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0015\u001a\u00020\u0016H\u0002R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R/\u0010\n\u001a\u0004\u0018\u00010\t2\b\u0010\b\u001a\u0004\u0018\u00010\t8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u0017\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00070\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel;", "Landroidx/lifecycle/ViewModel;", "getNetAssetsChartDataEntriesUseCase", "Lcom/etrade/mobilepro/chart/netassets/GetNetAssetsChartDataEntriesUseCase;", "(Lcom/etrade/mobilepro/chart/netassets/GetNetAssetsChartDataEntriesUseCase;)V", "_chartState", "Landroidx/lifecycle/MutableLiveData;", "Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState;", "<set-?>", "", "accountId", "getAccountId", "()Ljava/lang/String;", "setAccountId", "(Ljava/lang/String;)V", "accountId$delegate", "Lkotlin/properties/ReadWriteProperty;", "chartState", "Landroidx/lifecycle/LiveData;", "getChartState", "()Landroidx/lifecycle/LiveData;", "loadChart", "", "ChartState", "netassets_debug"})
public final class NetAssetsChartViewModel extends androidx.lifecycle.ViewModel {
    private final com.etrade.mobilepro.chart.netassets.GetNetAssetsChartDataEntriesUseCase getNetAssetsChartDataEntriesUseCase = null;
    @org.jetbrains.annotations.Nullable()
    private final kotlin.properties.ReadWriteProperty accountId$delegate = null;
    private final androidx.lifecycle.MutableLiveData<com.etrade.mobilepro.chart.netassets.NetAssetsChartViewModel.ChartState> _chartState = null;
    
    @javax.inject.Inject()
    public NetAssetsChartViewModel(@org.jetbrains.annotations.NotNull()
    com.etrade.mobilepro.chart.netassets.GetNetAssetsChartDataEntriesUseCase getNetAssetsChartDataEntriesUseCase) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.etrade.mobilepro.chart.netassets.NetAssetsChartViewModel.ChartState> getChartState() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccountId() {
        return null;
    }
    
    public final void setAccountId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    private final void loadChart() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b\u00a8\u0006\t"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState;", "", "()V", "Error", "Idle", "Loading", "Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState$Idle;", "Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState$Loading;", "Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState$Error;", "netassets_debug"})
    public static abstract class ChartState {
        
        private ChartState() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\u0012"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState$Idle;", "Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState;", "entries", "", "Lcom/etrade/mobilepro/barchartview/DataEntry;", "(Ljava/util/List;)V", "getEntries", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "netassets_debug"})
        public static final class Idle extends com.etrade.mobilepro.chart.netassets.NetAssetsChartViewModel.ChartState {
            @org.jetbrains.annotations.NotNull()
            private final java.util.List<com.etrade.mobilepro.barchartview.DataEntry> entries = null;
            
            @org.jetbrains.annotations.NotNull()
            public final com.etrade.mobilepro.chart.netassets.NetAssetsChartViewModel.ChartState.Idle copy(@org.jetbrains.annotations.NotNull()
            java.util.List<? extends com.etrade.mobilepro.barchartview.DataEntry> entries) {
                return null;
            }
            
            @java.lang.Override()
            public boolean equals(@org.jetbrains.annotations.Nullable()
            java.lang.Object other) {
                return false;
            }
            
            @java.lang.Override()
            public int hashCode() {
                return 0;
            }
            
            @org.jetbrains.annotations.NotNull()
            @java.lang.Override()
            public java.lang.String toString() {
                return null;
            }
            
            public Idle(@org.jetbrains.annotations.NotNull()
            java.util.List<? extends com.etrade.mobilepro.barchartview.DataEntry> entries) {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<com.etrade.mobilepro.barchartview.DataEntry> component1() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.util.List<com.etrade.mobilepro.barchartview.DataEntry> getEntries() {
                return null;
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState$Loading;", "Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState;", "()V", "netassets_debug"})
        public static final class Loading extends com.etrade.mobilepro.chart.netassets.NetAssetsChartViewModel.ChartState {
            @org.jetbrains.annotations.NotNull()
            public static final com.etrade.mobilepro.chart.netassets.NetAssetsChartViewModel.ChartState.Loading INSTANCE = null;
            
            private Loading() {
                super();
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState$Error;", "Lcom/etrade/mobilepro/chart/netassets/NetAssetsChartViewModel$ChartState;", "()V", "netassets_debug"})
        public static final class Error extends com.etrade.mobilepro.chart.netassets.NetAssetsChartViewModel.ChartState {
            @org.jetbrains.annotations.NotNull()
            public static final com.etrade.mobilepro.chart.netassets.NetAssetsChartViewModel.ChartState.Error INSTANCE = null;
            
            private Error() {
                super();
            }
        }
    }
}