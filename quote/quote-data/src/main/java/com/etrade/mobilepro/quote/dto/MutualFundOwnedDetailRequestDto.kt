package com.etrade.mobilepro.quote.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MutualFundOwnedDetailRequestDto(
    @Json(name = "value")
    val value: Value
) {
    @JsonClass(generateAdapter = true)
    class Value(
        @Json(name = "accountId")
        val accountId: String = "",
        @Json(name = "symbol")
        val symbol: String = "",
        @Json(name = "typeCode")
        val typeCode: String = ""
    )
    companion object {
        fun toMutualFundSellRequest(accountId: String, symbol: String, typeCode: String): MutualFundOwnedDetailRequestDto =
            MutualFundOwnedDetailRequestDto(
                Value(accountId = accountId, symbol = symbol, typeCode = typeCode)
            )
    }
}
