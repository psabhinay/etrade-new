package com.etrade.mobilepro.appwidget.watchlist.selection

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.appwidget.R
import com.etrade.mobilepro.appwidget.api.settings.WidgetId
import com.etrade.mobilepro.appwidget.databinding.AppwidgetWatchlistSelectionFragmentBinding
import com.etrade.mobilepro.appwidget.settings.WIDGET_ID_KEY
import com.etrade.mobilepro.overlayfragment.OverlayBaseFragment
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class WatchlistSelectionFragment @Inject constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val userPreferences: UserPreferences,
    snackbarUtilFactory: SnackbarUtilFactory
) : OverlayBaseFragment() {
    private val widgetId: WidgetId? by lazy { arguments?.getInt(WIDGET_ID_KEY)?.let { WidgetId(it) } }
    private val viewModel: WatchlistSelectionViewModel by activityViewModels { viewModelFactory }
    private lateinit var watchlistAdapter: WatchlistSelectionAdapter
    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    override val layoutRes: Int = R.layout.appwidget_watchlist_selection_fragment

    private val binding by viewBinding(AppwidgetWatchlistSelectionFragmentBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        viewModel.viewState.observe(viewLifecycleOwner, ::handleViewState)
        widgetId?.let { viewModel.fetch(it) }
    }

    private fun setupViews() {
        watchlistAdapter = WatchlistSelectionAdapter {
            userPreferences.setRecentWatchlist(it.id, it.name)
            viewModel.onWatchlistSelected(it)
            findNavController().navigateUp()
        }
        with(binding.rvWatchlist) {
            layoutManager = LinearLayoutManager(requireContext())
            setupRecyclerViewDivider(R.drawable.thin_divider, countOfLastItemsWithoutDivider = 0)
            adapter = watchlistAdapter
        }
    }

    private fun handleViewState(viewState: ViewState) {
        if (viewState.isLoading) {
            binding.pbWatchlist.show()
            binding.rvWatchlist.visibility = View.INVISIBLE
        } else {
            binding.pbWatchlist.hide()
            binding.rvWatchlist.visibility = View.VISIBLE
        }
        viewState.watchlistRows?.let { watchlistAdapter.items = it }
        viewState.retryAction?.let { retry ->
            snackBarUtil.retrySnackbar { retry() }
        }
    }
}
