package com.etrade.mobilepro.inboxmessages.dynamicui.bannerwidget

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

class BannerWidgetViewFactory @Inject constructor(
    val context: Context,
    val inboxMessagesRepository: InboxMessagesRepository
) : DynamicUiViewFactory {
    val logger: Logger = LoggerFactory.getLogger(BannerWidgetViewFactory::class.java)

    override fun createView(dto: Any, meta: List<Any>): GenericLayout =
        if (dto is BannerWidgetViewDto) {
            object : GenericLayout {
                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    BannerWidgetViewModel(dto, inboxMessagesRepository, context)
                }
                override val viewModelClass: Class<out DynamicViewModel> =
                    BannerWidgetViewModel::class.java

                override val uniqueIdentifier: String = "${javaClass.name}_${dto.type}"
            }
        } else {
            throw DynamicUiViewFactoryException("BannerWidgetViewFactory registered to unknown type")
        }
}
