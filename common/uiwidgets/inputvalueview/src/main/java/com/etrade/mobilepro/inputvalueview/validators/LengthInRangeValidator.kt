package com.etrade.mobilepro.inputvalueview.validators

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.inputvalueview.InputFieldManager.Value

/**
 * Is used to verify the value length is in the specified range
 * @see IntRange range can be defined using the operator `..`
 * @sample
 * [LengthInRangeValidator]
 * LengthInRangeValidator(1..32, resources.getString(R.string.error_code)
 *
 * @param range Int range that the value length should match
 */
class LengthInRangeValidator(private val range: IntRange, private val validationErrorDesc: String) : InputFieldManager.Validator {
    override fun validate(value: String): Value {
        require(!range.isEmpty()) { "Range is empty" }
        return if (value.length in range) {
            Value.Valid(value)
        } else {
            Value.Invalid(value, validationErrorDesc)
        }
    }
}
