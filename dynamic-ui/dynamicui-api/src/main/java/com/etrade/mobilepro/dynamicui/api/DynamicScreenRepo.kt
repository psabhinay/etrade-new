package com.etrade.mobilepro.dynamicui.api

import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable

interface DynamicScreenRepo {
    fun getScreen(request: ScreenRequest, showCachedData: Boolean = true, cacheExpiration: Long? = null): Observable<Resource<List<GenericLayout>>>
}
