package com.etrade.mobilepro.alerts.data.dto

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root

@Root(strict = false, name = "SetAlerts")
class SetAlertResponseXmlDto(
    @field:Element(name = "haserror", required = false)
    var hasError: String? = null,
    @field:Element(name = "errormessage", required = false)
    var errorMessage: String? = null,
    @field:Element(name = "hasalerts", required = false)
    var hasAlerts: String? = null,
    @field:ElementList(name = "AlertsDetails", required = false)
    var alerts: List<SetAlertDetailXmlDto?>? = null
)

@Root(strict = false, name = "Alert")
class SetAlertDetailXmlDto(
    @field:Element(name = "altDelete", required = false)
    var delete: String? = null,
    @field:Element(name = "lastedited", required = false)
    var lastEdited: String? = null,
    @field:Element(name = "altsymbol", required = false)
    var symbol: String? = null,
    @field:Element(name = "deleteinfo", required = false)
    var deleteInfo: String? = null,
    @field:Element(name = "altcontent", required = false)
    var content: String? = null,
    @field:Element(name = "altedit", required = false)
    var edit: String? = null,
    @field:Element(name = "altpeh", required = false)
    var peTargetHigh: String? = null,
    @field:Element(name = "altpehstatus", required = false)
    var peTargetHighStatus: String? = null,
    @field:Element(name = "altpvl", required = false)
    var targetPriceLow: String? = null,
    @field:Element(name = "altpvlstatus", required = false)
    var targetPriceLowStatus: String? = null,
    @field:Element(name = "altpch", required = false)
    var dailyPercentHigh: String? = null,
    @field:Element(name = "altpchstatus", required = false)
    var dailyPercentHighStatus: String? = null,
    @field:Element(name = "altpvh", required = false)
    var targetPriceHigh: String? = null,
    @field:Element(name = "altpvhstatus", required = false)
    var targetPriceHighStatus: String? = null,
    @field:Element(name = "altpel", required = false)
    var peTargetLow: String? = null,
    @field:Element(name = "altpelstatus", required = false)
    var peTargetLowStatus: String? = null,
    @field:Element(name = "altvol", required = false)
    var dailyVolume: String? = null,
    @field:Element(name = "altvolstatus", required = false)
    var dailyVolumeStatus: String? = null,
    @field:Element(name = "altpcl", required = false)
    var dailyPercentLow: String? = null,
    @field:Element(name = "altpclstatus", required = false)
    var dailyPercentLowStatus: String? = null
)
