package io.realm;


import android.annotation.TargetApi;
import android.os.Build;
import android.util.JsonReader;
import android.util.JsonToken;
import io.realm.ImportFlag;
import io.realm.ProxyUtils;
import io.realm.exceptions.RealmMigrationNeededException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsList;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.Property;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.Row;
import io.realm.internal.Table;
import io.realm.internal.android.JsonUtils;
import io.realm.internal.objectstore.OsObjectBuilder;
import io.realm.log.RealmLog;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("all")
public class com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy extends com.etrade.mobilepro.positions.dao.PositionRealmObject
    implements RealmObjectProxy, com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface {

    static final class PositionRealmObjectColumnInfo extends ColumnInfo {
        long positionIdColKey;
        long accountUuidColKey;
        long accountIdColKey;
        long pricePaidRawColKey;
        long basisPriceColKey;
        long symbolColKey;
        long commissionRawColKey;
        long quantityRawColKey;
        long displayQuantityRawColKey;
        long feesRawColKey;
        long todayQuantityColKey;
        long todayPricePaidColKey;
        long todayCommissionsColKey;
        long daysGainValueColKey;
        long daysGainPercentageRawColKey;
        long totalGainValueColKey;
        long totalGainPercentageRawColKey;
        long todaysCloseColKey;
        long daysQuantityColKey;
        long daysPurchaseColKey;
        long hasLotsColKey;
        long inTheMoneyFlagColKey;
        long optionUnderlierColKey;
        long strikePriceColKey;
        long markToMarketColKey;
        long marketValueRawColKey;
        long baseSymbolPriceColKey;

        PositionRealmObjectColumnInfo(OsSchemaInfo schemaInfo) {
            super(27);
            OsObjectSchemaInfo objectSchemaInfo = schemaInfo.getObjectSchemaInfo("PositionRealmObject");
            this.positionIdColKey = addColumnDetails("positionId", "positionId", objectSchemaInfo);
            this.accountUuidColKey = addColumnDetails("accountUuid", "accountUuid", objectSchemaInfo);
            this.accountIdColKey = addColumnDetails("accountId", "accountId", objectSchemaInfo);
            this.pricePaidRawColKey = addColumnDetails("pricePaidRaw", "pricePaidRaw", objectSchemaInfo);
            this.basisPriceColKey = addColumnDetails("basisPrice", "basisPrice", objectSchemaInfo);
            this.symbolColKey = addColumnDetails("symbol", "symbol", objectSchemaInfo);
            this.commissionRawColKey = addColumnDetails("commissionRaw", "commissionRaw", objectSchemaInfo);
            this.quantityRawColKey = addColumnDetails("quantityRaw", "quantityRaw", objectSchemaInfo);
            this.displayQuantityRawColKey = addColumnDetails("displayQuantityRaw", "displayQuantityRaw", objectSchemaInfo);
            this.feesRawColKey = addColumnDetails("feesRaw", "feesRaw", objectSchemaInfo);
            this.todayQuantityColKey = addColumnDetails("todayQuantity", "todayQuantity", objectSchemaInfo);
            this.todayPricePaidColKey = addColumnDetails("todayPricePaid", "todayPricePaid", objectSchemaInfo);
            this.todayCommissionsColKey = addColumnDetails("todayCommissions", "todayCommissions", objectSchemaInfo);
            this.daysGainValueColKey = addColumnDetails("daysGainValue", "daysGainValue", objectSchemaInfo);
            this.daysGainPercentageRawColKey = addColumnDetails("daysGainPercentageRaw", "daysGainPercentageRaw", objectSchemaInfo);
            this.totalGainValueColKey = addColumnDetails("totalGainValue", "totalGainValue", objectSchemaInfo);
            this.totalGainPercentageRawColKey = addColumnDetails("totalGainPercentageRaw", "totalGainPercentageRaw", objectSchemaInfo);
            this.todaysCloseColKey = addColumnDetails("todaysClose", "todaysClose", objectSchemaInfo);
            this.daysQuantityColKey = addColumnDetails("daysQuantity", "daysQuantity", objectSchemaInfo);
            this.daysPurchaseColKey = addColumnDetails("daysPurchase", "daysPurchase", objectSchemaInfo);
            this.hasLotsColKey = addColumnDetails("hasLots", "hasLots", objectSchemaInfo);
            this.inTheMoneyFlagColKey = addColumnDetails("inTheMoneyFlag", "inTheMoneyFlag", objectSchemaInfo);
            this.optionUnderlierColKey = addColumnDetails("optionUnderlier", "optionUnderlier", objectSchemaInfo);
            this.strikePriceColKey = addColumnDetails("strikePrice", "strikePrice", objectSchemaInfo);
            this.markToMarketColKey = addColumnDetails("markToMarket", "markToMarket", objectSchemaInfo);
            this.marketValueRawColKey = addColumnDetails("marketValueRaw", "marketValueRaw", objectSchemaInfo);
            this.baseSymbolPriceColKey = addColumnDetails("baseSymbolPrice", "baseSymbolPrice", objectSchemaInfo);
        }

        PositionRealmObjectColumnInfo(ColumnInfo src, boolean mutable) {
            super(src, mutable);
            copy(src, this);
        }

        @Override
        protected final ColumnInfo copy(boolean mutable) {
            return new PositionRealmObjectColumnInfo(this, mutable);
        }

        @Override
        protected final void copy(ColumnInfo rawSrc, ColumnInfo rawDst) {
            final PositionRealmObjectColumnInfo src = (PositionRealmObjectColumnInfo) rawSrc;
            final PositionRealmObjectColumnInfo dst = (PositionRealmObjectColumnInfo) rawDst;
            dst.positionIdColKey = src.positionIdColKey;
            dst.accountUuidColKey = src.accountUuidColKey;
            dst.accountIdColKey = src.accountIdColKey;
            dst.pricePaidRawColKey = src.pricePaidRawColKey;
            dst.basisPriceColKey = src.basisPriceColKey;
            dst.symbolColKey = src.symbolColKey;
            dst.commissionRawColKey = src.commissionRawColKey;
            dst.quantityRawColKey = src.quantityRawColKey;
            dst.displayQuantityRawColKey = src.displayQuantityRawColKey;
            dst.feesRawColKey = src.feesRawColKey;
            dst.todayQuantityColKey = src.todayQuantityColKey;
            dst.todayPricePaidColKey = src.todayPricePaidColKey;
            dst.todayCommissionsColKey = src.todayCommissionsColKey;
            dst.daysGainValueColKey = src.daysGainValueColKey;
            dst.daysGainPercentageRawColKey = src.daysGainPercentageRawColKey;
            dst.totalGainValueColKey = src.totalGainValueColKey;
            dst.totalGainPercentageRawColKey = src.totalGainPercentageRawColKey;
            dst.todaysCloseColKey = src.todaysCloseColKey;
            dst.daysQuantityColKey = src.daysQuantityColKey;
            dst.daysPurchaseColKey = src.daysPurchaseColKey;
            dst.hasLotsColKey = src.hasLotsColKey;
            dst.inTheMoneyFlagColKey = src.inTheMoneyFlagColKey;
            dst.optionUnderlierColKey = src.optionUnderlierColKey;
            dst.strikePriceColKey = src.strikePriceColKey;
            dst.markToMarketColKey = src.markToMarketColKey;
            dst.marketValueRawColKey = src.marketValueRawColKey;
            dst.baseSymbolPriceColKey = src.baseSymbolPriceColKey;
        }
    }

    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();

    private PositionRealmObjectColumnInfo columnInfo;
    private ProxyState<com.etrade.mobilepro.positions.dao.PositionRealmObject> proxyState;

    com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy() {
        proxyState.setConstructionFinished();
    }

    @Override
    public void realm$injectObjectContext() {
        if (this.proxyState != null) {
            return;
        }
        final BaseRealm.RealmObjectContext context = BaseRealm.objectContext.get();
        this.columnInfo = (PositionRealmObjectColumnInfo) context.getColumnInfo();
        this.proxyState = new ProxyState<com.etrade.mobilepro.positions.dao.PositionRealmObject>(this);
        proxyState.setRealm$realm(context.getRealm());
        proxyState.setRow$realm(context.getRow());
        proxyState.setAcceptDefaultValue$realm(context.getAcceptDefaultValue());
        proxyState.setExcludeFields$realm(context.getExcludeFields());
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$positionId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.positionIdColKey);
    }

    @Override
    public void realmSet$positionId(String value) {
        if (proxyState.isUnderConstruction()) {
            // default value of the primary key is always ignored.
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        throw new io.realm.exceptions.RealmException("Primary key field 'positionId' cannot be changed after object was created.");
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$accountUuid() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.accountUuidColKey);
    }

    @Override
    public void realmSet$accountUuid(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'accountUuid' to null.");
            }
            row.getTable().setString(columnInfo.accountUuidColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'accountUuid' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.accountUuidColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$accountId() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.accountIdColKey);
    }

    @Override
    public void realmSet$accountId(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'accountId' to null.");
            }
            row.getTable().setString(columnInfo.accountIdColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'accountId' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.accountIdColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$pricePaidRaw() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.pricePaidRawColKey);
    }

    @Override
    public void realmSet$pricePaidRaw(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'pricePaidRaw' to null.");
            }
            row.getTable().setString(columnInfo.pricePaidRawColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'pricePaidRaw' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.pricePaidRawColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$basisPrice() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.basisPriceColKey);
    }

    @Override
    public void realmSet$basisPrice(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'basisPrice' to null.");
            }
            row.getTable().setString(columnInfo.basisPriceColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'basisPrice' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.basisPriceColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$symbol() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.symbolColKey);
    }

    @Override
    public void realmSet$symbol(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'symbol' to null.");
            }
            row.getTable().setString(columnInfo.symbolColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'symbol' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.symbolColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$commissionRaw() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.commissionRawColKey);
    }

    @Override
    public void realmSet$commissionRaw(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'commissionRaw' to null.");
            }
            row.getTable().setString(columnInfo.commissionRawColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'commissionRaw' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.commissionRawColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$quantityRaw() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.quantityRawColKey);
    }

    @Override
    public void realmSet$quantityRaw(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'quantityRaw' to null.");
            }
            row.getTable().setString(columnInfo.quantityRawColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'quantityRaw' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.quantityRawColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$displayQuantityRaw() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.displayQuantityRawColKey);
    }

    @Override
    public void realmSet$displayQuantityRaw(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'displayQuantityRaw' to null.");
            }
            row.getTable().setString(columnInfo.displayQuantityRawColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'displayQuantityRaw' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.displayQuantityRawColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$feesRaw() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.feesRawColKey);
    }

    @Override
    public void realmSet$feesRaw(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'feesRaw' to null.");
            }
            row.getTable().setString(columnInfo.feesRawColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'feesRaw' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.feesRawColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$todayQuantity() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.todayQuantityColKey);
    }

    @Override
    public void realmSet$todayQuantity(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'todayQuantity' to null.");
            }
            row.getTable().setString(columnInfo.todayQuantityColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'todayQuantity' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.todayQuantityColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$todayPricePaid() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.todayPricePaidColKey);
    }

    @Override
    public void realmSet$todayPricePaid(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'todayPricePaid' to null.");
            }
            row.getTable().setString(columnInfo.todayPricePaidColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'todayPricePaid' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.todayPricePaidColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$todayCommissions() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.todayCommissionsColKey);
    }

    @Override
    public void realmSet$todayCommissions(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'todayCommissions' to null.");
            }
            row.getTable().setString(columnInfo.todayCommissionsColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'todayCommissions' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.todayCommissionsColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$daysGainValue() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.daysGainValueColKey);
    }

    @Override
    public void realmSet$daysGainValue(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'daysGainValue' to null.");
            }
            row.getTable().setString(columnInfo.daysGainValueColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'daysGainValue' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.daysGainValueColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$daysGainPercentageRaw() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.daysGainPercentageRawColKey);
    }

    @Override
    public void realmSet$daysGainPercentageRaw(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'daysGainPercentageRaw' to null.");
            }
            row.getTable().setString(columnInfo.daysGainPercentageRawColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'daysGainPercentageRaw' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.daysGainPercentageRawColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$totalGainValue() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.totalGainValueColKey);
    }

    @Override
    public void realmSet$totalGainValue(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalGainValue' to null.");
            }
            row.getTable().setString(columnInfo.totalGainValueColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'totalGainValue' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.totalGainValueColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$totalGainPercentageRaw() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.totalGainPercentageRawColKey);
    }

    @Override
    public void realmSet$totalGainPercentageRaw(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'totalGainPercentageRaw' to null.");
            }
            row.getTable().setString(columnInfo.totalGainPercentageRawColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'totalGainPercentageRaw' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.totalGainPercentageRawColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$todaysClose() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.todaysCloseColKey);
    }

    @Override
    public void realmSet$todaysClose(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'todaysClose' to null.");
            }
            row.getTable().setString(columnInfo.todaysCloseColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'todaysClose' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.todaysCloseColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$daysQuantity() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.daysQuantityColKey);
    }

    @Override
    public void realmSet$daysQuantity(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.daysQuantityColKey, row.getObjectKey(), true);
                return;
            }
            row.getTable().setString(columnInfo.daysQuantityColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.daysQuantityColKey);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.daysQuantityColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public boolean realmGet$daysPurchase() {
        proxyState.getRealm$realm().checkIfValid();
        return (boolean) proxyState.getRow$realm().getBoolean(columnInfo.daysPurchaseColKey);
    }

    @Override
    public void realmSet$daysPurchase(boolean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setBoolean(columnInfo.daysPurchaseColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setBoolean(columnInfo.daysPurchaseColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public boolean realmGet$hasLots() {
        proxyState.getRealm$realm().checkIfValid();
        return (boolean) proxyState.getRow$realm().getBoolean(columnInfo.hasLotsColKey);
    }

    @Override
    public void realmSet$hasLots(boolean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            row.getTable().setBoolean(columnInfo.hasLotsColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        proxyState.getRow$realm().setBoolean(columnInfo.hasLotsColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public Boolean realmGet$inTheMoneyFlag() {
        proxyState.getRealm$realm().checkIfValid();
        if (proxyState.getRow$realm().isNull(columnInfo.inTheMoneyFlagColKey)) {
            return null;
        }
        return (boolean) proxyState.getRow$realm().getBoolean(columnInfo.inTheMoneyFlagColKey);
    }

    @Override
    public void realmSet$inTheMoneyFlag(Boolean value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.inTheMoneyFlagColKey, row.getObjectKey(), true);
                return;
            }
            row.getTable().setBoolean(columnInfo.inTheMoneyFlagColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.inTheMoneyFlagColKey);
            return;
        }
        proxyState.getRow$realm().setBoolean(columnInfo.inTheMoneyFlagColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$optionUnderlier() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.optionUnderlierColKey);
    }

    @Override
    public void realmSet$optionUnderlier(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'optionUnderlier' to null.");
            }
            row.getTable().setString(columnInfo.optionUnderlierColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'optionUnderlier' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.optionUnderlierColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$strikePrice() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.strikePriceColKey);
    }

    @Override
    public void realmSet$strikePrice(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'strikePrice' to null.");
            }
            row.getTable().setString(columnInfo.strikePriceColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'strikePrice' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.strikePriceColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$markToMarket() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.markToMarketColKey);
    }

    @Override
    public void realmSet$markToMarket(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'markToMarket' to null.");
            }
            row.getTable().setString(columnInfo.markToMarketColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'markToMarket' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.markToMarketColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$marketValueRaw() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.marketValueRawColKey);
    }

    @Override
    public void realmSet$marketValueRaw(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'marketValueRaw' to null.");
            }
            row.getTable().setString(columnInfo.marketValueRawColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            throw new IllegalArgumentException("Trying to set non-nullable field 'marketValueRaw' to null.");
        }
        proxyState.getRow$realm().setString(columnInfo.marketValueRawColKey, value);
    }

    @Override
    @SuppressWarnings("cast")
    public String realmGet$baseSymbolPrice() {
        proxyState.getRealm$realm().checkIfValid();
        return (java.lang.String) proxyState.getRow$realm().getString(columnInfo.baseSymbolPriceColKey);
    }

    @Override
    public void realmSet$baseSymbolPrice(String value) {
        if (proxyState.isUnderConstruction()) {
            if (!proxyState.getAcceptDefaultValue$realm()) {
                return;
            }
            final Row row = proxyState.getRow$realm();
            if (value == null) {
                row.getTable().setNull(columnInfo.baseSymbolPriceColKey, row.getObjectKey(), true);
                return;
            }
            row.getTable().setString(columnInfo.baseSymbolPriceColKey, row.getObjectKey(), value, true);
            return;
        }

        proxyState.getRealm$realm().checkIfValid();
        if (value == null) {
            proxyState.getRow$realm().setNull(columnInfo.baseSymbolPriceColKey);
            return;
        }
        proxyState.getRow$realm().setString(columnInfo.baseSymbolPriceColKey, value);
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        OsObjectSchemaInfo.Builder builder = new OsObjectSchemaInfo.Builder("PositionRealmObject", 27, 0);
        builder.addPersistedProperty("positionId", RealmFieldType.STRING, Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("accountUuid", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("accountId", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("pricePaidRaw", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("basisPrice", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("symbol", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("commissionRaw", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("quantityRaw", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("displayQuantityRaw", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("feesRaw", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("todayQuantity", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("todayPricePaid", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("todayCommissions", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("daysGainValue", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("daysGainPercentageRaw", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("totalGainValue", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("totalGainPercentageRaw", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("todaysClose", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("daysQuantity", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("daysPurchase", RealmFieldType.BOOLEAN, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("hasLots", RealmFieldType.BOOLEAN, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("inTheMoneyFlag", RealmFieldType.BOOLEAN, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        builder.addPersistedProperty("optionUnderlier", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("strikePrice", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("markToMarket", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("marketValueRaw", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, Property.REQUIRED);
        builder.addPersistedProperty("baseSymbolPrice", RealmFieldType.STRING, !Property.PRIMARY_KEY, !Property.INDEXED, !Property.REQUIRED);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static PositionRealmObjectColumnInfo createColumnInfo(OsSchemaInfo schemaInfo) {
        return new PositionRealmObjectColumnInfo(schemaInfo);
    }

    public static String getSimpleClassName() {
        return "PositionRealmObject";
    }

    public static final class ClassNameHelper {
        public static final String INTERNAL_CLASS_NAME = "PositionRealmObject";
    }

    @SuppressWarnings("cast")
    public static com.etrade.mobilepro.positions.dao.PositionRealmObject createOrUpdateUsingJsonObject(Realm realm, JSONObject json, boolean update)
        throws JSONException {
        final List<String> excludeFields = Collections.<String> emptyList();
        com.etrade.mobilepro.positions.dao.PositionRealmObject obj = null;
        if (update) {
            Table table = realm.getTable(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
            PositionRealmObjectColumnInfo columnInfo = (PositionRealmObjectColumnInfo) realm.getSchema().getColumnInfo(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
            long pkColumnKey = columnInfo.positionIdColKey;
            long colKey = Table.NO_MATCH;
            if (!json.isNull("positionId")) {
                colKey = table.findFirstString(pkColumnKey, json.getString("positionId"));
            }
            if (colKey != Table.NO_MATCH) {
                final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
                try {
                    objectContext.set(realm, table.getUncheckedRow(colKey), realm.getSchema().getColumnInfo(com.etrade.mobilepro.positions.dao.PositionRealmObject.class), false, Collections.<String> emptyList());
                    obj = new io.realm.com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy();
                } finally {
                    objectContext.clear();
                }
            }
        }
        if (obj == null) {
            if (json.has("positionId")) {
                if (json.isNull("positionId")) {
                    obj = (io.realm.com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy) realm.createObjectInternal(com.etrade.mobilepro.positions.dao.PositionRealmObject.class, null, true, excludeFields);
                } else {
                    obj = (io.realm.com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy) realm.createObjectInternal(com.etrade.mobilepro.positions.dao.PositionRealmObject.class, json.getString("positionId"), true, excludeFields);
                }
            } else {
                throw new IllegalArgumentException("JSON object doesn't have the primary key field 'positionId'.");
            }
        }

        final com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface objProxy = (com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) obj;
        if (json.has("accountUuid")) {
            if (json.isNull("accountUuid")) {
                objProxy.realmSet$accountUuid(null);
            } else {
                objProxy.realmSet$accountUuid((String) json.getString("accountUuid"));
            }
        }
        if (json.has("accountId")) {
            if (json.isNull("accountId")) {
                objProxy.realmSet$accountId(null);
            } else {
                objProxy.realmSet$accountId((String) json.getString("accountId"));
            }
        }
        if (json.has("pricePaidRaw")) {
            if (json.isNull("pricePaidRaw")) {
                objProxy.realmSet$pricePaidRaw(null);
            } else {
                objProxy.realmSet$pricePaidRaw((String) json.getString("pricePaidRaw"));
            }
        }
        if (json.has("basisPrice")) {
            if (json.isNull("basisPrice")) {
                objProxy.realmSet$basisPrice(null);
            } else {
                objProxy.realmSet$basisPrice((String) json.getString("basisPrice"));
            }
        }
        if (json.has("symbol")) {
            if (json.isNull("symbol")) {
                objProxy.realmSet$symbol(null);
            } else {
                objProxy.realmSet$symbol((String) json.getString("symbol"));
            }
        }
        if (json.has("commissionRaw")) {
            if (json.isNull("commissionRaw")) {
                objProxy.realmSet$commissionRaw(null);
            } else {
                objProxy.realmSet$commissionRaw((String) json.getString("commissionRaw"));
            }
        }
        if (json.has("quantityRaw")) {
            if (json.isNull("quantityRaw")) {
                objProxy.realmSet$quantityRaw(null);
            } else {
                objProxy.realmSet$quantityRaw((String) json.getString("quantityRaw"));
            }
        }
        if (json.has("displayQuantityRaw")) {
            if (json.isNull("displayQuantityRaw")) {
                objProxy.realmSet$displayQuantityRaw(null);
            } else {
                objProxy.realmSet$displayQuantityRaw((String) json.getString("displayQuantityRaw"));
            }
        }
        if (json.has("feesRaw")) {
            if (json.isNull("feesRaw")) {
                objProxy.realmSet$feesRaw(null);
            } else {
                objProxy.realmSet$feesRaw((String) json.getString("feesRaw"));
            }
        }
        if (json.has("todayQuantity")) {
            if (json.isNull("todayQuantity")) {
                objProxy.realmSet$todayQuantity(null);
            } else {
                objProxy.realmSet$todayQuantity((String) json.getString("todayQuantity"));
            }
        }
        if (json.has("todayPricePaid")) {
            if (json.isNull("todayPricePaid")) {
                objProxy.realmSet$todayPricePaid(null);
            } else {
                objProxy.realmSet$todayPricePaid((String) json.getString("todayPricePaid"));
            }
        }
        if (json.has("todayCommissions")) {
            if (json.isNull("todayCommissions")) {
                objProxy.realmSet$todayCommissions(null);
            } else {
                objProxy.realmSet$todayCommissions((String) json.getString("todayCommissions"));
            }
        }
        if (json.has("daysGainValue")) {
            if (json.isNull("daysGainValue")) {
                objProxy.realmSet$daysGainValue(null);
            } else {
                objProxy.realmSet$daysGainValue((String) json.getString("daysGainValue"));
            }
        }
        if (json.has("daysGainPercentageRaw")) {
            if (json.isNull("daysGainPercentageRaw")) {
                objProxy.realmSet$daysGainPercentageRaw(null);
            } else {
                objProxy.realmSet$daysGainPercentageRaw((String) json.getString("daysGainPercentageRaw"));
            }
        }
        if (json.has("totalGainValue")) {
            if (json.isNull("totalGainValue")) {
                objProxy.realmSet$totalGainValue(null);
            } else {
                objProxy.realmSet$totalGainValue((String) json.getString("totalGainValue"));
            }
        }
        if (json.has("totalGainPercentageRaw")) {
            if (json.isNull("totalGainPercentageRaw")) {
                objProxy.realmSet$totalGainPercentageRaw(null);
            } else {
                objProxy.realmSet$totalGainPercentageRaw((String) json.getString("totalGainPercentageRaw"));
            }
        }
        if (json.has("todaysClose")) {
            if (json.isNull("todaysClose")) {
                objProxy.realmSet$todaysClose(null);
            } else {
                objProxy.realmSet$todaysClose((String) json.getString("todaysClose"));
            }
        }
        if (json.has("daysQuantity")) {
            if (json.isNull("daysQuantity")) {
                objProxy.realmSet$daysQuantity(null);
            } else {
                objProxy.realmSet$daysQuantity((String) json.getString("daysQuantity"));
            }
        }
        if (json.has("daysPurchase")) {
            if (json.isNull("daysPurchase")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'daysPurchase' to null.");
            } else {
                objProxy.realmSet$daysPurchase((boolean) json.getBoolean("daysPurchase"));
            }
        }
        if (json.has("hasLots")) {
            if (json.isNull("hasLots")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'hasLots' to null.");
            } else {
                objProxy.realmSet$hasLots((boolean) json.getBoolean("hasLots"));
            }
        }
        if (json.has("inTheMoneyFlag")) {
            if (json.isNull("inTheMoneyFlag")) {
                objProxy.realmSet$inTheMoneyFlag(null);
            } else {
                objProxy.realmSet$inTheMoneyFlag((boolean) json.getBoolean("inTheMoneyFlag"));
            }
        }
        if (json.has("optionUnderlier")) {
            if (json.isNull("optionUnderlier")) {
                objProxy.realmSet$optionUnderlier(null);
            } else {
                objProxy.realmSet$optionUnderlier((String) json.getString("optionUnderlier"));
            }
        }
        if (json.has("strikePrice")) {
            if (json.isNull("strikePrice")) {
                objProxy.realmSet$strikePrice(null);
            } else {
                objProxy.realmSet$strikePrice((String) json.getString("strikePrice"));
            }
        }
        if (json.has("markToMarket")) {
            if (json.isNull("markToMarket")) {
                objProxy.realmSet$markToMarket(null);
            } else {
                objProxy.realmSet$markToMarket((String) json.getString("markToMarket"));
            }
        }
        if (json.has("marketValueRaw")) {
            if (json.isNull("marketValueRaw")) {
                objProxy.realmSet$marketValueRaw(null);
            } else {
                objProxy.realmSet$marketValueRaw((String) json.getString("marketValueRaw"));
            }
        }
        if (json.has("baseSymbolPrice")) {
            if (json.isNull("baseSymbolPrice")) {
                objProxy.realmSet$baseSymbolPrice(null);
            } else {
                objProxy.realmSet$baseSymbolPrice((String) json.getString("baseSymbolPrice"));
            }
        }
        return obj;
    }

    @SuppressWarnings("cast")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static com.etrade.mobilepro.positions.dao.PositionRealmObject createUsingJsonStream(Realm realm, JsonReader reader)
        throws IOException {
        boolean jsonHasPrimaryKey = false;
        final com.etrade.mobilepro.positions.dao.PositionRealmObject obj = new com.etrade.mobilepro.positions.dao.PositionRealmObject();
        final com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface objProxy = (com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) obj;
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (false) {
            } else if (name.equals("positionId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$positionId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$positionId(null);
                }
                jsonHasPrimaryKey = true;
            } else if (name.equals("accountUuid")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$accountUuid((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$accountUuid(null);
                }
            } else if (name.equals("accountId")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$accountId((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$accountId(null);
                }
            } else if (name.equals("pricePaidRaw")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$pricePaidRaw((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$pricePaidRaw(null);
                }
            } else if (name.equals("basisPrice")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$basisPrice((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$basisPrice(null);
                }
            } else if (name.equals("symbol")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$symbol((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$symbol(null);
                }
            } else if (name.equals("commissionRaw")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$commissionRaw((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$commissionRaw(null);
                }
            } else if (name.equals("quantityRaw")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$quantityRaw((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$quantityRaw(null);
                }
            } else if (name.equals("displayQuantityRaw")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$displayQuantityRaw((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$displayQuantityRaw(null);
                }
            } else if (name.equals("feesRaw")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$feesRaw((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$feesRaw(null);
                }
            } else if (name.equals("todayQuantity")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$todayQuantity((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$todayQuantity(null);
                }
            } else if (name.equals("todayPricePaid")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$todayPricePaid((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$todayPricePaid(null);
                }
            } else if (name.equals("todayCommissions")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$todayCommissions((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$todayCommissions(null);
                }
            } else if (name.equals("daysGainValue")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$daysGainValue((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$daysGainValue(null);
                }
            } else if (name.equals("daysGainPercentageRaw")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$daysGainPercentageRaw((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$daysGainPercentageRaw(null);
                }
            } else if (name.equals("totalGainValue")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$totalGainValue((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$totalGainValue(null);
                }
            } else if (name.equals("totalGainPercentageRaw")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$totalGainPercentageRaw((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$totalGainPercentageRaw(null);
                }
            } else if (name.equals("todaysClose")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$todaysClose((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$todaysClose(null);
                }
            } else if (name.equals("daysQuantity")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$daysQuantity((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$daysQuantity(null);
                }
            } else if (name.equals("daysPurchase")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$daysPurchase((boolean) reader.nextBoolean());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'daysPurchase' to null.");
                }
            } else if (name.equals("hasLots")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$hasLots((boolean) reader.nextBoolean());
                } else {
                    reader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'hasLots' to null.");
                }
            } else if (name.equals("inTheMoneyFlag")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$inTheMoneyFlag((boolean) reader.nextBoolean());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$inTheMoneyFlag(null);
                }
            } else if (name.equals("optionUnderlier")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$optionUnderlier((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$optionUnderlier(null);
                }
            } else if (name.equals("strikePrice")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$strikePrice((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$strikePrice(null);
                }
            } else if (name.equals("markToMarket")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$markToMarket((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$markToMarket(null);
                }
            } else if (name.equals("marketValueRaw")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$marketValueRaw((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$marketValueRaw(null);
                }
            } else if (name.equals("baseSymbolPrice")) {
                if (reader.peek() != JsonToken.NULL) {
                    objProxy.realmSet$baseSymbolPrice((String) reader.nextString());
                } else {
                    reader.skipValue();
                    objProxy.realmSet$baseSymbolPrice(null);
                }
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
        if (!jsonHasPrimaryKey) {
            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'positionId'.");
        }
        return realm.copyToRealm(obj);
    }

    private static com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy newProxyInstance(BaseRealm realm, Row row) {
        // Ignore default values to avoid creating unexpected objects from RealmModel/RealmList fields
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        objectContext.set(realm, row, realm.getSchema().getColumnInfo(com.etrade.mobilepro.positions.dao.PositionRealmObject.class), false, Collections.<String>emptyList());
        io.realm.com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy obj = new io.realm.com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy();
        objectContext.clear();
        return obj;
    }

    public static com.etrade.mobilepro.positions.dao.PositionRealmObject copyOrUpdate(Realm realm, PositionRealmObjectColumnInfo columnInfo, com.etrade.mobilepro.positions.dao.PositionRealmObject object, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null) {
            final BaseRealm otherRealm = ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm();
            if (otherRealm.threadId != realm.threadId) {
                throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
            }
            if (otherRealm.getPath().equals(realm.getPath())) {
                return object;
            }
        }
        final BaseRealm.RealmObjectContext objectContext = BaseRealm.objectContext.get();
        RealmObjectProxy cachedRealmObject = cache.get(object);
        if (cachedRealmObject != null) {
            return (com.etrade.mobilepro.positions.dao.PositionRealmObject) cachedRealmObject;
        }

        com.etrade.mobilepro.positions.dao.PositionRealmObject realmObject = null;
        boolean canUpdate = update;
        if (canUpdate) {
            Table table = realm.getTable(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
            long pkColumnKey = columnInfo.positionIdColKey;
            long colKey = table.findFirstString(pkColumnKey, ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$positionId());
            if (colKey == Table.NO_MATCH) {
                canUpdate = false;
            } else {
                try {
                    objectContext.set(realm, table.getUncheckedRow(colKey), columnInfo, false, Collections.<String> emptyList());
                    realmObject = new io.realm.com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy();
                    cache.put(object, (RealmObjectProxy) realmObject);
                } finally {
                    objectContext.clear();
                }
            }
        }

        return (canUpdate) ? update(realm, columnInfo, realmObject, object, cache, flags) : copy(realm, columnInfo, object, update, cache, flags);
    }

    public static com.etrade.mobilepro.positions.dao.PositionRealmObject copy(Realm realm, PositionRealmObjectColumnInfo columnInfo, com.etrade.mobilepro.positions.dao.PositionRealmObject newObject, boolean update, Map<RealmModel,RealmObjectProxy> cache, Set<ImportFlag> flags) {
        RealmObjectProxy cachedRealmObject = cache.get(newObject);
        if (cachedRealmObject != null) {
            return (com.etrade.mobilepro.positions.dao.PositionRealmObject) cachedRealmObject;
        }

        com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface realmObjectSource = (com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) newObject;

        Table table = realm.getTable(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, flags);

        // Add all non-"object reference" fields
        builder.addString(columnInfo.positionIdColKey, realmObjectSource.realmGet$positionId());
        builder.addString(columnInfo.accountUuidColKey, realmObjectSource.realmGet$accountUuid());
        builder.addString(columnInfo.accountIdColKey, realmObjectSource.realmGet$accountId());
        builder.addString(columnInfo.pricePaidRawColKey, realmObjectSource.realmGet$pricePaidRaw());
        builder.addString(columnInfo.basisPriceColKey, realmObjectSource.realmGet$basisPrice());
        builder.addString(columnInfo.symbolColKey, realmObjectSource.realmGet$symbol());
        builder.addString(columnInfo.commissionRawColKey, realmObjectSource.realmGet$commissionRaw());
        builder.addString(columnInfo.quantityRawColKey, realmObjectSource.realmGet$quantityRaw());
        builder.addString(columnInfo.displayQuantityRawColKey, realmObjectSource.realmGet$displayQuantityRaw());
        builder.addString(columnInfo.feesRawColKey, realmObjectSource.realmGet$feesRaw());
        builder.addString(columnInfo.todayQuantityColKey, realmObjectSource.realmGet$todayQuantity());
        builder.addString(columnInfo.todayPricePaidColKey, realmObjectSource.realmGet$todayPricePaid());
        builder.addString(columnInfo.todayCommissionsColKey, realmObjectSource.realmGet$todayCommissions());
        builder.addString(columnInfo.daysGainValueColKey, realmObjectSource.realmGet$daysGainValue());
        builder.addString(columnInfo.daysGainPercentageRawColKey, realmObjectSource.realmGet$daysGainPercentageRaw());
        builder.addString(columnInfo.totalGainValueColKey, realmObjectSource.realmGet$totalGainValue());
        builder.addString(columnInfo.totalGainPercentageRawColKey, realmObjectSource.realmGet$totalGainPercentageRaw());
        builder.addString(columnInfo.todaysCloseColKey, realmObjectSource.realmGet$todaysClose());
        builder.addString(columnInfo.daysQuantityColKey, realmObjectSource.realmGet$daysQuantity());
        builder.addBoolean(columnInfo.daysPurchaseColKey, realmObjectSource.realmGet$daysPurchase());
        builder.addBoolean(columnInfo.hasLotsColKey, realmObjectSource.realmGet$hasLots());
        builder.addBoolean(columnInfo.inTheMoneyFlagColKey, realmObjectSource.realmGet$inTheMoneyFlag());
        builder.addString(columnInfo.optionUnderlierColKey, realmObjectSource.realmGet$optionUnderlier());
        builder.addString(columnInfo.strikePriceColKey, realmObjectSource.realmGet$strikePrice());
        builder.addString(columnInfo.markToMarketColKey, realmObjectSource.realmGet$markToMarket());
        builder.addString(columnInfo.marketValueRawColKey, realmObjectSource.realmGet$marketValueRaw());
        builder.addString(columnInfo.baseSymbolPriceColKey, realmObjectSource.realmGet$baseSymbolPrice());

        // Create the underlying object and cache it before setting any object/objectlist references
        // This will allow us to break any circular dependencies by using the object cache.
        Row row = builder.createNewObject();
        io.realm.com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy realmObjectCopy = newProxyInstance(realm, row);
        cache.put(newObject, realmObjectCopy);

        return realmObjectCopy;
    }

    public static long insert(Realm realm, com.etrade.mobilepro.positions.dao.PositionRealmObject object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey();
        }
        Table table = realm.getTable(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
        long tableNativePtr = table.getNativePtr();
        PositionRealmObjectColumnInfo columnInfo = (PositionRealmObjectColumnInfo) realm.getSchema().getColumnInfo(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
        long pkColumnKey = columnInfo.positionIdColKey;
        long colKey = Table.NO_MATCH;
        Object primaryKeyValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$positionId();
        if (primaryKeyValue != null) {
            colKey = Table.nativeFindFirstString(tableNativePtr, pkColumnKey, (String)primaryKeyValue);
        }
        if (colKey == Table.NO_MATCH) {
            colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, primaryKeyValue);
        } else {
            Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
        }
        cache.put(object, colKey);
        String realmGet$accountUuid = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$accountUuid();
        if (realmGet$accountUuid != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.accountUuidColKey, colKey, realmGet$accountUuid, false);
        }
        String realmGet$accountId = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$accountId();
        if (realmGet$accountId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.accountIdColKey, colKey, realmGet$accountId, false);
        }
        String realmGet$pricePaidRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$pricePaidRaw();
        if (realmGet$pricePaidRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.pricePaidRawColKey, colKey, realmGet$pricePaidRaw, false);
        }
        String realmGet$basisPrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$basisPrice();
        if (realmGet$basisPrice != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.basisPriceColKey, colKey, realmGet$basisPrice, false);
        }
        String realmGet$symbol = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$symbol();
        if (realmGet$symbol != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.symbolColKey, colKey, realmGet$symbol, false);
        }
        String realmGet$commissionRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$commissionRaw();
        if (realmGet$commissionRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.commissionRawColKey, colKey, realmGet$commissionRaw, false);
        }
        String realmGet$quantityRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$quantityRaw();
        if (realmGet$quantityRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.quantityRawColKey, colKey, realmGet$quantityRaw, false);
        }
        String realmGet$displayQuantityRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$displayQuantityRaw();
        if (realmGet$displayQuantityRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.displayQuantityRawColKey, colKey, realmGet$displayQuantityRaw, false);
        }
        String realmGet$feesRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$feesRaw();
        if (realmGet$feesRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.feesRawColKey, colKey, realmGet$feesRaw, false);
        }
        String realmGet$todayQuantity = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayQuantity();
        if (realmGet$todayQuantity != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.todayQuantityColKey, colKey, realmGet$todayQuantity, false);
        }
        String realmGet$todayPricePaid = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayPricePaid();
        if (realmGet$todayPricePaid != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.todayPricePaidColKey, colKey, realmGet$todayPricePaid, false);
        }
        String realmGet$todayCommissions = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayCommissions();
        if (realmGet$todayCommissions != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.todayCommissionsColKey, colKey, realmGet$todayCommissions, false);
        }
        String realmGet$daysGainValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysGainValue();
        if (realmGet$daysGainValue != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.daysGainValueColKey, colKey, realmGet$daysGainValue, false);
        }
        String realmGet$daysGainPercentageRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysGainPercentageRaw();
        if (realmGet$daysGainPercentageRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.daysGainPercentageRawColKey, colKey, realmGet$daysGainPercentageRaw, false);
        }
        String realmGet$totalGainValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$totalGainValue();
        if (realmGet$totalGainValue != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.totalGainValueColKey, colKey, realmGet$totalGainValue, false);
        }
        String realmGet$totalGainPercentageRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$totalGainPercentageRaw();
        if (realmGet$totalGainPercentageRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.totalGainPercentageRawColKey, colKey, realmGet$totalGainPercentageRaw, false);
        }
        String realmGet$todaysClose = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todaysClose();
        if (realmGet$todaysClose != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.todaysCloseColKey, colKey, realmGet$todaysClose, false);
        }
        String realmGet$daysQuantity = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysQuantity();
        if (realmGet$daysQuantity != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.daysQuantityColKey, colKey, realmGet$daysQuantity, false);
        }
        Table.nativeSetBoolean(tableNativePtr, columnInfo.daysPurchaseColKey, colKey, ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysPurchase(), false);
        Table.nativeSetBoolean(tableNativePtr, columnInfo.hasLotsColKey, colKey, ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$hasLots(), false);
        Boolean realmGet$inTheMoneyFlag = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$inTheMoneyFlag();
        if (realmGet$inTheMoneyFlag != null) {
            Table.nativeSetBoolean(tableNativePtr, columnInfo.inTheMoneyFlagColKey, colKey, realmGet$inTheMoneyFlag, false);
        }
        String realmGet$optionUnderlier = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$optionUnderlier();
        if (realmGet$optionUnderlier != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.optionUnderlierColKey, colKey, realmGet$optionUnderlier, false);
        }
        String realmGet$strikePrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$strikePrice();
        if (realmGet$strikePrice != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.strikePriceColKey, colKey, realmGet$strikePrice, false);
        }
        String realmGet$markToMarket = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$markToMarket();
        if (realmGet$markToMarket != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.markToMarketColKey, colKey, realmGet$markToMarket, false);
        }
        String realmGet$marketValueRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$marketValueRaw();
        if (realmGet$marketValueRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.marketValueRawColKey, colKey, realmGet$marketValueRaw, false);
        }
        String realmGet$baseSymbolPrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$baseSymbolPrice();
        if (realmGet$baseSymbolPrice != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.baseSymbolPriceColKey, colKey, realmGet$baseSymbolPrice, false);
        }
        return colKey;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
        long tableNativePtr = table.getNativePtr();
        PositionRealmObjectColumnInfo columnInfo = (PositionRealmObjectColumnInfo) realm.getSchema().getColumnInfo(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
        long pkColumnKey = columnInfo.positionIdColKey;
        com.etrade.mobilepro.positions.dao.PositionRealmObject object = null;
        while (objects.hasNext()) {
            object = (com.etrade.mobilepro.positions.dao.PositionRealmObject) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey());
                continue;
            }
            long colKey = Table.NO_MATCH;
            Object primaryKeyValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$positionId();
            if (primaryKeyValue != null) {
                colKey = Table.nativeFindFirstString(tableNativePtr, pkColumnKey, (String)primaryKeyValue);
            }
            if (colKey == Table.NO_MATCH) {
                colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, primaryKeyValue);
            } else {
                Table.throwDuplicatePrimaryKeyException(primaryKeyValue);
            }
            cache.put(object, colKey);
            String realmGet$accountUuid = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$accountUuid();
            if (realmGet$accountUuid != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.accountUuidColKey, colKey, realmGet$accountUuid, false);
            }
            String realmGet$accountId = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$accountId();
            if (realmGet$accountId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.accountIdColKey, colKey, realmGet$accountId, false);
            }
            String realmGet$pricePaidRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$pricePaidRaw();
            if (realmGet$pricePaidRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.pricePaidRawColKey, colKey, realmGet$pricePaidRaw, false);
            }
            String realmGet$basisPrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$basisPrice();
            if (realmGet$basisPrice != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.basisPriceColKey, colKey, realmGet$basisPrice, false);
            }
            String realmGet$symbol = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$symbol();
            if (realmGet$symbol != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.symbolColKey, colKey, realmGet$symbol, false);
            }
            String realmGet$commissionRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$commissionRaw();
            if (realmGet$commissionRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.commissionRawColKey, colKey, realmGet$commissionRaw, false);
            }
            String realmGet$quantityRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$quantityRaw();
            if (realmGet$quantityRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.quantityRawColKey, colKey, realmGet$quantityRaw, false);
            }
            String realmGet$displayQuantityRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$displayQuantityRaw();
            if (realmGet$displayQuantityRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.displayQuantityRawColKey, colKey, realmGet$displayQuantityRaw, false);
            }
            String realmGet$feesRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$feesRaw();
            if (realmGet$feesRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.feesRawColKey, colKey, realmGet$feesRaw, false);
            }
            String realmGet$todayQuantity = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayQuantity();
            if (realmGet$todayQuantity != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.todayQuantityColKey, colKey, realmGet$todayQuantity, false);
            }
            String realmGet$todayPricePaid = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayPricePaid();
            if (realmGet$todayPricePaid != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.todayPricePaidColKey, colKey, realmGet$todayPricePaid, false);
            }
            String realmGet$todayCommissions = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayCommissions();
            if (realmGet$todayCommissions != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.todayCommissionsColKey, colKey, realmGet$todayCommissions, false);
            }
            String realmGet$daysGainValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysGainValue();
            if (realmGet$daysGainValue != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.daysGainValueColKey, colKey, realmGet$daysGainValue, false);
            }
            String realmGet$daysGainPercentageRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysGainPercentageRaw();
            if (realmGet$daysGainPercentageRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.daysGainPercentageRawColKey, colKey, realmGet$daysGainPercentageRaw, false);
            }
            String realmGet$totalGainValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$totalGainValue();
            if (realmGet$totalGainValue != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.totalGainValueColKey, colKey, realmGet$totalGainValue, false);
            }
            String realmGet$totalGainPercentageRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$totalGainPercentageRaw();
            if (realmGet$totalGainPercentageRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.totalGainPercentageRawColKey, colKey, realmGet$totalGainPercentageRaw, false);
            }
            String realmGet$todaysClose = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todaysClose();
            if (realmGet$todaysClose != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.todaysCloseColKey, colKey, realmGet$todaysClose, false);
            }
            String realmGet$daysQuantity = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysQuantity();
            if (realmGet$daysQuantity != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.daysQuantityColKey, colKey, realmGet$daysQuantity, false);
            }
            Table.nativeSetBoolean(tableNativePtr, columnInfo.daysPurchaseColKey, colKey, ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysPurchase(), false);
            Table.nativeSetBoolean(tableNativePtr, columnInfo.hasLotsColKey, colKey, ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$hasLots(), false);
            Boolean realmGet$inTheMoneyFlag = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$inTheMoneyFlag();
            if (realmGet$inTheMoneyFlag != null) {
                Table.nativeSetBoolean(tableNativePtr, columnInfo.inTheMoneyFlagColKey, colKey, realmGet$inTheMoneyFlag, false);
            }
            String realmGet$optionUnderlier = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$optionUnderlier();
            if (realmGet$optionUnderlier != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.optionUnderlierColKey, colKey, realmGet$optionUnderlier, false);
            }
            String realmGet$strikePrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$strikePrice();
            if (realmGet$strikePrice != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.strikePriceColKey, colKey, realmGet$strikePrice, false);
            }
            String realmGet$markToMarket = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$markToMarket();
            if (realmGet$markToMarket != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.markToMarketColKey, colKey, realmGet$markToMarket, false);
            }
            String realmGet$marketValueRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$marketValueRaw();
            if (realmGet$marketValueRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.marketValueRawColKey, colKey, realmGet$marketValueRaw, false);
            }
            String realmGet$baseSymbolPrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$baseSymbolPrice();
            if (realmGet$baseSymbolPrice != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.baseSymbolPriceColKey, colKey, realmGet$baseSymbolPrice, false);
            }
        }
    }

    public static long insertOrUpdate(Realm realm, com.etrade.mobilepro.positions.dao.PositionRealmObject object, Map<RealmModel,Long> cache) {
        if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
            return ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey();
        }
        Table table = realm.getTable(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
        long tableNativePtr = table.getNativePtr();
        PositionRealmObjectColumnInfo columnInfo = (PositionRealmObjectColumnInfo) realm.getSchema().getColumnInfo(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
        long pkColumnKey = columnInfo.positionIdColKey;
        long colKey = Table.NO_MATCH;
        Object primaryKeyValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$positionId();
        if (primaryKeyValue != null) {
            colKey = Table.nativeFindFirstString(tableNativePtr, pkColumnKey, (String)primaryKeyValue);
        }
        if (colKey == Table.NO_MATCH) {
            colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, primaryKeyValue);
        }
        cache.put(object, colKey);
        String realmGet$accountUuid = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$accountUuid();
        if (realmGet$accountUuid != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.accountUuidColKey, colKey, realmGet$accountUuid, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.accountUuidColKey, colKey, false);
        }
        String realmGet$accountId = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$accountId();
        if (realmGet$accountId != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.accountIdColKey, colKey, realmGet$accountId, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.accountIdColKey, colKey, false);
        }
        String realmGet$pricePaidRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$pricePaidRaw();
        if (realmGet$pricePaidRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.pricePaidRawColKey, colKey, realmGet$pricePaidRaw, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.pricePaidRawColKey, colKey, false);
        }
        String realmGet$basisPrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$basisPrice();
        if (realmGet$basisPrice != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.basisPriceColKey, colKey, realmGet$basisPrice, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.basisPriceColKey, colKey, false);
        }
        String realmGet$symbol = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$symbol();
        if (realmGet$symbol != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.symbolColKey, colKey, realmGet$symbol, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.symbolColKey, colKey, false);
        }
        String realmGet$commissionRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$commissionRaw();
        if (realmGet$commissionRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.commissionRawColKey, colKey, realmGet$commissionRaw, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.commissionRawColKey, colKey, false);
        }
        String realmGet$quantityRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$quantityRaw();
        if (realmGet$quantityRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.quantityRawColKey, colKey, realmGet$quantityRaw, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.quantityRawColKey, colKey, false);
        }
        String realmGet$displayQuantityRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$displayQuantityRaw();
        if (realmGet$displayQuantityRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.displayQuantityRawColKey, colKey, realmGet$displayQuantityRaw, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.displayQuantityRawColKey, colKey, false);
        }
        String realmGet$feesRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$feesRaw();
        if (realmGet$feesRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.feesRawColKey, colKey, realmGet$feesRaw, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.feesRawColKey, colKey, false);
        }
        String realmGet$todayQuantity = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayQuantity();
        if (realmGet$todayQuantity != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.todayQuantityColKey, colKey, realmGet$todayQuantity, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.todayQuantityColKey, colKey, false);
        }
        String realmGet$todayPricePaid = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayPricePaid();
        if (realmGet$todayPricePaid != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.todayPricePaidColKey, colKey, realmGet$todayPricePaid, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.todayPricePaidColKey, colKey, false);
        }
        String realmGet$todayCommissions = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayCommissions();
        if (realmGet$todayCommissions != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.todayCommissionsColKey, colKey, realmGet$todayCommissions, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.todayCommissionsColKey, colKey, false);
        }
        String realmGet$daysGainValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysGainValue();
        if (realmGet$daysGainValue != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.daysGainValueColKey, colKey, realmGet$daysGainValue, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.daysGainValueColKey, colKey, false);
        }
        String realmGet$daysGainPercentageRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysGainPercentageRaw();
        if (realmGet$daysGainPercentageRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.daysGainPercentageRawColKey, colKey, realmGet$daysGainPercentageRaw, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.daysGainPercentageRawColKey, colKey, false);
        }
        String realmGet$totalGainValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$totalGainValue();
        if (realmGet$totalGainValue != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.totalGainValueColKey, colKey, realmGet$totalGainValue, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.totalGainValueColKey, colKey, false);
        }
        String realmGet$totalGainPercentageRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$totalGainPercentageRaw();
        if (realmGet$totalGainPercentageRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.totalGainPercentageRawColKey, colKey, realmGet$totalGainPercentageRaw, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.totalGainPercentageRawColKey, colKey, false);
        }
        String realmGet$todaysClose = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todaysClose();
        if (realmGet$todaysClose != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.todaysCloseColKey, colKey, realmGet$todaysClose, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.todaysCloseColKey, colKey, false);
        }
        String realmGet$daysQuantity = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysQuantity();
        if (realmGet$daysQuantity != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.daysQuantityColKey, colKey, realmGet$daysQuantity, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.daysQuantityColKey, colKey, false);
        }
        Table.nativeSetBoolean(tableNativePtr, columnInfo.daysPurchaseColKey, colKey, ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysPurchase(), false);
        Table.nativeSetBoolean(tableNativePtr, columnInfo.hasLotsColKey, colKey, ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$hasLots(), false);
        Boolean realmGet$inTheMoneyFlag = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$inTheMoneyFlag();
        if (realmGet$inTheMoneyFlag != null) {
            Table.nativeSetBoolean(tableNativePtr, columnInfo.inTheMoneyFlagColKey, colKey, realmGet$inTheMoneyFlag, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.inTheMoneyFlagColKey, colKey, false);
        }
        String realmGet$optionUnderlier = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$optionUnderlier();
        if (realmGet$optionUnderlier != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.optionUnderlierColKey, colKey, realmGet$optionUnderlier, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.optionUnderlierColKey, colKey, false);
        }
        String realmGet$strikePrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$strikePrice();
        if (realmGet$strikePrice != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.strikePriceColKey, colKey, realmGet$strikePrice, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.strikePriceColKey, colKey, false);
        }
        String realmGet$markToMarket = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$markToMarket();
        if (realmGet$markToMarket != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.markToMarketColKey, colKey, realmGet$markToMarket, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.markToMarketColKey, colKey, false);
        }
        String realmGet$marketValueRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$marketValueRaw();
        if (realmGet$marketValueRaw != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.marketValueRawColKey, colKey, realmGet$marketValueRaw, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.marketValueRawColKey, colKey, false);
        }
        String realmGet$baseSymbolPrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$baseSymbolPrice();
        if (realmGet$baseSymbolPrice != null) {
            Table.nativeSetString(tableNativePtr, columnInfo.baseSymbolPriceColKey, colKey, realmGet$baseSymbolPrice, false);
        } else {
            Table.nativeSetNull(tableNativePtr, columnInfo.baseSymbolPriceColKey, colKey, false);
        }
        return colKey;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> objects, Map<RealmModel,Long> cache) {
        Table table = realm.getTable(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
        long tableNativePtr = table.getNativePtr();
        PositionRealmObjectColumnInfo columnInfo = (PositionRealmObjectColumnInfo) realm.getSchema().getColumnInfo(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
        long pkColumnKey = columnInfo.positionIdColKey;
        com.etrade.mobilepro.positions.dao.PositionRealmObject object = null;
        while (objects.hasNext()) {
            object = (com.etrade.mobilepro.positions.dao.PositionRealmObject) objects.next();
            if (cache.containsKey(object)) {
                continue;
            }
            if (object instanceof RealmObjectProxy && !RealmObject.isFrozen(object) && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm() != null && ((RealmObjectProxy) object).realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                cache.put(object, ((RealmObjectProxy) object).realmGet$proxyState().getRow$realm().getObjectKey());
                continue;
            }
            long colKey = Table.NO_MATCH;
            Object primaryKeyValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$positionId();
            if (primaryKeyValue != null) {
                colKey = Table.nativeFindFirstString(tableNativePtr, pkColumnKey, (String)primaryKeyValue);
            }
            if (colKey == Table.NO_MATCH) {
                colKey = OsObject.createRowWithPrimaryKey(table, pkColumnKey, primaryKeyValue);
            }
            cache.put(object, colKey);
            String realmGet$accountUuid = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$accountUuid();
            if (realmGet$accountUuid != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.accountUuidColKey, colKey, realmGet$accountUuid, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.accountUuidColKey, colKey, false);
            }
            String realmGet$accountId = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$accountId();
            if (realmGet$accountId != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.accountIdColKey, colKey, realmGet$accountId, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.accountIdColKey, colKey, false);
            }
            String realmGet$pricePaidRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$pricePaidRaw();
            if (realmGet$pricePaidRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.pricePaidRawColKey, colKey, realmGet$pricePaidRaw, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.pricePaidRawColKey, colKey, false);
            }
            String realmGet$basisPrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$basisPrice();
            if (realmGet$basisPrice != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.basisPriceColKey, colKey, realmGet$basisPrice, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.basisPriceColKey, colKey, false);
            }
            String realmGet$symbol = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$symbol();
            if (realmGet$symbol != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.symbolColKey, colKey, realmGet$symbol, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.symbolColKey, colKey, false);
            }
            String realmGet$commissionRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$commissionRaw();
            if (realmGet$commissionRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.commissionRawColKey, colKey, realmGet$commissionRaw, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.commissionRawColKey, colKey, false);
            }
            String realmGet$quantityRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$quantityRaw();
            if (realmGet$quantityRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.quantityRawColKey, colKey, realmGet$quantityRaw, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.quantityRawColKey, colKey, false);
            }
            String realmGet$displayQuantityRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$displayQuantityRaw();
            if (realmGet$displayQuantityRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.displayQuantityRawColKey, colKey, realmGet$displayQuantityRaw, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.displayQuantityRawColKey, colKey, false);
            }
            String realmGet$feesRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$feesRaw();
            if (realmGet$feesRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.feesRawColKey, colKey, realmGet$feesRaw, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.feesRawColKey, colKey, false);
            }
            String realmGet$todayQuantity = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayQuantity();
            if (realmGet$todayQuantity != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.todayQuantityColKey, colKey, realmGet$todayQuantity, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.todayQuantityColKey, colKey, false);
            }
            String realmGet$todayPricePaid = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayPricePaid();
            if (realmGet$todayPricePaid != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.todayPricePaidColKey, colKey, realmGet$todayPricePaid, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.todayPricePaidColKey, colKey, false);
            }
            String realmGet$todayCommissions = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todayCommissions();
            if (realmGet$todayCommissions != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.todayCommissionsColKey, colKey, realmGet$todayCommissions, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.todayCommissionsColKey, colKey, false);
            }
            String realmGet$daysGainValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysGainValue();
            if (realmGet$daysGainValue != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.daysGainValueColKey, colKey, realmGet$daysGainValue, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.daysGainValueColKey, colKey, false);
            }
            String realmGet$daysGainPercentageRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysGainPercentageRaw();
            if (realmGet$daysGainPercentageRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.daysGainPercentageRawColKey, colKey, realmGet$daysGainPercentageRaw, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.daysGainPercentageRawColKey, colKey, false);
            }
            String realmGet$totalGainValue = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$totalGainValue();
            if (realmGet$totalGainValue != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.totalGainValueColKey, colKey, realmGet$totalGainValue, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.totalGainValueColKey, colKey, false);
            }
            String realmGet$totalGainPercentageRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$totalGainPercentageRaw();
            if (realmGet$totalGainPercentageRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.totalGainPercentageRawColKey, colKey, realmGet$totalGainPercentageRaw, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.totalGainPercentageRawColKey, colKey, false);
            }
            String realmGet$todaysClose = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$todaysClose();
            if (realmGet$todaysClose != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.todaysCloseColKey, colKey, realmGet$todaysClose, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.todaysCloseColKey, colKey, false);
            }
            String realmGet$daysQuantity = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysQuantity();
            if (realmGet$daysQuantity != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.daysQuantityColKey, colKey, realmGet$daysQuantity, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.daysQuantityColKey, colKey, false);
            }
            Table.nativeSetBoolean(tableNativePtr, columnInfo.daysPurchaseColKey, colKey, ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$daysPurchase(), false);
            Table.nativeSetBoolean(tableNativePtr, columnInfo.hasLotsColKey, colKey, ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$hasLots(), false);
            Boolean realmGet$inTheMoneyFlag = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$inTheMoneyFlag();
            if (realmGet$inTheMoneyFlag != null) {
                Table.nativeSetBoolean(tableNativePtr, columnInfo.inTheMoneyFlagColKey, colKey, realmGet$inTheMoneyFlag, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.inTheMoneyFlagColKey, colKey, false);
            }
            String realmGet$optionUnderlier = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$optionUnderlier();
            if (realmGet$optionUnderlier != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.optionUnderlierColKey, colKey, realmGet$optionUnderlier, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.optionUnderlierColKey, colKey, false);
            }
            String realmGet$strikePrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$strikePrice();
            if (realmGet$strikePrice != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.strikePriceColKey, colKey, realmGet$strikePrice, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.strikePriceColKey, colKey, false);
            }
            String realmGet$markToMarket = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$markToMarket();
            if (realmGet$markToMarket != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.markToMarketColKey, colKey, realmGet$markToMarket, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.markToMarketColKey, colKey, false);
            }
            String realmGet$marketValueRaw = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$marketValueRaw();
            if (realmGet$marketValueRaw != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.marketValueRawColKey, colKey, realmGet$marketValueRaw, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.marketValueRawColKey, colKey, false);
            }
            String realmGet$baseSymbolPrice = ((com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) object).realmGet$baseSymbolPrice();
            if (realmGet$baseSymbolPrice != null) {
                Table.nativeSetString(tableNativePtr, columnInfo.baseSymbolPriceColKey, colKey, realmGet$baseSymbolPrice, false);
            } else {
                Table.nativeSetNull(tableNativePtr, columnInfo.baseSymbolPriceColKey, colKey, false);
            }
        }
    }

    public static com.etrade.mobilepro.positions.dao.PositionRealmObject createDetachedCopy(com.etrade.mobilepro.positions.dao.PositionRealmObject realmObject, int currentDepth, int maxDepth, Map<RealmModel, CacheData<RealmModel>> cache) {
        if (currentDepth > maxDepth || realmObject == null) {
            return null;
        }
        CacheData<RealmModel> cachedObject = cache.get(realmObject);
        com.etrade.mobilepro.positions.dao.PositionRealmObject unmanagedObject;
        if (cachedObject == null) {
            unmanagedObject = new com.etrade.mobilepro.positions.dao.PositionRealmObject();
            cache.put(realmObject, new RealmObjectProxy.CacheData<RealmModel>(currentDepth, unmanagedObject));
        } else {
            // Reuse cached object or recreate it because it was encountered at a lower depth.
            if (currentDepth >= cachedObject.minDepth) {
                return (com.etrade.mobilepro.positions.dao.PositionRealmObject) cachedObject.object;
            }
            unmanagedObject = (com.etrade.mobilepro.positions.dao.PositionRealmObject) cachedObject.object;
            cachedObject.minDepth = currentDepth;
        }
        com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface unmanagedCopy = (com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) unmanagedObject;
        com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface realmSource = (com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) realmObject;
        unmanagedCopy.realmSet$positionId(realmSource.realmGet$positionId());
        unmanagedCopy.realmSet$accountUuid(realmSource.realmGet$accountUuid());
        unmanagedCopy.realmSet$accountId(realmSource.realmGet$accountId());
        unmanagedCopy.realmSet$pricePaidRaw(realmSource.realmGet$pricePaidRaw());
        unmanagedCopy.realmSet$basisPrice(realmSource.realmGet$basisPrice());
        unmanagedCopy.realmSet$symbol(realmSource.realmGet$symbol());
        unmanagedCopy.realmSet$commissionRaw(realmSource.realmGet$commissionRaw());
        unmanagedCopy.realmSet$quantityRaw(realmSource.realmGet$quantityRaw());
        unmanagedCopy.realmSet$displayQuantityRaw(realmSource.realmGet$displayQuantityRaw());
        unmanagedCopy.realmSet$feesRaw(realmSource.realmGet$feesRaw());
        unmanagedCopy.realmSet$todayQuantity(realmSource.realmGet$todayQuantity());
        unmanagedCopy.realmSet$todayPricePaid(realmSource.realmGet$todayPricePaid());
        unmanagedCopy.realmSet$todayCommissions(realmSource.realmGet$todayCommissions());
        unmanagedCopy.realmSet$daysGainValue(realmSource.realmGet$daysGainValue());
        unmanagedCopy.realmSet$daysGainPercentageRaw(realmSource.realmGet$daysGainPercentageRaw());
        unmanagedCopy.realmSet$totalGainValue(realmSource.realmGet$totalGainValue());
        unmanagedCopy.realmSet$totalGainPercentageRaw(realmSource.realmGet$totalGainPercentageRaw());
        unmanagedCopy.realmSet$todaysClose(realmSource.realmGet$todaysClose());
        unmanagedCopy.realmSet$daysQuantity(realmSource.realmGet$daysQuantity());
        unmanagedCopy.realmSet$daysPurchase(realmSource.realmGet$daysPurchase());
        unmanagedCopy.realmSet$hasLots(realmSource.realmGet$hasLots());
        unmanagedCopy.realmSet$inTheMoneyFlag(realmSource.realmGet$inTheMoneyFlag());
        unmanagedCopy.realmSet$optionUnderlier(realmSource.realmGet$optionUnderlier());
        unmanagedCopy.realmSet$strikePrice(realmSource.realmGet$strikePrice());
        unmanagedCopy.realmSet$markToMarket(realmSource.realmGet$markToMarket());
        unmanagedCopy.realmSet$marketValueRaw(realmSource.realmGet$marketValueRaw());
        unmanagedCopy.realmSet$baseSymbolPrice(realmSource.realmGet$baseSymbolPrice());

        return unmanagedObject;
    }

    static com.etrade.mobilepro.positions.dao.PositionRealmObject update(Realm realm, PositionRealmObjectColumnInfo columnInfo, com.etrade.mobilepro.positions.dao.PositionRealmObject realmObject, com.etrade.mobilepro.positions.dao.PositionRealmObject newObject, Map<RealmModel, RealmObjectProxy> cache, Set<ImportFlag> flags) {
        com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface realmObjectTarget = (com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) realmObject;
        com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface realmObjectSource = (com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxyInterface) newObject;
        Table table = realm.getTable(com.etrade.mobilepro.positions.dao.PositionRealmObject.class);
        OsObjectBuilder builder = new OsObjectBuilder(table, flags);
        builder.addString(columnInfo.positionIdColKey, realmObjectSource.realmGet$positionId());
        builder.addString(columnInfo.accountUuidColKey, realmObjectSource.realmGet$accountUuid());
        builder.addString(columnInfo.accountIdColKey, realmObjectSource.realmGet$accountId());
        builder.addString(columnInfo.pricePaidRawColKey, realmObjectSource.realmGet$pricePaidRaw());
        builder.addString(columnInfo.basisPriceColKey, realmObjectSource.realmGet$basisPrice());
        builder.addString(columnInfo.symbolColKey, realmObjectSource.realmGet$symbol());
        builder.addString(columnInfo.commissionRawColKey, realmObjectSource.realmGet$commissionRaw());
        builder.addString(columnInfo.quantityRawColKey, realmObjectSource.realmGet$quantityRaw());
        builder.addString(columnInfo.displayQuantityRawColKey, realmObjectSource.realmGet$displayQuantityRaw());
        builder.addString(columnInfo.feesRawColKey, realmObjectSource.realmGet$feesRaw());
        builder.addString(columnInfo.todayQuantityColKey, realmObjectSource.realmGet$todayQuantity());
        builder.addString(columnInfo.todayPricePaidColKey, realmObjectSource.realmGet$todayPricePaid());
        builder.addString(columnInfo.todayCommissionsColKey, realmObjectSource.realmGet$todayCommissions());
        builder.addString(columnInfo.daysGainValueColKey, realmObjectSource.realmGet$daysGainValue());
        builder.addString(columnInfo.daysGainPercentageRawColKey, realmObjectSource.realmGet$daysGainPercentageRaw());
        builder.addString(columnInfo.totalGainValueColKey, realmObjectSource.realmGet$totalGainValue());
        builder.addString(columnInfo.totalGainPercentageRawColKey, realmObjectSource.realmGet$totalGainPercentageRaw());
        builder.addString(columnInfo.todaysCloseColKey, realmObjectSource.realmGet$todaysClose());
        builder.addString(columnInfo.daysQuantityColKey, realmObjectSource.realmGet$daysQuantity());
        builder.addBoolean(columnInfo.daysPurchaseColKey, realmObjectSource.realmGet$daysPurchase());
        builder.addBoolean(columnInfo.hasLotsColKey, realmObjectSource.realmGet$hasLots());
        builder.addBoolean(columnInfo.inTheMoneyFlagColKey, realmObjectSource.realmGet$inTheMoneyFlag());
        builder.addString(columnInfo.optionUnderlierColKey, realmObjectSource.realmGet$optionUnderlier());
        builder.addString(columnInfo.strikePriceColKey, realmObjectSource.realmGet$strikePrice());
        builder.addString(columnInfo.markToMarketColKey, realmObjectSource.realmGet$markToMarket());
        builder.addString(columnInfo.marketValueRawColKey, realmObjectSource.realmGet$marketValueRaw());
        builder.addString(columnInfo.baseSymbolPriceColKey, realmObjectSource.realmGet$baseSymbolPrice());

        builder.updateExistingObject();
        return realmObject;
    }

    @Override
    @SuppressWarnings("ArrayToString")
    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder stringBuilder = new StringBuilder("PositionRealmObject = proxy[");
        stringBuilder.append("{positionId:");
        stringBuilder.append(realmGet$positionId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{accountUuid:");
        stringBuilder.append(realmGet$accountUuid());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{accountId:");
        stringBuilder.append(realmGet$accountId());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{pricePaidRaw:");
        stringBuilder.append(realmGet$pricePaidRaw());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{basisPrice:");
        stringBuilder.append(realmGet$basisPrice());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{symbol:");
        stringBuilder.append(realmGet$symbol());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{commissionRaw:");
        stringBuilder.append(realmGet$commissionRaw());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{quantityRaw:");
        stringBuilder.append(realmGet$quantityRaw());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{displayQuantityRaw:");
        stringBuilder.append(realmGet$displayQuantityRaw());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{feesRaw:");
        stringBuilder.append(realmGet$feesRaw());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{todayQuantity:");
        stringBuilder.append(realmGet$todayQuantity());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{todayPricePaid:");
        stringBuilder.append(realmGet$todayPricePaid());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{todayCommissions:");
        stringBuilder.append(realmGet$todayCommissions());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{daysGainValue:");
        stringBuilder.append(realmGet$daysGainValue());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{daysGainPercentageRaw:");
        stringBuilder.append(realmGet$daysGainPercentageRaw());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{totalGainValue:");
        stringBuilder.append(realmGet$totalGainValue());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{totalGainPercentageRaw:");
        stringBuilder.append(realmGet$totalGainPercentageRaw());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{todaysClose:");
        stringBuilder.append(realmGet$todaysClose());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{daysQuantity:");
        stringBuilder.append(realmGet$daysQuantity() != null ? realmGet$daysQuantity() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{daysPurchase:");
        stringBuilder.append(realmGet$daysPurchase());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{hasLots:");
        stringBuilder.append(realmGet$hasLots());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{inTheMoneyFlag:");
        stringBuilder.append(realmGet$inTheMoneyFlag() != null ? realmGet$inTheMoneyFlag() : "null");
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{optionUnderlier:");
        stringBuilder.append(realmGet$optionUnderlier());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{strikePrice:");
        stringBuilder.append(realmGet$strikePrice());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{markToMarket:");
        stringBuilder.append(realmGet$markToMarket());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{marketValueRaw:");
        stringBuilder.append(realmGet$marketValueRaw());
        stringBuilder.append("}");
        stringBuilder.append(",");
        stringBuilder.append("{baseSymbolPrice:");
        stringBuilder.append(realmGet$baseSymbolPrice() != null ? realmGet$baseSymbolPrice() : "null");
        stringBuilder.append("}");
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    @Override
    public ProxyState<?> realmGet$proxyState() {
        return proxyState;
    }

    @Override
    public int hashCode() {
        String realmName = proxyState.getRealm$realm().getPath();
        String tableName = proxyState.getRow$realm().getTable().getName();
        long colKey = proxyState.getRow$realm().getObjectKey();

        int result = 17;
        result = 31 * result + ((realmName != null) ? realmName.hashCode() : 0);
        result = 31 * result + ((tableName != null) ? tableName.hashCode() : 0);
        result = 31 * result + (int) (colKey ^ (colKey >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy aPositionRealmObject = (com_etrade_mobilepro_positions_dao_PositionRealmObjectRealmProxy)o;

        BaseRealm realm = proxyState.getRealm$realm();
        BaseRealm otherRealm = aPositionRealmObject.proxyState.getRealm$realm();
        String path = realm.getPath();
        String otherPath = otherRealm.getPath();
        if (path != null ? !path.equals(otherPath) : otherPath != null) return false;
        if (realm.isFrozen() != otherRealm.isFrozen()) return false;
        if (!realm.sharedRealm.getVersionID().equals(otherRealm.sharedRealm.getVersionID())) {
            return false;
        }

        String tableName = proxyState.getRow$realm().getTable().getName();
        String otherTableName = aPositionRealmObject.proxyState.getRow$realm().getTable().getName();
        if (tableName != null ? !tableName.equals(otherTableName) : otherTableName != null) return false;

        if (proxyState.getRow$realm().getObjectKey() != aPositionRealmObject.proxyState.getRow$realm().getObjectKey()) return false;

        return true;
    }
}
