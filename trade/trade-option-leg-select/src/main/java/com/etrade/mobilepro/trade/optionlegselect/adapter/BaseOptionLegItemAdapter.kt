package com.etrade.mobilepro.trade.optionlegselect.adapter

import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.trade.optionlegselect.OptionLegItem
import com.etrade.mobilepro.trade.optionlegselect.adapter.viewholder.OptionLegItemViewHolder
import com.etrade.mobilepro.util.android.extension.dispatchUpdates

abstract class BaseOptionLegItemAdapter : RecyclerView.Adapter<OptionLegItemViewHolder>() {

    var items: List<OptionLegItem> = emptyList()
        set(value) {
            if (field != value) {
                field = value
                underlyingItems = value.mapIndexed { index, item ->
                    item.toUnderlyingItem(index)
                }
            }
        }

    protected var underlyingItems: List<Item> = emptyList()
        set(newItems) {
            val oldItems = field
            if (oldItems != newItems) {
                field = newItems
                if (oldItems.size == newItems.size) {
                    dispatchUpdates(oldItems, newItems)
                } else {
                    notifyDataSetChanged()
                }
            }
        }

    final override fun getItemCount(): Int = underlyingItems.size

    final override fun onBindViewHolder(holder: OptionLegItemViewHolder, position: Int) {
        holder.bindTo(underlyingItems[position], position, position == underlyingItems.lastIndex)
    }

    private fun OptionLegItem.toUnderlyingItem(index: Int): Item {
        val prototype = underlyingItems.getOrNull(index)
        return Item(
            displayName = displayName,
            quantity = prototype.assign(quantity) { quantity },
            isDeletePending = prototype.assign(isDeletePending) { isDeletePending }
        )
    }

    private inline fun <T> Item?.assign(initialValue: T, property: Item.() -> MutableLiveData<T>): MutableLiveData<T> {
        return this?.property()
            ?.apply { value = initialValue }
            ?: MutableLiveData(initialValue)
    }

    data class Item(
        val displayName: String,
        val quantity: MutableLiveData<String>,
        val isDeletePending: MutableLiveData<Boolean>
    )
}
