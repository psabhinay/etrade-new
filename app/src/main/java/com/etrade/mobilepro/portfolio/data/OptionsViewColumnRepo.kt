package com.etrade.mobilepro.portfolio.data

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.tableviewutils.api.ColumnRepo
import com.etrade.mobilepro.tableviewutils.presentation.data.BaseColumnRepo
import javax.inject.Inject

/**
 * This repository handles everything related to columns of a portfolio table's entries in options view.
 */
interface OptionsViewColumnRepo : ColumnRepo<PortfolioColumn>

private const val COLUMN_MARK = "Mark"
private const val COLUMN_QUANTITY = "Quantity"
private const val COLUMN_CHANGE_DOLLAR = "ChangeDollar"
private const val COLUMN_PRICE_PAID = "PricePaid"
private const val COLUMN_TOTAL_GAIN_DOLLAR = "TotalGainDollar"
private const val COLUMN_DAYS_GAIN_DOLLAR = "DaysGainDollar"
private const val COLUMN_DAYS_GAIN_PERCENT = "ColumnDaysGainPercent"
private const val COLUMN_TOTAL_GAIN_PERCENT = "TotalGainPercent"
private const val COLUMN_BASE_SYMBOL_AND_PRICE = "BaseSymbolAndPrice"
private const val COLUMN_DAYS_TO_EXPIRE = "DaysToExpire"
private const val COLUMN_BID = "Bid"
private const val COLUMN_ASK = "Ask"
private const val COLUMN_LAST_PRICE = "LastPrice"
private const val COLUMN_DELTA = "Delta"
private const val COLUMN_GAMMA = "Gamma"
private const val COLUMN_THETA = "Theta"
private const val COLUMN_VEGA = "Vega"
private const val COLUMN_IMPLIED_VOLATILITY_PERCENT = "ImpliedVolatilityIndex"

class OptionsViewColumnRepoImpl @Inject constructor(storage: KeyValueStorage) : BaseColumnRepo<PortfolioColumn>(storage), OptionsViewColumnRepo {

    override val storageKey: String = "portfolioOptionsViewTableColumns"

    override val allColumns: Map<String, PortfolioColumn> = mapOf(
        COLUMN_MARK to PortfolioColumn.MARK,
        COLUMN_QUANTITY to PortfolioColumn.QUANTITY,
        COLUMN_CHANGE_DOLLAR to PortfolioColumn.CHANGE_DOLLAR,
        COLUMN_PRICE_PAID to PortfolioColumn.PRICE_PAID,
        COLUMN_TOTAL_GAIN_DOLLAR to PortfolioColumn.TOTAL_GAIN_DOLLAR,
        COLUMN_DAYS_GAIN_DOLLAR to PortfolioColumn.DAYS_GAIN_DOLLAR,
        COLUMN_DAYS_GAIN_PERCENT to PortfolioColumn.DAYS_GAIN_PERCENT,
        COLUMN_TOTAL_GAIN_PERCENT to PortfolioColumn.TOTAL_GAIN_PERCENT,
        COLUMN_BASE_SYMBOL_AND_PRICE to PortfolioColumn.BASE_SYMBOL_AND_PRICE,
        COLUMN_DAYS_TO_EXPIRE to PortfolioColumn.DAYS_TO_EXPIRE,
        COLUMN_BID to PortfolioColumn.BID,
        COLUMN_ASK to PortfolioColumn.ASK,
        COLUMN_LAST_PRICE to PortfolioColumn.LAST_PRICE,
        COLUMN_DELTA to PortfolioColumn.DELTA,
        COLUMN_GAMMA to PortfolioColumn.GAMMA,
        COLUMN_THETA to PortfolioColumn.THETA,
        COLUMN_VEGA to PortfolioColumn.VEGA,
        COLUMN_IMPLIED_VOLATILITY_PERCENT to PortfolioColumn.IMPLIED_VOLATILITY_PERCENT
    )

    override val defaultColumnsOrder: List<String> = listOf(
        COLUMN_MARK,
        COLUMN_QUANTITY,
        COLUMN_CHANGE_DOLLAR,
        COLUMN_PRICE_PAID,
        COLUMN_TOTAL_GAIN_DOLLAR,
        COLUMN_DAYS_GAIN_DOLLAR,
        COLUMN_DAYS_GAIN_PERCENT,
        COLUMN_TOTAL_GAIN_PERCENT,
        COLUMN_BASE_SYMBOL_AND_PRICE,
        COLUMN_DAYS_TO_EXPIRE,
        COLUMN_BID,
        COLUMN_ASK,
        COLUMN_LAST_PRICE,
        COLUMN_DELTA,
        COLUMN_GAMMA,
        COLUMN_THETA,
        COLUMN_VEGA,
        COLUMN_IMPLIED_VOLATILITY_PERCENT
    )
}
