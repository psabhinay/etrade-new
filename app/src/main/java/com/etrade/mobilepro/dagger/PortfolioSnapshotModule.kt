package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.movers.portfolio.GetPortfolioMoversUseCase
import com.etrade.mobilepro.movers.portfolio.GetPortfolioMoversUseCaseImpl
import com.etrade.mobilepro.movers.portfolio.caching.PortfolioSnapshotRepo
import com.etrade.mobilepro.movers.portfolio.caching.PortfolioSnapshotRepoImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class PortfolioSnapshotModule {

    @Binds
    @Singleton
    abstract fun bindPortfolioSnapshotRepo(repo: PortfolioSnapshotRepoImpl): PortfolioSnapshotRepo

    @Binds
    @Singleton
    abstract fun bindGetPortfolioMoversUseCase(useCase: GetPortfolioMoversUseCaseImpl): GetPortfolioMoversUseCase
}
