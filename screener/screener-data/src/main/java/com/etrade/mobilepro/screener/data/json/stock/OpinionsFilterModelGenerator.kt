package com.etrade.mobilepro.screener.data.json.stock

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.stock.OpinionsFilter
import javax.inject.Inject

private const val DOWNGRADED_FIELD = "Downgraded"
private const val UPGRADED_FIELD = "Upgraded"

class OpinionsFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val optionsFilter = screenerFilter as OpinionsFilter
        val arguments = mutableListOf<Argument>()

        if (optionsFilter.downgraded) {
            arguments.add(Argument(listOf(optionsClause), DOWNGRADED_FIELD))
        }

        if (optionsFilter.upgraded) {
            arguments.add(Argument(listOf(optionsClause), UPGRADED_FIELD))
        }

        return PartialScreenerParams(arguments)
    }

    private val optionsClause = Clause(OPERATOR_GREATER_THAN, listOf("0"))
}
