package com.etrade.mobilepro.optionschains.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Request data model needed to make a options chains API call
 */
@JsonClass(generateAdapter = true)
class OptionsChainRequestDto(
    @Json(name = "value")
    val optionsChainRequestValue: OptionsChainRequestValueDto,
    @Json(name = "userID")
    val userID: String?
)

/**
 * Nested inner object of json references "value"
 */
@JsonClass(generateAdapter = true)
class OptionsChainRequestValueDto(
    @Json(name = "allMonths")
    val allMonths: String,
    @Json(name = "leapsOnly")
    val leapsOnly: String,
    @Json(name = "skipAdjusted")
    val skipAdjusted: String,
    @Json(name = "chainType")
    val chainType: String,
    @Json(name = "priceType")
    val priceType: String,
    @Json(name = "strikeRange")
    val strikeRange: String,
    @Json(name = "skipLeaps")
    val skipLeaps: String,
    @Json(name = "displayBy")
    val displayBy: String,
    @Json(name = "optionScalabilityCd")
    val optionScalabilityCd: String,
    @Json(name = "ed")
    val ed: ExpirationDateDto,
    @Json(name = "pid")
    val pid: OptionsChainPidDto
)

/**
 * Nested inner object within "value" that contains PID info
 */
@JsonClass(generateAdapter = true)
class OptionsChainPidDto(
    @Json(name = "symbol")
    val symbol: String,
    @Json(name = "typeCode")
    val typeCode: String
)
