package com.etrade.mobilepro.chartiq.api.model

data class Environment(val name: String, val baseUrl: String, val chartUrl: String)
