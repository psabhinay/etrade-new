package com.etrade.mobilepro.tableviewutils.presentation.decorator

import android.content.Context
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.roundToInt

class CustomTableViewDividerDecoration(
    context: Context,
    private val orientation: Int,
    private val divider: Drawable? = null
) : DividerItemDecoration(context, orientation) {

    private val bounds = Rect()

    override fun getDrawable(): Drawable? {
        return divider
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.layoutManager == null || drawable == null) {
            return
        }
        if (orientation == VERTICAL) {
            customDrawVertical(c, parent, state.itemCount)
        } else {
            super.onDraw(c, parent, state)
        }
    }

    private fun customDrawVertical(canvas: Canvas, parent: RecyclerView, itemsCount: Int) {
        canvas.save()
        val left: Int
        val right: Int
        if (parent.clipToPadding) {
            left = parent.paddingLeft
            right = parent.width - parent.paddingRight
            canvas.clipRect(
                left, parent.paddingTop, right,
                parent.height - parent.paddingBottom
            )
        } else {
            left = 0
            right = parent.width
        }

        val childCount = parent.childCount
        for (i in 0 until childCount) {
            val child = parent.getChildAt(i)
            parent.getDecoratedBoundsWithMargins(child, bounds)
            val bottom = bounds.bottom + child.translationY.roundToInt()
            val top = bottom - (divider?.intrinsicHeight ?: 0)
            divider?.setBounds(left, top, right, bottom)
            if (parent.getChildAdapterPosition(child) < itemsCount) {
                divider?.draw(canvas)
            }
        }
        canvas.restore()
    }
}
