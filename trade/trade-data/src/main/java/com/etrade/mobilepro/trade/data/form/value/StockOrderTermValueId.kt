package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.util.MutualConverter

object StockOrderTermValueId : OrderTermValueId {
    override val extendedHoursDay: String = "ehday"
    override val extendedHoursImmediateOrCancel: String = "ehioc"
    override val fillOrKill: String = "fok"
    override val goodForDay: String = "day"
    override val goodUntilCancel: String = "gtc"
    override val immediateOrCancel: String = "ioc"
    override val converter: MutualConverter<String, OrderTerm> by lazy { createConverter() }
}
