package com.etrade.mobilepro.dynamicui.datasource

import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.backends.mgs.BaseMobileResponse
import com.etrade.mobilepro.backends.mgs.MobileScreen
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.chart.linechart.dto.MarketChartViewDto
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.dynamicui.CachedResponseModel
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.dynamicui.delegate.WithRealmDataSourceDelegate
import com.etrade.mobilepro.home.overview.dto.OverviewFooterViewDto
import com.etrade.mobilepro.indices.dto.MarketIndicesViewDto
import com.etrade.mobilepro.movers.markets.dto.MarketMoverViewDto
import com.etrade.mobilepro.movers.portfolio.dto.PortfolioMoverViewDto
import com.etrade.mobilepro.news.api.dto.NewsWidgetBriefingDto
import com.etrade.mobilepro.news.api.dto.NewsWidgetPortfolioDto
import com.etrade.mobilepro.portfolio.PositionTypeSelectorViewDto
import com.etrade.mobilepro.positions.data.dto.PositionListViewDto
import com.etrade.mobilepro.util.Resource
import com.squareup.moshi.JsonDataException
import com.squareup.moshi.Moshi
import io.reactivex.Observable
import org.slf4j.LoggerFactory
import java.io.IOException
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(RemoteDynamicScreenDataSource::class.java)

/**
 * Constructs Dynamic Screen views by Fetching preconfigured views from remote or local cached source.
 */
class RemoteDynamicScreenDataSource @Inject constructor(
    moshi: Moshi,
    private val dataSourceDelegate: WithRealmDataSourceDelegate
) : ScreenDataSource {

    private val moshiAdapter = moshi.adapter(BaseMobileResponse::class.java)

    @SuppressWarnings("LongMethod", "ComplexMethod")
    override fun getScreen(
        request: ScreenRequest,
        showCachedData: Boolean,
        cacheExpiration: Long?
    ): Observable<Resource<BaseMobileResponse>> {
        val isAccountOverView = request is ServicePath.AccountOverView
        val cachedScreen = SimpleCachedResource(
            id = request,
            dataSourceDelegate = dataSourceDelegate,
            controller = CachedResponseModelCachedResource(cacheExpiration),
            showCachedData = showCachedData
        )
        return cachedScreen.asObservable()
            .map { cachedResponse ->
                try {
                    val response = cachedResponse.data?.response?.let {
                        moshiAdapter.fromJson(it)
                    }
                    val deserializedResponse = addDummyScreensIfNeeded(request, response)

                    when (cachedResponse) {
                        is Resource.Loading -> Resource.Loading(deserializedResponse)
                        is Resource.Success -> Resource.Success(deserializedResponse)
                        is Resource.Failed -> {
                            if (isAccountOverView && deserializedResponse == null) {
                                Resource.Success(getOfflineAccountOverviewResponse())
                            } else {
                                Resource.Failed(deserializedResponse, cachedResponse.error)
                            }
                        }
                    }
                } catch (e: IOException) {
                    logger.error("IO failed while parsing cached screen response", e)
                    Resource.Failed<BaseMobileResponse>(error = e)
                } catch (e: JsonDataException) {
                    logger.error("Failed parsing cached screen response", e)
                    Resource.Failed<BaseMobileResponse>(error = e)
                }
            }
    }

    private fun addDummyScreensIfNeeded(
        request: ScreenRequest,
        deserializedResponse: BaseMobileResponse?
    ): BaseMobileResponse? {
        if (deserializedResponse != null) {
            val index =
                deserializedResponse.screen.views.indexOfFirst { it is PositionListViewDto }
            if (index > 0) {
                val views = deserializedResponse.screen.views.toMutableList()
                views.add(
                    index,
                    PositionTypeSelectorViewDto(request is ServicePath.Portfolio, null, null)
                )
                return deserializedResponse.copy(deserializedResponse.screen.copy(views))
            }
        }
        return deserializedResponse
    }

    private class CachedResponseModelCachedResource(val cacheExpiration: Long?) :
        SimpleCachedResource.Controller<CachedResponseModel> {
        override fun shouldRefresh(itemFromCache: CachedResponseModel) =
            cacheExpiration?.let {
                System.currentTimeMillis() - itemFromCache.retrievedTimestamp >= it
            } ?: true
    }

    private fun getOfflineAccountOverviewResponse(): BaseMobileResponse {
        val views = listOf(
            MarketIndicesViewDto(null, null),
            MarketChartViewDto(null, null),
            PortfolioMoverViewDto(null, null),
            MarketMoverViewDto(null, null),
            NewsWidgetPortfolioDto(null, null),
            NewsWidgetBriefingDto(null, null),
            OverviewFooterViewDto(null, null)
        )
        return BaseMobileResponse(MobileScreen(views, emptyList()))
    }
}
