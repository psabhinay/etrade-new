package com.etrade.mobilepro.appstart.data

import com.etrade.mobilepro.appstart.api.AppStartRepo
import com.etrade.mobilepro.appstart.api.AppStartRepo.Companion.IS_SUBJECT_TO_GDPR_DEFAULT
import com.etrade.mobilepro.appstart.data.rest.AppStartService
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import retrofit2.Retrofit
import javax.inject.Inject

class DefaultAppStartRepo @Inject constructor(val retrofit: Retrofit) : AppStartRepo {
    private val apiClient = retrofit.create(AppStartService::class.java)

    override suspend fun getPingSessionCallInterval(): ETResult<Long?> {
        return runCatchingET {
            apiClient.getAppStart()
                .screen.references.firstOrNull()?.data?.firstOrNull()?.pingSessionInterval
        }
    }

    override suspend fun getGDPRStatus(): ETResult<Boolean> {
        return runCatchingET {
            apiClient.getAppStart()
                .screen.references.firstOrNull()?.data?.firstOrNull()?.isSubjectToGDPR ?: IS_SUBJECT_TO_GDPR_DEFAULT
        }
    }
}
