package com.etrade.mobilepro.util

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

private const val COOKIE_HEADER = "Cookie"

class CookieSweepInterceptor(
    private val cookiesToSweep: Set<String>,
    private val excludePaths: Set<String> = emptySet()
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val url = originalRequest.url.toString()
        var sweepCookies = true
        for (path in excludePaths) {
            if (url.contains(path)) {
                sweepCookies = false
                break
            }
        }
        val request = if (sweepCookies) {
            originalRequest.newBuilder().apply {
                val cookies = originalRequest.sweepCookies(cookiesToSweep)
                removeHeader(COOKIE_HEADER)
                addHeader(COOKIE_HEADER, cookies)
            }.build()
        } else {
            originalRequest
        }
        return chain.proceed(request)
    }
}

fun Request.sweepCookies(cookies: Set<String>): String {
    return header(COOKIE_HEADER)
        .orEmpty()
        .split(";")
        .map { it.trim() }
        .filter { cookie ->
            val endIndex = cookie.indexOfFirst { it == '=' }
            if (endIndex >= 0) {
                val cookieName = cookie.substring(0, endIndex)
                !cookies.contains(cookieName)
            } else {
                true
            }
        }
        .joinToString("; ")
}
