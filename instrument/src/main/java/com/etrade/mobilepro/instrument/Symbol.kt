package com.etrade.mobilepro.instrument

data class Symbol(
    override val symbol: String,
    override val instrumentType: InstrumentType
) : WithSymbolInfo {

    constructor(prototype: WithSymbolInfo) : this(prototype.symbol, prototype.instrumentType)
}
