package com.etrade.mobilepro.order.details.presentation.delegate

import android.content.res.Resources
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.dynamic.form.api.Action
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem.OrderLegItem
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem.OrderSectionDividerItem
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem.OrderSectionItem
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel.ActionsArgument
import com.etrade.mobilepro.order.details.presentation.helper.cancelOrderAction
import com.etrade.mobilepro.order.details.presentation.helper.placeOrder
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.orders.presentation.OrderButtonVisibilityManager
import com.etrade.mobilepro.orders.presentation.orderButtonVisibilityManager
import com.etrade.mobilepro.presentation.handleServerMessages
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quoteapi.FundsTradeQuoteItem
import com.etrade.mobilepro.quoteapi.FundsTradeTitleViewItem
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesResult
import com.etrade.mobilepro.trade.api.OrderResponse
import com.etrade.mobilepro.trade.api.TradeRepo
import com.etrade.mobilepro.trade.api.action
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrder
import com.etrade.mobilepro.trade.api.mutualfunds.MutualFundsOrderResponse
import com.etrade.mobilepro.trade.api.mutualfunds.hasExchangeLeg
import com.etrade.mobilepro.trade.api.quantity
import com.etrade.mobilepro.trade.api.ticker
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.presentation.util.R
import com.etrade.mobilepro.trade.presentation.util.TradeExceptionViewModelDelegate
import com.etrade.mobilepro.trade.presentation.util.adapters.DefaultOrderAdapter
import com.etrade.mobilepro.trade.presentation.util.adapters.getMutualFundAmountDisplayText
import com.etrade.mobilepro.trade.presentation.util.adapters.sectionMultiLineItem
import com.etrade.mobilepro.trade.presentation.util.adapters.toAccountsOrderTypeItems
import com.etrade.mobilepro.trade.presentation.util.adapters.toAmountItems
import com.etrade.mobilepro.trade.presentation.util.adapters.toMutualFundsCommissionItems
import com.etrade.mobilepro.trade.presentation.util.toMutualFundsQuoteSummaryItems
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class PreviewMutualFundsOrderDetailsDelegate @Inject constructor(
    private val resources: Resources,
    private val tradeRepo: TradeRepo,
    private val orderAdapter: DefaultOrderAdapter,
    @Web
    private val baseUrl: String
) : OrderDetailsViewModel.BaseOrderDetailsDelegate() {

    private val placeLoadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)

    override val loadingIndicators: Set<LiveData<Boolean>> = setOf(placeLoadingIndicator)

    override suspend fun loadItems(
        argument: OrderDetailsArgument,
        quotes: Deferred<Map<String, OrderDetailItemInterface>>
    ): ETResult<OrderDetailsViewModel.OrderDetails> {

        val orderId = argument.orderId
        return tradeRepo.loadOrder(orderId)
            .onFailure { sendOrderDeletedSignal(orderId) }
            .map { tradeOrder ->
                val response = tradeOrder.response as MutualFundsOrderResponse
                val (quoteWidgetViewModel, _) = getQuoteDetailsViewModels()
                if (response.hasExchangeLeg) {
                    constructMutualFundsExchangeOrderPreview(response.orders, quoteWidgetViewModel)
                } else {
                    constructMutualFundsOrderPreview(response.orders.first(), quoteWidgetViewModel)
                }
            }
    }

    override fun createActions(argument: ActionsArgument, manager: OrderButtonVisibilityManager): List<Action> {
        val orderId = argument.orderDetailsArgument.orderId
        return listOfNotNull(
            manager.cancelOrderAction(resources) {
                sendOrderDeletedSignal(orderId)
            },
            placeOrder(resources) {
                placeOrder(orderId)
            }
        )
    }

    override fun createButtonVisibilityManager(argument: OrderDetailsArgument): OrderButtonVisibilityManager {
        return orderButtonVisibilityManager {
            isCancellable = true
            isCancelOnly = false
        }
    }

    override fun resetActionStates() = Unit

    private fun placeOrder(orderId: String) {
        executeOperation(
            loadingIndicator = placeLoadingIndicator,
            operation = { tradeRepo.placeOrder(orderId) },
            onSuccess = { handleSuccess(orderId, it.response) },
            interceptFailure = { TradeExceptionViewModelDelegate(resources, baseUrl, mutableDialog, emptyMap()).handleTradeOrderException(it) }
        )
    }

    private fun handleSuccess(previewId: String, response: OrderResponse) {
        handleServerMessages(resources, baseUrl, response.warnings, mutableDialog, emptyMap()) {
            sendOrderConfirmedSignal(previewId, response, SecurityType.MUTUAL_FUND)
        }
    }

    private fun constructMutualFundsOrderPreview(
        response: MutualFundsOrder,
        quoteWidgetViewModel: QuotesWidgetViewModel
    ): OrderDetailsViewModel.OrderDetails {
        return OrderDetailsViewModel.OrderDetails(
            listOfNotNull(
                quoteWidgetViewModel.toQuoteHeaderItem(),
                OrderLegItem(
                    displaySymbol = response.ticker,
                    displayQuantity = getMutualFundAmountDisplayText(
                        response.orderActionType,
                        response.quantity,
                        response.quantityType,
                        resources
                    ),
                    transactionType = response.action
                ),
                OrderSectionItem(
                    orderAdapter.createOrderDisplayItem(response).sections +
                        quoteWidgetViewModel.toTradeQuoteSectionItem() +
                        response.toMutualFundsCommissionItems(resources, quoteWidgetViewModel.isLoadWaived())
                )
            ),
            OrderStatus.UNKNOWN
        )
    }

    private fun constructMutualFundsExchangeOrderPreview(
        response: List<MutualFundsOrder>,
        quoteWidgetViewModel: QuotesWidgetViewModel
    ): OrderDetailsViewModel.OrderDetails {
        return OrderDetailsViewModel.OrderDetails(
            exchangeSellItems(response.first(), quoteWidgetViewModel) + exchangeBuyItems(response.last(), quoteWidgetViewModel),
            OrderStatus.UNKNOWN
        )
    }

    private fun exchangeSellItems(response: MutualFundsOrder, quoteWidgetViewModel: QuotesWidgetViewModel): List<OrderDetailItemInterface> {
        return listOfNotNull(
            quoteWidgetViewModel.toQuoteHeaderItem(),
            OrderLegItem(
                displaySymbol = response.ticker,
                displayQuantity = getMutualFundAmountDisplayText(
                    response.orderActionType,
                    response.quantity,
                    response.quantityType,
                    resources
                ),
                transactionType = TransactionType.SELL_CLOSE
            ),
            OrderSectionItem(
                toAccountsOrderTypeItems(resources, response.account, response.orderActionType) +
                    response.toAmountItems(resources) +
                    quoteWidgetViewModel.toTradeQuoteSectionItem() +
                    toExchangeCommissionItems(response.priceTypeMessage),
                View.GONE
            )
        )
    }

    private fun exchangeBuyItems(order: MutualFundsOrder, quoteWidgetViewModel: QuotesWidgetViewModel): List<OrderDetailItemInterface> {
        val exchangeLeg = order.legs.firstOrNull()
        return listOfNotNull(
            OrderSectionDividerItem,
            quoteWidgetViewModel.toQuoteHeaderItem(isExchangeLeg = true),
            OrderLegItem(
                displaySymbol = exchangeLeg?.symbol ?: "",
                displayQuantity = getMutualFundAmountDisplayText(
                    order.orderActionType,
                    exchangeLeg?.quantity,
                    order.quantityType,
                    resources
                ),
                transactionType = TransactionType.BUY_OPEN
            ),
            OrderSectionItem(
                toAccountsOrderTypeItems(resources, order.account, order.orderActionType) +
                    quoteWidgetViewModel.toTradeQuoteSectionItem(isExchangeLeg = true) +
                    order.toMutualFundsCommissionItems(resources, quoteWidgetViewModel.isLoadWaived(true))
            )
        )
    }

    private fun toExchangeCommissionItems(message: String): List<TableSectionItemLayout> {
        return listOfNotNull(
            resources.sectionMultiLineItem(R.string.trade_label_estimated_total_proceeds, message)
        )
    }

    private fun toMutualFundsQuoteItems(result: MutualFundsTradeQuotesResult): List<TableSectionItemLayout> = if (result is FundsTradeQuoteItem) {
        toMutualFundsQuoteSummaryItems(resources, result.summaryItem)
    } else {
        emptyList()
    }

    private fun QuotesWidgetViewModel.primaryQuote() = primaryFundsQuote.value?.peekContent()

    private fun QuotesWidgetViewModel.secondaryQuote() = secondaryFundsQuote.value?.peekContent()

    private fun QuotesWidgetViewModel.getFundsQuote(isExchangeLeg: Boolean = false): MutualFundsTradeQuotesResult? {
        return if (isExchangeLeg) {
            this.secondaryQuote()
        } else {
            this.primaryQuote()
        }
    }

    private fun QuotesWidgetViewModel.toQuoteHeaderItem(isExchangeLeg: Boolean = false): FundsTradeTitleViewItem? = this.getFundsQuote(isExchangeLeg)?.let {
        (it as FundsTradeQuoteItem).headerItem
    }

    private fun QuotesWidgetViewModel.toTradeQuoteSectionItem(isExchangeLeg: Boolean = false): List<TableSectionItemLayout> =
        getFundsQuote(isExchangeLeg)?.let {
            toMutualFundsQuoteItems(it)
        }.orEmpty()

    private fun QuotesWidgetViewModel.isLoadWaived(isExchangeLeg: Boolean = false): Boolean = this.getFundsQuote(isExchangeLeg)?.let {
        it is FundsTradeQuoteItem && it.isLoadWaived
    } ?: false
}
