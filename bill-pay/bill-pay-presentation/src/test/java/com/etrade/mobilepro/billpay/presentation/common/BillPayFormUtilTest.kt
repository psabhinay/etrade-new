package com.etrade.mobilepro.billpay.presentation.common

import com.etrade.mobile.accounts.billpay.BillPay
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.util.safeParseBigDecimal
import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal
import java.math.BigInteger

class BillPayFormUtilTest {
    @Test
    fun `test valid and invalid amounts`() {
        // positive case
        assertEquals(true, BigDecimal("99999.99").isValidAmount())
        assertEquals(true, BigDecimal("01.00").isValidAmount())

        // negative case
        assertEquals(false, BigDecimal("100000").isValidAmount())
        assertEquals(false, BigDecimal("0.99").isValidAmount())
    }

    @Test
    fun `test maximum amounts`() {
        // positive case
        assertEquals(true, BigDecimal("01.00").isValidMaximumAmount())
        assertEquals(true, BigDecimal("50000").isValidMaximumAmount())
        assertEquals(true, BigDecimal("99999.99").isValidMaximumAmount())

        // negative case
        assertEquals(false, BigDecimal("100000").isValidMaximumAmount())
        assertEquals(false, BigDecimal("5000000").isValidMaximumAmount())
        assertEquals(false, "600000.50".safeParseBigDecimal().isValidMaximumAmount())
    }

    @Test
    fun `test minimum amounts`() {
        // positive case
        assertEquals(true, BigDecimal("01.00").isValidMinimumAmount())
        assertEquals(true, BigDecimal("50000").isValidMinimumAmount())
        assertEquals(true, BigDecimal("99999.99").isValidMinimumAmount())

        // negative case
        assertEquals(false, BigDecimal("0.01").isValidMinimumAmount())
        assertEquals(false, BigDecimal("0.99").isValidMinimumAmount())
        assertEquals(false, "-100.10".safeParseBigDecimal().isValidMinimumAmount())
    }

    @Test
    fun testFormatPaymentAmount() {
        // positive case
        assertEquals("0.00", BigDecimal.ZERO.formatAmount()?.toPlainString())
        assertEquals("10.00", BigDecimal("10.0").formatAmount()?.toPlainString())
        assertEquals("1000.00", BigDecimal("1000.0").formatAmount()?.toPlainString())

        // negative case
        assertEquals(null, null.formatAmount()?.toPlainString())
    }

    @Test
    fun testPayeeFormatName() {
        // positive case
        assertEquals("user -5678", getMockPayee("user", "1234-5678").formatName())
        assertEquals("name -acct", getMockPayee("name", "1234-acct").formatName())

        // add nick name
        assertEquals("nickName", getMockPayeeWithNickName("user", "1234-5678", "nickName").formatName())
        assertEquals("user -5678", getMockPayeeWithNickName("user", "1234-5678", "").formatName())
        assertEquals("user -5678", getMockPayeeWithNickName("user", "1234-5678", null).formatName())

        // negative case
        assertEquals("", getMockPayee("", "").formatName())
        assertEquals("name -acct", getMockPayee("name", "1234-acct").formatName())
    }

    private fun getMockPayee(name: String, acct: String) =
        Payee(name = name, accountNumber = acct, leadDays = 0, isAddressAvailable = false, nickName = "", phoneNumber = "")

    private fun getMockPayeeWithNickName(name: String, acct: String, nickName: String?) =
        Payee(name = name, accountNumber = acct, leadDays = 0, isAddressAvailable = false, nickName = nickName, phoneNumber = "")

    @Test
    fun testBillPayFormatAccountName() {
        // positive case
        assertEquals("test -5678", getMockAccountDetails("test", "1234-5678").formatAccountName())

        // negative case
        assertEquals("", getMockAccountDetails("", "").formatAccountName())
    }

    private fun getMockAccountDetails(accountName: String, accountId: String): AccountDetails = object : AccountDetails {
        override val accountIdKey: String = ""
        override val maskedAccountId: String = ""
        override val accountMode: String = ""
        override val accountName: String = accountName
        override val accountShortName: String = ""
        override val accountLongName: String = ""
        override val mobileDescription: String = ""
        override val accountValue: BigDecimal? = null
        override val institutionType: String = ""
        override val accountStatus: String = ""
        override val restrictionLevel: String = ""
        override val closedDate: Long = 0
        override val brokerage: AccountDetails.Brokerage? = null
        override val billPay: BillPay? = null
        override val accountId: String = accountId
        override val nickname: String? = null
        override val instNo: BigInteger = BigInteger.ZERO
        override val accountType: String = ""
        override val accountCategory: String = ""
    }

    @Test
    fun testBillPayFormatCurrency() {
        // positive case
        assertEquals("$0.00", formatCurrency(BigDecimal.ZERO))
        assertEquals("$10.00", formatCurrency(BigDecimal.TEN))
        assertEquals("$10,000,000.00", formatCurrency(BigDecimal("10000000")))

        // negative case
        assertEquals("--", formatCurrency(null))
        assertEquals("$0.00", formatCurrency(BigDecimal("00000")))
    }
}
