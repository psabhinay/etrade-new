package com.etrade.mobilepro.chart.data;

import com.etrade.mobilepro.chart.api.ChartPoint;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0007\u0018\u00002\u00020\u0001B!\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u000e\b\u0001\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/etrade/mobilepro/chart/data/AccountDataDto;", "", "accountNumber", "", "points", "", "Lcom/etrade/mobilepro/chart/data/PointDataDto;", "(Ljava/lang/String;Ljava/util/List;)V", "getAccountNumber", "()Ljava/lang/String;", "getPoints", "()Ljava/util/List;", "chartdata"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class AccountDataDto {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String accountNumber = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.etrade.mobilepro.chart.data.PointDataDto> points = null;
    
    public AccountDataDto(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "accountNo")
    java.lang.String accountNumber, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "pointData")
    java.util.List<com.etrade.mobilepro.chart.data.PointDataDto> points) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccountNumber() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.chart.data.PointDataDto> getPoints() {
        return null;
    }
}