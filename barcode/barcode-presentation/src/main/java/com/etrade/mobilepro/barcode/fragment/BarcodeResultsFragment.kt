package com.etrade.mobilepro.barcode.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.barcode.R
import com.etrade.mobilepro.barcode.adapter.BarcodeResultAdapter
import com.etrade.mobilepro.barcode.databinding.FragmentBarcodeResultsBinding
import com.etrade.mobilepro.barcode.viewmodel.BarcodeResultsViewModel
import com.etrade.mobilepro.barcode.viewmodel.BarcodeSharedViewModel
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import org.threeten.bp.ZoneOffset
import org.threeten.bp.format.DateTimeFormatter
import java.util.Locale
import javax.inject.Inject

class BarcodeResultsFragment @Inject constructor(
    private val barcodeResultsViewModel: BarcodeResultsViewModel,
    private val mainNavigation: MainNavigation,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val viewModelFactory: ViewModelProvider.Factory
) : BarcodeTermsAndConditionsFragment(R.layout.fragment_barcode_results, snackbarUtilFactory) {

    private val binding by viewBinding(FragmentBarcodeResultsBinding::bind)
    private lateinit var barcodeAdapter: BarcodeResultAdapter
    private val barcodeSharedViewModel: BarcodeSharedViewModel by activityViewModels { viewModelFactory }

    override fun getBarcodeTermsAndConditions() = binding.barcodeTermsAndConditionsView.barcodeTermsAndConditions

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        barcodeAdapter = BarcodeResultAdapter(
            LayoutInflater.from(context),
            itemClickListener = { barcode ->
                activity?.also {
                    (it as? AppCompatActivity)?.let { appCompatActivity ->
                        mainNavigation.navigateToQuoteDetails(appCompatActivity, SearchResultItem.Symbol(barcode.symbol, InstrumentType.EQ).toBundle())
                    }
                }
            },
            itemDeleteClickListener = { barcodeResultsViewModel.deleteBarcode(it) },
            upcStringTemplate = getString(R.string.barcode_upc_template),
            dateFormat = DateTimeFormatter.ofPattern(getString(R.string.barcode_date_template), Locale.US).withZone(ZoneOffset.systemDefault())
        )

        binding.barcodesRv.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = barcodeAdapter
            setupRecyclerViewDivider(R.drawable.thin_divider, countOfLastItemsWithoutDivider = 0)
        }

        setupViewModels(barcodeAdapter)
    }

    @Suppress("LongMethod")
    private fun setupViewModels(barcodeAdapter: BarcodeResultAdapter) {
        barcodeResultsViewModel.result.observe(
            viewLifecycleOwner,
            Observer {
                if (it.isEmpty()) {
                    binding.barcodeNoResults.visibility = View.VISIBLE
                } else {
                    binding.barcodeNoResults.visibility = View.GONE
                }

                barcodeAdapter.updateItems(it)
            }
        )

        barcodeResultsViewModel.barcodeDeletedEvent.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.also {
                    snackbarUtil.snackbar(getString(R.string.barcode_deleted), Snackbar.LENGTH_SHORT)?.show()
                }
            }
        )

        barcodeSharedViewModel.resetSwipeToDeleteActions.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.also { barcodeAdapter.resetSwipeToDeleteActions() }
            }
        )

        barcodeSharedViewModel.focusToRecentBarcode.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.also { binding.barcodesRv.scrollToPosition(0) }
            }
        )
    }
}
