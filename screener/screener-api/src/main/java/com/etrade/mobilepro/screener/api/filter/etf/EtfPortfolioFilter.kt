package com.etrade.mobilepro.screener.api.filter.etf

import com.etrade.mobilepro.screener.api.Country
import com.etrade.mobilepro.screener.api.Industry
import com.etrade.mobilepro.screener.api.IndustryType
import com.etrade.mobilepro.screener.api.Percent
import com.etrade.mobilepro.screener.api.Region
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class EtfPortfolioFilter(
    val region: Region,
    val regionPercentInvested: Percent,
    val country: Country,
    val countryPercentInvested: Percent,
    val industryType: IndustryType,
    val industry: Industry,
    val industryTypePercentInvested: Percent,

    val netAssetsLessThan100: Boolean = false,
    val netAssets100to500: Boolean = false,
    val netAssets500to1: Boolean = false,
    val netAssets1to2: Boolean = false,
    val netAssetsGreaterThan2: Boolean = false,

    val yieldBottom20: Boolean = false,
    val yieldBelowMid20: Boolean = false,
    val yieldMid20: Boolean = false,
    val yieldAboveMid20: Boolean = false,
    val yieldTop20: Boolean = false
) : ScreenerFilter {
    fun isNetAssetEmpty(): Boolean = !netAssetsLessThan100 && !netAssets100to500 && !netAssets500to1 && !netAssets1to2 && !netAssetsGreaterThan2

    fun isYieldEmpty(): Boolean = !yieldBottom20 && !yieldBelowMid20 && !yieldMid20 && !yieldAboveMid20 && !yieldTop20

    override fun isEmpty(): Boolean = isNetAssetEmpty() && isYieldEmpty() && region.isAllItemsElement && regionPercentInvested.isAllItemsElement &&
        country.isAllItemsElement && countryPercentInvested.isAllItemsElement && industryType.isAllItemsElement &&
        industryTypePercentInvested.isAllItemsElement

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.ETF
}
