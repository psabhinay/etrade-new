package com.etrade.mobilepro.quote.screen.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.fragment.app.createViewModelLazy
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.chartiq.presentation.ChartIqDelegate
import com.etrade.mobilepro.compose.components.FragmentBasedComposable
import com.etrade.mobilepro.earnings.EarningsDetailsViewDelegate
import com.etrade.mobilepro.etcompose.tabhorizontalpager.PagerItem
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.optionschain.presentation.OptionsChainViewDelegate
import com.etrade.mobilepro.quote.presentation.QuoteOverviewEventListener
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.quote.screen.presentation.fees.FundFeesViewDelegate
import com.etrade.mobilepro.quote.screen.presentation.qualifiers.QuoteOverview
import com.etrade.mobilepro.quote.screen.presentation.qualifiers.QuoteViewModelClass
import com.etrade.mobilepro.quote.screen.presentation.ui.MainContent
import com.etrade.mobilepro.quote.screen.presentation.ui.performance.FundPerformanceViewDelegate
import com.etrade.mobilepro.quoteapi.MobileQuoteDataHolder
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.userviewmodel.UserViewModel
import kotlinx.coroutines.flow.collect
import javax.inject.Inject
import kotlin.reflect.KClass

interface BottomSheetBackPressDelegate {
    fun onBackPressed(onBackPress: () -> Unit)
}

private const val CHART_INDEX = 1
private const val LAST_PAGER_INDEX = 3

@Suppress("LongParameterList")
class QuoteTabsFragment @Inject constructor(
    private val argsDelegate: ArgsDelegate,
    private val userViewModel: UserViewModel,
    private val viewModelFactory: ViewModelProvider.Factory,
    private val fragmentBasedComposable: FragmentBasedComposable,
    @QuoteOverview private val composableQuoteOverviewParams: FragmentBasedComposable.Params,
    @QuoteViewModelClass private val quoteViewModelClass: KClass<out ViewModel>,
    chartIqDelegateFactory: ChartIqDelegate.Factory,
    optionsChainDelegateFactory: OptionsChainViewDelegate.Factory,
    earningsViewDelegateFactory: EarningsDetailsViewDelegate.Factory,
    fundPerformanceDelegateFactory: FundPerformanceViewDelegate.Factory,
    feeViewDelegateFactory: FundFeesViewDelegate.Factory
) : Fragment(), BottomSheetBackPressDelegate, QuoteOverviewEventListener {

    private val symbol: SearchResultItem.Symbol
        get() = argsDelegate.extractSymbol(this)

    private val viewModel: QuoteTabsViewModel by viewModels { viewModelFactory }

    private val quoteViewModel by createViewModelLazy(
        quoteViewModelClass,
        { viewModelStore }
    ) {
        viewModelFactory
    }

    private val quoteWidgetViewModel by viewModels<QuotesWidgetViewModel> { viewModelFactory }

    private val mobileQuoteDataHolder by lazy {
        (quoteViewModel as? MobileQuoteDataHolder)
    }

    private val chartIqDelegate by lazy {
        chartIqDelegateFactory.create(this, symbol, mobileQuoteDataHolder?.mobileQuoteDataFlow)
    }

    private val optionsChainDelegate by lazy {
        optionsChainDelegateFactory.create(this, symbol)
    }

    private val earningsDetailsViewDelegate by lazy {
        earningsViewDelegateFactory.create(this, symbol)
    }

    private val fundPerformanceDelegate by lazy {
        fundPerformanceDelegateFactory.create(this, symbol)
    }

    private val feesViewDelegate by lazy {
        feeViewDelegateFactory.create(this, symbol, mobileQuoteDataHolder?.mobileQuoteDataFlow)
    }

    private var selectedPageIndex = 0

    private val delegatesMap by lazy {
        mapOf(
            1 to chartIqDelegate,
            2 to optionsChainDelegate,
            LAST_PAGER_INDEX to earningsDetailsViewDelegate
        )
    }

    private val loginAbortListener: () -> Boolean = {
        viewModel.navigateToIndex(selectedPageIndex)
        true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        optionsChainDelegate.onCreate(savedInstanceState, ::refreshQuotes)
        return ComposeView(requireContext()).apply {
            setContent {
                MainContent(
                    pagerItems = pagerItems(),
                    onTabChangedListener = {
                        var indexToSelect = it
                        if (it == LAST_PAGER_INDEX) {
                            chartIqDelegate.retainWebView()
                            if (!userViewModel.isAuthenticated) {
                                userViewModel.onLoginAbortedListenerSet.add(loginAbortListener)
                                navigateToEarningDetails()
                                indexToSelect = selectedPageIndex
                            }
                        }
                        delegatesMap.forEach { (index, tabVisibilityListener) ->
                            tabVisibilityListener.onVisibilityChange(index == it)
                        }
                        selectedPageIndex = indexToSelect
                    },
                    pageScrollLiveData = viewModel.scrollToPageEvent
                )
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        chartIqDelegate.onViewCreated()
        feesViewDelegate.onViewCreated()
        mobileQuoteDataHolder?.quoteWidgetItemFlow?.let {
            lifecycleScope.launchWhenStarted {
                it.collect { quoteWidgetItem ->
                    quoteWidgetViewModel.updateQuotes(quoteWidgetItem)
                }
            }
        }
        fundPerformanceDelegate.onViewCreated()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        optionsChainDelegate.onDestroyView()
    }

    override fun onStop() {
        chartIqDelegate.retainWebView()
        super.onStop()
    }

    override fun onResume() {
        super.onResume()
        chartIqDelegate.placeRetainedWebViewIfNecessary()
    }

    @Composable
    private fun pagerItems() = listOf(
        PagerItem(
            title = getString(R.string.quote_tab_title_overview),
            content = {
                fragmentBasedComposable.Content(
                    fragmentManager = childFragmentManager,
                    isLazilyHoisted = true,
                    params = composableQuoteOverviewParams
                )
            }
        ),
        PagerItem(
            title = getString(R.string.quote_tab_title_chart),
            content = {
                chartIqDelegate.ChartIqCompose()
            }
        ),
        if (symbol.type.isMutualFundFund) {
            PagerItem(
                title = getString(R.string.quote_performance),
                content = {
                    fundPerformanceDelegate.FundPerformanceQuoteViewCompose()
                }
            )
        } else {
            PagerItem(
                title = getString(R.string.options),
                content = {
                    optionsChainDelegate.OptionsChainCompose()
                }
            )
        },
        if (symbol.type == InstrumentType.MF) {
            feesPagerItem()
        } else {
            earningsPagerItem()
        }
    )

    @Composable
    private fun feesPagerItem(): PagerItem = PagerItem(
        title = getString(R.string.fees),
        content = {
            feesViewDelegate.FundFeesViewCompose()
        }
    )

    @Composable
    private fun earningsPagerItem() = PagerItem(
        title = getString(R.string.earnings),
        content = {
            if (userViewModel.isAuthenticated) {
                earningsDetailsViewDelegate.EarningsDetailsViewCompose()
            } else {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier.fillMaxSize()
                ) {
                    Column {
                        Text("")
                    }
                }
            }
        }
    )

    override fun onBackPressed(onBackPress: () -> Unit) {
        if (selectedPageIndex == 1) {
            chartIqDelegate.onBackPressed(onBackPress)
        } else {
            onBackPress()
        }
    }

    override fun onChartClicked() {
        viewModel.navigateToIndex(CHART_INDEX)
    }

    private fun refreshQuotes() {
        mobileQuoteDataHolder?.refresh()
    }

    private fun navigateToEarningDetails() {
        viewModel.indexToNavigateTo = LAST_PAGER_INDEX
        findNavController().navigate(R.id.activity_login)
    }

    // TODO: ETAND-18590 - Delete this interface once :app:quote graph is migrated to this module
    interface ArgsDelegate {
        fun extractSymbol(fragment: QuoteTabsFragment): SearchResultItem.Symbol
    }
}
