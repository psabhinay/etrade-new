package com.etrade.mobilepro.marketscalendar.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.util.android.CalendarEvent
import io.reactivex.disposables.CompositeDisposable
import org.threeten.bp.Instant
import org.threeten.bp.ZonedDateTime
import javax.inject.Inject

class CalendarEventDetailsViewModel @Inject constructor() : ViewModel() {

    private val _calendarEvent: MediatorLiveData<CalendarEvent> = MediatorLiveData()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    internal var currentDateTime: ZonedDateTime = ZonedDateTime.now()

    val item: MutableLiveData<EventDetails> = MutableLiveData()

    val addToCalendarActionVisibility: LiveData<Boolean> = Transformations.map(item) {
        it.date?.let { date ->
            val eventDateTime: ZonedDateTime = ZonedDateTime.from(EVENTS_DATE_TIME_FORMATTER.parse(date))

            eventDateTime.isAfter(currentDateTime)
        } ?: false
    }

    init {
        _calendarEvent.addSource(addToCalendarActionVisibility) { isVisible ->
            if (isVisible) {
                item.value?.also { details ->
                    getCalendarEvent(details)?.run {
                        _calendarEvent.value = this
                    }
                }
            }
        }
    }

    private fun getCalendarEvent(item: EventDetails): CalendarEvent? = item.date?.let { date ->
        item.title?.let { title ->
            val eventDateTime: ZonedDateTime = ZonedDateTime.parse(date, EVENTS_DATE_TIME_FORMATTER)
            val instant: Instant = eventDateTime.toInstant()
            CalendarEvent(title, instant.toEpochMilli())
        }
    }

    override fun onCleared() {
        compositeDisposable.clear()

        super.onCleared()
    }

    fun getCalendarEvent(): LiveData<CalendarEvent> = _calendarEvent
}
