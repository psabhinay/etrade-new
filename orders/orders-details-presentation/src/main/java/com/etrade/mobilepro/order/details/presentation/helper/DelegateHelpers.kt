package com.etrade.mobilepro.order.details.presentation.helper

import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem
import com.etrade.mobilepro.order.details.presentation.R
import com.etrade.mobilepro.quoteapi.QuoteBidAskItem

internal fun mergeResult(orderItems: List<OrderDetailItemInterface>, quoteBidAskItem: QuoteBidAskItem) {
    orderItems.filterIsInstance<OrderDetailDisplayItem.OrderBidAskItem.StaticOrderBidAskItem>().firstOrNull()
        ?.updateWithQuote(quoteBidAskItem)
}

internal fun OrderDetailDisplayItem.OrderBidAskItem.StaticOrderBidAskItem.updateWithQuote(quoteWidgetItem: QuoteBidAskItem) = apply {
    val askValue = quoteWidgetItem.askPrice.value
    ask = askValue?.price.orEmpty()
    askSize = askValue?.size.orEmpty()

    val bidValue = quoteWidgetItem.bidPrice.value
    bid = bidValue?.price.orEmpty()
    bidSize = bidValue?.size.orEmpty()
}

internal fun getStaticBidAskDelegate(orderDetail: BaseOrderDetailsItem.OrderDetailsItem): OrderDetailDisplayItem.OrderBidAskItem.StaticOrderBidAskItem {
    val bidLabel: Int
    val askLabel: Int
    if (orderDetail.hasMultipleLegs) {
        bidLabel = R.string.net_bid_label_text
        askLabel = R.string.net_ask_title_text
    } else {
        bidLabel = R.string.bid_title_text
        askLabel = R.string.ask_title_text
    }
    return OrderDetailDisplayItem.OrderBidAskItem.StaticOrderBidAskItem(
        bidLabel, askLabel,
        orderDetail.netBid.toString(), "",
        orderDetail.netAsk.toString(), ""
    )
}

internal fun getDefaultStaticBidAskDelegate() =
    OrderDetailDisplayItem.OrderBidAskItem.StaticOrderBidAskItem(R.string.bid_title_text, R.string.ask_title_text, "", "", "", "")

internal fun BaseOrderDetailsItem.OrderDetailsItem.needToFetchStrategy(): Boolean {
    return strategyType == null && orderDescriptions.firstOrNull { it.typeCode.isOption } != null
}
