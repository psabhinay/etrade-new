package com.etrade.mobilepro.order.details.api

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.TradeOrderType

class OrderData(
    val formValues: InputValuesSnapshot,
    val inputsDisabled: Array<String>,
    val tradeOrderType: TradeOrderType
)
