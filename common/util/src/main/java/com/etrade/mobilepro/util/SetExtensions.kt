package com.etrade.mobilepro.util

/**
 * Calculates [symmetric difference](https://en.wikipedia.org/wiki/Symmetric_difference) between [this] and [other] sets.
 *
 * @param T Type of values in a set.
 * @param other The other symmetric difference.
 *
 * @return A set containing a symmetric difference of two sets.
 */
fun <T> Set<T>.symmetricDifference(other: Set<T>): Set<T> {
    return when {
        this.isEmpty() -> other
        other.isEmpty() -> this
        else -> (this - other) union (other - this)
    }
}
