package com.etrade.mobilepro.news.caching

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.dynamicui.CachedResponseModel
import com.etrade.mobilepro.news.briefing.data.rest.BriefingNewsApiClient
import com.etrade.mobilepro.news.briefing.data.rest.BriefingNewsRequest
import io.reactivex.Single
import io.realm.Realm
import retrofit2.Retrofit
import javax.inject.Inject

class BriefingNewsDataSourceDelegate @Inject constructor(
    @MobileTrade retrofit: Retrofit
) : SimpleCachedResource.DataSourceDelegate<CachedResponseModel, BriefingNewsRequest> {

    private val apiClient = retrofit.create(BriefingNewsApiClient::class.java)

    override fun fetchFromNetwork(id: BriefingNewsRequest): Single<CachedResponseModel> =
        apiClient
            .getBriefingNews()
            .map {
                val cachedModel = CachedResponseModel(
                    id = id.contentId,
                    retrievedTimestamp = System.currentTimeMillis(),
                    response = it
                )
                cachedModel
            }

    override fun putToCache(fetchedResult: CachedResponseModel): Single<CachedResponseModel> = Single.fromCallable {
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction { realmTxn ->
                realmTxn.insertOrUpdate(fetchedResult)
            }
        }
        fetchedResult
    }

    override fun fetchFromDb(id: BriefingNewsRequest): Single<CachedResponseModel> = Single.fromCallable {
        Realm.getDefaultInstance().use { realm ->
            val cachedResponse = realm
                .where(CachedResponseModel::class.java)
                .equalTo("id", id.contentId)
                .findFirst() ?: throw NoSuchElementException()
            realm.copyFromRealm(cachedResponse)
        }
    }
}
