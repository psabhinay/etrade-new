package com.etrade.mobilepro.order.details.dto;

import com.etrade.mobilepro.backends.neo.BaseDataDto;
import com.etrade.mobilepro.orders.data.dto.AdvancedOrderDto;
import com.etrade.mobilepro.orders.data.dto.AuditInfoDto;
import com.etrade.mobilepro.orders.data.dto.HeaderDto;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonClass;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001B7\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0001\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\n\b\u0001\u0010\t\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\bH\u00c6\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\bH\u00c6\u0003J;\u0010\u0016\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\u000e\b\u0003\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0003\u0010\t\u001a\u0004\u0018\u00010\bH\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\bH\u00d6\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\t\u001a\u0004\u0018\u00010\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010\u00a8\u0006\u001d"}, d2 = {"Lcom/etrade/mobilepro/order/details/dto/OrderDetailsDto;", "", "orderDetails", "Lcom/etrade/mobilepro/order/details/dto/OrderDetailsItemDto;", "events", "", "Lcom/etrade/mobilepro/order/details/dto/EventDto;", "preparedOrderId", "", "preparedStatus", "(Lcom/etrade/mobilepro/order/details/dto/OrderDetailsItemDto;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V", "getEvents", "()Ljava/util/List;", "getOrderDetails", "()Lcom/etrade/mobilepro/order/details/dto/OrderDetailsItemDto;", "getPreparedOrderId", "()Ljava/lang/String;", "getPreparedStatus", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "orders-details-data"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class OrderDetailsDto {
    @org.jetbrains.annotations.NotNull()
    private final com.etrade.mobilepro.order.details.dto.OrderDetailsItemDto orderDetails = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.etrade.mobilepro.order.details.dto.EventDto> events = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String preparedOrderId = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String preparedStatus = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.OrderDetailsDto copy(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderDetails")
    com.etrade.mobilepro.order.details.dto.OrderDetailsItemDto orderDetails, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "events")
    java.util.List<com.etrade.mobilepro.order.details.dto.EventDto> events, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "preparedOrderId")
    java.lang.String preparedOrderId, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "preparedStatus")
    java.lang.String preparedStatus) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public OrderDetailsDto(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "orderDetails")
    com.etrade.mobilepro.order.details.dto.OrderDetailsItemDto orderDetails, @org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "events")
    java.util.List<com.etrade.mobilepro.order.details.dto.EventDto> events, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "preparedOrderId")
    java.lang.String preparedOrderId, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "preparedStatus")
    java.lang.String preparedStatus) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.OrderDetailsItemDto component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.etrade.mobilepro.order.details.dto.OrderDetailsItemDto getOrderDetails() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.order.details.dto.EventDto> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.etrade.mobilepro.order.details.dto.EventDto> getEvents() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPreparedOrderId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPreparedStatus() {
        return null;
    }
}