package com.etrade.mobilepro.login.api.repo

import com.etrade.mobilepro.common.result.ETResult

/**
 * This interface can be used to session related backend communication.
 */
interface LoginRepo {

    /**
     * This function can be used to login to E-TRADE systems and notify
     * the application with the result of the login call.
     *
     * @param username The display name of a user.
     * @param password The password for the user to log in.
     *
     */
    suspend fun login(username: String, password: String): ETResult<LoginResult>
}
