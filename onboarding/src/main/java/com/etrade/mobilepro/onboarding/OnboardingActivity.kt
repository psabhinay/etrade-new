package com.etrade.mobilepro.onboarding

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.etrade.mobilepro.common.extension.screenWidthPixels
import com.etrade.mobilepro.imageloader.ImageLoader
import com.etrade.mobilepro.onboarding.databinding.OnboardingActivityBinding
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.localytics.androidx.Localytics
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject

private const val LOGIN_PATH = "/login"
private const val OPEN_ACCOUNT_PATH = "/open_account"
private val logger: Logger = LoggerFactory.getLogger(OnboardingActivity::class.java)

class OnboardingActivity : AppCompatActivity(), HasAndroidInjector {

    private val binding by viewBinding(OnboardingActivityBinding::inflate)

    @Inject
    lateinit var applicationPreferences: ApplicationPreferences

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var imageLoader: ImageLoader

    @Inject
    lateinit var onboardingViewModel: OnboardingViewModel

    @Inject
    lateinit var localyticsOnboardingViewModel: LocalyticsOnboardingViewModel

    @Inject
    lateinit var onboardingAction: OnboardingAction

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)

        applicationPreferences.shouldDisplayOnboarding = false

        setContentView(binding.root)

        setupViewModel()

        onboardingViewModel.showLocalyticsInAppMessage()

        showLoading()
    }

    private fun showLoading() {
        binding.progressIndicator.show()
        hideButtons()
    }

    private fun hideButtons() {
        setButtonsVisibility(View.GONE)
    }

    private fun displayButtons() {
        setButtonsVisibility(View.VISIBLE)
    }

    private fun setButtonsVisibility(visibility: Int) {
        binding.openAnAccount.visibility = visibility
        binding.logOn.visibility = visibility
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Localytics.onNewIntent(this, intent)

        when (intent?.data?.path) {
            LOGIN_PATH -> {
                onboardingViewModel.trackOnboardingLogin()
                onboardingAction.logonAction(this)
            }
            OPEN_ACCOUNT_PATH -> {
                onboardingViewModel.trackOnboardingOpenAccount()
                onboardingAction.openAnAccountAction(this)
            }
            else -> {
                logger.error("Invalid link path")
                onboardingAction.logonAction(this)
            }
        }
    }

    private fun setupViewModel() {
        with(onboardingViewModel) {
            onboardingPages.observe(this@OnboardingActivity, Observer { showOnboardingPages(it) })
            dismissInAppMessageSignal.observe(this@OnboardingActivity, Observer { handleInAppMessageDismiss(it) })
        }
    }

    private fun handleInAppMessageDismiss(dismissSignal: ConsumableLiveEvent<Unit>?) {
        dismissSignal?.consume {
            applicationPreferences.isLocalyticsOnboardingShow = true
            onboardingViewModel.trackOnboardingDismiss()
            onboardingAction.logonAction(this)
        }
    }

    private fun showOnboardingPages(pages: List<OnboardingPage>?) {
        val currentPages = pages ?: return
        setupViewPager(currentPages)

        displayButtons()
        binding.progressIndicator.hide()
        binding.openAnAccount.setOnClickListener {
            onboardingViewModel.trackOnboardingOpenAccount()
            onboardingAction.openAnAccountAction(this)
        }
        binding.logOn.setOnClickListener {
            onboardingViewModel.trackOnboardingLogin()
            onboardingAction.logonAction(this)
        }
    }

    private fun setupViewPager(onboardingPages: List<OnboardingPage>) {
        with(binding) {
            val parallaxView =
                parallaxView.apply { layoutParams.width = screenWidthPixels * onboardingPages.size }

            viewPager.adapter = OnboardingViewPagerAdapter(onboardingPages, imageLoader)
            tabs.setupWithViewPager(viewPager)

            viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

                override fun onPageScrollStateChanged(state: Int) {
                    // nop
                }

                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    val x =
                        ((viewPager.width * position + positionOffsetPixels) * computeFactor()).toInt()
                    horizontalScrollView.scrollTo(x, 0)
                }

                override fun onPageSelected(position: Int) {
                    onboardingViewModel.onboardingPageViewed(position)
                }

                private fun computeFactor(): Float {
                    return (parallaxView.width - viewPager.width) / (
                        viewPager.width * (
                            viewPager.adapter?.count
                                ?: 0 - 1
                            )
                        ).toFloat()
                }
            })
        }
    }
}
