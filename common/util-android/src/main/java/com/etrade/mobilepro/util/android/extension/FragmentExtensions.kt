package com.etrade.mobilepro.util.android.extension

import android.os.Bundle
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.whenStarted
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.util.android.ViewModelWithIdLazy
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

val Fragment.contentView: View?
    get() = activity?.findViewById(android.R.id.content)

val Fragment.viewCoroutineScope: CoroutineScope
    get() = viewLifecycleOwner.lifecycle.coroutineScope

@MainThread
inline fun <reified VM : ViewModel> Fragment.navGraphViewModelsWithId(
    @IdRes navGraphId: Int,
    viewModelId: String,
    noinline factoryProducer: (() -> ViewModelProvider.Factory)? = null
): Lazy<VM> {
    val backStackEntry by lazy {
        findNavController().getBackStackEntry(navGraphId)
    }
    val storeProducer: () -> ViewModelStore = {
        backStackEntry.viewModelStore
    }
    return createViewModelWithIdLazy(
        VM::class, viewModelId, storeProducer,
        {
            factoryProducer?.invoke() ?: backStackEntry.defaultViewModelProviderFactory
        }
    )
}

@MainThread
fun <VM : ViewModel> Fragment.createViewModelWithIdLazy(
    viewModelClass: KClass<VM>,
    viewModelId: String,
    storeProducer: () -> ViewModelStore,
    factoryProducer: (() -> ViewModelProvider.Factory)? = null
): Lazy<VM> {
    val factoryPromise = factoryProducer ?: {
        defaultViewModelProviderFactory
    }
    return ViewModelWithIdLazy(viewModelClass, viewModelId, storeProducer, factoryPromise)
}

/**
 * Safely postpones the action until after the fragment has started.
 * If the fragment is stopped before running the action will not be run.
 */
fun Fragment.afterStarted(action: () -> Unit) {
    viewLifecycleOwner.lifecycleScope.launch {
        // Safely postpones showing the dialog until the activity has time to change orientation. Otherwise the dialog may be "stuck"
        // in a landscape size.
        delay(1)
        whenStarted {
            action()
        }
    }
}

/**
 * Lifecycle aware utility which causes the action to be added
 * to the message queue on the user interface thread.
 */
fun Fragment.post(action: suspend () -> Unit) {
    view?.post viewPost@{
        // Return here if [view] is null after executing view.post. We cannot access
        // viewLifecycleOwner if [view] is null.
        view ?: return@viewPost
        viewLifecycleOwner.lifecycle.coroutineScope.launch(Dispatchers.Main) {
            action()
        }
    }
}

/**
 * Suspends the current coroutine scope until the main thread is available to enqueue messages again.
 */
suspend fun Fragment.awaitForMainThread() {
    suspendCoroutine<Unit> { cont ->
        post { cont.resume(Unit) }
    }
}

fun <T> Fragment.mutableArgs(initializer: () -> T, onArgsAccessed: (args: Bundle?) -> Bundle?) =
    MutableArgsDelegate(initializer, onArgsAccessed)

class MutableArgsDelegate<T>(
    private val initializer: () -> T,
    private val onArgsAccessed: (args: Bundle?) -> Bundle?
) : ReadOnlyProperty<Fragment, T> {
    private var value: T? = null
    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        val result = initializer()
        thisRef.arguments = onArgsAccessed(thisRef.arguments)
        return result
    }
}
