package com.etrade.mobilepro.dagger

import com.etrade.eo.queueprocessor.QueueProcessor
import com.etrade.eo.queueprocessor.QueueProcessorProvider
import com.etrade.eo.queueprocessor.SerializingScheduler
import com.etrade.eo.queueprocessor.SerializingSchedulerFactory
import com.etrade.eo.streaming.LSClient
import com.etrade.eo.streaming.SubscriptionManager
import com.etrade.eo.streaming.UpdateInfo
import com.etrade.eo.streaming.lightstreamer.LsClientFactory
import com.etrade.eo.streaming.lightstreamer.LsSubscriptionManagerBuilder
import com.etrade.eo.streaming.rx.RxStreamingStatusProvider
import com.etrade.mobilepro.appconfig.api.repo.AppConfigRepo
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.common.di.Level1Streamer
import com.etrade.mobilepro.common.di.Level2Streamer
import com.etrade.mobilepro.dao.PositionsDaoProvider
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.DefaultLevel1StreamIdFactoryParameters
import com.etrade.mobilepro.streaming.DefaultLevel2StreamIdFactoryParameters
import com.etrade.mobilepro.streaming.DefaultStreamingSessionDelegateHelper
import com.etrade.mobilepro.streaming.DefaultStreamingStatusController
import com.etrade.mobilepro.streaming.StreamingSessionDelegate
import com.etrade.mobilepro.streaming.StreamingSessionDelegateHelper
import com.etrade.mobilepro.streaming.StreamingSessionDelegateImpl
import com.etrade.mobilepro.streaming.StreamingStatusNotifierDelegate
import com.etrade.mobilepro.streaming.StreamingStatusNotifierDelegateImpl
import com.etrade.mobilepro.streaming.StreamingSubscriptionControllerImpl
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.Level2DataEvent
import com.etrade.mobilepro.streaming.api.StreamIdFactory
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import com.etrade.mobilepro.streaming.base.DefaultSubscriptionManagerHelper
import com.etrade.mobilepro.streaming.base.SubscriptionManagerHelper
import com.etrade.mobilepro.streaming.lightstreamer.LightstreamerFieldMapper
import com.etrade.mobilepro.streaming.lightstreamer.level1.Level1LightstreamerFieldMapper
import com.etrade.mobilepro.streaming.lightstreamer.level1.Level1StreamIdFactory
import com.etrade.mobilepro.streaming.lightstreamer.level1.Level1StreamingQuoteProvider
import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2BookType
import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2Data
import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2StreamIdFactory
import com.etrade.mobilepro.streaming.lightstreamer.level2.Level2StreamingQuoteProvider
import com.etrade.mobilepro.streaming.positions.cache.StreamingQuoteProviderWithCachedPositions
import com.example.lightstreamer.mergemode.L2BookResponseMapper
import com.example.lightstreamer.mergemode.L2DecoratorSubscriptionManagerHelper
import com.google.common.util.concurrent.ThreadFactoryBuilder
import dagger.Binds
import dagger.Module
import dagger.Provides
import java.util.Collections
import java.util.concurrent.Executors
import javax.inject.Singleton

internal const val LIGHT_STREAMER_ADAPTER_SET = "DHLSADAPTER"
private const val LIGHT_STREAMER_LEVEL2_DATA_ADAPTER = "L2_MERGE_ADAPTER"

/* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */
@Module(
    includes = [
        ApplicationModule::class,
        AppWidgetModule::class
    ]
)
@Suppress("TooManyFunctions")
abstract class StreamingModule {

    @Binds
    @Singleton
    abstract fun bindLevel1LightstreamerFieldMapper(mapper: Level1LightstreamerFieldMapper): LightstreamerFieldMapper<Level1Field>

    @Binds
    @Singleton
    abstract fun bindLevel1StreamIdFactory(factory: Level1StreamIdFactory): StreamIdFactory<Any>

    @Binds
    @Singleton
    abstract fun bindLevel1StreamIdFactoryParameters(parameters: DefaultLevel1StreamIdFactoryParameters): Level1StreamIdFactory.Parameters

    @Binds
    @Singleton
    abstract fun bindLevel1StreamingQuoteProvider(provider: Level1StreamingQuoteProvider): StreamingQuoteProvider<Level1Field, Level1Data, Any>

    @Binds
    @Singleton
    abstract fun bindLevel2StreamIdFactory(factory: Level2StreamIdFactory): StreamIdFactory<Level2BookType>

    @Binds
    @Singleton
    abstract fun bindLevel2StreamIdFactoryParameters(parameters: DefaultLevel2StreamIdFactoryParameters): Level2StreamIdFactory.Parameters

    @Binds
    @Singleton
    abstract fun bindStreamingStatusController(controller: DefaultStreamingStatusController): StreamingStatusController

    @Binds
    @Singleton
    @Level1Streamer
    abstract fun bindLevel1LSClient(lsClient: LSClient): LSClient

    @Binds
    @Singleton
    @Level2Streamer
    abstract fun bindLevel2LSClient(lsClient: LSClient): LSClient

    @Binds
    @Singleton
    abstract fun bindStreamingSessionDelegateHelper(streamingSessionDelegateHelper: DefaultStreamingSessionDelegateHelper): StreamingSessionDelegateHelper

    companion object {
        @Provides
        @Singleton
        internal fun provideLevel2StreamingQuoteProvider(
            streamIdFactory: Level2StreamIdFactory,
            subscriptionManagerHelper: SubscriptionManagerHelper<Level2DataEvent>,
            streamFactoryParams: Level2StreamIdFactory.Parameters
        ): StreamingQuoteProvider<String, Level2Data, Level2BookType> =
            Level2StreamingQuoteProvider(
                streamIdFactory,
                subscriptionManagerHelper,
                streamFactoryParams
            )

        @Provides
        @Singleton
        internal fun provideLevel1StreamingQuoteProviderWithCachedPositions(
            daoProvider: PositionsDaoProvider,
            streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>
        ): StreamingQuoteProviderWithCachedPositions {
            return StreamingQuoteProviderWithCachedPositions(daoProvider, streamingQuoteProvider)
        }

        @Provides
        internal fun provideDefaultStreamingSubscriptionController(
            streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>
        ): StreamingSubscriptionController {
            return StreamingSubscriptionControllerImpl(streamingQuoteProvider)
        }

        @Provides
        @Singleton
        fun provideLevel1SubscriptionManagerHelper(@Level1Streamer subscriptionManager: SubscriptionManager): SubscriptionManagerHelper<UpdateInfo> {
            return DefaultSubscriptionManagerHelper(subscriptionManager) {
                it
            }
        }

        @Provides
        @Singleton
        fun provideLevel2SubscriptionManagerHelper(@Level2Streamer subscriptionManager: SubscriptionManager): SubscriptionManagerHelper<Level2DataEvent> {
            val mergeMapper = L2BookResponseMapper()
            val underlyingHelper = DefaultSubscriptionManagerHelper(
                subscriptionManager = subscriptionManager,
                lsDataAdapter = LIGHT_STREAMER_LEVEL2_DATA_ADAPTER,
                updateInfoParser = { it }
            )
            return L2DecoratorSubscriptionManagerHelper(mergeMapper, underlyingHelper)
        }

        @Provides
        @Singleton
        fun provideQueueProcessor(provider: QueueProcessorProvider): QueueProcessor =
            provider.createQueueProcessor()

        @Provides
        @Singleton
        fun provideQueueProcessorProvider(): QueueProcessorProvider = QueueProcessorProvider()

        @Provides
        @Singleton
        fun provideSerializingScheduler(
            factory: SerializingSchedulerFactory,
            processor: QueueProcessor
        ): SerializingScheduler = factory.create(processor)

        @Provides
        @Singleton
        fun provideSerializingSchedulerFactory(): SerializingSchedulerFactory {
            val threadFactory = ThreadFactoryBuilder()
                .setDaemon(false)
                .setNameFormat("serializing-scheduler-%d")
                .build()
            return SerializingSchedulerFactory(
                Executors.newSingleThreadScheduledExecutor(
                    threadFactory
                )
            )
        }

        @Provides
        @Singleton
        @Level1Streamer
        fun provideLevel1SubscriptionManager(
            processor: QueueProcessor,
            @Level1Streamer lsClient: LSClient
        ): SubscriptionManager = LsSubscriptionManagerBuilder(
            queueProcessor = processor,
            lsClient = lsClient
        ).build()

        @Provides
        @Singleton
        @Level2Streamer
        fun provideLevel2SubscriptionManager(
            processor: QueueProcessor,
            @Level2Streamer lsClient: LSClient
        ): SubscriptionManager = LsSubscriptionManagerBuilder(
            queueProcessor = processor,
            lsClient = lsClient
        ).build()

        @Provides
        @Singleton
        @Level1Streamer
        fun provideLevel1StreamingSessionDelegate(
            appConfigRepo: AppConfigRepo,
            user: User,
            @Level1Streamer lsClient: LSClient,
            streamingStatusController: StreamingStatusController
        ): StreamingSessionDelegate {
            val streamerUrlMediator = {
                val defaultStreamerUrl = appConfigRepo.getAppConfig().baseUrls.level1StreamerUrl
                val level1StreamerUrl = user.session?.level1StreamerUrl
                user.getValidStreamerUrl(
                    streamerUrl = level1StreamerUrl,
                    defaultStreamerUrl = defaultStreamerUrl
                )
            }
            return StreamingSessionDelegateImpl(
                user = user,
                lsClient = lsClient,
                streamerAdapterSet = LIGHT_STREAMER_ADAPTER_SET,
                streamerUrlMediator = streamerUrlMediator,
                streamingStatusController = streamingStatusController
            )
        }

        @Provides
        @Singleton
        @Level2Streamer
        fun provideLevel2StreamingSessionDelegate(
            appConfigRepo: AppConfigRepo,
            user: User,
            @Level2Streamer lsClient: LSClient,
            streamingStatusController: StreamingStatusController
        ): StreamingSessionDelegate {
            val streamerUrlMediator = {
                val defaultStreamerUrl = appConfigRepo.getAppConfig().baseUrls.level2StreamerUrl
                val level2StreamerUrl = user.session?.level2StreamerUrl
                user.getValidStreamerUrl(
                    streamerUrl = level2StreamerUrl,
                    defaultStreamerUrl = defaultStreamerUrl
                )
            }
            return StreamingSessionDelegateImpl(
                user = user,
                lsClient = lsClient,
                streamerAdapterSet = LIGHT_STREAMER_ADAPTER_SET,
                streamerUrlMediator = streamerUrlMediator,
                streamingStatusController = streamingStatusController
            )
        }

        @Provides
        fun provideStreamingStatusNotifierDelegate(
            @Level1Streamer subscriptionManager: SubscriptionManager,
            streamingStatusController: StreamingStatusController,
            user: User
        ): StreamingStatusNotifierDelegate = StreamingStatusNotifierDelegateImpl(
            rxStreamingStatusProvider = RxStreamingStatusProvider(subscriptionManager),
            streamingStatusController = streamingStatusController,
            user = user
        )

        @Provides
        fun provideLSClient(applicationInfo: ApplicationInfo): LSClient =
            LsClientFactory.createLsClient(
                httpHeadersOnSessionCreationOnly = Collections.singletonMap(
                    "appIdTag",
                    "DType:${applicationInfo.deviceType}"
                )
            )
    }
}
