package com.etrade.mobilepro.markets.futures

import android.content.res.Resources
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.eo.core.util.forEachNotNull
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.markets.common.MarketsBaseViewModel
import com.etrade.mobilepro.markets.common.SymbolRowInfo
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.data.response.FutureQuote
import com.etrade.mobilepro.markets.tableHeaders
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.rx.awaitResult
import com.etrade.mobilepro.util.safeParseBigDecimal
import javax.inject.Inject

class MarketsFuturesViewModel @Inject constructor(
    marketsService: MoverService,
    resources: Resources,
    dataColorResolver: DataColorResolver
) : MarketsBaseViewModel(marketsService, dataColorResolver) {

    override val viewModelType = MarketsFuturesViewModel::class.java
    override val columnHeaderList by lazy { resources.tableHeaders() }
    private val listOfSymbols = mutableListOf<SymbolCell>()

    init {
        fetch()
    }

    override suspend fun fetchDataFromServer(): ViewState {
        listOfSymbols.clear()
        return marketsService.getMarketFutures().toObservable().awaitResult().fold(
            onSuccess = {
                val quotes = it.quotes
                if (quotes.isNullOrEmpty()) {
                    return ViewState.Empty
                }
                val listOfCells = mutableListOf<List<ResultDataCell>>()
                addQuotes(quotes.toMutableList(), listOfSymbols, listOfCells)
                ViewState.Success(listOfSymbols, listOfCells, quotes[quotes.size - 1].expireDate)
            },
            onFailure = { ViewState.Error("Markets futures fetch failed", it) }
        )
    }

    private fun addQuotes(
        quotes: MutableList<FutureQuote?>,
        listOfSymbols: MutableList<SymbolCell>,
        listOfCells: MutableList<List<ResultDataCell>>
    ) {
        quotes.forEachNotNull { quote ->
            val symbol = quote.symbol
            val price = quote.price
            if (symbol != null && price != null) {
                val instrumentType = InstrumentType.from("EQ")
                listOfSymbols.add(
                    SymbolCell(
                        symbol,
                        getCompanyName(symbol),
                        symbol,
                        instrumentType,
                        null
                    )
                )
                val change = quote.change?.safeParseBigDecimal()
                val symbolCells = buildSymbolRowItem(
                    SymbolRowInfo(
                        symbol = symbol,
                        price = MarketDataFormatter.formatMoneyValue(price.toBigDecimalOrNull()),
                        change = MarketDataFormatter.formatMarketDataCustom(change),
                        percentChange = quote.percentChange(),
                        tickIndicator = (change?.signum() ?: -1).toShort(),
                        volume = null
                    ),
                    fromMoversResponse = true,
                    instrumentType = instrumentType
                )
                listOfCells.add(symbolCells)
            }
        }
    }

    // We have to use this mapping method to get the short desc, neither xml nor json service return the value
    private fun getCompanyName(symbol: String): String {
        return when (symbol) {
            "YM" -> "DJIA Index"
            "NQ" -> "NASDAQ 100"
            "ES" -> "S&P 500"
            else -> symbol
        }
    }
}

private fun FutureQuote.percentChange(): String {
    val percentChangeWithoutSign = percentchange.let { percentChange -> percentChange?.trim()?.dropLast(1) }
    return MarketDataFormatter.formatMarketPercData(percentChangeWithoutSign?.toBigDecimalOrNull())
}
