package com.etrade.mobilepro.watchlist.overview.viewmodel

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.etrade.eo.core.util.DateFormattingUtils.formatFullDateTime
import com.etrade.mobilepro.inboxmessages.InAppCampaigns
import com.etrade.mobilepro.inboxmessages.InAppMessage
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.instrument.isOption
import com.etrade.mobilepro.livedata.combineLatest
import com.etrade.mobilepro.livedata.first
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.preferences.UserPreferences
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.QuoteEmptyResponse
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.tableviewutils.presentation.sorting.TableDataSortingHandler
import com.etrade.mobilepro.tableviewutils.presentation.toPreferenceString
import com.etrade.mobilepro.tableviewutils.presentation.toSortStateInfo
import com.etrade.mobilepro.tracking.SCREEN_TITLE_WATCH_LISTS
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.AddToWatchListAnalyticsParams
import com.etrade.mobilepro.tracking.params.WatchListNullStateAnalyticsParams
import com.etrade.mobilepro.tracking.screen
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.SingleLiveEvent
import com.etrade.mobilepro.util.android.timer.TimeoutTimer
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.executeBlocking
import com.etrade.mobilepro.util.safeLet
import com.etrade.mobilepro.walkthrough.api.SymbolTrackingState
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.etrade.mobilepro.walkthrough.presentation.createConditionalSignal
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.WatchlistColumn
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.WatchlistDataCell
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.getRowDataCellItems
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.mapToWatchlistDataCellParams
import com.etrade.mobilepro.watchlist.overview.viewmodel.tableview.updateWatchlistDataCell
import com.etrade.mobilepro.watchlist.usecase.ResolveMutualFundTimestampUseCase
import com.etrade.mobilepro.watchlist.usecase.ResolveTimestampUseCase
import com.etrade.mobilepro.watchlist.usecase.WatchlistNullStateUseCase
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.DuplicateSymbolException
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.WatchlistCRUDCallbacks
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnInterface
import com.etrade.mobilepro.watchlistapi.repo.WatchlistColumnRepo
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.threeten.bp.Duration
import java.math.BigDecimal

private const val WATCHLIST_NULL_STATE_TIMEOUT_SECONDS = 10L

@Suppress("LargeClass", "TooManyFunctions", "LongParameterList")
class WatchlistOverviewViewModel @AssistedInject constructor(
    private val dataColorResolver: DataColorResolver,
    private val watchlistNullStateUseCase: WatchlistNullStateUseCase,
    private val inboxMessagesRepository: InboxMessagesRepository,
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase,
    private val marketStatusDelegate: MarketTradeStatusDelegate,
    private val quoteRepo: MobileQuoteRepo,
    private val resolveMutualFundTimestamp: ResolveMutualFundTimestampUseCase,
    private val resolveTimestamp: ResolveTimestampUseCase,
    private val resources: Resources,
    private val streamingController: StreamingSubscriptionController,
    private val streamingStatusController: StreamingStatusController,
    private val tracker: Tracker,
    private val user: User,
    private val userPreferences: UserPreferences,
    @Assisted private val watchlistId: String?,
    private val watchlistColumnRepo: WatchlistColumnRepo,
    private val watchlistRepo: WatchlistRepo,
    private val applicationPreferences: ApplicationPreferences,
    private val timeoutTimer: TimeoutTimer,
    walkthroughStatesViewModel: WalkthroughStatesViewModel,
) : ViewModel() {

    val isStreamingToggleEnabled: Boolean
        get() = streamingStatusController.isStreamingToggleEnabled
    private var watchlistColumns: List<WatchlistColumnInterface> = listOf()

    private fun mapAndPostStreamingUpdates(streamingUpdates: Map<String, Level1Data>) {
        val state = viewState.value as? ViewState.FetchEntries.Success
        if (state != null) {
            val watchlistItems = state.tableViewData
            var watchlistEntryList = state.watchlistEntryList
            var lastUpdatedTime: Long? = null

            val updatedCells = watchlistItems.cellItems.map { row ->
                safeLet(row.first().ticker, streamingUpdates[row.first().ticker]) { symbol, data ->
                    val watchlistCellData = data.mapToWatchlistDataCellParams()

                    val updatedRow = row.mapIndexed { index, cell ->
                        val column = watchlistColumns[index]
                        updateWatchlistDataCell(cell, column as WatchlistColumn, watchlistCellData[column], dataColorResolver, data.tickIndicator)
                    }

                    watchlistEntryList = watchlistEntryList.updateWatchlistEntry(watchlistCellData, symbol)
                    lastUpdatedTime = data.timestampSinceEpoch

                    updatedRow
                } ?: row
            }

            val sortedData = sortingHandler.sortTableData(updatedCells, watchlistItems.rowHeaders, sortState.value)

            mutableViewState.postValue(
                ViewState.FetchEntries.Success(
                    watchlistEntryList = watchlistEntryList,
                    tableViewData = watchlistItems.copy(
                        rowHeaders = sortedData.first,
                        cellItems = sortedData.second,
                        timestamp = lastUpdatedTime?.let { timeStamp ->
                            formatFullDateTime(timeStamp)
                        } ?: watchlistItems.timestamp
                    )
                )
            )
        }
    }

    private val mutableViewState = MutableLiveData<ViewState>()
    val viewState: LiveData<ViewState> = mutableViewState

    private val _isRefreshing = MutableLiveData<Boolean>()
    val isRefreshing: LiveData<Boolean> = _isRefreshing

    private val mutableWatchLists = MutableLiveData<List<Watchlist>>()
    val watchlists: LiveData<List<Watchlist>> = mutableWatchLists

    val selectedWatchList: LiveData<Watchlist> = userPreferences.recentWatchlistData.map {
        var (id, name) = it
        if (Watchlist.isAllWatchlistsItemId(id)) { // Overview tab does not have "All Watchlists" item
            id = DEVICE_WATCHLIST_ID
            name = resources.getString(R.string.watchlist_device_watch_list)
        }
        Watchlist(
            id ?: DEVICE_WATCHLIST_ID,
            name ?: resources.getString(R.string.watchlist_device_watch_list)
        )
    }

    private val mutableNeedRefresh = MutableLiveData<ConsumableLiveEvent<Boolean>>()
    val needRefresh: LiveData<ConsumableLiveEvent<Boolean>> = mutableNeedRefresh

    private val _startSearchSignal = SingleLiveEvent<StartSearchParams>()
    val startSearchSignal: LiveData<StartSearchParams> = _startSearchSignal

    private val _viewEffects = SingleLiveEvent<ViewEffects>()
    val viewEffects: LiveData<ViewEffects> = _viewEffects

    private val _addSymbolToWatchListEvent = MutableLiveData<AddSymbolToWatchlistEvent>()
    val addSymbolToWatchListEvent: LiveData<AddSymbolToWatchlistEvent> = _addSymbolToWatchListEvent

    private val _inAppMessage = MutableLiveData<InAppMessage>()
    val inAppMessage: LiveData<InAppMessage> = _inAppMessage

    val inAppMessageCta = inAppMessage.map {
        it.callToAction ?: resources.getString(R.string.watchlist_add_symbol_cta)
    }

    private val _showInAppMessage = MutableLiveData(false)
    val showInAppMessage: LiveData<Boolean> = _showInAppMessage

    private var hasTriggeredNullState = false

    private val compositeDisposable = CompositeDisposable()
    private var entriesDisposable: Disposable? = null
    private var fetchWatchlistDisposable: Disposable? = null

    private val _startEditWatchlistSignal = SingleLiveEvent<StartWatchlistEditParams>()
    val startWatchlistEditSignal: LiveData<StartWatchlistEditParams> = _startEditWatchlistSignal

    private val _startEditWatchlistColumnsSignal = SingleLiveEvent<Unit>()
    val startEditWatchlistColumnsSignal: LiveData<Unit> = _startEditWatchlistColumnsSignal

    private val logger: Logger = LoggerFactory.getLogger(WatchlistOverviewViewModel::class.java)

    private val sortingHandler = TableDataSortingHandler<WatchlistDataCell>()

    private val visibleSymbolsSubject = BehaviorSubject.create<Set<Symbol>>()

    private val quoteLookupTooltipDismissedSignal = MutableLiveData<Unit>()

    private var shouldDispatchTooltipEvents = false

    /**
     * In memory cache of watchlist entries.
     */
    private var lastWatchlistEntries: WatchlistEntries? = null

    private val symbolSubscriptionFields = setOf(
        Level1Field.ASK,
        Level1Field.BID,
        Level1Field.CHANGE,
        Level1Field.CHANGE_PERCENT,
        Level1Field.DAY_CHANGE,
        Level1Field.DAY_CHANGE_PERCENT,
        Level1Field.DAY_HIGH,
        Level1Field.DAY_LOW,
        Level1Field.EPS,
        Level1Field.LAST_PRICE,
        Level1Field.PRICE_TO_EARNINGS,
        Level1Field.TIMESTAMP_SINCE_EPOCH,
        Level1Field.VOLUME,
        Level1Field.YEAR_HIGH,
        Level1Field.YEAR_LOW,
        Level1Field.TICK_INDICATOR,
        Level1Field.TRADE_STATUS,
        Level1Field.TRADE_HALT_REASON
    )

    val sortState: LiveData<SortStateInfo?>
        get() = _sortState
    private val _sortState = MutableLiveData(
        applicationPreferences.watchlistSortState.toSortStateInfo(
            watchlistColumnRepo.getColumnsAsString()
        )
    )

    val highlightWatchlistActionSignal: LiveData<ConsumableLiveEvent<SymbolTrackingState>>

    @AssistedFactory
    interface Factory {
        fun create(watchlistId: String?): WatchlistOverviewViewModel
    }

    init {
        highlightWatchlistActionSignal = walkthroughStatesViewModel.createConditionalSignal<SymbolTrackingState, ViewState, ViewState.FetchEntries.Success>(
            viewState = viewState,
            goalState = SymbolTrackingState.HighlightWatchlistActionInWatchlistPage
        ).combineLatest(
            quoteLookupTooltipDismissedSignal.first()
        ).map { (signal, _) -> signal }

        tracker.screen(SCREEN_TITLE_WATCH_LISTS)

        getUserWatchlists()
    }

    fun getUserWatchlists(onSuccess: (() -> Unit)? = null) {
        mutableViewState.value = ViewState.FetchWatchlist.Loading
        fetchWatchlistDisposable?.let { compositeDisposable.remove(it) }
        fetchWatchlistDisposable = watchlistRepo.getUserWatchlists()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = { userWatchlists ->
                    mutableWatchLists.value = userWatchlists
                    updateRecentWatchlist(userWatchlists)
                    mutableViewState.value = ViewState.FetchWatchlist.Success(userWatchlists)

                    checkWatchListNullState(userWatchlists)
                    mutableNeedRefresh.value = ConsumableLiveEvent(true)
                    onSuccess?.invoke()
                },
                onError = { throwable ->
                    logger.error("fetching user watchlists failed", throwable)
                    // try get previously loaded entries if they match selected watchlist
                    handleWatchlistError()
                }
            ).addTo(compositeDisposable)
    }

    private fun updateRecentWatchlist(watchLists: List<Watchlist>) {
        val selectedWatchlist = resolveSelectedWatchlist(watchLists)
        updateRecentWatchlist(selectedWatchlist)
    }

    private fun updateRecentWatchlist(selectedWatchlist: Watchlist?) {
        userPreferences.setRecentWatchlist(selectedWatchlist?.id ?: DEVICE_WATCHLIST_ID, selectedWatchlist?.name)
    }

    private fun markGetUserWatchListsSuccess(watchLists: List<Watchlist>) {
        mutableWatchLists.value = watchLists
        updateRecentWatchlist(watchLists)
        mutableNeedRefresh.value = ConsumableLiveEvent(true)
        mutableViewState.value = ViewState.FetchWatchlist.Success(watchLists)
    }

    fun getIndexOfSelectedWatchlist(): Int {
        mutableWatchLists.value?.let {
            val recentWatchlistId = watchlistId ?: userPreferences.getRecentWatchlist().first
            for ((index, watchlist) in it.withIndex()) {
                if (watchlist.id == recentWatchlistId) {
                    return index
                }
            }
        }
        return 0
    }

    fun swipeRefresh() {
        _isRefreshing.value = true
        refreshCurrentWatchList()
    }

    fun getWatchlistEntries(selected: Watchlist?) {
        if (selectedWatchList.value != selected) {
            updateRecentWatchlist(selected)
            selected?.id?.let { watchlistId ->
                userPreferences.setRecentWatchlist(watchlistId, selected.name)
            }
            mutableNeedRefresh.postValue(ConsumableLiveEvent(true))
        }
    }

    fun refreshCurrentWatchList() {
        if (streamingStatusController.isStreamingToggleEnabled) {
            streamingController.cancelSubscription()
        }
        selectedWatchList.value?.also { watchlist ->
            if (isRefreshing.value != true) {
                mutableViewState.value = ViewState.FetchEntries.Loading
                mutableNeedRefresh.value = ConsumableLiveEvent(false)
            }
            entriesDisposable?.let { compositeDisposable.remove(it) }
            entriesDisposable = watchlistRepo.getWatchlistEntries(watchlist.id, isExtendedHoursOnUseCase.executeBlocking()).subscribeBy(
                onSuccess = { entries ->
                    handleOnSuccessGetWatchlistEntries(watchlist, entries)
                    _isRefreshing.postValue(false)
                },
                onError = { throwable ->
                    logger.error("Refresh Watchlist failed", throwable)
                    handleError()
                    _isRefreshing.postValue(false)
                }
            ).addTo(compositeDisposable)
        }
    }

    private fun checkWatchListNullState(watchLists: List<Watchlist>) {
        if (hasTriggeredNullState) {
            markGetUserWatchListsSuccess(watchLists)
            return
        }
        viewModelScope.launch {
            if (watchlistNullStateUseCase.execute(watchLists)) {
                logger.debug("checkWatchListNullState triggering Watch List Null State")
                tracker.event(WatchListNullStateAnalyticsParams())
                inboxMessagesRepository.triggerMessageCustomUi(InAppCampaigns.WATCH_LIST_NULL_STATE) {
                    logger.debug("Watch List Null State message triggered by Localytics $it")
                    timeoutTimer.stop()
                    _inAppMessage.value = it
                    _showInAppMessage.value = true
                }
                startLocalyticsNullStateTimer(watchLists)
                hasTriggeredNullState = true
            } else {
                markGetUserWatchListsSuccess(watchLists)
            }
        }
    }

    private fun startLocalyticsNullStateTimer(watchLists: List<Watchlist>) {
        timeoutTimer.start(Duration.ofSeconds(WATCHLIST_NULL_STATE_TIMEOUT_SECONDS)) {
            markGetUserWatchListsSuccess(watchLists)
        }
    }

    fun onQuoteLookupTooltipDismissed() {
        if (shouldDispatchTooltipEvents) {
            quoteLookupTooltipDismissedSignal.value = Unit
        }
    }

    private fun resolveSelectedWatchlist(watchlists: List<Watchlist>): Watchlist? {
        val resolvedWatchlist = watchlists.find {
            if (watchlistId != null) {
                it.id == watchlistId
            } else {
                it.id == userPreferences.getRecentWatchlist().first
            }
        } ?: watchlists.firstOrNull()

        return resolvedWatchlist?.also { userPreferences.setRecentWatchlist(it.id, it.name) }
    }

    private fun handleOnSuccessGetWatchlistEntries(watchlist: Watchlist, entries: List<WatchlistEntry>) {
        viewModelScope.launch {
            if (entries.isEmpty()) {
                mutableViewState.postValue(ViewState.FetchEntries.NoItem)
            } else {
                watchlistColumns = watchlistColumnRepo.getColumns()
                lastWatchlistEntries = prepareWatchlistEntries(watchlist, entries).also {
                    mutableViewState.postValue(
                        ViewState.FetchEntries.Success(
                            tableViewData = it,
                            watchlistEntryList = entries.sortedWith { entry1, entry2 -> entry1.ticker.compareTo(entry2.ticker) }
                        )
                    )
                }
                val throttler = Throttler(coroutineScope = viewModelScope, destinationFunction = ::mapAndPostStreamingUpdates)
                streamingController.initSubscription({ symbolSubscriptionFields }, visibleSymbolsSubject) { symbol, _, data ->
                    throttler.throttle(symbol, data)
                    marketStatusDelegate.onMarketStatusUpdate(data)
                }
                if (streamingStatusController.isStreamingToggleEnabled) {
                    streamingController.resumeSubscription()
                }
            }
        }
    }

    @Suppress("LongMethod") // because we add tracking here, we reached method length restriction
    fun addSymbolToWatchList(ticker: SearchResultItem.Symbol, onCompletion: ((SearchResultItem.Symbol) -> Unit)? = null) {
        val displaySymbol = ticker.watchlistDisplay

        _addSymbolToWatchListEvent.value = AddSymbolToWatchlistEvent.Loading
        selectedWatchList.value?.let { watchlist ->
            compositeDisposable.add(
                quoteRepo.loadMobileQuote(ticker.title)
                    .firstOrError()
                    .flatMap { watchlistRepo.addToWatchList(watchlist.id, ticker.title) }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(
                        onSuccess = { response ->
                            if (response) {
                                _viewEffects.value = ViewEffects.AddedToWatchlist(ticker)
                                _addSymbolToWatchListEvent.value = AddSymbolToWatchlistEvent.Success
                                mutableNeedRefresh.value = ConsumableLiveEvent(true)

                                trackAddSymbolToWatchList(ticker)
                                onCompletion?.invoke(ticker)
                            } else {
                                handleError(shouldRetry = false)
                            }
                        },
                        onError = { throwable ->
                            _addSymbolToWatchListEvent.value = AddSymbolToWatchlistEvent.Error

                            // try to get cached entries for current watchlist
                            when (throwable) {
                                is DuplicateSymbolException -> {
                                    _viewEffects.value = ViewEffects.SnackBarMessage(
                                        resources.getString(
                                            R.string.watchlist_duplicate_symbol_error,
                                            displaySymbol
                                        )
                                    )
                                    getUserWatchlists()
                                }
                                is QuoteEmptyResponse ->
                                    _viewEffects.value =
                                        ViewEffects.SnackBarMessage(
                                            resources.getString(
                                                R.string.watchlist_error_invalid_symbol,
                                                displaySymbol
                                            )
                                        )
                                else -> {
                                    logger.error("Add to watchlist failed", throwable)
                                    _viewEffects.value = ViewEffects.GenericError
                                }
                            }
                        }
                    )
            )
        }
    }

    private fun trackAddSymbolToWatchList(ticker: SearchResultItem.Symbol) {
        tracker.event(
            AddToWatchListAnalyticsParams(
                customerId = user.session?.aatId,
                deviceWatchList = selectedWatchList.value?.id == DEVICE_WATCHLIST_ID,
                symbol = ticker.underlyerSymbol,
                type = ticker.type,
                securityType = ticker.securityType
            )
        )
    }

    val watchlistCRUDCallbacks = WatchlistCRUDCallbacks(
        currentWatchlists = { mutableWatchLists.value },
        onStartCreating = { mutableViewState.postValue(ViewState.FetchWatchlist.Loading) },
        onWatchlistCreated = { newWatchList, updatedList ->
            mutableWatchLists.value = updatedList
            mutableViewState.value = ViewState.FetchWatchlist.Success(updatedList)
            mutableViewState.value = ViewState.FetchEntries.Success(WatchlistEntries(newWatchList), watchlistEntryList = emptyList())
            _viewEffects.value = ViewEffects.WatchlistCreated
        },
        onWatchlistDeleted = { updatedList ->
            mutableWatchLists.value = updatedList
            mutableNeedRefresh.value = ConsumableLiveEvent(true)
        },
        onError = {
            _viewEffects.postValue(ViewEffects.GenericError)
            handleError(shouldRetry = false)
        }
    )

    fun pauseStreaming() {
        streamingController.pauseSubscription()
    }

    fun resumeStreaming() {
        if (streamingStatusController.isStreamingToggleEnabled) {
            streamingController.resumeSubscription()
        }
    }

    fun notifyVisibleItemsChanged(updatedSymbols: Set<Symbol>) {
        if (streamingStatusController.isStreamingToggleEnabled) {
            visibleSymbolsSubject.onNext(updatedSymbols)
        }
    }

    private fun correctSortedColumn() {
        val oldSortState = sortState.value
        val newSortState = applicationPreferences.watchlistSortState.toSortStateInfo(
            watchlistColumnRepo.getColumnsAsString()
        )
        if (oldSortState != newSortState) {
            _sortState.value = newSortState
        }
    }

    override fun onCleared() {
        streamingController.cancelSubscription()
        compositeDisposable.clear()
        if (shouldDispatchTooltipEvents) {
            applicationPreferences.shouldDisplayWatchListTooltip = false
        }
        super.onCleared()
    }

    @Suppress("LongMethod")
    private fun prepareWatchlistEntries(watchlist: Watchlist, entries: List<WatchlistEntry>): WatchlistEntries {
        return if (entries.isEmpty()) {
            WatchlistEntries(watchlist)
        } else {
            val listOfSymbols = mutableListOf<SymbolCell>()
            val listOfCells = mutableListOf<List<WatchlistDataCell>>()

            entries.forEach { entry ->
                entry.entryID?.let { entryID ->
                    listOfSymbols.add(
                        SymbolCell(
                            id = entryID,
                            displayText = entry.ticker,
                            symbol = entry.ticker,
                            instrumentType = entry.typeCode ?: InstrumentType.UNKNOWN,
                            description = entry.description ?: ""
                        )
                    )
                    listOfCells.add(getRowDataCellItems(entryID, watchlistColumns, entry, dataColorResolver))
                }
            }

            correctSortedColumn()
            val (sortedSymbols, sortedCells) = sortingHandler.sortTableData(listOfCells, listOfSymbols, _sortState.value)

            WatchlistEntries(
                watchlist,
                getColumnHeaders(watchlistColumns),
                sortedSymbols,
                sortedCells,
                resolveTimestamp.executeBlocking(entries).getOrNull(),
                resolveMutualFundTimestamp.executeBlocking(entries).getOrNull()
            )
        }
    }

    private fun getColumnHeaders(columns: Collection<WatchlistColumnInterface>): List<Cell> {
        return columns.mapIndexed { index: Int, column ->
            Cell(
                index.toString(),
                resources.getString(column.textRes),
                resources.getString(column.contentDescriptionRes)
            )
        }
    }

    fun openSearchActivity(query: SearchQuery, finishOnSearch: Boolean = false) {
        _startSearchSignal.value = StartSearchParams(
            query = query,
            stockQuoteSearchOnly = false,
            finishOnSearch = finishOnSearch
        )
    }

    /**
     * @param shouldRetry default true, show a retry snackbar to retry fetching watchlist
     */
    private fun handleWatchlistError(shouldRetry: Boolean = true) {
        // try to get cached entries for current watchlist
        val entries = lastWatchlistEntries?.takeIf { it.watchlist.id == selectedWatchList.value?.id }
        if (shouldRetry) {
            mutableViewState.postValue(ViewState.FetchWatchlist.Error.RetryError(entries))
        } else {
            mutableViewState.postValue(ViewState.FetchWatchlist.Error.NoRetryError(entries))
        }
    }

    /**
     * @param shouldRetry default true, show a retry snackbar to retry fetching watchlist
     */
    private fun handleError(shouldRetry: Boolean = true) {
        // try to get cached entries for current watchlist
        val entries = lastWatchlistEntries?.takeIf { it.watchlist.id == selectedWatchList.value?.id }
        if (shouldRetry) {
            mutableViewState.postValue(ViewState.FetchEntries.Error.RetryError(entries))
        } else {
            mutableViewState.postValue(ViewState.FetchEntries.Error.NoRetryError(entries))
        }
    }

    /**
     * Fire an event to start [EditWatchlistActivity].
     */
    fun openEditWatchlistActivity() {
        selectedWatchList.value?.also {
            _startEditWatchlistSignal.value = with(it) {
                StartWatchlistEditParams(id, isDeletable)
            }
        }
    }

    /**
     * Fire an event to start [EditWatchlistColumnsActivity].
     */
    fun openEditWatchlistColumnsActivity() {
        _startEditWatchlistColumnsSignal.value = Unit
    }

    fun onInAppMessageCtaClicked() {
        logger.debug("onInAppMessageCtaClicked")
        _showInAppMessage.value = false
        openSearchActivity(SearchQuery(SearchType.SYMBOL), finishOnSearch = true)
    }

    fun setShouldDispatchTooltipEvents(value: Boolean) {
        shouldDispatchTooltipEvents = value
    }

    fun sortTable(column: Int?, visibleItemSize: Int? = null) {
        val oldSortState = _sortState.value
        val newSortState = sortingHandler.resolveSortState(oldSortState, column)
        if (newSortState != oldSortState) {
            val tableModel = (viewState.value as? ViewState.FetchEntries.Success)?.tableViewData
            val watchlistEntryList = (viewState.value as? ViewState.FetchEntries.Success)?.watchlistEntryList

            if (tableModel != null && !tableModel.isEmpty && watchlistEntryList != null) {
                val sortedData = sortingHandler.sortTableData(tableModel.cellItems, tableModel.rowHeaders, newSortState)
                val updatedModel = tableModel.copy(
                    rowHeaders = sortedData.first,
                    cellItems = sortedData.second
                )
                mutableViewState.postValue(
                    ViewState.FetchEntries.Success(
                        updatedModel,
                        forceNotifyData = true,
                        watchlistEntryList = watchlistEntryList
                    )
                )
                visibleItemSize?.let {
                    updateStreamingSymbolOnSort(it, updatedModel)
                }
            }
            _sortState.value = newSortState
            if (user.isLoggedIn()) {
                applicationPreferences.watchlistSortState = newSortState.toPreferenceString(watchlistColumnRepo.getColumnsAsString())
            }
        }
    }

    private fun updateStreamingSymbolOnSort(updatedToIndex: Int, tableViewData: WatchlistEntries) {
        if (isStreamingToggleEnabled) {
            tableViewData.rowHeaders.subList(0, updatedToIndex).map { withSymbolInfo ->
                Symbol(withSymbolInfo)
            }.toSet().let {
                notifyVisibleItemsChanged(it)
            }
        }
    }

    @Suppress("ComplexMethod")
    private fun List<WatchlistEntry>.updateWatchlistEntry(
        watchlistCellData: Map<WatchlistColumn, BigDecimal?>,
        symbol: String
    ) = map {
        if (it.ticker == symbol) {
            it.copy(
                ask = watchlistCellData[WatchlistColumn.Ask] ?: it.ask,
                bid = watchlistCellData[WatchlistColumn.Bid] ?: it.bid,
                change = watchlistCellData[WatchlistColumn.ChangeDollar] ?: it.change,
                changePercent = watchlistCellData[WatchlistColumn.ChangePercent] ?: it.changePercent,
                dayHigh = watchlistCellData[WatchlistColumn.DayHigh] ?: it.dayHigh,
                dayLow = watchlistCellData[WatchlistColumn.DayLow] ?: it.dayLow,
                earningsPerShare = watchlistCellData[WatchlistColumn.EarningsPerShare] ?: it.earningsPerShare,
                lastPrice = watchlistCellData[WatchlistColumn.LastPrice] ?: it.lastPrice,
                priceEarningsRatio = watchlistCellData[WatchlistColumn.PriceEarningRatio] ?: it.priceEarningsRatio,
                volume = watchlistCellData[WatchlistColumn.Volume] ?: it.volume,
                week52High = watchlistCellData[WatchlistColumn.FiftyTwoWeekHigh] ?: it.week52High,
                week52Low = watchlistCellData[WatchlistColumn.FiftyTwoWeekLow] ?: it.week52Low,
            )
        } else {
            it
        }
    }

    fun getSymbolCell(index: Int, isGridView: Boolean = true): SymbolCell? = (viewState.value as? ViewState.FetchEntries.Success)?.let {
        if (isGridView) {
            it.tableViewData.rowHeaders[index]
        } else {
            val entryID = it.watchlistEntryList[index].entryID
            it.tableViewData.rowHeaders.find { header -> header.id == entryID }
        }
    }

    fun getSymbolFromCell(symbolCell: SymbolCell): SearchResultItem.Symbol {
        return SearchResultItem.Symbol(title = symbolCell.displayText, type = symbolCell.instrumentType)
            .let { symbol ->
                if (symbol.isOption) {
                    symbol.copy(displaySymbol = symbolCell.description ?: "")
                } else {
                    symbol
                }
            }
    }

    internal fun resetWatchListNullState() {
        _showInAppMessage.value = false
    }
}

sealed class ViewState {

    interface WithEntries {
        val tableViewData: WatchlistEntries?
        val watchlistEntryList: List<WatchlistEntry>?
    }

    sealed class FetchWatchlist : ViewState() {
        data class Success(val listOfWatchlist: List<Watchlist>) : FetchWatchlist()
        object Loading : FetchWatchlist()
        sealed class Error : FetchWatchlist() {
            data class NoRetryError(
                override val tableViewData: WatchlistEntries? = null,
                override val watchlistEntryList: List<WatchlistEntry>? = null
            ) : Error(),
                WithEntries
            data class RetryError(
                override val tableViewData: WatchlistEntries? = null,
                override val watchlistEntryList: List<WatchlistEntry>? = null
            ) : Error(),
                WithEntries
        }
    }

    sealed class FetchEntries : ViewState() {
        data class Success(
            override val tableViewData: WatchlistEntries,
            val forceNotifyData: Boolean = false,
            override val watchlistEntryList: List<WatchlistEntry>
        ) : FetchEntries(),
            WithEntries
        object Loading : FetchEntries()
        object NoItem : FetchEntries()
        sealed class Error : FetchEntries() {
            data class NoRetryError(
                override val tableViewData: WatchlistEntries? = null,
                override val watchlistEntryList: List<WatchlistEntry>? = null
            ) : Error(),
                WithEntries
            data class RetryError(
                override val tableViewData: WatchlistEntries? = null,
                override val watchlistEntryList: List<WatchlistEntry>? = null
            ) : Error(),
                WithEntries
        }
    }
}

sealed class AddSymbolToWatchlistEvent {
    object Success : AddSymbolToWatchlistEvent()
    object Loading : AddSymbolToWatchlistEvent()
    object Error : AddSymbolToWatchlistEvent()
}

sealed class ViewEffects {
    object GenericError : ViewEffects()
    data class SnackBarMessage(val message: String) : ViewEffects()
    data class AddedToWatchlist(val symbol: SearchResultItem.Symbol) : ViewEffects()
    object WatchlistCreated : ViewEffects()
}

data class WatchlistEntries(
    val watchlist: Watchlist,
    val columnHeaders: List<Cell> = emptyList(),
    val rowHeaders: List<SymbolCell> = emptyList(),
    val cellItems: List<List<WatchlistDataCell>> = emptyList(),
    val timestamp: String? = null,
    val mfTimestamp: String? = null
) {
    val isEmpty: Boolean
        get() = rowHeaders.isEmpty() && cellItems.isEmpty()
}

val SearchResultItem.Symbol.watchlistDisplay: String
    get() = if (displaySymbol.isNotEmpty()) displaySymbol else underlyerSymbol
