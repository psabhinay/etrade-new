package com.etrade.mobilepro.chart.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class AggregationChartResponseDto(
    @Json(name = "ApiAggregatorResponse")
    val body: AggregationResponseBody?
)

@JsonClass(generateAdapter = true)
data class AggregationResponseBody(
    @Json(name = "Api")
    val entries: List<AggregationResponseEntry>?
)

@JsonClass(generateAdapter = true)
data class AggregationResponseEntry(
    @Json(name = "id")
    val id: String?,
    @Json(name = "responseCode")
    val responseCode: Int?,
    @Json(name = "response")
    val response: ChartDataDto?
)
