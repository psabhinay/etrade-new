package com.etrade.mobilepro.transfermoney.api

import com.etrade.mobilepro.fundsflow.api.FundsFlowAccount
import java.math.BigInteger

data class TransferMoneyAccount(
    val title: String,
    override val accountId: String,
    override val nickname: String?,
    override val instNo: BigInteger,
    override val accountType: String,
    override val accountCategory: String,
    val canBeFrom: Boolean,
    val canBeTo: Boolean
) : FundsFlowAccount
