package com.etrade.mobilepro.companyoverview

import android.os.Bundle
import android.widget.TextView
import androidx.core.widget.ContentLoadingProgressBar
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.overlay.R
import com.etrade.mobilepro.overlay.TextOverlayFragment
import com.etrade.mobilepro.overlay.databinding.FragmentTextOverlayBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import javax.inject.Inject

class CompanyOverviewFragment @Inject constructor() : TextOverlayFragment<CompanyOverviewViewModel>(CompanyOverviewViewModel::class.java) {

    private val binding by viewBinding(FragmentTextOverlayBinding::bind)

    private val activityArgs: CompanyOverviewFragmentArgs by navArgs()

    override val layoutRes: Int = R.layout.fragment_text_overlay

    override val overlayTextLoading: ContentLoadingProgressBar
        get() = binding.overlayTextLoading

    override val contentText: TextView
        get() = binding.contentText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getText()
    }

    override fun getText() {
        viewModel.showCompanyOverview(activityArgs.companySymbol)
    }
}
