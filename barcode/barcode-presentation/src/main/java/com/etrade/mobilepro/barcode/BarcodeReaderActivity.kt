package com.etrade.mobilepro.barcode

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.barcode.databinding.ActivityBarcodeReaderBinding
import com.etrade.mobilepro.barcode.viewmodel.BarcodeReaderViewModel
import com.etrade.mobilepro.baseactivity.ActivityNetworkConnectionDelegate
import com.etrade.mobilepro.baseactivity.ActivitySessionDelegate
import com.etrade.mobilepro.common.setupToolbarWithUpButton
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.selfPermissionGranted
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

const val CAMERA_PERMISSION_MISSING_RESULT_CODE = Activity.RESULT_FIRST_USER + 1
const val BARCODE_LOOKUP_FAILED = Activity.RESULT_FIRST_USER + 2
const val EXTRA_QUOTE_SYMBOL = "EXTRA_QUOTE_SYMBOL"

class BarcodeReaderActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var activitySessionDelegate: ActivitySessionDelegate

    @Inject
    lateinit var activityNetworkConenectionDelegate: ActivityNetworkConnectionDelegate

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: BarcodeReaderViewModel by viewModels { viewModelFactory }

    private val binding by viewBinding(ActivityBarcodeReaderBinding::inflate)

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(binding.root)

        viewModel.barcodeReaderHandler = BarcodeReaderHandler(
            previewView = binding.cameraView,
            scanCallback = {
                viewModel.lookupBarcode(it.rawValue, it.format)
            },
            activity = this
        )

        setupToolbarWithUpButton(
            titleResId = R.string.barcode_tab_scan,
            titleContentDescription = R.string.label_barcode_scan_header,
            upButtonResId = R.drawable.ic_close
        )

        viewModel.result.observe(
            this,
            Observer {
                when (it) {
                    is Resource.Success -> {
                        sendResult(Activity.RESULT_OK, it.data)
                    }
                    is Resource.Failed -> {
                        sendResult(BARCODE_LOOKUP_FAILED)
                    }
                    is Resource.Loading -> {
                        binding.barcodeReaderProgress.visibility = View.VISIBLE
                        binding.barcodeReaderOverlay.visibility = View.VISIBLE
                    }
                }
            }
        )

        activityNetworkConenectionDelegate.initNetworkConnectivityBanner(this)

        activitySessionDelegate.observeTimeoutEvent(this)
    }

    override fun onResume() {
        super.onResume()

        if (selfPermissionGranted(Manifest.permission.CAMERA)) {
            viewModel.startCamera()
        } else {
            sendResult(CAMERA_PERMISSION_MISSING_RESULT_CODE)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onUserInteraction() {
        super.onUserInteraction()
        activitySessionDelegate.onUserInteraction(this)
    }

    private fun sendResult(resultCode: Int, symbol: String? = null) {
        val intent = Intent()

        intent.putExtra(EXTRA_QUOTE_SYMBOL, symbol)

        setResult(resultCode, intent)

        finish()
    }
}
