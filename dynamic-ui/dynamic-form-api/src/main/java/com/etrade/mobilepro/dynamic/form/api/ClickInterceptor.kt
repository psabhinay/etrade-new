package com.etrade.mobilepro.dynamic.form.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable

interface ClickInterceptor {
    val shouldInterceptClick: LiveData<Boolean>
    val clickInterceptedSignal: LiveData<ConsumableLiveEvent<Unit>>
    fun notifyClickIntercepted()
}

class ClickInterceptorImpl(
    _shouldInterceptClick: LiveData<Boolean> = MutableLiveData(false)
) : ClickInterceptor {
    override val shouldInterceptClick = _shouldInterceptClick
    override val clickInterceptedSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _clickInterceptedSignal

    private val _clickInterceptedSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()

    override fun notifyClickIntercepted() {
        _clickInterceptedSignal.value = Unit.consumable()
    }
}
