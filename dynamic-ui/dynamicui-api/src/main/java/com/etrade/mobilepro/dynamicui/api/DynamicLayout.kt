package com.etrade.mobilepro.dynamicui.api

import androidx.annotation.LayoutRes

interface DynamicLayout {

    val viewModel: Any

    val layoutId: Int
        @LayoutRes get

    val variableId: Int
}
