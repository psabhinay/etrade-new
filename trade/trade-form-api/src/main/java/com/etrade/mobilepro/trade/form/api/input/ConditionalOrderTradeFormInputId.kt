package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.trade.form.api.input.shared.WithTrailingStopInputId
import com.etrade.mobilepro.trade.form.api.value.ConditionalPriceTypeValueId

const val CONDITIONAL_ORDER_SECURITY_TYPE_ID = "conditional_order_security_type_id"

interface ConditionalOrderTradeFormInputId : BaseTradeFormInputId, WithActionTradeFormInputId, WithQuantity, WithTrailingStopInputId, WithWalkthroughInputId {
    override val priceTypeValueId: ConditionalPriceTypeValueId
    val underlierSymbol: String
}
