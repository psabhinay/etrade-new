package com.etrade.mobilepro.dagger

import androidx.lifecycle.ViewModel
import com.etrade.mobile.accounts.dto.AccountType
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.stockplan.AccountIndexHandler
import com.etrade.mobilepro.stockplan.AccountTypeHandler
import com.etrade.mobilepro.stockplan.StockPlanWebViewSharedViewModel
import com.etrade.mobilepro.userviewmodel.UserViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class StockPlanModule {

    @Binds
    @IntoMap
    @ViewModelKey(StockPlanWebViewSharedViewModel::class)
    abstract fun bindSharedViewModel(viewModel: StockPlanWebViewSharedViewModel): ViewModel

    companion object {
        @Provides
        internal fun providesAccountIndex(userViewModel: UserViewModel): AccountIndexHandler =
            object : AccountIndexHandler {
                override fun getAccountIndex(): String? {
                    return userViewModel.currentSelectedAccount.value?.account?.accountIndex
                }
            }

        @Provides
        internal fun providesAccountType(userViewModel: UserViewModel): AccountTypeHandler =
            object : AccountTypeHandler {
                override fun getAccountType(): AccountType? {
                    return userViewModel.currentSelectedAccount.value?.account?.accountType
                }
            }
    }
}
