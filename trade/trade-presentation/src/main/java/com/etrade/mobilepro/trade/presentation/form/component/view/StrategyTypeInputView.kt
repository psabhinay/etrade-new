package com.etrade.mobilepro.trade.presentation.form.component.view

import android.content.Context
import android.view.Gravity
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.TextViewCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.etrade.mobilepro.dynamic.form.ui.ComponentView
import com.etrade.mobilepro.dynamic.form.ui.InputViewDelegate
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.form.component.StrategyTypeInput

internal class StrategyTypeInputView(context: Context) : AppCompatTextView(context), ComponentView<StrategyTypeInput> {

    private val inputViewDelegate: InputViewDelegate = InputViewDelegate(this)

    private val displayValueObserver: Observer<CharSequence?> = Observer<CharSequence?> {
        text = it
    }

    private var input: StrategyTypeInput? = null

    init {
        TextViewCompat.setTextAppearance(this, R.style.QuoteWidget_Trade_Title)
        gravity = Gravity.CENTER_HORIZONTAL
        resources.getDimensionPixelSize(R.dimen.spacing_small).let {
            setPadding(it, it, it, it)
        }
        setBackgroundResource(R.drawable.thick_divider)
        importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_YES
        isFocusable = true
    }

    override fun render(component: StrategyTypeInput, owner: LifecycleOwner) {
        inputViewDelegate.render(component, owner)

        input?.displayValue?.removeObserver(displayValueObserver)

        input = component

        component.displayValue.observe(owner, displayValueObserver)
    }
}
