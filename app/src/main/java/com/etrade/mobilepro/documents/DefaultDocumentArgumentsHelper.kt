package com.etrade.mobilepro.documents

import android.os.Bundle
import com.etrade.mobilepro.documents.details.DocumentDetailsFragmentArgs
import com.etrade.mobilepro.documents.router.DocumentArgumentsHelper
import javax.inject.Inject

private const val ADDITIONAL_PARAM_KEY = "additional_arguments"
class DefaultDocumentArgumentsHelper @Inject constructor() : DocumentArgumentsHelper {
    override fun getDocumentId(arguments: Bundle?): String =
        arguments?.let { DocumentDetailsFragmentArgs.fromBundle(it).documentId } ?: throw IllegalArgumentException("DocId is required for DocumentsFragment")

    override fun getAccountUuid(arguments: Bundle?): String? = arguments?.let { DocumentsFragmentArgs.fromBundle(it).accountUuid }

    override fun getDocumentType(arguments: Bundle?): DocumentType = arguments?.getString(ADDITIONAL_PARAM_KEY)?.let {
        if (it == DocumentType.TAX_DOCS.deepLinkPath) {
            DocumentType.TAX_DOCS
        } else {
            DocumentType.STATEMENTS
        }
    } ?: DocumentType.STATEMENTS
}
