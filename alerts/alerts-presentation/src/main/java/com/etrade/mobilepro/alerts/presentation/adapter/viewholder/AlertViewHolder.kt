package com.etrade.mobilepro.alerts.presentation.adapter.viewholder

import android.os.Handler
import android.os.Looper
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.alerts.api.model.AlertAction
import com.etrade.mobilepro.alerts.api.model.AlertItem
import com.etrade.mobilepro.alerts.presentation.R
import com.etrade.mobilepro.alerts.presentation.databinding.ItemAlertBinding
import com.etrade.mobilepro.common.customview.SwipeRevealLayout

class AlertViewHolder(
    private val binding: ItemAlertBinding,
    private val alertListener: ((AlertItem.Alert, AlertAction) -> Unit)?,
    private val swipeRevealListener: ((AlertItem.Alert, SwipeRevealLayout.Action) -> Unit)?,
    dragLocker: (() -> Boolean)?
) : RecyclerView.ViewHolder(binding.root) {
    private var item: AlertItem.Alert? = null

    private val handler = Handler(Looper.getMainLooper())

    fun onDetached() {
        handler.removeCallbacksAndMessages(null)
        binding.swipeReveal.close(animation = false)
    }

    init {
        with(binding) {
            alertCell.setOnClickListener {
                item?.let { item ->
                    alertListener?.invoke(item, AlertAction.SELECT)
                    bindTo(item, true)
                }
            }
            alertCell.rootView.findViewById<CheckBox>(R.id.alert_checkbox)
                .setOnClickListener {
                    item?.let { item ->
                        alertListener?.invoke(item, AlertAction.MARK)
                    }
                }
            swipeReveal.actionListener = { action ->
                item?.let {
                    swipeRevealListener?.invoke(it, action)
                }
            }
            deleteAlertBtn.setOnClickListener {
                item?.let { alert ->
                    alertListener?.invoke(alert, AlertAction.DELETE)
                }
            }
            swipeReveal.dragLocker = dragLocker
        }
    }

    fun bindTo(item: AlertItem.Alert, wasRead: Boolean) {
        this.item = item

        binding.alertCell.bindTo(item, wasRead)

        if (item.isDeleteButtonVisible) {
            binding.swipeReveal.open(animation = false)
        } else {
            handler.post { binding.swipeReveal.close(animation = true) }
        }
    }
}
