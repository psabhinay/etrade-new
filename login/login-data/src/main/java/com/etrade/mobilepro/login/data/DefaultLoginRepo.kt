package com.etrade.mobilepro.login.data

import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.login.api.repo.LoginRepo
import com.etrade.mobilepro.login.api.repo.LoginResult
import com.etrade.mobilepro.login.data.restclient.LoginService
import com.etrade.mobilepro.login.data.restclient.dto.LoginUserSSOResponse
import com.etrade.mobilepro.session.api.User
import org.simpleframework.xml.core.Persister
import retrofit2.Retrofit
import javax.inject.Inject

const val LOGIN_TARGET = "/e/t/mobile/LoginUserSSO?PLATFORM_TOKEN=ANDROID"
const val LOGIN_LANG = "us_english"

class DefaultLoginRepo @Inject constructor(
    private val user: User,
    private val applicationInfo: ApplicationInfo,
    retrofit: Retrofit
) : LoginRepo {

    private val xmlParser = Persister()

    private val loginService: LoginService = retrofit.create(LoginService::class.java)

    override suspend fun login(username: String, password: String): ETResult<LoginResult> {

        return runCatchingET {
            val result = loginService.login(applicationInfo.appVersionCode(), LOGIN_TARGET, username, password, LOGIN_LANG)
            parseLoginResult(parseLoginResponse(result), user)
        }
    }

    private fun parseLoginResponse(xml: String): LoginUserSSOResponse? {
        return xmlParser.read(LoginUserSSOResponse::class.java, xml, false)
    }
}
