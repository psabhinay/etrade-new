package com.etrade.mobilepro.watchlist.presentation

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.painterResource
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.eo.corelibandroid.createViewModel
import com.etrade.mobilepro.common.compose.theme.EtradeTheme
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.compose.etdialogs.edit.TextValueEditDialog
import com.etrade.mobilepro.compose.etdialogs.edit.show
import com.etrade.mobilepro.compose.etdialogs.info.InfoDialog
import com.etrade.mobilepro.compose.etdialogs.info.show
import com.etrade.mobilepro.etcompose.actionsheet.ActionSheet
import com.etrade.mobilepro.etcompose.tabhorizontalpager.CombinedTabHorizontalPager
import com.etrade.mobilepro.etcompose.tabhorizontalpager.ETPagerState
import com.etrade.mobilepro.etcompose.tabhorizontalpager.PagerItem
import com.etrade.mobilepro.etcompose.tabhorizontalpager.rememberPagerState
import com.etrade.mobilepro.etcompose.toolbar.ETToolbar
import com.etrade.mobilepro.etcompose.toolbar.ETToolbarBridge
import com.etrade.mobilepro.etcompose.toolbar.ToolbarAction
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.searchingapi.searchResultSymbol
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.etrade.mobilepro.watchlist.overview.viewmodel.ViewEffects
import com.etrade.mobilepro.watchlist.overview.viewmodel.WatchlistOverviewViewModel
import com.etrade.mobilepro.watchlist.overview.viewmodel.watchlistDisplay
import com.etrade.mobilepro.watchlist.presentation.EditWatchlistActivity.Companion.EXTRA_WATCHLIST_ID
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

const val WL_DROPDOWN_TAG = "USER WATCH LIST"
const val PATTERN_WATCHLIST_NAME_FILTER = "[^a-zA-Z0-9_ $%./()-]"

@Suppress("TooManyFunctions", "LargeClass")
class WatchlistContainerFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    private val watchlistOverviewViewModelFactory: WatchlistOverviewViewModel.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val searchIntentFactory: SearchIntentFactory,
    private val navRouter: NewsRouter,
    walkthroughStatesViewModel: WalkthroughStatesViewModel,
    mainNavigation: MainNavigation,
    isAuth: UserAuthenticationState
) : Fragment() {
    private val pagerState = ETPagerState((arguments?.get("tabIndex") ?: 0) as Int)

    private val watchlistId: String?
        get() = arguments?.get("watchlistId")?.toString()

    private val newsTabDelegate = WatchlistNewsTabDelegate(this, isAuth, viewModelFactory)
    private val overviewTabDelegate = WatchlistOverviewTabDelegate(
        this,
        walkthroughStatesViewModel,
        mainNavigation,
        onUnAuthFilterClickAction = {
            newsTabDelegate.resetState() // need to reset to force data reloading
            navRouter.navigateWatchlistTabWithLogin(findNavController(), tabIndex = 0)
        },
        containerViewModel = { viewModel },
        viewModelFactory,
        watchlistOverviewViewModelFactory,
        watchlistId
    )

    private val overviewViewModel: WatchlistOverviewViewModel by createViewModel {
        watchlistOverviewViewModelFactory.create(
            watchlistId
        )
    }
    private val viewModel by viewModels<WatchlistContainerViewModel> { viewModelFactory }

    private lateinit var snackbarUtil: SnackBarUtil

    private val symbolLookupRequest = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        onCodeSymbolSearch(result.resultCode, result.data)
    }

    private val editWatchlistRequest = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        onEditWatchlist(result.resultCode, result.data)
    }

    @ExperimentalFoundationApi
    private val pagerItems: List<PagerItem>
        get() = listOf(
            PagerItem(
                title = getString(R.string.watchlist_overview),
                content = overviewTabDelegate.watchlistOverview()
            ),
            PagerItem(
                title = getString(R.string.watchlist_news),
                content = newsTabDelegate.watchlistNews()
            )
        )

    private val toolbarBridge by lazy { ETToolbarBridge(this) }

    @ExperimentalComposeUiApi
    @Suppress("LongMethod")
    @ExperimentalFoundationApi
    @ExperimentalMaterialApi
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            EtradeTheme {
                Scaffold { innerPadding ->
                    CombinedTabHorizontalPager(
                        items = pagerItems,
                        modifier = Modifier.padding(innerPadding),
                        pagerState = rememberPagerState(pagerState)
                    )
                }

                BuildActionSheet()

                TextValueEditDialog(viewModel.newWatchlistNameEditor.state)
                InfoDialog(viewModel.deleteWatchlistConfirmationDialog.state)
                SideEffect {
                    toolbarBridge.updateToolbar {
                        ETToolbar(
                            title = context?.getString(R.string.watchlist_toolbar_title) ?: "",
                            actionStart = toolbarActionStart(),
                            actionEnd = toolbarActionEnd { viewModel.setBottomSheetVisibility(true) }
                        )
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        toolbarBridge.removeToolbar()
    }

    @ExperimentalComposeUiApi
    @ExperimentalMaterialApi
    @Composable
    fun BuildActionSheet() {
        val watchlist by overviewViewModel.selectedWatchList.observeAsState()
        val isBottomSheetVisible by viewModel.isBottomSheetVisible.observeAsState()

        val onDelete = watchlist?.let {
            if (it.isDeletable) {
                {
                    viewModel.deleteWatchlistConfirmationDialog.show()
                }
            } else null
        }

        ActionSheet(
            bottomSheetItems = viewModel.actionSheetItems(
                onManage = {
                    overviewViewModel.openEditWatchlistActivity()
                },
                onCreateAuth = {
                    viewModel.newWatchlistNameEditor.show()
                },
                onCreateUnAuth = {
                    newsTabDelegate.resetState() // need to reset to force data reloading
                    navRouter.navigateWatchlistTabWithLogin(findNavController(), tabIndex = 0)
                },
                onDelete = onDelete
            ),
            isActionSheetVisible = isBottomSheetVisible ?: false
        ) { viewModel.setBottomSheetVisibility(it) }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        newsTabDelegate.onViewCreated()
        overviewTabDelegate.onViewCreated()

        snackbarUtil = snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
        bindViewModels()
    }

    @Suppress("LongMethod")
    private fun bindViewModels() {
        viewModel.crudCallbacks = overviewViewModel.watchlistCRUDCallbacks
        overviewViewModel.selectedWatchList.observe(viewLifecycleOwner) {
            overviewViewModel.refreshCurrentWatchList()
        }
        overviewViewModel.startSearchSignal.observe(
            viewLifecycleOwner,
            { params ->
                activity?.let {
                    val (intent, options) = searchIntentFactory.createSearchIntentAndOptions(it, params)
                    symbolLookupRequest.launch(intent, options)
                }
            }
        )

        overviewViewModel.viewEffects.observe(
            viewLifecycleOwner,
            {
                when (it) {
                    is ViewEffects.SnackBarMessage -> {
                        snackbarUtil.snackbar(it.message, Snackbar.LENGTH_LONG)?.show()
                    }
                    is ViewEffects.GenericError -> {
                        snackbarUtil.snackbar(getString(R.string.error_message_general), Snackbar.LENGTH_LONG)?.show()
                    }
                    is ViewEffects.AddedToWatchlist -> {
                        showAddedToWatchlistSnackBar(it.symbol)
                        newsTabDelegate.onWatchlistContentChanged()
                    }
                    is ViewEffects.WatchlistCreated -> {
                        newsTabDelegate.forceUpdateTab()
                        overviewViewModel.openSearchActivity(SearchQuery(SearchType.SYMBOL), finishOnSearch = true)
                    }
                }
            }
        )

        overviewViewModel.startWatchlistEditSignal.observe(
            viewLifecycleOwner,
            { params ->
                context?.also { context ->
                    val intent = EditWatchlistActivity.intent(context) {
                        emptyListMessageResId = R.string.watchlist_no_items
                        watchlist {
                            watchlistId = params.watchlistId
                        }
                    }
                    editWatchlistRequest.launch(intent)
                }
            }
        )
    }

    private fun onCodeSymbolSearch(resultCode: Int, data: Intent?) {
        val isWatchlistJustCreated = viewModel.consumeIsWatchlistJustCreated()
        if (resultCode == Activity.RESULT_OK) {
            data?.searchResultSymbol?.let {
                overviewViewModel.addSymbolToWatchList(
                    ticker = it,
                    onCompletion = {
                        newsTabDelegate.onWatchlistContentChanged()
                    }
                )
            }
        } else if (isWatchlistJustCreated) {
            snackbarUtil.snackbar(
                getString(R.string.watchlist_create_success),
                Snackbar.LENGTH_LONG
            )?.show()
        }
    }

    private fun onEditWatchlist(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val watchlistId = data?.getStringExtra(EXTRA_WATCHLIST_ID)
            if (watchlistId != null) {
                viewModel.deleteWatchlist(watchlistId)
            } else {
                overviewViewModel.getUserWatchlists()
            }
            newsTabDelegate.onWatchlistContentChanged()
        }
    }

    private fun toolbarActionStart(): ToolbarAction = ToolbarAction.IconAction(
        onClick = { overviewViewModel.openSearchActivity(SearchQuery(SearchType.SYMBOL), finishOnSearch = true) },
        icon = {
            Icon(
                imageVector = Icons.Rounded.Add,
                contentDescription = context?.getString(R.string.watchlist_action_in_watchlist) ?: ""
            )
        }
    )

    @ExperimentalMaterialApi
    private fun toolbarActionEnd(onActionEnd: () -> Unit): ToolbarAction = ToolbarAction.IconAction(
        onClick = onActionEnd,
        icon = {
            Icon(
                painter = painterResource(id = R.drawable.ic_more_horizontal),
                contentDescription = getString(R.string.watchlist_edit)
            )
        }
    )

    private fun showAddedToWatchlistSnackBar(symbol: SearchResultItem.Symbol) {
        if (symbol.instrumentType == InstrumentType.EQ || symbol.instrumentType == InstrumentType.INDX) {
            snackbarUtil.snackbarWithCustomViewAction(
                message = resources.getString(R.string.watchlist_symbol_added_message, symbol.watchlistDisplay),
                titleId = R.string.watchlist_action_set_alert,
                duration = Snackbar.LENGTH_SHORT,
                action = {
                    navRouter.navigateToSetAlert(findNavController(), symbol)
                }
            )?.show()
        } else {
            snackbarUtil.snackbar(
                resources.getString(R.string.watchlist_symbol_added_message, symbol.watchlistDisplay),
                Snackbar.LENGTH_SHORT
            )?.show()
        }
    }
}
