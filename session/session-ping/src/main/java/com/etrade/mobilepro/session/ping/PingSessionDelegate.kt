package com.etrade.mobilepro.session.ping

import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.appstart.api.AppStartRepo
import com.etrade.mobilepro.common.di.ApplicationScope
import com.etrade.mobilepro.session.api.SessionEvents
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.session.ping.repo.PingSessionRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PingSessionDelegate @Inject constructor(
    @ApplicationScope val appScope: CoroutineScope,
    private val pingSessionRepo: PingSessionRepo,
    private val isUserLoggedIn: UserAuthenticationState,
    private val appStartRepo: AppStartRepo
) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    private val _sessionEvent: MutableLiveData<SessionEvents> = MutableLiveData()

    val sessionEvent: LiveData<SessionEvents> = _sessionEvent

    private val pingSessionEventHandler = Handler(Looper.getMainLooper())

    private var isStarted: Boolean = false

    private val pingTaskRunnable = Runnable {
        if (isUserLoggedIn()) {
            pingSession()
        }
    }

    private fun pingSession() {
        appScope.launch {
            val message = pingSessionRepo.ping()

            if (isStarted) {
                onPingSession(message)
            } else {
                logger.debug("Ignoring invalid ping message")
            }
        }
    }

    private fun onPingSession(msg: PingMessage) {
        if (msg.isSessionValid) {
            val interval = appStartInterval.takeIf { it > 0L } ?: msg.nextCheckInterval
            logger.debug("interval is: $interval")
            pingSessionEventHandler.postAtTime(pingTaskRunnable, SystemClock.uptimeMillis().plus(interval))
        } else {
            _sessionEvent.postValue(SessionEvents.MSG_SESSION_TERMINATED)
        }

        logger.debug("Android ping session : Session Valid: ${msg.isSessionValid} ${msg.errorMsg ?: ""}")
    }

    var appStartInterval = DEFAULT_PING_SESSION_INTERVAL_MILLIS
    fun start() {
        if (!isStarted) {
            appScope.launch {
                appStartRepo.getPingSessionCallInterval().fold(
                    onSuccess = { interval ->
                        appStartInterval = interval?.let { TimeUnit.SECONDS.toMillis(it) } ?: DEFAULT_PING_SESSION_INTERVAL_MILLIS
                    },
                    onFailure = {
                        logger.error("appstart call failed: ${it.localizedMessage}")
                    }
                )

                // Initial delay
                delay(DEFAULT_PING_SESSION_INTERVAL_MILLIS)
                pingTaskRunnable.run()
                isStarted = true
            }
        }
    }

    fun stop() {
        isStarted = false
        pingSessionEventHandler.removeCallbacks(pingTaskRunnable)
    }
}
