package com.etrade.mobilepro.watchlist.mapper

import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.getTimeStamp
import com.etrade.mobilepro.util.domain.data.deriveDayChange
import com.etrade.mobilepro.util.domain.data.deriveDayChangePercent
import com.etrade.mobilepro.watchlistapi.WatchlistEntry

@Suppress("LongMethod")
internal fun MobileQuote.toWatchlistEntry(entryId: String?, isExtendedHours: Boolean): WatchlistEntry {
    val price = quotePrice.lastPrice.toBigDecimalOrNull()
    val extendedHoursPrice = extendedQuoteHrDetail?.lastPrice?.toBigDecimalOrNull()
    val dayChange = deriveDayChange(extendedQuoteHrDetail?.change ?: "0", quotePrice.change)

    return WatchlistEntry(
        entryID = entryId,
        ticker = symbol,
        description = description,
        underlyingSymbol = underlyingSymbol,
        typeCode = typeCode,
        lastTradeTime = lastTradeTime,
        lastTradeTimeStamp = getTimeStamp(isExtendedHours),
        lastPrice = if (isExtendedHours && extendedHoursPrice != null) extendedHoursPrice else price,
        change = dayChange?.toBigDecimalOrNull(),
        changePercent = deriveDayChangePercent(dayChange, quotePrice.lastPrice, quotePrice.change)?.toBigDecimalOrNull(),
        volume = quotePrice.volume.toBigDecimalOrNull(),
        bid = quotePrice.bid.toBigDecimalOrNull(),
        ask = quotePrice.ask.toBigDecimalOrNull(),
        dayHigh = dayHigh.toBigDecimalOrNull(),
        dayLow = dayLow.toBigDecimalOrNull(),
        week52High = week52High.toBigDecimalOrNull(),
        week52Low = week52Low.toBigDecimalOrNull(),
        priceEarningsRatio = pe.toBigDecimalOrNull(),
        earningsPerShare = eps.toBigDecimalOrNull()
    )
}
