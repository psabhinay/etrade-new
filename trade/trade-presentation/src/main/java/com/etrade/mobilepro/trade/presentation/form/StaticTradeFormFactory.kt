package com.etrade.mobilepro.trade.presentation.form

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.presentation.form.delegate.TradeFormDelegates
import com.etrade.mobilepro.trade.presentation.form.delegate.TradeFormFactoryDelegate
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class StaticTradeFormFactory @Inject constructor(
    private val delegates: TradeFormDelegates
) : TradeFormFactory {

    override suspend fun createTradeForm(
        accounts: List<TradeAccount>,
        accountDelegate: SelectedAccountDelegate,
        parameter: CreateTradeFormParameter
    ): ETResult<LiveData<Form>> {
        // Main dispatcher is being used here because LiveData.observeForever(Observable) can only be called on the main thread.
        return withContext(Dispatchers.Main) {
            runCatchingET {
                MutableLiveData<Form>().apply {
                    val onFormChangedListener = object : TradeFormFactoryDelegate.OnFormChangeListener {
                        override fun onSecurityType(securityType: SecurityType) {
                            val parameterWithoutDefaultValues = parameter.copy(defaultValues = emptyMap())
                            accountDelegate.onSecurityType(securityType)
                            value = requireDelegate(TradeFormType(securityType)).createForm(
                                accounts,
                                accountDelegate,
                                parameterWithoutDefaultValues,
                                this
                            )
                        }

                        override fun onAdvancedOrderType(formType: TradeFormType, snapshot: InputValuesSnapshot) {
                            value = requireDelegate(formType).createForm(
                                accounts,
                                accountDelegate, parameter, this
                            ).apply {
                                injectValuesFrom(snapshot)
                            }
                        }
                    }

                    value = requireDelegate(TradeFormType(parameter.securityType ?: SecurityType.STOCK))
                        .createForm(accounts, accountDelegate, parameter, onFormChangedListener)
                }
            }
        }
    }

    private fun requireDelegate(tradeOrderType: TradeFormType): TradeFormFactoryDelegate = requireNotNull(delegates.getFormDelegate(tradeOrderType))
}
