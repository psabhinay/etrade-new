package com.etrade.mobilepro.quote.dto

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class MutualFundEntryDetailResponseDto(
    @Json(name = "FundDetails")
    val fundDetails: FundDetails?
) : BaseDataDto() {
    @JsonClass(generateAdapter = true)
    class FundDetails(
        @Json(name = "productFundDetail")
        val productFundDetail: ProductFundDetail?
    )

    @JsonClass(generateAdapter = true)
    class ProductFundDetail(
        @Json(name = "fundName")
        val fundName: String,
        @Json(name = "typeCode")
        val typeCode: String,
        @Json(name = "symbol")
        val symbol: String
    )
}
