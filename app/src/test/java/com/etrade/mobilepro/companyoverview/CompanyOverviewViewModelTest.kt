package com.etrade.mobilepro.companyoverview

import android.app.Application
import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.mod.api.CompanyOverview
import com.etrade.mobilepro.util.Resource
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import org.robolectric.annotation.LooperMode

private const val SECTOR = "SECTOR"
private const val INDUSTRY = "INDUSTRY"
private const val OVERVIEW = "OVERVIEW"
private const val WEB_ADDRESS = "WEB_ADDRESS"

@RunWith(AndroidJUnit4::class)
@Suppress("DEPRECATION")
@LooperMode(value = LooperMode.Mode.LEGACY)
@Config(application = Application::class)
class CompanyOverviewViewModelTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()

    private val companyOverview: CompanyOverview = mock {
        on { sector } doReturn SECTOR
        on { industry } doReturn INDUSTRY
        on { overview } doReturn OVERVIEW
        on { webAddress } doReturn WEB_ADDRESS
    }

    private val companyOverviewProvider: CompanyOverviewProvider = mock {
        on { createCompanyOverviewLiveData(any()) } doReturn MutableLiveData<Resource<CompanyOverview>>().apply { value = Resource.Success(companyOverview) }
    }

    private val sut = CompanyOverviewViewModel(context.resources, companyOverviewProvider)

    @Test
    fun `company overview is formatted to design spec`() {
        val observer = mock<Observer<Resource<CharSequence?>>>()
        sut.showCompanyOverview("AAPL")
        verify(companyOverviewProvider).createCompanyOverviewLiveData("AAPL")

        sut.text.observeForever(observer)
        verify(observer, atLeastOnce()).onChanged(Resource.Success("$SECTOR: $INDUSTRY\n$WEB_ADDRESS\n\n$OVERVIEW"))
    }
}
