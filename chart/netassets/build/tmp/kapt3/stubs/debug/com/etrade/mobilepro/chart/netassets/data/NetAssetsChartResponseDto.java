package com.etrade.mobilepro.chart.netassets.data;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0001\u0018\u00002\u00020\u0001B\u0011\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/etrade/mobilepro/chart/netassets/data/NetAssetsChartResponseDto;", "", "data", "Lcom/etrade/mobilepro/chart/netassets/data/NetAssetsChartResponseDataDto;", "(Lcom/etrade/mobilepro/chart/netassets/data/NetAssetsChartResponseDataDto;)V", "getData", "()Lcom/etrade/mobilepro/chart/netassets/data/NetAssetsChartResponseDataDto;", "netassets_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class NetAssetsChartResponseDto {
    @org.jetbrains.annotations.Nullable()
    private final com.etrade.mobilepro.chart.netassets.data.NetAssetsChartResponseDataDto data = null;
    
    public NetAssetsChartResponseDto(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "data")
    com.etrade.mobilepro.chart.netassets.data.NetAssetsChartResponseDataDto data) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.etrade.mobilepro.chart.netassets.data.NetAssetsChartResponseDataDto getData() {
        return null;
    }
}