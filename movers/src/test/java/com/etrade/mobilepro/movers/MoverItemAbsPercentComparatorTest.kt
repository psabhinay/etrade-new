package com.etrade.mobilepro.movers

import org.junit.Test
import java.math.BigDecimal

class MoverItemAbsPercentComparatorTest {

    private val comparator = MoverItemAbsPercentComparator()

    @Test
    fun bigPercentLooserIsHigherThanSmallPercentGainer() {
        val bigLooser = createMoverItem(percentageChange = BigDecimal.valueOf(-100))
        val smallGainer = createMoverItem(percentageChange = BigDecimal.valueOf(50))

        assert(comparator.compare(bigLooser, smallGainer) > 0)
    }

    @Test
    fun bigPercentGainerIsHigherThanSmallPercentGainer() {
        val bigGainer = createMoverItem(percentageChange = BigDecimal.valueOf(20))
        val smallGainer = createMoverItem(percentageChange = BigDecimal.valueOf(5))

        assert(comparator.compare(bigGainer, smallGainer) > 0)
    }

    @Test
    fun bigPercentLooserIsHigherSmallPercentLooser() {
        val bigLooser = createMoverItem(percentageChange = BigDecimal.valueOf(-25))
        val smallLooser = createMoverItem(percentageChange = BigDecimal.valueOf(-20))

        assert(comparator.compare(bigLooser, smallLooser) > 0)
    }

    @Test
    fun percentGainerIsEqualToSamePercentLooser() {
        val gainer = createMoverItem(percentageChange = BigDecimal.valueOf(15))
        val looser = createMoverItem(percentageChange = BigDecimal.valueOf(-15))

        assert(comparator.compare(gainer, looser) == 0)
    }

    private fun createMoverItem(percentageChange: BigDecimal) = MoverItem(
        ticker = "AAPL",
        displaySymbol = "AAPL",
        price = "100",
        gain = "1",
        gainPercentage = "1%",
        decimalPercentChange = percentageChange,
        shouldDisplayOptionsData = true,
    )
}
