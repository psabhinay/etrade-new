package com.etrade.mobilepro.optionschains.data.dto

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.etrade.mobilepro.optionschains.api.BaseOption
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

/**
 * Outermost data class "data" obj
 */
@JsonClass(generateAdapter = true)
data class OptionsChainDto(
    @Json(name = "RootList")
    val rootList: List<String>? = null,
    @Json(name = "OptionChainsResponse")
    val optionChainsResponse: OptionChainsResponseDto?
) : BaseDataDto()

/**
 * Object references for options month list and options by month
 */
@JsonClass(generateAdapter = true)
data class OptionChainsResponseDto(
    @Json(name = "optionMonthsList")
    val optionsMonthsList: OptionsMonthListDto,
    @Json(name = "optionsByMonth")
    val optionsByMonth: OptionsByMonthDto? = null
)

/**
 * Object containing references to options month list
 */
@JsonClass(generateAdapter = true)
data class OptionsMonthListDto(
    @Json(name = "selectedED")
    val selectedExpirationDate: ExpirationDateDto? = null,
    @Json(name = "hasAdjustedFlag")
    val hasAdjustedFlag: String,
    @Json(name = "hasStandardFlag")
    val hasStandardFlag: String,
    @Json(name = "rootSymbol")
    val rootSymbol: String,
    @Json(name = "ed")
    val expirationDateList: List<ExpirationDateDto>
)

/**
 * Options by month object
 */
@JsonClass(generateAdapter = true)
data class OptionsByMonthDto(
    @Json(name = "quoteType")
    val quoteType: Int,
    @Json(name = "timeStamp")
    val timeStamp: String,
    @Json(name = "nearPrice")
    val nearPrice: BigDecimal,
    @Json(name = "selectedED")
    val expirationDate: ExpirationDateDto? = null,
    @Json(name = "optionPairGroup")
    val optionPairGroup: Map<String, List<OptionPairCallsAndPutsDto>?>?
)

/**
 * Options calls and puts
 */
@JsonClass(generateAdapter = true)
data class OptionPairCallsAndPutsDto(
    @Json(name = "call")
    val call: OptionDto?,
    @Json(name = "put")
    val put: OptionDto?
)

/**
 * Data class that wraps a CallModel object
 */
@JsonClass(generateAdapter = true)
data class OptionDto(
    @Json(name = "strikePrice")
    override val strikePrice: BigDecimal,
    @Json(name = "displaySymbol")
    override val displaySymbol: String,
    @Json(name = "symbol")
    override val symbol: String,
    @Json(name = "optionType")
    val optionTypeString: String,
    @Json(name = "rootSymbol")
    val rootSymbol: String,
    @Json(name = "bid")
    public override val bid: BigDecimal,
    @Json(name = "ask")
    public override val ask: BigDecimal,
    @Json(name = "inTheMoney")
    val inTheMoney: String,
    @Json(name = "volume")
    public override val volume: String,
    @Json(name = "openInterest")
    public override val openInterest: String,
    @Json(name = "lastPrice")
    public override val lastPrice: BigDecimal,
    @Json(name = "timeStamp")
    val timeStamp: String,
    @Json(name = "netChange")
    public override val change: BigDecimal,
    @Json(name = "bidSize")
    public override val bidSize: String,
    @Json(name = "askSize")
    public override val askSize: String,
    @Json(name = "amOption")
    override val isAMOption: Boolean = false
) : BaseOption() {

    override val isInTheMoney: Boolean
        get() {
            if (inTheMoney == "y") {
                return true
            }
            return false
        }
}
