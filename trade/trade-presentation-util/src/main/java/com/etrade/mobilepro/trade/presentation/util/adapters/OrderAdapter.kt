package com.etrade.mobilepro.trade.presentation.util.adapters

import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.presentation.util.OrderDisplayItem

interface OrderAdapter {
    fun createOrderDisplayItem(order: Order): OrderDisplayItem
}
