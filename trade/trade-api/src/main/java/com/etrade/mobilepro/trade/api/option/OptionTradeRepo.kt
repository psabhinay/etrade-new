package com.etrade.mobilepro.trade.api.option

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.OrderType

interface OptionTradeRepo {
    suspend fun getStrategyIdentifier(symbol: String, legs: List<TradeLeg>, params: InputValuesSnapshot): ETResult<OptionTradeStrategyResponse>
    suspend fun getStrategyIdentifier(
        symbol: String,
        legs: List<TradeLeg>,
        accountId: String,
        orderType: OrderType
    ): ETResult<OptionTradeStrategyResponse>
}
