package com.etrade.mobilepro.transfermoney.presentation.common

import android.os.Parcel
import androidx.core.os.ParcelCompat.readBoolean
import androidx.core.os.ParcelCompat.writeBoolean
import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import kotlinx.parcelize.Parceler
import java.math.BigInteger

object TransferMoneyAccountParceler : Parceler<TransferMoneyAccount> {

    override fun create(parcel: Parcel): TransferMoneyAccount = parcel.run {
        TransferMoneyAccount(
            title = requireNotNull(readString()),
            accountId = requireNotNull(readString()),
            nickname = requireNotNull(readString()),
            instNo = requireNotNull(readSerializable()) as BigInteger,
            accountType = requireNotNull(readString()),
            accountCategory = requireNotNull(readString()),
            canBeFrom = readBoolean(this),
            canBeTo = readBoolean(this)
        )
    }

    override fun TransferMoneyAccount.write(parcel: Parcel, flags: Int) {
        parcel.apply {
            writeString(title)
            writeString(accountId)
            writeString(nickname)
            writeSerializable(instNo)
            writeString(accountType)
            writeString(accountCategory)
            writeBoolean(this, canBeFrom)
            writeBoolean(this, canBeTo)
        }
    }
}
