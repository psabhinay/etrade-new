package com.etrade.mobilepro.trade.api

enum class AdvancedTradeType {
    Contingent,
    StopLoss,
    None
}
