package com.etrade.mobilepro.chartiq.data

internal object ChartIqCommandNames {

    const val JS_BRIDGE_PREFIX = "javascript:jsbridge."
    const val INIT_CONFIG = JS_BRIDGE_PREFIX + "initConfig"
    const val ON_CHART_STREAMING = JS_BRIDGE_PREFIX + "onChartStreaming"
    const val CLOSE_SETTINGS = JS_BRIDGE_PREFIX + "closeSettings"
    const val TOGGLE_EXTENDED_HOUR = JS_BRIDGE_PREFIX + "toggleExtHour"
    const val ON_SYMBOL_LOOKUP_CALLBACK = JS_BRIDGE_PREFIX + "onSymLookupCallback"
    const val UPDATE_SESSION = JS_BRIDGE_PREFIX + "onLoginSuccess"
    const val HANDLE_LOAD_DATA_RESPONSE = JS_BRIDGE_PREFIX + "handleChartDataResponse"
}
