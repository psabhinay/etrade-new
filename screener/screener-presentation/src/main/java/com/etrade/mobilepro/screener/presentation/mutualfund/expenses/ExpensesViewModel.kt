package com.etrade.mobilepro.screener.presentation.mutualfund.expenses

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.mutualfund.ExpensesFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.MutualFund
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class ExpensesViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @MutualFund screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<ExpensesFilter, ExpensesViewModel.ViewState>(aggregator, ScreenerType.MutualFund.Expenses, screenerInteractor) {
    override fun provideInitialState(initialFilter: ExpensesFilter): ViewState = ViewState(filter = initialFilter)

    data class ViewState(
        override val filter: ExpensesFilter
    ) : BaseFilterScreenViewModel.ViewState<ExpensesFilter>
}
