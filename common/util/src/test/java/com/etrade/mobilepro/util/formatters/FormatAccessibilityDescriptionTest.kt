package com.etrade.mobilepro.util.formatters

import com.etrade.mobilepro.util.formatAccessibilityDescription
import com.etrade.mobilepro.util.hasDollarPattern
import org.junit.jupiter.api.Test

class FormatAccessibilityDescriptionTest {
    @Test
    fun `test Invalid Dollar Amount`() {
        assert(!"NotValid$100.00".hasDollarPattern())
    }

    @Test
    fun `test Matches only Dollar Amount`() {
        assert("$100.00".hasDollarPattern())
    }

    @Test
    fun `test Matches only Negative Dollar Amount`() {
        assert("-$100.00".hasDollarPattern())
    }

    @Test
    fun `test Matches only Dollar Amount With Percentage`() {
        assert("$100.00(100.00%)".hasDollarPattern())
    }

    @Test
    fun `test Matches only Dollar Amount With Negative Percentage`() {
        assert("-$19,107,100.00(-134.00%)".hasDollarPattern())
        assert("$19,107,100.00(-134.00%)".hasDollarPattern())
        assert("$-19,107,100.00(-134.00%)".hasDollarPattern())
    }

    @Test
    fun `test Null Or Empty to format `() {
        assert(null.formatAccessibilityDescription(skipWords = listOf(".")).isEmpty())
        assert("".formatAccessibilityDescription(skipWords = listOf(".")).isEmpty())
    }

    @Test
    fun `test skip Dot Character `() {
        assert(
            "Other $100.00".formatAccessibilityDescription(skipWords = listOf(".")).contains(".")
        )
    }

    @Test
    fun `test do not skip Other Characters `() {
        assert(
            "Other $100.00".formatAccessibilityDescription(skipWords = listOf(".", "Other"))
                .contains("Other")
        )
    }
}
