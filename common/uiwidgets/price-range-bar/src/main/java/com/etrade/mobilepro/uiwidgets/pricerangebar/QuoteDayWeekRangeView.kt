package com.etrade.mobilepro.uiwidgets.pricerangebar

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.view.ViewCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.quoteapi.QuoteRangeBarData
import com.etrade.mobilepro.uiwidgets.pricerangebar.databinding.ViewQuote52weekDayRangeBinding
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom

/***
 * This class assumes that, activity/fragment calling this has included
 * quote_redesigned_header.xml layout in its main layout.
 *
 * fragment should called init method with fragmentView as input
 *
 */
@Suppress("SetTextI18n")
class QuoteDayWeekRangeView : LinearLayout {
    private var hi52: Double = 0.0
    private var low52: Double = 0.0
    private var dayHi: Double = 0.0
    private var dayLow: Double = 0.0
    private var weekHigh52Date: String? = null
    private var weekLow52Date: String? = null
    private lateinit var lifecycleOwner: LifecycleOwner
    private var data: LiveData<QuoteRangeBarData>? = null
    private val observer = Observer<QuoteRangeBarData> { data ->
        bindTo(data)
    }

    private val binding =
        ViewQuote52weekDayRangeBinding.inflate(LayoutInflater.from(context), this, true)

    private val seekBar: RangeSeekBar<Double> = RangeSeekBar(context)

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defStyle: Int) : super(context, attributeSet, defStyle)

    init {
        seekBar.setMaxText("--.--")
        seekBar.setMinText("--.--")
        binding.rangebarContainer.addView(seekBar)
        ViewCompat.setImportantForAccessibility(seekBar, ViewCompat.IMPORTANT_FOR_ACCESSIBILITY_NO)
    }

    fun setLifecycleOwner(lifecycleOwner: LifecycleOwner) {
        this.lifecycleOwner = lifecycleOwner
    }

    fun setData(data: LiveData<QuoteRangeBarData>) {
        this.data = data
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        data?.observe(lifecycleOwner, observer)
    }

    override fun onDetachedFromWindow() {
        data?.removeObserver(observer)
        super.onDetachedFromWindow()
    }

    /**
     * This method should be called after a quote fetch to update the ticker
     * information.
     */
    @Suppress("LongMethod")
    private fun bindTo(barData: QuoteRangeBarData) {
        var showRangeBar = true

        try {
            hi52 = barData.week52High.toDouble()
            low52 = barData.week52Low.toDouble()
            dayHi = barData.dayHigh.toDouble()
            dayLow = barData.dayLow.toDouble()
        } catch (ex: NumberFormatException) {
            hi52 = 0.0
            low52 = 0.0
            dayHi = 0.0
            dayLow = 0.0
            showRangeBar = false
        }

        seekBar.setRange(low52, hi52)
        seekBar.minimumWidth = binding.rangebarContainer.width

        if (dayHi in low52..hi52) {
            seekBar.setSelectedMaxValue(dayHi)
        }

        if (dayLow in low52..hi52) {
            seekBar.setSelectedMinValue(dayLow)
        }

        if (showRangeBar) {
            seekBar.setMaxText(MarketDataFormatter.formatMarketDataCustom(dayHi.toBigDecimal()))
            seekBar.setMinText(MarketDataFormatter.formatMarketDataCustom(dayLow.toBigDecimal()))
            binding.weekLow52.text =
                MarketDataFormatter.formatMarketDataCustom(low52.toBigDecimal())
            binding.weekHigh52.text =
                MarketDataFormatter.formatMarketDataCustom(hi52.toBigDecimal())
            weekHigh52Date = barData.week52HighDate
            binding.weekHigh52Date.text = "($weekHigh52Date)"
            weekLow52Date = barData.week52LowDate
            binding.weekLow52Date.text = "($weekLow52Date)"
            seekBar.setRange(low52, hi52)
        } else {
            seekBar.setMaxText(context.getString(R.string.empty_default))
            seekBar.setMinText(context.getString(R.string.empty_default))
            binding.weekLow52.text = context.getString(R.string.empty_default)
            binding.weekHigh52.text = context.getString(R.string.empty_default)
            binding.weekHigh52Date.text = "(${context.getString(R.string.empty_date_default)})"
            binding.weekLow52Date.text = "(${context.getString(R.string.empty_date_default)})"
        }

        contentDescription =
            "${resources.getString(R.string.fifty_two_week_low)} ${binding.weekLow52.text} $weekLow52Date, " +
            "${resources.getString(R.string.day_low)} ${
            MarketDataFormatter.formatMarketDataCustom(
                dayLow.toBigDecimal()
            )
            }, " +
            "${resources.getString(R.string.day_high)} ${
            MarketDataFormatter.formatMarketDataCustom(
                dayHi.toBigDecimal()
            )
            }, " +
            "${resources.getString(R.string.fifty_two_week_high)} ${binding.weekHigh52.text} $weekHigh52Date"
    }
}
