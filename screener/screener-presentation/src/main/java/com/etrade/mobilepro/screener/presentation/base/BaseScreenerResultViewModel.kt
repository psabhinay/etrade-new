package com.etrade.mobilepro.screener.presentation.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.screener.api.result.ResultDataSet
import com.etrade.mobilepro.screener.api.result.ResultSymbolCell
import com.etrade.mobilepro.screener.api.result.ScreenerResult
import com.etrade.mobilepro.screener.data.result.ResultColumn
import com.etrade.mobilepro.screener.data.result.ResultColumnTitleProvider
import com.etrade.mobilepro.screener.data.result.ResultColumnsProvider
import com.etrade.mobilepro.screener.data.result.ResultRowDataProvider
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingSubscriptionController
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.sorting.TableDataSortable
import com.etrade.mobilepro.tableviewutils.presentation.sorting.TableDataSortingHandler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject

private const val SYMBOL_NOT_FOUND = -1

abstract class BaseScreenerResultViewModel(
    private val interactor: ScreenerInteractor,
    private val columnsProvider: ResultColumnsProvider,
    private val columnTitleProvider: ResultColumnTitleProvider,
    private val rowDataProvider: ResultRowDataProvider,
    private val quoteDetailItemsMapper: FundQuoteDetailItemsMapper,
    private val streamingController: StreamingSubscriptionController,
    private val streamingStatusController: StreamingStatusController
) : ViewModel(), TableDataSortable {
    private val _viewState = MutableLiveData<ViewState>()
    private val compositeDisposable = CompositeDisposable()
    private var filtersSnapshot: List<ScreenerFilter> = emptyList()
    internal val viewState: LiveData<ViewState> = _viewState
    private val isSteamingAvailable
        get() = isSteamingSupportedByViewModel && streamingStatusController.isStreamingToggleEnabled
    protected open val isSteamingSupportedByViewModel = false
    private val symbolSubscriptionFields: Set<Level1Field> = StockData.fields()
    private val visibleSymbolsSubject = BehaviorSubject.create<Set<Symbol>>()
    private val sortingHandler = TableDataSortingHandler<ResultDataCell>()

    val sortState: LiveData<SortStateInfo>
        get() = _sortState
    private val _sortState = MutableLiveData<SortStateInfo>()

    internal fun refresh() {
        if (viewState.value !is ViewState.Success) {
            preRefreshSetup()
            compositeDisposable.add(
                interactor
                    .combinedScreenersResult()
                    .flatMap(this::mapToDataSet)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(
                        onSuccess = { data ->
                            _viewState.value = ViewState.Success(data.first.sort(_sortState.value))
                            streamingController.initSubscription({ symbolSubscriptionFields }, visibleSymbolsSubject) { symbol, _, level1Data ->
                                handleSymbolUpdate(symbol, level1Data, data.second)
                            }
                            if (isSteamingAvailable) {
                                streamingController.resumeSubscription()
                            }
                        },
                        onError = {
                            _viewState.value = ViewState.Error
                        }
                    )
            )
        }
    }

    private fun preRefreshSetup() {
        _viewState.value = ViewState.Loading
        compositeDisposable.clear()
        filtersSnapshot = interactor.getFiltersSnapshot()
        streamingController.cancelSubscription()
    }

    fun resumeStreaming() {
        if (isSteamingAvailable) {
            streamingController.resumeSubscription()
        }
    }

    fun pauseStreaming() {
        streamingController.pauseSubscription()
    }

    fun notifyVisibleItemsChanged(updatedSymbols: Set<Symbol>) {
        if (isSteamingAvailable) {
            visibleSymbolsSubject.onNext(updatedSymbols)
        }
    }

    override fun onCleared() {
        streamingController.cancelSubscription()
        compositeDisposable.clear()
        super.onCleared()
    }

    override fun sortTable(column: Int?) {
        val oldSortState = _sortState.value
        val newSortState = sortingHandler.resolveSortState(oldSortState, column)

        if (newSortState != oldSortState) {
            val dataSet = (viewState.value as? ViewState.Success)?.dataSet

            if (dataSet != null && !dataSet.isEmpty) {
                _viewState.postValue(ViewState.Success(dataSet.sort(newSortState)))
            }
            _sortState.value = newSortState
        }
    }

    private fun ResultDataSet.sort(sortStateInfo: SortStateInfo?): ResultDataSet {
        return if (!isEmpty && sortStateInfo != null) {
            val sortedData = sortingHandler.sortTableData(cellItems, rowHeaders, sortStateInfo)
            val updatedDataSet = copy(
                rowHeaders = sortedData.first,
                cellItems = sortedData.second
            )
            updatedDataSet
        } else {
            this
        }
    }

    private fun mapToDataSet(screenerResult: ScreenerResult): Single<Pair<ResultDataSet, List<ResultColumn>>> =
        columnsProvider
            .getColumns(filtersSnapshot)
            .map { listOfColumns ->
                val items = screenerResult.mobileQuotes

                val columnHeaders = listOfColumns.map { column: ResultColumn ->
                    Cell(
                        id = column.storageKey,
                        displayText = columnTitleProvider.getTitle(
                            column,
                            screenerResult.asMap.values
                        )
                    )
                }
                val rowHeaders = items.mapIndexed { index, item ->
                    val mfIconSection = quoteDetailItemsMapper.getFundIconSection(item)
                    ResultSymbolCell(
                        id = index.toString(),
                        displayText = item.symbol,
                        symbol = item.symbol,
                        instrumentType = item.typeCode,
                        description = "",
                        hasNTF = mfIconSection.hasNoTransactionsFee,
                        hasCF = item.commissionFree,
                        hasStar = mfIconSection.isAllStar || item.allStar
                    )
                }
                val cellItems = items.map {
                    rowDataProvider.getRowData(listOfColumns, screenerResult.asMap[it.symbol])
                }
                Pair(
                    ResultDataSet(columnHeaders, rowHeaders, cellItems),
                    listOfColumns
                )
            }

    private fun handleSymbolUpdate(symbol: String, data: Level1Data, columnOrder: List<ResultColumn>) {
        val dataSet = (_viewState.value as? ViewState.Success)?.dataSet ?: return
        val symbolRowIndex = dataSet.rowHeaders.map { it.displayText }.indexOf(symbol)

        if (symbolRowIndex != SYMBOL_NOT_FOUND) {
            val updatedSymbolRow = rowDataProvider.updateRowData(dataSet.cellItems[symbolRowIndex], data, columnOrder)
            updateViewState(symbolRowIndex, updatedSymbolRow, dataSet)
        }
    }

    private fun updateViewState(symbolRowIndex: Int, updatedRow: List<ResultDataCell>, dataSet: ResultDataSet) {
        val updatedCells = mutableListOf<List<ResultDataCell>>()
        dataSet.cellItems.forEachIndexed { index, row ->
            updatedCells.add(
                if (index == symbolRowIndex) updatedRow else row
            )
        }
        val sortedData = sortingHandler.sortTableData(updatedCells, dataSet.rowHeaders, sortState.value)

        _viewState.postValue(
            ViewState.Success(
                dataSet.copy(
                    rowHeaders = sortedData.first,
                    cellItems = sortedData.second
                )
            )
        )
    }

    internal sealed class ViewState {
        object Loading : ViewState()
        data class Success(
            val dataSet: ResultDataSet
        ) : ViewState()

        object Error : ViewState()
    }
}
