package com.etrade.mobilepro.uiwidgets.pdfview

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener
import kotlinx.parcelize.Parcelize
import java.io.File

class StateSavingPdfView(context: Context, attrs: AttributeSet?) : PDFView(context, attrs) {
    internal var trackedPage: Int? = null
    internal var pdfId: String? = null
    internal var pendingAction: (() -> Unit)? = null

    override fun onSaveInstanceState(): Parcelable? = PdfViewState(
        parentState = super.onSaveInstanceState(),
        currentPage = trackedPage,
        pdfId = pdfId
    )

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is PdfViewState) {
            trackedPage = state.currentPage
            pdfId = state.pdfId
            super.onRestoreInstanceState(state.parentState)
        } else {
            super.onRestoreInstanceState(state)
        }
        pendingAction?.invoke()
    }

    /**
     * Helper method for displaying PDF.
     * Loads and displays a PDF file. Current page is saved and restored on configuration change
     * For full list of supported features use Configurator methods (.fromAsset(..), .fromFile(..) etc)
     */
    fun loadFile(file: File, fitEachPage: Boolean = true) {
        this.fromFile(file)
            .fitEachPage(fitEachPage)
            .loadAndEnableStateSaving(this, file.absolutePath)
    }
}

/**
 * Loads a PDF file with page state saving/restoring.
 * Previously set onPageChangeListener in Configurator will be rewritten by this method call
 * @param view PDF View
 * @param pdfId String Id for the PDF, can be filename. Used to check if we reload the same PDF or the new one
 * @param onPageChangeListener Page change listener. When using this method must be set here if required
 */
fun PDFView.Configurator.loadAndEnableStateSaving(view: StateSavingPdfView, pdfId: String, onPageChangeListener: OnPageChangeListener? = null) {
    val pendingAction = {
        onPageChange { page, pageCount ->
            view.trackedPage = page
            onPageChangeListener?.onPageChanged(page, pageCount)
        }
        if (view.pdfId == pdfId) {
            view.trackedPage?.let { defaultPage(it) }
        } else {
            view.pdfId = pdfId
        }
        load()
    }
    view.pendingAction = pendingAction
    view.pendingAction?.invoke()
}

@Parcelize
private data class PdfViewState(
    val parentState: Parcelable?,
    val currentPage: Int?,
    val pdfId: String?
) : Parcelable
