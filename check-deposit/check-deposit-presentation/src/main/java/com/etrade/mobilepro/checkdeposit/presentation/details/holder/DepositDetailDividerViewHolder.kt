package com.etrade.mobilepro.checkdeposit.presentation.details.holder

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.checkdeposit.presentation.databinding.AdapterDepositDetailDividerBinding

class DepositDetailDividerViewHolder(binding: AdapterDepositDetailDividerBinding) :
    RecyclerView.ViewHolder(binding.root)
