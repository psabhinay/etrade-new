package com.etrade.mobilepro.portfolio

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.extension.getUsableScreenWidth
import com.etrade.mobilepro.navigation.getParentActivity
import com.etrade.mobilepro.portfolio.tableview.PortfolioRecordsTableRow
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

class PortfolioListViewAdapter(context: Context) : PortfolioNonTableViewAdapter(context) {

    override val layoutId: Int
        get() = R.layout.view_item_list_cell_layout

    override fun getItems(): List<List<PortfolioRecordsTableRow>> = data.rows.map { listOf(it) }

    override fun onCreateCellViewHolder(parent: ViewGroup, viewType: Int) =
        PortfolioListDataCellViewHolder(
            LayoutInflater.from(parent.context).inflate(viewType, parent, false).apply {
                layoutParams =
                    ViewGroup.LayoutParams(getParentActivity()!!.getUsableScreenWidth, WRAP_CONTENT)
            }
        )

    override fun onBindCellViewHolder(
        holder: AbstractViewHolder?,
        cellItemModel: Any?,
        columnPosition: Int,
        rowPosition: Int
    ) {
        val row = data.rows[rowPosition]
        (holder as PortfolioListDataCellViewHolder).bind(data.columns.toHeaders(row), row, columnMap)
    }
}
