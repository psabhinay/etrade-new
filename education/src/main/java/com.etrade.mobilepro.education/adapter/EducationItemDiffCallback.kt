package com.etrade.mobilepro.education.adapter

import androidx.recyclerview.widget.DiffUtil
import com.etrade.mobilepro.education.EducationItem

class EducationItemDiffCallback(
    private val oldItems: List<EducationItem>,
    private val newItems: List<EducationItem>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val old: EducationItem = oldItems[oldItemPosition]
        val new: EducationItem = newItems[newItemPosition]

        return old.description == new.description
    }

    override fun getOldListSize(): Int = oldItems.size

    override fun getNewListSize(): Int = newItems.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItems[oldItemPosition] == newItems[newItemPosition]
    }
}
