package com.etrade.mobilepro.menu.repo

import com.etrade.mobilepro.common.result.ETResult

interface MenuRepo {
    suspend fun prefetchDynamicMenuItems()
    suspend fun getDynamicMenuItems(): ETResult<List<DynamicMenuItem>>
}
