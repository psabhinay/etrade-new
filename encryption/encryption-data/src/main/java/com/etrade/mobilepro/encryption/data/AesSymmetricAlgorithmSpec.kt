package com.etrade.mobilepro.encryption.data

import android.security.keystore.KeyProperties
import com.etrade.mobilepro.encryption.api.SymmetricAlgorithmSpec
import javax.inject.Inject

class AesSymmetricAlgorithmSpec @Inject constructor() : SymmetricAlgorithmSpec {
    override val algorithm: String = KeyProperties.KEY_ALGORITHM_AES
    override val blockMode: String = KeyProperties.BLOCK_MODE_GCM
    override val encryptionPadding: String = KeyProperties.ENCRYPTION_PADDING_NONE
}
