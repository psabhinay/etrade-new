package com.etrade.mobilepro.completeview

import android.graphics.Rect
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView

/**
 * Adds offsets for all items except the first where and sets a vertical separator for the first element of the list.
 *
 * @param secondItemOffsetRes vertical offset to separate first item of the list
 * @param verticalOffsetRes vertical offset for all items in the list except first
 * @param horizontalOffsetRes horizontal offset for all items in the list except first
 */
fun RecyclerView.decorateCompleteViewItems(
    @DimenRes secondItemOffsetRes: Int,
    @DimenRes verticalOffsetRes: Int,
    @DimenRes horizontalOffsetRes: Int,
    @DimenRes lastItemOffsetRes: Int
) {
    addItemDecoration(object : RecyclerView.ItemDecoration() {

        @Suppress("LongMethod")
        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val horizontalItemOffset = resources.getDimension(horizontalOffsetRes).toInt()
            val verticalItemOffset = resources.getDimension(verticalOffsetRes).toInt()
            val secondItemOffset = resources.getDimension(secondItemOffsetRes).toInt()
            val lastItemOffset = resources.getDimension(lastItemOffsetRes).toInt()

            val isFirstItem = parent.getChildAdapterPosition(view) == 0
            val isSecondItem = parent.getChildAdapterPosition(view) == 1
            val isLastItem = parent.adapter?.let {
                parent.getChildAdapterPosition(view) == (it.itemCount - 1)
            } ?: false
            with(outRect) {
                if (isFirstItem || isSecondItem) {
                    top = 0
                    left = 0
                    right = 0
                    bottom = if (isFirstItem) {
                        0
                    } else {
                        secondItemOffset
                    }
                } else {
                    top = verticalItemOffset
                    left = horizontalItemOffset
                    right = horizontalItemOffset
                    bottom = if (isLastItem) {
                        lastItemOffset
                    } else {
                        verticalItemOffset
                    }
                }
            }
        }
    })
}
