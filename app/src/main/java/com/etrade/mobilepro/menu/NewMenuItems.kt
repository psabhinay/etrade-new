package com.etrade.mobilepro.menu

object NewMenuItems {
    const val ACCOUNT_TRANSFER_KEY = "ACCOUNT_TRANSFER_KEY"

    const val NEW_IN_VERSION_9_5 = "9.5"

    fun getNewMenuItems(): List<NewMenuItem> = listOf(
        NewMenuItem(ACCOUNT_TRANSFER_KEY, NEW_IN_VERSION_9_5),
        NewMenuItem(MenuDeepLink.TRANSFER.appUrl, NEW_IN_VERSION_9_5)
    )
}
