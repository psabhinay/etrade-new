package com.etrade.mobilepro.transactions.api

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class CachedTransactions(
    @PrimaryKey
    var queryValue: String = "",
    var transactions: RealmList<CachedTransactionDetail> = RealmList(),
    var savedAt: Long = 0
) : RealmObject()
