package com.etrade.mobilepro.trade.presentation

import android.content.res.Resources
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import com.etrade.mobilepro.backends.api.EXTENDED_HOURS_DISCLOSURE_ERROR_CODE
import com.etrade.mobilepro.backends.api.LEVERAGED_ETF_AGREEMENT_ERROR_CODE
import com.etrade.mobilepro.backends.api.MRB_FLAGGED_SYMBOL_ERROR_CODE
import com.etrade.mobilepro.backends.api.OTC_DISCLOSURE_ERROR_CODE
import com.etrade.mobilepro.balance.viewmodel.AccountBalanceViewModel
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.common.viewmodel.SnackbarViewModel
import com.etrade.mobilepro.companyoverview.CompanyOverviewProvider
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.dynamic.form.api.OnActionClickListener
import com.etrade.mobilepro.dynamic.form.api.inputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.api.requireInput
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.order.details.router.OrderConfirmationAction
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.sharedviewmodel.OrderRefreshDelegate
import com.etrade.mobilepro.orders.sharedviewmodel.OrderSharedViewModel
import com.etrade.mobilepro.quote.search.history.QuoteSearchHistoryViewModel
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.tracking.SCREEN_TITLE_TRADE
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.EVENT_TRADE_SAVED
import com.etrade.mobilepro.tracking.params.TradeEventAnalyticsParams
import com.etrade.mobilepro.trade.accountupgrade.util.getApprovalLevelUpgradeUrl
import com.etrade.mobilepro.trade.api.SaveOrderResponse
import com.etrade.mobilepro.trade.approvallevel.router.ApprovalLevelRouter
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormInputIdMap
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.getInputId
import com.etrade.mobilepro.trade.form.api.getSecurityType
import com.etrade.mobilepro.trade.form.api.input.MutualFundTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithActionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithPriceTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithSymbolTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithTradeLegsTradeFormInputId
import com.etrade.mobilepro.trade.mutualfund.presentation.MutualFundTradeViewModel
import com.etrade.mobilepro.trade.optionlegselect.OptionTradeActionItemProvider
import com.etrade.mobilepro.trade.optionlegselect.TradeActionItemProvider
import com.etrade.mobilepro.trade.optionlegselect.TradeLegBidAskViewModel
import com.etrade.mobilepro.trade.presentation.form.TradeFormAction
import com.etrade.mobilepro.trade.presentation.form.component.TradeLegsInput
import com.etrade.mobilepro.trade.presentation.form.delegate.InboxMessagesDelegate
import com.etrade.mobilepro.trade.presentation.form.util.displayValue
import com.etrade.mobilepro.trade.presentation.form.util.getTradeFormInputId
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormParameter
import com.etrade.mobilepro.trade.presentation.usecase.CreateTradeFormUseCase
import com.etrade.mobilepro.trade.presentation.usecase.PreviewOrderUseCase
import com.etrade.mobilepro.trade.presentation.usecase.PreviewOrderUseCaseParams
import com.etrade.mobilepro.trade.presentation.usecase.SaveOrderUseCase
import com.etrade.mobilepro.trade.presentation.usecase.TradeFormParameter
import com.etrade.mobilepro.trade.presentation.util.TradeExceptionViewModelDelegate
import com.etrade.mobilepro.trade.presentation.util.TradePreviewViewModelDelegate
import com.etrade.mobilepro.trade.router.OptionTradeRouter
import com.etrade.mobilepro.trade.router.QuoteRouter
import com.etrade.mobilepro.trade.router.TradeMutualFundRouter
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.tradingdefaults.preferences.DEFAULT_STOCKS_PRICE_TYPE
import com.etrade.mobilepro.tradingdefaults.preferences.DEFAULT_STOCKS_TERM
import com.etrade.mobilepro.tradingdefaults.preferences.DEFAULT_WALKTHROUGH_COMPLETED_FLAG
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.util.filterNotNullValues
import com.etrade.mobilepro.util.invoke
import com.etrade.mobilepro.viewmodel.BlockingLoadingIndicatorViewModel
import com.etrade.mobilepro.walkthrough.api.IdleState
import kotlinx.coroutines.launch
import java.math.BigDecimal
import javax.inject.Inject
import javax.inject.Provider

typealias EditOrderEvent = (NavController, FragmentActivity) -> Unit

@Suppress("LargeClass", "LongParameterList")
class TradeViewModel @Inject constructor(
    createTradeFormUseCase: CreateTradeFormUseCase,
    resources: Resources,
    inboxMessagesDelegate: InboxMessagesDelegate,
    tradeRouter: TradeRouter,
    @Web baseUrl: String,
    private val cachedDaoProvider: CachedDaoProvider,
    private val daoProvider: AccountDaoProvider,
    private val previewOrderUseCase: PreviewOrderUseCase,
    private val saveOrderUseCase: SaveOrderUseCase,
    private val approvalLevelRouter: ApprovalLevelRouter,
    private val mutualFundRouter: TradeMutualFundRouter,
    private val optionTradeRouter: OptionTradeRouter,
    private val orderRefreshDelegate: OrderRefreshDelegate,
    @OptionTradeActionItemProvider private val optionTradeActionItemProvider: TradeActionItemProvider,
    private val snackbarViewModel: SnackbarViewModel,
    private val tracker: Tracker,
    private val user: User,
    private val companyOverviewProvider: CompanyOverviewProvider,
    private val quoteRouter: QuoteRouter,
    private val tradeFormInputIdMap: TradeFormInputIdMap,
    private val tradingDefaultsPreferences: TradingDefaultsPreferences,
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>
) : BaseTradeFormViewModel(createTradeFormUseCase, resources, inboxMessagesDelegate, tradeRouter, baseUrl),
    ActionSheetViewModel,
    BlockingLoadingIndicatorViewModel,
    ScreenViewModel {

    var orderConfirmationAction: OrderConfirmationAction = OrderConfirmationAction.VIEW_PORTFOLIO

    internal lateinit var balanceViewModel: AccountBalanceViewModel
    internal lateinit var mutualFundTradeViewModel: MutualFundTradeViewModel
    internal lateinit var quoteSearchHistoryViewModel: QuoteSearchHistoryViewModel
    internal lateinit var tradeLegBidAskViewModel: TradeLegBidAskViewModel

    init {
        cachedDaoProvider.open()
    }

    override fun onCleared() {
        super.onCleared()
        cachedDaoProvider.close()
    }

    override val disableIfNoChange get() = tradeOrderType.isEdit()

    override val actions: LiveData<List<ActionItem>>
        get() = _actions

    private var symbol: SearchResultItem.Symbol? = null

    override fun selectAction(item: ActionItem) {
        _actions.value = emptyList()
        navigateToOptionLegSelect(true, optionTradeActionItemProvider.getTransactionType(item.id))
    }

    override fun cancelActions() {
        _actions.value = emptyList()
    }

    override val blockingLoadingIndicator: LiveData<Boolean>
        get() = _actionLoadingIndicator
    override val screenName: String? = SCREEN_TITLE_TRADE
    private val _actions: MutableLiveData<List<ActionItem>> = MutableLiveData()

    val editOrderSignal: LiveData<ConsumableLiveEvent<EditOrderEvent>>
        get() = _editOrderSignal
    val tradeOrderFlowEndSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _tradeOrderFlowEndSignal
    val optionsApprovalUpgradeUrl: LiveData<ConsumableLiveEvent<String>>
        get() = _optionsApprovalUpgradeUrl

    internal lateinit var orderSharedViewModel: OrderSharedViewModel

    private val _optionsApprovalUpgradeUrl: MutableLiveData<ConsumableLiveEvent<String>> = MutableLiveData()
    private val _actionLoadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)
    private val _editOrderSignal: MutableLiveData<ConsumableLiveEvent<EditOrderEvent>> = MutableLiveData()
    private val _tradeOrderFlowEndSignal: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()

    private val serverMessagesNavMap: Map<String, () -> Unit> = mapOf(
        EXTENDED_HOURS_DISCLOSURE_ERROR_CODE.toString() to {
            navigateTo {
                tradeRouter.openExtendedHoursDisclosure(it, resources.getString(R.string.trade_disclaimer_title))
            }
        },
        OTC_DISCLOSURE_ERROR_CODE.toString() to {
            navigateTo {
                tradeRouter.openOtcDisclosure(it, resources.getString(R.string.trade_otc_agreement_webview_title))
            }
        },
        LEVERAGED_ETF_AGREEMENT_ERROR_CODE.toString() to {
            navigateTo {
                tradeRouter.openLiEtfDisclosure(it, resources.getString(R.string.trade_leveraged_etf_webview_title))
            }
        },
        MRB_FLAGGED_SYMBOL_ERROR_CODE.toString() to {
            navigateTo {
                tradeRouter.openMRBDisclosure(it, resources.getString(R.string.trade_mrb_agreement_webview_title))
            }
        }
    )

    private val tradeExceptionViewModelDelegate = TradeExceptionViewModelDelegate(resources, baseUrl, mutableDialog, serverMessagesNavMap) { accountId ->
        viewModelScope.launch {
            val account = daoProvider.getAccountDao().getAccountWithAccountId(accountId)
            val isIra = account?.isIra ?: false
            val url = "$baseUrl${getApprovalLevelUpgradeUrl(isIra)}"
            _optionsApprovalUpgradeUrl.value = url.consumable()
        }
    }

    private val tradePreviewViewModelDelegate =
        TradePreviewViewModelDelegate(
            resources, baseUrl, mutableDialog,
            reSubmitMfOrder = { previewOrder() },
            openPreview = {
                navigateTo { navController ->
                    tradeRouter.openPreview(navController, it)
                }
            }
        )

    private val orderRefreshMessage = resources.getString(R.string.trade_confirmation_order_saved)

    @Suppress("ComplexMethod")
    override fun handleAction(action: TradeFormAction) {
        when (action) {
            is TradeFormAction.ApprovalLevel -> navigateTo { approvalLevelRouter.navigateToApprovalLevelFragment(it, action.accountMode) }
            is TradeFormAction.ConditionalOrder -> onConditionalOrderAction(action)
            is TradeFormAction.ErrorMessage -> errorMessage(action.message)
            is TradeFormAction.MutualFundExchangeBuyEntry -> navigateTo { mutualFundRouter.openMutualFundExchangeBuy(it, action.accountId, action.symbol) }
            is TradeFormAction.MutualFundExchangeSellEntry -> navigateTo { mutualFundRouter.openMutualFundExchangeSell(it, action.accountId) }
            is TradeFormAction.MutualFundDisclosure -> navigateTo { mutualFundRouter.openDisclosures(it, action.fundsData) }
            is TradeFormAction.MutualFundProspectus -> navigateTo { mutualFundRouter.openProspectus(it, action.symbol) }
            is TradeFormAction.MutualFundSellEntry -> navigateTo { mutualFundRouter.openMutualFundSell(it, action.accountId) }
            is TradeFormAction.OptionLegSelect -> openOptionLegSelect(action.newSearch, action.symbol)
            TradeFormAction.SaveOrder -> saveOrder()
            is TradeFormAction.ShowFundsPerformance -> navigateTo { mutualFundRouter.openFundsPerformancePage(it, action.symbol) }
            TradeFormAction.SubmitOrder -> previewOrder()
            else -> throw UnsupportedOperationException("Action is not supported: $action")
        }
    }

    override fun createParameter(
        initialValues: InputValuesSnapshot,
        defaultValues: InputValuesSnapshot,
        disableIfNoChange: Boolean,
        onActionClickListener: OnActionClickListener
    ): TradeFormParameter {
        return CreateTradeFormParameter(
            disclosureViewModel,
            onActionClickListener,
            quoteWidgetViewModel,
            defaultValues,
            tradeOrderType,
            walkthroughViewModel,
            advancedOrderBidAskViewModel,
            advancedQuoteWidgetViewModel,
            balanceViewModel,
            quoteSearchHistoryViewModel,
            tradeLegBidAskViewModel,
            mutualFundTradeViewModel,
            disableIfNoChange,
            tradeFormInputIdMap.getSecurityType(initialValues)
        )
    }

    private fun onConditionalOrderAction(action: TradeFormAction.ConditionalOrder) {
        navigateTo {
            tradeRouter.editConditionalOrder(
                navController = it,
                orderInputId = action.inputId,
                orderNumber = action.orderNumber,
                accountId = action.accountId,
                snapshot = action.snapshot
            )
        }
    }

    private fun openOptionLegSelect(newSearch: Boolean, searchResultSymbol: SearchResultItem.Symbol?) {
        symbol = searchResultSymbol
        if (searchResultSymbol?.type?.isOption == true) {
            _actions.value = optionTradeActionItemProvider.getActions()
        } else {
            navigateToOptionLegSelect(newSearch)
        }
    }

    private fun navigateToOptionLegSelect(newSearch: Boolean, transactionType: TransactionType? = null) {
        withForm { form ->
            navigateTo {
                val snapshot = form.inputValuesSnapshot.run {
                    if (newSearch) withoutTradeLegs else this
                }
                optionTradeRouter.openOptionLegSelect(it, snapshot, symbol, transactionType)
            }
        }
    }

    override fun addDefaultValuesIfNeeded(form: Form, initialValues: InputValuesSnapshot): InputValuesSnapshot {
        return when (val formInput = form.getTradeFormInputId(tradeFormInputIdMap)) {
            is StockTradeFormInputId -> { getStockTradingDefaults(initialValues, formInput) }
            is OptionTradeFormInputId -> {
                if (initialValues.containsKey(formInput.orderId)) {
                    initialValues
                } else {
                    tradeFormParametersBuilderProvider.get().apply {
                        quantity = tradingDefaultsPreferences.optionsContracts.toBigDecimal()
                        term = initialValues.getElseDefault(formInput.term, tradingDefaultsPreferences.optionsTerm) {
                            initialValues.useDefaultTerm(formInput.priceType, tradingDefaultsPreferences.optionsPriceType)
                        }
                    }.create(SecurityType.OPTION) + initialValues.filterNotNullValues()
                }
            }
            else -> initialValues
        }
    }

    private fun getStockTradingDefaults(initialValues: InputValuesSnapshot, formInput: StockTradeFormInputId): InputValuesSnapshot {
        val defaultQuantity: BigDecimal
        val defaultPriceType: PriceType?
        val defaultTerm: OrderTerm?
        val defaultWalkthroughValue: String?

        if (walkthroughViewModel.state.value is IdleState) {
            defaultQuantity = tradingDefaultsPreferences.stocksQuantity.toBigDecimalOrNull() ?: BigDecimal.ONE
            defaultPriceType = tradingDefaultsPreferences.stocksPriceType
            defaultTerm = initialValues.getElseDefault(formInput.term, tradingDefaultsPreferences.stocksTerm) {
                initialValues.useDefaultTerm(formInput.priceType, tradingDefaultsPreferences.stocksPriceType)
            }
            defaultWalkthroughValue = DEFAULT_WALKTHROUGH_COMPLETED_FLAG
        } else {
            defaultQuantity = BigDecimal.ONE
            defaultPriceType = DEFAULT_STOCKS_PRICE_TYPE
            defaultTerm = DEFAULT_STOCKS_TERM
            defaultWalkthroughValue = null
        }

        return tradeFormParametersBuilderProvider.get().apply {
            quantity = defaultQuantity
            priceType = defaultPriceType
            term = defaultTerm
            walkthroughFormCompleted = defaultWalkthroughValue
        }.create(SecurityType.STOCK) + initialValues.filterNotNullValues()
    }

    private fun InputValuesSnapshot.useDefaultTerm(priceTypeId: String, defaultPriceType: PriceType?): Boolean {
        val priceType = this[priceTypeId]
        return priceType == null || priceType == defaultPriceType
    }

    private inline fun <reified T> InputValuesSnapshot.getElseDefault(id: String, default: T, useDefault: () -> Boolean): T? =
        if (useDefault()) {
            default
        } else {
            getOrDefault(id, default) as? T
        }

    private fun previewOrder() {
        withForm { form ->
            previewOrderUseCase(PreviewOrderUseCaseParams(form.inputValuesSnapshot, tradeOrderType.isOpen()))
                .onSuccess {
                    tradePreviewViewModelDelegate.handlePreview(it, orderConfirmationAction)
                }
                .onFailure {
                    handleTradeOrderException(it) {
                        previewOrder()
                    }
                }
        }
    }

    private fun saveOrder() {
        withForm { form ->
            saveOrderUseCase(form)
                .onSuccess { response ->
                    getTradeEventAnalyticsParams(form, EVENT_TRADE_SAVED)?.let {
                        tracker.event(it)
                    }
                    handleSaveOrder(response)
                }
                .onFailure {
                    handleTradeOrderException(it) {
                        saveOrder()
                    }
                }
        }
    }

    private fun handleSaveOrder(saveOrderResult: SaveOrderResponse) {
        resetFormValues()
        sendEditOrderEvent { navController, activity ->
            if (tradeOrderType == TradeOrderType.NEW) {
                if (navController.graph.id == R.id.quoteGraph) {
                    quoteRouter.popQuotePageWithMessage(navController, message = orderRefreshMessage)
                } else {
                    _tradeOrderFlowEndSignal.value = Unit.consumable()
                    snackbarViewModel.postAppMessage(orderRefreshMessage)
                }
            }
            orderRefreshDelegate.refreshOrderPages(
                orderSharedViewModel,
                navController,
                activity,
                saveOrderResult.accountId,
                saveOrderResult.orderId,
                null,
                tradeOrderType
            )
        }
    }

    private fun withForm(block: suspend (Form) -> Unit) {
        val form = form.value
        if (form == null) {
            errorMessage(resources.getString(R.string.error_message_general))
            return
        }

        viewModelScope.launch {
            _actionLoadingIndicator.value = true

            block(form)

            _actionLoadingIndicator.value = false
        }
    }

    private fun handleTradeOrderException(exception: Throwable, retryAction: () -> Unit) {
        val formInput = form.value?.getTradeFormInputId(tradeFormInputIdMap)
        val accountId = form.value?.inputValuesSnapshot?.get(formInput?.accountId) as? String
        if (!tradeExceptionViewModelDelegate.handleTradeOrderException(exception, accountId)) {
            handleException(exception, retryAction)
        }
    }

    private fun sendEditOrderEvent(editOrderEvent: EditOrderEvent) {
        _editOrderSignal.value = ConsumableLiveEvent(object : EditOrderEvent {
            override fun invoke(navController: NavController, activity: FragmentActivity) {
                editOrderEvent(navController, activity)
            }
        })
    }

    private val InputValuesSnapshot.withoutTradeLegs: InputValuesSnapshot
        get() {
            val inputId = tradeFormInputIdMap.getInputId(this)
            return if (inputId is OptionTradeFormInputId) {
                toMutableMap().apply {
                    remove(inputId.lookupSymbol)
                    remove(inputId.tradeLegs)
                }.toMap()
            } else {
                this
            }
        }

    private suspend fun getTradeEventAnalyticsParams(form: Form, event: String): TradeEventAnalyticsParams? {
        val inputId = form.getTradeFormInputId(tradeFormInputIdMap)
        val symbol: String = form.displayValue(inputId, WithSymbolTradeFormInputId::symbol) ?: return null

        return TradeEventAnalyticsParams(
            symbol = symbol,
            securityType = requireNotNull(form.displayValue(inputId, TradeFormInputId::securityType)),
            priceType = form.displayValue(inputId, WithPriceTradeFormInputId::priceType),
            sector = companyOverviewProvider.companySector(symbol),
            customerId = requireNotNull(user.session?.aatId),
            name = event,
            orderType = getOrderType(inputId, form),
            quantity = getQuantity(inputId, form)
        )
    }

    private fun getOrderType(inputId: TradeFormInputId?, form: Form): String? {
        return when (inputId) {
            is WithActionTradeFormInputId -> form.displayValue(inputId, StockTradeFormInputId::action)
            is WithTradeLegsTradeFormInputId -> {
                form.requireInput<TradeLegsInput>(inputId.tradeLegs).value.value?.let {
                    it.joinToString(",") { leg -> resources.getString(leg.transactionType.getLabel()) }
                }
            }
            else -> null
        }
    }

    private fun getQuantity(inputId: TradeFormInputId?, form: Form): String? {
        return when (inputId) {
            is StockTradeFormInputId -> form.displayValue(inputId, StockTradeFormInputId::quantity)
            is MutualFundTradeFormInputId -> form.displayValue(inputId, MutualFundTradeFormInputId::amount)
            is WithTradeLegsTradeFormInputId -> {
                form.requireInput<TradeLegsInput>(inputId.tradeLegs).value.value?.let {
                    it.fold(0) { acc, leg -> acc + leg.quantity }
                }?.toString()
            }
            else -> null
        }
    }
}
