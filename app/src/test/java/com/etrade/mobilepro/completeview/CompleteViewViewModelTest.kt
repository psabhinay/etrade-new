package com.etrade.mobilepro.completeview

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.dynamicui.api.DynamicScreenRepo
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.inboxmessages.InboxMessagesRepository
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.msaccounts.api.MsAccountsRepo
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.anyOrNull
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.spy
import com.nhaarman.mockitokotlin2.verify
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.reactivex.Observable
import io.realm.Realm
import io.realm.RealmObjectSchema
import io.realm.RealmSchema
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

private const val DEFAULT_ACCOUNT_ID = "1234567890"

@FlowPreview
@ExperimentalCoroutinesApi
class CompleteViewViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    private val mockResources: Resources = mock()

    private val mockRealm = mockk<Realm>()

    private val mockMsAccountsRepo = mockk<MsAccountsRepo>()

    private val mockStreamingStatusController: StreamingStatusController = mock()

    private val inboxMessagesRepo = mockk<InboxMessagesRepository>()

    @Before
    fun mockAllRealmInteractions() {
        val mockRealmObjectSchema = mockk<RealmObjectSchema>()
        val mockRealmSchema = mockk<RealmSchema>()
        mockkStatic(Realm::class)
        every { Realm.getDefaultInstance() } returns mockRealm
        every { mockRealm.schema } returns mockRealmSchema
        every { mockRealmSchema.get(any()) } returns mockRealmObjectSchema
        every { mockRealmObjectSchema.primaryKey } returns "primaryKey"
        every { mockMsAccountsRepo.getMsCompleteView() } returns flowOf(Resource.Success(emptyList()))
    }

    private val mockLoginPreferences: LoginPreferencesRepo = mock {
        on { isLoginForSameUser } doReturn true
    }

    private val darkModeChecker: DarkModeChecker = mock {
        on { uiMode } doReturn 1
    }

    private fun mockDynamicScreenService(views: List<GenericLayout> = listOf()): DynamicScreenRepo {
        val viewMocks: Observable<Resource<List<GenericLayout>>> = Observable.just(Resource.Success(views))
        return mock {
            on { getScreen(any(), any(), anyOrNull()) } doReturn viewMocks
        }
    }

    private val mockUserViewModel: UserViewModel = mock {
        on { defaultAccountId } doReturn DEFAULT_ACCOUNT_ID
    }

    private fun buildDefaultCompleteViewViewModel(
        mockDynamicService: DynamicScreenRepo,
        mockLoginPreferences: LoginPreferencesRepo = this.mockLoginPreferences,
        msAccountsRepo: MsAccountsRepo = mockMsAccountsRepo
    ) =
        CompleteViewViewModel(
            mockResources,
            mockDynamicService,
            mockLoginPreferences,
            darkModeChecker,
            mockStreamingStatusController,
            mockUserViewModel,
            msAccountsRepo,
            inboxMessagesRepo
        )

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `fetches complete view upon creation`() {
        val mockDynamicService = mockDynamicScreenService()

        buildDefaultCompleteViewViewModel(mockDynamicService, mockLoginPreferences)

        verify(mockDynamicService).getScreen(
            eq(ServicePath.CompleteView(DEFAULT_ACCOUNT_ID)),
            any(),
            anyOrNull()
        )
    }

    @Test
    fun `returns view state with generic views on success`() {
        val mockObserver = mock<Observer<CompleteViewViewModel.ViewState>>()
        val viewList = listOf<GenericLayout>()
        val mockDynamicService = mockDynamicScreenService(viewList)
        val vm = spy(buildDefaultCompleteViewViewModel(mockDynamicService, mockLoginPreferences))

        vm.viewState.observeForever(mockObserver)

        verify(mockObserver).onChanged(eq(CompleteViewViewModel.ViewState.Success(viewList, darkModeChecker.uiMode)))
    }

    @Test
    fun `returns error view state when dynamic screen rpc call is not successful`() {
        val mockObserver = mock<Observer<CompleteViewViewModel.ViewState>>()
        val mockDynamicService = mock<DynamicScreenRepo> {
            on { getScreen(any(), any(), anyOrNull()) } doReturn Observable.error(RuntimeException("Bad things"))
        }

        val vm = buildDefaultCompleteViewViewModel(mockDynamicService, mockLoginPreferences)
        vm.viewState.observeForever(mockObserver)

        verify(mockObserver).onChanged(eq(CompleteViewViewModel.ViewState.Error))
    }

    @Test
    fun `returns success view state when msCompleteView rpc call is not successful`() {
        val mockObserver = mock<Observer<CompleteViewViewModel.ViewState>>()
        val viewList = listOf<GenericLayout>()
        val mockDynamicService = mockDynamicScreenService(viewList)
        val msAccountsRepo = mock<MsAccountsRepo> {
            on { getMsCompleteView() } doReturn flowOf(Resource.Failed())
        }

        val vm = buildDefaultCompleteViewViewModel(
            mockDynamicService,
            mockLoginPreferences,
            msAccountsRepo
        )
        vm.viewState.observeForever(mockObserver)

        verify(mockObserver).onChanged(eq(CompleteViewViewModel.ViewState.Success(emptyList(), darkModeChecker.uiMode)))
    }

    @Test
    fun `returns non-cached view state when login was from a different user`() {
        verifyViewStateBasedOnLoginInfo(isLoginForSameUser = false)
    }

    @Test
    fun `returns cached view state when login was from the same user`() {
        verifyViewStateBasedOnLoginInfo(isLoginForSameUser = true)
    }

    private fun verifyViewStateBasedOnLoginInfo(isLoginForSameUser: Boolean) {
        val mockObserver = mock<Observer<CompleteViewViewModel.ViewState>>()
        val nonCachedList = listOf<GenericLayout>()
        val cachedList = listOf<GenericLayout>(mock())
        val targetList = if (isLoginForSameUser) { cachedList } else { nonCachedList }
        val nonCachedViewMocks: Observable<Resource<List<GenericLayout>>> = Observable.just(Resource.Success(nonCachedList))
        val cachedViewMocks: Observable<Resource<List<GenericLayout>>> = Observable.just(Resource.Success(cachedList))
        val mockLoginPreferences: LoginPreferencesRepo = mock {
            on { this.isLoginForSameUser } doReturn isLoginForSameUser
        }
        val mockDynamicService = mock<DynamicScreenRepo> {
            on { getScreen(any(), eq(false), anyOrNull()) } doReturn nonCachedViewMocks
            on { getScreen(any(), eq(true), anyOrNull()) } doReturn cachedViewMocks
        }
        val vm = spy(buildDefaultCompleteViewViewModel(mockDynamicService, mockLoginPreferences))
        vm.viewState.observeForever(mockObserver)

        verify(mockObserver).onChanged(eq(CompleteViewViewModel.ViewState.Success(targetList, darkModeChecker.uiMode)))
    }
}
