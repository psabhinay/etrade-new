package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.form.api.value.AdvancedOrderTypeValueId
import com.etrade.mobilepro.util.MutualConverter

object DefaultAdvancedOrderTypeValueId : AdvancedOrderTypeValueId {
    override val contingent: String = "advancedOrderTypeContingent"
    override val stopLoss: String = "advancedOrderTypeStopLoss"
    override val converter: MutualConverter<String, AdvancedTradeType> by lazy { createConverter() }
}
