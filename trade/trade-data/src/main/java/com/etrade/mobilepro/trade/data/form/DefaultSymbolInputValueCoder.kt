package com.etrade.mobilepro.trade.data.form

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.util.ValueCoder

private const val DELIMITER = ':'

object DefaultSymbolInputValueCoder : ValueCoder<WithSymbolInfo> {

    override fun encode(value: WithSymbolInfo): Any {
        return "${value.symbol}$DELIMITER${value.instrumentType.typeCode}"
    }

    override fun decode(encoded: Any): WithSymbolInfo? {
        if (encoded !is String || encoded.isBlank()) return null
        val parts = encoded.split(DELIMITER)
        return when {
            parts.isEmpty() -> null
            parts.size == 1 -> Symbol(parts[0], InstrumentType.UNKNOWN)
            else -> Symbol(parts[0], InstrumentType.from(parts[1]))
        }
    }
}
