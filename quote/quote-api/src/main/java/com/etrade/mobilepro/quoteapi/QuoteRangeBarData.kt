package com.etrade.mobilepro.quoteapi

data class QuoteRangeBarData(
    val week52High: String,
    val week52Low: String,
    val dayHigh: String,
    val dayLow: String,
    val week52HighDate: String,
    val week52LowDate: String
)
