package com.etrade.mobilepro.screener.data.json.stock

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_EQUALS
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.stock.EarningsDividendsFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val EPS_GROWTH_ANNUAL_FOR_CURRENT_FISCAL_YEAR_FIELD = "EPSGrowthAnnual_Current"
private const val EPS_GROWTH_ANNUAL_FOR_NEXT_FISCAL_YEAR_FIELD = "EPSGrowthAnnual_Next"

private const val REVENUE_GROWTH_ANNUAL_FOR_CURRENT_FISCAL_YEAR_FIELD = "RevenueGrowthAnnual_Current"
private const val REVENUE_GROWTH_ANNUAL_FOR_NEXT_FISCAL_YEAR_FIELD = "RevenueGrowthAnnual_Next"

private const val EPS_GROWTH_QUARTERLY_FIELD = "EPSGrowthQuarterly"
private const val REVENUE_GROWTH_QUARTERLY_FIELD = "RevenueGrowthQuarterly"

private const val DIVIDEND_YIELD_VALUES_FIELD = "DividendYield"
private const val DIVIDEND_YIELD_BELOW_INDUSTRY_FIELD = "DividendYield_BelowIndustryAvg"
private const val DIVIDEND_YIELD_ABOVE_INDUSTRY_FIELD = "DividendYield_AboveIndustryAvg"

class EarningsDividendsFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val earningsDividendsFilter = screenerFilter as EarningsDividendsFilter
        val arguments = mutableListOf<Argument>()

        createEpsGrowthAnnualArguments(arguments, earningsDividendsFilter)
        createRevenueGrowthAnnualArguments(arguments, earningsDividendsFilter)
        createGrowthQuarterlyArguments(earningsDividendsFilter, arguments)
        createDividendYieldArguments(earningsDividendsFilter, arguments)

        return PartialScreenerParams(arguments)
    }

    private fun createDividendYieldArguments(
        earningsDividendsFilter: EarningsDividendsFilter,
        arguments: MutableList<Argument>
    ) {
        if (earningsDividendsFilter.isDividendYieldValuesSelected()) {
            arguments.add(
                Argument(
                    getDividendYieldClauses(earningsDividendsFilter),
                    DIVIDEND_YIELD_VALUES_FIELD
                )
            )
        }

        if (earningsDividendsFilter.dividendYieldBelowIndustryAvg) {
            arguments.add(
                Argument(
                    listOf(dividendYieldClause),
                    DIVIDEND_YIELD_BELOW_INDUSTRY_FIELD
                )
            )
        }

        if (earningsDividendsFilter.dividendYieldAboveIndustryAvg) {
            arguments.add(
                Argument(
                    listOf(dividendYieldClause),
                    DIVIDEND_YIELD_ABOVE_INDUSTRY_FIELD
                )
            )
        }
    }

    private fun createGrowthQuarterlyArguments(
        earningsDividendsFilter: EarningsDividendsFilter,
        arguments: MutableList<Argument>
    ) {
        if (earningsDividendsFilter.isEpsGrowthQuarterlyValuesSelected()) {
            arguments.add(
                Argument(
                    getEpsGrowthQuarterlyClauses(earningsDividendsFilter),
                    EPS_GROWTH_QUARTERLY_FIELD
                )
            )
        }

        if (earningsDividendsFilter.isRevenueGrowthQuarterlyValuesSelected()) {
            arguments.add(
                Argument(
                    getRevenueGrowthQuarterlyClauses(earningsDividendsFilter),
                    REVENUE_GROWTH_QUARTERLY_FIELD
                )
            )
        }
    }

    private fun createEpsGrowthAnnualArguments(arguments: MutableList<Argument>, earningsDividendsFilter: EarningsDividendsFilter) {
        if (earningsDividendsFilter.isEpsGrowthAnnualValuesSelected() && earningsDividendsFilter.epsGrowthAnnualForCurrentFiscalYear) {
            arguments.add(
                Argument(
                    getEpsGrowthAnnualClauses(earningsDividendsFilter),
                    EPS_GROWTH_ANNUAL_FOR_CURRENT_FISCAL_YEAR_FIELD
                )
            )
        }

        if (earningsDividendsFilter.isEpsGrowthAnnualValuesSelected() && earningsDividendsFilter.epsGrowthAnnualForNextFiscalYear) {
            arguments.add(
                Argument(
                    getEpsGrowthAnnualClauses(earningsDividendsFilter),
                    EPS_GROWTH_ANNUAL_FOR_NEXT_FISCAL_YEAR_FIELD
                )
            )
        }
    }

    private fun createRevenueGrowthAnnualArguments(arguments: MutableList<Argument>, earningsDividendsFilter: EarningsDividendsFilter) {
        if (earningsDividendsFilter.isRevenueGrowthAnnualValuesSelected() && earningsDividendsFilter.revenueGrowthAnnualForCurrentFiscalYear) {
            arguments.add(
                Argument(
                    getRevenueGrowthAnnualClauses(earningsDividendsFilter),
                    REVENUE_GROWTH_ANNUAL_FOR_CURRENT_FISCAL_YEAR_FIELD
                )
            )
        }

        if (earningsDividendsFilter.isRevenueGrowthAnnualValuesSelected() && earningsDividendsFilter.revenueGrowthAnnualForNextFiscalYear) {
            arguments.add(
                Argument(
                    getRevenueGrowthAnnualClauses(earningsDividendsFilter),
                    REVENUE_GROWTH_ANNUAL_FOR_NEXT_FISCAL_YEAR_FIELD
                )
            )
        }
    }

    private fun getEpsGrowthAnnualClauses(earningsDividendsFilter: EarningsDividendsFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (earningsDividendsFilter.epsGrowthAnnual0to5) {
            clauses.addAll(growth0to5Clauses)
        }
        if (earningsDividendsFilter.epsGrowthAnnual5to10) {
            clauses.addAll(growth5to10Clauses)
        }
        if (earningsDividendsFilter.epsGrowthAnnual10to15) {
            clauses.addAll(growth10to15Clauses)
        }
        if (earningsDividendsFilter.epsGrowthAnnual15to25) {
            clauses.addAll(growth15to25Clauses)
        }
        if (earningsDividendsFilter.epsGrowthAnnual25to50) {
            clauses.addAll(growth25to50Clauses)
        }
        if (earningsDividendsFilter.epsGrowthAnnualGreaterThan50) {
            clauses.addAll(growth50toMoreClauses)
        }
        if (earningsDividendsFilter.epsGrowthAnnualPositiveChange) {
            clauses.addAll(growthPositiveChangeClauses)
        }
        if (earningsDividendsFilter.epsGrowthAnnualNegativeChange) {
            clauses.addAll(growthNegativeChangeClauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private fun getRevenueGrowthAnnualClauses(earningsDividendsFilter: EarningsDividendsFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (earningsDividendsFilter.revenueGrowthAnnual0to5) {
            clauses.addAll(growth0to5Clauses)
        }
        if (earningsDividendsFilter.revenueGrowthAnnual5to10) {
            clauses.addAll(growth5to10Clauses)
        }
        if (earningsDividendsFilter.revenueGrowthAnnual10to15) {
            clauses.addAll(growth10to15Clauses)
        }
        if (earningsDividendsFilter.revenueGrowthAnnual15to25) {
            clauses.addAll(growth15to25Clauses)
        }
        if (earningsDividendsFilter.revenueGrowthAnnual25to50) {
            clauses.addAll(growth25to50Clauses)
        }
        if (earningsDividendsFilter.revenueGrowthAnnualGreaterThan50) {
            clauses.addAll(growth50toMoreClauses)
        }
        if (earningsDividendsFilter.revenueGrowthAnnualPositiveChange) {
            clauses.addAll(growthPositiveChangeClauses)
        }
        if (earningsDividendsFilter.revenueGrowthAnnualNegativeChange) {
            clauses.addAll(growthNegativeChangeClauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private fun getEpsGrowthQuarterlyClauses(earningsDividendsFilter: EarningsDividendsFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (earningsDividendsFilter.epsGrowthQuarterly0to5) {
            clauses.addAll(growth0to5Clauses)
        }
        if (earningsDividendsFilter.epsGrowthQuarterly5to10) {
            clauses.addAll(growth5to10Clauses)
        }
        if (earningsDividendsFilter.epsGrowthQuarterly10to15) {
            clauses.addAll(growth10to15Clauses)
        }
        if (earningsDividendsFilter.epsGrowthQuarterly15to25) {
            clauses.addAll(growth15to25Clauses)
        }
        if (earningsDividendsFilter.epsGrowthQuarterly25to50) {
            clauses.addAll(growth25to50Clauses)
        }
        if (earningsDividendsFilter.epsGrowthQuarterlyGreaterThan50) {
            clauses.addAll(growth50toMoreClauses)
        }
        if (earningsDividendsFilter.epsGrowthQuarterlyPositiveChange) {
            clauses.addAll(growthPositiveChangeClauses)
        }
        if (earningsDividendsFilter.epsGrowthQuarterlyNegativeChange) {
            clauses.addAll(growthNegativeChangeClauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private fun getRevenueGrowthQuarterlyClauses(earningsDividendsFilter: EarningsDividendsFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (earningsDividendsFilter.revenueGrowthQuarterly0to5) {
            clauses.addAll(growth0to5Clauses)
        }
        if (earningsDividendsFilter.revenueGrowthQuarterly5to10) {
            clauses.addAll(growth5to10Clauses)
        }
        if (earningsDividendsFilter.revenueGrowthQuarterly10to15) {
            clauses.addAll(growth10to15Clauses)
        }
        if (earningsDividendsFilter.revenueGrowthQuarterly15to25) {
            clauses.addAll(growth15to25Clauses)
        }
        if (earningsDividendsFilter.revenueGrowthQuarterly25to50) {
            clauses.addAll(growth25to50Clauses)
        }
        if (earningsDividendsFilter.revenueGrowthQuarterlyGreaterThan50) {
            clauses.addAll(growth50toMoreClauses)
        }
        if (earningsDividendsFilter.revenueGrowthQuarterlyPositiveChange) {
            clauses.addAll(growthPositiveChangeClauses)
        }
        if (earningsDividendsFilter.revenueGrowthQuarterlyNegativeChange) {
            clauses.addAll(growthNegativeChangeClauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private fun getDividendYieldClauses(earningsDividendsFilter: EarningsDividendsFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (earningsDividendsFilter.dividendYieldPositive) {
            clauses.addAll(dividendYieldPositiveClauses)
        }
        if (earningsDividendsFilter.dividendYield0to2) {
            clauses.addAll(dividendYield0to2Clauses)
        }
        if (earningsDividendsFilter.dividendYield2to4) {
            clauses.addAll(dividendYield2to4Clauses)
        }
        if (earningsDividendsFilter.dividendYield4to6) {
            clauses.addAll(dividendYield4to6Clauses)
        }
        if (earningsDividendsFilter.dividendYield6to8) {
            clauses.addAll(dividendYield6to8Clauses)
        }
        if (earningsDividendsFilter.dividendYieldGreaterThan8) {
            clauses.addAll(dividendYield8toMoreClauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private val growth0to5Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("5"))
    )
    private val growth5to10Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("5")),
        Clause(OPERATOR_LESS_THAN, listOf("10"))
    )
    private val growth10to15Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("10")),
        Clause(OPERATOR_LESS_THAN, listOf("15"))
    )
    private val growth15to25Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("15")),
        Clause(OPERATOR_LESS_THAN, listOf("25"))
    )
    private val growth25to50Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("25")),
        Clause(OPERATOR_LESS_THAN, listOf("50"))
    )
    private val growth50toMoreClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("50")),
        Clause(OPERATOR_LESS_THAN, listOf("10001"))
    )
    private val growthPositiveChangeClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("10001"))
    )
    private val growthNegativeChangeClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("-100001")),
        Clause(OPERATOR_LESS_THAN, listOf("0"))
    )

    // overrides all other yieldValue values, but can be selected with this values
    private val dividendYieldPositiveClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(
            OPERATOR_LESS_THAN,
            listOf("1000")
        ) // unusual max value leads to loss 3 results, 3403/3406 with 10001 as max value
    )
    private val dividendYield0to2Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("2"))
    )
    private val dividendYield2to4Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("2")),
        Clause(OPERATOR_LESS_THAN, listOf("4"))
    )
    private val dividendYield4to6Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("4")),
        Clause(OPERATOR_LESS_THAN, listOf("6"))
    )
    private val dividendYield6to8Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("6")),
        Clause(OPERATOR_LESS_THAN, listOf("8"))
    )
    private val dividendYield8toMoreClauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("8")),
        Clause(OPERATOR_LESS_THAN, listOf("10001"))
    )

    private val dividendYieldClause = Clause(OPERATOR_EQUALS, listOf("1"))
}
