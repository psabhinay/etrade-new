package com.etrade.mobilepro.orders.presentation.section.viewfactory

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderDetailDescription
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.presentation.section.OrderDetailLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import java.math.BigDecimal
import javax.inject.Inject

interface MutualFundOrderDetailSectionViewFactory {
    fun createOrderSectionView(detailsDto: BaseOrderDetailsItem.OrderDetailsItem): List<TableSectionItemLayout>

    fun createOrderLegSectionView(orderLeg: OrderDetailDescription): List<TableSectionItemLayout>

    fun mutualFundFeesSection(detailsDto: BaseOrderDetailsItem.OrderDetailsItem?): List<TableSectionItemLayout>
}

class MutualOrderSectionViewFactory @Inject constructor(private val resources: OrderDetailLabelResources) : MutualFundOrderDetailSectionViewFactory {
    override fun createOrderSectionView(
        detailsDto: BaseOrderDetailsItem.OrderDetailsItem
    ): List<TableSectionItemLayout> {
        return with(resources.stock) {
            listOfNotNull(
                createSummarySection(orderStatus, resources.defaultResources.resource.getString(detailsDto.orderStatus.getLabel())),
                detailsDto.orderNumber.takeIf { it.toInt() != 0 }?.let {
                    createSummarySection(orderNumber, it)
                },
                detailsDto.orderPlacedDate?.let {
                    createSummarySection(orderPlaced, it)
                }
            )
        }
    }

    override fun createOrderLegSectionView(orderLeg: OrderDetailDescription): List<TableSectionItemLayout> {
        return with(resources.stock) {
            listOf(
                createSummarySection(
                    orderType, resources.defaultResources.resource.getString(orderLeg.transactionType.getLabel())
                ),
                createSummarySection(quantity, orderLeg.displayQuantity)
            )
        }
    }

    override fun mutualFundFeesSection(detailsDto: BaseOrderDetailsItem.OrderDetailsItem?): List<TableSectionItemLayout> {

        return if ((detailsDto?.orderStatus == OrderStatus.EXECUTED || detailsDto?.orderStatus == OrderStatus.OPEN)) {

            val estimatedCommission = detailsDto.estimatedCommission ?: BigDecimal.ZERO

            listOfNotNull(
                when (detailsDto.orderStatus) {
                    OrderStatus.OPEN -> {
                        createSummarySection(
                            resources.stock.estimatedCommissionFees,
                            MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(estimatedCommission)
                        )
                    }
                    OrderStatus.EXECUTED -> {
                        createSummarySection(
                            resources.stock.commissionFees,
                            MarketDataFormatter.formatMoneyValueWithMaxFiveDecimalsWithCurrency(estimatedCommission)
                        )
                    }
                    else -> null
                }
            )
        } else emptyList()
    }
}
