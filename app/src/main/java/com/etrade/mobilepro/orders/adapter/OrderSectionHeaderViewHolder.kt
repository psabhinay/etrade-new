package com.etrade.mobilepro.orders.adapter

import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.databinding.ItemOrderSectionHeaderBinding
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem
import com.etrade.mobilepro.util.android.binding.resources

class OrderSectionHeaderViewHolder(
    private val binding: ItemOrderSectionHeaderBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(item: OrderDetailDisplayItem.OrderSectionHeader?) {
        with(binding) {
            if (item != null) {
                sectionHeader.text = if (item.order != null) {
                    resources.getString(item.text, item.order)
                } else {
                    resources.getString(item.text)
                }
            } else {
                sectionHeader.text = resources.getString(R.string.empty_default)
            }
        }
    }
}
