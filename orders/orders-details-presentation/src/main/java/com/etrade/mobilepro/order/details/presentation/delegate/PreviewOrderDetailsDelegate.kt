package com.etrade.mobilepro.order.details.presentation.delegate

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.dynamic.form.api.Action
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.displaySymbol
import com.etrade.mobilepro.instrument.getSymbolUnderlier
import com.etrade.mobilepro.instrument.isSymbolLongOption
import com.etrade.mobilepro.order.details.api.OrderDetailItemInterface
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem.OrderBidAskItem
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem.OrderLegItem
import com.etrade.mobilepro.order.details.presentation.OrderDetailDisplayItem.OrderSectionHeader
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel
import com.etrade.mobilepro.order.details.presentation.OrderDetailsViewModel.ActionsArgument
import com.etrade.mobilepro.order.details.presentation.R
import com.etrade.mobilepro.order.details.presentation.helper.cancelOrderAction
import com.etrade.mobilepro.order.details.presentation.helper.conditionalOtoSectionHeaderResource
import com.etrade.mobilepro.order.details.presentation.helper.placeOrder
import com.etrade.mobilepro.order.details.presentation.securityType
import com.etrade.mobilepro.order.details.router.OrderDetailsArgument
import com.etrade.mobilepro.order.details.router.PreviewOrderDetailsArgument
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.StrategyType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.orders.extensions.android.getLabel
import com.etrade.mobilepro.orders.presentation.OrderButtonVisibilityManager
import com.etrade.mobilepro.orders.presentation.orderButtonVisibilityManager
import com.etrade.mobilepro.presentation.handleServerMessages
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.OrderLeg
import com.etrade.mobilepro.trade.api.OrderRequest
import com.etrade.mobilepro.trade.api.OrderResponse
import com.etrade.mobilepro.trade.api.TradeRepo
import com.etrade.mobilepro.trade.api.advanced.AdvancedOrderResponse
import com.etrade.mobilepro.trade.api.advanced.ContingentOrderDetails
import com.etrade.mobilepro.trade.api.advanced.ContingentOrderResponse
import com.etrade.mobilepro.trade.api.option.OptionOrder
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.api.ticker
import com.etrade.mobilepro.trade.optionlegselect.TradeLegBidAskViewModel
import com.etrade.mobilepro.trade.presentation.util.TradeExceptionViewModelDelegate
import com.etrade.mobilepro.trade.presentation.util.adapters.OrderAdapter
import com.etrade.mobilepro.trade.presentation.util.getSectionItems
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class PreviewOrderDetailsDelegate @Inject constructor(
    private val resources: Resources,
    private val tradeRepo: TradeRepo,
    private val orderAdapter: OrderAdapter,
    @Web
    private val baseUrl: String
) : OrderDetailsViewModel.BaseOrderDetailsDelegate() {

    private val placeLoadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)

    override val loadingIndicators: Set<LiveData<Boolean>> = setOf(placeLoadingIndicator)

    override suspend fun loadItems(
        argument: OrderDetailsArgument,
        quotes: Deferred<Map<String, OrderDetailItemInterface>>
    ): ETResult<OrderDetailsViewModel.OrderDetails> {

        val orderId = argument.orderId

        return tradeRepo.loadOrder(orderId)
            .onFailure { cancelOrder(orderId) }
            .map { tradeOrder ->
                val response = tradeOrder.response
                val shouldRefreshViewModels = argument.isViewModelRefreshRequired || response.orders.size > 1
                val items = response.orders.mapIndexed { index, order ->
                    createItems(index, order, tradeOrder.orderRequest, shouldRefreshViewModels, response is AdvancedOrderResponse)
                }.flatten().toMutableList().apply {
                    if (response is ContingentOrderResponse) {
                        addAll(response.createContingentItems())
                    }
                }
                OrderDetailsViewModel.OrderDetails(items, OrderStatus.UNKNOWN)
            }
    }

    override fun createActions(argument: ActionsArgument, manager: OrderButtonVisibilityManager): List<Action> {
        val orderId = argument.orderDetailsArgument.orderId
        return listOfNotNull(
            manager.cancelOrderAction(resources) {
                cancelOrder(orderId)
            },
            placeOrder(resources) {
                placeOrder(orderId)
            }
        )
    }

    override fun createButtonVisibilityManager(argument: OrderDetailsArgument): OrderButtonVisibilityManager {
        return orderButtonVisibilityManager {
            isCancellable = if (argument is PreviewOrderDetailsArgument) { argument.allowCancel } else { true }
            isCancelOnly = false
        }
    }

    override fun resetActionStates() = Unit

    private fun createItems(
        index: Int,
        order: Order,
        orderRequest: OrderRequest?,
        refreshViewModels: Boolean,
        isAdvancedOrder: Boolean
    ): List<OrderDetailItemInterface> {
        val (quoteWidget, tradeLegBidAskViewModel) = getQuoteDetailsViewModels(index)

        if (refreshViewModels) {
            val ticker = order.getOptionsUnderlierForAdvancedOrder(isAdvancedOrder)
            quoteWidget.getQuotes(ticker, true)
        }

        val orderLegs = order.legs.map { leg ->
            val isAMLegOption = orderRequest?.legDetails?.firstOrNull {
                it.symbol == leg.symbol
            }?.isAMOption ?: false
            leg.toOrderLegItem(isAMLegOption)
        }
        return listOfNotNull(conditionalOtoSectionHeader(index), order.toStrategyItem(), quoteWidget) +
            orderLegs +
            listOfNotNull<OrderDetailItemInterface>(
                order.toBidAskItem(quoteWidget, tradeLegBidAskViewModel),
                order.toOrderSectionItem()
            )
    }

    private fun Order.getOptionsUnderlierForAdvancedOrder(isAdvancedOrder: Boolean) =
        if (isAdvancedOrder && this is OptionOrder) {
            getSymbolUnderlier(ticker, InstrumentType.OPTN)
        } else {
            ticker
        }

    private fun cancelOrder(orderId: String) {
        sendOrderDeletedSignal(orderId)
    }

    private fun placeOrder(orderId: String) {
        executeOperation(
            loadingIndicator = placeLoadingIndicator,
            operation = { tradeRepo.placeOrder(orderId) },
            onSuccess = { handleSuccess(orderId, it.response) },
            interceptFailure = { TradeExceptionViewModelDelegate(resources, baseUrl, mutableDialog, emptyMap()).handleTradeOrderException(it) }
        )
    }

    private fun handleSuccess(previewId: String, response: OrderResponse) {
        handleServerMessages(resources, baseUrl, response.warnings, mutableDialog, emptyMap()) {
            sendOrderConfirmedSignal(previewId, response, response.orders.first().securityType)
        }
    }

    private val OrderDetailsArgument.isViewModelRefreshRequired: Boolean
        get() = (this as? PreviewOrderDetailsArgument)?.allowCancel == false

    private fun Order.toBidAskItem(
        quoteWidget: QuotesWidgetViewModel,
        tradeLegBidAskViewModel: TradeLegBidAskViewModel
    ): OrderBidAskItem {
        return when (this) {
            is OptionOrder -> tradeLegBidAskViewModel.createOptionsBidAskWidgetItem {
                legs.map { it.toTradeLeg() }
            }
            else -> quoteWidget.let { OrderBidAskItem.DynamicOrderBidAskItem(it) }
        }
    }

    private inline fun TradeLegBidAskViewModel.createOptionsBidAskWidgetItem(getOptionLegs: () -> Iterable<TradeLeg>): OrderBidAskItem {
        subscribeToBidAsk(getOptionLegs())
        return OrderBidAskItem.DelegatedOrderBidAskItem(quoteBidAskItem)
    }

    private fun OrderLeg.toOrderLegItem(isAMOption: Boolean): OrderLegItem {
        return OrderLegItem(
            displaySymbol = displaySymbol,
            displayQuantity = quantity?.toPlainString().orEmpty(),
            transactionType = action,
            isAMOption = isAMOption
        )
    }

    private fun OrderLeg.toTradeLeg(): TradeLeg {
        return object : TradeLeg {
            override val symbol: String = this@toTradeLeg.symbol
            override val transactionType: TransactionType = this@toTradeLeg.action
            override val quantity: Int = this@toTradeLeg.quantity?.toInt() ?: 0
            override val displaySymbol: String = this@toTradeLeg.displaySymbol
            override val isAMOption: Boolean = false
        }
    }

    private fun conditionalOtoSectionHeader(index: Int): OrderSectionHeader? {
        return conditionalOtoSectionHeaderResource(index)?.let {
            OrderSectionHeader(it)
        }
    }

    private fun Order.toStrategyItem(): OrderSectionHeader? {
        if (this !is OptionOrder || strategy == StrategyType.UNKNOWN) {
            return null
        }
        return OrderSectionHeader(strategy.getLabel())
    }

    private fun Order.toOrderSectionItem(): OrderDetailDisplayItem.OrderSectionItem {
        val items = orderAdapter.createOrderDisplayItem(this).sections
        return OrderDetailDisplayItem.OrderSectionItem(items)
    }

    private fun ContingentOrderResponse.createContingentItems(): List<OrderDetailItemInterface> {
        val details = contingentOrderDetails.firstOrNull()
        return if (details != null) {
            val (quoteWidget, tradeLegBidAskViewModel) = getQuoteDetailsViewModels(1)
            val isOption = isSymbolLongOption(details.symbol)
            val ticker = details.symbol.let {
                if (isOption) {
                    getSymbolUnderlier(it, InstrumentType.OPTN)
                } else {
                    it
                }
            }
            quoteWidget.getQuotes(ticker, true)
            listOfNotNull(
                OrderSectionHeader(R.string.orders_divider_contingent),
                quoteWidget,
                details.toContingentOptionsSymbolDisplayItem(isOption),
                createContingentSymbolBidAskItem(details, tradeLegBidAskViewModel, quoteWidget),
                OrderDetailDisplayItem.OrderSectionItem(details.getSectionItems(resources))
            )
        } else {
            emptyList()
        }
    }

    // For Options Symbol, QuoteWidget should display underlier quotes and bid ask should be of options ticker
    private fun createContingentSymbolBidAskItem(
        details: ContingentOrderDetails,
        tradeLegBidAskViewModel: TradeLegBidAskViewModel,
        quoteWidget: QuotesWidgetViewModel
    ) = if (isSymbolLongOption(details.symbol)) {
        tradeLegBidAskViewModel.createOptionsBidAskWidgetItem {
            val leg = object : TradeLeg {
                override val symbol: String = details.symbol

                // Contingent symbol transactions / Quantity is added here to calculate placeholder bid ask
                override val transactionType: TransactionType = TransactionType.BUY_CLOSE
                override val quantity: Int = 1
                override val displaySymbol: String = details.displaySymbol
                override val isAMOption: Boolean = false
            }
            listOf(leg)
        }
    } else {
        OrderBidAskItem.DynamicOrderBidAskItem(quoteWidget)
    }

    private fun ContingentOrderDetails.toContingentOptionsSymbolDisplayItem(isOption: Boolean): OrderDetailItemInterface? = if (isOption) {
        OrderLegItem(displaySymbol)
    } else {
        null
    }
}
