package com.etrade.mobilepro.news.dynamic

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.briefing.api.BriefingNewsRepo
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.news.presentation.dynamic.NewsWidgetBriefingViewModel
import com.etrade.mobilepro.rx.test.RxImmediateSchedulerRule
import com.etrade.mobilepro.util.Resource
import com.jraska.livedata.test
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations

private const val CACHE_EXPIRATION_TIME = 15000L

class NewsWidgetBriefingViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    companion object {
        @ClassRule
        @JvmField
        val schedulers = RxImmediateSchedulerRule()
    }

    private lateinit var sut: NewsWidgetBriefingViewModel

    @Mock
    private lateinit var widgetNewsRepo: BriefingNewsRepo

    @Mock
    private lateinit var statusRepo: ReadStatusRepo

    @Mock
    private lateinit var router: NewsRouter
    private val layoutId = R.layout.widget_briefing_news
    private val newsItemText =
        NewsItem.Text("datetime", "title", "url", emptyList(), "content", "docId")

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun `success case`() {
        whenever(widgetNewsRepo.getBriefingNews(CACHE_EXPIRATION_TIME)).thenReturn(
            Observable.just(
                Resource.Success(listOf(newsItemText as NewsItem))
            )
        )
        whenever(statusRepo.wasRead(any())).thenReturn(false)
        sut = NewsWidgetBriefingViewModel(widgetNewsRepo, statusRepo, router, layoutId)
        sut.fetchNews()
        sut.newsItems
            .test()
            .awaitValue()
            .assertValue {
                it.size == 1 && !it[0].wasRead && it[0].item is NewsItem.Text && (it[0].item as NewsItem.Text).docId == "docId"
            }
    }

    @Test
    fun `failed case shows empty item and failed view state`() {
        whenever(widgetNewsRepo.getBriefingNews(CACHE_EXPIRATION_TIME)).thenReturn(
            Observable.just(
                Resource.Failed(error = IllegalStateException("boom"))
            )
        )
        whenever(statusRepo.wasRead(any())).thenReturn(false)
        sut = NewsWidgetBriefingViewModel(widgetNewsRepo, statusRepo, router, layoutId)
        sut.fetchNews()

        sut.newsItems
            .test()
            .awaitValue()
            .assertValue {
                it.size == 1 && !it[0].wasRead && it[0].item is NewsItem.Empty
            }
        sut.viewState
            .test()
            .awaitValue()
            .assertValue {
                it is DynamicUiViewState.Error
            }
    }
}
