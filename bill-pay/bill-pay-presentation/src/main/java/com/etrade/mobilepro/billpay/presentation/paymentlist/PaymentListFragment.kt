package com.etrade.mobilepro.billpay.presentation.paymentlist

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.billpay.api.Payment
import com.etrade.mobilepro.billpay.api.PaymentStatus
import com.etrade.mobilepro.billpay.presentation.BillPayContainerFragmentDirections
import com.etrade.mobilepro.billpay.presentation.BillPayViewModel
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.billpay.presentation.common.NavigationViewModel
import com.etrade.mobilepro.billpay.presentation.databinding.BillPayFragmentPaymentListBinding
import com.etrade.mobilepro.billpay.presentation.getLabel
import com.etrade.mobilepro.billpay.presentation.paymentlist.adapter.PaymentListAdapter
import com.etrade.mobilepro.billpay.presentation.viewmodel.PaymentListViewModelFactoryFactory
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.disposeOnDestinationChanged
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

private const val FILTER_PAYMENTSTATUSLIST_TAG = "FILTER_PAYMENTSTATUSLIST_TAG"
private const val FILTER_DATERANGE_TAG = "FILTER_DATERANGE_TAG"
private const val LAST_SELECTED_PAYMENTSTATUS = "LAST_SELECTED_PAYMENTSTATUS"
private const val LAST_SELECTED_DATERANGE = "LAST_SELECTED_DATERANGE"

@RequireLogin
class PaymentListFragment @Inject constructor(
    private val paymentListViewModelFactory: PaymentListViewModelFactoryFactory,
    private val viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory
) : Fragment(R.layout.bill_pay_fragment_payment_list) {

    private val binding by viewBinding(BillPayFragmentPaymentListBinding::bind)

    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.bill_pay_nav_graph)
    private val sharedViewModel: BillPayViewModel by navGraphViewModels(R.id.bill_pay_nav_graph) { viewModelFactory }
    private val viewModel: PaymentListViewModel by viewModels {
        paymentListViewModelFactory.create(
            sharedViewModel.viewState.value?.billPaySessionId,
            sharedViewModel.viewState.value?.allAccounts
        )
    }

    private val snackBarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private var snackbar: Snackbar? = null

    private val paymentAdapter: PaymentListAdapter = PaymentListAdapter {
        viewModel.findParcelablePaymentById(it.paymentId, it.accountName)?.let { payment ->
            findNavController().navigate(
                BillPayContainerFragmentDirections.actionPaymentListToPaymentDetails(
                    payment
                )
            )
        }
    }

    private val disposeListener = disposeOnDestinationChanged { sharedViewModel.disposeState() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        savedInstanceState?.let { bundle ->
            viewModel.onInitBundleData(
                getPaymentStatusFromBundleOrDefault(bundle),
                getDateRangeFromBundleOrDefault(bundle)
            )
        }

        setupFragment()
        setupViewModels()
        setupDropDowns()
    }

    private fun getPaymentStatusFromBundleOrDefault(bundle: Bundle) =
        PaymentStatus.fromStatusString(bundle.getString(LAST_SELECTED_PAYMENTSTATUS, PaymentStatus.ALL.status))

    private fun getDateRangeFromBundleOrDefault(bundle: Bundle) =
        resources.getString(
            (
                DateRangeFilter.fromRangeName(bundle.getString(LAST_SELECTED_DATERANGE, DateRangeFilter.DAYS_30.rangeName))
                    ?: DateRangeFilter.DAYS_30
                ).labelRes
        )

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(LAST_SELECTED_PAYMENTSTATUS, viewModel.viewState.value?.selectedStatus?.status)
        outState.putString(LAST_SELECTED_DATERANGE, viewModel.viewState.value?.selectedDateRange?.rangeName)
    }

    override fun onResume() {
        super.onResume()
        findNavController().addOnDestinationChangedListener(disposeListener)
    }

    override fun onPause() {
        super.onPause()
        findNavController().removeOnDestinationChangedListener(disposeListener)
    }

    private fun setupFragment() {
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.refresh()
        }
        binding.swipeRefresh.isRefreshing = false
        with(binding.paymentListRv) {
            layoutManager = LinearLayoutManager(requireContext())
            setupRecyclerViewDivider(R.drawable.thin_divider, countOfLastItemsWithoutDivider = 0)
            adapter = paymentAdapter
        }
    }

    private fun setupDropDowns() {
        setupPaymentStatusDropDown()
        setupDateRangeDropDown()
    }

    private fun setupViewModels() {
        navigationViewModel.refreshPayments.observe(
            viewLifecycleOwner,
            Observer {
                it.getNonConsumedContent()?.let { viewModel.refresh() }
            }
        )
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer { state ->
                renderError(state.error)
                renderLoading(state.isLoading, state.isRefreshing)
                renderContent(state)
            }
        )
    }

    private fun setupPaymentStatusDropDown() {
        parentFragmentManager.let { fragmentManager ->
            val dropDownManager = BottomSheetSelector<PaymentStatusFilterItem>(fragmentManager, resources)
            dropDownManager.init(
                tag = FILTER_PAYMENTSTATUSLIST_TAG,
                title = getString(R.string.status),
                items = viewModel.paymentStatusFilterList,
                initialSelectedPosition = viewModel.selectedPaymentStatusFilterIndex
            )
            dropDownManager.setListener { _, selected ->
                viewModel.onPaymentStatusSelected(selected.status)
            }
            binding.paymentStatus.setOnClickListener {
                dropDownManager.openDropDown()
            }
        }
    }

    private fun setupDateRangeDropDown() {
        parentFragmentManager.let { fragmentManager ->
            val dropDownManager = BottomSheetSelector<String>(fragmentManager, resources)
            dropDownManager.init(
                tag = FILTER_DATERANGE_TAG,
                title = getString(R.string.bill_pay_status_datarange_title),
                items = viewModel.dateRangeFilterLabelList,
                initialSelectedPosition = viewModel.selectedDateRangeFilterIndex
            )
            dropDownManager.setListener { _, selected ->
                viewModel.onDateRangeSelected(selected)
            }
            binding.paymentDateRange.setOnClickListener {
                dropDownManager.openDropDown()
            }
        }
    }

    private fun renderError(error: Error?) {
        if (error == null) {
            snackbar?.dismiss()
        } else {
            snackbar = snackBarUtil.retrySnackbar {
                error.retryAction?.invoke()
            }
        }
    }

    private fun renderLoading(isLoading: Boolean, isRefreshing: Boolean) {
        if (isLoading) {
            binding.progressBar.show()
        } else {
            binding.progressBar.hide()
        }

        if (binding.swipeRefresh.isRefreshing && !isRefreshing) {
            binding.paymentListRv.scrollToPosition(0)
        }
        binding.swipeRefresh.isRefreshing = isRefreshing
    }

    private fun renderContent(state: ViewState) = with(state) {
        binding.noContent.visibility = if (items?.isEmpty() == true) {
            View.VISIBLE
        } else {
            View.GONE
        }

        binding.paymentStatus.text = resources.getText(selectedStatus.getLabel())
        binding.paymentStatus.contentDescription = getString(
            R.string.label_filter_value,
            getString(R.string.filter_type),
            getPaymentStatusContentDescription(selectedStatus.getLabel())
        )
        binding.paymentDateRange.text = resources.getText(selectedDateRange.labelRes)
        binding.paymentDateRange.contentDescription = getString(
            R.string.label_filter_value,
            getString(R.string.filter_date),
            getString(selectedDateRange.labelRes)
        )

        items?.toPaymentList(sharedViewModel)?.also { paymentListItem ->
            paymentAdapter.items = paymentListItem
        }
    }

    private fun getPaymentStatusContentDescription(@StringRes id: Int): String {
        return if (id == R.string.empty_default) {
            getString(R.string.empty_default_accessible)
        } else {
            getString(id)
        }
    }
}

private fun List<Payment>.toPaymentList(sharedViewModel: BillPayViewModel): List<PaymentListItem> {
    return this.map { payment ->
        val accountShortName = sharedViewModel.viewState.value?.allAccounts?.firstOrNull() { accountDetails ->
            accountDetails.accountId == payment.accountId
        }?.accountName
        payment.toPaymentListItem(accountShortName)
    }
}
