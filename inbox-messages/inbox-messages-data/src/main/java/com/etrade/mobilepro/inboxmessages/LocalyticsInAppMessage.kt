package com.etrade.mobilepro.inboxmessages

import com.etrade.mobilepro.inboxmessages.InboxAttributes.CALL_TO_ACTION
import com.etrade.mobilepro.inboxmessages.InboxAttributes.DEEP_LINK
import com.etrade.mobilepro.inboxmessages.InboxAttributes.SUBTITLE
import com.etrade.mobilepro.inboxmessages.InboxAttributes.TITLE
import com.localytics.androidx.InAppCampaign

data class LocalyticsInAppMessage(val campaign: InAppCampaign) : InAppMessage {

    override val eventName: String? = campaign.eventName

    override val rawAttributes: Map<String, String> = campaign.attributes.toCaseInsensitiveMap()

    override val title: String? = rawAttributes[TITLE]

    override val subtitle: String? = rawAttributes[SUBTITLE]

    override val callToAction: String? = rawAttributes[CALL_TO_ACTION]

    override val deepLink: String? = rawAttributes[DEEP_LINK]
}
