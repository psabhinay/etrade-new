package com.etrade.mobilepro.calendar

import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.threeten.bp.LocalDate
import org.threeten.bp.Month

class CalendarPeriodFunctionsTest {

    private val bias = (50 / 2) + 1 // 50 pages for calendar 26 should be middle of calendar with current date

    @Test
    fun `should calculate correct values for month calendar mode`() {
        val mode = CalendarMode.MONTH
        val currentDate = LocalDate.of(2019, Month.NOVEMBER, 20)
        var calendarPage = bias // current month

        assertEquals(0, mode.getOffset(bias, calendarPage))
        assertEquals(LocalDate.of(2019, Month.OCTOBER, 27), mode.getFirstDateOfRange(currentDate, calendarPage))
        assertEquals(LocalDate.of(2019, Month.NOVEMBER, 30), mode.getLastDateOfRange(currentDate, calendarPage))

        calendarPage = bias + 1 // next month

        assertEquals(1, mode.getOffset(bias, calendarPage))
        assertEquals(LocalDate.of(2019, Month.DECEMBER, 1), mode.getFirstDateOfRange(currentDate, calendarPage))
        assertEquals(LocalDate.of(2019, Month.DECEMBER, 31), mode.getLastDateOfRange(currentDate, calendarPage))

        calendarPage = bias - 1 // previous month

        assertEquals(-1, mode.getOffset(bias, calendarPage))
        assertEquals(LocalDate.of(2019, Month.SEPTEMBER, 29), mode.getFirstDateOfRange(currentDate, calendarPage))
        assertEquals(LocalDate.of(2019, Month.OCTOBER, 31), mode.getLastDateOfRange(currentDate, calendarPage))
    }

    @Test
    fun `should calculate correct values for two weeks calendar mode`() {
        val mode = CalendarMode.TWO_WEEKS
        val currentDate = LocalDate.of(2019, Month.NOVEMBER, 20)
        var calendarPage = bias

        assertEquals(0, mode.getOffset(bias, calendarPage))
        assertEquals(LocalDate.of(2019, Month.NOVEMBER, 17), mode.getFirstDateOfRange(currentDate, calendarPage))
        assertEquals(LocalDate.of(2019, Month.NOVEMBER, 30), mode.getLastDateOfRange(currentDate, calendarPage))

        calendarPage = bias + 1 // next two weeks

        assertEquals(1, mode.getOffset(bias, calendarPage))
        assertEquals(LocalDate.of(2019, Month.DECEMBER, 1), mode.getFirstDateOfRange(currentDate, calendarPage))
        assertEquals(LocalDate.of(2019, Month.DECEMBER, 14), mode.getLastDateOfRange(currentDate, calendarPage))

        calendarPage = bias - 1 // previous two weeks

        assertEquals(-1, mode.getOffset(bias, calendarPage))
        assertEquals(LocalDate.of(2019, Month.NOVEMBER, 3), mode.getFirstDateOfRange(currentDate, calendarPage))
        assertEquals(LocalDate.of(2019, Month.NOVEMBER, 16), mode.getLastDateOfRange(currentDate, calendarPage))
    }
}
