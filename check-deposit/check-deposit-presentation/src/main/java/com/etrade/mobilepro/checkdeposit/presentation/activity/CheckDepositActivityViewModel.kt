package com.etrade.mobilepro.checkdeposit.presentation.activity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.checkdeposit.activity.DepositActivityRepository
import com.etrade.mobilepro.checkdeposit.activity.service.DepositActivityItemDto
import kotlinx.coroutines.launch
import javax.inject.Inject

class CheckDepositActivityViewModel @Inject constructor(
    private val depositActivityRepository: DepositActivityRepository
) : ViewModel() {
    private val _viewState = MutableLiveData<ViewState>(ViewState.Loading)
    val viewState: LiveData<ViewState>
        get() = _viewState

    init {
        loadDepositActivityItems()
    }

    fun loadDepositActivityItems() {
        _viewState.value = ViewState.Loading
        viewModelScope.launch {
            _viewState.value = depositActivityRepository.loadDepositActivityItems().fold(
                onSuccess = {
                    ViewState.Success(it)
                },
                onFailure = {
                    ViewState.Error(it.localizedMessage)
                }
            )
        }
    }

    sealed class ViewState {
        object Loading : ViewState()

        class Error(val message: String? = null) : ViewState()

        data class Success(val depositActivityItems: List<DepositActivityItemDto>) : ViewState()
    }
}
