package com.etrade.mobilepro.earnings.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
data class EarningsDetailResponse(
    @Json(name = "mobile_response")
    val response: EarningsDetailReferenceResponse?
)

@JsonClass(generateAdapter = true)
data class EarningsDetailReferenceResponse(
    @Json(name = "references")
    val detail: List<EarningsReferencesDetailList>?,
    @Json(name = "views")
    val views: List<EarningsDetailViews>?
)

@JsonClass(generateAdapter = true)
data class EarningsDetailViews(
    @Json(name = "type")
    val viewType: String?,
    @Json(name = "data")
    val viewData: EarningsDetailViewData?,
)

@JsonClass(generateAdapter = true)
data class EarningsDetailViewData(
    @Json(name = "symbol")
    val symbol: String?,
    @Json(name = "cusip")
    val cusip: String?,
    @Json(name = "companyName")
    val companyName: String?
)

@JsonClass(generateAdapter = true)
data class EarningsReferencesDetailList(
    @Json(name = "type")
    val referenceType: String?,
    @Json(name = "data")
    val referenceData: List<EarningsReferenceDetailData>?
)

@JsonClass(generateAdapter = true)
data class EarningsReferenceDetailData(
    @Json(name = "epsActiveDate")
    val activeDate: String?,
    @Json(name = "epsActiveTime")
    val activeTime: String?,
    @Json(name = "epsActualValue")
    val actualValue: BigDecimal?,
    @Json(name = "epsAnnounceDate")
    val announceDate: String?,
    @Json(name = "epsAnnounceTime")
    val announceTime: String?,
    @Json(name = "epsEstLower")
    val estLower: Int?,
    @Json(name = "epsEstMean")
    val estMean: BigDecimal?,
    @Json(name = "epsEstRaise")
    val estRaise: Int?,
    @Json(name = "epsEstCount")
    val estCount: Int?,
    @Json(name = "epsHigh")
    val epsHigh: BigDecimal?,
    @Json(name = "epsLow")
    val epsLow: BigDecimal?,
    @Json(name = "epsNumberRecm")
    val numberRecm: Int?,
    @Json(name = "epsQtr1ExpectedDate")
    val qtr1ExpectedDate: String?,
    @Json(name = "epsQtr2ExpectedDate")
    val qtr2ExpectedDate: String?,
    @Json(name = "epsRecmLower")
    val recmLower: Int?,
    @Json(name = "epsRecmMean")
    val recmMean: BigDecimal?,
    @Json(name = "epsRecmRaised")
    val recmRaised: Int?,
    @Json(name = "epsSurpriseMean")
    val surpriseMean: BigDecimal?,
    @Json(name = "fiscalPeriod")
    val fiscalPeriod: String?,
    @Json(name = "fiscalPeriodYear")
    val fiscalPeriodYear: Int?,
    @Json(name = "fiscalPeriodMonth")
    val fiscalPeriodMonth: Int?,
    @Json(name = "forecastPeriod")
    val forecastPeriod: String?
)
