package com.etrade.mobilepro.trade.data.form.input

import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.data.form.CommonAllOrNoneValueCoder
import com.etrade.mobilepro.trade.data.form.DefaultSymbolInputValueCoder
import com.etrade.mobilepro.trade.data.form.value.AdvancedActionValueId
import com.etrade.mobilepro.trade.data.form.value.CommonOrderTermValueId
import com.etrade.mobilepro.trade.data.form.value.CommonPriceTypeValueId
import com.etrade.mobilepro.trade.data.form.value.OptionStrategyTypeValueId
import com.etrade.mobilepro.trade.form.api.input.ContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ContingentOrderInputId
import com.etrade.mobilepro.trade.form.api.input.TradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.WithAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.form.api.value.ActionValueId
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.trade.form.api.value.StrategyTypeValueId
import com.etrade.mobilepro.util.ValueCoder

object DefaultContingentOptionTradeFormInputId :
    ContingentOptionTradeFormInputId,
    TradeFormInputId by DefaultTradeFormInputId,
    WithAdvancedTradeFormInputId by DefaultAdvancedTradeFormInputId {
    override val accountId: String = "accountid"
    override val action: String = "transaction0"
    override val actionValueId: ActionValueId = AdvancedActionValueId
    override val allOrNone: String = "aon0"
    override val allOrNoneValueCoder: ValueCoder<Boolean> = CommonAllOrNoneValueCoder
    override val contingentOrderInputId: ContingentOrderInputId = DefaultContingentOrderInputId
    override val limitPrice: String = "price0"
    override val limitPriceForStopLimit: String = "limitPriceForStopLimit"
    override val lookupSymbol: String = "underlier"
    override val orderId: String = "originalOrderNo"
    override val priceType: String = "pricetype0"
    override val priceTypeValueId: PriceTypeValueId = CommonPriceTypeValueId
    override val quantity: String = "quantity0"
    override val stopPrice: String = "stopprice0"
    override val stopPriceForStopLimit: String = "stopPriceForStopLimit"
    override val strategy: String = "strategyType"
    override val strategyValueId: StrategyTypeValueId = OptionStrategyTypeValueId
    override val symbol: String = "symbol0"
    override val symbolCoder: ValueCoder<WithSymbolInfo> = DefaultSymbolInputValueCoder
    override val term: String = "orderterm0"
    override val termValueId: OrderTermValueId = CommonOrderTermValueId
    override val tradeLegs: String = "legRequestList"
}
