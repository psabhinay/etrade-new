package com.etrade.mobilepro.transfermoney.data.rest

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TransferActivitiesRequestDto(
    @Json(name = "transferActivities")
    val transferActivities: Any? = Any()
)
