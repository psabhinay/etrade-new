package com.etrade.mobilepro.inboxmessages.dynamicui.api.fundingprompt

import com.etrade.mobile.accounts.dto.AccountDto

sealed class FundingPrompt {
    companion object {
        const val PROMPT_INBOX_CODE = "U"
    }

    abstract val account: AccountDto
    abstract val image: String

    class SmallWithAccountName(override val account: AccountDto, override val image: String) : FundingPrompt() {
        companion object {
            const val HEIGHT = "72"
        }
    }

    class Small(override val account: AccountDto, override val image: String) : FundingPrompt() {
        companion object {
            const val HEIGHT = "72"
        }
    }

    class Medium(override val account: AccountDto, override val image: String) : FundingPrompt() {
        companion object {
            const val HEIGHT = "254"
        }
    }

    class Large(override val account: AccountDto, override val image: String) : FundingPrompt() {
        companion object {
            const val HEIGHT = "302"
        }
    }
}
