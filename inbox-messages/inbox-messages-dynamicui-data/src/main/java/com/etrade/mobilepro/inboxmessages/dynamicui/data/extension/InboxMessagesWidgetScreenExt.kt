package com.etrade.mobilepro.inboxmessages.dynamicui.data.extension

import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.inboxmessages.dynamicui.api.InboxMessagesWidgetPlace

@Suppress("ComplexMethod") // just a lot of one statement cases
fun ServicePath.toInboxMessagesWidgetPlace(): InboxMessagesWidgetPlace? = when (this) {
    ServicePath.AccountList -> null
    is ServicePath.BaseAccountOverView -> InboxMessagesWidgetPlace.OVERVIEW
    is ServicePath.AllBrokeragePositions -> null
    ServicePath.AuthenticatedSettings -> null
    is ServicePath.Balances -> null
    is ServicePath.CompleteView -> InboxMessagesWidgetPlace.COMPLETE_VIEW
    ServicePath.MsCompleteView -> null
    ServicePath.OverView -> null
    is ServicePath.Portfolio -> InboxMessagesWidgetPlace.OVERVIEW
    ServicePath.Settings -> null
    is ServicePath.TaxLots -> null
    is ServicePath.StatementDocuments -> null
    is ServicePath.TaxDocuments -> null
    ServicePath.DynamicMenu -> null
    is ServicePath.AuthenticatedMarketOverview -> null
    ServicePath.MarketOverview -> null
}

fun ScreenRequest.toInboxMessagesWidgetPlace(): InboxMessagesWidgetPlace? = (this as? ServicePath)?.toInboxMessagesWidgetPlace()
