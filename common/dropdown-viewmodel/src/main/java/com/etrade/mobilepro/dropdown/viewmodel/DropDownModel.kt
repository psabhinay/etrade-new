package com.etrade.mobilepro.dropdown.viewmodel

class DropDownModel<out T>(
    val tag: String,
    val items: List<T>,
    val title: String,
    val initialSelectedPosition: Int,
    val dividerPosition: Int?
)
