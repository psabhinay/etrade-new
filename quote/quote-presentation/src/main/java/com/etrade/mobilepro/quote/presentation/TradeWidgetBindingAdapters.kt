package com.etrade.mobilepro.quote.presentation

import android.content.res.Resources
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.eo.core.util.MarketDataFormatter.EMPTY_FORMATTED_VALUE
import com.etrade.mobilepro.quoteapi.QuotePriceWidgetItem
import com.etrade.mobilepro.util.android.accessibility.processAccessibilityStreamingEvent
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.isQuoteSizeValid
import com.etrade.mobilepro.util.safeParseBigDecimal

@BindingAdapter("formatTradeScreenPrice")
internal fun TextView.formatTradeScreenPrice(item: QuotePriceWidgetItem?) {
    setQuoteTradePrice(item)
    contentDescription = item?.getQuotePriceDescription(resources)
}

@BindingAdapter("formatTradeScreenBidAsk")
internal fun TextView.setQuoteTradePrice(item: QuotePriceWidgetItem?) {
    item?.let {
        formatQuotePrice(item.price, item.size, item.exchangeCode)
    } ?: run {
        text = EMPTY_FORMATTED_VALUE
    }
}

@BindingAdapter("processQuoteBidDescription")
internal fun View.processQuoteBidDescription(item: QuotePriceWidgetItem?) {
    processQuoteBidAskDescription(
        item,
        if (item?.isNet == true) {
            R.string.net_bid
        } else {
            R.string.bid_title_text
        }
    )
}

@BindingAdapter("processQuoteAskDescription")
internal fun View.processQuoteAskDescription(item: QuotePriceWidgetItem?) {
    processQuoteBidAskDescription(
        item,
        if (item?.isNet == true) {
            R.string.net_ask
        } else {
            R.string.ask_title_text
        }
    )
}

private fun QuotePriceWidgetItem.getQuotePriceDescription(resources: Resources): String {
    val quotePrice = resources.getString(
        R.string.quote_content_description_trade_price,
        MarketDataFormatter.formatMarketDataCustom(price.safeParseBigDecimal())
    )
    return if (size.isQuoteSizeValid()) {
        val quoteSize =
            resources.getString(R.string.quote_content_description_trade_size, getFormattedSize())
        "$quotePrice $quoteSize"
    } else {
        quotePrice
    }
}

private fun View.processQuoteBidAskDescription(item: QuotePriceWidgetItem?, labelRes: Int) {
    processAccessibilityStreamingEvent("${resources.getString(labelRes)} ${item?.getQuotePriceDescription(resources)}")
}
