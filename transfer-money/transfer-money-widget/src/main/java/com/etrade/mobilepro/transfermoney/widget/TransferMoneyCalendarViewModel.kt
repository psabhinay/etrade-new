package com.etrade.mobilepro.transfermoney.widget

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.transfermoney.usecase.calendar.GetTransferCalendarRequest
import com.etrade.mobilepro.transfermoney.usecase.calendar.GetTransferCalendarUseCase
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate

private const val LEAD_DAYS_FOR_EXTERNAL_ACCOUNTS = 3
private const val LEAD_DAYS_FOR_INTERNAL_ACCOUNTS = 0

private val logger = LoggerFactory.getLogger(TransferMoneyCalendarViewModel::class.java)

@Suppress("LongParameterList")
class TransferMoneyCalendarViewModel @AssistedInject constructor(
    private val transferCalendarUseCase: GetTransferCalendarUseCase,
    @Assisted("isOriginAccountExternal") private val isOriginAccountExternal: Boolean,
    @Assisted("isDestinationAccountExternal") private val isDestinationAccountExternal: Boolean,
    @Assisted("currentDate") private val currentDate: LocalDate,
    @Assisted("initialDate") private val initialDate: LocalDate?,
    @Assisted private val leadDays: Int?
) : ViewModel() {

    private val _viewState: MutableLiveData<ViewState> = MutableLiveData(ViewState())
    internal val viewState: LiveData<ViewState> get() = _viewState

    @AssistedFactory
    interface Factory {
        fun create(
            @Assisted("isOriginAccountExternal") isOriginAccountExternal: Boolean,
            @Assisted("isDestinationAccountExternal") isDestinationAccountExternal: Boolean,
            @Assisted("currentDate") currentDate: LocalDate,
            @Assisted("initialDate") initialDate: LocalDate?,
            leadDays: Int?
        ): TransferMoneyCalendarViewModel
    }

    init {
        val days = leadDays
            ?: if (isOriginAccountExternal || isDestinationAccountExternal) {
                LEAD_DAYS_FOR_EXTERNAL_ACCOUNTS
            } else {
                LEAD_DAYS_FOR_INTERNAL_ACCOUNTS
            }

        _viewState.updateValue { it.copy(leadDays = days) }
        fetchData()
    }

    internal fun setSelectedDate(date: LocalDate?) {
        _viewState.updateValue { it.copy(selectedDate = date) }
    }

    internal fun setSelectedPage(page: Int) {
        _viewState.updateValue { it.copy(currentPage = page) }
    }

    internal fun isDateValid(date: LocalDate): Boolean {
        return isDateValid(date, viewState.value?.holidays ?: emptySet())
    }

    private fun isDateValid(date: LocalDate, holidays: Set<LocalDate>): Boolean {
        return isBusinessDay(date, holidays) && isDateValidForExternalAccount(date)
    }

    private fun fetchData() {
        _viewState.updateValue { it.copy(isLoading = true, error = null) }

        viewModelScope.launch {
            transferCalendarUseCase.execute(GetTransferCalendarRequest(currentDate.year))
                .onSuccess { holidays ->
                    _viewState.updateValue {
                        it.copy(
                            isLoading = false,
                            holidays = holidays,
                            selectedDate = getPreSelectedDate(initialDate, holidays)
                        )
                    }
                }
                .onFailure { exception ->
                    logger.error(exception.localizedMessage)
                    _viewState.updateValue {
                        it.copy(
                            isLoading = false,
                            error = ErrorAction(exception, ::fetchData)
                        )
                    }
                }
        }
    }

    private fun getPreSelectedDate(preferred: LocalDate?, holidays: Set<LocalDate>): LocalDate? {
        return preferred ?: getValidDate(currentDate, holidays)
    }

    private fun getValidDate(startDate: LocalDate, holidays: Set<LocalDate>): LocalDate? {
        return startDate
            .takeIf { isDateValid(it, holidays) }
            ?: getValidDate(startDate.plusDays(1L), holidays)
    }

    private fun isBusinessDay(date: LocalDate, holidays: Set<LocalDate>): Boolean {
        return date >= currentDate && !holidays.contains(date)
    }

    private fun isDateValidForExternalAccount(date: LocalDate): Boolean {
        return if (isDestinationAccountExternal || isOriginAccountExternal) {
            date.dayOfWeek != DayOfWeek.SATURDAY && date.dayOfWeek != DayOfWeek.SUNDAY
        } else {
            true
        }
    }
}

internal data class ViewState(
    val isLoading: Boolean = false,
    val leadDays: Int? = null,
    val currentPage: Int? = null,
    val selectedDate: LocalDate? = null,
    val holidays: Set<LocalDate>? = null,
    val error: ErrorAction? = null
)

internal class ErrorAction(
    val error: Throwable? = null,
    val autoRetry: (() -> Unit)? = null
)
