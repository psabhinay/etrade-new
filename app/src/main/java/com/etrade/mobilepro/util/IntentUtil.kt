package com.etrade.mobilepro.util

import android.app.Activity
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.etrade.mobilepro.common.di.EntryPointActivity

fun sendIntentWith(
    context: Context,
    @EntryPointActivity activityClass: Class<out Activity>,
    setProperties: Intent.() -> Unit,
) = with(Intent(context, activityClass)) {
    setProperties()
    val flags = PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
    PendingIntent
        .getActivity(context, 0, this, flags)
        .send()
}
