package com.etrade.mobilepro.orders.extensions

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem.OrderDetailsItem
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.toSecurityType

fun BaseOrderDetailsItem?.getSecurityType(defaultInstrumentType: InstrumentType): SecurityType {
    return if (this == null || this !is OrderDetailsItem || orderDescriptions.count() == 1) {
        defaultInstrumentType.toSecurityType()
    } else {
        getSecurityTypeForMultipleItems()
    }
}

fun OrderDetailsItem.getSecurityTypeForMultipleItems(): SecurityType {
    val instrumentTypes = orderDescriptions.map { it.typeCode }
    val hasOptions = instrumentTypes.any { it.isOption }
    val hasStocks = instrumentTypes.any { !it.isOption }
    return if (hasOptions && hasStocks) {
        SecurityType.STOCK_AND_OPTIONS
    } else if (hasOptions) {
        SecurityType.OPTION
    } else {
        SecurityType.STOCK
    }
}
