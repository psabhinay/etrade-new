package com.etrade.mobilepro.ui_demo.calendar

import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.calendar.CalendarDialog
import com.etrade.mobilepro.calendar.CalendarMode
import com.etrade.mobilepro.ui_demo.R
import com.etrade.mobilepro.ui_demo.databinding.ActivityCalendarBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.jakewharton.threetenabp.AndroidThreeTen
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import kotlin.random.Random

class CalendarActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityCalendarBinding::inflate)

    private val viewModel: CalendarActivityViewModel by lazy {
        ViewModelProvider(this)[CalendarActivityViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidThreeTen.init(this)

        setContentView(binding.root)

        bindViewModel()

        if (savedInstanceState == null) {
            viewModel.setCalendarMode(CalendarMode.TWO_WEEKS)
            viewModel.markPreviousDays(true)

            binding.modeTwoWeeks.isChecked = true
        }

        with(binding.markPreviousDays) {
            isChecked = viewModel.markPreviousDays.value ?: true

            setOnCheckedChangeListener { _, isChecked ->
                viewModel.markPreviousDays(isChecked)
            }
        }

        with(binding.disableWeekends) {
            isChecked = viewModel.disableWeekends.value ?: false

            setOnCheckedChangeListener { _, isChecked ->
                viewModel.disableWeekends(isChecked)
            }
        }

        binding.calendarDialog.setOnClickListener {
            CalendarDialog(this)
                .show(
                    onDateSelected = { date, _ ->
                        Toast.makeText(
                            this,
                            "Selected date: $date",
                            Toast.LENGTH_SHORT
                        ).show()
                    },
                    clickableDaysPredicate = {
                        it >= LocalDate.now() && it.dayOfWeek != DayOfWeek.SATURDAY && it.dayOfWeek != DayOfWeek.SUNDAY
                    }
                )
        }

        binding.calendarDialogWithMessage.setOnClickListener {
            CalendarDialog(this)
                .show(
                    onDateSelected = { date, _ ->
                        Toast.makeText(
                            this,
                            "Selected date: $date",
                            Toast.LENGTH_SHORT
                        ).show()
                    },
                    clickableDaysPredicate = {
                        it >= LocalDate.now() && it.dayOfWeek != DayOfWeek.SATURDAY && it.dayOfWeek != DayOfWeek.SUNDAY
                    },
                    message = "Please Note: It will take 3 business days for this payee to receive the payment. Please select the schedule date keeping this in mind."
                )
        }

        with(binding.events) {
            isChecked = viewModel.showEvents.value ?: false

            setOnCheckedChangeListener { _, isChecked ->
                viewModel.showEvents(isChecked)
            }
        }

        binding.calendar.onDateSelected = {
            Toast.makeText(this, "Selected date: $it", Toast.LENGTH_SHORT).show()
        }
    }

    fun onModeChangeClicked(view: View) {
        if (view is RadioButton) {
            viewModel.setCalendarMode(
                when (view.id) {
                    R.id.mode_two_weeks -> CalendarMode.TWO_WEEKS
                    R.id.mode_month -> CalendarMode.MONTH
                    else -> throw IllegalArgumentException("Unknown calendar mode")
                }
            )
        }
    }

    private fun bindViewModel() {
        viewModel.calendarMode.observe(this, Observer {
            binding.calendar.updateCalendarMode(it)
        })

        viewModel.markPreviousDays.observe(this, Observer {
            binding.calendar.disabledDaysPredicate = { date ->
                it && date < LocalDate.now()
            }
        })

        viewModel.disableWeekends.observe(this, Observer {
            binding.calendar.clickableDaysPredicate = { date ->
                if (it) {
                    listOf(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY).all { it != date.dayOfWeek }
                } else {
                    true
                }
            }
        })

        viewModel.events.observe(this, Observer {
            binding.calendar.updateCalendarEvents(it)
        })
    }
}

class CalendarActivityViewModel : ViewModel() {

    private val _calendarMode: MutableLiveData<CalendarMode> = MutableLiveData()
    val calendarMode: LiveData<CalendarMode>
        get() = _calendarMode

    private val _markPreviousDays: MutableLiveData<Boolean> = MutableLiveData()
    val markPreviousDays: LiveData<Boolean>
        get() = _markPreviousDays

    private val _showEvents: MutableLiveData<Boolean> = MutableLiveData()
    val showEvents: LiveData<Boolean>
        get() = _showEvents

    private val _disableWeekends: MutableLiveData<Boolean> = MutableLiveData()
    val disableWeekends: LiveData<Boolean>
        get() = _disableWeekends

    val events: LiveData<Set<LocalDate>> = Transformations.switchMap(showEvents) {
        liveData(context = viewModelScope.coroutineContext) {
            if (it) {
                emit((List(100) { LocalDate.now().plusDays(Random.nextLong(-100, 100)) }).toSet())
            } else {
                emit(emptySet<LocalDate>())
            }
        }
    }

    fun setCalendarMode(mode: CalendarMode) {
        _calendarMode.value = mode
    }

    fun markPreviousDays(value: Boolean) {
        _markPreviousDays.value = value
    }

    fun disableWeekends(value: Boolean) {
        _disableWeekends.value = value
    }

    fun showEvents(value: Boolean) {
        _showEvents.value = value
    }
}
