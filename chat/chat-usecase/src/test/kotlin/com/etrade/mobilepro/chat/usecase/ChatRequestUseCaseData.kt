package com.etrade.mobilepro.chat.usecase

import com.etrade.mobilepro.chat.api.ChatMetadataModel
import com.etrade.mobilepro.chat.api.message.ChatRetrieveMessageType
import com.etrade.mobilepro.chat.api.message.models.ChatForMobileModel
import com.etrade.mobilepro.chat.api.message.models.ChatInfoModel
import com.etrade.mobilepro.chat.api.message.models.ChatRequestModel
import com.etrade.mobilepro.chat.api.message.models.ChatRetrieveMessageItemModel
import com.etrade.mobilepro.chat.api.message.models.ChatRetrievedMessageDataModel
import com.etrade.mobilepro.chat.api.message.models.ChatRetrievedMessageModel
import com.etrade.mobilepro.chat.api.message.models.MessageModel
import com.etrade.mobilepro.chat.api.message.models.MobileChatDataModel

internal const val TEST_BUTTON_ID = "TEST_BUTTON_ID"
internal const val TEST_SESSION_KEY = "TEST_SESSION_KEY"
internal const val TEST_MESSAGES = "TEST_MESSAGES"
internal const val TEST_SESSION_ID = "TEST_SESSION_ID"
internal const val TEST_TEXT_MESSAGE = "TEST_TEXT_MESSAGE"
internal const val TEST_ESTIMATED_WAIT_TIME = 1234

internal val chatTestMetadataModel = ChatMetadataModel(buttonId = TEST_BUTTON_ID, sessionKey = TEST_SESSION_KEY)
internal val chatTestMessagesData = ChatRequestModel.GetMessages(TEST_SESSION_ID, chatTestMetadataModel)
internal val chatTestPostMessageData = ChatRequestModel.PostMessage(TEST_SESSION_ID, chatTestMetadataModel, TEST_TEXT_MESSAGE)
internal val chatTestTerminateData = ChatRequestModel.TerminateChat(TEST_SESSION_ID, chatTestMetadataModel)

internal val chatTestForMobileModel = ChatForMobileModel(
    data = MobileChatDataModel(
        chatInfoModel = ChatInfoModel(TEST_SESSION_ID),
        messages = TEST_MESSAGES
    ),
    success = true
)

internal val chatRetrievedMessageTextModel = ChatRetrievedMessageModel(
    data = ChatRetrievedMessageDataModel(
        listOf(
            ChatRetrieveMessageItemModel(ChatRetrieveMessageType.ChatMessage, MessageModel(text = TEST_TEXT_MESSAGE))
        )
    ),
    success = true,
    sequence = 1
)

internal val chatRetrievedMessageQueueUpdateModel = ChatRetrievedMessageModel(
    data = ChatRetrievedMessageDataModel(
        listOf(
            ChatRetrieveMessageItemModel(ChatRetrieveMessageType.QueueUpdate, MessageModel(estimatedWaitTime = TEST_ESTIMATED_WAIT_TIME))
        )
    ),
    success = true,
    sequence = 1
)

internal val chatRetrievedMessageAgentTypingModel = ChatRetrievedMessageModel(
    data = ChatRetrievedMessageDataModel(
        listOf(
            ChatRetrieveMessageItemModel(ChatRetrieveMessageType.AgentTyping, MessageModel())
        )
    ),
    success = true,
    sequence = 1
)

internal val chatRetrievedMessageAgentNotTypingModel = ChatRetrievedMessageModel(
    data = ChatRetrievedMessageDataModel(
        listOf(
            ChatRetrieveMessageItemModel(ChatRetrieveMessageType.AgentNotTyping, MessageModel())
        )
    ),
    success = true,
    sequence = 1
)

internal val chatRetrievedMessageChatEndedModel = ChatRetrievedMessageModel(
    data = ChatRetrievedMessageDataModel(
        listOf(
            ChatRetrieveMessageItemModel(ChatRetrieveMessageType.ChatEnded, MessageModel())
        )
    ),
    success = true,
    sequence = 1
)

internal val chatRetrievedMessageFailedModel = ChatRetrievedMessageModel(
    data = ChatRetrievedMessageDataModel(
        listOf(
            ChatRetrieveMessageItemModel(ChatRetrieveMessageType.ChatRequestFail, MessageModel(reason = "reason"))
        )
    ),
    success = true,
    sequence = 1
)
