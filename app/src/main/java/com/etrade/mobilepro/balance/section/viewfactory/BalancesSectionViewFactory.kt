package com.etrade.mobilepro.balance.section.viewfactory

import com.etrade.mobilepro.balance.data.dto.BalancesDetailsDto
import com.etrade.mobilepro.balance.section.BalancesActionType
import com.etrade.mobilepro.balance.section.BalancesLabelResources
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

interface BalancesSectionViewFactory {
    fun createSectionView(
        detailsDto: BalancesDetailsDto,
        resources: BalancesLabelResources,
        actionClickListener: (BalancesActionType) -> Unit
    ): List<TableSectionItemLayout>
}
