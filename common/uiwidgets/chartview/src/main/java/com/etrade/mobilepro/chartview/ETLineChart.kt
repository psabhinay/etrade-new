package com.etrade.mobilepro.chartview

import android.content.Context
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import androidx.core.content.withStyledAttributes
import com.github.mikephil.charting.charts.LineChart

private const val EXTRA_BOTTOM_OFFSET = 10f

class ETLineChart @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LineChart(context, attrs, defStyleAttr) {

    init {
        description = null
        isFocusable = true
        legend.isEnabled = false
        setTouchEnabled(true)
        setPinchZoom(false)
        isDoubleTapToZoomEnabled = false
        setScaleEnabled(false)
        isDragEnabled = false
        isHighlightPerDragEnabled = false
        extraBottomOffset = EXTRA_BOTTOM_OFFSET
        setNoDataTextColor(ContextCompat.getColor(context, R.color.textColorPrimary))
        context.withStyledAttributes(attrs, R.styleable.ETLineChart) {
            getString(R.styleable.ETLineChart_noDataText)?.let {
                setNoDataText(it)
            }
        }
    }
}
