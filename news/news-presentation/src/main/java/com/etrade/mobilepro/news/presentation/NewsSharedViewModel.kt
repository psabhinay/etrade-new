package com.etrade.mobilepro.news.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.news.api.filter.FilterDataSource
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsType
import com.etrade.mobilepro.news.presentation.filter.portfolio.PortfolioFilterItem
import com.etrade.mobilepro.session.api.SessionStateChange
import com.etrade.mobilepro.usersession.api.UserSessionStateProvider
import com.etrade.mobilepro.watchlistapi.Watchlist
import javax.inject.Inject

class NewsSharedViewModel @Inject constructor(
    val portfolioFilterDataSource: FilterDataSource<PortfolioFilterItem>,
    val watchlistFilterDataSource: FilterDataSource<Watchlist>,
    userSessionStateProvider: UserSessionStateProvider
) : ViewModel() {

    private val _savedSelectedTextNewsItem = MutableLiveData<NewsItem.Text?>()
    private val _activeTab = MutableLiveData<NewsType>()
    val activeTab: LiveData<NewsType> = _activeTab
    val savedSelectedNewsItem: LiveData<NewsItem.Text?> = _savedSelectedTextNewsItem
    val authState: LiveData<Boolean> = Transformations.map(userSessionStateProvider.userSessionState) {
        it.peekContent() is SessionStateChange.AUTHENTICATED
    }
    var passedInSymbol: String? = null

    var isCompanyTabOpenedFirstTime: Boolean = false

    fun saveSelectedNewsItem(item: NewsItem.Text) {
        _savedSelectedTextNewsItem.value = item
    }

    fun didSelectTab(type: NewsType) {
        _activeTab.value = type
    }

    fun clear() {
        _savedSelectedTextNewsItem.value = null
        portfolioFilterDataSource.clear()
        watchlistFilterDataSource.clear()
    }
}
