package com.etrade.mobilepro.tracking

class ViewPagerTabsTrackerHelperImpl(private val tracker: Tracker) : ViewPagerTabsTrackerHelper {

    override var lastTabPosition: Int = -1

    override fun trackTab(position: Int, tabTitle: String) {
        if (lastTabPosition != position) {
            tracker.screen(tabTitle)
        }

        lastTabPosition = position
    }
}
