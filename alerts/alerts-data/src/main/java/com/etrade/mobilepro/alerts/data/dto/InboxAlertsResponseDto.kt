package com.etrade.mobilepro.alerts.data.dto

import com.etrade.mobilepro.util.json.SingleToArray
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class InboxAlertsResponseDto(
    @SingleToArray
    @Json(name = "events")
    var events: List<InboxEventDto?>? = null
)

@JsonClass(generateAdapter = true)
class InboxEventDto(
    @Json(name = "EventIdentifier")
    var id: String? = null,
    @Json(name = "Read")
    var wasRead: String? = null,
    @Json(name = "Occurred")
    var occurredAtEpochTimestamp: Long? = null,
    @Json(name = "Subject")
    var subject: String? = null,
    @Json(name = "Type")
    var alertCode: String? = null
)
