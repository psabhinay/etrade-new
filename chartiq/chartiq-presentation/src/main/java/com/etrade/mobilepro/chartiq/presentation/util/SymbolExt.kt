package com.etrade.mobilepro.chartiq.presentation.util

import android.os.Bundle
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.searchingapi.items.SearchResultItem

private const val KEY_SYMBOL = "SYMBOL"

internal fun SearchResultItem.Symbol.toBundle() = Bundle().apply {
    putParcelable(KEY_SYMBOL, this@toBundle)
}

internal fun Bundle.getSymbol(): SearchResultItem.Symbol? = getParcelable(KEY_SYMBOL)

/**
 * Since PChart doesn't support OPTNC/OPTNP types, pass OPTN manually when instrument type is Option
 */
internal fun SearchResultItem.Symbol.getPchartCompatibleInstrumentType() = if (type.isOption) {
    InstrumentType.OPTN
} else {
    type
}
