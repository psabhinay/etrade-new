package com.etrade.mobilepro.quote.screen.presentation

import androidx.annotation.ColorRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.chart.model.ChartLineData
import com.etrade.mobilepro.chart.model.ChartTimeFrame
import com.etrade.mobilepro.chart.util.getChartLineDataWithDisplayDto
import com.etrade.mobilepro.chartengine.ChartRepo
import com.etrade.mobilepro.chartengine.ChartRequest
import com.etrade.mobilepro.chartengine.SymbolChartData
import com.etrade.mobilepro.common.di.PrimaryColor
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import org.slf4j.LoggerFactory
import javax.inject.Inject

private val logger = LoggerFactory.getLogger(QuoteChartViewModel::class.java)

class QuoteChartViewModel @Inject constructor(
    private val chartRepo: ChartRepo,
    @ColorRes @PrimaryColor private val chartColor: Int
) : ViewModel() {
    private val compositeDisposable = CompositeDisposable()

    private val mutableLineData: MutableLiveData<ChartLineData> = MutableLiveData()
    val lineData: LiveData<ChartLineData> = mutableLineData
    private val mutableShouldShowChart: MutableLiveData<Boolean> = MutableLiveData()
    val shouldShowChart: LiveData<Boolean> = mutableShouldShowChart
    private val mutableIsChartLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isChartLoading: LiveData<Boolean> = mutableIsChartLoading
    private val mutableIsRetryShowing: MutableLiveData<Boolean> = MutableLiveData()
    val isRetryShowing: LiveData<Boolean> = mutableIsRetryShowing
    private val mutableShowChartIq: MutableLiveData<ConsumableLiveEvent<Boolean>> = MutableLiveData()
    val showChartIq: LiveData<ConsumableLiveEvent<Boolean>> = mutableShowChartIq
    private val quoteSubject: Subject<SearchResultItem.Symbol> = PublishSubject.create<SearchResultItem.Symbol>()
    private var selectedTimeFrame: ChartTimeFrame = ChartTimeFrame.OneYear
    private var currentSymbol: SearchResultItem.Symbol? = null

    init {
        initChart()
    }

    fun onChartClicked() {
        mutableShowChartIq.postValue(ConsumableLiveEvent(true))
    }

    fun updateSymbol(symbol: SearchResultItem.Symbol) {
        currentSymbol = symbol
        onRetryClick()
    }

    fun onRetryClick() {
        initChart()
        currentSymbol?.let { quoteSubject.onNext(it) }
    }

    private fun initChart() {
        mutableIsChartLoading.postValue(true)
        mutableIsRetryShowing.postValue(false)
        compositeDisposable.clear()
        compositeDisposable.add(
            quoteSubject
                .flatMap { symbol ->
                    val isOption = symbol.type.isOption
                    selectedTimeFrame = if (isOption) {
                        ChartTimeFrame.OneMonth
                    } else {
                        ChartTimeFrame.OneYear
                    }
                    chartRepo.getChart(ChartRequest(symbol.title, timeFrame = selectedTimeFrame.timeFrame, isOption = isOption))
                }
                .subscribeBy(
                    onNext = { response ->
                        handleResponse(response)
                    },
                    onError = {
                        logger.error("Get quote chart failed", it)
                        clearChart()
                    }
                )
        )
    }

    private fun handleResponse(response: Resource<SymbolChartData>) {
        when (response) {
            is Resource.Success -> {
                val isOption = currentSymbol?.type?.isOption == true
                val lineData = ChartLineData(selectedTimeFrame, false, isOption)
                response.data?.let {
                    currentSymbol?.let { symbol ->
                        handleSymbol(it, lineData, symbol, isOption)
                    }
                }
                mutableLineData.postValue(lineData)
                mutableIsChartLoading.postValue(false)
            }
            is Resource.Failed -> {
                mutableLineData.postValue(null)
                mutableIsRetryShowing.postValue(true)
                mutableIsChartLoading.postValue(false)
                mutableShouldShowChart.postValue(true)
            }
        }
    }

    private fun handleSymbol(
        symbolData: SymbolChartData,
        lineData: ChartLineData,
        symbol: SearchResultItem.Symbol,
        isOption: Boolean
    ) {
        if (symbolData.displayData.isNotEmpty()) {
            mutableShouldShowChart.postValue(true)
            lineData.addLineSet(getChartLineDataWithDisplayDto(symbol.title, chartColor, symbolData, selectedTimeFrame, false, isOption))
        } else {
            mutableShouldShowChart.postValue(false)
        }
    }

    private fun clearChart() {
        mutableLineData.postValue(null)
        mutableIsChartLoading.postValue(false)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }
}
