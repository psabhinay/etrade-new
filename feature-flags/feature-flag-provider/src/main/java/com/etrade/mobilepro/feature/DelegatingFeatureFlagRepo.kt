package com.etrade.mobilepro.feature

import androidx.annotation.VisibleForTesting
import com.etrade.mobilepro.feature.qualifiers.AvailableFeatureFlagProviders
import com.etrade.mobilepro.feature.qualifiers.AvailableFeatures
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

/**
 * Combines a list of [FeatureFlagProvider]s to provide values.
 */
interface DelegatingFeatureFlagRepo : FeatureFlagRepo {
    val providers: List<FeatureFlagProvider>
}

/**
 * A [DelegatingFeatureFlagRepo] in which the providers are evaluated from first to last,
 * and the feature value from the first provider that has the feature is returned, or the default
 * value if no provider has the feature.
 */
@ExperimentalCoroutinesApi
class DefaultDelegatingFeatureFlagRepo(
    override val providers: List<FeatureFlagProvider>,
    private val dispatcher: CoroutineDispatcher,
    private val availableFeatures: List<Feature>
) : DelegatingFeatureFlagRepo {

    @Inject
    constructor(
        @AvailableFeatureFlagProviders
        providers: List<@JvmSuppressWildcards FeatureFlagProvider>,
        @AvailableFeatures
        features: List<@JvmSuppressWildcards Feature>
    ) : this(providers, Dispatchers.IO, features)

    /**
     * Emits the enabled status of the feature for the highest-priority provider that has the feature.
     */
    override fun isFeatureEnabled(feature: Feature): Flow<Boolean> =
        findProvider(feature)
            .flatMapLatest { it?.isFeatureEnabled(feature) ?: flowOf(feature.defaultValue) }
            .distinctUntilChanged()
            .flowOn(dispatcher)

    /**
     * Reactively finds the first provider that has the given feature. If a higher-priority provider
     * changes state, then a different provider will be emitted.
     */
    @VisibleForTesting
    internal fun findProvider(feature: Feature, startAt: Int = 0): Flow<FeatureFlagProvider?> {
        val providerFlow = providers.getOrNull(startAt)?.let { provider ->
            provider.hasFeature(feature).flatMapLatest { hasFeature ->
                if (hasFeature) {
                    if (provider is FeatureFlagStore && !availableFeatures.contains(feature)) {
                        provider.removeFeature(feature)
                        flowOf(null)
                    } else {
                        flowOf(provider)
                    }
                } else {
                    findProvider(feature, startAt + 1)
                }
            }
        } ?: flowOf(null)
        return providerFlow.distinctUntilChanged()
    }
}
