package com.etrade.mobilepro.walkthrough.api

enum class WalkthroughScenario {
    SYMBOL_LOOKUP,
    SYMBOL_TRACKING,
    TRADE_PLACING
}
