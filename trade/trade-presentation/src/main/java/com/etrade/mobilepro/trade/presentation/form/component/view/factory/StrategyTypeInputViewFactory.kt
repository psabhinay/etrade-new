package com.etrade.mobilepro.trade.presentation.form.component.view.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.ui.factory.ComponentViewFactory
import com.etrade.mobilepro.trade.presentation.form.component.StrategyTypeInput
import com.etrade.mobilepro.trade.presentation.form.component.view.StrategyTypeInputView

internal class StrategyTypeInputViewFactory : ComponentViewFactory<StrategyTypeInput, StrategyTypeInputView> {
    override fun createView(context: Context): StrategyTypeInputView = StrategyTypeInputView(context)
}
