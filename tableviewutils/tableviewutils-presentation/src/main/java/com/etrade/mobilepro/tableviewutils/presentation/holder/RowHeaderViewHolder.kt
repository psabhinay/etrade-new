package com.etrade.mobilepro.tableviewutils.presentation.holder

import android.view.View
import android.widget.TextView
import com.etrade.mobilepro.tableviewutils.presentation.R
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder

class RowHeaderViewHolder(itemView: View) : AbstractViewHolder(itemView) {
    val rowHeaderTextView: TextView = itemView.findViewById<View>(R.id.txtRowHeader) as TextView
}
