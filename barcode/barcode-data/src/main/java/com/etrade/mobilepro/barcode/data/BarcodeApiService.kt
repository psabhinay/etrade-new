package com.etrade.mobilepro.barcode.data

import com.etrade.mobilepro.util.json.JsonMoshi
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface BarcodeApiService {

    /**
     * Api to get barcode info via HTTP Get method
     * @param upc Decoded barcode number from image.
     * @param format Barcode format, for app we only support UPC-A and UPC-E
     */
    @JsonMoshi
    @GET("/app/barcode-lookup/v1/lookup/lookup.json")
    fun barcodeLookup(@Query("upc") upc: String, @Query("format") format: String): Single<BarcodeLookupResponse>
}
