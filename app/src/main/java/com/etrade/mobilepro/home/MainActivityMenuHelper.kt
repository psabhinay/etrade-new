package com.etrade.mobilepro.home

import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.OnNavigateUpListener
import com.etrade.mobilepro.home.model.MainActivityViewModel
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchType

class MainActivityMenuHelper(
    private val inflater: MenuInflater,
    private val navController: NavController,
    private val mainActivityViewModel: MainActivityViewModel,
    private val fragmentManager: FragmentManager,
    private val navigationUpListener: NavigationUpListener
) {

    private var searchMenu: MenuItem? = null

    var isSearchVisible = false
        set(value) {
            if (field == value) return
            field = value
            searchMenu?.isVisible = value
        }

    fun interface NavigationUpListener {
        fun onNavigationUp()
    }

    fun inflate(menu: Menu): Boolean {
        inflater.inflate(R.menu.options_menu_main, menu)

        searchMenu = menu.findItem(R.id.menu_quote_lookup)?.apply {
            isVisible = isSearchVisible
        }
        return true
    }

    fun onOptionsItemSelected(item: MenuItem, chain: (MenuItem) -> Boolean): Boolean {
        return when (item.itemId) {
            R.id.menu_quote_lookup -> {
                mainActivityViewModel.openSearchActivity(
                    SearchQuery(SearchType.SYMBOL),
                    simpleSymbolSearch = false,
                    quoteLookupLocation = navController.currentDestination?.label?.toString()
                )
                false
            }
            android.R.id.home -> {
                val currentFragment = fragmentManager.getCurrentNavigationFragment()
                if (currentFragment is OnNavigateUpListener && currentFragment.onNavigateUp()) {
                    return true
                }
                navigationUpListener.onNavigationUp()
                navController.navigateUp()
            }
            else -> chain(item)
        }
    }
}
