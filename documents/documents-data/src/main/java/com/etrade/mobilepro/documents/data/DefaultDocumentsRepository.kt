package com.etrade.mobilepro.documents.data

import com.etrade.mobilepro.ServicePath
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.common.di.RemoteDynamicScreen
import com.etrade.mobilepro.documents.DocumentsRepository
import com.etrade.mobilepro.documents.StatementDocument
import com.etrade.mobilepro.documents.StatementDocumentsRequest
import com.etrade.mobilepro.documents.TaxDocument
import com.etrade.mobilepro.documents.TaxDocumentsRequest
import com.etrade.mobilepro.dynamic.screen.data.source.ScreenDataSource
import com.etrade.mobilepro.util.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.rx2.asFlow
import org.threeten.bp.Duration
import javax.inject.Inject

private val CACHE_EXPIRATION = Duration.ofDays(1).toMillis()
private const val AMEND = "(Amended)"

class DefaultDocumentsRepository @Inject constructor(
    @RemoteDynamicScreen private val screenDataSource: ScreenDataSource,
    private val applicationInfo: ApplicationInfo
) : DocumentsRepository {

    @ExperimentalCoroutinesApi
    override fun getStatementDocuments(request: StatementDocumentsRequest): Flow<Resource<List<StatementDocument>>> {
        return screenDataSource.getScreen(
            request = ServicePath.StatementDocuments(
                request.accountUuid,
                request.startDate,
                request.endDate,
                applicationInfo.appVersionName()
            ),
            cacheExpiration = CACHE_EXPIRATION
        )
            .map {
                it.map { response ->
                    response?.screen?.references
                        ?.asSequence()
                        ?.filterIsInstance<StatementDocumentsReferenceDto>()
                        ?.first()
                        ?.convertToModel()
                }
            }
            .asFlow()
    }

    @ExperimentalCoroutinesApi
    override fun getTaxDocuments(request: TaxDocumentsRequest): Flow<Resource<List<TaxDocument>>> {
        return screenDataSource.getScreen(
            request = ServicePath.TaxDocuments(request.accountUuid, request.year, applicationInfo.appVersionName()),
            cacheExpiration = CACHE_EXPIRATION
        )
            .map {
                it.map { response ->
                    response?.screen?.references
                        ?.asSequence()
                        ?.filterIsInstance<TaxDocumentsReferenceDto>()
                        ?.firstOrNull()
                        ?.convertToModel()
                        ?.map { model ->
                            model.copy(
                                headerTitle = DOCUMENTS_DATE_FORMATTER_FOR_UI.format(model.reportDate).let { date ->
                                    if (model.amended) {
                                        "$date $AMEND"
                                    } else {
                                        date
                                    }
                                }
                            )
                        }
                }
            }
            .asFlow()
    }
}
