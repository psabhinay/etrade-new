package com.etrade.mobilepro.stockplan

import com.etrade.mobile.accounts.dto.AccountType

interface AccountTypeHandler {

    fun getAccountType(): AccountType?
}
