package com.etrade.mobilepro.alerts.presentation.setalert.setvalue

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.map
import com.etrade.mobilepro.alerts.api.model.AlertValueType
import com.etrade.mobilepro.alerts.api.model.NewsAlertValueType
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.livedata.combineLatest

interface UserInputValidator {
    fun userInputIsValid(
        inputValues: Map<AlertValueType, LiveData<String>>,
        switchValues: Map<NewsAlertValueType, LiveData<Boolean>>,
        inputValueDepsProvider: AlertInputValueDependenciesProvider
    ): LiveData<Boolean>
}

private val lowHighPairs = listOf(
    Pair(AlertValueType.LOW_PE_TARGET, AlertValueType.HIGH_PE_TARGET),
    Pair(AlertValueType.LOW_TARGET_PRICE, AlertValueType.HIGH_TARGET_PRICE),
    Pair(AlertValueType.LOW_DAILY_PERCENT, AlertValueType.HIGH_DAILY_PERCENT)
)

class DefaultUserInputValidator : UserInputValidator {

    override fun userInputIsValid(
        inputValues: Map<AlertValueType, LiveData<String>>,
        switchValues: Map<NewsAlertValueType, LiveData<Boolean>>,
        inputValueDepsProvider: AlertInputValueDependenciesProvider
    ): LiveData<Boolean> {
        return validateInputValues(inputValues, inputValueDepsProvider)
            .combineLatest(validateSwitchValues(switchValues))
            .map {
                it.first && (it.second || hasNonEmpties(inputValues))
            }
    }

    private fun validateInputValues(
        inputValues: Map<AlertValueType, LiveData<String>>,
        inputValueDepsProvider: AlertInputValueDependenciesProvider
    ): LiveData<Boolean> {
        return MediatorLiveData<Boolean>().apply {
            value = false
            AlertValueType.values().forEach {
                addSource(inputValues.getValue(it)) {
                    value = everyFieldHasValidState(inputValues, inputValueDepsProvider) && lowValuesAreLowerThanHighValues(inputValues)
                }
            }
        }
    }

    private fun validateSwitchValues(
        switchValues: Map<NewsAlertValueType, LiveData<Boolean>>
    ): LiveData<Boolean> {
        return MediatorLiveData<Boolean>().apply {
            value = false
            NewsAlertValueType.values().forEach {
                addSource(switchValues.getValue(it)) {
                    value = atLeastOneIsOn(switchValues)
                }
            }
        }
    }

    private fun everyFieldHasValidState(
        inputValues: Map<AlertValueType, LiveData<String>>,
        inputValueDepsProvider: AlertInputValueDependenciesProvider
    ): Boolean {
        AlertValueType.values().forEach {
            if (inputValueDepsProvider.getDependencies(it).validator.validate(inputValues[it]?.value ?: "") !is InputFieldManager.Value.Valid) {
                return false
            }
        }
        return true
    }

    private fun lowValuesAreLowerThanHighValues(inputValues: Map<AlertValueType, LiveData<String>>): Boolean {
        lowHighPairs.forEach { lowHighPair ->
            val lowValue = inputValues[lowHighPair.first]?.value ?: ""
            val highValue = inputValues[lowHighPair.second]?.value ?: ""
            if (lowValue.isNotEmpty() && highValue.isNotEmpty()) {
                val low = lowValue.toDoubleOrNull() ?: 0.0
                val high = highValue.toDoubleOrNull() ?: 0.0
                if (low > high) {
                    return false
                }
            }
        }
        return true
    }

    private fun hasNonEmpties(inputValues: Map<AlertValueType, LiveData<String>>): Boolean {
        AlertValueType.values().forEach {
            if ((inputValues[it]?.value ?: "").isNotBlank()) {
                return true
            }
        }
        return false
    }

    private fun atLeastOneIsOn(switchValues: Map<NewsAlertValueType, LiveData<Boolean>>): Boolean {
        NewsAlertValueType.values().forEach {
            if (switchValues[it]?.value == true) {
                return true
            }
        }
        return false
    }
}
