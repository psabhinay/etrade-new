package com.etrade.mobilepro.trade.calculator

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.etrade.mobilepro.common.Navigation
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.common.getNavigationButtonDescription
import com.etrade.mobilepro.common.setupToolbar
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.trade.calculator.databinding.TradeCalculatorFragmentBinding
import com.etrade.mobilepro.trade.presentation.util.SharedTradeViewModel
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.binding.dataBinding
import com.etrade.mobilepro.util.android.consume
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import javax.inject.Inject

internal const val KEY_PRICE_VALUE = "KEY_PRICE_VALUE"
internal const val KEY_AMOUNT_VALUE = "KEY_AMOUNT_VALUE"
internal const val KEY_PRICE_TYPE = "KEY_PRICE_TYPE"
private const val KEY_SYMBOL = "KEY_SYMBOL"

@RequireLogin
class TradeCalculatorFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    tradeRouter: TradeRouter
) : BottomSheetDialogFragment() {

    private val binding by dataBinding(TradeCalculatorFragmentBinding::bind)

    private val quotesWidgetViewModel: QuotesWidgetViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }
    private val sharedTradeViewModel: SharedTradeViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }
    private val tradeCalculatorViewModel: TradeCalculatorViewModel by viewModels { viewModelFactory }
    private var initialRestoredData: Bundle? = null

    override fun getTheme() = R.style.BottomSheetDialogTheme

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.trade_calculator_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        initialRestoredData = savedInstanceState

        with(binding) {
            quotesViewModel = quotesWidgetViewModel
            viewModel = tradeCalculatorViewModel

            amountInputLayout.setEditTextListeners()
            priceInputLayout.setEditTextListeners()
        }

        initToolbar()

        Transformations.switchMap(quotesWidgetViewModel.quote) { it?.lastPrice }.observe(viewLifecycleOwner) {
            tradeCalculatorViewModel.refresh(savedInstanceState, it.price)
            if (savedInstanceState != null && quotesWidgetViewModel.quote.value == null) {
                quotesWidgetViewModel.getQuotes(savedInstanceState.getString(KEY_SYMBOL))
            }
        }

        tradeCalculatorViewModel.applyFormParametersSignal.observe(viewLifecycleOwner, Observer { onApplyFormParametersSignal(it) })
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener {
            val bottomSheetDialog = dialog as BottomSheetDialog
            val bottomSheet = bottomSheetDialog.findViewById<FrameLayout>(com.google.android.material.R.id.design_bottom_sheet)!!
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            val layoutParams: CoordinatorLayout.LayoutParams = CoordinatorLayout.LayoutParams(
                CoordinatorLayout.LayoutParams.MATCH_PARENT,
                CoordinatorLayout.LayoutParams.MATCH_PARENT
            )
            layoutParams.behavior = bottomSheetBehavior
            bottomSheet.layoutParams = layoutParams
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        return dialog
    }

    override fun onSaveInstanceState(outState: Bundle) {
        with(outState) {
            if (tradeCalculatorViewModel.wasUpdated) {
                putString(KEY_AMOUNT_VALUE, tradeCalculatorViewModel.amount.value)
                putString(KEY_PRICE_VALUE, tradeCalculatorViewModel.price.value)
                putSerializable(KEY_PRICE_TYPE, tradeCalculatorViewModel.priceType)
                putString(KEY_SYMBOL, quotesWidgetViewModel.quote.value?.symbol)
            } else {
                putString(KEY_AMOUNT_VALUE, initialRestoredData?.getString(KEY_AMOUNT_VALUE))
                putString(KEY_PRICE_VALUE, initialRestoredData?.getString(KEY_PRICE_VALUE))
                putSerializable(KEY_PRICE_TYPE, initialRestoredData?.getSerializable(KEY_PRICE_TYPE))
                putString(KEY_SYMBOL, initialRestoredData?.getString(KEY_SYMBOL))
            }
        }
        super.onSaveInstanceState(outState)
        tradeCalculatorViewModel.wasUpdated = false
    }

    private fun onApplyFormParametersSignal(event: ConsumableLiveEvent<InputValuesSnapshot>) {
        event.consume {
            sharedTradeViewModel.injectFormInputValues(it)
            findNavController().popBackStack()
        }
    }

    private fun initToolbar() {
        setupToolbar(
            navigation = Navigation(
                icon = R.drawable.ic_close,
                onClick = View.OnClickListener {
                    dismiss()
                },
                contentDescription = context?.getNavigationButtonDescription(R.drawable.ic_close)
            ),
            title = getString(R.string.trade_calculator_title),
            actionEnd = ToolbarActionEnd(
                text = R.string.apply,
                onClick = View.OnClickListener {
                    tradeCalculatorViewModel.apply(sharedTradeViewModel.securityType)
                }
            )
        )
        tradeCalculatorViewModel.isValidQuantity().observe(viewLifecycleOwner) { toolbarActionEndView?.isEnabled = it }
    }
}
