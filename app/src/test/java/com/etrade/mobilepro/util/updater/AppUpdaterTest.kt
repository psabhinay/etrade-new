package com.etrade.mobilepro.util.updater

import android.database.sqlite.SQLiteDatabase
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.util.DatabaseLocator
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import java.io.File

private const val DATABASE_NAME = "EtmpDatabase.db"

internal val databaseFile = File(DATABASE_NAME)

private val isDatabaseExists: Boolean
    get() = databaseFile.exists()

@RunWith(AndroidJUnit4::class)
@Config(application = android.app.Application::class)
class AppUpdaterTest {

    private val databaseLocator: DatabaseLocator = object : DatabaseLocator {
        override fun getDatabasePath(name: String): File = databaseFile
    }

    @Test
    fun `test database not exists`() {
        val migrations = setOf(
            MigrationImpl(),
            MigrationImpl(),
            MigrationImpl()
        )

        createAppUpdater(migrations).update()

        migrations.forEach {
            assertNull(it.isSuccessful)
        }
    }

    @Test
    fun `test successful migration`() = withExistingDatabaseFile {
        val migrations = setOf(
            MigrationImpl(),
            MigrationImpl(),
            MigrationImpl()
        )

        createAppUpdater(migrations).update()

        migrations.forEach {
            val result = it.isSuccessful
            assertNotNull(result)
            assertTrue(result ?: false)
        }
    }

    @Test
    fun `test migrations failed`() = withExistingDatabaseFile {
        val migrations = setOf(
            MigrationImpl(true),
            MigrationImpl(true),
            MigrationImpl(true)
        )

        createAppUpdater(migrations).update()

        migrations.forEach {
            val result = it.isSuccessful
            assertNotNull(result)
            assertFalse(result ?: true)
        }
    }

    @Test
    fun `test single migration failed`() = withExistingDatabaseFile {
        val migrations = setOf(
            MigrationImpl(true),
            MigrationImpl(false),
            MigrationImpl(true)
        )

        createAppUpdater(migrations).update()

        migrations.forEach {
            val result = it.isSuccessful
            if (it.throwException) {
                assertNotNull(result)
                assertFalse(result ?: true)
            } else {
                assertNotNull(result)
                assertTrue(result ?: false)
            }
        }
    }

    private fun createAppUpdater(migrations: Set<Migration>): AppUpdater = DefaultAppUpdater(mock(), databaseLocator, mock(), migrations, mock())

    private class MigrationImpl(val throwException: Boolean = false) : Migration {

        var isSuccessful: Boolean? = null

        override suspend fun migrate(databaseFile: File) {
            if (throwException) {
                isSuccessful = false
                throw RuntimeException()
            } else {
                isSuccessful = true
            }
        }
    }
}

internal fun withExistingDatabaseFile(prepare: (SQLiteDatabase) -> Unit = {}, onReady: suspend () -> Unit) {
    if (isDatabaseExists) {
        deleteDatabase()
    }

    try {
        SQLiteDatabase.openOrCreateDatabase(DATABASE_NAME, null).use(prepare)
        runBlocking { onReady() }
    } catch (e: Throwable) {
        e.printStackTrace()
        fail(e.message)
    } finally {
        deleteDatabase()
    }
}

private fun deleteDatabase() {
    databaseFile.delete()
}
