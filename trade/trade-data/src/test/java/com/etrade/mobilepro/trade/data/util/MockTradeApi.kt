package com.etrade.mobilepro.trade.data.util

import com.etrade.mobilepro.trade.data.stock.PREVIEW_NEW_STOCK_ORDER
import com.etrade.mobilepro.trade.data.stock.StockOrderResponseDto
import com.etrade.mobilepro.trade.data.stock.StockTradingApiClient
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory

private val responseString = StockOrderResponseDto::class.java.getResource("preview_stock_order_response.xml").readText()

internal fun mockApi() = createMockApi(
    MockNetworkInterceptor().mock(
        "http://localhost/e/t/mobile/$PREVIEW_NEW_STOCK_ORDER",
        responseString, 200,
        1000
    )
)

private fun createMockApi(interceptor: MockNetworkInterceptor): StockTradingApiClient {
    val okHttpClient = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .build()
    val retrofit = Retrofit.Builder()
        .baseUrl("http://localhost/")
        .client(okHttpClient)
        .addConverterFactory(SimpleXmlConverterFactory.create())
        .build()

    return retrofit.create(StockTradingApiClient::class.java)
}
