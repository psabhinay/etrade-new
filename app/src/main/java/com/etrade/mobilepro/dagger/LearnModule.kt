package com.etrade.mobilepro.dagger

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.common.di.EntryPointActivity
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.dagger.qualifiers.ViewModelKey
import com.etrade.mobilepro.home.ACTION_OPEN_SYMBOL_LOOKUP
import com.etrade.mobilepro.home.ACTION_OPEN_TRADE_PAGE
import com.etrade.mobilepro.learn.presentation.LearnContainerFragment
import com.etrade.mobilepro.learn.presentation.learnpricetypes.LearnPriceTypesFragment
import com.etrade.mobilepro.learn.presentation.learnpricetypes.LearnPriceTypesViewModel
import com.etrade.mobilepro.learn.presentation.library.LibraryFragment
import com.etrade.mobilepro.learn.presentation.tutorial.LearnTutorialFragment
import com.etrade.mobilepro.learn.presentation.tutorial.LearnTutorialViewModel
import com.etrade.mobilepro.learn.presentation.tutorial.OpenSymbolLookupNavDelegate
import com.etrade.mobilepro.learn.presentation.tutorial.OpenTradePage
import com.etrade.mobilepro.learn.presentation.tutorials.WalkthroughMessageFragment
import com.etrade.mobilepro.util.sendIntentWith
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class LearnModule {

    @Binds
    @IntoMap
    @FragmentKey(LearnContainerFragment::class)
    internal abstract fun bindLearnContainerFragment(fragment: LearnContainerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(LearnPriceTypesFragment::class)
    internal abstract fun bindLearnPriceTypesFragment(fragment: LearnPriceTypesFragment): Fragment

    @Binds
    @IntoMap
    @ViewModelKey(LearnPriceTypesViewModel::class)
    internal abstract fun bindLearnPriceTypesViewModel(viewModel: LearnPriceTypesViewModel): ViewModel

    @Binds
    @IntoMap
    @FragmentKey(LearnTutorialFragment::class)
    internal abstract fun bindLearnTutorialFragment(fragment: LearnTutorialFragment): Fragment

    @Binds
    @IntoMap
    @ViewModelKey(LearnTutorialViewModel::class)
    internal abstract fun bindLearnTutorialViewModel(viewModel: LearnTutorialViewModel): ViewModel

    @Binds
    @IntoMap
    @FragmentKey(LibraryFragment::class)
    internal abstract fun bindLibraryFragment(fragment: LibraryFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(WalkthroughMessageFragment::class)
    internal abstract fun bindOverBottomSheetMessageFragment(fragment: WalkthroughMessageFragment): Fragment

    companion object {
        @Provides
        fun provideOpenSymbolLookupNavDelegate(
            @EntryPointActivity activityClass: Class<out Activity>,
            context: Context
        ) = object : OpenSymbolLookupNavDelegate {
            override fun invoke() {
                sendIntentWith(context, activityClass) {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    action = ACTION_OPEN_SYMBOL_LOOKUP
                }
            }
        }

        @Provides
        fun provideOpenTradePageDelegate(
            @EntryPointActivity activityClass: Class<out Activity>,
            context: Context
        ) = object : OpenTradePage {
            override fun invoke() {
                sendIntentWith(context, activityClass) {
                    flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    action = ACTION_OPEN_TRADE_PAGE
                }
            }
        }
    }
}
