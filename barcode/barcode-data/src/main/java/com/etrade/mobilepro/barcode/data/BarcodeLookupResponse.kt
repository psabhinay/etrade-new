package com.etrade.mobilepro.barcode.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BarcodeLookupResponse(
    @Json(name = "data")
    val data: Data?
)

@JsonClass(generateAdapter = true)
data class Data(
    @Json(name = "response")
    val response: Response?
)

@JsonClass(generateAdapter = true)
data class Response(
    @Json(name = "query")
    val query: Query?,
    @Json(name = "pubDate")
    val pubDate: String?,
    @Json(name = "securities")
    val securities: Securities?,
    @Json(name = "product")
    val product: Product?
)

@JsonClass(generateAdapter = true)
data class Query(
    @Json(name = "upc")
    val upc: String?,
    @Json(name = "format")
    val format: String?
)

@JsonClass(generateAdapter = true)
data class Securities(
    @Json(name = "match")
    val match: List<Match>?
)

@JsonClass(generateAdapter = true)
data class Product(
    @Json(name = "name")
    val name: String?,
    @Json(name = "description")
    val description: String?,
    @Json(name = "companyName")
    val companyName: String?,
    @Json(name = "companyId")
    val companyId: String?
)

@JsonClass(generateAdapter = true)
data class Match(
    @Json(name = "description")
    val description: String?,
    @Json(name = "symbol")
    val symbol: String?
)
