package com.etrade.mobilepro.quote.screen.presentation.fees

import androidx.compose.runtime.Composable
import androidx.compose.ui.viewinterop.AndroidViewBinding
import androidx.fragment.app.Fragment
import com.etrade.eo.corelibandroid.createViewModel
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.quote.screen.presentation.R
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemMutualFundFeesExpensesBinding
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.StateFlow

class FundFeesViewDelegate @AssistedInject constructor(
    @Assisted private val fragment: Fragment,
    assistedModelFactory: FundFeesViewViewModel.Factory,
    snackBarUtilFactory: SnackbarUtilFactory,
    @Assisted private val symbol: SearchResultItem.Symbol,
    @Assisted private val mobileQuoteStateFlow: StateFlow<MobileQuote>?
) {

    private var snackBar: Snackbar? = null
    private val snackBarUtil by lazy { snackBarUtilFactory.createSnackbarUtil({ fragment.viewLifecycleOwner }, { fragment.view }) }
    private lateinit var binding: QuoteItemMutualFundFeesExpensesBinding
    private val viewModel: FundFeesViewViewModel = fragment.createViewModel {
        assistedModelFactory.create(symbol, mobileQuoteStateFlow)
    }.value

    @AssistedFactory
    interface Factory {
        fun create(
            fragment: Fragment,
            symbol: SearchResultItem.Symbol,
            mobileQuoteStateFlow: StateFlow<MobileQuote>?
        ): FundFeesViewDelegate
    }

    fun onViewCreated() {
        fragment.lifecycle.addObserver(viewModel)
    }

    @Composable
    fun FundFeesViewCompose() {
        AndroidViewBinding(QuoteItemMutualFundFeesExpensesBinding::inflate) {
            if (!::binding.isInitialized || binding != this) {
                binding = this
                this@FundFeesViewDelegate.bindViewModel()
                this@FundFeesViewDelegate.bindViews()
                this@FundFeesViewDelegate.viewModel.configureFeeData()
            }
        }
    }

    private fun bindViewModel() {
        viewModel.feesAndExpenseLiveData.observe(fragment.viewLifecycleOwner) {
            when (it) {
                is Resource.Success -> handleOnSuccess(it.data)
                is Resource.Failed -> {
                    handleFailed()
                }
                else -> Unit
            }
        }
    }

    private fun bindViews() {
        binding.feesDisclosureView.disclosureTitle.setOnClickListener { setUpDisclosureDialog() }
        binding.feesDisclosureView.definitionsTitle.setOnClickListener { setUpDefinitionsDialog() }
    }

    private fun handleFailed() {
        if (snackBar?.isShown == true) {
            return
        }

        snackBar = snackBarUtil.retrySnackbar {
            loadDetails()
            snackBar?.dismiss()
        }
    }

    private fun loadDetails() {
        viewModel.configureSymbol()
    }

    private fun handleOnSuccess(feeData: FeeAndExpenseData?) {
        feeData?.data?.let { binding.feesExpensesLayout.setContent(data = it) }
    }

    private fun setUpDisclosureDialog() {
        fragment.apply {
            startActivity(
                SpannableTextOverlayActivity.intent(
                    requireContext(),
                    getString(R.string.disclosures),
                    getString(R.string.mutual_fund_disclosure),
                    R.drawable.ic_close
                )
            )
        }
    }

    private fun setUpDefinitionsDialog() {
        fragment.apply {
            startActivity(
                SpannableTextOverlayActivity.intent(
                    requireContext(),
                    getString(R.string.definitions),
                    getString(R.string.mutual_fund_definitions),
                    R.drawable.ic_close
                )
            )
        }
    }
}
