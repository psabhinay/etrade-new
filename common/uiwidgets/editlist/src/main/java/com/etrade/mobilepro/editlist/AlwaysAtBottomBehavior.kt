package com.etrade.mobilepro.editlist

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.min
import kotlin.math.roundToInt

/**
 * This behaviour allows a child view of [CoordinatorLayout] to be attached to the bottom of a viewport or to the last element of [RecyclerView] if the number
 * of its elements is more than the viewport can display.
 *
 * To place a child in a correct position this behaviour translates it and its dependent view respectively to a scrolling position, the number of items it has
 * and their visibility.
 *
 * Right now the behavior supports [RecyclerView]s with [LinearLayoutManager]s only.
 */
class AlwaysAtBottomBehavior(context: Context, attrs: AttributeSet) : CoordinatorLayout.Behavior<View>(context, attrs) {

    private var adapterDataObserver: RecyclerView.AdapterDataObserver? = null
    private var recyclerView: RecyclerView? = null

    override fun layoutDependsOn(parent: CoordinatorLayout, child: View, dependency: View): Boolean {
        return dependency is RecyclerView
    }

    override fun onDependentViewChanged(parent: CoordinatorLayout, child: View, dependency: View): Boolean {
        if (adapterDataObserver != null) return false // already registered to another dependency

        recyclerView = dependency as RecyclerView
        val adapter = dependency.adapter ?: return false // there is nothing to do if there is no adapter

        adapterDataObserver = DataObserver(dependency, child)
            .also { adapter.registerAdapterDataObserver(it) }
        initAfterItemsChanged(recyclerView, child)
        return true
    }

    override fun onDependentViewRemoved(parent: CoordinatorLayout, child: View, dependency: View) {
        adapterDataObserver?.also { (dependency as RecyclerView).adapter?.unregisterAdapterDataObserver(it) }
        adapterDataObserver = null
    }

    override fun onLayoutChild(parent: CoordinatorLayout, child: View, layoutDirection: Int): Boolean {
        initAfterItemsChanged(recyclerView, child)
        return super.onLayoutChild(parent, child, layoutDirection)
    }

    override fun onStartNestedScroll(coordinatorLayout: CoordinatorLayout, child: View, directTargetChild: View, target: View, axes: Int, type: Int): Boolean {
        return true
    }

    override fun onNestedPreScroll(coordinatorLayout: CoordinatorLayout, child: View, target: View, dx: Int, dy: Int, consumed: IntArray, type: Int) {
        // if we haven't reached the end of RecyclerView yet, then we don't need to show child
        if ((target as RecyclerView).canScrollVertically(1)) return

        if (dy > 0) { // list items go up
            consumeScrollUp(child, target, dy, consumed)
        } else if (dy < 0) { // list items go down
            consumeScrollDown(child, target, dy, consumed)
        }
    }

    private fun initAfterItemsChanged(recyclerView: RecyclerView?, child: View) {
        recyclerView?.let {
            val firstItemView = it.firstItemView
            val lastItemView = it.lastItemView

            if (firstItemView != null && lastItemView != null) {
                // every items in the list are visible on inside a view port
                val visibleChildrenHeight = lastItemView.bottom - firstItemView.top
                val itemsAvailableHeight = it.height - child.trueHeight

                // this is a translation required to show the last item in a
                val requiredTranslation = visibleChildrenHeight - itemsAvailableHeight
                if (requiredTranslation <= 0) {
                    // there is enough space for all items and a child
                    child.translationY = 0f
                    it.translationY = 0f
                } else {
                    // translate recycler view and a child view accordingly
                    val oldChildTransitionY = child.translationY
                    child.translationY = min(requiredTranslation, child.trueHeight).toFloat()
                    if (it.translationY < 0) {
                        // we scrolled all the way down
                        it.translateToY(Math.max(-requiredTranslation + oldChildTransitionY, 0f))
                    }
                }
            } else {
                if (it.isLastItemCompletelyVisible && lastItemView == null) {
                    child.translationY = 0f
                    it.translationY = 0f
                } else {
                    child.translationY = child.trueHeight.toFloat()
                    it.translateToY(0f)
                }
            }
        }
    }

    private fun consumeScrollUp(child: View, target: View, dy: Int, consumed: IntArray) {
        consumeScroll(child, target, dy, consumed, child.translationY) { n1, n2 -> n1 > n2 }
    }

    private fun consumeScrollDown(child: View, target: View, dy: Int, consumed: IntArray) {
        consumeScroll(child, target, dy, consumed, target.translationY) { n1, n2 -> n1 < n2 }
    }

    private inline fun consumeScroll(child: View, target: View, dy: Int, consumed: IntArray, canConsume: Float, compare: (Int, Float) -> Boolean) {
        consumed[1] = if (compare(dy, canConsume)) {
            // scrolled more than we can consume
            child.translationY -= canConsume
            target.translationY -= canConsume
            canConsume.roundToInt()
        } else {
            // consume dy completely
            child.translationY -= dy
            target.translationY -= dy
            dy
        }
    }

    private fun RecyclerView.translateToY(newTranslateY: Float) {
        val oldTranslateY = translationY
        translationY = newTranslateY
        scrollBy(0, (newTranslateY - oldTranslateY).roundToInt())
    }

    private val RecyclerView.isLastItemCompletelyVisible: Boolean
        get() = with(adapter) {
            this == null || itemCount == 0 || lastCompletelyVisibleItemPosition == itemCount - 1
        }

    private val RecyclerView.lastCompletelyVisibleItemPosition: Int
        get() = (layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()

    private val RecyclerView.firstItemView: View?
        get() = with(layoutManager as LinearLayoutManager) {
            findViewByPosition(0)
        }

    private val RecyclerView.lastItemView: View?
        get() = with(layoutManager as LinearLayoutManager) {
            findViewByPosition(itemCount - 1)
        }

    /**
     * This is a true height of a view including top and bottom margins.
     */
    private val View.trueHeight: Int
        get() = measuredHeight + topMargin + bottomMargin

    private val View.topMargin: Int
        get() = (layoutParams as ViewGroup.MarginLayoutParams).topMargin

    private val View.bottomMargin: Int
        get() = (layoutParams as ViewGroup.MarginLayoutParams).bottomMargin

    private inner class DataObserver(private val recyclerView: RecyclerView, private val child: View) : RecyclerView.AdapterDataObserver() {

        private val postDelay = 10L

        override fun onChanged() {
            recyclerView.postDelayed(
                {
                    initAfterItemsChanged(recyclerView, child)
                },
                postDelay
            )
        }

        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) = onChanged()

        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) = onChanged()
    }
}
