package com.etrade.mobilepro.billpay.presentation.common

import android.content.res.Resources
import android.os.Parcelable
import android.telephony.PhoneNumberUtils
import com.etrade.mobilepro.billpay.api.MerchantInfo
import com.etrade.mobilepro.billpay.api.Payee
import com.etrade.mobilepro.billpay.api.PayeeAddress
import com.etrade.mobilepro.billpay.presentation.R
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection
import com.etrade.mobilepro.util.android.accessibility.characterByCharacter
import kotlinx.parcelize.Parcelize
import java.util.Locale

@Parcelize
class ParcelablePayee(
    val payeeId: String?,
    val name: String,
    val nickName: String?,
    val phoneNumber: String?,
    val accountNumber: String,
    val leadDays: Int,
    val isAddressAvailable: Boolean?,
    val address: ParcelablePayeeAddress? = null,
    val merchantInfo: ParcelableMerchantInfo? = null,
    val merchantZipRequired: Boolean? = null,
    val category: String? = null
) : Parcelable

@Parcelize
class ParcelablePayeeAddress(
    val address1: String?,
    val address2: String?,
    val city: String?,
    val state: String?,
    val zipCode: String?
) : Parcelable

@Parcelize
data class ParcelableMerchantInfo(
    val id: Int?,
    val merchantZip: String?,
    val merchantZip4: String?
) : Parcelable

internal fun Payee.toParcelable(): ParcelablePayee = ParcelablePayee(
    payeeId = payeeId,
    name = name,
    nickName = nickName,
    phoneNumber = phoneNumber,
    accountNumber = accountNumber,
    leadDays = leadDays ?: 0,
    isAddressAvailable = isAddressAvailable,
    address = address?.toParcelable(),
    merchantInfo = merchantInfo?.toParcelable(),
    merchantZipRequired = merchantZipRequired,
    category = category
)

internal fun PayeeAddress.toParcelable(): ParcelablePayeeAddress = ParcelablePayeeAddress(
    address1 = address1,
    address2 = address2,
    city = city,
    state = state,
    zipCode = zipCode
)

internal fun MerchantInfo.toParcelable(): ParcelableMerchantInfo = ParcelableMerchantInfo(
    id = id,
    merchantZip = merchantZip,
    merchantZip4 = merchantZip4
)

internal fun ParcelablePayee.toPayee(): Payee = Payee(
    payeeId = payeeId,
    name = name,
    nickName = nickName,
    phoneNumber = phoneNumber,
    accountNumber = accountNumber,
    leadDays = leadDays,
    isAddressAvailable = isAddressAvailable,
    address = address?.toAddress(),
    merchantInfo = merchantInfo?.toMerchantInfo(),
    merchantZipRequired = merchantZipRequired,
    category = category
)

internal fun ParcelablePayeeAddress.toAddress(): PayeeAddress = PayeeAddress(
    address1 = address1,
    address2 = address2,
    city = city,
    state = state,
    zipCode = zipCode
)

internal fun ParcelableMerchantInfo.toMerchantInfo(): MerchantInfo = MerchantInfo(
    id = id,
    merchantZip = merchantZip,
    merchantZip4 = merchantZip4
)

internal fun ParcelablePayee.toListOfTableSectionItemLayout(resources: Resources):
    List<TableSectionItemLayout> {

    val sections = mutableListOf<TableSectionItemLayout>()

    if (name.isNotEmpty()) {
        sections.add(createSummarySection(resources.getString(R.string.bill_pay_payee_details_name), name))
    }
    if (nickName?.isNotEmpty() == true) {
        sections.add(createSummarySection(resources.getString(R.string.bill_pay_payee_details_nickname), nickName))
    }
    if (accountNumber.isNotEmpty()) {
        val label = resources.getString(R.string.account_number)
        val description = accountNumber.characterByCharacter()
        sections.add(createSummarySection(label, accountNumber, valueDescription = description))
    }

    addAddressSections(sections, resources)

    if (phoneNumber?.isNotEmpty() == true) {
        val label = resources.getString(R.string.phone_number)
        val formattedPhoneNumber = PhoneNumberUtils.formatNumber(phoneNumber, Locale.US.country)
        val description = phoneNumber.characterByCharacter()
        sections.add(createSummarySection(label, formattedPhoneNumber, valueDescription = description))
    }
    return sections
}

private fun ParcelablePayee.addAddressSections(
    sections: MutableList<TableSectionItemLayout>,
    resources: Resources
) {
    if (address?.address1?.isNotEmpty() == true) {
        sections.add(createSummarySection(resources.getString(R.string.address1), address.address1))
    }
    if (address?.address2?.isNotEmpty() == true) {
        sections.add(createSummarySection(resources.getString(R.string.address2), address.address2))
    }
    if (address?.city?.isNotEmpty() == true) {
        sections.add(createSummarySection(resources.getString(R.string.city), address.city))
    }
    if (address?.state?.isNotEmpty() == true) {
        sections.add(createSummarySection(resources.getString(R.string.state), address.state))
    }
    if (address?.zipCode?.isNotEmpty() == true) {
        val label = resources.getString(R.string.zip_code)
        val description = address.zipCode.characterByCharacter()
        sections.add(createSummarySection(label, address.zipCode, valueDescription = description))
    }
}
