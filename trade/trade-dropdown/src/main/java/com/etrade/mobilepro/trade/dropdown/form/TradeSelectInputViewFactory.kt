package com.etrade.mobilepro.trade.dropdown.form

import android.content.Context
import com.etrade.mobilepro.dynamic.form.ui.factory.ComponentViewFactory

class TradeSelectInputViewFactory : ComponentViewFactory<TradeSelectInput, TradeSelectInputView> {
    override fun createView(context: Context): TradeSelectInputView = TradeSelectInputView(context)
}
