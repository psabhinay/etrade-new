package com.etrade.mobilepro.markets.common

import com.etrade.eo.core.util.MarketDataFormatter
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.markets.data.response.Quote
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.util.color.DataColorResolver
import com.etrade.mobilepro.util.formatters.formatMarketDataCustom
import com.etrade.mobilepro.util.formatters.formatMarketDataCustomWithoutDollarSign
import com.etrade.mobilepro.util.formatters.formatMarketPercentDataCustom
import com.etrade.mobilepro.util.safeParseBigDecimal
import java.math.BigDecimal

private fun Quote.mainValues() = Triple(symbol, price, symbolDesc)

fun String.formatPrice() = if (contains('%')) {
    this
} else {
    val priceWithoutSeparator = replace(",", "")
    MarketDataFormatter.formatMarketDataCustomWithoutDollarSign(priceWithoutSeparator.safeParseBigDecimal())
}

fun String?.formatPercentChange() =
    MarketDataFormatter.formatMarketPercentDataCustom(this?.trim()?.dropPercentSign()?.toBigDecimalOrNull())

fun String.dropPercentSign(): String = if (isNotEmpty() && this[lastIndex] == '%') {
    dropLast(1)
} else {
    this
}

fun String?.formatChange() = MarketDataFormatter.formatMarketDataCustom(this?.safeParseBigDecimal())

@Suppress("LongMethod")
fun MutableList<Quote>.setSymbolsAndCells(
    listOfSymbols: MutableList<SymbolCell>,
    listOfCells: MutableList<List<ResultDataCell>>,
    dataColorResolver: DataColorResolver
) {
    forEach { quote ->
        val (symbol, price, symbolDesc) = quote.mainValues()
        if (symbol != null && price != null && symbolDesc != null) {
            val instrumentType = InstrumentType.from(quote.productType)
            listOfSymbols.add(
                SymbolCell(
                    symbol,
                    symbolDesc,
                    symbol,
                    instrumentType,
                    symbolDesc
                )
            )
            with(
                SymbolRowInfo(
                    symbol = symbol,
                    price = price.formatPrice(),
                    change = quote.change.formatChange(),
                    percentChange = quote.percentChange.formatPercentChange(),
                    tickIndicator = null,
                    volume = null
                )
            ) {
                listOfCells.add(
                    buildSymbolRowItem(
                        fromMoversResponse = true,
                        instrumentType = instrumentType,
                        dataColorResolver = dataColorResolver
                    )
                )
            }
        }
    }
}

fun SymbolRowInfo.buildSymbolRowItem(
    fromMoversResponse: Boolean = false,
    instrumentType: InstrumentType = InstrumentType.EQ,
    dataColorResolver: DataColorResolver
): List<ResultDataCell> {
    val changeDecimal = change?.safeParseBigDecimal()
    val priceBigDecimal = price.safeParseBigDecimal()
    val dropLength = if (fromMoversResponse) 1 else 0
    val percentChangeWithoutSign = percentChange?.trim()?.dropLast(dropLength)
    val percentChangeValue = percentChange?.safeParseBigDecimal()
    val formattedPercentChange = "${MarketDataFormatter.formatMarketDataCustom(percentChangeWithoutSign?.toBigDecimalOrNull())}%"
    val formattedChange: String = MarketDataFormatter.formatMarketDataCustom(changeDecimal)
    val changeColor = dataColorResolver.resolveDataColor(changeDecimal)
    return listOf(
        ResultDataCell(
            value = priceBigDecimal ?: BigDecimal.ZERO,
            displayText = price,
            instrumentType = instrumentType,
            symbol = symbol,
            dataColor = dataColorResolver.resolveFilledDataColor(tickIndicator)
        ),
        ResultDataCell(changeDecimal ?: BigDecimal.ZERO, formattedChange, instrumentType, symbol, changeColor),
        ResultDataCell(percentChangeValue ?: BigDecimal.ZERO, formattedPercentChange, instrumentType, symbol, changeColor)
    )
}

fun getSymbolRowInfo(
    data: Level1Data,
    symbol: String,
    lastPrice: String
): SymbolRowInfo {
    val (change, percentChange) = if (data is StockData) {
        Pair(data.dayChange, data.dayChangePercent)
    } else {
        Pair(data.change, data.changePercent)
    }
    return SymbolRowInfo(
        symbol = symbol,
        price = lastPrice,
        change = change,
        percentChange = percentChange,
        volume = data.volume,
        tickIndicator = data.tickIndicator
    )
}

fun getUpdatedCellItemsForSymbol(
    currentCellItems: List<List<ResultDataCell>>,
    symbolCellItems: List<ResultDataCell>,
    symbol: String
): List<List<ResultDataCell>> {
    val updatedCellItems = mutableListOf<List<ResultDataCell>>()
    currentCellItems.forEach { cells ->
        updatedCellItems.add(
            if (cells.first().id == symbol) {
                symbolCellItems
            } else {
                cells
            }
        )
    }
    return updatedCellItems
}
