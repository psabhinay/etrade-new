package com.etrade.mobilepro.transfermoney.data.repo

import com.etrade.mobilepro.transfermoney.api.TransferRepeat
import com.etrade.mobilepro.transfermoney.api.repo.TransferRepeatDataSource
import javax.inject.Inject

class DefaultTransferRepeatDataSource @Inject constructor() : TransferRepeatDataSource {
    override suspend fun getTransferRepeats(): List<TransferRepeat> = listOf(
        TransferRepeat.None,
        TransferRepeat.Week,
        TransferRepeat.TwoWeeks,
        TransferRepeat.FourWeeks,
        TransferRepeat.Month,
        TransferRepeat.EndOfMonth,
        TransferRepeat.TwoMonths,
        TransferRepeat.ThreeMonths,
        TransferRepeat.FourMonths,
        TransferRepeat.SixMonths,
        TransferRepeat.Year
    )
}
