package com.etrade.mobilepro.trade.presentation

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.CallSuper
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.dialog.showDialog
import com.etrade.mobilepro.dialog.viewmodel.AppDialogViewDelegate
import com.etrade.mobilepro.dialog.viewmodel.DialogViewModel
import com.etrade.mobilepro.dialog.viewmodel.plus
import com.etrade.mobilepro.dynamic.form.fragment.FormFragment
import com.etrade.mobilepro.dynamic.form.ui.ActionListView
import com.etrade.mobilepro.dynamic.form.ui.FormView
import com.etrade.mobilepro.dynamic.form.ui.WithInputId
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.searchingapi.symbolFromSearchResult
import com.etrade.mobilepro.tooltip.AppTooltip
import com.etrade.mobilepro.tooltip.TOOLTIP_INDETERMINATE_DURATION
import com.etrade.mobilepro.tooltip.Tooltip
import com.etrade.mobilepro.tooltip.TooltipGravity
import com.etrade.mobilepro.tooltip.TooltipViewModel
import com.etrade.mobilepro.tooltip.viewmodel.AppTooltipViewDelegate
import com.etrade.mobilepro.trade.conditionalOrderDisclosure.presentation.ConditionalOrderDisclosureViewDelegate
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.presentation.form.FormActionTooltip
import com.etrade.mobilepro.trade.presentation.form.FormTooltip
import com.etrade.mobilepro.trade.presentation.form.component.FooterComponent
import com.etrade.mobilepro.trade.presentation.form.component.view.factory.FooterComponentViewFactory
import com.etrade.mobilepro.trade.presentation.util.SharedTradeViewModel
import com.etrade.mobilepro.trade.router.TradeRouter
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.consumeIf
import com.etrade.mobilepro.util.android.extension.awaitForMainThread
import com.etrade.mobilepro.util.android.extension.viewCoroutineScope
import com.etrade.mobilepro.util.android.fragment.OrientationLock
import com.etrade.mobilepro.util.android.recyclerviewutil.ViewLocator
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.etrade.mobilepro.viewdelegate.BlockingLoadingIndicatorViewDelegate
import com.etrade.mobilepro.viewdelegate.LifecycleObserverViewDelegate
import com.etrade.mobilepro.viewdelegate.NavigationSignalViewDelegate
import com.etrade.mobilepro.viewdelegate.ViewDelegate
import com.etrade.mobilepro.walkthrough.presentation.WalkthroughStatesViewModel
import com.squareup.moshi.Moshi
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference

const val KEY_TRADE_ORDER_TYPE = "tradeOrderType"
private const val KEY_PENDING_SYMBOL_INPUT_ID = "FormFragment.pendingSymbolInputId"

abstract class BaseTradeFormFragment(
    snackbarUtilFactory: SnackbarUtilFactory,
    viewModelFactory: ViewModelProvider.Factory,
    tradeRouter: TradeRouter,
    dialogViewModel: DialogViewModel,
    private val orientationLock: OrientationLock,
    final override val moshi: Moshi,
    private val walkthroughViewModel: WalkthroughStatesViewModel
) : FormFragment(snackbarUtilFactory) {

    abstract override val formViewModel: BaseTradeFormViewModel

    open val securityType: SecurityType = SecurityType.STOCK

    private var pendingSymbolInputId: String? = null

    protected val quoteWidgetViewModel: QuotesWidgetViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }

    private val sharedTradeViewModel: SharedTradeViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }

    private val disclosureViewModel: TradeConditionalOrderDisclosureViewModel by navGraphViewModels(tradeRouter.navGraphId) { viewModelFactory }

    private val tooltipViewModel: TooltipViewModel by activityViewModels()

    private var layoutManagerRef: WeakReference<LinearLayoutManager>? = null

    private var actionListViewRef: WeakReference<View>? = null

    private val viewLocator: ViewLocator by lazy { ViewLocator(::awaitForMainThread) }

    private val viewDelegates: Iterable<ViewDelegate> by lazy {
        listOf(
            AppDialogViewDelegate(formViewModel + dialogViewModel) {
                showDialog(it)
            },
            AppTooltipViewDelegate(formViewModel) { onAppTooltipReceived(it) },
            BlockingLoadingIndicatorViewDelegate(disclosureViewModel, childFragmentManager),
            ConditionalOrderDisclosureViewDelegate(
                requireContext(),
                snackBarUtil,
                disclosureViewModel
            ),
            LifecycleObserverViewDelegate(listOf(quoteWidgetViewModel)),
            NavigationSignalViewDelegate(formViewModel) { findNavController() }
        )
    }

    private val symbolLookupRequest =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            formViewModel.injectSymbol(
                symbolFromSearchResult(result.resultCode, result.data),
                pendingSymbolInputId
            )
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        formViewModel.symbolLookupSignal.observe(viewLifecycleOwner) { onSymbolLookupSignal(it) }

        sharedTradeViewModel.securityType = securityType

        viewDelegates.forEach { delegate ->
            delegate.observe(viewLifecycleOwner)
        }

        pendingSymbolInputId = savedInstanceState?.getString(KEY_PENDING_SYMBOL_INPUT_ID)
        showBanner()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        // Explicitly save Symbol Input Id  since Form does not retain the SymbolInputId
        // This occurs due to its form action i.e Symbol Lookup is launched in a different Activity and if current Activity (Form) happens to be destroyed,
        // A new Form Activity is launched and hence Symbol lookup result is not associated to any input ID.
        outState.putString(KEY_PENDING_SYMBOL_INPUT_ID, formViewModel.pendingSymbolInputId)
    }

    @CallSuper
    override fun beforeFormFetch() {
        formViewModel.sharedTradeViewModel = sharedTradeViewModel
        formViewModel.quoteWidgetViewModel = quoteWidgetViewModel
        formViewModel.disclosureViewModel = disclosureViewModel
        formViewModel.tradeOrderType = arguments?.getSerializable(KEY_TRADE_ORDER_TYPE) as? TradeOrderType ?: TradeOrderType.NEW
        formViewModel.walkthroughViewModel = walkthroughViewModel

        sharedTradeViewModel.apply {
            injectFormInputValuesSignal.observe(viewLifecycleOwner) {
                onInjectFormInputValuesSignal(
                    it
                )
            }
            orderTerminatedSignal.observe(viewLifecycleOwner) { onOrderTerminatedSignal(it) }
            optionLegSelectedSignal.observe(viewLifecycleOwner) { formViewModel.onSymbolSelectedForOptionsType() }
        }
    }

    @CallSuper
    override fun initFormView(formView: FormView) {
        formView.apply {
            addViewFactory(FooterComponent::class, FooterComponentViewFactory())
        }
        layoutManagerRef = (formView.layoutManager as? LinearLayoutManager)?.let(::WeakReference)
    }

    @CallSuper
    override fun initActionListView(actionListView: ActionListView) {
        super.initActionListView(actionListView)
        actionListViewRef = WeakReference(actionListView)
    }

    private fun showBanner() {
        with(formViewModel.inboxMessagesDelegate) {
            showBanner.observeBy(
                viewLifecycleOwner,
                {
                    toggleTradeHaltInboxMessage(
                        it,
                        requireView().findViewById(R.id.trade_inbox_messages)
                    )
                }
            )
        }
    }

    private fun onInjectFormInputValuesSignal(event: ConsumableLiveEvent<InputValuesSnapshot>) {
        event.consumeIf(formViewModel.allowConsumeFormInputValues) { data ->
            formViewModel.injectValues(data)
        }
    }

    private fun onOrderTerminatedSignal(event: ConsumableLiveEvent<String>) {
        event.consume {
            formViewModel.resetFormValues()
            scrollFormToTop()
        }
    }

    private fun onSymbolLookupSignal(event: ConsumableLiveEvent<Intent>) {
        event.consume {
            symbolLookupRequest.launch(it)
        }
    }

    private fun onAppTooltipReceived(consumableTooltip: ConsumableLiveEvent<AppTooltip>) {
        val tooltip = consumableTooltip
            .takeIf { !it.isConsumed }
            ?.peekContent()
            ?: return

        viewLifecycleOwner.lifecycleScope.launch {
            // There may be pending RecyclerView rendering operations, so let's wait for any
            // pending ui task to be finished before we traverse views.
            awaitForMainThread()
            val targetView = getTooltipTargetView(tooltip) ?: return@launch

            // Await for main thread before trying to attach a tooltip to a View which is inside
            // a RecyclerView.
            awaitForMainThread()
            tooltipViewModel.show {
                target = targetView
                text = tooltip.message
                gravity = tooltip.gravity
                yOffset = getTooltipYOffset(gravity)
                shouldConsumeDismissEvent = tooltip.shouldConsumeDismissEvent
                durationParams = Tooltip.DurationParams(viewCoroutineScope, TOOLTIP_INDETERMINATE_DURATION)
                onExplicitDismiss = {
                    tooltip.onExplicitDismiss?.invoke()
                    consumableTooltip.consume()
                }
                onImplicitDismiss = {
                    tooltip.onImplicitDismiss?.invoke()
                    consumableTooltip.consume()
                }
            }
        }
    }

    private suspend fun getTooltipTargetView(tooltip: AppTooltip): View? {
        val linearLayoutManager = layoutManagerRef?.get() ?: return null
        val actionListView = actionListViewRef?.get() ?: return null

        return when (tooltip) {
            is FormTooltip -> {
                viewLocator.locateView(linearLayoutManager) { candidateView ->
                    candidateView is WithInputId && candidateView.inputId == tooltip.targetInputId
                }
            }
            is FormActionTooltip -> {
                viewLocator.locateView(actionListView) { candidateView ->
                    candidateView is TextView && candidateView.text == tooltip.targetActionTitle
                }
            }
            else -> { null }
        }
    }

    private fun getTooltipYOffset(gravity: TooltipGravity): Int {
        return when (gravity) {
            TooltipGravity.TOP -> -resources.getDimensionPixelOffset(R.dimen.spacing_small)
            else -> resources.getDimensionPixelOffset(R.dimen.spacing_small)
        }
    }
}
