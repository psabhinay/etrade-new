package com.etrade.mobilepro.trade.data.common

import com.etrade.mobilepro.testutil.getObjectFromJson
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class LegDetailsCommonDtoTest {

    @Test
    fun `check full json`() {
        readFile("leg-details-full.json").run {
            checkBaseProperties()
            assertEquals("1", expiryDay)
            assertEquals("5", expiryMonth)
            assertEquals("2020", expiryYear)
            assertEquals("1", expiryDay)
            assertEquals("192.500000", strikePrice)
            assertEquals("1", optionType)
        }
    }

    @Test
    fun `check short json`() {
        readFile("leg-details-short.json").run {
            checkBaseProperties()
        }
    }

    private fun LegDetailsCommonDto.checkBaseProperties() {
        assertEquals("FB----200501C00192500", symbol)
        assertEquals("1", transactionCode)
        assertEquals(1, quantity)
    }

    private fun readFile(path: String): LegDetailsCommonDto {
        return LegDetailsCommonDto::class.getObjectFromJson(
            resourcePath = path
        )
    }
}
