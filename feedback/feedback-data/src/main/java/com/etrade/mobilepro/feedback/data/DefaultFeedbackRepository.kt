package com.etrade.mobilepro.feedback.data

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.feedback.api.FeedbackRepository
import com.etrade.mobilepro.feedback.api.FeedbackRequest
import com.etrade.mobilepro.feedback.api.FeedbackResponse

class DefaultFeedbackRepository(private val feedbackService: FeedbackService) : FeedbackRepository {
    override suspend fun sendFeedback(feedbackRequest: FeedbackRequest): ETResult<FeedbackResponse> {
        return runCatchingET {
            feedbackService.getFeedbackResponse(
                feedbackRequest
            )
        }
    }
}
