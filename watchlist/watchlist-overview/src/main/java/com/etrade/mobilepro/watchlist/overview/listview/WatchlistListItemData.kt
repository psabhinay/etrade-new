package com.etrade.mobilepro.watchlist.overview.listview

import androidx.compose.ui.graphics.Color

data class WatchlistListItemData(
    val header: String,
    val headerDetails: String,
    val lastPrice: String,
    val change: String,
    val changeColor: Color,
    val highAndLow: String,
)
