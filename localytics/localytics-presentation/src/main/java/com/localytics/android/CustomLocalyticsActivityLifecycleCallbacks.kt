package com.localytics.android

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.lifecycle.lifecycleScope
import com.etrade.mobilepro.appstart.api.AppStartRepo
import com.etrade.mobilepro.appstart.api.AppStartRepo.Companion.IS_SUBJECT_TO_GDPR_DEFAULT
import com.etrade.mobilepro.localytics.LocalyticsHelper
import com.localytics.androidx.Localytics
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import javax.inject.Inject

class CustomLocalyticsActivityLifecycleCallbacks @Inject constructor(
    val localyticsHelper: LocalyticsHelper,
    val appStartRepo: AppStartRepo
) : Application.ActivityLifecycleCallbacks {

    private val logger by lazy { LoggerFactory.getLogger(javaClass) }

    override fun onActivityPaused(activity: Activity) {
        // np
    }

    override fun onActivityStarted(activity: Activity) {
        // np
    }

    override fun onActivityDestroyed(activity: Activity) {
        // np
    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        // np
    }

    override fun onActivityStopped(activity: Activity) {
        // np
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        if (localyticsHelper.isAppLauncherActivity(activity)) {
            (activity as? ComponentActivity)?.lifecycleScope?.launch {
                appStartRepo.getGDPRStatus().fold(
                    onSuccess = { Localytics.setOptedOut(it) },
                    onFailure = {
                        Localytics.setOptedOut(IS_SUBJECT_TO_GDPR_DEFAULT)
                    }
                )

                // Refresh Inbox campaigns on app start
                Localytics.refreshInboxCampaigns {
                    logger.debug("Number of inbox campaigns : ${it.size}")
                }
            }
        }
    }

    override fun onActivityResumed(activity: Activity) {
        // np
    }
}
