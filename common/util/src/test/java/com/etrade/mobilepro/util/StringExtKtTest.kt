package com.etrade.mobilepro.util

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class StringExtKtTest {

    // isNegativeNumber

    @Test
    fun isNegativeNumber_negativeString_returnsTrue() {
        assertTrue("-123".isNegativeNumber())
    }

    @Test
    fun isNegativeNumber_negativeDecimalString_returnsTrue() {
        assertTrue("-12.3".isNegativeNumber())
    }

    @Test
    fun isNegativeNumber_negativeDecimalStringWithLeadingZero_returnsTrue() {
        assertTrue("-0.123".isNegativeNumber())
    }

    @Test
    fun isNegativeNumber_negativeDecimalStringWithLeadingSeparator_returnsTrue() {
        assertTrue("-.123".isNegativeNumber())
    }

    @Test
    fun isNegativeNumber_positiveString_returnsFalse() {
        assertFalse("123".isNegativeNumber())
    }

    @Test
    fun isNegativeNumber_positiveDecimalString_returnsFalse() {
        assertFalse("12.3".isNegativeNumber())
    }

    @Test
    fun isNegativeNumber_positiveDecimalStringWithZero_returnsFalse() {
        assertFalse("0.123".isNegativeNumber())
    }

    @Test
    fun isNegativeNumber_positiveDecimalStringWithLeadingSeparator_returnsFalse() {
        assertFalse(".123".isNegativeNumber())
    }

    @Test
    fun isNegativeNumber_nonNumericalString_returnsFalse() {
        assertFalse("asd.123".isNegativeNumber())
    }

    @Test
    fun isNegativeNumber_incompleteNumericalString_returnsFalse() {
        assertFalse("123.".isNegativeNumber())
    }

    @Test
    fun isNegativeNumber_zeroString_returnsFalse() {
        assertFalse("0".isNegativeNumber())
    }

    @Test
    fun isNegativeNumber_decimalZeroString_returnsFalse() {
        assertFalse("0.0".isNegativeNumber())
    }

    // isPositiveNumber

    @Test
    fun isPositiveNumber_negativeString_returnsFalse() {
        assertFalse("-123".isPositiveNumber())
    }

    @Test
    fun isPositiveNumber_negativeDecimalString_returnsFalse() {
        assertFalse("-12.3".isPositiveNumber())
    }

    @Test
    fun isPositiveNumber_negativeDecimalStringWithZero_returnsFalse() {
        assertFalse("-0.123".isPositiveNumber())
    }

    @Test
    fun isPositiveNumber_negativeDecimalStringWithLeadingSeparator_returnsFalse() {
        assertFalse("-.123".isPositiveNumber())
    }

    @Test
    fun isPositiveNumber_positiveString_returnsTrue() {
        assertTrue("123".isPositiveNumber())
    }

    @Test
    fun isPositiveNumber_positiveDecimalString_returnsTrue() {
        assertTrue("12.3".isPositiveNumber())
    }

    @Test
    fun isPositiveNumber_positiveDecimalStringWithLeadingZero_returnsTrue() {
        assertTrue("0.123".isPositiveNumber())
    }

    @Test
    fun isPositiveNumber_positiveDecimalStringWithLeadingSeparator_returnsTrue() {
        assertTrue(".123".isPositiveNumber())
    }

    @Test
    fun isPositiveNumber_nonNumericalString_returnsFalse() {
        assertFalse("asd.123".isPositiveNumber())
    }

    @Test
    fun isPositiveNumber_incompleteNumericalString_returnsFalse() {
        assertFalse("123.".isPositiveNumber())
    }

    @Test
    fun isPositiveNumber_zeroString_returnsFalse() {
        assertFalse("0".isPositiveNumber())
    }

    @Test
    fun isPositiveNumber_decimalZeroString_returnsFalse() {
        assertFalse("0.0".isPositiveNumber())
    }
}
