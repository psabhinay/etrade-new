package com.etrade.mobilepro.screener.data.rest.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FundCategoryResponse(
    @Json(name = "FundCategories")
    val categories: List<String>
)
