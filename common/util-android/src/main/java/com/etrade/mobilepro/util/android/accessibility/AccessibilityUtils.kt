package com.etrade.mobilepro.util.android.accessibility

import android.content.Context
import android.content.res.Resources
import android.view.View
import android.view.View.IMPORTANT_FOR_ACCESSIBILITY_YES
import android.view.ViewGroup
import android.view.accessibility.AccessibilityEvent
import android.view.accessibility.AccessibilityManager
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.TextView
import androidx.annotation.PluralsRes
import androidx.core.content.getSystemService
import androidx.core.view.AccessibilityDelegateCompat
import androidx.core.view.ViewCompat
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat.ACTION_CLICK
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat.AccessibilityActionCompat
import androidx.core.widget.doOnTextChanged
import androidx.databinding.BindingAdapter
import com.etrade.mobilepro.util.android.R
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import org.slf4j.LoggerFactory
import java.math.BigDecimal

internal val logger = LoggerFactory.getLogger("AccessibilityUtils")

val Context.accessibilityEnabled: Boolean
    get() = accessibilityManager?.let { manager ->
        manager.isEnabled && manager.isTouchExplorationEnabled
    } ?: false

val Context.accessibilityManager get() = getSystemService<AccessibilityManager>()

/**
 * If ignoreForAccessibility is true, then the view will be marked as not clickable, not focusable, and not important for accessibility.
 * Otherwise, importantForAccessability will be set to auto, and focusable and clickable will be set to the values given by
 * accessibilityDefaultFocusable and accessibilityDefaultClickable, if set (they are optional).
 */
@BindingAdapter(value = ["ignoreForAccessibility", "accessibilityDefaultFocusable", "accessibilityDefaultClickable"], requireAll = false)
fun ignoreForAccessibility(view: View, ignore: Boolean, defaultFocusable: Boolean?, defaultClickable: Boolean?) {
    if (ignore) {
        view.isClickable = false
        view.isFocusable = false
        view.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO
    } else {
        defaultFocusable?.let { view.isFocusable = it }
        defaultClickable?.let { view.isClickable = it }
        view.importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_AUTO
    }
}

private const val ACCOUNT_NUMBER_PREFIX_POS = 1
private const val ACCOUNT_NUMBER_POSTFIX_POS = 2
private const val ACCESSIBILITY_FOCUS_DELAY_MS = 100L

fun Context.accountNameContentDescription(accountName: String?): String? =
    resources.accountNameContentDescription(accountName)

/**
 * Replaces the "-XXXX" or "XXXXXX-XXXX" in an account name (where each 'X' is a number) with an accessible pronunciation.
 */
fun Resources.accountNameContentDescription(accountName: String?): String? {
    accountName ?: return null
    return """(\d*)\s*-\s*(\d+)"""
        .toRegex()
        .replace(accountName) {
            val prefix = it.groupValues[ACCOUNT_NUMBER_PREFIX_POS].characterByCharacter()
            val dash = getString(R.string.accessibility_dash)
            val postfix = it.groupValues[ACCOUNT_NUMBER_POSTFIX_POS].characterByCharacter()
            "$prefix $dash $postfix"
        }
}

@BindingAdapter("accountNameContentDescription")
fun accountNameContentDescription(view: View, accountName: String?) {
    view.contentDescription = view.context.accountNameContentDescription(accountName)
}

fun Resources.formatAccountNameContentDescription(accountName: String?): String {
    return accountNameContentDescription(accountName).orEmpty()
}

class OnFirstAccessibilityFocusRunner(private val rootView: View, private val runOnFirstFocus: () -> Unit) : AccessibilityDelegateCompat() {
    private var hasAlreadyRun = false

    override fun onRequestSendAccessibilityEvent(
        host: ViewGroup?,
        child: View?,
        event: AccessibilityEvent?
    ): Boolean {
        if (!hasAlreadyRun && event != null && event.eventType == AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED) {
            hasAlreadyRun = true
            rootView.accessibilityDelegate = null
            runOnFirstFocus.invoke()
        }
        return super.onRequestSendAccessibilityEvent(host, child, event)
    }

    init {
        ViewCompat.setAccessibilityDelegate(rootView, this)
    }
}

fun View.initAccessibility(onInitialize: AccessibilityNodeInfoCompat.() -> Unit) {
    ViewCompat.setAccessibilityDelegate(
        this,
        object : AccessibilityDelegateCompat() {
            override fun onInitializeAccessibilityNodeInfo(host: View, info: AccessibilityNodeInfoCompat) {
                super.onInitializeAccessibilityNodeInfo(host, info)
                onInitialize.invoke(info)
            }
        }
    )
}

fun View.populateAccessibilityEvent(block: View.() -> Unit) {
    ViewCompat.setAccessibilityDelegate(
        this,
        object : AccessibilityDelegateCompat() {
            override fun onPopulateAccessibilityEvent(host: View?, event: AccessibilityEvent?) {
                host?.let { block(it) }

                super.onPopulateAccessibilityEvent(host, event)
            }
        }
    )
}

fun View.clearAccessibilityFocus() {
    performAccessibilityAction(AccessibilityNodeInfo.ACTION_CLEAR_ACCESSIBILITY_FOCUS, null)
}

fun TextInputLayout.initTextInputAccessibility(onInitialize: AccessibilityNodeInfoCompat.() -> Unit) {
    setTextInputAccessibilityDelegate(object : TextInputLayout.AccessibilityDelegate(this) {
        override fun onInitializeAccessibilityNodeInfo(host: View, info: AccessibilityNodeInfoCompat) {
            super.onInitializeAccessibilityNodeInfo(host, info)
            info.onInitialize()
            logger.debug("initialized text input with $info")
        }
    })
}

fun AccessibilityNodeInfoCompat.addClickAction(actionLabel: String) {
    addAction(AccessibilityActionCompat(ACTION_CLICK, actionLabel))
}

fun TextView.bindButtonAccessibility() {
    initAccessibility {
        className = context.getString(R.string.accessibility_class_name_button)
    }
}

fun View.bindButtonAccessibility(description: String?, actionLabel: String? = null) {
    initAccessibility { bindButtonAccessibility(this, description, actionLabel) }
}

fun TextInputLayout.bindTextInputButtonAccessibility(description: String?, actionLabel: String? = null) {
    initTextInputAccessibility { bindButtonAccessibility(this, description, actionLabel) }
}

private fun View.bindButtonAccessibility(nodeInfo: AccessibilityNodeInfoCompat, description: String?, actionLabel: String?) {
    nodeInfo.apply {
        contentDescription = description
        className = context.getString(R.string.accessibility_class_name_button)
        hintText = ""
        addClickAction(actionLabel ?: context.getString(R.string.accessibility_action_select))
        removeAction(AccessibilityActionCompat.ACTION_LONG_CLICK)
        isLongClickable = false
        importantForAccessibility = IMPORTANT_FOR_ACCESSIBILITY_YES
    }
}

fun TextInputLayout.bindTextEditAccessibility(label: String, value: String?, characterByCharacter: Boolean = false, actionLabel: String? = null) {
    bindTextInputButtonAccessibility(createContentDescription(label, value, characterByCharacter), actionLabel)
}

fun TextInputLayout.bindTextEditAccessibilityListeners(
    editText: TextInputEditText,
    label: String,
    characterByCharacter: Boolean = false,
    actionLabel: String? = null
) {
    editText.doOnTextChanged { text, _, _, _ ->
        bindTextInputButtonAccessibility(createContentDescription(label, text.toString(), characterByCharacter), actionLabel)
    }
}

private fun createContentDescription(label: String, value: String?, characterByCharacter: Boolean): String {
    val content = if (characterByCharacter) {
        value?.characterByCharacter()
    } else {
        value
    }
    return "$label ${content.orEmpty()}"
}

/**
 * Disables accessibility auto focus change when UI changes are executing
 */
fun View.executeWithoutAccessibilityFocusChange(block: () -> Unit) {
    if (context.accessibilityEnabled) {
        importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_NO_HIDE_DESCENDANTS
        block.invoke()
        post {
            postOnAnimationDelayed({ importantForAccessibility = View.IMPORTANT_FOR_ACCESSIBILITY_AUTO }, ACCESSIBILITY_FOCUS_DELAY_MS)
        }
    } else {
        block.invoke()
    }
}

// Talk back pronounces 'a' as 'uh' on some devices. Replacing it with 'ay' ensures proper phonetic pronunciation
fun String.characterByCharacter() = toCharArray().joinToString(" ").replace("a", "ay", true)

fun String.separateNumbers(): String {
    return """\d+"""
        .toRegex()
        .replace(this) {
            it.value.characterByCharacter()
        }
}

fun BigDecimal.toUSCurrency(): String {
    return "$" + this.toPlainString()
}

fun List<String>.listDescription(
    resources: Resources,
    @PluralsRes prefixRes: Int = R.plurals.default_list_desc_prefix
): String {
    if (isEmpty()) return ""
    return buildString {
        append("${resources.getQuantityString(prefixRes, size)} ")
        this@listDescription.forEachIndexed { index, content ->
            if (index > 0) {
                if (index == size - 1) {
                    append(" ${resources.getString(R.string.and)} ")
                } else {
                    append(", ")
                }
            }
            append(content)
        }
    }
}

@BindingAdapter("contentDescriptionDate")
fun TextView.setContentDescriptionDate(date: String?) {
    val emptyDate = resources.getString(R.string.empty_date_default)
    contentDescription = if (date.isNullOrEmpty() || date == emptyDate) {
        resources.getString(R.string.value_not_available)
    } else {
        date
    }
}
