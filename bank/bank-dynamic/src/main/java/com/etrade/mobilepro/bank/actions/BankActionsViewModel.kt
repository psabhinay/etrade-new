package com.etrade.mobilepro.bank.actions

import com.etrade.mobilepro.bank.BR
import com.etrade.mobilepro.bank.BankHomeFragmentDirections
import com.etrade.mobilepro.bank.R
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel

class BankActionsViewModel(private val bankAccountId: String?) : DynamicViewModel(R.layout.bank_actions) {

    override val variableId: Int = BR.obj

    fun onCheckDepositClicked() {
        clickEvents.value = CtaEvent.NavDirectionsNavigation(BankHomeFragmentDirections.toCheckDeposit(bankAccountId.orEmpty()))
    }

    fun onTransferMoneyClicked() {
        clickEvents.value = CtaEvent.UriEvent("etrade://show/transferMoney?fromAccountId=${bankAccountId.orEmpty()}")
    }

    fun onPayBillClicked() {
        clickEvents.value = CtaEvent.UriEvent("etrade://show/billpay?fromAccountId=${bankAccountId.orEmpty()}")
    }
}
