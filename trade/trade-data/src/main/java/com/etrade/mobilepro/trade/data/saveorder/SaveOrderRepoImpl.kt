package com.etrade.mobilepro.trade.data.saveorder

import com.etrade.mobilepro.backends.neo.toResponse
import com.etrade.mobilepro.common.di.IsTablet
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.api.SaveOrderRepo
import com.etrade.mobilepro.trade.api.SaveOrderResponse
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.data.form.input.DefaultAdvancedTradeFormInputId
import com.etrade.mobilepro.trade.data.form.input.DefaultTradeFormInputId
import com.etrade.mobilepro.trade.data.form.value.DefaultAdvancedOrderTypeValueId
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormInputIdMap
import com.etrade.mobilepro.trade.form.api.TradeFormType
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ConditionalTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.ContingentStockTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossOptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.StopLossStockTradeFormInputId
import com.etrade.mobilepro.util.ValueCoder
import com.etrade.mobilepro.util.json.toJsonString
import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import javax.inject.Inject

class SaveOrderRepoImpl @Inject constructor(
    conditionalOrderTradeFormInputId: ConditionalOrderTradeFormInputId,
    conditionalTradeFormInputId: ConditionalTradeFormInputId,
    contingentOptionTradeFormInputId: ContingentOptionTradeFormInputId,
    contingentStockTradeFormInputId: ContingentStockTradeFormInputId,
    stopLossOptionTradeFormInputId: StopLossOptionTradeFormInputId,
    stopLossStockTradeFormInputId: StopLossStockTradeFormInputId,
    @MobileTrade retrofit: Retrofit,
    tradeFormInputIdMap: TradeFormInputIdMap,
    tradeLegsCoder: ValueCoder<List<TradeLeg>>,
    @IsTablet private val isTablet: Boolean,
    private val moshi: Moshi
) : SaveOrderRepo {

    private val apiClient: SaveOrderApiClient = retrofit.create(SaveOrderApiClient::class.java)

    private val requestParametersProviders: Map<TradeFormType, SaveOrderRequestParametersProvider>

    init {
        val optionSaveOrderRequestBodyProvider = OptionSaveOrderRequestParametersProvider(tradeLegsCoder, tradeFormInputIdMap.securityTypeValueId, isTablet)

        requestParametersProviders = mapOf(
            TradeFormType(SecurityType.CONDITIONAL) to ConditionalSaveOrderRequestParametersProvider(
                conditionalOrderTradeFormInputId,
                conditionalTradeFormInputId
            ),
            TradeFormType(SecurityType.OPTION) to optionSaveOrderRequestBodyProvider,
            TradeFormType(SecurityType.OPTION, AdvancedTradeType.Contingent) to ContingentSaveOrderRequestBodyProvider(
                contingentOptionTradeFormInputId
            ),
            TradeFormType(SecurityType.OPTION, AdvancedTradeType.StopLoss) to StopLossSaveOrderRequestBodyProvider(
                stopLossOptionTradeFormInputId
            ),
            TradeFormType(SecurityType.STOCK_AND_OPTIONS) to optionSaveOrderRequestBodyProvider,
            TradeFormType(SecurityType.STOCK) to StockSaveOrderRequestParametersProvider(),
            TradeFormType(SecurityType.STOCK, AdvancedTradeType.Contingent) to ContingentSaveOrderRequestBodyProvider(
                contingentStockTradeFormInputId
            ),
            TradeFormType(SecurityType.STOCK, AdvancedTradeType.StopLoss) to StopLossSaveOrderRequestBodyProvider(
                stopLossStockTradeFormInputId
            )
        )
    }

    override suspend fun saveOrder(map: InputValuesSnapshot): ETResult<SaveOrderResponse> {
        return runCatchingET {
            val advancedTradeType = map.getAdvancedOrderType()
            val provider = map.getRequestBodyProvider(advancedTradeType)

            apiClient.saveOrder(provider.path, provider.body(map)).toResponse { data, warnings ->
                SaveOrderGeneralResponse(data.response, warnings)
            }
        }
    }

    private fun InputValuesSnapshot.getAdvancedOrderType(): AdvancedTradeType {
        return this[DefaultAdvancedTradeFormInputId.advancedOrderType]?.let {
            DefaultAdvancedOrderTypeValueId.converter.toValue(it as String)
        } ?: AdvancedTradeType.None
    }

    private fun InputValuesSnapshot.getRequestBodyProvider(
        advancedTradeType: AdvancedTradeType
    ): SaveOrderRequestParametersProvider {
        val securityType = this[DefaultTradeFormInputId.securityType]?.let {
            DefaultTradeFormInputId.securityTypeValueId.converter.toValue(it as String)
        }
        val tradeFormType = TradeFormType(securityType ?: SecurityType.STOCK, advancedTradeType)
        return tradeFormType.let { requestParametersProviders[it] }
            ?: throw IllegalArgumentException("Unknown security type")
    }

    private fun SaveOrderRequestParametersProvider.body(params: InputValuesSnapshot): String = moshi.toJsonString(createBody(params))
}
