package com.etrade.mobilepro.util.delegate

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun <T> consumable(initialValue: T): ReadWriteProperty<Any?, T?> = ConsumableProperty(initialValue)

/**
 * Delegate that represents a "consumable" property. Retrieving the value the first time "consumes" it, resulting in
 * every subsequent access returning `null`. Once a new value is set, it is no longer "consumed" until retrieved again
 * for the first time.
 */
class ConsumableProperty<T>(
    private var value: T? = null
) : ReadWriteProperty<Any?, T?> {

    private var consumed = false

    override fun getValue(thisRef: Any?, property: KProperty<*>): T? {
        return if (consumed) {
            null
        } else {
            consumed = true
            value
        }
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
        this.value = value
        consumed = false
    }
}
