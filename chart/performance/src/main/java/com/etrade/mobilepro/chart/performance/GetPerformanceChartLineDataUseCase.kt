package com.etrade.mobilepro.chart.performance

import com.etrade.mobilepro.chart.api.ChartLine
import com.etrade.mobilepro.chart.data.ChartParams
import com.etrade.mobilepro.chart.data.GetChartLineDataParameter
import com.etrade.mobilepro.chart.data.GetChartLineDataUseCase
import com.etrade.mobilepro.chart.model.ChartLineData
import com.etrade.mobilepro.chart.model.ChartLineSet
import com.etrade.mobilepro.chart.model.ChartTimeFrame
import com.etrade.mobilepro.chart.performance.data.PerformanceChartRepo
import com.etrade.mobilepro.chart.performance.model.PerformanceChartData
import com.etrade.mobilepro.chart.performance.model.toPerformanceChartPoint
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import java.util.Locale
import javax.inject.Inject

class GetPerformanceChartLineDataUseCase @Inject constructor(
    private val performanceChartRepo: PerformanceChartRepo
) : GetChartLineDataUseCase {

    override suspend fun execute(parameter: GetChartLineDataParameter): Observable<Resource<ChartLineData>> {
        return performanceChartRepo.getPerformanceChart(
            parameter.accounts.chartIds,
            parameter.indexes.chartIds,
            parameter.chartTimeFrame.timeFrame
        ).map { resource ->
            val performanceChartData = resource.data
            if (resource is Resource.Success && performanceChartData != null) {
                // Resource.Success with data may become Resource.Failed if mapping will fail.
                performanceChartData.toChartLineData(parameter)
            } else {
                // Resource.Loading and Resource.Failed are passed as is.
                resource.map { data -> data?.toChartLineData(parameter)?.data }
            }
        }
    }

    private val List<ChartParams>.chartIds: List<String>
        get() = map { it.chartId }.filter { it.isNotBlank() }

    private fun PerformanceChartData.toChartLineData(parameter: GetChartLineDataParameter): Resource<ChartLineData> {
        var exception: Exception? = null

        val chartTimeFrame = parameter.chartTimeFrame
        val chartLineData = ChartLineData(chartTimeFrame, true).apply {
            val accountParams = parameter.accounts.associateBy { it.chartId }
            accounts.forEach {
                addLineSet(it, accountParams, chartTimeFrame) { e ->
                    if (exception == null) {
                        exception = e
                    }
                }
            }

            val indexParams = parameter.indexes.associateBy { it.chartId }
            indexes.forEach {
                addLineSet(it, indexParams, chartTimeFrame) { e ->
                    if (exception == null) {
                        exception = e
                    }
                }
            }
        }

        return if (exception == null) {
            Resource.Success(chartLineData)
        } else {
            Resource.Failed(chartLineData, exception)
        }
    }

    private fun ChartLine.toLineSet(params: Map<String, ChartParams>, chartTimeFrame: ChartTimeFrame): ChartLineSet {
        val chartParams = params[normalizeChartId(chartId)] ?: throw IllegalArgumentException("There is no params for chartId = \"$chartId\"")
        var previousValue = 0.0
        return ChartLineSet(
            label = chartParams.displayName.toString(),
            color = chartParams.lineColor,
            baseClosingPrice = points.firstOrNull()?.toPerformanceChartPoint(previousValue)?.y ?: previousValue,
            showAsPercentage = false,
            isOption = false
        ).apply {
            points.forEach { point ->
                if (point.rawY.isNotEmpty()) {
                    previousValue = point.rawY.toDouble()
                }
                val performancePoint = point.toPerformanceChartPoint(previousValue)
                addPoint(performancePoint, chartTimeFrame)
            }
        }
    }

    private fun ChartLineData.addLineSet(
        line: ChartLine,
        chartParams: Map<String, ChartParams>,
        chartTimeFrame: ChartTimeFrame,
        onException: (Exception) -> Unit
    ) {
        try {
            addLineSet(line.toLineSet(chartParams, chartTimeFrame))
        } catch (e: IllegalArgumentException) {
            onException(e)
        }
    }

    private fun normalizeChartId(rawChartId: String): String {
        return when (val chartId = rawChartId.uppercase(Locale.ENGLISH)) {
            "COMP.ID" -> "COMP.IDX"
            "DJIN" -> "DJIND"
            "SP" -> "SPX"
            else -> chartId
        }
    }
}
