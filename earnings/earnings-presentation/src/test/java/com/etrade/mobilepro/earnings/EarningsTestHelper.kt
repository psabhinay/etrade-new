package com.etrade.mobilepro.earnings

import android.content.res.Resources
import com.etrade.mobilepro.earnings.dto.MarketPeriod
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock

fun mockResourcesString() = mock<Resources> {
    on { getString(R.string.empty_default) } doReturn ("--")
    on { getString(R.string.empty_date_default) } doReturn ("--/--/--")
    on { getString(R.string.earnings) } doReturn ("Earnings")
    on { getString(eq(R.string.earnings_date)) } doReturn ("Next earnings date %1\$s")
    on { getString(R.string.next_earnings_date_est) } doReturn ("est.")
    on { getString(R.string.earnings_release) } doReturn ("Earnings Release")
    on { getString(R.string.earnings_to_be_released_on) } doReturn ("Q%1\$s earnings to be released on %2\$s")
    on { getString(R.string.matches_analyst) } doReturn ("matches")
    on { getString(R.string.missing_analyst) } doReturn ("missing")
    on { getString(R.string.beating_analyst) } doReturn ("beating")
    on { getString(R.string.earnings_announcement) } doReturn ("Earnings Announcement")
    on { getString(R.string.announcement_text) } doReturn ("Q%1\$s earnings of \$%2\$s were announced on %3\$s, %4\$s analyst estimate")
    on { getString(R.string.announcement_text_by) } doReturn ("by \$%1\$s")
    on { getInteger(R.integer.quarterly_display_count) } doReturn (5)
    on { getString(R.string.market_graph_x_template) } doReturn ("Q%s\\'%s")
}

fun createMarkerEarningsResponseResult(period: List<MarketPeriod>?) = arrayListOf<EarningsDetailData>().apply {
    period?.map { data ->
        add(
            EarningsDetailData(
                high = data.high,
                low = data.low,
                estMean = data.estMean,
                quarter = data.quarter,
                expectedDate = data.expectedDate,
                announceDate = data.announceDate,
                announceTime = data.announceTime,
                actualValue = data.actualValue,
                surpriseMean = data.surpriseMean,
                fiscalPeriodYear = data.fiscalPeriodYear,
                dateTime = data.dateTime
            )
        )
    }
}
