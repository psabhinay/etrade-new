package com.etrade.mobilepro.iraaccount.data.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.iraaccount.api.IraContributionDetails
import com.etrade.mobilepro.iraaccount.api.repo.IraContributionDetailRepo
import com.etrade.mobilepro.iraaccount.data.fromDto
import com.etrade.mobilepro.iraaccount.data.rest.IraContributionInfoRequestDto
import com.etrade.mobilepro.iraaccount.data.rest.IraContributionService
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.Locale
import javax.inject.Inject

private val CASH_TRANSFER_DATE_FORMAT by lazy { DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.US) }

class DefaultIraContributionDetailRepo @Inject constructor(
    private val iraContributionService: IraContributionService
) : IraContributionDetailRepo {
    override suspend fun getIraContributionDetails(iraAccountNumber: String, instrumentNumber: String): ETResult<IraContributionDetails> = runCatchingET {
        val iraDetails = iraContributionService.iraContributionInfo(
            IraContributionInfoRequestDto(
                IraContributionInfoRequestDto.Value(
                    fromAcctNum = iraAccountNumber,
                    fromInstNum = instrumentNumber,
                    toAcctNum = iraAccountNumber,
                    toAcctType = "IRA",
                    toInstNum = instrumentNumber,
                    amount = "0",
                    enteredDate = LocalDate.now().format(CASH_TRANSFER_DATE_FORMAT)
                )
            )
        )

        iraDetails.doIfSuccessfulOrThrow {
            it.iraInfo.fromDto()
        }
    }
}
