package com.etrade.mobilepro.msaccounts.dao

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class MsAccountRealmModule
