package com.etrade.mobilepro.quotelookup.options.put

import androidx.lifecycle.Observer
import com.etrade.mobilepro.quotelookup.options.StockOptionLookupBaseFragment
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.util.android.consume

class StockPutOptionLookupFragment : StockOptionLookupBaseFragment() {
    override fun observeCurrentSymbol() {
        viewModel.currentPutSymbol.observe(
            viewLifecycleOwner,
            Observer { currentPutSymbol ->
                currentPutSymbol.consume { searchByQuery(it) }
            }
        )
    }

    override val optionSearchType: SearchType
        get() = SearchType.PUT_OPTION
}
