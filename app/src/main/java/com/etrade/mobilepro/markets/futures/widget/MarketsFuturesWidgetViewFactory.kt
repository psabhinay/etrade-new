package com.etrade.mobilepro.markets.futures.widget

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.DynamicWidget
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

class MarketsFuturesWidgetViewFactory @Inject constructor(
    private val futureIndicesLoader: FutureIndicesLoader
) : DynamicUiViewFactory {
    override fun createView(dto: Any, meta: List<Any>): GenericLayout = if (dto is MarketsFuturesViewDto) {
        object : GenericLayout, DynamicWidget {
            override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory { MarketsFuturesWidgetViewModel(futureIndicesLoader) }
            override val viewModelClass: Class<out DynamicViewModel> = MarketsFuturesWidgetViewModel::class.java
            override val type: DynamicWidget.Type = DynamicWidget.Type.FUTURES
            override fun refresh(viewModel: DynamicViewModel) {
                (viewModel as? MarketsFuturesWidgetViewModel)?.getFutureIndices()
            }
            override val uniqueIdentifier: String = javaClass.name
        }
    } else {
        throw DynamicUiViewFactoryException("MarketsFuturesWidgetViewFactory registered to unknown type")
    }
}
