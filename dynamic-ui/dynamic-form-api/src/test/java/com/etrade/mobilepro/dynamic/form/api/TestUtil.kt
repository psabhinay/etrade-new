package com.etrade.mobilepro.dynamic.form.api

import android.content.res.Resources
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever

fun mockResources(): Resources {
    return mock<Resources>().apply {
        whenever(getString(any())).thenAnswer { invocation ->
            invocation.arguments[0].toString()
        }
    }
}
