package com.etrade.mobilepro.realm

import com.etrade.mobilepro.account.dao.AccountDao
import com.etrade.mobilepro.accounts.model.RealmAccountDao
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.instrument.dao.InstrumentDao
import com.etrade.mobilepro.instrument.data.RealmInstrumentDao
import com.etrade.mobilepro.portfolio.data.RealmPortfolioPositionDao
import com.etrade.mobilepro.positions.dao.PortfolioPositionDao
import javax.inject.Inject

/**
 *  Prior to usage of this component to access realm db based data model, realm instances needs to be opened
 *  @see com.etrade.mobilepro.realm.RealmManager
 */
class DefaultRealmDaoProvider @Inject constructor(private val realmManager: RealmManager) :
    CachedDaoProvider {
    override fun open() {
        realmManager.open()
    }

    override fun close() {
        realmManager.close()
    }

    override fun getAccountDao(): AccountDao = RealmAccountDao(realmManager.getDefaultInstance())

    override fun getInstrumentDao(): InstrumentDao =
        RealmInstrumentDao(realmManager.getDefaultInstance())

    override fun getPositionsDao(): PortfolioPositionDao =
        RealmPortfolioPositionDao(realmManager.getDefaultInstance())
}
