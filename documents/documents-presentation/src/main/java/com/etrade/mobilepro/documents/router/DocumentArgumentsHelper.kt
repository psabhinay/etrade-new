package com.etrade.mobilepro.documents.router

import android.os.Bundle
import com.etrade.mobilepro.documents.DocumentType

interface DocumentArgumentsHelper {
    fun getDocumentId(arguments: Bundle?): String
    fun getAccountUuid(arguments: Bundle?): String?
    fun getDocumentType(arguments: Bundle?): DocumentType
}
