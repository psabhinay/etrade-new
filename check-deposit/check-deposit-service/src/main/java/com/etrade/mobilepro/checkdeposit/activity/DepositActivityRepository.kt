package com.etrade.mobilepro.checkdeposit.activity

import com.etrade.mobilepro.checkdeposit.activity.service.DepositActivityItemDto
import com.etrade.mobilepro.common.result.ETResult

interface DepositActivityRepository {
    suspend fun loadDepositActivityItems(): ETResult<List<DepositActivityItemDto>>
}
