package com.etrade.mobilepro.watchlist

import android.content.Context
import android.text.SpannedString
import android.text.style.RelativeSizeSpan
import android.widget.TextView
import androidx.core.text.getSpans
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.util.android.textutil.setSymbolSpan
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

private const val STOCK_TICKER = "APPL"
private const val STOCK_DESCRIPTON = "INVESCO QQQ TR UNIT SER 1"
private const val OPTION_TICKER = "SPX---211217P04000000"
private const val OPTION_DESCRIPTION = "SPX Dec 17 '21 $4000 Put"
private const val OPTION_DESCRIPTION_W = "SPX Dec 17 '21 $4000 Put w"
private const val OPTION_DESCRIPTION_Q = "SPX Dec 17 '21 $4000 Put q"
private const val EXPECTED_DESCRIPTION = "SPX \nDec 17 '21 \n$4000 Put "

@RunWith(AndroidJUnit4::class)
@Config(application = android.app.Application::class)
class WatchlistTextViewSpanTest {

    private lateinit var sutTextView: TextView

    private val context = getApplicationContext<Context>()

    @Before
    fun setUp() {
        sutTextView = TextView(context)
    }

    @Test
    fun `test stock textview no span`() {
        sutTextView.setSymbolSpan(STOCK_TICKER, InstrumentType.EQ, STOCK_DESCRIPTON)
        assertTrue("not spanned", sutTextView.text !is SpannedString)
        assertEquals(STOCK_TICKER, sutTextView.text)
    }

    @Test
    fun `test options description symbol span`() {
        sutTextView.setSymbolSpan(OPTION_TICKER, InstrumentType.OPTN, OPTION_DESCRIPTION)
        verifyOptionsSpan()

        sutTextView.setSymbolSpan(OPTION_TICKER, InstrumentType.OPTN, OPTION_DESCRIPTION_W)
        verifyOptionsSpan()

        sutTextView.setSymbolSpan(OPTION_TICKER, InstrumentType.OPTN, OPTION_DESCRIPTION_Q)
        verifyOptionsSpan()
    }

    private fun verifyOptionsSpan() {
        assertTrue("check textview is spanned", sutTextView.text is SpannedString)
        val spannedString: SpannedString = sutTextView.text as SpannedString
        val spans = spannedString.getSpans<RelativeSizeSpan>()
        assertEquals("number of spans is 1", 1, spans.size)
        assertEquals("proportion is 0.6f", 0.6f, spans[0].sizeChange)
        assertEquals("span start after SPX", 3, spannedString.getSpanStart(spans[0]))
        assertEquals("span ends at full length of description", EXPECTED_DESCRIPTION.length, spannedString.getSpanEnd(spans[0]))
        assertEquals("options description string", EXPECTED_DESCRIPTION, spannedString.toString())
    }
}
