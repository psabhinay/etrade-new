package com.etrade.mobilepro.dagger

import com.etrade.mobilepro.dagger.qualifiers.AmericanDateOnly
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import dagger.Module
import dagger.Provides
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Singleton

@Module
abstract class FormatterModule {

    companion object {

        @Provides
        @Singleton
        @AmericanDateOnly
        fun provideAmericanDateFormatter(): DateTimeFormatter = createAmericanSimpleDateFormatter()
    }
}
