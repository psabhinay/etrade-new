package com.etrade.mobilepro.compose.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.etrade.mobilepro.common.compose.theme.Dimens
import com.etrade.mobilepro.common.compose.theme.etColors

@Suppress("FunctionNaming")
fun LazyListScope.DisclosuresItem(onClick: () -> Unit) = item {
    Row(
        modifier = Modifier.fillMaxWidth().clickable(onClick = onClick),
        horizontalArrangement = Arrangement.Center
    ) {
        Text(
            stringResource(R.string.disclosures),
            modifier = Modifier.padding(Dimens.Size16dp),
            color = MaterialTheme.etColors.purple
        )
    }
}
