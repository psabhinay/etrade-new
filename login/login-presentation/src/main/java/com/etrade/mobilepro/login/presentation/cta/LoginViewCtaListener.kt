package com.etrade.mobilepro.login.presentation.cta

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil

interface LoginViewProvider {
    fun isRememberUserNameEnabled(): Boolean
    fun isUseSecurityCodeEnabled(): Boolean
    suspend fun getUserName(): ETResult<String>
    suspend fun setUserName(name: String)
}

interface LoginViewCtaListener {
    fun onCtaEvent(actionItem: LoginCtaItem, snackBarUtil: () -> SnackBarUtil)
}
