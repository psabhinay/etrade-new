package com.etrade.mobilepro.portfolio

import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import androidx.core.view.doOnLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.common.ScrollOnTopListener
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.home.AccountsTabHostFragmentArgs
import com.etrade.mobilepro.positions.PositionListFragment
import com.etrade.mobilepro.positions.PositionListViewModel
import com.etrade.mobilepro.preferences.ApplicationPreferences
import com.etrade.mobilepro.streaming.market.status.MarketTradeStatusDelegate
import com.etrade.mobilepro.tooltip.TOOLTIP_SHOW_DURATION_3SEC
import com.etrade.mobilepro.tooltip.Tooltip
import com.etrade.mobilepro.tooltip.TooltipGravity
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.darkmode.DarkModeChecker
import com.etrade.mobilepro.util.android.extension.awaitForMainThread
import com.etrade.mobilepro.util.android.extension.viewCoroutineScope
import com.etrade.mobilepro.util.android.goneUnless
import com.etrade.mobilepro.util.android.logAppAction
import com.etrade.mobilepro.util.android.recyclerviewutil.ViewLocator
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import org.slf4j.LoggerFactory
import java.lang.ref.WeakReference
import javax.inject.Inject
import kotlin.reflect.KClass

private const val SLIDE_ANIMATION_DELAY = 100L
const val PORTFOLIO_PATH = "/Portfolio"

private val logger = LoggerFactory.getLogger(PortfolioFragment::class.java)

/**
 * Designs for this fragment [https://etrade.invisionapp.com/d/#/projects/prototypes/15411437].
 */
@RequireLogin
internal class PortfolioFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    mainNavigation: MainNavigation,
    marketTradeStatusDelegate: MarketTradeStatusDelegate,
    darkModeChecker: DarkModeChecker,
    private val applicationPreferences: ApplicationPreferences
) : PositionListFragment<PortfolioViewModel>(
    viewModelFactory,
    snackbarUtilFactory,
    mainNavigation,
    marketTradeStatusDelegate,
    darkModeChecker
),
    ScrollOnTopListener {

    private val tooltipViewModel: PortfolioToolTipViewModel by viewModels { viewModelFactory }
    private var layoutManagerRef: WeakReference<LinearLayoutManager>? = null
    private val viewLocator: ViewLocator by lazy { ViewLocator(::awaitForMainThread) }
    private var tooltip: Tooltip? = null
    private var dataLoaded: Boolean = false

    private val mode: PortfolioPositionsDisplayMode?
        get() = portfolioRecordsViewModels.firstOrNull()?.positionsDisplayMode?.value

    private var bannerView: View? = null

    override val viewModelClass: KClass<PortfolioViewModel> = PortfolioViewModel::class

    private val accountUuid: String by lazy {
        parentFragment?.arguments?.let {
            AccountsTabHostFragmentArgs.fromBundle(
                it
            ).accountUuid
        } ?: ""
    }
    private val accountId: String by lazy {
        parentFragment?.arguments?.let {
            AccountsTabHostFragmentArgs.fromBundle(
                it
            ).accountId
        } ?: ""
    }

    override fun onCreate(viewModel: PortfolioViewModel) {
        viewModel.provideParameters(accountUuid, accountId)
        logAppAction(activity?.intent, PORTFOLIO_PATH)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        layoutManagerRef = (binding.contentView.layoutManager as? LinearLayoutManager)?.let(::WeakReference)

        applicationPreferences.displayPortfolioTooltip.observe(
            viewLifecycleOwner,
            Observer {
                showTooltipIfNeeded()
            }
        )

        bannerView = view.findViewById(R.id.position_refresh_banner)
        bannerView?.setOnClickListener {
            positionListViewModel.refresh()
        }

        positionListViewModel.dataUpdated.observe(
            viewLifecycleOwner,
            Observer {
                it.consume { showBanner ->
                    bannerView?.goneUnless(showBanner)
                }
            }
        )
    }

    override fun onViewStateChanged(viewState: PositionListViewModel.ViewState) {
        super.onViewStateChanged(viewState)

        dataLoaded = viewState is PositionListViewModel.ViewState.Idle

        showTooltipIfNeeded()
    }

    override fun onResume() {
        super.onResume()

        view?.postDelayed({ showTooltipIfNeeded() }, SLIDE_ANIMATION_DELAY)

        positionListViewModel.subscribeForPortfolioUpdates(false)
    }

    override fun onPause() {
        super.onPause()

        tooltipViewModel.dismiss(tooltip)
        tooltip = null

        positionListViewModel.pauseSubscription()
    }

    override fun onDestroyView() {
        bannerView = null
        super.onDestroyView()
    }

    override fun saveScrollState() {
        val view = view.positionsView ?: return
        view.getScrollState()?.let {
            if (mode != PortfolioPositionsDisplayMode.TABLE) {
                val layoutManager = view.cellLayoutManager
                it.rowPosition = layoutManager.findFirstVisibleItemPosition()
                it.rowPositionOffset = layoutManager.findViewByPosition(it.rowPosition)?.top ?: 0
            }
            positionListViewModel.saveScrollState(it)
        }
    }

    override fun onSwipeToRefresh() {
        tooltipViewModel.isToolTipEnabled = true
    }

    private fun canShowTooltip() = isResumed && dataLoaded

    private fun showTooltipIfNeeded() {
        if (canShowTooltip()) {
            binding.contentView.doOnLayout {
                when {
                    tooltipViewModel.showMoreInfoToolTip -> {
                        locateViewAndShowToolTip(
                            R.id.expandSummaryView,
                            R.string.tooltip_portfolio_more_info,
                            PortfolioToolTipViewModel.PortfolioToolTipType.MORE_INFO
                        )
                    }
                    hasPositions && tooltipViewModel.showControlBarToolTip -> {
                        locateViewAndShowToolTip(
                            R.id.tab_toggle,
                            R.string.tooltip_portfolio_control_bar,
                            PortfolioToolTipViewModel.PortfolioToolTipType.CONTROL_BAR
                        )
                    }
                    hasPositions && tooltipViewModel.showSettingsToolTip -> {
                        locateViewAndShowToolTip(
                            R.id.actionCustomizeView,
                            R.string.tooltip_portfolio_settings,
                            PortfolioToolTipViewModel.PortfolioToolTipType.SETTINGS
                        )
                    }
                }
            }
        }
    }

    private fun locateViewAndShowToolTip(
        targetViewId: Int,
        textRes: Int,
        toolTipType: PortfolioToolTipViewModel.PortfolioToolTipType
    ) {
        val linearLayoutManager = layoutManagerRef?.get() ?: return
        viewLifecycleOwner.lifecycleScope.launchWhenResumed {
            // Wait for any pending RecyclerView rendering operations before we traverse views.
            awaitForMainThread()
            viewLocator.locateView(linearLayoutManager, shouldScrollWithOffset(toolTipType)) { view ->
                view.id == targetViewId
            }?.let { targetView ->
                // Wait for any pending RecyclerView rendering operations before attaching the
                // tooltip to a view which is inside a RecyclerView.
                awaitForMainThread()
                runCatching {
                    showPortfolioToolTip(targetView, textRes, toolTipType)
                }.onFailure {
                    logger.debug("failed to show tooltip", it)
                }
            }
        }
    }

    private fun showPortfolioToolTip(
        targetView: View,
        textRes: Int,
        toolTipType: PortfolioToolTipViewModel.PortfolioToolTipType
    ) {
        tooltip = tooltipViewModel.show {
            target = targetView
            text = getString(textRes)
            gravity = TooltipGravity.BOTTOM
            durationParams =
                Tooltip.DurationParams(
                    viewCoroutineScope,
                    TOOLTIP_SHOW_DURATION_3SEC
                )
            onDismiss = { tooltipViewModel.onDismiss(toolTipType) }
        }
        tooltipViewModel.isToolTipEnabled = false
    }

    private fun shouldScrollWithOffset(toolTipType: PortfolioToolTipViewModel.PortfolioToolTipType): Boolean {
        return resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE &&
            toolTipType != PortfolioToolTipViewModel.PortfolioToolTipType.MORE_INFO
    }

    override val isOnTop get() = binding.contentView.computeVerticalScrollOffset() == 0

    override fun scrollUp() {
        binding.contentView.apply {
            stopScroll()
            scrollBy(0, -computeVerticalScrollOffset())
        }
    }
}
