package com.etrade.mobilepro.iraaccount.data.rest

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigInteger

@JsonClass(generateAdapter = true)
class IraContributionInfoResponseDto(

    @Json(name = "IRAInfo")
    val iraInfo: IraInfo

) : BaseDataDto() {

    @JsonClass(generateAdapter = true)
    class IraInfo(
        @Json(name = "acctNum")
        val acctNum: String,
        @Json(name = "instNum")
        val instNum: BigInteger,
        @Json(name = "contribYear")
        val contribYear: Int,
        @Json(name = "contribAmount")
        val contribAmount: String,
        @Json(name = "pendingAmount")
        val pendingAmount: String,
        @Json(name = "previousYearContrib")
        val previousYearContribution: ContributionAmount?,
        @Json(name = "currentYearContrib")
        val currentYearContrib: ContributionAmount,
        @Json(name = "validAmt")
        val validAmount: String,
        @Json(name = "availableYears")
        val availableYears: List<String?>?
    ) {

        @JsonClass(generateAdapter = true)
        class ContributionAmount(
            @Json(name = "contributedAmount")
            val contributedAmount: String, // e.g. "0.00"
            @Json(name = "contribMaxLimit")
            val contribMaxLimit: String,
            @Json(name = "taxYear")
            val taxYear: Int, // e.g. 2019
            @Json(name = "taxDueDate")
            val taxDueDate: String, // e.g. "04/15/2020 00:00:00"
            @Json(name = "xferDate")
            val transferDate: Long, // e.g. 1569604674441
            @Json(name = "reachedMaxAmount")
            val reachedMaxAmount: Boolean,
            @Json(name = "beforeTaxDue")
            val beforeTaxDue: Boolean,
            @Json(name = "contributionAmountLeft")
            val contributionAmountLeft: String // e.g. "0.00"
        )
    }
}
