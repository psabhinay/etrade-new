package com.etrade.mobilepro.dao.realm

import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.realm.RealmChangeListener
import io.realm.RealmModel
import io.realm.RealmResults

fun <T : RealmModel> RealmResults<T>.asObservable(): Observable<List<T>> {
    val subject: BehaviorSubject<List<T>> = BehaviorSubject.createDefault(realm.copyFromRealm(this))
    val listener = RealmChangeListener<RealmResults<T>> { result ->
        subject.onNext(result.realm.copyFromRealm(result))
    }

    return subject
        .doOnSubscribe {
            this.addChangeListener(listener)
        }
        .doFinally {
            this.removeChangeListener(listener)
        }
}
