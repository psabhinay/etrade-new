package com.etrade.mobilepro.screener.presentation.stock.technicals.percentinput

import com.etrade.mobilepro.inputvalueview.InputFieldManager
import java.util.regex.Pattern

private val pattern: Pattern by lazy { Pattern.compile("""[\d]{1,3}""") }

internal class PercentValidator(private val genericValidationErrorDesc: String) : InputFieldManager.Validator {
    override fun validate(value: String): InputFieldManager.Value {
        if (value.isEmpty()) {
            return InputFieldManager.Value.Invalid(value, genericValidationErrorDesc)
        }
        return if (pattern.matcher(value).matches()) {
            InputFieldManager.Value.Valid(value)
        } else {
            InputFieldManager.Value.Invalid(value, genericValidationErrorDesc)
        }
    }
}
