package com.etrade.mobilepro.chat.presentation.message.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.chat.api.ChatMessage
import com.etrade.mobilepro.chat.presentation.databinding.ChatItemMessageAgentBinding
import com.etrade.mobilepro.chat.presentation.message.viewHolder.ChatMessageAgentViewHolder
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class ChatMessageAgentAdapterDelegate : AbsListItemAdapterDelegate<ChatMessage.Agent, ChatMessage, ChatMessageAgentViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup) =
        ChatMessageAgentViewHolder(parent.viewBinding(ChatItemMessageAgentBinding::inflate))

    override fun isForViewType(item: ChatMessage, items: MutableList<ChatMessage>, position: Int) = item is ChatMessage.Agent

    override fun onBindViewHolder(item: ChatMessage.Agent, holder: ChatMessageAgentViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }
}
