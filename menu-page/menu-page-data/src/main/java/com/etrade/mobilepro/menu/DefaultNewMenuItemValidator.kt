package com.etrade.mobilepro.menu

import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.preferences.ApplicationPreferences
import javax.inject.Inject

private const val MAX_MENU_ITEM_CLICK_COUNT = 1
private const val MAX_VERSION_LOGIN = 4

data class NewMenuItem(
    val key: String,
    val newInVersion: String
)

open class DefaultNewMenuItemValidator @Inject constructor(
    private val prefs: ApplicationPreferences,
    private val newMenuItems: List<NewMenuItem>,
    private val appInfo: ApplicationInfo
) : NewMenuItemValidator {

    private val newMenuKeys: Set<String> by lazy {
        newMenuItems.filter { it.newInVersion == appInfo.appMajorMinorVersion }
            .map { it.key }
            .toSet()
    }

    override fun isNewMenuItem(menuItemKey: String?): Boolean {
        menuItemKey ?: return false
        if (!newMenuKeys.contains(menuItemKey)) return false
        if (prefs.versionLoginCount > MAX_VERSION_LOGIN) return false
        return prefs.menuItemClickCounts[menuItemKey] < MAX_MENU_ITEM_CLICK_COUNT
    }

    override fun incrementClickCount(menuItemKey: String?) {
        menuItemKey ?: return
        if (isNewMenuItem(menuItemKey)) prefs.menuItemClickCounts[menuItemKey]++
    }
}
