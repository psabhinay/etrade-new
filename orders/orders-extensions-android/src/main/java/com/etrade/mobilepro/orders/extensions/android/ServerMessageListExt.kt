package com.etrade.mobilepro.orders.extensions.android

import androidx.core.text.HtmlCompat
import com.etrade.mobilepro.backends.api.ServerMessage

fun List<ServerMessage>.asSingleMessage() = HtmlCompat.fromHtml(
    joinToString { it.text + System.lineSeparator() },
    HtmlCompat.FROM_HTML_MODE_LEGACY
).toString()
