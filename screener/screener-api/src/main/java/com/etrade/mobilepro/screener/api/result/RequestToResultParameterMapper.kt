package com.etrade.mobilepro.screener.api.result

import com.etrade.mobilepro.screener.api.ScreenerType

interface RequestToResultParameterMapper {
    fun map(group: ScreenerType.Group, requestParameters: List<String>): List<String>
}
