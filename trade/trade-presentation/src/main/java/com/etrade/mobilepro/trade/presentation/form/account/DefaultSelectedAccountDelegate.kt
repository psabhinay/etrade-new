package com.etrade.mobilepro.trade.presentation.form.account

import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject

class DefaultSelectedAccountDelegate @AssistedInject constructor(
    private val tradingDefaults: TradingDefaultsPreferences,
    @Assisted private val accounts: List<TradeAccount>,
    @Assisted private val fallbackAccountId: String?
) : SelectedAccountDelegate {

    @AssistedFactory
    interface Factory : SelectedAccountDelegate.Factory {
        override fun create(accounts: List<TradeAccount>, fallbackAccountId: String?): DefaultSelectedAccountDelegate
    }

    private var selectedAccountId: String? = null
    private var lastSecurityType: SecurityType? = null

    override fun getCurrentAccountId(securityType: SecurityType): String? {
        // if the user has actively selected an account ID, use that
        selectedAccountId.ifValidAccount { return it }

        // Otherwise, if the user has set a valid trading default account ID, use that.
        getTradingDefault(securityType)?.let { return it }

        // Otherwise, use the fallback account id, if it is valid
        fallbackAccountId.ifValidAccount { return it }

        // Otherwise, use the first account's id
        return accounts.firstOrNull()?.id
    }

    override fun setUserSelectedAccountId(securityType: SecurityType, selectedAccountId: String) {
        // Note that we do not actually save a per-SecurityType account id. Once the user has actively selected an
        // account ID, that ID will continue to be used for every security type.
        lastSecurityType = securityType
        this.selectedAccountId = selectedAccountId
    }

    override fun onSecurityType(securityType: SecurityType) {
        if (securityType != lastSecurityType) {
            getTradingDefault(securityType)?.let { selectedAccountId = null }
            lastSecurityType = securityType
        }
    }

    private fun getTradingDefault(securityType: SecurityType): String? {
        when (securityType) {
            SecurityType.OPTION -> tradingDefaults.optionsAccountId
            SecurityType.STOCK -> tradingDefaults.stocksAccountId
            else -> null
        }.ifValidAccount { return it }
        return null
    }

    private inline fun String?.ifValidAccount(block: (String) -> Unit) {
        this?.let { if (accounts.any { account -> account.id == this }) block(this) }
    }
}
