package com.etrade.mobilepro.quote.screen.presentation.ui.performance

import android.content.res.Resources
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.quote.screen.presentation.R
import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.quoteapi.FundQuoteItem
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.invoke
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.coroutines.Job
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory

internal const val NON_ADJUSTED = 0
internal const val ADJUSTED = 1

class FundPerformanceViewModel @AssistedInject constructor(
    private val resources: Resources,
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase,
    private val fundQuoteDetailItemsMapper: FundQuoteDetailItemsMapper,
    private val quoteService: MobileQuoteRepo,
    @Assisted val symbol: SearchResultItem.Symbol
) : ViewModel(), LifecycleObserver {

    private val compositeDisposable = CompositeDisposable()
    private var nonAdjustedFundPerformance: FundPerformance = FundPerformance()
    private var adjustedFundPerformance: FundPerformance = FundPerformance()
    lateinit var disclosureClickListener: (() -> Unit)
    private val _symbol = MutableLiveData<SearchResultItem.Symbol>()
    private var activeJob: Job? = null

    private val logger = LoggerFactory.getLogger(FundPerformanceViewModel::class.java)

    private val _fundPerformance: MutableLiveData<FundPerformance> by lazy {
        MutableLiveData<FundPerformance>().apply {
            value = nonAdjustedFundPerformance
        }
    }
    val fundPerformance: LiveData<FundPerformance> by lazy { _fundPerformance }

    var tableViewIndex: Int = NON_ADJUSTED
        set(value) {
            field = value
            updateValues(value)
        }

    @AssistedFactory
    interface Factory {
        fun create(symbol: SearchResultItem.Symbol): FundPerformanceViewModel
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }

    fun getMobileQuote(ticker: SearchResultItem.Symbol) {
        _symbol.value = ticker
        runBlocking {
            fetch()
        }
    }

    private suspend fun fetch() {
        val ticker = _symbol.value ?: return
        activeJob?.cancel()
        compositeDisposable += quoteService.loadMobileQuote(
            ticker.title,
            requireEarningsDate = false,
            extendedHours = isExtendedHoursOnUseCase(),
            requireExchangeCode = false
        ).subscribeBy(
            onError = { exception ->
                logger.error("Could not load quote(s)", exception)
            },
            onNext = {
                handleDeferred(it)
            }
        )
    }

    private fun handleDeferred(quotesDeferred: MobileQuote) {
        quotesDeferred.let {
            if (it.typeCode.isMutualFundFund) {
                val fundCategoryPerformanceItem = fundQuoteDetailItemsMapper.getFundCategoryPerformanceItem(it)
                setPerformanceItem(fundCategoryPerformanceItem)
            }
        }
    }

    private fun updateValues(tab: Int) {
        if (tab == ADJUSTED) {
            _fundPerformance.postValue(adjustedFundPerformance)
        } else {
            _fundPerformance.postValue(nonAdjustedFundPerformance)
        }
    }

    fun setPerformanceItem(performanceItem: FundQuoteItem.FundPerformanceItem) {
        nonAdjustedFundPerformance = mapFundPerformanceNonAdjusted(
            performanceItem,
            resources.getString(R.string.quote_performance_non_load_adjusted)
        )
        adjustedFundPerformance = mapFundPerformanceAdjusted(
            performanceItem,
            resources.getString(R.string.quote_performance_load_adjusted)
        )
        updateValues(tableViewIndex)
    }
}
