package com.etrade.mobilepro.chart.model

data class ChartLineEntry(
    val lineDataSetLabel: String,
    val entryX: Float,
    val entryY: Float
)
