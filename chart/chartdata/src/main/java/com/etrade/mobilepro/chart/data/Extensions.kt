package com.etrade.mobilepro.chart.data

import com.etrade.mobilepro.chart.api.ChartLine

fun AccountDataDto.toChartLine(): ChartLine = ChartLine(accountNumber.orEmpty(), points)

fun IndexDataDto.toChartLine(): ChartLine = ChartLine(index, points)
