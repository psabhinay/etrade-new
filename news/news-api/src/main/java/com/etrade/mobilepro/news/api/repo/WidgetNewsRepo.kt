package com.etrade.mobilepro.news.api.repo

import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.api.model.NewsRequest
import io.reactivex.Single

interface WidgetNewsRepo {
    fun getWidgetNews(request: NewsRequest, amount: Int): Single<List<NewsItem>>
}
