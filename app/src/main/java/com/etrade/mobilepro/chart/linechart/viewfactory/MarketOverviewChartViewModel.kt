package com.etrade.mobilepro.chart.linechart.viewfactory

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.Transformations
import com.etrade.mobilepro.BR
import com.etrade.mobilepro.R
import com.etrade.mobilepro.chart.data.ChartParams
import com.etrade.mobilepro.chart.data.GetChartLineDataParameter
import com.etrade.mobilepro.chart.data.GetChartLineDataUseCase
import com.etrade.mobilepro.chart.model.ChartLineData
import com.etrade.mobilepro.chart.model.ChartLineEntry
import com.etrade.mobilepro.chart.model.ChartTimeFrame
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StockData
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.executeBlocking
import com.etrade.mobilepro.util.market.time.isMarketTime
import com.etrade.mobilepro.util.safeParseBigDecimal
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import org.threeten.bp.Duration
import org.threeten.bp.temporal.ChronoUnit
import javax.inject.Inject

class MarketOverviewChartViewModel @Inject constructor(
    private val getChartLineDataUseCase: GetChartLineDataUseCase,
    private val markets: List<ChartParams>,
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any>,
    private val streamingStatusController: StreamingStatusController
) : DynamicViewModel(R.layout.market_overview_chart), LifecycleObserver {

    override val variableId: Int = BR.obj

    private val compositeDisposable = CompositeDisposable()
    private val chartStreamDisposable = CompositeDisposable()

    private val _streamingUpdate: MutableLiveData<ChartLineEntry> = MutableLiveData()
    val streamingUpdate: LiveData<ChartLineEntry>
        get() = _streamingUpdate

    private val mutableLineData: MutableLiveData<ChartLineData> = MutableLiveData()
    val lineData: LiveData<ChartLineData>
        get() = Transformations.map(mutableLineData) {
            subscribeToLiveStreaming()
            it
        }

    private val mutableSelectedTimeFrame: MutableLiveData<ChartTimeFrame> = MutableLiveData()
    val selectedTimeFrame: LiveData<ChartTimeFrame>
        get() = mutableSelectedTimeFrame

    private val _defaultSelectedIndex: MutableLiveData<Int> = MutableLiveData()
    val defaultSelectedIndex: LiveData<Int>
        get() = _defaultSelectedIndex

    private val mutableIsChartLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isChartLoading: LiveData<Boolean>
        get() = mutableIsChartLoading

    /** This list needs to match <string-array name="overview_chart_time_frames"> */
    private val timeFrames = listOf(
        ChartTimeFrame.Day,
        ChartTimeFrame.OneWeek,
        ChartTimeFrame.OneMonth,
        ChartTimeFrame.ThreeMonths,
        ChartTimeFrame.SixMonths,
        ChartTimeFrame.OneYear,
        ChartTimeFrame.TwoYears,
        ChartTimeFrame.ThreeYears
    )

    private val chartSubscriptionFields: Set<Level1Field> = setOf(Level1Field.DAY_CHANGE_PERCENT, Level1Field.TIMESTAMP_SINCE_EPOCH)

    init {
        if (isMarketTime()) {
            mutableSelectedTimeFrame.value = ChartTimeFrame.Day
            _defaultSelectedIndex.value = 0
        } else {
            mutableSelectedTimeFrame.value = ChartTimeFrame.OneWeek
            _defaultSelectedIndex.value = 1
        }
    }

    val retry: () -> Unit = {
        loadChart()
    }

    /** Used by layout to bind to Charts data for a selected timeframe */
    fun bindChart(index: Int) {
        mutableSelectedTimeFrame.value = timeFrames.getOrNull(index)
        _defaultSelectedIndex.value = index
        loadChart()
    }

    private fun loadChart() {
        selectedTimeFrame.value?.let { chartTimeFrame ->
            compositeDisposable.clear()
            compositeDisposable.add(
                getChartLineDataUseCase.executeBlocking(GetChartLineDataParameter(markets, emptyList(), chartTimeFrame)).subscribeBy(
                    onNext = { response ->
                        when (response) {
                            is Resource.Success -> {
                                mutableLineData.postValue(response.data)
                                mutableIsChartLoading.postValue(false)
                            }
                            is Resource.Loading -> {
                                if (lineData.value == null) {
                                    mutableIsChartLoading.postValue(true)
                                }
                            }
                            is Resource.Failed -> {
                                clearChart()
                            }
                        }
                    },
                    onError = {
                        clearChart()
                    }
                )
            )
        }
    }

    /** This method needs to be get called right after snapshot chart data becomes available */
    private fun subscribeToLiveStreaming() {
        if (streamingStatusController.isStreamingToggleEnabled && selectedTimeFrame.value == ChartTimeFrame.Day) {
            chartStreamDisposable.clear()
            markets.forEach { market ->
                chartStreamDisposable.add(
                    streamingQuoteProvider.getQuote(market.chartId, InstrumentType.INDX, chartSubscriptionFields)
                        .subscribeBy { data ->
                            if (data is StockData) {
                                val timestampSinceEpoch = data.timestampSinceEpoch
                                val changePercent = data.dayChangePercent?.safeParseBigDecimal()?.toFloat()
                                if (timestampSinceEpoch != null && changePercent != null) {
                                    handleIndexUpdate(market.chartId, timestampSinceEpoch, changePercent)
                                }
                            }
                        }
                )
            }
        } else if (!streamingStatusController.isStreamingToggleEnabled) {
            chartStreamDisposable.clear()
        }
    }

    private fun handleIndexUpdate(symbol: String, timestampSinceEpoch: Long, changePercentage: Float) {
        val newXValue = Duration.of(timestampSinceEpoch, ChronoUnit.MILLIS).toMinutes().toFloat()
        _streamingUpdate.postValue(ChartLineEntry(symbol, newXValue, changePercentage))
    }

    private fun clearChart() {
        mutableLineData.postValue(null)
        mutableIsChartLoading.postValue(false)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        chartStreamDisposable.clear()
        super.onCleared()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onLifeCyclePause() {
        /** We want to unsubscribe from live chart streaming */
        chartStreamDisposable.clear()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onLifeCycleResume() {
        /** We also need to subscribe live chart streaming here because gets unsubscribe onPause lifecycle */
        subscribeToLiveStreaming()
    }
}
