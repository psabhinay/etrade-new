package com.etrade.mobilepro.trade.data

import android.content.res.Resources
import com.etrade.mobile.accounts.dto.InstitutionType
import com.etrade.mobilepro.R
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.trade.api.AccountMode
import com.etrade.mobilepro.trade.api.EMPTY_ACCOUNT_ID
import com.etrade.mobilepro.trade.api.TradeAccount
import com.etrade.mobilepro.trade.api.TradeAccountRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LocalTradeAccountRepo @Inject constructor(
    private val daoProvider: AccountDaoProvider,
    private val resources: Resources
) : TradeAccountRepo {

    override suspend fun getTradeAccounts(): ETResult<List<TradeAccount>> {
        return withContext(Dispatchers.Main) {
            runCatchingET { daoProvider.getAccountDao().getAllAccountsNonAsync() }
                .map { accounts ->
                    accounts.asSequence()
                        .filter { it.institutionType == InstitutionType.ADP || it.institutionType == InstitutionType.MSSB }
                        .sortedBy { it.accountIndex }
                        .map { it.toTradeAccount() }
                        .toList()
                        .takeIf { it.isNotEmpty() }
                        ?: listOf(resources.createEmptyAccount())
                }
        }
    }

    private fun Account.toTradeAccount(): TradeAccount {
        val am = accountMode
        return object : TradeAccount {
            override val id: String
                get() = accountId
            override val name: String
                get() = accountShortName
            override val accountMode: AccountMode
                get() {
                    return if (isIra == true) {
                        AccountMode.IRA
                    } else {
                        when (am) {
                            "CASH" -> AccountMode.CASH
                            "MARGIN" -> AccountMode.MARGIN
                            "PM" -> AccountMode.PORTFOLIO_MARGIN
                            else -> AccountMode.UNKNOWN
                        }
                    }
                }
            override val approvalLevel: Int
                get() = optionLevel ?: 0
        }
    }
}

private fun Resources.createEmptyAccount(): TradeAccount {
    return object : TradeAccount {
        override val id: String = EMPTY_ACCOUNT_ID
        override val name: String = getString(R.string.trade_warning_no_eligible_accounts)
        override val accountMode: AccountMode = AccountMode.MARGIN
        override val approvalLevel: Int = 0
    }
}
