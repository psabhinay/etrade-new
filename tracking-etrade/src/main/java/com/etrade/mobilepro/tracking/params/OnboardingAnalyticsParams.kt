package com.etrade.mobilepro.tracking.params

import com.etrade.mobilepro.tracking.trackerValue

private const val ONBOARDING_EVENT = "Onboarding Event"
private const val LOGIN_TAPPED = "Login Tapped"
private const val OPEN_ACCOUNT_TAPPED = "Open Account Tapped"
private const val PAGES_VIEWED = "Pages Viewed"
private const val BUILT_IN_FLOW = "Built-in flow was shown"
private const val BACKUP_BUILT_IN_FLOW = "Built-in flow was shown as backup"
private const val LOCALYTICS_FLOW = "Localytics flow was shown"

class OnboardingAnalyticsParams(
    private val viewedPages: List<Int>,
    private val action: Action
) : AnalyticsParams {

    override val name: String = ONBOARDING_EVENT

    override val params: Map<String, String>
        get() = mapOf(
            OPEN_ACCOUNT_TAPPED to (action is Action.OpenAccount).trackerValue,
            LOGIN_TAPPED to (action is Action.Login).trackerValue,
            PAGES_VIEWED to viewedPages.joinToString(","),
            BUILT_IN_FLOW to (action is Action.BuiltInOnboardingFlow).trackerValue,
            BACKUP_BUILT_IN_FLOW to (action is Action.BackUpBuiltInOnboardingFlow).trackerValue,
            LOCALYTICS_FLOW to (action is Action.LocalyticsOnboardingFlow).trackerValue
        )

    sealed class Action {

        object Login : Action()
        object OpenAccount : Action()
        object BuiltInOnboardingFlow : Action()
        object BackUpBuiltInOnboardingFlow : Action()
        object LocalyticsOnboardingFlow : Action()
    }
}
