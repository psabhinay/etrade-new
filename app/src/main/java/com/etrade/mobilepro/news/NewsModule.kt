package com.etrade.mobilepro.news

import com.etrade.mobilepro.caching.ResponseModel
import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.readstatus.DefaultReadStatusRepo
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.dynamicui.CachedResponseModel
import com.etrade.mobilepro.news.api.model.NewsSettings
import com.etrade.mobilepro.news.api.repo.NewsRepo
import com.etrade.mobilepro.news.api.repo.WidgetNewsRepo
import com.etrade.mobilepro.news.briefing.api.BriefingNewsRepo
import com.etrade.mobilepro.news.briefing.data.repo.DefaultBriefingNewsRepo
import com.etrade.mobilepro.news.briefing.data.rest.BriefingNewsRequest
import com.etrade.mobilepro.news.caching.BriefingNewsDataSourceDelegate
import com.etrade.mobilepro.news.data.repo.DefaultNewsRepo
import com.etrade.mobilepro.news.data.repo.DefaultWidgetNewsRepo
import com.etrade.mobilepro.news.data.rest.NewsApiClient
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.news.presentation.NewsSetupResolver
import com.etrade.mobilepro.news.presentation.adapter.AddItemsAnimator
import com.etrade.mobilepro.news.presentation.detail.helper.DefaultTextNewsContentBindHelper
import com.etrade.mobilepro.news.presentation.detail.helper.DefaultTextNewsContentLoader
import com.etrade.mobilepro.news.presentation.detail.helper.DefaultTextNewsFontAdjustHandler
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentBindHelper
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentLoader
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsFontAdjustHandler
import com.etrade.mobilepro.watchlist.repo.CombinedWatchlistRepo
import com.etrade.mobilepro.watchlist.repo.DeviceWatchlistRepo
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(
    includes = [
        NewsFilterModule::class
    ]
)
class NewsModule {

    @Provides
    internal fun provideAddItemsAnimator(): AddItemsAnimator = AddItemsAnimator()

    @Provides
    @Singleton
    internal fun provideNewsApiClient(@MobileTrade retrofit: Retrofit): NewsApiClient = retrofit.create(NewsApiClient::class.java)

    @Provides
    internal fun provideNewsRepo(apiClient: NewsApiClient, deviceWatchlistRepo: DeviceWatchlistRepo, combinedWatchlistRepo: CombinedWatchlistRepo): NewsRepo {
        return DefaultNewsRepo(apiClient, NewsSettings(), deviceWatchlistRepo, combinedWatchlistRepo)
    }

    @Provides
    @Singleton
    internal fun provideNewsStatusesRepo(): ReadStatusRepo = DefaultReadStatusRepo()

    @Provides
    internal fun provideNewsContentLoader(repo: NewsRepo): TextNewsContentLoader = DefaultTextNewsContentLoader(repo)

    @Provides
    internal fun provideNewsContentFontAdjustHandler(): TextNewsFontAdjustHandler = DefaultTextNewsFontAdjustHandler()

    @Provides
    internal fun provideTextNewsContentBindHelper(): TextNewsContentBindHelper = DefaultTextNewsContentBindHelper()

    @Provides
    @Singleton
    internal fun provideBriefingNewsDataSourceDelegate(@MobileTrade retrofit: Retrofit):
        SimpleCachedResource.DataSourceDelegate<CachedResponseModel, BriefingNewsRequest> = BriefingNewsDataSourceDelegate(retrofit)

    @Provides
    @Singleton
    internal fun provideBriefingNewsRepo(delegate: SimpleCachedResource.DataSourceDelegate<CachedResponseModel, BriefingNewsRequest>): BriefingNewsRepo {
        @Suppress("UNCHECKED_CAST")
        val requiredDelegate = delegate as SimpleCachedResource.DataSourceDelegate<ResponseModel, BriefingNewsRequest>
        return DefaultBriefingNewsRepo(requiredDelegate)
    }

    @Provides
    @Singleton
    internal fun provideWidgetNewsRepo(apiClient: NewsApiClient): WidgetNewsRepo = DefaultWidgetNewsRepo(apiClient)

    @Provides
    fun bindNewsRouter(newsRouter: DefaultNewsRouter): NewsRouter = newsRouter

    @Provides
    fun bindNewsSetupResolver(resolver: DefaultNewsSetupResolver): NewsSetupResolver = resolver
}
