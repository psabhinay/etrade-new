package com.etrade.mobilepro.settings.viewmodel

import com.etrade.mobilepro.BR
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.settings.dto.SettingsViewType

open class SettingDynamicViewModel(
    open val title: String,
    open val value: String? = null,
    override val layoutId: Int,
    open val viewType: SettingsViewType = SettingsViewType.UNKNOWN,
    override val variableId: Int = BR.obj
) : DynamicViewModel(layoutId)
