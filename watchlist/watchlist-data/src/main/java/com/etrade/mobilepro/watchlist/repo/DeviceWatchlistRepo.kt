package com.etrade.mobilepro.watchlist.repo

import android.content.res.Resources
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.watchlist.R
import com.etrade.mobilepro.watchlist.mapper.toWatchlistEntry
import com.etrade.mobilepro.watchlistapi.DEVICE_WATCHLIST_ID
import com.etrade.mobilepro.watchlistapi.DuplicateSymbolException
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import io.reactivex.Single
import javax.inject.Inject

internal const val KEY_DEVICE_WATCHLIST_TICKERS = "deviceWatchlistTickers"

internal const val TICKERS_DELIMITER = ","

/**
 * Repository for [Watchlist]'s stored locally on a user's device.
 */
class DeviceWatchlistRepo @Inject constructor(
    private val storage: KeyValueStorage,
    private val quoteRepo: MobileQuoteRepo,
    private val res: Resources
) : WatchlistRepo {

    private var tickers: String
        get() = storage.getStringValue(KEY_DEVICE_WATCHLIST_TICKERS) ?: ""
        set(value) {
            storage.putStringValue(KEY_DEVICE_WATCHLIST_TICKERS, value)
        }

    override fun getUserWatchlists(): Single<List<Watchlist>> =
        Single.just(
            listOf(
                Watchlist(
                    id = DEVICE_WATCHLIST_ID,
                    uuid = DEVICE_WATCHLIST_ID,
                    name = res.getString(R.string.device_watch_list)
                )
            )
        )

    override fun getWatchlistEntries(watchlistId: String, isExtendedHours: Boolean): Single<List<WatchlistEntry>> {
        val currentTickers = tickers
        if (currentTickers.isEmpty()) {
            return Single.just(emptyList())
        }

        val tickersArray = currentTickers.split(TICKERS_DELIMITER).withIndex().associate { it.value to it.index }

        return quoteRepo.loadMobileQuotes(symbols = currentTickers, extendedHours = isExtendedHours)
            .map { items ->
                items
                    .distinct()
                    .filter { quote -> tickersArray.containsKey(quote.symbol) }
                    .map { quote -> quote.toWatchlistEntry(quote.symbol, isExtendedHours) }
                    .sortedWith(WatchlistEntryComparator(tickersArray))
            }
            .let { Single.fromObservable(it) }
    }

    override fun getWatchlistSymbols(watchlistId: String): Single<List<String>> {
        return Single.just(
            if (tickers.isEmpty()) {
                emptyList()
            } else {
                tickers.split(TICKERS_DELIMITER)
            }
        )
    }

    override fun addToWatchList(watchlistId: String, ticker: String): Single<Boolean> {
        checkWatchlistId(watchlistId)
        if (ticker.isBlank()) throw IllegalArgumentException("Ticker must have a name.")
        if (ticker.contains(TICKERS_DELIMITER)) throw IllegalArgumentException("Ticker must not contain a comma.")
        val oldTickers = tickers
        tickers = when {
            oldTickers.isEmpty() -> ticker
            oldTickers.split(TICKERS_DELIMITER).contains(ticker) -> return Single.fromCallable {
                throw DuplicateSymbolException()
            }
            else -> "$ticker,$tickers"
        }
        return Single.just(true)
    }

    override fun saveWatchList(watchlistId: String, entries: List<WatchlistEntry>, removedEntries: List<WatchlistEntry>): Single<Boolean> {
        checkWatchlistId(watchlistId)
        tickers = entries.map { it.ticker }.distinct().joinToString(",")
        return Single.just(true)
    }

    override fun createWatchlist(watchlistName: String): Single<Watchlist> {
        throw UnsupportedOperationException("There can be only one Device Watchlist.")
    }

    override fun deleteWatchList(watchlistId: String): Single<Boolean> {
        throw UnsupportedOperationException("Device Watchlist can not be deleted.")
    }

    private fun checkWatchlistId(watchlistId: String) {
        if (watchlistId != DEVICE_WATCHLIST_ID) throw IllegalArgumentException("Invalid watchlist ID: $watchlistId")
    }

    private class WatchlistEntryComparator(private val tickers: Map<String, Int>) : Comparator<WatchlistEntry> {

        override fun compare(o1: WatchlistEntry, o2: WatchlistEntry): Int {
            val i1 = tickers[o1.ticker] ?: throw IllegalArgumentException("Unsupported entry: $o1")
            val i2 = tickers[o2.ticker] ?: throw IllegalArgumentException("Unsupported entry: $o2")
            return i1.compareTo(i2)
        }
    }

    override fun getAllWatchlistsEntries(isExtendedHours: Boolean): Single<List<WatchlistEntry>> = getWatchlistEntries(DEVICE_WATCHLIST_ID, isExtendedHours)
}
