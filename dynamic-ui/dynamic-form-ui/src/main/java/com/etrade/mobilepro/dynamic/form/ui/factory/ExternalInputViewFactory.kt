package com.etrade.mobilepro.dynamic.form.ui.factory

import android.content.Context
import com.etrade.mobilepro.dynamic.form.api.ExternalInput
import com.etrade.mobilepro.dynamic.form.ui.ExternalInputView

internal object ExternalInputViewFactory : ComponentViewFactory<ExternalInput<*>, ExternalInputView> {
    override fun createView(context: Context): ExternalInputView = ExternalInputView(context)
}
