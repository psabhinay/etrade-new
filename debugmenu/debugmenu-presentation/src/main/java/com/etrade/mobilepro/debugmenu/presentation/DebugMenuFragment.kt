package com.etrade.mobilepro.debugmenu.presentation

import android.os.Bundle
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.etrade.mobilepro.debugmenu.api.EnvChange
import com.etrade.mobilepro.debugmenu.presentation.adapter.DebugMenuAdapter
import com.etrade.mobilepro.debugmenu.presentation.databinding.DebugmenuFragmentDebugmenuBinding
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.extension.hideSoftKeyboard
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import javax.inject.Inject
import kotlin.system.exitProcess

class DebugMenuFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory
) : Fragment(R.layout.debugmenu_fragment_debugmenu) {
    private val viewModel: DebugMenuViewModel by viewModels { viewModelFactory }
    private val binding by viewBinding(DebugmenuFragmentDebugmenuBinding::bind)
    private val menuAdapter = DebugMenuAdapter(
        timeoutListener = { limitTo30Sec ->
            viewModel.updateTimeout(limitTo30Sec)
        },
        environmentSelectedListener = {
            viewModel.updateAppEnvironment(it.value)
        },
        circuitBreakerListener = {
            viewModel.toggleCircuitBreaker(it)
        },
        featureFlagsListener = {
            viewModel.navigateToFeatureFlags()
        }
    )

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        setupFragment()
        bindViewModel()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupFragment() {
        with(binding.recyclerView) {
            layoutManager = LinearLayoutManager(requireContext())
            setupRecyclerViewDivider(R.drawable.thin_divider)
            adapter = menuAdapter
            itemAnimator = null
        }
        binding.apply {
            ok.setOnClickListener {
                viewModel.updateEnvironment(name = name.text?.toString(), pwd = password.text?.toString())
            }
            cancel.setOnClickListener {
                viewModel.cancelEnvironmentUpdate()
            }
            restart.setOnClickListener {
                restartApp()
            }
            password.setOnEditorActionListener { _, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE || event.keyCode == KeyEvent.KEYCODE_ENTER) {
                    view?.hideSoftKeyboard()
                    viewModel.updateEnvironment(name = name.text?.toString(), pwd = password.text?.toString())
                }
                false
            }
        }
        viewModel.loadData()
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            {
                menuAdapter.submitItems(it.items)
                when (it.status) {
                    DebugMenuViewModel.ViewState.Status.Idle -> handleIdleStatus()
                    DebugMenuViewModel.ViewState.Status.Restart -> handleRestartStatus()
                    DebugMenuViewModel.ViewState.Status.ExitScreen -> activity?.onBackPressed()
                    DebugMenuViewModel.ViewState.Status.EnterCredentials.Idle -> handleCredIdleStatus()
                    is DebugMenuViewModel.ViewState.Status.EnterCredentials.CredError.AuthError,
                    DebugMenuViewModel.ViewState.Status.EnterCredentials.CredError.NetworkError,
                    DebugMenuViewModel.ViewState.Status.EnterCredentials.CredError.NoAppEnvSelected -> handleCredErrorStatus(it)
                    DebugMenuViewModel.ViewState.Status.EnterCredentials.Loading -> handleCredLoadingStatus()
                }
                binding.credentialsSubtitle.text = getString(R.string.debugmenu_cred_header_description, it.appEnvCandidate?.asString)
            }
        )

        viewModel.navigateActions.observe(viewLifecycleOwner) {
            it.consume { directions ->
                findNavController().navigate(directions)
            }
        }
    }

    private fun handleCredLoadingStatus() {
        binding.apply {
            scrim.visibility = View.VISIBLE
            credentialsLayout.visibility = View.VISIBLE
            nameLayout.visibility = View.INVISIBLE
            passwordLayout.visibility = View.INVISIBLE
            restart.visibility = View.GONE
            cancel.visibility = View.INVISIBLE
            ok.visibility = View.INVISIBLE
            credentialsTitle.visibility = View.GONE
            credentialsSubtitle.visibility = View.GONE
            loadingPb.show()
        }
    }

    private fun handleCredErrorStatus(it: DebugMenuViewModel.ViewState) {
        binding.apply {
            scrim.visibility = View.VISIBLE
            credentialsLayout.visibility = View.VISIBLE
            nameLayout.visibility = View.VISIBLE
            passwordLayout.visibility = View.VISIBLE
            restart.visibility = View.GONE
            cancel.visibility = View.VISIBLE
            ok.visibility = View.VISIBLE
            getErrorDescription(it.status).let { desc ->
                nameLayout.error = desc
                passwordLayout.error = desc
            }
            credentialsTitle.visibility = View.VISIBLE
            credentialsSubtitle.visibility = View.VISIBLE
            ok.text = getString(R.string.retry)
            loadingPb.hide()
        }
    }

    private fun handleCredIdleStatus() {
        binding.apply {
            scrim.visibility = View.VISIBLE
            credentialsLayout.visibility = View.VISIBLE
            nameLayout.visibility = View.VISIBLE
            passwordLayout.visibility = View.VISIBLE
            restart.visibility = View.GONE
            cancel.visibility = View.VISIBLE
            ok.visibility = View.VISIBLE
            nameLayout.error = null
            passwordLayout.error = null
            ok.text = getString(R.string.ok)
            credentialsTitle.visibility = View.VISIBLE
            credentialsSubtitle.visibility = View.VISIBLE
            loadingPb.hide()
        }
    }

    private fun handleRestartStatus() {
        binding.apply {
            scrim.visibility = View.VISIBLE
            credentialsLayout.visibility = View.VISIBLE
            nameLayout.visibility = View.GONE
            passwordLayout.visibility = View.GONE
            restart.visibility = View.VISIBLE
            cancel.visibility = View.GONE
            ok.visibility = View.GONE
            credentialsTitle.visibility = View.GONE
            credentialsSubtitle.visibility = View.GONE
            loadingPb.hide()
        }
    }

    private fun handleIdleStatus() {
        binding.apply {
            name.setText("")
            password.setText("")
            credentialsLayout.visibility = View.GONE
            scrim.visibility = View.GONE
            loadingPb.hide()
        }
    }

    private fun getErrorDescription(errorStatus: DebugMenuViewModel.ViewState.Status): String? = when (errorStatus) {
        is DebugMenuViewModel.ViewState.Status.EnterCredentials.CredError.AuthError -> getErrorCauseDescription(errorStatus.value)
        DebugMenuViewModel.ViewState.Status.EnterCredentials.CredError.NetworkError -> getString(R.string.debugmenu_cred_network_error)
        DebugMenuViewModel.ViewState.Status.EnterCredentials.CredError.NoAppEnvSelected -> getString(R.string.debugmenu_cred_no_app_env_error)
        else -> null
    }

    private fun getErrorCauseDescription(envChange: EnvChange): String? = when {
        envChange is EnvChange.Success -> null
        envChange == EnvChange.AuthFailed -> getString(R.string.debugmenu_cred_error)
        envChange is EnvChange.ServerError -> {
            envChange.code?.let { getString(R.string.debugmenu_server_error, it) }
                ?: getString(R.string.debugmenu_server_parse_error)
        }
        else -> null
    }

    private fun restartApp() {
        activity?.run {
            val intent = packageManager.getLaunchIntentForPackage(packageName)
            finishAffinity()
            startActivity(intent)
            exitProcess(0)
        }
    }
}
