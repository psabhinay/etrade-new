package com.etrade.mobilepro.settings.viewmodel

import android.net.Uri
import com.etrade.mobilepro.settings.dto.SettingsViewType
import com.etrade.mobilepro.themeselection.NightModePreferences
import com.etrade.mobilepro.tracking.Tracker

class SettingActionDynamicRefreshViewModel(
    override val title: String,
    override val value: String? = null,
    override val layoutId: Int,
    appUrl: String?,
    webViewUri: Uri?,
    webUri: Uri?,
    override val viewType: SettingsViewType,
    tracker: Tracker,
    private val nightModePreferences: NightModePreferences
) : SettingActionDynamicViewModel(title, value, layoutId, appUrl, webViewUri, webUri, viewType, tracker) {

    val refreshedValue: String?
        get() = when (viewType) {
            SettingsViewType.SETTINGS_ACTION_THEME_SELECTION -> nightModePreferences.uiModeName
            else -> null
        }
}
