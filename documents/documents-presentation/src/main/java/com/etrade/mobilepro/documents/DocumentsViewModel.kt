package com.etrade.mobilepro.documents

import android.content.res.Resources
import android.util.Range
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobilepro.account.dao.Account
import com.etrade.mobilepro.dao.AccountDaoProvider
import com.etrade.mobilepro.dao.CachedDaoProvider
import com.etrade.mobilepro.dropdown.multiselect.DropDownMultiSelector
import com.etrade.mobilepro.livedata.updateValue
import com.etrade.mobilepro.preferences.preference
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.threeten.bp.LocalDate

private const val YEARS_CAPACITY = 7L

@Suppress("LargeClass", "LongParameterList")
class DocumentsViewModel @AssistedInject constructor(
    daoProvider: AccountDaoProvider,
    @Assisted var accountUuid: String,
    @Assisted var defaultDocumentType: DocumentType,
    @Assisted val pageType: PageType,
    private val cachedDaoProvider: CachedDaoProvider,
    private val defaultAccountRepo: DefaultAccountRepo,
    private val resources: Resources,
    private val repository: DocumentsRepository,
    private val documentStorage: DocumentStorage,
    keyValueStorage: KeyValueStorage
) : ViewModel() {

    @AssistedFactory
    interface Factory {
        fun create(
            accountUuid: String,
            defaultDocumentType: DocumentType,
            pageType: PageType
        ): DocumentsViewModel
    }

    private var job: Job? = null

    private val defaultTaxYear: Int = LocalDate.now().year - 1

    private var persistedDocumentsEndDateEpochDay: Int by keyValueStorage.preference(
        key = "persistedDocumentsEndDateEpochDay",
        default = LocalDate.now().toEpochDay().toInt()
    ) // safe for EpochDay

    private var persistedDocumentsStartDateEpochDay: Int by keyValueStorage.preference(
        key = "persistedDocumentsStartDateEpochDay",
        default = LocalDate.ofEpochDay(persistedDocumentsEndDateEpochDay.toLong())
            .minusYears(1L).toEpochDay().toInt() // defaults to the current date and looking back 1 year
    )

    private var persistedDocumentsType: Int by keyValueStorage.preference(
        key = "persistedDocumentsType",
        default = DocumentType.STATEMENTS.ordinal
    )

    private var persistedTaxYear: Int by keyValueStorage.preference(
        key = "persistedTaxYear",
        default = defaultTaxYear
    )

    private val defaultDateRange: Range<LocalDate> = Range(
        LocalDate.ofEpochDay(persistedDocumentsStartDateEpochDay.toLong()),
        LocalDate.ofEpochDay(persistedDocumentsEndDateEpochDay.toLong())
    )

    private val _documentsFilterState: MutableLiveData<DocumentsFilterState> = MutableLiveData(
        DocumentsFilterState(
            type = DocumentType.values().find { it.ordinal == persistedDocumentsType }
                ?: defaultDocumentType,
            taxYear = persistedTaxYear,
            dateRange = defaultDateRange
        )
    )

    private val _accountsFilterState: MutableLiveData<AccountsFilterState> by lazy {
        MutableLiveData(
            AccountsFilterState(
                accountDisplayName = accounts.value?.firstOrNull { it.accountUuid == accountUuid }?.accountLongName.orEmpty(),
                type = defaultDocumentType,
                taxYear = defaultTaxYear
            )
        )
    }

    private val accounts: LiveData<List<Account>> by lazy {
        daoProvider.getAccountDao().getAllAccounts()
    }

    internal val accountsFilterState: LiveData<AccountsFilterState>
        get() = _accountsFilterState

    internal val documentsFilterState: LiveData<DocumentsFilterState>
        get() = _documentsFilterState

    private val _viewState: MutableLiveData<ViewState> = MutableLiveData()
    internal val viewState: LiveData<ViewState>
        get() = _viewState

    internal val startDate: LocalDate
        get() = requireNotNull(_documentsFilterState.value?.dateRange?.lower)
    internal val endDate: LocalDate
        get() = requireNotNull(_documentsFilterState.value?.dateRange?.upper)
    internal val taxYear: Int
        get() = requireNotNull(
            if (pageType == PageType.ACCOUNT_DETAIL_DOCUMENTS_PAGE) {
                _documentsFilterState.value?.taxYear
            } else {
                _accountsFilterState.value?.taxYear
            }
        )

    // according to https://jira.corp.etradegrp.com/browse/ETAND-12255
    internal val minYear: LocalDate = LocalDate.now().minusYears(YEARS_CAPACITY)

    internal val documentsTypeSelectorData: LiveData<List<DropDownMultiSelector.Selectable<DocumentType>>> =
        Transformations.map(_viewState) {
            listOf(DocumentType.STATEMENTS, DocumentType.TAX_DOCS).map { type ->
                DropDownMultiSelector.Selectable(
                    title = resources.getString(type.getLabel()),
                    value = type,
                    selected = _documentsFilterState.value?.type == type
                )
            }
        }

    internal val accountSelectorData: LiveData<List<DropDownMultiSelector.Selectable<Account>>> =
        Transformations.map(_viewState) {
            accounts.value?.map { account ->
                DropDownMultiSelector.Selectable(
                    title = account.accountLongName,
                    value = account,
                    selected = _accountsFilterState.value?.accountDisplayName == account.accountLongName
                )
            }
        }

    private val _navSignal = MutableLiveData<ConsumableLiveEvent<StatefulDocument>>()
    internal val navSignal: LiveData<ConsumableLiveEvent<StatefulDocument>>
        get() = _navSignal
    private var loadingState = LoadingState.IDLE
    private val _errorSignal = MutableLiveData<ConsumableLiveEvent<Unit>>()
    internal val errorSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _errorSignal

    init {
        cachedDaoProvider.open()
        if (accountUuid.isEmpty()) {
            _viewState.value = ViewState.Loading
            loadingState = LoadingState.IDLE
            viewModelScope.launch {
                updateAccountUuid(getDefaultAccountUuid())
            }
        } else {
            load()
        }
    }

    override fun onCleared() {
        cachedDaoProvider.close()
        documentStorage.clearAllCachedDocuments()
        super.onCleared()
    }

    internal fun updateAccountUuid(selectedAccountUuid: String) {
        accountUuid = selectedAccountUuid
        _accountsFilterState.updateValue {
            it.copy(
                accountDisplayName = accounts.value?.firstOrNull { account ->
                    account.accountUuid == accountUuid
                }?.accountLongName.orEmpty()
            )
        }
        load()
    }

    internal fun updateDocumentsType(type: DocumentType) {
        _documentsFilterState.updateValue { it.copy(type = type) }
        persistedDocumentsType = type.ordinal
        defaultDocumentType = type
        if (pageType == PageType.ACCOUNT_DETAIL_DOCUMENTS_PAGE) {
            _documentsFilterState.updateValue { it.copy(type = type) }
        } else {
            _accountsFilterState.updateValue { it.copy(type = type) }
        }
        load()
    }

    internal fun updateTaxYear(taxYear: Int) {
        if (pageType == PageType.ACCOUNT_DETAIL_DOCUMENTS_PAGE) {
            _documentsFilterState.updateValue { it.copy(taxYear = taxYear) }
        } else {
            _accountsFilterState.updateValue { it.copy(taxYear = taxYear) }
        }
        persistedTaxYear = taxYear
        load()
    }

    internal fun updateStartDate(date: LocalDate) {
        _documentsFilterState.updateValue { it.copy(dateRange = Range(date, endDate)) }
        persistedDocumentsStartDateEpochDay = date.toEpochDay().toInt()
        load()
    }

    internal fun updateEndDate(date: LocalDate) {
        _documentsFilterState.updateValue { it.copy(dateRange = Range(startDate, date)) }
        persistedDocumentsEndDateEpochDay = date.toEpochDay().toInt()
        load()
    }

    internal fun load() {
        _viewState.value = ViewState.Loading
        loadingState = LoadingState.IDLE
        job?.cancel()
        job = viewModelScope.launch {
            if (pageType == PageType.ACCOUNT_DETAIL_DOCUMENTS_PAGE) {
                when (_documentsFilterState.value?.type) {
                    DocumentType.STATEMENTS -> loadDocuments(
                        {
                            getStatementDocuments(
                                StatementDocumentsRequest(
                                    accountUuid,
                                    startDate,
                                    endDate
                                )
                            )
                        },
                        resources.getString(R.string.documents_no_statements)
                    )
                    DocumentType.TAX_DOCS -> loadTaxDocuments()
                }
            } else {
                loadTaxDocuments()
            }
        }
    }

    internal fun loadDocument(document: StatefulDocument) {
        if (isAlreadyLoading()) {
            return
        }
        loadingState = LoadingState.LOADING_WITH_NAV_PENDING
        viewModelScope.launch {
            documentStorage
                .getDocumentData(document.document)
                .collect {
                    val statefulDocument = updateStatefulDocument(it, document)
                    when (it) {
                        is Resource.Loading -> { /* nop */ }
                        is Resource.Success -> {
                            emitNavSignal(statefulDocument)
                            loadingState = LoadingState.IDLE
                        }
                        is Resource.Failed -> {
                            loadingState = LoadingState.IDLE
                            _errorSignal.value = ConsumableLiveEvent(Unit)
                        }
                    }
                    updateDocumentListWith(statefulDocument)
                }
        }
    }

    internal fun cancelPendingNavigation() {
        if (loadingState == LoadingState.LOADING_WITH_NAV_PENDING) {
            loadingState = LoadingState.LOADING
        }
    }

    private suspend fun getDefaultAccountUuid(): String {
        val defaultAccountId = defaultAccountRepo.getAccountId().getOrNull()
        return if (defaultAccountId.isNullOrEmpty()) {
            accounts.value?.firstOrNull()?.accountUuid.orEmpty()
        } else {
            accounts.value?.firstOrNull { it.accountId == defaultAccountId }?.accountUuid.orEmpty()
        }
    }

    private fun isAlreadyLoading(): Boolean = loadingState != LoadingState.IDLE

    private fun emitNavSignal(statefulDocument: StatefulDocument) {
        if (loadingState == LoadingState.LOADING_WITH_NAV_PENDING) {
            _navSignal.value = ConsumableLiveEvent(statefulDocument)
        }
    }

    private suspend fun loadDocuments(
        request: DocumentsRepository.() -> Flow<Resource<List<Document>>>,
        emptyText: String
    ) {
        request(repository)
            .map { mapToStatefulDocumentsResource(it) }
            .collect {
                when (it) {
                    is Resource.Success -> _viewState.postValue(ViewState.Success(it.data ?: emptyList(), emptyText))
                    is Resource.Failed -> _viewState.postValue(ViewState.Failed)
                }
            }
    }

    private fun mapToStatefulDocumentsResource(resource: Resource<List<Document>>): Resource<List<StatefulDocument>> {
        val documents = resource.data ?: emptyList()
        val availableDocIds = documentStorage.getCachedDocuments(documents).map { it.documentId }.toSet()
        val statefulDocs = documents.map {
            StatefulDocument(
                document = it,
                status = if (it.documentId in availableDocIds) {
                    StatefulDocument.Status.Available
                } else {
                    StatefulDocument.Status.Unavailable
                }
            )
        }
        return resource.map { statefulDocs }
    }

    private fun updateDocumentListWith(document: StatefulDocument) {
        val currentState = (_viewState.value as? ViewState.Success) ?: return
        val currentList = currentState.documents
        val updatedList = currentList.map {
            if (it.document.documentId == document.document.documentId) {
                document
            } else {
                it
            }
        }
        _viewState.value = currentState.copy(documents = updatedList)
    }

    private fun updateStatefulDocument(
        resource: Resource<Document>,
        sourceDocument: StatefulDocument
    ): StatefulDocument {
        return when (resource) {
            is Resource.Loading -> sourceDocument.copy(status = StatefulDocument.Status.Loading)
            is Resource.Success -> resource.data?.let { downloadedDocument ->
                sourceDocument.copy(
                    document = downloadedDocument,
                    status = StatefulDocument.Status.Available
                )
            } ?: sourceDocument.copy(
                status = StatefulDocument.Status.Failed {
                    loadDocument(
                        sourceDocument
                    )
                }
            )
            is Resource.Failed -> sourceDocument.copy(
                status = StatefulDocument.Status.Failed {
                    loadDocument(
                        sourceDocument
                    )
                }
            )
        }
    }

    private suspend fun loadTaxDocuments() {
        loadDocuments(
            { getTaxDocuments(TaxDocumentsRequest(accountUuid, taxYear)) },
            resources.getString(R.string.documents_no_tax)
        )
    }

    internal sealed class ViewState {

        object Failed : ViewState()

        object Loading : ViewState()

        data class Success(
            val documents: List<StatefulDocument> = emptyList(),
            val emptyText: String
        ) : ViewState()
    }

    internal data class DocumentsFilterState(
        val type: DocumentType,
        val taxYear: Int,
        val dateRange: Range<LocalDate>
    )

    internal data class AccountsFilterState(
        val accountDisplayName: String,
        val type: DocumentType,
        val taxYear: Int
    )

    /**
     * Document pages can be accessed by following steps:
     * ACCOUNT_DETAIL_DOCUMENTS_PAGE: Complete View -> Account Details -> Documents
     * MENU_TAX_PAGE: Bottom Navigation -> Main Menu -> Tax Documents
     * */
    enum class PageType {
        ACCOUNT_DETAIL_DOCUMENTS_PAGE,
        MENU_TAX_PAGE
    }

    private enum class LoadingState {
        IDLE, LOADING_WITH_NAV_PENDING, LOADING
    }
}
