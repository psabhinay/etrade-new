package com.etrade.mobilepro.quotes.usecase

import com.etrade.mobilepro.quoteapi.FundQuoteItem
import com.etrade.mobilepro.quoteapi.QuoteItemInterface
import com.etrade.mobilepro.quoteapi.QuoteItems
import com.etrade.mobilepro.util.UseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

interface GetQuoteDetailsUseCase : UseCase<GetQuoteDetailsUseCaseParameter, List<QuoteItemInterface>>

class GetQuoteDetailsUseCaseImpl @Inject constructor() : GetQuoteDetailsUseCase {

    override suspend fun execute(parameter: GetQuoteDetailsUseCaseParameter): List<QuoteItemInterface> = withContext(Dispatchers.IO) {
        with(parameter) {
            mutableListOf<QuoteItemInterface>().apply {
                addAll(selectTab(quoteItems))
                addAll(newsItems)
            }.filter { it !is FundQuoteItem.FundClassSection || it.fundClassInfoList.isNotEmpty() }
        }
    }
}

class GetQuoteDetailsUseCaseParameter(
    val quoteItems: QuoteItems,
    val newsItems: List<QuoteItemInterface>,
    val selectTab: (QuoteItems) -> List<QuoteItemInterface>
)
