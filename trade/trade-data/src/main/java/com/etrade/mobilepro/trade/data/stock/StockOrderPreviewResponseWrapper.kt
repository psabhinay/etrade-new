package com.etrade.mobilepro.trade.data.stock

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.trade.api.stock.StockOrderPreviewResponse
import com.etrade.mobilepro.trade.api.stock.StockOrderResponse
import com.etrade.mobilepro.trade.data.priceTypeWarnings

internal class StockOrderPreviewResponseWrapper(private val dto: StockOrderResponseDto) : StockOrderPreviewResponse, StockOrderResponse by dto {
    override val isOvernightTrading: Boolean = dto.result?.fault?.errors?.any { it.errorCode == DAY_TRADING_WARNING_CODE } ?: false
    override val warnings: List<ServerMessage> = dto.order?.first()?.priceTypeWarnings(dto.warnings).orEmpty()
}
