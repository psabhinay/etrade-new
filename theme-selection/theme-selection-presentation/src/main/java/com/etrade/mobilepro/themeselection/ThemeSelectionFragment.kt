package com.etrade.mobilepro.themeselection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.etrade.mobilepro.themeselection.databinding.FragmentThemeSelectionFragmentBinding
import javax.inject.Inject

class ThemeSelectionFragment @Inject constructor(
    private var nightModePreferences: NightModePreferences
) : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentThemeSelectionFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_theme_selection_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.light.setOnClickListener(this)
        binding.dark.setOnClickListener(this)
        binding.system.setOnClickListener(this)
        updateSelection()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.light -> nightModePreferences.switchTheme(LIGHT_MODE)
            R.id.dark -> nightModePreferences.switchTheme(DARK_MODE)
            R.id.system -> nightModePreferences.switchTheme(FOLLOWING_SYSTEM_PREFERENCES)
        }
        updateSelection()
    }

    private fun updateSelection() {
        nightModePreferences.uiMode.let {
            binding.light.isModeSelected = it == LIGHT_MODE
            binding.dark.isModeSelected = it == DARK_MODE
            binding.system.isModeSelected = it == FOLLOWING_SYSTEM_PREFERENCES
        }
    }
}
