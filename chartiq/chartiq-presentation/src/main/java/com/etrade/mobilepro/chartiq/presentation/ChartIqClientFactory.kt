package com.etrade.mobilepro.chartiq.presentation

import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.chartiq.api.ChartIqClient
import com.etrade.mobilepro.chartiq.data.ChartIqClientImpl
import com.etrade.mobilepro.chartiq.data.ChartIqCommandSink
import com.etrade.mobilepro.util.AuthenticationStatusProvider
import com.squareup.moshi.Moshi
import javax.inject.Inject

interface ChartIqClientFactory {
    fun create(commandSink: ChartIqCommandSink): ChartIqClient
}

class ChartIqClientFactoryImpl @Inject constructor(
    private val moshi: Moshi,
    private val applicationInfo: ApplicationInfo,
    private val authStatusProvider: AuthenticationStatusProvider
) : ChartIqClientFactory {
    override fun create(commandSink: ChartIqCommandSink) = ChartIqClientImpl(
        moshi = moshi,
        applicationInfo = applicationInfo,
        authStatusProvider = authStatusProvider,
        commandSink = commandSink
    )
}
