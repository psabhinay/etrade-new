package com.etrade.mobilepro.movers.markets

import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.markets.data.response.MarketMoverResponse
import com.etrade.mobilepro.markets.data.response.Quotes
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.Single
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.ArgumentMatchers.eq

@ExperimentalCoroutinesApi
internal class GetMarketMoversUseCaseTest {

    private val marketMoversService: MoverService = mockMoverService()
    private val isExtendedHoursOnUseCase: IsExtendedHoursOnUseCase = mock()
    private val mockMobileQuoteRepo: MobileQuoteRepo = mock {
        on { loadMobileQuotes(anyString(), eq(true), eq(false), eq(false)) } doReturn Observable.just(emptyList())
    }

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    private lateinit var sut: GetMarketMoversUseCaseImpl

    private fun mockMoverService(marketMoverResponse: MarketMoverResponse = MarketMoverResponse(listOf(Quotes("AAPL")))) = mock<MoverService> {
        on { getMarketMovers() } doReturn Single.just(marketMoverResponse)
    }

    @Before
    fun setUp() {
        Dispatchers.setMain(testCoroutineDispatcher)
        sut = GetMarketMoversUseCaseImpl(
            marketMoversService,
            isExtendedHoursOnUseCase,
            mockMobileQuoteRepo
        )
    }

    @After
    fun cleanup() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `test when extended hours is on fetch quotes`() {
        runBlocking(testCoroutineDispatcher) {
            whenever(isExtendedHoursOnUseCase.execute(any())).thenReturn(true)
            sut.execute(Unit)
        }
        verify(mockMobileQuoteRepo).loadMobileQuotes(anyString(), eq(true), eq(true), eq(false))
    }
}
