package com.etrade.mobilepro.util

import com.etrade.mobilepro.util.debuglog.DebugLog
import com.etrade.mobilepro.util.debuglog.RemoteDebugLogger
import com.google.firebase.crashlytics.FirebaseCrashlytics

class FireBaseNonFatalEventLogger : RemoteDebugLogger {
    override fun log(debugLog: DebugLog) {
        FirebaseCrashlytics.getInstance().recordException(debugLog)
    }
}
