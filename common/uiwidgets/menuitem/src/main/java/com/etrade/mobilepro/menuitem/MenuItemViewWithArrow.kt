package com.etrade.mobilepro.menuitem

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import com.etrade.mobilepro.menuitem.databinding.MenuItemWithArrowBinding
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.layoutInflater

/**
 * Class implements custom view which has text on the left and a icon anchored on the right parent edge.
 * The icon width and height depends on the size of image.
 */
class MenuItemViewWithArrow : ConstraintLayout {

    constructor(context: Context) : super(context) {
        init(null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    private val binding = MenuItemWithArrowBinding.inflate(layoutInflater, this)

    /**
     * Property to get/set menu title.
     */
    var menuTitleText: CharSequence
        get() = binding.menuTitle.text
        set(value) {
            binding.menuTitle.text = value
        }

    var showLabelPill: Boolean
        get() = binding.menuTitle.showLabelPill
        set(value) {
            binding.menuTitle.showLabelPill = value
        }

    private fun init(attributeSet: AttributeSet?) {
        inflate(R.layout.menu_item_with_arrow, true)
        context.withStyledAttributes(attributeSet, R.styleable.MenuItemViewWithArrow) {
            menuTitleText = getString(R.styleable.MenuItemViewWithArrow_menu_title).orEmpty()
        }
    }
}
