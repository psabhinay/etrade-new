package com.etrade.mobilepro.checkdeposit.deposit.service

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import java.math.BigDecimal

@JsonClass(generateAdapter = true)
data class DepositImageDto(
    @Json(name = "value")
    val value: DepositImageValueDto
)

@JsonClass(generateAdapter = true)
data class DepositImageValueDto(
    @Json(name = "userId")
    val userId: String,
    @Json(name = "frontImageToken")
    val frontImageToken: String?,
    @Json(name = "frontImage")
    val frontImage: String?,
    @Json(name = "rearImageToken")
    val rearImageToken: String?,
    @Json(name = "rearImage")
    val rearImage: String?,
    @Json(name = "depositMode")
    val depositMode: String?,
    @Json(name = "iraDepositType")
    val iraDepositType: String?,
    @Json(name = "amount")
    val amount: String,
    @Json(name = "accountNo")
    val accountNo: String,
    @Json(name = "instNo")
    val instNo: String,
    @Json(name = "workflowId")
    val workflowId: String,
    @Json(name = "isUniversalApp")
    val isUniversalApp: String = "true",
    @Json(name = "phoneKey")
    val phoneKey: String = "A0000000000000",
    @Json(name = "phoneType")
    val phoneType: String = "android"
)

@JsonClass(generateAdapter = true)
class DepositImageResponseDto(
    @Json(name = "DepositImageResult")
    val depositImageResultDto: DepositImageResultDto
) : BaseDataDto() {

    @JsonClass(generateAdapter = true)
    data class DepositImageResultDto(
        @Json(name = "workflowId")
        val workflowId: String?,
        @Json(name = "amount")
        val amount: BigDecimal?,
        @Json(name = "accountNo")
        val accountNumber: String?,
        @Json(name = "frontImageFieldToFlag")
        val frontImageFieldToFlag: Boolean,
        @Json(name = "rearImageFieldToFlag")
        val rearImageFieldToFlag: Boolean,
        @Json(name = "amountFieldToFlag")
        val amountFieldToFlag: Boolean
    )
}
