package com.etrade.mobilepro.settings.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.login.api.repo.LoginPreferencesRepo
import com.etrade.mobilepro.pushnotification.api.AlertPushSettings
import com.etrade.mobilepro.pushnotification.api.repo.AlertPushSettingsRepo
import com.etrade.mobilepro.pushnotification.usecase.ManageSmartAlertSubscriptionUseCase
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.settings.dto.SettingsViewType
import com.etrade.mobilepro.settings.viewmodel.feature.HasAction
import com.etrade.mobilepro.settings.viewmodel.feature.ViewModelAction
import com.etrade.mobilepro.settings.viewmodel.feature.ViewModelActionId
import com.etrade.mobilepro.settings.viewmodel.feature.ViewModelActionResult
import com.etrade.mobilepro.settings.viewmodel.feature.getVmActionById
import com.etrade.mobilepro.streaming.StreamingStatusNotifierDelegate
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.etrade.mobilepro.streaming.api.StreamingStatusUpdate
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.AccountAlertsAnalyticsParams
import com.etrade.mobilepro.tracking.params.BiometricAuthenticationAnalyticsParams
import com.etrade.mobilepro.tracking.params.StockAlertsAnalyticsParams
import com.etrade.mobilepro.usersubscriptionapi.UserSubscriptionRepo
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.applySideEffect
import kotlinx.coroutines.launch

class SettingToggleViewModel(
    override val title: String,
    override val layoutId: Int,
    private val streamingStatusController: StreamingStatusController,
    private val streamingStatusNotifierDelegate: StreamingStatusNotifierDelegate,
    private val loginPreferences: LoginPreferencesRepo,
    override val viewType: SettingsViewType,
    private val alertPushSettingsRepo: AlertPushSettingsRepo,
    private val userSubscriptionRepo: UserSubscriptionRepo,
    private val user: User,
    override val actionSet: Set<ViewModelAction>,
    private val manageAlertSubscription: ManageSmartAlertSubscriptionUseCase,
    private val tracker: Tracker
) : SettingDynamicViewModel(title, null, layoutId, viewType), HasAction {

    private val _isEnabled = MutableLiveData(getValue())
    val isEnabled: LiveData<Boolean>
        get() = _isEnabled

    private val _isInProgress = MutableLiveData(false)
    val isInProgress: LiveData<Boolean>
        get() = _isInProgress

    private val _actionSignal = MutableLiveData<ConsumableLiveEvent<ViewModelAction>>()
    override val actionSignal: LiveData<ConsumableLiveEvent<ViewModelAction>>
        get() = _actionSignal

    private val streamingStatusUpdateListener: (StreamingStatusUpdate) -> Unit = { status ->
        if (status is StreamingStatusUpdate.StreamingToggleUpdate) {
            if (!status.enabled && viewType == SettingsViewType.SETTINGS_TOGGLE_STREAMING) {
                _isEnabled.value = false
            }
        }
    }

    init {
        when (viewType) {
            SettingsViewType.SETTINGS_TOGGLE_STOCK_ALERTS,
            SettingsViewType.SETTINGS_TOGGLE_STREAMING -> streamingStatusController.subscribeForStreamingStatusUpdates(streamingStatusUpdateListener)
            else -> { /* Do Nothing */ }
        }
    }

    override fun onCleared() {
        when (viewType) {
            SettingsViewType.SETTINGS_TOGGLE_STOCK_ALERTS,
            SettingsViewType.SETTINGS_TOGGLE_STREAMING -> streamingStatusController.unsubscribeFromStreamingStatusUpdates(streamingStatusUpdateListener)
            else -> { /* Do Nothing */ }
        }
        super.onCleared()
    }

    fun onSwitchChanged(isChecked: Boolean) {
        if (isChecked != _isEnabled.value) {
            _isEnabled.value = isChecked
            if (!checkIfActionRequired(isChecked)) {
                setValue(isChecked)
            }
        }
    }

    private fun getValue(): Boolean {
        return when (viewType) {
            SettingsViewType.SETTINGS_TOGGLE_BIOMETRIC_AUTH -> loginPreferences.isBiometricAuthenticationEnabled
            SettingsViewType.SETTINGS_TOGGLE_STREAMING -> streamingStatusController.isStreamingToggleEnabled
            SettingsViewType.SETTINGS_TOGGLE_REMEMBER_USER_ID -> loginPreferences.isRememberUserNameEnabled
            SettingsViewType.SETTINGS_TOGGLE_STOCK_ALERTS -> alertPushSettingsRepo.getCurrentSettings().stockAlertsEnabled
            SettingsViewType.SETTINGS_TOGGLE_ACCOUNT_ALERTS -> alertPushSettingsRepo.getCurrentSettings().accountAlertsEnabled
            else -> false
        }
    }

    private fun setValue(value: Boolean) {
        return when (viewType) {
            SettingsViewType.SETTINGS_TOGGLE_BIOMETRIC_AUTH -> {
                if (!value) {
                    loginPreferences.disableBiometricAuthentication()
                }
                tracker.event(BiometricAuthenticationAnalyticsParams(value))
            }
            SettingsViewType.SETTINGS_TOGGLE_STREAMING -> streamingStatusController.isStreamingToggleEnabled = value
            SettingsViewType.SETTINGS_TOGGLE_REMEMBER_USER_ID -> loginPreferences.isRememberUserNameEnabled = value
            SettingsViewType.SETTINGS_TOGGLE_ACCOUNT_ALERTS,
            SettingsViewType.SETTINGS_TOGGLE_STOCK_ALERTS -> saveNetworkBoundValue(value)
            else -> { /* do nothing */
            }
        }
    }

    private fun saveNetworkBoundValue(isChecked: Boolean) {
        val currentValue = getValue()
        if (currentValue != isChecked) {
            _isInProgress.value = true
            viewModelScope.launch {
                val result = setValueForNetworkBoundValue(isChecked)

                if (result.isSuccess) {
                    _isEnabled.value = isChecked

                    if (viewType == SettingsViewType.SETTINGS_TOGGLE_STOCK_ALERTS) {
                        tracker.event(StockAlertsAnalyticsParams(isChecked))
                    } else if (viewType == SettingsViewType.SETTINGS_TOGGLE_ACCOUNT_ALERTS) {
                        tracker.event(AccountAlertsAnalyticsParams(isChecked))
                    }
                } else {
                    sendViewModelAction(ViewModelActionId.SHOW_ERROR_SNACK_BAR)
                }
                _isInProgress.value = false
            }
        } else {
            _isEnabled.value = currentValue
        }
    }

    private suspend fun setValueForNetworkBoundValue(value: Boolean): ETResult<AlertPushSettings> =
        when (viewType) {
            SettingsViewType.SETTINGS_TOGGLE_STOCK_ALERTS -> alertPushSettingsRepo.setAlertPushSettings(
                settings = alertPushSettingsRepo.getCurrentSettings().copy(
                    stockAlertsEnabled = value
                )
            ).applySideEffect { manageAlertSubscription.execute() }
            SettingsViewType.SETTINGS_TOGGLE_ACCOUNT_ALERTS -> alertPushSettingsRepo.setAlertPushSettings(
                settings = alertPushSettingsRepo.getCurrentSettings().copy(
                    accountAlertsEnabled = value
                )
            ).applySideEffect { manageAlertSubscription.execute() }
            else -> ETResult.failure(IllegalArgumentException("Unknown type $viewType for network bound toggle"))
        }

    @SuppressWarnings("LongMethod", "ComplexMethod")
    private fun checkIfActionRequired(value: Boolean): Boolean {
        return when (viewType) {
            SettingsViewType.SETTINGS_TOGGLE_STOCK_ALERTS,
            SettingsViewType.SETTINGS_TOGGLE_ACCOUNT_ALERTS -> {
                if (value) {
                    sendViewModelAction(
                        if (viewType == SettingsViewType.SETTINGS_TOGGLE_STOCK_ALERTS) {
                            ViewModelActionId.SHOW_STOCK_ALERT_POPUP
                        } else {
                            ViewModelActionId.SHOW_ACCOUNT_ALERT_POPUP
                        }
                    )
                }
                value
            }
            SettingsViewType.SETTINGS_TOGGLE_BIOMETRIC_AUTH -> {
                if (value) {
                    sendViewModelAction(ViewModelActionId.START_ENABLING_BIOMETRIC)
                }
                value
            }
            SettingsViewType.SETTINGS_TOGGLE_STREAMING -> {
                when {
                    streamingStatusNotifierDelegate.isUnableToStream.get() -> {
                        streamingStatusNotifierDelegate.showNotificationDialog(true)
                        _isEnabled.value = false
                        true
                    }
                    isMarketDataAgreementCheckRequired(value) -> {
                        checkMarketDataAgreement()
                        true
                    }
                    else -> false
                }
            }
            SettingsViewType.SETTINGS_TOGGLE_REMEMBER_USER_ID -> false
            else -> {
                false
            }
        }
    }

    private fun isMarketDataAgreementCheckRequired(toggleEnabling: Boolean): Boolean {
        return toggleEnabling && !user.isRealtimeUser
    }

    private fun checkMarketDataAgreement() {
        _isInProgress.value = true
        viewModelScope.launch {
            userSubscriptionRepo.loadUserMda()
                .onSuccess {
                    _isInProgress.value = false
                    if (it.data?.isRealtime == true) {
                        setValue(true)
                    } else {
                        sendViewModelAction(ViewModelActionId.START_MARKET_DATA_AGREEMENT)
                    }
                }.onFailure {
                    _isEnabled.value = false
                    _isInProgress.value = false
                    logger.error("Failed to retrieve user MDA status", it)
                    setValue(false)
                    sendViewModelAction(ViewModelActionId.SHOW_UNABLE_TO_ENABLE_STREAMING_POPUP)
                }
        }
    }

    private fun sendViewModelAction(viewModelActionId: ViewModelActionId) {
        when (viewModelActionId) {
            ViewModelActionId.SHOW_ERROR_SNACK_BAR -> ConsumableLiveEvent(ViewModelAction.ErrorSnackBar())
            else -> {
                actionSet.getVmActionById(viewModelActionId)?.let { ConsumableLiveEvent(it) }
            }
        }?.let { _actionSignal.value = it }
    }

    override fun onActionResult(viewModelActionResult: ViewModelActionResult) {
        when (viewModelActionResult) {
            is ViewModelActionResult.ToggleEnablingResult -> {
                _isEnabled.value = viewModelActionResult.enable
                if (getValue() != viewModelActionResult.enable) {
                    setValue(viewModelActionResult.enable)
                }
            }
            is ViewModelActionResult.NetworkBoundToggleEnablingResult -> {
                saveNetworkBoundValue(viewModelActionResult.enable)
            }
        }
    }
}
