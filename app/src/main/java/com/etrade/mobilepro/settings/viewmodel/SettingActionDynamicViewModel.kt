package com.etrade.mobilepro.settings.viewmodel

import android.net.Uri
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.settings.dto.SettingsViewType
import com.etrade.mobilepro.tracking.SCREEN_TITLE_DISCLOSURES
import com.etrade.mobilepro.tracking.SCREEN_TITLE_END_USER_LICENSE_AGREEMENT
import com.etrade.mobilepro.tracking.SCREEN_TITLE_PRIVACY_POLICY
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen

open class SettingActionDynamicViewModel(
    override val title: String,
    override val value: String? = null,
    override val layoutId: Int,
    private val appUrl: String?,
    private val webViewUri: Uri?,
    private val webUri: Uri?,
    override val viewType: SettingsViewType,
    private val tracker: Tracker
) : SettingDynamicViewModel(title, value, layoutId, viewType) {

    fun onCtaClicked() {
        when {
            appUrl != null -> appUrl.also { clickEvents.value = CtaEvent.ActionEvent(it, title) }
            webViewUri != null -> webViewUri.also { clickEvents.value = CtaEvent.UriEvent(it) }
            webUri != null -> webUri.also { clickEvents.value = CtaEvent.UriEvent(it) }
        }

        trackScreen()
    }

    private fun trackScreen() {
        when (viewType) {
            SettingsViewType.SETTINGS_ACTION_END_USER_LICENSE_AGREEMENT -> tracker.screen(SCREEN_TITLE_END_USER_LICENSE_AGREEMENT)
            SettingsViewType.SETTINGS_ACTION_PRIVACY_POLICY -> tracker.screen(SCREEN_TITLE_PRIVACY_POLICY)
            SettingsViewType.SETTINGS_ACTION_DISCLOSURES -> tracker.screen(SCREEN_TITLE_DISCLOSURES)
            else -> {
                /* Do Nothing */
            }
        }
    }
}
