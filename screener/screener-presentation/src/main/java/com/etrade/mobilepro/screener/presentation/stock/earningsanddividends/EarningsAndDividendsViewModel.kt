package com.etrade.mobilepro.screener.presentation.stock.earningsanddividends

import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.aggregator.ScreenerFilterAggregator
import com.etrade.mobilepro.screener.api.filter.stock.EarningsDividendsFilter
import com.etrade.mobilepro.screener.api.interactor.ScreenerInteractor
import com.etrade.mobilepro.screener.api.qualifier.Stock
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenViewModel
import javax.inject.Inject

class EarningsAndDividendsViewModel @Inject constructor(
    aggregator: ScreenerFilterAggregator,
    @Stock screenerInteractor: ScreenerInteractor
) : BaseFilterScreenViewModel<EarningsDividendsFilter, EarningsAndDividendsViewModel.ViewState>(
    aggregator,
    ScreenerType.Stock.EarningsAndDividends,
    screenerInteractor
) {

    override fun provideInitialState(initialFilter: EarningsDividendsFilter): ViewState = ViewState(filter = initialFilter)

    data class ViewState(
        override val filter: EarningsDividendsFilter
    ) : BaseFilterScreenViewModel.ViewState<EarningsDividendsFilter>
}
