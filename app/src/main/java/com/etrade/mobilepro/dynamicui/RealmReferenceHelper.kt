package com.etrade.mobilepro.dynamicui

import com.etrade.mobilepro.backends.mgs.BaseReference
import com.etrade.mobilepro.dynamic.referencehelper.ReferenceHelper
import com.squareup.moshi.Moshi
import io.realm.Realm
import io.realm.RealmModel
import javax.inject.Inject

class RealmReferenceHelper @Inject constructor(private val moshi: Moshi) : ReferenceHelper {

    override fun persist(reference: BaseReference<*>) {
        Realm.getDefaultInstance().use { realm ->
            val model = reference.convertToModel()
            realm.executeTransaction {
                if (model is Iterable<*>) {
                    model.forEach {
                        if (it is RealmModel) {
                            realm.createOrUpdateObjectFromJson(it)
                        }
                    }
                } else if (model is RealmModel) {
                    realm.createOrUpdateObjectFromJson(model)
                }
            }
        }
    }

    private fun Realm.createOrUpdateObjectFromJson(model: RealmModel) {
        createOrUpdateObjectFromJson(model.javaClass, moshi.adapter(model.javaClass).toJson(model))
    }
}
