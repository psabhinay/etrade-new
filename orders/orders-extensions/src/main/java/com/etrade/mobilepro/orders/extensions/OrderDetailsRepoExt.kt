package com.etrade.mobilepro.orders.extensions

import com.etrade.mobilepro.order.details.api.OrderCancelRequest
import com.etrade.mobilepro.order.details.api.OrderDetailsRepo
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.OrderIdentifier
import com.etrade.mobilepro.orders.api.OrderType

fun OrderDetailsRepo.cancelOrder(
    orderIdentifier: OrderIdentifier,
    request: OrderCancelRequest,
    orderType: OrderType,
    advancedOrderType: AdvancedOrderType? = null
) = when (orderIdentifier) {
    OrderIdentifier.STOCK -> cancelOrder(request)
    OrderIdentifier.CONDITIONAL -> cancelGroupOrder(request)
    OrderIdentifier.CONTINGENT -> {
        if (orderType == OrderType.CUSTOM && advancedOrderType == AdvancedOrderType.AO_CONTINGENT_CANCEL) {
            cancelOrder(request)
        } else {
            cancelGroupOrder(request)
        }
    }
    OrderIdentifier.OPTION -> cancelOrder(request)
    OrderIdentifier.BRACKETED -> cancelOrder(request)
}
