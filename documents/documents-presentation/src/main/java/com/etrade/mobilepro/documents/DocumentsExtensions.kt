package com.etrade.mobilepro.documents

import androidx.annotation.StringRes

@StringRes
internal fun DocumentType.getLabel(): Int {
    return when (this) {
        DocumentType.STATEMENTS -> R.string.documents_type_statements
        DocumentType.TAX_DOCS -> R.string.documents_type_tax_docs
    }
}
