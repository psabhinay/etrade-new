package com.etrade.mobilepro.login

import android.content.Intent
import android.os.Bundle
import android.os.Message
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.Observer
import com.etrade.mobilepro.R
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticationDelegate
import com.etrade.mobilepro.common.LoginChecker
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.common.setupToolbarWithUpButton
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.common.toolbarView
import com.etrade.mobilepro.databinding.ActivityLoginBinding
import com.etrade.mobilepro.dynamicui.api.CtaEvent
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.dynamicui.api.toDynamicViewModel
import com.etrade.mobilepro.landing.api.LandingNavigationHelper
import com.etrade.mobilepro.login.presentation.LoginViewDelegate
import com.etrade.mobilepro.login.presentation.LoginViewModel
import com.etrade.mobilepro.login.presentation.MESSAGE_LOGIN_ACTION
import com.etrade.mobilepro.login.presentation.cta.LoginCtaItem
import com.etrade.mobilepro.login.presentation.cta.LoginViewCtaListener
import com.etrade.mobilepro.navigation.hasBeenToLoginScreen
import com.etrade.mobilepro.session.api.UserAuthenticationState
import com.etrade.mobilepro.userviewmodel.UserViewModel
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import org.chromium.customtabsclient.shared.ObserverForCustomTabWarmUp
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var isAuthenticated: UserAuthenticationState

    @Inject
    lateinit var loginCtaListener: LoginViewCtaListener

    @Inject
    lateinit var fragmentFactory: FragmentFactory

    @Inject
    lateinit var biometricAuthenticationDelegate: BiometricAuthenticationDelegate

    @Inject
    lateinit var loginViewDelegate: LoginViewDelegate

    @Inject
    lateinit var userViewModel: UserViewModel

    @Inject
    lateinit var snackbarUtilFactory: SnackbarUtilFactory

    @Inject
    lateinit var loginChecker: LoginChecker

    private val binding by viewBinding(ActivityLoginBinding::inflate)

    private val snackBarUtil: SnackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ this }, { binding.loginContainer }) }

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    private var isRecreated = false

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        supportFragmentManager.fragmentFactory = fragmentFactory
        isRecreated = savedInstanceState != null
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupToolbarWithUpButton(
            R.string.title_activity_login,
            actionEnd = ToolbarActionEnd(
                text = R.string.cancel,
                contentDescription = getString(R.string.button_content_description_cancel),
                onClick = View.OnClickListener {
                    loginChecker.onLoginAborted()
                    userViewModel.onLoginAborted()
                    finish()
                },
                textColor = R.color.black
            )
        )
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        toolbarView.setBackgroundResource(R.color.white)
        toolbarTitleView?.visibility = View.GONE
        bindViewModels(loginViewDelegate)
        lifecycle.addObserver(ObserverForCustomTabWarmUp(this))
        hasBeenToLoginScreen = true
    }

    override fun onResume() {
        super.onResume()
        if (!isRecreated) {
            binding.loginWidget.post { binding.loginWidget.authenticateUsingBiometrics() }
        }
    }

    override fun onBackPressed() {
        loginChecker.onLoginAborted()
        userViewModel.onLoginAborted()
        super.onBackPressed()
    }

    override fun onNewIntent(newIntent: Intent?) {
        super.onNewIntent(intent)
        isRecreated = true
    }

    private fun bindViewModels(layout: GenericLayout) {
        val viewModel = layout.toDynamicViewModel(this, LoginActivity::class.java.simpleName) as LoginViewModel
        binding.loginWidget.loginViewModel = viewModel
        binding.loginWidget.setupView(
            activity = this@LoginActivity,
            biometricDelegate = biometricAuthenticationDelegate,
            shouldHandleLanding = LandingNavigationHelper.checkIfShouldHandleLanding(intent)
        )
        viewModel.clickEvents.observe(
            this@LoginActivity,
            Observer { ctaEvent ->
                when (ctaEvent) {

                    is CtaEvent.MessageEvent -> onMessageEvent(event = ctaEvent.message)
                }
            }
        )
    }

    private fun onMessageEvent(event: Message) {
        when (event.what) {
            MESSAGE_LOGIN_ACTION -> {
                loginCtaListener.onCtaEvent(event.obj as LoginCtaItem) { snackBarUtil }
            }
        }
    }
}
