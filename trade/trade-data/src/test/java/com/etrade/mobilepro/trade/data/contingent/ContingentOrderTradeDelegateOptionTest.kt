package com.etrade.mobilepro.trade.data.contingent

import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.trade.data.DAY_TRADING_FLAG
import com.etrade.mobilepro.trade.data.DAY_TRADING_VER
import com.etrade.mobilepro.trade.data.TradeRequestParamsUpdater
import com.etrade.mobilepro.trade.data.advanced.ADVANCED_ORDER_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.FORM_TARGET_KEY
import com.etrade.mobilepro.trade.data.advanced.OPTION_EXPIRATION_KEY
import com.etrade.mobilepro.trade.data.advanced.OPTION_STRIKE_PRICE_KEY
import com.etrade.mobilepro.trade.data.advanced.OPTION_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.OPT_LEVEL
import com.etrade.mobilepro.trade.data.advanced.ORDER_COUNT_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_TRIGGER_KEY
import com.etrade.mobilepro.trade.data.advanced.ORDER_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.PLATFORM_KEY
import com.etrade.mobilepro.trade.data.advanced.PLATFORM_VALUE
import com.etrade.mobilepro.trade.data.advanced.POSITION_EFFECT_KEY
import com.etrade.mobilepro.trade.data.advanced.PREVIEW_ID_KEY
import com.etrade.mobilepro.trade.data.advanced.PREVIEW_X_KEY
import com.etrade.mobilepro.trade.data.advanced.PRIMARY_SECURITY_TYPE_KEY
import com.etrade.mobilepro.trade.data.advanced.SIDE_KEY
import com.etrade.mobilepro.trade.data.advanced.TRADE_X_KEY
import com.etrade.mobilepro.trade.data.advanced.VALUE_SECURITY_TYPE_OPTION
import com.etrade.mobilepro.trade.data.advanced.VALUE_SECURITY_TYPE_STOCK
import com.etrade.mobilepro.trade.data.form.input.DefaultContingentOptionTradeFormInputId
import com.etrade.mobilepro.trade.data.form.value.AdvancedOrderOptionsActionValueId
import com.etrade.mobilepro.trade.data.option.ContingentOptionTradeRequestParamsUpdater
import com.etrade.mobilepro.trade.data.stock.StockOrderConstants
import com.etrade.mobilepro.trade.form.api.input.ContingentOptionTradeFormInputId
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
internal class ContingentOrderTradeDelegateOptionTest : AbstractContingentOrderTradeDelegateTest<ContingentOptionTradeFormInputId>() {

    override val inputId: ContingentOptionTradeFormInputId = DefaultContingentOptionTradeFormInputId
    override val requestParamUpdater: TradeRequestParamsUpdater = ContingentOptionTradeRequestParamsUpdater()
    override val orderType: OrderType = OrderType.SIMPLE_OPTION

    @Test
    fun `PREVIEW - MARKET BUY_OPEN 1 AAPL--210115C00018750 - GOOG--210205C01040000 LAST_PRICE LESS_THAN_OR_EQUAL_TO 1000`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.option,
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.symbol to "AAPL--210115C00018750:OPTNC",
            inputId.strategy to inputId.strategyValueId.call,
            inputId.tradeLegs to "[{\"expiryDay\":\"15\",\"expiryMonth\":\"1\",\"expiryYear\":\"2021\",\"optionType\":\"1\",\"quantity\":1,\"strikePrice\":\"18.75\",\"symbol\":\"AAPL--210115C00018750\",\"transaction\":\"1\"}]",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to "0",
            inputId.quantity to "1",
            inputId.action to inputId.actionValueId.buy,
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.contingent,
            inputId.contingentOrderInputId.symbol to "GOOG--210205C01040000:OPTNC",
            inputId.contingentOrderInputId.contingentSymbol to "GOOG--210205C01040000:OPTNC",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.lastPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.lessThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "1000"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to "0",
            inputId.action to inputId.actionValueId.buy,
            inputId.quantity to "1",
            inputId.symbol to "AAPL--210115C00018750",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_OPTION,
            inputId.contingentOrderInputId.securityType to VALUE_SECURITY_TYPE_OPTION,
            inputId.contingentOrderInputId.symbol to "GOOG--210205C01040000",
            inputId.contingentOrderInputId.contingentSymbol to "GOOG--210205C01040000",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.lastPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.lessThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "1000",
            "${OPTION_EXPIRATION_KEY}0" to "1",
            "${OPTION_STRIKE_PRICE_KEY}0" to "18.75",
            "${OPTION_TYPE_KEY}0" to "1",
            OPTION_EXPIRATION_KEY to "1",
            OPTION_STRIKE_PRICE_KEY to "1040",
            OPTION_TYPE_KEY to "1",
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "contingentcreateentry",
            PREVIEW_X_KEY to "53",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }

    @Test
    fun `PLACE - MARKET BUY_OPEN 1 AAPL--210115C00018750 - GOOG--210205C01040000 LAST_PRICE LESS_THAN_OR_EQUAL_TO 1000`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.option,
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.symbol to "AAPL--210115C00018750:OPTNC",
            inputId.strategy to inputId.strategyValueId.call,
            inputId.tradeLegs to "[{\"expiryDay\":\"15\",\"expiryMonth\":\"1\",\"expiryYear\":\"2021\",\"optionType\":\"1\",\"quantity\":1,\"strikePrice\":\"18.75\",\"symbol\":\"AAPL--210115C00018750\",\"transaction\":\"1\"}]",
            inputId.priceType to inputId.priceTypeValueId.market,
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to "0",
            inputId.quantity to "1",
            inputId.action to inputId.actionValueId.buy,
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.contingent,
            inputId.contingentOrderInputId.symbol to "GOOG--210205C01040000:OPTNC",
            inputId.contingentOrderInputId.contingentSymbol to "GOOG--210205C01040000:OPTNC",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.lastPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.lessThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "1000"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to "0",
            inputId.quantity to "1",
            inputId.symbol to "AAPL--210115C00018750",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_OPTION,
            inputId.contingentOrderInputId.securityType to VALUE_SECURITY_TYPE_OPTION,
            inputId.contingentOrderInputId.contingentSymbol to "GOOG--210205C01040000",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.lastPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.lessThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "1000",
            "${OPTION_EXPIRATION_KEY}0" to "1",
            "${OPTION_STRIKE_PRICE_KEY}0" to "18.75",
            "${OPTION_TYPE_KEY}0" to "1",
            OPTION_EXPIRATION_KEY to "1",
            OPTION_STRIKE_PRICE_KEY to "1040",
            OPTION_TYPE_KEY to "1",
            PLATFORM_KEY to PLATFORM_VALUE,
            OPT_LEVEL to "4",
            FORM_TARGET_KEY to "contingentpreview",
            TRADE_X_KEY to "55",
            "${SIDE_KEY}0" to "2",
            "${POSITION_EFFECT_KEY}0" to "1",
            "${ORDER_TYPE_KEY}0" to "3",
            "${ORDER_TRIGGER_KEY}0" to "1",
            "${ADVANCED_ORDER_TYPE_KEY}0" to "7",
            PREVIEW_ID_KEY to "11428708279",
            StockOrderConstants.OVERNIGHT_INDICATOR_FLAG to "0",
            ORDER_COUNT_KEY to "1"
        )

        comparePlaceInputAndOutput(input, output, "11428708279")
    }

    @Test
    fun `PREVIEW - LIMIT SELL_CLOSE 3 AAPL--210115C00117500 - GOOG ASK_PRICE GREATER_THAN_OR_EQUAL_TO 5000`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.option,
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.symbol to "AAPL--210115C00117500:OPTNC",
            inputId.quantity to "3",
            inputId.action to AdvancedOrderOptionsActionValueId.buyToCover,
            inputId.strategy to inputId.strategyValueId.call,
            inputId.tradeLegs to "[{\"expiryDay\":\"15\",\"expiryMonth\":\"1\",\"expiryYear\":\"2021\",\"optionType\":\"1\",\"quantity\":1,\"strikePrice\":\"117.5\",\"symbol\":\"AAPL--210115C00117500\",\"transaction\":\"1\"}]",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "5",
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to "0",
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.contingent,
            inputId.contingentOrderInputId.symbol to "GOOG:EQ",
            inputId.contingentOrderInputId.contingentSymbol to "GOOG:EQ",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.askPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.greaterThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "5000"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "5",
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to "0",
            inputId.action to "2",
            inputId.quantity to "3",
            inputId.symbol to "AAPL--210115C00117500",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_OPTION,
            inputId.contingentOrderInputId.securityType to VALUE_SECURITY_TYPE_STOCK,
            inputId.contingentOrderInputId.symbol to "GOOG",
            inputId.contingentOrderInputId.contingentSymbol to "GOOG",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.askPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.greaterThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "5000",
            "${OPTION_EXPIRATION_KEY}0" to "1",
            "${OPTION_STRIKE_PRICE_KEY}0" to "117.5",
            "${OPTION_TYPE_KEY}0" to "1",
            PLATFORM_KEY to PLATFORM_VALUE,
            FORM_TARGET_KEY to "contingentcreateentry",
            PREVIEW_X_KEY to "53",
            OPT_LEVEL to "4",
            DAY_TRADING_FLAG to "true",
            DAY_TRADING_VER to "1"
        )

        comparePreviewInputAndOutput(input, output)
    }

    @Test
    fun `PLACE - LIMIT SELL_CLOSE 3 AAPL--210115C00117500 - GOOG ASK_PRICE GREATER_THAN_OR_EQUAL_TO 5000`() {
        val input = mapOf(
            inputId.accountId to "31463268",
            inputId.securityType to inputId.securityTypeValueId.option,
            inputId.lookupSymbol to "AAPL:EQ",
            inputId.symbol to "AAPL--210115C00117500:OPTNC",
            inputId.quantity to "3",
            inputId.action to AdvancedOrderOptionsActionValueId.sellShort,
            inputId.strategy to inputId.strategyValueId.call,
            inputId.tradeLegs to "[{\"expiryDay\":\"15\",\"expiryMonth\":\"1\",\"expiryYear\":\"2021\",\"optionType\":\"1\",\"quantity\":1,\"strikePrice\":\"117.5\",\"symbol\":\"AAPL--210115C00117500\",\"transaction\":\"1\"}]",
            inputId.priceType to inputId.priceTypeValueId.limit,
            inputId.limitPrice to "5",
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to "0",
            inputId.advancedOrder to "true",
            inputId.advancedOrderType to inputId.advancedOrderTypeValueId.contingent,
            inputId.contingentOrderInputId.symbol to "GOOG:EQ",
            inputId.contingentOrderInputId.contingentSymbol to "GOOG:EQ",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.askPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.greaterThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "5000"
        )

        val output = mapOf(
            inputId.accountId to "31463268",
            inputId.limitPrice to "5",
            inputId.term to inputId.termValueId.goodUntilCancel,
            inputId.allOrNone to "0",
            inputId.quantity to "3",
            inputId.symbol to "AAPL--210115C00117500",
            PRIMARY_SECURITY_TYPE_KEY to VALUE_SECURITY_TYPE_OPTION,
            inputId.contingentOrderInputId.securityType to VALUE_SECURITY_TYPE_STOCK,
            inputId.contingentOrderInputId.contingentSymbol to "GOOG",
            inputId.contingentOrderInputId.qualifier to inputId.contingentOrderInputId.qualifierValueId.askPrice,
            inputId.contingentOrderInputId.condition to inputId.contingentOrderInputId.conditionValueId.greaterThanOrEqualTo,
            inputId.contingentOrderInputId.conditionValue to "5000",
            "${OPTION_EXPIRATION_KEY}0" to "1",
            "${OPTION_STRIKE_PRICE_KEY}0" to "117.5",
            "${OPTION_TYPE_KEY}0" to "1",
            PLATFORM_KEY to PLATFORM_VALUE,
            OPT_LEVEL to "4",
            FORM_TARGET_KEY to "contingentpreview",
            TRADE_X_KEY to "55",
            "${SIDE_KEY}0" to "3",
            "${POSITION_EFFECT_KEY}0" to "2",
            "${ORDER_TYPE_KEY}0" to "3",
            "${ORDER_TRIGGER_KEY}0" to "2",
            "${ADVANCED_ORDER_TYPE_KEY}0" to "7",
            PREVIEW_ID_KEY to "11428708279",
            StockOrderConstants.OVERNIGHT_INDICATOR_FLAG to "0",
            ORDER_COUNT_KEY to "1"
        )

        comparePlaceInputAndOutput(input, output, "11428708279")
    }
}
