package com.etrade.mobilepro.quotelookup.stock

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.quotelookup.StockLookupBaseFragment
import com.etrade.mobilepro.searchingapi.items.SearchResultItem

class StockQuoteSearchFragment : StockLookupBaseFragment() {

    override val resultsDataSource: LiveData<List<SearchResultItem>?>
        get() = viewModel.stockFundResults

    override fun finishOnSearch(): Boolean = true

    override fun navigateToNextPage(symbol: SearchResultItem.Symbol) {
        viewModel.queryType?.also { type ->
            viewModel.itemSelected(type, symbol)

            dismissOrGoToQuote(symbol)
        }
    }

    override fun getDisclosureResId() = viewModel.searchStockFundDisclosureResId
}
