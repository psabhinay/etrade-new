package com.etrade.mobilepro.landing.repo

import com.etrade.mobilepro.landing.api.LandingPage
import com.etrade.mobilepro.landing.api.LandingState
import com.etrade.mobilepro.landing.api.repo.LandingPagesDataSource
import com.etrade.mobilepro.landing.api.repo.LandingRepo
import com.etrade.mobilepro.preferences.LandingPageParams
import com.etrade.mobilepro.preferences.UserPreferences
import javax.inject.Inject

class DefaultLandingRepo @Inject constructor(
    private val landingPagesDataSource: LandingPagesDataSource,
    private val userPreferences: UserPreferences
) : LandingRepo {

    override fun getLandingState(): LandingState =
        LandingState(
            selected = getSelectedPage() ?: landingPagesDataSource.getDefaultPage(),
            pages = landingPagesDataSource.getLandingPages(),
            userAccounts = landingPagesDataSource.getUserAccounts()
        )

    override fun setSelectedPage(page: LandingPage) = userPreferences.setDefaultLandingPage(
        LandingPageParams(pageId = page.id, pageUuid = page.uuid, accountId = page.accountId)
    )

    private fun getSelectedPage(): LandingPage? {
        val (id, uuid, accountId) = userPreferences.getDefaultLandingPage()
        return if (!id.isNullOrEmpty()) {
            landingPagesDataSource.createPageFrom(id, uuid = uuid ?: "", accountId = accountId)
        } else {
            null
        }
    }
}
