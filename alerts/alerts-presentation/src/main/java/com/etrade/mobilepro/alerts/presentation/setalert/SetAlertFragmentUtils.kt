package com.etrade.mobilepro.alerts.presentation.setalert

import android.content.res.Resources
import android.view.View
import androidx.appcompat.widget.SwitchCompat
import com.etrade.mobilepro.alerts.presentation.R
import com.etrade.mobilepro.util.android.extension.clearFocusSafely
import com.etrade.mobilepro.util.android.extension.hideSoftKeyboard
import com.google.android.material.textfield.TextInputEditText

fun toggleViewsEnabledState(editTexts: List<TextInputEditText>, switches: List<SwitchCompat>, isEnabled: Boolean) {
    // Only need to enable/disable if not already
    if (isEnabled != editTexts.first().isEnabled) {
        editTexts.forEach {
            it.isEnabled = isEnabled
            if (!isEnabled) {
                it.clearFocus()
            }
        }
        switches.forEach { it.isClickable = isEnabled }
    }
}

fun resetViews(editTexts: List<TextInputEditText>, switches: List<SwitchCompat>) {
    editTexts.forEach {
        it.editableText.clear()
        it.clearFocusSafely()
    }
    switches.forEach {
        it.isChecked = false
    }
}

fun clearTextFieldFocuses(editTexts: List<TextInputEditText>, dummy: View, rootView: View?) {
    editTexts.forEach { it.clearFocusSafely() }
    dummy.requestFocus()
    rootView?.hideSoftKeyboard()
}

fun getPeTargetFieldHint(resources: Resources, isHigh: Boolean, hasFocus: Boolean, pe: String?): String {
    val peFormatted = formatPe(pe, hasFocus)
    return when {
        peFormatted.isNullOrEmpty() && isHigh -> resources.getString(R.string.high_pe_target)
        peFormatted.isNullOrEmpty() && !isHigh -> resources.getString(R.string.low_pe_target)
        !peFormatted.isNullOrEmpty() && isHigh -> resources.getString(R.string.high_pe_target_with_value, peFormatted)
        !peFormatted.isNullOrEmpty() && !isHigh -> resources.getString(R.string.low_pe_target_with_value, peFormatted)
        else -> throw IllegalStateException("Unable to define pe target hint text")
    }
}

private fun formatPe(pe: String?, hasFocus: Boolean): String? {
    val peValue = pe?.toDoubleOrNull()
    return when {
        peValue == null -> pe
        hasFocus -> String.format("%.2f", peValue)
        !hasFocus -> String.format("%.1f", peValue)
        else -> throw IllegalStateException("Unable to format pe value")
    }
}
