package com.etrade.mobilepro.biometric.presentation.enabler

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.biometric.api.BiometricProviderError
import com.etrade.mobilepro.biometric.api.BiometricsProvider
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticationException
import com.etrade.mobilepro.biometric.presentation.BiometricAuthenticationFragment
import com.etrade.mobilepro.biometric.presentation.bindCallbacksToExistingBiometricAuthenticationFragment
import com.etrade.mobilepro.biometric.presentation.buildPasswordConfirmDialog
import com.etrade.mobilepro.biometric.presentation.buildPasswordRsaConfirmDialog
import com.etrade.mobilepro.biometric.presentation.showBiometricAuthenticationFragment
import com.etrade.mobilepro.dialog.CustomDialogFragment
import com.etrade.mobilepro.dialog.customDialogRequestCode
import com.etrade.mobilepro.progressoverlay.ProgressDialogFragment
import javax.crypto.Cipher
import javax.inject.Inject

private const val ERROR_MESSAGE_FRAGMENT_TAG = "ERROR_MESSAGE_FRAGMENT_TAG"

private const val INVOKE_ON_SUCCESS = 0
private const val INVOKE_ON_FAIL = 1
private const val INVOKE_ON_CANCEL = 2

class BiometricEnablerFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    private val biometricsProvider: BiometricsProvider
) : Fragment() {

    private val authenticator: (Cipher) -> Unit = { cipher ->
        showBiometricPrompt(cipher)
    }
    private val passwordConfirmPositiveListener: (String) -> Unit = { password ->
        viewModel.validatePassword(password, authenticator)
    }
    private val passwordConfirmNegativeListener: () -> Unit = {
        invokeCallback(INVOKE_ON_CANCEL)
    }
    private val rsaConfirmPositiveListener: (String) -> Unit = { password ->
        viewModel.validatePassword(password, authenticator)
    }
    private val rsaConfirmNegativeListener: () -> Unit = {
        invokeCallback(INVOKE_ON_CANCEL)
    }

    private val passwordConfirmDialog = buildPasswordConfirmDialog(passwordConfirmPositiveListener, passwordConfirmNegativeListener)
    private val passwordRsaConfirmDialog = buildPasswordRsaConfirmDialog(rsaConfirmPositiveListener, rsaConfirmNegativeListener)

    var onAuthenticationSuccess: (() -> Unit)? = null
    var onAuthenticationFail: ((reason: BiometricEnablerError) -> Unit)? = null
    var onAuthenticationCancel: (() -> Unit)? = null

    private val viewModel: BiometricEnablerViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDialogResults()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return View(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBiometricViewModel()
        passwordConfirmDialog.restore(parentFragmentManager)
        passwordRsaConfirmDialog.restore(parentFragmentManager)
        ProgressDialogFragment.restore(parentFragmentManager)?.prepareProgressDialog()
        findBiometricAuthenticationFragmentAndBindCallbacks()
    }

    private fun setupDialogResults() {
        setFragmentResultListener(CustomDialogFragment.CUSTOM_RESULT_OK) { _, bundle ->
            invokeCallback(INVOKE_ON_FAIL, BiometricEnablerError.fromBiometricConstants(bundle.customDialogRequestCode))
        }
    }

    private fun findBiometricAuthenticationFragmentAndBindCallbacks() {
        activity?.supportFragmentManager?.bindCallbacksToExistingBiometricAuthenticationFragment {
            bindBiometricCallbacks()
        }
    }

    fun startEnablingProcess() {
        val biometricResult = biometricsProvider.canAuthenticate()
        val biometricError = biometricResult.providerError
            ?: BiometricProviderError.ERROR_BIOMETRIC_GENERIC
        when {
            !biometricResult.isSuccessful -> showErrorMessage(
                biometricError.errorCode(),
                biometricError.getLabelFrom(resources)
            )
            viewModel.isUserRsa -> passwordRsaConfirmDialog.show(parentFragmentManager)
            else -> passwordConfirmDialog.show(parentFragmentManager)
        }
    }

    private fun setupBiometricViewModel() {
        viewModel.passwordValidationState.observe(
            viewLifecycleOwner,
            {
                when (val viewState = it.getNonConsumedContent()) {
                    is PasswordValidationState.Loading -> ProgressDialogFragment.show(parentFragmentManager).prepareProgressDialog()
                    is PasswordValidationState.Canceled -> invokeCallback(INVOKE_ON_CANCEL, BiometricEnablerError.ERROR_USER_CANCELED)
                    is PasswordValidationState.Success -> ProgressDialogFragment.dismiss(parentFragmentManager)
                    is PasswordValidationState.Failed -> {
                        ProgressDialogFragment.dismiss(parentFragmentManager)
                        showErrorMessage(viewState.biometricEnablerError.biometricConstant, viewState.message)
                    }
                }
            }
        )
    }

    private fun showBiometricPrompt(cipher: Cipher) {
        activity?.supportFragmentManager?.showBiometricAuthenticationFragment(cipher) {
            bindBiometricCallbacks()
        }
    }

    private fun BiometricAuthenticationFragment.bindBiometricCallbacks() {
        onAuthenticationSuccess = {
            viewModel.onCipherAuthenticated(it)
            this@BiometricEnablerFragment.invokeCallback(INVOKE_ON_SUCCESS)
        }
        onAuthenticationFail = {
            viewModel.onCipherAuthenticationFailed(BiometricAuthenticationException())
            this@BiometricEnablerFragment.invokeCallback(INVOKE_ON_FAIL, BiometricEnablerError.fromBiometricConstants(it))
        }
        onAuthenticationCancel = {
            viewModel.onCipherAuthenticationCancelled()
            this@BiometricEnablerFragment.invokeCallback(INVOKE_ON_CANCEL)
        }
        doFinally = {
            removeBiometricAuthenticationFragment()
        }
    }

    private fun BiometricAuthenticationFragment.removeBiometricAuthenticationFragment() {
        activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
    }

    private fun ProgressDialogFragment.prepareProgressDialog() {
        onCancelListener = {
            viewModel.cancelLoginRequest()
        }
    }

    private fun showErrorMessage(errorCode: Int, errorMessage: String) {
        val dialog = CustomDialogFragment.newInstance(
            message = errorMessage,
            okTitle = getString(android.R.string.ok),
            cancelable = false,
            requestCode = errorCode
        )
        dialog.show(parentFragmentManager, ERROR_MESSAGE_FRAGMENT_TAG)
    }

    private fun invokeCallback(type: Int, failureReason: BiometricEnablerError? = null) {
        when (type) {
            INVOKE_ON_SUCCESS -> { onAuthenticationSuccess?.invoke() }
            INVOKE_ON_FAIL -> { onAuthenticationFail?.invoke(failureReason ?: BiometricEnablerError.BIOMETRIC_GENERAL_ERROR) }
            INVOKE_ON_CANCEL -> { onAuthenticationCancel?.invoke() }
        }
        activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
    }
}
