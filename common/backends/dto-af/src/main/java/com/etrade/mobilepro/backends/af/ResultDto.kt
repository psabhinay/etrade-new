package com.etrade.mobilepro.backends.af

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "Result", strict = false)
data class ResultDto constructor(
    @field:Attribute(name = "code", required = false)
    var code: String = "",
    @field:Element(name = "Fault", required = false)
    var fault: FaultDto? = null
) {

    fun isSuccess(): Boolean = code == "0"
}
