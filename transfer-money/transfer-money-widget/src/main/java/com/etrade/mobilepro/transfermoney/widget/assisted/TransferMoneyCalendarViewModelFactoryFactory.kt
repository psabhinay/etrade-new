package com.etrade.mobilepro.transfermoney.widget.assisted

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.transfermoney.widget.TransferMoneyCalendarViewModel
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import org.threeten.bp.LocalDate
import javax.inject.Inject

@Suppress("LongParameterList")
interface TransferMoneyCalendarViewModelFactoryFactory {
    fun create(
        isOriginAccountExternal: Boolean,
        isDestinationAccountExternal: Boolean,
        currentDate: LocalDate,
        initialDate: LocalDate?,
        leadDays: Int?
    ): ViewModelProvider.Factory
}

@Suppress("LongParameterList")
class TransferMoneyCalendarViewModelFactoryFactoryImpl @Inject constructor(
    private val factory: TransferMoneyCalendarViewModel.Factory
) : TransferMoneyCalendarViewModelFactoryFactory {
    override fun create(
        isOriginAccountExternal: Boolean,
        isDestinationAccountExternal: Boolean,
        currentDate: LocalDate,
        initialDate: LocalDate?,
        leadDays: Int?
    ): ViewModelProvider.Factory =
        viewModelFactory {
            factory.create(
                isOriginAccountExternal = isOriginAccountExternal,
                isDestinationAccountExternal = isDestinationAccountExternal,
                currentDate = currentDate,
                initialDate = initialDate,
                leadDays = leadDays
            )
        }
}
