package com.etrade.mobilepro.movers.markets

import android.content.res.Resources
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.R
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState
import com.etrade.mobilepro.movers.Movers
import com.etrade.mobilepro.movers.MoversRouter
import com.etrade.mobilepro.movers.markets.dto.MarketMoverViewDto
import com.etrade.mobilepro.movers.markets.viewfactory.MarketMoverViewFactory
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.streaming.api.Level1Field
import com.etrade.mobilepro.streaming.api.StreamingQuoteProvider
import com.etrade.mobilepro.streaming.api.StreamingStatusController
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

@ExperimentalCoroutinesApi
internal class MarketMoverViewFactoryTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    private val mockGetMarketMoversUseCase = mock<GetMarketMoversUseCase>()

    private val marketMoverViewDto = MarketMoverViewDto("", provideDummyMarketMoverCTA())
    private val mockMainNavigation = mock<MoversRouter>()
    private val streamingQuoteProvider: StreamingQuoteProvider<Level1Field, Level1Data, Any> = mock()
    private val streamingStatusController: StreamingStatusController = mock()
    private val mockRes = lazy { mockResourcesString() }.value
    private val sut = MarketMoverViewFactory(
        mockGetMarketMoversUseCase,
        mockMainNavigation,
        streamingQuoteProvider,
        streamingStatusController,
        mockRes
    )
    private val genericLayout = sut.createView(marketMoverViewDto)

    private fun mockResourcesString() = mock<Resources> {
        on { getString(R.string.portfolio_movers_no_positions) } doReturn ("No positions in this portfolio")
        on { getString(R.string.unknown_server_error) } doReturn ("Something went wrong. Please try again.")
        on { getString(R.string.market_movers_title) } doReturn ("Market Movers")
        on { getString(R.string.nasdaq_most_active) } doReturn("NASDAQ Most Active")
        on { getString(R.string.view_all) } doReturn ("View all")
        on { getString(R.string.market_mover_deep_link) } doReturn ("etrade://markets/movers")
    }

    @Before
    fun setUp() {
        Dispatchers.setMain(testCoroutineDispatcher)
    }

    @After
    fun cleanup() {
        Dispatchers.resetMain()
        testCoroutineDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `fetches market movers`() {
        val movers = sut.createView(marketMoverViewDto).viewModelFactory.create(Movers::class.java)
        runBlockingTest {
            whenever(mockGetMarketMoversUseCase.execute(Unit)).thenReturn(ETResult.success(emptyList()))
            movers.getMovers()
            verify(mockGetMarketMoversUseCase).execute(Unit)
        }
    }

    @Test
    fun `returns error message when movers call is not successful`() {
        runBlocking(testCoroutineDispatcher) {
            whenever(mockGetMarketMoversUseCase.execute(Unit)).thenReturn(ETResult.failure(RuntimeException("Something went wrong!")))
        }
        val marketMovers = genericLayout.viewModelFactory.create(genericLayout.viewModelClass) as Movers
        marketMovers.getMovers()
        marketMovers.viewState.observeForever {
            Assert.assertEquals(DynamicUiViewState.Error(), it)
        }
    }

    @Test
    fun `view model creation when market movers service is successful`() {
        Assert.assertNotNull(genericLayout)
        val movers = genericLayout.viewModelFactory.create(genericLayout.viewModelClass) as Movers
        Assert.assertEquals(R.layout.layout_movers, movers.layoutId)
        Assert.assertNotNull(movers)
    }

    private fun provideDummyMarketMoverCTA(): List<CallToActionDto> =
        listOf(CallToActionDto("View all Market Movers", ClickActionDto("etrade://markets/movers")))
}
