package com.etrade.mobilepro.portfolio.data

import com.etrade.mobilepro.dagger.qualifiers.AllBrokerage
import com.etrade.mobilepro.dagger.qualifiers.Portfolio
import com.etrade.mobilepro.portfolio.PortfolioPositionsDisplayMode
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.positions.presentation.PortfolioTableViewType
import com.etrade.mobilepro.positions.presentation.PositionTableMode
import com.etrade.mobilepro.tableviewutils.api.ColumnRepo
import java.util.Collections
import javax.inject.Inject

/**
 * Stores order of columns for portfolio table.
 */
interface PortfolioColumnRepo {

    /**
     * Gets portfolio columns for a provided view type.
     *
     * @param positionTableMode position list mode
     * @param viewType view type
     * @param portfolioDisplayMode portfolio display mode
     *
     * @return list of columns
     */
    suspend fun getPortfolioColumns(
        positionTableMode: PositionTableMode,
        viewType: PortfolioTableViewType,
        portfolioDisplayMode: PortfolioPositionsDisplayMode
    ): List<PortfolioColumn>

    /**
     * Saves list of [columns] for a specified [viewType].
     *
     * @param positionTableMode position list mode
     * @param viewType view type
     * @param columns list of columns to save
     * @param portfolioDisplayMode portfolio display mode
     */
    suspend fun savePortfolioColumns(
        positionTableMode: PositionTableMode,
        viewType: PortfolioTableViewType,
        columns: List<PortfolioColumn>,
        portfolioDisplayMode: PortfolioPositionsDisplayMode
    )

    /**
     * Adds a [listener] for columns changes.
     *
     * @param listener listens for changes in columns data
     */
    fun addOnColumnsDataChangedListener(listener: OnColumnsDataChangedListener)

    /**
     * Removes a [listener] for columns changes.
     *
     * @param listener listens for changes in columns data
     */
    fun removeOnColumnsDataChangedListener(listener: OnColumnsDataChangedListener)

    /**
     * Listens for columns data changes.
     */
    interface OnColumnsDataChangedListener {

        /**
         * Called when repository has been updated.
         */
        fun onColumnDataChanged()
    }
}

/**
 * Default implementation of [PortfolioColumnRepo].
 */
class PortfolioColumnRepoImpl @Inject constructor(
    @AllBrokerage private val allBrokerageStandardViewColumnRepo: StandardViewColumnRepo,
    @Portfolio private val portfolioStandardViewColumnRepo: StandardViewColumnRepo,
    private val portfolioOptionsViewColumnRepo: OptionsViewColumnRepo,
    private val portfolioListViewColumnRepo: ListViewColumnRepo,
    private val portfolioTileViewColumnRepo: TileViewColumnRepo
) : PortfolioColumnRepo {

    private val listeners: MutableList<PortfolioColumnRepo.OnColumnsDataChangedListener> =
        Collections.synchronizedList(mutableListOf())

    override suspend fun getPortfolioColumns(
        positionTableMode: PositionTableMode,
        viewType: PortfolioTableViewType,
        portfolioDisplayMode: PortfolioPositionsDisplayMode
    ): List<PortfolioColumn> {
        return getColumnRepo(positionTableMode, viewType, portfolioDisplayMode).getColumns()
    }

    override suspend fun savePortfolioColumns(
        positionTableMode: PositionTableMode,
        viewType: PortfolioTableViewType,
        columns: List<PortfolioColumn>,
        portfolioDisplayMode: PortfolioPositionsDisplayMode
    ) {
        getColumnRepo(positionTableMode, viewType, portfolioDisplayMode)
            .saveColumns(columns)
            .also {
                synchronized(listeners) {
                    listeners.forEach {
                        it.onColumnDataChanged()
                    }
                }
            }
    }

    override fun addOnColumnsDataChangedListener(listener: PortfolioColumnRepo.OnColumnsDataChangedListener) {
        listeners.add(listener)
    }

    override fun removeOnColumnsDataChangedListener(listener: PortfolioColumnRepo.OnColumnsDataChangedListener) {
        listeners.remove(listener)
    }

    private fun getColumnRepo(
        positionTableMode: PositionTableMode,
        portfolioTableViewType: PortfolioTableViewType,
        portfolioPositionsDisplayMode: PortfolioPositionsDisplayMode
    ): ColumnRepo<PortfolioColumn> {
        return when (positionTableMode) {
            PositionTableMode.ALL_BROKERAGE_VIEW -> allBrokerageStandardViewColumnRepo
            PositionTableMode.PORTFOLIO_VIEW -> when (portfolioPositionsDisplayMode) {
                PortfolioPositionsDisplayMode.TABLE -> getTableRepo(portfolioTableViewType)
                PortfolioPositionsDisplayMode.LIST -> portfolioListViewColumnRepo
                PortfolioPositionsDisplayMode.TILE -> portfolioTileViewColumnRepo
            }
        }
    }

    private fun getTableRepo(portfolioTableViewType: PortfolioTableViewType) =
        when (portfolioTableViewType) {
            PortfolioTableViewType.STANDARD_VIEW -> portfolioStandardViewColumnRepo
            PortfolioTableViewType.OPTIONS_VIEW -> portfolioOptionsViewColumnRepo
        }
}
