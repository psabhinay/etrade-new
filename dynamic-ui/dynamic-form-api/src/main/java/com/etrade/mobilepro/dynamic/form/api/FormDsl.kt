package com.etrade.mobilepro.dynamic.form.api

import androidx.annotation.LayoutRes
import androidx.annotation.StyleRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.common.ToolbarActionEnd
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.livedata.combineWith
import com.etrade.mobilepro.livedata.merge
import com.etrade.mobilepro.util.ValueCoder
import com.etrade.mobilepro.util.ValueFormatter

fun form(init: FormBuilder.() -> Unit): Form {
    return FormBuilder().apply(init).create()
}

fun action(id: Any, init: ActionBuilder.() -> Unit): Action {
    return ActionBuilder(id).apply(init).create()
}

@DslMarker
annotation class FormDsl

@FormDsl
class FormBuilder internal constructor() : InputHolder {

    private val groups: MutableList<Group> = mutableListOf()
    private val inputMap: MutableMap<String, Input<Any>> = mutableMapOf()
    private val hiddenInputs: MutableList<HiddenInput> = mutableListOf()

    private var actions: List<Action> = emptyList()

    @Suppress("UNCHECKED_CAST")
    override fun <T : Input<*>> getInput(id: String): T? = inputMap[id] as T?

    fun actions(onActionClickListener: OnActionClickListener, init: ActionListBuilder.() -> Unit) {
        actions = ActionListBuilder(this, onActionClickListener).apply(init).create()
    }

    fun verticalGroup(init: GroupBuilder.() -> Unit) {
        groups += VerticalGroupBuilder(inputMap, this).apply(init).create()
    }

    fun hidden(id: String, init: HiddenInputBuilder.() -> Unit = { /* Intentionally blank. */ }) {
        val input = HiddenInputBuilder(id, this).apply(init).create()
        hiddenInputs += input
        @Suppress("UNCHECKED_CAST")
        inputMap[id] = input as Input<Any>
    }

    fun rules(init: RuleListBuilder.() -> Unit) = RuleListBuilder(this).init()

    internal fun create(): Form = Form(groups.toList(), actions.toList(), hiddenInputs.toList())
}

// region Actions

@FormDsl
class ActionListBuilder internal constructor(
    private val inputHolder: InputHolder,
    private val onActionClickListener: OnActionClickListener
) {

    private val actions: MutableList<Action> = mutableListOf()

    fun action(id: Any, init: FormActionBuilder.() -> Unit) {
        actions += FormActionBuilder(id, inputHolder, onActionClickListener).apply(init).create()
    }

    fun dynamicDependentIdAction(id: Any, init: DynamicDependentIdFormActionBuilder.() -> Unit) {
        actions += DynamicDependentIdFormActionBuilder(id, inputHolder, onActionClickListener).apply(init).create()
    }

    fun create(): List<Action> = actions.toList()
}

@FormDsl
open class ActionBuilder internal constructor(
    private val id: Any
) {

    var actionType: ActionType = ActionType.PRIMARY
    var title: String = ""
    var onActionClick: (Any) -> Unit = { /* Intentionally blank. */ }
        set(value) {
            field = value
            onActionClickListener = object : OnActionClickListener {
                override fun onActionClick(actionId: Any) = value(actionId)
            }
        }

    protected var onActionClickListener: OnActionClickListener = object : OnActionClickListener {
        override fun onActionClick(actionId: Any) = Unit
    }

    var isEnabled: LiveData<Boolean> = MutableLiveData(true)

    fun create(): Action = Action(id, title, actionType, isEnabled, onActionClickListener)

    open fun dependsOnValuesOf(inputIds: Collection<String>, disableIfNoChange: Boolean, defaultValues: InputValuesSnapshot) = Unit
}

@FormDsl
open class FormActionBuilder internal constructor(
    id: Any,
    private val inputHolder: InputHolder,
    listener: OnActionClickListener
) : ActionBuilder(id) {

    init {
        onActionClickListener = listener
    }

    override fun dependsOnValuesOf(inputIds: Collection<String>, disableIfNoChange: Boolean, defaultValues: InputValuesSnapshot) {
        if (inputIds.isEmpty()) {
            return
        }

        isEnabled = combineValuesOfIds(inputIds, disableIfNoChange, defaultValues)
    }

    internal fun combineValuesOfIds(inputIds: Collection<String>, disableIfNoChange: Boolean, defaultValues: InputValuesSnapshot): LiveData<Boolean> {
        if (inputIds.isEmpty()) {
            return MutableLiveData(false)
        }

        val iterator = inputIds.iterator()
        var result = combineHasValueWithIsVisible(iterator.next())
        while (iterator.hasNext()) {
            result = result.combineWith(combineHasValueWithIsVisible(iterator.next())) { one, two ->
                one && two == true
            }
        }
        return if (disableIfNoChange) {
            combineHasChanges(inputIds, defaultValues, result)
        } else {
            result
        }
    }

    private fun combineHasValueWithIsVisible(inputId: String): LiveData<Boolean> {
        val input = inputHolder.requireAnyInput(inputId)
        return if (input.isVisible == input.hasValue) {
            input.hasValue
        } else {
            input.hasValue.combineWith(input.isVisible) { hasValue, isVisible ->
                if (isVisible == true) {
                    hasValue
                } else {
                    true
                }
            }
        }
    }

    private fun combineHasChanges(inputIds: Collection<String>, defaultValues: InputValuesSnapshot, result: LiveData<Boolean>) =
        result.combineWith(hasAnyChangedInput(inputIds, defaultValues)) { existing, hasChange ->
            if (hasChange == true) {
                existing
            } else {
                false
            }
        }

    private fun hasAnyChangedInput(inputIds: Collection<String>, defaultValues: InputValuesSnapshot): LiveData<Boolean> {
        var hasAnyChangedInputLiveData: LiveData<Boolean> = MutableLiveData(false)
        val iterator = inputIds.iterator()
        while (iterator.hasNext()) {
            hasAnyChangedInputLiveData = hasAnyChangedInputLiveData
                .combineWith(inputHolder.requireAnyInput(iterator.next()).value) { _, _ ->
                    hasChange(inputIds, defaultValues)
                }
        }
        return hasAnyChangedInputLiveData
    }

    private fun hasChange(inputIds: Collection<String>, defaultValues: InputValuesSnapshot): Boolean {
        val iterator = inputIds.iterator()
        while (iterator.hasNext()) {
            val next = inputHolder.requireAnyInput(iterator.next())
            if (next.parameterValue.value != defaultValues[next.id]) {
                return true
            }
        }
        return false
    }
}

// endregion
@FormDsl
class DynamicDependentIdFormActionBuilder internal constructor(
    id: Any,
    inputHolder: InputHolder,
    listener: OnActionClickListener
) : FormActionBuilder(id, inputHolder, listener) {

    private val _shouldEnable: MutableLiveData<Boolean> = MutableLiveData(false)
    private val dependentIdsLiveDta: MutableLiveData<Collection<String>> = MutableLiveData(emptyList())

    override fun dependsOnValuesOf(inputIds: Collection<String>, disableIfNoChange: Boolean, defaultValues: InputValuesSnapshot) {
        dependentIdsLiveDta.observeForever { ids ->
            combineValuesOfIds(ids, disableIfNoChange, defaultValues).observeForever {
                _shouldEnable.value = it
            }
        }

        isEnabled = _shouldEnable

        if (inputIds.isEmpty()) {
            return
        }
        dependentIdsLiveDta.value = inputIds
    }

    fun updateDependentIds(ids: LiveData<Collection<String>>) {
        ids.observeForever {
            dependentIdsLiveDta.value = it
        }
    }
}

// region Components

@FormDsl
abstract class GroupBuilder internal constructor(
    private val inputMap: MutableMap<String, Input<Any>>,
    inputHolder: InputHolder
) : ComponentBuilder(inputHolder) {

    protected val components: MutableList<Component> = mutableListOf()

    fun horizontalGroup(init: GroupBuilder.() -> Unit) {
        components += HorizontalGroupBuilder(inputMap, this).apply(init).create()
    }

    fun verticalGroup(init: GroupBuilder.() -> Unit) {
        components += VerticalGroupBuilder(inputMap, this).apply(init).create()
    }

    fun custom(createComponent: InputHolder.() -> Component) {
        val component = createComponent()
        components += component
        if (component is Input<*>) {
            @Suppress("UNCHECKED_CAST")
            inputMap[component.id] = component as Input<Any>
        }
    }

    fun staticText(init: StaticTextBuilder.() -> Unit) {
        components += StaticTextBuilder(this).apply(init).create()
    }

    fun dynamicText(init: DynamicTextBuilder.() -> Unit) {
        components += DynamicTextBuilder(this).apply(init).create()
    }

    fun layout(@LayoutRes resId: Int, init: LayoutBuilder.() -> Unit) {
        components += LayoutBuilder(resId, this).apply(init).create()
    }

    fun <T> externalSelect(id: String, valueCoder: ValueCoder<T>, init: ExternalInputBuilder<T>.() -> Unit) {
        createInput(id, ExternalInputBuilder(id, this, valueCoder), init)
    }

    fun select(id: String, init: SelectInputBuilder.() -> Unit) = createInput(id, SelectInputBuilder(id, this), init)
    fun switch(id: String, init: SwitchInputBuilder.() -> Unit) = createInput(id, SwitchInputBuilder(id, this), init)
    fun text(id: String, init: TextInputBuilder.() -> Unit) = createInput(id, TextInputBuilder(id, this), init)

    private fun <P, T : InputBuilder<P>> createInput(id: String, builder: T, init: T.() -> Unit = { /* intentionally blank */ }) {
        builder.apply(init).create().also {
            components += it
            @Suppress("UNCHECKED_CAST")
            inputMap[id] = it as Input<Any>
        }
    }
}

class HorizontalGroupBuilder internal constructor(
    inputMap: MutableMap<String, Input<Any>>,
    inputHolder: InputHolder
) : GroupBuilder(inputMap, inputHolder) {
    override fun create(): Group = HorizontalGroup(components.toList(), isVisible)
}

class VerticalGroupBuilder internal constructor(
    inputMap: MutableMap<String, Input<Any>>,
    inputHolder: InputHolder
) : GroupBuilder(inputMap, inputHolder) {
    override fun create(): Group = VerticalGroup(components.toList(), isVisible)
}

@FormDsl
class LayoutBuilder internal constructor(@LayoutRes private val resId: Int, inputHolder: InputHolder) : ComponentBuilder(inputHolder) {

    private var bindings: Iterable<Layout.Binding> = emptyList()

    fun bindings(init: LayoutBindingListBuilder.() -> Unit) {
        bindings = LayoutBindingListBuilder().apply(init).create()
    }

    fun rule(init: InputHolder.() -> Unit) = init()

    override fun create(): Layout = Layout(resId, bindings, isVisible)
}

@FormDsl
class LayoutBindingListBuilder internal constructor() {

    private val bindings: MutableSet<Layout.Binding> = mutableSetOf()

    fun binding(variableId: Int, init: LayoutBindingBuilder.() -> Unit) {
        bindings += LayoutBindingBuilder(variableId).apply(init).create()
    }

    fun create(): Set<Layout.Binding> = bindings.toSet()
}

class LayoutBindingBuilder internal constructor(private val variableId: Int) {

    var variable: Any? = null

    fun create(): Layout.Binding = Layout.Binding(variableId, variable)
}

@FormDsl
abstract class InputBuilder<T> protected constructor(protected val id: String, inputHolder: InputHolder) : ComponentBuilder(inputHolder) {

    var title: String = ""
        set(value) {
            dynamicTitle = MutableLiveData(value)
        }

    var contentDescription: CharSequence? = null

    var contentDescriptionFormatter: ContentDescriptionFormatter? = null

    protected var dynamicTitle: LiveData<CharSequence?> = MutableLiveData()

    protected var isEnabled: LiveData<Boolean> = MutableLiveData(true)

    protected var shouldInterceptClick: LiveData<Boolean> = MutableLiveData(false)

    fun dynamicTitle(init: InputHolder.() -> LiveData<CharSequence?>) {
        dynamicTitle = init()
    }

    fun enabledIf(init: InputHolder.() -> LiveData<Boolean>) {
        isEnabled = init()
    }

    fun interceptClickIf(init: InputHolder.() -> LiveData<Boolean>) {
        shouldInterceptClick = MutableLiveData(false).merge(init())
    }
}

@FormDsl
abstract class ComponentBuilder constructor(inputHolder: InputHolder) : InputHolder by inputHolder {

    protected var isVisible: LiveData<Boolean> = MutableLiveData(true)

    fun visibleIf(init: InputHolder.() -> LiveData<Boolean>) {
        isVisible = init()
    }

    abstract fun create(): Component
}

class ExternalInputBuilder<T> internal constructor(
    id: String,
    inputHolder: InputHolder,
    private val valueCoder: ValueCoder<T>
) : InputBuilder<T>(id, inputHolder) {

    var action: ExternalAction = { /* Intentionally blank. */ }
    var doBeforeReset: () -> Unit = { /* Intentionally blank. */ }
    var showClearAction: Boolean = false
    var value: T? = null
    var valueFormatter: (T) -> CharSequence = { it.toString() }
    var isValid: (T) -> Boolean = { true }
    var onInvalidValue: () -> Unit = { /* Intentionally blank. */ }

    private val formatter: ValueFormatter<T>
        get() = object : ValueFormatter<T> {
            override fun format(value: T): CharSequence = valueFormatter(value)
        }

    override fun create(): ExternalInput<T> {
        return ExternalInput(
            id = id,
            title = dynamicTitle,
            contentDescription = contentDescription,
            contentDescriptionFormatter = contentDescriptionFormatter,
            action = action,
            showClearAction = showClearAction,
            initialValue = value,
            isVisible = isVisible,
            isEnabled = isEnabled,
            valueCoder = valueCoder,
            valueFormatter = formatter,
            isValid = isValid,
            onInvalidValue = onInvalidValue,
            doBeforeReset = doBeforeReset
        )
    }
}

open class SelectInputBuilder constructor(id: String, inputHolder: InputHolder) : InputBuilder<DropDownOption>(id, inputHolder) {

    var doNotReset: Boolean = false
    var initialOptionId: String? = null
    var defaultOptionId: String? = null
    var defaultOptionIndex: Int = -1
    var toolbarActionEnd: ToolbarActionEnd? = null

    protected var options: DropDown = DropDown(emptyList(), null, null, doNotReset)

    fun dynamic(init: DynamicDropDownBuilder.() -> Unit) {
        options = DropDown(DynamicDropDownBuilder(this).apply(init).create(), initialOptionId, defaultOptionId)
    }

    fun options(init: DropDownOptionsBuilder.() -> Unit) {
        val optionList = DropDownOptionsBuilder().apply(init).create()
        options = DropDown(optionList, initialOptionId, defaultOptionId ?: optionList.getOrNull(defaultOptionIndex)?.id, doNotReset)
    }

    override fun create(): SelectInput {
        return SelectInput(
            id,
            dynamicTitle,
            contentDescription,
            contentDescriptionFormatter,
            isVisible,
            isEnabled,
            shouldInterceptClick,
            options,
            toolbarActionEnd
        )
    }
}

class SwitchInputBuilder internal constructor(id: String, inputHolder: InputHolder) : InputBuilder<Boolean>(id, inputHolder) {

    var value: Boolean? = null

    var valueCoder: ValueCoder<Boolean> = object : ValueCoder<Boolean> {
        override fun encode(value: Boolean): Any = value.toString()
        override fun decode(encoded: Any): Boolean? = (encoded as? String)?.toBoolean()
    }

    private val valueFormatter: ValueFormatter<Boolean>
        get() = object : ValueFormatter<Boolean> {
            override fun format(value: Boolean): CharSequence = value.toString()
        }

    override fun create(): SwitchInput {
        return SwitchInput(
            id = id,
            title = dynamicTitle,
            contentDescription = contentDescription,
            contentDescriptionFormatter = contentDescriptionFormatter,
            initialValue = value,
            valueCoder = valueCoder,
            valueFormatter = valueFormatter,
            isVisible = isVisible,
            isEnabled = isEnabled
        )
    }
}

class HiddenInputBuilder internal constructor(id: String, inputHolder: InputHolder) : InputBuilder<String>(id, inputHolder) {
    var value: String? = null
    var formatter: (String) -> CharSequence = { it }

    var valueCoder: ValueCoder<String> = object : ValueCoder<String> {
        override fun encode(value: String): Any = value
        override fun decode(encoded: Any): String? = encoded as? String
    }

    private val valueFormatter: ValueFormatter<String>
        get() = object : ValueFormatter<String> {
            override fun format(value: String): CharSequence = formatter(value)
        }

    override fun create(): HiddenInput = HiddenInput(
        id = id,
        initialValue = value,
        valueCoder = valueCoder,
        valueFormatter = valueFormatter,
        isVisible = isVisible,
        isEnabled = isEnabled
    )
}

class TextInputBuilder internal constructor(id: String, inputHolder: InputHolder) : InputBuilder<String>(id, inputHolder) {

    var value: String? = null
    var textInputIcon: TextInputIcon? = null
    var textInputType: TextInputType? = null
        set(value) {
            _textInputType = MutableLiveData(value)
        }
    var formatter: (String) -> CharSequence = { it }

    var valueCoder: ValueCoder<String> = object : ValueCoder<String> {
        override fun encode(value: String): Any = value
        override fun decode(encoded: Any): String? = encoded as? String
    }

    private val valueFormatter: ValueFormatter<String>
        get() = object : ValueFormatter<String> {
            override fun format(value: String): CharSequence = formatter(value)
        }

    private var _textInputType: LiveData<TextInputType> = MutableLiveData(textInputType)

    fun dynamicTextInputType(init: InputHolder.() -> LiveData<TextInputType>) {
        _textInputType = init()
    }

    override fun create(): TextInput {
        return TextInput(
            id = id,
            title = dynamicTitle,
            contentDescription = contentDescription,
            contentDescriptionFormatter = contentDescriptionFormatter,
            initialValue = value,
            valueCoder = valueCoder,
            valueFormatter = valueFormatter,
            isVisible = isVisible,
            isEnabled = isEnabled,
            textInputIcon = textInputIcon,
            textInputType = _textInputType
        )
    }
}

@FormDsl
class StaticTextBuilder internal constructor(
    inputHolder: InputHolder
) : ComponentBuilder(inputHolder) {

    var text: String = ""

    @StyleRes
    var styleId: Int? = 0

    override fun create(): Component = StaticText(text, styleId)
}

@FormDsl
class DynamicTextBuilder internal constructor(inputHolder: InputHolder) : ComponentBuilder(inputHolder) {

    var title: String = ""

    var displayValue: LiveData<CharSequence?> = MutableLiveData()

    fun displayValue(init: InputHolder.() -> LiveData<CharSequence?>) {
        displayValue = init()
    }

    override fun create(): Component = DynamicText(title, displayValue)
}

@FormDsl
class DynamicDropDownBuilder internal constructor(private val inputHolder: InputHolder) {

    private var options: LiveData<List<DropDownOption>> = MutableLiveData()

    fun rule(init: InputHolder.() -> LiveData<List<DropDownOption>>) {
        options = inputHolder.run(init)
    }

    internal fun create(): LiveData<List<DropDownOption>> = options
}

@FormDsl
class DropDownOptionsBuilder internal constructor() {

    private val options: MutableList<DropDownOption> = mutableListOf()

    fun option(id: String, init: DropDownOptionBuilder.() -> Unit) {
        options.add(DropDownOptionBuilder(id).apply(init).create())
    }

    internal fun create(): List<DropDownOption> = options.toList()
}

@FormDsl
class DropDownOptionBuilder internal constructor(val id: String) {

    var title: String = ""
    var description: String = ""
    var contentDescription: String? = null

    internal fun create(): DropDownOption {
        val dropdownContentDescription = if (contentDescription == null) {
            "$title\n$description"
        } else {
            contentDescription.orEmpty()
        }
        return DropDownOption(id, title, description, dropdownContentDescription)
    }
}

// endregion

// region Rules

@FormDsl
class RuleListBuilder internal constructor(private val inputHolder: InputHolder) {
    fun rule(init: InputHolder.() -> Unit) = inputHolder.init()
}

// endregion
