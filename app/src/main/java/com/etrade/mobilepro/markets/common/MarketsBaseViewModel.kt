package com.etrade.mobilepro.markets.common

import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.eo.core.util.DateFormattingUtils.formatFullDateTime
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.markets.data.MoverService
import com.etrade.mobilepro.screener.api.result.ResultDataCell
import com.etrade.mobilepro.streaming.api.Level1Data
import com.etrade.mobilepro.tableviewutils.presentation.cell.Cell
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.etrade.mobilepro.util.color.DataColorResolver
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

abstract class MarketsBaseViewModel(
    protected val marketsService: MoverService,
    private val dataColorResolver: DataColorResolver
) : ViewModel() {

    abstract val columnHeaderList: List<Cell>

    val viewState: LiveData<ViewState>
        get() = mutableViewState

    protected abstract val viewModelType: Class<out MarketsBaseViewModel>

    protected val mutableViewState = MutableLiveData<ViewState>()

    protected val logger by lazy { LoggerFactory.getLogger(viewModelType) }

    abstract suspend fun fetchDataFromServer(): ViewState

    fun fetch() {
        if (viewState.value == null) {
            refresh()
        }
    }

    fun refresh() {
        beforeRefresh()
        viewModelScope.launch {
            val state = async { fetchDataFromServer() }
            when (val result = state.await()) {
                is ViewState.Success -> onSuccess(result)
                is ViewState.Error -> onError(result.message, result.throwable)
                is ViewState.Empty -> onEmpty()
            }
        }
    }

    @CallSuper
    protected open fun onSuccess(result: ViewState.Success) {
        mutableViewState.postValue(result)
    }

    @CallSuper
    protected open fun beforeRefresh() {
        mutableViewState.value = ViewState.Loading
    }

    protected open fun buildSymbolRowItem(
        symbolInfo: SymbolRowInfo,
        fromMoversResponse: Boolean = false,
        instrumentType: InstrumentType = InstrumentType.EQ
    ): List<ResultDataCell> {
        return symbolInfo.buildSymbolRowItem(fromMoversResponse, instrumentType, dataColorResolver)
    }

    protected fun formatTimeStamp(timeStampInMillis: Long): String = formatFullDateTime(timeStampInMillis)

    protected fun updateViewStateForSymbol(
        model: ViewState.Success,
        symbolCells: List<ResultDataCell>,
        symbol: String,
        updatedTimeStamp: String?
    ): ViewState.Success {
        return ViewState.Success(
            model.rowHeaders,
            getUpdatedCellItemsForSymbol(model.cellItems, symbolCells, symbol),
            updatedTimeStamp,
            model.indices
        )
    }

    protected fun buildSymbolCells(symbol: String, instrumentType: InstrumentType, data: Level1Data, lastPrice: String): List<ResultDataCell> {
        val symbolRowInfo = getSymbolRowInfo(data, symbol, lastPrice)
        return buildSymbolRowItem(symbolRowInfo, instrumentType = instrumentType)
    }

    private fun onEmpty() {
        mutableViewState.postValue(ViewState.Empty)
    }

    private fun onError(message: String?, throwable: Throwable?) {
        logger.error(message, throwable)
        mutableViewState.postValue(ViewState.Error())
    }

    sealed class ViewState {
        object Loading : ViewState()
        object Empty : ViewState()
        data class Error(val message: String? = "Something went wrong!", val throwable: Throwable? = null) : ViewState()
        data class Success(
            val rowHeaders: List<SymbolCell>,
            val cellItems: List<List<ResultDataCell>>,
            val timeStamp: String?,
            val indices: List<DynamicViewModel>? = null
        ) : ViewState()
    }
}
