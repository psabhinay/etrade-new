package com.etrade.mobilepro.trade.form.api.input

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.trade.form.api.value.OrderTermValueId
import com.etrade.mobilepro.trade.form.api.value.PriceTypeValueId
import com.etrade.mobilepro.util.ValueCoder

interface WithPriceTradeFormInputId : TradeFormInputId {
    val allOrNone: String
    val allOrNoneValueCoder: ValueCoder<Boolean>
    val limitPrice: String
    val limitPriceForStopLimit: String
    val priceType: String
    val priceTypeValueId: PriceTypeValueId
    val stopPrice: String
    val stopPriceForStopLimit: String
    val term: String
    val termValueId: OrderTermValueId
}

fun WithPriceTradeFormInputId.extractAllOrNone(map: InputValuesSnapshot): Boolean? {
    return map[allOrNone]?.let {
        allOrNoneValueCoder.decode(it)
    }
}

fun WithPriceTradeFormInputId.extractPriceType(map: InputValuesSnapshot): PriceType? {
    return (map[priceType] as? String)?.let {
        priceTypeValueId.converter.toValue(it)
    }
}

fun WithPriceTradeFormInputId.extractTerm(map: InputValuesSnapshot): OrderTerm? {
    return (map[term] as? String)?.let {
        termValueId.converter.toValue(it)
    }
}
