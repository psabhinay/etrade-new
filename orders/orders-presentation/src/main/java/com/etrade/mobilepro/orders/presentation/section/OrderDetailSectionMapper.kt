package com.etrade.mobilepro.orders.presentation.section

import android.content.res.Resources
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderDetailDescription
import com.etrade.mobilepro.orders.api.AdvancedOrderType
import com.etrade.mobilepro.orders.api.OrderType
import com.etrade.mobilepro.orders.presentation.section.viewfactory.AdvancedOrderDetailSectionViewFactory
import com.etrade.mobilepro.orders.presentation.section.viewfactory.ContingentOrderSectionViewFactory
import com.etrade.mobilepro.orders.presentation.section.viewfactory.MultiLegsOrderSectionViewFactory
import com.etrade.mobilepro.orders.presentation.section.viewfactory.OptionOrderSectionViewFactory
import com.etrade.mobilepro.orders.presentation.section.viewfactory.OrderDetailSectionViewFactory
import com.etrade.mobilepro.orders.presentation.section.viewfactory.StockOrderSectionViewFactory
import com.etrade.mobilepro.orders.presentation.section.viewfactory.StockPlanOrderSectionViewFactory
import com.etrade.mobilepro.orders.presentation.section.viewfactory.getPartialInfo
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout

/**
 * Maps the details to list of section items
 *
 * supported Order types:
 * 1. Order,
 * 2. Option
 * 3. Stock plan orders
 */
class OrderDetailSectionMapper(
    private val resources: OrderDetailLabelResources
) {
    private val orderDetailViewMap = mutableMapOf<OrderType, OrderDetailSectionViewFactory>()
    private val advancedOrderDetailMap = mutableMapOf<AdvancedOrderType, AdvancedOrderDetailSectionViewFactory>()
    private val stockPlanOrderDetailMap = StockPlanOrderSectionViewFactory()

    init {
        orderDetailViewMap[OrderType.EQUITY] = StockOrderSectionViewFactory()
        orderDetailViewMap[OrderType.MONEY_MARKET_FUND] = StockOrderSectionViewFactory()
        orderDetailViewMap[OrderType.OPTION_ASSIGNED] = OptionOrderSectionViewFactory()
        orderDetailViewMap[OrderType.OPTION_EXERCISED] = OptionOrderSectionViewFactory()
        orderDetailViewMap[OrderType.SIMPLE_OPTION] = OptionOrderSectionViewFactory()
        orderDetailViewMap[OrderType.OPTION_EXPIRED] = OptionOrderSectionViewFactory()
        orderDetailViewMap[OrderType.OPTION_DO_NOT_EXERCISE] = OptionOrderSectionViewFactory()
        orderDetailViewMap[OrderType.ADVANCE_OPTION] = OptionOrderSectionViewFactory()
        orderDetailViewMap[OrderType.CUSTOM] = MultiLegsOrderSectionViewFactory()
        orderDetailViewMap[OrderType.SPREADS] = MultiLegsOrderSectionViewFactory()
        orderDetailViewMap[OrderType.BUY_WRITES] = MultiLegsOrderSectionViewFactory()
        orderDetailViewMap[OrderType.BUTTERFLY] = MultiLegsOrderSectionViewFactory()
        orderDetailViewMap[OrderType.IRON_BUTTERFLY] = MultiLegsOrderSectionViewFactory()
        orderDetailViewMap[OrderType.CONDOR] = MultiLegsOrderSectionViewFactory()
        orderDetailViewMap[OrderType.IRON_CONDOR] = MultiLegsOrderSectionViewFactory()
        orderDetailViewMap[OrderType.ADVANCE_EQUITY] = StockOrderSectionViewFactory()
        orderDetailViewMap[OrderType.COLLARS] = MultiLegsOrderSectionViewFactory()

        advancedOrderDetailMap[AdvancedOrderType.AO_CONTINGENT] = ContingentOrderSectionViewFactory()
    }

    fun map(orderDto: BaseOrderDetailsItem, isConditionalOrder: Boolean): List<TableSectionItemLayout> {
        return when (orderDto) {
            is BaseOrderDetailsItem.OrderDetailsItem -> {
                requireNotNull(orderDetailViewMap[orderDto.orderType]?.createSectionView(orderDto, resources, isConditionalOrder)) {
                    "Order type ${orderDto.orderType} contains no data"
                }
            }
            is BaseOrderDetailsItem.AdvancedOrderItem -> {
                requireNotNull(advancedOrderDetailMap[orderDto.orderType]?.createSectionView(orderDto, resources)) {
                    "Advanced Order type ${orderDto.orderType} contains no data"
                }
            }
            is BaseOrderDetailsItem.PreviewOrderDetailsItem -> throw UnsupportedOperationException()
            is BaseOrderDetailsItem.StockPlanOrderDetailsItem -> {
                stockPlanOrderDetailMap.createSectionView(orderDto, resources)
            }
        }
    }

    fun map(orderDto: BaseOrderDetailsItem.OrderDetailsItem, orderDetailDescription: OrderDetailDescription): List<TableSectionItemLayout> {
        return getPartialInfo(
            orderDto.execInstruction, orderDetailDescription, orderDto.orderType.isConditional(),
            resources, orderDto.advancedItem?.orderType, orderDto.term
        )
    }
}

data class OrderDetailLabelResources(
    val stock: OrderLabels,
    val defaultResources: OrderResources
)

data class OrderLabels(
    val allOrNone: String,
    val orderNumber: String,
    val orderPlaced: String,
    val orderStatus: String,
    val executed: String,
    val orderType: String,
    val quantity: String,
    val term: String,
    val priceType: String,
    val limitPrice: String,
    val stopPrice: String,
    val priceExecuted: String,
    val commissionFees: String,
    val cancelRequested: String,
    val expired: String,
    val systemRejected: String,
    val cancelledDate: String,
    val orderSavedDate: String,
    val account: String,
    val strategy: String,
    val estimatedCommission: String,
    val estimatedTotalCost: String,
    val estimatedTotalProceeds: String,
    val quantityExecutedEntered: String,
    val orderTermGTD: String,
    val orderTermWeb: String,
    val orderTermNight: String,
    val marketDestination: String,
    val orderExecuted: String,
    val qualifier: String,
    val condition: String,
    val advancedValue: String,
    val stopValDollar: String,
    val stopValPercent: String,
    val price: String,
    val limitOffsetValue: String,
    val estimatedCommissionFees: String,
    val description: String,
    val exerciseValue: String,
    val sharesExercised: String,
    val sharesSold: String,
    val sharesToSell: String,
    val estGrossProceeds: String,
    val totalPrice: String,
    val secFee: String,
    val brokerAssistFee: String,
    val disbursementFee: String,
    val estTaxesWithheld: String,
    val estSharesWithheldValue: String,
    val estNetProceeds: String,
    val sharesToExcercise: String,
    val sharesToIssue: String,
    val sharesWithheldValue: String
)

data class OrderResources(
    val currencySign: String,
    val emptyPriceDefault: String,
    val percentageSign: String,
    val marginCallTextColor: Int,
    val resource: Resources
)
