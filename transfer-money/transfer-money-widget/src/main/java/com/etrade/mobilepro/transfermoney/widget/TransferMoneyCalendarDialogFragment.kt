package com.etrade.mobilepro.transfermoney.widget

import android.app.Dialog
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import com.etrade.mobilepro.calendar.CalendarDialog
import com.etrade.mobilepro.calendar.MIDDLE_OF_CALENDAR_RANGE
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.transfermoney.widget.assisted.TransferMoneyCalendarViewModelFactoryFactory
import com.etrade.mobilepro.util.android.extension.instantiateFragment
import com.etrade.mobilepro.util.android.fragment.OrientationLock
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import org.threeten.bp.LocalDate
import javax.inject.Inject

private val CLASS_NAME = TransferMoneyCalendarDialogFragment::class.java.canonicalName

val CALENDAR_SELECTED_DATE = "$CLASS_NAME.selected_date"
val CALENDAR_CURRENT_PAGE = "$CLASS_NAME.current_page"
val CALENDAR_TAG = "$CLASS_NAME.tag"

const val CALENDAR_REQUEST_KEY = "CALENDAR_REQUEST_KEY"

@RequireLogin
class TransferMoneyCalendarDialogFragment @Inject constructor(
    private val viewModelFactory: TransferMoneyCalendarViewModelFactoryFactory,
    private val orientationLock: OrientationLock,
    snackbarUtilFactory: SnackbarUtilFactory
) : DialogFragment() {

    private var initialDate: LocalDate? = null
    private var initialPage: Int = MIDDLE_OF_CALENDAR_RANGE
    private var isOriginAccountExternal: Boolean? = null
    private var isDestinationAccountExternal: Boolean? = null
    private var leadDays: Int? = null

    private var calendarDialog: CalendarDialog? = null

    private val viewModel: TransferMoneyCalendarViewModel by viewModels {
        viewModelFactory.create(
            isOriginAccountExternal = requireNotNull(isOriginAccountExternal),
            isDestinationAccountExternal = requireNotNull(isDestinationAccountExternal),
            currentDate = LocalDate.now(),
            initialDate = initialDate,
            leadDays = leadDays
        )
    }

    private val snackBarUtil by lazy { snackbarUtilFactory.createSnackbarUtil({ this@TransferMoneyCalendarDialogFragment }, { calendarDialog }) }
    private var snackbar: Snackbar? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        orientationLock.init(this, ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT)
        setupViewModel()
        return setupCalendarDialog()
    }

    private fun setupViewModel() {
        viewModel.viewState.observe(this, ::render)
    }

    private fun setupCalendarDialog(): Dialog {
        return CalendarDialog(requireContext())
            .also { calendarDialog = it }
            .createDialog(
                onDateSelected = { date, page ->
                    setFragmentResult(
                        CALENDAR_REQUEST_KEY,
                        bundleOf(
                            CALENDAR_SELECTED_DATE to date,
                            CALENDAR_CURRENT_PAGE to page
                        )
                    )
                },
                onDateClick = { date, page ->
                    viewModel.setSelectedDate(date)
                    viewModel.setSelectedPage(page)
                },
                clickableDaysPredicate = viewModel::isDateValid,
                selectedDate = viewModel.viewState.value?.selectedDate ?: initialDate,
                currentCalendarPage = viewModel.viewState.value?.currentPage ?: initialPage,
                message = getLeadDays().let {
                    resources.getQuantityString(R.plurals.transfer_selected_date_disclaimer, it, it)
                }
            )
    }

    private fun getLeadDays(): Int {
        return viewModel.viewState.value?.leadDays ?: throw IllegalStateException("Could not provide lead days to transfer money calendar")
    }

    private fun render(state: ViewState) {
        (dialog as? AlertDialog)?.getButton(AlertDialog.BUTTON_POSITIVE)?.isEnabled = state.holidays != null && state.selectedDate != null
        calendarDialog?.isLoading = state.isLoading
        state.holidays?.let { calendarDialog?.refresh() }

        if (state.selectedDate != null) {
            calendarDialog?.selectedDate = state.selectedDate
        }

        if (state.error == null) {
            snackbar?.dismiss()
        } else {
            snackBarUtil.retrySnackbar { state.error.autoRetry?.invoke() }
        }
    }

    class Builder(
        private val isOriginAccountExternal: Boolean,
        private val isDestinationAccountExternal: Boolean,
        private var selectedDate: LocalDate? = null,
        private val selectedPage: Int? = null,
        private val leadDays: Int? = null,
        private val fragmentTag: String = CALENDAR_TAG
    ) {
        fun show(targetFragment: Fragment) {
            targetFragment.parentFragmentManager.fragmentFactory.instantiateFragment(TransferMoneyCalendarDialogFragment::class.java)
                .let { it as TransferMoneyCalendarDialogFragment }
                .apply { setup() }
                .show(targetFragment.parentFragmentManager, fragmentTag)
        }

        fun restore(targetFragment: Fragment) {
            targetFragment.parentFragmentManager.findFragmentByTag(fragmentTag)
                ?.let { it as TransferMoneyCalendarDialogFragment }
                ?.setup()
        }

        private fun TransferMoneyCalendarDialogFragment.setup() {
            isOriginAccountExternal = this@Builder.isOriginAccountExternal
            isDestinationAccountExternal = this@Builder.isDestinationAccountExternal
            initialDate = this@Builder.selectedDate
            leadDays = this@Builder.leadDays
            this@Builder.selectedPage?.let { initialPage = it }
        }
    }

    class Result(val selectedDate: LocalDate, val selectedPage: Int)
}

fun Bundle.extractCalendarDialogResult(): TransferMoneyCalendarDialogFragment.Result {
    check(containsKey(CALENDAR_SELECTED_DATE)) { "Key for selected date not available" }
    check(containsKey(CALENDAR_CURRENT_PAGE)) { "Key for current page not available" }
    val extractedDate: LocalDate = getSerializable(CALENDAR_SELECTED_DATE).apply {
        check(this is LocalDate) { "Value for selected date must be an instance of ${LocalDate::class.java.canonicalName}" }
    }.let { it as LocalDate }

    return TransferMoneyCalendarDialogFragment.Result(
        selectedDate = extractedDate,
        selectedPage = getInt(CALENDAR_CURRENT_PAGE, 0)
    )
}
