package com.etrade.mobilepro.order.details.router

enum class OrderConfirmationAction {
    VIEW_PORTFOLIO,
    CLOSE
}
