package com.etrade.mobilepro.markets.earnings

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactory
import com.etrade.mobilepro.dynamicui.DynamicUiViewFactoryException
import com.etrade.mobilepro.dynamicui.api.DynamicViewModel
import com.etrade.mobilepro.dynamicui.api.GenericLayout
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

class TodayEarningsViewFactory @Inject constructor(
    private val getTodayEarningsUseCase: GetTodayEarningsUseCase,
    private val tracker: Tracker
) : DynamicUiViewFactory {

    override fun createView(dto: Any, meta: List<Any>): GenericLayout {
        if (dto is TodayEarningsViewDto) {
            return object : GenericLayout {
                override fun refresh(viewModel: DynamicViewModel) {
                    if (viewModel is TodayEarningsViewModel) {
                        viewModel.loadTodayEarnings()
                    }
                }

                override val viewModelFactory: ViewModelProvider.Factory = viewModelFactory {
                    TodayEarningsViewModel(getTodayEarningsUseCase, tracker)
                }

                override val viewModelClass: Class<out DynamicViewModel> = TodayEarningsViewModel::class.java

                override val uniqueIdentifier: String = javaClass.name
            }
        } else {
            throw DynamicUiViewFactoryException("TodayEarningsViewFactory registered to unknown type")
        }
    }
}
