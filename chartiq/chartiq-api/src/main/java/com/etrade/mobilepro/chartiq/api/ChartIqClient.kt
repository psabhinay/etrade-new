package com.etrade.mobilepro.chartiq.api

import com.etrade.mobilepro.androidconfig.api.AndroidConfigInfo
import com.etrade.mobilepro.chartiq.api.model.Account
import com.etrade.mobilepro.chartiq.api.model.Environment
import com.etrade.mobilepro.chartiq.api.model.SymbolInfo
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.streaming.api.Level1Data

interface ChartIqClient {

    /**
     * This supposed to be the very first method called to setup communication with ChartIQ.
     *
     * @param akamaiConfiguration Akamai service configuration
     * @param environmentName name of the environment (e.g. SIT, UAT, PROD)
     */
    fun initialize(akamaiConfiguration: AndroidConfigInfo, environmentName: String)

    /**
     * Used to supply all necessary information to start plotting charts for a particular symbol.
     * This method is intended to be called once ChartIQ is loaded and {ChartIqJavascriptBridge#webviewReady} method is invoked.
     *
     * @param symbolInfo symbol related information
     * @param accounts list of brokerage accounts
     * @param defaultAccount an account to be used by default to poll the data
     * @param environment essential information about environment, like endpoint name and urls
     * @param syncServerData indicates whether chart needs to sync data with server (e.g. studies)
     * @param sessionCookie SMSESSION cookie
     * @param csrfToken Cross-Site Request Forgery token, only added if user is logged in
     * @param restoreState indicates whether to restore previously saved state of the chart (e.g. open/closed settings)
     * @param showDrawingNotImplementedAlert indicates whether to show alert that drawing is not implemented if user enables drawing via the chart settings
     */
    @Suppress("LongParameterList")
    fun updateChartConfiguration(
        symbolInfo: SymbolInfo,
        accounts: List<Account>,
        defaultAccount: Account?,
        environment: Environment,
        syncServerData: Boolean,
        sessionCookie: String = "",
        csrfToken: String? = null,
        updateSession: Boolean = false,
        restoreState: Boolean = false,
        showDrawingNotImplementedAlert: Boolean = false
    )

    /**
     * Used to add realtime streaming data to the ChartIQ for plotting
     *
     * @param data time series data to plot
     * @param ticker ticker that the data relates to. Could be null if streaming data corresponds to the symbol that
     * chart was originally configured with
     *
     * @see updateChartConfiguration
     */
    fun addStreamingData(data: Level1Data, ticker: String? = null)

    /**
     * Used to notify ChartIQ chart about extended hours state after chart configuration was already supplied via #updateConfiguration
     *
     * @see updateChartConfiguration
     */
    fun updateExtendedHoursState(isExtendedHours: Boolean)

    /**
     * Used to notify ChartIQ of button pressed on the alert populated via {ChartIqJavascriptBridge#showAlertDialog} method
     *
     * @param callBack name of the provided callback
     * @param alertId identifier for the alert dialog
     */
    fun forwardAlertCallback(alertId: String, callBack: String)

    /**
     * Used to close chart settings once they are open.
     * Note: invoking this method consequently will not open settings once they are closed.
     */
    fun closeChartSettings()

    /**
     * Adds a symbol to compare against the symbol provided during chart configuration.
     *
     * @see updateChartConfiguration
     */
    fun addSymbolToCompare(symbol: WithSymbolInfo)

    /**
     * Used to send data requested by ChartIQ back to ChartIQ
     *
     * @param requestId is id to identify data load request
     * @param requestType is requestType to identify a service that requested the data
     * @param result is loaded data that was requested by ChartIQ
     */
    fun sendLoadedData(requestId: String, requestType: String, result: String?)
}
