package com.etrade.mobilepro.trade.api.advanced

import com.etrade.mobilepro.trade.api.option.OptionOrder

interface AdvancedOptionOrder : AdvancedOrder, OptionOrder
