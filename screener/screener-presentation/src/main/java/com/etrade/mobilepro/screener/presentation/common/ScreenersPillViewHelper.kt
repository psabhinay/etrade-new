package com.etrade.mobilepro.screener.presentation.common

import android.content.res.Resources
import androidx.annotation.MainThread
import com.etrade.mobilepro.pillview.DefaultPillViewHelper
import com.etrade.mobilepro.pillview.PillViewHelper
import com.etrade.mobilepro.screener.presentation.R

interface ScreenersPillViewHelper : PillViewHelper {
    fun show(count: Int, hasLoadingBar: Boolean = false, autoHide: Boolean = true)
}

@MainThread
class DefaultScreenersPillViewHelper(
    private val resources: Resources
) : DefaultPillViewHelper(), ScreenersPillViewHelper {

    override fun show(count: Int, hasLoadingBar: Boolean, autoHide: Boolean) =
        show(formatCount(count), hasLoadingBar, autoHide)

    private fun formatCount(count: Int): String =
        String.format(resources.getQuantityText(R.plurals.results, count) as String, count)
}
