package com.etrade.mobilepro.markets.movers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnLayout
import androidx.core.widget.ContentLoadingProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.main.navigation.MainNavigation
import com.etrade.mobilepro.databinding.FragmentMarketsMoversBinding
import com.etrade.mobilepro.dropdown.BottomSheetSelector
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.markets.common.MarketsBaseFragment
import com.etrade.mobilepro.markets.common.MarketsBaseViewModel
import com.etrade.mobilepro.markets.common.MarketsTableViewListener
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.swiperefresh.MultiSwipeRefreshLayout
import com.etrade.mobilepro.tableviewutils.presentation.RestrictedTableView
import com.etrade.mobilepro.tableviewutils.presentation.SortStateInfo
import com.etrade.mobilepro.tableviewutils.presentation.getVisibleSymbols
import com.etrade.mobilepro.tableviewutils.presentation.updateSortViewState
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

private const val FILTER_EXCHANGE_TAG = "markets_mover_filter_exchange_tag"
private const val FILTER_ACTIVITY_TAG = "markets_mover_filter_activity_tag"

internal class MarketsMoversFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    private val mainNavigation: MainNavigation
) : MarketsBaseFragment<MarketsMoversViewModel>(viewModelFactory, snackbarUtilFactory, R.layout.fragment_markets_movers) {

    private val binding by viewBinding(FragmentMarketsMoversBinding::bind) { onDestroyBinding() }

    override val marketsRowHeaderWidth = R.dimen.markets_movers_row_header_width

    override val table: RestrictedTableView
        get() = binding.tableScroll.tableView
    override val loading: ContentLoadingProgressBar
        get() = binding.loading.loadingIndicator
    override val emptyQuotesText: View
        get() = binding.emptyQuotes.emptyQuotesText
    override val swipeRefresh: MultiSwipeRefreshLayout
        get() = binding.swipeRefresh

    override val viewModelClassType = MarketsMoversViewModel::class.java

    private val onTableViewScrollListener = object : RecyclerView.OnScrollListener() {

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            table.getVisibleSymbols().let { visibleItems ->
                viewModel.notifyVisibleItemsChanged(visibleItems)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initExchangeFilterDropDown()
        initActivityFilterDropDown()
        table.isSortable = true
    }

    override fun setUpViewModels() {
        super.setUpViewModels()
        viewModel.selectedExchange.observe(
            viewLifecycleOwner,
            Observer { exchangeType ->
                binding.filterExchange.text = exchangeType.name
            }
        )

        viewModel.selectedActivity.observe(
            viewLifecycleOwner,
            Observer { activityType ->
                binding.filterActivity.text = activityType.name
            }
        )

        viewModel.sortState.observe(
            viewLifecycleOwner,
            Observer { sortInfo ->
                table.updateSortViewState(sortInfo ?: SortStateInfo())
            }
        )
    }

    override fun onResume() {
        super.onResume()
        viewModel.resumeStreaming()
    }

    override fun onPause() {
        super.onPause()
        viewModel.pauseStreaming()
    }

    override fun handleError() {
        super.handleError()
        updateTopViewsVisibility(false)
    }

    override fun handleSuccess(success: MarketsBaseViewModel.ViewState.Success) {
        super.handleSuccess(success)
        updateTopViewsVisibility(true)
        with(table) {
            rowHeaderRecyclerView.removeOnScrollListener(onTableViewScrollListener)
            rowHeaderRecyclerView.addOnScrollListener(onTableViewScrollListener)
            tableViewListener = MarketsTableViewListener(
                sortable = viewModel,
                cellClickListener = { title -> navigateToQuoteDetails(title, InstrumentType.EQ) },
                rowHeaderClickListener = { title, type -> navigateToQuoteDetails(title, type) }
            )
            doOnLayout {
                viewModel.notifyVisibleItemsChanged(getVisibleSymbols())
            }
        }

        binding.tableScroll.footerTimeStamp.text = getTimeStampText(R.string.as_of_time, success.timeStamp)
    }

    private fun initExchangeFilterDropDown() {
        parentFragmentManager.let { fragmentManager ->
            val dropDownManager = BottomSheetSelector<MarketsMoversViewModel.ExchangeType>(fragmentManager, resources)

            dropDownManager.init(
                tag = FILTER_EXCHANGE_TAG,
                title = getString(R.string.exchange),
                items = viewModel.exchangeList,
                initialSelectedPosition = viewModel.selectedExchangeIndex
            )
            dropDownManager.setListener { _, selected ->
                viewModel.exchangeItemSelected(selected)
            }
            binding.filterExchange.setOnClickListener {
                dropDownManager.openDropDown()
            }
        }
    }

    private fun initActivityFilterDropDown() {
        parentFragmentManager.let { fragmentManager ->
            val dropDownManager = BottomSheetSelector<MarketsMoversViewModel.ActivityType>(fragmentManager, resources)
            dropDownManager.init(
                tag = FILTER_ACTIVITY_TAG,
                title = getString(R.string.activity),
                items = viewModel.activitiesList,
                initialSelectedPosition = viewModel.selectedActivityIndex
            )
            dropDownManager.setListener { _, selected ->
                viewModel.activityItemSelected(selected)
            }

            binding.filterActivity.setOnClickListener {
                dropDownManager.openDropDown()
            }
        }
    }

    private fun updateTopViewsVisibility(show: Boolean) {
        if (show) {
            binding.filterContainer.visibility = View.VISIBLE
            binding.divider1.visibility = View.VISIBLE
            binding.divider2.visibility = View.VISIBLE
        } else {
            binding.filterContainer.visibility = View.GONE
            binding.divider1.visibility = View.GONE
            binding.divider2.visibility = View.GONE
        }
    }

    private fun navigateToQuoteDetails(title: String, type: InstrumentType) {
        mainNavigation.navigateToQuoteDetails(activity, SearchResultItem.Symbol(title, type).toBundle())
    }
}
