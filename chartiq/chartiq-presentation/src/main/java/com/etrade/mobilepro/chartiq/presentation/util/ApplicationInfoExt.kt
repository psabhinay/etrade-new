package com.etrade.mobilepro.chartiq.presentation.util

import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo

private const val HTTP_AGENT = "http.agent"

internal val ApplicationInfo.userAgentDeviceType
    get() = "${System.getProperty(HTTP_AGENT)}$deviceType"
