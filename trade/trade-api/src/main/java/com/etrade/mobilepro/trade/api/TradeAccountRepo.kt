package com.etrade.mobilepro.trade.api

import com.etrade.mobilepro.common.result.ETResult

interface TradeAccountRepo {
    suspend fun getTradeAccounts(): ETResult<List<TradeAccount>>
}
