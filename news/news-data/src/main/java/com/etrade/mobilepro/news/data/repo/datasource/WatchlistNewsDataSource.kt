package com.etrade.mobilepro.news.data.repo.datasource

import com.etrade.mobilepro.news.api.model.NewsRequest
import com.etrade.mobilepro.news.data.dto.NewsDto
import com.etrade.mobilepro.news.data.rest.NewsApiClient
import com.etrade.mobilepro.watchlist.repo.CombinedWatchlistRepo
import com.etrade.mobilepro.watchlist.repo.DeviceWatchlistRepo
import com.etrade.mobilepro.watchlistapi.Watchlist
import io.reactivex.Single

internal class WatchlistNewsDataSource(
    private val newsApiClient: NewsApiClient,
    private val watchlistRepo: DeviceWatchlistRepo,
    private val combinedWatchlistRepo: CombinedWatchlistRepo,
    requestParams: NewsRequest?
) : NewsPagingSource(requestParams) {

    override fun getNewsRange(startIndex: Int, size: Int, requestParams: NewsRequest?): Single<NewsDto> {
        return when (requestParams) {
            is NewsRequest.Watchlist -> getNewsRange(requestParams.watchlistId, startIndex, size)
            else -> throw UnsupportedOperationException()
        }
    }

    private fun getNewsRange(watchlistId: String, startIndex: Int, size: Int): Single<NewsDto> {
        return when {
            Watchlist.isDeviceWatchlist(watchlistId) ->
                watchlistRepo
                    .getWatchlistSymbols(watchlistId)
                    .flatMap { symbols ->
                        newsApiClient.getSymbolsNews(symbols.joinToString(","), startIndex, size)
                    }
            Watchlist.isAllWatchlistsItemId(watchlistId) ->
                combinedWatchlistRepo
                    .getAllWatchlistsEntries()
                    .flatMap { entries ->
                        newsApiClient.getSymbolsNews(entries.map { it.ticker }.toSet().joinToString(","), startIndex, size)
                    }
            else -> newsApiClient.getWatchlistNews(watchlistId, startIndex, size)
        }
    }
}
