package com.etrade.mobilepro.valuepicker

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.annotation.StringRes
import androidx.core.content.withStyledAttributes
import com.etrade.mobilepro.util.android.accessibility.bindTextInputButtonAccessibility
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.view.toggleStyle
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

open class ValuePickerWidget : FrameLayout {

    constructor(ctx: Context) : super(ctx) {
        initAttributes(null)
    }

    constructor(ctx: Context, attributeSet: AttributeSet?) : super(ctx, attributeSet) {
        initAttributes(attributeSet)
    }

    constructor(ctx: Context, attributeSet: AttributeSet?, defStyle: Int) : super(ctx, attributeSet, defStyle) {
        initAttributes(attributeSet)
    }

    var hint: CharSequence?
        get() = inputValueView.hint
        set(value) {
            inputValueView.hint = value
        }

    var value: CharSequence?
        get() = textInputView.text
        set(value) = textInputView.setText(value)

    protected val inputValueView: TextInputLayout

    private val textInputView: TextInputEditText

    protected lateinit var clearInputBtn: ImageView

    private val textChangeListener = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) = Unit

        override fun afterTextChanged(text: Editable?) {
            if (!text.isNullOrBlank()) {
                clearInputBtn.visibility = View.VISIBLE
            } else {
                clearInputBtn.visibility = View.GONE
            }
        }
    }

    init {
        inflate(R.layout.value_picker_widget, true)
        inputValueView = getChildAt(0) as TextInputLayout
        textInputView = inputValueView.editText as TextInputEditText
        clearInputBtn = getChildAt(1) as ImageView
    }

    protected fun enableClearAction(action: ((View) -> Unit)) {
        clearInputBtn.visibility = View.VISIBLE
        clearInputBtn.setOnClickListener(action)
        inputValueView.editText?.addTextChangedListener(textChangeListener)
    }

    protected fun disableClearAction() {
        clearInputBtn.visibility = View.GONE
        clearInputBtn.setOnClickListener(null)
        inputValueView.editText?.removeTextChangedListener(textChangeListener)
    }

    private fun initAttributes(attrs: AttributeSet?) {
        context.withStyledAttributes(attrs, R.styleable.ValuePickerWidget) {
            hint = getString(R.styleable.ValuePickerWidget_android_hint)
            value = getString(R.styleable.ValuePickerWidget_android_value)
        }
    }

    final override fun getChildAt(index: Int): View? = super.getChildAt(index)

    override fun setContentDescription(contentDescription: CharSequence?) {
        super.setContentDescription(contentDescription)
        hint?.let { inputValueView.bindTextInputButtonAccessibility(it.toString()) }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        inputValueView.toggleStyle(enabled)
    }

    final override fun setOnClickListener(l: OnClickListener?) {
        super.setOnClickListener(l)
        textInputView.setOnClickListener(l)
    }

    fun setHint(@StringRes textResId: Int) {
        hint = context.getString(textResId)
    }
}
