package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.delegate

import android.view.ViewGroup
import com.etrade.mobilepro.messagecenter.presentation.R
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.Separator
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.SupportMessageItem
import com.etrade.mobilepro.messagecenter.presentation.details.support.adapter.viewholder.SupportMessageSeparatorViewHolder
import com.etrade.mobilepro.util.android.extension.inflate
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

internal class SupportMessageSeparatorDelegate : AbsListItemAdapterDelegate<Separator, SupportMessageItem, SupportMessageSeparatorViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup): SupportMessageSeparatorViewHolder =
        SupportMessageSeparatorViewHolder(parent.inflate(R.layout.message_center_item_support_message_separator))

    override fun isForViewType(item: SupportMessageItem, items: MutableList<SupportMessageItem>, position: Int): Boolean =
        item is Separator

    override fun onBindViewHolder(item: Separator, holder: SupportMessageSeparatorViewHolder, payloads: MutableList<Any>) {
        // nop
    }
}
