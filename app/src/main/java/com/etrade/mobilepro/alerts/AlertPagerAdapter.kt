package com.etrade.mobilepro.alerts

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.etrade.mobilepro.alerts.presentation.base.AlertTab
import com.etrade.mobilepro.common.readstatus.ReadStatusRepo
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory

class AlertPagerAdapter(
    private val fragment: Fragment,
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val readStatusRepo: ReadStatusRepo
) : FragmentStateAdapter(fragment) {
    fun getPageTitle(position: Int) = fragment.getString(AlertTab.values()[position].titleId)

    override fun getItemCount() = AlertTab.values().size

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> MarketAlertsFragment(viewModelFactory, snackbarUtilFactory, readStatusRepo)
            1 -> AccountAlertsFragment(viewModelFactory, snackbarUtilFactory, readStatusRepo)
            else -> throw IllegalArgumentException("No alerts fragment for position $position")
        }
    }
}
