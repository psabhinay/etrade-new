package com.etrade.mobilepro.chartiq.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class AlertDialogContentDto(

    @Json(name = "alert")
    val alert: AlertDto,

    @Json(name = "actions")
    val actions: List<AlertActionDto>
)

@JsonClass(generateAdapter = true)
internal data class AlertDto(
    @Json(name = "id")
    val id: String,

    @Json(name = "title")
    val title: String?,

    @Json(name = "message")
    val message: String
)

@JsonClass(generateAdapter = true)
internal data class AlertActionDto(

    @Json(name = "title")
    val title: String,

    @Json(name = "callback")
    val callback: String
)
