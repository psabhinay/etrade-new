package com.etrade.mobilepro.watchlist.repo

import com.etrade.mobilepro.util.UseCase
import com.etrade.mobilepro.watchlistapi.Watchlist
import com.etrade.mobilepro.watchlistapi.Watchlist.Companion.isDeviceWatchlist
import com.etrade.mobilepro.watchlistapi.WatchlistEntry
import com.etrade.mobilepro.watchlistapi.repo.WatchlistRepo
import io.reactivex.Single
import kotlinx.coroutines.runBlocking

interface SetHasAddedToWatchlistUseCase : UseCase<Boolean, Unit>

/**
 * This watchlist repository that combines two implementation of [WatchlistRepo].
 *
 * @param deviceWatchlistRepo repository for watchlist stored locally on a user's device
 * @param defaultWatchlistRepo default implementation of watchlist repository for all user's watchlists
 * @param isAuthenticated checks if a user is authenticated (`true`) or not (`false`)
 */
class CombinedWatchlistRepo(
    private val deviceWatchlistRepo: WatchlistRepo,
    private val defaultWatchlistRepo: WatchlistRepo,
    private val addedToWatchlistUseCase: SetHasAddedToWatchlistUseCase,
    private val isAuthenticated: () -> Boolean
) : WatchlistRepo {

    override fun getUserWatchlists(): Single<List<Watchlist>> {
        val deviceWatchlists = deviceWatchlistRepo.getUserWatchlists()
        return if (isAuthenticated()) {
            val userWatchlists = defaultWatchlistRepo
                .getUserWatchlists()
                .onErrorResumeNext {
                    Single.just(emptyList())
                }

            deviceWatchlists.zipWith(userWatchlists) { l1, l2 -> l1 + l2 }
        } else {
            deviceWatchlists
        }
    }

    override fun getWatchlistEntries(watchlistId: String, isExtendedHours: Boolean): Single<List<WatchlistEntry>> {
        return if (isDeviceWatchlist(watchlistId)) {
            deviceWatchlistRepo.getWatchlistEntries(watchlistId, isExtendedHours)
        } else {
            defaultWatchlistRepo.getWatchlistEntries(watchlistId, isExtendedHours)
        }
    }

    override fun getWatchlistSymbols(watchlistId: String): Single<List<String>> {
        return if (isDeviceWatchlist(watchlistId)) { deviceWatchlistRepo } else { defaultWatchlistRepo }.getWatchlistSymbols(watchlistId)
    }

    override fun addToWatchList(watchlistId: String, ticker: String): Single<Boolean> {
        return if (isDeviceWatchlist(watchlistId)) {
            deviceWatchlistRepo.addToWatchList(watchlistId, ticker)
        } else {
            defaultWatchlistRepo.addToWatchList(watchlistId, ticker)
        }
            .doAfterSuccess { success ->
                if (success) runBlocking { addedToWatchlistUseCase.execute(true) }
            }
    }

    override fun saveWatchList(watchlistId: String, entries: List<WatchlistEntry>, removedEntries: List<WatchlistEntry>): Single<Boolean> {
        val repo = if (isDeviceWatchlist(watchlistId)) deviceWatchlistRepo else defaultWatchlistRepo
        return repo.saveWatchList(watchlistId, entries, removedEntries)
    }

    override fun createWatchlist(watchlistName: String): Single<Watchlist> = defaultWatchlistRepo.createWatchlist(watchlistName)

    override fun deleteWatchList(watchlistId: String): Single<Boolean> = defaultWatchlistRepo.deleteWatchList(watchlistId)

    override fun getAllWatchlistsEntries(isExtendedHours: Boolean): Single<List<WatchlistEntry>> = if (isAuthenticated()) {
        defaultWatchlistRepo.getAllWatchlistsEntries(isExtendedHours)
            .zipWith(deviceWatchlistRepo.getAllWatchlistsEntries(isExtendedHours)) { l1, l2 -> l1 + l2 }
    } else {
        deviceWatchlistRepo.getAllWatchlistsEntries(isExtendedHours)
    }
}
