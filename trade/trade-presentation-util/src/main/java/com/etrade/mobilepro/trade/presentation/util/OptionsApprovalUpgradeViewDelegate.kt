package com.etrade.mobilepro.trade.presentation.util

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.etrade.mobilepro.formsandapplications.presentation.AccountServicesWebViewActivity
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.viewdelegate.ViewDelegate

class OptionsApprovalUpgradeViewDelegate(
    private val context: Context,
    private val optionsApprovalUpgradeUrl: LiveData<ConsumableLiveEvent<String>>
) : ViewDelegate {

    override fun observe(owner: LifecycleOwner) {
        optionsApprovalUpgradeUrl.observe(
            owner,
            Observer { event ->
                event.consume { url ->
                    context.startActivity(
                        AccountServicesWebViewActivity.intent(
                            context,
                            url,
                            R.string.trade_label_upgrade_account
                        )
                    )
                }
            }
        )
    }
}
