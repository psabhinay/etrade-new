package com.etrade.mobilepro.barchartview

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.DataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.listener.ChartTouchListener
import com.github.mikephil.charting.listener.OnChartGestureListener
import com.github.mikephil.charting.utils.Utils
import java.util.ArrayList

private const val ANIMATION_DURATION_MS = 1000
private const val DEFAULT_LABEL_COUNT = 6
private const val DEFAULT_BAR_WIDTH = 0.8f
private const val DEFAULT_VISIBLE_X_RANGE_MAXIMUM = 6f

/**
 * Custom Bar Chart that makes use of the "Zero Bar" feature by default. It can also notify client code when
 * one of the X axis boundaries have been reached (for example, to load more data).
 */
class BarChartView : BarChart {

    var xAxisValueFormatter: ((Float) -> String)? = null
        set(value) {
            field = value
            xAxis.valueFormatter = ValueFormatterWrapper(value)
        }
    var yAxisValueFormatter: ((Float) -> String)? = null
        set(value) {
            field = value
            axisRight.valueFormatter = ValueFormatterWrapper(value)
        }
    var barValueFormatter: ((Float) -> String)? = null
    var barValueDescriptionFormatter: ((Int, Float) -> String?)? = null
    var onLeftBoundaryReached: (() -> Unit)? = null
    var onRightBoundaryReached: (() -> Unit)? = null

    var highLightAlpha: Int? = null
    var highLightColor: Int? = null

    private var barColorList = mutableListOf<Int>()
    private var valueColorList = mutableListOf<Int>()
    private var valueDescriptionColorList = mutableListOf<Int?>()

    var defaultVisibleXRangeMaximum: Float = DEFAULT_VISIBLE_X_RANGE_MAXIMUM

    /**
     * Use a backing property to because the lib itself converts dp to px as soon as you call
     * [BarDataSet.setValueTextSize]
     */
    private var _valueTextSizeDp: Float = -1f

    var valueTextSizeDp: Float
        set(valueDp) {
            _valueTextSizeDp = valueDp // Backing property
            barData?.let { it.setValueTextSize(valueDp) } // [BarData]'s property
            this.valueTextSize = Utils.convertDpToPixel(valueDp) // [BarLineChartBase]'s property
        }
        get() { return _valueTextSizeDp }

    constructor(context: Context) : super(context) {
        initView()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initView()
    }

    override fun clear() {
        super.clear()

        barColorList.clear()
        valueColorList.clear()
        valueDescriptionColorList.clear()
    }

    private fun initView() {
        setDrawValueAboveBar(true)
        setDrawBarShadow(false)
        setScaleEnabled(false)
        setDrawGridBackground(false)
        isHighlightPerDragEnabled = false
        isHighlightPerTapEnabled = false
        isHighlightFullBarEnabled = false
        description.isEnabled = false
        legend.isEnabled = false
        onChartGestureListener = CustomGestureListener()
        setupXAxis()
        setupYAxis()

        zeroBarHeight = context.resources.getDimensionPixelOffset(R.dimen.zeroBarHeight).toFloat()
        valueDescriptionMargin = context.resources.getDimensionPixelOffset(R.dimen.valueDescriptionMargin).toFloat()

        setNoDataTextColor(ContextCompat.getColor(context, R.color.textColorPrimary))
    }

    private fun setupXAxis() {
        xAxis.apply {
            textColor = ContextCompat.getColor(context, R.color.black)
            valueFormatter = ValueFormatterWrapper(xAxisValueFormatter)
            setDrawGridLines(false)
            granularity = 1f
            labelCount = DEFAULT_LABEL_COUNT
            isZeroBarEnabled = true
        }
    }

    private fun setupYAxis() {
        axisRight.apply {
            textColor = ContextCompat.getColor(context, R.color.black)
            valueFormatter = ValueFormatterWrapper(yAxisValueFormatter)
            setDrawAxisLine(false)
            setDrawGridLines(false)
            setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
        }
        axisLeft.apply {
            textColor = ContextCompat.getColor(context, R.color.black)
            isEnabled = false
        }
    }

    /**
     * Adds entries to this bar chart whether it already has data or not.
     *
     * Note: The [entries] collection must be in a descending order.
     *
     * @param entries data to be added.
     * @param animate true if changes should be animated.
     */
    fun add(entries: List<DataEntry>, animate: Boolean = false) {
        if (entries.isNotEmpty()) {

            if (barData == null) {
                setEntries(entries, animate)
            } else {
                addEntries(entries, animate)
            }
        }
    }

    private fun setEntries(entries: List<DataEntry>, animate: Boolean = false) {
        val values = ArrayList<BarEntry>()

        entries.map {
            values.add(BarEntry(it.x, it.y))
            barColorList.add(ContextCompat.getColor(context, it.barColor))
            valueColorList.add(ContextCompat.getColor(context, it.valueColor))
            valueDescriptionColorList.add(it.valueDescriptionColor?.let { color -> ContextCompat.getColor(context, color) })
        }

        feedInitialData(values)

        refresh()

        if (animate) {
            animateXY(ANIMATION_DURATION_MS, ANIMATION_DURATION_MS)
        }
    }

    private fun addEntries(entries: List<DataEntry>, animate: Boolean = false) {
        val dataSet = barData?.getDataSetByIndex(0) as? BarDataSet

        if (dataSet != null && dataSet.entryCount > 0) {
            val min = dataSet.getEntryForIndex(0).x
            val max = dataSet.getEntryForIndex(dataSet.entryCount - 1).x

            if (entries.first().x > max) {
                entries.forEach {
                    dataSet.addEntry(BarEntry(it.x, it.y))
                    barColorList.add(resolveColor(it.barColor))
                    valueColorList.add(resolveColor(it.valueColor))
                    valueDescriptionColorList.add(it.valueDescriptionColor?.let(::resolveColor))
                }
            } else if (entries.last().x < min) {
                entries.reversed().forEach {
                    dataSet.addEntryOrdered(BarEntry(it.x, it.y))
                    barColorList.add(0, resolveColor(it.barColor))
                    valueColorList.add(0, resolveColor(it.valueColor))
                    valueDescriptionColorList.add(0, it.valueDescriptionColor?.let(::resolveColor))
                }
            }

            dataSet.notifyDataSetChanged()
            refresh()

            if (animate) {
                animateXY(ANIMATION_DURATION_MS, ANIMATION_DURATION_MS)
            }
        }
    }

    @ColorInt
    private fun resolveColor(@ColorRes color: Int) = ContextCompat.getColor(context, color)

    private fun feedInitialData(entries: List<BarEntry>) {
        val barDataSet = BarDataSet(entries, "data")

        highLightAlpha?.also { barDataSet.highLightAlpha = it }
        highLightColor?.also { barDataSet.highLightColor = it }

        barDataSet.setDrawIcons(false)
        barDataSet.colors = barColorList
        barDataSet.setValueTextColors(valueColorList)
        barDataSet.setValueDescriptionTextColors(valueDescriptionColorList)

        BarData(barDataSet).apply {
            barWidth = DEFAULT_BAR_WIDTH
            setValueFormatter(ValueFormatterWrapper(barValueFormatter))
            setValueDescriptionFormatter(object : ValueFormatter() {
                override fun getBarLabel(barEntry: BarEntry?): String {
                    return barEntry?.let {
                        barValueDescriptionFormatter?.invoke(it.x.toInt(), it.y)
                    } ?: ""
                }
            })
            this@BarChartView.data = this
        }

        if (valueTextSizeDp > 0) {
            barDataSet.valueTextSize = valueTextSizeDp
            this.valueTextSize = Utils.convertDpToPixel(valueTextSizeDp)
        } else {
            /**
             * [BarDataSet.setValueTextSize] is a method defined in the mpandroidchart library and is widely used.
             * This setter requires a value in DP, that's why we first need to read the dimen and then convert it to PX.
             */
            barDataSet.valueTextSize = Utils.convertPixelsToDp(context.resources.getDimension(R.dimen.barValueTextSize))
            this.valueTextSize = context.resources.getDimensionPixelOffset(R.dimen.barValueTextSize).toFloat()
        }
    }

    private fun refresh() {
        (barData?.getDataSetByIndex(0) as? BarDataSet)?.let { dataSet ->
            if (dataSet.entryCount > 0) {
                setupCollectionBySign(dataSet)
                barData.notifyDataChanged()
                notifyDataSetChanged()
                setVisibleXRangeMaximum(defaultVisibleXRangeMaximum)
                invalidate()
            }
        }
    }

    private fun setupCollectionBySign(dataSet: BarDataSet) {
        var positiveCount = 0
        var negativeCount = 0
        var zeroCount = 0

        dataSet.values.forEach { entry ->
            when {
                entry.y == 0f -> zeroCount++
                entry.y > 0f -> positiveCount++
                else -> negativeCount++
            }
        }

        // There is a specific setup depending on the collection's number signs.
        when {
            positiveCount + zeroCount == dataSet.entryCount -> setupForPositiveValues()
            negativeCount + zeroCount == dataSet.entryCount -> setupForNegativeValues()
            else -> setupForMixedValues()
        }
    }

    private fun setupForPositiveValues() {
        this.isZeroBarEnabled = false
        xAxis.apply {
            setDrawAxisLine(true)
            position = XAxis.XAxisPosition.BOTTOM
        }
        axisLeft.axisMinimum = 0.0f // max is 0
        axisRight.apply {
            isZeroBarEnabled = true
            setDrawZeroLine(false)
            axisMinimum = 0.0f // max is 0
        }
    }

    private fun setupForNegativeValues() {
        this.isZeroBarEnabled = false
        xAxis.apply {
            setDrawAxisLine(true)
            position = XAxis.XAxisPosition.TOP
        }
        axisLeft.axisMaximum = 0.0f // min is 0
        axisRight.apply {
            isZeroBarEnabled = true
            setDrawZeroLine(false)
            axisMaximum = 0.0f // min is 0
        }
    }

    private fun setupForMixedValues() {
        this.isZeroBarEnabled = true
        xAxis.apply {
            setDrawAxisLine(false)
            position = XAxis.XAxisPosition.CENTER
        }
        axisLeft.apply {
            resetAxisMaximum()
            resetAxisMinimum()
        }
        axisRight.apply {
            isZeroBarEnabled = true
            setDrawZeroLine(true)
            resetAxisMaximum()
            resetAxisMinimum()
        }
    }

    inner class CustomGestureListener : OnChartGestureListener {

        private var lastLowestVisibleXIndex: Int = -1
        private var lastHighestVisibleXIndex: Int = -1

        override fun onChartGestureEnd(me: MotionEvent?, lastPerformedGesture: ChartTouchListener.ChartGesture?) {
            if (lastPerformedGesture == ChartTouchListener.ChartGesture.DRAG) {
                if (lastLowestVisibleXIndex == 0) {
                    onLeftBoundaryReached?.invoke()
                } else if (lastHighestVisibleXIndex == barData.dataSets[0].entryCount - 1) {
                    onRightBoundaryReached?.invoke()
                }
            }

            lastLowestVisibleXIndex = -1
            lastHighestVisibleXIndex = -1
        }

        override fun onChartFling(me1: MotionEvent?, me2: MotionEvent?, velocityX: Float, velocityY: Float) { /*Not implemented*/
        }

        override fun onChartSingleTapped(me: MotionEvent?) { /*Not implemented*/
        }

        override fun onChartGestureStart(me: MotionEvent?, lastPerformedGesture: ChartTouchListener.ChartGesture?) { /*Not implemented*/
        }

        override fun onChartScale(me: MotionEvent?, scaleX: Float, scaleY: Float) { /*Not implemented*/
        }

        override fun onChartLongPressed(me: MotionEvent?) { /*Not implemented*/
        }

        override fun onChartDoubleTapped(me: MotionEvent?) { /*Not implemented*/
        }

        override fun onChartTranslate(me: MotionEvent?, dX: Float, dY: Float) {
            barData.getDataSetByIndex(0)?.let { dataSet ->
                dataSet.getEntryForXValue(lowestVisibleX, 0f, DataSet.Rounding.CLOSEST)?.let { lowestVisibleEntry ->
                    lastLowestVisibleXIndex = dataSet.getEntryIndex(lowestVisibleEntry)
                }

                dataSet.getEntryForXValue(highestVisibleX, 0f, DataSet.Rounding.CLOSEST)?.let { highestVisibleEntry ->
                    lastHighestVisibleXIndex = dataSet.getEntryIndex(highestVisibleEntry)
                }
            }
        }
    }
}
