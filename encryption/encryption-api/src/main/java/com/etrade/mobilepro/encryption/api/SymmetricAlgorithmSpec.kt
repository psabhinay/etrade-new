package com.etrade.mobilepro.encryption.api

/**
 * Specify an algorithm to use for symmetric encryption / decryption.
 */
interface SymmetricAlgorithmSpec {

    /**
     * Algorithm's name.
     */
    val algorithm: String

    /**
     * Block mode of the algorithm.
     */
    val blockMode: String

    /**
     * Encryption padding.
     */
    val encryptionPadding: String
}

/**
 * This extension should be used to get `Cipher` object that implements the transformation specified by [SymmetricAlgorithmSpec].
 *
 * A transformation is a string that describes the operation (or set of operations) to be performed on the given input, to produce some output. A transformation
 * always includes the name of a cryptographic algorithm (e.g., AES), and may be followed by a feedback mode and padding scheme.
 *
 * A transformation is of the form:
 *
 * * "algorithm/mode/padding" or
 * * "algorithm".
 */
val SymmetricAlgorithmSpec.transformation: String
    get() = "$algorithm/$blockMode/$encryptionPadding"
