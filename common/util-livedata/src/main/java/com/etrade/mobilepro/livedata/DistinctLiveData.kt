package com.etrade.mobilepro.livedata

import androidx.lifecycle.MutableLiveData
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class DistinctLiveData<T>(initialValue: T? = null) : MutableLiveData<T>(initialValue) {

    private val logger: Logger = LoggerFactory.getLogger(DistinctLiveData::class.java)

    override fun setValue(value: T) {
        if (this.value != value) {
            super.setValue(value)
            log("accepted", "setValue")
        } else {
            log("rejected", "setValue")
        }
    }

    override fun postValue(value: T) {
        if (this.value != value) {
            super.postValue(value)
            log("accepted", "postValue")
        } else {
            log("rejected", "postValue")
        }
    }

    private fun log(action: String, method: String) {
        logger.debug("Value $action in $method.")
    }
}
