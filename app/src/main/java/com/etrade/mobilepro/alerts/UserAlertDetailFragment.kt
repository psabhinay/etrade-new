package com.etrade.mobilepro.alerts

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.R
import com.etrade.mobilepro.alerts.api.model.AlertResponse
import com.etrade.mobilepro.alerts.api.model.AlertValueType
import com.etrade.mobilepro.alerts.api.model.NewsAlertValueType
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.alerts.presentation.AlertsSharedViewModel
import com.etrade.mobilepro.alerts.presentation.detail.UserAlertDetailViewModel
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.databinding.FragmentUserAlertDetailBinding
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.quote.presentation.QuotesWidgetViewModel
import com.etrade.mobilepro.styledalertdialog.StyledAlertDialog
import com.etrade.mobilepro.util.android.binding.dataBinding
import com.etrade.mobilepro.util.android.extension.updateValue
import com.etrade.mobilepro.util.android.goneUnless
import com.etrade.mobilepro.util.android.hideSoftKeyboard
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

@RequireLogin
class UserAlertDetailFragment @Inject constructor(
    snackbarUtilFactory: SnackbarUtilFactory,
    val viewModelFactory: ViewModelProvider.Factory
) : Fragment(R.layout.fragment_user_alert_detail) {

    private val binding by dataBinding(FragmentUserAlertDetailBinding::bind)

    private var snackBar: Snackbar? = null
    private val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private val viewModel by viewModels<UserAlertDetailViewModel> { viewModelFactory }
    private val quoteWidgetViewModel: QuotesWidgetViewModel by viewModels { viewModelFactory }
    private val sharedViewModel by activityViewModels<AlertsSharedViewModel> { viewModelFactory }
    private val switchesAndDividers: List<View> by lazy {
        with(binding.switches) {
            listOf(
                upDowngrade,
                inPlay,
                earnings,
                splitNotice,
                upDowngradeDivider,
                inPlayDivider,
                earningsDivider,
                splitNoticeDivider
            )
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        setupFragment()
        bindViewModel()
        if (savedInstanceState == null) {
            sharedViewModel.selectedUserAlert?.let {
                viewModel.loadAlert(it)
            }
        }
    }

    override fun onDestroy() {
        activity?.hideSoftKeyboard()
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.options_menu_alerts, menu)
        val deleteItem = menu.findItem(R.id.menu_alert_delete)?.apply { isVisible = true }
        deleteItem?.actionView?.setOnClickListener {
            context?.let {
                StyledAlertDialog(it).showAlert(
                    title = getString(R.string.alert_delete_dialog_title),
                    message = getString(R.string.do_you_want_to_delete_alert),
                    positiveAnswerAction = {
                        viewModel.deleteAlert()
                    },
                    negativeAnswerAction = {}
                )
            }
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    @Suppress("LongMethod")
    private fun setupFragment() {
        with(binding.inputs) {
            tHighTargetPriceLayout.setupView(
                dependencies = viewModel.inputValueDepsProvider.getDependencies(AlertValueType.HIGH_TARGET_PRICE),
                rawContentData = viewModel.inputValueHolder(AlertValueType.HIGH_TARGET_PRICE)
            )
            tLowTargetPriceLayout.setupView(
                dependencies = viewModel.inputValueDepsProvider.getDependencies(AlertValueType.LOW_TARGET_PRICE),
                rawContentData = viewModel.inputValueHolder(AlertValueType.LOW_TARGET_PRICE)
            )
            tHighDailyPercentLayout.setupView(
                dependencies = viewModel.inputValueDepsProvider.getDependencies(AlertValueType.HIGH_DAILY_PERCENT),
                rawContentData = viewModel.inputValueHolder(AlertValueType.HIGH_DAILY_PERCENT)
            )
            tLowDailyPercentLayout.setupView(
                dependencies = viewModel.inputValueDepsProvider.getDependencies(AlertValueType.LOW_DAILY_PERCENT),
                rawContentData = viewModel.inputValueHolder(AlertValueType.LOW_DAILY_PERCENT)
            )
            tHighPeTargetLayout.setupView(
                dependencies = viewModel.inputValueDepsProvider.getDependencies(AlertValueType.HIGH_PE_TARGET),
                rawContentData = viewModel.inputValueHolder(AlertValueType.HIGH_PE_TARGET)
            )
            tLowPeTargetLayout.setupView(
                dependencies = viewModel.inputValueDepsProvider.getDependencies(AlertValueType.LOW_PE_TARGET),
                rawContentData = viewModel.inputValueHolder(AlertValueType.LOW_PE_TARGET)
            )
            tDailyVolumeLayout.setupView(
                dependencies = viewModel.inputValueDepsProvider.getDependencies(AlertValueType.DAILY_VOLUME),
                rawContentData = viewModel.inputValueHolder(AlertValueType.DAILY_VOLUME)
            )

            tHighPeTargetLayout.visibility = View.GONE
            tLowPeTargetLayout.visibility = View.GONE
            tDailyVolumeLayout.visibility = View.GONE
        }
        with(binding.switches) {
            upDowngrade.setOnCheckedChangeListener { _, isChecked ->
                viewModel.switchValueHolder(NewsAlertValueType.UP_DOWN).value = isChecked
            }
            inPlay.setOnCheckedChangeListener { _, isChecked ->
                viewModel.switchValueHolder(NewsAlertValueType.IN_PLAY).value = isChecked
            }
            earnings.setOnCheckedChangeListener { _, isChecked ->
                viewModel.switchValueHolder(NewsAlertValueType.EARNINGS).value = isChecked
            }
            splitNotice.setOnCheckedChangeListener { _, isChecked ->
                viewModel.switchValueHolder(NewsAlertValueType.SPLIT_NOTICE).value = isChecked
            }
        }
        binding.updateAlertPb.hide()
        binding.updateAlertPb.bringToFront()
        binding.updateAlertBtn.setOnClickListener {
            viewModel.updateAlert()
        }
    }

    private fun adjustView(symbolType: InstrumentType) {
        with(binding.inputs) {
            tHighPeTargetLayout.goneUnless(symbolType.isEquity)
            tLowPeTargetLayout.goneUnless(symbolType.isEquity)
            tDailyVolumeLayout.goneUnless(symbolType.isEquity)
        }
    }

    @Suppress("LongMethod", "ComplexMethod")
    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is UserAlertDetailViewModel.ViewState.Loading.QuoteWidget -> {
                        view?.findViewById<TextView>(R.id.quote_widget_title)?.text =
                            it.alert.symbol
                        binding.updateAlertPb.show()
                        toggleUi(it.alert)
                    }
                    UserAlertDetailViewModel.ViewState.Loading.AlertUpdate -> {
                        activity?.hideSoftKeyboard()
                        binding.updateAlertPb.show()
                    }
                    is UserAlertDetailViewModel.ViewState.Success -> {
                        binding.updateAlertPb.hide()
                        binding.root.findViewById<View>(R.id.quote_widget_root_layout).visibility =
                            View.VISIBLE
                        binding.obj = quoteWidgetViewModel
                        it.quoteWidgetItem?.let { quoteWidget ->
                            quoteWidgetViewModel.updateQuotes(quoteWidget)
                        }
                        // Depending on the symbol type, we might have to remove some of the Irrelevant user input options
                        it.quoteWidgetItem?.instrumentType?.let { symbol ->
                            adjustView(symbol)
                        }
                        toggleUi(it.alert)
                    }
                    UserAlertDetailViewModel.ViewState.Error.Loading -> {
                        binding.updateAlertPb.hide()
                        snackBar = snackbarUtil.retrySnackbar {
                            sharedViewModel.selectedUserAlert?.let { alert ->
                                viewModel.loadAlert(alert)
                            }
                        }
                    }
                    UserAlertDetailViewModel.ViewState.Error.Deleting -> {
                        binding.updateAlertPb.hide()
                        snackBar = snackbarUtil.retrySnackbar {
                            viewModel.deleteAlert()
                        }
                    }
                    UserAlertDetailViewModel.ViewState.Error.Updating -> {
                        binding.updateAlertPb.hide()
                        snackBar = snackbarUtil.retrySnackbar {
                            viewModel.updateAlert()
                        }
                    }
                }
            }
        )
        viewModel.setAlertButtonState().observe(
            viewLifecycleOwner,
            Observer {
                binding.updateAlertBtn.isEnabled =
                    it.first && it.second != UserAlertDetailViewModel.ViewState.Loading.AlertUpdate
            }
        )
        viewModel.viewEffects.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is UserAlertDetailViewModel.ViewEffects.AlertWasDeleted -> {
                        sharedViewModel.didDeleteUserAlert(it.alert)
                        activity?.onBackPressed()
                    }
                    is UserAlertDetailViewModel.ViewEffects.LoadAlertValues -> {
                        when (it.alert) {
                            is UserAlertItem.Alert -> setupTextInputs(it.alert as UserAlertItem.Alert)
                            is UserAlertItem.NewsAlert -> setupSwitches(it.alert as UserAlertItem.NewsAlert)
                        }
                    }
                    is UserAlertDetailViewModel.ViewEffects.AlertWasUpdated -> {
                        val description = getResultDescription(it.response)
                        if (it.response?.hasError == true) {
                            binding.updateAlertPb.hide()
                            snackBar = snackbarUtil.snackbar(description, Snackbar.LENGTH_LONG)
                                ?.apply { show() }
                        } else {
                            sharedViewModel.didUpdateAlert(description)
                            activity?.onBackPressed()
                        }
                    }
                }
            }
        )
    }

    private fun toggleUi(alert: UserAlertItem) {
        when (alert) {
            is UserAlertItem.Alert -> {
                binding.inputs.root.visibility = View.VISIBLE
                switchesAndDividers.forEach { it.visibility = View.GONE }
            }
            is UserAlertItem.NewsAlert -> {
                binding.inputs.root.visibility = View.GONE
                switchesAndDividers.forEach { it.visibility = View.VISIBLE }
            }
        }
    }

    private fun setupSwitches(alert: UserAlertItem.NewsAlert) {
        with(binding.switches) {
            upDowngrade.isChecked = alert.upDownStatus
            inPlay.isChecked = alert.inPlayStatus
            earnings.isChecked = alert.earningsStatus
            splitNotice.isChecked = alert.splitNoticeStatus
        }
    }

    private fun setupTextInputs(alert: UserAlertItem.Alert) {
        with(binding.inputs) {
            tHighTargetPrice.text?.updateValue(alert.targetPriceHigh.activeValue())
            tHighTargetPriceLayout.formatContents()
            tLowTargetPrice.text?.updateValue(alert.targetPriceLow.activeValue())
            tLowTargetPriceLayout.formatContents()
            tHighDailyPercent.text?.updateValue(alert.dailyPercentHigh.activeValue())
            tHighDailyPercentLayout.formatContents()
            tLowDailyPercent.text?.updateValue(alert.dailyPercentLow.activeValue())
            tLowDailyPercentLayout.formatContents()
            tHighPeTarget.text?.updateValue(alert.peTargetHigh.activeValue())
            tHighPeTargetLayout.formatContents()
            tLowPeTarget.text?.updateValue(alert.peTargetLow.activeValue())
            tLowPeTargetLayout.formatContents()
            tDailyVolume.text?.updateValue(alert.dailyVolume.activeValue())
            tDailyVolumeLayout.formatContents()
        }
    }

    private fun getResultDescription(setAlertResponse: AlertResponse?): String {
        setAlertResponse?.let {
            if (it.hasError) {
                return it.errorMessage
            }
        }
        return getString(R.string.alert_was_updated)
    }
}
