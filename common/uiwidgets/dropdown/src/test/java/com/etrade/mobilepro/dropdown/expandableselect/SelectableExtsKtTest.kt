package com.etrade.mobilepro.dropdown.expandableselect

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import java.util.Stack

internal class SelectableExtsKtTest {
    @Test
    fun testFindNested() {
        val path = Stack<DropDownExpandableSelector.Selectable<String>>()
        val output = TestData.listWithNestedSelected.findInTree(path) { it.selected }
        val pathString = path.joinToString { it.value }
        assertEquals("l2_1", output?.value)
        assertEquals("l2_1, l1_1, l0_1", pathString)
    }

    @Test
    fun testFindNestedNoSelected() {
        val path = Stack<DropDownExpandableSelector.Selectable<String>>()
        val output = TestData.listWithNestedNoSelected.findInTree(path) { it.selected }
        val pathString = path.joinToString { it.value }
        assertEquals(null, output)
        assertEquals("", pathString)
    }

    @Test
    fun testFindTopSelected() {
        val path = Stack<DropDownExpandableSelector.Selectable<String>>()
        val output = TestData.listWithTopLevelSelected.findInTree(path) { it.selected }
        val pathString = path.joinToString { it.value }
        assertEquals("l0_2", output?.value)
        assertEquals("l0_2", pathString)
    }

    @Test
    fun findPathToItemNested() {
        val path = TestData.listWithNestedSelected.findPathToItem(TestData.selectedItem1)
        val pathString = path.joinToString { it.value }
        assertEquals("l2_1, l1_1, l0_1", pathString)
    }

    @Test
    fun findPathToItemTopLevel() {
        val path = TestData.listWithTopLevelSelected.findPathToItem(TestData.selectedItem2)
        val pathString = path.joinToString { it.value }
        assertEquals("l0_2", pathString)
    }

    @Test
    fun findPathToItemNoSelected1() {
        val path = TestData.listWithNestedNoSelected.findPathToItem(TestData.selectedItem1)
        val pathString = path.joinToString { it.value }
        assertEquals("", pathString)
    }

    @Test
    fun findPathToItemNoSelected2() {
        val path = TestData.listWithNestedSelected.findPathToItem(TestData.selectedItem2)
        val pathString = path.joinToString { it.value }
        assertEquals("", pathString)
    }

    @Test
    fun clearAllSelectedInTopLevelSelected() {
        val selected1 = TestData.listWithTopLevelSelected.findInTree(Stack()) { it.selected }
        assertNotNull(selected1)

        val output = TestData.listWithTopLevelSelected.clearAllSelected()
        val selected2 = output.findInTree(Stack()) { it.selected }
        assertNull(selected2)
    }

    @Test
    fun clearAllSelectedInNestedSelected() {
        val selected1 = TestData.listWithNestedSelected.findInTree(Stack()) { it.selected }
        assertNotNull(selected1)

        val output = TestData.listWithNestedSelected.clearAllSelected()
        val selected2 = output.findInTree(Stack()) { it.selected }
        assertNull(selected2)
    }

    @Test
    fun testAddNewSelected() {
        val selected1 = TestData.listWithNestedNoSelected.findInTree(Stack()) { it.selected }
        assertNull(selected1)

        val output = TestData.listWithNestedNoSelected.addNewSelected { it.value == "l2_2" }
        val selected2 = output.findInTree(Stack()) { it.selected }
        assertEquals("l2_2", selected2?.value)
    }

    @Test
    fun testFindAllInTreeOneBranch() {
        val output = TestData.listWithNestedSelected.findAllInTree { it.selected }
        assertEquals(1, output.size)
        val path = output[0].joinToString { it.value }
        assertEquals("l0_1, l1_1, l2_1", path)
    }

    @Test
    fun testFindAllInTreeTwoBranches() {
        val output = TestData.listWithTwoExpandedBranches.findAllInTree { it.isExpanded }
        assertEquals(4, output.size)
        assertEquals("l0_1", output[0].joinToString { it.value })
        assertEquals("l0_1, l1_1", output[1].joinToString { it.value })
        assertEquals("l4_1", output[2].joinToString { it.value })
        assertEquals("l4_1, l4_l1_1", output[3].joinToString { it.value })
    }

    @Test
    fun testRemoveSingleElementSublists() {
        assertEquals(1, TestData.listWithSingleItemSubItems[1].subItems?.size)
        val output = TestData.listWithSingleItemSubItems.removeSingleElementSubItems()
        assertNotNull(output)
        assertEquals(3, output.size)
        assertEquals("l0_0, l0_1, l0_2", output.joinToString { it.value })
        assertNull(output[1].subItems)
    }
}

private object TestData {
    val selectedItem1 = DropDownExpandableSelector.Selectable(
        title = "l2_1",
        value = "l2_1",
        selected = true
    )
    val selectedItem2 = DropDownExpandableSelector.Selectable(
        title = "l0_2",
        value = "l0_2",
        selected = true
    )

    val listWithNestedSelected = listOf(
        DropDownExpandableSelector.Selectable(
            title = "l0_0",
            value = "l0_0",
            selected = false
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_1",
            value = "l0_1",
            selected = false,
            subItems = listOf(
                DropDownExpandableSelector.Selectable(
                    title = "l1_0",
                    value = "l1_0",
                    selected = false
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l1_1",
                    value = "l1_1",
                    selected = false,
                    subItems = listOf(
                        DropDownExpandableSelector.Selectable(
                            title = "l2_0",
                            value = "l2_0",
                            selected = false
                        ),
                        selectedItem1,
                        DropDownExpandableSelector.Selectable(
                            title = "l2_2",
                            value = "l2_2",
                            selected = false
                        )
                    )
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l2_2",
                    value = "l2_2",
                    selected = false
                )
            )
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_2",
            value = "l0_2",
            selected = false
        )
    )

    val listWithNestedNoSelected = listOf(
        DropDownExpandableSelector.Selectable(
            title = "l0_0",
            value = "l0_0",
            selected = false
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_1",
            value = "l0_1",
            selected = false,
            subItems = listOf(
                DropDownExpandableSelector.Selectable(
                    title = "l1_0",
                    value = "l1_0",
                    selected = false
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l1_1",
                    value = "l1_1",
                    selected = false,
                    subItems = listOf(
                        DropDownExpandableSelector.Selectable(
                            title = "l2_0",
                            value = "l2_0",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_1",
                            value = "l2_1",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_2",
                            value = "l2_2",
                            selected = false
                        )
                    )
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l2_2",
                    value = "l2_2",
                    selected = false
                )
            )
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_2",
            value = "l0_2",
            selected = false
        )
    )

    val listWithTopLevelSelected = listOf(
        DropDownExpandableSelector.Selectable(
            title = "l0_0",
            value = "l0_0",
            selected = false
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_1",
            value = "l0_1",
            selected = false,
            subItems = listOf(
                DropDownExpandableSelector.Selectable(
                    title = "l1_0",
                    value = "l1_0",
                    selected = false
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l1_1",
                    value = "l1_1",
                    selected = false,
                    subItems = listOf(
                        DropDownExpandableSelector.Selectable(
                            title = "l2_0",
                            value = "l2_0",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_1",
                            value = "l2_1",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_2",
                            value = "l2_2",
                            selected = false
                        )
                    )
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l2_2",
                    value = "l2_2",
                    selected = false
                )
            )
        ),
        selectedItem2
    )

    val listWithTwoExpandedBranches = listOf(
        DropDownExpandableSelector.Selectable(
            title = "l0_0",
            value = "l0_0",
            selected = false
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_1",
            value = "l0_1",
            selected = false,
            isExpanded = true,
            subItems = listOf(
                DropDownExpandableSelector.Selectable(
                    title = "l1_0",
                    value = "l1_0",
                    selected = false
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l1_1",
                    value = "l1_1",
                    selected = false,
                    isExpanded = true,
                    subItems = listOf(
                        DropDownExpandableSelector.Selectable(
                            title = "l2_0",
                            value = "l2_0",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_1",
                            value = "l2_1",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_2",
                            value = "l2_2",
                            selected = false
                        )
                    )
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l2_2",
                    value = "l2_2",
                    selected = false
                )
            )
        ),
        selectedItem2,
        DropDownExpandableSelector.Selectable(
            title = "l4_1",
            value = "l4_1",
            selected = false,
            isExpanded = true,
            subItems = listOf(
                DropDownExpandableSelector.Selectable(
                    title = "l4_l1_0",
                    value = "l4_l1_0",
                    selected = false
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l4_l1_1",
                    value = "l4_l1_1",
                    selected = false,
                    isExpanded = true,
                    subItems = listOf(
                        DropDownExpandableSelector.Selectable(
                            title = "l4_l2_0",
                            value = "l4_l2_0",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l4_l2_1",
                            value = "l4_l2_1",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l4_l2_2",
                            value = "l4_l2_2",
                            selected = false
                        )
                    )
                ),
                DropDownExpandableSelector.Selectable(
                    title = "l4_l2_2",
                    value = "l4_l2_2",
                    selected = false
                )
            )
        )
    )

    val listWithSingleItemSubItems = listOf(
        DropDownExpandableSelector.Selectable(
            title = "l0_0",
            value = "l0_0",
            selected = false
        ),
        DropDownExpandableSelector.Selectable(
            title = "l0_1",
            value = "l0_1",
            selected = false,
            subItems = listOf(
                DropDownExpandableSelector.Selectable(
                    title = "l1_1",
                    value = "l1_1",
                    selected = false,
                    subItems = listOf(
                        DropDownExpandableSelector.Selectable(
                            title = "l2_0",
                            value = "l2_0",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_1",
                            value = "l2_1",
                            selected = false
                        ),
                        DropDownExpandableSelector.Selectable(
                            title = "l2_2",
                            value = "l2_2",
                            selected = false
                        )
                    )
                )
            )
        ),
        selectedItem2
    )
}
