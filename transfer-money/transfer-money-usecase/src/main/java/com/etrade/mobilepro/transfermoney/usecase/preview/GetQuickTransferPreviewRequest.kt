package com.etrade.mobilepro.transfermoney.usecase.preview

import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.etrade.mobilepro.transfermoney.api.TransferRepeat
import com.etrade.mobilepro.transfermoney.api.repo.PreviewRequest
import com.etrade.mobilepro.transfermoney.usecase.confirm.GetQuickTransferConfirmRequest
import com.etrade.mobilepro.util.formatters.createAmericanSimpleDateFormatter
import org.threeten.bp.LocalDate

open class GetQuickTransferPreviewRequest(
    open val userId: String,
    open val platform: String,
    open val scheduleDate: LocalDate,
    open val fromAccount: TransferMoneyAccount,
    open val toAccount: TransferMoneyAccount,
    open val amount: String,
    open val frequency: TransferRepeat
) {

    fun toRepositoryPreviewRequest(): PreviewRequest = PreviewRequest(
        userId,
        platform,
        scheduleDate.format(createAmericanSimpleDateFormatter()),
        fromAccount.accountId,
        fromAccount.accountType,
        toAccount.accountId,
        toAccount.accountType,
        amount,
        frequency
    )

    fun toRepositoryConfirmRequest(iRAContribYear: String?): GetQuickTransferConfirmRequest = GetQuickTransferConfirmRequest(
        userId,
        scheduleDate,
        fromAccount,
        toAccount,
        amount,
        frequency,
        platform,
        iRAContribYear
    )
}
