package com.etrade.mobilepro.holdings.mapper

import com.etrade.mobilepro.holdings.dto.HoldingsResponseDto
import com.etrade.mobilepro.holdingsapi.PositionHoldingsItem

class PositionHoldingJsonMapper {
    fun map(jsonDto: HoldingsResponseDto): PositionHoldingsItem {
        return PositionHoldingsItem(
            jsonDto.data?.holdings?.totalPositionsCount ?: "",
            jsonDto.data?.holdings?.hasHoldings ?: false
        )
    }
}
