# BarChartView module

## Example code snippet

```
    fun demo() {
        val purple = Color.parseColor("#673AB7")
        val green = Color.parseColor("#5BA122")
        val red = Color.parseColor("#C2185B")
        val black = Color.BLACK

        val data = mutableListOf(
                DataEntry(0f,-4f, barColor = purple, valueColor = black, valueDescriptionColor = green),
                DataEntry(1f,3f, barColor = purple, valueColor = black, valueDescriptionColor = red),
                DataEntry(2f,-0.5f, barColor = purple, valueColor = black, valueDescriptionColor = green),
                DataEntry(3f,1f, barColor = purple, valueColor = black, valueDescriptionColor = green),
                DataEntry(4f,-2f, barColor = purple, valueColor = black, valueDescriptionColor = red),
                DataEntry(5f,6f, barColor = purple, valueColor = black, valueDescriptionColor = green),
                DataEntry(6f,3f, barColor = purple, valueColor = black, valueDescriptionColor = green),
                DataEntry(7f,4f, barColor = purple, valueColor = black, valueDescriptionColor = green),
                DataEntry(8f,3f, barColor = purple, valueColor = black, valueDescriptionColor = red),
                DataEntry(9f,2f, barColor = purple, valueColor = black, valueDescriptionColor = red),
                DataEntry(10f,1f, barColor = purple, valueColor = black, valueDescriptionColor = red),
                DataEntry(11f,0.5f, barColor = purple, valueColor = black, valueDescriptionColor = red),
                DataEntry(12f,-4f, barColor = purple, valueColor = black, valueDescriptionColor = red),
                DataEntry(13f,-0.5f, barColor = purple, valueColor = black, valueDescriptionColor = green),
                DataEntry(14f,-1f, barColor = purple, valueColor = black, valueDescriptionColor = green),
                DataEntry(15f,-2f, barColor = purple, valueColor = black, valueDescriptionColor = green),
                DataEntry(16f,-3f, barColor = purple, valueColor = black, valueDescriptionColor = green)
        )

        barChartView.apply {
            //formatter
            xAxisValueFormatter = { value -> value.toString()+"a" }
            yAxisValueFormatter = { value -> value.toString()+"b" }
            barValueFormatter = { value -> value.toString()+"c" }
            barValueDescriptionFormatter = { _, value -> value.toString()+"d" }

            zeroBarHeight = Utils.convertDpToPixel(32f)
            valueTextSizeDp = 25f
            valueDescriptionMargin = Utils.convertDpToPixel(5f)
            defaultVisibleXRangeMaximum = 6f

            //left listener
            onLeftBoundaryReached = {
                val min = barChartView.barData.getDataSetByIndex(0).xMin
                val moreData = listOf(
                        DataEntry(min-2f,-1f, red, red, red),
                        DataEntry(min-1f,4.5f, purple, purple, purple)
                )

                barChartView.add(moreData, true)
            }

            //right listener
            onRightBoundaryReached = {
                val max = barChartView.barData.getDataSetByIndex(0).xMax
                val moreData = listOf(
                        DataEntry(max+1f,3f, red, red, green),
                        DataEntry(max+2f,3f, red, red, green),
                        DataEntry(max+3f,3f, red, red, green),
                        DataEntry(max+4f,5f, purple, red, purple),
                        DataEntry(max+5f,-1f, purple, green, purple),
                        DataEntry(max+6f,4.5f, purple, red, purple)
                )

                barChartView.add(moreData, true)
            }
        }

        //Adding first set of data
        barChartView.add(data)
    }
```


