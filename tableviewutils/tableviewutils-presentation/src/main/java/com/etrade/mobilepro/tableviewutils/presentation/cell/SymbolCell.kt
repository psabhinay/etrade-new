package com.etrade.mobilepro.tableviewutils.presentation.cell

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.toBondSymbolWithSubTitle
import com.etrade.mobilepro.instrument.toOptionsSymbolWithSubtitle
import com.evrencoskun.tableview.filter.IFilterableModel
import com.evrencoskun.tableview.sort.ISortableModel

open class SymbolCell(
    id: String,
    displayText: String,
    override val symbol: String,
    override val instrumentType: InstrumentType,
    var description: String?
) : Cell(id, displayText), ISortableModel, IFilterableModel, WithSymbolInfo

fun SymbolCell.getRowHeaderTextAndSubText(displaySymbolDescription: Boolean = false) = when {
    instrumentType == InstrumentType.BOND -> {
        description to (description?.toBondSymbolWithSubTitle() ?: displayText)
    }
    instrumentType == InstrumentType.INDX -> {
        symbol to displayText
    }
    instrumentType.isOption -> {
        description?.toOptionsSymbolWithSubtitle() ?: Pair(displayText, "")
    }
    else -> {
        displayText to

            if (displaySymbolDescription && description?.isNotEmpty() == true) {
                description
            } else {
                ""
            }
    }
}
