package com.etrade.mobilepro.screener.data.json.etf

import com.etrade.mobilepro.screener.api.Argument
import com.etrade.mobilepro.screener.api.Clause
import com.etrade.mobilepro.screener.api.FilterModelGenerator
import com.etrade.mobilepro.screener.api.OPERATOR_GREATER_THAN_OR_EQUAL_TO
import com.etrade.mobilepro.screener.api.OPERATOR_LESS_THAN
import com.etrade.mobilepro.screener.api.PartialScreenerParams
import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.filter.etf.EtfPerformanceFilter
import com.etrade.mobilepro.screener.api.mergeFollowingClauses
import javax.inject.Inject

private const val WITHIN_CATEGORY_OVER_1M_FIELD = "Mkt_1Month_Ptile"
private const val WITHIN_CATEGORY_OVER_3M_FIELD = "Mkt_3Month_Ptile"
private const val WITHIN_CATEGORY_OVER_6M_FIELD = "Mkt_6Month_Ptile"
private const val WITHIN_CATEGORY_OVER_YTD_FIELD = "Mkt_YTD_Ptile"
private const val WITHIN_CATEGORY_OVER_1Y_FIELD = "Mkt_1Year_Ptile"
private const val WITHIN_CATEGORY_OVER_3Y_FIELD = "Mkt_3Year_Ptile"
private const val WITHIN_CATEGORY_OVER_5Y_FIELD = "Mkt_5Year_Ptile"
private const val WITHIN_CATEGORY_OVER_10Y_FIELD = "Mkt_10Year_Ptile"

private const val NAV_RETURN_OVER_1M_FIELD = "NavRetCat1Month"
private const val NAV_RETURN_OVER_3M_FIELD = "NavRetCat3Month"
private const val NAV_RETURN_OVER_6M_FIELD = "NavRetCat6Month"
private const val NAV_RETURN_OVER_YTD_FIELD = "NavRetCatYTD"
private const val NAV_RETURN_OVER_1Y_FIELD = "NavRetCat1Year"
private const val NAV_RETURN_OVER_3Y_FIELD = "NavRetCat3Year"
private const val NAV_RETURN_OVER_5Y_FIELD = "NavRetCat5Year"
private const val NAV_RETURN_OVER_10Y_FIELD = "NavRetCat10Year"

class EtfPerformanceFilterModelGenerator @Inject constructor() : FilterModelGenerator {
    @Suppress("LongMethod", "ComplexMethod") // not complex method, there is no nesting conditions
    override fun toModel(screenerFilter: ScreenerFilter): PartialScreenerParams {
        val etfPerformanceFilter = screenerFilter as EtfPerformanceFilter
        val arguments = mutableListOf<Argument>()

        if (!etfPerformanceFilter.isWithinCategoryEmpty()) {
            val clauses = getWithinCategoryValuesClauses(etfPerformanceFilter)

            if (etfPerformanceFilter.withinCategoryOverLast1Month) {
                arguments.add(Argument(clauses, WITHIN_CATEGORY_OVER_1M_FIELD))
            }
            if (etfPerformanceFilter.withinCategoryOverLast3Months) {
                arguments.add(Argument(clauses, WITHIN_CATEGORY_OVER_3M_FIELD))
            }
            if (etfPerformanceFilter.withinCategoryOverLast6Months) {
                arguments.add(Argument(clauses, WITHIN_CATEGORY_OVER_6M_FIELD))
            }
            if (etfPerformanceFilter.withinCategoryOverLastYTD) {
                arguments.add(Argument(clauses, WITHIN_CATEGORY_OVER_YTD_FIELD))
            }
            if (etfPerformanceFilter.withinCategoryOverLast1Year) {
                arguments.add(Argument(clauses, WITHIN_CATEGORY_OVER_1Y_FIELD))
            }
            if (etfPerformanceFilter.withinCategoryOverLast3Years) {
                arguments.add(Argument(clauses, WITHIN_CATEGORY_OVER_3Y_FIELD))
            }
            if (etfPerformanceFilter.withinCategoryOverLast5Years) {
                arguments.add(Argument(clauses, WITHIN_CATEGORY_OVER_5Y_FIELD))
            }
            if (etfPerformanceFilter.withinCategoryOverLast10Years) {
                arguments.add(Argument(clauses, WITHIN_CATEGORY_OVER_10Y_FIELD))
            }
        }

        if (!etfPerformanceFilter.isNavReturnEmpty()) {
            val clauses = getNavReturnValuesClauses(etfPerformanceFilter)

            if (etfPerformanceFilter.navReturnOverLast1Month) {
                arguments.add(Argument(clauses, NAV_RETURN_OVER_1M_FIELD))
            }
            if (etfPerformanceFilter.navReturnOverLast3Months) {
                arguments.add(Argument(clauses, NAV_RETURN_OVER_3M_FIELD))
            }
            if (etfPerformanceFilter.navReturnOverLast6Months) {
                arguments.add(Argument(clauses, NAV_RETURN_OVER_6M_FIELD))
            }
            if (etfPerformanceFilter.navReturnOverLastYTD) {
                arguments.add(Argument(clauses, NAV_RETURN_OVER_YTD_FIELD))
            }
            if (etfPerformanceFilter.navReturnOverLast1Year) {
                arguments.add(Argument(clauses, NAV_RETURN_OVER_1Y_FIELD))
            }
            if (etfPerformanceFilter.navReturnOverLast3Years) {
                arguments.add(Argument(clauses, NAV_RETURN_OVER_3Y_FIELD))
            }
            if (etfPerformanceFilter.navReturnOverLast5Years) {
                arguments.add(Argument(clauses, NAV_RETURN_OVER_5Y_FIELD))
            }
            if (etfPerformanceFilter.navReturnOverLast10Years) {
                arguments.add(Argument(clauses, NAV_RETURN_OVER_10Y_FIELD))
            }
        }

        return PartialScreenerParams(arguments)
    }

    private fun getWithinCategoryValuesClauses(etfPerformanceFilter: EtfPerformanceFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (etfPerformanceFilter.withinCategoryBottom20) {
            clauses.addAll(bottom20Clauses)
        }
        if (etfPerformanceFilter.withinCategoryBelowMid20) {
            clauses.addAll(belowMid20Clauses)
        }
        if (etfPerformanceFilter.withinCategoryMid20) {
            clauses.addAll(mid20Clauses)
        }
        if (etfPerformanceFilter.withinCategoryAboveMid20) {
            clauses.addAll(aboveMid20Clauses)
        }
        if (etfPerformanceFilter.withinCategoryTop20) {
            clauses.addAll(top20Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private fun getNavReturnValuesClauses(etfPerformanceFilter: EtfPerformanceFilter): List<Clause> {
        val clauses = mutableListOf<Clause>()

        if (etfPerformanceFilter.navReturnBottom20) {
            clauses.addAll(bottom20Clauses)
        }
        if (etfPerformanceFilter.navReturnBelowMid20) {
            clauses.addAll(belowMid20Clauses)
        }
        if (etfPerformanceFilter.navReturnMid20) {
            clauses.addAll(mid20Clauses)
        }
        if (etfPerformanceFilter.navReturnAboveMid20) {
            clauses.addAll(aboveMid20Clauses)
        }
        if (etfPerformanceFilter.navReturnTop20) {
            clauses.addAll(top20Clauses)
        }

        return clauses.mergeFollowingClauses()
    }

    private val bottom20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("0")),
        Clause(OPERATOR_LESS_THAN, listOf("21"))
    )
    private val belowMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("21")),
        Clause(OPERATOR_LESS_THAN, listOf("41"))
    )
    private val mid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("41")),
        Clause(OPERATOR_LESS_THAN, listOf("61"))
    )
    private val aboveMid20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("61")),
        Clause(OPERATOR_LESS_THAN, listOf("81"))
    )
    private val top20Clauses = listOf(
        Clause(OPERATOR_GREATER_THAN_OR_EQUAL_TO, listOf("81")),
        Clause(OPERATOR_LESS_THAN, listOf("101"))
    )
}
