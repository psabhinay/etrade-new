package com.etrade.mobilepro.portfolio.data

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.portfolio.tableview.PortfolioColumn
import com.etrade.mobilepro.tableviewutils.api.ColumnRepo
import com.etrade.mobilepro.tableviewutils.presentation.data.BaseColumnRepo
import javax.inject.Inject

/**
 * This repository handles everything related to columns of a portfolio table's entries in list view.
 */
interface ListViewColumnRepo : ColumnRepo<PortfolioColumn>

private const val COLUMN_QUANTITY = "Quantity"
private const val COLUMN_PRICE_PAID = "PricePaid"
private const val COLUMN_TOTAL_GAIN_DOLLAR = "TotalGainDollar"
private const val COLUMN_DAYS_GAIN_DOLLAR = "DaysGainDollar"
private const val COLUMN_DAYS_GAIN_PERCENT = "ColumnDaysGainPercent"
private const val COLUMN_TOTAL_GAIN_PERCENT = "TotalGainPercent"

class ListViewColumnRepoImpl @Inject constructor(storage: KeyValueStorage) :
    BaseColumnRepo<PortfolioColumn>(storage), ListViewColumnRepo {

    override val storageKey: String = "portfolioListViewTableColumns"

    override val allColumns: Map<String, PortfolioColumn> = mapOf(
        COLUMN_PRICE_PAID to PortfolioColumn.PRICE_PAID,
        COLUMN_QUANTITY to PortfolioColumn.QUANTITY,
        COLUMN_DAYS_GAIN_DOLLAR to PortfolioColumn.DAYS_GAIN_DOLLAR,
        COLUMN_DAYS_GAIN_PERCENT to PortfolioColumn.DAYS_GAIN_PERCENT,
        COLUMN_TOTAL_GAIN_DOLLAR to PortfolioColumn.TOTAL_GAIN_DOLLAR,
        COLUMN_TOTAL_GAIN_PERCENT to PortfolioColumn.TOTAL_GAIN_PERCENT
    )

    override val defaultColumnsOrder: List<String> = listOf(
        COLUMN_PRICE_PAID,
        COLUMN_QUANTITY,
        COLUMN_DAYS_GAIN_DOLLAR,
        COLUMN_DAYS_GAIN_PERCENT,
        COLUMN_TOTAL_GAIN_DOLLAR,
        COLUMN_TOTAL_GAIN_PERCENT
    )
}
