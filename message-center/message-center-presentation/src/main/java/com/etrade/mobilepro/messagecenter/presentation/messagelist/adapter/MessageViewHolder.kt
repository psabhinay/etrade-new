package com.etrade.mobilepro.messagecenter.presentation.messagelist.adapter

import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.presentation.R
import com.etrade.mobilepro.messagecenter.presentation.databinding.MessageCenterItemMessageBinding
import com.etrade.mobilepro.util.android.accessibility.accountNameContentDescription

internal class MessageViewHolder(
    private val binding: MessageCenterItemMessageBinding,
    private val clickListener: (Message) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    private var item: Message? = null

    init {
        binding.root.setOnClickListener {
            item?.let {
                clickListener.invoke(it)
            }
        }
    }

    fun bindTo(item: Message) {
        this.item = item
        setupStatus(item)
        setupSubject(item)
        setupAccount(item)
        setupDate(item)
    }

    private fun setupDate(item: Message) {
        binding.date.text = item.createdAtFormatted
        val dateTimeStyle = if (item.isUnread) {
            R.style.message_center_date
        } else {
            R.style.message_center_date_read
        }
        TextViewCompat.setTextAppearance(binding.date, dateTimeStyle)
    }

    private fun setupAccount(item: Message) {
        val shortName = item.accountShortName
        binding.accountName.text = shortName
        binding.accountName.contentDescription = binding.root.context.accountNameContentDescription(shortName)
        val accountTextStyle = if (item.isUnread) {
            R.style.message_center_account
        } else {
            R.style.message_center_account_read
        }
        TextViewCompat.setTextAppearance(binding.accountName, accountTextStyle)
    }

    private fun setupSubject(item: Message) {
        binding.subject.text = item.caseTypeDesc
        val subjectStyle = if (item.isUnread) {
            R.style.TextAppearance_Etrade_Title
        } else {
            R.style.TextAppearance_Etrade_Title_Read30
        }
        TextViewCompat.setTextAppearance(binding.subject, subjectStyle)
    }

    private fun setupStatus(item: Message) {
        binding.status.text = item.status
        val statusTextStyle = if (item.isUnread) {
            R.style.message_center_status
        } else {
            R.style.message_center_status_read
        }
        TextViewCompat.setTextAppearance(binding.status, statusTextStyle)
    }
}
