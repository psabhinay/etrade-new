package com.etrade.mobilepro.marketscalendar.adapter

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.etrade.mobilepro.marketscalendar.R
import com.etrade.mobilepro.marketscalendar.fragment.EarningsMarketsCalendarFragment
import com.etrade.mobilepro.marketscalendar.fragment.EconomicMarketsCalendarFragment

sealed class MarketsCalendarTab(@StringRes val title: Int, val fragmentClass: Class<out Fragment>) {

    object Earnings : MarketsCalendarTab(R.string.markets_calendar_earnings, EarningsMarketsCalendarFragment::class.java)
    object Events : MarketsCalendarTab(R.string.markets_calendar_events, EconomicMarketsCalendarFragment::class.java)
}
