package com.etrade.mobilepro.watchlist.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class WatchlistResponseDto(
    @Json(name = "mobile_response")
    val mobileResponse: WatchlistResponseReferencesDto
)

@JsonClass(generateAdapter = true)
class WatchlistResponseReferencesDto(
    @Json(name = "references")
    val references: List<WatchlistResponseDataDto>? = null
)

@JsonClass(generateAdapter = true)
class WatchlistResponseDataDto(
    @Json(name = "data")
    val data: List<WatchlistDto>? = null
)

@JsonClass(generateAdapter = true)
class WatchlistDto(
    @Json(name = "watchListId")
    val watchListId: String,
    @Json(name = "watchListName")
    val watchListName: String?,
    @Json(name = "watchListUuid")
    val watchListUuid: String?
)

@JsonClass(generateAdapter = true)
class WatchlistRequestDto(
    @Json(name = "all")
    val all: Any? = Object()
)
