package com.etrade.mobilepro.ui.nestedrecyclerview

import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import com.evrencoskun.tableview.TableScrollView

/**
 * Helper class that helps a [RestrictedHeightView] direct or indirect child to constraint it's height.
 */
class RestrictedViewHelper {

    /**
     * Map of restricted views associated with a direct child view of the content view.
     */
    private val restrictedChildViews: MutableMap<View, RestrictedHeightView> = mutableMapOf()

    /**
     * Registers all [RestrictedHeightView] instances among the content view's children.
     */
    private val childAttachStateChangeListener: RecyclerView.OnChildAttachStateChangeListener = object : RecyclerView.OnChildAttachStateChangeListener {

        override fun onChildViewAttachedToWindow(view: View) {
            findRestrictedViews(view)?.also {
                restrictedChildViews[view] = it
            }
        }

        override fun onChildViewDetachedFromWindow(view: View) {
            restrictedChildViews.remove(view)
        }
    }

    /**
     * Registers all [RestrictedHeightView] instances among the content view's children.
     */
    private val attachStateChangeListener: View.OnAttachStateChangeListener = object : View.OnAttachStateChangeListener {
        override fun onViewDetachedFromWindow(view: View) {
            restrictedChildViews.remove(view)
        }

        override fun onViewAttachedToWindow(view: View) {
            findRestrictedViews(view)?.also {
                restrictedChildViews[view] = it
            }
        }
    }

    /**
     * Listens for the contents view's size change.
     */
    private val contentViewLayoutChangeListener: View.OnLayoutChangeListener = View.OnLayoutChangeListener { _, _, top, _, bottom, _, _, _, _ ->
        maxRestrictedViewHeight = bottom - top
    }

    /**
     * Maximal height of a restricted view. Restricted with the size of the content view.
     */
    private var maxRestrictedViewHeight: Int = 0
        set(value) {
            if (field == value) { return }
            field = value
            restrictedChildViews.values.forEach { view ->
                view.viewMaxHeight = maxRestrictedViewHeight
            }
        }

    /**
     * Attaches helper to an instance of [RecyclerView].
     *
     * @param recyclerView The instance of [RecyclerView] to attach this helper to.
     */
    fun attachTo(recyclerView: RecyclerView) {
        recyclerView.apply {
            addOnChildAttachStateChangeListener(childAttachStateChangeListener)
            addOnLayoutChangeListener(contentViewLayoutChangeListener)
        }
    }

    /**
     * Attaches helper to an instance of [TableScrollView].
     *
     * @param tableScrollView The instance of [TableScrollView] to attach this helper to.
     */
    fun attachTo(tableScrollView: TableScrollView) {
        tableScrollView.apply {
            addOnAttachStateChangeListener(attachStateChangeListener)
            addOnLayoutChangeListener(contentViewLayoutChangeListener)
        }
    }

    /**
     * Recursively looks for any child in the [view] that implements [RestrictedHeightView] interface.
     *
     * The [view] itself can be [RestrictedHeightView].
     *
     * @param view a view that may have [RestrictedHeightView] instances within it
     *
     * @return instance of [RestrictedHeightView] if found, `null` otherwise
     */
    private fun findRestrictedViews(view: View): RestrictedHeightView? {
        when (view) {
            is RestrictedHeightView -> {
                val restrictedHeightView: RestrictedHeightView = view
                restrictedHeightView.viewMaxHeight = maxRestrictedViewHeight
                return restrictedHeightView
            }
            is ViewGroup -> {
                for (child in view.children) {
                    val candidate = findRestrictedViews(child)
                    if (candidate != null) {
                        return candidate
                    }
                }
            }
        }
        return null
    }
}
