package com.etrade.mobilepro.optionschain.tableview.view

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.etrade.mobilepro.optionschain.tableview.OptionsChainColumn
import com.etrade.mobilepro.optionschain.tableview.R
import com.etrade.mobilepro.optionschain.tableview.RowHeightCache
import com.etrade.mobilepro.optionschain.tableview.SelectedOptionsPair
import com.etrade.mobilepro.optionschain.tableview.adapter.ColumnHeadersAdapter
import com.etrade.mobilepro.optionschain.tableview.adapter.OptionsListAdapter
import com.etrade.mobilepro.optionschain.tableview.adapter.StrikePriceRecyclerAdapter
import com.etrade.mobilepro.uiwidgets.groupscrollrecyclerview.VerticalGroupScrollListener
import com.etrade.mobilepro.uiwidgets.groupscrollrecyclerview.VerticalGroupScrollRecyclerView
import com.etrade.mobilepro.uiwidgets.groupscrollrecyclerview.VerticalGroupScrollRecyclerView.OnScrollStoppedListener
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.setupRecyclerViewDivider
import com.etrade.mobilepro.util.android.goneIf
import com.etrade.mobilepro.util.android.goneUnless

private const val SCROLL_ANIMATION_DURATION = 1000L

class OptionsChainTableView : ConstraintLayout {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val rowHeightCache = RowHeightCache()
    private val strikePriceAdapter = StrikePriceRecyclerAdapter(context).also {
        it.setRowHeightCache(rowHeightCache)
    }
    private val callColumnsAdapter = ColumnHeadersAdapter(context)
    private val putColumnsAdapter = ColumnHeadersAdapter(context)

    private var strikePricesRecyclerView: VerticalGroupScrollRecyclerView
    private var callOptionsRecyclerView: VerticalGroupScrollRecyclerView
    private var putOptionsRecyclerView: VerticalGroupScrollRecyclerView
    private var callOptionsScrollView: CompatibleHorizontalScrollView
    private var putOptionsScrollView: CompatibleHorizontalScrollView

    private var emptyTextView: TextView
    private var optionsChainGroup: Group

    private var callColumnHeadersRecyclerView: RecyclerView
    private var putColumnHeadersRecyclerView: RecyclerView
    private var strikePriceHeader: View

    private var allRecyclerViews: List<VerticalGroupScrollRecyclerView>

    init {
        isNestedScrollingEnabled = true

        val view = this.inflate(R.layout.options_chain_table_view, true)

        callColumnHeadersRecyclerView = view.findViewById<RecyclerView>(R.id.calls_columns_rv)
        putColumnHeadersRecyclerView = view.findViewById<RecyclerView>(R.id.puts_columns_rv)

        strikePricesRecyclerView = view.findViewById(R.id.strike_prices_rv)
        callOptionsRecyclerView = view.findViewById(R.id.calls_options_rv)
        putOptionsRecyclerView = view.findViewById(R.id.puts_options_rv)

        allRecyclerViews = listOf(strikePricesRecyclerView, callOptionsRecyclerView, putOptionsRecyclerView)

        callOptionsScrollView = view.findViewById(R.id.call_options_hs)
        putOptionsScrollView = view.findViewById(R.id.put_options_hs)

        emptyTextView = view.findViewById(R.id.options_no_data_msg)

        optionsChainGroup = view.findViewById(R.id.options_chain_group)
        strikePriceHeader = view.findViewById(R.id.strike_price_text_view)

        val callColumns = OptionsChainColumn.getDefaultColumns(true)
        callColumnHeadersRecyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true)
            callColumnsAdapter.columnHeaders = callColumns
            adapter = callColumnsAdapter
        }

        putColumnHeadersRecyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            putColumnsAdapter.columnHeaders = OptionsChainColumn.getDefaultColumns(false)
            adapter = putColumnsAdapter
        }

        strikePricesRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = strikePriceAdapter
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
            setupRecyclerViewDivider(R.drawable.thin_divider)
        }

        callOptionsRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
            setupRecyclerViewDivider(R.drawable.thin_divider)
        }

        putOptionsRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
            setupRecyclerViewDivider(R.drawable.thin_divider)
        }

        measureAndAlignColumnHeadersHeight(callColumns)
        setupScrolling()
    }

    private fun setupScrolling() {
        val horizontalScrollListener = object : CompatibleScrollChangeListener {
            override fun onScrollChange(v: View, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
                // using the same view to get max scrollX is intended to avoid infinite loops where left scroll view and right scroll view
                // are one pixel off in some cases.
                if (v == callOptionsScrollView) {
                    putOptionsScrollView.scrollTo(-(scrollX - getMaxScrollX(callOptionsScrollView)), scrollY)
                } else if (v == putOptionsScrollView) {
                    callOptionsScrollView.scrollTo(-(scrollX - getMaxScrollX(callOptionsScrollView)), scrollY)
                }
            }
        }

        callOptionsScrollView.compatibleScrollChangeListener = horizontalScrollListener
        putOptionsScrollView.compatibleScrollChangeListener = horizontalScrollListener

        val verticalGroupScrollListener = VerticalGroupScrollListener(allRecyclerViews)

        allRecyclerViews.forEach {
            it.addOnItemTouchListener(verticalGroupScrollListener)
        }
    }

    private fun getMaxScrollX(scrollView: CompatibleHorizontalScrollView) = scrollView.getChildAt(0).width - scrollView.width

    fun setAdapters(callAdapter: OptionsListAdapter, putAdapter: OptionsListAdapter) {
        callAdapter.setRowHeightCache(rowHeightCache)
        putAdapter.setRowHeightCache(rowHeightCache)
        callOptionsRecyclerView.adapter = callAdapter
        putOptionsRecyclerView.adapter = putAdapter
    }

    fun setOptions(optionsPairs: List<SelectedOptionsPair>) {
        rowHeightCache.clearAllData()

        val strikePriceData = optionsPairs.map { it.call.option.strikePrice }
        strikePriceAdapter.measureRowHeights(strikePriceData)

        val callOptionsData = optionsPairs.map { it.call }
        val callOptionsAdapter = callOptionsRecyclerView.adapter as? OptionsListAdapter
        callOptionsAdapter?.measureRowHeights(callOptionsData)

        val putOptionsData = optionsPairs.map { it.put }
        val putOptionsAdapter = putOptionsRecyclerView.adapter as? OptionsListAdapter
        putOptionsAdapter?.measureRowHeights(putOptionsData)

        strikePriceAdapter.setData(strikePriceData)
        callOptionsAdapter?.setOptionsData(callOptionsData)
        putOptionsAdapter?.setOptionsData(putOptionsData)

        val isEmpty = optionsPairs.isEmpty()
        emptyTextView.goneUnless(isEmpty)
        optionsChainGroup.goneIf(isEmpty)
    }

    fun getCurrentVisibleRange(): IntRange {
        val layoutManager = (strikePricesRecyclerView.layoutManager as LinearLayoutManager)
        return layoutManager.findFirstVisibleItemPosition()..layoutManager.findLastVisibleItemPosition()
    }

    fun setOnScrollStoppedListener(scrollStoppedListener: OnScrollStoppedListener?) {
        allRecyclerViews.forEach { it.onScrollStoppedListener = scrollStoppedListener }
    }

    @Suppress("MagicNumber")
    private val rowsOffSet = 5

    /**
     * Return the number of entries below the table to shift to show *offset*
     * number of entries that are "in the money"
     */
    fun shiftTableForInTheMoneyVisibility(totalInTheMoneyCount: Int) {
        // scrolls to a certain strike price.
        val position = getStrikePricePosition(totalInTheMoneyCount)
        (strikePricesRecyclerView.layoutManager as? LinearLayoutManager)?.scrollToPositionWithOffset(position, 0)
        (callOptionsRecyclerView.layoutManager as? LinearLayoutManager)?.scrollToPositionWithOffset(position, 0)
        (putOptionsRecyclerView.layoutManager as? LinearLayoutManager)?.scrollToPositionWithOffset(position, 0)
    }

    fun animateFanIn() {
        // animates columns to fan in
        ObjectAnimator.ofInt(
            callOptionsScrollView,
            "scrollX", getMaxScrollX(callOptionsScrollView)
        ).setDuration(SCROLL_ANIMATION_DURATION).start()
    }

    private fun getStrikePricePosition(totalInTheMoneyCount: Int) = totalInTheMoneyCount - rowsOffSet

    private fun measureAndAlignColumnHeadersHeight(headers: List<OptionsChainColumn>) {
        val headersHeightCache = RowHeightCache()
        callColumnsAdapter.setRowHeightCache(headersHeightCache)
        callColumnsAdapter.measureRowHeights(headers)
        val maxHeaderHeight = headersHeightCache.findMaxHeight()

        callColumnHeadersRecyclerView.layoutParams.height = maxHeaderHeight
        putColumnHeadersRecyclerView.layoutParams.height = maxHeaderHeight
        strikePriceHeader.layoutParams.height = maxHeaderHeight
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        when (ev?.action) {
            MotionEvent.ACTION_DOWN -> {
                val callOptionsRect = Rect()
                callOptionsScrollView.getHitRect(callOptionsRect)
                val putOptionsRect = Rect()
                putOptionsScrollView.getHitRect(putOptionsRect)
                val strikePriceRect = Rect()
                strikePricesRecyclerView.getHitRect(strikePriceRect)
                val evX = ev.x.toInt()
                val evY = ev.y.toInt()
                if (callOptionsRect.contains(evX, evY) ||
                    putOptionsRect.contains(evX, evY) ||
                    strikePriceRect.contains(evX, evY)
                ) {
                    parent?.requestDisallowInterceptTouchEvent(true)
                } else {
                    parent?.requestDisallowInterceptTouchEvent(false)
                }
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> parent?.requestDisallowInterceptTouchEvent(false)
        }
        return super.onInterceptTouchEvent(ev)
    }
}
