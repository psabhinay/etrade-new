<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <variable
            name="quoteItem"
            type="com.etrade.mobilepro.quoteapi.QuoteItem.ClosingPriceItem" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:id="@+id/quote_ext_hrs_widget_root_layout"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:importantForAccessibility="no">

        <TextView
            android:id="@+id/closing_price_title"
            style="@style/Title.Divider"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:focusable="true"
            android:gravity="center_horizontal"
            android:padding="@dimen/spacing_small"
            android:text="@{quoteItem.title}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            tools:text="Closing Price" />

        <LinearLayout
            android:id="@+id/quote_close_price_container"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/spacing_medium"
            android:focusable="true"
            android:importantForAccessibility="yes"
            android:orientation="vertical"
            app:processPriceDescription="@{quoteItem.lastPrice.price}"
            app:processPriceLabelDescription="@{@string/quote_close_price_title}"
            app:layout_constraintTop_toBottomOf="@id/closing_price_title"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintEnd_toStartOf="@id/quote_close_change_container">

            <TextView
                android:id="@+id/quote_close_price_label"
                style="@style/QuoteWidget.Label"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:focusable="false"
                android:importantForAccessibility="no"
                android:layout_gravity="center"
                android:text="@string/quote_close_price_title"
                tools:text="Last" />

            <TextView
                android:id="@+id/quote_close_price"
                style="@style/QuoteWidget"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:focusable="false"
                android:importantForAccessibility="no"
                android:layout_gravity="center"
                android:layout_marginTop="@dimen/spacing_xsmall"
                app:formatPrice="@{quoteItem.lastPrice.price}"
                app:liveTextColor="@{quoteItem.lastPrice.priceColor}"
                tools:text="91355541.73" />
        </LinearLayout>

        <LinearLayout
            android:id="@+id/quote_close_change_container"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:focusable="true"
            android:importantForAccessibility="yes"
            android:orientation="vertical"
            app:processChangeDescription="@{quoteItem.change}"
            app:processChangeLabelDescription="@{@string/quote_close_change_title}"
            app:layout_constraintStart_toEndOf="@id/quote_close_price_container"
            app:layout_constraintEnd_toStartOf="@id/quote_close_volume_container"
            app:layout_constraintTop_toTopOf="@id/quote_close_price_container">

            <TextView
                android:id="@+id/quote_close_change_label"
                style="@style/QuoteWidget.Label"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:focusable="false"
                android:importantForAccessibility="no"
                android:text="@string/quote_close_change_title"
                android:layout_gravity="center"
                tools:text="Change" />

            <TextView
                android:id="@+id/quote_close_change"
                style="@style/QuoteWidget"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:focusable="false"
                android:importantForAccessibility="no"
                android:layout_gravity="center"
                android:layout_marginTop="@dimen/spacing_xsmall"
                app:formatChange="@{quoteItem.change}"
                app:liveTextColor="@{quoteItem.changeColor}"
                tools:text="1090.32 (329.51%)" />
        </LinearLayout>

        <LinearLayout
            android:id="@+id/quote_close_volume_container"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:focusable="true"
            android:importantForAccessibility="yes"
            android:orientation="vertical"
            app:processVolumeDescription="@{quoteItem.volume}"
            app:processVolumeLabelDescription="@{@string/quote_close_volume_title}"
            app:layout_constraintStart_toEndOf="@id/quote_close_change_container"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="@id/quote_close_price_container">

            <TextView
                android:id="@+id/quote_close_volume_label"
                style="@style/QuoteWidget.Label"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:focusable="false"
                android:importantForAccessibility="no"
                android:text="@string/quote_close_volume_title"
                android:layout_gravity="center"
                tools:text="Volume" />

            <TextView
                android:id="@+id/quote_close_volume"
                style="@style/QuoteWidget"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:focusable="false"
                android:importantForAccessibility="no"
                android:layout_gravity="center"
                android:layout_marginTop="@dimen/spacing_xsmall"
                app:formatVolume="@{quoteItem.volume}"
                tools:text="12.57M" />
        </LinearLayout>

        <TextView
            android:id="@+id/quote_last_price_label"
            style="@style/TextAppearance.Etrade.Text.Regular.Small"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/spacing_small"
            android:focusable="true"
            android:text="@{quoteItem.lastTradeTime}"
            app:goneIf="@{quoteItem.lastTradeTime.isEmpty()}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/quote_close_change_container"
            tools:text="As of 9:45 AM EST 03/01/2018" />

        <View
            android:id="@+id/quote_widget_borderline"
            android:layout_width="match_parent"
            android:layout_height="@dimen/divider_height"
            android:layout_marginTop="@dimen/spacing_small"
            android:background="@color/light_grey"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/quote_last_price_label" />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
