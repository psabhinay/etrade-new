package com.etrade.mobilepro.order.details.api

import com.etrade.mobilepro.orders.api.TransactionType

interface OrderLegItemInterface : OrderDetailItemInterface {
    val displaySymbol: String
    val displayQuantity: String
    val transactionType: TransactionType
}
