package com.etrade.mobilepro.chartiq.usecase

import com.etrade.mobilepro.chartiq.api.model.OrderDetails
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem.OrderDetailsItem
import com.etrade.mobilepro.order.details.api.OrderData
import com.etrade.mobilepro.order.details.api.OrderDetailsRepo
import com.etrade.mobilepro.order.details.api.OrderDetailsRequestParams
import com.etrade.mobilepro.orders.api.OrderStatus
import com.etrade.mobilepro.orders.api.TradeOrderType
import com.etrade.mobilepro.orders.extensions.applyOrderDetailValues
import com.etrade.mobilepro.orders.extensions.getDisabledInputFields
import com.etrade.mobilepro.orders.extensions.getSecurityType
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormInputIdMap
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.util.UseCase
import kotlinx.coroutines.rx2.awaitLast
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Provider

interface EditOrderDataUseCase : UseCase<OrderDetails, ETResult<OrderData>>

class EditOrderDataUseCaseImpl @Inject constructor(
    private val orderDetailsRepo: OrderDetailsRepo,
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    private val tradeFormInputIdMap: TradeFormInputIdMap
) : EditOrderDataUseCase {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    override suspend fun execute(parameter: OrderDetails): ETResult<OrderData> {
        val requestParams = OrderDetailsRequestParams(parameter.accountId, parameter.orderNumber, OrderStatus.ALL)
        return orderDetailsRepo.getOrderDetails(requestParams)
            .runCatchingET { awaitLast() }
            .map {
                val orderDetailsItem = it[0] as OrderDetailsItem
                val securityType = orderDetailsItem.getSecurityType(parameter.instrumentType)
                val formParameters = createFormParameters(parameter, orderDetailsItem, securityType)
                val disabledFields = tradeFormInputIdMap.getDisabledInputFields(orderDetailsItem.isOpen(), securityType, formParameters)
                OrderData(formParameters, disabledFields, TradeOrderType.EDIT_OPEN)
            }
            .onFailure {
                logger.error("Unable to build order data", it)
            }
    }

    private fun createFormParameters(details: OrderDetails, item: OrderDetailsItem, securityType: SecurityType): InputValuesSnapshot {
        return tradeFormParametersBuilderProvider.get().apply {
            accountId = details.accountId
            symbol = object : WithSymbolInfo {
                override val symbol = details.symbol
                override val instrumentType = details.instrumentType
            }
            orderId = details.orderNumber
            originalQuantity = item.quantity
            applyOrderDetailValues(item)
        }.create(securityType)
    }

    private fun OrderDetailsItem.isOpen(): Boolean {
        return orderStatus == OrderStatus.OPEN || orderStatus == OrderStatus.OPEN_AND_UNSETTLED
    }
}
