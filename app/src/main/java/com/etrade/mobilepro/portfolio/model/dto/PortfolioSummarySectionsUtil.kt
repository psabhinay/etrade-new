package com.etrade.mobilepro.portfolio.model.dto

import com.etrade.completeview.dto.AccountSummarySectionDto
import com.etrade.mobilepro.backends.mgs.CallToActionDto
import com.etrade.mobilepro.backends.mgs.ClickActionDto
import com.etrade.mobilepro.completeview.toCallToAction
import com.etrade.mobilepro.portfolio.model.PortfolioCtaList

private const val SAVED_ORDER_TAG = "Saved Order"
private const val OPEN_ORDER_TAG = "Open Order"

private val portfolioDrawerViewCtas = hashSetOf("Performance", "News", "DRIP")

internal fun List<AccountSummarySectionDto>.toPortfolioSummarySections(): List<ParcelablePortfolioSummaryItemDto> {
    return map {
        ParcelablePortfolioSummaryItemDto(
            title = it.title,
            movementType = it.value.movementType,
            streamId = it.value.streamId,
            initialValue = it.value.initial
        )
    }.reversed()
}

private fun CallToActionDto.toCallToActionParcelable(): CallToActionParcelable =
    CallToActionParcelable(
        label,
        clickActionDto.toClickActionParcelable()
    )

internal fun ClickActionDto.toClickActionParcelable(): ClickActionParcelable =
    ClickActionParcelable(
        webUrl = toWebDeepLink(),
        webViewUrl = toWebViewDeepLink(),
        appUrl = appUrl
    )

internal fun List<CallToActionDto>?.getExtendPortfolioSummaryCta(): PortfolioCtaList {
    val headerViewCtaList = mutableListOf<CallToActionDto>()
    val drawerViewCtaList = mutableListOf<CallToActionParcelable>()
    this?.forEach {
        if (it.isHeaderViewCta()) {
            headerViewCtaList.add(it)
        } else {
            drawerViewCtaList.addDrawerViewCta(it)
        }
    }
    return PortfolioCtaList(headerViewCtaList.map { it.toCallToAction() }, drawerViewCtaList)
}

private fun CallToActionDto.isHeaderViewCta() =
    label.startsWith(SAVED_ORDER_TAG) || label.startsWith(OPEN_ORDER_TAG)

private fun MutableList<CallToActionParcelable>.addDrawerViewCta(it: CallToActionDto) {
    portfolioDrawerViewCtas.forEach { ctaLabel ->
        if (it.label.contains(ctaLabel)) {
            add(it.toCallToActionParcelable())
        }
    }
}
