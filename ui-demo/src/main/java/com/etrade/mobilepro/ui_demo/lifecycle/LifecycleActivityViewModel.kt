package com.etrade.mobilepro.ui_demo.lifecycle

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LifecycleActivityViewModel : ViewModel() {

    private val _items = MutableLiveData<List<LifecycleItemViewModel>>(listOf())
    val items
        get() = _items

    fun addItem() {
        _items.postValue(_items.value.orEmpty() + LifecycleItemViewModel())
    }
}
