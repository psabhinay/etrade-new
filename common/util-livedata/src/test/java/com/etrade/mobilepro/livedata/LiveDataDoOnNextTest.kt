package com.etrade.mobilepro.livedata

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.jraska.livedata.test
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

internal class LiveDataDoOnNextTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Test
    fun `doOnNext doesn't receive initial null value`() {
        val liveData = MutableLiveData<Int>()
        var count = 0

        liveData
            .doOnNext { count++ }
            .test()
            .assertNoValue()

        Assert.assertEquals(0, count)
    }

    @Test
    fun `doOnNext receive initial non-null value`() {
        val liveData = MutableLiveData(10)
        var value = 0

        liveData
            .doOnNext { value = it }
            .test()
            .assertHasValue()

        Assert.assertEquals(10, value)
    }

    @Test
    fun `doOnNext receives current value`() {
        val liveData = MutableLiveData<Int>()
        var value = 0

        liveData
            .doOnNext { value = it }
            .test()

        liveData.value = 1

        Assert.assertEquals(1, value)
    }

    @Test
    fun `doOnNext receives latest value`() {
        val liveData = MutableLiveData<Int>()
        var value = 0

        val testFlow = liveData
            .doOnNext { value = it }
            .test()

        liveData.value = 1

        liveData.value = 2

        testFlow.assertHistorySize(2)
        Assert.assertEquals(2, value)
    }

    @Test
    fun `doOnNext doesn't receive null`() {
        val liveData = MutableLiveData<Int>()
        var value = 0

        val testFlow = liveData
            .doOnNext { value = it }
            .test()

        liveData.value = 1

        liveData.value = null

        testFlow.assertHistorySize(2)
        Assert.assertEquals(1, value)
    }
}
