package com.etrade.mobilepro.accountvalues.completeview

import com.etrade.eo.streaming.UpdateInfo
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult.CalculatedAccountValuesVO
import com.etrade.mobilepro.accountvalues.pnl.calculator.CalculationResult.EmptyCalculationResult
import com.etrade.mobilepro.util.domain.data.deriveDaysGainPercentage
import com.etrade.mobilepro.util.safeParseBigDecimal
import java.math.BigDecimal

fun UpdateInfo.toCalculationResult(): CalculationResult {
    val net = getValue(AccountValuesFields.NET_ACCOUNT_VALUE) ?: return EmptyCalculationResult
    val daysGain = getValue(AccountValuesFields.DAYS_GAIN_DOLLAR) ?: return EmptyCalculationResult
    val totalGain = getValue(AccountValuesFields.TOTAL_GAIN_DOLLAR) ?: return EmptyCalculationResult
    val totalGainPercent = getValue(AccountValuesFields.TOTAL_GAIN_PERCENT) ?: return EmptyCalculationResult

    return CalculatedAccountValuesVO(
        netLiquidationValue = net,
        plDay = daysGain,
        plDayPercent = calculateDaysGainPercent(daysGain, net),
        totalGain = totalGain,
        totalGainPercent = totalGainPercent
    )
}

private fun UpdateInfo.getValue(field: AccountValuesFields): BigDecimal? =
    runCatching { getNewValue(field.field) }
        .getOrNull()
        ?.safeParseBigDecimal()

private fun calculateDaysGainPercent(daysGain: BigDecimal?, net: BigDecimal?): BigDecimal? {
    return if (daysGain != null && net != null) {
        deriveDaysGainPercentage(daysGain, net)
    } else {
        null
    }
}
