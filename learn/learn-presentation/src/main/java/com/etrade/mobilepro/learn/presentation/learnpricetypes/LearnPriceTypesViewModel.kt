package com.etrade.mobilepro.learn.presentation.learnpricetypes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject
import javax.inject.Provider

class LearnPriceTypesViewModel @Inject constructor(
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>
) : ViewModel() {
    val applyFormParametersSignal: LiveData<ConsumableLiveEvent<InputValuesSnapshot>>
        get() = _applyFormParametersSignal
    private val _applyFormParametersSignal: MutableLiveData<ConsumableLiveEvent<InputValuesSnapshot>> =
        MutableLiveData()

    fun marketOrLimitButtonPress(chosenPriceType: PriceType, securityType: SecurityType) {
        _applyFormParametersSignal.value = ConsumableLiveEvent(
            tradeFormParametersBuilderProvider.get()
                .apply {
                    priceType = chosenPriceType
                }
                .create(securityType)
        )
    }
}
