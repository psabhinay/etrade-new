package com.etrade.mobilepro.dagger

import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.dagger.qualifiers.AmericanDateOnly
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.transfermoney.presentation.TransferMoneyContainerFragment
import com.etrade.mobilepro.transfermoney.presentation.activity.TransferActivityFragment
import com.etrade.mobilepro.transfermoney.presentation.confirmation.TransferMoneyConfirmationFragment
import com.etrade.mobilepro.transfermoney.presentation.contributiondetail.TransferIraContributionDetailFragment
import com.etrade.mobilepro.transfermoney.presentation.detail.TransferActivityDetailFragment
import com.etrade.mobilepro.transfermoney.presentation.preview.TransferMoneyPreviewFragment
import com.etrade.mobilepro.transfermoney.presentation.transfer.AddExternalAccountFragment
import com.etrade.mobilepro.transfermoney.presentation.transfer.TransferFragment
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import org.threeten.bp.format.DateTimeFormatter

@Module
interface TransferMoneyFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(AddExternalAccountFragment::class)
    fun bindAddExternalAccountFragment(fragment: AddExternalAccountFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransferFragment::class)
    fun bindTransferFragment(fragment: TransferFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransferActivityFragment::class)
    fun bindTransferMoneyActivityFragment(fragment: TransferActivityFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(com.etrade.mobilepro.transfermoney.widget.TransferMoneyCalendarDialogFragment::class)
    fun bindTransferMoneyCalendarDialogFragment_fromWidgetModule(
        fragment: com.etrade.mobilepro.transfermoney.widget.TransferMoneyCalendarDialogFragment
    ): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransferMoneyContainerFragment::class)
    fun bindTransferMoneyContainerFragment(fragment: TransferMoneyContainerFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransferMoneyPreviewFragment::class)
    fun bindTransferMoneyPreviewFragment(fragment: TransferMoneyPreviewFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransferMoneyConfirmationFragment::class)
    fun bindTransferMoneyConfirmationFragment(fragment: TransferMoneyConfirmationFragment): Fragment

    @Binds
    @IntoMap
    @FragmentKey(TransferActivityDetailFragment::class)
    fun bindTransferActivityDetailFragment(fragment: TransferActivityDetailFragment): Fragment

    companion object {

        @Provides
        @IntoMap
        @FragmentKey(TransferIraContributionDetailFragment::class)
        fun provideTransferIraContributionDetailFragment(
            viewModelFactory: ViewModelProvider.Factory,
            snackbarFactry: SnackbarUtilFactory,
            @AmericanDateOnly dateFormatter: DateTimeFormatter
        ): Fragment = TransferIraContributionDetailFragment(
            viewModelProvider = viewModelFactory,
            snackbarUtilFactory = snackbarFactry,
            deadlineDateFormatter = dateFormatter
        )
    }
}
