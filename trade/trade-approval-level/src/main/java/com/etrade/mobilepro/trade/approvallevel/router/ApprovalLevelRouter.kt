package com.etrade.mobilepro.trade.approvallevel.router

import androidx.navigation.NavController
import com.etrade.mobilepro.trade.api.AccountMode

interface ApprovalLevelRouter {
    fun navigateToApprovalLevelFragment(navController: NavController, accountMode: AccountMode)
    fun navigateToNewMessage(navController: NavController)
}
