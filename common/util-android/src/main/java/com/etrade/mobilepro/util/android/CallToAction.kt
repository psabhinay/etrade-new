package com.etrade.mobilepro.util.android

import android.content.Intent
import android.net.Uri
import android.view.View
import com.etrade.mobilepro.util.ClickAction
import org.slf4j.Logger
import org.slf4j.LoggerFactory

private val logger: Logger = LoggerFactory.getLogger(CallToAction::class.java)

class CallToAction(val label: String, val action: ClickAction, val isEnabled: Boolean = true) {

    fun onClick(view: View) {
        action.appUrl?.let {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
            logger.debug("Sending intent: $intent")
            view.context.startActivity(intent)
        }
    }
}
