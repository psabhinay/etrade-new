package com.etrade.mobilepro.util

import java.util.Random

fun ClosedRange<Char>.randomString(length: Int) =
    (1..length)
        .map { (Random().nextInt(endInclusive.code - start.code) + start.code).toChar() }
        .joinToString("")
