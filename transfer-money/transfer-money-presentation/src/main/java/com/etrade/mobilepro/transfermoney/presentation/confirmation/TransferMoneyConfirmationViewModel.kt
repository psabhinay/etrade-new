package com.etrade.mobilepro.transfermoney.presentation.confirmation

import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.tracking.SCREEN_TITLE_TRANSFER_MONEY_CONFIRMATION
import com.etrade.mobilepro.tracking.ScreenViewModel
import javax.inject.Inject

class TransferMoneyConfirmationViewModel @Inject constructor() : ViewModel(), ScreenViewModel {

    override val screenName: String? = SCREEN_TITLE_TRANSFER_MONEY_CONFIRMATION
}
