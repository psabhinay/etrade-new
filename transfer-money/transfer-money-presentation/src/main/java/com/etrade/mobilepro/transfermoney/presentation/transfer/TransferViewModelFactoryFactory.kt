package com.etrade.mobilepro.transfermoney.presentation.transfer

import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.util.android.viewmodel.viewModelFactory
import javax.inject.Inject

interface TransferViewModelFactoryFactory {
    fun create(
        preFilledFromAccount: String?,
        preFilledAmount: String?
    ): ViewModelProvider.Factory
}

class TransferViewModelFactoryFactoryImpl @Inject constructor(
    private val vmFactory: TransferViewModel.Factory
) : TransferViewModelFactoryFactory {
    override fun create(preFilledFromAccount: String?, preFilledAmount: String?): ViewModelProvider.Factory {
        return viewModelFactory {
            vmFactory.create(preFilledFromAccount, preFilledAmount)
        }
    }
}
