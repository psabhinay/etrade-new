package com.etrade.mobilepro.quoteapi

import androidx.annotation.ColorRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.livedata.DistinctLiveData

data class FundsTradeSummaryItem(
    val annualAvgReturn: String,
    val change: String,
    val deferredSalesCharge: List<FundsFee>? = null,
    val etradeEarlyRedemptionFee: String,
    val frontEndSalesCharge: List<FundsFee>? = null,
    val frontLoadText: String?,
    val fundRedemptionFee: String,
    val fundName: String,
    val lastTradeTime: String,
    val initialInvestment: String,
    val instrumentType: InstrumentType,
    val iraInitialInvestment: String?,
    val iraSubsequentInvestment: String?,
    val netAssetValue: String,
    val ordCutOffTime: String,
    val positions: List<MutualFundPosition>,
    val salesCharge: String,
    val salesChargeType: SalesChargeType,
    val sevenDayCurrentYield: String,
    val subsequentInvestment: String,
    val transactionFee: String,
    val weightedAvgMaturity: String,
    val isFundOwned: Boolean
)

enum class MutualPositionType {
    CASH,
    MARGIN
}

enum class SalesChargeType {
    FRONT_END,
    FRONT_END_LOAD,
    BACK_END,
    NONE
}

data class FundsFee(
    val percent: String,
    val range: String
)

@Suppress("LongParameterList")
class FundsTradeTitleViewItem constructor(
    label: String,
    netAssetValue: String,
    @ColorRes netAssetValueColor: Int,
    changeVal: QuoteChangeWidgetItem,
    @ColorRes changeColorVal: Int,
    timeStampLabel: String,
    priceFormat: String,
    priceWithoutSizeFormat: String,
    changeFormat: String,
    contentDescriptionFormat: String,
    override val isAffiliatedProduct: Boolean
) : QuoteHeaderViewItem {

    override val title: String = label

    override val titleContentDescription: String
        get() = title

    override val lastPrice: LiveData<QuotePriceWidgetItem> = MutableLiveData(QuotePriceWidgetItem(price = netAssetValue, priceColor = netAssetValueColor))

    override val change: LiveData<QuoteChangeWidgetItem> = MutableLiveData(changeVal)

    override val changeColor: LiveData<Int> = MutableLiveData(changeColorVal)

    override val volume: LiveData<String>? = null

    override val asOfDateTime: LiveData<String> = MutableLiveData(timeStampLabel)

    override val contentDescription: LiveData<String?>
        get() = _contentDescription

    private val _contentDescription: MutableLiveData<String?> = DistinctLiveData()

    init {
        updateContentDescription(priceFormat, priceWithoutSizeFormat, changeFormat, contentDescriptionFormat, _contentDescription)
    }
}

sealed class MutualFundsTradeQuotesResult

object InvalidMutualFunds : MutualFundsTradeQuotesResult()

class NonSupportedFunds(val message: String) : MutualFundsTradeQuotesResult()

data class FundsTradeQuoteItem(
    val symbol: String,
    val instrumentType: InstrumentType,
    val hasEarlyRedemptionFee: Boolean,
    val isFrontEndOrSalesFeeFund: Boolean,
    val isNoLoadNoTransactionFeeFund: Boolean,
    val isLoadWaived: Boolean,
    val headerItem: FundsTradeTitleViewItem,
    val summaryItem: FundsTradeSummaryItem
) : MutualFundsTradeQuotesResult()
