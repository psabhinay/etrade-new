package com.etrade.mobilepro.screener.presentation.stock.pricevolume

import com.etrade.mobilepro.screener.api.filter.stock.PriceVolumeFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class PricePerformanceSP500 : ScreenerFilterEvent<PriceVolumeFilter, PriceVolumeViewModel.ViewState, Boolean> {
    LESS_THAN_MINUS_40 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceSpLessThanMinus40 = eventValue))
    },
    MINUS_40_TO_MINUS_20 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceSpMinus40toMinus20 = eventValue))
    },
    MINUS_20_TO_0 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceSpMinus20to0 = eventValue))
    },
    ZERO_TO_20 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceSpZeroTo20 = eventValue))
    },
    TWENTY_TO_40 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceSp20to40 = eventValue))
    },
    GREATER_THAN_40 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceSpGreaterThan40 = eventValue))
    };

    override fun conditionalUpdate(
        viewState: PriceVolumeViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<PriceVolumeViewModel.ViewState> {
        var newState = update(viewState, eventValue)
        if (!newState.filter.isPricePerformanceSpValuesSelected()) {
            newState = uncheckPeriodButtons(newState)
        }
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = (!newState.filter.isEmpty() && !newState.filter.isPricePerformanceSpOverSelected()) ||
                (newState.filter.isEmpty() && viewState.filter.isNotEmpty())
        )
    }

    private fun uncheckPeriodButtons(viewState: PriceVolumeViewModel.ViewState): PriceVolumeViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                pricePerformanceSpOver4Weeks = false,
                pricePerformanceSpOver13Weeks = false,
                pricePerformanceSpOver52Weeks = false
            )
        )
}

internal enum class PricePerformanceIndustry : ScreenerFilterEvent<PriceVolumeFilter, PriceVolumeViewModel.ViewState, Boolean> {
    LESS_THAN_MINUS_40 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceIndustryLessThanMinus40 = eventValue))
    },
    MINUS_40_TO_MINUS_20 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceIndustryMinus40toMinus20 = eventValue))
    },
    MINUS_20_TO_0 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceIndustryMinus20to0 = eventValue))
    },
    ZERO_TO_20 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceIndustryZeroTo20 = eventValue))
    },
    TWENTY_TO_40 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceIndustry20to40 = eventValue))
    },
    GREATER_THAN_40 {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceIndustryGreaterThan40 = eventValue))
    };

    override fun conditionalUpdate(
        viewState: PriceVolumeViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<PriceVolumeViewModel.ViewState> {
        var newState = update(viewState, eventValue)
        if (!newState.filter.isPricePerformanceIndustryValuesSelected()) {
            newState = uncheckPeriodButtons(newState)
        }
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = (!newState.filter.isEmpty() && !newState.filter.isPricePerformanceIndustryOverSelected()) ||
                (newState.filter.isEmpty() && viewState.filter.isNotEmpty())
        )
    }

    private fun uncheckPeriodButtons(viewState: PriceVolumeViewModel.ViewState): PriceVolumeViewModel.ViewState =
        viewState.copy(
            filter = viewState.filter.copy(
                pricePerformanceIndustryOver4Weeks = false,
                pricePerformanceIndustryOver13Weeks = false,
                pricePerformanceIndustryOver52Weeks = false
            )
        )
}

internal enum class OverLastSP500 : ScreenerFilterEvent<PriceVolumeFilter, PriceVolumeViewModel.ViewState, Boolean> {
    FOUR_WEEKS {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceSpOver4Weeks = eventValue && viewState.filter.isPricePerformanceSpValuesSelected()))
    },
    THIRTEEN_WEEKS {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceSpOver13Weeks = eventValue && viewState.filter.isPricePerformanceSpValuesSelected()))
    },
    FIFTY_TWO_WEEKS {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(pricePerformanceSpOver52Weeks = eventValue && viewState.filter.isPricePerformanceSpValuesSelected()))
    };

    override fun conditionalUpdate(
        viewState: PriceVolumeViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<PriceVolumeViewModel.ViewState> {
        val newState = update(viewState, eventValue)
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = viewState.filter.isPricePerformanceSpValuesSelected() && newState.filter.isPricePerformanceSpOverSelected()
        )
    }
}

internal enum class OverLastIndustry : ScreenerFilterEvent<PriceVolumeFilter, PriceVolumeViewModel.ViewState, Boolean> {
    FOUR_WEEKS {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(pricePerformanceIndustryOver4Weeks = eventValue && viewState.filter.isPricePerformanceIndustryValuesSelected())
            )
    },
    THIRTEEN_WEEKS {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(pricePerformanceIndustryOver13Weeks = eventValue && viewState.filter.isPricePerformanceIndustryValuesSelected())
            )
    },
    FIFTY_TWO_WEEKS {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: Boolean): PriceVolumeViewModel.ViewState =
            viewState.copy(
                filter = viewState.filter.copy(pricePerformanceIndustryOver52Weeks = eventValue && viewState.filter.isPricePerformanceIndustryValuesSelected())
            )
    };

    override fun conditionalUpdate(
        viewState: PriceVolumeViewModel.ViewState,
        eventValue: Boolean
    ): ScreenerFilterEvent.Result<PriceVolumeViewModel.ViewState> {
        val newState = update(viewState, eventValue)
        return ScreenerFilterEvent.Result(
            value = newState,
            shouldTriggerRequest = viewState.filter.isPricePerformanceIndustryValuesSelected() && newState.filter.isPricePerformanceIndustryOverSelected()
        )
    }
}

internal enum class SharePrice : ScreenerFilterEvent<PriceVolumeFilter, PriceVolumeViewModel.ViewState, String> {
    GREATER_THAN {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: String): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(sharePriceGreaterThan = eventValue))
    },
    LESS_THAN {
        override fun update(viewState: PriceVolumeViewModel.ViewState, eventValue: String): PriceVolumeViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(sharePriceLessThan = eventValue))
    }
}
