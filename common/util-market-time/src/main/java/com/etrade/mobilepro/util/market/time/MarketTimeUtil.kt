package com.etrade.mobilepro.util.market.time

import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZonedDateTime

fun getCurrentTimeNewYork(): ZonedDateTime {
    val localDateTime = LocalDateTime.now(NEW_YORK_ZONE_ID)
    return localDateTime.atZone(NEW_YORK_ZONE_ID)
}

fun getCurrentTimeInMillis(): Long {
    return getCurrentTimeNewYork().toInstant().toEpochMilli()
}

fun isMarketDay(): Boolean {
    val zonedDateTime = getCurrentTimeNewYork()
    return !(zonedDateTime.dayOfWeek == DayOfWeek.SATURDAY || zonedDateTime.dayOfWeek == DayOfWeek.SUNDAY)
}

@SuppressWarnings("MagicNumber")
fun marketOpenTime(): ZonedDateTime {
    return getCurrentTimeNewYork().withHour(9).withMinute(30).withSecond(0)
}

@SuppressWarnings("MagicNumber")
fun marketClosedTime(): ZonedDateTime {
    return getCurrentTimeNewYork().withHour(16).withMinute(0).withSecond(0)
}

fun isPreMarketTime(): Boolean {
    return isMarketDay() && getCurrentTimeNewYork().isBefore(marketOpenTime())
}

fun isMarketTime(): Boolean {
    // Should be using market schedule component instead.
    val currentTime = getCurrentTimeNewYork()
    return (
        isMarketDay() && !currentTime.isBefore(marketOpenTime()) &&
            !currentTime.isAfter(marketClosedTime())
        )
}
