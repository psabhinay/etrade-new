package com.etrade.mobilepro.trade.presentation.confirmation

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.trade.presentation.R
import com.etrade.mobilepro.trade.presentation.databinding.TradeItemConfirmationDividerBinding
import com.etrade.mobilepro.trade.presentation.databinding.TradeItemConfirmationOrderBinding
import com.etrade.mobilepro.util.android.extension.viewBinding

internal class OrderConfirmationListAdapter : RecyclerView.Adapter<OrderConfirmationViewHolder>() {

    var values: List<OrderConfirmationItem> = emptyList()
        set(newValue) {
            field = newValue
            notifyDataSetChanged()
        }

    override fun getItemViewType(position: Int): Int {
        return when (values[position]) {
            is OrderConfirmationItem.Divider -> R.layout.trade_item_confirmation_divider
            is OrderConfirmationItem.Order -> R.layout.trade_item_confirmation_order
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderConfirmationViewHolder {
        return when (viewType) {
            R.layout.trade_item_confirmation_divider -> OrderConfirmationViewHolder.DividerViewHolder.from(parent)
            R.layout.trade_item_confirmation_order -> OrderConfirmationViewHolder.OrderViewHolder.from(parent)
            else -> throw UnsupportedOperationException("View type is not supported: $viewType")
        }
    }

    override fun getItemCount(): Int = values.size

    override fun onBindViewHolder(holder: OrderConfirmationViewHolder, position: Int) {
        holder.bind(values[position])
    }
}

internal sealed class OrderConfirmationViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun bind(item: OrderConfirmationItem)

    class DividerViewHolder(private val binding: TradeItemConfirmationDividerBinding) :
        OrderConfirmationViewHolder(binding.root) {
        override fun bind(item: OrderConfirmationItem) {
            require(item is OrderConfirmationItem.Divider)
            binding.textView.text = item.label
        }

        companion object {
            fun from(parent: ViewGroup): DividerViewHolder {
                return DividerViewHolder(parent.viewBinding(TradeItemConfirmationDividerBinding::inflate))
            }
        }
    }

    class OrderViewHolder(private val binding: TradeItemConfirmationOrderBinding) :
        OrderConfirmationViewHolder(binding.root) {
        override fun bind(item: OrderConfirmationItem) {
            require(item is OrderConfirmationItem.Order)
            with(item.displayItem) {
                binding.apply {
                    orderActionView.text = label
                    sectionLayoutView.setContent(sections)
                }
            }
        }

        companion object {
            fun from(parent: ViewGroup): OrderViewHolder {
                return OrderViewHolder(parent.viewBinding(TradeItemConfirmationOrderBinding::inflate))
            }
        }
    }
}
