package com.etrade.mobilepro.trade.form.api.value

interface InvestmentTypeValueId {
    val reInvestInFund: String
    val depositInFund: String
}
