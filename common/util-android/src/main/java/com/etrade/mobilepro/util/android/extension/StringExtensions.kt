package com.etrade.mobilepro.util.android.extension

import android.net.Uri

fun String.uriEncode(): String {
    return Uri.encode(this)
}
