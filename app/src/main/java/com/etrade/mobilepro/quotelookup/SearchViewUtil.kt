package com.etrade.mobilepro.quotelookup

import android.app.Activity
import android.app.SearchManager
import android.app.SharedElementCallback
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityOptionsCompat
import com.etrade.mobilepro.R
import com.etrade.mobilepro.searchingapi.SearchAnimationParams
import com.etrade.mobilepro.searchingapi.StartSearchParams

const val REQUEST_CODE_SYMBOL_SEARCH = 1
const val EXTRA_FLAG_FINISH_ON_SEARCH = "extra.flagFinishOnSearch"
const val EXTRA_STOCK_QUOTE_SEARCH_ONLY = "extra.stockQuoteSearchOnly"
const val EXTRA_STOCK_QUOTE_SHOW_OWNED_SYMBOLS = "extra.stockQuoteShowOwnedSymbols"
const val EXTRA_STOCK_QUOTE_SELECTED_ACCOUNT_ID = "extra.stockQuoteSelectedAccountId"
const val EXTRA_QUOTE_LOOKUP_LOCATION = "extra.quoteLookupLocation"

/**
 * returns a pair of [Intent] and [Bundle] used to start [QuoteLookupActivity]
 * with scene animation of toolbar's search view.
 *
 * @param activity activity context
 * @param params search parameters
 */
fun Toolbar.getSearchIntentAndOptions(activity: Activity, params: StartSearchParams): Pair<Intent, ActivityOptionsCompat?> {
    return activity.createSearchIntent(params) to params.animationParams?.run { activity.createActivityOptions(this, findViewById(sharedViewId)) }
}

fun Context.createSearchIntent(params: StartSearchParams, reorderToFront: Boolean = true): Intent {
    return Intent(this, QuoteLookupActivity::class.java).apply {
        if (reorderToFront) {
            addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        }
        putExtra(SearchManager.QUERY, params.query)
        putExtra(EXTRA_FLAG_FINISH_ON_SEARCH, params.finishOnSearch)
        putExtra(EXTRA_STOCK_QUOTE_SEARCH_ONLY, params.stockQuoteSearchOnly)
        putExtra(EXTRA_STOCK_QUOTE_SHOW_OWNED_SYMBOLS, params.showOwnedSymbols)
        putExtra(EXTRA_STOCK_QUOTE_SELECTED_ACCOUNT_ID, params.selectedAccountId)
        putExtra(EXTRA_QUOTE_LOOKUP_LOCATION, params.quoteLookupLocation)
    }
}

private fun Activity.createActivityOptions(params: SearchAnimationParams, searchMenuView: View?): ActivityOptionsCompat? {
    return if (params.animated) {
        // if this is null, we should have searchActivity already opened, i.e. after conf. change
        searchMenuView?.let {
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, searchMenuView, getString(R.string.transition_search_back))
            this.setExitSharedElementCallback(object : SharedElementCallback() {
                override fun onSharedElementEnd(
                    sharedElementNames: MutableList<String>?,
                    sharedElements: MutableList<View>?,
                    sharedElementSnapshots: MutableList<View>?
                ) {
                    super.onSharedElementEnd(sharedElementNames, sharedElements, sharedElementSnapshots)
                    it.alpha = 1f
                }
            })
            return@let options
        }
    } else {
        null
    }
}
