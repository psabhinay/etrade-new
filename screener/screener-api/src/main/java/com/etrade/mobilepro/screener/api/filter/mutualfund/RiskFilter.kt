package com.etrade.mobilepro.screener.api.filter.mutualfund

import com.etrade.mobilepro.screener.api.ScreenerFilter
import com.etrade.mobilepro.screener.api.ScreenerType

data class RiskFilter(
    val lessVolatile: Boolean = false,
    val asVolatile: Boolean = false,
    val moreVolatile: Boolean = false,

    val bottom20: Boolean = false,
    val belowMid20: Boolean = false,
    val mid20: Boolean = false,
    val aboveMid20: Boolean = false,
    val top20: Boolean = false
) : ScreenerFilter {
    fun isVolatileSelected(): Boolean = lessVolatile || asVolatile || moreVolatile

    fun isRiskValuesSelected(): Boolean = bottom20 || belowMid20 ||
        mid20 || aboveMid20 || top20

    override fun isEmpty(): Boolean =
        !isVolatileSelected() && !isRiskValuesSelected()

    override fun belongsTo(): ScreenerType.Group = ScreenerType.Group.MUTUAL_FUND
}
