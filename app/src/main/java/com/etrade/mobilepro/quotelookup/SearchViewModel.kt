package com.etrade.mobilepro.quotelookup

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.R
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.positions.api.PortfolioEntryRepo
import com.etrade.mobilepro.positions.api.PortfolioPosition
import com.etrade.mobilepro.searchingapi.SearchInteractor
import com.etrade.mobilepro.searchingapi.SelectionHistoryRepo
import com.etrade.mobilepro.searchingapi.SymbolSearchRepo
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.tracking.SCREEN_TITLE_QUOTE_LOOKUP
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.params.QuoteLookupAnalyticsParams
import com.etrade.mobilepro.tracking.screen
import com.etrade.mobilepro.util.NetworkState
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.safeLet
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import org.slf4j.LoggerFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val debounceMs: Long = 200
private val logger = LoggerFactory.getLogger(SearchViewModel::class.java)

class SearchViewModel @Inject constructor(
    private val searchRepo: SymbolSearchRepo,
    private val searchInteractor: SearchInteractor,
    private val selectionHistoryRepo: SelectionHistoryRepo,
    private val portfolioEntryRepo: PortfolioEntryRepo,
    private val user: User,
    tracker: Tracker
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    private val querySubject: Subject<SearchQuery> = PublishSubject.create()

    private val showOwnedSymbolsSubject: Subject<Boolean> = BehaviorSubject.create()
    private val locationSubject: Subject<String> = BehaviorSubject.create()
    private var selectedAccountId: String? = null

    private val _headerTitleState = MutableLiveData<HeaderTitleState>()
    val headerTitleState: LiveData<HeaderTitleState>
        get() = _headerTitleState

    private val _networkState = MutableLiveData<NetworkState>(NetworkState.Success)
    val networkState: LiveData<NetworkState>
        get() = _networkState

    private val _stockFundResults = MutableLiveData<List<SearchResultItem>?>()
    val stockFundResults: LiveData<List<SearchResultItem>?>
        get() = _stockFundResults

    private val _optionResults = MutableLiveData<List<SearchResultItem>?>()
    val optionResults: LiveData<List<SearchResultItem>?>
        get() = _optionResults

    private val _currentCallSymbol = MutableLiveData<ConsumableLiveEvent<SearchResultItem.Symbol>>()
    val currentCallSymbol: LiveData<ConsumableLiveEvent<SearchResultItem.Symbol>>
        get() = _currentCallSymbol

    private val _currentPutSymbol = MutableLiveData<ConsumableLiveEvent<SearchResultItem.Symbol>>()
    val currentPutSymbol: LiveData<ConsumableLiveEvent<SearchResultItem.Symbol>>
        get() = _currentPutSymbol

    var queryStr: String? = null
        private set
    var openQuote: Boolean = false
        private set
    var queryType: SearchType? = null
        private set

    val searchStockFundDisclosureResId: Int = R.string.quote_search_stock_fund_disclosure
    val searchOptionsDisclosureResId: Int = R.string.quote_search_options_disclosure

    init {
        tracker.screen(SCREEN_TITLE_QUOTE_LOOKUP)
        compositeDisposable.add(
            locationSubject.subscribeBy { location ->
                tracker.event(QuoteLookupAnalyticsParams(location))
            }
        )

        compositeDisposable.add(
            querySubject
                .debounce(debounceMs, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .switchMap(this::switchSearch)
                .subscribeBy(
                    onNext = {
                        when (it) {
                            is Resource.Failed -> {
                                _networkState.postValue(NetworkState.Failed(it.error ?: SearchException()))
                                _stockFundResults.postValue(it.data ?: emptyList())
                                _optionResults.postValue(it.data ?: emptyList())
                            }
                            is Resource.Success -> {
                                openQuote = false
                                _networkState.postValue(NetworkState.Success)

                                val stockFundResults = it.data?.toMutableList()?.apply {
                                    if (isNotEmpty()) {
                                        add(SearchResultItem.DisclosureText(searchStockFundDisclosureResId))
                                    }
                                }
                                _stockFundResults.postValue(stockFundResults)

                                val optionResults = it.data?.toMutableList()?.apply {
                                    if (isNotEmpty()) {
                                        add(SearchResultItem.DisclosureText(searchOptionsDisclosureResId))
                                    }
                                }
                                _optionResults.postValue(optionResults)
                            }
                            is Resource.Loading -> {
                                _networkState.postValue(NetworkState.Loading)
                                _stockFundResults.postValue(it.data ?: emptyList())
                                _optionResults.postValue(it.data ?: emptyList())
                            }
                        }
                    }
                )
        )
        compositeDisposable.add(
            searchInteractor.searchResultItemAsObservable(SearchType.CALL_OPTION)
                .subscribeBy(
                    onNext = {
                        if (it is SearchResultItem.Symbol) {
                            _currentCallSymbol.value = ConsumableLiveEvent(it)
                            _currentPutSymbol.value = ConsumableLiveEvent(it)
                        }
                    },
                    onError = {
                        logger.debug(it.localizedMessage)
                    }
                )
        )

        compositeDisposable.add(
            searchInteractor.searchResultItemAsObservable(SearchType.PUT_OPTION)
                .subscribeBy(
                    onNext = {
                        if (it is SearchResultItem.Symbol) {
                            _currentCallSymbol.value = ConsumableLiveEvent(it)
                            _currentPutSymbol.value = ConsumableLiveEvent(it)
                        }
                    },
                    onError = {
                        logger.debug(it.localizedMessage)
                    }
                )
        )
    }

    fun configure(
        showOwnedSymbols: Boolean,
        selectedAccountId: String?,
        location: String?
    ) {
        this.selectedAccountId = selectedAccountId
        showOwnedSymbolsSubject.onNext(showOwnedSymbols)
        location?.let { locationSubject.onNext(location) }
    }

    fun search(query: SearchQuery) {
        val trimmed = query.copy(value = query.value.trim())
        queryStr = trimmed.value
        queryType = trimmed.type
        querySubject.onNext(trimmed)
    }

    fun itemSelected(searchType: SearchType, item: SearchResultItem) {
        addItemSelected(item)
        searchInteractor.searchResultItemSelected(searchType, item)
    }

    fun addItemSelected(item: SearchResultItem) {
        selectionHistoryRepo.addToHistory(item)
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    private fun switchSearch(query: SearchQuery): Observable<out Resource<List<SearchResultItem>>> {
        return if (query.value.isNotBlank()) {
            _headerTitleState.postValue(HeaderTitleState.Hidden)
            searchRepo.searchAsObservable(query).onErrorReturn { Resource.Failed(emptyList()) }
        } else {
            showOwnedSymbolsSubject.switchMap { showOwnedSymbols ->
                if (showOwnedSymbols) {
                    _headerTitleState.postValue(HeaderTitleState.Visible(R.string.positions_owned))
                    getOwnedSymbols()
                } else {
                    _headerTitleState.postValue(HeaderTitleState.Visible(R.string.recent_symbols))
                    getPreviousSelections(query)
                }
            }
        }
    }

    @Suppress("TooGenericExceptionCaught")
    private fun getPreviousSelections(searchQuery: SearchQuery): Observable<Resource<List<SearchResultItem>>> {
        val selections = try {
            when (searchQuery.type) {
                SearchType.SYMBOL -> selectionHistoryRepo.symbolSelectionHistory()
                else -> emptyList()
            }
        } catch (exception: Exception) {
            emptyList<SearchResultItem>()
        }
        return Observable.just(Resource.Success(selections))
    }

    private fun getOwnedSymbols(): Observable<out Resource<List<SearchResultItem>>> {
        return safeLet(user.getUserId(), selectedAccountId) { userId, accountId ->
            Observable.just<Resource<List<SearchResultItem>>>(Resource.Loading()).mergeWith(
                portfolioEntryRepo.getPortfolioEntry(userId.toString(), accountId, false).map { entry ->
                    entry.positions.distinctBy { it.symbol }
                        .filter { it.instrumentType == InstrumentType.EQ }
                        .sortedBy { it.symbol }
                        .map { it.toSearchResultItem() }
                }.map<Resource<List<SearchResultItem>>> { Resource.Success(it) }.onErrorReturn { Resource.Failed(emptyList()) }.toObservable()
            )
        } ?: Observable.just(Resource.Failed(emptyList()))
    }

    fun searchSpecificOrPickTheFirst() {
        fun proceedWithSymbol(symbol: SearchResultItem.Symbol) {
            openQuote = true
            val results = listOf(symbol)
            _stockFundResults.postValue(results)
            _optionResults.postValue(results)
        }

        val items = _stockFundResults.value ?: emptyList()
        for (searchResultItem in items) {
            (searchResultItem as? SearchResultItem.Symbol)?.let {
                if (it.title.equals(queryStr, ignoreCase = true)) {
                    proceedWithSymbol(it)
                    return
                }
            }
        }
        (items.firstOrNull() as? SearchResultItem.Symbol)?.let {
            proceedWithSymbol(it)
            return
        }
        _stockFundResults.postValue(emptyList())
        _optionResults.postValue(emptyList())
    }
}

sealed class HeaderTitleState {
    data class Visible(@StringRes val resId: Int) : HeaderTitleState()
    object Hidden : HeaderTitleState()
}

private fun PortfolioPosition.toSearchResultItem(): SearchResultItem.Symbol =
    SearchResultItem.Symbol(
        title = symbol,
        type = instrumentType,
        displaySymbol = displaySymbol,
        description = symbolDescription
    )
