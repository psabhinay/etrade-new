package com.etrade.mobilepro.chat.api.setup.models

data class ChatProductItemsModel(val items: List<ChatProductItemModel>)

data class ChatProductItemModel(val id: String, val lookupName: String? = null)
