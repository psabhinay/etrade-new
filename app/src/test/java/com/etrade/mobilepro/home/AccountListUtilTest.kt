package com.etrade.mobilepro.home

import com.etrade.mobilepro.accounts.util.android.view.AccountViewType
import com.etrade.mobilepro.accounts.util.android.view.AccountsSpinnerModel
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class AccountListUtilTest {

    @Test
    fun `check divider position when no accounts`() {
        assertNull(emptyList<AccountsSpinnerModel>().dividerPosition())
    }

    @Test
    fun `check divider position when all accounts present`() {
        assertEquals(
            0,
            listOf(
                AccountsSpinnerModel("", AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW),
                AccountsSpinnerModel("", AccountViewType.SINGLE_BROKERAGE_ACCOUNT_VIEW)
            ).dividerPosition()
        )
    }

    @Test
    fun `check divider position when all accounts present with no other accounts`() {
        assertNull(listOf(AccountsSpinnerModel("", AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW)).dividerPosition())
    }

    @Test
    fun `check divider position when all brokerage present`() {
        assertEquals(
            0,
            listOf(
                AccountsSpinnerModel("", AccountViewType.ALL_BROKERAGE_ACCOUNT_VIEW),
                AccountsSpinnerModel("", AccountViewType.SINGLE_BROKERAGE_ACCOUNT_VIEW)
            ).dividerPosition()
        )
    }

    @Test
    fun `check divider position when all brokerage present with no other accounts`() {
        assertNull(listOf(AccountsSpinnerModel("", AccountViewType.ALL_BROKERAGE_ACCOUNT_VIEW)).dividerPosition())
    }

    @Test
    fun `check divider position when all accounts and all brokerage are present`() {
        assertEquals(
            1,
            listOf(
                AccountsSpinnerModel("", AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW),
                AccountsSpinnerModel("", AccountViewType.ALL_BROKERAGE_ACCOUNT_VIEW),
                AccountsSpinnerModel("", AccountViewType.SINGLE_BROKERAGE_ACCOUNT_VIEW)
            ).dividerPosition()
        )
    }

    @Test
    fun `check divider position when all accounts and all brokerage are present with no other accounts`() {
        assertNull(
            listOf(
                AccountsSpinnerModel("", AccountViewType.COMPLETE_MULTIPLE_ACCOUNT_VIEW),
                AccountsSpinnerModel("", AccountViewType.ALL_BROKERAGE_ACCOUNT_VIEW)
            ).dividerPosition()
        )
    }
}
