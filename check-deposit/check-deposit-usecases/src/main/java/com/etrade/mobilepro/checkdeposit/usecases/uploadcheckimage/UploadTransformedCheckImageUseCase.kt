package com.etrade.mobilepro.checkdeposit.usecases.uploadcheckimage

import com.etrade.mobilepro.checkdeposit.deposit.service.RemoteCheckDepositService
import com.etrade.mobilepro.checkdeposit.deposit.service.UploadImageDto
import com.etrade.mobilepro.checkdeposit.deposit.service.UploadImageValueDto
import com.etrade.mobilepro.checkdeposit.usecases.api.ImageUploadResult
import com.etrade.mobilepro.checkdeposit.usecases.api.UploadCheckImage
import com.etrade.mobilepro.checkdeposit.usecases.api.UploadCheckImageUseCase
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import org.slf4j.LoggerFactory
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

private val logger = LoggerFactory.getLogger(UploadTransformedCheckImageUseCase::class.java)

class UploadTransformedCheckImageUseCase(
    private val checkDepositService: RemoteCheckDepositService,
    private val encodeToBase64: (ByteArray) -> String,
    private val prettyPrintByteSize: (Int) -> String,
    private val checkBytesTransform: (ByteArray) -> ByteArray
) : UploadCheckImageUseCase {

    @ExperimentalTime
    override suspend fun execute(parameter: UploadCheckImage): ETResult<ImageUploadResult> = runCatchingET {

        val (finalCheckImage, transformDuration) = measureTimedValue {
            checkBytesTransform(parameter.checkImage)
        }

        logger.debug("Transforming check image took $transformDuration resulting in image size ${prettyPrintByteSize(finalCheckImage.size)}")

        val response = checkDepositService.uploadImage(
            UploadImageDto(
                UploadImageValueDto(
                    imageFace = parameter.checkSide,
                    instNo = parameter.instNo,
                    amount = parameter.depositAmount,
                    accountNo = parameter.accountNumber,
                    depositImage = encodeToBase64(finalCheckImage)
                )
            )
        )

        response.doIfSuccessfulOrThrow {
            UploadResult(
                tokenId = requireNotNull(it.tokenId) { "Missing token in check upload response" }
            )
        }
    }
}
