package com.etrade.mobilepro.trade.mutualfund.data

import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.api.ServerResponseException
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.trade.mutualfund.api.MutualFundEntry
import com.etrade.mobilepro.trade.mutualfund.api.MutualFundResult
import com.etrade.mobilepro.trade.mutualfund.api.MutualFundTradeRepo
import retrofit2.Retrofit
import javax.inject.Inject

class DefaultMutualFundTradeRepo @Inject constructor(
    @MobileTrade retrofit: Retrofit
) : MutualFundTradeRepo {

    private val apiClient: MutualFundTradingApiClient = retrofit.create(MutualFundTradingApiClient::class.java)

    override suspend fun getMutualFundSellEntry(accountId: String): ETResult<List<MutualFundEntry>> {
        return runCatchingET {
            apiClient.mutualFundSellEntry(MutualFundFundsRequestDto.toMutualFundSellRequest(accountId = accountId, orderAction = "2"))
                .doIfSuccessfulOrThrow {
                    it.fundsOwned?.funds?.map(MutualFundFundsResponseDto.Fund::toMutualFundEntry) ?: emptyList()
                }
        }
    }

    override suspend fun getMutualFundExchangeSellEntry(accountId: String): ETResult<List<MutualFundEntry>> {
        return runCatchingET {
            apiClient.mutualFundExchangeSellEntry(MutualFundFundsRequestDto.toMutualFundSellRequest(accountId = accountId, orderAction = "5"))
                .doIfSuccessfulOrThrow {
                    it.fundsOwned?.funds?.map(MutualFundFundsResponseDto.Fund::toMutualFundEntry) ?: emptyList()
                }
        }
    }

    override suspend fun getMutualFundExchangeBuyEntry(accountId: String, symbol: WithSymbolInfo): ETResult<MutualFundResult> = runCatchingET {
        val responseDto = apiClient.mutualFundExchangeBuyEntry(
            MutualFundFundsRequestDto.toMutualFundSellRequest(
                accountId = accountId,
                orderAction = "5",
                symbol = symbol.symbol,
                typeCode = symbol.instrumentType.typeCode
            )
        )
        when (val result = responseDto.extractError()) {
            ServerError.NoError -> {
                MutualFundResult(entryList = responseDto.data?.fundFamilyList?.funds?.map(MutualFundFundsResponseDto.Fund::toMutualFundEntry) ?: emptyList())
            }
            else -> throw ServerResponseException(result)
        }
    }
}

private fun MutualFundFundsResponseDto.Fund.toMutualFundEntry() =
    MutualFundEntry(securityName = this.securityName, symbol = this.symbol, instrumentType = InstrumentType.from(this.typeCode))
