package com.etrade.mobilepro.trade.api.advanced

interface ContingentOrderResponse {
    val contingentOrderDetails: List<ContingentOrderDetails>
}
