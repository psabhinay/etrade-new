package com.etrade.mobilepro.trade.conditionalOrderDisclosure.data

import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * Parameters sent in this request are required by legacy endpoint and should not be changed or removed.
 */
internal interface ConditionalOrderDisclosureApiClient {

    @POST("/e/t/mobile/conddisclosure.json")
    @FormUrlEncoded
    @JsonMoshi
    suspend fun sendAgreement(
        @Field("_formtarget") formTarget: String = "conddisclosure",
        @Field("agree") choice: String = "I Agree",
        @Field("condredirectinput") redirect: String = "/e/t/mobile/contingentcreateentry.json"
    ): DisclosureResponseDto

    @POST("/app/order/conditionalorderdisclosure.json")
    @JsonMoshi
    suspend fun sendAgreementConfirmationAcknowledgement(@JsonMoshi @Body request: AcknowledgeConfirmationDto)
}
