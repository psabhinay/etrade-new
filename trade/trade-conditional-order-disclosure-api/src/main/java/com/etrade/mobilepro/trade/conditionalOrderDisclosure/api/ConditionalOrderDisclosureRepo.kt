package com.etrade.mobilepro.trade.conditionalOrderDisclosure.api

import com.etrade.mobilepro.backends.api.ServerMessage
import com.etrade.mobilepro.common.result.ETResult

interface ConditionalOrderDisclosureRepo {

    suspend fun isTrailingOrderAllowed(): ETResult<Boolean>

    suspend fun isDisclosureSigned(): ETResult<Boolean>

    suspend fun signDisclosure(): ETResult<List<ServerMessage>>
}
