package com.etrade.mobilepro.trade.data.form.value

import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.value.SecurityTypeValueId
import com.etrade.mobilepro.util.MutualConverter

object DefaultSecurityTypeValueId : SecurityTypeValueId {
    override val conditional: String = "conditional"
    override val mutualFund: String = "mutualFund"
    override val option: String = "option"
    override val stock: String = "stock"
    override val stockAndOptions: String = "stockAndOptions"
    override val converter: MutualConverter<String, SecurityType> by lazy { createConverter() }
}
