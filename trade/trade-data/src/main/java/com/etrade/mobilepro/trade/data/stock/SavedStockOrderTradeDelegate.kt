package com.etrade.mobilepro.trade.data.stock

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.orders.api.MarketDestination
import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.trade.api.OrderPlaceResponse
import com.etrade.mobilepro.trade.api.OrderPreviewResponse
import com.etrade.mobilepro.trade.api.TradeOrder
import com.etrade.mobilepro.trade.api.stock.StockOrderPreviewResponse
import com.etrade.mobilepro.trade.api.stock.yourPrice
import com.etrade.mobilepro.trade.data.TradeDelegate
import com.etrade.mobilepro.trade.data.TradeOrderArguments

internal const val PREVIEW_CHANGE_STOCK_ORDER = "PreviewChangeStockOrder"
internal const val PLACE_CHANGE_STOCK_ORDER = "PlaceChangeStockOrder"

internal class SavedStockOrderTradeDelegate(
    private val apiClient: StockTradingApiClient,
    private val isTablet: Boolean,
    private val user: User
) : TradeDelegate {

    override suspend fun previewOrder(map: InputValuesSnapshot, marketDestination: MarketDestination): ETResult<OrderPreviewResponse> {
        val mutableMap = map.toMutableMap().also {
            addOrderParams(it, isTablet, user.getUserId().toString(), marketDestination)
            addPreviewOrderParams(it)
        }
        return mutableMap.makeStockOrder(apiClient, PREVIEW_CHANGE_STOCK_ORDER).mapCatching { StockOrderPreviewResponseWrapper(it) }
    }

    override suspend fun placeOrder(arguments: TradeOrderArguments, order: TradeOrder): ETResult<OrderPlaceResponse> {
        val response = order.response
        require(response is StockOrderPreviewResponse)

        val mutableMap = arguments.params.toMutableMap().also {
            addOrderParams(it, isTablet, user.getUserId().toString(), arguments.marketDestination)
            addPlaceOrderParams(
                map = it,
                yourPrice = response.yourPrice,
                overnightIndicatorFlag = response.isOvernightTrading
            )
        }
        return mutableMap.makeStockOrder(apiClient, PLACE_CHANGE_STOCK_ORDER).mapCatching { StockOrderPlaceResponseWrapper(it) }
    }
}
