package com.etrade.mobilepro.menu

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.activity.addCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.etrade.eo.corelibandroid.createViewModel
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.getToolbarTitleContentDescription
import com.etrade.mobilepro.common.setToolbarActionTextAndContentDescription
import com.etrade.mobilepro.common.toolbarActionEndView
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.util.android.clicklisteners.OnViewDebounceClickListener
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

open class MenuWebViewFragment @Inject constructor(
    @DeviceType deviceType: String,
    private val viewModelFactory: MenuWebViewModel.Factory
) : WebViewFragment(deviceType) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        createViewModel { viewModelFactory.create(args.title ?: "") }.value

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            webViewReference.let {
                if (it.canGoBack()) {
                    it.goBack()
                } else {
                    findNavController().navigateUp()
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        activity?.apply {
            toolbarActionEndView?.apply {
                setToolbarActionTextAndContentDescription(R.string.done)
                setOnClickListener(
                    OnViewDebounceClickListener(
                        View.OnClickListener {
                            findNavController().navigateUp()
                        }
                    )
                )
                visibility = View.VISIBLE
            }
            args.title?.let { pageTitle ->
                toolbarTitleView?.apply {
                    text = pageTitle
                    contentDescription = getToolbarTitleContentDescription(pageTitle, requireContext())
                }
            }
            (this as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }
}
