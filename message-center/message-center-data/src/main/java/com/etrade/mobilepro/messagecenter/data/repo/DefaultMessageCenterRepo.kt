package com.etrade.mobilepro.messagecenter.data.repo

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.messagecenter.api.model.AccountHolderRequest
import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.api.model.SendNewMessageRequest
import com.etrade.mobilepro.messagecenter.api.model.ServiceMessage
import com.etrade.mobilepro.messagecenter.api.model.SupportMessage
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterAccountsDataSource
import com.etrade.mobilepro.messagecenter.api.repo.MessageCenterRepo
import com.etrade.mobilepro.messagecenter.data.mapper.toCommunications
import com.etrade.mobilepro.messagecenter.data.mapper.toMessage
import com.etrade.mobilepro.messagecenter.data.mapper.toServiceMessage
import com.etrade.mobilepro.messagecenter.data.mapper.toSupportMessage
import com.etrade.mobilepro.messagecenter.data.rest.MessageCenterApiClient
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import javax.inject.Inject

class DefaultMessageCenterRepo @Inject constructor(
    private val apiClient: MessageCenterApiClient,
    private val accountsDataSource: MessageCenterAccountsDataSource,
    private val consumerKeyProvider: () -> String?,
    private val deviceInfo: String,
    private val messageListReducer: MessageListReducer
) : MessageCenterRepo {
    private val dateFormatter = DateTimeFormatter.ofPattern("MMM dd yyyy HH:mm:ss:SSSa").withZone(ZoneId.of("America/New_York"))
    private val logger = LoggerFactory.getLogger(DefaultMessageCenterRepo::class.java)
    private val supportMsgDateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm a").withZone(ZoneId.systemDefault())

    private val accountsMap by lazy {
        runBlocking {
            accountsDataSource.getAccountIdToShortDescriptionMap().fold(
                onSuccess = {
                    it
                },
                onFailure = {
                    logger.error("Get account id to short description failed", it)
                    emptyMap()
                }
            )
        }
    }

    override suspend fun getMessageList(): ETResult<List<Message>> =
        runCatchingET {
            apiClient
                .getMessageList()
                .messages
                ?.mapNotNull {
                    it?.toMessage(dateFormatter, accountsMap)
                }
                .reduceUsing(messageListReducer)
        }

    override suspend fun getServiceMessageDetails(message: Message): ETResult<ServiceMessage> =
        runCatchingET {
            apiClient
                .getServiceMessageDetails(message.caseNo)
                .toServiceMessage(message)
        }

    override suspend fun getSupportMessageDetails(message: Message): ETResult<SupportMessage> =
        runCatchingET {
            apiClient
                .getSupportMessageDetails(message.id, message.caseNo)
                .toSupportMessage(supportMsgDateFormatter, message)
        }

    override suspend fun sendReply(caseNo: String, msgId: String, content: String): ETResult<List<SupportMessage.Communication>> =
        runCatchingET {
            val response = apiClient.sendReply(caseNo, msgId, content)
            require(!response.refNumber.isNullOrEmpty())
            response.communicationList.toCommunications(supportMsgDateFormatter)
        }

    override suspend fun getAccountHoldersFirstNameLastName(request: AccountHolderRequest): ETResult<String> = runCatchingET {
        val holder = apiClient.getAccountHolders(
            request.acctNo,
            request.instNo,
            request.instName,
            requireNotNull(consumerKeyProvider.invoke())
        ).holder?.firstOrNull()

        if (holder == null) {
            request.name
        } else {
            val firstName = requireNotNull(holder.firstName)
            val lastName = requireNotNull(holder.lastName)

            "$firstName $lastName"
        }
    }

    override suspend fun sendNewMessage(request: SendNewMessageRequest): ETResult<Unit> = runCatchingET {
        requireNotNull(
            apiClient.sendNewMessage(
                request.selAcctHolder,
                request.accountId,
                request.subjectCode,
                request.topicCode,
                appendDeviceInfo(request.message, request.isStreamingOn, request.isExtendedHoursOn),
                request.instName,
                request.instNo
            )
        ).let {
            require(it.resultDto?.isSuccess() == true)
        }
    }

    private fun appendDeviceInfo(message: String, isStreamingOn: String, isExtendedHoursOn: String): String =
        "$message \r\n $deviceInfo Streaming: $isStreamingOn EH: $isExtendedHoursOn"
}
