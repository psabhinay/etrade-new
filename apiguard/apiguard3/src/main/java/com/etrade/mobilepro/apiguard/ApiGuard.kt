package com.etrade.mobilepro.apiguard

import android.app.Application
import androidx.annotation.RawRes
import com.apiguard3.AGRequest
import com.apiguard3.AGResponse
import com.apiguard3.APIGuard
import okhttp3.Headers.Companion.toHeaders
import okhttp3.Request
import okhttp3.Response
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.security.cert.Certificate

/**
 * Initialization object for API guard. After it is initialized, API guard can be used to filter unwanted API calls.
 *
 * Use [ApiGuardInterceptor] to embed API guard into an HTTP client.
 */
object ApiGuard {

    private val logger: Logger by lazy { LoggerFactory.getLogger(ApiGuard::class.java) }

    private val guard: APIGuard by lazy { APIGuard() }

    /**
     * Performs initialization of API guard. This method is intended to be called once, on an [application] startup (the best place to call it from is from
     * [Application.onCreate] method).
     *
     * This method won't wait or thrown an exception. If initialization fails, API guard will not be operable and may block API requests.
     *
     * @param application an application
     * @param configRes resource ID pointing to a configuration file for [APIGuard] (see readme for more details)
     * @param environment optional environment name
     */
    fun init(application: Application, @RawRes configRes: Int, environment: String = "default") {
        logger.debug("API Guard: init.")
        guard.initialize(application, Callback(), configRes, APIGuard.INIT_PROCEED, environment)
    }

    /**
     * Transforms initial [request] to gather required telemetry for API guard.
     *
     * @param request initial request
     *
     * @return actual request, that must be sent further down a chain
     */
    internal fun transform(request: Request): Request {
        val agRequest = AGRequest.Builder()
            .headersMultiMap(request.headers.toMultimap())
            .url(request.url.toString())
            .build()

        guard.transformRequest(agRequest)

        return request.newBuilder()
            .headers(agRequest.headers.toHeaders())
            .build()
    }

    /**
     * Parses a [response] to gather data for API guard.
     *
     * @param response actual response
     */
    internal fun parse(response: Response) {
        val agResponse = AGResponse.Builder()
            .headersMultiMap(response.headers.toMultimap())
            .build()
        guard.parseResponse(agResponse)
    }

    private class Callback : APIGuard.Callback {

        override fun checkCertificates(certificates: MutableList<Certificate>?, host: String?) {
            // used for certificate pinning - which we aren't doing
        }

        override fun log(message: String?) {
            logger.debug("API Guard: $message")
        }
    }
}
