package com.etrade.mobilepro.news.presentation.detail

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.etrade.mobilepro.common.readstatus.ReadableType
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.news.presentation.NewsRouter
import com.etrade.mobilepro.news.presentation.NewsSharedViewModel
import com.etrade.mobilepro.news.presentation.R
import com.etrade.mobilepro.news.presentation.databinding.FragmentTextNewsDetailBinding
import com.etrade.mobilepro.news.presentation.detail.helper.TextNewsContentBindHelper
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.searchingapi.items.toBundle
import com.etrade.mobilepro.util.android.accessibility.populateAccessibilityEvent
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.fragment.isChildOfBottomSheet
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class TextNewsDetailFragment @Inject constructor(
    private val bindHelper: TextNewsContentBindHelper,
    private val viewModelFactory: ViewModelProvider.Factory,
    private val snackbarUtilFactory: SnackbarUtilFactory,
    private val router: NewsRouter
) : Fragment(R.layout.fragment_text_news_detail) {

    private val binding by viewBinding(FragmentTextNewsDetailBinding::bind)

    private val viewModel by viewModels<TextNewsViewModel> { viewModelFactory }
    private val sharedViewModel by activityViewModels<NewsSharedViewModel> { viewModelFactory }
    private val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(!isChildOfBottomSheet(this))
        val (passedDocId, passedNewsItem) = router.newsDetailsArgs(arguments)
        setupFragment()
        bindViewModels(passedDocId, passedNewsItem)
    }

    override fun onDestroyView() {
        setHasOptionsMenu(false)
        super.onDestroyView()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.options_menu_news_detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                activity?.onBackPressed()
                true
            }
            R.id.menu_font_adjust -> {
                viewModel.fontAdjustHandler.toggleFontSize()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupFragment() {
        binding.textNewsCell.toggleDisclosureArrowVisibility(false)
        binding.textNewsCell.symbolSelectListener = {
            navigateToQuotePage(it)
        }
        binding.newsContent.populateAccessibilityEvent {
            isClickable = false
            isLongClickable = false
        }
    }

    @Suppress("LongMethod")
    private fun bindViewModels(passedDocId: String?, parcelableNewsItem: ParcelableNewsItem?) {
        bindHelper.bindTextNewsContent(
            viewModel.contentLoader,
            viewLifecycleOwner,
            binding.newsContentLoadingPb,
            binding.newsContent,
            binding.textNewsCell,
            actionOnRetry = {
                snackbarUtil.retrySnackbar {
                    viewModel.contentLoader.retry()
                }
            }
        )
        bindHelper.bindTextNewsFontAdjusting(
            viewModel.fontAdjustHandler,
            viewLifecycleOwner,
            binding.newsContent,
            binding.textNewsCell
        )
        when {
            parcelableNewsItem != null -> {
                viewModel.contentLoader.load(parcelableNewsItem.toNewsItem())
                viewModel.statusRepo.markAsRead(parcelableNewsItem.docId, type = ReadableType.NEWS_ARTICLE)
            }
            passedDocId != null -> {
                viewModel.contentLoader.load(passedDocId)
                viewModel.statusRepo.markAsRead(passedDocId, type = ReadableType.NEWS_ARTICLE)
            }
            else -> {
                sharedViewModel.savedSelectedNewsItem.observe(
                    viewLifecycleOwner,
                    Observer { item ->
                        binding.textNewsCell.bindTo(item)
                        item?.let { viewModel.contentLoader.load(it.docId) }
                    }
                )
            }
        }
    }

    private fun navigateToQuotePage(ticker: String) {
        val bundle = SearchResultItem.Symbol(title = ticker, type = InstrumentType.EQ).toBundle()
        if (isChildOfBottomSheet(this)) {
            router.navigateToQuotePage(findNavController(), bundle)
        } else {
            router.navigateToQuoteDetails(activity, bundle)
        }
    }
}
