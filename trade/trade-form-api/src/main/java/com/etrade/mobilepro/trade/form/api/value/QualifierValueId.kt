package com.etrade.mobilepro.trade.form.api.value

interface QualifierValueId {
    val askPrice: String
    val bidPrice: String
    val lastPrice: String
}
