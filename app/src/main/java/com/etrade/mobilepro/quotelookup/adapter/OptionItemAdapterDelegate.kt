package com.etrade.mobilepro.quotelookup.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.databinding.ItemOptionSearchResultSymbolBinding
import com.etrade.mobilepro.instrument.formatExpirationDate
import com.etrade.mobilepro.quote.search.presentation.OnSearchResultItemClickListener
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.android.extension.viewBinding
import com.hannesdorfmann.adapterdelegates4.AbsListItemAdapterDelegate

class OptionItemAdapterDelegate(private val listener: OnSearchResultItemClickListener?) :
    AbsListItemAdapterDelegate<SearchResultItem.Symbol, SearchResultItem, OptionItemAdapterDelegate.SymbolItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): SymbolItemViewHolder {
        val view = parent.viewBinding(ItemOptionSearchResultSymbolBinding::inflate)
        return SymbolItemViewHolder(view)
    }

    override fun isForViewType(item: SearchResultItem, items: MutableList<SearchResultItem>, position: Int): Boolean {
        return item is SearchResultItem.Symbol
    }

    override fun onBindViewHolder(item: SearchResultItem.Symbol, holder: SymbolItemViewHolder, payloads: MutableList<Any>) {
        holder.bind(item)
    }

    inner class SymbolItemViewHolder(private val binding: ItemOptionSearchResultSymbolBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private var item: SearchResultItem.Symbol? = null

        init {
            binding.symbolRootLayout.setOnClickListener {
                item?.let {
                    listener?.onSearchResultItemClicked(it)
                }
            }
        }

        fun bind(item: SearchResultItem.Symbol) {
            this.item = item

            val optionType = item.type.optionType
            binding.symbolPrice.text = if (optionType == null) {
                "$${item.strike}"
            } else {
                "$${item.strike} $optionType"
            }
            binding.symbolTitle.text = item.displaySymbol.split(" ").getOrElse(0) {
                item.queryValue
            }
            binding.symbolDescription.text =
                item.settlementSessionCd.formatExpirationDate(item.expire)
        }
    }
}
