package com.etrade.mobilepro.trade

import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.trade.help.TradeHelpFragmentArgs
import com.etrade.mobilepro.trade.help.getTitleId
import com.etrade.mobilepro.uiwidgets.bottomsheetnavcontainer.BottomSheetNavContainer
import com.etrade.mobilepro.user.session.manager.UserSessionHandler
import com.etrade.mobilepro.util.android.network.ConnectionNotifier
import javax.inject.Inject

@RequireLogin
internal class TradeBottomSheetFragment @Inject constructor(
    connectionNotifier: ConnectionNotifier,
    userSessionHandler: UserSessionHandler
) : BottomSheetNavContainer(connectionNotifier, userSessionHandler) {

    private val tradeBottomNavArgs: TradeBottomSheetFragmentArgs by navArgs()

    override fun createNavHost() = NavHostFragment.create(
        R.navigation.trade_bottom_sheet_navigation_graph,
        arguments?.apply {
            if (tradeBottomNavArgs.goToLegSelect) {
                putString(START_DESTINATION, TRADE_OPTION_LEG_SELECT_FRAGMENT)
            }
        }
    )

    override fun createDestinationChangeListener(): NavController.OnDestinationChangedListener {
        return NavController.OnDestinationChangedListener { _, destination, args ->
            when (destination.id) {
                R.id.liEtfAgreementWebViewFragment -> titleText = getString(R.string.trade_leveraged_etf_webview_title)
                else -> {
                    val helpTitle = args
                        ?.takeIf { destination.id == R.id.tradeHelpFragment }
                        ?.let { getString(TradeHelpFragmentArgs.fromBundle(args).tradeOptionInfoType.getTitleId()) }
                    titleText = helpTitle ?: destination.label?.toString() ?: titleText
                }
            }
            subTitleText = ""
        }
    }

    override fun onTitleChange(newTitle: String) {
        titleText = newTitle
    }

    override fun onDestroyView() {
        super.onDestroyView()
        // Setting the description to nothing in order to prevent old header text from flashing when opening a new quote.
        titleText = ""
    }
}
