package com.etrade.mobilepro.quote.screen.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.dropdown.DropDownManager
import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.news.api.model.NewsItem
import com.etrade.mobilepro.news.presentation.adapter.EmptyNewsItemViewHolder
import com.etrade.mobilepro.news.presentation.adapter.TextNewsItemViewHolder
import com.etrade.mobilepro.news.presentation.databinding.ItemNewsFooterBinding
import com.etrade.mobilepro.news.presentation.databinding.ItemTextNewsBinding
import com.etrade.mobilepro.quote.screen.presentation.QuoteChartViewModel
import com.etrade.mobilepro.quote.screen.presentation.R
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemChainBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemDisclosureBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemEtfDisclosureBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemEtfFooterBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemFooterBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemFundIconSectionBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemFundsFooterBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemFundsLinksBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemHeaderBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemMutualFundClassSectionBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemMutualFundFeesExpensesBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteItemRangeBinding
import com.etrade.mobilepro.quote.screen.presentation.databinding.QuoteLayoutErrorRetryBinding
import com.etrade.mobilepro.quote.screen.presentation.ui.performance.FundPerformanceViewModel
import com.etrade.mobilepro.quoteapi.FundClassInfo
import com.etrade.mobilepro.quoteapi.FundQuoteItem
import com.etrade.mobilepro.quoteapi.QuoteItem
import com.etrade.mobilepro.quoteapi.QuoteItemInterface
import com.etrade.mobilepro.quoteapi.QuoteWidgetItem
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.util.android.extension.dispatchUpdates
import com.etrade.mobilepro.util.android.extension.inflate
import com.etrade.mobilepro.util.android.extension.viewBinding

@Suppress("LongParameterList")
class QuoteAdapter(
    private val lifecycleOwner: LifecycleOwner,
    private val chartViewModel: QuoteChartViewModel,
    private val selectNewsItemListener: ((NewsItem.Text, Int) -> Unit)? = null,
    private val symbolSelectListener: ((String) -> Unit)? = null,
    private val statusChecker: ((NewsItem.Text) -> Boolean)? = null,
    private val selectionChecker: ((Int) -> Boolean)? = null,
    private val companyOverviewClickListener: (() -> Unit)? = null,
    private val disclosureClickListener: (() -> Unit)? = null,
    private val viewAllNewsClickListener: (() -> Unit)? = null,
    private val newsRetryClickListener: (() -> Unit)? = null,
    private val tradeClickListener: (() -> Unit)? = null,
    private val setAlertClickListener: (() -> Unit)? = null,
    private val addToWatchlistClickListener: (() -> Unit)? = null,
    private val fundClassDropDown: DropDownManager<FundClassInfo>,
    private val fetchQuoteAction: ((String?, InstrumentType) -> Unit)? = null,
    private val fundInfoPopUpListener: ((Int) -> Unit)? = null,
    private val fundPerformanceWidgetViewModel: FundPerformanceViewModel? = null,
    private val definitionsClickListener: (() -> Unit)? = null,
    private val fundDescriptionClickListener: ((description: String) -> Unit)? = null,
    private val prospectusClickListener: (() -> Unit)? = null,
    private val reportClickListener: (() -> Unit)? = null,
    private val level2ClickListener: (() -> Unit)? = null,
    private val optionsChainClickListener: ((SearchResultItem.Symbol) -> Unit)? = null,
    private val performanceClickListener: (() -> Unit)? = null
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var quoteItems: List<QuoteItemInterface> = emptyList()
        set(newItems) {
            val oldItems = field
            field = newItems
            dispatchUpdates(oldItems, newItems)
        }

    private val bindingViewType = listOf(
        R.layout.quote_details_widget,
        R.layout.quote_item_detail,
        R.layout.quote_closing_price_widget,
        R.layout.quote_chart,
        R.layout.quote_item_option_detail,
        R.layout.quote_item_mutual_fund_quote_details,
        R.layout.quote_item_mutual_fund_morningstar_ratings,
        R.layout.quote_item_mutual_fund_quarterly_return,
        R.layout.quote_fund_performance_widget,
        R.layout.quote_item_money_market_fund_details,
        R.layout.quote_item_fund_summary_section,
        R.layout.quote_item_fund_morningstar_details,
        R.layout.quote_momentum_bar
    )

    @Suppress("LongMethod", "ComplexMethod")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = quoteItems[position]
        when (holder) {
            is TextNewsItemViewHolder -> holder.bindTo(
                item as NewsItem.Text,
                wasRead = statusChecker?.invoke(item),
                isSelected = selectionChecker?.invoke(position) ?: false
            )
            is HeaderButtonsViewHolder -> holder.bindTo(item as QuoteItem.HeaderButtons?)
            is FooterViewHolder -> holder.bindTo(
                item as QuoteItem.QuoteFooterItem?,
                companyOverviewClickListener,
                level2ClickListener,
                reportClickListener
            )
            is DisclosureViewHolder -> holder.bindTo(item as QuoteItem.QuoteDisclosureItem?, disclosureClickListener)
            is QuoteRangeViewHolder -> holder.bindTo(item as QuoteItem.QuoteRangeBarItem?)
            is NewsFooterViewHolder -> holder.bindTo(
                item as QuoteItem.QuoteNewsFooterItem?, viewAllNewsClickListener, disclosureClickListener
            )
            is NewsRetryViewHolder -> holder.bindTo(item as QuoteItem.NewsRetry?, newsRetryClickListener)
            is QuoteChainViewHolder -> holder.bindTo(
                item as QuoteItem.QuoteChainItem?,
                level2ClickListener,
                optionsChainClickListener
            )
            is FundsLinksViewHolder -> holder.bindTo(
                disclosureClickListener = disclosureClickListener,
                definitionsClickListener = definitionsClickListener
            )
            is FundsFooterViewHolder -> holder.bindTo(
                item = item as FundQuoteItem.QuoteFundsFooterItem,
                fundDescriptionClickListener = fundDescriptionClickListener,
                prospectusClickListener = prospectusClickListener,
                fundReportClickListener = reportClickListener
            )
            is FundClassSectionViewHolder -> holder.bindTo(
                item as FundQuoteItem.FundClassSection?,
                fundClassDropDown,
                fetchQuoteAction
            )
            is FundIconSectionViewHolder -> holder.bindTo(
                item as FundQuoteItem.FundIconSection?,
                fundInfoPopUpListener
            )
            is FundFeesAndExpensesViewHolder -> holder.bindTo(
                item as? FundQuoteItem.FeesAndExpensesItem
            )
            is EtfFooterViewHolder -> holder.bindTo(
                item as QuoteItem.QuoteEtfLegalFooterItem?,
                prospectusClickListener,
                performanceClickListener
            )
            is EtfDisclosureViewHolder -> holder.bindTo(item as QuoteItem.QuoteEtfDisclosureItem?, disclosureClickListener)
            is QuoteBindingViewHolder -> {
                when (getItemViewType(position)) {
                    R.layout.quote_fund_performance_widget -> {
                        (item as FundQuoteItem.FundPerformanceItem?)?.let {
                            fundPerformanceWidgetViewModel?.setPerformanceItem(it)
                        }
                        holder.bindTo(fundPerformanceWidgetViewModel)
                    }
                    R.layout.quote_closing_price_widget -> {
                        holder.itemView.setOnClickListener {
                            (item as QuoteItem.ClosingPriceItem).takeIf { it.isOption }?.let { symbolSelectListener?.invoke(it.underlyingSymbol) }
                        }
                        holder.bindTo(item)
                    }
                    R.layout.quote_chart -> {
                        holder.bindTo(chartViewModel)
                        (item as? QuoteItem.QuoteChartItem)?.symbol?.let {
                            chartViewModel.updateSymbol(it)
                        }
                    }
                    else -> holder.bindTo(item)
                }
            }
        }
    }

    @Suppress("LongMethod", "ComplexMethod")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        if (viewType in bindingViewType) {
            val binding = DataBindingUtil.inflate<ViewDataBinding>(
                inflater, viewType, parent, false
            )
            binding.lifecycleOwner = lifecycleOwner
            return QuoteBindingViewHolder(binding)
        } else {
            return when (viewType) {
                R.layout.item_text_news ->
                    TextNewsItemViewHolder(
                        parent.viewBinding(ItemTextNewsBinding::inflate),
                        selectNewsItemListener,
                        symbolSelectListener
                    )
                R.layout.quote_item_header ->
                    HeaderButtonsViewHolder(
                        parent.viewBinding(QuoteItemHeaderBinding::inflate),
                        tradeClickListener,
                        setAlertClickListener,
                        addToWatchlistClickListener
                    )
                R.layout.quote_item_footer ->
                    FooterViewHolder(parent.viewBinding(QuoteItemFooterBinding::inflate))
                R.layout.quote_item_range ->
                    QuoteRangeViewHolder(
                        parent.viewBinding(QuoteItemRangeBinding::inflate),
                        lifecycleOwner
                    )
                R.layout.item_news_footer ->
                    NewsFooterViewHolder(parent.viewBinding(ItemNewsFooterBinding::inflate))
                R.layout.quote_layout_error_retry ->
                    NewsRetryViewHolder(parent.viewBinding(QuoteLayoutErrorRetryBinding::inflate))
                R.layout.quote_item_chain ->
                    QuoteChainViewHolder(parent.viewBinding(QuoteItemChainBinding::inflate))
                R.layout.quote_item_funds_footer ->
                    FundsFooterViewHolder(parent.viewBinding(QuoteItemFundsFooterBinding::inflate))
                R.layout.quote_item_disclosure ->
                    DisclosureViewHolder(parent.viewBinding(QuoteItemDisclosureBinding::inflate))
                R.layout.quote_item_mutual_fund_class_section ->
                    FundClassSectionViewHolder(
                        parent.viewBinding(
                            QuoteItemMutualFundClassSectionBinding::inflate
                        )
                    )
                R.layout.quote_item_fund_icon_section ->
                    FundIconSectionViewHolder(parent.viewBinding(QuoteItemFundIconSectionBinding::inflate))
                R.layout.quote_item_funds_links ->
                    FundsLinksViewHolder(parent.viewBinding(QuoteItemFundsLinksBinding::inflate))
                R.layout.quote_item_mutual_fund_fees_expenses ->
                    FundFeesAndExpensesViewHolder(
                        parent.viewBinding(
                            QuoteItemMutualFundFeesExpensesBinding::inflate
                        )
                    )
                R.layout.quote_item_etf_footer ->
                    EtfFooterViewHolder(parent.viewBinding(QuoteItemEtfFooterBinding::inflate))
                R.layout.quote_item_etf_disclosure ->
                    EtfDisclosureViewHolder(parent.viewBinding(QuoteItemEtfDisclosureBinding::inflate))
                else -> EmptyNewsItemViewHolder(parent.inflate(viewType))
            }
        }
    }

    @Suppress("LongMethod", "ComplexMethod") // huge when block
    override fun getItemViewType(position: Int): Int {
        return if (quoteItems.isEmpty()) {
            R.layout.item_empty_news
        } else {
            return when (quoteItems[position]) {
                is QuoteItem.HeaderButtons -> R.layout.quote_item_header
                is QuoteWidgetItem -> R.layout.quote_details_widget
                is QuoteItem.ClosingPriceItem -> R.layout.quote_closing_price_widget
                is QuoteItem.QuoteMomentumBar -> R.layout.quote_momentum_bar
                is QuoteItem.QuoteDetailItem -> R.layout.quote_item_detail
                is QuoteItem.QuoteFooterItem -> R.layout.quote_item_footer
                is QuoteItem.QuoteDisclosureItem -> R.layout.quote_item_disclosure
                is NewsItem.Text -> R.layout.item_text_news
                is QuoteItem.QuoteNewsFooterItem -> R.layout.item_news_footer
                is QuoteItem.QuoteChainItem -> R.layout.quote_item_chain
                is QuoteItem.QuoteRangeBarItem -> R.layout.quote_item_range
                is QuoteItem.QuoteChartItem -> R.layout.quote_chart
                is QuoteItem.NewsRetry -> R.layout.quote_layout_error_retry
                is QuoteItem.NewsLoading -> R.layout.quote_layout_loading
                is QuoteItem.ThinGutter -> R.layout.thin_divider
                is QuoteItem.ThickGutter -> R.layout.quote_layout_gutter
                is QuoteItem.QuoteHoldings -> R.layout.quote_item_holdings
                is QuoteItem.QuoteOptionDetailItem -> R.layout.quote_item_option_detail
                is FundQuoteItem.MutualFundDetailItem -> R.layout.quote_item_mutual_fund_quote_details
                is FundQuoteItem.MoneyMarketFundDetail -> R.layout.quote_item_money_market_fund_details
                is FundQuoteItem.QuoteFundsFooterItem -> R.layout.quote_item_funds_footer
                is FundQuoteItem.FundClassSection -> R.layout.quote_item_mutual_fund_class_section
                is FundQuoteItem.FundIconSection -> R.layout.quote_item_fund_icon_section
                is FundQuoteItem.MorningStarRatings -> R.layout.quote_item_mutual_fund_morningstar_ratings
                is FundQuoteItem.MorningStarDetails -> R.layout.quote_item_fund_morningstar_details
                is FundQuoteItem.QuarterlyTrailingReturnsSection -> R.layout.quote_item_mutual_fund_quarterly_return
                is FundQuoteItem.SummarySection -> R.layout.quote_item_fund_summary_section
                is FundQuoteItem.FundPerformanceItem -> R.layout.quote_fund_performance_widget
                is FundQuoteItem.LinksItem -> R.layout.quote_item_funds_links
                is FundQuoteItem.MutualFundDisclosureTextItem -> R.layout.quote_item_mutual_fund_disclosures_text
                is FundQuoteItem.MoneyMarketFundDisclosureTextItem -> R.layout.quote_item_money_market_fund_disclosures_text
                is FundQuoteItem.FeesAndExpensesItem -> R.layout.quote_item_mutual_fund_fees_expenses
                is QuoteItem.QuoteEtfLegalFooterItem -> R.layout.quote_item_etf_footer
                is QuoteItem.QuoteEtfDisclosureItem -> R.layout.quote_item_etf_disclosure
                else -> R.layout.item_empty_news
            }
        }
    }

    override fun getItemCount(): Int = quoteItems.size
}
