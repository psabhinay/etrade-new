package com.etrade.mobilepro.messagecenter.presentation.details.support.adapter

import com.etrade.mobilepro.messagecenter.api.model.Message
import com.etrade.mobilepro.messagecenter.api.model.ReferenceBook
import com.etrade.mobilepro.messagecenter.api.model.SupportMessage
import kotlinx.collections.immutable.persistentListOf
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.threeten.bp.Instant

internal class SupportMessageHolderTest {
    @Test
    fun testCreation() {
        val sut = SupportMessageHolder.createFrom(baseSupportMessage, baseMessage, baseReferenceBook)

        assertEquals(baseMessage.status, sut.header.status)
        assertEquals(baseMessage.caseNo, sut.header.referenceNumber)
        assertEquals(baseMessage, sut.initialMessage)
        assertEquals(baseSupportMessage, sut.backingSupportMessage)
        assertEquals(baseCommunication1.caseNo, sut.latest?.content?.caseNo)
        assertEquals(baseCommunication1.createdAt?.toEpochMilli(), sut.latest?.content?.createdAt?.toEpochMilli())
        assertEquals(false, sut.latest?.hasDivider)
        assertEquals(1, sut.otherHistory.size)
        assertEquals(baseCommunication2, sut.otherHistory[0].content)
        assertEquals(false, sut.otherHistory[0].hasDivider)
    }

    @Test
    fun testToList() {
        val sut = SupportMessageHolder.createFrom(baseSupportMessage, baseMessage, baseReferenceBook)

        val output = sut.toList()

        assertEquals(5, output.size)
        assertEquals(baseMessage.status, (output[0] as? Header)?.status)
        assertEquals(true, output[1] is Reply)
        assertEquals("", (output[1] as? Reply)?.content)
        assertEquals(baseCommunication1, (output[2] as? Letter)?.content)
        assertEquals(true, output[3] is Separator)
        assertEquals(baseCommunication2, (output[4] as? Letter)?.content)
    }

    @Test
    fun testWithNewHeaderStatus() {
        val sut = SupportMessageHolder.createFrom(baseSupportMessage, baseMessage, baseReferenceBook)
        assertEquals(baseSupportMessage.commonDetails.status, sut.header.status)

        val output = sut.withNewHeaderStatus("new status")

        assertEquals("new status", output.header.status)
    }

    @Test
    fun testWithNewHistory() {
        val sut = SupportMessageHolder.createFrom(baseSupportMessage, baseMessage, baseReferenceBook)
        val newHistory = listOf(newCommunication, baseCommunication1, baseCommunication2)
        val replySentAt = Instant.ofEpochMilli(3)
        val replySentAtFormatted = "formatted date"

        val output = sut.withNewHistory(newHistory, replySentAt, replySentAtFormatted)

        assertEquals(sut.header, output.header)
        assertEquals(newCommunication.message, output.latest?.content?.message)
        assertEquals(output.header.referenceNumber, output.latest?.content?.caseNo)
        assertEquals(replySentAt, output.latest?.content?.createdAt)
        assertEquals(replySentAtFormatted, output.latest?.content?.createdAtFormatted)
        assertEquals(2, output.otherHistory.size)
        assertEquals(baseCommunication1, output.otherHistory[0].content)
        assertEquals(baseCommunication2, output.otherHistory[1].content)
    }

    private val newCommunication = SupportMessage.Communication(
        caseNo = "--",
        message = "msg3",
        createdAt = null,
        createdAtFormatted = "--"
    )

    private val baseCommunication1 = SupportMessage.Communication(
        caseNo = "base case no",
        message = "msg1",
        createdAt = Instant.ofEpochMilli(1),
        createdAtFormatted = "1"
    )

    private val baseCommunication2 = SupportMessage.Communication(
        caseNo = "base case no",
        message = "msg2",
        createdAt = Instant.ofEpochMilli(2),
        createdAtFormatted = "2"
    )
    private val baseSupportMessage = SupportMessage(
        commonDetails = SupportMessage.CommonDetails(
            status = "base status"
        ),
        history = listOf(baseCommunication1, baseCommunication2)
    )
    private val baseMessage = Message(
        id = "msg1",
        accountId = "ac1",
        accountShortName = "account - 1",
        descId = "desc1",
        subjectId = "subj1",
        caseTypeId = "casetype1",
        caseTypeDesc = "casetype1desc",
        caseNo = "base case no",
        isUnread = false,
        status = "base status",
        createdAt = Instant.ofEpochMilli(0),
        createdAtFormatted = "0",
        readAt = null
    )
    private val baseReferenceBook = ReferenceBook(persistentListOf(), persistentListOf())
}
