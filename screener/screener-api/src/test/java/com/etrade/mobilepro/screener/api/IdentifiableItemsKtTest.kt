package com.etrade.mobilepro.screener.api

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class IdentifiableItemsKtTest {
    data class TestIdentifiable(
        override val id: String,
        override val title: String,
        override val isAllItemsElement: Boolean
    ) : Identifiable

    private val defaultItem = TestIdentifiable("defaultId", "defaultTitle", true)

    private val testItem1 = TestIdentifiable("id1", "title1", false)
    private val testItem2 = TestIdentifiable("id2", "title2", false)

    @Test
    fun `If user selects item other than default one, then default item is removed from the selection set`() {
        val sut = setOf(defaultItem)

        val result = sut.toggle(testItem1, defaultItem)

        Assertions.assertEquals(1, result.size)
        Assertions.assertFalse(result.contains(defaultItem))
        Assertions.assertTrue(result.contains(testItem1))
    }

    @Test
    fun `If user deselects the only left selected non-default item then default item is added to to selectiion set`() {
        val sut = setOf(testItem2)

        val result = sut.toggle(testItem2, defaultItem)

        Assertions.assertEquals(1, result.size)
        Assertions.assertTrue(result.contains(defaultItem))
    }

    @Test
    fun `if user selects default item, all other items become deselected`() {
        val sut = setOf(testItem1, testItem2)

        val result = sut.toggle(defaultItem, defaultItem)

        Assertions.assertEquals(1, result.size)
        Assertions.assertTrue(result.contains(defaultItem))
    }

    @Test
    fun `If user deselects selected default item nothing happens`() {
        val sut = setOf(defaultItem)

        val result = sut.toggle(defaultItem, defaultItem)

        Assertions.assertEquals(1, result.size)
        Assertions.assertTrue(result.contains(defaultItem))
    }

    @Test
    fun `If user selects item other than default one, and set contains only non-default items, selected item is just added to it`() {
        val sut = setOf(testItem1)

        val result = sut.toggle(testItem2, defaultItem)

        Assertions.assertEquals(2, result.size)
        Assertions.assertTrue(result.contains(testItem2))
        Assertions.assertTrue(result.contains(testItem1))
    }

    @Test
    fun `If user deselects item other than default one, and set contains only non-default items, selected item is just removed from it`() {
        val sut = setOf(testItem1, testItem2)

        val result = sut.toggle(testItem1, defaultItem)

        Assertions.assertEquals(1, result.size)
        Assertions.assertTrue(result.contains(testItem2))
        Assertions.assertFalse(result.contains(testItem1))
    }
}
