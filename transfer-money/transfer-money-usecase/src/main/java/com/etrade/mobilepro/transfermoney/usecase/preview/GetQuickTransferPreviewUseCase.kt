package com.etrade.mobilepro.transfermoney.usecase.preview

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.transfermoney.api.TransferMoneyAccount
import com.etrade.mobilepro.transfermoney.api.repo.TransferMoneyRepo
import com.etrade.mobilepro.util.UseCase
import javax.inject.Inject

interface GetQuickTransferPreviewUseCase : UseCase<GetQuickTransferPreviewRequest, ETResult<GetQuickTransferPreviewResult>>

class GetQuickTransferPreviewUseCaseIml @Inject constructor(
    private val repo: TransferMoneyRepo
) : GetQuickTransferPreviewUseCase {

    override suspend fun execute(parameter: GetQuickTransferPreviewRequest): ETResult<GetQuickTransferPreviewResult> = runCatchingET {
        repo.getPreview(parameter.toRepositoryPreviewRequest())
            .getOrThrow()
            .let {
                if (it.errorDescription != null) {
                    GetQuickTransferPreviewResult.Failed(it.errorCode, it.errorDescription?.trim())
                } else {
                    GetQuickTransferPreviewResult.Success(
                        transferFrom = formatAccountName(parameter.fromAccount),
                        transferTo = formatAccountName(parameter.toAccount),
                        repeat = parameter.frequency,
                        scheduleDate = it.scheduledDate?.trim(),
                        isIraAccount = parameter.toAccount.isIraAccount,
                        amount = it.amount,
                        message = it.fundHoldInfoMessage?.trim(),
                        headerMessage = it.headerMessage?.trim(),
                        footerMessage = it.footerMessage?.trim()
                    )
                }
            }
    }

    /**
     * Account name from server represented in such format "Individual Brokerage -7925 $47279165.51"
     * We should transform this to "Individual Brokerage -7925"
     */
    private fun formatAccountName(account: TransferMoneyAccount): String {
        val index = account.title.indexOf("$")

        return if (index < 0) {
            account.title
        } else {
            account.title.substring(0, index - 1)
        }
    }
}
