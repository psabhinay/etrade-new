package com.etrade.mobilepro.util

enum class SortOrder(val code: String) {
    ASCENDING("1") {
        override fun negate(): SortOrder = DESCENDING
    },
    DESCENDING("2") {
        override fun negate(): SortOrder = ASCENDING
    },
    UNDEFINED("0") {
        override fun negate(): SortOrder = ASCENDING
    };

    abstract fun negate(): SortOrder

    companion object {
        val defaultOrder = ASCENDING
    }
}
