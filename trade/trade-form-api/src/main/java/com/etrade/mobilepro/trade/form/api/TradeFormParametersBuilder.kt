package com.etrade.mobilepro.trade.form.api

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.trade.api.AdvancedTradeType
import com.etrade.mobilepro.trade.api.option.OptionTradeStrategy
import java.math.BigDecimal

interface TradeFormParametersBuilder {

    var action: TradeAction?
    var orderId: String?
    var originalQuantity: String?
    var accountId: String?
    var symbol: WithSymbolInfo?
    var term: OrderTerm?
    var quantity: BigDecimal?
    var priceType: PriceType?
    var limitPrice: BigDecimal?
    var offsetValue: BigDecimal?
    var stopPrice: BigDecimal?
    var optionTradeStrategy: OptionTradeStrategy?
    var fundToBuy: WithSymbolInfo?
    var positionLotId: String?
    var allOrNone: Boolean?
    var advancedOrder: Boolean?
    var advancedTradeType: AdvancedTradeType?
    var walkthroughFormCompleted: String?

    fun create(securityType: SecurityType): InputValuesSnapshot
}
