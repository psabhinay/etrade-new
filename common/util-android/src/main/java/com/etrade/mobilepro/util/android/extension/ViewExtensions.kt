package com.etrade.mobilepro.util.android.extension

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.res.Resources
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.autofill.AutofillManager
import android.view.inputmethod.InputMethodManager
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.findViewTreeLifecycleOwner
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.CoroutineScope

private const val ALPHA_ENABLED = 1f
private const val ALPHA_DISABLED = 0.5f

/**
 * Clears focus from this view by toggling [View.isFocusable] and [View.isFocusableInTouchMode] properties lets prevent gaining of focus by this [View] right
 * after calling [View.clearFocus] method on some devices (ex. LG G5 with Android 7.0).
 */
fun View.clearFocusSafely() {
    // Save current values of isFocusable and isFocusableInTouchMode properties.
    val focusable = isFocusable
    val focusableInTouchMode = isFocusableInTouchMode

    // Disable focus for this view.
    isFocusable = false
    isFocusableInTouchMode = false

    clearFocus()

    // Restore previous values.
    isFocusable = focusable
    isFocusableInTouchMode = focusableInTouchMode
}

fun View.hideSoftKeyboard(): Boolean =
    getInputMethodManager().hideSoftInputFromWindow(this.windowToken, 0)

fun View.showSoftKeyboard(forced: Boolean = false): Boolean =
    getInputMethodManager().showSoftInput(
        this,
        if (forced) {
            InputMethodManager.SHOW_FORCED
        } else {
            InputMethodManager.SHOW_IMPLICIT
        }
    )

fun View.setEnabledState(enabled: Boolean) {
    isEnabled = enabled
    alpha = if (enabled) {
        ALPHA_ENABLED
    } else {
        ALPHA_DISABLED
    }
}

fun View.installTouchBasedClickListener(action: () -> Unit) {
    setOnTouchListener { v, event ->
        if (event.action == MotionEvent.ACTION_DOWN) {
            action()
            v.performClick()
            true
        } else {
            false
        }
    }
}

inline val View.layoutInflater: LayoutInflater
    get() = LayoutInflater.from(context)

inline val View.autofillManager: AutofillManager?
    get() = context.autofillManager

val View.isKeyboardVisible: Boolean
    get() = ViewCompat.getRootWindowInsets(this)
        ?.isVisible(
            WindowInsetsCompat.Type.ime()
        )
        ?: false

val DialogInterface?.lifecycleScope: CoroutineScope?
    get() = (this as? Dialog)
        ?.findViewById<View>(android.R.id.content)
        ?.lifecycleScope

inline val View.lifecycleScope: CoroutineScope?
    get() = findViewTreeLifecycleOwner()?.lifecycleScope

fun View?.isVisibleOnScreen(): Boolean =
    this?.takeIf { isShown }?.let {
        with(Rect()) {
            getGlobalVisibleRect(this)
            val screen = Rect(
                0,
                0,
                Resources.getSystem().displayMetrics.widthPixels,
                Resources.getSystem().displayMetrics.heightPixels
            )
            intersect(screen)
        }
    } ?: false

val View.isCompletelyVisible
    get() = with(Rect()) {
        getGlobalVisibleRect(this) &&
            height == height() &&
            width == width()
    }

private fun View.getInputMethodManager() =
    context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
