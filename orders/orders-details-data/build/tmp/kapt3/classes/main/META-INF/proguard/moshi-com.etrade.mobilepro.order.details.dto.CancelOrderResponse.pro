-if class com.etrade.mobilepro.order.details.dto.CancelOrderResponse
-keepnames class com.etrade.mobilepro.order.details.dto.CancelOrderResponse
-if class com.etrade.mobilepro.order.details.dto.CancelOrderResponse
-keep class com.etrade.mobilepro.order.details.dto.CancelOrderResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
