package com.etrade.mobilepro.dao

import com.etrade.mobilepro.common.result.ETResult

interface BaseDao<T, R> {

    fun get(key: String): T?
    fun getWhere(field: String, value: String): T?
    fun getAll(async: Boolean = false): R
    fun getAllIn(field: String, value: Array<String>, async: Boolean = false): R

    fun save(obj: T)
    fun saveAsync(obj: T): ETResult<Unit>
    fun saveAll(objList: List<T>)
    fun saveAllAsync(objList: List<T>): ETResult<Unit>

    fun delete(obj: T)
    fun deleteAll()
}
