package com.etrade.mobilepro.trade.presentation.form.delegate.dependencies

import com.etrade.mobilepro.trade.presentation.form.delegate.shared.PriceInputVisibilityProvider

internal interface FormFactoryDelegateDependencies : WithSaveAndPreviewDependencies, WithLimitOrderTermDependencies {
    val isLimitPriceInputVisible: PriceInputVisibilityProvider
    val isStopPriceInputVisible: PriceInputVisibilityProvider
    val isStopLimitRowVisible: PriceInputVisibilityProvider
    val allOrNoneValidPriceTypes: List<String>
    val limitPriceValidPriceTypes: List<String>
    val stopPriceValidPriceTypes: List<String>
    val priceTypePriceLabelMap: Map<String, String>
    val limitPriceLabel: String?
    val stopPriceLabel: String?
    val allOrNoneInvalidOrderTerms: List<String>
    val allowShortSellUpdatedTerms: Boolean
}

internal interface WithSaveAndPreviewDependencies {
    val saveAndPreviewActionRequiredInputs: Set<String>
}

internal interface WithLimitOrderTermDependencies {
    val limitOrderTermLabelMap: Map<String, String>
}
