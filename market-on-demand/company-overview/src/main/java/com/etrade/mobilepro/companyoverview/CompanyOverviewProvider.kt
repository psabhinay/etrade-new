package com.etrade.mobilepro.companyoverview

import androidx.lifecycle.LiveData
import com.etrade.mobilepro.mod.api.CompanyOverview
import com.etrade.mobilepro.util.Resource

interface CompanyOverviewProvider {

    fun createCompanyOverviewLiveData(symbol: String): LiveData<Resource<CompanyOverview>>

    fun createCompanySectorLiveData(symbol: String): LiveData<Resource<String?>>

    suspend fun companySector(symbol: String): String?
}
