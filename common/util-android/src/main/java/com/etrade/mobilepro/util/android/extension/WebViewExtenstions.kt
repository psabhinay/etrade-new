package com.etrade.mobilepro.util.android.extension

import android.webkit.WebView
import okhttp3.Call
import okhttp3.Callback
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import org.slf4j.Logger
import java.io.IOException

fun WebView.postUrl(
    url: String,
    postData: String,
    headers: Map<String, String>,
    okHttpClient: OkHttpClient? = null,
    logger: Logger? = null
): Call {
    val requestBase = Request.Builder()
        .url(url)
        .post(postData.toRequestBody("application/x-www-form-urlencoded".toMediaTypeOrNull()))
    headers.forEach { entry ->
        requestBase.addHeader(entry.key, entry.value)
    }
    val client = okHttpClient ?: OkHttpClient()
    val call = client.newCall(requestBase.build())
    call.enqueue(object : Callback {
        override fun onFailure(call: Call, e: IOException) {
            logger?.error("WebView post with headers failed for $url", e)
        }

        override fun onResponse(call: Call, response: Response) {
            val html = response.body?.string() ?: ""
            this@postUrl.post { loadDataWithBaseURL(url, html, "text/html", "utf-8", null) }
        }
    })
    return call
}
