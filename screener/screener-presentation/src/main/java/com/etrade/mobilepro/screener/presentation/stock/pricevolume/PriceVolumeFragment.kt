package com.etrade.mobilepro.screener.presentation.stock.pricevolume

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.checkablebutton.CheckableButton
import com.etrade.mobilepro.inputvalueview.InputFieldManager
import com.etrade.mobilepro.screener.api.ScreenerType
import com.etrade.mobilepro.screener.api.filter.stock.PriceVolumeFilter
import com.etrade.mobilepro.screener.presentation.R
import com.etrade.mobilepro.screener.presentation.ScreenersRouter
import com.etrade.mobilepro.screener.presentation.base.BaseFilterScreenFragment
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentPriceAndVolumeInputFieldsBinding
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentPriceAndVolumePerformanceIndustryBinding
import com.etrade.mobilepro.screener.presentation.databinding.PartialFragmentPriceAndVolumePerformanceSpBinding
import com.etrade.mobilepro.screener.presentation.stock.pricevolume.sharePriceInput.SharePriceFormatter
import com.etrade.mobilepro.screener.presentation.stock.pricevolume.sharePriceInput.SharePriceValidator
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

class PriceVolumeFragment @Inject constructor(
    viewModelFactory: ViewModelProvider.Factory,
    snackbarUtilFactory: SnackbarUtilFactory,
    screenersRouter: ScreenersRouter,
    tracker: Tracker
) : BaseFilterScreenFragment<PriceVolumeFilter, PriceVolumeViewModel.ViewState>(snackbarUtilFactory, screenersRouter, tracker) {

    private val performanceSp by viewBinding(PartialFragmentPriceAndVolumePerformanceSpBinding::bind)
    private val performanceIndustry by viewBinding(PartialFragmentPriceAndVolumePerformanceIndustryBinding::bind)
    private val inputFields by viewBinding(PartialFragmentPriceAndVolumeInputFieldsBinding::bind)
    override val viewModel by viewModels<PriceVolumeViewModel> { viewModelFactory }
    override val layoutRes: Int = R.layout.fragment_price_and_volume
    override val screenerGroup: ScreenerType.Group = ScreenerType.Group.STOCK
    override val checkableButtonsToEvents: Map<CheckableButton, ScreenerFilterEvent<PriceVolumeFilter, PriceVolumeViewModel.ViewState, Boolean>>
        get() = mapOf(
            performanceSp.priceSpLessMinus40 to PricePerformanceSP500.LESS_THAN_MINUS_40,
            performanceSp.priceSpMinus40ToMinus20 to PricePerformanceSP500.MINUS_40_TO_MINUS_20,
            performanceSp.priceSpMinus20ToZero to PricePerformanceSP500.MINUS_20_TO_0,
            performanceSp.priceSp0To20 to PricePerformanceSP500.ZERO_TO_20,
            performanceSp.priceSp20To40 to PricePerformanceSP500.TWENTY_TO_40,
            performanceSp.priceSpMoreThan40 to PricePerformanceSP500.GREATER_THAN_40,
            performanceIndustry.industryPriceSpLessMinus40 to PricePerformanceIndustry.LESS_THAN_MINUS_40,
            performanceIndustry.industryPriceSpMinus40ToMinus20 to PricePerformanceIndustry.MINUS_40_TO_MINUS_20,
            performanceIndustry.industryPriceSpMinus20ToZero to PricePerformanceIndustry.MINUS_20_TO_0,
            performanceIndustry.industryPriceSp0To20 to PricePerformanceIndustry.ZERO_TO_20,
            performanceIndustry.industryPriceSp20To40 to PricePerformanceIndustry.TWENTY_TO_40,
            performanceIndustry.industryPriceSpMoreThan40 to PricePerformanceIndustry.GREATER_THAN_40,
            performanceSp.fourWeeks to OverLastSP500.FOUR_WEEKS,
            performanceSp.thirteenWeeks to OverLastSP500.THIRTEEN_WEEKS,
            performanceSp.fiftyTwoWeeks to OverLastSP500.FIFTY_TWO_WEEKS,
            performanceIndustry.industryFourWeeks to OverLastIndustry.FOUR_WEEKS,
            performanceIndustry.industryThirteenWeek to OverLastIndustry.THIRTEEN_WEEKS,
            performanceIndustry.industryFiftyTwoWeeks to OverLastIndustry.FIFTY_TWO_WEEKS
        )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment()
        bindViewModel()
    }

    private fun setupFragment() {
        inputFields.greaterThanLayout.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = SharePriceFormatter(),
                validator = SharePriceValidator(getString(R.string.percent_validation_error_desc))
            ),
            rawContentData = viewModel.greaterThanSharePriceData
        )
        inputFields.lessThanLayout.setupView(
            dependencies = InputFieldManager.Dependencies(
                formatter = SharePriceFormatter(),
                validator = SharePriceValidator(getString(R.string.percent_validation_error_desc))
            ),
            rawContentData = viewModel.lessThanSharePriceData
        )
    }

    private fun bindViewModel() {
        viewModel.viewState.observe(
            viewLifecycleOwner,
            Observer {
                updatePricePerformanceSPBlock(viewState = it)
                updatePricePerformanceIndustryBlock(viewState = it)
            }
        )
        viewModel.greaterThanSharePriceData.observe(
            viewLifecycleOwner,
            Observer {
                viewModel.consumeEvent(SharePrice.GREATER_THAN, it)
            }
        )
        viewModel.lessThanSharePriceData.observe(
            viewLifecycleOwner,
            Observer {
                viewModel.consumeEvent(SharePrice.LESS_THAN, it)
            }
        )
    }

    private fun updatePricePerformanceSPBlock(viewState: PriceVolumeViewModel.ViewState) {
        performanceSp.apply {
            priceSpLessMinus40.isChecked = viewState.filter.pricePerformanceSpLessThanMinus40
            priceSpMinus40ToMinus20.isChecked = viewState.filter.pricePerformanceSpMinus40toMinus20
            priceSpMinus20ToZero.isChecked = viewState.filter.pricePerformanceSpMinus20to0
            priceSp0To20.isChecked = viewState.filter.pricePerformanceSpZeroTo20
            priceSp20To40.isChecked = viewState.filter.pricePerformanceSp20to40
            priceSpMoreThan40.isChecked = viewState.filter.pricePerformanceSpGreaterThan40
            fourWeeks.isChecked = viewState.filter.pricePerformanceSpOver4Weeks
            thirteenWeeks.isChecked = viewState.filter.pricePerformanceSpOver13Weeks
            fiftyTwoWeeks.isChecked = viewState.filter.pricePerformanceSpOver52Weeks
        }
    }

    @Suppress("FunctionMaxLength")
    private fun updatePricePerformanceIndustryBlock(viewState: PriceVolumeViewModel.ViewState) {
        performanceIndustry.apply {
            industryPriceSpLessMinus40.isChecked = viewState.filter.pricePerformanceIndustryLessThanMinus40
            industryPriceSpMinus40ToMinus20.isChecked = viewState.filter.pricePerformanceIndustryMinus40toMinus20
            industryPriceSpMinus20ToZero.isChecked = viewState.filter.pricePerformanceIndustryMinus20to0
            industryPriceSp0To20.isChecked = viewState.filter.pricePerformanceIndustryZeroTo20
            industryPriceSp20To40.isChecked = viewState.filter.pricePerformanceIndustry20to40
            industryPriceSpMoreThan40.isChecked = viewState.filter.pricePerformanceIndustryGreaterThan40
            industryFourWeeks.isChecked = viewState.filter.pricePerformanceIndustryOver4Weeks
            industryThirteenWeek.isChecked = viewState.filter.pricePerformanceIndustryOver13Weeks
            industryFiftyTwoWeeks.isChecked = viewState.filter.pricePerformanceIndustryOver52Weeks
        }
    }
}
