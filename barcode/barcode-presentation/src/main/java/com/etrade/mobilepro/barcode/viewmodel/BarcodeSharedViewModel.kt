package com.etrade.mobilepro.barcode.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import javax.inject.Inject

class BarcodeSharedViewModel @Inject constructor() : ViewModel() {

    private val _quoteSymbolLookupFound: MutableLiveData<ConsumableLiveEvent<String>> = MutableLiveData()
    private val _resetSwipeToDeleteActions: MutableLiveData<ConsumableLiveEvent<Any>> = MutableLiveData()

    val quoteSymbolLookupFound: LiveData<ConsumableLiveEvent<String>> = _quoteSymbolLookupFound
    val resetSwipeToDeleteActions: LiveData<ConsumableLiveEvent<Any>> = _resetSwipeToDeleteActions
    val focusToRecentBarcode: LiveData<ConsumableLiveEvent<Any>> = Transformations.map(_quoteSymbolLookupFound) {
        ConsumableLiveEvent(Any())
    }

    fun quoteSymbolLookupFound(symbol: String) {
        _quoteSymbolLookupFound.value = ConsumableLiveEvent(symbol)
    }

    fun resetSwipeToDeleteActions() {
        _resetSwipeToDeleteActions.value = ConsumableLiveEvent(Any())
    }
}
