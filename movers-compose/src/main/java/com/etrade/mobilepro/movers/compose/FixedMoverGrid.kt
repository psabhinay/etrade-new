package com.etrade.mobilepro.movers.compose

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import com.etrade.mobilepro.movers.MoverGridData
import com.etrade.mobilepro.movers.MoverItem
import com.etrade.mobilepro.movers.R

/**
 * rows = 2 max
 * columns = 3
 */
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun FixedMoverGrid(
    data: MoverGridData,
    contentPadding: PaddingValues = PaddingValues(dimensionResource(R.dimen.spacing_medium)),
    onTap: ((MoverItem) -> Unit)? = null
) {
    val items = data.items.sliceWithPadding()
    Column(modifier = Modifier.fillMaxWidth().padding(contentPadding)) {
        for (row in items) {
            Row(modifier = Modifier.fillMaxWidth()) {
                for (item in row) {
                    if (item != null) {
                        MoverCell(item, modifier = Modifier.weight(1f).clickable { onTap?.invoke(item) })
                    } else {
                        Spacer(modifier = Modifier.weight(1f))
                    }
                }
            }
        }
    }
}

private fun <T> List<T>.sliceWithPadding(sliceSize: Int = 3): List<List<T?>> {
    val result = mutableListOf<List<T?>>()
    var slice = mutableListOf<T?>()
    forEach { item ->
        slice.add(item)
        if (slice.size == sliceSize) {
            result.add(slice)
            slice = mutableListOf()
        }
    }
    if (slice.size < sliceSize) {
        for (i in slice.size until sliceSize) {
            slice.add(null)
        }
        result.add(slice)
    }
    return result
}
