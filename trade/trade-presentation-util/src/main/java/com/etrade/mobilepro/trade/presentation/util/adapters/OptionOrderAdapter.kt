package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.api.WithOrderRequest
import com.etrade.mobilepro.trade.presentation.util.OrderDisplayItem

internal class OptionOrderAdapter(
    private val resources: Resources,
    private val confirmedOrderAdapterDelegate: OrderAdapterDelegate,
    private val withCostOrderAdapterDelegate: OrderAdapterDelegate,
    private val withPriceOrderAdapterDelegate: OrderAdapterDelegate
) : OrderAdapter {

    override fun createOrderDisplayItem(order: Order): OrderDisplayItem {
        return OrderDisplayItem(
            label = createLabel(resources, order.legs, (order as? WithOrderRequest)?.orderRequest?.legDetails),
            sections = confirmedOrderAdapterDelegate.createOrderSectionItems(order) +
                listOfNotNull(accountItem(resources, order)) +
                withPriceOrderAdapterDelegate.createOrderSectionItems(order) +
                withCostOrderAdapterDelegate.createOrderSectionItems(order)
        )
    }
}
