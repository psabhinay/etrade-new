package com.etrade.mobilepro.home

import androidx.fragment.app.Fragment
import com.etrade.mobilepro.accounts.util.android.view.AccountViewType
import com.etrade.mobilepro.balance.BalancesFragment
import com.etrade.mobilepro.documents.DocumentsFragment
import com.etrade.mobilepro.dynamicui.api.CtaEvent.AccountsTabNavigation
import com.etrade.mobilepro.orders.OrdersFragment
import com.etrade.mobilepro.portfolio.PortfolioFragment
import com.etrade.mobilepro.stockplan.exercise.SellAndExerciseFragment
import com.etrade.mobilepro.stockplan.holdings.HoldingsFragment
import com.etrade.mobilepro.stockplan.orders.StockOrdersWebFragment
import com.etrade.mobilepro.transactions.view.TransactionsFragment

interface NavigateTabs {
    fun navigateToTab(destination: AccountsTabNavigation)
}

interface AccountTabNavigationListener {
    fun onAccountTabNavigated(additionalArgs: String?)
}

sealed class AccountTab(val title: String, val fragmentClass: Class<out Fragment>) {
    object ManagedOverview : AccountTab("Overview", ManagedOverviewFragment::class.java)
    object Portfolio : AccountTab("Portfolio", PortfolioFragment::class.java)
    object Balances : AccountTab("Balances", BalancesFragment::class.java)
    object Orders : AccountTab("Orders", OrdersFragment::class.java)
    object StockPlanOrders : AccountTab("Orders", StockOrdersWebFragment::class.java)
    object Transactions : AccountTab("Transactions", TransactionsFragment::class.java)
    object Holdings : AccountTab("Overview / Holdings", HoldingsFragment::class.java)
    object Exercise : AccountTab("Sell / Exercise", SellAndExerciseFragment::class.java)
    object Documents : AccountTab("Documents", DocumentsFragment::class.java)
}

fun createAccountListByAccountId(selectedAccountViewType: AccountViewType): List<AccountTab>? {
    return when (selectedAccountViewType) {
        AccountViewType.SINGLE_BROKERAGE_ACCOUNT_VIEW -> listOf(
            AccountTab.Portfolio,
            AccountTab.Balances,
            AccountTab.Orders,
            AccountTab.Transactions,
            AccountTab.Documents
        )
        AccountViewType.ESP_ACCOUNT_VIEW -> listOf(
            AccountTab.Holdings,
            AccountTab.Exercise,
            AccountTab.StockPlanOrders,
            AccountTab.Documents
        )
        AccountViewType.MANAGED_ACCOUNT_VIEW -> listOf(
            AccountTab.ManagedOverview,
            AccountTab.Orders,
            AccountTab.Balances,
            AccountTab.Transactions,
            AccountTab.Documents
        )
        else -> {
            null
        }
    }
}
