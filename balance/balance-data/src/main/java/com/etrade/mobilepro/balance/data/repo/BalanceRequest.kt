package com.etrade.mobilepro.balance.data.repo

data class BalanceRequest(
    val accountId: String,
    val userId: String
)
