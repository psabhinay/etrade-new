package com.etrade.mobilepro.news.presentation.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.news.presentation.databinding.ItemNetworkStateLoadingBinding

class NetworkStateItemViewHolder(
    private val binding: ItemNetworkStateLoadingBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bindTo(isLoading: Boolean) {
        binding.newsProgressBar.visibility = if (isLoading) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }
}
