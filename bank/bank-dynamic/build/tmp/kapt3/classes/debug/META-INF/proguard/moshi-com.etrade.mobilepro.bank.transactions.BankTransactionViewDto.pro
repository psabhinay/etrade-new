-if class com.etrade.mobilepro.bank.transactions.BankTransactionViewDto
-keepnames class com.etrade.mobilepro.bank.transactions.BankTransactionViewDto
-if class com.etrade.mobilepro.bank.transactions.BankTransactionViewDto
-keep class com.etrade.mobilepro.bank.transactions.BankTransactionViewDtoJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
