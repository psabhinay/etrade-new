package com.etrade.mobilepro.dropdown.expandableselect

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LiveData
import com.etrade.mobilepro.dropdown.DropDownSelector
import com.etrade.mobilepro.util.Resource
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory

/**
 * Dropdown selection component, similar to DropDownMultiSelector
 * It allows to represent tree structure with ability to expand/collapse nodes with subitems
 * Nodes with subitems cannot be selected themselves.
 * The component autoexpands its items tree to the selected item
 * No multiselection is supported
 */
interface DropDownExpandableSelector<T> : DropDownSelector {

    /**
     * Item for the component. Similar to DropDownMultiSelector.Selectable
     * @param isExpanded flag that defines is item expanded or not, this can and is changed by component depending of user actions or in case of autoexpand
     * @param subItems sub items, if null - the node is considered to be a leaf, also see state: State field
     * @param level level of nesting, it is managed by the component itself. 0 is top level, 1 is one level of nesting and so on. Affects UI.
     * @param ghostText defines text to be shown in the cell instead of title. This is used mainly for footers. If ghostText != null item cannot be selected
     */
    data class Selectable<T>(
        val title: String,
        val value: T,
        val selected: Boolean,
        val enabled: Boolean = true,
        val titleColor: Int? = null,
        val titleFormatter: ((T) -> String)? = null,
        val descriptionFormatter: ((T) -> String)? = null,
        val isExpanded: Boolean = false,
        val subItems: List<Selectable<T>>? = null,
        val level: Int = 0,
        val ghostText: String? = null,
        val contentDescriptionFormatter: ((T) -> String)? = null
    ) {
        enum class State { NONE, COLLAPSED, EXPANDED }
        val state: State = when {
            subItems != null && isExpanded -> State.EXPANDED
            subItems != null && !isExpanded -> State.COLLAPSED
            subItems == null -> State.NONE
            else -> State.NONE
        }

        /**
         * hashCode analogue which considers isExpanded field is always true. This is required by internal logic
         */
        val hashCodeWithoutExpandedState by lazy { if (isExpanded) { hashCode() } else { copy(isExpanded = true).hashCode() } }
    }

    /**
     * Settings for drop down expandable selector
     * @param tag - Tag for the selection fragment
     * @param title - title for the selection fragment
     * @param itemsData - LiveData with the list of items which will be displayed in the selection fragment
     * @param selectionFlow - Object with additional dependencies, it is used when we need some processing before or after selection
     * @param snackbarUtilFactory - Factory for creating snack bars in case of errors
     * @param onCancelListener is called when user closes drop down in any way(close button, swipe down), first param is passed to open() fallBackValues
     * @param selectListener - listener for selection event
     * @param actionButtonSettings - Object containing a title and action for the action button
     * @param loadingState - Object containing information about the loading state of the data
     * @param hideSingleSubItems - When true the component will hide subitems containing only one element, root
     */

    class Settings<T> (
        val tag: String,
        val title: String,
        val itemsData: LiveData<List<Selectable<T>>>,
        val selectionFlow: SelectionFlow<T>? = null,
        val snackbarUtilFactory: SnackbarUtilFactory? = null,
        val onCancelListener: ((Set<T>?) -> Unit)? = null,
        val selectListener: (Int, Selectable<T>) -> Unit,
        val actionButtonSettings: ActionButtonSettings? = null,
        val loadingState: LiveData<Resource<T>>? = null,
        val hideSingleSubItems: Boolean
    )

    /**
     * Additional group of non obligatory dependencies. This is used when some processing before or after selection is needed.
     * When this is presented in Settings, the flow of DropDownMultiSelector will be as the following:
     *  1. Fragment is shown, fragment subscribes to stageData. Depending on type of incoming Resource fragment updates its UI:
     *          Resource.Loading - list hides, progress bar shows
     *          Resource.Failed - list shows, snackbar shows(if snackbarUtilFactory was provided), progress bar hides
     *          Resource.Success with data == null - list shows, progress bar hides, can be thought of as "Idle" or "Reset" state
     *          Resource.Success with data != null - list shows, progress bar hides,
     *                                               selectListener is called.
     *                                               If multiSelectEnabled == false then fragment dismisses.
     *                                               didSelectListener is called
     *  2. User taps on the item in the list
     *  3. willSelectListener is called. Here some async operations can be started. It is a client responsibility to update stageData accordingly
     *      during this async operations
     *
     * @param willSelectListener is called when user taps on an item. Good place to start some async operations
     * @param didSelectListener is called after selectListener. Good place to dispose resources/subscriptions, update stageDate if needed
     * @param stageData indicator of DropDown ViewState, also carries data
     * @param onError is called when stageData brings Resource.Failed
     * @param onRetry is called when user presses snackBar retry button
     */
    class SelectionFlow<T> (
        val willSelectListener: (Int, Selectable<T>) -> Unit,
        val didSelectListener: (Int, Selectable<T>) -> Unit,
        val stageData: LiveData<Resource<Pair<Int, Selectable<T>>>>,
        val onError: ((Int, Selectable<T>) -> Unit)? = null,
        val onRetry: ((Int, Selectable<T>) -> Unit)? = null
    )

    /**
     * Additional group of optional settings for the action button. This is used when it is desired to display an action button at the bottom of the list
     *
     * @param actionButtonListener the action to call on button click
     * @param buttonTitle the display text for the button
     */
    class ActionButtonSettings(
        val buttonTitle: String,
        val actionButtonListener: (() -> Unit)
    )
}

class DefaultDropDownExpandableSelector<T> (
    private val supportFragmentManager: FragmentManager,
    private val settings: DropDownExpandableSelector.Settings<T>,
    private val fallBackValuesProvider: () -> Set<T>? = { null }
) : DropDownExpandableSelector<T> {

    init {
        // this class is config.change - dependent, so we need to (re)apply settings on its (re)creation
        applySettings(supportFragmentManager, settings, fallBackValuesProvider)
    }

    override fun open() {
        val dialogFragment = supportFragmentManager.findFragmentByTag(settings.tag) as? OverlayDropDownExpandableFragment ?: OverlayDropDownExpandableFragment()
        dialogFragment.applySettings(settings, fallBackValuesProvider.invoke())
        if (!dialogFragment.isAdded) {
            dialogFragment.show(supportFragmentManager, settings.tag)
        }
    }

    override fun close() {
        (supportFragmentManager.findFragmentByTag(settings.tag) as? OverlayDropDownExpandableFragment)?.dismiss()
    }

    companion object {

        fun <T> applySettings(
            fragmentManager: FragmentManager,
            settings: DropDownExpandableSelector.Settings<T>,
            fallBackValuesProvider: () -> Set<T>? = { null }
        ) {
            val dialogFragment = fragmentManager.findFragmentByTag(settings.tag) as? OverlayDropDownExpandableFragment
            dialogFragment?.applySettings(settings, fallBackValuesProvider.invoke())
        }
    }
}
