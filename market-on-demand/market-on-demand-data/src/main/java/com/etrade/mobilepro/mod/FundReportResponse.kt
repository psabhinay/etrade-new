package com.etrade.mobilepro.mod

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FundReportsResponseDto(
    @Json(name = "TotalMatches")
    val totalMatches: Int?,
    @Json(name = "Results")
    val results: List<FundReportDto>?
)

@JsonClass(generateAdapter = true)
data class FundReportDto(
    @Json(name = "Provider")
    val provider: String?,
    @Json(name = "ReportUrl")
    val reportUrl: String?,
    @Json(name = "ReportDate")
    val reportDate: String?
)
