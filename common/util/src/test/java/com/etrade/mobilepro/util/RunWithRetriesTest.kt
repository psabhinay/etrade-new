package com.etrade.mobilepro.util

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class RunWithRetriesTest {

    @Test
    fun testRetriesFail() {
        var retries = 0
        val result = runBlocking {
            runWithRetries(RunWithRetriesParam(retries = 5, maxAwaitTime = 1, delay = 1, jitterMultiplier = 1)) {
                retries = it
                throw Exception("Fail")
            }
        }
        assertTrue(result.isFailure)
        assertEquals(5, retries)
    }

    @Test
    fun testDefaultMaxRetries() {
        var retries = 0
        val result = runBlocking {
            runWithRetries(RunWithRetriesParam(maxAwaitTime = 1, delay = 1, jitterMultiplier = 1)) {
                retries = it
            }
        }

        assertTrue(result.isSuccess)
        assertEquals(1, retries)
    }

    @Test
    fun testFailAttemptAndSuccess() {
        var retries = 0
        val result = runBlocking {
            runWithRetries(RunWithRetriesParam(maxAwaitTime = 1, delay = 1, jitterMultiplier = 1)) {
                retries = it
                if (retries > 1) {
                    throw Exception("Fail")
                }
            }
        }
        assertTrue(result.isSuccess)
        assertEquals(1, retries)
    }
}
