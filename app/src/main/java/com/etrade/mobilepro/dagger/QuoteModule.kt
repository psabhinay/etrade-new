package com.etrade.mobilepro.dagger

import android.content.res.Resources
import androidx.annotation.ColorRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.R
import com.etrade.mobilepro.common.di.MobileTrade
import com.etrade.mobilepro.common.di.NegativeColor
import com.etrade.mobilepro.common.di.NeutralColor
import com.etrade.mobilepro.common.di.PositiveColor
import com.etrade.mobilepro.companyoverview.CachingCompanyOverviewProvider
import com.etrade.mobilepro.companyoverview.CompanyLookupDataDelegate
import com.etrade.mobilepro.companyoverview.CompanyOverviewProvider
import com.etrade.mobilepro.companyoverview.InMemoryCompanyLookupDataSourceDelegate
import com.etrade.mobilepro.compose.components.FragmentBasedComposable
import com.etrade.mobilepro.compose.components.FragmentBasedComposableImpl
import com.etrade.mobilepro.dagger.qualifiers.FragmentKey
import com.etrade.mobilepro.quote.mapper.DefaultFundQuoteDetailItemsMapper
import com.etrade.mobilepro.quote.repo.DefaultMobileQuoteRepo
import com.etrade.mobilepro.quote.repo.DefaultMutualFundsTradeQuotesRepo
import com.etrade.mobilepro.quote.restapi.MutualFundsTradeQuoteApiClient
import com.etrade.mobilepro.quote.restapi.QuoteApiClient
import com.etrade.mobilepro.quote.screen.presentation.QuoteTabsFragment
import com.etrade.mobilepro.quote.screen.presentation.QuoteTabsFragmentArgs
import com.etrade.mobilepro.quote.screen.presentation.qualifiers.QuoteOverview
import com.etrade.mobilepro.quote.screen.presentation.qualifiers.QuoteViewModelClass
import com.etrade.mobilepro.quote.search.history.GetRecentQuoteSearchHistoryUseCase
import com.etrade.mobilepro.quote.search.history.GetRecentQuoteSearchHistoryUseCaseImpl
import com.etrade.mobilepro.quoteapi.FundQuoteDetailItemsMapper
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.MutualFundsTradeQuotesRepo
import com.etrade.mobilepro.quoteapi.QuoteTemplateResources
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuoteUseCase
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuotesUseCase
import com.etrade.mobilepro.quoteapi.usecase.IsExtendedHoursOnUseCase
import com.etrade.mobilepro.quoteapi.usecase.TradeIsExtendedHoursOnUseCase
import com.etrade.mobilepro.quotes.QuoteViewModel
import com.etrade.mobilepro.quotes.usecase.GetMobileQuoteUseCaseImpl
import com.etrade.mobilepro.quotes.usecase.GetMobileQuotesUseCaseImpl
import com.etrade.mobilepro.quotes.usecase.GetPositionHoldingsUseCase
import com.etrade.mobilepro.quotes.usecase.GetPositionHoldingsUseCaseImpl
import com.etrade.mobilepro.quotes.usecase.GetQuoteDetailsUseCase
import com.etrade.mobilepro.quotes.usecase.GetQuoteDetailsUseCaseImpl
import com.etrade.mobilepro.quotes.usecase.GetQuoteNewsUseCase
import com.etrade.mobilepro.quotes.usecase.GetQuoteNewsUseCaseImpl
import com.etrade.mobilepro.quotes.usecase.IsExtendedHoursOnUseCaseImpl
import com.etrade.mobilepro.quotes.usecase.MapDualPaneMobileQuoteUseCase
import com.etrade.mobilepro.quotes.usecase.MapDualPaneMobileQuoteUseCaseImpl
import com.etrade.mobilepro.quotes.usecase.MapMobileQuoteUseCase
import com.etrade.mobilepro.quotes.usecase.MapMobileQuoteUseCaseImpl
import com.etrade.mobilepro.quotes.usecase.MapQuoteNewsUseCase
import com.etrade.mobilepro.quotes.usecase.MapQuoteNewsUseCaseImpl
import com.etrade.mobilepro.quotes.usecase.TradeIsExtendedHoursOnUseCaseImpl
import com.etrade.mobilepro.quotes.util.QuoteRouterStub
import com.etrade.mobilepro.searchingapi.items.SearchResultItem
import com.etrade.mobilepro.trade.router.QuoteRouter
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit
import javax.inject.Singleton
import kotlin.reflect.KClass

/* MINIMIZE A CHANCE OF MERGE CONFLICTS! KEEP METHODS IN THIS FILE ORGANIZED ALPHABETICALLY */
@Module(
    includes = [
        MarketOnDemandModule::class,
        StreamingModule::class
    ]
)
abstract class QuoteModule {

    @Binds
    @Singleton
    abstract fun bindCompanyLookupDataDelegate(delegate: InMemoryCompanyLookupDataSourceDelegate): CompanyLookupDataDelegate

    @Binds
    @Singleton
    abstract fun bindCompanyOverviewProvider(provider: CachingCompanyOverviewProvider): CompanyOverviewProvider

    @Binds
    @Singleton
    abstract fun bindFundQuoteDetailItemsMapper(mapper: DefaultFundQuoteDetailItemsMapper): FundQuoteDetailItemsMapper

    @Binds
    @Singleton
    abstract fun bindGetMobileQuotesUseCase(useCase: GetMobileQuotesUseCaseImpl): GetMobileQuotesUseCase

    @Binds
    @Singleton
    abstract fun bindGetMobileQuoteUseCase(useCase: GetMobileQuoteUseCaseImpl): GetMobileQuoteUseCase

    @Binds
    @Singleton
    abstract fun bindGetPositionHoldingsUseCase(useCase: GetPositionHoldingsUseCaseImpl): GetPositionHoldingsUseCase

    @Binds
    @Singleton
    abstract fun bindGetQuoteDetailsUseCase(useCase: GetQuoteDetailsUseCaseImpl): GetQuoteDetailsUseCase

    @Binds
    @Singleton
    abstract fun bindGetQuoteNewsUseCase(useCase: GetQuoteNewsUseCaseImpl): GetQuoteNewsUseCase

    @Binds
    @Singleton
    abstract fun bindGetQuoteSearchHistoryUseCase(useCase: GetRecentQuoteSearchHistoryUseCaseImpl): GetRecentQuoteSearchHistoryUseCase

    @Binds
    @Singleton
    abstract fun bindIsExtendedHoursOnUseCase(useCase: IsExtendedHoursOnUseCaseImpl): IsExtendedHoursOnUseCase

    @Binds
    @Singleton
    abstract fun bindMapMobileQuoteUseCase(useCase: MapMobileQuoteUseCaseImpl): MapMobileQuoteUseCase

    @Binds
    @Singleton
    abstract fun bindMapDualPaneMobileQuoteUseCase(useCase: MapDualPaneMobileQuoteUseCaseImpl): MapDualPaneMobileQuoteUseCase

    @Binds
    @Singleton
    abstract fun bindMapQuoteNewsUseCase(useCase: MapQuoteNewsUseCaseImpl): MapQuoteNewsUseCase

    @Binds
    @Singleton
    abstract fun bindMobileQuoteRepo(repo: DefaultMobileQuoteRepo): MobileQuoteRepo

    @Binds
    @Singleton
    abstract fun bindMutualFundsTradeQuoteRepo(fundsQuoteTradeRepo: DefaultMutualFundsTradeQuotesRepo): MutualFundsTradeQuotesRepo

    @Binds
    abstract fun bindQuoteRouter(quoteRouterStub: QuoteRouterStub): QuoteRouter

    @Binds
    @IntoMap
    @FragmentKey(QuoteTabsFragment::class)
    abstract fun bindQuoteTabsFragment(fragment: QuoteTabsFragment): Fragment

    @Binds
    @Singleton
    abstract fun bindTradeIsExtendedHoursOnUseCase(useCase: TradeIsExtendedHoursOnUseCaseImpl): TradeIsExtendedHoursOnUseCase

    @Binds
    abstract fun bindsFragmentBasedComposable(impl: FragmentBasedComposableImpl): FragmentBasedComposable

    companion object {

        @Provides
        @Singleton
        internal fun provideFundsApiClient(
            @MobileTrade retrofit: Retrofit
        ): MutualFundsTradeQuoteApiClient = retrofit.create(MutualFundsTradeQuoteApiClient::class.java)

        @Provides
        @Singleton
        internal fun provideQuoteApiClient(@MobileTrade retrofit: Retrofit): QuoteApiClient = retrofit.create(QuoteApiClient::class.java)

        @Suppress("LongMethod")
        @Provides
        internal fun quoteTemplateResources(
            resources: Resources,
            @ColorRes @PositiveColor positiveColor: Int,
            @ColorRes @NegativeColor negativeColor: Int,
            @ColorRes @NeutralColor neutralColor: Int
        ): QuoteTemplateResources {
            return QuoteTemplateResources(
                valueMissing = resources.getString(R.string.empty_default),
                titleFormat = resources.getString(R.string.title_subtitle_format),
                asOfDateTimeFormat = resources.getString(R.string.as_of_time),
                nasdaqAsOfDateTimeFormat = resources.getString(R.string.nasdaq_as_of_time),
                positiveChangeColorRes = positiveColor,
                negativeChangeColorRes = negativeColor,
                neutralChangeColorRes = neutralColor,
                dateFormat = resources.getString(R.string.quote_widget_date_format),
                changeFormat = resources.getString(R.string.quote_widget_change_format),
                peFormat = resources.getString(R.string.quote_widget_pe_format),
                closingPrice = resources.getString(R.string.closing_price_title),
                underlyerClosingPrice = resources.getString(R.string.underlyer_closing_price_title),
                underlyerPrice = resources.getString(R.string.underlyer_price_title),
                fundClassTitle = resources.getString(R.string.quote_fund_class),
                fundDescriptionFormat = resources.getString(R.string.mf_description_body),
                fundDescriptionNotFound = resources.getString(R.string.mf_quote_description_not_found),
                fundPerformanceCategoryFormat = resources.getString(R.string.fund_category_format),
                priceFormat = resources.getString(R.string.quote_content_description_trade_price),
                sizeFormat = resources.getString(R.string.quote_content_description_trade_size),
                contentDescriptionFormat = resources.getString(R.string.quote_content_description_quote_summary)
            )
        }

        @Provides
        fun provideQuoteTabsFragmentArgsDelegate() = object : QuoteTabsFragment.ArgsDelegate {
            private fun QuoteTabsFragment.getArgumentBundle() = requireArguments().let(QuoteTabsFragmentArgs::fromBundle)

            override fun extractSymbol(fragment: QuoteTabsFragment): SearchResultItem.Symbol {
                return fragment.getArgumentBundle().quoteTicker
            }
        }

        @Provides
        @QuoteOverview
        fun provideFragmentBasedComposableOverviewParams() = FragmentBasedComposable.Params(
            layoutResId = R.layout.view_quote_overview,
            fragmentContainerViewId = R.id.fragmentContainerView
        )

        @Provides
        @QuoteViewModelClass
        fun provideQuoteViewModelClass(): KClass<out ViewModel> = QuoteViewModel::class
    }
}
