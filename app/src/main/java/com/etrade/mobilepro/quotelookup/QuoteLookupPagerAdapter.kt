package com.etrade.mobilepro.quotelookup

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.etrade.mobilepro.util.android.extension.instantiateFragment

class QuoteLookupPagerAdapter(
    private val quoteActivity: FragmentActivity,
    private val items: List<QuoteLookupTab>,
    private val bundle: Bundle?
) : FragmentStateAdapter(quoteActivity) {

    override fun createFragment(position: Int): Fragment {
        val fragmentClass = items[position].fragmentClass
        return quoteActivity.supportFragmentManager.fragmentFactory.instantiateFragment(fragmentClass, bundle)
    }

    fun getPageTitle(position: Int): String? {
        return items[position].title?.let { quoteActivity.getString(it) }
    }

    override fun getItemCount(): Int = items.size
}
