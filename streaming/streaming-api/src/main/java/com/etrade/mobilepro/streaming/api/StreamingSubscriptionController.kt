package com.etrade.mobilepro.streaming.api

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.Symbol
import io.reactivex.Observable

/**
 * Implementation of this interface encapsulates all logic related to symbol streaming subscription
 * and should be used within ViewModel object.
 */
interface StreamingSubscriptionController {

    /**
     * Initializes and starts Quote streaming subscription.
     */
    fun initSubscription(
        fieldsProvider: (InstrumentType) -> Set<Level1Field>,
        symbolsSource: Observable<Set<Symbol>>,
        onUpdateCallback: (symbol: String, instrumentType: InstrumentType, data: Level1Data) -> Unit
    )

    /**
     * Resumes Quote streaming subscription if it was previously paused.
     */
    fun resumeSubscription()

    /**
     * Pauses Quote streaming subscription with further ability to resume it.
     */
    fun pauseSubscription()

    /**
     * Cancels Quote streaming subscription without ability to resume it.
     */
    fun cancelSubscription()
}
