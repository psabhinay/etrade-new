package com.etrade.mobilepro.screener.presentation.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.screener.api.ScreenerItem
import com.etrade.mobilepro.screener.presentation.databinding.ItemScreenerSetBinding

class ScreenerSetViewHolder(
    private val binding: ItemScreenerSetBinding,
    private val screenerSetSelectedListener: (ScreenerItem.ScreenerSet) -> Unit,
    private val screenerSetClearListener: (ScreenerItem.ScreenerSet) -> Unit
) : RecyclerView.ViewHolder(binding.root) {
    private var item: ScreenerItem.ScreenerSet? = null

    init {
        binding.rootLayout.setOnClickListener {
            item?.let {
                screenerSetSelectedListener.invoke(it)
            }
        }
        binding.clearBtn.setOnClickListener {
            item?.let {
                screenerSetClearListener.invoke(it)
            }
        }
    }

    fun bindTo(item: ScreenerItem.ScreenerSet) {
        this.item = item
        binding.screenerTitle.text = item.title
        binding.clearBtn.visibility = if (item.isEmpty()) { View.GONE } else { View.VISIBLE }
    }
}
