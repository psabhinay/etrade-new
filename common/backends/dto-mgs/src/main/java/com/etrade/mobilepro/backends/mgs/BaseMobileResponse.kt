package com.etrade.mobilepro.backends.mgs

import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BaseMobileResponse(
    @Json(name = "mobile_response")
    val screen: MobileScreen
) : BaseDataDto()
