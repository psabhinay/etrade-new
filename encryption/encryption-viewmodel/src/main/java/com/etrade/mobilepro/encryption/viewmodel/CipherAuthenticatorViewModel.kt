package com.etrade.mobilepro.encryption.viewmodel

import javax.crypto.Cipher

/**
 * Marks a view model that is responsible for Cipher's authentication.
 */
interface CipherAuthenticatorViewModel {

    /**
     * Authenticates a [cipher] using [authenticate] method.
     *
     * The contract of this method requires to call one of these callbacks: [onCipherAuthenticated], [onCipherAuthenticationCancelled] or
     * [onCipherAuthenticationFailed].
     */
    suspend fun authenticate(cipher: Cipher, authenticate: (Cipher) -> Unit): Cipher

    /**
     * A callback that must be invoked after a cipher's authentication is succeeded.
     */
    fun onCipherAuthenticated(cipher: Cipher)

    /**
     * A callback tha must be invoked after a cipher's authentication got cancelled by a user.
     */
    fun onCipherAuthenticationCancelled()

    /**
     * A callback tha must be invoked after a cipher's authentication failed with an error.
     */
    fun onCipherAuthenticationFailed(cause: Throwable)
}
