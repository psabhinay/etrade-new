package com.etrade.mobilepro.quoteapi.usecase

import com.etrade.mobilepro.util.UseCase

interface TradeIsExtendedHoursOnUseCase : UseCase<Unit, Boolean>
