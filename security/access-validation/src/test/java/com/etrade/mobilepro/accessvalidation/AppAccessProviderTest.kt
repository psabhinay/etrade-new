package com.etrade.mobilepro.accessvalidation

import android.content.res.Resources
import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.etrade.mobilepro.androidconfig.api.repo.AndroidConfigRepo
import com.etrade.mobilepro.androidconfig.data.dto.AndroidConfigInfoDto
import com.etrade.mobilepro.applicationinfo.api.ApplicationInfo
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.testutil.XmlDeserializer
import com.etrade.mobilepro.util.android.DeviceRootChecker
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineScope
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.robolectric.util.ReflectionHelpers

@ExperimentalCoroutinesApi
class AppAccessProviderTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testScope = TestCoroutineScope()

    private val accessGranted: () -> Unit = mock()
    private val resources: Resources = mock<Resources>().apply {
        whenever(getString(any())).thenAnswer { invocation ->
            invocation.arguments[0].toString()
        }
    }

    private fun applicationInfo(versionCode: String): ApplicationInfo = mock {
        on { appVersionCode() } doReturn versionCode
    }

    private fun rootChecker(isRooted: Boolean): DeviceRootChecker = mock {
        on { isRooted() } doReturn isRooted
    }

    private fun getMockConfigDto(): AndroidConfigInfoDto {
        return XmlDeserializer.getObjectFromXml("android.xml", AndroidConfigInfoDto::class.java)
    }

    private fun androidConfigRepo(mockConfigInfoDto: AndroidConfigInfoDto): AndroidConfigRepo =
        mockk {
            coEvery { getAndroidConfiguration() } returns mockConfigInfoDto
        }

    @Test
    fun `Access Granted when no force update and device not rooted`() {
        // Given
        val sut = createAppAccessProvider()

        // When
        sut.requestAccess(testScope, accessGranted)

        // Then
        verify(accessGranted, times(1)).invoke()
    }

    @Test
    fun `If force update then Access Granted method never invoked and update dialog shown`() {
        // Given
        val sut = createAppAccessProvider(versionCode = "90200")

        // When
        sut.requestAccess(testScope, accessGranted)

        // Then
        sut.dialog.observeForever {
            assertFalse(it?.isConsumed == true)
            it?.peekContent()?.assertValidDialog(
                messageRes = R.string.access_validation_force_update_message.toString(),
                labelRes = R.string.access_validation_to_android_store.toString()
            )
        }

        verify(accessGranted, never()).invoke()
    }

    @Test
    fun `If device rooted then Access Granted method never invoked and rooted device dialog shown`() {
        // Given
        ReflectionHelpers.setStaticField(Build::class.java, "BRAND", "Samsung")
        ReflectionHelpers.setStaticField(Build::class.java, "DEVICE", "S21")
        ReflectionHelpers.setStaticField(Build::class.java, "FINGERPRINT", "")
        ReflectionHelpers.setStaticField(Build::class.java, "HARDWARE", "")
        ReflectionHelpers.setStaticField(Build::class.java, "MODEL", "")
        ReflectionHelpers.setStaticField(Build::class.java, "MANUFACTURER", "")
        ReflectionHelpers.setStaticField(Build::class.java, "PRODUCT", "")
        ReflectionHelpers.setStaticField(BuildConfig::class.java, "DEBUG", false)
        val sut = createAppAccessProvider(isRooted = true)

        // When
        sut.requestAccess(testScope, accessGranted)

        // Then
        sut.dialog.observeForever {
            assertFalse(it?.isConsumed == true)
            it?.peekContent()?.assertValidDialog(
                messageRes = R.string.access_validation_root_warning_message.toString(),
                labelRes = R.string.ok.toString()
            )
        }

        verify(accessGranted, never()).invoke()
    }

    private fun AppDialog.assertValidDialog(messageRes: String, labelRes: String) {
        assertEquals(messageRes, message)
        assertEquals(labelRes, positiveAction?.label)
    }

    private fun createAppAccessProvider(
        configDto: AndroidConfigInfoDto = getMockConfigDto(),
        versionCode: String = "90400",
        isRooted: Boolean = false
    ) =
        DefaultAppAccessProvider(
            androidConfigRepo = androidConfigRepo(configDto),
            applicationInfo = applicationInfo(versionCode),
            rootChecker = rootChecker(isRooted),
            resources = resources
        )
}
