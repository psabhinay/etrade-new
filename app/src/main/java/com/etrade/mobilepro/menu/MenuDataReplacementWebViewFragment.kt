package com.etrade.mobilepro.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.etrade.mobilepro.common.toolbarTitleView
import com.etrade.mobilepro.util.android.extension.loadFormattedData
import org.chromium.customtabsclient.shared.R
import javax.inject.Inject

const val REPLACEMENT_DATA = "REPLACEMENT_DATA"

class MenuDataReplacementWebViewFragment @Inject constructor() : Fragment() {

    private val menuWebViewFragmentArgs: MenuDataReplacementWebViewFragmentArgs by navArgs()
    private val replacementDataBundle: Bundle? by lazy { menuWebViewFragmentArgs.replacementData }
    private val htmlResourceId: Int by lazy { menuWebViewFragmentArgs.htmlResourceId }
    private val title: String by lazy { menuWebViewFragmentArgs.title ?: "" }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_data_replacement_webview, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.toolbarTitleView?.text = title
        if (title.isNotEmpty()) {
            activity?.toolbarTitleView?.contentDescription = getString(R.string.label_header_description, title)
        }

        @Suppress("UNCHECKED_CAST")
        val replacementData = replacementDataBundle?.getSerializable(REPLACEMENT_DATA) as? Map<String, String>
        val webView = view.findViewById<WebView>(R.id.web_view)
        webView.loadFormattedData(htmlResourceId, replacementData)
    }
}
