package com.etrade.mobilepro.timeout.widget

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class SharedSessionTerminatedViewModel @Inject constructor() : ViewModel() {

    private val _showSessionTerminatedPage: MutableLiveData<Pair<Boolean, (() -> Unit)?>> = MutableLiveData()
    val showSessionTerminatedPage: LiveData<Pair<Boolean, (() -> Unit)?>>
        get() = _showSessionTerminatedPage

    fun showSessionTerminatedPage(callbacks: (() -> Unit)?) {
        _showSessionTerminatedPage.value = true to callbacks
    }

    fun hideSessionTerminatedPage() {
        _showSessionTerminatedPage.value = false to null
    }
}
