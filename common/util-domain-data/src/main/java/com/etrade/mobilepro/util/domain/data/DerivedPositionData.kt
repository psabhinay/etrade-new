package com.etrade.mobilepro.util.domain.data

import java.math.BigDecimal
import java.math.RoundingMode

/**
 * Util class for deriving user-specific data to display when streaming is enabled
 */

@Suppress("LongParameterList", "LongMethod")
fun deriveDaysGain(
    streamingLastPriceDecimal: BigDecimal,
    previousClosePriceDecimal: BigDecimal,
    quantityDecimal: BigDecimal,
    todayQuantityDecimal: BigDecimal? = null,
    todayPricePaidDecimal: BigDecimal? = null,
    todayCommissionDecimal: BigDecimal? = null,
    feesDecimal: BigDecimal? = null,
    /* Price paid for positions bought yesterday or before a.k.a costBasis */
    /* Cost basis - Price paid for the positions in its entirety including today quantity */
    basisPrice: BigDecimal? = null
): Gain? {
    val sign = if (quantityDecimal < BigDecimal.ZERO) NEGATIVE_ONE else BigDecimal.ONE

    todayQuantityDecimal?.takeIf { it.compareTo(BigDecimal.ZERO) != 0 }?.let { todayQty ->
        // If todayQuantity != 0 but any of these can't parse, an invalid set of arguments was given to this function
        if (todayPricePaidDecimal == null || todayCommissionDecimal == null || feesDecimal == null) {
            return null
        }
        return calculateGainForTodayQuantity(
            todayPricePaidDecimal,
            todayCommissionDecimal,
            feesDecimal,
            quantityDecimal,
            todayQty,
            streamingLastPriceDecimal,
            previousClosePriceDecimal,
            basisPrice,
            sign
        )
    }

    if (previousClosePriceDecimal.compareTo(BigDecimal.ZERO) == 0) return null

    val marketPriceDecimal = streamingLastPriceDecimal - previousClosePriceDecimal
    val daysGainDollar: BigDecimal = marketPriceDecimal * quantityDecimal
    val daysGainPercentage: BigDecimal = streamingLastPriceDecimal
        .subtract(previousClosePriceDecimal)
        .divide(previousClosePriceDecimal, DIVIDE_SCALE, RoundingMode.HALF_UP)
        .multiply(BigDecimal(PERCENT_MULTIPLIER)).times(sign)

    return Gain(dollar = daysGainDollar, percent = daysGainPercentage)
}

@Suppress("LongParameterList")
private fun calculateGainForTodayQuantity(
    todayPricePaidDecimal: BigDecimal,
    todayCommissionDecimal: BigDecimal,
    feesDecimal: BigDecimal,
    quantityDecimal: BigDecimal,
    todayQty: BigDecimal,
    streamingLastPriceDecimal: BigDecimal,
    previousClosePriceDecimal: BigDecimal,
    basisPrice: BigDecimal?,
    sign: BigDecimal
): Gain {

    val prevQuantity = quantityDecimal - todayQty

    // Market value for positions / lots bought yesterday or before.
    val existingMarketValue = (streamingLastPriceDecimal - previousClosePriceDecimal) * prevQuantity

    val daysGainDollar = (streamingLastPriceDecimal - todayPricePaidDecimal) * todayQty - todayCommissionDecimal - feesDecimal + existingMarketValue

    val costBasis = if (prevQuantity.compareTo(BigDecimal.ZERO) == 0) {
        todayPricePaidDecimal
    } else {
        basisPrice ?: todayPricePaidDecimal
    }

    val divisor = costBasis * quantityDecimal * sign
    val daysGainPercentage = if (divisor.compareTo(BigDecimal.ZERO) == 0) {
        divisor
    } else {
        daysGainDollar.divide(divisor, DIVIDE_SCALE, RoundingMode.HALF_UP).times(BigDecimal(PERCENT_MULTIPLIER))
    }

    return Gain(dollar = daysGainDollar, percent = daysGainPercentage)
}

data class Gain(val dollar: BigDecimal, val percent: BigDecimal)

fun deriveMarketValue(price: BigDecimal, quantity: BigDecimal): BigDecimal = price * quantity

fun deriveDaysGainPercentage(
    daysGain: BigDecimal,
    marketValue: BigDecimal
): BigDecimal? {
    val closingMarketValue = marketValue - daysGain
    return if (closingMarketValue.compareTo(BigDecimal.ZERO) == 0) {
        null
    } else {
        daysGain
            .divide(closingMarketValue.abs(), DIVIDE_SCALE, RoundingMode.HALF_UP)
            .multiply(BigDecimal(PERCENT_MULTIPLIER))
    }
}
