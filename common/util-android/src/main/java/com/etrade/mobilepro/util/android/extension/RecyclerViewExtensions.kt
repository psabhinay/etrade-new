package com.etrade.mobilepro.util.android.extension

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.etrade.mobilepro.util.android.recyclerviewutil.RecyclerViewItemDivider

/**
 * Adds divider item decoration that draws divider under every item, including the last one.
 *
 * @param drawableResId drawable resource to use for drawing a divider
 */
fun RecyclerView.addDividerItemDecoration(
    @DrawableRes drawableResId: Int,
    isVerticalOrientation: Boolean = true
) {
    withDrawable(context, drawableResId) {
        addItemDecoration(
            DividerItemDecoration(
                context,
                if (isVerticalOrientation) {
                    DividerItemDecoration.VERTICAL
                } else {
                    DividerItemDecoration.HORIZONTAL
                }
            ).apply {
                setDrawable(it)
            }
        )
    }
}

fun RecyclerView.clearItemDecorations() {
    if (itemDecorationCount > 0) {
        for (i in itemDecorationCount - 1 downTo 0) {
            removeItemDecorationAt(i)
        }
    }
}

/**
 * Adds divider item decoration that draws divider under every item except for the last one.
 *
 * @param drawableResId drawable resource to use for drawing a divider
 * @param isVerticalOrientation `true` if [RecyclerView] items are arranged vertically
 */
fun RecyclerView.setupRecyclerViewDivider(
    @DrawableRes drawableResId: Int,
    isVerticalOrientation: Boolean = true,
    countOfLastItemsWithoutDivider: Int = 1,
    excludeIndexList: List<Int>? = null
) {
    withDrawable(context, drawableResId) {
        addItemDecoration(RecyclerViewItemDivider(it, isVerticalOrientation, countOfLastItemsWithoutDivider, excludeIndexList))
    }
}

inline fun withDrawable(context: Context, drawableResId: Int, block: (Drawable) -> Unit) {
    ContextCompat.getDrawable(context, drawableResId)?.also(block)
}

/**
 * Call this method if underlying data of the adapter had changed.
 *
 * @param oldItems old items
 * @param newItems new items
 * @param callback optional callback
 */
fun RecyclerView.Adapter<*>.dispatchUpdates(oldItems: List<*>, newItems: List<*>, callback: DiffUtil.Callback = AnyListDiffCallback(oldItems, newItems)) {
    DiffUtil.calculateDiff(callback).dispatchUpdatesTo(this)
}

/**
 * Allows easily to calculate diff between two lists. Compares items using equals method.
 */
private class AnyListDiffCallback(private val oldList: List<*>, private val newList: List<*>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean = oldList[oldItemPosition] == newList[newItemPosition]
}
