package com.etrade.mobilepro.quote.screen.api

interface TabVisibilityListener {
    fun onVisibilityChange(isVisible: Boolean)
}
