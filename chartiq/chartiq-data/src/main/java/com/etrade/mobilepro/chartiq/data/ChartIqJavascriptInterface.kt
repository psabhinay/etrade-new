package com.etrade.mobilepro.chartiq.data

import android.webkit.JavascriptInterface
import com.etrade.mobilepro.chartiq.data.dto.AlertDialogContentDto
import com.etrade.mobilepro.chartiq.data.dto.ChartServiceRequestDto
import com.etrade.mobilepro.chartiq.data.dto.SymbolDto
import com.etrade.mobilepro.chartiq.data.dto.mapping.toAlertDialogContent
import com.etrade.mobilepro.chartiq.data.dto.mapping.toOrderDetails
import com.etrade.mobilepro.instrument.Symbol
import com.etrade.mobilepro.orders.data.dto.OrderItemDto
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import org.slf4j.LoggerFactory
import java.io.IOException
import java.lang.reflect.Type

const val JAVASCRIPT_INTERFACE_NAME = "ChartIQInterface"

/**
 * This class is designed to be an intermediate layer to provide communication with ChartIQ library via JavaScript bridge.
 * Methods marked with @JavascriptInterface annotations provide the output from the library as callbacks notifying the user of certain
 * ChartIQ lifecycle state changes and actions needed to be taken.
 */
class ChartIqJavascriptInterface(private val moshi: Moshi, private val chartDelegate: ChartDelegate) {

    private val logger = LoggerFactory.getLogger(ChartIqJavascriptInterface::class.java)

    /**
     * Callback used to notify that user is trying to access content designed for authenticated users only and so the user has to login first.
     */
    @JavascriptInterface
    fun showLoginScreen() {
        logger.debug("JS output: showLoginScreen")
        chartDelegate.showLogin()
    }

    /**
     * Callback from JavaScript code notifying that chart need to be displayed.
     */
    @JavascriptInterface
    fun showChart() {
        logger.debug("JS output: showChart")
        chartDelegate.showChart()
    }

    /**
     * Callback used to show simple text message
     */
    @JavascriptInterface
    fun showToast(toast: String?) {
        logger.debug("JS output: showToast: $toast")
        toast?.let { chartDelegate.showMessage(it) }
    }

    /**
     * Callback from JavaScript code to show native Alert Dialog designed to provide interaction with the user.
     * The response back should be provided via #onAlertCallback method.
     */
    @JavascriptInterface
    fun showAlertDialog(messageJson: String?) {
        logger.debug("JS output: showAlertDialog $messageJson")
        val json = messageJson ?: return
        val jsonAdapter = moshi.adapter(AlertDialogContentDto::class.java)
        try {
            val dto = jsonAdapter.fromJson(json)
            dto?.toAlertDialogContent()?.let { content ->
                chartDelegate.showAlertDialog(content)
            }
        } catch (e: IOException) {
            logger.error("Unable to show AlertDialog", e)
        }
    }

    /**
     * Callback from the JavaScript code to notify that ChartIQ is loaded and WebView is ready for configuration.
     */
    @JavascriptInterface
    fun webviewReady() {
        logger.debug("JS output: webviewReady")
        chartDelegate.onWebViewReady()
    }

    /**
     * Callback to notify that user clicked on the settings button.
     * Since the callback doesn't provide the current status of the settings page it has to be tracked separately.
     */
    @JavascriptInterface
    fun chartSettingsClicked() {
        logger.debug("JS output: chartSettingsClicked")
        chartDelegate.onChartSettingsClicked()
    }

    /**
     * Callback to notify that user requested symbol lookup.
     */
    @JavascriptInterface
    fun symbolLookup() {
        logger.debug("JS output: symbolLookup")
        chartDelegate.onSymbolLookup()
    }

    /**
     * Callback used to notify that user has selected symbols to compare
     */
    @JavascriptInterface
    fun compareSymbols(jsonString: String) {
        logger.debug("JS output: compareSymbols $jsonString")
        try {
            val listType: Type = Types.newParameterizedType(List::class.java, SymbolDto::class.java)
            val adapter: JsonAdapter<List<SymbolDto>> = moshi.adapter(listType)
            val symbols = adapter.fromJson(jsonString) ?: emptyList()
            chartDelegate.onSymbolsToCompareUpdated(symbols.map { Symbol(it) })
        } catch (e: IOException) {
            logger.error("Unable to parse symbols to compare", e)
        }
    }

    /**
     * Callback used to notify that user has selected an order to show
     */
    @JavascriptInterface
    fun showOrderDetails(jsonString: String) {
        logger.debug("JS output: showOrderDetails $jsonString")
        val jsonAdapter = moshi.adapter(OrderItemDto::class.java)
        try {
            val dto = jsonAdapter.fromJson(jsonString)
            dto?.toOrderDetails()?.let {
                chartDelegate.showOrderDetails(it)
            }
        } catch (e: IOException) {
            logger.error("Unable to show order details", e)
        }
    }

    /**
     * Callback used to make a service call on android side
     */
    @JavascriptInterface
    fun makeRequest(jsonString: String) {
        val jsonAdapter = moshi.adapter(ChartServiceRequestDto::class.java)
        try {
            val dto = jsonAdapter.fromJson(jsonString)
            dto?.let {
                chartDelegate.makeServiceCall(it.requestId, it.query, it.requestBody, it.requestType)
            }
        } catch (e: IOException) {
            logger.error("Unable to make request")
        }
    }
}
