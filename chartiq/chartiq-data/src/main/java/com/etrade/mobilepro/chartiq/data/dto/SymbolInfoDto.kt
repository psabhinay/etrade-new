package com.etrade.mobilepro.chartiq.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class SymbolInfoDto(

    @Json(name = "symbol")
    val ticker: String,

    @Json(name = "securityType")
    val securityType: String,

    @Json(name = "extendedHours")
    val isExtendedHours: Toggle?,

    @Json(name = "streaming")
    val isStreaming: Toggle?,

    @Json(name = "streamingLastPrice")
    val price: PriceDto
)

@JsonClass(generateAdapter = true)
internal data class PriceDto(

    @Json(name = "price")
    val price: String
)
