package com.etrade.mobilepro.tooltip

open class AppTooltip(
    val message: String,
    val gravity: TooltipGravity = TooltipGravity.BOTTOM,
    val shouldConsumeDismissEvent: Boolean = true,
    val onExplicitDismiss: (() -> Unit)? = null,
    val onImplicitDismiss: (() -> Unit)? = null
)
