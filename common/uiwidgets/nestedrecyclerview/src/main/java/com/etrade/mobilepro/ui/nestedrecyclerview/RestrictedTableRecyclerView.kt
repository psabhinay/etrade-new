package com.etrade.mobilepro.ui.nestedrecyclerview

import android.content.Context
import android.util.AttributeSet
import com.evrencoskun.tableview.TableRecyclerView

/**
 * This recycler view delegates vertical scrolling to its child, if it supports it. The child must implement [RestrictedHeightView] interface.
 */
class RestrictedTableRecyclerView : TableRecyclerView {

    private val restrictedViewHelper: RestrictedViewHelper = RestrictedViewHelper()

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    init {
        restrictedViewHelper.attachTo(this)
    }
}
