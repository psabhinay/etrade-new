package com.etrade.mobilepro.alerts

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.webkit.WebChromeClient
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.etrade.mobilepro.R
import com.etrade.mobilepro.alerts.presentation.AlertParcelableContent
import com.etrade.mobilepro.alerts.presentation.AlertsSharedViewModel
import com.etrade.mobilepro.alerts.presentation.base.AlertTab
import com.etrade.mobilepro.alerts.presentation.detail.AlertDetailViewModel
import com.etrade.mobilepro.alerts.presentation.detail.AlertDetailViewModel.ViewState
import com.etrade.mobilepro.common.OnNavigateUpListener
import com.etrade.mobilepro.common.RequireLogin
import com.etrade.mobilepro.databinding.FragmentAlertDetailBinding
import com.etrade.mobilepro.livedata.observeBy
import com.etrade.mobilepro.styledalertdialog.StyledAlertDialog
import com.etrade.mobilepro.util.android.binding.viewBinding
import com.etrade.mobilepro.util.android.snackbar.SnackbarUtilFactory
import javax.inject.Inject

@RequireLogin
class AlertDetailFragment @Inject constructor(
    snackbarUtilFactory: SnackbarUtilFactory,
    viewModelFactory: ViewModelProvider.Factory
) : Fragment(R.layout.fragment_alert_detail), OnNavigateUpListener {

    private val binding by viewBinding(FragmentAlertDetailBinding::bind)

    private val snackbarUtil by lazy {
        snackbarUtilFactory.createSnackbarUtil(
            { viewLifecycleOwner },
            { view }
        )
    }
    private val viewModel: AlertDetailViewModel by viewModels { viewModelFactory }
    private val sharedViewModel: AlertsSharedViewModel by activityViewModels { viewModelFactory }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        setupFragment()
        bindViewModel()
        if (savedInstanceState == null) {
            loadAlert()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.options_menu_alerts, menu)
        menu.findItem(R.id.menu_alert_delete)?.apply {
            isVisible = true
        }?.actionView?.apply {
            setOnClickListener { viewModel.askConfirmDelete() }
        }
    }

    override fun onNavigateUp(): Boolean {
        if (sharedViewModel.hasAlertFromPush) {
            sharedViewModel.didSelectTab(AlertTab.ACCOUNT)
            sharedViewModel.hasAlertFromPush = false
        }
        return false
    }

    private fun setupFragment() {
        binding.alertCell.toggleDisclosureArrowVisibility(isVisible = false)
        binding.alertCell.setCheckboxVisibility(View.GONE)
        binding.alertContent.webChromeClient = WebChromeClient()
    }

    private fun handleLoading(state: ViewState.Loading) {
        binding.alertContentLoadingPb.show()
        state.preloadedAlertInfo?.let { bindAndLoadData(it) }
    }

    private fun handleSuccess(state: ViewState.Success) {
        binding.alertContentLoadingPb.hide()
        bindAndLoadData(state.alert)
    }

    private fun bindAndLoadData(data: AlertParcelableContent) {
        binding.alertCell.bindTo(data)
        binding.alertContent.loadDataWithBaseURL(
            null,
            data.content,
            "text/html",
            null,
            null
        )
    }

    private fun handleConfirmDelete() {
        with(viewModel) {
            StyledAlertDialog(requireContext())
                .showAlert(
                    title = getString(R.string.alert_delete_dialog_title),
                    message = getString(R.string.do_you_want_to_delete_alert),
                    positiveAnswerAction = { deleteAlert() },
                    negativeAnswerAction = { cancelDelete() },
                    dismissAction = { cancelDelete() }
                )
        }
    }

    private fun bindViewModel() {
        viewModel.viewState.observeBy(viewLifecycleOwner) {
            updateViewState(it)
        }
        viewModel.viewEffects.observeBy(viewLifecycleOwner) {
            when (it) {
                is AlertDetailViewModel.ViewEffects.AlertWasDeleted -> {
                    sharedViewModel.didDeleteAlert(it.alertId, it.group)
                    activity?.onBackPressed()
                }
            }
        }
    }

    private fun updateViewState(state: ViewState) {
        when (state) {
            is ViewState.Loading -> handleLoading(state)
            is ViewState.Success -> handleSuccess(state)
            is ViewState.ConfirmDelete -> {
                handleSuccess(ViewState.Success(state.alert))
                handleConfirmDelete()
            }

            ViewState.Error.DeletionError -> {
                binding.alertContentLoadingPb.hide()
                snackbarUtil.retrySnackbar {
                    viewModel.deleteAlert()
                }
            }
            ViewState.Error.LoadingError -> {
                binding.alertContentLoadingPb.hide()
                snackbarUtil.retrySnackbar {
                    loadAlert()
                }
            }
        }
    }

    private fun loadAlert() {
        arguments?.let {
            viewModel.loadAlert(AlertDetailFragmentArgs.fromBundle(it).alert)
        }
    }
}
