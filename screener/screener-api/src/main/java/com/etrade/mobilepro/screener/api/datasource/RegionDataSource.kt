package com.etrade.mobilepro.screener.api.datasource

import com.etrade.mobilepro.screener.api.Region

interface RegionDataSource {
    fun getAllRegionsItem(): Region
    fun getRegions(): List<Region>
}
