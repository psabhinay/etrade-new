package com.etrade.mobilepro.screener.api.result

import com.etrade.mobilepro.instrument.InstrumentType
import com.etrade.mobilepro.instrument.WithInstrumentType
import com.etrade.mobilepro.tableviewutils.presentation.cell.SymbolCell
import com.evrencoskun.tableview.filter.IFilterableModel
import com.evrencoskun.tableview.sort.ISortableModel

class ResultSymbolCell(
    id: String,
    displayText: String,
    symbol: String,
    override val instrumentType: InstrumentType,
    description: String?,
    val hasNTF: Boolean = false,
    val hasCF: Boolean = false,
    val hasStar: Boolean = false
) : SymbolCell(
    id = id,
    displayText = displayText,
    symbol = symbol,
    instrumentType = instrumentType,
    description = description
),
    ISortableModel,
    IFilterableModel,
    WithInstrumentType {
    val hasExtraHeight: Boolean = hasNTF || hasCF || hasStar
}
