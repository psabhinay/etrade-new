package com.etrade.mobilepro.watchlistapi.repo

import com.etrade.mobilepro.tableviewutils.api.ColumnRepo

/**
 * This repository handles everything related to columns of a watchlist table's entries.
 */
interface WatchlistColumnRepo : ColumnRepo<WatchlistColumnInterface>
