package com.etrade.mobilepro.balance.section

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.etrade.mobilepro.screener.presentation.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class MoreInfoPopupFragment : DialogFragment() {

    private val title by lazy {
        arguments?.let {
            MoreInfoPopupFragmentArgs.fromBundle(it).popupTitleResId
        } ?: throw IllegalArgumentException("Popup title resource ID is required")
    }
    private val message by lazy {
        arguments?.let {
            MoreInfoPopupFragmentArgs.fromBundle(it).popupMessageResId
        } ?: throw IllegalArgumentException("Popup message resource ID is required")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = MaterialAlertDialogBuilder(requireContext())
        .setTitle(title)
        .setMessage(message)
        .setPositiveButton(R.string.ok) { _, _ ->
            dismiss()
        }
        .create()
}
