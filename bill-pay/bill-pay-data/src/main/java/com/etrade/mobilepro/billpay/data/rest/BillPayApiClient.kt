package com.etrade.mobilepro.billpay.data.rest

import com.etrade.mobilepro.backends.neo.ServerResponseDto
import com.etrade.mobilepro.billpay.data.rest.cancelpayment.CancelPaymentRequestBodyDto
import com.etrade.mobilepro.billpay.data.rest.cancelpayment.CancelPaymentResponseBodyDto
import com.etrade.mobilepro.billpay.data.rest.schedulepayment.PaymentRequestBodyDto
import com.etrade.mobilepro.billpay.data.rest.schedulepayment.PaymentResponseBodyDto
import com.etrade.mobilepro.billpay.data.rest.searchpayee.SearchPayeeRequestBodyDto
import com.etrade.mobilepro.billpay.data.rest.searchpayee.SearchPayeeResponseBodyDto
import com.etrade.mobilepro.util.json.JsonMoshi
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface BillPayApiClient {
    @JsonMoshi
    @POST("/webapimm/billpay/mobile/cancelPayment.json")
    suspend fun cancelPayment(@JsonMoshi @Body requestBody: CancelPaymentRequestBodyDto): ServerResponseDto<CancelPaymentResponseBodyDto>

    @JsonMoshi
    @GET("webapimm/billpay/mobile/billPayEnrollmentDetails.json")
    suspend fun getEnrollmentDetails(): EnrollmentDetailsResponseDto

    @JsonMoshi
    @POST("webapimm/billpay/mobile/getPayeesList.json")
    suspend fun getPayeesList(@JsonMoshi @Body requestBody: PayeeRequestDto): ServerResponseDto<PayeeResponseDto>

    @JsonMoshi
    @GET("webapimm/billpay/mobile/billPay.json")
    suspend fun getBillpaySessionId(): ServerResponseDto<BillpaySessionResponseDto>

    @JsonMoshi
    @GET("webapimm/billpay/mobile/getStateCodes.json")
    suspend fun getStateList(): ServerResponseDto<StateCodeResponseDto>

    @JsonMoshi
    @POST("webapimm/billpay/mobile/paymentList.json")
    suspend fun getPaymentList(@JsonMoshi @Body requestBody: PaymentListRequestDto): ServerResponseDto<PaymentListResponseDto>

    @JsonMoshi
    @POST("webapimm/billpay/mobile/addPayee.json")
    suspend fun addPayee(@JsonMoshi @Body requestBody: SavePayeeRequestDto): ServerResponseDto<SavePayeeResponseDto>

    @JsonMoshi
    @POST("webapimm/billpay/mobile/modifyPayee.json")
    suspend fun updatePayee(@JsonMoshi @Body requestBody: SavePayeeRequestDto): ServerResponseDto<SavePayeeResponseDto>

    @JsonMoshi
    @POST("webapimm/billpay/mobile/deletePayee.json")
    suspend fun deletePayee(@JsonMoshi @Body requestBody: PayeeRequestDto): ServerResponseDto<SavePayeeResponseDto>

    @JsonMoshi
    @POST("/webapimm/billpay/mobile/{paymentType}.json")
    suspend fun schedulePayment(
        @JsonMoshi @Body requestBody: PaymentRequestBodyDto,
        @Path("paymentType") paymentType: String
    ): ServerResponseDto<PaymentResponseBodyDto>

    @JsonMoshi
    @POST("/webapimm/billpay/mobile/payeeSearch.json")
    suspend fun searchPayee(
        @JsonMoshi @Body requestBody: SearchPayeeRequestBodyDto
    ): ServerResponseDto<SearchPayeeResponseBodyDto>
}

private const val NEW_BILL_PAYMENT = "addPayment"
private const val MODIFIED_BILL_PAYMENT = "modifyPayment"

fun getSchedulePaymentPath(isNewPayment: Boolean) = if (isNewPayment) {
    NEW_BILL_PAYMENT
} else {
    MODIFIED_BILL_PAYMENT
}
