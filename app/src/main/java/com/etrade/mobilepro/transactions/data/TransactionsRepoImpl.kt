package com.etrade.mobilepro.transactions.data

import com.etrade.mobilepro.caching.SimpleCachedResource
import com.etrade.mobilepro.transactions.TransactionsResponseInfo
import com.etrade.mobilepro.transactions.api.TransactionsRepo
import com.etrade.mobilepro.transactions.api.TransactionsRequest
import com.etrade.mobilepro.transactions.api.TransactionsResponse
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable
import java.io.IOException
import javax.inject.Inject

class TransactionsRepoImpl @Inject constructor(
    private val cacheDataSourceDelegate: SimpleCachedResource.DataSourceDelegate<TransactionsResponse, TransactionsRequest>
) : TransactionsRepo {

    override fun getTransactions(transactionsRequest: TransactionsRequest, cacheExpiration: Long?): Observable<Resource<TransactionsResponseInfo>> =
        SimpleCachedResource(
            id = transactionsRequest,
            dataSourceDelegate = cacheDataSourceDelegate,
            controller = object : SimpleCachedResource.Controller<TransactionsResponse> {
                override fun shouldRefresh(itemFromCache: TransactionsResponse): Boolean {
                    return cacheExpiration?.let { System.currentTimeMillis() - itemFromCache.fetchedAt >= it } ?: true
                }
            },
            showCachedData = true
        ).asObservable().map { cachedResponse ->
            try {
                val transactionsResponseInfo = cachedResponse.data?.transactionsResponseInfo

                when (cachedResponse) {
                    is Resource.Loading -> Resource.Loading()
                    is Resource.Success -> Resource.Success(transactionsResponseInfo)
                    is Resource.Failed -> {
                        Resource.Failed(transactionsResponseInfo, cachedResponse.error)
                    }
                }
            } catch (e: IOException) {
                Resource.Failed<TransactionsResponseInfo>(error = e)
            }
        }
}
