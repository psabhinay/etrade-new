package com.etrade.mobilepro.trade.presentation.form

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.dynamic.form.api.Form
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.input.ConditionalOrderTradeFormInputId
import com.etrade.mobilepro.trade.form.api.toSecurityType
import com.etrade.mobilepro.trade.presentation.form.delegate.conditional.ConditionalFormFactoryDelegate
import com.etrade.mobilepro.trade.presentation.form.delegate.conditional.ConditionalOptionTradeFormFactoryDelegate
import com.etrade.mobilepro.trade.presentation.form.delegate.conditional.ConditionalStockTradeFormFactoryDelegate
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.ConditionalFactoryDelegateDependenciesImpl
import com.etrade.mobilepro.trade.presentation.form.delegate.dependencies.FormFactoryDelegateDependencies
import com.etrade.mobilepro.trade.presentation.form.delegate.shared.conditionalOrderSecurityType
import com.etrade.mobilepro.trade.presentation.usecase.CreateConditionalTradeFormParameter
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class StaticConditionalOrderFormFactory @Inject constructor(
    private val context: Context,
    private val searchIntentFactory: SearchIntentFactory,
    private val tradingDefaultPreferences: TradingDefaultsPreferences,
    inputId: ConditionalOrderTradeFormInputId
) : ConditionalOrderFormFactory {

    private val dependencies: FormFactoryDelegateDependencies = ConditionalFactoryDelegateDependenciesImpl(
        inputId = inputId,
        resources = context.resources
    )

    private val delegates: Map<SecurityType, ConditionalFormFactoryDelegate>

    init {
        fun createOptionTradeFormFactoryDelegate(): ConditionalFormFactoryDelegate {
            return ConditionalOptionTradeFormFactoryDelegate(context, inputId, dependencies, searchIntentFactory, tradingDefaultPreferences)
        }

        fun createStockTradeFormFactoryDelegate(): ConditionalFormFactoryDelegate {
            return ConditionalStockTradeFormFactoryDelegate(context, inputId, dependencies, searchIntentFactory, tradingDefaultPreferences)
        }

        delegates = mapOf(
            SecurityType.OPTION to createOptionTradeFormFactoryDelegate(),
            SecurityType.STOCK to createStockTradeFormFactoryDelegate()
        )
    }

    override suspend fun createConditionalOrderForm(parameter: CreateConditionalTradeFormParameter): ETResult<LiveData<Form>> {
        return withContext(Dispatchers.Main) {
            runCatchingET {
                MutableLiveData<Form>().apply {
                    val symbolChangedListener = object : ConditionalFormFactoryDelegate.SymbolChangedListener {
                        override fun onSymbolChange(withSymbolInfo: WithSymbolInfo) {
                            val securityType = withSymbolInfo.instrumentType.toSecurityType()
                            value = requireDelegate(securityType).createForm(parameter, this, withSymbolInfo)
                        }
                    }
                    val securityType = parameter.defaultValues.conditionalOrderSecurityType() ?: SecurityType.STOCK
                    value = requireDelegate(securityType).createForm(parameter, symbolChangedListener)
                }
            }
        }
    }

    private fun requireDelegate(securityType: SecurityType): ConditionalFormFactoryDelegate = requireNotNull(delegates[securityType])
}
