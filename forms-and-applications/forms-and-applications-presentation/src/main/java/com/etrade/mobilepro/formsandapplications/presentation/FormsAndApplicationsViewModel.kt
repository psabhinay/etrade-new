package com.etrade.mobilepro.formsandapplications.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.etrade.mobilepro.common.result.onSuccess
import com.etrade.mobilepro.formsandapplications.api.FormsAndApplicationsReference
import com.etrade.mobilepro.formsandapplications.api.FormsAndApplicationsRepository
import com.etrade.mobilepro.tracking.SCREEN_TITLE_FORMS_AND_APPLICATIONS
import com.etrade.mobilepro.tracking.ScreenViewModel
import com.etrade.mobilepro.tracking.Tracker
import com.etrade.mobilepro.tracking.screen
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * View model for [FormsAndApplicationsActivity].
 */
class FormsAndApplicationsViewModel @Inject constructor(
    private val repository: FormsAndApplicationsRepository,
    private val tracker: Tracker
) : ViewModel(), ScreenViewModel {

    /**
     * Live data of displayable documents.
     */
    val listItems: LiveData<List<FormsAndApplicationsReference>>
        get() = _listItems

    override val screenName: String? = SCREEN_TITLE_FORMS_AND_APPLICATIONS

    private val _listItems: MutableLiveData<List<FormsAndApplicationsReference>> = MutableLiveData()

    /**
     * Refreshes [listItems].
     */
    fun refreshItems() {
        viewModelScope.launch {
            repository.getFormsAndApplications()
                .onSuccess { _listItems.postValue(it) }
        }
    }

    fun trackPage(title: String) {
        tracker.screen(title)
    }
}
