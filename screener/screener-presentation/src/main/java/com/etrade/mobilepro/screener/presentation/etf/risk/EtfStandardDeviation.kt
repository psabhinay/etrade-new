package com.etrade.mobilepro.screener.presentation.etf.risk

import com.etrade.mobilepro.screener.api.filter.etf.EtfRiskFilter
import com.etrade.mobilepro.screener.presentation.base.ScreenerFilterEvent

internal enum class EtfStandardDeviation : ScreenerFilterEvent<EtfRiskFilter, EtfRiskViewModel.ViewState, Boolean> {
    BOTTOM_20 {
        override fun update(viewState: EtfRiskViewModel.ViewState, eventValue: Boolean): EtfRiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(bottom20 = eventValue))
    },
    BELOW_MID_20 {
        override fun update(viewState: EtfRiskViewModel.ViewState, eventValue: Boolean): EtfRiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(belowMid20 = eventValue))
    },
    MID_20 {
        override fun update(viewState: EtfRiskViewModel.ViewState, eventValue: Boolean): EtfRiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(mid20 = eventValue))
    },
    ABOVE_MID_20 {
        override fun update(viewState: EtfRiskViewModel.ViewState, eventValue: Boolean): EtfRiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(aboveMid20 = eventValue))
    },
    TOP_20 {
        override fun update(viewState: EtfRiskViewModel.ViewState, eventValue: Boolean): EtfRiskViewModel.ViewState =
            viewState.copy(filter = viewState.filter.copy(top20 = eventValue))
    }
}
