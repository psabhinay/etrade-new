package com.etrade.mobilepro.ui_demo.compose.basiccomponents

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import com.etrade.mobilepro.common.compose.theme.EtradeTheme
import com.etrade.mobilepro.compose.components.EtRetrySnackbar
import com.etrade.mobilepro.compose.components.LoadingIndicator
import com.etrade.mobilepro.compose.components.RetryButton
import com.etrade.mobilepro.compose.components.ThickDivider
import com.etrade.mobilepro.compose.components.UiStateBox
import com.etrade.mobilepro.dynamicui.presentation.DynamicUiViewState

class BasicComposeComponentsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EtradeTheme {
                LazyColumn {
                    item {
                        LoadingIndicator()
                    }
                    item { Spacer(modifier = Modifier.fillMaxWidth().height(1.dp).background(Color.Red)) }
                    item {
                        RetryButton {}
                    }
                    item { Spacer(modifier = Modifier.fillMaxWidth().height(1.dp).background(Color.Red)) }
                    item {
                        RetryButton(enabled = false) {}
                    }
                    item { Spacer(modifier = Modifier.fillMaxWidth().height(1.dp).background(Color.Red)) }
                    item {
                        EtRetrySnackbar {  }
                    }
                    item { Spacer(modifier = Modifier.fillMaxWidth().height(1.dp).background(Color.Red)) }
                    item {
                        ThickDivider()
                    }
                    item { Spacer(modifier = Modifier.fillMaxWidth().height(1.dp).background(Color.Red)) }
                    item {
                        UiStateBox(state = DynamicUiViewState.Loading, modifier = Modifier.fillParentMaxWidth(), ) {}
                    }
                    item { Spacer(modifier = Modifier.fillMaxWidth().height(1.dp).background(Color.Red)) }
                    item {
                        UiStateBox(state = DynamicUiViewState.Error(), modifier = Modifier.fillParentMaxWidth(), onRetry = {}) {}
                    }
                }
            }
        }
    }
}
