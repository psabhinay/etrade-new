package com.etrade.mobilepro.chartiq.usecase.util

import com.etrade.mobilepro.order.details.api.BaseOrderDetailsItem.OrderDetailsItem
import com.etrade.mobilepro.orders.extensions.extractOrderIdentifier

internal fun OrderDetailsItem.getOrderIdentifier() = extractOrderIdentifier(
    orderType = orderType,
    advancedOrderType = advancedItem?.orderType,
    origSysCode = origSysCode,
    instrumentType = orderDescriptions[0].typeCode,
    isMultiLeg = hasMultipleLegs
)
