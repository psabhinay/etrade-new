package com.etrade.mobile.accounts.details

import java.math.BigDecimal

interface AccountsDetailedList {
    val liabilityValue: BigDecimal?
    val netAssetValue: BigDecimal?
    val highRiskFlag: Boolean
    val accounts: List<AccountDetails>
}
