package com.etrade.mobilepro.compose.components

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.TextStyle
import com.etrade.mobilepro.common.compose.theme.Styles

private const val FONT_SIZE_FIT_MULTIPLIER = 0.9F
private const val VISIBLE_ITEM_OFFSET = 2

interface TableViewState {
    val rowSize: Int
    val columnSize: Int
    val isReadyToDraw: Boolean
    fun updateRowAndColumnSize(rowSize: Int, columnSize: Int)
    fun isStateChange(rowSize: Int, columnSize: Int): Boolean
    fun setCellWidth(rowIndex: Int, columnIndex: Int, width: Int)
    fun setTextWidth(rowIndex: Int, columnIndex: Int, textLayoutResult: TextLayoutResult)
    fun getFinalColumnWidth(columnIndex: Int): Int
    fun getTextStyle(rowIndex: Int, columnIndex: Int): TextStyle
    fun updateReadyToDraw(visibleItemSize: Int)
}

fun TableViewState(
    initialRowSize: Int,
    initialColumnSize: Int,
): TableViewState = TableViewStateImpl(initialRowSize, initialColumnSize)

@Stable
private class TableViewStateImpl(
    initialRowSize: Int,
    initialColumnSize: Int
) : TableViewState {

    var mutableRowSize by mutableStateOf(initialRowSize)

    var mutableColumnSize by mutableStateOf(initialColumnSize)

    override val rowSize: Int
        get() = mutableRowSize

    override val columnSize: Int
        get() = mutableColumnSize

    override val isReadyToDraw: Boolean
        get() = mutableReady

    override fun updateRowAndColumnSize(rowSize: Int, columnSize: Int) {
        initializeState(rowSize, columnSize)
    }

    private var finalTextWidthIsReady by mutableStateOf(createList(false, columnSize))

    var mutableReady by mutableStateOf(false)
    var lastRowIndex by mutableStateOf(Int.MIN_VALUE)
    var maxCellWidth by mutableStateOf(createList(Int.MIN_VALUE, columnSize))
    var finalTextWidth by mutableStateOf(createList(Int.MIN_VALUE, columnSize))
    var textStyleData by mutableStateOf(createListOfList(Styles.Regular16sp, rowSize, columnSize))

    private fun initializeState(initialRowSize: Int, initialColumnSize: Int) {
        mutableReady = false
        mutableRowSize = initialRowSize
        mutableColumnSize = initialColumnSize
        finalTextWidthIsReady = createList(false, columnSize)
        lastRowIndex = Int.MIN_VALUE
        maxCellWidth = createList(Int.MIN_VALUE, initialColumnSize)
        finalTextWidth = createList(Int.MIN_VALUE, columnSize)
        textStyleData = createListOfList(Styles.Regular16sp, initialRowSize, initialColumnSize)
    }

    override fun updateReadyToDraw(visibleItemSize: Int) {
        if (mutableReady) return

        val calculatedVisibleItemSize = if (visibleItemSize > rowSize) {
            rowSize - 1
        } else {
            visibleItemSize - VISIBLE_ITEM_OFFSET
        }

        var ready = true
        repeat(columnSize) {
            if (finalTextWidth[it] < maxCellWidth[it]) {
                finalTextWidth[it] = maxCellWidth[it]
            } else {
                finalTextWidthIsReady[it] = lastRowIndex >= calculatedVisibleItemSize
            }
            ready = finalTextWidthIsReady[it] && ready
        }
        mutableReady = ready
    }

    override fun isStateChange(rowSize: Int, columnSize: Int): Boolean {
        return (rowSize != mutableRowSize || columnSize != mutableColumnSize).also {
            if (it) {
                initializeState(rowSize, columnSize)
            }
        }
    }

    override fun setTextWidth(rowIndex: Int, columnIndex: Int, textLayoutResult: TextLayoutResult) {
        if (!mutableReady) {
            setCellWidth(rowIndex, columnIndex, textLayoutResult.size.width)
        } else if (textLayoutResult.didOverflowWidth) {
            val updatedStyle = textStyleData[rowIndex][columnIndex].copy(
                fontSize = textStyleData[rowIndex][columnIndex].fontSize * FONT_SIZE_FIT_MULTIPLIER
            )
            textStyleData[rowIndex][columnIndex] = updatedStyle
        }
    }

    override fun getFinalColumnWidth(columnIndex: Int): Int = finalTextWidth[columnIndex]
    override fun getTextStyle(rowIndex: Int, columnIndex: Int): TextStyle = textStyleData[rowIndex][columnIndex]

    override fun setCellWidth(rowIndex: Int, columnIndex: Int, width: Int) {
        if (rowIndex > lastRowIndex && columnIndex == columnSize - 1) {
            lastRowIndex = rowIndex
        }
        if (rowIndex <= mutableRowSize && !mutableReady) {
            if (maxCellWidth[columnIndex] < width) {
                maxCellWidth[columnIndex] = width
            }
        }
    }

    private fun <T> createList(initialValue: T, size: Int): MutableList<T> {
        val list = mutableListOf<T>()
        repeat(size) {
            list.add(initialValue)
        }
        return list
    }

    private fun <T> createListOfList(initialValue: T, rowSize: Int, columnSize: Int): List<MutableList<T>> {
        val table = mutableListOf<MutableList<T>>()
        repeat(rowSize) {
            table.add(createList(initialValue, columnSize))
        }
        return table
    }
}
