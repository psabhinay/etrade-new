package com.etrade.mobilepro.alerts.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.etrade.mobilepro.alerts.api.model.AlertGroup
import com.etrade.mobilepro.alerts.api.model.UserAlertItem
import com.etrade.mobilepro.alerts.presentation.base.AlertTab
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.SingleLiveEvent
import javax.inject.Inject

class AlertsSharedViewModel @Inject constructor() : ViewModel() {
    private val _askConfirmDelete = MutableLiveData<Boolean>(false)
    private val _deletedAccountAlert = SingleLiveEvent<String>()
    private val _deletedMarketAlert = SingleLiveEvent<String>()
    private val _alertWasSet = MutableLiveData<ConsumableLiveEvent<String>>()
    private val _alertWasUpdated = SingleLiveEvent<String>()
    private val _deletedUserAlert = SingleLiveEvent<UserAlertItem>()
    private val _selectedTab = MutableLiveData<AlertTab>(AlertTab.MARKET)
    private val _inEditMode = MutableLiveData<Boolean>(false)

    var selectedUserAlert: UserAlertItem? = null
    val askConfirmDelete: LiveData<Boolean> = _askConfirmDelete
    val deletedUserAlert: LiveData<UserAlertItem> = _deletedUserAlert
    val deletedAccountAlert: LiveData<String> = _deletedAccountAlert
    val deletedMarketAlert: LiveData<String> = _deletedMarketAlert
    val alertWasSet: LiveData<ConsumableLiveEvent<String>> = _alertWasSet
    val alertWasUpdated: LiveData<String> = _alertWasUpdated
    val selectedTab: LiveData<AlertTab> = _selectedTab
    val inEditMode: LiveData<Boolean> = _inEditMode
    var hasAlertFromPush: Boolean = false

    fun didSelectTab(tab: AlertTab) {
        _selectedTab.value = tab
    }

    fun askConfirmDelete() {
        _askConfirmDelete.value = true
    }

    fun cancelDelete() {
        _askConfirmDelete.value = false
    }

    fun didDeleteAlert(alertId: String, group: AlertGroup) {
        when (group) {
            AlertGroup.MARKET -> _deletedMarketAlert.value = alertId
            AlertGroup.ACCOUNT -> _deletedAccountAlert.value = alertId
        }
    }

    fun didSetAlert(message: String) {
        _alertWasSet.postValue(ConsumableLiveEvent(message))
    }

    fun didUpdateAlert(message: String) {
        _alertWasUpdated.value = message
    }

    fun reset() {
        _selectedTab.value = AlertTab.MARKET
    }

    fun didDeleteUserAlert(alert: UserAlertItem) {
        _deletedUserAlert.value = alert
    }

    fun enableEditMode(isEditable: Boolean) {
        _inEditMode.value = isEditable
    }
}
