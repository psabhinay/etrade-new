package com.etrade.mobilepro.holdings.dto

import com.etrade.mobile.power.backends.neo.BaseUsData
import com.etrade.mobile.power.backends.neo.BaseUsResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HoldingsResponseDto(
    @Json(name = "data")
    override val data: PositionsCount?
) : BaseUsResponse()

@JsonClass(generateAdapter = true)
class PositionsCount(
    @Json(name = "positionsCountResponse")
    val holdings: HoldingsDto?
) : BaseUsData()

@JsonClass(generateAdapter = true)
data class HoldingsDto(
    @Json(name = "totalPositionsCount")
    val totalPositionsCount: String?,
    @Json(name = "numberOfAccounts")
    val numberOfAccounts: String?,
    @Json(name = "hasholdings")
    val hasHoldings: Boolean?
)
