package com.etrade.mobilepro.session.ping.repo

import com.etrade.mobilepro.session.api.User
import com.etrade.mobilepro.session.ping.PingMessage
import com.etrade.mobilepro.session.ping.dto.LoginUserSSOResponse
import com.etrade.mobilepro.session.ping.dto.SessionError
import com.etrade.mobilepro.session.ping.rest.AndroidPingSessionService
import org.simpleframework.xml.core.Persister
import org.slf4j.LoggerFactory
import retrofit2.Retrofit

class DefaultPingSessionRepo(
    retrofit: Retrofit,
    private val user: User,
    private val deviceId: () -> String
) : PingSessionRepo {

    private val logger = LoggerFactory.getLogger(this::class.java)

    private val xmlParser = Persister()
    private val pingService: AndroidPingSessionService = retrofit.create(AndroidPingSessionService::class.java)

    override suspend fun ping(): PingMessage {

        return runCatching {
            val xmlResponse = pingService.mobilePingSession(userId = requireNotNull(user.getUserId()), deviceNumber = deviceId())
            val sessionError = parsePingSessionResponse(xmlResponse)?.result?.fault?.error
            if (sessionError?.isSessionInValid == true) {
                PingMessage(isSessionValid = false, errorMsg = sessionError.message)
            } else {
                PingMessage(isSessionValid = true)
            }
        }.fold(
            onSuccess = { it },
            onFailure = {
                // Fail Silently
                logger.debug("Ping service failed due to ${it.message}")
                PingMessage(isSessionValid = true, errorMsg = it.message)
            }
        )
    }

    private fun parsePingSessionResponse(xml: String): LoginUserSSOResponse? {
        return xmlParser.read(LoginUserSSOResponse::class.java, xml, false)
    }
}

private val SessionError.isSessionInValid: Boolean
    get() = this.type == 1

private val SessionError?.message: String
    get() = this?.description ?: "Invalid session error"
