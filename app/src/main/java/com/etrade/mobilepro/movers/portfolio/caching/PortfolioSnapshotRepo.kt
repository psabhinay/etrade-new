package com.etrade.mobilepro.movers.portfolio.caching

import com.etrade.mobilepro.dynamicui.api.ScreenRequest
import com.etrade.mobilepro.portfoliosnapshot.NeoPortfolioSnapshotResponse
import com.etrade.mobilepro.util.Resource
import io.reactivex.Observable

interface PortfolioSnapshotRepo {
    fun getPortfolioMovers(request: ScreenRequest, cacheExpiration: Long?): Observable<Resource<NeoPortfolioSnapshotResponse>>
}
