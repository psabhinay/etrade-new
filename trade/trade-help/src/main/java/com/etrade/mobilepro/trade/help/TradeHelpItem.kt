package com.etrade.mobilepro.trade.help

data class TradeHelpItem(
    val title: String,
    val description: String
)
