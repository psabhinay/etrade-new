package com.etrade.mobilepro.launcher

import com.etrade.eo.userprefrences.api.KeyValueStorage
import com.etrade.mobilepro.common.di.DefaultKeyStore
import com.etrade.mobilepro.encryption.api.CipherAuthenticator
import com.etrade.mobilepro.util.updater.LoginPreferencesMigration
import javax.inject.Inject

class AppLauncherMigration @Inject constructor(
    @DefaultKeyStore private val defaultKeyValueStorage: KeyValueStorage,
    private val loginPreferencesMigration: LoginPreferencesMigration
) {

    suspend fun migrate(authenticator: CipherAuthenticator) {
        loginPreferencesMigration.migrate(defaultKeyValueStorage, authenticator)
    }
}
