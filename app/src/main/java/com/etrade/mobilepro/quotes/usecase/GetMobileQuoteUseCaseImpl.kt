package com.etrade.mobilepro.quotes.usecase

import com.etrade.mobilepro.common.result.ETResult
import com.etrade.mobilepro.common.result.runCatchingET
import com.etrade.mobilepro.quote.mapper.MobileQuotePageItemMapper
import com.etrade.mobilepro.quoteapi.GetQuoteParameter
import com.etrade.mobilepro.quoteapi.MobileQuote
import com.etrade.mobilepro.quoteapi.MobileQuoteRepo
import com.etrade.mobilepro.quoteapi.usecase.GetMobileQuoteUseCase
import com.etrade.mobilepro.util.rx.awaitResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import javax.inject.Inject

class GetMobileQuoteUseCaseImpl @Inject constructor(
    private val repo: MobileQuoteRepo,
    private val mapper: MobileQuotePageItemMapper
) : GetMobileQuoteUseCase {
    override suspend fun execute(parameter: GetQuoteParameter): ETResult<MobileQuote> =
        withContext(Dispatchers.IO) {
            runCatchingET {
                val titleQuote = async { loadQuote(parameter.symbol.title, parameter.isExtendedHours) }
                if (parameter.symbol.type.isOption) {
                    val symbolQuote = async { loadQuote(parameter.symbol.underlyerSymbol, parameter.isExtendedHours) }
                    mapper.map(symbolQuote.await().getOrThrow(), titleQuote.await().getOrThrow())
                } else {
                    titleQuote.await().getOrThrow()
                }.apply {
                    addQueryData(parameter.symbol)
                }
            }
        }

    private suspend fun loadQuote(symbolTitle: String, isExtendedHours: Boolean): ETResult<MobileQuote> {
        return repo.loadMobileQuote(
            symbolTitle = symbolTitle,
            extendedHours = isExtendedHours
        ).awaitResult()
    }
}
