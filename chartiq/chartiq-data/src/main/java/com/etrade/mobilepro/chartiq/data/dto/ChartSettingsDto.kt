package com.etrade.mobilepro.chartiq.data.dto

import com.etrade.mobilepro.androidconfig.api.AndroidConfigInfo
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class ChartSettingsDto(

    @Json(name = "akamaiDomain")
    val akamaiDomain: String,

    @Json(name = "akamaiChartVersion")
    val akamaiChartVersion: String,

    @Json(name = "env")
    val environmentName: String
) {

    constructor(config: AndroidConfigInfo, environmentName: String) : this(config.akamaiDomain, config.chartVersion, environmentName)
}
