package com.etrade.mobilepro.trade.optionlegselect

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.etrade.mobile.accounts.defaultaccount.DefaultAccountRepo
import com.etrade.mobilepro.common.di.Web
import com.etrade.mobilepro.common.result.getOrElse
import com.etrade.mobilepro.common.result.onFailure
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.viewmodel.AppDialogViewModel
import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot
import com.etrade.mobilepro.dynamic.form.value.MutableInputValuesSnapshot
import com.etrade.mobilepro.instrument.WithSymbolInfo
import com.etrade.mobilepro.instrument.getDisplaySymbolFromOsiKey
import com.etrade.mobilepro.instrument.underlier
import com.etrade.mobilepro.livedata.OutsourcedLiveData
import com.etrade.mobilepro.livedata.sourceFor
import com.etrade.mobilepro.optionschain.tableview.OptionChainTableViewModel
import com.etrade.mobilepro.optionschains.api.Option
import com.etrade.mobilepro.optionschains.api.id
import com.etrade.mobilepro.orders.api.OrderTerm
import com.etrade.mobilepro.orders.api.PriceType
import com.etrade.mobilepro.orders.api.TransactionType
import com.etrade.mobilepro.presentation.handleResult
import com.etrade.mobilepro.searchingapi.SearchIntentFactory
import com.etrade.mobilepro.searchingapi.StartSearchParams
import com.etrade.mobilepro.searchingapi.items.SearchQuery
import com.etrade.mobilepro.searchingapi.items.SearchType
import com.etrade.mobilepro.trade.api.TradeAccountRepo
import com.etrade.mobilepro.trade.api.option.OptionTradeRepo
import com.etrade.mobilepro.trade.api.option.OptionTradeStrategyResponse
import com.etrade.mobilepro.trade.api.option.TradeLeg
import com.etrade.mobilepro.trade.api.option.isOption
import com.etrade.mobilepro.trade.form.api.SecurityType
import com.etrade.mobilepro.trade.form.api.SelectedAccountDelegate
import com.etrade.mobilepro.trade.form.api.TradeFormParametersBuilder
import com.etrade.mobilepro.trade.form.api.input.OptionTradeFormInputId
import com.etrade.mobilepro.trade.form.api.input.extractPriceType
import com.etrade.mobilepro.trade.form.api.input.extractSymbolLookupInfo
import com.etrade.mobilepro.trade.form.api.input.extractTerm
import com.etrade.mobilepro.trade.form.api.input.toSecurityType
import com.etrade.mobilepro.trade.presentation.util.isEligibleForOptionTrade
import com.etrade.mobilepro.tradingdefaults.preferences.DEFAULT_STOCK_AND_OPTIONS_STOCK_QUANTITY
import com.etrade.mobilepro.tradingdefaults.preferences.TradingDefaultsPreferences
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionItem
import com.etrade.mobilepro.uiwidgets.actionsheet.ActionSheetViewModel
import com.etrade.mobilepro.util.AppMessage
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.ValueCoder
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consumable
import com.etrade.mobilepro.viewmodel.AppMessageViewModel
import com.etrade.mobilepro.viewmodel.BlockingLoadingIndicatorViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.inject.Inject
import javax.inject.Provider

private const val MAX_NUMBER_OF_OPTION_LEGS = 4

@Suppress("LargeClass", "StaticFieldLeak", "TooManyFunctions", "LongParameterList")
class OptionLegSelectViewModel @Inject constructor(
    @OptionTradeActionItemProvider private val optionTradeActionItemProvider: TradeActionItemProvider,
    private val context: Context,
    private val resources: Resources,
    @Web
    private val baseUrl: String,
    private val searchIntentFactory: SearchIntentFactory,
    private val optionTradeRepo: OptionTradeRepo,
    private val tradeFormParametersBuilderProvider: Provider<TradeFormParametersBuilder>,
    private val optionTradeFormInputId: OptionTradeFormInputId,
    private val tradeLegsCoder: ValueCoder<List<TradeLeg>>,
    tradingDefaultsPreferences: TradingDefaultsPreferences,
    private val tradeAccountRepo: TradeAccountRepo,
    private val defaultAccountRepo: DefaultAccountRepo,
    private val accountDelegateFactory: SelectedAccountDelegate.Factory
) : ViewModel(), ActionSheetViewModel, AppDialogViewModel, AppMessageViewModel, BlockingLoadingIndicatorViewModel {

    override val actions: LiveData<List<ActionItem>>
        get() = _actions
    override val appMessageSignal: LiveData<AppMessage?>
        get() = _appMessageSignal
    override val blockingLoadingIndicator: LiveData<Boolean>
        get() = _loadingIndicator
    private val _dialog: MutableLiveData<AppDialog?> = MutableLiveData()
    override val dialog: LiveData<ConsumableLiveEvent<AppDialog?>?> =
        _dialog.map { ConsumableLiveEvent(it) }

    val finishSelectSignal: LiveData<ConsumableLiveEvent<InputValuesSnapshot>>
        get() = _finishSelectSignal
    val openQuoteLookupPage: LiveData<ConsumableLiveEvent<Intent>>
        get() = _openQuoteLookupPage
    val scrollUpSignal: LiveData<ConsumableLiveEvent<Unit>>
        get() = _scrollUpSignal
    val selectActionEnabled: LiveData<Boolean>
        get() = hasSelectedOptionLegs
    val selectedSymbol: LiveData<WithSymbolInfo>
        get() = _selectedSymbol

    private val _selectedOptionLegs: OutsourcedLiveData<List<Option?>, List<TradeLeg>> =
        OutsourcedLiveData<List<Option?>, List<TradeLeg>> { options, _ ->
            invalidateSelectedOptionLegs(options)
        }.apply { value = emptyList() }

    private val _actions: MutableLiveData<List<ActionItem>> = MutableLiveData()
    private val _appMessageSignal: MutableLiveData<AppMessage?> = MutableLiveData()
    private val _finishSelectSignal: MutableLiveData<ConsumableLiveEvent<InputValuesSnapshot>> = MutableLiveData()
    private val _loadingIndicator: MutableLiveData<Boolean> = MutableLiveData(false)
    private val _openQuoteLookupPage: MutableLiveData<ConsumableLiveEvent<Intent>> = MutableLiveData()
    private val _scrollUpSignal: MutableLiveData<ConsumableLiveEvent<Unit>> = MutableLiveData()
    private val _selectedSymbol: MutableLiveData<WithSymbolInfo> = MutableLiveData()
    private val _stockTradeLeg: MediatorLiveData<StockTradeLeg> = MediatorLiveData()

    private val hasSelectedOptionLegs: LiveData<Boolean> = _selectedOptionLegs.map { it.isNotEmpty() }
    private val optionLegProperties: MutableList<OptionLegProperty> = mutableListOf()
    private val defaultQuantity: Int = tradingDefaultsPreferences.optionsContracts
    private val defaultPriceType: PriceType? = tradingDefaultsPreferences.optionsPriceType
    private val defaultTerm: OrderTerm? = tradingDefaultsPreferences.optionsTerm
    private var selectedOptionCandidate: Option? = null

    private var optionChainTableViewModel: OptionChainTableViewModel? by sourceFor(_selectedOptionLegs) { selectedOptions }

    private var tradeLegBidAskViewModel: TradeLegBidAskViewModel? = null

    private val searchSymbolIntent: Intent
        get() = searchIntentFactory.createIntent(
            context = context,
            params = StartSearchParams(
                query = SearchQuery(SearchType.SYMBOL),
                stockQuoteSearchOnly = true
            ),
            reorderToFront = false
        )

    internal val selectedOptionLegs: LiveData<List<OptionLegItem>> = _selectedOptionLegs.map { legs ->
        legs.map { it.toOptionLegItem(resources) }
    }

    internal val optionLegVisible: LiveData<Boolean> = Transformations.map(hasSelectedOptionLegs) {
        it || isStockAndOptions
    }

    internal val selectedStockLegItem: LiveData<StockLegItem> = _stockTradeLeg.map {
        StockLegItem(
            quantity = it.quantity.toString(),
            displayName = it.symbol,
            orderType = it.formatAction(resources)
        )
    }

    private val formValues: MutableLiveData<InputValuesSnapshot> = MutableLiveData()
    private val securityType: LiveData<SecurityType> = formValues.map {
        optionTradeFormInputId.toSecurityType(it[optionTradeFormInputId.securityType]) ?: SecurityType.OPTION
    }

    val showStockLeg: LiveData<Boolean> = securityType.map { it == SecurityType.STOCK_AND_OPTIONS }

    private var isStockAndOptions: Boolean = false
    private var maxNumberOfOptionsLeg: Int = MAX_NUMBER_OF_OPTION_LEGS
    private var accountId: String? = null

    private val logger: Logger = LoggerFactory.getLogger(OptionLegSelectViewModel::class.java)

    init {
        showStockLeg.observeForever { stockAndOptions ->
            isStockAndOptions = stockAndOptions
            maxNumberOfOptionsLeg = if (stockAndOptions) {
                MAX_NUMBER_OF_OPTION_LEGS - 1
            } else {
                MAX_NUMBER_OF_OPTION_LEGS
            }

            if (stockAndOptions) {
                _stockTradeLeg.addSource(selectedSymbol) { symbolInfo ->
                    _stockTradeLeg.value = _stockTradeLeg.value?.copy(symbol = symbolInfo.symbol) ?: createStockTradeLeg()
                }
            } else {
                _stockTradeLeg.removeSource(selectedSymbol)
            }
        }
    }

    override fun selectAction(item: ActionItem) {
        _actions.value = emptyList()
        val option = selectedOptionCandidate ?: return
        val transactionType = optionTradeActionItemProvider.getTransactionType(item.id)
        selectOptionWithActionAndQuantity(transactionType, defaultQuantity, option.id)
        _scrollUpSignal.value = Unit.consumable()
    }

    override fun cancelActions() {
        selectedOptionCandidate = null
        _actions.value = emptyList()
    }

    override fun dismissAppMessage() {
        _appMessageSignal.value = null
    }

    fun attachTo(viewModel: OptionChainTableViewModel) {
        optionChainTableViewModel = viewModel
    }

    fun attachTo(viewModel: TradeLegBidAskViewModel) {
        tradeLegBidAskViewModel = viewModel
    }

    fun injectSymbol(symbol: WithSymbolInfo?, transactionType: TransactionType? = null) {
        setSymbolOrInvoke(symbol, transactionType) {
            _openQuoteLookupPage.value = ConsumableLiveEvent(searchSymbolIntent)
        }
    }

    fun injectValues(values: InputValuesSnapshot, symbol: WithSymbolInfo?, transactionType: TransactionType?) {
        injectSymbol(
            symbol ?: optionTradeFormInputId.extractSymbolLookupInfo(values),
            transactionType
        )

        formValues.value = values
        values[optionTradeFormInputId.tradeLegs]?.let {
            tradeLegsCoder.decode(it)?.forEach { leg ->
                if (leg.isOption) {
                    selectOptionWithActionAndQuantity(leg.transactionType, leg.quantity, leg.symbol, leg.isAMOption)
                } else {
                    _stockTradeLeg.value = StockTradeLeg(leg.symbol, leg.transactionType, leg.quantity, leg.isAMOption)
                }
            }
        }
    }

    fun invokeSelectAction() {
        dismissAppMessage()

        viewModelScope.launch {
            _loadingIndicator.value = true

            val params = buildParams()
            accountId = params[optionTradeFormInputId.accountId] as? String

            optionTradeRepo
                .getStrategyIdentifier(
                    symbol = selectedSymbol.value?.symbol.orEmpty(),
                    legs = createTradeLegs(),
                    params = params
                )
                .handleResult(
                    resources = resources,
                    baseUrl = baseUrl,
                    dialog = _dialog,
                    onSuccess = ::navigateBackToTrade,
                    onUnhandledFailure = {
                        sendGeneralErrorMessage {
                            invokeSelectAction()
                        }
                    }
                )
                .onFailure {
                    logger.error("Strategy call has failed.", it)
                }

            _loadingIndicator.value = false
        }
    }

    private suspend fun buildParams(): InputValuesSnapshot {
        return if (needToLoadAccountId(formValues.value.orEmpty())) {
            val formValuesWithAccountId: MutableInputValuesSnapshot = formValues.value.orEmpty().toMutableMap()
            formValuesWithAccountId[optionTradeFormInputId.accountId] = loadDefaultAccountId()
            formValuesWithAccountId
        } else {
            formValues.value.orEmpty()
        }
    }

    private suspend fun loadDefaultAccountId(): String {
        val (accounts, defaultAccountId) = withContext(Dispatchers.IO) {
            val tradeAccountDeferred = async { tradeAccountRepo.getTradeAccounts() }
            val defaultAccountIdDeferred = async { defaultAccountRepo.getAccountId() }
            tradeAccountDeferred.await() to defaultAccountIdDeferred.await()
        }

        val selectedAccountDelegate = accountDelegateFactory.create(
            accounts.getOrElse { emptyList() },
            defaultAccountId.getOrNull()
        )

        return selectedAccountDelegate.getCurrentAccountId(SecurityType.OPTION)
            ?: accounts.getOrThrow().first().id
    }

    private fun needToLoadAccountId(formValues: InputValuesSnapshot): Boolean {
        return formValues[optionTradeFormInputId.accountId] == null
    }

    fun resultSymbol(symbol: WithSymbolInfo?) {
        setSymbolOrInvoke(symbol) {
            sendFinishSelectSignal()
        }
    }

    private fun selectOptionWithActionAndQuantity(transactionType: TransactionType, quantity: Int, id: String, isAMOption: Boolean = false) {
        optionLegProperties += OptionLegProperty(transactionType, quantity, false)
        optionChainTableViewModel?.selectOptionWithIdAndTerm(id, isAMOption)
    }

    fun selectOption(option: Option) {
        dismissAppMessage()

        if (selectedOptionLegs.value?.size == maxNumberOfOptionsLeg) {
            _appMessageSignal.value = ErrorMessage(resources.getString(R.string.trade_error_max_number_of_legs_selected, MAX_NUMBER_OF_OPTION_LEGS))
            return
        }

        selectedOptionCandidate = option
        _actions.value = optionTradeActionItemProvider.getActions()
    }

    fun deselectOption(index: Int) {
        dismissAppMessage()
        optionLegProperties.removeAt(index) // Remove properties before deselecting an option to ensure consistency.
        optionChainTableViewModel?.deselectOption(index)
    }

    fun touchItemDelete(index: Int) {
        if (touchItemDeleteForInner(index)) {
            invalidateSelectedOptionLegs()
        }
    }

    fun pendDeletion(index: Int, deletePending: Boolean) {
        var invalidate = false
        if (deletePending) {
            // ensure that we have only one that is `deletePending` state
            invalidate = touchItemDeleteForInner(index)
        }
        if (updateIsDeletePendingProperty(index, deletePending) || invalidate) {
            invalidateSelectedOptionLegs()
        }
    }

    fun updateQuantity(index: Int, newQuantity: String) {
        dismissAppMessage()

        val quantityCandidate = newQuantity.toIntOrNull()
        if (quantityCandidate == null || quantityCandidate < 0) {
            invalidateSelectedOptionLegs()
            return
        }

        if (updateProperty(index, quantityCandidate, { quantity }, { copy(quantity = it) })) {
            invalidateSelectedOptionLegs()
        }
    }

    private fun touchItemDeleteForInner(index: Int): Boolean {
        var updated = false
        for (i in optionLegProperties.indices - index) {
            updated = updated or updateIsDeletePendingProperty(i, false)
        }
        return updated
    }

    private fun updateIsDeletePendingProperty(index: Int, value: Boolean): Boolean {
        return updateProperty(index, value, { isDeletePending }, { copy(isDeletePending = it) })
    }

    private fun <T> updateProperty(
        index: Int,
        propertyCandidate: T,
        property: OptionLegProperty.() -> T,
        spawn: OptionLegProperty.(T) -> OptionLegProperty
    ): Boolean {
        val oldProperties = optionLegProperties[index]
        return if (propertyCandidate != oldProperties.property()) {
            optionLegProperties[index] = oldProperties.spawn(propertyCandidate)
            true
        } else {
            false
        }
    }

    private inline fun setSymbolOrInvoke(symbol: WithSymbolInfo?, transactionType: TransactionType? = null, block: () -> Unit) {
        if (symbol != null) {
            onSymbolSelected(symbol, transactionType)
        } else {
            block()
        }
    }

    private fun navigateBackToTrade(response: OptionTradeStrategyResponse) {
        sendFinishSelectSignal(createParamsForFinishSelectSignal(_selectedSymbol.value, response))
    }

    private fun onSymbolSelected(selectedSymbol: WithSymbolInfo, transactionType: TransactionType? = null) {
        if (selectedSymbol.isEligibleForOptionTrade(isStockAndOptions)) {
            if (transactionType != null && transactionType != TransactionType.UNKNOWN) {
                selectOptionWithActionAndQuantity(transactionType, defaultQuantity, selectedSymbol.symbol)
            }
            _selectedSymbol.value = selectedSymbol.underlier
        } else {
            sendFinishSelectSignal(createParamsForFinishSelectSignal(selectedSymbol))
        }
    }

    private fun createParamsForFinishSelectSignal(
        selectedSymbol: WithSymbolInfo?,
        response: OptionTradeStrategyResponse? = null
    ): InputValuesSnapshot {
        val securityType = securityType.value ?: SecurityType.OPTION
        return tradeFormParametersBuilderProvider.get()
            .apply {
                symbol = selectedSymbol
                optionTradeStrategy = response
                val formValues = formValues.value.orEmpty()
                priceType = optionTradeFormInputId.extractPriceType(formValues) ?: defaultPriceType
                term = optionTradeFormInputId.extractTerm(formValues) ?: defaultTerm
                accountId = this@OptionLegSelectViewModel.accountId
            }
            .create(securityType)
    }

    private fun sendFinishSelectSignal(params: InputValuesSnapshot = emptyMap()) {
        _finishSelectSignal.value = ConsumableLiveEvent(params)
    }

    private fun sendGeneralErrorMessage(retryAction: () -> Unit) {
        _appMessageSignal.value = ErrorMessage(resources.getString(R.string.error_message_general), retryAction)
    }

    private fun invalidateSelectedOptionLegs(options: List<Option?> = optionChainTableViewModel?.selectedOptions?.value.orEmpty()) {
        tradeLegBidAskViewModel?.unsubscribeBidAsk()
        val selectedOptionLegs = options.mapIndexedNotNull { index, option ->
            optionLegProperties[index].let { property ->
                option?.let {
                    OptionTradeLegWithOption(
                        option = it,
                        transactionType = property.action,
                        quantity = property.quantity,
                        isAMOption = it.isAMOption,
                        isDeletePending = property.isDeletePending
                    )
                } ?: optionChainTableViewModel?.getSelectedOptionIdWithIndex(index)?.let { selectedSymbol ->
                    OptionTradeLeg(
                        transactionType = property.action,
                        quantity = property.quantity,
                        symbol = selectedSymbol,
                        displaySymbol = getDisplaySymbolFromOsiKey(selectedSymbol) ?: "",
                        isAMOption = false,
                        isDeletePending = property.isDeletePending
                    )
                }
            }
        }
        _selectedOptionLegs.value = selectedOptionLegs
        tradeLegBidAskViewModel?.subscribeToBidAsk(createTradeLegs(), false)
    }

    fun updateStockOrderQuantity(newQuantity: String) {
        newQuantity.toIntOrNull()?.let { quantity ->
            _stockTradeLeg.value?.copy(quantity = quantity)?.let {
                _stockTradeLeg.value = it
                invalidateSelectedOptionLegs()
            }
        }
    }

    fun updateStockOrderType(orderType: TransactionType) {
        _stockTradeLeg.value?.copy(transactionType = orderType)?.let {
            _stockTradeLeg.value = it
            invalidateSelectedOptionLegs()
        }
    }

    private fun createStockTradeLeg(
        action: TransactionType = TransactionType.BUY_OPEN,
        quantity: Int = DEFAULT_STOCK_AND_OPTIONS_STOCK_QUANTITY,
        symbol: String = selectedSymbol.value?.symbol.orEmpty()
    ): StockTradeLeg = StockTradeLeg(
        symbol = symbol,
        transactionType = action,
        quantity = quantity
    )

    private fun createTradeLegs(): List<TradeLeg> {
        val tradeLegs = mutableListOf<TradeLeg>()
        _stockTradeLeg.value?.let {
            tradeLegs.add(it)
        }
        _selectedOptionLegs.value?.let {
            tradeLegs.addAll(it)
        }
        return tradeLegs
    }

    private data class OptionLegProperty(
        val action: TransactionType,
        val quantity: Int,
        val isDeletePending: Boolean
    )
}
