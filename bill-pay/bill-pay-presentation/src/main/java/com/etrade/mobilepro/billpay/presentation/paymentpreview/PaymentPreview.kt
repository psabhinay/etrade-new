package com.etrade.mobilepro.billpay.presentation.paymentpreview

import android.os.Parcelable
import com.etrade.mobile.accounts.details.AccountDetails
import com.etrade.mobilepro.accounts.util.android.AccountDetailsParceler
import com.etrade.mobilepro.billpay.api.PaymentRequest
import com.etrade.mobilepro.billpay.presentation.common.ParcelablePayee
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import org.threeten.bp.LocalDate
import java.math.BigDecimal

@Parcelize
data class PaymentPreview(
    val originAccount: @WriteWith<AccountDetailsParceler> AccountDetails,
    val destinationAccount: ParcelablePayee,
    val amount: BigDecimal,
    val scheduleDate: LocalDate,
    val memo: String?
) : Parcelable

fun PaymentPreview.toRequest() = PaymentRequest(
    amount = amount,
    scheduleDate = scheduleDate,
    memo = memo,
    originAccount = PaymentRequest.OriginAccount(
        accountId = originAccount.accountId,
        instNo = originAccount.instNo.toString()
    ),
    destinationAccount = PaymentRequest.DestinationAccount(
        payeeId = requireNotNull(destinationAccount.payeeId),
        leadDays = destinationAccount.leadDays
    )
)
