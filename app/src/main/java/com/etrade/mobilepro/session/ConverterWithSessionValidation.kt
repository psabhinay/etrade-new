package com.etrade.mobilepro.session

import com.etrade.mobilepro.backends.af.ResultDto
import com.etrade.mobilepro.backends.api.ServerError
import com.etrade.mobilepro.backends.neo.BaseDataDto
import com.etrade.mobilepro.backends.neo.ServerResponseDto
import okhttp3.ResponseBody
import retrofit2.Converter

private const val INVALID_SESSION_ERROR_CODE = 9793
private const val INVALID_CSRF_TOKEN_ERROR_MESSAGE = "Invalid CSRF Token"

abstract class ConverterWithSessionValidation<T>(
    private val converter: Converter<ResponseBody, T>,
    private val sessionTerminationStateProvider: SessionTerminationStateProvider
) : Converter<ResponseBody, T> {

    override fun convert(value: ResponseBody): T? {
        val result = converter.convert(value)

        if (isSessionExpired(result)) {
            sessionTerminationStateProvider.notifySessionTermination()
        }

        return result
    }

    abstract fun isSessionExpired(conversionResult: T?): Boolean
}

class SimpleXmlConverterWithSessionValidation<T>(
    converter: Converter<ResponseBody, T>,
    sessionTerminationStateProvider: SessionTerminationStateProvider
) : ConverterWithSessionValidation<T>(converter, sessionTerminationStateProvider) {

    override fun isSessionExpired(conversionResult: T?): Boolean {
        (conversionResult as? Any)?.javaClass?.methods?.find { it.returnType == ResultDto::class.java }?.invoke(conversionResult)?.let { dto ->
            (dto as? ResultDto)?.fault?.errors?.find { it.errorCode == INVALID_SESSION_ERROR_CODE }?.let {
                return true
            }
        }

        return false
    }
}

class MoshiConverterWithSessionValidation<T>(
    converter: Converter<ResponseBody, T>,
    sessionTerminationStateProvider: SessionTerminationStateProvider
) : ConverterWithSessionValidation<T>(converter, sessionTerminationStateProvider) {

    override fun isSessionExpired(conversionResult: T?): Boolean {
        if (conversionResult is ServerResponseDto<BaseDataDto>) {
            (conversionResult.data?.extractError() as? ServerError.Known)?.messages?.find { it.text == INVALID_CSRF_TOKEN_ERROR_MESSAGE }?.let {
                return true
            }
        }

        return false
    }
}
