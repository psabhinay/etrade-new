package com.etrade.mobilepro.learn.presentation.library

import com.etrade.mobilepro.common.di.DeviceType
import com.etrade.mobilepro.common.di.Web
import org.chromium.customtabsclient.shared.WebViewFragment
import javax.inject.Inject

private const val LIBRARY_URL = "/knowledge/library/getting-started"

class LibraryFragment @Inject constructor(
    @DeviceType deviceType: String,
    @Web private val baseWebUrl: String
) : WebViewFragment(deviceType) {

    override fun getUrl(): String {
        return baseWebUrl + LIBRARY_URL
    }
}
