// Code generated by moshi-kotlin-codegen. Do not edit.
@file:Suppress("DEPRECATION", "unused", "ClassName", "REDUNDANT_PROJECTION",
    "RedundantExplicitType", "LocalVariableName", "RedundantVisibilityModifier",
    "PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package com.etrade.mobilepro.chart.performance.`data`

import com.etrade.mobilepro.chart.`data`.ChartDto
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import com.squareup.moshi.Moshi
import com.squareup.moshi.`internal`.Util
import java.lang.NullPointerException
import kotlin.String
import kotlin.Suppress
import kotlin.Unit
import kotlin.collections.emptySet
import kotlin.text.buildString

internal class PerformanceChartResponseDataDtoJsonAdapter(
  moshi: Moshi
) : JsonAdapter<PerformanceChartResponseDataDto>() {
  private val options: JsonReader.Options = JsonReader.Options.of("performance")

  private val chartDtoAdapter: JsonAdapter<ChartDto> = moshi.adapter(ChartDto::class.java,
      emptySet(), "performance")

  public override fun toString(): String = buildString(53) {
      append("GeneratedJsonAdapter(").append("PerformanceChartResponseDataDto").append(')') }

  public override fun fromJson(reader: JsonReader): PerformanceChartResponseDataDto {
    var performance: ChartDto? = null
    reader.beginObject()
    while (reader.hasNext()) {
      when (reader.selectName(options)) {
        0 -> performance = chartDtoAdapter.fromJson(reader) ?:
            throw Util.unexpectedNull("performance", "performance", reader)
        -1 -> {
          // Unknown name, skip it.
          reader.skipName()
          reader.skipValue()
        }
      }
    }
    reader.endObject()
    return PerformanceChartResponseDataDto(
        performance = performance ?: throw Util.missingProperty("performance", "performance",
            reader)
    )
  }

  public override fun toJson(writer: JsonWriter, value_: PerformanceChartResponseDataDto?): Unit {
    if (value_ == null) {
      throw NullPointerException("value_ was null! Wrap in .nullSafe() to write nullable values.")
    }
    writer.beginObject()
    writer.name("performance")
    chartDtoAdapter.toJson(writer, value_.performance)
    writer.endObject()
  }
}
