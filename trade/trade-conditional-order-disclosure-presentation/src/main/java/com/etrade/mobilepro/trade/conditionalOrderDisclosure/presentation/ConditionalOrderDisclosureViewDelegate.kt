package com.etrade.mobilepro.trade.conditionalOrderDisclosure.presentation

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import com.etrade.mobilepro.dialog.AppDialog
import com.etrade.mobilepro.dialog.showDialog
import com.etrade.mobilepro.overlay.SpannableTextOverlayActivity
import com.etrade.mobilepro.util.ErrorMessage
import com.etrade.mobilepro.util.android.ConsumableLiveEvent
import com.etrade.mobilepro.util.android.consume
import com.etrade.mobilepro.util.android.snackbar.SnackBarUtil
import com.etrade.mobilepro.viewdelegate.ViewDelegate
import com.google.android.material.snackbar.Snackbar

class ConditionalOrderDisclosureViewDelegate(
    private val context: Context,
    private val snackBarUtil: SnackBarUtil,
    private val viewModel: BaseConditionalOrderDisclosureViewModel
) : ViewDelegate {

    var currentSnackBar: Snackbar? = null

    override fun observe(owner: LifecycleOwner) {
        viewModel.errorMessage.observe(owner) { onErrorMessage(it) }
        viewModel.showDisclosureDialog.observe(owner) { onDisclosureDialog(it) }
        viewModel.showFullDisclosure.observe(owner) { onDisclosureWebPage(it) }
    }

    private fun onErrorMessage(event: ConsumableLiveEvent<ErrorMessage>) {
        event.consume { message ->
            val snackBar = message.retryAction?.let {
                snackBarUtil.retrySnackbar(message = message.message, actionOnRetry = it)
            } ?: snackBarUtil.errorSnackbar(message = message.message)
            currentSnackBar?.dismiss()
            snackBar?.let {
                it.show()
                currentSnackBar = it
            }
        }
    }

    private fun onDisclosureDialog(dialog: AppDialog?) {
        dialog?.let { context.showDialog(it) }
    }

    private fun onDisclosureWebPage(event: ConsumableLiveEvent<Unit>) {
        event.consume {
            with(context) {
                val title = getString(R.string.conditional_order_disclosure_title)
                val message = getString(R.string.conditional_order_disclosure_full)
                startActivity(SpannableTextOverlayActivity.intent(context, title, message))
            }
        }
    }
}
