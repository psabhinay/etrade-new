package com.etrade.mobilepro.dynamic.form.api

import com.etrade.mobilepro.dynamic.form.value.InputValuesSnapshot

interface InputValueHolder : InputValueCollector {

    /**
     * Disable interactions with this holder.
     */
    fun disabledFields(list: List<String>)

    /**
     * Injects values from a [map].
     *
     * @param map The map of values to inject.
     */
    fun injectValuesFrom(map: InputValuesSnapshot)

    /**
     * Resets values of this holder to their defaults.
     */
    fun resetValues()
}
