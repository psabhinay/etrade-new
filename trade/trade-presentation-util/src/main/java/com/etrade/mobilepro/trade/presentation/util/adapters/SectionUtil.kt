package com.etrade.mobilepro.trade.presentation.util.adapters

import android.content.res.Resources
import androidx.annotation.StringRes
import com.etrade.mobilepro.trade.api.Order
import com.etrade.mobilepro.trade.presentation.util.R
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.TableSectionItemLayout
import com.etrade.mobilepro.uiwidgets.tablesectionsview.model.createSummarySection

internal fun accountItem(resources: Resources, response: Order): TableSectionItemLayout? {
    return sectionItem(resources, R.string.trade_label_account, response.account)
}

internal fun sectionItem(resources: Resources, @StringRes labelRes: Int, @StringRes valueRes: Int): TableSectionItemLayout? {
    return sectionItem(resources, labelRes, resources.getString(valueRes))
}

internal fun sectionItem(resources: Resources, @StringRes labelRes: Int, value: String?): TableSectionItemLayout? {
    return value?.let { createSummarySection(resources.getString(labelRes), it) }
}
